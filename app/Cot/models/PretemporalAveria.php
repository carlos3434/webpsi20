<?php
namespace Cot\models;
class PretemporalAveria extends \Eloquent
{

    public $table = 'pre_temporales_averias';
    protected $fillable = 
    [   'id','pre_temporal_id', 'codactu', 'estado_legado',
        'fecha_registro_legado', 'fecha_liquidacion', 'cod_liquidacion',
        'detalle_liquidacion',
        'codcli',
        'nombre_cliente',
        'area',
        'contrata',
        'zonal',
        'proceso',
        'mdf',
        'observacion'
    ];

    /**
     * Pretemporal relationship
     */
    public function pretemporal()
    {
        return $this->hasMany('PreTemporal');
    }
    public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = \Auth::id();
        } );
        static::creating( function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
    public static function guardarAveria()
    {
        //DB::beginTransaction();

        // try {
            DB::table('pre_temporales_averias')
            ->insert(
                array(
                    'pre_temporal_id'       => \Input::get('id'),
                    'codactu'               => \Input::get('codactu'),
                    'estado_legado'         =>\Input::get('estado_legado', ''),
                    'fecha_registro_legado' =>\Input::get('fecha_registro_legado', ''),
                    'fecha_liquidacion'     =>\Input::get('fecha_liquidacion', ''),
                    'cod_liquidacion'       =>\Input::get("cod_liquidacion", ''),
                    'detalle_liquidacion'   =>\Input::get("detalle_liquidacion", ''),
                    'codcli'                =>\Input::get("codcli", ''),
                    'nombre_cliente'        =>\Input::get("nombre_cliente", ''),
                    'area'                  =>\Input::get("area", ''),
                    'contrata'              =>\Input::get("contrata", ''),
                    'zonal'                 =>\Input::get("zonal", ''),
                    'proceso'               =>\Input::get("proceso", ''),
                    'usuario_created_at'    => \Auth::id(),
                    'created_at'            => date('Y-m-d h:i:s', time()),
                    'usuario_updated_at'    => \Auth::id(),
                    'updated_at'            => date('Y-m-d h:i:s', time())
                )
            );
           // DB::commit();
            return 1;
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return 0;
        // }
    }

    public static function actualizarAveria($update)
    {
        //DB::beginTransaction();

        // try {
            \DB::table('pre_temporales_averias')
            ->where('pre_temporal_id', $update['id'])
            ->update(
                array(
                    'codactu'               =>$update['codactu'],
                    'estado_legado'         =>$update['estado_legado'],
                    'fecha_registro_legado' =>$update['fecha_registro_legado'],
                    'fecha_liquidacion'     =>$update['fecha_liquidacion'],
                    'cod_liquidacion'       =>$update["cod_liquidacion"],
                    'detalle_liquidacion'   =>$update["detalle_liquidacion"],
                    'codcli'                =>$update["codcli"],
                    'nombre_cliente'        =>$update["nombre_cliente"],
                    'area'                  =>$update["area"],
                    'contrata'              =>$update["contrata"],
                    'zonal'                 =>$update["zonal"],
                    'proceso'               =>$update["proceso"],
                    'usuario_updated_at'    =>Auth::id(),
                    'updated_at'            =>date('Y-m-d h:i:s', time())
                )
            );
           // DB::commit();
            return 1;
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return 0;
        // }
    }
}
