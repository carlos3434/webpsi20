<?php
namespace Cot\models;

class ComandoLiquidacion extends \Eloquent
{
    //protected $table = "actividades";
    public function __construct()
    {
         $this->table = "comandos_liquidacion";
    }

    public function dias()
    {
        return $this->hasMany('Cot\models\ComandoDia', 'comando_id', 'id');
    }

    public function quiebres()
    {
        return $this->hasMany('Cot\models\ComandoQuiebre', 'comando_id', 'id');
    }

    public function atenciones()
    {
        return $this->hasMany('Cot\models\ComandoAtencion', 'comando_id', 'id');
    }
}

