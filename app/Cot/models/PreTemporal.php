<?php
namespace Cot\models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Cot\controllers\PreTemporalController;

class PreTemporal extends \Eloquent
{
    public $table = 'pre_temporales';
    protected $dates = ['deleted_at'];

    protected $fillable = 
    [   'id','quiebre_id', 'actividad_id', 'gestion', 'codactu', 'codcli',
        'actividad_tipo_id', 'tipo_fuente', 'tipo_atencion',
        'estado_pretemporal_id',
        'cliente_nombre',
        'cliente_celular',
        'cliente_correo',
        'cliente_telefono',
        'cliente_dni',
        'contacto_nombre',
        'contacto_celular',
        'contacto_correo',
        'contacto_telefono',
        'contacto_dni',
        'embajador_nombre',
        'embajador_celular',
        'embajador_correo',
        'embajador_dni',
        'comentario',
        'fecha_registro_legado',
        'validado'
    ];
    public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating( function($table) {
            if (1014 != \Auth::id()) {
                $table->embajador_nombre = \Session::get('full_name');
                $table->embajador_dni = \Session::get('dni');
                $table->embajador_celular = \Session::get('celular');
                $table->embajador_correo = \Session::get('email');
            }
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function movimientos()
    {
        return $this->hasMany('Cot\models\PretemporalMovimiento');
    }

    public function ultimoMovimiento()
    {
        return $this->hasOne('Cot\models\PretemporalMovimiento')->latest();
    }

    public function ultimo()
    {
        return $this->hasOne('Cot\models\PretemporalUltimo');
    }

    public function pretemporalAveria()
    {
        return $this->hasMany('Cot\models\PretemporalAveria', 'pre_temporal_id', 'id');
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setCodact($codactu){
        $this->codactu = $codactu;
    }

    public function getCodact(){
        return $this->codactu;
    }

    public function setCodcli($codcli){
        $this->codcli = $codcli;
    }

    public function getCodcli(){
        return $this->codcli;
    }
    
    public static $rules = [
        'quiebre_id' => array('required' => true,'table'=>'quiebres','field'=>'nombre'),
        'tipo_atencion' => array('required'=> true,'table'=>'pre_temporales_atencion','field'=>'nombre'),
        'tipo_fuente' => array('required'=> true,'table'=>'pre_temporales_rrss','field'=>'nombre'),
        'actividad_tipo_id' => array('required'=> true,'table'=>'actividades_tipos','field'=>'apocope'),
        'motivo' => array('required' => true, 'table' => 'motivos','field'=>'nombre'),
        'sub_motivo' => array('required' => true, 'table' => 'submotivos','field'=>'nombre'),
        'estado_id' => array('required' => true, 'table' => 'estados','field'=>'nombre'),
        'tipo_solucion' => array('required' => false, 'table' => 'pre_temporales_solucionado','field'=>'accion'),
        'cod_multigestion' => array(
            'compound' => true, 
            'data' => 
                array(
                    ['type'=>'duplicate','field0'=> 'cod_multigestion','values'=>'','result' => false],
                    ['type'=>'validate','table'=>'','field1'=> 'quiebre_id','values'=>'51,52,56','result'=> true]
                )
        )
    ];
}

