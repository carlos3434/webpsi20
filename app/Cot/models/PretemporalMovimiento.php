<?php namespace Cot\models;

class PretemporalMovimiento extends \Eloquent
{

    public $table = 'pre_temporales_movimientos';
    protected $fillable = 
    [   'pre_temporal_id',
        'codcli', 
        'motivo_id',
        'submotivo_id',
        'estado_id',
        'estado_pretemporal_id',
        'observacion',
        'pre_temporal_solucionado_id',
        'usuario_created_at'
    ];

    public static $rules = [
        'pre_temporal_id'=> 'required',
        'codcli'=> 'required',
        'motivo_id'=> 'required',
        'submotivo_id'=> 'required',
        'estado_id'=> 'required',
        'estado_pretemporal_id'=> 'required',
        'observacion'=> 'required',
        'pre_temporal_solucionado_id' => '' ,
        'usuario_created_at' => ''        
    ];

    public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = \Auth::id();
        } );
        static::creating( function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    /**
     * relationship to pretemporal
     */
    public function PreTemporal()
    {
        return $this->belongsTo('PreTemporal');
    }

    public static function crearMovimiento()
    {
        $datos=array();
        $datos=\Input::all();
        $ticket=\Input::get('id');

        \DB::beginTransaction();

        try {
            //guardar movimiento
            \DB::table('pre_temporales_movimientos')
            ->insert(
                array(
                    'pre_temporal_id'               => $datos['id'],
                    'codcli'                        =>$datos['codcli'],
                    'motivo_id'                     =>$datos['motivo'], //$datos['motivo_id']
                    'submotivo_id'                  =>$datos['submotivo'],//$datos['submotivo_id']
                    'estado_id'                     =>$datos['estado'], //$datos['estado_id']
                    'estado_pretemporal_id'         =>$datos["estado_pretemporal_id"],
                    'observacion'                   =>trim($datos["observacion"]),
                    'pre_temporal_solucionado_id'   =>$datos["solucionado"],//$datos["solucionado_id"],
                    'usuario_created_at'            => \Auth::id(),
                    'created_at'                    => date('Y-m-d H:i:s'),
                    'usuario_updated_at'            => \Auth::id(),
                    'updated_at'                    => date('Y-m-d H:i:s')
                )
            );

            //actualiza estado de ticket
            \DB::table('pre_temporales')
                ->where('id', $datos['id'])
                ->update(
                    array(
                        'estado_pretemporal_id'=>$datos["estado_pretemporal_id"],
                        'usuario_updated_at' => \Auth::id(),
                        'updated_at' => date('Y-m-d H:i:s', time())
                    )
                );


            //Obtener ultimo movimiento antes de grabar la gestion
            $ultmov = \DB::table('pre_temporales_ultimos')
            ->where('pre_temporal_id', $datos['id'])
            ->first();
            
            if ($ultmov == NULL) {
                \DB::table('pre_temporales_ultimos')
                ->insert(
                    array(
                        'pre_temporal_id'       => $datos['id'],
                        'estado_pretemporal_id' => 1,
                        'usuario_created_at'    => \Auth::id(),
                        'created_at'            => date('Y-m-d H:i:s')
                    )
                );
            }
    
            \DB::table('pre_temporales_ultimos')
                ->where('pre_temporal_id', $ticket)
                ->update(
                    array(
                        'codcli'                        =>$datos['codcli'],
                        'motivo_id'                     =>$datos['motivo'], //$datos['motivo_id']
                        'submotivo_id'                  =>$datos['submotivo'], //$datos['submotivo_id']
                        'estado_id'                     =>$datos['estado'], //$datos['estado_id']
                        'estado_pretemporal_id'         =>$datos["estado_pretemporal_id"],
                        'observacion'                   =>trim($datos["observacion"]),
                        'pre_temporal_solucionado_id'   =>$datos["solucionado"], //$datos["solucionado_id"]
                        'usuario_updated_at'            => \Auth::id(),
                        'updated_at'                    => date('Y-m-d H:i:s')
                    )
                );

            \DB::commit();
            return 1;
        } catch (\Exception $exc) {
            \DB::rollback();
            $errorController = new ErrorController();
            $errorController->saveError($exc);     
            return 0;
        }
    }

    public static function listar_movimientos($id)
    {
	
        $data = \DB::table('pre_temporales_movimientos as ptm')
        ->join('motivos as m', 'ptm.motivo_id', '=', 'm.id')
        ->join('submotivos as sm', 'ptm.submotivo_id', '=', 'sm.id')
        ->join('estados as e', 'ptm.estado_id', '=', 'e.id')
        ->join('estados_pre_temporales as ept', 'ptm.estado_pretemporal_id', '=', 'ept.id')
        ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
        ->Leftjoin('pre_temporales_solucionado as pts', 'ptm.pre_temporal_solucionado_id', '=', 'pts.id')
        ->select(
            'ptm.id as id',
            'm.nombre as motivo',
            'sm.nombre as submotivo',
            'e.nombre as estado',
            'ept.nombre as estado_pretemporal',
            'ptm.observacion as observacion',
            \DB::raw('DATE_FORMAT(ptm.created_at,"%d-%m-%y %H:%i:%s") as fecha'),
            \DB::raw("CONCAT(u.nombre,' ',u.apellido) as usuario"),
            \DB::raw('IFNULL(CONCAT(pts.producto," - ", pts.accion), " ") as solucionado')
        )
        ->where('ptm.pre_temporal_id', '=', $id)
        ->orderBy('ptm.created_at', 'desc')
        ->get();
	
        return $data;
    }
}

