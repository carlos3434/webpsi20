<?php
namespace Cot\models;
class EventoMasiva extends \Eloquent {
	protected $fillable = 
    [   'id','nombre','nodo', 'troba', 'empresa_id', 'zonal', 'tecnico_liquidacion',
        'tipo_trabajo', 'tecnologia','amplificador','cod_liquidacion','sub_cod_liquidacion','cod_masiva','observacion','estado','fecha','inactivo','obs_cancelacion'
    ];
	public $table = 'eventos_masiva';


    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

	public function listar(){
		$query=  \DB::table('eventos_masiva as em')
                        ->select(
                            'em.id', 
                            'em.nombre'
                        )
                        ->where(function($query){
                                if ( \Input::get('selector') ) {
                                    $query->where('em.estado','<>','2');
                                }
                                $query->where('em.estado','<>','8');
                            }
                        )                        
                        ->get();
        return (count($query)>0) ? $query : false;
	}
}
