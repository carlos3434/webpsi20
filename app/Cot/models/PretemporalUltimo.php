<?php namespace Cot\models;

class PretemporalUltimo extends \Eloquent
{

    public $table = 'pre_temporales_ultimos';
    protected $fillable = 
    [   'pre_temporal_id',
        'codcli',
        'motivo_id',
        'submotivo_id',
        'estado_id',
        'estado_pretemporal_id',
        'observacion',
        'pre_temporal_solucionado_id'
    ];

    public static $rules = [
        'pre_temporal_id'=> 'required',
        'codcli'=> 'required',
        'motivo_id'=> 'required',
        'submotivo_id'=> 'required',
        'estado_id'=> 'required',
        'estado_pretemporal_id'=> 'required',
        'observacion'=> 'required',
        'pre_temporal_solucionado_id' => '',
        'usuario_created_at' => ''     
    ];

    public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating( function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function PreTemporal()
    {
        return $this->belongsTo('PreTemporal');
    }
}

