<?php

class PretemporalUsuario extends \Eloquent {
	public $table = 'pre_temporal_usuario';
	protected $fillable = ['usuario_id','pretemporal_id'];
}
