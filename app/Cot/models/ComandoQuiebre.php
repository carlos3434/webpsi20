<?php
namespace Cot\models;

class ComandoQuiebre extends \Eloquent
{
    //protected $table = "actividades";

    public function __construct()
    {
         $this->table = "comandos_quiebres";
    }
    /**
     * Quiebre relationship
     */
    public function quiebres()
    {
        return $this->belongsToMany('Cot\models\Quiebre');
    }
}

