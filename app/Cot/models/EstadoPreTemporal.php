<?php
namespace Cot\models;

class EstadoPreTemporal extends \Eloquent
{
    //protected $table = "actividades";
    public function __construct()
    {
         $this->table = "estados_pre_temporales";
    }

    protected $fillable = array('nombre', 'estado', 'deleted_at', 'usuario_created_at', 'usuario_updated_at');
}
