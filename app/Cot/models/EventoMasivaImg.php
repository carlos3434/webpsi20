<?php
namespace Cot\models;
class EventoMasivaImg extends \Eloquent {
    protected $fillable = ['id','eventos_masiva_id','nombre_img', 'estado'];
    public $table = 'eventos_masiva_image';

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }   
}
