<?php
namespace Cot\controllers;
class UsuarioExoneracionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /usuarioexoneracion.php
	 *
	 * @return Response
	 */
	public function postCrear(){
		if (\Request::ajax()) {

			$usuario_area = \UsuarioArea::findOrFail(\Input::get('usuario_area_id'));
			$usuario_area->fecha_inicio = \Input::get('fecha_inicio');
			$usuario_area->fecha_fin = \Input::get('fecha_fin');	
			$usuario_area->usuario_updated_at = \Auth::id();
			$usuario_area->updated_at = date('Y-m-d H:i:s');
			$usuario_area->save();

			if($usuario_area->id){
			 	\UsuarioExoneracion::where('usuario_id', '=', \Input::get('usuario_id'))
	                ->update(['estado' => '0']);

				$data = [];
				$data['usuario_id'] = \Input::get('usuario_id');
				$data['fecha_inicio'] = \Input::get('fecha_inicio');
				$data['fecha_fin'] = \Input::get('fecha_fin');

				if(\Input::get('obs')){
					$data['observacion'] = \Input::get('obs');
				}

				$rst = \UsuarioExoneracion::create($data);				
				return \Response::json(
	                array(
	                    'rst' => ($rst) ? 1 : 0,
	                    'usuario_id' => \Input::get('usuario_id')
	                )
	            );	
			}else{
				return \Response::json(
	                array(
	                    'rst' => 0,
	                    'msj' => 'Error al Registrar'
	                )
	            );	
			}		
		}
	}

	public function postListar(){
		if (\Request::ajax()) {
			$objClass = new \UsuarioExoneracion(); 
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );	
		}
	}

	public function postCambiarestado(){
		if (\Request::ajax()) {
			$update = \UsuarioExoneracion::find(\Input::get('id'));

			if($update){
				$usuario_area = \UsuarioArea::where('usuario_id',$update->usuario_id)->first();
				$usuario_area->fecha_inicio = NULL;
				$usuario_area->fecha_fin = NULL;
				$usuario_area->save();

				if($usuario_area->id){
					$update->estado = 0;				
					$update->usuario_updated_at = \Auth::id();
					$update->updated_at = date('Y-m-d H:i:s');
					$update->save();					
				}
				return \Response::json(
	                array(
	                    'rst'    => 1,
	                    'msj'    => "Actualizado Correctamente",
	                    'usuario_id' => $update->usuario_id
	                )
	            );	
			}
			return false;			
		}
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /usuarioexoneracion.php/create
	 *
	 * @return \Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /usuarioexoneracion.php
	 *
	 * @return \Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /usuarioexoneracion.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /usuarioexoneracion.php/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /usuarioexoneracion.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /usuarioexoneracion.php/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
