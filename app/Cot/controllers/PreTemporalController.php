<?php
namespace Cot\controllers;

use Cot\models\PreTemporal;
use Cot\models\PretemporalMovimiento as PretemporalMovimiento;
use Cot\models\PretemporalAveria;
use Cot\models\PretemporalAdmin;
use Cot\models\PretemporalUsuario;

use Cot\Repositories\CotEmbajadorRepositoryInterface;
use Cot\Repositories\CotHelpRepositoryInterface as Helper;
use Cot\Repositories\CotMethodsRepositoryInterface as Method;
use Cot\Repositories\CotPretemporalRepositoryInterface as Model;
use Illuminate\Support\Facades\Validator;

class PreTemporalController extends \BaseController
{
    protected $_pretemporal;
    protected $_helper;
    protected $_query;
    protected $_model;
    protected $_embajador;

    public function __construct(Helper $helper,Method $query,Model $model,
        PreTemporal $pretemporal,CotEmbajadorRepositoryInterface $embajador)
    {
        $this->_helper = $helper;
        $this->_query = $query;
        $this->_model = $model;
        $this->_pretemporal = $pretemporal;
        $this->_embajador = $embajador;
    }

    public function getBuscarembajador()
    {
        $data = $this->_embajador->buscarEmbajador();
        if (count($data) > 0) {
            $datos = array(
                    'rst' => 1,
                    'tipo' => 'a',
                    'datos' => $data,
                    'msj' => 'Busqueda Completa'
                );
        } else {
            $datos = array(
                    'rst' => 0,
                    'msj' => 'No se encontraron datos.'
                );
        }
        return $datos;
    }
    /**
     * busqueda individuual
     */
    public function getCargar()
    {
        $reglas = array(
            'tipo'        => 'Required|in:ticket,codactu',
            'busqueda'    => 'Required',
        );
        $validator = \Validator::make(\Input::all(), $reglas);
        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 0,
                'msj' => $validator->messages(),
                )
            );
        }

        $tipoBusqueda = \Input::get('tipo');
        $busqueda = \Input::get('busqueda'); //cod averia o n°ticket
        $ficha = [];

        if ($tipoBusqueda == 'ticket') {
            $ficha = $this->_model->buscarTicket(strtoupper($busqueda));
            if (!$ficha) {
                return array(
                    'rst' => 0,
                    'msj' => 'No se encontraron datos.'
                );
            }

            $fecha = $ficha->fecha_registro_legado;
            $segundos = strtotime($fecha) - strtotime('now');
            $diferencia = abs(intval($segundos/60/60));
            
            $plazo = 'Fuera del Plazo';
            if ($diferencia <= 24) {
                $plazo = 'Dentro del Plazo';
            }

            $ficha->plazo = $plazo;
            $ficha->origen = '';
            $movimientos =  \Cot\models\PretemporalMovimiento::listar_movimientos($busqueda);
            
            return \Response::json(
                [
                    'rst' => 1,
                    'tipo' =>'i',
                    'ficha' => $ficha,
                    'movimientos' => $movimientos,
                    'msj' => 'Busqueda Individual.'
                ]
            );
        } elseif ($tipoBusqueda == 'codactu') { //cargo la ficha
            $datos = $this->_model->getbyCodact($busqueda);

            return \Response::json(
                [
                    'rst' => 1,
                    'tipo' =>'i',
                    'ficha' => ($datos) ? $datos[0] : array(),
                    'msj' => 'Busqueda Individual.'
                ]
            );
        }
    }
    
    /**
     * Consulta de Pedidos 
     * Bandeja de Pedidos 
     */
    public function postBuscar()
    {
        $resultado = $this->_model->busquedaTicket();
        if ($resultado) {
            return $resultado;
        } else {
            $datos = array(
                'rst' => 0,
                'msj' => 'No se encontraron datos.'
            );
            return $datos;
        }
    }

	 public function postBuscarajax()
    {
        $resultado = $this->_model->listarTickets();
        if ($resultado) {
            return $resultado;
        } else {
            $datos = array(
                'rst' => 0,
                'msj' => 'No se encontraron datos.'
            );
            return $datos;
        }
    }

    public function postBuscarreasignacion()
    {
        $resultado = $this->_model->listarTicketsReasignacion();
        if ($resultado) {
            return $resultado;
        } else {
            $datos = array(
                'rst' => 0,
                'msj' => 'No se encontraron datos.'
            );
            return $datos;
        }
    }

    
    public function postReporte()
    {        
        $data = json_decode(json_encode($this->_model->getReporteMov()), true);
        if(!$data){
            $data[] = array('Movimientos' => 'No se encontró ninguno movimiento.'); 
        }
        return \Helpers::exportArrayToCsv($data, 'Reporte_Movimientos');
    }

    public function postGrafica()
    {
        $tipo = \Input::get("tipo", 1);
        switch ($tipo) {
            case 1:
                $ahora = date("Y-m-d");
                $fecha = [0 => $ahora." 00:00:00",
                    1 => $ahora." 23:59:59"];
                $hoy = \PreTemporal::where("validado", "=", 1)
                    ->whereBetween("fecha_registro_legado", $fecha)
                    ->get()
                    ->count();
                $anterior = \PreTemporal::where("validado", "=", 1)
                    ->where("fecha_registro_legado", "<", $fecha[0])
                    ->get()
                    ->count();
                return json_encode(
                    ["hoy" => $hoy,
                    "titulo" => "Averias",
                    "anterior" => $anterior]
                );
                break;
            case 2:
                $resultado = ["titulo" =>
                    "Fichas (Estados vs Dias)", "ytitulo" => "Total de Fichas"];
                $resultado ["dias"] = [];
                $hoy = date("Y-m-d");
                for ($i = 7; $i > 0; $i--) {
                    $resultado["dias"][] = date(
                        "Y-m-d", strtotime(
                            '-'.$i.' day', strtotime($hoy)
                        )
                    );
                }
                return json_encode($resultado);
                break;
            default:
                break;
        }
    }
    /**
     * guardar formulario de rrss
     */
    public function postRegistrar()
    {
        $reglas = array(
            'actividad_tipo_id'     => 'Required',
            'quiebre_id'            => 'Required',
            'tipo_atencion'         => 'Required',
            'cliente_nombre'        => 'Required',
            'cliente_celular'       => 'Required',
            'codcli'                => 'Required',
        );

        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'AlphaNum'      => ':attribute Solo debe ser alfanumerico'
        );

        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return \Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                )
            );
        }
        $this->_pretemporal->fill(\Input::all())->save();

        $ave = [
            'pre_temporal_id' => $this->_pretemporal->getId(),
            'codactu' => $this->_pretemporal->getCodact(),
            'codcli'=>  $this->_pretemporal->getCodcli(),
            'gestion'=>'psi','proceso'=>1
        ];

        $averia = new PretemporalAveria();
        $averia->fill((array)$ave);
        $this->_pretemporal->pretemporalAveria()->save($averia);
        
        return \Response::json(array(
            'rst' => '1',
            'ticket' => 'E'. str_pad($this->_pretemporal->getId(), 6, "0", STR_PAD_LEFT),
            'msj' => 'Por favor guarde el N°Ticket para futuras consultas.'
        ));
    }

    public function postRecepcion()
    {     
        $rgx =  array("/","_","-","#","$");
        $actividadTipoId = \Input::get('tipo_averia');
        $preTemporalAveria['pre_temporal_id'] = \Input::get('id');
        $preTemporalAveria['codcli']          = \Input::get('codcli');
        $preTemporalAveria['motivo_id']       = \Input::get('motivo');
        $preTemporalAveria['submotivo_id']    = \Input::get('submotivo');
        $preTemporalAveria['estado_id']       = \Input::get('estado');
        $preTemporalAveria['observacion']     = \Input::get('observacion');

        if (\Input::has('codactu_val')) {
            $preTemporalAveria['codactu'] =\Input::get('codactu_val');
            list($preTemporalAveria['codactu']) = explode('|', \Input::get('codactu_val'));
            $preTemporalAveria['proceso'] = 2;
        }
        //if (\Input::has('solucionado')) {
        if (\Input::has('solucionado') && \Input::get('solucionado')!='' ) {
            $preTemporalAveria['solucionado_id'] =\Input::get('solucionado');
        } else {
            $preTemporalAveria['solucionado_id'] =0;  //dd(\Input::get('solucionado'));
        }

        //se registra averia
        if (\Input::has('codactu') && \Input::get('codactu') != '' && \Input::get('submotivo')== 60) {
            $preTemporalAveria['codactu'] = str_replace($rgx, '', \Input::get('codactu'));
            $preTemporalAveria['proceso'] = 1;
        }
        //tiene reg averia
        if (\Input::has('codactu') && \Input::get('codactu') != '' && \Input::get('submotivo')== 75) {
            $preTemporalAveria['codactu'] =\Input::get('codactu');
            $preTemporalAveria['proceso'] = 1;
        }

        //estado ticket
        switch (\Input::get('motivo')) {
            case '23':
            case '24':
            case '25': 
            case '28': 
            case '31': 
            case '32':
            case '33':  
            case '18': $preTemporalAveria["estado_pretemporal_id"] = 2;
                break;
            case '19': $preTemporalAveria["estado_pretemporal_id"] = 3;
                break;
            case '20': $preTemporalAveria["estado_pretemporal_id"] = 4;
                break;
            case '21': $preTemporalAveria["estado_pretemporal_id"] = 6;
                break;
            case '22': $preTemporalAveria["estado_pretemporal_id"] = 5;
                break;
            default: $preTemporalAveria["estado_pretemporal_id"] = 1;
                break;
        }
        \Input::merge(array('estado_pretemporal_id' => $preTemporalAveria["estado_pretemporal_id"]));
        
        //\Input::replace($preTemporalAveria);

        //submotivo: ticket con averia
        if (\Input::has('codactu_val')) {
            list($codactu,$codcli,$fecha ) = explode('|', \Input::get('codactu_val'));
            \PreTemporal::findOrFail(\Input::get('id'))->update(
                [
                'codactu'                => $codactu,
                /*'codcli'                 => $codcli,*/
                'fecha_registro_legado'  => $fecha,
                'validado'               => 1,
                ]
            );
            $averia=\PretemporalAveria::where('pre_temporal_id', \Input::get('id'));

            if ($averia == null) {
                \PretemporalAveria::create($preTemporalAveria);
            } else {
                $legados = $this->buscarLegados(
                    $actividadTipoId, \Input::get('codactu'), \Input::get('codcli')
                );

                if (count($legados) > 0) { $legado = $legados[0];
                    $averia->update(
                        [
                        'codactu'               => $legado->codactu,
                        'estado_legado'         => $legado->estado,
                        'fecha_registro_legado' => $legado->fecha_registro,
                        'fecha_liquidacion'     => \Input::get('fecha_liquidacion'),
                        'cod_liquidacion'       => \Input::get("cod_liquidacion", ''),
                        'detalle_liquidacion'   => \Input::get("detalle_liquidacion", ''),
                        'codcli'                => $legado->telefono,
                        'nombre_cliente'        => \Input::get("nombre_cliente", ''),
                        'area'                  => $legado->area,
                        'contrata'              => \Input::get("contrata", ''),
                        'zonal'                 => \Input::get("zonal", ''),
                        'proceso'               => 1,
                        ]
                    );
                }
            }
        }
        //submotivo: 60: Se registra averia | 75: tiene reg avgeria
        if (\Input::has('codactu') && \Input::get('submotivo')== 60
                || \Input::has('codactu') && \Input::get('submotivo')== 75) {
            PreTemporal::findOrFail(\Input::get('id'))->update(
                [
                    'codactu' => str_replace($rgx, '', \Input::get('codactu'))
                ]
            );

            $averia = \DB::table('pre_temporales_averias')
                ->where('pre_temporal_id', \Input::get('id'))
                ->get();

            if ($averia == null) {
                \PretemporalAveria::create($preTemporalAveria);
            } else {
                 \PretemporalAveria::findOrFail($averia[0]->id)->update(
                     [
                     'codactu'    => str_replace($rgx, '', \Input::get('codactu')),
                     'codcli'     => \Input::get('codcli'),
                     'proceso'    => 1,
                     ]
                 );
            }
        }
        $result =  PretemporalMovimiento::crearMovimiento();

        if ($result == 0) {
            $mensaje = 'No se puedo completar el movimiento';
            return \Response::json(
                array(
                    'rst'    => 0,
                    'ticket' => \Input::get('id'),
                    'msj'    => 'No se puedo completar el movimiento',
                )
            );
        }

        /*if ticket its part of a event, we have to validate if it have already achieve their porcenture */
        $successfully = 0;
        if(\Input::get('idevento')){
            $successfully = $this->_model->calcCumpleEvento(\Input::get('idevento'));
        }
        /*end if*/

        return \Response::json(
            array(
                'rst'    => 1,
                'ticket' => \Input::get('id'),
                'msj'    => 'Movmiento creado',
                'achieve' => $successfully,
            )
        );
    }

    /*
        @tipo_id = 4 (television|CATV), 5 (telefono|STB), 6 (internet|ADSL)
        @1 -> where es AND
        @0 -> where es OR
        @television | CATV
    */
    public function buscarLegados($actividadTipoId, $codactu, $codcli)
    {
        $tipoId = $actividadTipoId;
        $result  = '';
        if ($tipoId == 4) {
            $result = $this->_model->getAveriaPenCatvPais($codactu, $codcli, 1);
            if (count($result) == 0) {
                $result = $this->_model->getAveriaPenCatvPais($codactu, $codcli, 0);
            }

            if (count($result) == 0) {
                $result = $this->_model->getAveriaPendMacro($codactu, $codcli, 1);
            }
        } elseif ($tipoId == 5) {
            if ($codcli) {
                $result = $this->_model->getAveriaPenAdslPais($codactu, $codcli, 0);
                //si tiene 1 adelante, buscar en Lima, sino en provincias
                if (count($result) == 0) {
                    if (substr($codcli, 0, 1) == 1) {
                        $telefono = substr($codcli, 1);
                        $result = $this->_model->getAveriaPenBasLima($codactu, $telefono, 1);
                        if (count($result) == 0) {
                            $result = $this->_model->getAveriaPenBasLima($codactu, $telefono, 0);
                        }
                    } else {
                        $telefono = substr($codcli, 2);
                        $result = $this->_model->getAveriaPenBasProv($codactu, $telefono, 1);
                        if (count($result) == 0) {
                            $result = $this->_model->getAveriaPenBasProv($codactu, $telefono, 0);
                        }
                    }
                    if (count($result) == 0) {
                        $telefono = $codcli;
                        $result = $this->_model->getAveriaPenBasLima($codactu, $telefono, 1);
                        if (count($result) == 0) {
                            $result = $this->_model->getAveriaPenBasLima($codactu, $telefono, 0);
                        }
                        if (count($result) == 0) {
                            $result = $this->_model->getAveriaPenBasProv($codactu, $telefono, 1);
                        }
                        if (count($result) == 0) {
                            $result = $this->_model->getAveriaPenBasProv($codactu, $telefono, 0);
                        }
                    }
                }
            } else {
                $telefono = $codcli;
                $result = $this->_model->getAveriaPenBasLima($codactu, $telefono, 0);
                if (count($result) == 0) {
                    $result = $this->_model->getAveriaPenBasProv($codactu, $telefono, 0);
                }
            }
        } elseif ($tipoId == 6) { //internet
            $result = $this->_model->getAveriaPenAdslPais($codactu, $codcli, 1);
            
            if (count($result) == 0) {
                $result = $this->_model->getAveriaPenAdslPais($codactu, $codcli, 0);
            }

            if (count($result) == 0) {
                $result = $this->_model->getAveriaPendMacro($codactu, $codcli, 1);
            }
        }
        return $result;
    }

    public function postListarmovimiento()
    {
        $id = \Input::get('busqueda');
        $data = PretemporalMovimiento::listar_movimientos($id);
        $datos = array(
            'rst' => 1,
            'data' => $data
        );
        return \Response::json($datos);
    }

    public function buscarLegadosProcauto($tipoActividad, $codactu)
    {
        $codcli = '';
        $result = $this->_model->getAveriaPenCatvPais($codactu, $codcli, 0);
        $actividadTipoId = 4; //television

        if (count($result) == 0) {
            $result = $this->_model->getAveriaPenBasLima($codactu, $codcli, 0);
            $actividadTipoId = 5;//telefono
        }
        if (count($result) == 0) {
            $result = $this->_model->getAveriaPenBasProv($codactu, $codcli, 0);
            $actividadTipoId = 5;//telefono
        }
        if (count($result) == 0) {
            $result = $this->_model->getAveriaPenAdslPais($codactu, $codcli, 0);
            $actividadTipoId = 6;//internet
        }
        if (count($result) > 0) {
            $result['actividad_tipo_id'] = $actividadTipoId;
        }
        return $result;
    }

    /*
    *  \input id (pk de pre_temporales)
    */
    public function postValidarlegados()
    {
         $reglas = array(
            'id'  => 'required|numeric'
        );
        $mensaje = array(
            'required'      => 'no se ingreso :attribute'
        );
        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);

        if ($validator->fails()) {
            return \Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages()->all()[0]
                )
            );
        }
        $preTemporal =  \Cot\models\PreTemporal::findOrFail(\Input::get('id'));
        if ($preTemporal->validado == 0) {
            $result = $this->buscarLegados(
                $preTemporal->actividad_tipo_id, 
                $preTemporal->codactu,
                $preTemporal->codcli
            );
            if (count($result) > 0) {
                return \Response::json(['data' => $result,'rst' => 1]);
            }
        }
        return \Response::json(['rst' => 2]);
    }

    public function postValidarpermiso()
    {
        $ticket = \Input::get('busqueda');
        $authId = \Auth::id();
        if (\Redis::get($ticket)) {
            $val = json_decode(\Redis::get($ticket));
            if ($val->id != \Auth::id()) {
                return \Response::json(
                    array(
                        "rst" => 1,
                        "type" => "1|".$val->user
                    )
                );
            }
        } else{
            $value = json_encode(['id'=>$authId,'user'=>\Session::get('full_name') ]);
            \Redis::set($ticket, $value);
            \Redis::expire($ticket, 15*60);
        }

        return \Response::json(
            array(
                "rst" => 1,
                "type" => "0"
            )
        );
    }

    public function postFinalizarpermiso()
    {
        $id = \Input::get('busqueda');
        $type = \Input::get('type');

        if (\Redis::get($id)) {
            if ($type == 0) {
                \Redis::del($id);                
            }
        }
        return \Response::json(array("rst" => 1));
    }

    public function postVisor()
    {
        $rst = $this->_model->Visorcot(); 
        return \Response::json(
            array(
                "rst" => 1,
                'motivo' =>  $rst['motivo'],
                'submotivo' => $rst['submotivo'],
                'estado_id' => $rst['estado_id'],
            )
        );
    }
    /**
     * consulta de pretemporales ara el visor
     */
    public function postVisorRecepcionados()
    {  
        $rst = $this->_model->visorRecepcionados();
        return $rst;
        return \Response::json(
            array(
                "rst" => 1,
                'recepcionados' => $rst['recepcionados'],
                'recepcionados_horas' => $rst['recepcionados_horas']
            )
        );
    }

    public function postProcesarmasivo()
    {   
        if(\Request::ajax()){ 
            $msj = 'Fallo al registrar';$rst = 2;          
            
            if($this->_query->archMasivo(\Input::all())){
                $registrados = $this->_query->getRegistrados();
                $msj = 'Registrados correctamente';
                $rst = 1;       
            }

            return \Response::json(
                array(
                    'rst' => $rst,
                    'msj' => $msj,
                    'registrados' => $registrados
                    )
            );
        }
    }

    public function postCargaradmin()
    {
        $data = $this->_model->getfieldsAdmin();        
        return \Response::json(
            array(
                'datos' => ($data) ? $data : false,
            )
        );
    }

    public function postConfiguracion()
    {        
        $msj = 'Ocurrió un error durante la actualización';
        $admin = array(
                'obligatorio'        => \Input::get('id'),
                'cantidad'           => \Input::get('cantidad')            
            );

        $pretemporal_admin = PretemporalAdmin::find(\Input::get('id'));      
        $pretemporal_admin->fill($admin);
        $pretemporal_admin->save();

        return \Response::json(
            array(
                'rst' => (isset($pretemporal_admin->id)) ? 1 : false,
                'msj' => (isset($pretemporal_admin->id)) ? 'Guardado Correctamente' : $msj,
            )
        );
    }

    public function postAutoasignar()
    {   
        $pendientes = $this->_model->ticketPendientes(\Input::get('quiebre_id'));//all tickets pending
        $quiebres = $this->_model->ticketPendientes(\Input::get('quiebre_id'),2); //count pendientes by quiebre

        $cant_asig_quiebre = [];
        foreach ($quiebres as $qkey => $value) {
            $users_act = $this->_model->getLibrebyusers('', $value->quiebre_id);
            $cant_asig_quiebre[$value->quiebre]['asig'] = 0;
            if($users_act){
                $len = count($users_act);$count_copado = 0;                 
                while (list($indx, $usuario) = each($users_act)) {
                    if($usuario->libres > 0 && $value->cantidad > 0){ 
                        foreach ($pendientes as $key => $pretemp) {
                            if($pretemp->quiebre_id == $value->quiebre_id){
                                if($ky = $this->saveAsig($pretemp->id, $usuario->usuario_id)){
                                    unset($pendientes[$key]);
                                    $users_act[$indx]->libres = $users_act[$indx]->libres - 1;
                                    $quiebres[$qkey]->cantidad = $quiebres[$qkey]->cantidad - 1; 
                                    $cant_asig_quiebre[$value->quiebre]['asig']+=1;  
                                    break;
                                }                                
                            }                        
                        }
                    }else{ $count_copado+=1; 
                    }
                    if(($indx+1 == $len) && count($pendientes) > 0 && $count_copado < $len){
                      reset($users_act);$count_copado=0;     
                    }                   
                }                  
            }
        }

        /*support quiebre*/
        foreach ($quiebres as $vkey => $quiebre) {
            if($quiebre->cantidad > 0){
                $users_act = $this->_model->getLibrebyusers('', $quiebre->quiebre_id, 2);
                if($users_act){
                    $len = count($users_act);$count_copado = 0;                 
                    while (list($indx, $usuario) = each($users_act)) {
                        if($usuario->libres > 0 && $quiebre->cantidad > 0){ 
                            foreach ($pendientes as $key => $pretemp) {
                                if($pretemp->quiebre_id == $quiebre->quiebre_id){
                                    if($ky = $this->saveAsig($pretemp->id, $usuario->usuario_id)){
                                        unset($pendientes[$key]);
                                        $users_act[$indx]->libres = $users_act[$indx]->libres - 1;
                                        $quiebres[$vkey]->cantidad = $quiebres[$vkey]->cantidad - 1; 
                                        $cant_asig_quiebre[$quiebre->quiebre]['asig']+=1;  
                                        break;
                                    }                                
                                }                        
                            }
                        }else{ $count_copado+=1; 
                        }
                        if(($indx+1 == $len) && count($pendientes) > 0 && $count_copado < $len){
                          reset($users_act);$count_copado=0;     
                        }                   
                    }                  
                }
            }
        }
        /*end support quiebre*/
        /*end get get averias pending*/ 
       
        $html = '';
        if($cant_asig_quiebre){
            foreach ($cant_asig_quiebre as $indice => $asig) {
                $html.= ",".$indice.":".$asig['asig'];
            }
        }

        return \Response::json(
            array(
                'rst' => 1,
                'asignados' =>$html,
                'pendientes' => count($pendientes)
            )
        );
        /*end pick up averias for each user*/
    }

    public function saveAsig($pre_temporal_id,$usuario_id)
    {
        $pre_temporal = PreTemporal::find($pre_temporal_id);
        $pre_temporal->usuario_id=$usuario_id;
        $pre_temporal->fecha_asignacion_usuario=date('Y-m-d H:i:s');
        $pre_temporal->save();
        if($pre_temporal->id){
            PretemporalUsuario::where('pretemporal_id', '=', $pre_temporal_id)->update(['estado' => '0']);
            $data = [];
            $data['usuario_id']= $usuario_id;
            $data['pretemporal_id'] =$pre_temporal_id;
            $preUsu = PretemporalUsuario::create($data);
            if ($preUsu->id) {
                return true;
            }
        }
        return false;
    }

    public function saveDesAsig($pre_temporal_id)
    {
        $pre_temporal = PreTemporal::find($pre_temporal_id);
        $pre_temporal->usuario_id='';
        $pre_temporal->fecha_asignacion_usuario='';
        $pre_temporal->save();
        if($pre_temporal->id){
            //PretemporalUsuario::where('pretemporal_id', '=', $pre_temporal_id)
            //->update(['estado' => '0']);
            return true;
        }
        return false;
    }

    public function postMultiproceso()
    {
        if(\Request::ajax()){
               $rst = $this->_query->multiproceso();
               return ($rst) ? 1 : 0;
        }
    }
   

    public function postReasignacion()
    {
        if(\Input::has('tiporea') && \Input::has('pedidos')){
            $reasignados = 0;
            $pendientes = explode(",", \Input::get('pedidos'));
            if(\Input::get('tiporea') == 1 && \Input::get('usuario')){ //person
                $users_act = $this->_model->getLibrebyusers(\Input::get('usuario'));
                $len = count($users_act);
                $count_copado = 0;                             
                while (list($indx, $usuario) = each($users_act)) {
                    if($usuario->libres > 0){ 
                        foreach ($pendientes as $key => $pretemp) {
                            if($ky = $this->saveAsig($pretemp, $usuario->usuario_id)){
                                $reasignados+=1;
                                unset($pendientes[$key]);
                                $users_act[$indx]->libres = $users_act[$indx]->libres - 1;
                                break;
                            }    
                        }
                    }else{ $count_copado+=1; 
                    }  
                    if(($indx+1 == $len) && count($pendientes) > 0 && $count_copado < $len){
                        reset($users_act);
                        $count_copado=0;     
                    }                                      
                }               
            }else if(\Input::get('tiporea') == 2){ //order's cloud           
                foreach ($pendientes as $key => $pretemp) {                    
                    if($ky = $this->saveDesAsig($pretemp)){
                        $reasignados+=1;
                    }     
                }                        
	    }else if(\Input::get('tiporea') == 3){ //change to quiebre
                $quiebre_id = \Input::get('quiebre_transferencia');                   
                $reasignados = $this->_query->DesAsigChangeQuiebre($pendientes,$quiebre_id);                
            }
        }
        return \Response::json(
            array(
                'rst' => 1,
                'msj' => 'Reasignado Exitosamente',
                'reasignados' => $reasignados
            )
        );
    }


    public function postUploadexcel() 
    {
        $data = $this->_query->getdataUpload();        
        return \Response::json(
            array(
                'rst' => ($data) ? 1 : 2,
                'data' => ($data) ? $data : ''
            )
        );
    }

    public function postMasivas()
    {
        $result = $this->_model->getMasivas();

        return \Response::json(
            array(
                'rst' => ($result) ? 1 : 2,
                'data' => ($result) ? $result : '',
            )
        );
    }


    public function postAsignarticketpersonal()
    {
        $usuario_logeado = \Auth::id();
        //return $usuario_logeado;
        PreTemporal::where('usuario_id', '=', $usuario_logeado)
                    ->where('estado_pretemporal_id', '=', 1)
                    ->update(['tipo_atencion'=> 24]);
        return ;            

        $usuario_area_quiebre = \DB::table('usuario_area as ua')
                                   ->leftJoin('grupos as g', 'g.id', '=', 'ua.grupo_id')
                                   ->where('ua.usuario_id', '=', $usuario_logeado)
                                   ->groupBy('ua.grupo_id')
                                   ->pluck('g.quiebre_id');
        //return $usuario_area_quiebre;
        if (count($usuario_area_quiebre) == 0) {
            return \Response::json(
                array(
                    'rst' =>  0,
                    'msj' => 'No se puede asignar porque no tiene un quiebre configurado.'
                )
            );
        }     

        $tickets_usuario = PreTemporal::where('usuario_id', '=', $usuario_logeado)
                                      ->where('estado_pretemporal_id', '=', 1)
                                      ->get();
        if (count($tickets_usuario) > 0)  {
            return \Response::json(
                array(
                    'rst' =>  0,
                    'msj' => 'No se puede asignar porque aún tiene tickets recepcionados.'
                )
            );
        }
        $ticket_libre = \PreTemporal::where('estado_pretemporal_id', '=', 1)
                                    ->where('quiebre_id', '=', $usuario_area_quiebre)
                                    ->whereNull('usuario_id')
                                    ->orderBy('created_at', 'asc')
                                    ->first();
        if (count($ticket_libre)) {

            $this->saveAsig($ticket_libre->id, $usuario_logeado);
            return \Response::json(
                array(
                    'rst' =>  1,
                    'msj' => 'Se ha asignado un ticket correctamente.',
                    'fecha_registro_ticket' => $ticket_libre->created_at
                )
            );
        } else {

            $tickets_libres_usuario = \DB::table('pre_temporales as pt1')
                                         ->select( \DB::raw("pt1.id as id, pt1.created_at, pt2.cant as cant") )
                                         ->leftJoin(\DB::raw('(SELECT count(id) as cant, usuario_id from pre_temporales 
                                                                    where  estado_pretemporal_id = 1 
                                                                    and usuario_id is not null
                                                                    group by usuario_id ) as pt2'), function ($lj){
                                            $lj->on('pt2.usuario_id', '=', 'pt1.usuario_id');
                                         })
                                         ->where('quiebre_id', '=', $usuario_area_quiebre)
                                         ->where('pt1.estado_pretemporal_id', '=', 1)
                                         ->whereNotNull('pt1.usuario_id')
                                         ->groupBy('pt1.usuario_id', 'id')
                                         ->orderBy('cant', 'desc')
                                         ->orderBy('pt1.usuario_id', 'asc')
                                         ->get();
            if (count($tickets_libres_usuario)) {
                foreach ($tickets_libres_usuario as $key => $ticket_libre) {
                    if (!(\Redis::get($ticket_libre->id))) {
                        $this->saveAsig($ticket_libre->id, $usuario_logeado);
                        return \Response::json(
                            array(
                                'rst' =>  1,
                                'msj' => 'Se ha asignado un ticket correctamente.',
                                'fecha_registro_ticket' => $ticket_libre->created_at
                            )
                        );
                    }
                }
            }

            return \Response::json(
                array(
                    'rst' =>  0,
                    'msj' => 'No se puede asignar porque no hay tickets disponibles.'
                )
            );
        }

    }


}
