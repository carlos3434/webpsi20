<?php
namespace Cot\controllers;

class PreTemporalRrssController extends \BaseController
{
    public function postListar() {
        $data = \DB::table('pre_temporales_rrss')
        ->select(
            'id',
            'nombre'
        )
        ->where('estado', '=', '1')
        ->get();

        $datos = array(
            'rst' => 1,
            'datos' => $data
        );

        return \Response::json($datos);
    }

}

