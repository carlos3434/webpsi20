<?php
namespace Cot\controllers;

use Cot\models\EventoMasiva as EventoMasiva;
use Cot\Repositories\EventoMasivaRepositoryInterface as Masiva;
use Cot\Repositories\EventMasivaModelRepositoryInterface as Model;

class EventoMasivoController extends \BaseController {
	protected $_masiva;
	protected $_model;

    public function __construct(Masiva $masiva, Model $model)
    {
        $this->_masiva = $masiva;
        $this->_model = $model;
    }


	public function postGuardar(){
		if (\Request::ajax()) {			
			$result = $this->_masiva->action();

			return \Response::json(
                array(
                    'rst'    => isset($result['rst']) ? $result['rst'] : $result,
                    'msj'    => "Registrado Correctamente",
                    'codgenerado' => (isset($result['data']->nombre)) ? $result['data']->nombre : '',
                    'id' => isset($result['data']->id) ? $result['data']->id : '',
                    'nodo' => (isset($result['data']->nodo)) ? $result['data']->nodo : '',
                    'troba' => (isset($result['data']->troba)) ? $result['data']->troba : '',
            	));	
		}
	}

	public function getExportcodcli(){
		$objClass = new \EventoMasiva(); 
		$data = $this->_model->getTicketsbyEvento();
		$html = '';
		if($data){
			foreach ($data as $key => $value) {
				$html.=$value->codcli."\r\n";	
			}
		}

		$data = $html;
	  	$fileName = time() . '_datafile.txt';
	  	\File::put(public_path('uploads/'.$fileName),$data);
	  	return \Response::download(public_path('uploads/'.$fileName));
	}

	public function getExportelefono(){

		$objClass = new EventoMasiva(); 
		$data = $objClass->getTicketsbyEvento();

		$html = '';
		if($data){
			foreach ($data as $key => $value) {
				$c_celular = (strpos($value->cliente_celular,'-') !== false) ? str_replace("-","|",$value->cliente_celular) : $value->cliente_celular;
				$c_telefono = (strpos($value->cliente_telefono,'-') !== false) ? str_replace("-","|",$value->cliente_telefono) : $value->cliente_telefono;
				$contac_celular = (strpos($value->contacto_celular,'-') !== false) ? str_replace("-","|",$value->contacto_celular) : $value->contacto_celular;
				$contac_telf =  (strpos($value->contacto_telefono,'-') !== false) ? str_replace("-","|",$value->contacto_telefono) : $value->contacto_telefono;
				$html.=$value->codcli."  ".$c_celular." ".$c_telefono." ".$contac_celular." ".$contac_telf."\r\n";	
			}
		}
		
		$data = $html;
	  	$fileName = time() . '_datafile.txt';
	  	\File::put(public_path('uploads/'.$fileName),$data);
	  	return \Response::download(public_path('uploads/'.$fileName));
	}


	public function postCargar(){

		if (\Request::ajax()) {
			$id_evento = \Input::get('idevento', '');
			$tipo = 2;
			if ( $id_evento != '') {
				$tipo = 1;
			}
			return \Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Cargado Correctamente",
                    'datos'  => $this->_model->listado(),
                    'tipo'   => $tipo
                )
            );	
		}
	}

	public function postListar(){
		if (\Request::ajax()) {			
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $this->_model->listadoSimple()
                )
            );	
		}
	}

	public function postCambiarestado(){
		if (\Request::ajax()) {
			/*$update = EventoMasiva::find(\Input::get('id'));
			$update->estado = \Input::get('estado');
			$update->usuario_updated_at = \Auth::id();
			$update->updated_at = date('Y-m-d H:i:s');
			$update->save();*/

            $result = $this->_masiva->action();
			return \Response::json(
                array(
                    'rst'    =>  ($result) ? 1 : 0,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}

	public function postGetticketsbyevent(){
		if(\Request::ajax()){
			$objClass = new EventoMasiva(); 
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $this->_model->getTicketsbyEvento()
                )
            );	
		}
	}


	public function postReporte(){
		$ahora = date('Y-m-d_H:i:s');
		$nameFile = "Evento-masivo_".$ahora ;
		$objClass = new EventoMasiva();
		$data = json_decode(json_encode($this->_model->listado()), true);
		$datos = [];
		if(count($data) === 0){
			$datos['0']['No existen registros']='';
			return \Helpers::exportArrayToCsv($datos, $nameFile);
		} else {
			$datos = $data;
			return \Helpers::exportArrayToCsv($datos, $nameFile);
		} 
	}
}
