<?php
namespace Cot\controllers;
class PreTemporalAtencionController extends \BaseController
{
    public function postListar() {
        $data = \DB::table('pre_temporales_atencion')
        ->select(
            'id',
            'nombre'
        )
        ->where('estado', '=', '1')
        ->get();

        $datos = array(
            'rst' => 1,
            'datos' => $data
        );

        return \Response::json($datos);
    }

}

