<?php
namespace Cot\controllers;
class GrupocotController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /grupocot
	 *
	 * @return Response
	 */

	public function postListar(){
		if (\Request::ajax()) {
			$objClass = new \Grupo(); 
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );	
		}
	}
	
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /grupocot/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /grupocot
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /grupocot/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /grupocot/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /grupocot/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /grupocot/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
