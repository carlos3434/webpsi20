<?php
namespace Cot\controllers;
use Cot\models\PretemporalAveria;
class PreTemporalEmbajadorController extends PreTemporalController
{
    /**
    * guardar formulario de embajador
    */
    public function postRegistrar()
    {
        $reglas = array(
            'actividad_tipo_id'       => 'Required',
            //'codcli'                  => 'Required',
            'embajador_nombre'        => 'Required',
            'embajador_correo'        => 'Required',
            'embajador_celular'       => 'Required',
            'embajador_dni'           => 'Required',
            'cliente_nombre'          => 'Required',
            'cliente_celular'         => 'Required',
        );
        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'AlphaNum'      => ':attribute Solo debe ser alfanumerico'
        );
        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return \Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                )
            );
        }

        if(\Input::get('codactu')){
            /*pendientes*/
            $legado = $this->_model->getAveriaPenCatvPais(\Input::get('codactu'), '', 0);
            if (count($legado) == 0) {
                $legado = $this->_model->getAveriaPendMacro(\Input::get('codactu'),'',0);
            }
            $actividadTipoId = 4; //television

            if (count($legado) == 0) {
                $legado = $this->_model->getAveriaPenBasLima(\Input::get('codactu'), '', 0);
                $actividadTipoId = 5;//telefono
            }
            if (count($legado) == 0) {
                $legado = $this->_model->getAveriaPenBasProv(\Input::get('codactu'), '', 0);
                $actividadTipoId = 5;//telefono
            }
            if (count($legado) == 0) {
                $legado = $this->_model->getAveriaPenAdslPais(\Input::get('codactu'), '', 0);
                if (count($legado) == 0) {
                    $legado = $this->_model->getAveriaPendMacro(\Input::get('codactu'),'',0);
                }
                $actividadTipoId = 6;//internet
            }

            if($legado){
                $codcli = $legado[0]->codcli;
                \Input::merge(['codcli'=>$codcli]);
                $legado_registro = new \DateTime($legado[0]->fecha_registro);
                $today = new \DateTime(date('Y-m-d h:i:s'));
                $interval = date_diff($legado_registro, $today);
                $hours = $interval->h;
                $hours = $hours + ($interval->days*24);
                if($hours <= 45){
                    return \Response::json(
                        array(
                        'rst' => 2,
                        'msj' => 'No se puede registrar el ticket.
                                    La avería '.\Input::get('codactu').' aún está dentro del plazo de atención de 48 horas (fecha de registro: '.$legado[0]->fecha_registro.').',
                        )
                    );
                }                
            }
            /*end pendientes*/


            /*liquidadas*/            
            $averia = $this->_model->getAveriaLiqCatvPais(\Input::get('codactu'), '', 0);
            $actividadTipoId = 4; //television

            if (count($averia) == 0) {
                $averia = $this->_model->getAveriaLiqBasLima(\Input::get('codactu'));
                $actividadTipoId = 5;//telefono
            }
            if (count($averia) == 0) {
                $averia = $this->_model->getAveriaLiqBasProv(\Input::get('codactu'));
                $actividadTipoId = 5;//telefono
            }
            if (count($averia) == 0) {
                $averia = $this->_model->getAveriaLiqAdslPais(\Input::get('codactu'));
                $actividadTipoId = 6;//internet
            }
            if($averia){
                $codcli = $averia[0]->codcli;
                \Input::merge(['codcli'=>$codcli]);
                $averia_registro = new \DateTime($averia[0]->fecha_liquidacion);
                $today = new \DateTime(date('Y-m-d h:i:s'));
                $interval = date_diff($averia_registro, $today);
                $days = $interval->days;
                if($days > 7 ){
                    return \Response::json(
                        array(
                        'rst' => 2,
                        'msj' => 'No se puede registrar el ticket.
                                    La avería '.\Input::get('codactu').' no se encuentra liquidada dentro de los últimos 7 días',
                        )
                    );
                }                
            }
            /*end liquidadas*/

            if(!$averia && !$legado){
                return \Response::json(
                        array(
                        'rst' => 2,
                        'msj' => 'No se puede registrar el ticket,La avería '.\Input::get('codactu').' no se encuentra liquidada dentro de los últimos 7 días ó no se encuentra en los sistemas.',
                        )
                );
            }
            //1014 usuario Gestion 101
            \Auth::loginUsingId(1014);
            $preTemporal = $this->_pretemporal->fill(\Input::all())->save();;
            $ticket = 'E'. str_pad($this->_pretemporal->getId(), 5, "0", STR_PAD_LEFT);
            $mensaje = "Ticket registrado correctamente con el número ".$ticket;
            $celular = substr(\Input::get('embajador_cel'), -9);
            \Sms::enviar($celular, $mensaje, '467');
            $response = [
                'rst' => '1',
                'ticket' => $ticket,
                'msj' => 'Se registro el ticket '.$ticket.' correctamente. Nos estaremos comunicando a la brevedad posible.'
            ];

            $ave=[
                'pre_temporal_id'=> $this->_pretemporal->getId(),
                'codactu'=>$this->_pretemporal->getCodact(),
                'codcli'=>$this->_pretemporal->getCodcli(),
                'gestion'=>'embajador',
                'proceso'=>1,
            ];

            $averia = new PretemporalAveria();
            $averia->fill($ave);
            $this->_pretemporal->pretemporalAveria()->save($averia);            
            \Auth::logout();
            return \Response::json($response);
        }
    }
}
