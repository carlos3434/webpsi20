<?php
namespace Cot\controllers;

class PreTemporalSolucionController extends \BaseController
{

    public function postListar()
    {
        //si la peticion es ajax
        if (\Request::ajax()) {
            $data = \DB::table('pre_temporales_solucionado')
            ->select('id',
                \DB::raw("CONCAT( producto,' - ',accion) as nombre"))
            ->where('estado', 1)
            ->where(
                        function($query) {
                            if (\Input::get('actividad_tipo')) {
                                $query->where(
                                    'actividad_tipo_id', \Input::get('actividad_tipo')
                                    );
                            }
                        }
                    )
            ->orderBy('producto')
            ->get();
            return \Response::json(array('rst' => 1, 'datos' => $data));
        }
    }

}

