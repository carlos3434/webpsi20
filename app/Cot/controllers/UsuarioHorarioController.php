<?php
namespace Cot\controllers;
class UsuarioHorarioController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /usuariohorario
	 *
	 * @return \Response
	 */

	public function postRegistrar(){
		if (\Request::ajax()) {
			$data = [];
			if(\Input::get('dia_id') && \Input::get('usuario_id')){
				$data['usuario_id']=\Input::get('usuario_id');
				$data['dia_id']=\Input::get('dia_id');
				$data['estado']=1;				
			}			
			$rst = \UsuarioHorario::create($data);
			
			return \Response::json(
	            array(
	            	'rst' => ($rst) ? 1 : 0,
	              	'msj' => 'Registrado Correctamente',
	            )
            );
		}
	}

	public function postCargar(){
		if (\Request::ajax()) {
			$objClass = new \UsuarioHorario(); 
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );	
		}
	}

	public function postDestroyday(){
		if (\Request::ajax()) {
			if(\Input::get('usuario_id') && \Input::get('dia_id')){
				$deleted = \UsuarioHorario::where('usuario_id', \Input::get('usuario_id'))
						->where('dia_id',\Input::get('dia_id'))
						->delete();

			}

			return \Response::json(
                array(
                    'rst'    => ($deleted) ? 1 : 0,
					'msj' => 'Eliminado Correctamente',
                )
            );	
		}
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /\usuariohorario/create
	 *
	 * @return \Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /\usuariohorario
	 *
	 * @return \Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /\usuariohorario/{id}
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /\usuariohorario/{id}/edit
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /\usuariohorario/{id}
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /\usuariohorario/{id}
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function destroy($id)
	{
		//
	}

}
