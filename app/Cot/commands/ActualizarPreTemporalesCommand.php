<?php
namespace Cot\commands;
use Cot\Repositories\CotPretemporalRepository;
use Cot\models\PretemporalAveria;
use Illuminate\Console\Command;

class ActualizarPreTemporalesCommand extends Command
{
    protected $name = 'cot:actualizarpretemporales';
    protected $_query;
    protected $description = 'actualiza actividades segun su estado en las tablas de pendientes y liquidados';

    public function __construct()
    {
        parent::__construct();
        $this->_query = new CotPretemporalRepository();
    }

    public function fire()
    {
        \Auth::loginUsingId(697);//proceso automatico
        $numLiquidados = 0;
        $numPendientes = 0;
        $numAreasActualizadas = 0;
        $numSinAveria = 0;

        //PENDIENTES
        $pretemporales = \DB::table('pre_temporales as p')
        ->leftjoin(
            'pre_temporales_averias as a',
            'p.id', '=', 'a.pre_temporal_id'
        )
        ->leftjoin(
            'pre_temporales_movimientos as m', function($join) {
            $join->on('p.id', '=', 'm.pre_temporal_id')
                 ->on(
                     'm.id', '=', \DB::raw(
                         "(SELECT MAX(m2.id)
                                            FROM pre_temporales_movimientos m2
                                            WHERE m2.pre_temporal_id=p.id)"
                     ) 
                 );
            }
        )
        ->leftjoin(
            'pre_temporales_ultimos as ptu',
            'ptu.pre_temporal_id',
            '=',
            'p.id'
        )
        ->where('a.proceso', '=', 1)
        ->where('ptu.motivo_id', '=', 24) //V3 En Operaciones
        ->where('ptu.estado_id', '=', 7) //Pendiente
        ->where('p.codactu', '<>', '')
        ->where(
            'p.created_at', '>', 
            \DB::raw(' date(date_sub(NOW(), INTERVAL 30 DAY))')
        )
        ->select(
            'p.id as pre_temporal_id',
            'p.codcli',
            'p.codactu',
            'a.id',
            'a.area',
            'a.proceso',
            'p.actividad_tipo_id',
            'ptu.estado_id',
            \DB::raw('4 as estado_pretemporal_id') //Ejecutado
        )
        ->get();
        foreach ($pretemporales as $pre) {
            $legado = $this->_query->getAveriaPenCatvPais($pre->codactu, '', 0);
            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPendMacro($pre->codactu,'',0);
            }
            $actividadTipoId = 4; //television

            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenBasLima($pre->codactu, '', 0);
                $actividadTipoId = 5;//telefono
            }
            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenBasProv($pre->codactu, '', 0);
                $actividadTipoId = 5;//telefono
            }
            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenAdslPais($pre->codactu, '', 0);
                if (count($legado) == 0) {
                    $legado = $this->_query->getAveriaPendMacro($pre->codactu,'',0);
                }
                $actividadTipoId = 6;//internet
            }
            if (count($legado) == 0) {
                continue;
            }

            $averia =[
                'estado_legado'         => $legado[0]->estado,
                'codcli'                => $legado[0]->codcli,
                'nombre_cliente'        => $legado[0]->nombre_cliente,
                'observacion'           => $legado[0]->observacion,
                'contrata'              => $legado[0]->contrata,
                'area'                  => $legado[0]->area,
                'zonal'                 => $legado[0]->zonal,
                'mdf'                   => $legado[0]->mdf,
                'fecha_registro_legado' => $legado[0]->fecha_registro,
                'proceso'               => 2
            ];
            $pretemporal = [
                'estado_pretemporal_id'  => $pre->estado_pretemporal_id,
                'fecha_registro_legado'  => $legado[0]->fecha_registro,
                'validado'               => 1,
                'actividad_tipo_id'      => $actividadTipoId
            ];

            $segundos=strtotime($legado[0]->fecha_registro) - strtotime('now');
            $diferencia=abs(intval($segundos/60/60));

            $submotivo = 66;//Pdte vencido
            if ($diferencia <= 24) $submotivo = 65; //Pdte en plazo

            $observacion = $legado[0]->observacion.
                        ' | Area: '.$legado[0]->area.
                        ' | Fecha Registro: '.$legado[0]->fecha_registro.
                        ' | Reg Averia: '.$legado[0]->codactu.
                        ' | MDF: '.$legado[0]->mdf;

            $mov=[
                'pre_temporal_id'            => $pre->pre_temporal_id,
                'codcli'                     => $pre->codcli,
                'motivo_id'                  => 20, //Ejecucion
                'submotivo_id'               => $submotivo,
                'estado_id'                  => $pre->estado_id,
                'estado_pretemporal_id'      => $pre->estado_pretemporal_id,
                'observacion'                => $observacion,
                'pre_temporal_solucionado_id'=> 0,
                //'usuario_created_at'         => 697,
                //'usuario_updated_at'         => 697,
                //'updated_at'                 => date('Y-m-d H:i:s')
            ];

            \Pretemporal::find($pre->pre_temporal_id)->update($pretemporal);
            \PretemporalAveria::findOrFail($pre->id)->update($averia);
            \PretemporalMovimiento::create($mov);
            //descomponer
            $ultmov = PretemporalUltimo::where('pre_temporal_id', $mov['pre_temporal_id'])->first();
            if ($ultmov == null) {
                \PretemporalUltimo::create($mov);
            } else{
                \PretemporalUltimo::find($ultmov->id)->update($mov);
            }
            //PretemporalUltimo::updateUltimoMovimiento($mov);
            $numPendientes++;
        
        }
        //FIN PENDIENTES

        //ACTUALIZAR AREA G
        $pretemporales = \DB::table('pre_temporales as p')
        ->leftjoin(
            'pre_temporales_averias as a', 
            'p.id', '=', 'a.pre_temporal_id'
        )
        ->leftjoin(
            'pre_temporales_ultimos as ptu',
            'ptu.pre_temporal_id',
            '=',
            'p.id'
        )
        ->where('ptu.motivo_id', '=', 20) //En Ejecucion
        ->where('ptu.estado_id', '=', 7) //Pendiente
        ->where('p.codactu', '<>', '')
        ->where(
            'p.created_at', '>', 
            \DB::raw(' date(date_sub(NOW(), INTERVAL 15 DAY))')
        )
        ->select(
            'a.id', 'p.codactu',
            \DB::raw('TRIM(a.area) AS area'),
            'a.observacion'
        )
        ->get();

        foreach ($pretemporales as $pre) {

            $legado = $this->_query->getAveriaPenCatvPais($pre->codactu, '', 0);

            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenBasLima($pre->codactu, '', 0);
            }
            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenBasProv($pre->codactu, '', 0);
            }
            if (count($legado) == 0) {
                $legado = $this->_query->getAveriaPenAdslPais($pre->codactu, '', 0);
            }
            if (count($legado) == 0) {
                continue;
            }
            $area=trim($legado[0]->area);
            if ( $pre->area == $area || $area =='') {
                continue;
            }

            $obsCambioArea =
                'Se cambio el area de '.$pre->area.' a '.$area.
                ' | Fecha Registro: '.$legado[0]->fecha_registro.
                ' | Reg Averia: '.$legado[0]->codactu;

            $averia =[
                'observacion'   => $pre->observacion.'||'.$obsCambioArea,
                'area'          => $area
            ];
            $data = \PretemporalAveria::find($pre->id);
            if($data){
                 $data->update($averia);
            }
            $numAreasActualizadas++;
        }
        //FIN ACTUALIZAR AREA

        //LIQUIDADOS
        $pretemporales = \DB::table('pre_temporales as p')
                ->leftjoin(
                    'pre_temporales_averias as a',
                    'p.id', '=', 'a.pre_temporal_id'
                )
                ->leftjoin(
                    'pre_temporales_movimientos as m', function($join) {
                    $join->on('p.id', '=', 'm.pre_temporal_id')
                         ->on(
                             'm.id', '=', \DB::raw(
                                 "(SELECT MAX(m2.id)
                                    FROM pre_temporales_movimientos m2
                                    WHERE m2.pre_temporal_id=p.id)"
                             ) 
                         );
                    }
                )
                ->leftjoin(
                    'pre_temporales_ultimos as ptu',
                    'ptu.pre_temporal_id',
                    '=',
                    'p.id'
                )
                ->where('a.estado_legado', '<>', 'liquidado')
                ->where('p.codactu', '<>', '')
                ->where('ptu.motivo_id', '=', 24) //V3 En Operaciones
                ->orwhere('ptu.motivo_id', '=', 20) // En Ejecución
                ->where('ptu.estado_id', '=', 7) //Pendiente
                ->select(
                    'p.id as pre_temporal_id',
                    "p.codactu", "a.id as averias_id",
                    "p.actividad_tipo_id", "p.codcli",
                    "p.estado_pretemporal_id",
                    "m.observacion",
                    "m.pre_temporal_solucionado_id"
                )
                ->get();
        foreach ($pretemporales as $pre) {

            $averia = $this->_query->getAveriaLiqCatvPais($pre->codactu, '', 0);
            $actividadTipoId = 4; //television

            if (count($averia) == 0) {
                $averia = $this->_query->getAveriaLiqBasLima($pre->codactu);
                $actividadTipoId = 5;//telefono
            }
            if (count($averia) == 0) {
                $averia = $this->_query->getAveriaLiqBasProv($pre->codactu);
                $actividadTipoId = 5;//telefono
            }
            if (count($averia) == 0) {
                $averia = $this->_query->getAveriaLiqAdslPais($pre->codactu);
                $actividadTipoId = 6;//internet
            }
            if (count($averia) == 0) {
                continue;
            }

            //PretemporalAveria
            $array=[
                'estado_legado'         => 'Liquidado',
                'codcli'                => $averia[0]->codcli,
                'cod_liquidacion'       => $averia[0]->codigo_liquidacion,
                'detalle_liquidacion'   => $averia[0]->detalle_liquidacion,
                'observacion'           => $averia[0]->observacion,
                'fecha_liquidacion'     => $averia[0]->fecha_liquidacion,
                'nombre_cliente'        => $averia[0]->nombre_cliente,
                'contrata'              => $averia[0]->contrata,
                'area'                  => $averia[0]->area,
                'zonal'                 => $averia[0]->zonal,
                'mdf'                   => $averia[0]->mdf,
                'fecha_registro_legado' => $averia[0]->fecha_registro
            ];

            $observacion =
                $averia[0]->observacion.
                ' | Reg Averia: '.$averia[0]->codactu.
                ' | Cod Liq: '.$averia[0]->codigo_liquidacion.
                ' | Detalle Liq: '.$averia[0]->detalle_liquidacion.
                ' | Fecha Liq: '.$averia[0]->fecha_liquidacion.
                ' | Area: '.$averia[0]->area.
                ' | MDF: '.$averia[0]->mdf;
            //buscar el maximo mov
            $mov=[
                'pre_temporal_id'            => $pre->pre_temporal_id,
                'codcli'                     => $pre->codcli,
                'motivo_id'                  => 22,//Liquidacion
                'submotivo_id'               => 12,//Liquidado
                'estado_id'                  => 7,//Pendiente
                'estado_pretemporal_id'      => 5,//Liquidado
                'observacion'                => $observacion,
                'pre_temporal_solucionado_id'=> 0,
                //'usuario_created_at'         => 697,
                //'usuario_updated_at'         => 697,
                //'updated_at'                 => date('Y-m-d H:i:s')
            ];
            $pretemporal=[
                'estado_pretemporal_id' => 5,
                'codactu'               => $pre->codactu,
                'validado'              => 1,
                'actividad_tipo_id'     => $actividadTipoId
            ];

            $pretemporal_ave = \PretemporalAveria::where('pre_temporal_id', $pre->pre_temporal_id);
            if($pretemporal_ave){
                $pretemporal_ave->update($array);
            }

            $pre_temporal = \Pretemporal::where('id', $pre->pre_temporal_id);
            if($pre_temporal){
                $pre_temporal->update($pretemporal);                
            }

            \PretemporalMovimiento::create($mov);
            //descomponer
            $ultmov = \PretemporalUltimo::where('pre_temporal_id', $mov['pre_temporal_id'])->first();
            if ($ultmov == null) {
                \PretemporalUltimo::create($mov);
            } else{
                \PretemporalUltimo::find($ultmov->id)->update($mov);
            }
            //PretemporalUltimo::updateUltimoMovimiento($mov);
            $numLiquidados++;
        }
        //FIN LIQUIDADOS

        //NO SE ENCONTRO AVERIA
        $pretemporales = \DB::table('pre_temporales as p')
        ->leftjoin(
            'pre_temporales_averias as a', 
            'p.id', '=', 'a.pre_temporal_id'
        )
        ->leftjoin(
            'pre_temporales_movimientos as m', function($join) {
            $join->on('p.id', '=', 'm.pre_temporal_id')
                 ->on(
                     'm.id', '=', DB::raw(
                         "(SELECT MAX(m2.id)
                            FROM pre_temporales_movimientos m2
                            WHERE m2.pre_temporal_id=p.id)"
                     ) 
                 );
            }
        )
        ->leftjoin(
            'pre_temporales_ultimos as ptu',
            'ptu.pre_temporal_id',
            '=',
            'p.id'
        )
        ->where('a.proceso', '=', 1)
        ->where('ptu.motivo_id', '=', 24) //V3 En Operaciones
        ->where('ptu.submotivo_id', '=', 59) //Tiene registro averia
        ->orwhere('ptu.submotivo_id', '=', 60) //Se registra averia
        ->where('ptu.submotivo_id', '<>', 72) //Tiene registro averia
        ->where('ptu.estado_id', '=', 7) //Pendiente
        ->where('p.codactu', '<>', '')
        ->where('p.validado', '=', 0)
        ->where(
            'ptu.updated_at', '<=', DB::raw(' date_sub(NOW(), INTERVAL 1 DAY)')
        )
        ->select(
            'p.id as pre_temporal_id',
            'p.codcli', 'p.codactu',
            'a.id',
            'a.area',
            'a.proceso',
            'p.actividad_tipo_id',
            'ptu.estado_id as estado'
        )
        ->get();

        foreach ($pretemporales as $pre) {

            $mov=[
                'pre_temporal_id'            => $pre->pre_temporal_id,
                'codcli'                     => $pre->codcli,
                'motivo_id'                  => 24,// V3 A Operaciones
                'submotivo_id'               => 72,//Averia No Asociada
                'estado_id'                  => 7, //pendiente
                'estado_pretemporal_id'      => 2,//Validado
                'observacion'                => 'No se encontró avería',
                'pre_temporal_solucionado_id'=> 0,
                //'usuario_created_at'         => 697,
                //'usuario_updated_at'         => 697,
                //'updated_at'                 => date('Y-m-d H:i:s')
            ];
            $pretemporal=[
                'estado_pretemporal_id' => 2,
            ];
            \PretemporalMovimiento::create($mov);
            \Pretemporal::where('id', $pre->pre_temporal_id)
            ->update($pretemporal);
            //descomponer
            $ultmov = \PretemporalUltimo::where('pre_temporal_id', $mov['pre_temporal_id'])->first();
            if ($ultmov == null) {
                \PretemporalUltimo::create($mov);
            } else {
                \PretemporalUltimo::find($ultmov->id)->update($mov);
            }
            //PretemporalUltimo::updateUltimoMovimiento($mov);
            $numSinAveria++;
        }
        //FIN NO SE ENCONTRO AVERIA
        Log::useDailyFiles(storage_path().'/logs/pre_temporales.log');
        Log::info($numLiquidados.' pedidos liquidados. ');
        Log::info($numPendientes.' pedidos pendientes actualizados. ');
        Log::info($numAreasActualizadas.' areas actualizadas. ');
        Log::info($numSinAveria.' pedidos sin averia. ');
        \Auth::logout();
    }
}