<?php
namespace Cot\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Cot\models\ComandoLiquidacion as Comando;
use Cot\models\ComandoQuiebre as ComandoQuiebre;
use Cot\models\ComandoDia as ComandoDia;
use Cot\models\ComandoPretemporalRrss as ComandoPretemporalRrss;
use Cot\models\PretemporalUltimo as PretemporalUltimo;
use Cot\models\PretemporalMovimiento as PretemporalMovimiento;
use Cot\models\PreTemporal as PreTemporal;
use Carbon\Carbon;
use Cot\models\EstadoPreTemporal as EstadoPreTemporal;

class ProcesoGenerarMovimientosTemp extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
    

	protected $name = 'cot:generarmovimientocot';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Permite generar nuevos comandos para procesar movimientos tickets en pretemporales que no estan cerrados.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        \Auth::loginUsingId(697);
        

        $fecha_actual =  Carbon::now()->format('Y-m-d');

		$q_comandos = Comando::with('dias', 'quiebres', 'atenciones')
                            ->where('estado', '=', 1)
                            ->where('fecha_inicio_comando', '<=', $fecha_actual);
        //actualizar el estado del comando
        $comandos_actualizar = clone $q_comandos;
        $comandos_actualizar->where('fecha_final_comando', '<', $fecha_actual)->update(['estado' => 0]);
        //obtener comandos disponibles
        $comandos = $q_comandos->where('fecha_final_comando', '>=', $fecha_actual)->get();
        
	    if (count($comandos) > 0) {
            $motivos = \Motivo::lists('nombre', 'id');
            $submotivos = \Submotivo::lists('nombre', 'id');
            $estados =  EstadoPreTemporal::lists('nombre', 'id');
            $soluciones_comerciales =  \SolucionComercial::lists('nombre', 'id');

	        foreach ($comandos as $comando) {
                $datos_exel = [];
                $modificados = [];
                $comando_fecha_inicio_busqueda = $comando->fecha_inicio_busqueda;
                $comando_fecha_final_busqueda = $comando->fecha_final_busqueda;

                if($comando->estado_programacion == 0) {
                    $comando_fecha_inicio_busqueda = $comando->fecha_inicio_busqueda.' '.$comando->dias_hora_inicio;
                    $comando_fecha_final_busqueda = $comando->fecha_final_busqueda.' '.$comando->dias_hora_fin;
                }
                
	            $fecha_comparacion = 'created_at';
	            if($comando->tipo_fecha == 2) {
                    $fecha_comparacion = 'fh_reg104'; //cambiar
                    $comando_fecha_inicio_busqueda = str_replace('-', '/', $comando_fecha_inicio_busqueda);
                    $comando_fecha_final_busqueda = str_replace('-', '/', $comando_fecha_final_busqueda);
	            }

                $dias = [];
                foreach ($comando->dias as $comando_dia) {
                    $dias[] = $comando_dia->dia;
                }
            	//programacion general (fecha busqueda tickets)
                $pretemporales = PreTemporal::with('ultimo')
                    ->where($fecha_comparacion, '>=', $comando_fecha_inicio_busqueda)
                    ->where($fecha_comparacion, '<=', $comando_fecha_final_busqueda)
                    ->where('estado_pretemporal_id', '=', 10)
                    ->where(function($q) use ($comando, $fecha_comparacion, $dias) {
                    	if ($comando->estado_programacion == 1) {
                            $hora_fin =  substr($comando->dias_hora_fin, 0, -2);
                            $hora_fin = $hora_fin.'59';
                            $q->whereRaw("( (WEEKDAY(".$fecha_comparacion.") + 1) IN (".implode(',',$dias).") 
                                            AND TIME(".$fecha_comparacion.") 
                                            BETWEEN TIME('".$comando->dias_hora_inicio."') and TIME('".$comando->dias_hora_fin."'))");
                    	}
                    })
                    ->where( function($q) use ($comando) {

                        $quiebres = [];
                        if( count($comando->quiebres) > 0 && $comando->quiebres != null ){
                            foreach ( $comando->quiebres as  $quiebre ) { ///modificar
                                $quiebres[] = $quiebre->quiebre_id;
                            }
                            $q->whereIn('pre_temporales.quiebre_id',$quiebres);
                        }
                    })
                    ->where( function($q) use ($comando) {

                        $atenciones = [];
                        if( count($comando->atenciones) > 0 && $comando->atenciones != null ){
                            foreach ( $comando->atenciones as  $atencion ) {
                                $atenciones[] = $atencion->atencion_id;
                            }
                           $q->whereIn('pre_temporales.tipo_atencion',$atenciones); 
                        }

                    })
                    ->get();
        		//information to modify
                $datos_liquidacion = [ 'motivo_id' => $comando->motivo_id,
                                       'submotivo_id' => $comando->submotivo_id,
                                       'estado_id' => $comando->estado_pretemporal_ultimo_id,
                                       'estado_pretemporal_id' => $comando->estado_pretemporal_id,
                                       'observacion' => $comando->observacion,
                                       'pre_temporal_solucionado_id' => $comando->solucionado_pretemporal_id
                                     ];
                                     
                if (count($pretemporales) > 0) {
                    foreach ($pretemporales as $pretemporal) {

                        $pretemporal->user_updated_at = $comando->user_created_at;
                        $pretemporal->update($datos_liquidacion);
                        $datos_liquidacion['pre_temporal_id'] = $pretemporal->id;
                        $pretemporal_ultimo = new PretemporalUltimo();
                        $pretemporal_ultimo->fill($datos_liquidacion);   
                        $pretemporal_ultimo->save();
              
                        $pretemporal_movimiento = new PretemporalMovimiento();
                        $pretemporal_movimiento->fill($datos_liquidacion);
                        $pretemporal_movimiento->save();

                        //$datos_exel = $datos_liquidacion;
                        $datos_exel['pretemporal_id'] = $pretemporal->id;
                        $datos_exel['comando_nombre'] = $comando->nombre;
                        $datos_exel['pre_temporal_ultimo_id'] = $pretemporal_ultimo->id;
                        $datos_exel['motivo'] = $motivos[$comando->motivo_id];
                        $datos_exel['submotivo'] = $submotivos[$comando->submotivo_id];
                        $datos_exel['estado'] = $estados[$comando->estado_pretemporal_id];

                        if($comando->solucionado_pretemporal_id !== null && $comando->solucionado_pretemporal_id !== ''){
                            $datos_exel['sol_comercial'] = $soluciones_comerciales[$comando->solucionado_pretemporal_id];
                        } else {
                            $datos_exel['sol_comercial'] = '';
                        }
                        
                        array_push($modificados, $datos_exel);
                    }
                }

                $name_file = $comando->nombre."_".\Auth::user()->nombre.'_'.date('Y-m-d H:i:s').'.csv';
                $publicPath = strpos(public_path('proceso_masivo/'), '\\') === false ? public_path('proceso_masivo/') : str_replace('\\', '/', public_path('proceso_masivo/'));
                $headers = ['PRETEMPORAL_ID', 'NOMBRE_COMANDO', 'PRETEMPORAl_ULTIMO_ID', 'MOTIVO', 'SUBMOTIVO', 'ESTADO', 'SOL_COMERCIAL'];
                \Helpers::createExcel($modificados, $publicPath.$name_file, 'CSV', $headers);
                \UploadLog::create(['file_accept'=>$name_file, 'type'=>2, 'number_acept'=>count($modificados)]);
	        }
	    }

        $pretemporales_restantes = PreTemporal::where('estado_pretemporal_id', '=', 1)->update(['estado_pretemporal_id' => 1, 'usuario_id' => null]);
   
         //creamos el arcchivo en la tabla log

	    Auth::logout();
        return;
	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
