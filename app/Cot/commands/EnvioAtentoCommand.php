<?php
namespace Cot\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Cot\controllers\PreTemporalController;
use Cot\Repositories\CotPretemporalRepository;

class EnvioAtentoCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cot:respuestaAtento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio movimientos atento de las ultimas 48 horas, comando a ejecutar cada hora';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $values = [
            "atento" => 1
        ];
        Input::replace($values);

        Auth::loginUsingId(697);//proceso automatico
        $repository = new CotPretemporalRepository();
        $data = $repository->Rptatento();                   
        if($data){
            $nameFileR = "RETORNO_ATTO1LINEA_".date('Ymd_H').".csv";
            $Path = strpos(base_path('archivos/output_atento/'), '\\') === false ? base_path('archivos/output_atento/') : str_replace('\\', '/', base_path('archivos/output_atento/'));
            Helpers::createExcel($data,$Path.$nameFileR,$type="CSV");
        }else{
            echo 'no hay data';
        }       
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    /*protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }*/

}
