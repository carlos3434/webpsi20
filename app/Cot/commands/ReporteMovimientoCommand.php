<?php
namespace Cot\commands;
use Illuminate\Console\Command;

class ReporteMovimientoCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cot:reportemovimientos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'descargar el reporte de movimientos en pagina reportecat 3 horas al dia';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{    
		$data = [];
		/*query to get info*/
        $data  = DB::table('pre_temporales as p')
                ->select(
                	'p.id as norden',
                    DB::raw('CONCAT("E",LPAD(p.id,5,0)) as ticket'),
                    'at.apocope as tipo_averia',
                    'pa.nombre as tipo_atencion',
                    'q.nombre as quiebre',
                    "ept.nombre as EstadoTicket",
                    'p.codactu as CodAveria',
                    'p.codcli',
                    'p.cliente_nombre as ClienteNombre',
                    'p.cliente_celular as ClienteCelular',
                    'p.cliente_telefono as ClienteTelefono',
                    'p.cliente_dni as ClienteDNI',
                    'p.embajador_nombre as EmbajadorNombre',
                    'p.embajador_celular as EmbajadorCelular',
                    'p.embajador_correo as EmbajadorCorreo',
                    'p.embajador_dni as EmbajadorDNI',
                    'p.comentario as ComentarioTicket',
                    DB::raw(
                        '
                            DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                            as FechaRegistroTicket'
                    ),
                    DB::raw('IFNULL(m.nombre," ") as Motivos'),
                    DB::raw('IFNULL(s.nombre," ") as Submotivos'),
                    DB::raw('IFNULL(e.nombre,"Recepcionado") as Estados'),
                    'ptm.created_at as FechaMov',
                    'ptm.observacion as ObsMov',
                    DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                    'pts.producto as Producto',
                    'pts.accion as Accion',
                    'pta.estado_legado as EstadoLegado',
                    'pta.fecha_registro_legado as FechaRegistroLegado',
                    'pta.fecha_liquidacion as FechaLiquidacion',
                    'pta.cod_liquidacion as CodigoLiquidacion',
                    'pta.detalle_liquidacion as DetalleLiquidacion',
                    'pta.area as Area',
                    'pta.contrata as Contrata',
                    'pta.zonal as Zonal',
                    DB::raw(
                        '(CASE ptm.id WHEN (
                            SELECT MAX(ptm2.id)
                            FROM pre_temporales_movimientos as ptm2
                            WHERE ptm2.pre_temporal_id = ptm.pre_temporal_id
                        )
                        THEN "X" ELSE "" END) as ultimo_movimiento'
                    )
                )
                ->leftjoin(
                    'pre_temporales_movimientos as ptm',
                    'ptm.pre_temporal_id',
                    '=',
                    'p.id'
                )
                ->join(
                    'actividades_tipos as at',
                    'p.actividad_tipo_id',
                    '=',
                    'at.id'
                )
                ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
                ->Leftjoin('quiebres as q', 'p.quiebre_id', '=', 'q.id')
                ->leftjoin('motivos as m', 'm.id', '=', 'ptm.motivo_id')
                ->leftjoin('submotivos as s', 's.id', '=', 'ptm.submotivo_id')
                ->leftjoin('estados as e', 'e.id', '=', 'ptm.estado_id')
                ->leftjoin(
                    'estados_pre_temporales as ept', 'ept.id',
                    '=',
                    'ptm.estado_pretemporal_id'
                )
                ->leftjoin(
                    'pre_temporales_solucionado as pts',
                    'pts.id', '=',
                    'ptm.pre_temporal_solucionado_id'
                )
                ->join(
                    'pre_temporales_atencion as pa',
                    'pa.id',
                    '=',
                    'p.tipo_atencion'
                )
                ->Leftjoin(
                    'pre_temporales_averias as pta',
                    'pta.pre_temporal_id', '=',
                    'p.id'
                )

                ->whereBetween('p.created_at', array('2017-05-01 00:00:00','2017-05-11 23:59:59'))
                ->get();
        /*end query to get info*/

        /*if exits data so create a excel with data and save it in file and db*/
        if($data){
        	$rst=$data;														
	        $propiedades = array(
	          'creador'=>'Gerencia Modernizacion',
	          'subject'=>'Procesos y sus Actividades',
	          'tittle'=>'Reporte',
	          'font-name'=>'Bookman Old Style',
	          'font-size'=>8,
	        );

	        $cabecera = array(
	          'N°',        
	          'TICKET',
	          'TIPO_AVERIA',
	          'TIPO_ATENCION',
	          'QUIEBRE',
	          'ESTADOTICKET',
	          'CODAVERIA',
	          'CODCLI',
	          'CLIENTENOMBRE',
	          'CLIENTECELULAR',
	          'CLIENTETELEFONO',
	          'CLIENTEDNI',
	          'EMBAJADORNOMBRE',
	          'EMBAJADORCELULAR',
	          'EMBAJADORCORREO',
	          'EMBAJADORDNI',
	          'COMENTARIOTICKET',
	          'FECHAREGISTROTICKET',
	          'MOTIVOS',
	          'SUBMOTIVOS',
	          'ESTADOS',
	          'FECHAMOV',
	          'OBSMOV',
	          'USUARIOMOV',
	          'PRODUCTO',
	          'ACCION',
	          'ESTADOLEGADO',
	          'FECHAREGISTROLEGADO',
	          'FECHALIQUIDACION',
	          'CODIGOLIQUIDACION',
	          'DETALLELIQUIDACION',
	          'AREA',
	          'CONTRATA',
	         'ZONAL',
	         'ULTIMO_MOVIMIENTO'
	        );
	        $nameFile = "ReporteMovimientos_".Helpers::getRandomCode().".xls";
	        $path = 'uploads/';
			$publicPath = strpos(public_path($path), '\\') === false ? public_path($path) : str_replace('\\', '/', public_path($path));
        	Helpers::exportExcel($propiedades,'',$cabecera,$rst,1,$publicPath.$nameFile);

        	/*$registro = new ReportePedidoMovimiento();
        	$registro->archivo = $nameFile;
        	$registro->created_at = date('Y-m-d H:i:s');
        	$registro->save();*/
        }
        /*end if exits data so create a excel with data and save it in file and db*/

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	/*protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}*/

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	/*protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}*/

}
