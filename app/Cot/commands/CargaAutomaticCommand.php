<?php
namespace Cot\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Cot\controllers\PreTemporalController as PreTemporal;
use Cot\models\PreTemporal as PreTmp;
use Cot\models\PreTemporalUltimo as PreTmpUltimo;
use Cot\Repositories\CotMethodsRepository;

class CargaAutomaticCommand extends Command
{
    protected $_pretemporal;
    

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cot:cargautomatica';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cargar automaticamente cada hora data de atento';

    /**
     * Create a new command instance.
     *
     * @return void

       protected $_errorController;
    protected $_estadoRespuesta;

    public function __construct(ErrorController $errorController,
        EstadoRespuesta $estadoRespuesta)
    {
        $this->_errorController = $errorController;
        $this->_estadoRespuesta = $estadoRespuesta;
    }

     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \Auth::loginUsingId(697);//proceso automatico
        $row = 1;

        //$path = "/home/psi/Documentos";
        $path = strpos(base_path('archivos/input_atento/'), '\\') === false ? base_path('archivos/input_atento/') : str_replace('\\', '/', base_path('archivos/input_atento/')); 
        $latest_ctime = 0;
        $latest_filename = '';    

        $d = dir($path);        
        while (false !== ($entry = $d->read())) {
          $filepath = "{$path}/{$entry}";
          // could do also other checks than just checking whether the entry is a file
          if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
            $latest_ctime = filectime($filepath);
            $latest_filename = $entry;
          }
        }

        $valida = explode("_", $latest_filename);
        if($valida[0] == 'ENVIO' && $valida[1] == 'ATTO1LINEA'){
            $root = $path.$latest_filename;       
            $arch = fopen($root, "r");
            $name = basename($root).PHP_EOL;

            $cabecera = [];$data=[];
            if($arch){
                while (($datos = fgetcsv($arch, 1000, ",")) !== false) { //fgets
                    if($row == 1){
                        $datos[0] = "quiebre_id";
                        $datos[1] = "tipo_atencion";
                        $datos[2] = "tipo_fuente";
                        $datos[3] = "codactu";
                        $datos[4] = "codcli";
                        $datos[5] = "actividad_tipo_id";
                        $datos[6] = "cliente_nombre";
                        $datos[7] = "cliente_celular";
                        $datos[8] = "cliente_telefono";
                        $datos[9] = "cliente_correo";
                        $datos[10] = "cliente_dni";
                        $datos[11] = "contacto_nombre";
                        $datos[12] = "contacto_celular";
                        $datos[13] = "contacto_telefono";
                        $datos[14] = "contacto_correo";
                        $datos[15] = "contacto_dni";
                        $datos[16] = "embajador_nombre";
                        $datos[17] = "embajador_correo";

                        $datos[18] = "embajador_celular";
                        $datos[19] = "embajador_dni";
                        $datos[20] = "comentario";

                        $datos[21] = "fh_reg104";
                        $datos[22] = "fh_reg1l";
                        $datos[23] = "fh_reg2l";
                        $datos[24] = "cod_multigestion";
                        $datos[25] = "llamador";
                        $datos[26] = "titular";
                        $datos[27] = "direccion";
                        $datos[28] = "distrito";
                        $datos[29] = "urbanizacion";
                        $datos[30] = "telf_gestion";
                        $datos[31] = "telf_entrante";
                        $datos[32] = "operador";
                        $datos[33] = "motivo_call";
                        $cabecera[] = $datos;
                    } else {
                        $data[] = $datos;
                    }
                    $row++;
                }
            }

            if ($data) {
                $fila = 0;
                foreach ($data as $key => $value) {
                    foreach ($value as $index => $val) {
                        $data[$fila][$cabecera[0][$index]] = $val;
                        unset($data[$fila][$index]);
                    }
                    $fila++;
                }
            }

            $patrón = array('/.xlsx/','/.csv/','/.xls/');
            $latest_filename = preg_replace($patrón, '', $latest_filename);
            $createdAt = $this->option("created_at");

            // validando si existe ya un ticket con cod_multigestion
            foreach ($data as $key => $value) {
                $codMultigestion = isset($value[24]) ? $value[24] : "";
                if ($codMultigestion!="") {
                    $cabecerasPreTmp = PreTmp::where("cod_multigestion", "=", $codMultigestion)
                    ->where("estado_pretemporal_id", "<>", 7)
                    ->whereIn("quiebre_id", [51,52,56])
                    ->whereRaw("created_at = '{$createdAt}' ")
                    ->get();
                    if (count($cabecerasPreTmp) > 1) {
                        $contador = count($cabecerasPreTmp);
                        foreach ($cabecerasPreTmp as $key2 => $value2) {
                            if ($contador > 1) {
                                $ultimoPreTmp = PreTmpUltimo::where("pre_temporal_id", "=", $value2->id)
                                    ->where("estado_id", "<>", 6)
                                    ->where("estado_id", "<>", 17)
                                    ->first();
                                if (!is_null($ultimoPreTmp)) {
                                    $ultimoPreTmp->delete();
                                }
                                $value->delete();
                                $contador = $contador - 1;
                            }
                        }
                    }
                }
                
                //print_r($value);
                //dd();
            }
            $values = [
                "pedidos" => array(json_encode($data)),
                "file" => $latest_filename,
                "automatica" => 1
            ];
            \Input::replace($values);

            //Log::useDailyFiles(storage_path().'/logs/carga_automatica.log');
            //Log::info(['archivo'=>$latest_filename,'upload'=>'ok']);

            $repository = new CotMethodsRepository();
            $repository->archMasivo($data);

            /*$pretemporal = new PreTemporalController();
            $pretemporal->postProcesarmasivo();         */
        } else {
            //Log::useDailyFiles(storage_path().'/logs/carga_automatica.log');
            //Log::info(['archivo'=>$latest_filename,'upload'=>'Error']);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('created_at', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
