<?php
namespace Cot\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Cot\controllers\PreTemporalController;
use Repositories\CotPretemporalRepository;
use Cot\models\PreTemporal;


class CotEliminarRepetidos extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cot:coteliminarRepetidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Eliminar repetidos en cot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $pre_temporal = DB::table('pre_temporales as p')
                        ->select('p.cod_multigestion',DB::raw('COUNT(*) as repetidos'))
                        ->whereBetween('p.created_at',array("2017-12-20 00:00:00","2017-12-26 23:59:59"))
                        ->whereIn('p.quiebre_id', array(51,52,56))
                        ->where('p.usuario_created_at','=',697)
                        ->groupBy('p.cod_multigestion')
                        ->having('repetidos', '>', 1)
                        ->get();      

       if($pre_temporal){
            foreach ($pre_temporal as $key => $value) {
                $delete = PreTemporal::where('cod_multigestion','=',$value->cod_multigestion)
                        ->select('id')
                        ->whereBetween('created_at',array("2017-12-20 00:00:00","2017-12-26 23:59:59"))
                        ->whereIn('quiebre_id', array(51,52,56))
                        ->where('usuario_created_at','=',697)
                        ->orderBy('created_at','asc')
                        ->get()->toArray();
                
                for($i = 1;$i < $value->repetidos;$i ++){
                    $ticket = PreTemporal::find($delete[$i]['id']);
                    $ticket->delete();
                }                
            }
       }
       //SELECT id,cod_multigestion,count(*) FROM pre_temporales   WHERE DATE(created_at) = '2017-12-22' AND estado_pretemporal_id!=7 AND quiebre_id IN (51,52,56) GROUP BY cod_multigestion HAVING COUNT(*) > 1
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    /*protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }*/

}
