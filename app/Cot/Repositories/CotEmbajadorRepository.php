<?php namespace Cot\Repositories;

class CotEmbajadorRepository implements CotEmbajadorRepositoryInterface
{
    public function buscarEmbajador(){
        $tipoBusqueda = \Input::get('tipo');
        $busqueda = \Input::get('busqueda');
        $gestion = \Input::get('gestion');
        
        $query = \DB::table('pre_temporales as p')
            ->Leftjoin('actividades as a', 'a.id', '=', 'p.actividad_id')
            ->join(
                'actividades_tipos as at',
                'at.id',
                '=',
                'p.actividad_tipo_id'
            )
            ->join(
                'estados_pre_temporales as ept',
                'ept.id',
                '=',
                'p.estado_pretemporal_id'
            )
            ->Leftjoin(
                'pre_temporales_ultimos as ptm',
                'ptm.pre_temporal_id',
                '=',
                'p.id'
            )
            ->Leftjoin('motivos as m', 'ptm.motivo_id', '=', 'm.id')
            ->Leftjoin('submotivos as s', 'ptm.submotivo_id', '=', 's.id')
            ->Leftjoin('estados as e', 'ptm.estado_id', '=', 'e.id')
            ->select(
                \DB::raw('CONCAT("E",LPAD(p.id,5,0)) as ticket'),
                'p.id',
                'at.apocope as tipo_averia',
                'ept.nombre as estado_ticket',
                'p.codactu',
                'p.codcli',
                'p.cliente_nombre',
                'p.cliente_celular',
                'p.cliente_telefono',
                'p.cliente_dni',
                'p.embajador_nombre',
                //  'p.created_at as fecha_registro',
                \DB::raw(
                    'DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                        as fecha_registro'
                ),
                'm.nombre as motivo',
                's.nombre as submotivo',
                'e.nombre as estado'
            )->orderBy('p.created_at', 'desc');

        if ($busqueda != '') {
            switch ($tipoBusqueda) {
                    case 'ticket':
                        $datos= array();
                        $array = explode("E", strtoupper($busqueda));
                        if (count($array)>1) {
                            $ticketId = $array[1];
                            $query->where('p.id', '=', (int)$ticketId);
                        } else {
                            $query->where('p.id', '=', (int)$busqueda);
                        }
                        break;
                    case 'codactu':
                        $query->where('p.codactu', '=', $busqueda);
                        break;
                    case 'dni':
                        $query->where('p.embajador_dni', '=', $busqueda);
                        break;
            }
        }

        $query->where('p.gestion', '=', $gestion);
        $resultado = $query->limit(200)->get();       
        return $resultado;
    }
}
