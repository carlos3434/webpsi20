<?php namespace Cot\Repositories;

interface EventMasivaModelRepositoryInterface
{
    public function listado();
    public function getTicketsbyEvento();
}

