<?php namespace Cot\Repositories;

use Cot\models\EventoMasivaImg;
use Cot\models\EventoMasiva;
use Libraries\Helpers;
use Cot\models\PretemporalUltimo as Ultimo;
use Cot\Repositories\CotHelpRepository as Helper;
use Cot\Repositories\CotMethodsRepository as Method;
use Cot\models\PreTemporal as PreTemporales;

class EventoMasivaRepository implements EventoMasivaRepositoryInterface
{
    protected $_helper;
    protected $_method;
    public function __construct()
    {   
        $this->_method = new Method();
        $this->_helper = new Helper();
    }
  
   public function action(){     
        $inputs =  $this->_helper->convertInput();
        $eventoMasiva = '';         
        if(isset($inputs['idevento'])){ //update           
            $eventoMasiva = EventoMasiva::find($inputs['idevento']);    
            foreach (json_decode(json_encode($eventoMasiva),true) as $key => $value) {
                if (array_key_exists($key, $inputs)) {
                    $eventoMasiva->$key = $inputs[$key];

                    if(isset($inputs['cod_franqueo'])){
                        $eventoMasiva->fecha_registro_franqueo = date('Y-m-d H:i:s');
                        $eventoMasiva->usuario_liquida = \Auth::user()->id;
                        $eventoMasiva->estado = 5;
                    }
                    if(isset($inputs['inactivo'])){
                        $eventoMasiva->estado = 12;
                    }
                }
            }
            $eventoMasiva->save();

            if($eventoMasiva->inactivo == 1 || $eventoMasiva->estado == 5){
                $this->CerrarMasivaTickets($eventoMasiva->id,$eventoMasiva->estado,$eventoMasiva->obs_cancelacion);
            }
            $rst = 2;
        }else{ //insert checar por la fecha no lo encuentra el pluck   ;  
            $rst = 0;    
            if(isset($inputs['nodo']) && isset($inputs['troba'])){
                if($this->ValidateExist($inputs['nodo'],$inputs['troba']) == 0){
                    $inputs['nombre'] = $this->makeNombre($inputs);
                    $eventoMasiva = new EventoMasiva();
                    $eventoMasiva->fill($inputs);
                    $eventoMasiva->save();
                    $rst = 1;    
                }else{
                    $rst = 3; //ya existe
                }
            }
        }

        /*if there are images*/
        if($eventoMasiva != '' && $_FILES){
            $this->_helper->uploadFiles($_FILES,$eventoMasiva->id,'uploads/image_evento_masiva/',true);
        }
        return (isset($eventoMasiva)) ? array('rst' => $rst,'data' => $eventoMasiva) : 0;
    }

    public function ValidateExist($nodo,$troba){
        if($nodo && $troba){
            $exits = \DB::table('eventos_masiva')->select(\DB::raw('COUNT(id) as cantidad'))
                    ->where('nodo',$nodo)
                    ->where('troba',$troba)
                    ->WhereRaw('(inactivo!=1 AND estado!=5)')
                    ->WhereRaw('DATE(created_at)="'.date('Y-m-d').'"')->get();
            $cant = $exits[0]->cantidad;
            return $cant;            
        }
    }

    public function makeNombre($inputs){
        $nombre = $inputs['zonal']."/". $inputs['nodo']."/".$inputs['troba'];
        if($inputs['amplificador']){
            $nombre.="/".$inputs['amplificador'];
        }
        $nombre.="/".date('Y-m-d'); 
        return $nombre;
    }

    public function getTicketsMasiva($id_masiva){
        $data = \DB::table('pre_temporales')->select('id')
                    ->where('evento_masiva_id',$id_masiva)
                    ->where('estado_pretemporal_id',1)
                    ->get();       
        return $data;     
    }

    public function CerrarMasivaTickets($id_masiva,$estado,$comentario = ''){
        $tickets = $this->getTicketsMasiva($id_masiva);
        $mov = [];$data = [];
        if($tickets){
            foreach ($tickets as $key => $value) {
                $mov['pre_temporal_id'] = $value->id;

                if($estado == 12){
                    $mov['motivo_id'] =9;
                    $mov['submotivo_id'] = 3;
                    $mov['estado_id'] = 17;
                }else if($estado == 5){
                    $mov['motivo_id'] =33;
                    $mov['submotivo_id'] = 12;
                    $mov['estado_id'] = 6;     
                }

                $mov['estado_pretemporal_id'] = 7;
                //$mov['pre_temporal_solucionado_id'] = 1;
                $mov['codactu'] = '';
                $mov['observacion'] = \Auth::user()->usuario."|".\Auth::user()->full_name."|".$comentario;
                $this->_method->GenerarMov($mov);
            }
        }
        return 1;
    }

}