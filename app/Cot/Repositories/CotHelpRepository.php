<?php namespace Cot\Repositories;

use Cot\Repositories\CotRepositoryInterface;
use Cot\models\PreTemporal as PreTemporales;
use Cot\models\PretemporalMovimiento as Movimiento;
use Cot\models\PretemporalUltimo as Ultimo;
use Cot\models\EventoMasivaImg;

class CotHelpRepository implements CotHelpRepositoryInterface
{
     public function rulesCarga($data)
     {   
        $rst = [];$motives = [];
        if($data){
           $rules = PreTemporales::$rules;
            foreach ($data as $key => $value) {
                if(array_key_exists($key, $rules)){
                    if(array_key_exists('required', $rules[$key])){                                   
                        $rst[$key] = $this->getIdByTable($rules[$key]['table'], array($rules[$key]['field']), array($value));
                        if($rst[$key] == NULL && $rules[$key]['required'] == true){
                            $motives[] = "No se encontro $key en base de datos";                        
                        } 
                    }else if(array_key_exists('compound', $rules[$key])){
            			if((in_array($rst['quiebre_id'],array(51,52,56)))){
                                    $validation = [];
                                    foreach ($rules[$key]['data'] as $index => $val) {
                                        if($val['type']=='validate'){                              
                                            if(array_key_exists($val["field$index"], $rst)){
                                                if(!$val['table']){
                                                    $evaluate = in_array($rst[$val["field$index"]], explode(',', ($val['values'])));
                                                }else{
                                                    $evaluate = ($this->getIdByTable($val['table'], $val["field$index"], explode(',', ($val['values'])))) ? true : false;
                                                }                                   
                                                $validation[] = ($evaluate == $val['result']) ? true : false;
                                            }
                                        }else{  
                                            $evaluate = (PreTemporales::select('id')->where($val["field$index"], '=', ($val['values']) ? $val['values'] : $value)
                                                ->where($val["field$index"], '<>', '')->take(1)->pluck('id')) ? true : false;                               
                                            $validation[] = ($evaluate == $val['result']) ? true : false;
                                        }
                                    }
                                    $rst[$key] = (in_array(false, $validation)) ? false : $value;
                                    if($rst[$key] == false){
                                        $motives[] = "Registro ya existe";                        
                                    }
            			}                                                       
                    }
                }
            }
        }    
        return  
            array(
                'data' => $rst,
                'motive' => $motives                        
            );
     }

    public function headerAtento(){
        $header_atento = [
        'QUIEBRE','ATENCION','FUENTE', 'CODACTU','CODCLI','TIPOAVERIA','CLIENTENOMBRE','CLIENTECELULAR', 'CLIENTETELEFONO','CLIENTECORREO','CLIENTEDNI','CONTACTONOMBRE','CONTACTOCELULAR',' CONTACTOTELEFONO','CONTACTOCORREO','CONTACTODNI','EMBAJADORNOMBRE','EMBAJADORCORREO','EMBAJADORCELULAR','EMBAJADORDNI','COMENTARIO','FH_REG104','FH_REG1L','FH_REG2L','CODMULTIGESTION','LLAMADOR','TITULAR','Direccion','Distrito','Urbanizacion','TELF_GESTION','TELF_ENTRANTE','OPERADOR','MOTIVO_CALL'];
        return $header_atento;
    } 


    public function setMotiveUpload($fields = array())
    {
        if($fields){
            $motives = [];
            foreach ($fields as $key => $value) {
                if($key == 'COD_MULTIGESTION'){
                    if($value !== NULL){
                        $motives[]="Registro ya existe";
                    }
                }else{
                    if($value == NULL){
                        $motives[]="No se encontro $key en base de datos";
                    }else if($value == ''){
                        $motives[]="No registro $key";
                    }                    
                }
            }
            return (empty($motives)) ? true : $motives;
        }
    }

    //search into table according to position, example: filter 1 : nombre and search 1 :quiebre , match between them
    public function getIdByTable($table,$filter = array(),$search = array())
    {
        if($table && $filter && $search){
            $sql = "";
            $sql.= "SELECT id FROM $table";

            if($filter && $search){
                $sql.= " WHERE ";
                foreach ($filter as $key => $value) {
                    $sql.= ($key == 0) ? "$value='".$search[$key]."'" : " AND $value='".$search[$key]."'";                                                    
                }
            }
            $result = \DB::select($sql);        
            return ($result) ? $result[0]->id : NULL;            
        }
    } 


    /*
     * @param $data data para guardar
     * @param $format_file formato de archivo a guardar
     * @param $name_file archivo de ingreso
     * @param $repetidos numero de registros ya existentes
    */
    public function saveLog($data,$name_file,$route,$format_file,$repetidos='',$type=1,$header=''){
        $upload = [];
        if($data){
            $publicPath = strpos(public_path("$route/"), '\\') === false ? public_path("$route/") : str_replace('\\', '/', public_path("$route/"));
            foreach ($data as $key => $value) {
                $name_f = ($name_file) ? $name_file."_".$key.".".strtolower($format_file) : $key."_".\Auth::user()->nombre."_".date('Ymdh:i:s').".".strtolower($format_file);                
		if($key !== 'Rechazados' || count($value) !== 0){                
                    \Helpers::createExcel($value, $publicPath.$name_f, $format_file,$header);
                }
                $upload[] = $name_f;$upload[] = count($data[$key]);
            }        
            $rst = \UploadLog::create(['file_accept' => $upload[0],'file_refused' => (isset($upload[2])) ? $upload[2] : '','number_acept' => $upload[1],'number_refused' => (isset($upload[3])) ? $upload[3] : '','number_exist' => $repetidos,'type' => $type]);            
            return ($rst) ? true : false;         
        }
        return false;
    }


    public function convertInput(){
        $post = \Input::all();
        foreach ($post as $k => $value) {
            $bk = array("slct_","txt_","chk_","\rdb_","_modal");
            $rk = array("","","","","");
            $k = str_replace($bk, $rk, $k);
            $post[$k] = $value;
        }
        return $post;
    }

    public static function generateRandomString($length = 20) { 
      return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
    }

    public function uploadFiles($files,$identificador,$path,$save = false){
        foreach ($files as $key => $value) {      
            if($value['name']){
                $file = \Input::file($key);
                $tmpArchivo = $file->getRealPath();
                $original_name = $identificador."&".$this->generateRandomString()."&".$file->getClientOriginalName();
                $file->move($path, $original_name);

                if($save){
                    EventoMasivaImg::create([
                        'eventos_masiva_id' => $identificador,
                        'nombre_img' => $original_name,
                    ]);
                }   
            }             
        }
        return 1;
    }

}

