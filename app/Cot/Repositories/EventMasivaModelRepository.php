<?php namespace Cot\Repositories;

use Cot\models\EventoMasivaImg;
use Cot\models\EventoMasiva;
use Libraries\Helpers;
use Cot\models\PretemporalUltimo as Ultimo;
use Cot\Repositories\CotHelpRepository as Helper;
use Cot\Repositories\CotMethodsRepository as Method;
use Cot\models\PreTemporal as PreTemporales;

class EventMasivaModelRepository implements EventMasivaModelRepositoryInterface
{
    protected $_helper;
    protected $_method;
    public function __construct()
    {   
        $this->_method = new Method();
        $this->_helper = new Helper();
    }
  

    public function listado(){
        $query=  \DB::table('eventos_masiva as em')
                        ->select(
                            'em.id',
                            'em.nombre',
                            'em.nodo',
                            'em.empresa_id',
                            'em.zonal',
                            'em.tipo_trabajo',
                            'em.cod_liquidacion',
                            'em.sub_cod_liquidacion',
                            'em.observacion',
                            'em.tecnico_liquidacion',
                            'em.tecnologia',
                            'em.cod_franqueo',
                            'em.estado',
                            'em.obs_liquidacion',
                            \DB::raw('IFNULL(em.fecha_registro_franqueo,"") as registro_franqueo'),
                            \DB::raw('(select COUNT(*) from pre_temporales where evento_masiva_id=em.id) as cantickets'),
                            \DB::raw('(
                                SELECT COUNT(ptu.id) FROM pre_temporales pt 
                                LEFT JOIN pre_temporales_ultimos ptu ON ptu.pre_temporal_id=pt.id 
                                WHERE pt.evento_masiva_id=em.id) as atendidos'),
                            'em.troba',
                            'e.nombre as empresa',
                            'em.amplificador',
                            \DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) as userCreated'),
                            'em.created_at as fecharegistro',
                            'ept.nombre as estadoActual',
                            \DB::raw('(select GROUP_CONCAT(nombre_img) from eventos_masiva_image where eventos_masiva_id=em.id and estado=1) as imagenes'),
                            \DB::raw('CONCAT_WS(" ",u1.nombre,u1.apellido) as usuario_liquida'),
                            \DB::raw('(
                                SELECT COUNT(ptu.id) FROM pre_temporales pt 
                                LEFT JOIN pre_temporales_ultimos ptu ON ptu.pre_temporal_id=pt.id AND ptu.submotivo_id=87 
                                WHERE pt.evento_masiva_id=em.id) as con_servicio'),
                            \DB::raw('(
                                SELECT COUNT(ptu.id) FROM pre_temporales pt 
                                LEFT JOIN pre_temporales_ultimos ptu ON ptu.pre_temporal_id=pt.id AND ptu.submotivo_id=88 
                                WHERE pt.evento_masiva_id=em.id) as sin_servicio'),
                            \DB::raw('(
                                SELECT editar FROM submodulo_usuario WHERE submodulo_id=87 AND usuario_id='.\Auth::id().') as editar')
                        )
                        ->join('empresas as e','e.id','=','em.empresa_id')
                        ->join('estados_pre_temporales as ept','ept.id','=','em.estado')
                        ->Leftjoin('usuarios as u','u.id','=','em.usuario_created_at')
                        ->Leftjoin('usuarios as u1','u1.id','=','em.usuario_liquida')
                        ->where(function($query){
                                if ( \Input::get('fechas_busqueda') ) {
                                    $fechas = explode(' - ', \Input::get('fechas_busqueda'));
                                    $fechas[0]= $fechas[0].' 00:00:00';
                                    $fechas[1]= $fechas[1].' 23:59:59';
                                    $query->whereBetween('em.created_at', $fechas);
                                } 
                                if ( \Input::get('idevento') ) {
                                    $query->where('em.id','=', \Input::get('idevento'));
                                }
                                //$query->where('inactivo',0);                       
                            })
                        ->orderBy('em.created_at', 'desc')
                        ->get();
        return $query;
    }

    public function getTicketsbyEvento(){
        $query = \DB::table('pre_temporales as pt')
                        ->select(
                            'pt.evento_masiva_id as masivaid',
                            'pt.id',
                            'q.nombre as quiebre',
                            'pt.codcli',
                            \DB::raw('IFNULL(pt.codactu," ") as codaveria'),
                            'pt.quiebre_id',
                            'pt.cliente_nombre',
                            'pt.cliente_celular',
                            'pt.cliente_telefono',
                            'pt.contacto_celular',
                            'pt.contacto_telefono',
                            \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
                            'ptu.estado_id',
                            'e.nombre',
                            'pt.created_at as fecharegistro'
                        )
                        ->Leftjoin('pre_temporales_ultimos as ptu','ptu.pre_temporal_id','=','pt.id')
                        ->Leftjoin('estados as e','e.id','=','ptu.estado_id')
                        ->Leftjoin('quiebres as q','q.id','=','pt.quiebre_id')
                        ->where(function($query){
                                $query->where('pt.evento_masiva_id',\Input::get('idevento'));
                            }
                        )
                        ->get();
        return $query;
    }

    public function listadoSimple() {
        $query=  \DB::table('eventos_masiva as em')
                        ->select(
                            'em.id',
                            'em.nombre'
                        )
                        ->get();
        return $query;
    }
}
