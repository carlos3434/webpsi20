<?php namespace Cot\Repositories;

interface CotMethodsRepositoryInterface
{
	public function GenerarMov($array);
	public function archMasivo(array $data);
	public function getdataUpload();
    public function multiproceso();
	  public function DesAsigChangeQuiebre(array $tickets,$quiebres);
}


