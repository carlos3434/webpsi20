<?php namespace Cot\Repositories;

use Cot\models\PreTemporal as PreTemporales;
use Cot\models\PretemporalMovimiento as Movimiento;
use Cot\models\PretemporalUltimo as Ultimo;
use Cot\Repositories\CotHelpRepository as Helper;

class CotMethodsRepository implements CotMethodsRepositoryInterface
{
    protected $_helper;
    protected $registrados;

    public function __construct()
    {
        $this->_helper = new Helper();
    }
  
    public function GenerarMov($array)
    { 
        try {
            \DB::beginTransaction();
            $pre_temporal = PreTemporales::where("id", $array['pre_temporal_id'])->first();
            $pre_temporal->estado_pretemporal_id = $array['estado_pretemporal_id'];
            $pre_temporal->usuario_id = \Auth::id();
            if(isset($array['codactu']) && $array['codactu'] != ''){
                $pre_temporal->codactu = $array['codactu'];
            }
            $pre_temporal->save();

            $ultimo = $pre_temporal->ultimo; 
            if($ultimo){
                foreach (Ultimo::$rules as $key => $value) {         
                    if (array_key_exists($key, $array) && ($array[$key] != '' or $array[$key] != NULL) ) {
                        $ultimo->$key = $array[$key];
                    }
                }
                $pre_temporal->ultimo()->save($ultimo);
            }else{
                $ultimo = new Ultimo;
                $ultimo->fill($array);
                $ultimo->save();
                //$pre_temporal->ultimo()->save($ultimo);               
            }

            $ultimoMovimiento = ($pre_temporal->ultimoMovimiento) ? $pre_temporal->ultimoMovimiento->replicate() : '';
            if($ultimoMovimiento){
                foreach (Movimiento::$rules as $key => $value) {
                    if (array_key_exists($key, $array)) {
                        $ultimoMovimiento[$key] = $array[$key];
                    }
                }
                $ultimoMovimiento->save();            
            }else{
                $mov = new Movimiento;
                $mov->fill($array);
                $mov->save();
                //$pre_temporal->movimientos()->save($mov);
            }  
            \DB::commit();      
            return true;            
        } catch (Exception $exc) {
            \DB::rollback();
            $errorController = new \ErrorController();
            $errorController->saveError($exc);
            return false;
        }       
    }

	  public function DesAsigChangeQuiebre(array $tickets,$quiebre = '')
    {
        $reasignados = 0;
        foreach ($tickets as $key => $value) {
	      try{
                \DB::beginTransaction();
                $pre_temporal = \PreTemporal::find($value);
                if($quiebre != ''){ 
                    if($pre_temporal->ultimo && $pre_temporal->ultimo->estado_id == 17){
                    }else{
                        $pre_temporal->usuario_id='';
                        $pre_temporal->fecha_asignacion_usuario='';
                        $pre_temporal->quiebre_id=$quiebre;
                        $pre_temporal->save();
                        \PretemporalUsuario::where('pretemporal_id', '=', $value)->update(['estado' => '0']);            
                        $reasignados++;                            
                        
                    }
                }
                \DB::commit();   
            } catch (Exception $exc) {
                \DB::rollback();
                $errorController = new \ErrorController();
                $errorController->saveError($exc);             
            }       
        }
        return $reasignados;
    }

    /*
    @Regist and validate tickets
    @this function occur in averia masiva,automatic upload,manual upload 
    */
    public function archMasivo(array $data)
    {     
        $no_registrados = [];$registrados = [];$exists = 0;

        if($data = \Input::get('pedidos')){
            foreach ($data as $key => $value) {
                $rst = (is_array($value)) ? $value : json_decode($value, true);
                foreach ($rst as $index => $val) {			                     
		            array_pop($rst[$index]);
                    $obj = (!is_object($rst[$index])) ? (object) $rst[$index] : $rst[$index];
                    \DB::beginTransaction();
                    set_time_limit(0);
                    $validator = (\Input::get('evento_id')) ? array('motive'=>array()) : $this->_helper->rulesCarga($obj);
                    if(isset($validator['motive']) && empty($validator['motive'])){
                      	try {
                            $pre_temporal = new PreTemporales();
                            foreach ($obj as $keys => $element) {
                                if(isset($validator['data']) && array_key_exists($keys,$validator['data'])){
                                    $pre_temporal->$keys = $validator['data'][$keys];
                                }else{
                                    $pre_temporal->$keys = $element;                                    
                                }
                                $pre_temporal->estado_pretemporal_id = (\Input::has('automatica')) ? 10 : 1;
				                $pre_temporal->evento_masiva_id = (\Input::has('evento_id')) ? \Input::get('evento_id') : NULL;
                            }
                            $pre_temporal->save();
                            $registrados[]=$rst[$index];      
                        } catch (Exception $exc) {
                            $errorController = new \ErrorController();
                            $errorController->saveError($exc);
                            $no_registrados[]=$rst[$index];          
                        }                      
                    }else{
                        if(in_array("Registro ya existe", $validator['motive'])){ 
                            $exists+=1;
                        }else{
                            $rst[$index]['detalle'] =  implode($validator['motive'], ',');  
                            $no_registrados[]=$rst[$index];
                        }
                    }
                    \DB::commit();
                }
            }      

            $this->setRegistrados(count($registrados));                                             
            $type = (\Input::has('evento_id')) ? 3 : 1;
            $rst = $this->_helper->saveLog(['Registrados'=>$registrados,'Rechazados'=>$no_registrados],\Input::get('file'),'cargas_rpt','CSV',$exists,$type,$this->_helper->headerAtento());
            return ($rst) ? true : false;
        }
    }

    public function setRegistrados($registrados) {
        $this->registrados = $registrados;

    }
    
    public function getRegistrados() {
        return $this->registrados;
    }
    
    /*
    get data of upload with header 
    */
    public function getdataUpload() 
    {
        $data = array();
        $cabecera = array();
        $file = \Input::file('excel');
        $tmpArchivo = $file->getRealPath();
        $name = $file->getClientOriginalName();
        $column = '';

        $origin_name = 'uploads/' . $name;
        $path = 'uploads/';
        $file->move($path, $name);

        $publicPath = strpos(public_path($path), '\\') === false?
            public_path($path) :
            str_replace('\\', '/', public_path($path));

        $objPHPExcel = \PHPExcel_IOFactory::load($publicPath.$name);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        if ($column == '') {
            $highestColumn = $sheet->getHighestColumn();
        } else {
            $highestColumn = $column;
        }

        for ($row = 1; $row <= $highestRow; $row++) {
            $count_vacio = 0;
            $fila = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($row > 1){
                $error = 0;
                foreach ($fila[$row - 1] as $key => $value) {
                    $count_vacio = ($value == '' or $value == NULL) ? $count_vacio + 1 : $count_vacio + 0;   
                    $fila[$row - 1][strtoupper($cabecera[0][$key])] =  ($value == '' or $value == NULL) ? '' : $value;                   
                    unset($fila[$row - 1][$key]);
                }
                if($count_vacio < count($fila[$row -1]) - 1){
                    $data[] = $fila[$row - 1];
                }                
            }else{
                $fila[$row -1][21] = "FH_REG104";
                $fila[$row -1][22] = "FH_REG1L";
                $fila[$row -1][23] = "FH_REG2L";
                $fila[$row -1][24] = "CODMULTIGESTION";
                $fila[$row -1][25] = "LLAMADOR";
                $fila[$row -1][26] = "TITULAR";
                $fila[$row -1][32] = "OPERADOR";
                $fila[$row -1][33] = "MOTIVO_CALL";
                $cabecera[] = $fila[$row -1];
            }
        }
        return ($data) ? $data : false;
    }

    public function multiproceso(){
        $arch = \Input::File('archivocolumna');
        $patrón = array('/.xlsx/','/.csv/','/.xls/');
        $name = preg_replace($patrón, '', $arch->getClientOriginalName());
                $row = 0;$log = [];                
                if (($gestor = fopen($arch, "r")) !== false) {
                    while (($datos = fgetcsv($gestor, 1000, ",")) !== false) {
                        if($row != 0){                           
                            $ticket = substr($datos[1], 1, 6);
                            $msj = 'No se encuentra ticket';
                            $pre_temporal = PreTemporales::find($ticket);

                            if(isset($pre_temporal->id)){
                                $msj = 'Estado no es recepcionado';
                                if($datos[0] == 'ELIMINAR' && $pre_temporal->estado_pretemporal_id == 1){
                                    $pre_temporal->delete();
                                    $msj = 'Eliminado';    
                                }else if($datos[0] == 'CAMBIO_QUIEBRE' && $pre_temporal->estado_pretemporal_id == 1){
                                    $msj = 'Quiebre a cambiar no existe';
                                    $quiebre_id = \Quiebre::select('id')->where('nombre', $datos[6])->take(1)->pluck('id');
                                    if($quiebre_id){
                                        $pre_temporal->quiebre_id = $quiebre_id;$pre_temporal->usuario_id='';
                                        $pre_temporal->save();                                        
                                        $msj = 'Cambio de quiebre exitoso';
                                    }
                                }else if($datos[0] == 'MOVIMIENTO' && $pre_temporal->estado_pretemporal_id == 1){
                                    $obj = array('motivo' =>$datos[2],'sub_motivo'=>$datos[3],'estado_id'=>$datos[4],'tipo_solucion'=>$datos[7]);
                                    $rules = $this->_helper->rulesCarga($obj);
                                    if(isset($rules['motive']) && empty($rules['motive'])){
                                        
                                        if($rules['data']['motivo'] == 23 && $rules['data']['sub_motivo'] == 58 && $rules['data']['tipo_solucion'] == NULL){
                                            $msj = 'Combinacion Motivo y submotivo requiere de tipo de solucion';
                                        }else if((
                                            ($rules['data']['motivo'] == 24 && $rules['data']['sub_motivo'] == 60) || 
                                            ($rules['data']['motivo'] == 24 && $rules['data']['sub_motivo'] == 75)) && $datos[8] == NULL){
                                            $msj = 'Combinacion Motivo y submotivo requiere de codigo averia';
                                        }else{
                                            $msj = 'Error al generar movimiento';
                                             //estado ticket
                                                $estado = '';
                                                switch ($rules['data']['motivo']) {
                                                    case '23':
                                                    case '24':
                                                    case '25':
                                                    case '28': 
                                                    case '18': $estado = 2;
                                                        break;
                                                    case '19': $estado = 3;
                                                        break;
                                                    case '20': $estado = 4;
                                                        break;
                                                    case '21': $estado = 6;
                                                        break;
                                                    case '22': $estado = 5;
                                                        break;
                                                    default: $estado = 1;
                                                        break;
                                                }

                                            if($this->GenerarMov(array('pre_temporal_id'=>$ticket,'motivo_id'=>$rules['data']['motivo'],'submotivo_id'=>$rules['data']['sub_motivo'],'estado_pretemporal_id'=>$estado,'estado_id'=>$rules['data']['estado_id'],'pre_temporal_solucionado_id'=>$rules['data']['tipo_solucion'],'codactu'=>$datos[8],'observacion'=>$datos[5]))){                                                   
                                                   $msj = 'Movimiento Generado';                                                       
                                            }
                                        }                                       
                                    }else{
                                        $msj = implode($rules['motive'], ",");
                                    }
                                }
                            }                            
                            $datos[9] = $msj;
                            $log[]=$datos;
                        }else{
                            $log[]=$datos;
                        }
                        $row++;
                    }                   
                    if(count($log) > 0){
			            $cabecera[] = $log[0];
                        unset($log[0]);
                        $rst = $this->_helper->saveLog(['Multiproceso'=>$log], $name, 'proceso_masivo', 'CSV', '', 2,$cabecera[0]);
                        return ($rst) ? 1 : 0;                                    
                    }
                }        
                return 0;
    }
}

