<?php

namespace Cot;
use Illuminate\Support\ServiceProvider;
use Cot\Repositories;

class CotServiceProvider extends ServiceProvider {

  /**
   * Register
   */
    public function register()
    {
        $this->app->bind('Cot\Repositories\EventoMasivaRepositoryInterface', 'Cot\Repositories\EventoMasivaRepository');
        $this->app->bind('Cot\Repositories\EventMasivaModelRepositoryInterface', 'Cot\Repositories\EventMasivaModelRepository');
        $this->app->bind('Cot\Repositories\CotHelpRepositoryInterface','Cot\Repositories\CotHelpRepository');
        $this->app->bind('Cot\Repositories\CotMethodsRepositoryInterface','Cot\Repositories\CotMethodsRepository');
        $this->app->bind('Cot\Repositories\CotPretemporalRepositoryInterface','Cot\Repositories\CotPretemporalRepository');
        $this->app->bind('Cot\Repositories\CotEmbajadorRepositoryInterface','Cot\Repositories\CotEmbajadorRepository');
    }
}
