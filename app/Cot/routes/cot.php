<?php 

Route::group(['before' => 'auth'], function() {
    Route::controller('pretemporal', 'Cot\controllers\PreTemporalController');
    Route::controller('estado_pretemporal', 'Cot\controllers\EstadoPreTemporalController');
    Route::controller('pretemporal_rrss', 'Cot\controllers\PreTemporalRrssController');
    Route::controller('pretemporal_atencion', 'Cot\controllers\PreTemporalAtencionController');
    Route::controller('pretemporal_solucion', 'Cot\controllers\PreTemporalSolucionController');
    Route::controller('programadormovimiento', 'Cot\controllers\ProgramadorMovimientoController');
});

Route::controller('eventomasivo', 'Cot\controllers\EventoMasivoController');
Route::controller('embajador', 'Cot\controllers\PreTemporalEmbajadorController');
Route::controller('usuariohorario', 'Cot\controllers\UsuarioHorarioController');
Route::controller('grupocot', 'Cot\controllers\GrupocotController');
Route::controller('usuarioexoneracion', 'Cot\controllers\UsuarioExoneracionController');


Route::post(
    'gestion101/embajador', function () {
        return View::make('public.embajador');
    }
);
Route::get(
    'gestion101/rrss', function () {
        return View::make('public.rrssform');
    }
);
Route::get(
    'gestion101/embajadorget', function () {
        return View::make('public.embajadorget');
    }
);
?>
