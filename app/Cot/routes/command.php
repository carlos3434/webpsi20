<?php
Artisan::add(new \Cot\commands\ActualizarPreTemporalesCommand);
Artisan::add(new \Cot\commands\ReporteMovimientoCommand);
Artisan::add(new \Cot\commands\CargaAutomaticCommand);
Artisan::add(new \Cot\commands\EnvioAtentoCommand);
Artisan::add(new \Cot\commands\ProcesoGenerarMovimientosTemp);
Artisan::add(new \Cot\commands\CotEliminarRepetidos);
