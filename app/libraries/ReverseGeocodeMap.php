<?php

class ReverseGeocodeMap
{

    /**
     * echo getAddress($lat,$lon);
     * Test
     * @param double $lat
     * @param double $lon
     * @return string
     */
    public static function obtenerUbicacion($lat, $lon)
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" .
                $lat . "," . $lon . "&sensor=false";
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        $address = [];
        if ($status == "OK") {
            foreach ($data->results[0]->address_components as 
                $address_component) {

                $address[$address_component->types] = 
                    $address_component->long_name;
//                if (in_array('street_number', $address_component->types)) {
//                    $street_number = $address_component->long_name;
//                }
            }
        } else {
            $address = $status;
        }
        return $address;
    }

}
