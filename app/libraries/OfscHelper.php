<?php
use Ofsc\Resources;
use Ofsc\Activity;

class OfscHelper
{
    public static function getResource(
        $type = "BK",
        $resources = ["inicial" => [], "final" => []
        ]
    ) {
        $recursoTmp = $resources["inicial"];
        $resource = new Resources();
        $recursosActivos = [];
        $bucketsActivos = $resources["final"];
        foreach ($recursoTmp as $key => $value) {
            $recurso = $resource->getResources($value["id"]);
            $resultado = isset($recurso->data)? $recurso->data : [];

            foreach ($resultado as $key2 => $value2) {
                if ($value2["status"] === "active") {
                    if ($value2["type"] === $type) {
                        $bucketsActivos[] = $value2;
                    } else {
                        $recursosActivos[] = $value2;
                    }
                }
            }
        }
        return ["activos" => $recursosActivos, "final" => $bucketsActivos];

    }

    public static function getIdBucket($resources = [])
    {
        $data = [];
        foreach ($resources as $key => $value) {
            $data[] = $value["id"];
        }
        
        return $data;
    }

    public static function getFileResource(
        $type = "BK",
        $filtros = ["status" => "active"]
    ) {
        $from = date("Y-m-d");
        $rutaFolderResources = public_path()."/json/resources";
        //$fileName = preg_replace("/-/", "", $from).".json";
        $fileName = "resources.json";
        $tipoResourceFolder = "";
        switch ($type) {
            case 'PR':
                $tipoResourceFolder = "tecnicos";
                break;
            
            default:
                $tipoResourceFolder = "buckets";
                break;
        }
        $rutaFile = $rutaFolderResources."/".$tipoResourceFolder."/".$fileName;
        if (File::exists($rutaFile)) {
            $data = json_decode(File::get($rutaFile), true);
            foreach ($data as $key => $value) {
                if ($value["type"] !== $type) {
                    unset($data[$key]);
                }
                $data[$key]["time"] = date("Y-m-d");
            }
            // Aplicando filtros
            foreach ($data as $key => $value) {
                foreach ($filtros as $key2 => $value2) {
                    if (isset($data[$key]) && !($value[$key2] === $value2)) {
                        unset($data[$key]);
                    }
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    public static function getImage($aid = 0)
    {
        $tiposImagenesToa = \Config::get("ofsc.image");
        $imagenes = [];
        $objActivity = new Activity;
        $pathImagenesToa = public_path("img/ofsc/");
        FileHelper::validaCrearFolder($pathImagenesToa);

        foreach ($tiposImagenesToa as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $response = $objActivity->getFile(
                    $aid,
                    $key2
                );
                if (isset($response->result_code) && $response->result_code == 0) {
                    //foreach ($response->file_data as $val) {
                        $imagenes[$key][$key2] = $response->file_data;
                    //}
                }
            }
        }
        return $imagenes;
    }

    public static function getActividadesByBucket($buckets = [])
    {
        $pathActividades = public_path("json/actividades/");
        $actividades = [];
        foreach ($buckets as $key => $value) {
            $file = $pathActividades.$value."/".date('Ymd').".json";
            if (File::exists($file)) {
                $actividadestmp = json_decode(File::get($file), true);
                foreach ($actividadestmp as $key2 => $value2) {
                    if (isset($value2["resource_id"])) {
                        $actividades[$value][$value2["resource_id"]][] = $value2;
                    }
                    
                }
            }
        }
        
        foreach ($actividades as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $columnorder = [];
                foreach ($value2 as $key3 => $value3) {
                    $columnorder[] = $value3["start_time"];
                }
                array_multisort($columnorder, SORT_ASC, $actividades[$key][$key2]);
                $orden = 1;
                foreach ($actividades[$key][$key2] as $key3 => $value3) {
                    $actividades[$key][$key2][$key3]["orden"] = $orden;
                    $orden++;
                }
            }

        }
        return $actividades;
    }

    public static function getEstadoUltimaUbicacionTecnico()
    {

    }
}
