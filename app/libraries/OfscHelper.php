<?php

class OfscHelper
{
	public static function updateEstadoOfscbyGestion($codactu = 0)
	{
		if ($codactu > 0) {
		$ultimoMovimiento = DB::table("ultimos_movimientos")
                            ->select ("estado_ofsc_id", "codactu", "estado_ofsc_id")
                            ->where("codactu", "=", $codactu )
                            ->orderBy("id", "desc")
                            ->get();

	               if (count($ultimoMovimiento) > 0) {
	                   $update = \EnvioOfsc::where("codactu", "=", $codactu)
	                        ->update(array("estado_ofsc" => $ultimoMovimiento[0] ->estado_ofsc_id));
	                    }
		}
	}

	public static function saveUltimoEnvioOfsc($data = array())
	{
		if (Input::has("codactu")) {
			$permiso = \Toa::ValidaPermiso();
			if (count($permiso) > 0 && Input::get("codactu") > 0 ) {
				$ultimoEnvioOfsc = \EnvioOfscUltimo::get(array("codactu" => Input::get("codactu")));

				$envioOfsc = OfscHelper::getUltimoEnvioOfsc(
						array("codactu" => Input::get("codactu"),
							"accion" => "inbound_interface")
						);
				if (count($envioOfsc) > 0)
						$queryData["id_ultimo_envio"] = $envioOfsc[0]->id;

				$empresa = Empresa::find(Input::get("empresa_id"));
				if (isset( Auth::user()->id))
					$queryData["user_updated"] = Auth::user()->id;
				else
					$queryData["user_updated"] = 697;
				$queryData["nodo"] = Input::get("mdf");
				$queryData["tipo_actividad_id"] =  Input::get('actividad_tipo_id');
				//$queryData["aid"] = $data["aid"];
				//$queryData["tipo_envio"] = $data["tipo_envio"];
				//$queryData["use_from"] = $data["use_from"];
				$queryData["empresa"] = $empresa["nombre"];
				$queryData["id_gestion_movimiento"] = $data["id_gestion_movimiento"];

				if (count($ultimoEnvioOfsc) > 0) {
					$queryData["numintentos"] = $ultimoEnvioOfsc[0]->numintentos+1;
					$queryData["fec_updated"] = date("Y-m-d H:i:s");
					DB::table("envios_ofsc_ultimo")
						->where(array(
							"codactu" => Input::get("codactu"),
							"id" => $ultimoEnvioOfsc[0]->id
							)
						)
						->update($queryData);

				} else {
					$queryData["codactu"] = Input::get("codactu");
					DB::table("envios_ofsc_ultimo")->insert($queryData);
				}
			}
		}
	}

	public static function getUltimoEnvioOfsc($where = array())
	{
		if (count($where) > 0) {
			return DB::table("envios_ofsc")
				->where($where)
				->orderBy("id", "desc")
				->get();
		} else {
			return array();
		}
	}
}