<?php
use Ofsc\Inbound;
class Helpers
{
    /**
     * retorna la cantidad de registros en gestiones_movimientos segun tecnico,
     * entre fechaIni y fechaFin, entre hora_inicio y hora_fin, y dia, segun la
     * ultima gestion en gestiones_movimientos.
     *
     * @return Response
     * @param $fechaIni '2015-03-10'
     * @param $fechaFin '2015-03-10'
     * @param $tecnicoId 20
     * @param $hora     09:00:00
     * @param $diaId    [1-7]
     * Helpers::buscarGestion()
     */
    public static function buscarGestion(
        $fechaIni, $fechaFin, $tecnicoId, $hora, $diaId
        )
    {

        $query = "SELECT
                COUNT(g.id) as existe
                FROM
                gestiones g
                JOIN gestiones_movimientos gm ON g.id=gm.gestion_id
                JOIN (
                    SELECT MAX(gm2.id) id
                    FROM gestiones_movimientos gm2
                    INNER JOIN estado_motivo_submotivo ems
                    ON (ems.estado_id=gm2.estado_id
                    AND ems.submotivo_id=gm2.submotivo_id
                    AND ems.motivo_id=gm2.motivo_id
                    AND ems.req_horario=1
                    AND ems.estado=1
                    )
                    GROUP BY gm2.gestion_id
                ) mx
                ON gm.id=mx.id
                JOIN actividades a ON g.actividad_id=a.id
                JOIN horarios h ON gm.horario_id=h.id
                WHERE gm.estado_id IN (SELECT estado_id
                                       FROM estado_motivo_submotivo
                                       WHERE req_horario=1 AND estado=1)
                AND gm.fecha_agenda BETWEEN ? AND ?
                AND gm.tecnico_id=?
                AND ? BETWEEN hora_inicio AND hora_fin
                AND gm.dia_id=?
                AND gm.estado=1";

        $resultado= DB::select(
            $query,
            array(
                $fechaIni,
                $fechaFin,
                $tecnicoId,
                $hora,
                $diaId
            )
        );
        foreach ( $resultado as $val ) {
            $resultado = $val->existe;
        }
        return $resultado;

    }
    public static function isValidXml($content)
    {
        $content = trim($content);
        if (empty($content)) {
            return false;
        }
        //html go to hell!
        if (stripos($content, '<!DOCTYPE html>') !== false) {
            return false;
        }

        libxml_use_internal_errors(true);
        simplexml_load_string($content);
        $errors = libxml_get_errors();
        libxml_clear_errors();

        return empty($errors);
    }
    public static function getEstadoHtml($status)
    {
      $html = '<small class="label text-center label-estado bg-NOMBRE_CLASE"><i class="fa fa-ICONO"></i></small>';

      switch ($status) {
        case 'info':
          $html = str_replace('NOMBRE_CLASE', 'blue', $html);
          $html = str_replace('ICONO', 'circle-o', $html);
            break;
        case 'danger':
          $html = str_replace('NOMBRE_CLASE', 'red', $html);
          $html = str_replace('ICONO', 'circle-o', $html);
            break;
        case 'success':
          $html = str_replace('NOMBRE_CLASE', 'green', $html);
          $html = str_replace('ICONO', 'circle', $html);
            break;
        case 'warning':
          $html = str_replace('NOMBRE_CLASE', 'yellow', $html);
          $html = str_replace('ICONO', 'circle-o', $html);
            break;
        default:
          $html = str_replace('NOMBRE_CLASE', 'red', $html);
          $html = str_replace('ICONO', 'circle-o', $html);
            break;
      }
      return $html;
    }
    public static function getResponseJson($data, $mensaje = '', $extra = array())
    {
        $output = array();
        //if (!empty($data)) {
            $output['status']  = 'success';
            $output['mensaje'] = $mensaje;
            $output['data']    = $data;
        //}

        $output = array_merge($output, $extra);
        return \Response::json($output);
    }
    public static function getAccionesHtml($modal, $id)
    {
      $html =' <a data-toggle="modal" ';
      $html.=' data-target="#'.$modal.'"';
      $html.=' data-id="'. $id . '" ';
      $html.=' class="btn btn-primary"> ';
      $html.=' <i class="fa fa-eye"></i></a> ';

      return $html;
    }

    public static function getRandomCode(){
      $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-)(.:,;";
      $su = strlen($an) - 1;
      return substr($an, rand(0, $su), 1) .
          substr($an, rand(0, $su), 1) .
          substr($an, rand(0, $su), 1) .
          substr($an, rand(0, $su), 1) .
          substr($an, rand(0, $su), 1) .
          substr($an, rand(0, $su), 1);
    }
    /**
     * Metodo para crear request entre controladores
     *
     * @param type $url Metodo destino
     * @param type $method Metodo de envio (GET o POST)
     * @param type $data Arreglo de datos
     * @param type $json Si el retorno es json o array
     * @return type JSON o ARRAY
     */
    public static function ruta($url, $method, $data, $json=true)
    {
        //Datos enviados, vector
        Input::replace($data);
        //Crea Request via GET o POST
        $request = Request::create($url, $method);
        //Obtener response
        $response = Route::dispatch($request);
        //Solo contenido, en formato json
        $content = $response->getContent();

        if ($json) {
            //Retorna formato JSON
            return $content;
        } else {
            //Retorna un arreglo
            return json_decode($content);
        }
    }

    /**
     * Convierte una clase STD to Array
     * @param type $obj La StdClass
     * @return array
     */
    public static function stdToArray($obj,$tipo=true)
    {
        return json_decode(json_encode($obj), $tipo);
    }
    /**
     * Convierte una clase STD to Array
     * @param type $obj La StdClass
     * @return array
     */
    public static function limpia_campo_mysql_text($text)
    {
        $text = preg_replace('/<br\\\\s*?\\/??>/i', "\\n", $text);
        return str_replace("<br />", "\n", $text);
    }
    /**
     * Concatena fecha y hora a la caena ingresada
     * @param type $text string
     * @return $filename string
     */
    public static function convert_to_file_excel($text)
    {
        $fecha = date("d/m/Y");
        $hora = date("h:i:s");
        $hh = substr($hora, 0, 2);
        $mm = substr($hora, 3, 2);
        $ss = substr($hora, 6, 2);
        $hora = $hh."_".$mm."_".$ss;

        $filename = $text.'_'.$fecha."-".$hora.".xls";
        return $filename;
    }
    /**
     * Exportar un array a Excel
     * @param type $array string
     * @param type $text string
     * @return type csv
     */
    public static function exportArrayToExcel($reporte, $fileName,$excluir=[])
    {
      $table = '';
        if (count($reporte)>0) {
            $filename = Helpers::convert_to_file_excel($fileName);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename);
            header('Expires: 0');
            header(
                'Cache-Control: must-revalidate, post-check=0, pre-check=0'
            );
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');

            $n = 1;
            foreach ($reporte as $data) {
                //Encabezado
                if ($n == 1) {
                    foreach ($data as $key=>$val) {
                        if(!in_array($key, $excluir)){
                          echo $key . "\t";                          
                        }
                    }
                    echo "\r\n";
                }
                //Datos
                foreach ($data as $ky => $val) {
                    if(!in_array($ky, $excluir)){
                      $val = str_replace(
                          array("\r\n", "\n", "\n\n", "\t", "\r"),
                          array("", "", "", "", ""),
                          $val
                      );
                      echo $val . "\t";
                    }
                }
                echo "\r\n";
                $n++;
            }
        } else {
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$table
                )
            );
        }
    }
    /**
     * Concatena fecha y hora a la caena ingresada
     * @param type $fecha string {aaaa-mm-dd [hh:mm:ss]]}
     * @return $fecha string {dd-mm-aaaa [hh:mm:ss]}
     */
    public static function convert_to_date($fecha, $option = 'date')
    {
        list($fecha,$hora) = explode(" ", $fecha);
        list($anio,$mes,$dia) = explode("-", $fecha);

        if ($option=='date')
            $fecha = $dia."-".$mes."-".$anio;
        else
            $fecha = $dia."-".$mes."-".$anio." ".$hora;

        return $fecha;
    }

    /**
     * Retorna el contenido de un archivo en formato JSON
     * @param type $file
     * @return type JSON
     */
    public static function fileToJsonAddress($file, $cabecera=1)
    {
        $data = array();
        $gestor = fopen($file, "r");
        $n = 0;
        if ($gestor) {
            while (($bufer = fgets($gestor, 4096)) !== false) {
                if ($n >= $cabecera) {
                    $s = array("á", "é", "í", "ó", "ú", "\r", "\n", "\r\n", "\"");
                    $r = array("a", "e", "i", "o", "u", "", "", "", "");

                    $line = str_replace($s, $r, $bufer);

                    $data[] = utf8_encode($line);
                }
                $n++;
            }
            fclose($gestor);
        }

        return $data;
    }
    /**
     * convertir long a imagen
     */
    public static function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "w+");

        //$data = explode(',', $base64_string);
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);

        return $output_file;
    }
    /** 
      *Validar su Orden pertenecia a un quiebre de Ofsc
    */
    public static function cancelarOrdenOfsc ($codactu, $oldQuiebreId = 0){
        $toa = new Toa();
        $permiso = 0;//sin permiso de envio a toa
        if ($toa->obtenerConfiguracion(3)==true) {
            //si hay onfiguracion de envio, consultar si tiene permiso.
            $permiso = $toa->validacionIndividual($codactu);
        }
        if ($permiso==1) {
            //cancelar
            $properties["XA_NOTE"] = "Cancelado por cambio de quiebre";
            $inbound = new Inbound();
            $inbound->cancelActivity(0, $codactu, $properties);
        }
    }
    /**
     * actualizar el codigo segun los parametros contenidos en $array y $sql
     *  Si encuentra ->
     *    1.1 Si en ultimos_movimientos, estado_legado='LIQUIDADO' => FIN (msg: LIQ EN LEGADO)
     *    1.2 Si estado en webPSI = 'CANCELADO' => FIN (msg: ORDEN CANCELADA)
     *    1.3 Si tiene fecha_agenda fecha_agenda >= hoy
     *        1.3.1   Si forzar cambio => UPDATE ultimos_movimientos, fecha_agenda = '', estado='pendiente'
     *                     => UPDATE gestiones_detalles
     *                     => INSERT GESTIONES_MOVIMIENTOS
     *            (msg: Se actualiza ... )
     *        1.3.2   Si no fuerza cambio => FIN (msg: NO se actualiza por...)
     *    1.4 actualizar sin cambiar ultimos_movimientos
     *  No encuentra -> Buscar en temporal
     *    2.1 Buscar en averias_criticos_final => Actualiza => FIN
     *    2.2 Buscar en tmp_provision  => Actualiza => FIN
     *  Si no encuentra (gestion , temporal)
     * @param type string $codigo
     * @param type array $array
     * @param type $sql
     * @return type array
     *           $row = array(
     *          'codigo'=>$averia,
     *          'quiebre'=>$quiebre,
     *          'contrata'=>$empresa,
     *          'estado'=>$respuesta
     *          );
     */
    public static function actualizarQuiebre($codigo, $quiebreId, $quiebre, $empresaId, $empresa, $forzar)
    {
        $gestion = GestionDetalle::getGestiones($codigo);
        $cantGestion = count($gestion);
        $temporal = GestionDetalle::getGestionesTemporales($codigo);
        $cantTemporal = count($temporal);
        $hoy = date("Y-m-d");
        $mensaje='';
        $sql = '';
        if ($cantGestion>0) {//1
            $gestionId=$gestion[0]->id;
            $estadoLegado=$gestion[0]->estadoLegado;
            $estadoId=$gestion[0]->estadoId;
            $fechaAgenda=$gestion[0]->fechaAgenda;
            $oldQuiebreId=$gestion[0]->quiebreId;
            $oldEmpresaId=$gestion[0]->empresaId;
            $oldQuiebre=$gestion[0]->quiebre;
            $oldEmpresa=$gestion[0]->empresa;
            $origen=$gestion[0]->origen;
            $tipoActuacion=$gestion[0]->tipoActuacion;
            $tipoMotivo=$gestion[0]->tipoMotivo;
            $elem = [
                'quiebre' => $quiebre, 'origen' => $origen, 
                'actuacion' => $tipoActuacion, 'motivo' => $tipoMotivo
            ];
            $actividadTipo = Helpers::asignarActividadTipoPorQuiebre($elem);
                
            //validar empresa y/o quiebre igual
            if ($quiebre!='' && $empresa!='') {//selecciona quiebre y contrata
                if ($oldQuiebreId!=$quiebreId && $oldEmpresaId!=$empresaId) {
                    $array['quiebre_id']=$quiebreId;
                    $array['empresa_id']=$empresaId;
                    $mensaje="Se actualizo Quiebre y Contrata en PSI";
                } elseif ($oldQuiebreId!=$quiebreId) {
                    $array['quiebre_id']=$quiebreId;
                    $mensaje="Se actualizo solo quiebre, No se actualizo Contrata de ".$oldEmpresa." a ".$empresa;
                } elseif ($oldEmpresaId!=$empresaId) {
                    $array['empresa_id']=$empresaId;
                    $mensaje="Se actualizo solo Contrata, No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre;
                } else {//igual ambos
                    return "No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre.', ni Contrata '.$oldEmpresa." a ".$empresa;
                }
            } elseif ($quiebre!='') {//selecciona solo quiebre
                if ($oldQuiebreId!=$quiebreId) {//el quiebre anterior y el actual son diferentes
                    $array['quiebre_id']=$quiebreId;
                    $mensaje="Se actualizo Quiebre en PSI";
                } else {
                    return "No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre;
                }
            } elseif ($empresa!='') {//selecciona solo empresa
                if ($oldEmpresaId!=$empresaId) {//la contrata anterior es diferente a la actual
                    $array['empresa_id']=$empresaId;
                    $mensaje="Se actualizo Contrata en PSI";
                } else {
                    return "No se actualizo Contrata de ".$oldEmpresa." a ".$empresa;
                }
            } else {
                return "No se actualizo, no ha seleccionado quiebre ni contrata";
            }
            if (!is_null($actividadTipo)) {
                $array['actividad_tipo_id']=$actividadTipo;
                $mensaje .=". ActividadTipo: $actividadTipo";
            }
            if ($estadoLegado=='LIQUIDADO') {//1.1
                return "No se actualizo, se encuentra LIQUIDADO en LEGADO";
            } elseif ($estadoId==4) {//1.2
                return "No se actualizo, orden CANCELADA";
            } elseif ($fechaAgenda>=$hoy) {//1.3
                if ($forzar=='1') {//1.3.1
                    $respuesta=GestionDetalle::updateGestiones($codigo, $gestionId, $array, 'agendado', $oldQuiebre, $oldEmpresa);
                    //si hubo un error
                    if ($respuesta==0) {
                        return "Ocurrio un error al intentar actualizar";
                    } else {
                        // validar si la orden fue asignada a OFSC
                        Helpers::cancelarOrdenOfsc($codigo, $oldQuiebreId);
                        return $mensaje;
                    }
                } else {//1.3.2
                    return "No se actualizo, tiene agenda pendiente";
                }
            } else {//1.4
                $respuesta=GestionDetalle::updateGestiones($codigo, $gestionId, $array, '', $oldQuiebre, $oldEmpresa);
                //si hubo un error
                if ($respuesta==0) {
                    return "Ocurrio un error al intentar actualizar";
                } else {
                    Helpers::cancelarOrdenOfsc($codigo, $oldQuiebreId);
                    return $mensaje;
                }
            }
        } else {
            if($cantTemporal>0) {//2
                $actividad = $temporal[0]->actividad;
                $oldQuiebre = $temporal[0]->quiebre;
                $oldEmpresa = $temporal[0]->empresa;
                $origen=$temporal[0]->origen;
                $tipoActuacion=$temporal[0]->tipoActuacion;
                $tipoMotivo=$temporal[0]->tipoMotivo;
                $elem = [
                    'quiebre' => $quiebre, 'origen' => $origen, 
                    'actuacion' => $tipoActuacion, 'motivo' => $tipoMotivo
                ];
                $actividadTipo = Helpers::asignarActividadTipoPorQuiebre($elem);
                        //validar cambio de quiebre y/o empresa
                if ($quiebre!='' && $empresa!='') {
                    if ($oldQuiebre!=$quiebre && $oldEmpresa!=$empresa) {
                        $sql = " eecc_final='$empresa', quiebre='$quiebre' ";
                        $mensaje="Se actualizo  Quiebre y Contrata en Temporales";
                    } elseif ($oldQuiebre!=$quiebre) {
                        $sql = "  quiebre='$quiebre' ";
                        $mensaje="Se actualizo solo quiebre, No se actualizo Contrata de ".$oldEmpresa." a ".$empresa;
                    } elseif ($oldEmpresa!=$empresa) {
                        $sql = " eecc_final='$empresa' ";
                        $mensaje="Se actualizo solo Contrata, No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre;
                    } else {//igual ambos
                        return "No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre.', ni Contrata '.$oldEmpresa." a ".$empresa;
                    } 
                } elseif ($quiebre!='') {
                    if ($oldQuiebre!=$quiebre) {//el quiebre anterior y el actual son diferentes
                        $sql = " quiebre='$quiebre' ";
                        $mensaje="Se actualizo Quiebre en Temporales";
                    } else {
                        return "No se actualizo Quiebre de ".$oldQuiebre." a ".$quiebre;
                    }
                } elseif ($empresa!='') {
                    if ($oldEmpresa!=$empresa) {//la contrata anterior es diferente a la actual
                        $sql = " eecc_final='$empresa' ";
                        $mensaje="Se actualizo Contrata en Temporales";
                    } else {
                        return "No se actualizo Contrata de ".$oldEmpresa." a ".$empresa;
                    }
                } else {
                    return "No se actualizo, no ha seleccionado quiebre ni contrata";
                }
                if (!is_null($actividadTipo)) {
                    $sql .= ", actividad_tipo_id='$actividadTipo' ";
                    $mensaje .=". ActividadTipo: $actividadTipo";
                }
                //2.1
                //2.2
                try {
                    GestionDetalle::updateGestionesTemporales($actividad, ltrim($sql, ','), $codigo);
                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                    if ($exc->errorInfo['0']=='42S02') {   //id, tabla not found
                        return 'No se actualizo, vuelva a intentar';
                    }
                    return "No se actualizo, hubo un error";
                }
                return $mensaje;
            } else {
                return "No se actualizo, orden no existe";
            }
        }
        return '';
    }
    
    /** 
     *     Actualizacion del Tipo de Actividad de las
     *     Provisiones y Averias en la Carga Individual,
     *     para asi poder ser enviadas a TOA 
     */
    public static function updateActividadTipoIdCarga($codActuacion, $fecRegistro, 
          $origen = "", $servicio = "", $quiebre ="", 
            $tipoActuacion = "", $tipoMotivo = "")
    {
        $query = "";
        $update = "";
        $set = "";
        $set_valor = 0;
        $where = "";
        $tabla = "";
        $campoupdate = "";
        switch ($servicio)
        {
            case 'provision':
                $update = "UPDATE webpsi_coc.tmp_provision ";
                if ($origen == "pen_prov_catv") {
                    if ($quiebre!="") {
                        if ($quiebre!="PENDMOV1") {
                            if ($tipoActuacion == "ALTA") {
                                $set_valor = 1;//actividad_tipo_id = 1
                            } else {
                                $tipoMotivo = substr($tipoMotivo, 0, 3);
                                switch ($tipoMotivo) {
                                    case 'AT|':
                                        $set_valor = 7;// SET actividad_tipo_id = 7
                                        break;
                                    case 'MT|':
                                        $set_valor = 8;//SET actividad_tipo_id = 8
                                        break;
                                    case 'VA|':
                                        $set_valor = 9;//SET actividad_tipo_id = 9
                                        
                                        break;
                                }
                            }
                        } else {
                            if ($tipoActuacion == "ALTA") {
                                $set_valor = 3;//SET actividad_tipo_id = 3
                                
                            }
                        }
                    }
                }
                //$where.=" WHERE codigo_req = ".$codActuacion;
                $campoupdate = "codigo_req";
                break;
            
            case 'averia':
                $update = "UPDATE webpsi_coc.averias_criticos_final ";
                if ($origen == "aver-catv-pais") {
                    if ($quiebre!="") {
                        if ($quiebre!="PENDMOV1") {
                            $set_valor = 4;//SET actividad_tipo_id = 4
                            
                        } else {
                            $set_valor = 6;//SET actividad_tipo_id=6
                        }
                    }
                }
                //$where.=" WHERE averia = ".$codActuacion;
                $campoupdate = "averia";
                break;
        }
        $query = $update.$set.$where;
        if ($tabla!="" && strlen($tabla) > 0) {
           try {
               //$sql =  DB::update($query, array($codActuacion));
               $sql = DB::table($tabla)
                  ->where($campoupdate, $codActuacion)
                  ->update(array('actividad_tipo_id' => $set_valor));
           } catch (\Exception $exc) {
            $_errorController = new \ErrorController();
               $_errorController->saveError($exc);
           }
        }
    }
    
    /**
     * Lee iconos de un directorio
     * @return type
     */
    private function doIconList()
    {
        $folder = "./img/icons/visorgps/";

        if ($od = opendir($folder)) {
            while (false !== ($file = readdir($od))) {
                if ($file != "." && $file != "..") {
                    $this->_iconList[] = $file;
                }
            }
            closedir($od);
        }

        return $this->_iconList;
    }

    /**
     * Genera un arreglo de iconos de tecnicos y agendas
     * @return string
     */
    public static function iconArray()
    {
        $lista = array();
        $iconArray = array();

        $folder = "./img/icons/visorgps/";

        if ($od = opendir($folder)) {
            while (false !== ($file = readdir($od))) {
                if ($file != "." && $file != "..") {
                    $lista[] = $file;
                }
            }
            closedir($od);
        }

        foreach ($lista as $val) {
            $part = explode("_", $val);
            $iconArray[substr($part[1], 0, 6)] = $part[1];
        }

        $n = 0;
        foreach ($iconArray as $key=>$val) {
            $iconArray[$n] = $iconArray[$key];
            unset($iconArray[$key]);
            $n++;
        }

        return $iconArray;
    }

    public static function htmlDetalleOfsc($resultado)
    {
      $html = "";
      foreach ($resultado as $key => $value) {
        switch ($key) {
          case "historial-envios-ofsc":
            if (count($value) > 0)
              $html = Helpers::htmlHistorialOfsc($value, $html);
            break;
          case "detalle-orden-ofsc":
            if ($value!=="")
              $html = Helpers::htmlOrdenOfsc($value, $html);
            break;
          default:
            # code...
            break;
        }
      }
      return $html;
    }
    public static function htmlHistorialOfsc($data = array(), $html = "")
    {
      $html.="<label>HISTORIAL DE AGENDAMIENTOS</label>";
      $html.="<table class='table table-bordered table-striped col-sm-12 responsive helper' width='100%''>";
      $html.="<thead class='btn-info'>
                          <tr>
                             <th><b>F. Envio</b></th>
                             <th><b>AID</b></th>
                             <th><b>T. Agenda</b></th>
                             <th><b>Empresa</b></th>
                             <th><b>Nodo</b></th>
                          </tr>
                       </thead>";
      $html.="<tbody>";
      foreach ($data as $key => $value) {
        if ($value->accion === "inbound_interface") {
           $html.="<tr>
                          <td>".$value->fecha_envio."</td>
                          <td>".$value->aid."</td>
                          <td>".$value->tipoAgenda."</td>
                          <td>".$value->empresa."</td>
                          <td>".$value->nodo."</td>
                      </tr>";
        }
       
      }
      $html.="</tbody>";
      $html.="</table>";
      return $html;
    }
    public static function htmlOrdenOfsc($response, $html = "")
    {
      
      $titulos_espanol = array(
        "activityId" => "Id de Actividad",
        "resourceId" => "Id de Recuros",
        "timeOfBooking" => "Fecha y Hora de Agenda",
        "XA_COMPANY_NAME" => "Empresa",
        "XA_CONTACT_NAME" => "Persona de Contacto",
        "customerName" => "Cliente",

        );
      //$html.="<label>ORDEN EN OFSC</label>";
      $html.="<table class='table table-bordered table-striped col-sm-12 responsive helper' width='100%''>";
      $html.="<thead >
                        <tr>
                        <th colspan='6'>ORDEN EN OFSC</th>
                        </tr>
                          <tr class='btn-success'>
                             <th><b>Id de Actividad</b></th>
                             <th><b>Id de Recursos</b></th>
                             <th><b>Fecha y Hora de Agenda</b></th>
                             <th><b>Empresa</b></th>
                             <th><b>Persona de Contacto </b></th>
                             <th><b>Cliente</b></th>
                          </tr>
                       </thead>";
       $html.="<tbody>";
       $html.="<tr>";
       foreach ($titulos_espanol as $key => $value) {
          if (isset($response->$key))
          $html.="<td>".$response->$key."</td>";
       }
       $html.="</tr>";
        
        $html.="</table>";
        return $html;
    }

    public static function htmlGestionesMovimientos($data = array())
    {
      $html = "";
      if (count($data) > 0 ){
        $html.="<table class='table table-bordered table-striped col-sm-12 responsive helper' width='100%''>";
        $html.="<thead >
                          <tr class='btn-success'>
                            <th><b>ID</b></th>
                             <th><b>F. Movimiento</b></th>
                             <th><b>F. Agenda</b></th>
                             <th><b>AID</b></th>
                             <th><b>Estado Ofsc</b></th>
                             <th><b>Cliente </b></th>
                          </tr>
                       </thead>";
       $html.="<tbody>";
       rsort($data);
       foreach ($data as $key => $value) {
         $html.="<tr>
                            <td>$value->id</td>
                            <td>$value->fecha_movimiento</td>
                            <td>$value->fh_agenda</td>
                            <td>$value->aid</td>
                            <td>$value->estado_ofsc</td>
                            <td>$value->nombre_cliente_critico</td>
                        </tr>";
       }
       $html.="</body>";
        $html.="</table>";
      }
      return $html;
    }
    
    /**
     * $data: quiebre, actuacion, origen, motivo (opcional)
     * 
     * @param array $data
     * @return integer Null si no existe coincidencias
     */
    public static function asignarActividadTipoPorQuiebre ( $data)
    {
        $actividad = null;
        if (!isset($data['quiebre'], $data['actuacion'], $data['origen'])) {
            return null;
        }     
        $motivo = isset($data['motivo'])? $data['motivo'] : '';
                
        if ($data['origen'] == 'pen_prov_catv') {
            if ($data['quiebre'] == 'PENDMOV1' 
                && $data['actuacion'] == 'ALTA') {
                $actividad = 3;
            } elseif ($data['quiebre'] == 'PENDMOV1' 
                && $data['actuacion'] == 'TRASLADO') {
                $actividad = 3;
            } elseif ($data['quiebre'] != 'PENDMOV1' 
                && $data['actuacion'] == 'ALTA') {
                $actividad = 1;
            } elseif ($data['quiebre'] != 'PENDMOV1' 
                && $data['actuacion'] != 'ALTA' && strlen($motivo)> 0) {
                $actividad = strpos($motivo, 'AT|') !== false ?  
                        7 : (strpos($motivo, 'MT|') !== false ? 
                        8 : (strpos($motivo, 'VA|') !== false ? 
                        9 : null));
            }
        } elseif($data['origen'] == 'aver-catv-pais') {
            if ($data['quiebre'] == 'PENDMOV1') {
                $actividad = 6;
            } else {
                $actividad = 4;
            }
        }
        return $actividad;
    }

    public static function getActualizarEstado($id,$gestionid)
    {
        $xml = (array) simplexml_load_file( public_path().'/xml/actividades.xml');
        $xml = json_decode(json_encode(  $xml ), 1);
        $datos=array();
        $nombreestado='';
        if (array_key_exists('actividad'.$id, $xml ) ) { // antes de usar revisar el xml 
            $datos['aid']=$id;
            $datos['gestion_id']=$gestionid;
            if ( isset( $xml['actividad'.$id]['status'] )) {
                $rst=EstadoOfsc::where('name', '=', trim($xml['actividad'.$id]['status']))->get();
                $datos['estado_ofsc_id']  = $rst[0]->id;
                $nombreestado             = $rst[0]->nombre;
              }
            if ( isset( $xml['actividad'.$id]['resource_id'] )) {
                $datos['resource_id'] = trim($xml['actividad'.$id]['resource_id']);
              }
           GestionMovimiento::OfscUpdate($datos);
           $datos['estado_ofsc_id']=$nombreestado; //solo para vista
           return $datos;
        } else {
          return 0;
        }
       
    }

    public static function xmlToArray($rutaXml="")
    {
      $datos= [];
        if ($rutaXml!==""){
          $xml = (array) simplexml_load_file($rutaXml);
          $xml = json_decode(json_encode($xml), true);
          foreach ($xml as $key => $value) {
            $datos[] = $value;
          }
        }
      return $datos;
    }
    
    /**
     * Validate if exists path filename and mimetype (jpg, jpeg, gif, png) <br>
     * and return value otherwise return message error
     * NOTE:  "'extension', 'mime', 'path'" no remove neither change position
     * @param type $dirname
     * @param type $filename
     * @return string
     */
    public static function validateImagen($dirname, $filename) 
    {
        if (is_null($filename) || empty($filename) || empty($dirname)) {
            return [
                'extension' => '', 'mime' => '', 'path' => '', 'aPath' => '', 
                'msj' => "(Image {$filename}) / (folder {$dirname}) NO exists.",
                'rst' => 2
            ];
        }
        $msj = '';
        $rst = 1;
        $mime = [
            'jpg' => 'image/jpeg'
            ,'jpeg'=> 'image/jpeg'
            ,'png' => 'image/png'
            ,'gif' => 'image/gif'
        ];
        $path = "img/{$dirname}/{$filename}";
        $pre = explode('.', $filename);
        $extension = end($pre);
        $aPath = strpos(public_path($path), '\\') === false?  
            public_path($path) : str_replace('\\', '/', public_path($path));

        if (!File::exists($aPath)) {
            $msj = "Image {$path} NO EXISTS."; // aPath
            $rst = 2;
        } elseif (!in_array($extension, array_keys($mime))) {
            $msj = "Mimetype {$extension} INVALIDO.";
            $rst = 2;
        }
        return [
            'extension' => $extension, 
            'mime' => $mime,
            'path' => $path,
            'aPath' => $aPath,
            'rst' => $rst,
            'msj' => $msj
        ];
    }

    /**
     * Validate if exists path filename and mimetype (jpg, jpeg, gif, png) <br>
     * and return value otherwise return message error
     * NOTE:  "'extension', 'mime', 'path'" no remove neither change position
     * @param type $dirname
     * @param type $filename
     * @return array
     */

    public static function xmlJsontoExcel($dataFuente = [], $plantilla_response = []) 
    {
        $data = [];
       /* foreach ($dataFuente as $key => $value) {
            $plantillaTmp = $plantilla_response;
            foreach ($value as $key2 => $value2) {
                if (isset($plantillaTmp[$key2])) {
                    $plantillaTmp[$key2] = preg_replace('/,/', '', $value2);
                }
            }
            $data[$key] = $plantillaTmp;
        } */
      //print_r($data);
      //dd();
        if (count($plantilla_response) > 0 && is_array($dataFuente)) {
            foreach ($dataFuente as $key => $value) {
                $plantilla_response_tmp = $plantilla_response;
                foreach ($value as $key2 => $value2) {
                    if (isset($plantilla_response_tmp[$key2])) {
                        if (is_array($value2)) {
                            if (count($value2) > 0) {
                                 $plantilla_response_tmp[$key2] = preg_replace('/,/', '', $value2[0]);
                            } else{
                                $plantilla_response_tmp[$key2]  = "";
                            }
                        } else {
                            $plantilla_response_tmp[$key2] = preg_replace('/,/', '',$value2);
                        }
                    } else {
                        $plantilla_response_tmp[$key2] = "";
                    }
                }
             $data[$key] = $plantilla_response_tmp;
        }
      } else {
        $data = $dataFuente;
      }
         if (count($data) > 0)
            return Helpers::exportArrayToExcel($data, "ActividadesOfsc");
          else 
            return json_encode(["rst" => 1, "msj" => "La data está vacía"]);
    }
    public static function saveError($exc = [])
    {
       $error = new Error;
       if (is_array($exc) && isset($exc["errorInfo"]) && count($exc)==3) {
            list($code,$line,$message) = array_shift($exc);
            $error["code"] = $code;
            $error["file"] = 'mysql';
            $error["line"] = $line;
            $error["message"] = $message;
            $error["trace"] = '';
        } else if (is_array($exc)) { 
            $error["code"] = isset($exc["code"]) ? $exc["code"] : '';
            $error["file"] = isset($exc["file"]) ? $exc["file"] : '';
            $error["line"] = isset($exc["line"]) ? $exc["line"] : '';
            $error["message"] = isset($exc["message"]) ? $exc["message"] : '';
            $error["trace"] = isset($exc["trace"]) ? $exc["trace"] : '';
        } elseif(is_object($exc) ){
            $error["code"] = $exc->getCode();
            $error["file"] = $exc->getFile();
            $error["line"] = $exc->getLine();
            $error["message"] = $exc->getMessage();
            $error["trace"] = $exc->getTraceAsString();
        } else {
            $error["code"] = 'code';
            $error["file"] = 'file';
            $error["line"] = 'line';
            $error["message"] = 'message';
            $error["trace"] = 'trace';
        }

        $error["usuario_id"] = Auth::id() ? Auth::id() : '697';
        $error["date"] = date("Y-m-d H:i:s");
        $error["url"] = Request::url();
        $error->push();
    }

    /**
     * Exportar un array a Excel
     * @param type $array string
     * @param type $text string
     * @return type csv
     */
    public static function exportArrayToExcelHTML($reporte, $fileName)
    {
            $filename = Helpers::convert_to_file_excel($fileName);

            header('Content-Type: text/html; charset=UTF-8');
            header("Content-type:application/vnd.ms-excel;charset=latin");
            header("Content-Disposition:atachment;filename=".$filename);
            header("Pragma:no-cache");
            header("Expires:0");

            $cab = 1;
            echo '<table border="0">';
            foreach ($reporte as $data) {
                if ($cab == 1) {
                    echo '<tr>';
                    foreach ($data as $key=>$val) {
                        echo "<td align='center' style='width:auto;'>"
                        . "<b><font size='2'>"
                        . strtoupper($key)
                        ."</font></b>"
                        ."</td>";
                    }
                    echo '</tr>';
                    $cab = 2;
                }
                echo '<tr>';
                foreach ($data as $val) {
                    echo "<td style='width:auto;'>$val</td>";
                }
                echo '</tr>';
            }
            echo '</table>';
    }

    public static function exportExcel($propiedades,$x,$cabecera,$data,$type = 1,$path=''){ //falta una modificaciones
       /*style*/
        $styleThinBlackBorderAllborders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
            'font'    => array(
                'bold'      => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        $styleAlignmentBold= array(
            'font'    => array(
                'bold'      => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        $styleAlignment= array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
        );
        /*end style*/
      $head=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');
      /*instanciar phpExcel*/            
      $objPHPExcel = new PHPExcel();
      /*end instanciar phpExcel*/
      /*configure*/
      $objPHPExcel->getProperties()->setCreator($propiedades['creador'])
                                  ->setSubject($propiedades['subject']);
      $objPHPExcel->getDefaultStyle()->getFont()->setName($propiedades['font-name']);
      $objPHPExcel->getDefaultStyle()->getFont()->setSize($propiedades['font-size']);
      $objPHPExcel->getActiveSheet()->setTitle($propiedades['tittle']);
      /*end configure*/
      /*set up structure*/
      array_unshift($data,(object) $cabecera);
      foreach($data as $key => $value){
        $cont = 0;
        if($key == 0){ // set style to header
          end($value);       
          $objPHPExcel->getActiveSheet()->getStyle('A1:'.$head[key($value)].'1')->applyFromArray($styleThinBlackBorderAllborders);
        }
        foreach($value as $index => $val){
          $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($head[$cont])->setAutoSize(true);
            
          if($index == 'norden' && $key > 0){ //set orden in excel
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($head[$cont].($key + 1), $key);                
          }else{ //poblate info
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($head[$cont].($key + 1), $val);
          }
          $cont++;
        }          
      }
      /*end set up structure*/
      $objPHPExcel->setActiveSheetIndex(0);
      // Redirect output to a client’s web browser (Excel5)
      /*header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="reporte.xls"'); // file name of excel
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0*/
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      if($type == 1){
        $objWriter->save($path);        
      }else{
        $objWriter->save('php://output');
        exit;
      }
    }


    public static function createExcel($data = array(), $path = '', $type = "Excel5", $header = ''){
      $head=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ');

      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setCreator('PSI')->setSubject('Reporte');
      $objPHPExcel->getDefaultStyle()->getFont()->setName('Bookman Old Style');
      $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
      $objPHPExcel->getActiveSheet()->setTitle('Reporte');

      $cabecera = ($header) ? $header: ((count($data)>0) ? array_keys((array)$data[0]): array('No se encontraron datos'));
      if(isset($data[1]) && (count($cabecera) < count($data[1]))) array_push($cabecera,'Detalle');   
      array_unshift($data, $cabecera);
      foreach($data as $key => $value){
        $cont = 0;
        foreach($value as $index => $val){
          $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($head[$cont])->setAutoSize(true);
          $objPHPExcel->setActiveSheetIndex(0)->setCellValue($head[$cont].($key + 1), $val);
          $cont++;
        }          
      }                               
      /*end set up structure*/
      $objPHPExcel->setActiveSheetIndex(0);
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $type);
      $objWriter->save($path);
    }

    public static function getDefaultImage ($desc = 'Imagen por defecto.')
    {
      $fichero = 'img/no_image_256x256.png';
      header('Content-Description: ' . $desc);
      header('Content-Type: image/png');
      header('Expires: 0');
      header('Pragma: public');
      ob_end_clean(); 
      readfile($fichero);
      exit;
    }

    public static function exportArrayToCsv($reporte, $fileName)
    {
        $fecha = date("d/m/Y");
        $hora = date("h:i:s");
        $hh = substr($hora, 0, 2);
        $mm = substr($hora, 3, 2);
        $ss = substr($hora, 6, 2);
        $hora = $hh."_".$mm."_".$ss;
        $filename = $fileName.'_'.$fecha."-".$hora.".csv";

        header("Expires: 0");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($reporte)));
        foreach ($reporte as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public static function formatoFecha($date)
    {
        // yyyy-mm-dd to dd/mm/yyyy
        if ($date == "0000-00-00") {
            return null;
        } elseif (is_null($date)) {
            return null;
        }
        $fecha = explode("-", $date);
        $fecha = $fecha[2]."/".$fecha[1]."/".$fecha[0];
        return $fecha;
    }
    /**
     * $date 2017-04-21
     */
    public static function dailyExtract($date){
        $url='https://api.etadirect.com/rest/ofscCore/v1/folders/dailyExtract/folders/'.$date.'/files/telefonica-pe.csv.'.$date.'.zip';

        header('Content-Type: application/octet-stream');
        $curl = curl_init($url);

        //curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl, CURLOPT_PROXY, $host);
        //curl_setopt($curl, CURLOPT_PROXYPORT, $port);
        $head = [
            "Content-type: application/octet-stream",
            "Content-Disposition: attachment; filename=file",
            "Content-Transfer-Encoding: binary"
        ];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $head);
        //curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300); //set timeout to 5 mins

        ob_start();
        $file='telefonica-pe.csv.'.$date.'.zip';
        $fp = fopen($file, 'w+');
        if($fp === false){
            throw new Exception('Could not open: ' . $saveTo);
        }
        curl_setopt($curl, CURLOPT_FILE, $fp);
        $out = fopen('php://output', 'w');

        curl_setopt($curl, CURLOPT_USERPWD, \AuthBasic::getAuthString());

        curl_setopt($curl, CURLOPT_VERBOSE, true);  
        curl_setopt($curl, CURLOPT_STDERR, $out);
        //curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $error = curl_error($curl); 
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);   //get status code
        curl_close($curl);

        print_r($result);
        print_r($error);
        var_dump($status_code);
    }


  public static function ChangeNomenclature($value){
        if($result = strpos($value, '-')) {
            $searchable = explode(substr($value, $result,1),$value);
            $cadena = '';
            foreach ($searchable as $index => $val) {
                $cadena.=($index == 0) ? trim(strtoupper($val)) : '-'.trim(strtoupper($val));
            }
            return $cadena;
        }
        return false;
    }

    public static function findValueIntoArray($array,$search,$byfield = 'nombre'){
        if($array && $search){
            foreach ($array as $key => $value) {
                if(trim(strtoupper($value->$byfield)) == trim(strtoupper($search))){
                    return true;
                }
                if(strpos($value->$byfield, '-') && strpos($search, '-')){
                  if(Helpers::ChangeNomenclature($value->$byfield) == Helpers::ChangeNomenclature($search)){
                      return true;
                  }
                }
            }
            return false;
        }
    }

    public static function generateRandomString($length = 20) { 
      return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
    }

    public static function swapString($oldString) {
        $character = [
            "á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò",
            "Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚",
            "ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹","#"
        ];
        $rules = [
            "a","e","i","o","u","A","E","I","O","U","n","N","A","E","I",
            "O","U","a","e","i","o","u","c","C","a","e","i","o","u","A",
            "E","I","O","U","u","o","O","i","a","e","U","I","A","E",""
        ];

        $newString = str_replace($character, $rules ,$oldString);
        return $newString;
    }
}
