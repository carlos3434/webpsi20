<?php

/**
* 
*/
class FileHelper
{
	
	public function __construct($argument = null)
	{
		# code...
	}

	public static function validaCrearFolder ($urlFolder = "" , 
		$createFolder = true, $permisos = 0777) {
		$urlPublica = public_path();
		if ($urlFolder!=="") {
			$urlFolder = str_replace($urlPublica, "", $urlFolder);
			
			$folders = explode("/", $urlFolder);
			$response = false;
			$ruta = $urlPublica;
			if (count($folders) > 0) {
				foreach ($folders as $key => $value) {
					if ($value!=="") {
						$ruta.="/".$value;
						if (!File::exists($ruta)){
							if ($createFolder === true){
								//echo "cree folder";
								
							 File::makeDirectory($ruta, $permisos = 0777, true, true);
							 $response = true;
							}
						} else {
							$response = true;
						}
					}
				}
			} else $response;

			return $response;
		}
	}
}