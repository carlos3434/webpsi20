<?php
class FuncionesMatematicas
{
	public static function mediaAritmetica ($datos = [])
	{
		$media = 0;
		foreach ($datos as $key => $value) {
			$media+=$value;
		}
		if (count($datos) > 0)
			$media = $media/count($datos);

		return $media;
	}

	public static function geoDistanciaKM ($puntoInicio = [], $puntoFin = [])
	{
		
		$lat1= $puntoInicio["latitud"];
		$lat2 = $puntoFin["latitud"];
		$long1 = $puntoInicio["longitud"];
		$long2 = $puntoFin["longitud"];
		$km = 111.302;
		$degtorad = 0.01745329;//1 Grado = 0.01745329 Radianes
		$radtodeg = 57.29577951;//1 Radian = 57.29577951 Grados
		$dlong = ($long1 - $long2); 
		$dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
		$dd = acos($dvalue) * $radtodeg; 
		if (is_nan($dd))
			return 100000000000000000;
		else
		 	return abs($dd * $km);
	}

    public static function randomHexadecimal($inicio = 0, $fin = 0)
    {
        $hexadecimal = "#000000";
        $number = rand($inicio, $fin);
        $hexadecimal = "#".dechex($number);
        return $hexadecimal;
    }

    public static function validarRangoXY($coordx, $coordy)
    {
        if ($coordx > 180 || $coordx < -180) {
            return false;
        }

        if ($coordy > 90 || $coordy < -90) {
            return false;
        }

        return true;
    }

    public static function validarRangoXYLima($coordx, $coordy)
    {
        if ($coordy > -10.20 || $coordy < -13.30) {
            return false;
        }

        if ($coordx > -75.70 || $coordx < -77.90) {
            return false;
        }
        return true;
    }

    public static function validarRangoXYPeru($coordx, $coordy)
    {
        if ($coordx > -68.6 || $coordx < -81.15) {
            return false;
        }

        if ($coordy > -0.03 || $coordy < -18.30) {
            return false;
        }

        return true;
    }
}
