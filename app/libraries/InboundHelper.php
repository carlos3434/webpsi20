<?php 
use Ofsc\Inbound;
class InboundHelper
{
    /**
     * 
     * @param {String}  $parametros:
     *        {String}  tipoEnvio            tipo de envio: agendo, sla
     *        {String}  phone                telefono
     *        {String}  cell                 celular
     *        {String}  telefonoContacto2    telefono de contacto 2
     *        {String}  telefonoContacto3    telefono de contacto 3
     *        {String}  telefonoContacto4    telefono de contacto 4
     *        {String}  nenvio               numero de envio
     *        {String}  obsToa               observacion de la actividad
     *        {String}  fechaAgenda          fecha de cita de la actividad
     *        {String}  timeSlot             franja horaria: AM, PM
     *        {String}  external_id          bucket de la actividad
     * @return {Boolean} $sla                indica si es sla o agneda
     * @return {Boolean} $estadoComponente   indica el  envio de componente
     * @return {Array}$telefonosContacto     telefonos de contacto
     * @return {Array}$response              respuesta de envio a ofsc
     */
    public static function envioOrdenOfsc($parametros=[],$actuObj, &$sla,&$estadoComponente, &$telefonosContacto) 
    {
        $actuArray = (array) $actuObj;
        //telefono
        $phone = $actuArray['telefono_cliente_critico'];
        $cell = $actuArray['celular_cliente_critico'];
        //fonos de contacto
        $telefonos = str_replace(" ", "", $actuArray['fonos_contacto']);
        $telefonos=explode("|", $telefonos);
        $telefonosContacto[0] = $telefonosContacto[1] = $telefonosContacto[2] = null;
        for ($i=0; $i < count($telefonos); $i++) { 
            $telefonosContacto[$i]=$telefonos[$i];
        }
        //si se recive telefono, se reemplaza con los nuevos valores
        if (isset($parametros['phone']) && $parametros['phone']!='') {
            $phone=$parametros['phone'];
        }
        if (isset($parametros['cell']) && $parametros['cell']!='') {
            $cell=$parametros['cell'];
        }
        if (isset($parametros['telefonoContacto2']) && $parametros['telefonoContacto2']!='') {
            $telefonosContacto[0]=$parametros['telefonoContacto2'];
        }
        if (isset($parametros['telefonoContacto3']) && $parametros['telefonoContacto3']!='') {
            $telefonosContacto[1]=$parametros['telefonoContacto3'];
        }
        if (isset($parametros['telefonoContacto4']) && $parametros['telefonoContacto4']!='') {
            $telefonosContacto[2]=$parametros['telefonoContacto4'];
        }

        $codactu=$actuObj->codactu;
        $actuArray = (array) $actuObj;
        $date = new DateTime(date("c"));
        $hoy = $date->format('Y-m-d H:i:s');
        $actividadTipo=ActividadTipo::find($actuArray['actividad_tipo_id']);
        if (!isset($actividadTipo->sla)) {
            return [];
        }
        $gestionId = $actuObj->id;
        //$sla=false;
        $slaWindowStart = $slaWindowEnd = Null;
        
        $xaAppointmentScheduler = "CLI";
        //$fechaCita= $fechaAgenda = $parametros['fechaAgenda'];
        //$fechaCita = $parametros['fechaAgenda'];
        $timeSlot = $parametros['timeSlot'];
        if ($parametros['tipoEnvio']=='sla') {
            $slaWindowStart = $hoy;
            $date->add(new DateInterval('PT'.($actividadTipo->sla*60).'M'));
            $slaWindowEnd = $date->format('Y-m-d H:i:s');
            $sla = true;
            //$fechaAgenda=$hoy;
            $parametros['fechaAgenda']=''; //para mas adelante, y comentar la fechaAgenda
            $timeSlot =Null;
            //TEL: cita ficticia
            $xaAppointmentScheduler = "TEL";
        }

        //HEAD
        $head['external_id'] = $parametros['external_id'];
        $head['date'] = $parametros['fechaAgenda']; //cuando es no programado es vacio
        //propiedades de la cita
        $cita['date'] = $parametros['fechaAgenda'];
        $cita['appt_number'] = $actuArray['codactu'];
        $cita['customer_number'] = $actuArray['inscripcion'];
        $cita['worktype_label'] = $actividadTipo->label;//provisio averia
        $cita['sla_window_start'] = $slaWindowStart;
        $cita['sla_window_end'] = $slaWindowEnd;
        $cita['time_slot'] = $timeSlot;
        $cita['time_of_booking'] = $actuArray['fecha_registro']; //fecha que se genero la solicitud
        $cita['duration'] = $actividadTipo->duracion;
        $cita['name'] = $actuArray['nombre_cliente'];
        $cita['phone'] = $phone;
        $cita['cell'] = $cell;
        $cita['email'] = null;
        $cita['address'] = $actuArray['direccion_instalacion'];

        //inicio funcion
        $codigodepartamento = substr($actuArray['codigo_distrito'], 0, 3);
        $codigoprovincia = substr($actuArray['codigo_distrito'], 3, 3);
        $state = $city = null;
        $departamento = UbigeoCms::getUbigeo($codigodepartamento);

        if ( count($departamento)>0 && isset($departamento[0]))
            $state = $departamento[0]->nombre;

        $ciudad = UbigeoCms::getUbigeo($codigoprovincia, $codigodepartamento);

        if ( count($ciudad)>0 && isset($ciudad[0]))
            $city = $ciudad[0]->nombre;

        //fin funcion
        $cita['city'] = $city;//ubigeo cms
        $cita['state'] = $state;//ubigeo cms
        $cita['zip'] = 'LIMA 07';
        $cita['language'] = '1';
        $cita['reminder_time'] = 15;//tiempo recordatorio sms
        $cita['time_zone'] = 19;
        $cita['coordx'] = $actuArray['coord_x'];
        $cita['coordy'] = $actuArray['coord_y'];

        //propiedades custom ofsc
        $propiedades['XA_CREATION_DATE'] = $hoy;
        $propiedades['XA_SOURCE_SYSTEM'] = 'PSI';

        //inicio funcion
        $businessType = null;
        $preDiagnosis='';
        if (strpos($actuArray['tipo_averia'], "catv") > 0) {
            $businessType = "CATV";
            $preDiagnosis='TV_';
        }
        if (strpos($actuArray['tipo_averia'], "adsl") > 0) {
            $businessType = "ADSL";
            $preDiagnosis='SP_';
        }
        if (strpos($actuArray['tipo_averia'], "bas") > 0) {
            $businessType = "BASICA";
            $preDiagnosis='SB_';
        }

        $segmento = null;
        $segmentoNoValidos = ['','null',null,'0',''];
        if (!in_array($actuArray['segmento'], $segmentoNoValidos)) {
            if (strtoupper($actuArray['segmento']) == "NO-VIP") {
                $segmento = "N";
            } elseif ( strtoupper($actuArray['segmento'])=='VIP') {
                if ( $businessType == "CATV") {
                    $segmento = "S";
                }
                $segmento = "D";
            }
            $segmento=$actuArray['segmento'];
        }
        //fin funcion

        $propiedades['XA_CUSTOMER_SEGMENT'] = null;//$segmento; //revisar cuando es NO-VIP
        $propiedades['XA_CUSTOMER_TYPE'] = null;// no hay en fase 0
        $propiedades['XA_CONTACT_NAME'] = $actuArray['nombre_cliente'];
        $propiedades['XA_CONTACT_PHONE_NUMBER_2'] = $telefonosContacto[0];
        $propiedades['XA_CONTACT_PHONE_NUMBER_3'] = $telefonosContacto[1];
        $propiedades['XA_CONTACT_PHONE_NUMBER_4'] = $telefonosContacto[2];
        $propiedades['XA_CITY_CODE'] = 'Lima-01';
        $propiedades['XA_DISTRICT_CODE'] = $actuArray['codigo_distrito'];
        $propiedades['XA_DISTRICT_NAME'] = $actuArray['distrito'];
        $propiedades['XA_ZONE'] = $actuArray['zonal'];
        $propiedades['XA_QUADRANT'] = GeoValidacion::getquadrant($actuArray['coord_x'], $actuArray['coord_y']);
        $propiedades['XA_ADDRESS_LINK_HTTP'] = Config::get("wpsi.geo.public.mapofsc") . $actuArray['codactu'];
        list($primera, $segunda) = explode('|', $actuArray['fftt']);
        $propiedades['XA_WORK_ZONE_KEY'] = $primera.'_'.$segunda;
        $propiedades['XA_RURAL'] = 0;
        $propiedades['XA_RED_ZONE'] = 0;
        $propiedades['XA_WORK_TYPE'] = $actividadTipo->nombre;//subtipo de actividad
        $propiedades['XA_APPOINTMENT_SCHEDULER'] = $xaAppointmentScheduler;
        $propiedades['XA_USER'] = Session::get('full_name');
        $propiedades['XA_REQUIREMENT_NUMBER'] = $actuArray['codactu'];
        $propiedades['XA_NUMBER_SERVICE_ORDER'] = $actuArray['orden_trabajo'];
        $propiedades['XA_CHANNEL_ORIGIN'] = 'call';//canal de origen
        $propiedades['XA_SALES_POINT_CODE'] = "NULL";//codigo punto de venta
        $propiedades['XA_SALES_POINT_DESCRIPTION'] = "NULL";//descripcion del punto de venta
        $propiedades['XA_COMMERCIAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        //inicio funcion
        $content = null;
        if (isset($actuArray['wu_nagendas']) || 
            isset($actuArray['wu_nmovimientos']) ||
            isset($actuArray['wu_fecha_ult_agenda']) ) {
            $content = '<XA_WEB_UNIFICADA>';

            if (isset($actuArray['wu_nagendas'])) {
                $wuNagendas = $actuArray['wu_nagendas'];
                $content .= "<NumeroAgendas>$wuNagendas</NumeroAgendas>";
            }
            if (isset($actuArray['wu_nmovimientos'])) {
                $wuNmovimientos = $actuArray['wu_nmovimientos'];
                $content .=
                    "<NumeroMovimientos>$wuNmovimientos</NumeroMovimientos>";
            }
            if (isset($actuArray['wu_fecha_ult_agenda'])) {
                $wuFechaUltAgenda = $actuArray['wu_fecha_ult_agenda'];
                $content .= 
                    "<FechaUltimaAgenda>$wuFechaUltAgenda</FechaUltimaAgenda>";
            }
            $content .= '</XA_WEB_UNIFICADA>';
        }
        //fin funcion
        $propiedades['XA_WEB_UNIFICADA'] = $content;
        $propiedades['XA_ORDER_AREA'] = $actuArray['area'];//area responsable de la orden
        $propiedades['XA_COMMERCIAL_PACKET'] = $actuArray['paquete'];
        $propiedades['XA_COMPANY_NAME'] = $actuArray['empresa'];
        
        $quiebreGrupo = QuiebreGrupo::find($actuArray['quiebre_grupo_id']);

        $propiedades['XA_GRUPO_QUIEBRE'] = isset($quiebreGrupo->nombre) ? $quiebreGrupo->nombre : null;
        $propiedades['XA_QUIEBRES'] = $actuArray['quiebre'];
        $propiedades['XA_BUSINESS_TYPE'] = $businessType;
        $propiedades['XA_PRODUCTS_SERVICES'] = null;
        $propiedades['XA_CURRENT_PRODUCTS_SERVICES'] = null;

        //inicio funcion
        $dataComp = [];
        $equipment = null;
        $compGestion = new ComponenteGestion();
        if (trim($gestionId) != "") {
            $dataComp = $compGestion->getComponenteId($gestionId);
        } else {
            $dataComp = $compGestion->getComponenteFaltanteGrupal($actuArray['codactu']);
        }
        if (isset($dataComp["data"]) and count($dataComp["data"]) > 0) {
            $equipment = $compGestion->getArrayformat($dataComp["data"]);
            $estadoComponente = 1;
        }
        //fin funcion
        $propiedades['XA_EQUIPMENT'] = $equipment;
        $obsToa='';
        if (isset($parametros['obsToa'])) {
            $obsToa = $parametros['obsToa']." | ";
        }
        $propiedades['XA_NOTE'] = $obsToa . $actuArray['observacion'];

        //inicio funcion
        $ffttController = new \FfttController();
        $val = new stdClass();
        $val->fftt = $actuArray['fftt'];
        $val->tipoactu = $actuArray['tipo_averia'];
        $ffttArray = $ffttController->getExplodefftt($val);
        if ($ffttArray["tipo"]=='catv') {
            //$propiedades['XA_HFC_ZONE'] = $actuArray['zona_movistar_uno'];//si:1, no:0
            $propiedades['XA_HFC_TROBA'] = $ffttArray["troba"];
            $propiedades['XA_HFC_NODE'] = $ffttArray["nodo"];
            $propiedades['XA_HFC_AMPLIFIER'] = $ffttArray["amplificador"];
            $propiedades['XA_HFC_TAP'] = $ffttArray["tap"];
            $propiedades['XA_HFC_BORNE'] = $ffttArray["borne"];
            $propiedades['XA_HFC_TAP_LINKHTTP'] = Config::get("wpsi.geo.public.maptap") . $actuArray['codactu'];
        } else {
            $propiedades['XA_MDF'] = $ffttArray["mdf"];
            //validar si es armario o cable
            $propiedades['XA_CABLE'] = null;//cable
            $propiedades['XA_CABINET'] = $ffttArray["armario"];//armario
            $propiedades['XA_BOX'] = null;//caja terminal
            $propiedades['XA_TERMINAL_LINKHTTP'] = Config::get("wpsi.geo.public.mapterminal") . $actuArray['codactu'];
        }
        //fin funcion

        //inicio funcion
        $diagnosis = explode('|', $actuArray['codmotivo_req_catv']);
        if (isset($diagnosis[0])) {
            //Tipo de Requerimiento
            $propiedades["XA_REQUIREMENT_TYPE"] = $diagnosis[0];
        }
        if (isset($diagnosis[1])) {
            //Motivo de Requerimiento
            $propiedades["XA_REQUIREMENT_REASON"] = $diagnosis[1];
        }

        $propiedades['XA_TELEPHONE_TECHNOLOGY'] = "NULL";
        $propiedades['XA_BROADBAND_TECHNOLOGY'] = "NULL";
        $propiedades['XA_TV_TECHNOLOGY'] = "NULL";
        $propiedades['XA_ACCESS_TECHNOLOGY'] = 'COAXIAL';
        $propiedades['XA_CATV_SERVICE_CLASS'] = $actuArray['clase_servicio_catv'];//Clase de Servicio CATV
        $propiedades['XA_TERMINAL_ADDRESS'] = $actuArray['dir_terminal'];//direccion de terminal
        $propiedades['XA_ADSLSTB_PREFFIX'] = $actuArray["tipo_actuacion"];
        $propiedades['XA_ADSLSTB_MOVEMENT'] = 'NULL';//acciones relizadas por el tecnico
        $propiedades['XA_ADSL_SPEED'] = $actuArray['veloc_adsl'];//
        $propiedades['XA_ADSLSTB_SERVICE_TYPE'] = $actuArray['tipo_servicio'];//tipo de servicio
        $propiedades['XA_PENDING_EXTERNAL_ACTION'] = 'RESCHEDULE';//solo pendiente de reagendamiento
        $propiedades['XA_SMS_1'] = $actuArray['sms1'];
        $propiedades['XA_SMS_2'] = null;//$actuArray['sms2']; //Property is not visible:

        //Campos adicionales para averia
        $xaDiagnosis=null;
        if (strtolower($actuObj->actividad) == 'averia') {
            if (isset($diagnosis[1])) {
                $xaDiagnosis = $preDiagnosis.$diagnosis[1];
            }
        }
        $propiedades['XA_DIAGNOSIS'] = $xaDiagnosis; //Property is not visible: validar cundo es TV_R303 TV_I115  TV_I129 TV_I128 TV_I115
        $propiedades['XA_TOTAL_REPAIRS'] = $actuArray['total_averias'];
        
        $propiedades['XA_NUMERO_ENVIO'] = $parametros['nenvio'];
        $propiedades['XA_TERMINATION_TYPE'] = null;//1; // Property is not visible
        $propiedades['XA_TIPO_XY'] = 1;
        $propiedades['XA_SYMPTOM'] = null;//1; //Property is not visible:
        if (isset($actuArray['peticion']) && $actuArray['peticion']!='') {
            $propiedades['XA_PETICION'] =$actuArray['peticion'];
        }
        $propiedades['XA_SYMPTOM'] = null;
        $inbound = new Inbound();
        $response = $inbound->createActivity($head, $cita, $propiedades, [], $sla);
        
        return $response;
    }

}
