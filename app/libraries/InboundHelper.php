<?php 
class InboundHelper
{
    public static function getEquimentXML($array,$cantidad)
    {
            $equipment = "";
            $equipment .= "<XA_EQUIPMENT>";
            foreach ($array as $value) {
                $equipment .= "<Equipo>";
                $equipment .= "<IdEquipo>".$value->idEquipo."</IdEquipo>";
                $equipment .= "<TipoEquipo>".$value->tipoEquipo."</TipoEquipo>";
                $equipment .= "<Cantidad>".$cantidad."</Cantidad>";
                $equipment .= "</Equipo>";
            }
            $equipment .= "</XA_EQUIPMENT>";

            return $equipment;
    }

     public static function getFFTTXML(
        $ffttArray = array(), $actuArray = array(), 
        $dataOfsc = array(), $codactu = ""
    ) 
     {
        //link address - fftt
        $dataOfsc["XA_ADDRESS_LINK_HTTP"] = 
            \Config::get("wpsi.geo.public.mapofsc") . $codactu;
        if ($ffttArray["tipo"]=='catv') {
            if ($actuArray['averia_m1']=='MOVISTAR UNO')
                $dataOfsc['XA_HFC_ZONE'] ="1";
            $dataOfsc['XA_HFC_TROBA'] = $ffttArray["troba"];
            $dataOfsc['XA_HFC_NODE'] = $ffttArray["nodo"];
            $dataOfsc['XA_HFC_AMPLIFIER'] = $ffttArray["amplificador"];
            $dataOfsc['XA_HFC_TAP'] = $ffttArray["tap"];
            $dataOfsc['XA_HFC_BORNE'] = $ffttArray["borne"];
            $dataOfsc['XA_HFC_TAP_LINKHTTP'] = 
                \Config::get("wpsi.geo.public.maptap") . $codactu;
        } else {
            $dataOfsc['XA_MDF'] = $ffttArray["mdf"];
                //validar si es armario o cable
            $dataOfsc['XA_CABLE'] = '';
            $dataOfsc['XA_CABINET'] = $ffttArray["armario"];//armario
            $dataOfsc['XA_BOX'] = '';//caja terminal
            $dataOfsc['XA_TERMINAL_LINKHTTP'] = 
                \Config::get("wpsi.geo.public.mapterminal") . $codactu;
        }
        return $dataOfsc;
     }

    public static function getElementXaWebUnificada(
        $wuNagendas, $wuNmovimientos, $wuFechaUltAgenda 
    ) 
    {
        $content = '<XA_WEB_UNIFICADA>';
        if (isset($wuNagendas)) {
            $content .= "<NumeroAgendas>$wuNagendas</NumeroAgendas>";
        }
        if (isset($wuNmovimientos)) {
            $content .=
                "<NumeroMovimientos>$wuNmovimientos</NumeroMovimientos>";
        }
        if (isset($wuFechaUltAgenda)) {
            $content .= 
                "<FechaUltimaAgenda>$wuFechaUltAgenda</FechaUltimaAgenda>";
        }
        $content .= '</XA_WEB_UNIFICADA>';
        return $content;
    }
    
    public static function getElementXaTotalRepairs(
        $totalAveriasCable, $totalAveriasCobre, $totalAverias)
    {
            $content = '<XA_TOTAL_REPAIRS>';
            if (isset($totalAveriasCable)) {
                $content .= "<ReiteradasCable>$totalAveriasCable</ReiteradasCable>";
            }
            if (isset($totalAveriasCobre)) {
                $content .= "<ReiteradasCobre>$totalAveriasCobre</ReiteradasCobre>";
            }
            if (isset($totalAverias)) {
                $content .= "<ReiteradasAverias>$totalAverias</ReiteradasAverias>";
            }
            $content .= '</XA_TOTAL_REPAIRS>';
            return $content;
    }

    public static function getXaDiagnosisXML($actuArray = array(), $actuObj = object, $preDiagnosis = "")
    {
    	 $xaDiagnosis = "";
    	 //Campos adicionales para averia
                if (strtolower($actuObj->actividad) == 'averia') {
                    $diagnosis = explode('|', $actuArray['codmotivo_req_catv']);
                    if (isset($diagnosis[1])) {
                        $xaDiagnosis = $preDiagnosis.$diagnosis[1];
                    } else {
                         $xaDiagnosis = '';
                    }
                }
                return $xaDiagnosis;
    }

    public static function getUbigeoXML ($dataOfsc = array(), $actuArray = array())
    {
            $codigodepartamento = substr($actuArray['codigo_distrito'], 0, 3);
            $codigoprovincia = substr($actuArray['codigo_distrito'], 3, 3);
            $state = UbigeoCms::getUbigeo($codigodepartamento);

            if (isset($state[0]))
                $state = $state[0]->nombre;
            else
                $state = "";
            $city = UbigeoCms::getUbigeo($codigoprovincia, $codigodepartamento);
            if (isset($city[0]))
                $city = $city[0]->nombre;
            else
                $city = "";

            $dataOfsc['city'] = $city;
            $dataOfsc['state'] = $state;

            return $dataOfsc;
    }

    public static function gettXaRequirimentTypeReason($actuArray= array(), $dataOfsc = array())
    {
           $diagnosis = explode('|', $actuArray['codmotivo_req_catv']);
            if (isset($diagnosis[0])) {
                $dataOfsc["XA_REQUIREMENT_TYPE"] = $diagnosis[0];
            }
            if (isset($diagnosis[1])) {
                $dataOfsc["XA_REQUIREMENT_REASON"] = $diagnosis[1];
            }
            return $dataOfsc;
    }


       public static function getCliente(
            $bucket='',$actividad='',$date='',$type='',$external_id='',
            $start_time='',$end_time='',$appt_number='',$customer_number='',$worktype_label='',
            $time_slot='',$time_of_booking='',$duration='',$name='',$phone='',
            $email='',$cell='',$address='',$city='',$state='',
            $zip='',$language='', $reminder_time='',$time_zone='',$coordx='',
            $coordy=''
            )
    {

         $dataOfsc = array(
                "bucket" => $bucket,
                "actividad" => $actividad,
                "date" => $date,
                "type" => $type,
                "external_id" => $external_id,
                "start_time" => $start_time,
                "end_time" => $end_time,
                "appt_number" => $appt_number,
                "customer_number" =>$customer_number,
                "worktype_label" => $worktype_label,
                "time_slot" => $time_slot,
                "time_of_booking" => $time_of_booking,
                "duration" => $duration,
                "name" => $name,
                "phone" => $phone,
                "email" => $email,
                "cell" => $cell,
                "address" => $address,                
                "city" =>$city,
                "state" => $state,          
                "zip" => $zip,
                "language" => $language,
                "reminder_time" => $reminder_time,
                "time_zone" => $time_zone,
                "coordx" => $coordx,
                "coordy" => $coordy,
            );

            return $dataOfsc;
        
    }


    public static function getActividad(
            $XA_CREATION_DATE="",
            $XA_SOURCE_SYSTEM="",
            $XA_CUSTOMER_SEGMENT="",
            $XA_CUSTOMER_TYPE="",
            $XA_CONTACT_NAME="",
            $XA_CONTACT_PHONE="",
            $XA_CONTACT_PHONE_NUMBER_2="",
            $XA_CONTACT_PHONE_NUMBER_3="",
            $XA_CONTACT_PHONE_NUMBER_4="",
            $XA_CITY_CODE="",
            $XA_DISTRICT_CODE="",
            $XA_DISTRICT_NAME="",
            $XA_ZONE="",
            $XA_QUADRANT="",
            $XA_ADDRESS_LINK_HTTP="",
            $XA_WORK_ZONE_KEY="",
            $XA_RURAL="",
            $XA_RED_ZONE="",
            $XA_WORK_TYPE="",
            $XA_APPOINTMENT_SCHEDULER="",
            $XA_USER="",
            $XA_REQUIREMENT_NUMBER="",
            $XA_NUMBER_SERVICE_ORDER="",
            $XA_CHANNEL_ORIGIN="",
            $XA_SALES_POINT_CODE="",
            $XA_SALES_POINT_DESCRIPTION="",
            $XA_COMMERCIAL_VALIDATION="",
            $XA_TECHNICAL_VALIDATION="",
            $XA_WEB_UNIFICADA="",
            $XA_ORDER_AREA="",
            $XA_COMMERCIAL_PACKET="",
            $XA_COMPANY_NAME="",
            $XA_GRUPO_QUIEBRE="",
            $XA_QUIEBRES="",
            $XA_BUSINESS_TYPE="",
            $XA_PRODUCTS_SERVICES="",
            $XA_CURRENT_PRODUCTS_SERVICES="",
            $XA_EQUIPMENT="",
            $XA_NOTE="",
            $XA_TELEPHONE_TECHNOLOGY="",
            $XA_BROADBAND_TECHNOLOGY="",
            $XA_TV_TECHNOLOGY="",
            $XA_ACCESS_TECHNOLOGY="",
            $XA_HFC_ZONE="",
            $XA_REQUIREMENT_TYPE="",
            $XA_REQUIREMENT_REASON="",
            $XA_CATV_SERVICE_CLASS="",
            $XA_MDF="",
            $XA_CABLE="",
            $XA_CABINET="",
            $XA_BOX="",
            $XA_TERMINAL_ADDRESS="",
            $XA_TERMINAL_LINKHTTP="",
            $XA_ADSLSTB_PREFFIX="",
            $XA_ADSLSTB_MOVEMENT="",
            $XA_ADSL_SPEED="",
            $XA_ADSLSTB_SERVICE_TYPE="",
            $XA_PENDING_EXTERNAL_ACTION="",
            $XA_SMS_1="",
            $XA_DIAGNOSIS="",
            $XA_TOTAL_REPAIRS="",
            $XA_NUMERO_ENVIO=""        
        )
        {

         $dataOfsc = array(
                    "XA_CREATION_DATE" => $XA_CREATION_DATE,
                    "XA_SOURCE_SYSTEM" => $XA_SOURCE_SYSTEM,
                    "XA_CUSTOMER_SEGMENT" => $XA_CUSTOMER_SEGMENT,
                    "XA_CUSTOMER_TYPE" => $XA_CUSTOMER_TYPE,
                    "XA_CONTACT_NAME" => $XA_CONTACT_NAME,
                    "XA_CONTACT_PHONE" => $XA_CONTACT_PHONE,
                    "XA_CONTACT_PHONE_NUMBER_2" => $XA_CONTACT_PHONE_NUMBER_2,
                    "XA_CONTACT_PHONE_NUMBER_3" => $XA_CONTACT_PHONE_NUMBER_3,
                    "XA_CONTACT_PHONE_NUMBER_4" => $XA_CONTACT_PHONE_NUMBER_4,
                    "XA_CITY_CODE" => $XA_CITY_CODE,
                    "XA_DISTRICT_CODE" => $XA_DISTRICT_CODE,
                    "XA_DISTRICT_NAME" => $XA_DISTRICT_NAME,
                    "XA_ZONE" => $XA_ZONE,
                    "XA_QUADRANT" => $XA_QUADRANT,
                    "XA_ADDRESS_LINK_HTTP" => $XA_ADDRESS_LINK_HTTP,
                    "XA_WORK_ZONE_KEY" => $XA_WORK_ZONE_KEY,
                    "XA_RURAL" => $XA_RURAL,
                    "XA_RED_ZONE" => $XA_RED_ZONE,
                    "XA_WORK_TYPE" => $XA_WORK_TYPE,
                    "XA_APPOINTMENT_SCHEDULER" => $XA_APPOINTMENT_SCHEDULER,
                    "XA_USER" => $XA_USER,
                    "XA_REQUIREMENT_NUMBER" => $XA_REQUIREMENT_NUMBER,
                    "XA_NUMBER_SERVICE_ORDER" => $XA_NUMBER_SERVICE_ORDER,
                    "XA_CHANNEL_ORIGIN" => $XA_CHANNEL_ORIGIN,
                    "XA_SALES_POINT_CODE" => $XA_SALES_POINT_CODE,
                    "XA_SALES_POINT_DESCRIPTION" => $XA_SALES_POINT_DESCRIPTION,
                    "XA_COMMERCIAL_VALIDATION" => $XA_COMMERCIAL_VALIDATION,
                    "XA_TECHNICAL_VALIDATION" => $XA_TECHNICAL_VALIDATION,
                    "XA_WEB_UNIFICADA" => $XA_WEB_UNIFICADA,
                    "XA_ORDER_AREA" => $XA_ORDER_AREA,
                    "XA_COMMERCIAL_PACKET" => $XA_COMMERCIAL_PACKET,
                    "XA_COMPANY_NAME" => $XA_COMPANY_NAME,
                    "XA_GRUPO_QUIEBRE" => $XA_GRUPO_QUIEBRE,
                    "XA_QUIEBRES" => $XA_QUIEBRES,
                    "XA_BUSINESS_TYPE" => $XA_BUSINESS_TYPE,
                    "XA_PRODUCTS_SERVICES" => $XA_PRODUCTS_SERVICES,
                    "XA_CURRENT_PRODUCTS_SERVICES" => $XA_CURRENT_PRODUCTS_SERVICES,
                    "XA_EQUIPMENT" => $XA_EQUIPMENT,
                    "XA_NOTE" => $XA_NOTE,
                    "XA_TELEPHONE_TECHNOLOGY" => $XA_TELEPHONE_TECHNOLOGY,
                    "XA_BROADBAND_TECHNOLOGY" => $XA_BROADBAND_TECHNOLOGY,
                    "XA_TV_TECHNOLOGY" => $XA_TV_TECHNOLOGY,
                    "XA_ACCESS_TECHNOLOGY" => $XA_ACCESS_TECHNOLOGY,
                    "XA_HFC_ZONE" => $XA_HFC_ZONE,
                    "XA_REQUIREMENT_TYPE" => $XA_REQUIREMENT_TYPE,
                    "XA_REQUIREMENT_REASON" => $XA_REQUIREMENT_REASON,
                    "XA_CATV_SERVICE_CLASS" => $XA_CATV_SERVICE_CLASS,
                    "XA_MDF" => $XA_MDF,
                    "XA_CABLE" => $XA_CABLE,
                    "XA_CABINET" => $XA_CABINET,
                    "XA_BOX" => $XA_BOX,
                    "XA_TERMINAL_ADDRESS" => $XA_TERMINAL_ADDRESS,
                    "XA_TERMINAL_LINKHTTP" => $XA_TERMINAL_LINKHTTP,
                    "XA_ADSLSTB_PREFFIX" => $XA_ADSLSTB_PREFFIX,
                    "XA_ADSLSTB_MOVEMENT" => $XA_ADSLSTB_MOVEMENT,
                    "XA_ADSL_SPEED" => $XA_ADSL_SPEED,
                    "XA_ADSLSTB_SERVICE_TYPE" => $XA_ADSLSTB_SERVICE_TYPE,
                    "XA_PENDING_EXTERNAL_ACTION" => $XA_PENDING_EXTERNAL_ACTION,
                    "XA_SMS_1" => $XA_SMS_1,
                    "XA_DIAGNOSIS" => $XA_DIAGNOSIS,
                    "XA_TOTAL_REPAIRS" => $XA_TOTAL_REPAIRS,
                    "XA_NUMERO_ENVIO" => $XA_NUMERO_ENVIO,
                );

         return $dataOfsc;

    }



}
