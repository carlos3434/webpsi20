<?php

class Sms
{

    public static function enviar($celular = '953690299', $mensaje = '', $idUser = '', $legado = "produccion")
    {
        if ($celular != '' || $id_user != '') {
            $celular = substr($celular, -9);
            
            if (App::environment('local') || $legado == "test") {
                //$celular='953669813';
                $url = Config::get("wpsi.sms.url");
                $postData = [
                    "enviar_sms" => 1,
                    "celular" => "$celular",
                    "iduser" => "$idUser",
                    "mensaje" => "$mensaje"
                ];
                print_r($postData);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //Retorno
                $result = curl_exec($ch);
                curl_close($ch);
                return;
            }

            $datos = array(
                'celular'=> $celular,
                'id_user'=> $idUser,
                'estado_envio'=> 1,
                'mensaje' => $mensaje[0]
            );
            $push = Queue::later(5, 'Legados\controllers\EnvioSmsController@enviar', $datos, 'envio_sms');
            return;
            
        } else {
            echo "no enviado normal \n";
            return false;
        }
    }
    /**
     * Envio de mensajes de texto
     *
     * @param String $numero    numero de celular movistar a enviar
     * @param String $mensaje   mensaje de texto a enviar
     * @param Integer $iduser   variable de envio al api de sms
     * @param Integer $legado   entorno de los legados:  test, prod, dev
     * @return type
     */
    public static function enviarSincrono($celular = '953690299', $mensaje = '', $id_user = '', $legado = '')
    {
        if ($celular != '' || $id_user != '') {

            $celular = substr($celular, -9);

            if (App::environment('local') || $legado == "test") {
                $celular='953669813';
            }

            $estadoEnvio = 1;
            exec("perl /www/psi20/htdocs/enviar_sms.pl ".$celular." '".$mensaje."' ".$id_user." ".$estadoEnvio);

            $path = base_path().'/../enviar_sms.pl';
            exec("perl ".$path." ".$celular." '".$mensaje."' ".$id_user." ".'1');
            return;
            
        } else {
            echo "no enviado sincrono \n";
            return false;
        }
        
    }
}
