<?php

namespace Ofsc\Auth;
/**
* 
*/
class AuthSoap
{

    /**
     * Cadena de autenticación para OFSC API's
     * @return String
     */
    private function getAuthString($now)
    {
        $authString = $now
                      . md5(\Config::get("ofsc.auth.pass"));
        return md5($authString);
    }

    /**
     * Retorna arreglo de autenticación
     * @return array
     */
    public function getAuthArray()
    {
        $now = date("c");
        $xmlArray = [
            "user"=>[
                "now" => $now,
                "login" => (\Config::get("ofsc.auth.login")),
                "company" => (\Config::get("ofsc.auth.company")),
                "auth_string" => $this->getAuthString($now)
            ]
        ];

        return $xmlArray;
    }

    /**
     * Retorna la estructura xml para autenticacion
     * @return String
     */
    protected function getAuthXml()
    {
        $now = date("c");
        $xmlArray = [
            "now" => $now,
            "login" => (\Config::get("ofsc.auth.login")),
            "company" => (\Config::get("ofsc.auth.company")),
            "auth_string" => $this->getAuthString($now)
        ];

        $xml = new \SimpleXMLElement('<user/>');
        foreach ($xmlArray as $k => $v) {
            $xml->addChild($k, $v);
        }
        return $xml->asXML();
    }
}