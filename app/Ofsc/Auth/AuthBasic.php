<?php

/*
 * To Rest
 */

namespace Ofsc\Auth;
use Ofsc\Auth;

/**
 * Description of OfscRest
 *
 * @author JNt
 */
class AuthBasic implements AuthInterface
{
    /**
     * Convert string auth with base_64 encode
     * @return String
     */
    protected function getAuthBase64()
    {
        return base64_encode($this->getAuthString());
    }

    /**
     * Cadena de autenticación para OFSC API's
     * @return String
     */
    public function getAuthString()
    {
        $authString = \Config::get("ofsc.auth.login")
                . '@' . \Config::get("ofsc.auth.company")
                . ':' . \Config::get("ofsc.auth.pass");
        return $authString;
    }

    /**
     * Structure aditional for debug or testing operation
     * @return mix
     */
    public function toString()
    {
        $r['auth'] =  $this->getAuthString();
        $r['httpHeader'] =  Authentication::httpHeader;
        return $r;
    }
}
