<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ofsc\Auth;

/**
 * Description of Curl
 *
 * @author Administrador
 */
class AuthTwo implements AuthInterface
{

    /**
     * Convert string auth with base_64 encode
     * @return String
     */
    protected function getAuthBase64()
    {
        return '';
    }

    /**
     * Cadena de autenticación para OFSC API's
     * @return String
     */
    public function getAuthString()
    {
        $authString = \Config::get("ofsc.oAuth2.client_id")
                . ':' . \Config::get("ofsc.oAuth2.client_secret");
        return $authString;
    }

    /**
     * Cadena de "IP:PUERTO" proxy
     * @return string
     */
    public function getProxy()
    {
        $proxyString = \Config::get("wpsi.proxy.host")
                . ':' . \Config::get("wpsi.proxy.post");
        return $proxyString;
    }

    /**
     * Structure aditional for debug or testing operation
     * @return mix
     */
    public function toString()
    {
        return '';
    }

}
