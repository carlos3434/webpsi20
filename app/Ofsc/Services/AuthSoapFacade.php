<?php
namespace Ofsc\Services;

use Illuminate\Support\Facades\Facade;

class AuthSoapFacade extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'authSoap'; }

}