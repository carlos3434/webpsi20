<?php
namespace Ofsc\Services;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class OfscServiceProvider extends ServiceProvider {

    public function register()
    {
        //registrar los tipos de autentificacion para OFSC:
        $this->app->bind('authSoap', 'Ofsc\Auth\AuthSoap');
        $this->app->bind('authBasic', 'Ofsc\Auth\AuthBasic');
        $this->app->bind('authTwo', 'Ofsc\Auth\AuthTwo');
        $this->setAliases();
    }

    public function setAliases()
    {
        $this->app->booting(function ()
        {
            $loader = AliasLoader::getInstance();
            // Facades
            $loader->alias('AuthSoap', 'Ofsc\Services\AuthSoapFacade');
            $loader->alias('AuthBasic', 'Ofsc\Services\AuthBasicFacade');
            $loader->alias('AuthTwo', 'Ofsc\Services\AuthTwoFacade');
        });
    }
}