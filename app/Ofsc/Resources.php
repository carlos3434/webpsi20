<?php

namespace Ofsc;
use Ofsc\Ofsc;

class Resources extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.resource");
        $this->_tipo = \Config::get("ofsc.tipo.resource");
        parent::__construct();
    }
    public function getResources($parentResourced = 'TDP')
    {
        $url = \Config::get("ofsc.rest.resource.gets");
        $properties=['id','parent_id','name','status','type','phone'];
        
        $body = [
            "root_resource_id" => $parentResourced,
            "required_properties" => 
                ['label' => $properties]
        ];
        $elementos= [
            'action' => 'get_resources_list',
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => '',
        ];
        $response = $this->doAction($elementos);

        if (isset($response->data->resources->resource)) {
            $array = [];
            $result = json_decode(json_encode($response->data->resources->resource), true);
            for ($i=0; $i <count($result); $i++) {
                $count = count($result[$i]['properties']['property']);
                for ($j=0; $j < $count; $j++) { 
                    $name = $result[$i]['properties']['property'][$j]['name'];
                    $value = $result[$i]['properties']['property'][$j]['value'];
                    $resource[$name] = $value;
                }
                $resources[] = $resource;
            }
            $response->totalResults = $response->data->resources_count;
            $response->data = $resources;
        } else {
            $response->totalResults = $response->data['totalResults'];
            $response->data = $response->data['items'];
        }
        return $response;

    }
    public function getResourceslimit($limit = 0, $offset = 0)
    {
        $url =sprintf(\Config::get("ofsc.rest.resource.getsLimit"), $limit, $offset);
        $properties=['id','parent_id','name','status','type','workskills'];
        $body = [
            "select_from" => $offset,
            "select_count" => $limit,
            "required_properties" => 
                ['label' => $properties]
        ];
        $elementos= [
            'action' => 'get_resources_list',
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        $changed = ['id'=>'resourceId','parent_id'=>'parentResourceId','type'=>'resourceType'];
        if (isset($response->data->resources->resource)) {
            $result = json_decode(json_encode($response->data->resources->resource), true);
            for ($i=0; $i <count($result); $i++) {
                $count = count($result[$i]['properties']['property']);
                for ($j=0; $j < $count; $j++) { 
                    $name = $result[$i]['properties']['property'][$j]['name'];
                    $value = $result[$i]['properties']['property'][$j]['value'];
                    if ($name=='id' || $name =='parent_id' || $name=='type') {
                        $resource[$changed[$name]] = $value;
                    } else {
                        $resource[$name] = $value;
                    }
                }
                $resources[] = $resource;
            }
            $response->totalResults = $response->data->resources_count;
            $response->data = $resources;
        } elseif (isset($response->data['items'])) {
            $response->totalResults = $response->data['totalResults'];
            $response->data = $response->data['items'];
        } else {
            $response->data = [];
            return $response;
        }
        return $response;
    }
    public function updateLocations($locations){
        $url = '';//\Config::get("ofsc.rest.resource.gets");
        $properties=['id','parent_id','name','status','type','phone'];
        
        $body = [
            "locations" => $locations
        ];
        $elementos= [
            'action' => 'update_locations',
            'body' => $body,
            'url' => $url,
            'method' => '',
            'request' => '',
        ];
        $response = $this->doAction($elementos);

        return $response;
    }
}