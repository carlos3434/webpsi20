<?php
namespace Ofsc;
use Ofsc\Ofsc;
/**
 * API Capacity
 */
class Outbound extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.outbound");
        $this->_tipo = \Config::get("ofsc.tipo.outbound");
        parent::__construct();

    }
    /**
     * este metodo se envia a TOAdirect para informar del estado de los mensajes
     * 
     */
    public function setMessageStatus($messages=array())
    {
        $url ='';

        $body = array(
            "messages" => $messages
        );
        $elementos= [
            'action' => 'set_message_status',
            'body' => $body,
            'url' => $url,
            'method' => '',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        
        return $response;
    }


}
