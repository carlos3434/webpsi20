<?php

namespace Ofsc;
/**
* clase padre de configuracion para consumir web service
*/
use Logs\models\EnvioOfsc as EnvioOfsc;

Abstract class Ofsc
{
    protected $_wsdl;
    protected $_client;
    protected $_tipo;
    /**
     * funcion que inicia el enlace entre OFSC y la webpsi
     * mediante el SOAP, en case falle retorna false
     * y se guarda en la tabla de errores
     */
    public function __construct() 
    {
        if ($this->_tipo=='soap') {
            //try {
                $wsOptArray = [
                    "proxy_host" => \Config::get("wpsi.proxy.host"),
                    "proxy_port" => \Config::get("wpsi.proxy.port"),
                    "trace" => 1,
                    "exception" => 0
                ];
                $opts = array('http' => array('protocol_version' => '1.0'));
                $wsOptArray["stream_context"] = stream_context_create($opts);
                /*$soapClient = new \SoapClient(
                    $this->_wsdl,
                    $wsOptArray
                );
                $this->_client = $soapClient;*/

                try {
                    $soapClient = new \SoapClient(
                        $this->_wsdl,
                        $wsOptArray
                    );
                    
                } catch (\SoapFault $e) {
                    return  false;
                }
                $this->_client = $soapClient; 
            /*} catch (\SoapFault $fault) {
                //registrar error 
                //enviar sms a celular
                $this->_client= false;
                throw new \Exception('no se pudo establecer conexion con ofsc');
            } catch (\Exception $exc) {
                //registrar error 
                //enviar sms a celular
                $this->_client= false;
                throw new \Exception('no se pudo establecer conexion con ofsc');
            }*/
        }
    }

    /**
     * ejecuta una accion (metodo) de una determinada api
     * se le envia un array ($setArray) como parametro
     * puede guardarse un log en Base de datos o un log temporal
     *
     * @param String $action
     * @param String $setArray
     * @param Double $save
     * @return type $response
     */
    protected function doAction($elementos=array())
    {
        if ($this->_tipo=='soap') {
            return $this->soap($elementos);
        } else {
            return $this->rest($elementos);
        }

    }
    
    /**
     * $elementos: body, action
     * si no hay data retornar $result->error:false y $result->data:vacio
     */
    private function soap($elementos)
    {
        $response = new \stdClass();
        $response->error = false;
        $response->errorMsg ='';

        if ($this->_client===false || $this->_client===null) {;
            return false;
        }
        try {
            $request = array_merge(\AuthSoap::getAuthArray(), $elementos['body']);
            
            $result = $this->_client->__soapCall(
                $elementos['action'], ["request"=>$request]
            );

            $response->data = $result;

            if (isset($result->result_code) && $result->result_code==0) {
                $response->data = $result;
                
            } elseif (isset($result->result_code) && $result->result_code!=0) {
                $response->error = true;
                $response->errorMsg = $result->error_msg;
            } else {
                $response->data = $result;
            }

            return $response;
        } catch (\SoapFault $fault) {
            $response->error = true;
            $response->errorMsg = $fault->faultstring;
            return $response;
        }
    }
    /**
     * $elementos: url, method, request
     */
    private function rest($elementos)
    {
        $response = new \stdClass();
        $response->error = false;
        $response->errorMsg ='';
        //try {
            $curl = curl_init($elementos['url']);
            if ($curl === false) {
                return 'NOT INIT Curl';
            }
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, \AuthBasic::getAuthString());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($curl, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
            curl_setopt($curl, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));

            if ($elementos['method'] != 'POST') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $elementos['method']);
            }
            if (strlen($elementos['request']) > 2) {
                curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $elementos['request']);
            }
            curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($curl);
            
            if ($result == false) {
                $response->error=true;
                $response->errorMsg=curl_error($curl);
            } elseif (is_string($result)) {
                //$response->data=(array)json_decode(strip_tags($result));

                $json_result = (array)json_decode(strip_tags($result));


                if ( isset( $json_result["type"] ) && isset( $json_result["title"] ) && isset( $json_result["status"] ) && isset( $json_result["detail"] ) ) {

                    $response->error = true;
                    $response->errorMsg = implode("::::", (array)$json_result);
                    $response->data = [];

                } else if ( isset( $json_result["items"] ) ) {

                    $response->data = $json_result["items"];
                
                } else {
                
                    $response->data = $json_result;
                
                }


            } else {
                $response->data = (array)json_decode($result);
            }
            curl_close($curl);
        /*} catch (\Exception $error) {
            $response->error = true;
            $response->errorMsg = $error->getMessage();

            return $response;
        }*/
        return $response;
    }
    /**
     * @param $request array
     * @param $response array
     */
    protected function tracer( $elementos = [], $response = [] )
    {
        //try {
            $userId = \Auth::id() ? \Auth::id() :'697';
            $envio = new EnvioOfsc();
            $envio['usuario_created_at'] = $userId;
            if ($this->_tipo=='soap') {
                $envio['tipo'] = '1';
                $envio['accion'] = $elementos['action'];
                $envio['enviado'] = json_encode($elementos['body']);
            } else {
                $envio['tipo'] = '2';
                $envio['accion'] = $elementos['method'].' : '.$elementos['url'];
                $envio['enviado'] = $elementos['request'];
            }
            //validar propiedades
            $valores=['contrata','nodo','codactu','tipo_actividad_id','tipo_envio','aid'];
            foreach ($elementos as $key => $value) {
                if (array_search($key, $valores)!==false)
                    $envio[$key] = $value;
            }
            $envio['respuesta'] = json_encode($response);
            
            $envio->save();
        /*} catch (Exception $e) {
            
        }*/

    }
}
