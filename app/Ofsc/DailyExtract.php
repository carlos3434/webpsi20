<?php namespace Ofsc;

use Ofsc\Ofsc;

/**
 * API Inbound
 */
class DailyExtract extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.resource");
        $this->_tipo = \Config::get("ofsc.tipo.dailyextract");
        parent::__construct();
    }
    
    /**
     * Crear actividad en OFSC
     *
     * @param Array $data datos de la actividad
     * @param Boolean $sla false=agenda, true=sla
     * @return Object
     */
    public function getDatesExtract()
    {
        $url =\Config::get("ofsc.rest.dailyExtract.getDailyExtractDates");
        $body = [];
        $elementos= [
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => json_encode($body),
        ];

        $response = $this->doAction($elementos);
        return $response;
    }
    
    /**
     * metodo para actualizar propiedades en ofsc
     */
    public function getFilesByDate($date = '')
    {
        $url =sprintf(\Config::get("ofsc.rest.dailyExtract.getListDailyExtractFilesForDate"), $date);
        $body = [];
        $elementos= [
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => json_encode($body),
        ];

        $response = $this->doAction($elementos);
        return $response;
    }
    
    
    public function getFileDownloadByName($date = '', $name = '')
    {
        $url =sprintf(\Config::get("ofsc.rest.dailyExtract.DownloadDailyExtractFile"), $date, $name);
        $response = new \stdClass();
        $response->error = false;
        $response->errorMsg ='';
        //try {
            $curl = curl_init($url);
            if ($curl === false) {
                return 'NOT INIT Curl';
            }
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, \AuthBasic::getAuthString());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($curl, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
            curl_setopt($curl, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));

            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            print_r($result);

            $fh = fopen('/var/www/webpsi20/file.zip', 'w');
curl_setopt($curl, CURLOPT_FILE, $fh); 
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); // this will follow redirects
fclose($fh);

            if ($result == false) {
                $response->error=true;
                $response->errorMsg=curl_error($curl);
            } elseif (is_string($result)) {
                $response->data=(array)json_decode(strip_tags($result));
            } else {
                $response->data = (array)json_decode($result);
            }
            curl_close($curl);
        /*} catch (\Exception $error) {
            $response->error = true;
            $response->errorMsg = $error->getMessage();

            return $response;
        }*/
        return $response;
        return $response;
    }
}
