<?php
namespace Ofsc;
use Ofsc\Ofsc;
/**
 * API Capacity
 */
class Capacity extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.capacity");
        $this->_tipo = \Config::get("ofsc.tipo.capacity");
        parent::__construct();
    }
    /**
     * *se puede buscar capacidad por:
     *  1. location: TEST_LARI_INS_MOVISTAR1   LA3321
     *  2. zona de trabajo: se debe enviar 'XA_WORK_ZONE_KEY'
     *      y 'determine_location_by_work_zone'
     *
     *  OJO:
     *  - si se envia  calculate_work_skill -> true
     *    se debe enviar work_skill
     *    si se le envia 'work_skil' es mas exacto el calculo
     *  - si se envia default_duration = 0
     *    se debe enviar worktype_label 'PROV_INS_M' por ejemplo
     *  - sino se envia time_slo, se recibira todos los backet
     *     de la zona de trabajo o location
     */
    public function getCapacity($data = array())
    {
        $activityField=[
            [
                "name" => "XA_WORK_ZONE_KEY",
                "value" => $data['zona_trabajo']
            ],
            [
                "name" => "worktype_label",
                "value" => $data['worktype_label']
            ],
            [
                "name" => "XA_QUIEBRES",
                "value" => $data['quiebre']
            ]
        ];
        $body = [
            "date" => $data["fecha"],//debe ser un array
            "return_time_slot_info" => true,
            "determine_location_by_work_zone" => true,
            "activity_field" => $activityField,
            "calculate_duration" => false,
            "calculate_travel_time" => false,
            "calculate_work_skill" => true,
            //"dont_aggregate_results" => false,
            //"min_time_to_end_of_time_slot" => "0",
            //"default_duration" => 60,
        ];

        if (isset($data["bucket"]) and $data["bucket"]!="") {
            $body["location"] = $data["bucket"];
        }
        if ($data["time_slot"]!="") {
            $body["time_slot"] = $data["time_slot"];
        }

        $elementos= [
            'action' => 'get_capacity',
            'body' => $body
        ];
        $response = $this->doAction($elementos);
        //$this->tracer($elementos, $response);
        return $response;
    }

    public function getCapacityOld($data=array())
    {
        $activityField=[
            [
                "name" => "XA_WORK_ZONE_KEY",
                "value" => $data['zona_trabajo']
            ]
        ];
        $body = [
            "date" => $data["fecha"],//debe ser un array
            "return_time_slot_info" => true,
            "determine_location_by_work_zone" => true,
            "activity_field" => $activityField
        ];

        if (isset($data["bucket"]) and $data["bucket"]!="") {
            $body["location"] = $data["bucket"];
        }
        if ($data["time_slot"]!="") {
            $body["time_slot"] = $data["time_slot"];
        }
        if ($data["work_skill"]!="") {
            $body["calculate_work_skill"] = true;
            $body["work_skill"] = $data["work_skill"];
        }
        $elementos= [
            'action' => 'get_capacity',
            'body' => $body
        ];
        $response = $this->doAction($elementos);
        //$this->tracer($elementos, $response);
        return $response;
    }
    public function getQuotaData()
    {

        try {
            $setArray = array(
            "date" => "",
            "reosurce_id" => "",
            "aggregate_results" => "",
            "calculate_totals" => "",
            "time_slot" => "",
            "category" => "",
            "day_quota_field" => "",
            "time_slot_quota_field" => "",
            "category_quota_field" => "",
            "work_zone_quota_field" => ""
            );
//            $requestArray = array_merge($this->getAuthArray(), $setArray);
//            $response = $this->doAction('get_quota_data', $setArray);
//
            //temporal -------------------------------
            $capacity = new Simulador();
            $response = $capacity->get_quota_data($setArray);
            //temporal -------------------------------

            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function SetQuota()
    {

        try {
            $setArray = array(
            "bucket" => array(
                    "bucket_id" => "",
                    "day" => array(
                            "date" => "",
                            "quota_percent" => "",
                            "min_quota" => "",
                            "quota" => "",
                            "status" => "",
                            "time_slot" => array(
                                    "label" => "",
                                    "quota_percent" => "",
                                    "min_quota" => "",
                                    "quota" => "",
                                    "stop_booking_at" => "",
                                    "status" =>"",
                                    "category" => array(
                                        "label" => "",
                                        "quota_percent" => "",
                                        "min_quota" => "",
                                        "quota" => "",
                                        "stop_booking_at" => "",
                                        "status" => "",
                                        "work_zone" => array(
                                                "label" => "",
                                                "status" => ""
                                            )
                                    )
                               )
                        )
                )

            );
            $requestArray = array_merge($this->getAuthArray(), $setArray);

            $response = $this->doAction('set_quota', $setArray);

            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function getQuotaCloseTime()
    {

        try {
            $setArray = array(
            "bucket_id" => "",
            "day_offset" => "",
            "time_slot" => "",
            "category" => "",
            "work_zone" => ""
            );

            $requestArray = array_merge($this->getAuthArray(), $setArray);

            $response = $this->doAction('get_quota_close_time', $setArray);

            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function setQuotaCloseTime()
    {

        try {
            $setArray = array(
            "bucket_id" => "",
            "day_offset" => "",
            "time_slot" => "",
            "category" => "",
            "work_zone" => "",
            "close_time" => ""
            );

            $requestArray = array_merge($this->getAuthArray(), $setArray);

            $response = $this->doAction('set_quota_close_time', $setArray);

            return $response;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
