<?php namespace Ofsc;

use Ofsc\Ofsc;

/**
 * API Inbound
 */
class Inbound extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.inbound");
        $this->_tipo = \Config::get("ofsc.tipo.inbound");
        parent::__construct();
    }
    
    /**
     * Crear actividad en OFSC
     *
     * @param Array $data datos de la actividad
     * @param Boolean $sla false=agenda, true=sla
     * @return Object
     */
    public function createActivity($head = [], $cita = [], $propiedades = [], $inventarios = [], $sla = false)
    {

        $appointment=[];
        foreach ($cita as $key => $value) {
            if (isset($value)) {
                $appointment[$key]=$value;
            }
        }
        //propiedades de la actividad
        $properties=[];
        foreach ($propiedades as $key => $value) {
            if (isset($value)) {
                $properties[]=['label'=>$key,'value'=>$value];
            }
        }
        $appointment["properties"] = ["property"=>$properties];
        //inventarios de la actividad
        
        if (count($inventarios)>0) {
            $properties=[];
            foreach ($inventarios as $key_ => $value) {
                $property=[];
                $i=0;
                foreach ($value as $key => $valores) {
                    $property[$i]=['label'=>$key,'value'=>$valores];
                    $i++;
                }
                $properties[]=[
                    "properties" => [
                        "property" =>$property
                    ]
                ];
            }
            $appointment["inventories"] = [
                "inventory" =>$properties
            ];
        }

        $body = array(
            "head" => array(
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "date" => $head["date"],
                //"allow_change_date" => "yes",
                "appointment" => array(
                    "keys" => array(
                        "field" => "appt_number"
                    ),
                    //"upload_type" => "full"
                    "action_if_completed" => "create"
                ),
                "inventory" => array(
                    "keys" => array(
                        "field" => "invsn"
                    ),
                    "upload_type" => "full"
                ),
                "properties_mode" => "replace"
            ),
            "data" => array(
                "commands" => array(
                    "command" => array(
                        "date" => $head["date"],
                        "type" => "update_activity",
                        "external_id" => $head["external_id"],
                        "appointment" => $appointment
                    )
                )
            )
        );
        //1: agenda, 2:sla
        $tipoEnvio=1;
        if ($sla) {
            $tipoEnvio=2;
        }

        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body,
            ////'data-ofsc' => $data,
            'codactu' => $cita["appt_number"],
            'tipo_envio' => $tipoEnvio,
            'nodo' => $propiedades["XA_HFC_NODE"],
            'tipo_actividad_id' => '',
            'contrata' => $propiedades["XA_COMPANY_NAME"],
            'aid'=>''
        ];
        $response = $this->doAction($elementos);
        if (isset($response->data->data->commands->command->appointment->aid)) {
            $elementos["aid"]=$response->data->data->commands->command->appointment->aid;
        }
        $this->tracer($elementos, $response);
        return $response;
    }
    
    /**
     * metodo para actualizar propiedades en ofsc
     */
    public function updateActivity($codactu, $properties = [], $appointment = [], $type = "update_activity")
    {
        $propiedades=[];
        foreach ($properties as $key => $value) {
            $propiedades[]=['label'=>$key,'value'=>$value];
        }

        $appointment["appt_number"]=$codactu;
        if (count($propiedades)) {
            $appointment["properties"]=["property" => $propiedades];
        }

        $body = [
            "head" => [
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "appointment" => [
                    "keys" => [
                        "field" => "appt_number",
                        "action_if_completed" => "create"
                    ],
                    //"upload_type" => "full"
                ],
                "inventory" => [
                    "keys" => [
                        "field" => "invsn"
                    ],
                    //"upload_type" => "full"
                ],
                "properties_mode" => "update"
            ],
            "data" => [
                "commands" => [
                    "command" => [
                        //"date" => $data["date"],
                        "type" => $type,
                        //"external_id" => $data["bucket"],
                        //"start_time" => $data["start_time"],
                        //"end_time" => $data["end_time"],
                        "appointment" =>$appointment
                        /*"appointment" => [
                            "appt_number" => $codactu,
                            //"customer_number" => $data["customer_number"],
                            //"worktype_label" => $data["worktype_label"],
                            //"time_slot" => $data["time_slot"],
                            //"time_of_booking" => $data["time_of_booking"],
                            //"duration" => $data["duration"],
                            //"name" => $data["name"],
                            //"phone" => $data["phone"],
                            //"email" => $data["email"],
                            //"cell" => $data["cell"],
                            //"address" => $data["address"],
                            //"city" => $data["city"],
                            //"state" => $data["state"],
                            //"zip" => $data["zip"],
                            //"language" => $data["language"],
                            //"reminder_time" => $data["reminder_time"],
                            //"time_zone" => $data["time_zone"],
                            //"coordx" => $data["coordx"],
                            //"coordy" => $data["coordy"],
                            "properties" => [
                                "property" => $propiedades
                            ]
                        ] */
                    ]
                ]
            ]
        ];
        
        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body
        ];
        //$response = $this->doAction('inbound_interface', $body);
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        if (isset($response->data->data->commands->command->appointment->report->message)) {
            return $response->data->data->commands->command->appointment->report->message;
        }
        return $response;
    }
    
    
    public function cancelActivity($activityId, $codactu, $actividad)
    {
    //$actividad['XA_NOTE'] puede venir de EnvioMasivoOfsc.php
        $body = array(
            "head" => array(
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "appointment" => array(
                    "keys" => array(
                        "field" => "appt_number"
                    ),
                    "action_if_completed" => "create"
                ),
                "inventory" => array(
                    "keys" => array(
                        "field" => "invsn"
                    ),
                    "upload_type" => "full"
                ),
                "properties_mode" => "update"
            ),
            "data" => array(
                "commands" => array(
                    "command" => array(
                        "type" => "cancel_activity",
                        "appointment" => array(
                            "appt_number" => $codactu,
                            "properties" => array(
                                "property" => array(
                                    array(
                                        "label" => "XA_CANCEL_REASON",
                                        "value" => 'CC001'
                                    ),
                                    array(
                                        "label" => "XA_NOTE",
                                        "value" => isset($actividad['XA_NOTE']) ? $actividad['XA_NOTE'] : ""
                                    ),
                                    array(
                                        "label" => "A_OBSERVATION",
                                        "value" => isset($actividad['A_OBSERVATION']) ? $actividad['A_OBSERVATION'] : ""
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
        
        /*
        $requestArray = array_merge($this->getAuthArray(), $body);
        /*
        $response = $this->client->call('inbound_interface_request', $body);
         * 
         */
        /*return $requestArray;*/
        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body
        ];
        //$response = $this->doAction('inbound_interface', $body);
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }
}
