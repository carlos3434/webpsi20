<?php
namespace Ofsc;
use Ofsc\Ofsc;

/**
 * API Inbound
 */
class Inbound extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.inbound");
        $this->_tipo = \Config::get("ofsc.tipo.inbound");
        parent::__construct();
    }
    
    /**
     * Crear actividad en OFSC
     * 
     * @param Array $data datos de la actividad
     * @param Boolean $sla false=agenda, true=sla
     * @return Object
     */
    public function createActivity($data=array(), $sla=false)
    {
        $body = array(
            "head" => array (
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "date" => $data["date"],
                //"allow_change_date" => "yes",
                "appointment" => array(
                    "keys" => array(
                        "field" => "appt_number"
                    ),
                    //"upload_type" => "full"
                    "action_if_completed" => "create"
                ),
                "inventory" => array(
                    "keys" => array(
                        "field" => "invsn"
                    ),
                    "upload_type" => "full"
                ),
                "properties_mode" => "replace"
            ),
            "data" => array(
                "commands" => array(
                    "command" => array(
                        "date" => $data["date"],
                        "type" => "update_activity",
                        "external_id" => $data["bucket"],
                        "appointment" => array(
                            "appt_number" => $data["appt_number"],
                            "customer_number" => $data["customer_number"],
                            "worktype_label" => $data["worktype_label"],
                            "sla_window_start" => "",
                            "sla_window_end" => "",
                            "time_slot" => $data["time_slot"],
                            "time_of_booking" => $data["time_of_booking"],
                            "duration" => $data["duration"],
                            "name" => $data["name"],
                            "phone" => $data["phone"],
                            "email" => $data["email"],
                            "cell" => $data["cell"],
                            "address" => $data["address"],
                            "city" => $data["city"],
                            "state" => $data["state"],
                            "zip" => $data["zip"],
                            "language" => $data["language"],
                            "reminder_time" => $data["reminder_time"],
                            "time_zone" => $data["time_zone"],
                            "coordx" => $data["coordx"],
                            "coordy" => $data["coordy"],
                            "properties" => array(
                                "property" => array(
                                    array(
                                        "label" => "XA_CREATION_DATE",
                                        "value" => $data["XA_CREATION_DATE"]
                                    ),
                                    array(
                                        "label" => "XA_SOURCE_SYSTEM",
                                        "value" => $data["XA_SOURCE_SYSTEM"]
                                    ),
                                    array(
                                        "label" => "XA_CUSTOMER_SEGMENT",
                                        "value" => $data["XA_CUSTOMER_SEGMENT"]
                                    ),
                                    array(
                                        "label" => "XA_CUSTOMER_TYPE",
                                        "value" => $data["XA_CUSTOMER_TYPE"]
                                    ),
                                    array(
                                        "label" => "XA_CONTACT_NAME",
                                        "value" => $data["XA_CONTACT_NAME"]
                                    ),
                                    array(
                                        "label" => "XA_CONTACT_PHONE_NUMBER_2",
                                        "value" 
                                        => $data["XA_CONTACT_PHONE_NUMBER_2"]
                                    ),
                                    array(
                                        "label" => "XA_CONTACT_PHONE_NUMBER_3",
                                        "value" 
                                        => $data["XA_CONTACT_PHONE_NUMBER_3"]
                                    ),
                                    array(
                                        "label" => "XA_CONTACT_PHONE_NUMBER_4",
                                        "value" 
                                        => $data["XA_CONTACT_PHONE_NUMBER_4"]
                                    ),
                                    array(
                                        "label" => "XA_CITY_CODE",
                                        "value" => $data["XA_CITY_CODE"]
                                    ),
                                    array(
                                        "label" => "XA_DISTRICT_CODE",
                                        "value" => $data["XA_DISTRICT_CODE"]
                                    ),
                                    array(
                                        "label" => "XA_DISTRICT_NAME",
                                        "value" => $data["XA_DISTRICT_NAME"]
                                    ),
                                    array(
                                        "label" => "XA_ZONE",
                                        "value" => $data["XA_ZONE"]
                                    ),
                                    array(
                                        "label" => "XA_QUADRANT",
                                        "value" => $data["XA_QUADRANT"]
                                    ),
                                    array(
                                        "label" => "XA_ADDRESS_LINK_HTTP",
                                        "value" => $data["XA_ADDRESS_LINK_HTTP"]
                                    ),
                                    array(
                                        "label" => "XA_WORK_ZONE_KEY",
                                        "value" => $data["XA_WORK_ZONE_KEY"]
                                    ),
                                    array(
                                        "label" => "XA_RURAL",
                                        "value" => $data["XA_RURAL"]
                                    ),
                                    array(
                                        "label" => "XA_RED_ZONE",
                                        "value" => $data["XA_RED_ZONE"]
                                    ),
                                    array(
                                        "label" => "XA_WORK_TYPE",
                                        "value" => $data["XA_WORK_TYPE"]
                                    ),
                                    array(
                                        "label" => "XA_APPOINTMENT_SCHEDULER",
                                        "value" 
                                        => $data["XA_APPOINTMENT_SCHEDULER"]
                                    ),
                                    array(
                                        "label" => "XA_USER",
                                        "value" => $data["XA_USER"]
                                    ),
                                    array(
                                        "label" => "XA_REQUIREMENT_NUMBER",
                                        "value" 
                                        => $data["XA_REQUIREMENT_NUMBER"]
                                    ),
                                    array(
                                        "label" => "XA_NUMBER_SERVICE_ORDER",
                                        "value" 
                                        => $data["XA_NUMBER_SERVICE_ORDER"]
                                    ),
                                    array(
                                        "label" => "XA_CHANNEL_ORIGIN",
                                        "value" => $data["XA_CHANNEL_ORIGIN"]
                                    ),
                                    array(
                                        "label" => "XA_SALES_POINT_CODE",
                                        "value" => $data["XA_SALES_POINT_CODE"]
                                    ),
                                    array(
                                        "label" => "XA_SALES_POINT_DESCRIPTION",
                                        "value" 
                                        => $data["XA_SALES_POINT_DESCRIPTION"]
                                    ),
                                    array(
                                        "label" => "XA_COMMERCIAL_VALIDATION",
                                        "value" 
                                        => $data["XA_COMMERCIAL_VALIDATION"]
                                    ),
                                    array(
                                        "label" => "XA_TECHNICAL_VALIDATION",
                                        "value" 
                                        => $data["XA_TECHNICAL_VALIDATION"]
                                    ),
                                    array(
                                        "label" => "XA_WEB_UNIFICADA",
                                        "value" => $data["XA_WEB_UNIFICADA"]
                                        //xml
                                    ),
                                    array(
                                        "label" => "XA_ORDER_AREA",
                                        "value" => $data["XA_ORDER_AREA"]
                                    ),
                                    array(
                                        "label" => "XA_COMMERCIAL_PACKET",
                                        "value" => $data["XA_COMMERCIAL_PACKET"]
                                    ),
                                    array(
                                        "label" => "XA_COMPANY_NAME",
                                        "value" => $data["XA_COMPANY_NAME"]
                                    ),                                    
                                    array(
                                        "label" => "XA_GRUPO_QUIEBRE",
                                        "value" => $data["XA_GRUPO_QUIEBRE"]
                                    ),
                                    array(
                                        "label" => "XA_QUIEBRES",
                                        "value" => $data["XA_QUIEBRES"]
                                    ),
                                    array(
                                        "label" => "XA_BUSINESS_TYPE",
                                        "value" => $data["XA_BUSINESS_TYPE"]
                                    ),
                                    array(
                                        "label" => "XA_PRODUCTS_SERVICES",
                                        "value" => $data["XA_PRODUCTS_SERVICES"]
                                    ),
                                    array(
                                        "label" 
                                        => "XA_CURRENT_PRODUCTS_SERVICES",
                                        "value" 
                                        => $data["XA_CURRENT_PRODUCTS_SERVICES"]
                                    ),
                                    array(
                                        "label" => "XA_EQUIPMENT",
                                        "value" => $data["XA_EQUIPMENT"]
                                    ),
                                    array(
                                        "label" => "XA_NOTE",
                                        "value" => $data["XA_NOTE"]
                                    ),
                                    array(
                                        "label" => "XA_TELEPHONE_TECHNOLOGY",
                                        "value" 
                                        => $data["XA_TELEPHONE_TECHNOLOGY"]
                                    ),
                                    array(
                                        "label" => "XA_BROADBAND_TECHNOLOGY",
                                        "value" 
                                        => $data["XA_BROADBAND_TECHNOLOGY"]
                                    ),
                                    array(
                                        "label" => "XA_TV_TECHNOLOGY",
                                        "value" => $data["XA_TV_TECHNOLOGY"]
                                    ),
                                    array(
                                        "label" => "XA_ACCESS_TECHNOLOGY",
                                        "value" => $data["XA_ACCESS_TECHNOLOGY"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_ZONE",
                                        "value" => $data["XA_HFC_ZONE"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_NODE",
                                        "value" => $data["XA_HFC_NODE"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_TROBA",
                                        "value" => $data["XA_HFC_TROBA"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_AMPLIFIER",
                                        "value" => $data["XA_HFC_AMPLIFIER"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_TAP",
                                        "value" => $data["XA_HFC_TAP"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_BORNE",
                                        "value" => $data["XA_HFC_BORNE"]
                                    ),
                                    array(
                                        "label" => "XA_HFC_TAP_LINKHTTP",
                                        "value" => $data["XA_HFC_TAP_LINKHTTP"]
                                    ),
                                    array(
                                        "label" => "XA_REQUIREMENT_TYPE",
                                        "value" => $data["XA_REQUIREMENT_TYPE"]
                                    ),
                                    array(
                                        "label" => "XA_REQUIREMENT_REASON",
                                        "value" 
                                        => $data["XA_REQUIREMENT_REASON"]
                                    ),
                                    array(
                                        "label" => "XA_CATV_SERVICE_CLASS",
                                        "value" 
                                        => $data["XA_CATV_SERVICE_CLASS"]
                                    ),
                                    array(
                                        "label" => "XA_MDF",
                                        "value" => $data["XA_MDF"]
                                    ),
                                    array(
                                        "label" => "XA_CABLE",
                                        "value" => $data["XA_CABLE"]
                                    ),
                                    array(
                                        "label" => "XA_CABINET",
                                        "value" => $data["XA_CABINET"]
                                    ),
                                    array(
                                        "label" => "XA_BOX",
                                        "value" => $data["XA_BOX"]
                                    ),
                                    array(
                                        "label" => "XA_TERMINAL_ADDRESS",
                                        "value" => $data["XA_TERMINAL_ADDRESS"]
                                    ),
                                    array(
                                        "label" => "XA_TERMINAL_LINKHTTP",
                                        "value" => $data["XA_TERMINAL_LINKHTTP"]
                                    ),
                                    array(
                                        "label" => "XA_ADSLSTB_PREFFIX",
                                        "value" => $data["XA_ADSLSTB_PREFFIX"]
                                    ),
                                    array(
                                        "label" => "XA_ADSLSTB_MOVEMENT",
                                        "value" => $data["XA_ADSLSTB_MOVEMENT"]
                                    ),
                                    array(
                                        "label" => "XA_ADSL_SPEED",
                                        "value" => $data["XA_ADSL_SPEED"]
                                    ),
                                    array(
                                        "label" => "XA_ADSLSTB_SERVICE_TYPE",
                                        "value" 
                                        => $data["XA_ADSLSTB_SERVICE_TYPE"]
                                    ),
                                    array(
                                        "label" => "XA_PENDING_EXTERNAL_ACTION",
                                        "value" 
                                        => $data["XA_PENDING_EXTERNAL_ACTION"]
                                    ),
                                    array(
                                        "label" => "XA_SMS_1",
                                        "value" => $data["XA_SMS_1"]
                                    ),
                                    array(
                                        "label" => "XA_NUMERO_ENVIO",
                                        "value" => $data["XA_NUMERO_ENVIO"]
                                    ),
                                    array(
                                        "label" => "XA_DIAGNOSIS",
                                        "value" => $data["XA_DIAGNOSIS"]
                                    )
                                    ,
                                    array(
                                        "label" => "XA_TOTAL_REPAIRS",
                                        "value" => $data["XA_TOTAL_REPAIRS"]
                                    )
                                )
                            )/*,
                            "inventories" => array(
                                "inventory" => array(
                                    "upload_type" => "full",
                                    "properties" => array(
                                        "property" => array(
                                            array(
                                                "label" => "invsn",
                                                "value" => "SN34634987"
                                            ),
                                            array(
                                                "label" => "invtype",
                                                "value" => "1"
                                            ),
                                            array(
                                                "label" => "quantity",
                                                "value" => "2"
                                            ),
                                            array(
                                                "label" => "invpool",
                                                "value" => "customer"
                                            ),
                                        )
                                    )
                                )
                            )*/
                        )
                    )
                )
            )
        );
        
        //Nodo base
        $base = &$body["data"]["commands"]["command"];
        $tipoEnvio=1;//agenda
        //SLA
        if ($sla) { 
            $tipoEnvio=2;
            //Eliminar tags no usados para SLA
            //unset($body["head"]["date"]);            
            //unset($base["date"]);
            unset($base["start_time"]);
            unset($base["end_time"]);
            unset($base["appointment"]["time_slot"]);
            
            //Agregar tags para SLA            
            $base["appointment"]["sla_window_start"] 
                    = $data["sla_window_start"];
            $base["appointment"]["sla_window_end"] 
                    = $data["sla_window_end"];
        } else {
            unset($base["appointment"]["sla_window_start"]);
            unset($base["appointment"]["sla_window_end"]);
        }
        
        //Arreglo de propiedadas
        $propArray = &$body["data"]["commands"]["command"]
                      ["appointment"]["properties"]["property"];
        
        //Datos averias y/o provision
        if ($data["actividad"] != "averia") {
            //Averias
            foreach ($propArray as $key=>$val) {
                if ($val["label"] == "XA_DIAGNOSIS") {
                    unset($propArray[$key]);
                }
                
                if ($val["label"] == "XA_TOTAL_REPAIRS") {
                    unset($propArray[$key]);
                }
            }
        } else {
            //Provision
            foreach ($propArray as $key=>$val) {
                
            }
        }
       
       
        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body,
            'data-ofsc' => $data,
            'codactu' => $data["appt_number"],
            'tipo_envio' => $tipoEnvio,
            'nodo' => $data["XA_HFC_ZONE"],
            'tipo_actividad_id' => '',
            'contrata' => $data["XA_COMPANY_NAME"],
            'aid'=>''
        ];
        //$response = $this->doAction('inbound_interface', $body, true, $data);
        $response = $this->doAction($elementos);
        if (isset($response->data->data->commands->command->appointment->aid)) {
            $elementos["aid"]=$response->data->data->commands->command->appointment->aid;
        }
        $this->tracer($elementos, $response);
        return $response;
    }
    
    /**
     * metodo para actualizar propiedades en ofsc
     */
    public function updateActivity( $codactu , $actividad = [], $appointment = [] )
    {
        $propiedades=[];
        foreach ($actividad as $key => $value) {
            $propiedades[]=['label'=>$key,'value'=>$value];
        }

        $appointment["appt_number"]=$codactu;
        if(count($propiedades)) {
        $appointment["properties"]=["property" => $propiedades]; }

        $body = [
            "head" => [
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "appointment" => [
                    "keys" => [
                        "field" => "appt_number",
                        "action_if_completed" => "create"
                    ],
                    //"upload_type" => "full"
                ],
                "inventory" => [
                    "keys" => [
                        "field" => "invsn"
                    ],
                    //"upload_type" => "full"
                ],
                "properties_mode" => "update"
            ],
            "data" => [
                "commands" => [
                    "command" => [
                        //"date" => $data["date"],
                        "type" => "update_activity",
                        //"external_id" => $data["bucket"],
                        //"start_time" => $data["start_time"],
                        //"end_time" => $data["end_time"],
                        "appointment" =>$appointment
                        /*"appointment" => [
                            "appt_number" => $codactu,
                            //"customer_number" => $data["customer_number"],
                            //"worktype_label" => $data["worktype_label"],
                            //"time_slot" => $data["time_slot"],
                            //"time_of_booking" => $data["time_of_booking"],
                            //"duration" => $data["duration"],
                            //"name" => $data["name"],
                            //"phone" => $data["phone"],
                            //"email" => $data["email"],
                            //"cell" => $data["cell"],
                            //"address" => $data["address"],
                            //"city" => $data["city"],
                            //"state" => $data["state"],
                            //"zip" => $data["zip"],
                            //"language" => $data["language"],
                            //"reminder_time" => $data["reminder_time"],
                            //"time_zone" => $data["time_zone"],
                            //"coordx" => $data["coordx"],
                            //"coordy" => $data["coordy"],
                            "properties" => [
                                "property" => $propiedades
                            ]
                        ] */
                    ]
                ]
            ]
        ];
        
        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body
        ];
        //$response = $this->doAction('inbound_interface', $body);
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        $result=$response->data->data->commands->command->appointment->report->message;
        return $result;
    }
    
    
    public function cancelActivity($activityId,$codactu,$actividad)
    {
    //$actividad['XA_NOTE'] puede venir de EnvioMasivoOfsc.php
        $body = array(
            "head" => array(
                "processing_mode" => "appointment_only",
                "upload_type" => "incremental",
                "appointment" => array(
                    "keys" => array(
                        "field" => "appt_number"
                    ),
                    "action_if_completed" => "create"
                ),
                "inventory" => array(
                    "keys" => array(
                        "field" => "invsn"
                    ),
                    "upload_type" => "full"
                ),
                "properties_mode" => "update"
            ),
            "data" => array(
                "commands" => array(
                    "command" => array(
                        "type" => "cancel_activity",
                        "appointment" => array(
                            "appt_number" => $codactu, 
                            "properties" => array(
                                "property" => array(
                                    array(
                                        "label" => "XA_CANCEL_REASON",
                                        "value" => 'CC001'
                                    ),
                                    array(
                                        "label" => "XA_NOTE",
                                        "value" => $actividad['XA_NOTE'] 
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
        
        /*
        $requestArray = array_merge($this->getAuthArray(), $body);
        /*
        $response = $this->client->call('inbound_interface_request', $body);
         * 
         */
        /*return $requestArray;*/
        $elementos= [
            'action' => 'inbound_interface',
            'body' => $body
        ];
        //$response = $this->doAction('inbound_interface', $body);
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }
    
    
}
