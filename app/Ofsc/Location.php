<?php
namespace Ofsc;
use Ofsc\Ofsc;

/**
 * API Location
 */
class Location extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.location");
        $this->_tipo = \Config::get("ofsc.tipo.location");
        parent::__construct();
    }

    /**
     * Permite actualizar la ubicacion del tecnico
     *
     * @param String $company
     * @param String $device
     * @param Double $x
     * @param Double $y
     * @param Datetime $time
     * @return type
     */
    public function setPosition($device, $x, $y,$time)
    {
        $url ='';
        $body = [
            "company" => 'telefonica-pe',
            "device" => $device,
            "longitude" => $x,
            "latitude" => $y,
            "time" => $time

        ];
        $elementos= [
            'action' => 'set_position',
            'body' => $body,
            'url' => $url,
            'method' => '',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        return $response;
    }

    /**
     * Permite obtener una ubicacion de tecnico
     *
     * @param String $company
     * @param String $device
     * @return type
     */
    public function getPosition($device)
    {
        $url ='';
        $body = [
            "company" => 'telefonica-pe',
            "device" => $device
        ];
        $elementos= [
            'action' => 'get_position',
            'body' => $body,
            'url' => $url,
            'method' => '',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        return $response;
    }

     /**
     * Permite obtener los atributos adicionales
     *
     * @param String $company
     * @param String $device
     * @return type
     */
    public function getPositionAttr($company,$device)
    {
        $url ='';
        $body = [
            "company" => $company,
            "device" => $device
        ];
        $elementos= [
            'action' => 'get_position_attr',
            'body' => $body,
            'url' => $url,
            'method' => '',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        return $response;
    }

}
