<?php namespace Ofsc;

use Ofsc\Ofsc;
use Legados\helpers\ActivityHelper;

/**
 * API Activity
 */
class Activity extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.activity");
        $this->_tipo = \Config::get("ofsc.tipo.activity");
        parent::__construct();
    }

    /**
     * Permite cancelar una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function cancelActivity($activityId, $properties = null)
    {
        $url =sprintf(\Config::get("ofsc.rest.activity.cancel"), $activityId);
        $propiedades = [];
        if (is_array($properties)) {
            foreach ($properties as $key => $value) {
                $propiedades[]=['name'=>$key,'value'=>$value];
            }
        }
        $body = [
            "activity_id" => $activityId,
            "date" => "",
            "time" => ""
        ];
        if (count($propiedades) > 0) {
            $body["properties"] = $propiedades;
        }
        $elementos= [
            'action' => 'cancel_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'POST',
            'request' => json_encode($body),
        ];

        $response = $this->doAction($elementos);
        return $response;
    }
    /**
     * Permite suspender una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function suspendActivity($activityId, $codigomotivo)
    {
        $url =sprintf(\Config::get("ofsc.rest.activity.cancel"), $activityId);
        $body = [
            "activity_id" => $activityId,
            "date" => "",
            "time" => ""
        ];
        $elementos= [
            'action' => 'suspend_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'POST',
            'request' => json_encode($body),
        ];

        $response = $this->doAction($elementos);
        return $response;
    }

    /**
     * Permite iniciar una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function startActivity($activityId)
    {
        $url =sprintf(\Config::get("ofsc.rest.activity.start"), $activityId);
        $body = [
            "activity_id" => $activityId,
            "date" => "",
            "time" => ""
        ];
        $elementos= [
            'action' => 'start_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'POST',
            'request' => json_encode($body),
        ];
        $response = $this->doAction($elementos);
        return $response;
    }

    /**
     * Permite completar (liquidar) una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function completeActivity($activityId)
    {
        $url = sprintf(\Config::get("ofsc.rest.activity.start"), $activityId);
        $body = [
            "activity_id" => $activityId,
            "date" => "",
            "time" => ""
        ];
        $elementos= [
            'action' => 'complete_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'POST',
            'request' => json_encode($body),
        ];
        $response = $this->doAction($elementos);
        return $response;
    }
    /**
     * Permite obtener actividades en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function getActivitiesOld($resourceId, $dataFrom, $dateTo)
    {
        $url = '';
        $body = [
            "resources" => [
                "resource" => [
                    "resource_id" => $resourceId,
                    "include_children" => 'all'
                ]
            ],
            "date_range" => [
                "date_from" => $dataFrom,
                "date_to" => $dateTo,
                "include_unscheduled" => 'all'
            ],
            "select_count" => 50000,
            "filter_expression" => [
                'group_operator' => 'and',
                'group' => [
                    "filter_expression" => [
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_TRAINING'
                        ],
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_LUNCH'
                        ],
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_MEETING'
                        ],
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_MEDICAL'
                        ],
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_WAREHOUSE'
                        ],
                        [
                            "field" => "worktype",
                            "operator" => 'not_equals',
                            "value" => 'INT_VEHICLE'
                        ],
                        [
                            "field" => "status",
                            "operator" => 'not_equals',
                            "value" => 'deleted'
                        ],
                    ]
                ],
            ],
            "required_properties" =>
                [
                    'id','name','status','resource_id','appt_number', 'coordx',
                    'coordy', 'address','date','start_time','end_time',
                    'time_slot','time_of_booking',
                    'sla_window_start','sla_window_end',
                    'service_window_start','service_window_end', 'time_of_booking',
                    'time_slot', 'XA_APPOINTMENT_SCHEDULER', 'A_ASIGNACION_MANUAL_FLAG',
                    'XA_WORK_ZONE_KEY', 'A_CONTROL','XA_IDENTIFICADOR_ST'
                ]
        ];
        $elementos= [
            'action' => 'get_activities',
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => '',
        ];
        //soap:  action, body
        //rest:  url, method, request
        $response = $this->doAction($elementos);
        //$this->tracer($elementos, $response);
        $actividades=[];
        if (isset($response->data->activities->activity)) { //solo soap
            $actividad = [];
            $result = json_decode(json_encode($response->data->activities->activity), true);
            for ($i=0; $i <count($result); $i++) {
                if (isset($result[$i]['properties'])) {
                    $count = count($result[$i]['properties']);
                    for ($j=0; $j < $count; $j++) {
                        $name = $result[$i]['properties'][$j]['name'];
                        $value = $result[$i]['properties'][$j]['value'];
                        $actividad[$name] = $value;
                    }
                    $actividades[] = $actividad;
                }
            }
            $response->data=$actividades;
        }
        $response->data=$actividades;

        return $response;
    }



    public function getActivities($resourceId, $dataFrom, $dateTo, $fields = null)
    {

        if ( $fields == null ) {
            $fields = [
                'activityId',
                'customerName',
                'status',
                'resourceId',
                'apptNumber',
                'longitude',
                'latitude',
                'streetAddress',
                'date',
                'startTime',
                'endTime',
                'timeSlot',
                'timeOfBooking',
                'slaWindowStart',
                'slaWindowEnd',
                'serviceWindowStart',
                'serviceWindowEnd',
                'XA_APPOINTMENT_SCHEDULER',
                'A_ASIGNACION_MANUAL_FLAG',
                'XA_WORK_ZONE_KEY',
                'A_CONTROL',
                'XA_IDENTIFICADOR_ST'
            ];
        }
        
        $this->_tipo = "rest";

        $url = \Config::get("ofsc.rest.activity.get_activities") .
        "?dateFrom=" .$dataFrom .
        "&dateTo=" .$dateTo .
        "&resources=" .$resourceId .
        "&fields=".implode(',', $fields) .
        "&limit=50000" .
        "&includeChildren=all" .
        "&includeNonScheduled=true"  .
        "&q=not(XA_WORK_TYPE%20in%20['INT_TRAINING','INT_LUNCH','INT_MEETING','INT_MEDICAL','INT_WAREHOUSE','INT_VEHICLE'])%20and%20status!='deleted'";

        $elementos = [
            'action' => 'get_activities',
            'body' => [],
            'url' => $url,
            'method' => 'GET',
            'request' => json_encode([]),
        ];

        $response = $this->doAction($elementos);

        return ActivityHelper::formatGetActivitiesByRest($response, $fields);
    }

    function formatGetActivitiesByRest($response, $fields) {

        if( isset($response->data) ) {

            $equivalencias = [
                'activityId' => 'id',
                'customerName' => 'name',
                'resourceId' => 'resource_id',
                'apptNumber' => 'appt_number',
                'longitude' => 'coordx',
                'latitude' => 'coordy',
                'streetAddress' => 'address',
                'startTime' => 'start_time',
                'endTime' => 'end_time',
                'timeSlot' => 'time_slot',
                'timeOfBooking' => 'time_of_booking',
                'slaWindowStart' => 'sla_window_start',
                'slaWindowEnd' => 'sla_window_end',
                'serviceWindowStart' => 'service_window_start',
                'serviceWindowEnd' => 'service_window_end'
            ];

            foreach ($response->data as &$actividad) {

                $actividad = (array)$actividad;

                $new_actividad = [];

                foreach ($fields as $field) { 

                    if (!isset($actividad[$field]) ) {
                        if ($field == "date") {
                            $actividad[$field] = "3000-01-01";
                        } else if ($field == "longitude" || $field == "latitude") {
                            $actividad[$field] = '0.00000';
                        } else {
                            $actividad[$field] = "";
                        }
                    } else {
                        if ($field == "activityId" || $field == "longitude" || $field == "latitude") {
                            $actividad[$field] = (String)$actividad[$field];
                        }
                    }

                    if( isset( $equivalencias[$field] ) ) {
                        $new_actividad[ $equivalencias[$field] ] = $actividad[$field];
                    } else {
                        $new_actividad[ $field ] = $actividad[$field];
                    }
                }

                $actividad = $new_actividad;

            }

        }
        
        return $response;
    }

    /**
     * Permite obtener una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function getActivity($activityId)
    {
        $url = sprintf(\Config::get("ofsc.rest.activity.get"), $activityId);
        $body = [
            "activity_id" => $activityId,
        ];
        $elementos= [
            'action' => 'get_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => '',
        ];
        //soap:  action, body
        //rest:  url, method, request
        $response = $this->doAction($elementos);
        //$this->tracer($elementos, $response);
        $changed = ['activityId'=>'id','resourceId'=>'resource_id','data'=>'date_sub()','customerName'=>'name','streetAddress'=>'address'];
        $activity=[];

        if (isset($response->data->activity->properties)) { //solo soap
            $array = [];
            $result = json_decode(json_encode($response->data->activity->properties), true);
            for ($i=0; $i <count($result); $i++) {
                $array[$result[$i]['name']] = $result[$i]['value'];
            }
            foreach ($array as $key => $value) {
                if (array_key_exists($key, $changed)) {
                    $activity[$changed[$key]]=$value;
                } else {
                    $activity[$key]=$value;
                }
            }
            $response->data=$activity;
        }

        $response->data=$activity;

        return $response;
    }
    /**
     * Actualizar propiedades de una actividad en OFSC
     */
    public function updateActivity($activityId, $actividad)
    {
        $url = sprintf(\Config::get("ofsc.rest.activity.update"), $activityId);

        $propiedades=[];
        foreach ($actividad as $key => $value) {
            $propiedades[]=['name'=>$key,'value'=>$value];
        }
        $body = array(
            "activity_id" => $activityId,
            "position_in_route" => "0",
            "properties" => $propiedades,
        );
        $elementos= [
            'action' => 'update_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'PATCH',
            'request' => json_encode($body),
        ];
        $response = $this->doAction($elementos);
        if (isset($response->data->activity->properties)) {
            $array = [];
            $result = json_decode(json_encode($response->data->activity->properties), true);
            for ($i=0; $i <count($result); $i++) {
                $array[$result[$i]['name']] = $result[$i]['value'];
            }
            $response->data=$array;
        }
        //$this->tracer($elementos, $response);
        return $response;
    }
    /**
     * Permite buscar una actividad en OFSC
     *
     * @param Int $activityId AppointemntID
     * @return type
     */
    public function searchActivity($datos)
    {
        $setArray = array(
            "search_in" => $datos['search_in'],
            "search_for" => $datos['search_for'],
            "date_from" => $datos['date_from'],
            "date_to" => date('Y-m-d'),
            "select_from" => 1,
            "select_count" => 1,
            "property_filter" => $datos['property_filter']
        );

        $response = $this->doAction('search_activities', $setArray);


        if (isset($response->data->activity_list->total) &&
                  $response->data->activity_list->total>0 ) {
            $return=$response
                    ->data
                    ->activity_list
                    ->activities
                    ->activity
                    ->properties
                    ->value;
        } else {
            $return=0;
        }
        return $return;
    }

    /**
     * Retorna una imagen de una actividad
     *
     * @param type $aid AppointmentID
     * @param type $label Etiqueta de la imagen/archivo
     * @return type
     */
    public function getFile($aid, $label)
    {
        $url = sprintf(\Config::get("ofsc.rest.activity.getFile"), $aid, $label);
        $body = array(
            "entity_id" => $aid,
            "property_id" => $label
        );
        $elementos= [
            'action' => 'get_file',
            'body' => $body,
            'url' => $url,
            'method' => 'GET',
            'request' => '',
        ];
        $response = $this->doAction($elementos);
        //$this->tracer($elementos, $response);
        if (isset($response->data)) {
            return $response->data;
        }
        return [];
    }
}
