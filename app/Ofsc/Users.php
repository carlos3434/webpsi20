<?php namespace Ofsc;

use Ofsc\Ofsc;

class Users extends Ofsc
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("ofsc.wsdl.activity");
        $this->_tipo = "rest";
        parent::__construct();
    }

    public function updateUser($login, $password = "")
    {
        $url =sprintf(\Config::get("ofsc.rest.user.update"), $login);
        $body = [
            "password" => $password
        ];
        $elementos= [
            'action' => 'cancel_activity',
            'body' => $body,
            'url' => $url,
            'method' => 'PATCH',
            'request' => json_encode($body),
        ];

        $response = $this->doAction($elementos);
        return $response;
    }
}
