<?php

/**
 * es
 * 20160303
 */
return array(
    'admin' => 'Admin'
    , 'georeferencia' => 'Georeferencia'
    , 'alarmas' => 'Alarmas'
    , 'bitacora' => 'Bitacora'
    , 'no_carga' => 'Error obtener registros'
    , 'Close' => 'Cerrar'
    , 'Save' => 'Guardar'
    , 'Update' => 'Actualizar'
    , 'update_successful' => 'Actualizar (:action) registro satisfactorio'
    , 'update_error' => 'Error al actualizar registro'
    , 'error_select_db' => 'Error al obtener datos de la base de datos.'
    , 'error_operacion' => 'Error en la operación.'
    , 'no_edificio_alrededor' => 'No se encontraron edificios alrededor'
    , 'success_register' => 'Registro satisfactorio.'
    , 'success_update' => 'Actualización satisfactorio.'
    , 'success_delete' => 'Eliminación satisfactorio.'
    , 'error_gestion_movimiento' => 'Error al gestionar, verifique la información ingresada.'
    , 'success_gestion_movimiento' => 'Se gestiono correctamente'
    , 'esta_seguro?' => '¿Esta seguro de :accion?'
    , 'no_parametro' => 'Parámetros incompletos'
    , 'seleccionar' => '.:Seleccionar:.'
    , 'volver' => 'Volver'
    , 'eliminar' => 'Eliminar'
    , 'editar' => 'Editar'
    , 'eliminar id' => 'Eliminar :id'
    , 'editar id' => 'Editar :id'
    , 'agregar' => 'Agregar'
);
