<?php

/**
 * en
 */
return array(
    'admin' => 'Admin'
    , 'georeferencia' => 'Georeferencia'
    , 'alarmas' => 'Alarmas'
    , 'no_carga' => 'Error obtener registros'
    , 'Close' => 'Close'
    , 'Save' => 'Save'
    , 'Update' => 'Update'
    , 'update_successful' => '(:action) record update successful'
    , 'update_error' => 'Error record update'
    
);
