<?php

return array(
    'id' => 'ID'
    , 'servidor' => 'Servidor'
    , 'puerto' => 'Puerto'
    , 'url' => 'URL'
    , 'id_mensaje' => 'Mensaje Id'
    , 'id_empresa' => 'Empresa Id'
    , 'empresa' => 'Empresa'
    , 'envio' => 'Envio'
    , 'email' => 'E-mail'
    , 'asunto' => 'Asunto'
    , 'cuerpo' => 'Cuerpo'
    , 'zonal' => 'Zonal'
    , 'filtro' => 'Filtro(s):'
    , 'mostrar' => 'Mostrar'
    , 'table_accion' => '[]'
    , 'detalle' => 'Detalle'
    //
    , 'mostrar_alarma' => 'Mostrar alarma'
    , 'enviar' => 'Enviar'
    , 'actualizacion' => 'Actualización automática'
    , 'alarma_tap' => 'Alarmas Tap'
    , 'limpiar_mapa' => 'Limpiar mapa'
    , 'leyenda' => 'Leyenda'
);
