<?php

namespace OfscRest;

/**
 * Parent class of the configuration consume API CORE RESTFUL
 */
class Rest
{
    /**
     * Instance Auth
     * @var Object 
     */
    protected $_client;
    /**
     * error, info
     * @var String 
     */
    protected $_message = '[]';
    /**
     * BASIC or OAUTH2
     * @var String 
     */
    private $_type = 'BASIC';
    /**
     * Estructure request body
     * @var Array 
     */
    protected $_schema = '';
    /**
     * Trace partial include in "tracer function"
     * @var Array 
     */
    protected $_trace = '';
    /**
     * Convert response in object o json 
     * @var boolean 
     */
    public $toJson = true;
    
    const GET = "GET";
    const POST = "POST";
    const PUT = "PUT";
    const PATCH = "PATCH";
    const DELETE = "DELETE";

    public function __construct($type = '')
    {
        if (!empty($type)) {
            $this->_type = $type;
        }
        $objAF = new \OfscRest\AuthenticationFactory();
        $this->_client = $objAF->getAuthentication($this->_type);
    }

    public function setClient($type)
    {
        $objAF = new \OfscRest\AuthenticationFactory();
        $this->_client = $objAF->getAuthentication($type);
    }
    
    /**
     * Tracert I/O use API CORE function
     * @param String $url
     * @param String $type
     * @param String $action
     * @param Object $response
     * @param Array $requestArray Default empty array
     */
    public function tracer($url, $type, $action, $response, $requestArray = [])
    {
        $sAction = '';
        if (strlen($action) > 0) {
            $sAction = preg_split('/(?=[A-Z])/', $action); 
            $sAction = strtolower(implode('_', $sAction));
        }
        if (!is_array($requestArray)) {
            $requestArray = (array) $requestArray;
        }
        $iRequest['url'] = $url;
        $iRequest['http'] = $type;
        $iRequest['action'] = $sAction;
        $iRequest['aditional'] = $this->_trace;
        $request = array_merge($iRequest, $requestArray);
    }
    
    /**
     * 
     * @param Object $object type array object, if is NULL get Reference $this,
     * value default is NULL
     * @return array
     */
    public function buildSchemetoArray($object = null) 
    {
        try {
            $schema = is_null($object)? $this : $object;
            $array = [];
            foreach ($schema as $key => $val) {
                $array[$key] = $val;
            }
            unset(
                $array['_client'],$array['_message'], $array['toJson'], 
                $array['_schema'], $array['_type'], $array['_trace']
            );
            $this->_schema = $array;
            return $array;
        } catch(\Exception $e) {
            return 'EXCEPTION - ' . __FUNCTION__ . ' : '.$e->getMessage();
        }
    }
  
    /**
     * Return format response
     * @param mix $response
     * @return mix Object|JSon(String)
     */
    public function toJsonOrObject($response)
    {
        if ($this->toJson === true) {
            return $response;
        } else {
            return json_decode($response);
        }
    }
    
    /**
     * Testing connection to REST API CORE<br>
     * code:<br>
     * print_r((new OfscRest\Rest())->test())
     * 
     * @return JSon
     */
    public function test()
    {
        $urlData = \Config::get("ofsc.rest.test");
        $response = $this->_client->doActionCurl(Rest::POST, $urlData);
        $this->_trace = $this->_client->toString();
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response);
        return $response;
    }
    
    /**
     * If if empty, no have any error, otherwise return http error. 
     * Code 200 is OK, ignore
     * 
     * e.g. 
     * $resultCode = $object->getHttpResponseCode($response);<br>
     * @param Object $response
     * @return string
     */
    public function getHttpResponseCode($response)
    {
        $msg = '';
        if (is_object($response) && isset($response->status) 
            && $response->status != '' && intval($response->status) > 200) {
            $msg = '(' . $response->status . ') '
                . $response->title . ': ' . $response->detail;
        } else if (is_string($response) || is_int($response)) {
            $msg = 'var $response Is ' . gettype($response) 
                . ', It must be variable Object: "' . $response . '"';
        } else if (is_array($response)) {
            $msg = 'var $response Is Array, It must be variable Object';
        }
        return $msg;
    }
}
