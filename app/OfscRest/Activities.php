<?php

namespace OfscRest;

/**
 * API CORE for Activity
 * 
 * @property string $activityType required
 * @property string $apptNumber 
 * @property string $city required
 * @property string $coordStatus [ "found", "not_found", "invalid" ]
 * @property string $country_code
 * @property string $customerCell
 * @property string $customerEmail
 * @property string $customerName
 * @property string $customerNumber
 * @property string $customerPhone
 * @property string $date
 * @property string $deliveryWindowEnd
 * @property string $deliveryWindowStart
 * @property integer $duration Minimum Value: 0, Maximum Value: 65535
 * @property string $endTime "YYYY-MMDD HH:MM:SS" format
 * @property string $firstManualOperation
 * @property string $firstManualOperationUser
 * @property string $language required
 * @property number $latitude Minimum Value: -90, Maximum Value: 90
 * @property number $longitude Minimum Value: -180, Maximum Value: 180
 * @property integer $masterActivityId
 * @property string $multidayActivityStatus
 * @property string $multidayTimeToComplete
 * @property integer $points Minimum Value: 0, Maximum Value: 65535
 * @property integer $positionInRoute
 * @property string $postalCode
 * @property string $recordType [ "regular", "reopened", "prework", 
 * "multiday_activity", "multiday_activity_segment" ]
 * @property integer $reminderTime
 * @property string $resourceId required
 * @property string $serviceWindowEnd HH:MM:SS format
 * @property string $serviceWindowStart HH:MM:SS format
 * @property string $slaWindowEnd "YYYY-MM-DD HH:MM:SS" format
 * @property string $slaWindowStart "YYYY-MM-DD HH:MM:SS" format
 * @property string $startTime "YYYY-MM-DD HH:MM:SS" format
 * @property string $stateProvince
 * @property string $status [ "cancelled", "complete", "suspended", "started", 
 * "pending", "notdone" ]
 * @property string $streetAddress
 * @property string $teamResourceId
 * @property string $timeDeliveredEnd
 * @property string $timeDeliveredStart
 * @property string $timeOfAssignment
 * @property string $timeOfBooking
 * @property string $timeSlot
 * @property string $timeZone required
 * @property integer $travelTime
 * @property string $workZone
 * @property string $XA_WORK_TYPE
 * @property string $XA_NUMBER_SERVICE_ORDER
 * @property string $XA_APPOINTMENT_SCHEDULER
 * @property string $XA_COMMERCIAL_PACKET
 * @property string $XA_USER
 * @property string $XA_NOTE
 * @property string $XA_REQUIREMENT_TYPE
 * @property string $XA_COMPANY_NAME
 * @property string $XA_QUADRANT
 * @property string $XA_MDF
 * @property string $XA_DISTRICT_NAME
 * @property string $XA_QUADRANT      
 * @property string $XA_MDF         
 * @property string $XA_DISTRICT_NAME  
 * @property string $XA_SMS_1 
 * @property string $XA_DISTRICT_CODE
 * @property string $XA_CATV_SERVICE_CLASS 
 * @property string $XA_HFC_TAP_LINKHTTP
 * @property string $XA_HFC_BORNE     
 * @property string $XA_HFC_TAP     
 * @property string $XA_HFC_TROBA  
 * @property string $XA_HFC_NODE
 * @property string $XA_HFC_AMPLIFIER
 * @property string $XA_REQUIREMENT_REASON
 * @property string $XA_CREATION_DATE
 * @property string $XA_ORDER_AREA
 * @property string $XA_WORK_ZONE_KEY
 * @property string $XA_SOURCE_SYSTEM
 * @property string $XA_EQUIPMENT
 * @property string $XA_HFC_ZONE
 * @property string $XA_ADDRESS_LINK_HTTP
 * @property string $XA_REQUIREMENT_NUMBER
 * @property string $XA_WEB_UNIFICADA
 * @property string $XA_ACCESS_TECHNOLOGY
 * @property string $XA_BROADBAND_TECHNOLOGY
 * @property string $XA_CUSTOMER_SEGMENT
 * @property string $XA_ZONE
 * @property string $XA_TELEPHONE_TECHNOLOGY
 * @property string $XA_TV_TECHNOLOGY
 * @property string $XA_GRUPO_QUIEBRE
 * @property string $XA_BUSINESS_TYPE
 * @property string $XA_QUIEBRES
 * @property string $XA_ADSLSTB_PREFFIX
 * @property string $XA_ADSLSTB_MOVEMENT
 * @property string $XA_NUMERO_ENVIO
 * 
 */
class Activities extends Rest
{
    /**
     * Convert response in object o json 
     * @var boolean 
     */
    public $toJson = false;
    
    public function __construct($type = '')
    {
        parent::__construct($type);
        $this->_trace = $this->_client->toString();
    }
    /**
     * GET (PROBE)
     * 
     * @param Integer $activityId
     * @return Object Activity properties array
     */
    public function getActivity($activityId)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.get"), $activityId
        );
        $response = $this->_client->doActionCurl(Rest::GET, $urlData);
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $this->toJsonOrObject($response);
//        if ($this->toJson === true) {
//            return $response;
//        } else {
//            return json_decode($response);
//        }
    }

    /**
     * GET
     * 
     * LABEL: items, links
     * @param Integer $activityId
     * @return Object Activity properties array
     */
    public function getResourcePreferencesActivity($activityId)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.getResourcePreferences"), 
            $activityId
        );

        $response = $this->_client->doActionCurl(Rest::GET, $urlData);
        $this->tracer(
            $urlData, Rest::GET, __FUNCTION__, $response, 
            ['auth' => $this->_client->getAuthString()]
        );
        return $this->toJsonOrObject($response);
    }

    /**
     * GET
     * 
     * NOTE: (403:Forbidden)
     * @param Integer $activityId
     * @return Object 
     */
    public function getRequiredInventoriesActivity($activityId)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.getRequiredInventories"), 
            $activityId
        );

        $response = $this->_client->doActionCurl(Rest::GET, $urlData);
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $this->toJsonOrObject($response);
    }

    /**
     * POST (PROBE)
     * 
     * @param Array $aData if empty, 
     * values extract Instanceof OfscRest\Activities
     * @return Object
     */
    public function createActivity($aData = [])
    {
        $urlData = \Config::get("ofsc.rest.activity.create");
        //
        if (is_array($aData) && count($aData) == 0) {
            $aData = $this->buildSchemetoArray();
        }
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::POST, $urlData, $postData
        );
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response, $postData);
        return $this->toJsonOrObject($response);
    }

    /**
     * PATCH (PROBE)
     * 
     * :: e.g. (1) :: 
     *   $a = new OfscRest\Activities;
     *   $a->language = 'en';
     *   $a->timeZone =  "Peru";
     * 
     *   print_r($a->updateActivity('<activityId>',[]));
     * 
     * Action on past date is not allowed (400:Bad Request)
     * @param Integer $activityId
     * @param Array $aData if empty, values extract Instanceof OfscRest\Users
     * @return Object
     */
    public function updateActivity($activityId, $aData = [])
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.update"), 
            $activityId
        );
        //
        if (is_array($aData) && count($aData) == 0) {
            $aData = $this->buildSchemetoArray();
        }
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::PATCH, $urlData, $postData
        );
        $this->tracer(
            $urlData, Rest::PATCH, __FUNCTION__, $response, $postData
        );
        return $this->toJsonOrObject($response);
    }

    /**
     * POST
     * (405:Method Not Allowed)
     * 
     * Permite cancelar una actividad en OFSC
     * @param Integer $activityId
     * @param Array $aData array(String:time => current_time)
     * @return Object
     */
    public function cancelActivity($activityId, $aData = '')
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.cancel"), $activityId
        );
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::POST, $urlData, $postData
        );
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response, $postData);
        return $this->toJsonOrObject($response);
    }

    /**
     * POST
     * 
     * Permite iniciar una actividad en OFSC
     * @param Integer $activityId
     * @param Array $aData array(String:time => current_time)
     * @return Object
     */
    public function startActivity($activityId, $aData = '')
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.start"), $activityId
        );
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::POST, $urlData, $postData
        );
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response, $postData);
        return $this->toJsonOrObject($response);
    }

    /**
     * POST
     * 
     * Permite completar (liquidar) una actividad en OFSC
     * @param Integer $activityId
     * @param Array $aData array(String:time => current_time)
     * @return Object
     */
    public function completeActivity($activityId, $aData = '')
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.start"), $activityId
        );
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::POST, $urlData, $postData
        );
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response, $postData);
        return $this->toJsonOrObject($response);
    }

    /**
     * DELETE (PROBE)
     * 
     * Action on past date is not allowed (400:Bad Request)
     * @param Integer $activityId
     * @param Array $aData
     * @return Object
     */
    public function deleteActivity($activityId, $aData = '')
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.activity.delete"), $activityId
        );
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::DELETE, $urlData, $postData
        );
        $this->tracer(
            $urlData, Rest::DELETE, __FUNCTION__, $response, $postData
        );
        return $this->toJsonOrObject($response);
    }

    /**
     * DONT EXISTS!!!
     * 
     * @param Array $datos
     * @return type
     * @deprecated since version number
     */
    public function searchActivity($datos)
    {
        return $datos;
    }

    /**
     * DONT EXISTS!!!
     * 
     * @param type $aid AppointmentID
     * @param type $label Etiqueta de la imagen/archivo
     * @return type
     * @deprecated since version number
     */
    public function getFile($aid, $label)
    {
        return $aid . $label;
    }

}
