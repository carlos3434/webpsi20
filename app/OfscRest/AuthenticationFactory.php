<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OfscRest;

/**
 * Description of AuthenticationFactory
 *
 * @author Administrador
 */
class AuthenticationFactory
{

    public function getAuthentication($authentication)
    {
        if ($authentication == null) {
            return null;
        }

        if (strcmp($authentication, 'BASIC') === 0) {
            return new \OfscRest\AuthBasic();
        } else if (strcmp($authentication, 'OAUTH2') === 0) {
            return new \OfscRest\AuthOauthTwo();
        }

        return null;
    }

}
