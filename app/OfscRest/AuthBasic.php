<?php

/*
 * To Rest
 */

namespace OfscRest;

/**
 * Description of OfscRest
 *
 * @author JNt
 */
class AuthBasic implements Authentication
{
    /**
     * Convert string auth with base_64 encode
     * @return String
     */
    protected function getAuthBase64()
    {
        return base64_encode($this->getAuthString());
    }

    /**
     * Cadena de autenticación para OFSC API's
     * @return String
     */
    public function getAuthString()
    {
        $authString = \Config::get("ofsc.auth.login")
                . '@' . \Config::get("ofsc.auth.company")
                . ':' . \Config::get("ofsc.auth.pass");
        return $authString;
    }

    /**
     * Structure aditional for debug or testing operation
     * @return mix
     */
    public function toString()
    {
        $r['auth'] =  $this->getAuthString();
        $r['httpHeader'] =  Authentication::httpHeader;
        return $r;
    }

    /**
     * test:
     * curl_setopt($curl, CURLOPT_POST, true);
     * curl_setopt($curl, CURLOPT_HEADER, false);
     * curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, 1); default
     *
     * @param String $http
     * @param String $url
     * @param JSon $aData
     * @return Object
     */
    public function doActionCurl($http, $url, $aData = '')
    {
        $curl = curl_init($url);
        if ($curl === false) {
            return 'NOT INIT Curl';
        }
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->getAuthString());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [Authentication::httpHeader]);
        curl_setopt($curl, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
        curl_setopt($curl, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));

        if ($http != 'POST') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $http);
        }
        if (strlen($aData) > 2) {
            curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE); // --data-binary
            curl_setopt($curl, CURLOPT_POSTFIELDS, ($aData));
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); /* alternative */
        $curlResponse = curl_exec($curl);

        if ($curlResponse == false) {
            return curl_error($curl);
        } elseif (is_string($curlResponse)) {
            return strip_tags($curlResponse);
        }

        $response = json_decode($curlResponse);
        curl_close($curl);
        return $response;
    }

    /**
     *
     * @param String $url
     * @param JSon $aData
     * @return Object
     * @deprecated since version number
     */
    public function doActionPOST($url, $aData = '')
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->getAuthString());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']
        );

        if ($aData) {
            curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE); // --data-binary
            curl_setopt($curl, CURLOPT_POSTFIELDS, ($aData));
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curlResponse = curl_exec($curl);
        $response = json_decode($curlResponse);
        curl_close($curl);
        return $response;
    }

    /**
     *
     * @param String $url
     * @param JSon $aData
     * @return Object
     * @deprecated since version number
     */
    public function doActionPATCH($url, $aData)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->getAuthString());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']
        );

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE); // --data-binary
        curl_setopt($curl, CURLOPT_POSTFIELDS, ($aData));

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curlResponse = curl_exec($curl);
        $response = json_decode($curlResponse);
        curl_close($curl);
        return $response;
    }

}
