<?php

namespace OfscRest;


class Resources extends Rest
{
    public function getResources($limit = 0, $offset = 0)
    {
        if ($limit > 0 && is_int($limit) && is_int($offset)) {
            $urlData = sprintf(
                \Config::get("ofsc.rest.resource.getsLimit"),
                $limit, $offset
            );
        } else {
            $urlData = \Config::get("ofsc.rest.resource.gets");
        }
        $response = $this->_client->doActionCurl(
            Rest::GET, $urlData
        );
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $response;
    }
}