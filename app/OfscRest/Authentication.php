<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OfscRest;

/**
 *
 * @author Administrador
 */
interface Authentication
{

    const httpHeader = 'Content-Type: application/json';

    public function getAuthString();

    public function doActionCurl($http, $url, $aData = '');

    public function toString();
}
