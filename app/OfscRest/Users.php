<?php

namespace OfscRest;

/**
 * API Users CORE API
 *
 *
 * @property Object $avatar
 * @property string $blockedUntilTime e.g. too many failed login attempts
 * @property string $createdTime
 * @property string $dateFormat
 * @property string $language
 * @property string $lastLoginTime
 * @property string $lastPasswordChangeTime
 * @property string $login it has to be unique per instance
 * @property integer $loginAttempts
 * @property string $longDateFormat
 * @property string $mainResourceId
 * @property string $name
 * @property string $password
 * @property boolean $passwordTemporary
 * @property array  $resources
 * @property boolean $selfAssignment
 * @property string $status [ "active", "inactive" ]
 * @property string $timeFormat
 * @property string $timeZone
 * @property integer $timeZoneDiff
 * @property string $uphone
 * @property string $userType
 * @property string $weekStart [ "sunday", "monday", "tuesday", "wednesday",
 * "thursday", "friday", "saturday", "default" ]
 *
 * @author Administrador
 */
class Users extends Rest
{

    /**
     * Validate param into the function
     * @param String $login
     */
    public function validateIn($login)
    {
        try {
            if (!is_string($login)) {
                throw new \Exception('LOGIN, no es una STRING valido.');
            }
        } catch (\Exception $e) {
            $this->_message = 'Excepción: '.  $e->getMessage(). "\n";
            return true;
        }
        return false;
    }

    /**
     *
     * @param String $login
     * @param Array $aData
     * @return Object
     */
    public function addCollaborationGroups($login, $aData)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.addCollaborationGroups"),
            $login
        );
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::POST, $urlData, $postData
        );
        $this->tracer($urlData, Rest::POST, __FUNCTION__, $response, $postData);
        return $response;
    }

    /**
     *
     * :: e.g. (1) ::
     *   $user = new OfscRest\Users;
     *   $user->language = 'en';
     *   $user->name = 'Willy Jhon';
     *   $user->timeZone =  "Peru";
     *   $user->userType = 'Despachador';
     *   $user->login = 'willy.jhon';
     *   $user->password = 'apicoredemo';
     *   $user->resources = ['TDP'];
     *
     *   print_r($user->createUser('<login>',[]));
     *
     *
     * :: e.g. (2) ::
     *  $user = array(
     *      "name" => "Willy Jhon",
     *      "resources" => ['TDP'],
     *      "language" => "en",
     *      "timeZone" => "Peru",
     *      "userType" => "Despachador",
     *      "login" => "willy.jhon"
     *      "password" => "apicoredemo2016"
     *  );
     *
     * (new OfscRest\Users())->getUser('<login>', $user);
     *
     * @param String $login
     * @param Array $aData if empty, values extract Instanceof OfscRest\Users
     * @return Object
     */
    public function createUser($login, $aData = [])
    {
        if ($this->validateIn($login)) {
            return $this->_message;
        }
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.create"),
            $login
        );
        //
        if (is_array($aData) && count($aData) == 0) {
            $aData = $this->buildSchemetoArray();
        }
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::PUT, $urlData, $postData
        );
        $this->tracer($urlData, Rest::PUT, __FUNCTION__, $response, $postData);
        return $response;
    }

    /**
     *
     * @param String $login
     * @return Object
     */
    public function deleteCollaborationGroups($login)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.deleteCollaborationGroups"),
            $login
        );

        $response = $this->_client->doActionCurl(
            Rest::DELETE, $urlData
        );
        $this->tracer($urlData, Rest::DELETE, __FUNCTION__, $response);
        return $response;
    }

    /**
     *
     * @param String $login
     * @return Object
     */
    public function deleteUser($login)
    {
        if ($this->validateIn($login)) {
            return $this->_message;
        }
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.delete"),
            $login
        );

        $response = $this->_client->doActionCurl(
            Rest::DELETE, $urlData
        );
        $this->tracer($urlData, Rest::DELETE, __FUNCTION__, $response);
        return $response;
    }

    /**
     *
     * @param String $login
     * @return Object
     */
    public function getCollaborationGroups($login)
    {
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.getCollaborationGroups"),
            $login
        );

        $response = $this->_client->doActionCurl(
            Rest::GET, $urlData
        );
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $response;
    }

    /**
     *
     * @param String $login
     * @return Object
     */
    public function getUser($login)
    {
        if ($this->validateIn($login)) {
            return $this->_message;
        }
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.get"),
            $login
        );

        $response = $this->_client->doActionCurl(
            Rest::GET, $urlData
        );
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $response;
    }

    /**
     * For what Limit y offset works, both values must be.
     * @param integer $limit default is 0. Limit exists if is greater than 0
     * @param integer $offset default is 0. Offset exists if is greater than 0
     * @return Object
     */
    public function getUsers($limit = 0, $offset = 0)
    {
        if ($limit > 0 && is_int($limit) && is_int($offset)) {
            $urlData = sprintf(
                \Config::get("ofsc.rest.user.getsLimit"),
                $limit, $offset
            );
        } else {
            $urlData = \Config::get("ofsc.rest.user.gets");
        }

        $response = $this->_client->doActionCurl(
            Rest::GET, $urlData
        );
        $this->tracer($urlData, Rest::GET, __FUNCTION__, $response);
        return $response;
    }

    /**
     *
     * :: e.g. (1) ::
     *   $user = new OfscRest\Users;
     *   $user->language = 'en';
     *   $user->timeZone =  "Peru";
     *
     *   print_r($user->updateUser('<login>',[]));
     *
     * @param String $login
     * @param Array $aData if empty, values extract Instanceof OfscRest\Users
     * @return Object
     */
    public function updateUser($login, $aData = [])
    {
        if ($this->validateIn($login)) {
            return $this->_message;
        }
        $urlData = sprintf(
            \Config::get("ofsc.rest.user.update"),
            $login
        );
        //
        if (is_array($aData) && count($aData) == 0) {
            $aData = $this->buildSchemetoArray();
        }
        //
        $postData = json_encode($aData);

        $response = $this->_client->doActionCurl(
            Rest::PATCH, $urlData, $postData
        );
        $this->tracer(
            $urlData, Rest::PATCH, __FUNCTION__, $response, $postData
        );
        return $response;
    }

}
