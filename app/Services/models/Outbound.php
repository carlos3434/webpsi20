<?php
namespace Services\models;

use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaGestion;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as Detalle;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;


class Outbound
{
    private $_data;
    private $_body;
    private $_subject;
    private $_bucket;
    private $_resourceId;
    private $_aid;
    private $_codactu;
    private $_st;
    private $_numOrdenTrabajo;
    private $_numOrdenServicio;
    private $_ultimo;
    private $_respuesta;
    private $_inventario;
    private $_activateTime;
    private $_starttime;
    private $_endtime;
    private $_enddate;

    public function recibir($data)
    {
        $this->setData($data);
        $body = str_replace(["<![CDATA[","]]>"], "", $data['body']);
        if (\Helpers::isValidXml($body)) {
            $this->setBody(simplexml_load_string($body));
        } else {
            $this->setBody("");
        }
        $date = new \DateTime(date("Y-m-d"));
        $hoy = $date->format('Y-m-d');
        $this->setSubject(strtoupper(trim($data['subject'])));

        switch ($this->getSubject()) {
            case 'TOA_OUTBOUND_COMPLETED':
                if (isset($this->getBody()->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->appt_number));
                }
                if (isset($this->getBody()->external_id)) {
                    $this->setResourceId(trim($this->getBody()->external_id));
                }
                break;
            case 'TOA_OUTBOUND_NOTDONE':
                if (isset($this->getBody()->activity_properties->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->activity_properties->appt_number));
                }
                if (isset($this->getBody()->activity_properties->external_id)) {
                    $this->setResourceId(trim($this->getBody()->activity_properties->external_id));
                }
                break;
            case 'TOA_OUTBOUND_SUSPEND':
            case 'TOA_OUTBOUND_PRE_NOTDONE':
                if (isset($this->getBody()->activity_properties->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->activity_properties->appt_number));
                }
                if (isset($this->getBody()->activity_properties->external_id)) {
                    $this->setResourceId(trim($this->getBody()->activity_properties->external_id));
                }
                break;
            case 'UNSCHEDULE':
            case 'WO_INIT':
            case 'WO_CANCEL':
            case 'WO_PRE_COMPLETED':
            case 'UNSCHEDULE_START':
            case 'NOT_LEG':
            case 'PRUEBA':
            case 'TOA_OUTBOUND_AUT_SUSPEND':
            case 'TOA_OUTBOUND_AUT_COORDENADAS':
            case 'TOA_OUTBOUND_AUT_TEST_MODEM':
            case 'TOA_OUTBOUND_AUT_COMPLETED':
            case 'TOA_OUTBOUND_AUT_COMPLETED':
            case 'WO_PRE_CLOSEHOME':
                if (isset($this->getBody()->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->appt_number));
                }
                if (isset($this->getBody()->external_id)) {
                    $this->setResourceId(trim($this->getBody()->external_id));
                }
                break;
            case 'TOA_ACTIVATE_ROUTE':
                if (isset($this->getBody()->external_id)) {
                    $this->setResourceId(trim($this->getBody()->external_id));
                }
                if (isset($this->getBody()->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->appt_number));
                }

                if (isset($this->getBody()->r_activate_route_time)) {
                    $this->setActivateRouteTime(trim($this->getBody()->r_activate_route_time));
                }

                if (isset($this->getBody()->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->appt_number));
                }

                if (isset($this->getBody()->external_id)) {
                    $this->setBucket(trim($this->getBody()->external_id));
                }
                break;
            case 'INT_SMS':
                return;
                break;
            case 'TOA_VALIDATE_COORDS':
                if (isset($this->getBody()->validation_properties->aid)) {
                    $this->setAid(trim($this->getBody()->validation_properties->aid));
                }
                if (isset($this->getBody()->appt_number)) {
                    $this->setCodactu(trim($this->getBody()->appt_number));
                }
                if (isset($this->getBody()->external_id)) {
                    $this->setResourceId(trim($this->getBody()->external_id));
                }
                break;
            default:
                # code...
                break;
        }

        if (isset($this->getBody()->aid)) {
            $this->setAid(trim($this->getBody()->aid));
        }
        if (isset($this->getBody()->activity_properties->aid)) {
            $this->setAid(trim($this->getBody()->activity_properties->aid));
        }
        if (isset($this->getBody()->xa_identificador_st)) {
            $this->setSt(trim($this->getBody()->xa_identificador_st));
        }
        if (isset($this->getBody()->activity_properties->xa_identificador_st)) {
            $this->setSt(trim($this->getBody()->activity_properties->xa_identificador_st));
        }
        if ($this->getBody()->IDENTIFICADOR_ST) {
            $this->setSt(trim($this->getBody()->IDENTIFICADOR_ST));
        }
        if (isset($this->getBody()->XA_NUMBER_WORK_ORDER)) {
            $this->setNumOrdenTrabajo(trim($this->getBody()->XA_NUMBER_WORK_ORDER));
            if (is_null($this->getBody()->XA_NUMBER_WORK_ORDER) ||
                $this->getBody()->XA_NUMBER_WORK_ORDER == "") {
                    $this->setNumOrdenTrabajo("");
            }
        }
        if (isset($this->getBody()->activity_properties->XA_NUMBER_WORK_ORDER)) {
            $this->setNumOrdenTrabajo(trim($this->getBody()->activity_properties->XA_NUMBER_WORK_ORDER));
            if (is_null($this->getBody()->activity_properties->XA_NUMBER_WORK_ORDER) ||
                $this->getBody()->activity_properties->XA_NUMBER_WORK_ORDER == "") {
                $this->setNumOrdenTrabajo("");
            }
        }
        if (isset($this->getBody()->XA_NUMBER_SERVICE_ORDER)) {
            $this->setNumOrdenServicio(trim($this->getBody()->XA_NUMBER_SERVICE_ORDER));
            if (is_null($this->getBody()->XA_NUMBER_SERVICE_ORDER) ||
                $this->getBody()->XA_NUMBER_SERVICE_ORDER == "") {
                $this->setNumOrdenServicio("");
            }
        }
        if (isset($this->getBody()->activity_properties->XA_NUMBER_SERVICE_ORDER)) {
            $this->setNumOrdenServicio(trim($this->getBody()->activity_properties->XA_NUMBER_SERVICE_ORDER));
            if (is_null($this->getBody()->activity_properties->XA_NUMBER_SERVICE_ORDER) ||
                $this->getBody()->activity_properties->XA_NUMBER_SERVICE_ORDER == "") {
                $this->setNumOrdenServicio("");
            }
        }

        if (isset($this->getBody()->time_authorization)) {
            $this->setTimeAutorization(trim($this->getBody()->IDENTIFICADOR_ST));
        }
        if (isset($this->getBody()->user_name)) {
            $this->setUserAutorization($this->getBody()->user_name);
        }

        if (isset($this->getBody()->activity_properties)) {
            foreach ($this->getBody()->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $this->setRespuesta($respuesta);
        }
        if (isset($this->getBody()->resource_inventory)) {
            $inventario = [];
            foreach ($this->getBody()->resource_inventory->inventory as $key => $value) {
                $inventario[]=$value;
            }
            $this->setInventario($inventario);
        }
        $ultimo = $this->getUltimo();
        if (!is_null($ultimo)) {
            $this->setBucket($ultimo->bucket_id);
        }
    }

    public function procesar()
    {
    }
    public function getBody()
    {
        return $this->_body;
    }
    public function getSubject()
    {
        return $this->_subject;
    }
    public function getBucket()
    {
        return $this->_bucket;
    }
    public function getResourceId()
    {
        return $this->_resourceId;
    }
    public function getData()
    {
        return $this->data;
    }
    public function getAid()
    {
        return $this->_aid;
    }
    public function getSt()
    {
        return $this->_st;
    }
    public function getCodactu()
    {
        return $this->_codactu;
    }
    public function getNumOrdenTrabajo()
    {
        return $this->_numOrdenTrabajo;
    }
    public function getNumOrdenServicio()
    {
        return $this->_numOrdenServicio;
    }
    public function getRespuesta()
    {
        return $this->_respuesta;
    }
    public function getInventario()
    {
        return $this->_inventario;
    }
    public function getMotivo()
    {
        $respuesta = $this->getRespuesta();
        $where["estado"] = 1;
        if (isset($respuesta['estado_ofsc_id']) &&
            ($respuesta['estado_ofsc_id'] == 4 || $respuesta['estado_ofsc_id'] == 6)) {
            $where["actividad_id"] = $respuesta["actividad_id"];
        }
        $propiedades = \PropiedadOfsc::get(['tipo'=>'M','estado'=>1]);
        $motivo = new \stdClass;
        $motivo->id = null;
        $motivo->codigo_ofsc = null;
        $motivo->tipo_tratamiento_legado = null;
        $motivo->codigo_legado = '';
        $motivo->actividad_id = null;
        $motivo->estado_ofsc_id = null;

        //print_r($propiedades);


        //echo $respuesta['actividad_id']." \n";
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                echo "motivo: ".$propiedad->label." \n";
                $where["tipo_tecnologia"] = $propiedad->tipo_tecnologia;
                $where["codigo_ofsc"] = trim($respuesta[$propiedad->label]);
                $obj = \MotivoOfsc::get($where);
                if (count($obj) > 0) {
                    if (!is_null($obj[0])) {
                        $motivo = $obj[0];
                    }
                }
            }
        }

        return $motivo;
    }
    public function getSubmotivo($motivoId)
    {
        $respuesta = $this->getRespuesta();
        $subMotivo = new \stdClass;
        $subMotivo->id =null;
        $subMotivo->codigo_legado = '';

        $propiedades = \PropiedadOfsc::get(['tipo'=>'S','estado'=>1]);

        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                echo "subMotivo: ".$propiedad->label." \n";
                $rst = [];
                $rst = \SubmotivoOfsc::get([
                    'codigo_ofsc'=>trim($respuesta[$propiedad->label]),
                    'motivo_ofsc_id'=>$motivoId,
                ]);
                if (count($rst)>0) {
                    $subMotivo=$rst[0];
                }
            }
        }
        //dd();
        return $subMotivo;
    }
    public function getCarnetTmp()
    {
        return \Tecnico::where('carnet_tmp', $this->getResourceId())->first();
    }
    public function getEmpresa()
    {
        return \Tecnico::getEmpresa($this->getResourceId());
    }
    public function getActivateRouteTime()
    {
        return $this->_activateTime;
    }
    public function getStartTime()
    {
        return $this->_starttime;
    }
    public function getEndTime()
    {
        return $this->_endtime;
    }
    public function getEndDate()
    {
        return $this->_enddate;
    }
    public function setData($data)
    {
        $this->_data = $data;
    }
    public function setBody($body)
    {
        $this->_body = $body;
    }
    public function setRespuesta($respuesta)
    {
        $this->_respuesta = $respuesta;
    }
    public function setInventario($inventario)
    {
        $this->_inventario = $inventario;
    }
    public function setSubject($subject)
    {
        $this->_subject=$subject;
    }
    public function setCodactu($codactu)
    {
        $this->_codactu=$codactu;
    }
    public function setBucket($bucketId)
    {
        if (!is_null($bucketId) && $bucketId!="") {
            $bucket = \Bucket::find($bucketId);
            if (!is_null($bucket)) {
                $this->_bucket = $bucket->bucket_ofsc;
            } else {
                $this->_bucket = "";
            }
        } else {
            $this->_bucket = "";
        }
    }
    public function setResourceId($resourceId)
    {
        $this->_resourceId = $resourceId;
    }
    public function setAid($aid)
    {
        $this->_aid=$aid;
    }
    public function setSt($st)
    {
        $this->_st=$st;
    }
    public function setNumOrdenTrabajo($order)
    {
        $this->_numOrdenTrabajo = $order;
    }
    public function setNumOrdenServicio($servicio)
    {
        $this->_numOrdenServicio = $servicio;
    }
    public function setActivateRouteTime($time)
    {
        $this->_activateTime = $time;
    }
    public function setStartTime($starttime)
    {
        $this->_starttime = $starttime;
    }
    public function setEndTime($_endtime)
    {
        $this->_endtime = $endtime;
    }
    public function setEndDate($_enddate)
    {
        $this->_enddate = $enddate;
    }
    public function setTimeAutorization($time_authorization)
    {
        $this->timeAutorization = $time_authorization;
    }
    public function getTimeAutorization()
    {
        return $this->timeAutorization;
    }
    public function setUserAutorization($user_name)
    {
        $this->userAutorization = $user_name;
    }

    public function getUltimo()
    {
        $ultimo = Ultimo::where("num_requerimiento", $this->getCodactu())
            ->leftJoin(
                "solicitud_tecnica as st",
                "st.id",
                "=",
                "solicitud_tecnica_ultimo.solicitud_tecnica_id"
            );
        if (!is_null($this->getSt())) {
            $ultimo->where("st.id_solicitud_tecnica", "=", $this->getSt());
        }
        $ultimo->orderBy("solicitud_tecnica_ultimo.updated_at", "DESC");
        return $ultimo->first();
    }

    public function getSolicitudTecnica()
    {
        $solicitud = null;
        if ($this->getNumOrdenTrabajo() == "" && $this->getNumOrdenServicio() == "") {
            $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $this->getSt())
                            ->orderBy("updated_at", "DESC")->first();

        } else {


            $solicitud = SolicitudTecnica::select("solicitud_tecnica.*")
                            ->leftJoin(
                                "solicitud_tecnica_ultimo as stu",
                                "stu.solicitud_tecnica_id",
                                "=",
                                "solicitud_tecnica.id"
                            )
                            ->where("id_solicitud_tecnica", $this->getSt())
                            ->where("num_requerimiento", $this->getCodactu());

            if (!is_null($this->getNumOrdenTrabajo()) && $this->getNumOrdenTrabajo() !="") {
                $solicitud->where("stu.num_orden_trabajo", '=', $this->getNumOrdenTrabajo());
            }

            if (!is_null($this->getNumOrdenServicio()) && $this->getNumOrdenServicio() !="" && $this->getNumOrdenServicio() != 0) {
                $solicitud = $solicitud->where('stu.num_orden_servicio', '=', $this->getNumOrdenServicio());
            }

            $solicitud = $solicitud->where("stu.estado_st", "=", 1)
                ->orderBy("solicitud_tecnica.updated_at", "desc")
                ->first();
        }

        return $solicitud;
    }

    public function consultarCapacidad($actuacion, $date)
    {
        $bucket='';
        $actividadTipo= \ActividadTipo::find($actuacion['actividad_tipo_id']);
        list($nodoOrMdf, $troba) = explode("|", $actuacion['fftt']);
        $quiebre = $actuacion['quiebre'];
        $zonaTrabajo = $nodoOrMdf.'_'.$troba;
        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            ) {
            $rst = \Redis::get($date.'|AM|'.$actividadTipo->label.'|'.$quiebre.'|'.$zonaTrabajo);
            $capacidad = json_decode($rst, true);
            if (isset($capacidad['location'])) {
                $bucket = $capacidad['location'];
            }
        }
        return $bucket;
    }

    public function updateMateriales($inventario, $tecnico_id)
    {
    //en el mensaje no viene serie, debe acordarse luego si se agregará o no
        foreach ($inventario as $key => $value) {
            $material=Material::where('codmat', (string) $value->invtype)
                      ->first();
            if ($material) {
                $datos['material_id']=$material->id;
                $datos['tecnico_id']=$tecnico_id;
                $datos['opcion']=2;

                $tecmaterial=TecnicoMaterial:: where('material_id', $datos['material_id'])
                        ->where('tecnico_id', $datos['tecnico_id'])
                        ->where('estado', 1)
                        ->first();

                if ($tecmaterial) {
                    $datos['cantidad']=floatval($tecmaterial->cantidad)-floatval($value->quantity);
                    $tecmaterial->cantidad=$datos['cantidad'];
                    //$tecmaterial->save();

                    $movimientoObj= new TecnicoMaterialMovimiento($datos);
                    $tecmaterial->movimiento()->saveMany([$movimientoObj]);
                    $tecmaterial->save();
                }
            }

        }
    }
    
    public function getUserAutorization()
    {
        return $this->userAutorization;
    }

    
}