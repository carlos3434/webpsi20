<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as Detalle;
use Ofsc\Inbound;

class ValidarCoordenadas extends MensajeUno
{
    public function recibir($data)
    {
         parent::recibir($data);
    }

    public function procesar()
    {   
        $solicitudTecnica=SolicitudTecnicaCms::getUltimoCms()
                ->where('solicitud_tecnica_cms.num_requerimiento', $this->getCodactu())
                ->where('u.aid', $this->getAid())
                ->first();

        if (is_null($solicitudTecnica)) {
            return $actuacion['rst'] = 2;
        }

        $location=array('x'=>'','y'=>'');
        if (isset($this->getBody()->validation_properties->appt_coordinates->acoord_x)) {
            $this->acoord_x =
                trim($this->getBody()->validation_properties->appt_coordinates->acoord_x);
        }
        if (isset($this->getBody()->validation_properties->appt_coordinates->acoord_y)) {
            $this->acoord_y =
                trim($this->getBody()->validation_properties->appt_coordinates->acoord_y);
        }

        if (isset($this->getBody()->validation_properties->resource_coordinates)) {
            $Coordtecnico = $this->getBody()->validation_properties->resource_coordinates;
            $Coordtecnico = str_replace(["lat:", "lng:"], "", trim($Coordtecnico));
            $array= explode(",", $Coordtecnico);
            if (count($array)==2) {
                $location['x']=$array['1'];
                $location['y']=$array['0'];
            }
        }
        // solicitud();
        if ($solicitudTecnica->actividad_id==2) {// provision
            if ($location['x']!='' && $location['y']!='') {
                $solicitudTecnicaCms= SolicitudTecnicaCms::where('solicitud_tecnica_id', $solicitudTecnica->solicitud_tecnica_id)->first();
                $solicitudTecnicaCms['coordx_cliente'] = $location['x'];
                $solicitudTecnicaCms['coordy_cliente'] = $location['y'];
                $solicitudTecnicaCms->save();
    
                 $movimiento = Movimiento::where('solicitud_tecnica_id', $solicitudTecnica->solicitud_tecnica_id)
                ->latest()->first();
        
                \Auth::loginUsingId(952);
                if ($movimiento) {
                    $detalle = new Detalle();
                    $detalle['coordx_tecnico']        = $location['x'];
                    $detalle['coordy_tecnico']        = $location['y'];
                    $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                    $detalle['tipo']                  = 9; //guardar de cordenadas
                    $detalle['sms']                   = 'Coordenadas Actualizadas Correctamente';
                    $movimiento->detalle()->save($detalle);
                }
                \Auth::logout();
                return $actuacion['rst'] = 1;
            }
        } else {
            if (isset($this->acoord_x) && $this->acoord_x !='' && isset($this->acoord_y) && $this->acoord_y !='' && $location['x']!='' && $location['y']!='') {
                $data=[
                'coord_x1'=>$this->acoord_x, //coord de actividad
                'coord_y1'=>$this->acoord_y, //coord de actividad lat 1
                'coord_x2'=> $location['x'], // coord de tecnicos
                'coord_y2'=> $location['y'], //coord de tecnico lat 2
                'actividad_id'=> ($solicitudTecnica->actividad_id) //actividad_id
                ];
                
                $respuesta = \GeoValidacion::getValidadistancia($data);
                if ($respuesta['rpta']==1) {
                    //coordenadas validadas
                    $properties["A_ESTADO_VALIDACION_COORDENADAS"]='2';
                    $mensaje = "Req: ".$this->getCodactu()." : Coordenadas OK";
                } else {
                    //coordenadas invalidas
                    $properties["A_ESTADO_VALIDACION_COORDENADAS"]='1';
                    $mensaje = 'Req: '.$this->getCodactu().' : Coordenadas incorrectas. A '.$respuesta['metros'].' metros del Req';
                }
                
                $inbound = new Inbound();
                $inbound->updateActivity($this->getCodactu(), $properties);
    
                $tecnico = json_decode($this->getCarnetTmp(),false);
                if ($tecnico) {
                    \Sms::enviarSincrono($tecnico->celular, $mensaje, '1', '');
                }
                //insertar registro de transaccion
                $movimiento = Movimiento::where('aid', $this->getAid())->latest()->first();
                if ($movimiento) {
                    $detalle = new Detalle();
                    $detalle['coordx_tecnico']        = $data['coord_x2'];
                    $detalle['coordy_tecnico']        = $data['coord_y2'];
                    $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                    $detalle['tipo']                  = 1; // validacion de cordenadas
                    $detalle['distancia']             = $respuesta['metros'];
                    $detalle['sms']                   = $mensaje;
                    \Auth::loginUsingId(952);
                    $movimiento->detalle()->save($detalle);
                    \Auth::logout();
                }
                return $actuacion['rst'] = 1;
            }
        }

        return $actuacion['rst'] = 2;
    }
}