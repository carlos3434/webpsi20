<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class Sms extends Outbound
{
    private $_number;
    private $_text;
    public function recibir($data)
    {
        parent::recibir($data);
        if (isset($this->getBody()->number)) {
            $this->setNumber(trim($this->getBody()->number));
        }
        if (isset($this->getBody()->text)) {
            $this->setText($this->getBody()->text);
        }
    }
    public function getNumber()
    {
        return $this->_number;
    }
    public function getText()
    {
        return $this->_text;
    }
    public function setNumber($number)
    {
        $this->_number = $number;
    }
    public function setText($text)
    {
        $this->_text = $text;
    }
    public function procesar()
    {
        \Sms::enviar(
            $this->getNumber(),
            $this->getText()
        );
    }
}
