<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class ActivacionRuta extends MensajeUno
{
    public function recibir($data)
    {
         parent::recibir($data);
    }

    public function procesar()
    {   
        $tecnico =  $this->getResourceId();
        $fecha_activacion = $this->getActivateRouteTime();
        if ($tecnico && $fecha_activacion) {
            $obj = new \ActivacionRoute;
            $obj->fecha_activacion = $fecha_activacion;
            $obj->carnet_id = $tecnico;
            $obj->save();
        } else {
               
        }

        return $actuacion['rst'] = 1;
    }
}
