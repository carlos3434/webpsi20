<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;
use Legados\controllers\BandejaLegadoController;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;

class NoRealizada extends MensajeUno
{
    public function recibir($data)
    {
         parent::recibir($data);
    }

    public function procesar()
    {   
        $respuesta=[]; 
        if(isset($this->getBody()->activity_properties->property)){
            foreach ($this->getBody()->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label] = (string)$value->value;
            }
        }

        $inventario=[];
        if(isset($this->getBody()->resource_inventory->inventory)){
            foreach ($this->getBody()->resource_inventory->inventory as $key => $value) {
                $inventario[]=$value;
            }
        }
        
        //paso1
        $actuacion = $this->norealizadoOfsc($respuesta, $inventario);

        //paso2
        if ($actuacion['rst'] == 1) {
            if (isset($actuacion['automatico']) && $actuacion['automatico'] == 0
                || $actuacion['estado_aseguramiento'] == 7) {
                return;
            }
            if ((int) $actuacion['motivo_ofsc_id'] == 79 ||
                ((int)$actuacion['motivo_ofsc_id']== 454 &&
                (int) $actuacion['submotivo_ofsc_id']==1347 &&
                (int) $actuacion['actividad_id']==1)) {
                $condicion = [
                    'codactu' => $this->getCodactu(),
                    'estado'  => $actuacion['estado_ofsc_id'],
                    'razon'   => 'motivo_ofsc_id',
                    'valor'   => $actuacion['motivo_ofsc_id'],
                    'aid'     =>  $this->getAid(),
                    'st'      =>  $this->getSt()
                ];
                $visitas = Movimiento::getcontarvisita($condicion);

                switch ($visitas['tipo_envio_ofsc']) {
                    case '1'://agenda
                        $ultimo = $this->getUltimo();
                        $respuesta['estado_ofsc_id'] = 4;
                        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
                        $respuesta['actividad_id'] = $ultimo->actividad_id;
                        $objmotivoofsc = $this->getMotivo();
                        $inputs = [
                            'codactu' => $this->getCodactu(),
                            'comentario' => "",
                            'tipoDevolucion' => $objmotivoofsc->estado_ofsc,
                            'motivodevolucion' => "",
                            'motivoliquidacion' => "",
                            'submotivoDevolucion' => "",
                            'tematicos' => null,
                            'solicitud_tecnica_id' => $ultimo->solicitud_tecnica_id
                        ];

                        \Auth::loginUsingId(952);
                        $solicitudGestion = new StGestion;
                        $solicitudTecnicaId=$ultimo->solicitud_tecnica_id;
                        $response = $solicitudGestion->stopOrden($solicitudTecnicaId,$inputs);
                        \Auth::logout();
                        print_r($response);
                        break;
                    
                    case '2'://sla
                        if ($visitas['visitas'] < 3) {
                            $inputs['agdsla'] ='agendasla';
                            $inputs['hf'] = date("Y-m-d").'||'.$this->getResourceId().'||||';
                            $inputs['nenvio'] = $visitas['visitas'] + 1;
                            $inputs['quiebre'] = $visitas['quiebre_id'];
                            $inputs['solicitud'] = $visitas['id_solicitud_tecnica'];
                            $inputs['actividad_tipo_id'] = $visitas['actividad_tipo_id'];
                            $inputs['actividad_id'] = $visitas['actividad_id'];
                            $inputs['solicitud_tecnica_id'] = $actuacion['solicitud_tecnica_id'];
                            $inputs['XA_IND_NOT_TIDY'] = 1;
                            $inputs['estado_aseguramiento'] = 1;
                            
                            \Auth::loginUsingId(952);
                            $solicitudGestion = new StGestion;
                            $solicitudTecnicaId=$actuacion['solicitud_tecnica_id'];
                            $response = $solicitudGestion->envioOfsc($solicitudTecnicaId, $inputs);
                            \Auth::logout();
                            print_r($response);
                        } else {
                            if ($visitas['actividad_id'] == 2) {
                                $ultimo = $this->getUltimo();
                                $respuesta['estado_ofsc_id'] = 4;
                                $respuesta['tipo_legado'] = $ultimo->tipo_legado;
                                $respuesta['actividad_id'] = $ultimo->actividad_id;
                                $objmotivoofsc = $this->getMotivo();
                                $inputs = [
                                    'codactu' => $this->getCodactu(),
                                    'comentario' => "",
                                    'tipoDevolucion' => $objmotivoofsc->estado_ofsc,
                                    'motivodevolucion' => "",
                                    'motivoliquidacion' => "",
                                    'submotivoDevolucion' => "",
                                    'tematicos' => null,
                                    'solicitud_tecnica_id' => $ultimo->solicitud_tecnica_id
                                ];

                                \Auth::loginUsingId(952);
                                $solicitudGestion = new StGestion;
                                $solicitudTecnicaId=$ultimo->solicitud_tecnica_id;
                                $response = $solicitudGestion->stopOrden($solicitudTecnicaId,$inputs);
                                \Auth::logout();
                                print_r($response);
                            } else {
                                $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])
                                    ->first();

                                if (count($solicitudCms) > 0) {
                                    $solicitudCms["valor1"] = \Config::get("legado.area_transferencia");
                                    $solicitudCms["valor2"] = '77';
                                    $solicitudCms["valor3"] = '05';
                                    $solicitudCms->save();

                                    $solicitudCms->liquidacionCms();
                                    if ($actuacion["celular"] != "") {
                                        $sms = "Req. ".$this->getCodactu().", Cierre Aceptada, puede Continuar con la Siguiente Orden";
                                        \Sms::enviarSincrono($actuacion["celular"], $sms, '1');
                                    }
                                }
                            }
                        }
                        break;
                }

            } else {
                if ($actuacion['actividad_id'] ==1) {//solo averias
                    $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();
                    if (count($solicitudCms) > 0) {
                        $solicitudCms->devolucionCms();
                        if ($actuacion["celular"] != "") {
                            $sms = "Req. ".$this->getCodactu().", Devolución Aceptada, puede Continuar con la Siguiente Orden";
                            \Sms::enviarSincrono($actuacion["celular"], $sms, '1');
                        }
                    }
                } elseif ($actuacion['actividad_id'] == 2) {//solo provision
                    $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])
                                    ->first();
                    if (count($solicitudCms) > 0) {
                        $solicitudCms->devolucionCms();
                        if ($actuacion["celular"] != "") {
                            $sms = "Req. ".$this->getCodactu().", Devolución Aceptada, puede Continuar con la Siguiente Orden";
                            \Sms::enviarSincrono($actuacion["celular"], $sms, '1');
                        }
                    }
                }
            }   
        }

    }

    public function norealizadoOfsc($respuesta, $inventario = [])
    {
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }
        $respuesta['estado_ofsc_id'] = 4;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        $this->setRespuesta($respuesta);

        $tecnico = json_decode($this->getCarnetTmp(),false);
        $objmotivoofsc = $this->getMotivo();
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);

        $estado_aseguramiento = $ultimo->estado_aseguramiento == 7 ? 7 : 6;

        $actuacion = array(
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ? $respuesta['A_OBSERVATION'] : '',
            'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME']) ?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
            'motivo_ofsc_id'        => $objmotivoofsc->id,
            'submotivo_ofsc_id'     => $objsubmotivoofsc->id,
            'estado_ofsc_id'        => 4,
            'end_time'              => $this->getEndTime(),
            'end_date'              => $this->getEndDate(),
            'aid'                   => $this->getAid(),
            'estado_aseguramiento'  => $estado_aseguramiento,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";
            if(count($inventario)>0){
                $this->updateMateriales($inventario, $tecnico->id);
            }
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    if ($ultimo->actividad_id == 1) {
                        if ($objmotivoofsc->id != 454) { // diferente de OTROS
                            $where = [
                                'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id : null,
                                'actividad_id'          => $ultimo->actividad_id,
                                'tipo_legado'           => $ultimo->tipo_legado,
                                'area_transferencia'    => $objmotivoofsc->codigo_legado,
                                'estado'                => 1
                            ];
                            $transferencia = $this->getContrataTransferencia($where);
                            $solicitud = [
                                'codigo_contrata' => isset($transferencia->contrata_transferencia)? $transferencia->contrata_transferencia : "",
                                'valor1' => $objmotivoofsc->codigo_legado,
                            ];
                        }
                    } elseif ($ultimo->actividad_id == 2) {
                        $where = [
                            'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id: null,
                            'actividad_id'          => $ultimo->actividad_id,
                            'tipo_legado'           => $ultimo->tipo_legado,
                            'area_transferencia'    => $objmotivoofsc->codigo_legado,
                            'estado'                => 1
                        ];
                        $codigoContrata = "";
                        $descripcionContrata = "";
                        if (!is_null($tecnico)) {
                            $objempresa = \Empresa::find($tecnico->empresa_id);
                            if (!is_null($objempresa)) {
                                $codigoContrata = $objempresa->cms;
                                $descripcionContrata = $objempresa->nombre;
                            }
                        }
                        //$transferencia = $this->getContrataTransferencia($where);
                        $solicitud = [
                            'codigo_contrata' => $codigoContrata,
                            'desc_contrata' => $descripcionContrata,
                            'valor1' => $objmotivoofsc->codigo_legado,
                        ];
                    }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }
            // ActivityHelper::getImagenOfsc($ultimo->aid);
        }

        if ($ultimo->estado_aseguramiento != 5 && $ultimo->estado_aseguramiento != 6) {
            \Auth::loginUsingId(952);
            try {
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimo[$key] = $actuacion[$key];
                    }
                }
                $solicitudTecnica->ultimo()->save($ultimo);
                $objmovimientoreciente = $solicitudTecnica->ultimoMovimiento;
                if (!is_null($objmovimientoreciente)) {
                    $ultimoMovimiento = $objmovimientoreciente->replicate();
                    foreach (Movimiento::$rules as $key => $value) {
                        if (array_key_exists($key, $actuacion)) {
                            $ultimoMovimiento[$key] = $actuacion[$key];
                        }
                    }
                    $ultimoMovimiento->save();
                }

                switch ($ultimo->tipo_legado) {
                    case '1':
                        $solicitudTecnicaCms = $solicitudTecnica->cms;
                        foreach (SolicitudTecnicaCms::$rules as $key => $value) {
                            if (array_key_exists($key, $solicitud)) {
                                $solicitudTecnicaCms[$key] = $solicitud[$key];
                            }
                        }
                        $solicitudTecnica->cms()->save($solicitudTecnicaCms);
                        break;
                    case '2':
                        if ($ultimo->actividad_id == 1) {
                            $solicitudTecnicaGestelAveria = $solicitudTecnica->gestelAveria;
                            foreach (SolicitudTecnicaGestelAveria::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelAveria[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelAveria->save();
                            break;
                        } elseif ($ultimo->actividad_id == 2) {
                            $solicitudTecnicaGestelProvision = $solicitudTecnica->gestelProvision;
                            foreach (SolicitudTecnicaGestelProvision::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelProvision[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelProvision->save();
                            break;
                        }
                        break;
                }

                $actuacion['rst'] = 1;
            } catch (Exception $e) {
                $actuacion['rst'] = 0;
            }
            \Auth::logout();
        } else {
            $actuacion['rst'] = 1;
            $actuacion['automatico'] = 0;
        }
        
        return $actuacion;
    }
}
