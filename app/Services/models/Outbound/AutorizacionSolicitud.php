<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Legados\models\TipoAutorizacion as TipoAutorizacion;
use Legados\Repositories\StGestionRepository as StGestion;

class AutorizacionSolicitud extends Outbound
{
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        try {
            $tiposAutorizaciones = \Cache::get('tipo_autorizacion');
            $tiposAutorizaciones = \Cache::remember('tipo_autorizacion', 24*60*60, function () {
                return json_encode(TipoAutorizacion::lists('id', 'subject'));
            });

            $objgestion = new StGestion;
            $solicitudTecnica = $this->getSolicitudTecnica();
            $usuarioAutorizacion = $this->getUserAutorization();
            $tiempoAutorizacion = $this->getTimeAutorization();
            $tiposAutorizaciones = json_decode($tiposAutorizaciones, true);
            $tipoAutorizacion = $tiposAutorizaciones[$this->getSubject()];
            $inputs = [];
            $movimiento = array(
                        'tipo_autorizacion_id' => $tipoAutorizacion,
                        'usuario_autorizacion' => $usuarioAutorizacion,
                        'fecha_autorizacion' => $tiempoAutorizacion,
                        'subject' => $this->getSubject()
                    );
            
            $objgestion->registrarMovimientoDetalle($solicitudTecnica, $movimiento);
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }
}
