<?php
namespace Services\models\Outbound;

use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class Cancelar extends MensajeUno
{
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        $tecnico = $this->getCarnetTmp();
        $objmotivoofsc = $this->getMotivo();
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $actuacion = array(
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ?
                                       $respuesta['A_OBSERVATION'] : '',
            'motivo_ofsc_id'        => $this->getMotivo()->id,
            'estado_ofsc_id'        => 5,
            'estado_aseguramiento'  => ($ultimo->estado_aseguramiento == 7 || $ultimo->estado_aseguramiento == 12)? $ultimo->estado_aseguramiento : 1,
            'aid'                   => $this->getAid(),
            'resource_id'           => $this->getResourceId(),
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
        );
        
        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
        }

        \Auth::loginUsingId(952);
        try {
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);
            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $actuacion['rst'] = 0;
            $objerror = new Error;
            $objerror->saveError($e);
        }
        \Auth::logout();
        return $actuacion;
    }
}
