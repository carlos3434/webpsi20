<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\Repositories\StGenerarMovimientosRepository as StGenerarMovimientos;

class PreCasaCerrada extends MensajeUno  {
	public function recibir($data) {
		parent::recibir($data);
 
	}

	public function procesar() {
		$respuesta = $this->getRespuesta();
		$solicitudTecnica = $this->getSolicitudTecnica();
        

        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }

        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $respuesta['estado_ofsc_id'] = 6;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        //actializamos la respuesta
        $this->setRespuesta($respuesta);
        $tecnico =  $this->getCarnetTmp();
        $objMotivoOfsc = $this->getMotivo();
        $objSubmotivoOfsc = $this->getSubmotivo($objMotivoOfsc->id);
        //$actuacion = $this->setActuacion($respuesta, $tecnico);


        $actuacion = array(
            'a_receive_person_id'   => isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '',
            'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME'])?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ?
                                       $respuesta['A_CH_OBSERVATION'] : '',
            'estado_ofsc_id'        => 2, //se mantiene el estado iniciado
            'motivo_ofsc_id'        => $objMotivoOfsc->id,
            'submotivo_ofsc_id'     => $objSubmotivoOfsc->id,
            'end_time'              => $this->getEndTime(),
            'estado_aseguramiento'  => 4,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
            'tipo_liquidacion'      => 1
        );
        //$tecnico, $actuacion, 
        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    // if ($ultimo->actividad_id == 1) {
                    $solicitud = [
                        'valor1' => \Config::get("legado.area_transferencia"),
                        'valor2' => $objMotivoOfsc->codigo_legado,
                        'valor3' => $objSubmotivoOfsc->codigo_legado,
                    ];
                    $this->setAveriaComponentes($solicitud);
                    // } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    // }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }
        }

        \Auth::loginUsingId(952);
        try {

            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

            switch ($ultimo->tipo_legado) {
                case '1':
                    $objStcmsImpl = new \Legados\Repositories\SolicitudTecnicaCmsRepository;
                    $objStcmsImpl->updateCampos($solicitudTecnica, $solicitud);
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        $objStgestelAveImpl = new \Legados\Repositories\SolicitudTecnicaGestelAveriaRepository;
                        $objStgestelAveImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    } elseif ($ultimo->actividad_id == 2) {
                        $objStgestelProvImpl = new \Legados\Repositories\SolicitudTecnicaGestelProvisionRepository;
                        $objStgestelProvImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    }
                    break;
            }

            $actuacion['bucket_id'] = $ultimo->bucket_id;
            $actuacion['solicitud_tecnica_id'] = $ultimo->solicitud_tecnica_id;
            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $actuacion['rst'] = 0;
            $errorController = new \ErrorController();
            $errorController->saveError($e);
        }
        \Auth::logout();
        return $actuacion;
	}

    

}