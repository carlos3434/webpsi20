<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class AsignarTecnico extends Outbound
{
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        $tecnico =  $this->getCarnetTmp();
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return;
        }

        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0 || $ultimo->estado_ofsc_id <> 1) {
            return;
        }

        $actuacion["estado_ofsc_id"] = 1;
        $actuacion["estado_aseguramiento"] = 1;
        $actuacion["resource_id"] = $this->getResourceId();
        $actuacion["aid"] = $this->getAid();

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
        }
        if (isset($this->getBody()->delivery_window_start)) {
            $actuacion['delivery_window_start']=$this->getBody()->delivery_window_start;
        }
        if (isset($this->getBody()->delivery_window_end)) {
            $actuacion['delivery_window_end']=$this->getBody()->delivery_window_end;
        }
        if (isset($this->getBody()->date)) {
            $actuacion['fecha_agenda']=trim($this->getBody()->date);
        }
        if (isset($this->getBody()->time_slot)) {
            $actuacion['intervalo']=$this->getBody()->time_slot;
        }
        if (isset($this->getBody()->duration)) {
            $actuacion['duration']=$this->getBody()->duration;
        }
        if (isset($this->getBody()->time_of_assignment)) {
            $actuacion['time_of_assignment']=$this->getBody()->time_of_assignment;
        }
        if (isset($this->getBody()->time_of_booking)) {
            $actuacion['time_of_booking']=$this->getBody()->time_of_booking;
        }
        $actuacion["actividad_id"] = $ultimo->actividad_id;
        $actuacion["tipo_legado"] = $ultimo->tipo_legado;
        
        \Auth::loginUsingId(952);
        $objgestion = new StGestion;
        try {
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);
            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $objerror = new Error;
            $objerror->saveError($e);
        }
        \Auth::logout();
        return $actuacion;
    }
}
