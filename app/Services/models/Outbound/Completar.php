<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;
use Legados\controllers\BandejaLegadoController;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;

class Completar extends MensajeUno
{
    public function recibir($data)
    {
         parent::recibir($data);
    }

    public function procesar()
    {
        $respuesta=[];
        if (isset($this->getBody()->activity_properties->property)) {
            foreach ($this->getBody()->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label] = (string)$value->value;
            }
        }
        $this->setRespuesta($respuesta);

        $inventario=[];
        if (isset($this->getBody()->resource_inventory->inventory)) {
            foreach ($this->getBody()->resource_inventory->inventory as $key => $value) {
                $inventario[]=$value;
            }
        }
        $this->setInventario($inventario);

        //paso1
        $actuacion = $this->completarOfsc();
        //paso2
        if ($actuacion['rst'] == 1) {
            if (isset($actuacion['automatico']) && $actuacion['automatico'] == 0) {
                return;
            }

            if ($actuacion['submotivo_ofsc_id'] == '1071') { // submotivo (CMS, AVERIA)
                $condicion = [
                    'codactu' => $this->getCodactu(),
                    'estado'  => $actuacion['estado_ofsc_id'],
                    'razon'   => 'submotivo_ofsc_id',
                    'valor'   => '32',
                    'aid'     => $this->getAid(),
                    'st'      => $this->getSt()
                ];
                $visitas = Movimiento::getcontarvisita($condicion);
                
                if ($visitas['tipo_envio_ofsc'] == 2 && $visitas['visitas'] < 3) {
                    $inputs['agdsla'] = "sla";
                    $inputs['hf'] = date("Y-m-d") .'||'.$this->getResourceId().'||||';
                    $inputs['nenvio'] = $visitas['visitas'] + 1;
                    $inputs['quiebre'] = $visitas['quiebre_id'];
                    $inputs['solicitud'] = $visitas['id_solicitud_tecnica'];
                    $inputs['actividad_tipo_id'] = $visitas['actividad_tipo_id'];
                    $inputs['actividad_id'] = $visitas['actividad_id'];
                    $inputs['solicitud_tecnica_id'] = $actuacion['solicitud_tecnica_id'];

                    \Auth::loginUsingId(952);
                    $solicitudGestion = new StGestion;
                    $solicitudTecnicaId=$actuacion['solicitud_tecnica_id'];
                    $response = $solicitudGestion->envioOfsc($solicitudTecnicaId, $inputs);
                    \Auth::logout();
                    print_r($response);
                } else {
                    if ($actuacion['actividad_id'] == 1) {
                        $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();

                        if (count($solicitudCms) > 0) {
                            $solicitudCms->liquidacionCms();
                        }
                        
                        if ($actuacion["celular"] != "") {
                            $sms = "Req. ".$this->getCodactu().", Cierre Aceptada, puede Continuar con la Siguiente Orden";
                            \Sms::enviarSincrono($actuacion["celular"], $sms, '1');
                        }
                    }
                }
            } else {
                if ($actuacion['actividad_id'] == 1) {//solo averias
                    $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();

                    if (count($solicitudCms) > 0) {
                        $solicitudCms->liquidacionCms();
                    }
                    
                    if ($actuacion["celular"] != "") {
                        $sms = "Req. ".$this->getCodactu().", Cierre Aceptada, puede Continuar con la Siguiente Orden";
                        \Sms::enviarSincrono($actuacion["celular"], $sms, '1');
                    }
                } elseif ($actuacion['actividad_id'] == 2) {
                    $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();

                    if (count($solicitudCms) > 0) {
                        $solicitudCms->liquidacionCms();
                    }
                    
                    if ($actuacion["celular"] != "") {
                        $sms = "Req. ".$this->getCodactu().", Cierre Aceptada, puede Continuar con la Siguiente Orden";
                    }
                }
            }
        }

    }

        
    public function completarOfsc()
    {

        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }
        $respuesta = $this->getRespuesta();
        $inventario = $this->getInventario();
        $respuesta['estado_ofsc_id'] = 6;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        $this->setRespuesta($respuesta);
        
        $tecnico = json_decode($this->getCarnetTmp(), false);
        $objmotivoofsc = $this->getMotivo();
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);
        
        $actuacion = array(
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ?
                                       $respuesta['A_OBSERVATION']:'',
            'a_receive_person_id'   => isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '',
            'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME']) ?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
            'estado_ofsc_id'        => 6,
            'motivo_ofsc_id'        => $objmotivoofsc->id,
            'submotivo_ofsc_id'     => $objsubmotivoofsc->id,
            'end_time'              => $this->getEndTime(),
            'end_date'              => $this->getEndDate(),
            'estado_aseguramiento'  => 6,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";

            if (count($inventario)>0) {
                $this->updateMateriales($inventario, $tecnico->id);
            }
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    $solicitud = [
                        'valor1' => \Config::get("legado.area_transferencia"),
                        'valor2' => $objmotivoofsc->codigo_legado,
                        'valor3' => $objsubmotivoofsc->codigo_legado,
                    ];
                    $this->setAveriaComponentes($solicitud);
                
                    if ($ultimo->actividad_id == 2) {
                        if ($ultimo->estado_ofsc_id == 6) {
                            if ($ultimo->estado_aseguramiento == 5) {
                                $actuacion['rst'] = 1;
                                return $actuacion;
                            }
                        }
                    }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }
        }

        if ($ultimo->estado_aseguramiento != 6) {
            try {
                \Auth::loginUsingId(952);
                $objgestion = new StGestion;
                $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

                switch ($ultimo->tipo_legado) {
                    case '1':
                        $objStcmsImpl = new \Legados\Repositories\SolicitudTecnicaCmsRepository;
                        $objStcmsImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    case '2':
                        if ($ultimo->actividad_id == 1) {
                            $objStgestelAveImpl = new \Legados\Repositories\SolicitudTecnicaGestelAveriaRepository;
                            $objStgestelAveImpl->updateCampos($solicitudTecnica, $solicitud);
                            break;
                        } elseif ($ultimo->actividad_id == 2) {
                            $objStgestelProvImpl = new \Legados\Repositories\SolicitudTecnicaGestelProvisionRepository;
                            $objStgestelProvImpl->updateCampos($solicitudTecnica, $solicitud);
                            break;
                        }
                        break;
                }
                \Auth::logout();
                $actuacion['rst'] = 1;
            } catch (Exception $e) {
                $actuacion['rst'] = 0;
            }
        } else {
            $actuacion['rst'] = 1;
            $actuacion['automatico'] = 0;
        }
        return $actuacion;
    }
}
