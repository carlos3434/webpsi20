<?php
namespace Services\models\Outbound;

use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class Iniciar extends MensajeUno
{
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        $tecnico = $this->getCarnetTmp();
        $empresa = $this->getEmpresa();
        $objmotivoofsc = $this->getMotivo();
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $actuacion = array(
            'xa_observation'    => $this->getRespuesta()['A_COMMENT_TECHNICIAN'],
            'estado_ofsc_id'    => 2,
            'aid'               => $this->getAid(),
            'resource_id'       => $this->getResourceId(),
            'start_time'        => $this->getStartTime(),
            'tipo_legado'       => $ultimo->tipo_legado,
            'actividad_id'      => $ultimo->actividad_id,
            'estado_aseguramiento' => 1
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
        }

        $solicitud = [];
        if (count($empresa) > 0 && count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    $solicitud['codigo_contrata'] = $empresa->contrata_cms;
                    break;
                case '2':
                    $solicitud['contrata'] = $empresa->contrata_gestel;
                    break;
            }
        }

        \Auth::loginUsingId(952);
        try {
            $ultimo = $solicitudTecnica->ultimo;
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

            switch ($ultimo->tipo_legado) {
                case '1':
                    $objStcmsImpl = new \Legados\Repositories\SolicitudTecnicaCmsRepository;
                    $objStcmsImpl->updateCampos($solicitudTecnica, $solicitud);
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        $objStgestelAveImpl = new \Legados\Repositories\SolicitudTecnicaGestelAveriaRepository;
                        $objStgestelAveImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    } elseif ($ultimo->actividad_id == 2) {
                        $objStgestelProvImpl = new \Legados\Repositories\SolicitudTecnicaGestelProvisionRepository;
                        $objStgestelProvImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    }
                    break;
            }

            // ActivityHelper::getImagenOfsc($ultimo->aid);

            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $actuacion['rst'] = 0;
            $objerror = new Error;
            $objerror->saveError($e);
        }
        \Auth::logout();
        return $actuacion;
    }
}
