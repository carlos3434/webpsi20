<?php
namespace Services\models\Outbound;

use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;

class Suspender extends MensajeUno
{
    private $_startTime;
    private $_endTime;
    
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        $tecnico = $this->getCarnetTmp();
        $objmotivoofsc = $this->getMotivo($this->getRespuesta());
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $actuacion = array(
            'motivo_ofsc_id'   => $objmotivoofsc->id,
            'submotivo_ofsc_id'=> $objsubmotivoofsc->id,
            'end_time'         => $this->getEndTime(),
            'estado_ofsc_id'   => 3, //1 se agenda atuomaticamente en TOA
            'estado_aseguramiento' => 1,
            'resource_id'      => $this->getResourceId(),
            'tipo_legado'      => $ultimo->tipo_legado,
            'actividad_id'     => $ultimo->actividad_id,
            'tipo_envio_ofsc'  => $ultimo->tipo_envio_ofsc,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id,
            'xa_observation'   => isset($respuesta['A_OBSERVATION']) ?
                                    $respuesta['A_OBSERVATION'] : ''
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            //$this->updateMateriales($inventario, $tecnico->id);
        }

        \Auth::loginUsingId(952);
        try {
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

            $actuacion = [
                "estado_ofsc_id" => 1,
                "estado_aseguramiento" => 1
            ];
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $actuacion['rst'] = 0;
        }
        \Auth::logout();

        return $actuacion;

    }
}
