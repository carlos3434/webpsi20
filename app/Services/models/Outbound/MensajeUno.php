<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Legados\models\ComponenteOperacion as ComponenteOperacion;
use Legados\Repositories\StGestionRepository as StGestion;

class MensajeUno extends Outbound
{
    private $_startTime;
    private $_endTime;
    private $_endDate;
    
    public function recibir($data)
    {
        parent::recibir($data);
        if (isset($this->getBody()->activity_properties->ETA)) {
            $hoy = date("Y-m-d");
            $this->setStartTime($hoy.' '.trim($this->getBody()->activity_properties->ETA));
        } else if (isset($this->getBody()->eta_start_time)) {
            $hoy = date("Y-m-d");
            $this->setStartTime($hoy.' '.trim($this->getBody()->eta_start_time));
        }
        if (isset($this->getBody()->activity_properties->eta_end_time)) {
            $hoy = date("Y-m-d");
            $this->setEndTime($hoy.' '.trim($this->getBody()->activity_properties->eta_end_time));
        } else if (isset($this->getBody()->eta_end_time)) {
            $hoy = date("Y-m-d");
            $this->setEndTime($hoy.' '.trim($this->getBody()->eta_end_time));
        }
        if (isset($this->getBody()->date)) {
            $this->setEndDate($this->getBody()->date);
        } else if (isset($this->getBody()->activity_properties->date)) {
            $this->setEndDate($this->getBody()->activity_properties->date);
        }
    }
    public function getStartTime()
    {
        return $this->_startTime;
    }
    public function getEndTime()
    {
        return $this->_endTime;
    }
    public function getEndDate()
    {
        return $this->_endDate;
    }
    public function setStartTime($startTime)
    {
        $this->_startTime = $startTime;
    }
    public function setEndTime($endTime)
    {
        $this->_endTime = $endTime;
    }
    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
    }

    public function setAveriaComponentes($averias)
    {
        ComponenteOperacion::setAveriaComponente($averias, $this->getSt());
    }
    public function getContrataTransferencia($where)
    {
        return \ContrataTransferencia::where($where)->first();
    }
}
