<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Services\models\Outbound\MensajeUno as MensajeUno;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;
use Configuracion\models\ParametroMotivo;

class PreCompletar extends MensajeUno
{
    public function recibir($data)
    {
         parent::recibir($data);
    }

    public function procesar()
    {
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }
        $respuesta = $this->getRespuesta();
        $respuesta['estado_ofsc_id'] = 6;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        $tecnico =  $this->getCarnetTmp();
        $this->setRespuesta($respuesta);
        $objmotivoofsc = $this->getMotivo();
        $objsubmotivoofsc = $this->getSubmotivo($objmotivoofsc->id);

        $actuacion = array(
            'a_receive_person_id'   => isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '',
            'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME'])?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ?
                                       $respuesta['A_OBSERVATION'] : '',
            'estado_ofsc_id'        => 2, //se mantiene el estado iniciado
            'motivo_ofsc_id'        => $objmotivoofsc->id,
            'submotivo_ofsc_id'     => $objsubmotivoofsc->id,
            'end_time'              => $this->getEndTime(),
            'estado_aseguramiento'  => 4,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    // if ($ultimo->actividad_id == 1) {
                        $solicitud = [
                            'valor1' => \Config::get("legado.area_transferencia"),
                            'valor2' => $objmotivoofsc->codigo_legado,
                            'valor3' => $objsubmotivoofsc->codigo_legado,
                        ];
                        $this->setAveriaComponentes($solicitud);
                    // } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    // }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }
        }

        \Auth::loginUsingId(952);
        try {
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

            switch ($ultimo->tipo_legado) {
                case '1':
                    $objStcmsImpl = new \Legados\Repositories\SolicitudTecnicaCmsRepository;
                    $objStcmsImpl->updateCampos($solicitudTecnica, $solicitud);
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        $objStgestelAveImpl = new \Legados\Repositories\SolicitudTecnicaGestelAveriaRepository;
                        $objStgestelAveImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    } elseif ($ultimo->actividad_id == 2) {
                        $objStgestelProvImpl = new \Legados\Repositories\SolicitudTecnicaGestelProvisionRepository;
                        $objStgestelProvImpl->updateCampos($solicitudTecnica, $solicitud);
                        break;
                    }
                    break;
            }

            // ActivityHelper::getImagenOfsc($ultimo->aid);
            $actuacion['bucket_id'] = $ultimo->bucket_id;
            $actuacion['solicitud_tecnica_id'] = $ultimo->solicitud_tecnica_id;

            $actuacion['rst'] = 1;

        } catch (Exception $e) {
            $actuacion['rst'] = 0;
            $errorController = new Error;
            $errorController->saveError($e);
        }
        \Auth::logout();
        return $actuacion;
    }

    public function toolbox($actuacion = [])
    {
        $parametro = ParametroMotivo::where('tipo_legado', $actuacion['tipo_legado'])
                        ->where('actividad_id', $actuacion['actividad_id'])
                        ->where('bucket_id', $actuacion['bucket_id'])
                        ->where('estado', 1)
                        ->first();

        if (count($parametro) > 0) {
            $parametroDetalle = $parametro->parametroMotivoDetalle()
                ->where('motivo_ofsc_id', $actuacion['motivo_ofsc_id'])
                ->where('submotivo_ofsc_id', $actuacion['submotivo_ofsc_id'])
                ->first();

            if (isset($parametroDetalle->toolbox) && $parametroDetalle->toolbox == 1) {
                $solicitudTecnica = $this->getSolicitudTecnica();
                $solicitudTecnica->envioToolbox('PRE_LIQUIDADO');

                $ultimo = $this->getUltimo();
                $actuacion['sistema_externo_id'] = '1';
                $actuacion['xa_observation'] = $ultimo->xa_observation.' | '.'Enviado a Toolbox';

                $objgestion = new StGestion;
                $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);
            }
        }
    }
}
