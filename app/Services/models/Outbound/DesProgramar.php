<?php
namespace Services\models\Outbound;

use Services\models\Outbound as Outbound;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\helpers\ErrorHelper as Error;

class DesProgramar extends Outbound
{
    private $_startTime;
    private $_endTime;
    
    public function recibir($data)
    {
        parent::recibir($data);
    }
    public function procesar()
    {
        $tecnico =  $this->getCarnetTmp();
        $solicitudTecnica = $this->getSolicitudTecnica();
        $ultimo = $this->getUltimo();

        if (is_null($solicitudTecnica)) {
            return;
        }
        if (is_null($ultimo)) {
            return;
        }
        if ($ultimo->estado_ofsc_id!==1) {
            return;
        }

        $actuacion = array(
            'estado_ofsc_id'    => 7,
            'estado_aseguramiento' => 1,
            'resource_id'       => $this->getResourceId(),
            'tipo_legado'       => $ultimo->tipo_legado,
            'actividad_id'      => $ultimo->actividad_id
        );
        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
        }
        \Auth::loginUsingId(952);
        $objgestion = new StGestion;
        try {
            $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);
            $actuacion['actividad_id']= $ultimo->actividad_id;
            $actuacion['actividad_tipo_id']=$ultimo->actividad_tipo_id;
            $quiebre = \Quiebre::find($ultimo->quiebre_id);
            $actuacion['quiebre']=$quiebre->apocope; //quiebre apocope
            $actuacion['envio_ofsc']=$ultimo->tipo_envio_ofsc;
            $actuacion['nenvio']=1;

            if ($ultimo->tipo_legado==1) {//cms
                $detallesolicitud = $solicitudTecnica->cms;
                if ($detallesolicitud->cod_troba!='') {
                    $actuacion['fftt'] = $detallesolicitud->cod_nodo."|".$detallesolicitud->cod_troba;
                } else {
                    $actuacion['fftt'] = $detallesolicitud->cod_nodo."|".$detallesolicitud->cod_plano;
                }
                $actuacion['solicitud'] = $detallesolicitud->id_solicitud_tecnica;
            } else {
                $actuacion['fftt']=null;
            }
            if (trim($actuacion['envio_ofsc']) == '1') {
                return 1;
            }
            $date = date("Y-m-d", strtotime("+ 1 days"));
            $bucket = $this->consultarCapacidad($actuacion, $date);

            if ($bucket == '') {
                $bucket = $this->getBucket();
                if ($bucket == "") {
                    switch ($ultimo->tipo_legado) {
                        case 1:
                            $objcms = $solicitudTecnica->cms;
                            $bucket = $objcms->capacityOfsc();
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                }
            }
            //2-> sla, 1 ->agenda
            if (trim($ultimo->tipo_envio_ofsc) == '2') {
                $actuacion['agdsla'] = 'sla';
                $actuacion['hf'] = $date."||".$bucket."||AM|| - ";
            } else if (trim($actuacion['envio_ofsc'])=='1') {
                $actuacion['agdsla'] = 'agenda';
                $actuacion['hf'] = $date."||".$bucket."||AM||09-13";
            }
            if (is_null($ultimo->tipo_envio_ofsc)){
                $actuacion['agdsla'] = 'sla';
                $actuacion['hf'] = $date."||".$bucket."||AM|| - ";
            }
            $actuacion['quiebre'] = $ultimo->quiebre_id; //quiebre_id
            $actuacion['solicitud_tecnica_id'] = $ultimo->solicitud_tecnica_id;
            $response = $objgestion->envioOfsc($ultimo->solicitud_tecnica_id, $actuacion);
        } catch (Exception $e) {
            $objerror = new Error;
            $objerror->saveError($e);
        }
        \Auth::logout();
        return 1;
    }
}