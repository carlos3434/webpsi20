<?php
namespace Services\models;
use Config;
use Configuracion\models\ParametroMotivo;;

class Solicitud extends \Eloquent
{
	public static $rules_usuario = [
		'user' 					=> 'required',
		'company' 				=> 'required',
		'password' 				=> 'required',
		'key' 					=> 'required',
    ];

	public static $rules_cierre = [			
		'tipo_accion_respuesta' => 'required|in:STOP,RESET,CIERRE',
		'solicitud_tecnica' 	=> 'required|regex:/^([a-zA-Z]{1}[0-9]{10})/',
		'num_requerimiento' 	=> 'required',
		'tipo_motivo' 			=> 'required|in:T,C',
		'motivo_ofsc' 			=> 'required',
		'submotivo_ofsc' 		=> 'required',
		'motivo_ofsc_nuevo' 	=> 'required',
		'submotivo_ofsc_nuevo' 	=> '',
		'observacion_gestion' 	=> 'required',
		'tematico1' 			=> '',
		'tematico2' 			=> '',
		'tematico3' 			=> '',
		'tematico4' 			=> '',
		'telefono1_contacto' 	=> '',
		'telefono2_contacto' 	=> '',
		'persona_contacto' 		=> '',
		'direccion_contacto' 	=> '',
		'freeview' 				=> '',
		'descuento' 			=> '',
		'usuario_gestion' 		=> 'required',
		'dni_usuario_gestion' 	=> 'required|regex:/^([0-9]{8})/',
    ];

    public function getValidarUsuario($login) {
    	$user = Config::get('servicio.solicitud.user');
    	$company = Config::get('servicio.solicitud.company');
    	$password = Config::get('servicio.solicitud.password');
    	$diferenciaSegundos = (int) Config::get('servicio.solicitud.diferenciaSegundos');

    	$segPsi = strtotime(date("Y-m-d H:i:s"));
        $segExterno = strtotime($login->key);
        $diferencia = abs($segPsi-$segExterno);

        if ($diferencia > $diferenciaSegundos) {
            return "la diferencia de la hora del servidor y la ingresada exceden";
        }

    	if ($login->user != $user) {
    		return "usuario invalido";
    	}

    	if ($login->company != $company) {
    		return "company invalido";
    	}

    	if ($login->password != $password) {
    		return "password invalido";
    	}

    	return 1;
    }

    public function getParametrizacionMotivos($request, $type = 1) {
		$parametro = ParametroMotivo::where('tipo_legado', $request->tipo_legado)
                    ->where('actividad_id', $request->actividad_id)
                    ->where('bucket_id', $request->bucket_id)
                    ->where('estado', 1)
                    ->first();

        if (count($parametro) > 0) {
        	if ($type == 2) {
        		$submotivo_ofsc = isset($request->submotivo_ofsc_nuevo) && 
        							$request->submotivo_ofsc_nuevo == "" ? 
        							NULL : $request->submotivo_ofsc_nuevo;

        		$parametroDetalle = $parametro->parametroMotivoDetalle()
                                    ->select(
                                        'parametro_motivo_detalle.motivo_ofsc_id',
                                        'mo.id as motivo_ofsc_id',
                                        'mo.codigo_ofsc',
                                        'parametro_motivo_detalle.submotivo_ofsc_id',
                                        'parametro_motivo_detalle.accion_contador',
                                        'parametro_motivo_detalle.toolbox',
                                        'mo.tipo_tecnologia',
                                        'mo.estado_ofsc_id',
                                        'parametro_motivo_detalle.contador'
                                    )
        							->join('motivos_ofsc as mo', 'mo.id', '=', 'parametro_motivo_detalle.motivo_ofsc_id')
        							->leftJoin('submotivos_ofsc as sm', 'sm.id', '=', 'parametro_motivo_detalle.submotivo_ofsc_id')
	            					->where('parametro_motivo_id', $parametro['id'])
	                                ->where('mo.codigo_ofsc', $request->motivo_ofsc_nuevo)
	                                ->where('sm.codigo_ofsc', $submotivo_ofsc)
	                                ->first();

	            if (isset($parametroDetalle->toolbox) && $parametroDetalle->toolbox == 1) {
	                return $parametroDetalle;
	            } else {
	            	return false;
	            }
	        }

	        $parametroDetalle = $parametro->parametroMotivoDetalle()
                                ->select(
                                    'parametro_motivo_detalle.motivo_ofsc_id',
                                    'mo.id as motivo_ofsc_id',
                                    'mo.codigo_ofsc',
                                    'parametro_motivo_detalle.submotivo_ofsc_id',
                                    'parametro_motivo_detalle.accion_contador',
                                    'parametro_motivo_detalle.toolbox',
                                    'mo.tipo_tecnologia',
                                    'mo.estado_ofsc_id'
                                )
                                ->join('motivos_ofsc as mo', 'mo.id', '=', 'parametro_motivo_detalle.motivo_ofsc_id')
                                ->leftJoin('submotivos_ofsc as sm', 'sm.id', '=', 'parametro_motivo_detalle.submotivo_ofsc_id')
            					->where('parametro_motivo_id', $parametro['id'])
                                ->where('motivo_ofsc_id', $request->motivo_ofsc_id)
                                ->where('submotivo_ofsc_id', $request->submotivo_ofsc_id)
                                ->first();

            if (isset($parametroDetalle->toolbox) && $parametroDetalle->toolbox == 1) {
                return $parametroDetalle;
            } else {
            	return false;
            }
		} else {
			return false;
		}
    }
}