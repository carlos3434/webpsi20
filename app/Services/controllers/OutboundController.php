<?php
namespace Services\controllers;

use Services\models\Outbound;
use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;
use Legados\Repositories\StMensajeRepository as StMensaje;
use Log;

class OutboundController extends \BaseController 
{
    public function send_message ($request)
    {
        $outbound = new Outbound();
        $validacion="is not authorized";
        if (isset($request->user)) {
            $validacion = $outbound->validarUsuario($request->user);
            if ($validacion<>1) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
        } else {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $return = array();
        if (!isset($request->messages->message)) {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $message = $request->messages->message;
        //validar si es array u objeto
        //cuando llega un elemento llega como objecto
        if (is_object($message)) {
            $objstmensaje = new StMensaje;
            $return = $objstmensaje->sendingToa($message, $request, $return);
            return $return;
        }
        if (is_array($message)) {
            $mensajeexistente = null;
            for ($i=0; $i < count($message); $i++) {

                $messageId =  $message[$i]->message_id;
                $mensajeexistente = Mensaje::where("message_id", "=", $messageId)->first();

                $data = array(
                    'message_id' => $messageId,
                    'status' => 'sending',//sent failed delivered
                    //'description' => '',//everything is fine</
                    //'data' => '',
                    //'external_id' => '',
                    //'duration' => '',
                    //'sent' => '',
                    //'fault_attempt' => '',
                    //'stop_further_attempts' => '',
                    //'time_delivered_start' => '',
                    //'time_delivered_end' => ''
                );
                array_push($return, $data);
                Mensaje::crear(null, (array) $message[$i]);

                //print_r($request->messages->message->message_id);
                 //(4) REGISTRO TOA-PSI
                $transaccionesGoRepo = new TransaccionesGoRepo();

                $log_m_body = null;

                if ( isset( $message[$i]->body ) ) {

                    try {
                        $log_m_body = simplexml_load_string( preg_replace("[\n|\r|\n\r|\t]", "", $message[$i]->body) );
                    } catch (Exception $e) {
                        Log::useDailyFiles(storage_path().'/logs/flujo_4_2.log');
                        Log::info([$message[$i]->body]);
                    }

                    if ( $log_m_body !== null ) {

                        if( isset($log_m_body->xa_identificador_st) ) {

                            //$transaccionesGoRepo->registrarLogsFlujo($log_m_body->xa_identificador_st, 4, 2, null, $message[$i]->message_id);
                        }
                    }
                }

            }
            if (is_null($mensajeexistente)) {
                $push = Queue::push('NotificacionController', $message, 'outbound_notification');
            }
            return $return;
        }
    }
    
    public function drop_message ($request)
    {
        //se debe abortar el proceso (Pag 14 Outbound)
        $outbound = new Outbound();
        $validacion="is not authorized";
        if (isset($request->user)) {
            $validacion = $outbound->validarUsuario($request->user);
            if ($validacion<>1) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
        } else {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $return = array();

        if (!isset($request->messages->message)) {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $message = $request->messages->message;
        if (is_object($message)) {
            $message = array($message);
        }
        if (is_array($message)) {

            for ($i=0; $i < count($message); $i++) {
                $messageId =  $message[$i]->message_id;
                //borrar los mensajes que se envian
                try {
                    $respuesta = Mensaje::drop($messageId);
                } catch (Exception $e) {
                    $respuesta = array(
                        'code'=>"ERROR",
                        'desc'=>""//$e->getMessage()
                    );
                }
                $data = array(
                    'message_id' => $messageId,
                    'result' => $respuesta
                );
                array_push($return, $data);
            }
            return $return;
        }
    }
    
    public function get_message_status($request)
    {
        //
        $outbound = new Outbound();
        $validacion="is not authorized";
        if (isset($request->user)) {
            $validacion = $outbound->validarUsuario($request->user);
            if ($validacion<>1) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
        } else {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $return = array();
        if (!isset($request->messages->message)) {
            return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
        }
        $message = $request->messages->message;
        if (is_object($message)) {
            $message = array($message);
        }

        if (is_array($message)) {

            for ($i=0; $i < count($message); $i++) {
                $messageId =  $message[$i]->message_id;
                //borrar los mensajes que se envian
                try {
                    $respuesta = Mensaje::getStatus($messageId);
                } catch (Exception $e) {
                    $respuesta = array(
                        'code'=>"ERROR",
                        'desc'=>""//$e->getMessage()
                    );
                }
                $data = array(
                    'message_id' => $messageId,
                    'result' => $respuesta
                );
                array_push($return, $data);
            }
            return $return;
        }
    }
}