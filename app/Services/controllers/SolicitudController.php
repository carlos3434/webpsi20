<?php
namespace Services\controllers;

use Services\models\Solicitud;
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Log;
use Validator;
use Auth;
use Helpers;
use Legados\models\SolicitudTecnicaGestion as Gestion;
use Ofsc\Inbound;
use Toolbox\ToolboxApi;
use Legados\Repositories\StGestionRepository;

class SolicitudController extends \BaseController 
{
    public function cerrar ($request) {
        Log::useDailyFiles(storage_path().'/logs/services_solicitud.log');
        Log::info([$request]);

        $regexUsuario = Validator::make((array)$request->user, Solicitud::$rules_usuario);
        if ($regexUsuario->fails()) {
            return [
            	'error' => "0009",
            	'message' => $regexUsuario->messages()->all()[0],
            	'solicitud' => $request->solicitud_tecnica
            ];
        }

        $solicitud = new Solicitud();
        $validarUsuario = $solicitud->getValidarUsuario($request->user);
        if ($validarUsuario <> 1) {
        	return [
        		'error' => "0008",
            	'message' => $validarUsuario,
            	'solicitud' => $request->solicitud_tecnica
        	];
        }

        $regexCierre = Validator::make((array)$request, Solicitud::$rules_cierre);
        if ($regexCierre->fails()) {
            return [
            	'error' => "0007",
            	'message' => $regexCierre->messages()->all()[0],
            	'solicitud' => $request->solicitud_tecnica
            ];
        }

        $solicitudTecnica = SolicitudTecnica::select("solicitud_tecnica.*")
                            ->join(
                                "solicitud_tecnica_ultimo as stu",
                                "stu.solicitud_tecnica_id",
                                "=",
                                "solicitud_tecnica.id"
                            )
                            ->where("id_solicitud_tecnica", $request->solicitud_tecnica)
                            ->where("num_requerimiento", $request->num_requerimiento)
                            ->orderBy("solicitud_tecnica.updated_at", "desc")
                            ->first();
// 123456789a.
        if (!is_null($solicitudTecnica)) {
            $ultimo = $solicitudTecnica->ultimo;
            if (!is_null($ultimo)) {
                $bucket = $ultimo->bucket;
                $bucket_ofsc = isset($bucket->bucket_ofsc) ? $bucket->bucket_ofsc : "";
                $tecnico = $ultimo->tecnico;
                $celular = isset($tecnico->celular) ? $tecnico->celular : "";
                $date = date("Y-m-d");
                $request->tipo_legado = $ultimo->tipo_legado;
                $request->actividad_id = $ultimo->actividad_id;
                $request->bucket_id = $ultimo->bucket_id;
                $tematicos[0]["nombre"] = $request->tematico1;
                $tematicos[1]["nombre"] = $request->tematico2;
                $tematicos[2]["nombre"] = $request->tematico3;
                $tematicos[3]["nombre"] = $request->tematico4;
                $smsEstado = "";
                $parametrizacion = $solicitud->getParametrizacionMotivos($request, 2);
                if ($parametrizacion) {
                    $solicitudTecnicaGestion = new Gestion;
                    $datos = [
                        "solicitud_tecnica_id"  => $solicitudTecnica->id,
                        "codigo_ofsc"           => $parametrizacion->codigo_ofsc,
                        "motivo_ofsc_id"        => $parametrizacion->motivo_ofsc_id,
                        "submotivo_ofsc_id"     => $parametrizacion->submotivo_ofsc_id,
                        "tipo_tecnologia"       => $parametrizacion->tipo_tecnologia,
                        "telefono1_contacto"    => $request->telefono1_contacto,
                        "telefono2_contacto"    => $request->telefono2_contacto,
                        "persona_contacto"      => $request->persona_contacto,
                        "direccion_contacto"    => $request->direccion_contacto,
                        "freeview"              => $request->freeview,
                        "descuento"             => $request->descuento,
                        "tipo_motivo"           => $request->tipo_motivo,
                        "observacion_gestion"   => $request->tipo_accion_respuesta.' | '.$request->observacion_gestion,
                        "tematicos"             => $tematicos,
                    ];
                    switch ($request->tipo_accion_respuesta) {
                        case 'STOP':
                            if ($parametrizacion->accion_contador == 2) { // 3 vueltas
                                $condicion = [
                                    'codactu' => $ultimo->num_requerimiento,
                                    'estado'  => $ultimo->estado_ofsc_id,
                                    'razon'   => 'motivo_ofsc_id',
                                    'valor'   => $parametrizacion->motivo_ofsc_id,
                                    'aid'     => $ultimo->aid,
                                    'st'      => $request->solicitud_tecnica
                                ];
                                $visitas = Movimiento::getcontarvisita($condicion);
                                if ($visitas['visitas'] < $parametrizacion->contador) {
                                    $agenda['agdsla'] ='agendasla';
                                    $agenda['hf'] = $date .'||'. $bucket_ofsc .'||||';
                                    $agenda['nenvio'] = $visitas['visitas'] + 1;
                                    $agenda['quiebre'] = $visitas['quiebre_id'];
                                    $agenda['solicitud'] = $visitas['id_solicitud_tecnica'];
                                    $agenda['actividad_tipo_id'] = $visitas['actividad_tipo_id'];
                                    $agenda['actividad_id'] = $visitas['actividad_id'];
                                    $agenda['solicitud_tecnica_id'] = $ultimo->solicitud_tecnica_id;
                                    $agenda['XA_IND_NOT_TIDY'] = 1;
                                    $agenda['estado_aseguramiento'] = 1;
                                    $agenda['tematicos'] = $tematicos;
                                    Auth::loginUsingId(952);
                                    Helpers::ruta('bandejalegado/envioofsc', 'POST', $agenda, false);
                                    Auth::logout();
                                } else {
                                    if ($ultimo->tipo_legado == 1) {
                                        $cms = $solicitudTecnica->cms;
                                        if (count($cms) > 0) {
                                            if ($parametrizacion->estado_ofsc_id == 4) {
                                                $response = $solicitudTecnicaGestion->devolucionOfsc($datos);
                                                if ($response["rst"] == 1) {
                                                    $responseLegado = $cms->devolucionCms();
                                                    $elementos = [
                                                        'tipo_accion_respuesta' => "CIERRE LEGADO",
                                                        'solicitud_tecnica'     => $solicitudTecnica->id_solicitud_tecnica,
                                                        'error'                 => isset($responseLegado->error) ? 
                                                                                   $responseLegado->error : true,
                                                        'observacion'           => isset($responseLegado->data->descripcion) ? 
                                                                                   $responseLegado->data->descripcion : "",
                                                        'solicitud_tecnica_id'  => $solicitudTecnica->id
                                                    ];
                                                    $toolboxApi = new ToolboxApi();
                                                    $toolboxApi->cierre($elementos);
                                                }
                                            } elseif ($parametrizacion->estado_ofsc_id == 6) {
                                                $response = $solicitudTecnicaGestion->liquidacionOfsc($datos);
                                                if ($response["rst"] == 1) {
                                                    $responseLegado = $cms->liquidacionCms();
                                                    $elementos = [
                                                        'tipo_accion_respuesta' => "CIERRE LEGADO",
                                                        'solicitud_tecnica'     => $solicitudTecnica->id_solicitud_tecnica,
                                                        'error'                 => isset($responseLegado->error) ? 
                                                                                   $responseLegado->error : true,
                                                        'observacion'           => isset($responseLegado->data->descripcion) ? 
                                                                                   $responseLegado->data->descripcion : "",
                                                        'solicitud_tecnica_id'  => $solicitudTecnica->id
                                                    ];
                                                    $toolboxApi = new ToolboxApi();
                                                    $toolboxApi->cierre($elementos);
                                                }
                                            } else {
                                                $response["message"] = "Motivo no cuenta con estado en TOA";
                                                $response["error"] = "0006";
                                            }
                                        } else {
                                            $response["message"] = "Error al buscar la Solicitud Tecnica CMS";
                                            $response["error"] = "0005";
                                        }
                                    } else {
                                        # code GESTEL ...
                                    }
                                }
                            } else {
                                $inputs = [
                                    'comentario'            => $request->tipo_accion_respuesta.' | '.$request->observacion_gestion,
                                    'tipoDevolucion'        => $request->tipo_motivo,
                                    'tematicos'             => $tematicos,
                                    'motivodevolucion'      => $parametrizacion->motivo_ofsc_id,
                                    'motivoliquidacion'     => '',
                                    'submotivoDevolucion'   => $parametrizacion->submotivo_ofsc_id
                                ];
                                $solicitudGestion = new StGestionRepository();
                                $response = $solicitudGestion->stopOrden($solicitudTecnica->id, $inputs);
                            }
                            break;
                        case 'RESET':
                            $inputs = [
                                'comentario'            => $request->tipo_accion_respuesta.' | '.$request->observacion_gestion,
                                'tipoDevolucion'        => $request->tipo_motivo,
                                'tematicos'             => $tematicos
                            ];
                            $solicitudGestion = new StGestionRepository();
                            $response = $solicitudGestion->resetformulariotoa($solicitudTecnica->id, $inputs);
                            break;
                        case 'CIERRE':
                            if ($ultimo->tipo_legado == 1) {
                                $cms = $solicitudTecnica->cms;
                                if (count($cms) > 0) {
                                    if ($parametrizacion->estado_ofsc_id == 4) {
                                        $response = $solicitudTecnicaGestion->devolucionOfsc($datos);
                                        if ($response["rst"] == 1) {
                                            $responseLegado = $cms->devolucionCms();
                                            $elementos = [
                                                'tipo_accion_respuesta' => "CIERRE LEGADO",
                                                'solicitud_tecnica'     => $solicitudTecnica->id_solicitud_tecnica,
                                                'error'                 => isset($responseLegado->error) ? 
                                                                           $responseLegado->error : true,
                                                'observacion'           => isset($responseLegado->data->descripcion) ? 
                                                                           $responseLegado->data->descripcion : "",
                                                'solicitud_tecnica_id'  => $solicitudTecnica->id
                                            ];
                                            $toolboxApi = new ToolboxApi();
                                            $toolboxApi->cierre($elementos);
                                        }
                                    } elseif ($parametrizacion->estado_ofsc_id == 6) {
                                        $response = $solicitudTecnicaGestion->liquidacionOfsc($datos);
                                        if ($response["rst"] == 1) {
                                            $responseLegado = $cms->liquidacionCms();
                                            $elementos = [
                                                'tipo_accion_respuesta' => "CIERRE LEGADO",
                                                'solicitud_tecnica'     => $solicitudTecnica->id_solicitud_tecnica,
                                                'error'                 => isset($responseLegado->error) ? 
                                                                           $responseLegado->error : true,
                                                'observacion'           => isset($responseLegado->data->descripcion) ? 
                                                                           $responseLegado->data->descripcion : "",
                                                'solicitud_tecnica_id'  => $solicitudTecnica->id
                                            ];
                                            $toolboxApi = new ToolboxApi();
                                            $toolboxApi->cierre($elementos);
                                        }
                                    } else {
                                        $response["message"] = "Motivo no cuenta con estado en TOA";
                                        $response["error"] = "0006";
                                    }
                                } else {
                                    $response["message"] = "Error al buscar la Solicitud Tecnica CMS";
                                    $response["error"] = "0005";
                                }
                            } else {
                                # code GESTEL ...
                            }
                            break;
                        default:
                            $response["message"] = "Error tipo de accion respuesta no existe";
                            $response["error"] = "0004";
                            break;
                    }
                } else {
                    $response["message"] = "Error motivos y submotivos OFSC no cumplen la parametrización";
                    $response["error"] = "0003";
                }
            } else {
                $response["message"] = "Error al buscar la Ultima Gestion de la Solicitud Tecnica";
                $response["error"] = "0002";
            }
        } else {
            $response["message"] = "Error al buscar la Solicitud Tecnica";
            $response["error"] = "0001";
        }

        unset($response["rst"]);
        $response["solicitud"] = $request->solicitud_tecnica;
        $response["message"] = isset($response["msj"]) ? $response["msj"] : $response["message"];
        return $response;
    }
}