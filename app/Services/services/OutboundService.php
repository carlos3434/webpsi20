<?php
namespace Services\services;
use Config;
use SoapServer;

class OutboundService extends \BaseController {
    public function getServer()
    {
        try {
            $options = ['cache_wsdl' => WSDL_CACHE_NONE];
            $url = Config::get('ofsc.outbound.wsdl');
            $server = new SoapServer($url, $options);
            $server->setClass('Services\controllers\OutboundController');
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }

        /*try {
            $server = new SoapServer(Config::get('ofsc.outbound.wsdl'));
            $server->addFunction("send_message");
            $server->addFunction("drop_message");
            $server->addFunction("get_message_status");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }*/
    }
}