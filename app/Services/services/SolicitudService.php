<?php
namespace Services\services;
use Config;
use SoapServer;

class SolicitudService extends \BaseController {
    public function getServer()
    {
        try {
            $options = ['cache_wsdl' => WSDL_CACHE_NONE];
            $url = Config::get('servicio.solicitud.wsdl');
            $server = new SoapServer($url, $options);
            $server->setClass('Services\controllers\SolicitudController');
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}