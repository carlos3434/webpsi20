<?php
namespace Configuracion\controllers;

use Configuracion\models\ParametroMotivoDetalle;
class ParamMotivoDetalleController extends \BaseController
{
    protected $_paraMotivoDetalle;

    public function __construct(ParametroMotivoDetalle $paraMotivoDetalle) {
        $this->_paraMotivoDetalle = $paraMotivoDetalle;
    }
    
    public function postCargar()
    {        
        if (\Request::ajax()) {
            $inputs = \Input::all();
            $result = $this->_criterio->listar($inputs);
            return \Response::json(array('rst' => 1, 'datos' => $result));
        }
    }

    public function postUpdate()
    {
        $this->_paraMotivoDetalle->setData(\Input::all());
        $result = $this->_paraMotivoDetalle->actualizar();
        return ($result) ? ['rst' => 1] : ['rst' => 0];
    }


}
