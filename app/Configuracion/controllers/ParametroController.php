<?php
namespace Configuracion\controllers;

use Configuracion\models\Parametro as Parametro;
use Configuracion\models\ParametroCriterio;
use Configuracion\models\ParametroMotivo;
use Configuracion\models\ParametroMotivoDetalle;

class ParametroController extends \BaseController
{
    protected $_parametro;

    public function __construct(Parametro $parametro) {
        $this->_parametro = $parametro;
    }

    public function postSave()
    {
        if (\Request::ajax()) {
            
            $inputs = \Input::all();
            $this->_parametro->setData($inputs);
            $this->_parametro->action();

            $msj='Registro realizado correctamente';
            $rst= 1;

            if($this->_parametro->getTipo() == 4 && $this->_parametro->getEstado() == 1){
                $result = $this->_parametro->getFiltrarST();
                if ($result["rst"] == 2) {
                    $rst = 1;
                    $msj='¡ Esta Masiva esta en proceso !';
                }
                if ($result["rst"] == 3) {
                    $rst = 1;
                    $msj='¡ No se puede procesar esta Masiva !';
                }
                if ($result["rst"] == 1) {
                    $envio = $result["data"];
                    $msj = $result["msj"];
                    \Queue::push('ProcesoMasiva', $envio, 'procesomasiva');
                }
            }

            if($this->_parametro->getTipo() == 4 && $this->_parametro->getEstado() == 0){
                $result= \Masivast::where('parametro_id', $parametros->id)->delete();
                $msj = "Masiva Inactivada, Cancelacion Masiva Detenida";
            }

            \Cache::forget("listParametrizacionCms");
            \Cache::forget("listParametrizacionCmsMasiva");
            \Cache::forget("listParametroCms");
            \Cache::forget('listParametroCmsMasiva');

            return \Response::json(array(
                'rst' => $rst,
                'msj' => $msj
            ));
        }
    }

    public function postListar()
    {
        if (\Request::ajax()) {
            $inputs = \Input::all();

            if (isset($inputs['id'])) {
                $parametros =  $this->_parametro->getBuscar($inputs);
            } else {
                $parametros =  $this->_parametro->getAllParameters($inputs);
            }

            return \Response::json(
                array(
                    'rst' => 1,
                    'datos' => $parametros,
                )
            );
        }
    }

    public function postBucket()
    {
        if (\Request::ajax()) {
            $buckets = \OfscHelper::getFileResource();
            $array_bk=array();
            $i=0;
            foreach ($buckets as $key => $value) {
                 $array_bk[$i]['id']= $value['id'];
                 $array_bk[$i]['nombre']= $value['name'];
                 $i++;
            }

            return \Response::json(
                array(
                'rst' => 1,
                'datos' => $array_bk,
                )
            );
        }
    }
}
