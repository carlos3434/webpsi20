<?php
namespace Configuracion\controllers;

use Configuracion\models\ParametroMotivo;
use Configuracion\models\ParametroMotivoDetalle;

class ParametroMotivoController extends \BaseController
{
    protected $_paramMotivo;
    protected $_paramMotivoDetalle;

    public function __construct(ParametroMotivo $paramMotivo,
        ParametroMotivoDetalle $paramMotivoDetalle) {
        $this->_paramMotivo = $paramMotivo;
        $this->_paramMotivoDetalle = $paramMotivoDetalle;
    }
    
    public function postList()
    {
        return [
            'datos' => $this->_paramMotivo->listar()
        ];
    }

    public function postBuscar()
    {
        $this->_paramMotivo->setId(\Input::get('id'));

        return [
            'datos' => $this->_paramMotivo->cargarDetalle()
        ];
    }

    public function postInsert()
    {
        $this->_paramMotivo->setData(\Input::all());
        $result = $this->_paramMotivo->actualizar();
        return ($result) ? ['rst' => 1] : ['rst' => 0];

        return [
            'rst' => 1
        ];
    }

    public function postAction()
    {   
        $this->_paramMotivo->setData(\Input::all());
        $result = $this->_paramMotivo->action();

        if($result && count($this->_paramMotivo->getDetalle()) > 0){         
            foreach ($this->_paramMotivo->getDetalle() as $key => $value) {
                $value['parametro_motivo_id'] = $this->_paramMotivo->getId();
                $this->_paramMotivoDetalle->setData($value);
                $this->_paramMotivoDetalle->guardar();                
            }
        }

        return ($result) ? ['rst' => 1] : ['rst' => 0];
    }
}

