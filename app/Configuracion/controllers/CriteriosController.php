<?php
namespace Configuracion\controllers;

use Configuracion\models\Criterios;
class CriteriosController extends \BaseController
{
    protected $_criterio;

    public function __construct(Criterios $criterio) {
        $this->_criterio = $criterio;
    }
    
    public function postCargar()
    {
        if (\Request::ajax()) {
            $inputs = \Input::all();
            $result = $this->_criterio->listar($inputs);
            return \Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
