<?php
namespace Configuracion\models;
class ParametroMotivoDetalle extends \Eloquent
{
	protected $guarded =[];
	public $table = "parametro_motivo_detalle";

	private $id;
    private $parametro_motivo_id;
    private $motivo_ofsc_id;
    private $submotivo_ofsc_id;
    private $contador;
    private $accion_contador;
    private $toolbox;
    private $estado;

	public static function boot()
   	{
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

    public function setData($data) {
        if(isset($data['id'])){
            $this->setId($data['id']);
        }

        if(isset($data['parametro_motivo_id'])){
            $this->setParamMotivoId($data['parametro_motivo_id']);
        }        

        if(isset($data['motivo_ofsc_id'])){
            $this->setMotivoOfscId($data['motivo_ofsc_id']);
        }

        if(isset($data['submotivo_ofsc_id'])){
            $this->setSubmotivoOfscId($data['submotivo_ofsc_id']);           
        }

        if(isset($data['contador'])){
            $this->setContador($data['contador']);
        }

        if(isset($data['accion_contador'])){
            $this->setAccionContador($data['accion_contador']); 
        }

        if(isset($data['toolbox'])){
            $this->setToolbox($data['toolbox']);
        }

        if(isset($data['estado'])){
            $this->setEstado($data['estado']);
        }
    }

    public function getData(){
        $data = array(
            'id' => $this->getId(),
            'parametro_motivo_id' => $this->getParamMotivoId(),
            'motivo_ofsc_id' => $this->getMotivoOfscId(),
            'submotivo_ofsc_id' => $this->getSubmotivoOfscId(),
            'contador' => $this->getContador(),
            'accion_contador' => $this->getAccionContador(),
            'toolbox' => $this->getToolbox(),
            'estado' => $this->getEstado()
        ); 
        return array_filter($data,'strlen');
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setParamMotivoId($parametro_motivo_id)
    {
        $this->parametro_motivo_id = $parametro_motivo_id;
    }

    public function getParamMotivoId(){
        return $this->parametro_motivo_id;
    }

    public function setMotivoOfscId($motivo_ofsc_id)
    {
        $this->motivo_ofsc_id = $motivo_ofsc_id;
    }

    public function getMotivoOfscId(){
        return $this->motivo_ofsc_id;
    }

    public function setSubmotivoOfscId($submotivo_ofsc_id)
    {
        $this->submotivo_ofsc_id = $submotivo_ofsc_id;
    }

    public function getSubmotivoOfscId(){
        return $this->submotivo_ofsc_id;
    }

    public function setContador($contador)
    {
        $this->contador = $contador;
    }

    public function getContador(){
        return $this->contador;
    }

    public function setAccionContador($accion_contador)
    {
        $this->accion_contador = $accion_contador;
    }

    public function getAccionContador(){
        return $this->accion_contador;
    }

    public function setToolbox($toolbox)
    {
        $this->toolbox = $toolbox;
    }

    public function getToolbox(){
        return $this->toolbox;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function motivosofsc()
    {
        return $this->hasOne('MotivoOfsc','id','motivo_ofsc_id');
    }

    public function submotivosofsc()
    {
        return $this->hasOne('SubmotivoOfsc','id','submotivo_ofsc_id');
    }

	public function guardar(){
        $paraMotivoDetalle = new ParametroMotivoDetalle();
        $paraMotivoDetalle->fill($this->getData());                
        $paraMotivoDetalle->save();
        //$this->setData(json_decode(json_encode($paraMotivoDetalle),true));
        return ($paraMotivoDetalle) ? $paraMotivoDetalle : false;
    } 

    public function actualizar(){    
        $paraMotivoDetalle = ParametroMotivoDetalle::find($this->getId());
        $inputs = $this->getData();
        foreach (json_decode(json_encode($paraMotivoDetalle),true) as $key => $value) {  
            if (array_key_exists($key, $inputs)) {
                $paraMotivoDetalle[$key] = $inputs[$key];                  
            }
        }
        $paraMotivoDetalle->save();
        //$this->setData(json_decode(json_encode($paraMotivoDetalle),true));
        return ($paraMotivoDetalle) ? $paraMotivoDetalle : false;
    }

}