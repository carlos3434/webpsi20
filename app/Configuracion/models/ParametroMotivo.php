<?php
namespace Configuracion\models;
class ParametroMotivo extends \Eloquent
{
    protected $guarded =[];
	public $table = "parametro_motivo";

    private $id;
    private $nombre;
    private $tipo_legado;
    private $actividad_id;
    private $bucket_id;
    private $estado;
    private $detalle;

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

    public function setData($data) {
        if(isset($data['id'])){
            $this->setId($data['id']);
        }

        if(isset($data['nombre'])){
            $this->setNombre($data['nombre']);
        }

        if(isset($data['estado'])){
            $this->setEstado($data['estado']);           
        }

        if(isset($data['tipo_legado'])){
            $this->setTipoLegado($data['tipo_legado']); 
        }

        if(isset($data['actividad_id'])){
            $this->setActividadId($data['actividad_id']);
        }

        if(isset($data['bucket_id'])){
            $this->setBucketId($data['bucket_id']);
        }

        if(isset($data['detalle'])){
            $this->setDetalle($data['detalle']);
        }
    }

    public function getData(){
        $data = array(
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'estado' => $this->getEstado(),
            'tipo_legado' => $this->getTipoLegado(),
            'actividad_id' => $this->getActividadId(),
            'bucket_id' => $this->getBucketId()
        ); 
        return array_filter($data,'strlen');
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setTipoLegado($tipo_legado)
    {
        $this->tipo_legado = $tipo_legado;
    }

    public function getTipoLegado(){
        return $this->tipo_legado;
    }

    public function setActividadId($actividad_id)
    {
        $this->actividad_id = $actividad_id;
    }

    public function getActividadId(){
        return $this->actividad_id;
    }

    public function setBucketId($bucketId)
    {
        $this->bucket_id = $bucketId;
    }

    public function getBucketId(){
        return $this->bucket_id;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }

    public function getDetalle(){
        return $this->detalle;
    }

	public function motivodetalle()
    {
        return $this->hasMany('Configuracion\models\ParametroMotivoDetalle','parametro_motivo_id','id');
    }

    public function listar($data = []){
    	$datos = ParametroMotivo::select(
                    'parametro_motivo.id',
                    'parametro_motivo.nombre',
                    \DB::raw('if(tipo_legado = 1, "CMS", "GESTEL") as tipo_legado'),
                    'parametro_motivo.estado as estado',
                    'b.id as bucket_id',
                    'b.nombre as bucket_nombre',
                    'tipo_legado as tipo_legado_id'
                )
                ->leftJoin('bucket as b', 'b.id', '=', 'parametro_motivo.bucket_id')
                ->where(function($query) use ($data){
                    if(isset($data['id'])){
                        $query->where('parametro_motivo.id',$data['id']);
                    }
                })
                ->get();
        return $datos;
    }

    public function action()
    {
        if($this->getId()){ //update
            $result = $this->actualizar();
        }else{ //save
            $result = $this->guardar();
        }
        return $result;
    }

    public function guardar(){
        $ParamMotivo = new ParametroMotivo();
        $ParamMotivo->fill($this->getData());                
        $ParamMotivo->save();
        $this->setData(json_decode(json_encode($ParamMotivo),true));
        return ($ParamMotivo) ? $ParamMotivo : false;
    } 

    public function actualizar(){    
        $ParamMotivo = ParametroMotivo::find($this->getId());
        $inputs = $this->getData();
        foreach (json_decode(json_encode($ParamMotivo),true) as $key => $value) {  
            if (array_key_exists($key, $inputs)) {
                $ParamMotivo[$key] = $inputs[$key];                  
            }
        }
        $ParamMotivo->save();
        $this->setData(json_decode(json_encode($ParamMotivo),true));
        return ($ParamMotivo) ? $ParamMotivo : false;
    }

    public function cargarDetalle(){
        $datos = ParametroMotivo::with('motivodetalle.motivosofsc','motivodetalle.submotivosofsc')
                    ->where('id', $this->id)
                    ->get();
        return $datos;
    }
}
