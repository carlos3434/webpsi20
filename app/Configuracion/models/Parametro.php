<?php
namespace Configuracion\models;

use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaCms;

class Parametro extends \Eloquent
{
    protected $guarded =[];
    public $table = 'parametro';

    private $id;
    private $nombre;
    private $proveniencia;
    private $tipo;
    private $bucket;
    private $tipo_masiva_id;
    private $quiebre_id;
    private $estado;

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

    public function setData($data) {

        if(isset($data['id'])){
            $this->setId($data['id']);
        }

        if(isset($data['nombre'])){
            $this->setName($data['nombre']);
        }

        if(isset($data['estado'])){
            $this->setEstado($data['estado']);
        }

        if(isset($data['proveniencia'])){
            $this->setProveniencia($data['proveniencia']); 
        }

        if(isset($data['tipo'])){
            $this->setTipo($data['tipo']);
        }

        if(isset($data['bucket'])){
            $this->setBucket($data['bucket']);
        }

        if(isset($data['tipo_masiva_id'])){
            $this->setTipoMasivaId($data['tipo_masiva_id']);
        }

        if(isset($data['quiebre_id'])){
            $this->setQuiebreId($data['quiebre_id']);
        }

        if(isset($data['Parametro_Criterio'])){
            $this->setParamCriterio($data['Parametro_Criterio']);
        }

        if(isset($data['check'])){
            $this->setSeleccionable($data['check']);    
        }

        if ($criterio = preg_grep('~' . preg_quote('criterio', '~') . '~', array_keys($data))) {
            $criterios = [];
            foreach ($criterio as $key => $value) {
                $criterios[$value] = $data[$value];                
            }
            $this->setCriterio($criterios);
        }
    }

    public function getData(){
        $data = array(
            'id' => $this->getId(),
            'nombre' => $this->getName(),
            'estado' => $this->getEstado(),
            'proveniencia' => $this->getProveniencia(),
            'tipo' => $this->getTipo(),
            'bucket' => $this->getBucket(), 
            'tipo_masiva_id' => $this->getTipoMasivaId(),
            'quiebre_id' => $this->getQuiebreId()
        );
        return array_filter($data,'strlen');
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setName($name){
        $this->nombre = $name;
    }

    public function getName(){
        return $this->nombre;
    }

    public function setEstado($estado){
        $this->estado = $estado;
    }

    public function getEstado(){
        return $this->estado;
    }

    public function setProveniencia($proveniencia){
        $this->proveniencia = $proveniencia;
    }

    public function getProveniencia(){
        return $this->proveniencia;
    }

    public function setTipo($tipo){
        $this->tipo = $tipo;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function setBucket($bucket){
        $this->bucket = $bucket;
    }

    public function getBucket(){
        return $this->bucket;
    }

    public function setQuiebreId($quiebreId){
        $this->quiebre_id = $quiebreId;
    }

    public function getQuiebreId(){
        return $this->quiebre_id;
    }

    public function setTipoMasivaId($tipoMasivaId){
        $this->tipo_masiva_id = $tipoMasivaId;
    }

    public function getTipoMasivaId(){
        return $this->tipo_masiva_id;
    }

    public function setParamCriterio($paramCriterio){
        $this->parametro_criterio = $paramCriterio;
    }

    public function getParamCriterio(){
        return $this->parametro_criterio;
    }   

    public function setSeleccionable($checks){
        $this->seleccionables = $checks;
    }

    public function getSeleccionable(){
        return $this->seleccionables;
    }

    public function setCriterio($criterio){
        $this->criterio = $criterio;
    }

    public function getCriterio(){
        return $this->criterio;
    }

    public function tipo_masiva()
    {
        return $this->belongsTo('TipoMasiva');
    }

    public function parametroCriterio()
    {
        return $this->hasMany('Configuracion\models\ParametroCriterio');
    }

    public function action(){      
        if($this->getId()){ //update
            $result = $this->actualizar();
        }else{ //save
            $result = $this->guardar();
        }

        $cache = [];
        if($result){           
            if($this->parametro_criterio && $this->seleccionables){
                $checks = $this->seleccionables;
                $criterios = [];

                foreach ($checks as $key => $value) {
                    $criterios[]=explode("|", $value)[0];
                }
                       
                \DB::table('parametro_criterio')->where('parametro_id', $this->getId())->whereNotIn('criterios_id', $criterios)
                ->update(array('estado' => 0));

                if($this->criterio){
                    foreach ($this->criterio as $key => $value) {
                        if (in_array(substr($key, 8, 8), $criterios)) {
                            if(substr($key, 8, 8)==11){ //nodo
                                $cache[] = $value;
                            }
                            $detalle = implode($value, ",");
                            $sql = "INSERT INTO parametro_criterio (parametro_id,criterios_id,detalle,created_at)
                                    values(".$this->id.",".substr($key, 8, 8).",'".$detalle."',now())
                                    ON DUPLICATE KEY UPDATE updated_at=now(),estado=1,detalle='".$detalle."' ";
                            \DB::insert($sql);
                        }
                    } 
                }
            }

            if($this->tipo == 1){
                $cache = $this->CacheForget($this->id,'listParametroCms',$cache);
            }

            if($this->tipo == 2){
                $cache = $this->CacheForget($this->id,'listParametrosGoToa',$cache);
            }
            return ($result) ? true : false;   
        }
    }

    public function guardar(){
        $Parametro = new Parametro();
        $Parametro->fill($this->getData());                
        $Parametro->save();
        $this->setData(json_decode(json_encode($Parametro),true));
        return ($Parametro) ? $Parametro : false;
    } 

    public function actualizar(){
        $inputs = $this->getData();
        $Parametro = Parametro::find($this->getId());
        foreach (json_decode(json_encode($Parametro),true) as $key => $value) {  
            if (array_key_exists($key, $inputs)) {
                $Parametro[$key] = $inputs[$key];

                if(isset($inputs['nombre'])  && (isset($inputs['estado']) && count($inputs['estado'])==0)){
                    $Parametro['nombre'] = 'Sin Nombre_'.date('Ymd_His');
                }        
            }
        }
        $Parametro->save();
        $this->setData(json_decode(json_encode($Parametro),true));
        return ($Parametro) ? $Parametro : false;
    }

    public function CacheForget($param_id,$name_cache,$cache){
        if(count($cache) == 0){
            $buscarnodos = \DB::table('parametro_criterio')
                ->where('parametro_id', $param_id)
                ->where('criterios_id', 11)
                ->select('detalle')->first();
                
            if(!is_null($buscarnodos))
                $cache[] = explode(",", $buscarnodos->detalle);
            
            foreach ($cache as $key => $value) {
                foreach ($value as $keys => $values) {
                    \Cache::forget($name_cache."|".$values);    
                }
            }
            return $cache;
        }
    }    

    public function getBuscar($inputs)
    {
            $listafiltro =  \DB::table('parametro as p')
                            ->leftjoin('parametro_criterio as pc','p.id','=','pc.parametro_id')
                            ->leftjoin('criterios as c','c.id','=','pc.criterios_id')
                            ->select(
                                'p.id',
                                'p.nombre',
                                'p.tipo',
                                'p.proveniencia',
                                'p.bucket',
                                'pc.criterios_id',
                                'c.nombre as criterio',
                                'c.nombre_trama',
                                'c.controlador as control',
                                'pc.detalle',
                                'p.estado',
                                'pc.estado as criterio_activo',
                                'p.created_at'
                            );

            if (isset($inputs['id'])) {
                $listafiltro->where('p.id', $inputs['id']);
            }
            if (isset($inputs['estado'])) {
                $listafiltro->where('p.estado', $inputs['estado']);
            }
            if (isset($inputs['proveniencia'])) {
                $listafiltro->where('p.proveniencia', $inputs['proveniencia']);
            }
            $listafiltro->orderBy('p.created_at','desc');
            $listafiltro = $listafiltro->get();
            return $listafiltro;
    }

    public function getAllParameters($inputs){
        $parametros =  Parametro::where('proveniencia', '<>', '1')->where("tipo", "<>", 3);

        if (isset($inputs['proveniencia']) && count($inputs['proveniencia']) > 0) {
             $parametros->whereIn("proveniencia", $inputs['proveniencia']);
        }

        if (isset($inputs['bucket']) && count($inputs['bucket']) > 0) {
             $parametros->whereIn("bucket", $inputs['bucket']);
        }

        if (isset($inputs['tipo']) && count($inputs['tipo']) > 0) {
             $parametros->whereIn("tipo", $inputs['tipo']);
        }

        if (isset($inputs['tipo_masiva_id']) && count($inputs['tipo_masiva_id']) > 0) {
              $parametros->whereIn("tipo_masiva_id", $inputs['tipo_masiva_id']);
        }

        if (isset($inputs['perfilId']) && $inputs['perfilId']==17) {
              $parametros->where("tipo", 4);
        }
                
        $parametros = $parametros->with("tipo_masiva")->orderBy('created_at','desc')->get();
        return $parametros;
    }

    public static function getParametroValidacion($data = [])
    {     
        if(empty($data)){
            return false;
        }

        $nombrecampo ='nombre_trama';
        if($data['proveniencia']){
            $proveniencia = $data['proveniencia'];
            if ($proveniencia==1) {
                $nombrecampo ='nombre_gestion';
            }
            if ($proveniencia==3) {
                $nombrecampo ='nombre_gestel_prov';
            }
            if ($proveniencia==4) {
                $nombrecampo ='nombre_gestel_ave';
            }
        }

        if(isset($data['campo'])){
            $nombrecampo =$data['campo'];
        }

        try{
            \DB::statement("SET GLOBAL group_concat_max_len=4096");
        } catch(Exception $e){
            $error = new \ErrorController;
            $error->saveError($e);
        }

        $listafiltro =  \DB::table('parametro as p')
                        ->leftjoin('parametro_criterio as pc','p.id','=','pc.parametro_id')
                        ->leftjoin('criterios as c','c.id','=','pc.criterios_id')
                        ->select(
                                'p.nombre AS nombre_parametro',
                                'pc.parametro_id as filtro',
                                'p.tipo',
                                'p.proveniencia',
                                'p.bucket',
                                \DB::RAW('GROUP_CONCAT('.$nombrecampo.' SEPARATOR "||") as trama'),
                                \DB::RAW('GROUP_CONCAT(detalle SEPARATOR "||") as detalle')
                        )
                        ->whereRaw('pc.estado = 1');

        if (isset($data['id'])) {
            $listafiltro->whereRaw('p.id IN ('.$data['id'].')');
        }
        if (isset($data['tipo'])) {
            $listafiltro->whereRaw('p.tipo IN ('.$data['tipo'].')');
        }
        if (isset($data['estado'])) {
            $listafiltro->whereRaw('p.estado IN ('.$data['estado'].')');
        }
        if (isset($data['proveniencia'])) {
            $listafiltro->whereRaw('p.proveniencia IN ('.$data['proveniencia'].')');
        }
        if (isset($data['bucket'])) {
            $listafiltro->whereRaw('p.bucket IN ('.$data['bucket'].')');
        }

        $listafiltro->groupBy('p.id');
        $listafiltro->orderBy('p.id');
        
        if (isset($data['filtrodetalle'])) {
            $filtrodetalle = $data['filtrodetalle'];
            $listafiltro = \DB::table(\DB::raw('(' . $listafiltro->toSql() . ')  as a'))
                        ->where('a.detalle','like','%'.$filtrodetalle.'%');
        }

        return $listafiltro->get();    
    }

    public function getFiltrarST()
    {
        $masivast = \Masivast::where('parametro_id', $id)->get();
        $response = ["rst" => 1, "msj" => "", "data" => []];
        $hoy = date("Y-m-d");

        if (count($masivast) > 0) {
            $response["rst"] = 2;
            return $response;
        }

        $array['id']= $this->id;
        $array['estado']= 1;
        $parametrizacion = $this->getParametroValidacion($array);
        if($this->proveniencia == 2) {
            foreach ($parametrizacion as $key => $value) {
                $trama = explode("||", $value->trama);
                $detalle = explode("||", $value->detalle);
                $stmasivas = \SolicitudTecnicaCms::select(\DB::RAW($id. ' as parametro_id'),'u.solicitud_tecnica_id')->getUltimoCms();

                foreach ($trama as $keys => $valor) {
                    if ($valor!='' && $detalle[$keys]!='') {
                        $stmasivas->whereIn(\DB::RAW($valor), explode(",", $detalle[$keys]));
                    }
                }

                $stmasivas = $stmasivas
                    ->where("u.estado_ofsc_id", 1)
                    ->where("estado_aseguramiento", "=", 1)
                    ->where("u.estado_st", "=", 1)
                    ->whereRaw("DATE(u.updated_at) = '{$hoy}'")
                    ->get();

                $stmasivas = $stmasivas->toArray();
                if (count($stmasivas) > 0) {
                    \Masivast::insert($stmasivas);
                }
                
                $envio = ["parametro" => $id,"st" => $stmasivas];
                $response["data"] = $envio;
                $count = count($stmasivas);
                $response["msj"] = "Registro Actualizado. Se enviaran a cancelar {$count}   solicitudes";
            }
        } else {
            $response["rst"] = 3;
        }
        return $response;
    }
}