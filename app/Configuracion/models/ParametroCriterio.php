<?php
namespace Configuracion\models;

class ParametroCriterio extends \Base
{
	public $table = 'parametro_criterio';

	protected $fillable = ['parametro_id','criterios_id','detalle', 'estado', 'updated_at', 'created_at'];

	public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

	public function criterio(){
		return $this->hasOne('Configuracion\models\Criterios', 'id', 'criterios_id');
	}
}