<?php
namespace Configuracion\models;

class Criterios extends \Base
{	
    public $table = "criterios";

    public static $where =['id', 'nombre', 'controlador', 'afectado', 'cod','estado'];
    public static $selec =['id', 'nombre', 'controlador','afectado',  'cod','estado'];

    public function setId($id){
    	$this->id = $id;
    }

    public function getId(){
    	return $this->id;
    }

    public function setNombre($nombre){
    	$this->nombre = $nombre;
    }

    public function getNombre(){
    	return $this->nombre;
    }

    public function setControlador($controlador){
    	$this->controlador = $controlador;
    }

    public function getControlador(){
    	return $this->controlador;
    }

    public function setAfectado($afectado){
    	$this->afectado = $afectado;
    }

    public function getAfectado(){
    	return $this->afectado;
    }

    public function setCod($cod){
    	$this->cod = $cod;
    }

    public function getCod(){
    	return $this->cod;
    }

    public function setEstado($estado){
    	$this->estado = $estado;
    }

    public function getEstado(){
    	return $this->estado;
    }

    public function listar($inputs)
    { 
        $nombre='nombre_trama';
    	$proveniencia = $inputs['proveniencia'];

        if($proveniencia==1) $nombre='nombre_gestion';
        if($proveniencia==3) $nombre='nombre_gestel_prov';
        if($proveniencia==4) $nombre='nombre_gestel_ave';
                
        $result = Criterios::where('estado',1)
         		->where($nombre,'<>','')
                ->where('controlador','<>','');

        if($inputs['masiva']==1) {
        	$result->where('estado_masiva',1);
        }
            
        $result->orderBy('nombre','asc');
        $result = $result->get();    
        return $result;
    }
}