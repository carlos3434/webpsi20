<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
class LegoActividadesSeeder extends Seeder {

    public function run()
    {
        
		$faker = Faker::create('es_ES');
		LegoActividadServicio::truncate();
		LegoActividad::truncate();

		for ($i=0; $i < 10000; $i++) {
		  echo $faker->name, "\n";
		  $name = substr($faker->name, 0, 5);
		  $objAOM = new LegoActividad;
		  $data = ["codreq" 	=> $faker->randomNumber(7), 
		  		   "tiporeq"	=> $faker->word,
		  		   "zonal" 		=> $faker->word,
		  		   "oficina" 	=> $faker->word,
		  		   "observacion"=> $faker->sentence,
		  		   "desclaseservicio" => $faker->sentence];
	    	foreach ($data as $key => $value) {
	    		$objAOM[$key] = $value;
	    	}

	        $objAOM->save();
	        $idlego = $objAOM->id;

	        $objLEGO = new LegoActividadServicio;
		  	$data = ["descripcion" 	=> $faker->sentence, 
		  		   "idactividadlego"	=> $idlego,
		  		   "clase" 		=> $faker->randomNumber(4),
		  		   "codigo"=> $faker->sentence,
		  		   "descdepartamento" => $faker->sentence];
	    	foreach ($data as $key => $value) {
	    		$objLEGO[$key] = $value;
	    	}

	        $objLEGO->save();
	        echo $idlego."\n";
		}
    }

}