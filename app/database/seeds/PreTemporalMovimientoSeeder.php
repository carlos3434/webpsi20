<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
class PreTemporalMovimientoSeeder extends Seeder {

    public function run()
    {
        
		$faker = Faker::create('es_ES');
		$input = ["quiebre_id" => $faker->numberBetween(29,30),
		'actividad_id' => 1,
		"estado_pretemporal_id" => 1,
		"tipo_atencion" => "Normal",
		"tipo_fuente" => "rrss",
		"gestion" => "embajador",
		"codactu" 	=> $faker->randomNumber(7),
		"codcli" 	=> $faker->randomNumber(8),
		'actividad_tipo_id' => $faker->numberBetween(4,5),
		];

		for ($i=0; $i < 100; $i++) {
			$array = [
            
            'tipo_atencion'   => Input::get('tipo_atencion',''),
            'tipo_fuente'     => Input::get('tipo_fuente',''),
            'gestion'         => Input::get('gestion',''),
            'codactu'         => Input::get('codactu',''),
            'codcli'          => Input::get('codcli',''),
            'quiebre_id'      => Input::get('quiebre_id',0),
            'actividad'       => Input::get('actividad',''),
            
            'cliente_nom'     => Input::get('cliente_nom',''),
            'cliente_cel'     => Input::get('cliente_cel',''),
            'cliente_correo'  => Input::get('cliente_correo',''),
            'cliente_tel'     => Input::get('cliente_tel',''),
            'cliente_dni'     => Input::get('cliente_dni',''),

            'contacto_nom'     => Input::get('contacto_nom',''),
            'contacto_cel'     => Input::get('contacto_cel',''),
            'contacto_correo'  => Input::get('contacto_correo',''),
            'contacto_tel'     => Input::get('contacto_tel',''),
            'contacto_dni'     => Input::get('contacto_dni',''),

            'embajador_nom'    => Input::get('embajador_nom',''),
            'embajador_cel'    => Input::get('embajador_cel',''),
            'embajador_correo' => Input::get('embajador_correo',''),
            'embajador_dni'    => Input::get('embajador_dni',''),
            'comentario'       => Input::get('comentario','')
        ];
		  echo $faker->name, "\n";
		  $name = substr($faker->name, 0, 5);
		  $objAOM = new LegoActividad;
		  $data = ["codreq" 	=> $faker->randomNumber(7), 
		  		   "tiporeq"	=> $faker->word,
		  		   "zonal" 		=> $faker->word,
		  		   "oficina" 	=> $faker->word,
		  		   "observacion"=> $faker->sentence,
		  		   "desclaseservicio" => $faker->sentence];
	    	foreach ($data as $key => $value) {
	    		$objAOM[$key] = $value;
	    	}

	        $objAOM->save();
	        $idlego = $objAOM->id;

	        $objLEGO = new LegoActividadServicio;
		  	$data = ["descripcion" 	=> $faker->sentence, 
		  		   "idactividadlego"	=> $idlego,
		  		   "clase" 		=> $faker->randomNumber(4),
		  		   "codigo"=> $faker->sentence,
		  		   "descdepartamento" => $faker->sentence];
	    	foreach ($data as $key => $value) {
	    		$objLEGO[$key] = $value;
	    	}

	        $objLEGO->save();
	        echo $idlego."\n";
		}
    }

}