<?php
namespace Legados;

use Legados\models\EnvioLegado;

class Assia
{
    protected $_client;
    protected $_error;
    public function __construct()
    {

    }

    public function diagnostico($telefono)
    {
        $elementos = [
            'action'    => 'ASSIADiagnostico',
            'telefono'  => $telefono
        ];

        $xml_data = '
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.api.report.dslo.assia.com" xmlns:xsd="http://model.napi.dslo.assia.com/xsd">
                <soapenv:Header/>
                <soapenv:Body>
                    <ws:getData>
                        <!--Optional:-->
                        <ws:reportID>LINE_SUMMARY_DATA</ws:reportID>
                        <!--Optional:-->
                        <ws:key>
                            <!--Optional:-->
                            <xsd:type>LINE_ID</xsd:type>
                            <!--Optional:-->
                            <xsd:value>'.$telefono.'</xsd:value>
                        </ws:key>
                        <!--Zero or more repetitions:-->
                        <ws:parameters>
                            <!--Optional:-->
                            <xsd:type>REQUEST_TYPE</xsd:type>
                            <xsd:value>REAL_TIME_DIAGNOSTICS</xsd:value>
                        </ws:parameters>
                        <ws:parameters>
                            <xsd:type>REAL_TIME_ANALYSIS_AFTER_FIX</xsd:type>
                            <xsd:value>true</xsd:value>
                        </ws:parameters>
                    </ws:getData>
                </soapenv:Body>
            </soapenv:Envelope>
        ';

        $headers = array(
            "POST  HTTP/1.1",
            "Host: 172.28.13.121:8080",
            "Content-type: text/xml; charset=\"UTF-8\"",
            "SOAPAction: \"http://dpd.com/common/service/LoginService/2.0/getAuth\"",
            "Content-length: ".strlen($xml_data)
        );

        $url = \Config::get("legado.wsdl.assia");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

        $response = curl_exec($ch);
        $this->tracer($elementos, $response);
        return $response;
    }
    /**
     * @param $request array
     * @param $response array
     */
    protected function tracer($elementos = [], $response = [])
    {
        $userId = \Auth::id();
        $envio = new EnvioLegado;
        $envio->request = json_encode($elementos);
        $envio->response = json_encode($response);
        $envio->accion = '';
        $envio->solicitud_tecnica_id = $elementos["telefono"];
        $envio->save();
        return;
    }
}