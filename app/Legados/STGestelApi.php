<?php
namespace Legados;

use Legados\Legado;

class STGestelApi extends Legado
{
    public function __construct($userId='GESTEL Legado')
    {
        $this->_wsdl = \Config::get("legado.wsdl.respuestast_gestel");
        $this->_userId = $userId;
        parent::__construct();
    }

    public function cierreProvision($peticion)
    {
        $body = [
            'tipo_operacion'            => $peticion['tipo_operacion'], // dinamico
            'num_solicitud_tecnica'     => $peticion['num_solicitud_tecnica'], // id_solicitud_tecnica
            'fecha_programacion'        => $peticion['fecha_programacion'], // fecha_programacion
            'contrata'                  => $peticion['contrata'], // contrata
            'tecnico1'                  => $peticion['tecnico1'],
            'tecnico2'                  => $peticion['tecnico2'],
            'fecha_emision_boleta'      => $peticion['fecha_emision_boleta'],
            'nombre_persona_recibe'     => $peticion['nombre_persona_recibe'], // nombres_contacto
            'numero_doc_identidad'      => $peticion['numero_doc_identidad'], // numero_doc
            'observacion'               => $peticion['observacion'], // desobs
            'parentesco'                => $peticion['parentesco'],
            'indicador_fraude'          => $peticion['indicador_fraude'],
            'anexos'                    => $peticion['anexos'],
            'fecha_ejecucion'           => $peticion['fecha_ejecucion'],
            'hora_inicio'               => $peticion['hora_inicio'],
            'hora_fin'                  => $peticion['hora_fin'],
            'desplazamiento'            => $peticion['desplazamiento'],
            'escenario'                 => $peticion['escenario'], // escenario_gestel
            'cabina'                    => $peticion['cabina'],
            'ambiente'                  => $peticion['ambiente'],
            'cod_mot'                   => $peticion['cod_mot'],
            'des_obs'                   => $peticion['des_obs'],
            'fecha_hora_devolucion'     => $peticion['fecha_hora_devolucion'], // fecha_hora_devolucion
            'dato1'                     => $peticion['dato1'], // valor1
            'dato2'                     => $peticion['dato2'], // valor2
            'dato3'                     => $peticion['dato3'], // valor3
            'dato4'                     => $peticion['dato4'], // valor4
            'dato5'                     => $peticion['dato5'], // valor5
            'DatosMateriales'           => $peticion['DatosMateriales']
        ];

        $elementos = [
            'action'                => 'CierreSTProvision',
            'body'                  => $body,
            'operation'             => 'CierreSTProvision',
            'solicitud_tecnica_id'  => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }

    public function cierreAveria($peticion)
    {
        $body = [
            'tipo_operacion'            => $peticion['tipo_operacion'],
            'numero_solicitud_tecnica'  => $peticion['numero_solicitud_tecnica'],
            'fec_hora_programacion'     => $peticion['fec_hora_programacion'],
            // 'observacion_mov'           => $peticion['observacion_mov'],
            'contrata'                  => $peticion['contrata'],
            'tecnico'                   => $peticion['tecnico'],
            'tecnico2'                  => $peticion['tecnico2'],
            'tecnico3'                  => $peticion['tecnico3'],
            // 'descrip_obs_des'           => $peticion['descrip_obs_des'],
            'ncompro'                   => $peticion['ncompro'],
            'fecha_emision'             => $peticion['fecha_emision'],
            'codigo_franqueo'           => $peticion['codigo_franqueo'],
            // 'observacion'               => $peticion['observacion'],
            't_liquid'                  => $peticion['t_liquid'],
            'hora_inicio'               => $peticion['hora_inicio'],
            'hora_fin'                  => $peticion['hora_fin'],
            // 'falla_reportada'           => $peticion['falla_reportada'],
            'codigo_liquidacion'        => $peticion['codigo_liquidacion'],
            'codigo_destino'            => $peticion['codigo_destino'],
            'codigo_version'            => $peticion['codigo_version'],
            'codigo_detalle'            => $peticion['codigo_detalle'],
            'fecha_sal'                 => $peticion['fecha_sal'],
            'fecha_destino'             => $peticion['fecha_destino'],
            'nomcon_liq'                => $peticion['nomcon_liq'],
            'telecon_liq'               => $peticion['telecon_liq'],
            'sintoma'                   => $peticion['sintoma'],
            'obs_ultima_liq'            => $peticion['obs_ultima_liq'],

            // 'causa'                     => $peticion['causa'],
            // 'codigo_categoria'          => $peticion['codigo_categoria'],
            // 'tipo_averia'               => $peticion['tipo_averia'],
            // 'Fecha_vencimiento_GICS'    => $peticion['Fecha_vencimiento_GICS'],
            // 'telfic'                    => $peticion['telfic'],
            // 'observacion_102'           => $peticion['observacion_102'],
            // 'codigo_boleta'             => $peticion['codigo_boleta'],
            // 'estado_boleta'             => $peticion['estado_boleta'],
            // 'detalle_contacto'          => $peticion['detalle_contacto'],
            // 'fecha_liquidacion'         => $peticion['fecha_liquidacion'],
            // 'codigo_tecnico_red'        => $peticion['codigo_tecnico_red'],
            // 'tipo_ruta'                 => $peticion['tipo_ruta'],
            // 'motivo'                    => $peticion['motivo'],
            // 'repuesto'                  => $peticion['repuesto'],
            
            'dato1'                     => $peticion['dato1'],
            'dato2'                     => $peticion['dato2'],
            'dato3'                     => $peticion['dato3'],
            'dato4'                     => $peticion['dato4'],
            'dato5'                     => $peticion['dato5'],
            'datosEquipo'               => $peticion['datosEquipo'],
            'reservado'                 => $peticion['reservado']
        ];

        $elementos = [
            'action'                => 'CierreSTAverias',
            'body'                  => $body,
            'operation'             => 'CierreSTAverias',
            'solicitud_tecnica_id'  => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }
}