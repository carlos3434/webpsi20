<?php

namespace Legados;

use Legados\Legado;

/**
 * API LEGADOS Componente
 */
class Componente extends Legado
{

    public function __construct()
    {
        $this->_wsdl = \Config::get("legado.wsdl.RegistroDecos");
        $this->_userId = 'CMS Legado';
        parent::__construct();
    }

    public function asignacionDecos ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'numcompxreq' => $peticion['numcompxreq'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'codtar'      => $peticion['codtar'],
              'numtar'      => $peticion['numtar'],
              'codact'      => $peticion['codact']
        ];
  
        $elementos= [
            'action'  => 'asignacionDecos',
            'body'    => $body,
            'operation' => 'asignacionDecos',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            $response->rst = 1;
            $response->msj = "prueba:Envio Trama a Legado con Exito!";
            //print_r($elementos);
            return $response;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;

    }


    public function desasignacionDecos ($peticion)
    {
        $body = [
                'codreq'      => $peticion['codreq'],
                'indorigreq'  => $peticion['indorigreq'],
                'codmat'      => $peticion['codmat'],
                'numser'      => $peticion['numser'],
                'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'desasignacionDecos',
            'body'    => $body,
            'operation' => 'desasignacionDecos',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }


    public function cambioAveria ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'numcompxreq' => $peticion['numcompxreq'],
              'numcompxsrv' => $peticion['numcompxsrv'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'numserold'   => $peticion['numserold'],
              'codmatpar'   => $peticion['codmatpar'],
              'numserpar'   => $peticion['numserpar'],
              'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'cambioAveria',
            'body'    => $body,
            'operation' => 'cambioAveria',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }


    public function revertirAveria ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'codmatpar'   => $peticion['codmatpar'],
              'numserpar'   => $peticion['numserpar'],
              'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'revertirAveria',
            'body'    => $body,
            'operation' => 'revertirAveria',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }


    public function reposicion ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'numcompxreq' => $peticion['numcompxreq'],
              'numcompxsrv' => $peticion['numcompxsrv'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'numserold'   => $peticion['numserold'],
              'codmatpar'   => $peticion['codmatpar'],
              'numserpar'   => $peticion['numserpar'],
              'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'reposicion',
            'body'    => $body,
            'operation' => 'reposicion',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }

    public function revertirReposicion ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'revertirReposicion',
            'body'    => $body,
            'operation' => 'revertirReposicion',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->indicador   ='';
            $data->observacion   ='';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            print_r($elementos);
            return;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }
    /**
     * codreq       num_requerimiento
     * indorigreq   indicador origen de requerimiento A (averia)  I (provision)
     * codmat       codigo_material
     * numser       numero_serie
     * codtar       07870141
     * codact       \Config::get("legado.user_operation")
     * 
     */
    public function refresh ($peticion)
    {
        $body = [
              'codreq'      => $peticion['codreq'],
              'indorigreq'  => $peticion['indorigreq'],
              'codmat'      => $peticion['codmat'],
              'numser'      => $peticion['numser'],
              'codtar'      => $peticion['codtar'],
              'codact'      => $peticion['codact']
        ];

        $elementos= [
            'action'  => 'refresh',
            'body'    => $body,
            'operation' => 'refresh',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local') ) {
            $data= new \stdClass();
            $data->codreq       = '10000000';
            $data->indorigreq   = 'A';
            $data->indicador    = '1';
            $data->idreg        = '86667112'; //operacion
            $data->observacion  = 'Valores Disponibles generados con \u00e9xito';
            $response = new \stdClass();
            $response->error = false;
            $response->errorMsg ='';
            $response->data = $data;
            return $response;
        }
        $response = $this->doAction($elementos);
        $this->tracer($elementos, $response);
        return $response;
    }
}