<?php

namespace Legados;
//use Cribbb\Events\UserEventHandler;
use Illuminate\Support\ServiceProvider;
use Legados\Repositories;
use Legados\events;
class SolicitudTecnicaServiceProvider extends ServiceProvider {

  /**
   * Register
   */
    public function register()
    {
        $this->app->events->subscribe(new events\StLogRecepcionEventSubscriber);
        $this->app->bind('Legados\Repositories\SolicitudTecnicaRepositoryInterface', 'Legados\Repositories\SolicitudTecnicaCmsRepository');
        $this->app->bind('Legados\Repositories\StGestionRepositoryInterface', 'Legados\Repositories\StGestionRepository');
        $this->app->bind('Legados\Repositories\StGenerarMovimientosRepositoryInterface', 'Legados\Repositories\StGenerarMovimientosRepository');
        
    }
}
