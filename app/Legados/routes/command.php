<?php
Artisan::add(new \Legados\commands\GestionesStPorDiaCommand);
Artisan::add(new \Legados\commands\ObtenerSolicitudesCommand);
Artisan::add(new \Legados\commands\CargaMasivaSolicitudesCommand);
Artisan::add(new \Legados\commands\EnvioMasivoLegadoCommand);
Artisan::add(new \Legados\commands\EliminarStCommand);
Artisan::add(new \Legados\commands\AltersReporteGestionesSt);
Artisan::add(new \Legados\commands\CargaMasivaStEnvLegadoCommand);
Artisan::add(new \Legados\commands\XlsSolicitudesProcesoCommand);
Artisan::add(new \Legados\commands\XlsSolicitudesProceso_7diasCommand);
Artisan::add(new \Legados\commands\CruceToaGoMasivoCommand);
//Artisan::add(new \Legados\commands\EnvioSlaMasivoCommand);
Artisan::add(new \Legados\commands\MensajesCsvContador);
Artisan::add(new \Legados\commands\CruceLiquidadasCommand);
Artisan::add(new \Legados\commands\CompletadoMasivoCommand);
Artisan::add(new \Legados\commands\AlertaSmsTransaccionesGo);
Artisan::add(new \Legados\commands\CompletadoMasivoCommand);