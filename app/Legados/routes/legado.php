<?php
    Route::controller('gestionesSt', 'Legados\controllers\GestionesStController');
    Route::controller('solicitudesenproceso', 'Legados\controllers\SolicitudesenProcesoController');
    Route::controller('actividadPorTecnico', 'Legados\controllers\ActividadPorTecnicoController');
    Route::controller('bandejalegado', 'Legados\controllers\BandejaLegadoController');
    Route::controller('trama', 'Legados\controllers\TramaController');
    Route::controller('envioMasivoSt', 'Legados\controllers\EnvioMasivoStController');

    /*web services para legados*/
    Route::group(['before' => 'auth.legados'], function () {
         Route::any('componente', 'ComponentesController@getServer');
         Route::any('solicitudtecnica', 'SolicitudTecnicaController@getServer');
         Route::any('solicitudtecnica_gestel_averia', 'SolicitudTecnicaGestelAveriaController@getServer');
         Route::any('solicitudtecnica_gestel_provision', 'SolicitudTecnicaGestelProvisionController@getServer');
         Route::any('wprueba_web_service', 'PruebawController@getServer');
         Route::any('ussd', 'UssdController@getServer');
    });
    Route::any('solicitud', 'Services\services\SolicitudService@getServer');
    Route::controller('recursosVsActividad', 'Legados\controllers\RecursosVsActividadController');
    Route::controller('transaccionGo', 'Legados\controllers\TransaccionGoController');