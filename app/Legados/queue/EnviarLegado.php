<?php
namespace Leagados\queue;

use Legados\Repositories\SolicitudTecnicaRepository as STRepository;

class EnviarLegado{

	public function failed() {
		Log::error('Job failed!');
	}

	public function fire($job, $data) {
		$respuesta = $data['respuesta'];
		try {
            $objApiST = new STCmsApi;
            $objApiST->responderST($respuesta);
        } catch (Exception $e) {}

        return;
	}

}
	