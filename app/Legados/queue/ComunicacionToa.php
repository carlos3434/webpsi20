<?php
namespace Legados\queue;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\SolicitudTecnica as SolicitudTecnica;
use Legados\helpers\InboundHelper;
use Legados\models\SolicitudCaida;
use Legados\Repositories\StGestionRepository as StGestion;
/**
 * php artisan queue:listen --queue=consultarcapacidad_st
 */
class ComunicacionToa{

    public function failed() {
        Log::error('Job failed!');
    }

    public function fire($job, $data) {

        \Auth::loginUsingId(\Config::get("legado.user_logs.genera_solicitud"));
        $logId = $data['log_id'];
        $tramaarrays = $data['tramaarrays'];
        $solicitudTecnicaId = $data['solicitud']['id'];
        $quiebreId = $data['quiebre_id'];
        $actividadId = $data['actividad_id'];
        $msjErrorOfsc = '';

        $solicitud = SolicitudTecnica::find($solicitudTecnicaId);
        //$ultimo = $solicitud->ultimo;
        $solicitudCms = $solicitud->cms;
        $codError = '';$msjError='';
        $solicitudCms->getWorkZone();
        $respuesta = $solicitudCms->capacityOfsc();
        $resourceId = '';
        $objgestion = new StGestion;
        $resp_arr[0]= 'vacio';
        $resp_arr[1]= $respuesta;
        $resp_arr[2]= $quiebreId;
        //hay bucket para st
        if (is_array($respuesta)) {
            $parametros = $respuesta;
            $parametros['tipoEnvio'] = 'sla';
            $parametros['nenvio'] = 1;
            $parametros['fechaAgenda'] = '';
            $parametros['timeSlot'] = '';
            $parametros['tipo_legado'] = 1;
            $parametros['actividad'] = $actividadId;
            $inventarios = [];
            //$inventarios=$solicitudCms->getMateriales($actividadId);
            $response = InboundHelper::envioOrdenOfsc($solicitudCms, $parametros, $tramaarrays, $inventarios);
            $resp_arr[0] = $response;
            
            if($response === false) {
                $msjError="Error coneccion TOA";
                $codError='ERR27';

                $solicitud_caida = new SolicitudCaida;
                $solicitud_caida->solicitud_tecnica_id = $solicitud->id;
                $solicitud_caida->flujo = '3';
                $solicitud_caida->codigo_error = $codError;
                $solicitud_caida->error = $msjError;
                $solicitud_caida->save();

            } else {
                if (isset($response->data->data->commands->command->appointment->report->message)) {
                    $messages = $response->data->data->commands->command->appointment->report->message;
                    if (isset($messages->description)) {
                        $msjErrorOfsc.=' '.$messages->description;
                    } else {
                        foreach ($messages as $message) {
                            if (isset($message->result) && $message->result=="error" || $message->result=="warning") {
                                $msjErrorOfsc.=' '.$message->description;
                            }
                        }
                    }
                }

                if (isset($response->data->data->commands->command->appointment->aid)) {
                    $aid = $response->data->data->commands->command->appointment->aid; 

                    $estadoOfsc = 1;
                    $tipoEnvioOfsc = 2;
                    $msjError = "Envio a TOA: OK".$msjErrorOfsc;
                    $resourceId = $respuesta['external_id'];

                    $bucket = \Bucket::where('bucket_ofsc', $resourceId)->first();
                    $movimiento['resource_id'] = $resourceId;
                    $movimiento['aid'] = $aid;
                    $movimiento['estado_ofsc_id'] = $estadoOfsc;
                    $movimiento['tipo_envio_ofsc'] = $tipoEnvioOfsc;
                    $movimiento['bucket_id'] = $bucket->id;
                    /*if (($ultimo->estado_ofsc_id == 8 || $ultimo->estado_ofsc_id == null) && $ultimo->estado_aseguramiento == 1 ) {*/

                    //$objgestion = new StGestion;
                    $objgestion->registrarMovimiento($solicitud, $movimiento, []);

                    $this->actualizarLogRecepcion($codError, $msjError, $solicitud->id, $logId);
                    \Auth::logout();
                    echo "fin consulta y envio TOA33  \n";
                    $job->delete();
                    return ;
                    /*} else {
                        \Auth::logout();
                        echo "fin consulta y envio TOA  \n";
                        $job->delete(); 
                    }*/
                } else {
                    $msjError="TOA:".$msjErrorOfsc;
                    $codError='ERR22';//go debe reenviar
                }
            }

        } else {
            $msjError = "Consulta CAPACIDAD a TOA: FAIL|".$respuesta;
            $codError = 'ERR12';//go debe reenviar
        }
        
        print_r($resp_arr);
        $this->actualizarLogRecepcion($codError, $msjError, $solicitud->id, $logId);
        \Auth::logout();
        echo "fin consulta y envio TOA444  \n";
        $job->delete();
        return;
    }

    public function actualizarDatos($movimiento, $solicitud, $ultimo) {
        $ultimo->update($movimiento);
        $solicitud->ultimoMovimiento()->update($movimiento);
    }   

    public function actualizarLogRecepcion($codError, $msjError = null, $solicitudId, $logId) {
        $objlogst = STLogRecepcion::find($logId);
        if (!is_null($objlogst)) {
            $objlogst->update([
                'code_error'            => $codError,
                'solicitud_tecnica_id'  => $solicitudId,
                'descripcion_error'     => $msjError
            ]);
        }
        return;
    }

}