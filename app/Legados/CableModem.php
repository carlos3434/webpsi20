<?php
namespace Legados;

use Legados\Legado;
use Legados\models\EnvioLegado;
class CableModem extends Legado
{
    public function __construct()
    {
        $this->_wsdl = \Config::get("legado.wsdl.cable_modem");
        try {
            $wsOptArray = [
                "proxy_host" => \Config::get("wpsi.proxy.host"),
                "proxy_port" => \Config::get("wpsi.proxy.port"),
                "trace"      => 1,
                "exception"  => 0
            ];
            $opts = array('http' => array('protocol_version' => '1.0'));
            $wsOptArray["stream_context"] = stream_context_create($opts);
            $soapClient = new \SoapClient(
                $this->_wsdl,
                $wsOptArray
            );
            $this->_client = $soapClient;
        } catch (Exception $e) {
            
        }
    }

    public function getPruebas($mac)
    {
        $param= [
            'mac_address' => $mac
        ];

        $result = $this->_client->__soapCall(
            'getPruebasCablemodem',
            $param
        );
        $elementos = [
            'action'=>'getPruebasCablemodem',
            'body'=>$param
        ];
        $this->tracer($elementos,$result);
        return $result;
    }

        /**
     * @param $request array
     * @param $response array
     */
    protected function tracer($elementos = [], $response = [])
    {
        $userId = \Auth::id();
        $envio = new EnvioLegado;
        $envio->request = json_encode($elementos["body"]);
        $envio->response = json_encode($response);
        $envio->accion = $elementos["action"];
        $envio->save();
        return;
    }
}