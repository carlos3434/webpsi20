<?php namespace Legados\helpers;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Query\Builder;
use Legados\models\Error;
use Legados\models\ComponenteCms;
use Legados\models\SolicitudTecnicaUltimo;

class SolicitudTecnicaHelper
{
    public static function getErrorByMessage($message = "")
    {
        $respuesta = ["error_code" => "", "desc_error" => ""];
        $error = Error::find(9);
        if ($message!="") {
            $coincidencia = strpos($message, "obligatorio");
            if ($coincidencia) {
                $error = Error::find(3);
                return $error->code;
            }
            $coincidencia = strpos($message, "debe tener entre");
            if ($coincidencia) {
                $error = Error::find(4);
                return $error->code;
            }
            $coincidencia = strpos($message, "formato");
            if ($coincidencia) {
                $error = Error::find(5);
                return $error->code;
            }
            $coincidencia = strpos($message, "inválido");
            if ($coincidencia) {
                $error = Error::find(6);
                return $error->code;
            }
            $coincidencia = strpos($message, "contener");
            if ($coincidencia) {
                $error = Error::find(2);
                return $error->code;
            } else {
                return $error->code;
            }
        } else {
            return $error->code;
        }
    }
    public static function parsearFechas($solicitud)
    {
        $datetime = 'd/m/Y H:i:s';
        $date = 'd/m/Y';
        //cms
        if (isset($solicitud['fecha_envio']) && strlen($solicitud['fecha_envio']) > 0) {
            $solicitud['fecha_envio'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_envio']);
        }
        if (isset($solicitud['fecha_registro_requerimiento']) && strlen($solicitud['fecha_registro_requerimiento']) > 0) {
            $solicitud['fecha_registro_requerimiento'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_registro_requerimiento']);
        }
        if (isset($solicitud["fecha_asignacion"]) && strlen($solicitud["fecha_asignacion"]) > 0) {
            $solicitud['fecha_asignacion'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_asignacion']);
        }
        if (isset($solicitud["fecha_instalacion_alta"]) && strlen($solicitud["fecha_instalacion_alta"]) > 0) {
            $solicitud['fecha_instalacion_alta'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_instalacion_alta']);
        }
        //gestel provision
        if (isset($solicitud['fecha_solicitud']) && strlen($solicitud['fecha_solicitud']) > 0) {
            $solicitud['fecha_solicitud'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_solicitud']);
        }
        if (isset($solicitud['fecha_formulacion']) && strlen($solicitud['fecha_formulacion']) > 0) {
            $solicitud['fecha_formulacion'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_formulacion']);
        }
        if (isset($solicitud["fecha_programacion"]) && strlen($solicitud["fecha_programacion"]) > 0) {
            $solicitud['fecha_programacion'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_programacion']);
        }
        if (isset($solicitud["fecha_inicio"]) && strlen($solicitud["fecha_inicio"]) > 0) {
            $solicitud['fecha_inicio'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_inicio']);
        }
        if (isset($solicitud["fecha_ejecucion_pin"]) && strlen($solicitud["fecha_ejecucion_pin"]) > 0) {
            $solicitud['fecha_ejecucion_pin'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_ejecucion_pin']);
        }
        if (isset($solicitud["fecha_ejecucion_mdf"]) && strlen($solicitud["fecha_ejecucion_mdf"]) > 0) {
            $solicitud['fecha_ejecucion_mdf'] = \DateTime::CreateFromFormat($date, $solicitud['fecha_ejecucion_mdf']);
        }
        if (isset($solicitud["fecha_registro_peticion"]) && strlen($solicitud["fecha_registro_peticion"]) > 0) {
            $solicitud['fecha_registro_peticion'] = \DateTime::CreateFromFormat($datetime, $solicitud['fecha_registro_peticion']);
        }
        if (isset($solicitud["registro_pai"]) && strlen($solicitud["registro_pai"]) > 0) {
            $solicitud['registro_pai'] = \DateTime::CreateFromFormat($datetime, $solicitud['registro_pai']);
        }
        if (isset($solicitud["fecha_hora_devolucion"]) && strlen($solicitud["fecha_hora_devolucion"]) > 0) {
            $solicitud['fecha_hora_devolucion'] = \DateTime::CreateFromFormat($datetime, $solicitud['fecha_hora_devolucion']);
        }
        if (isset($solicitud["fecha_instalacion_router"]) && strlen($solicitud["fecha_instalacion_router"]) > 0) {
            $solicitud['fecha_instalacion_router'] = \DateTime::CreateFromFormat($datetime, $solicitud['fecha_instalacion_router']);
        }
        return $solicitud;
    }

    public static function propiedadCms($solicitud, $tramaarrays = [])
    {
        $equivalenciaTipoSenalTroba = [
            ''  => '',
            '1' => "100% digital",
            '2' => "parcialmente digital",
            '3' => "100% analógico"
        ];
        //PROPIEDADES LEGADOS CMS
        $propiedades['XA_TIPO_OPER_ST'] = $solicitud['tipo_operacion'];
        $propiedades['XA_IDENTIFICADOR_ST'] = $solicitud['id_solicitud_tecnica'];
        $propiedades['XA_FEC_ENVIO_TR_ST'] = $solicitud['fecha_envio'];
        $propiedades['XA_HOR_ENVIO_TR_ST'] = $solicitud['hora_envio'];
        $propiedades['XA_OFICINA_CMS'] = $solicitud['oficina'];
        $propiedades['XA_ESTADO_CMS'] = $solicitud['estado'];

        // inicio
        $propiedades['XA_DATOS_REQ_CMS'] = '<XA_DATOS_REQ_CMS>
            <DATOS_REQ_CMS>'.
            '<IndicadorOrigenRequerimiento>'.$solicitud['ind_origen_requerimiento'].'</IndicadorOrigenRequerimiento>'.
            '<DescripcionTipoRequerimiento>'.$solicitud['desc_tipo_requerimiento'].'</DescripcionTipoRequerimiento>'.
            '<DescripcionMotivoDeGeneracion>'.$solicitud['desc_motivo_generacion'].'</DescripcionMotivoDeGeneracion>'.
            '<HoraRegistroDelRequerimiento>'.$solicitud['hora_registro_requerimiento'].'</HoraRegistroDelRequerimiento>'.
            '<FechaAsignacion>'.$solicitud['fecha_asignacion'].'</FechaAsignacion>'.
            '<HoraAsignacion>'.$solicitud['hora_asignacion'].'</HoraAsignacion>'.
            '<FechaInstalacionALTA>'.$solicitud['fecha_instalacion_alta'].'</FechaInstalacionALTA>'.
            '<HoraInstalacionALTA>'.$solicitud['hora_instalacion_alta'].'</HoraInstalacionALTA>'.
            '<IdTematico>'.$solicitud['id_tematico'].'</IdTematico>'.
            '<DescripcionTematico>'.$solicitud['desc_tematico'].'</DescripcionTematico>'.
            '<IdBbk>'.$solicitud['id_bbk'].'</IdBbk>'.
            '<IndDuo>'.$solicitud['ind_duo'].'</IndDuo></DATOS_REQ_CMS>'.
            '</XA_DATOS_REQ_CMS>';
        // fin

        //$propiedades['XA_COD_CONTRATA'] = $solicitud['codigo_contrata'];
        $propiedades['XA_COD_TECNICO'] = $solicitud['codigo_tecnico'];
        $propiedades['XA_DES_TECNICO'] = $solicitud['desc_tecnico'];

        $propiedades['XA_DATOS_CLIENTE']='<XA_DATOS_CLIENTE><DATOS_CLIENTE>' .
            '<DocumentoIdentidad>'.$solicitud['doc_identidad'].'</DocumentoIdentidad>'.
            '<CodigoDepartamento>'.$solicitud['cod_departamento'].'</CodigoDepartamento>'.
            '<CodigoProvincia>'.$solicitud['cod_provincia'].'</CodigoProvincia>'.
            '<DescripcionProvincia>'.$solicitud['desc_provincia'].'</DescripcionProvincia>'.
            '<Condicion>'.$solicitud['condicion'].'</Condicion>'.
            '<IndicadorVip>'.$solicitud['ind_vip'].'</IndicadorVip>'.
            '<Situacion>'.$solicitud['situacion'].'</Situacion>'.
            '<DescripcionSituacion>'.$solicitud['desc_situacion'].'</DescripcionSituacion>'.
            '<CategoriaCliente>'.$solicitud['categoria_cliente'].'</CategoriaCliente>'.
            '<DescripcionCategoriaCliente>'.$solicitud['desc_categoria_cliente'].'</DescripcionCategoriaCliente>'.
            '</DATOS_CLIENTE></XA_DATOS_CLIENTE>';

        $propiedades['XA_COORD_X_CAJA_TAP'] = $solicitud['coordx_direccion_tap'];
        $propiedades['XA_COORD_Y_CAJA_TAP'] = $solicitud['coordy_direccion_tap'];
        $propiedades['XA_TIPO_PORTADO'] = $solicitud['tipo_portado'];


        $propiedades['XA_DATOS_SERVICIO'] = '<XA_DATOS_SERVICIO><DATOS_SERVICIO>'.
            '<ClaseServicio>'.$solicitud['clase_servicio'].'</ClaseServicio>'.
            '<DescripcionClaseServicio>'.$solicitud['desc_clase_servicio'].'</DescripcionClaseServicio>'.
            '<CategoriaServicio>'.$solicitud['categoria_servicio'].'</CategoriaServicio>'.
            '<DescripcionCategoriaServicio>'.$solicitud['desc_categoria_servicio'].'</DescripcionCategoriaServicio>'.
            '</DATOS_SERVICIO></XA_DATOS_SERVICIO>';

        $propiedades['XA_COD_JEFATURA'] = $solicitud['cod_jefatura'];


        $propiedades['XA_DATOS_FFTT'] = '<XA_DATOS_FFTT><DATOS_FFTT>'.
            '<DescripcionNodo>'.$solicitud['desc_nodo'].'</DescripcionNodo>'.
            '<CodigoPlano>'.$solicitud['cod_plano'].'</CodigoPlano>'.
            '<DescripcionPlano>'.$solicitud['desc_plano'].'</DescripcionPlano>'.
            '<Sector>'.$solicitud['sector_troba'].'</Sector>'.
            '<CodigoCmts>'.$solicitud['cod_cmts'].'</CodigoCmts>'.
            '<DescripcionCmts>'.$solicitud['desc_cmts'].'</DescripcionCmts>'.
            '<TipoSenalTroba>'.$equivalenciaTipoSenalTroba[$solicitud['tipo_senal_troba']].'</TipoSenalTroba>'.
            '</DATOS_FFTT></XA_DATOS_FFTT>';

        $propiedades['XA_DATOS_FFTT_TAP'] ='<XA_DATOS_FFTT_TAP><DATOS_FFTT_TAP>'.
            '<TAPVia>'.$solicitud['tap_via'].'</TAPVia>'.
            '<TAPNombreVia>'.$solicitud['tap_nombre_via'].'</TAPNombreVia>'.
            '<TAPNumero>'.$solicitud['tap_numero'].'</TAPNumero>'.
            '<TAPPiso>'.$solicitud['tap_piso'].'</TAPPiso>'.
            '<TAPInterior>'.$solicitud['tap_interior'].'</TAPInterior>'.
            '<TAPManzana>'.$solicitud['tap_manzana'].'</TAPManzana>'.
            '<TAPLote>'.$solicitud['tap_lote'].'</TAPLote>'.
            '<Sector>'.$solicitud['sector_tecnico'].'</Sector>'.
            '<Referencia>'.str_replace("&QUOT;", "'", json_encode($solicitud['referencia'])) .'</Referencia>'.
            '</DATOS_FFTT_TAP></XA_DATOS_FFTT_TAP>';

        $propiedades['XA_VELOCIDAD_CONTRATADA'] = $solicitud['velocidad_internet_requerimiento'];
        $propiedades['XA_ESCENARIO_CMS'] = $solicitud['escenario_cms'];
        $propiedades['XA_ESCENARIO_GESTEL'] = $solicitud['escenario_gestel'];
        $propiedades['XA_IND_CALIDAD_VTA'] = $solicitud['ind_calidad_venta'];

        $propiedades['XA_DATOS_VOIP_TDM'] = '<XA_DATOS_VOIP_TDM><DATOS_VOIP_TDM>'.
            '<TipoDeLinea>'.$solicitud['tipo_linea'].'</TipoDeLinea>'.
            '<TelefonoVOIP>'.$solicitud['telefono_voip'].'</TelefonoVOIP>'.
            '<PromocionDeLinea>'.$solicitud['promocion_linea'].'</PromocionDeLinea>'.
            '<DescripcionDeLinea>'.$solicitud['desc_linea'].'</DescripcionDeLinea>'.
            '<NodoId>'.$solicitud['nodo_id'].'</NodoId></DATOS_VOIP_TDM>'.
            '</XA_DATOS_VOIP_TDM>';

        if ($tramaarrays) {
            $count=array();
            $compequipment=array();
            $i=0;
            $productservicexml='<XA_PRODUCTS_SERVICES>';
            //dd($tramaarrays['componentes']);
            foreach ($tramaarrays['componentes'] as $key => $value) {
                    $productservicexml.= '<PRODUCTS_SERVICE>'.
                    '<Codigo>'.$value['componente_cod'].'</Codigo>'.
                    '<Descripcion>'.$value['descripcion'].'</Descripcion>'.
                    '<numcompxreq>'.$value['numero_requerimiento'].'</numcompxreq>'.
                    '<numcompxot>'.$value['numero_ot'].'</numcompxot>'.
                    '<numcompxserv>'.$value['numero_servicio'].'</numcompxserv>'.
                    '<CodigoMaterial>'.$value['codigo_material'].'</CodigoMaterial>'.
                    '<TipoDeAdquisicion>'.$value['tipo_adquisicion'].'</TipoDeAdquisicion>'.
                    '<EquiposTipoDeProcedencia>'.$value['tipo_procedencia'].'</EquiposTipoDeProcedencia>'.
                    '<Marca>'.$value['marca'].'</Marca>'.
                    '<Modelo>'.$value['modelo'].'</Modelo>'.
                    '<CasID>'.$value['casid'].'</CasID>'.
                    '<Estado>'.$value['estado'].'</Estado>'.
                    '<EquiposCodigoComponente>'.$value['componente_cod'].'</EquiposCodigoComponente>'.
                    '<EquiposDescripcionDeComponentes>'.$value['descripcion'].'</EquiposDescripcionDeComponentes>'.
                    '<TipoEquipo>'.$value['componente_tipo'].'</TipoEquipo>'.
                    '<NumeroSerieMacAddress>'.$value['numero_serie'].'</NumeroSerieMacAddress>'.
                    '</PRODUCTS_SERVICE>';

                    $key = array_search($value['componente_cod'], $count);
                if ($key === false) {
                        $compequipment[$i]['cod']=$value['componente_cod'];
                        $compequipment[$i]['tipo']=$value['descripcion'];
                        $compequipment[$i]['count']=1;
                        $count[$i]=$value['componente_cod'];
                        $i++;
                } else {
                        $compequipment[$key]['count']++;
                }
            }
            $propiedades['XA_PRODUCTS_SERVICES']=$productservicexml.'</XA_PRODUCTS_SERVICES>';
    
            $equipmentxml='<XA_EQUIPMENT>';
            foreach ($compequipment as $key => $value) {
                $equipmentxml.= '<Equipo>'.
                '<IdEquipo>'.$value['cod'].'</IdEquipo>'.
                '<TipoEquipo>'.$value['tipo'].'</TipoEquipo>'.
                '<Cantidad>'.$value['count'].'</Cantidad>'.
                '</Equipo>';
            }
            $propiedades['XA_EQUIPMENT']= $equipmentxml.'</XA_EQUIPMENT>';
        }
        return $propiedades;
    }

    public static function propiedadGestelProvision($solicitud)
    {
        
        //PROPIEDADES LEGADOS CMS
        $propiedades['XA_TIPO_OPER_ST'] = $solicitud['tipo_operacion'];
        $propiedades['XA_IDENTIFICADOR_ST'] = $solicitud['id_solicitud_tecnica'];
        $propiedades['XA_FEC_ENVIO_TR_ST'] = null;
        $propiedades['XA_HOR_ENVIO_TR_ST'] = null;
        $propiedades['XA_OFICINA_CMS'] = null;
        $propiedades['XA_ESTADO_CMS'] = $solicitud['estado_req_cms'];

        //$propiedades['XA_COD_CONTRATA'] = $solicitud['contrata'];
        $propiedades['XA_COD_TECNICO'] = null;
        $propiedades['XA_DES_TECNICO'] = null;

        return $propiedades;
    }

    public static function propiedadGestelAveria($solicitud)
    {
        
        //PROPIEDADES LEGADOS CMS
        //PROPIEDADES LEGADOS CMS
        $propiedades['XA_TIPO_OPER_ST'] = $solicitud['tipo_operacion'];
        $propiedades['XA_IDENTIFICADOR_ST'] = $solicitud['id_solicitud_tecnica'];
        $propiedades['XA_FEC_ENVIO_TR_ST'] = null;
        $propiedades['XA_HOR_ENVIO_TR_ST'] = null;
        $propiedades['XA_OFICINA_CMS'] = null;
        $propiedades['XA_ESTADO_CMS'] = null;

        //$propiedades['XA_COD_CONTRATA'] = $solicitud['desc_contrata'];
        $propiedades['XA_COD_TECNICO'] = null;
        $propiedades['XA_DES_TECNICO'] = null;

        return $propiedades;
    }

    public static function arrayTematicos($datatematicos = [])
    {
        $tematicos = [];
        if (isset($datatematicos[0]) && $datatematicos[0] !="") {
            $tematicos["tematico_id1"] = $datatematicos[0];
        }
        if (isset($datatematicos[1]) && $datatematicos[1] !="") {
            $tematicos["tematico_id2"] = $datatematicos[1];
        }
        if (isset($datatematicos[2]) && $datatematicos[2] !="") {
            $tematicos["tematico_id3"] = $datatematicos[2];
        }
        if (isset($datatematicos[3]) && $datatematicos[3] !="") {
            $tematicos["tematico_id4"] = $datatematicos[3];
        }

        return $tematicos;
    }

    public static function estructuraCierre($input = [], $st = [])
    {
        
    }

    public static function getDiferenciadoMasivo($solicitud)
    {
         $codnodo = $solicitud->cod_nodo;
         $tiporequerimiento = $solicitud->tipo_requerimiento;

         if ($codnodo == "SR") {
            return 57;
         }
         return null;
    }
    public static function getQuiebreMovistarUno($solicitud)
    {
        $msj_error_log = [];
        // PENDMOV1: validando por clase de servicio
        $quiebreId = null;
        $claseservicio = $solicitud->clase_servicio;
        $codnodo = $solicitud->cod_nodo;

        $error_motivos = [];


        if ($codnodo == "LL" || $codnodo == "LM" || $codnodo == "SP") {
            if ($claseservicio == "26" || $claseservicio == "27" || $claseservicio == "33" || $claseservicio == "38") {
                $quiebreId = 25;
            } else if ($solicitud->ind_duo == "DUO") {
                $quiebreId = 25;
            } else if ($solicitud->tipo_tecnologia == "VOIP") {
                $quiebreId = 25;
            } else {
                $error_motivos[] = "Clase de servicio esperado (26, 27, 33 ó 38); Recibido: " . ($claseservicio == "" ? "Vacío" : $claseservicio). " Ó  ind_duo esperado (DUO); Recibido: " . ($solicitud->ind_duo == "" ? "Vacío" : $solicitud->ind_duo) . " Ó Tipo de tecnología esperado (VOIP); Recibido: " . ($solicitud->tipo_tecnologia == "" ? "Vacío" : $solicitud->tipo_tecnologia).'.';
            }

        } else {
            $error_motivos[] = "Código de nodo esperado (LL, LM ó SP); Recibido " . ($codnodo == "" ? "Vacío" : $codnodo).'.';

        }


        $msj_error_log[] = [
            "nombre" => "PENDIENTE M1",
            "motivos" => $error_motivos

        ];

        return [
            "msj_error_log" => $msj_error_log,
            "quiebre_id" => $quiebreId
        ];
    }

    public static function getQuiebrePremium($solicitud)
    {
        $msj_error_log = [];

        $quiebreId = null;

        $premium = \ZonaPremiumCatv::where(
            ["nodo" => $solicitud->cod_nodo,
                "troba" => $solicitud->cod_troba,
                "estado" => 1])
            ->first();

        if ( !is_null($premium) ) {

            $claseservicio = $solicitud->clase_servicio;

            if ($claseservicio == "26" || $claseservicio == "27" || $claseservicio == "33") {

                $quiebreId = 25;

            }

            if ( is_null($quiebreId) ) {

                if ($solicitud->tipo_operacion == "AVE_IND") {

                    $q_post_digit = SolicitudTecnicaHelper::getQuiebrePostDigitalizacion($solicitud);
                    $quiebreId = $q_post_digit["quiebre_id"];

                }
            }


            if ( !is_null($quiebreId) ) {

                $quiebre = \Quiebre::find($quiebreId);

                if ( !is_null($quiebre) ) {

                $error_motivos[] =  "EL nodo y troba recibidos (" . $solicitud->cod_nodo . ", " .  $solicitud->cod_troba . "); No se encuentran en la tabla de Trobas digitalizadas y deben tener estado activo.";
                }
            }


        } else {

            $error_motivos[] =  "Código contrata esperado (357 ó 387); Recibido: " . $solicitud->codigo_contrata;

        }


        $msj_error_log[] = [
            "nombre" => "POST DIGIT",
            "motivos" => $error_motivos
        ];
        
        return [
            "msj_error_log" => $msj_error_log,
            "quiebre_id" => $quiebre_id
        ];
    }

    public static function getQuiebreMonoCatvDigital($solicitud)
    {
        $combinaciones = ["SG-V187", "SL-V154", "SL-V162"];
        $tmp = $solicitud->tipo_requerimiento."-".$solicitud->cod_motivo_generacion;
        if (in_array($tmp, $combinaciones)) {
            return 8;
        }
    }

    public static function setCoordenadas($solicitud, $tipolegado)
    {
        if ($tipolegado == 1) {
            $rangookcliente = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_cliente, $solicitud->coordy_cliente);
            $rangookfftt = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_direccion_tap, $solicitud->coordy_direccion_tap);

            $coordenadasfftt = null;
            $zonal = $solicitud->zonal;
            $nodo = $solicitud->cod_nodo;
            $troba = ($solicitud->cod_troba == "")? $solicitud->cod_plano : $solicitud->cod_troba;
            $tap = $solicitud->cod_tap;
            $amplificador = $solicitud->cod_amplificador;

            if ($zonal !="" && $nodo != "" && $troba != "") {
                if ($amplificador!="" && $tap!="") {
                    $coordenadasfftt = \GeoTap::where(
                        ["zonal" => $zonal,
                            "nodo" => $nodo,
                            "troba" => $troba,
                            "tap" => $tap,
                            "amplificador" => $amplificador,
                            "orden" => 1
                        ]
                    )->first();
                } else if ($tap =="" && $amplificador!="") {
                    $coordenadasfftt = \GeoAmplificador::where(
                        ["zonal" => $zonal,
                            "nodo" => $nodo,
                            "troba" => $troba,
                            "amplificador" => $amplificador,
                            "orden" => 1
                        ]
                    )->first();
                } else if ($tap == "" || $amplificador == "") {
                    $coordenadasfftt = \GeoTrobaPunto::where(
                        ["zonal" => $zonal,
                            "nodo" => $nodo,
                            "troba" => $troba
                        ]
                    )->first();
                }
            }
            $coordenadascliente = \DB::table("webpsi_coc.tb_planta_catv")
                ->where("Servicio", $solicitud->cod_servicio)
                ->first();

            if ($rangookcliente == false && $rangookfftt == true) {
                // XY cliente incorrectos y XY de fftts correctos
                $solicitud->coordy_cliente = $solicitud->coordy_direccion_tap;
                $solicitud->coordx_cliente = $solicitud->coordx_direccion_tap;
                return $solicitud;
            } elseif ($rangookcliente == true && $rangookfftt == false) {
                // XY cliente correcto y XY de fftts incorrecto
                if (!is_null($coordenadasfftt)) {
                    $rangookcoordenadasfft = \FuncionesMatematicas::validarRangoXYLima($coordenadasfftt->coord_x, $coordenadasfftt->coord_y);
                    if ($rangookcoordenadasfft) {
                        $solicitud->coordx_direccion_tap = $coordenadasfftt->coord_x;
                        $solicitud->coordy_direccion_tap = $coordenadasfftt->coord_y;
                        return $solicitud;
                    }
                }
                $solicitud->coordx_direccion_tap = $solicitud->coordx_cliente;
                $solicitud->coordy_direccion_tap = $solicitud->coordy_cliente;
                return $solicitud;
            } elseif ($rangookcliente == false && $rangookfftt == false) {
                // XY cliente incorrecto y XY de fftts incorrecto
                if (!is_null($coordenadasfftt)) {
                    $rangookcoordenadasfft = \FuncionesMatematicas::validarRangoXYLima($coordenadasfftt->coord_x, $coordenadasfftt->coord_y);
                    if ($rangookcoordenadasfft) {
                        $solicitud->coordx_direccion_tap = $coordenadasfftt->coord_x;
                        $solicitud->coordy_direccion_tap = $coordenadasfftt->coord_y;
                    }
                }
                $coordenadascliente = \DB::table("webpsi_coc.tb_planta_catv")
                    ->where("Servicio", $solicitud->cod_servicio)
                    ->first();

                if (!is_null($coordenadascliente)) {
                    $rangook = \FuncionesMatematicas::validarRangoXYLima($coordenadascliente->numcoo_x, $coordenadascliente->numcoo_y);
                    if ($rangook) {
                        $solicitud->coordy_cliente  = $coordenadascliente->numcoo_y;
                        $solicitud->coordx_cliente  = $coordenadascliente->numcoo_x;
                    }
                }
                $validaXYCliente = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_cliente, $solicitud->coordy_cliente);
                $validoXYFftts = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_direccion_tap, $solicitud->coordy_direccion_tap);
                if ($validaXYCliente == false && $validoXYFftts == false) {
                    $solicitud->coordx_cliente = "-77.042768";
                    $solicitud->coordy_cliente = "-12.046316";
                    $solicitud->coordx_direccion_tap = "-77.042768";
                    $solicitud->coordy_direccion_tap = "-12.046316";
                    return $solicitud;
                }
                if ($validaXYCliente == true && $validoXYFftts == false) {
                    $solicitud->coordx_direccion_tap = $solicitud->coordx_cliente;
                    $solicitud->coordy_direccion_tap = $solicitud->coordy_cliente;
                    return $solicitud;
                }
                if ($validaXYCliente == false && $validoXYFftts == true) {
                    $solicitud->coordx_cliente = $solicitud->coordx_direccion_tap;
                    $solicitud->coordy_cliente = $solicitud->coordy_direccion_tap;
                    return $solicitud;
                }
            }
        }
        return $solicitud;
    }

    public static function validarNumeroRequerimiento($numero_requerimiento)
    {
        $num_requerimiento = SolicitudTecnicaUltimo::where('num_requerimiento', $numero_requerimiento)
            ->orWhere("estado_aseguramiento", "<", 6)
            ->orWhere("estado_ofsc_id", "<>", 5)
            ->where("estado_st", "=", 1)
            ->first();
        if (!is_null($num_requerimiento)) {
            return true;
        }
        return false;
    }

    public static function getQuiebreByBD($actividadId, $st_cms) {
        //quiebres
        //echo "---------------------------------------\n";
        $quiebres = \Quiebre::select(["id", "nombre", "estado"])->with([
            "parametros" => function( $query ) {
                return $query->select(["id", "quiebre_id", "bucket"])->where('proveniencia', 2);
            },
            "parametros.parametroCriterio" => function( $query ) {

                return $query->select(["parametro_id", "criterios_id", "detalle"]);

            },
            "parametros.parametroCriterio.criterio" => function( $query ) {

                return $query->select(["nombre_trama", "id"]);

            }
        ])->where('estado',1)->get();

        //orden, provinencia 2 es CSM
        $orden = \DB::table("quiebres_order")
        ->select("orden")
        ->where(["provinencia" => 2,  "actividad_id" => $actividadId])
        ->first();

        //Evaluando orden registrado
        if ( $orden !== null ) {

            $orden = explode(",", $orden->orden);

        } else {

            $orden = [];

        }

        $quiebre_id = null;


        if ( sizeof($orden) > 0 ) {


            foreach ($orden as $value) {
                
                foreach ($quiebres as $quiebre) {
                    
                    if ( $value == $quiebre->id ) {
                        //Procesa

                        $quiebre_id = self::evaluaQuiebre($quiebre, $st_cms);

                        if ( $quiebre_id != null ) {
                            return $quiebre_id;
                        }

                    }

                }
            }

        } else {

            foreach ($quiebres as $quiebre) {
                //Procesa
                $quiebre_id = self::evaluaQuiebre($quiebre, $st_cms);

                if ( $quiebre_id != null ) {
                    return $quiebre_id;
                }

            }

        }

        return null;
    }

    public static function evaluaQuiebre( $quiebre, $st_cms ) {
        //echo " [CASO: " . $quiebre->nombre . "] \n";
        $casos = $quiebre->parametros;

        //print_r($casos);

        //if ( $quiebre->estado == 1 ) {

            foreach ( $casos as $caso ) {

                $switch = true;

                //CASOS ESPECIALES
                if ( $caso->bucket ) {
                    $casos_especiales = explode(",", $caso->bucket);

                    //CASO PREMIUM
                    if ( $casos_especiales[0] == "1" ) {
                        $premium = \DB::table("zona_premium_catv")
                        ->where(["nodo" => $st_cms->cod_nodo,
                            "troba" => $st_cms->cod_troba,
                            "estado" => 1])
                        ->first();
                        //echo "*".is_null($premium)."*\n";
                        if( is_null($premium) ) {
                            $switch = false;
                        }
                    }

                    //CASO DIGITALIZADOS
                    if ( $casos_especiales[1] == "1" ) {
                        $quiebre_dig = \DB::table("dig_trobas")
                        ->leftJoin("geo_trobapunto as gt", "gt.id", "=", "dig_trobas.troba_id")
                        ->leftJoin("empresas as e", "e.id", "=", "dig_trobas.empresa_id")
                        ->where(["gt.troba" => $st_cms->cod_troba, "gt.nodo" => $st_cms->cod_nodo], "e.estado", 1)
                        ->where("dig_trobas.est_seguim", "A")
                        ->where(function($query)
                        {
                            $query->where("e.cms", "=", "357")
                                  ->orWhere("e.cms", "=", "387");
                        })
                        ->first();
                        //echo "*".is_null($quiebre_dig)."*\n";
                        if( is_null($quiebre_dig) ) {
                            $switch = false;
                        }
                    }
                }
                
                foreach ( $caso->parametro_criterio as $parametro_criterio ) {

                    $nombre_criterio = $parametro_criterio->criterio->nombre_trama;

                    $detalle = explode(",", $parametro_criterio->detalle);


                    //print_r($st_cms->$nombre_criterio);
                    if ( !in_array( $st_cms->$nombre_criterio, $detalle ) ) {

                        $switch = false;

                    }

                }

                if ( $switch == true ) {
                    return $quiebre->id;
                }

            }

        //}

        return null;
    }
}