<?php namespace Legados\helpers;

use Ofsc\Activity;
class ActivityHelper
{
    public static function getImagenOfsc ($aid) {
        $aid = $aid;
        $activity = new Activity();
        $listaImagenToa = \Config::get("ofsc.image.general");
        $labels = $imagenes = [];
        $pathRelativa = 'imagenes/';
        $i=1;
        $path = \public_path($pathRelativa);
        if (isset($listaImagenToa)) {
            foreach ($listaImagenToa as $key => $value) {
                $filename  = "{$aid}-{$key}.jpg";
                $response = $activity->getFile($aid, $key);
                if (isset($response->result_code) && $response->result_code == 0) {
                    $binFile = implode('', ((array) $response->file_data));
                    \File::put($path.$filename, $binFile);
                }
            }
        }
    }

    public static function getImagenOfscByType($aid, $key) {
        $pathRelativa = "imagenes/";
        $path = \public_path($pathRelativa);
        $filename  = "{$aid}-{$key}.jpg";
        $activity = new Activity();
        $response = $activity->getFile($aid, $key);
        if (isset($response->result_code) && $response->result_code == 0) {
            $binFile = implode('', ((array) $response->file_data));
            \File::put($path.$filename, $binFile);
            return $filename;
        }

        return "";
    }

    public static function formatGetActivitiesByRest($response, $fields) {
        if( isset($response->data) ) {

            $equivalencias = [
                'activityId' => 'id',
                'customerName' => 'name',
                'resourceId' => 'resource_id',
                'apptNumber' => 'appt_number',
                'longitude' => 'coordx',
                'latitude' => 'coordy',
                'streetAddress' => 'address',
                'startTime' => 'start_time',
                'endTime' => 'end_time',
                'timeSlot' => 'time_slot',
                'timeOfBooking' => 'time_of_booking',
                'slaWindowStart' => 'sla_window_start',
                'slaWindowEnd' => 'sla_window_end',
                'serviceWindowStart' => 'service_window_start',
                'serviceWindowEnd' => 'service_window_end'
            ];

            foreach ($response->data as &$actividad) {

                $actividad = (array)$actividad;

                $new_actividad = [];

                foreach ($fields as $field) { 

                    if (!isset($actividad[$field]) ) {
                        if ($field == "date") {
                            $actividad[$field] = "3000-01-01";
                        } else if ($field == "longitude" || $field == "latitude") {
                            $actividad[$field] = '0.00000';
                        } else {
                            $actividad[$field] = "";
                        }
                    } else {
                        if ($field == "activityId" || $field == "longitude" || $field == "latitude") {
                            $actividad[$field] = (String)$actividad[$field];
                        }
                    }

                    if( isset( $equivalencias[$field] ) ) {
                        $new_actividad[ $equivalencias[$field] ] = $actividad[$field];
                    } else {
                        $new_actividad[ $field ] = $actividad[$field];
                    }
                }

                $actividad = $new_actividad;

            }

        }
        
        return $response;
    }
}
