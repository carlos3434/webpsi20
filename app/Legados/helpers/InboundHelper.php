<?php namespace Legados\helpers;

use Ofsc\Inbound;
use Ofsc\Activity;
use Hash;

use Legados\Repositories\SolicitudTecnicaRepository as STCmsI;

use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;

class InboundHelper
{
    /**
     *
     * @param {String}  $parametros:
     *        {String}  tipoEnvio            tipo de envio: agendo, sla
     *        {String}  nenvio               numero de envio
     *        {String}  fechaAgenda          fecha de cita de la actividad
     *        {String}  timeSlot             franja horaria: AM, PM
     *        {String}  quiebre              apocope de quiebre: PENDMOV1
     *        {String}  workType             tipo de trabajo: A_PROV_INS_M1
     *        {String}  workZone             zona de trabajo: LM_R015
     *        {String}  external_id          bucket de la actividad
     *        {String}  duration             Duración del trabajo según actividad (en minutos)
     *        {String}  sla                  rango de duracion de sla (en horas)
     *        {String}  grupo quiebre        nombre del grupo quiebre
     * @return {Boolean} $sla                indica si es sla o agneda
     * @return {Boolean} $estadoComponente   indica el  envio de componente
     * @return {Array}$telefonosContacto     telefonos de contacto
     * @return {Array}$response              respuesta de envio a ofsc
     */
    public static function envioOrdenOfsc($solicitud, $parametros = [], $tramaarrays = [], $inventarios = [])
    {
        $date = new \DateTime(date("c"));
        $hoy = $date->format('Y-m-d H:i:s');
        $equivalenciaTipoPaquete = [
            ''  => '',
            '00' => '',
            '01' => 'MONOPRODUCTO',
            '02' => 'DUO TV',
            '03' => 'TRIO TDP',
            '04' => 'DUO MOVISTAR UNO',
            '05' => 'TRIO MOVISTAR UNO'
        ];

        $objstimplements = new STCmsI;
        $solicitud = $objstimplements->sanearCoordenadasCms($solicitud);

        //HEAD
        $head['external_id'] = $parametros['external_id'];
        $head['date'] = $parametros['fechaAgenda'];
        //propiedades de la cita
        $cita['date'] = $parametros['fechaAgenda'];
        $cita['appt_number'] = $solicitud['num_requerimiento'];
        $cita['customer_number'] = $solicitud['cod_cliente'];
        $cita['worktype_label'] = $parametros['workType'];
        $cita['time_slot']=$parametros['timeSlot'];

        //$slaWindowStart = $hoy;
        $objfecha = \DateTime::createFromFormat('d/m/Y', $solicitud->fecha_registro_requerimiento);
        if (is_object($objfecha)) {
            $fechareq = $objfecha->format('Y-m-d');
        } else {
            $fechareq = $solicitud->fecha_registro_requerimiento;
        }


        $fechahorareq = $fechareq." ".$solicitud->hora_registro_requerimiento;
        $datesla = new \DateTime($fechahorareq);
        $hoysla =  $datesla->format('Y-m-d H:i:s');
        $slaWindowStart = $hoysla;
        $datesla->add(new \DateInterval('PT'.($parametros['sla']*60).'M'));
        $slaWindowEnd = $datesla->format('Y-m-d H:i:s');

        $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'CLI';//para sla:tel, agenda CLI
        if ($parametros['tipoEnvio']=='sla' || $parametros['tipoEnvio']=='agendasla') {
            //date: cuando es no programado es vacio
            $head['date'] = $cita['date'] = '';
            if ($parametros['tipoEnvio']=='agendasla') {
                $head['date'] = $cita['date'] = $parametros['fechaAgenda'];
            }
            $cita['sla_window_start'] = $slaWindowStart;
            $cita['sla_window_end'] = $slaWindowEnd;
            $cita['time_slot'] = null;
            $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'TEL';
        }

        //fecha que se genero la solicitud
        $cita['time_of_booking'] = $solicitud->time_of_booking;
        $cita['duration'] = $parametros['duration'];
        $cita['name'] = $solicitud->full_name;
        $cita['phone'] = $solicitud->telefono1;
        $cita['cell'] =  $solicitud->telefono2;
        $cita['email'] = null;
        $cita['address'] = $solicitud->full_address;
        $cita['city'] = $solicitud->desc_departamento;
        $cita['state'] = $solicitud->desc_provincia;
        $cita['zip'] = 'LIMA 07';
        $cita['language'] = '1';
        $cita['reminder_time'] = 15;//tiempo recordatorio sms
        $cita['time_zone'] = 19;
        $cita['coordx'] = $solicitud->coordx_cliente;
        $cita['coordy'] = $solicitud->coordy_cliente;

        $propiedades['XA_PETICION'] = $solicitud->peticion;
        $propiedades['XA_CREATION_DATE'] = $hoy;
        $propiedades['A_FEC_ULTI_MODIF'] = $hoy;
        $propiedades['XA_SOURCE_SYSTEM'] = 'PSI';
        $propiedades['XA_CUSTOMER_SEGMENT'] = isset($solicitud->segmento)? $solicitud->segmento : "";//$segmento; //revisar cuando es NO-VIP
        $propiedades['XA_CUSTOMER_TYPE'] = null;//$solicitud->desc_categoria_cliente;// no hay en fase 0,
        $propiedades['XA_CONTACT_NAME'] =  $solicitud->full_name;
        $propiedades['XA_CONTACT_PHONE_NUMBER_2'] = $solicitud->telefono1;
        $propiedades['XA_CONTACT_PHONE_NUMBER_3'] = $solicitud->telefono2;
        $propiedades['XA_CONTACT_PHONE_NUMBER_4'] = null;
        $propiedades['XA_CITY_CODE'] = 'Lima-01';
        $propiedades['XA_DISTRICT_CODE'] = $solicitud->cod_distrito;
        $propiedades['XA_DISTRICT_NAME'] =  $solicitud->desc_distrito;
        $propiedades['XA_ZONE'] =  $solicitud->zonal;
        $propiedades['XA_QUADRANT'] = $solicitud->setQuadrant();
        $addressLink = \Config::get("wpsi.geo.public.mapupdate") . $solicitud['num_requerimiento'].'/'.$solicitud['id_solicitud_tecnica'];
        $propiedades['XA_ADDRESS_LINK_HTTP'] = $addressLink;
        
        $validaLink = \Config::get("wpsi.geo.public.mapvalida") . $solicitud['num_requerimiento'].'/'.$solicitud['id_solicitud_tecnica'];
        $propiedades['XA_VALIDATION_COORDINATE_LINK'] = $validaLink;

        $propiedades['XA_WORK_ZONE_KEY'] = $parametros['workZone'];
        $propiedades['XA_RURAL'] = 0;
        $propiedades['XA_RED_ZONE'] = 0;
        $propiedades['XA_WORK_TYPE'] = $parametros['workType'];//subtipo de actividad
        if (\Session::get('full_name')) {
            $user= \Session::get('full_name');
        } else if (\Auth::user()) {
            $user= \Auth::user()->apellido.' '.\Auth::user()->nombre;
        }
        
        $propiedades['XA_USER'] = $user;
        $propiedades['XA_REQUIREMENT_NUMBER'] = $solicitud['num_requerimiento'];
        $propiedades['XA_NUMBER_SERVICE_ORDER'] = $solicitud['cod_servicio'];
        $propiedades['XA_NUMBER_WORK_ORDER'] = $solicitud['orden_trabajo'];
        $propiedades['XA_CHANNEL_ORIGIN'] = 'call';//canal de origen
        $propiedades['XA_SALES_POINT_CODE'] = "NULL";//codigo punto de venta
        $propiedades['XA_SALES_POINT_DESCRIPTION'] = "NULL";//descripcion del punto de venta
        $propiedades['XA_COMMERCIAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        $propiedades['XA_WEB_UNIFICADA'] = null;//$content;
        $propiedades['XA_ORDER_AREA'] = null;//$actuArray['area'];//area responsable de la orden
        $propiedades['XA_COMMERCIAL_PACKET'] = isset($equivalenciaTipoPaquete[$solicitud['tipo_paquete']])? $equivalenciaTipoPaquete[$solicitud['tipo_paquete']] : "" ;
        $propiedades['XA_COMPANY_NAME'] = $solicitud['desc_contrata'];
        $propiedades['XA_GRUPO_QUIEBRE'] = $parametros['quiebreGrupo'];
        $propiedades['XA_QUIEBRES'] = $parametros['quiebre'];
        $propiedades['XA_BUSINESS_TYPE'] = 'CATV';//CATV  ADSL  BASICA
        $propiedades['XA_PRODUCTS_SERVICES'] = null;
        $propiedades['XA_CURRENT_PRODUCTS_SERVICES'] = null;
        $propiedades['XA_EQUIPMENT'] = null;
        $propiedades['XA_NOTE'] = $solicitud['observacion'];
        $propiedades['A_OBSERVATION'] = (isset($solicitud["xa_observation"]))? $solicitud["xa_observation"] : "";
        $propiedades['A_CONTROL'] = date("Y-m-d");
        //$propiedades['XA_HFC_ZONE'] = $solicitud['zona_movistar_uno'];//si:1, no:0
        $propiedades['XA_HFC_TROBA'] = $solicitud["cod_troba"];
        $propiedades['XA_HFC_NODE'] = $solicitud["cod_nodo"];
        $propiedades['XA_HFC_AMPLIFIER'] = $solicitud["cod_amplificador"];
        $propiedades['XA_HFC_TAP'] = $solicitud["cod_tap"];
        $propiedades['XA_HFC_BORNE'] = $solicitud["borne"];
        $propiedades['XA_HFC_TAP_LINKHTTP'] = \Config::get("wpsi.geo.public.maptaplego") . $solicitud['num_requerimiento'].'/'.$solicitud['id_solicitud_tecnica'];
        //$propiedades['XA_MDF'] = $solicitud["mdf"];
        ////validar si es armario o cable
        //$propiedades['XA_CABLE'] = null;//cable
        //$propiedades['XA_CABINET'] = $solicitud["armario"];//armario
        //$propiedades['XA_BOX'] = null;//caja terminal
        //$propiedades['XA_TERMINAL_LINKHTTP'] = Config::get("wpsi.geo.public.mapterminal") . $solicitud['num_requerimiento'];
        //$diagnosis = explode('|', $actuArray['codmotivo_req_catv']);
        $propiedades["XA_REQUIREMENT_TYPE"] ='<XA_REQUIREMENT_TYPE><REQUIREMENT_TYPE>'.
            '<TipoRequerimiento>'.$solicitud->tipo_requerimiento.'</TipoRequerimiento>'.
            '<DescripcionTipoRequerimiento>'.$solicitud->desc_tipo_requerimiento.'</DescripcionTipoRequerimiento>'.
            '</REQUIREMENT_TYPE></XA_REQUIREMENT_TYPE>';
        $propiedades["XA_REQUIREMENT_REASON"] = $solicitud->cod_motivo_generacion;
        $propiedades['XA_TELEPHONE_TECHNOLOGY'] = "NULL";
        $propiedades['XA_BROADBAND_TECHNOLOGY'] = "NULL";
        $propiedades['XA_TV_TECHNOLOGY'] = "NULL";
        $propiedades['XA_ACCESS_TECHNOLOGY'] = 'COAXIAL';//$solicitud['tipo_tecnologia'];
        //$propiedades['XA_CATV_SERVICE_CLASS'] = $actuArray['clase_servicio_catv'];//Clase de Servicio CATV
        //$propiedades['XA_TERMINAL_ADDRESS'] = $actuArray['dir_terminal'];//direccion de terminal
        //$propiedades['XA_ADSLSTB_PREFFIX'] = $actuArray["tipo_actuacion"];
        $propiedades['XA_ADSLSTB_MOVEMENT'] = 'NULL';//acciones relizadas por el tecnico
        //$propiedades['XA_ADSL_SPEED'] = $actuArray['veloc_adsl'];//
        //$propiedades['XA_ADSLSTB_SERVICE_TYPE'] = $actuArray['tipo_servicio'];//tipo de servicio
        $propiedades['XA_PENDING_EXTERNAL_ACTION'] = 'RESCHEDULE';//solo pendiente de reagendamiento
        //$propiedades['XA_SMS_1'] = $actuArray['sms1'];
        //$propiedades['XA_SMS_2'] = null;//$actuArray['sms2']; //Property is not visible:
        //Campos adicionales para averia
        $xaDiagnosis=null;
        if ($solicitud->tipo_operacion == 'AVE_IND') {//averia
            $xaDiagnosis = 'TV_'.$solicitud->cod_motivo_generacion;
        }
        $propiedades['XA_DIAGNOSIS'] = $xaDiagnosis; //Property is not visible: validar cundo es TV_R303 TV_I115  TV_I129 TV_I128 TV_I115
        $propiedades['XA_TOTAL_REPAIRS'] = 0;
        $propiedades['XA_NUMERO_ENVIO'] = $parametros['nenvio'];
        $propiedades['XA_TERMINATION_TYPE'] = null;//1; // Property is not visible
        $propiedades['XA_TIPO_XY'] = 1;
        $propiedades['XA_SYMPTOM'] = null;//1; //Property is not visible:

        $xa_adicionales=SolicitudTecnicaHelper::propiedadCms($solicitud, $tramaarrays);
        $propiedades=array_merge($propiedades, $xa_adicionales);
        if ($parametros["actividad"] == 1) {
            $pruebaTest = \Config::get("wpsi.modem.mac").$solicitud->cod_cliente.'/'.$solicitud->num_requerimiento.'/'.$solicitud['id_solicitud_tecnica'];
            $propiedades['XA_PRUEBA_TEST_LINKHTTP'] = $pruebaTest;

            $modemsCount = 0;
            foreach ($tramaarrays['componentes'] as $key => $value) {
                if (strpos($value['descripcion'], "MODEM")) {
                    $modemsCount++;
                }
            }
            $propiedades["A_IND_REQ_PRUEBA"] = $modemsCount > 0 ? 1 : 2;
        }
        //enlace de operaciones con deco
        $userHash = str_replace('/', ' ', Hash::make('1159'));
        $link = \Config::get("wpsi.legado.operaciones").$solicitud->num_requerimiento.'/'.$userHash.'/'.$parametros['actividad'].'/1/'.$solicitud->id_solicitud_tecnica;
        $propiedades['XA_ACTIVACION_EQ'] = $link;
        if (isset($parametros['XA_IND_NOT_TIDY'])) {
            $propiedades['XA_IND_NOT_TIDY'] = $parametros['XA_IND_NOT_TIDY'];
        }
        $inbound = new Inbound();
        $response = $inbound->createActivity($head, $cita, $propiedades, $inventarios, true);

        //(3) REGISTRO PSI-TOA
        $transaccionesGoRepo = new TransaccionesGoRepo();
        $transaccionesGoRepo->registrarLogsFlujo([
            "id_solicitud_tecnica" => $solicitud->id_solicitud_tecnica,
            "flujo" => 3,
            "tipo" => 1,
            "tipo_operacion" => $solicitud->tipo_operacion
        ]);

        return $response;
    }

    public static function envioOrdenOfscProvision($solicitud, $parametros = [], $tramaarrays = [], $inventarios = [])
    {
        $date = new \DateTime(date("c"));
        $hoy = $date->format('Y-m-d H:i:s');

        //HEAD
        $head['external_id'] = $parametros['external_id'];
        $head['date'] = $parametros['fechaAgenda']; //cuando es no programado es vacio
        //propiedades de la cita
        $cita['date'] = $parametros['fechaAgenda'];
        $cita['appt_number'] = $solicitud['peticion'];
        $cita['customer_number'] = $solicitud['cliente'];
        $cita['worktype_label'] = $parametros['workType'];
        $cita['time_slot']=$parametros['timeSlot'];

        $slaWindowStart = $hoy;
        $date->add(new \DateInterval('PT'.($parametros['sla']*60).'M'));
        $slaWindowEnd = $date->format('Y-m-d H:i:s');

        $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'CLI';//para sla:tel, agenda CLI
        if ($parametros['tipoEnvio']=='sla') {
            $cita['sla_window_start'] = $slaWindowStart;
            $cita['sla_window_end'] = $slaWindowEnd;
            $cita['time_slot'] = null;
            $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'TEL';
        }

        //fecha que se genero la solicitud
        $cita['time_of_booking'] = $solicitud->time_of_booking;
        $cita['duration'] = $parametros['duration'];
        $cita['name'] = $solicitud->full_name;
        
        //TABLA HIJA DIRECCION CLIENTE
        $cita['phone'] = $solicitud->telefono_contacto;
        $cita['cell'] =  $solicitud->celular;
        $cita['email'] = null;
        $cita['address']=null;
        $cita['city']=null;
        $cita['zip'] = 'LIMA 07';
        $cita['language'] = '1';
        $cita['reminder_time'] = 15;//tiempo recordatorio sms
        $cita['time_zone'] = 19;
        $cita['coordy']=null;
        $cita['coordx']=null;
        $cita['state']=null;

        if (isset($tramaarrays['direccioncliente'])) {
            foreach ($tramaarrays['direccioncliente'] as $key => $value) {
                if ($cita['address']==null and $value['direccion_completa']!='') {
                    $cita['address']=$value['direccion_completa'];
                }
                if ($cita['city']==null and $value['desc_departamento']!='') {
                    $cita['city']=$value['desc_departamento'];
                }
                if ($cita['state']==null and $value['desc_provincia']!='') {
                    $cita['state']=$value['desc_provincia'];
                }
                if ($cita['coordx']==null and $value['coordx']!='') {
                    $cita['coordx']=$value['coordx'];
                }
                if ($cita['coordy']==null and $value['coordy']!='') {
                    $cita['coordy']=$value['coordy'];
                }
                if ($value['cod_distrito']!='') {
                    $propiedades['XA_DISTRICT_CODE'] = $value['cod_distrito'];
                }
                if ($value['desc_distrito']!='') {
                    $propiedades['XA_DISTRICT_NAME'] = $value['desc_distrito'];
                }
            }
        }

        $propiedades['XA_CREATION_DATE'] = $hoy;
        $propiedades['XA_SOURCE_SYSTEM'] = 'PSI';

        //$propiedades['XA_CUSTOMER_SEGMENT'] = null;//$segmento; //revisar cuando es NO-VIP
        $propiedades['XA_CUSTOMER_TYPE'] = null;//$solicitud->desc_categoria_cliente;// no hay en fase 0,
        $propiedades['XA_CONTACT_NAME'] =  $solicitud->full_contact;
        $propiedades['XA_CONTACT_PHONE_NUMBER_2'] = $solicitud->telefono_referencia;
        $propiedades['XA_CONTACT_PHONE_NUMBER_3'] = null;
        $propiedades['XA_CONTACT_PHONE_NUMBER_4'] = null;
        $propiedades['XA_CITY_CODE'] = 'Lima-01';
        
        $propiedades['XA_ZONE'] =  $solicitud->zonal;
        $propiedades['XA_QUADRANT'] = $solicitud->setQuadrant($cita['coordx'], $cita['coordy']);
        $propiedades['XA_ADDRESS_LINK_HTTP'] = \Config::get("wpsi.geo.public.mapofsc") . $solicitud['peticion'];
        //list($primera, $segunda) = explode('|', $actuArray['fftt']);
        $propiedades['XA_WORK_ZONE_KEY'] = $parametros['workZone'];
        $propiedades['XA_RURAL'] = 0;
        $propiedades['XA_RED_ZONE'] = 0;
        $propiedades['XA_WORK_TYPE'] = $parametros['workType'];//subtipo de actividad
        $propiedades['XA_USER'] = \Session::get('full_name');
        $propiedades['XA_REQUIREMENT_NUMBER'] = $solicitud['peticion'];
        $propiedades['XA_NUMBER_SERVICE_ORDER'] = $solicitud['numero_orden_servicio'];
        $propiedades['XA_CHANNEL_ORIGIN'] = 'call';//canal de origen
        $propiedades['XA_SALES_POINT_CODE'] = "NULL";//codigo punto de venta
        $propiedades['XA_SALES_POINT_DESCRIPTION'] = "NULL";//descripcion del punto de venta
        $propiedades['XA_COMMERCIAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;

        $propiedades['XA_WEB_UNIFICADA'] = null;//$content;
        $propiedades['XA_ORDER_AREA'] = null;//$actuArray['area'];//area responsable de la orden
        $propiedades['XA_COMMERCIAL_PACKET'] = $solicitud->tipo_paquete;
        $propiedades['XA_COMPANY_NAME'] = $solicitud->contrata;
        
        //$quiebreGrupo = QuiebreGrupo::find($actuArray['quiebre_grupo_id']);

        $propiedades['XA_GRUPO_QUIEBRE'] = $parametros['quiebreGrupo'];//isset($quiebreGrupo->nombre) ? $quiebreGrupo->nombre : null;
        $propiedades['XA_QUIEBRES'] = $parametros['quiebre'];
        $propiedades['XA_BUSINESS_TYPE'] = 'CATV';//CATV  ADSL  BASICA
        $propiedades['XA_PRODUCTS_SERVICES'] = null;
        $propiedades['XA_CURRENT_PRODUCTS_SERVICES'] = null;

        $propiedades['XA_EQUIPMENT'] = null;
        
        $propiedades['XA_NOTE'] = $solicitud['desobs'];
        $propiedades['A_OBSERVATION'] = (isset($solicitud["xa_observation"]))? $solicitud["xa_observation"] : "";
        //FFTT MODIFICAR PARA GESTEL
        //$propiedades['XA_HFC_ZONE'] = $solicitud['zona_movistar_uno'];//si:1, no:0
        $propiedades['XA_HFC_NODE'] = null;
        $propiedades['XA_HFC_TROBA'] = null;
        $propiedades['XA_HFC_AMPLIFIER'] = null;
        $propiedades['XA_HFC_TAP'] = null;
        $propiedades['XA_HFC_BORNE'] = null;
        $propiedades['A_CONTROL'] = date("Y-m-d");

        if (isset($tramaarrays['ffttcobre'])) {
            foreach ($tramaarrays['ffttcobre'] as $key => $value) {
                if ($propiedades['XA_HFC_NODE']==null and $value['cabecera_mdf']!='') {
                    $propiedades['XA_HFC_NODE']=$value['cabecera_mdf'];
                    if ($value['cod_armario'] != "") {
                        $propiedades['XA_HFC_TROBA'] = $value['cod_armario'];//ARMARIO
                    } else {
                        $propiedades['XA_HFC_TROBA'] = $value['cable']; //CABLE
                    }
                    break;
                }
            }
        }

        $propiedades['XA_TELEPHONE_TECHNOLOGY'] = "NULL";
        $propiedades['XA_BROADBAND_TECHNOLOGY'] = "NULL";
        $propiedades['XA_TV_TECHNOLOGY'] = "NULL";
        $propiedades['XA_ACCESS_TECHNOLOGY'] = 'COAXIAL';//

        $propiedades['XA_ADSLSTB_MOVEMENT'] = 'NULL';//acciones relizadas por el tecnico
        
        $propiedades['XA_PENDING_EXTERNAL_ACTION'] = 'RESCHEDULE';//solo pendiente de reagendamiento
        $xaDiagnosis=' ';
        $propiedades['XA_DIAGNOSIS'] = $xaDiagnosis;
        $propiedades['XA_TOTAL_REPAIRS'] = 0;
        $propiedades['XA_NUMERO_ENVIO'] = $parametros['nenvio'];
        $propiedades['XA_TERMINATION_TYPE'] = null;//1; // Property is not visible
        $propiedades['XA_TIPO_XY'] = 1;
        $propiedades['XA_SYMPTOM'] = null;//1; //Property is not visible:
        if (isset($parametros['XA_IND_NOT_TIDY'])) {
            $propiedades['XA_IND_NOT_TIDY'] = $parametros['XA_IND_NOT_TIDY'];
        }
        $xa_adicionales=SolicitudTecnicaHelper::propiedadGestelProvision($solicitud, $tramaarrays);
        $propiedades=array_merge($propiedades, $xa_adicionales);

        $inbound = new Inbound();
        $response = $inbound->createActivity($head, $cita, $propiedades, $inventarios, true);

        //(3) REGISTRO PSI-TOA
        $transaccionesGoRepo = new TransaccionesGoRepo();
        $transaccionesGoRepo->registrarLogsFlujo([
            "id_solicitud_tecnica" => $solicitud->id_solicitud_tecnica,
            "flujo" => 3,
            "tipo" => 1,
            "tipo_operacion" => $solicitud->tipo_operacion
        ]);

        return $response;
    }

    public static function envioOrdenOfscAveria($solicitud, $parametros = [], $tramaarrays = [], $inventarios = [])
    {
        $date = new \DateTime(date("c"));
        $hoy = $date->format('Y-m-d H:i:s');

        //HEAD
        $head['external_id'] = $parametros['external_id'];
        $head['date'] = $parametros['fechaAgenda'];
        //propiedades de la cita
        $cita['date'] = $parametros['fechaAgenda'];
        $cita['appt_number'] = $solicitud['cod_averia'];
        $cita['customer_number'] = $solicitud['cod_cliente'];
        $cita['worktype_label'] = $parametros['workType'];
        $cita['time_slot']=$parametros['timeSlot'];

        $slaWindowStart = $hoy;
        $date->add(new \DateInterval('PT'.($parametros['sla']*60).'M'));
        $slaWindowEnd = $date->format('Y-m-d H:i:s');

        $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'CLI';//para sla:tel, agenda CLI
        if ($parametros['tipoEnvio']=='sla') {
            $cita['sla_window_start'] = $slaWindowStart;
            $cita['sla_window_end'] = $slaWindowEnd;
            $cita['time_slot'] = null;
            $propiedades['XA_APPOINTMENT_SCHEDULER'] = 'TEL';
        }

        //fecha que se genero la solicitud
        $cita['time_of_booking'] = $solicitud->time_of_booking;
        $cita['duration'] = $parametros['duration'];
        $cita['name'] = $solicitud->full_name;
        
        //TABLA HIJA DIRECCION CLIENTE
        $cita['phone'] = $solicitud->telefono;
        $cita['cell'] =  $solicitud->telefono_contacto;
        $cita['email'] = null;
        $cita['address']= $solicitud->direccion_completa;
        $cita['city']= $solicitud->desc_departamento;
        $cita['state']= $solicitud->desc_provincia;
        $cita['zip'] = 'LIMA 07';
        $cita['language'] = '1';
        $cita['reminder_time'] = 15;//tiempo recordatorio sms
        $cita['time_zone'] = 19;
        $cita['coordy']= $solicitud->coordy;
        $cita['coordx']= $solicitud->coordx;

        $propiedades['XA_CREATION_DATE'] = $hoy;
        $propiedades['XA_SOURCE_SYSTEM'] = 'PSI';

        //$propiedades['XA_CUSTOMER_SEGMENT'] = null;//$segmento; //revisar cuando es NO-VIP
        $propiedades['XA_CUSTOMER_TYPE'] = null;//$solicitud->desc_categoria_cliente;// no hay en fase 0,
        $propiedades['XA_CONTACT_NAME'] =  $solicitud->nombre_contacto;
        $propiedades['XA_CONTACT_PHONE_NUMBER_2'] = $solicitud->telefono_contacto;
        $propiedades['XA_CONTACT_PHONE_NUMBER_3'] = $solicitud->telefono_referencia;
        $propiedades['XA_CONTACT_PHONE_NUMBER_4'] = null;
        $propiedades['XA_CITY_CODE'] = 'Lima-01';
        $propiedades['XA_DISTRICT_CODE'] = $solicitud->cod_distrito;
        $propiedades['XA_DISTRICT_NAME'] =  $solicitud->desc_distrito;
        $propiedades['XA_ZONE'] =  $solicitud->zonal;
        $propiedades['XA_QUADRANT'] = $solicitud->setQuadrant($cita['coordx'], $cita['coordy']);
        $propiedades['XA_ADDRESS_LINK_HTTP'] = \Config::get("wpsi.geo.public.mapofsc") . $solicitud['cod_averia'];
        //list($primera, $segunda) = explode('|', $actuArray['fftt']);
        $propiedades['XA_WORK_ZONE_KEY'] = $parametros['workZone'];
        $propiedades['XA_RURAL'] = 0;
        $propiedades['XA_RED_ZONE'] = 0;
        $propiedades['XA_WORK_TYPE'] = $parametros['workType'];//subtipo de actividad
        $propiedades['XA_USER'] = \Session::get('full_name');
        $propiedades['XA_REQUIREMENT_NUMBER'] = $solicitud['cod_averia'];
        $propiedades['XA_NUMBER_SERVICE_ORDER'] = $solicitud['numero_orden_reparacion']; //order de servicio
        $propiedades['XA_CHANNEL_ORIGIN'] = 'call';//canal de origen
        $propiedades['XA_SALES_POINT_CODE'] = "NULL";//codigo punto de venta
        $propiedades['XA_SALES_POINT_DESCRIPTION'] = "NULL";//descripcion del punto de venta
        $propiedades['XA_COMMERCIAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;
        $propiedades['XA_TECHNICAL_VALIDATION'] = 0;

        $propiedades['XA_WEB_UNIFICADA'] = null;//$content;
        $propiedades['XA_ORDER_AREA'] = null;//$actuArray['area'];//area responsable de la orden
        $propiedades['XA_COMMERCIAL_PACKET'] = null; //$solicitud->tipo_paquete;
        $propiedades['XA_COMPANY_NAME'] = $solicitud->contrata;
        
        //$quiebreGrupo = QuiebreGrupo::find($actuArray['quiebre_grupo_id']);

        $propiedades['XA_GRUPO_QUIEBRE'] = $parametros['quiebreGrupo'];//isset($quiebreGrupo->nombre) ? $quiebreGrupo->nombre : null;
        $propiedades['XA_QUIEBRES'] = $parametros['quiebre'];
        $propiedades['XA_BUSINESS_TYPE'] = 'CATV';//CATV  ADSL  BASICA
        $propiedades['XA_PRODUCTS_SERVICES'] = null;
        $propiedades['XA_CURRENT_PRODUCTS_SERVICES'] = null;

        $propiedades['XA_EQUIPMENT'] = null;
        
        $propiedades['XA_NOTE'] = $solicitud['desobs'];

        //FFTT MODIFICAR PARA GESTEL
        //$propiedades['XA_HFC_ZONE'] = $solicitud['zona_movistar_uno'];//si:1, no:0
        $propiedades['XA_HFC_NODE'] = null;
        $propiedades['XA_HFC_TROBA'] = null;
        $propiedades['XA_HFC_AMPLIFIER'] = null;
        $propiedades['XA_HFC_TAP'] = null;
        $propiedades['XA_HFC_BORNE'] = null;

        $propiedades['XA_HFC_NODE']= $solicitud->cabecera_mdf;
        if ($solicitud->cod_armario!='') {
            $propiedades['XA_HFC_TROBA'] = $solicitud->cod_armario;
        } else {
            $propiedades['XA_HFC_TROBA'] = $solicitud->cable;
        }

        $propiedades['XA_TELEPHONE_TECHNOLOGY'] = "NULL";
        $propiedades['XA_BROADBAND_TECHNOLOGY'] = "NULL";
        $propiedades['XA_TV_TECHNOLOGY'] = "NULL";
        $propiedades['XA_ACCESS_TECHNOLOGY'] = 'COAXIAL';//

        $propiedades['XA_ADSLSTB_MOVEMENT'] = 'NULL';//acciones relizadas por el tecnico
        
        $propiedades['XA_PENDING_EXTERNAL_ACTION'] = 'RESCHEDULE';//solo pendiente de reagendamiento
        $xaDiagnosis=' ';
        $propiedades['XA_DIAGNOSIS'] = $xaDiagnosis;
        $propiedades['XA_TOTAL_REPAIRS'] = 0;
        $propiedades['XA_NUMERO_ENVIO'] = $parametros['nenvio'];
        $propiedades['XA_TERMINATION_TYPE'] = null;//1; // Property is not visible
        $propiedades['XA_TIPO_XY'] = 1;
        $propiedades['XA_SYMPTOM'] = null;//1; //Property is not visible:
        $propiedades['A_CONTROL'] = date("Y-m-d");
        if (isset($parametros['XA_IND_NOT_TIDY'])) {
            $propiedades['XA_IND_NOT_TIDY'] = $parametros['XA_IND_NOT_TIDY'];
        }
        $xa_adicionales=SolicitudTecnicaHelper::propiedadGestelAveria($solicitud, $tramaarrays);
        $propiedades=array_merge($propiedades, $xa_adicionales);

        $inbound = new Inbound();
        $response = $inbound->createActivity($head, $cita, $propiedades, $inventarios, true);

        //(3) REGISTRO PSI-TOA
        $transaccionesGoRepo = new TransaccionesGoRepo();

        $transaccionesGoRepo->registrarLogsFlujo([
            "id_solicitud_tecnica" => $solicitud->id_solicitud_tecnica,
            "flujo" => 3,
            "tipo" => 1,
            "tipo_operacion" => $solicitud->tipo_operacion
        ]);

        return $response;
    }
}
