<?php
namespace Legados;

use Legados\models\EnvioLegado;
use Legados\Legado;

class PruebaElectricaService
{
    protected $_client;
    protected $_error;
    public function __construct()
    {
        $wsdl = \Config::get("legado.wsdl.analizador");
        
        try {
            $config =[
                'soap_version' => SOAP_1_1,
                'trace' => true,
                'soap_version' => SOAP_1_1,
            ];
            //$this->_client = new \nusoap_client($wsdl, true);
            $this->_client = new \nusoap_client($wsdl, true,false,false,false,false,0,200);
            //$this->_client->soap_defencoding = 'ISO-8859-1';
            //$this->_client->decode_utf8 = false;
            //$this->_client->http_encoding='ISO-8859-1';
            //$this->_client->defencoding='ISO-8859-1';
            //$this->_client->http_encoding='utf-8';
            //$this->_client->defencoding='utf-8';
            $this->_error  = $this->_client->getError();

        } catch (Exception $e) {
            
        }
    }
    /**
     * 
     */
    public function obtener($telefono)
    {
        $request = [
            'telefono'             => $telefono
        ];
        $elementos = [
            'action'                => 'obtenerPruebaTelefono',
            'telefono'             => $telefono
        ];

        $response = $this->_client->call('obtenerPruebaTelefono',$request);
        $this->tracer($elementos, $response);
        return $response;
    }
    /**
     * 
     */
    public function ejecutar($telefono, $prueba)
    {
        $request = [
            'telefono'             => $telefono,
            'prueba'                  => $prueba,
        ];
        $elementos = [
            'action'                => 'ejecutarPruebaElectrica',
            'telefono'             => $telefono,
            'prueba'                  => $prueba
        ];

        try {
            $response = $this->_client->call('ejecutarPruebaElectrica',$request);
            $this->tracer($elementos, $response);
            if ($response) {
                return $response;
            }
            //$this->debug($response);
        } catch (Exception $e) {
            
        }

    }
    /**
     * 
     */
    public function obtenerHistorico($telefono)
    {
        $request = [
            'telefono'             => $telefono
        ];
        $elementos = [
            'action'                => 'obtenerHistoricoPruebaElectrica',
            'telefono'             => $telefono
        ];

        $response = $this->_client->call('obtenerHistoricoPruebaElectrica',$request);
        $this->tracer($elementos, $response);
        return $response;
    }
    public function debug($response)
    {
        if ($this->_client->fault) {
            echo "<h2>Fault</h2><pre>";
            print_r($response);
            echo "</pre>";
        } else {
            $this->_error = $this->_client->getError();
            if ($this->_error) {
                echo "<h2>Error</h2><pre>" . $this->_error . "</pre>";
            } else {
                echo "<h2>Main</h2>";
                var_dump( $response);
            }
        }

        // show soap request and response
         
        if ($this->_client->fault) {
            echo "<h2>Fault</h2><pre>";
            print_r($response);
            echo "</pre>";
        } else {
            $this->_error = $this->_client->getError();
            if ($this->_error) {
                echo "<h2>Error</h2><pre>" . $this->_error . "</pre>";
            } else {
                echo "<h2>Main</h2>";
                var_dump( $response);
            }
        }
        echo "<h2>Request</h2>";
        echo "<pre>" . htmlspecialchars($this->_client->request, ENT_QUOTES) . "</pre>";
        echo "<h2>Response</h2>";
        echo "<pre>" . htmlspecialchars($this->_client->response, ENT_QUOTES) . "</pre>";
    }
    /**
     * @param $request array
     * @param $response array
     */
    protected function tracer($elementos = [], $response = [])
    {
        $userId = \Auth::id();
        $envio = new EnvioLegado;
        $envio->request = json_encode($elementos);
        $envio->response = json_encode($response);
        $envio->accion = $elementos["action"];
        $envio->solicitud_tecnica_id = $elementos["telefono"];
        $envio->save();
        return;
    }
}