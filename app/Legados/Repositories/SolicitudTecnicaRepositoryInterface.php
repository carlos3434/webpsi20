<?php namespace Legados\Repositories;

interface SolicitudTecnicaRepositoryInterface {
    public function sanearCoordenadasCms($solicitud);
    public function saveLogCache($solicitudTecnicaId);
    public function getLogCache($solicitudTecnicaId);
    public function saveLogCambiosCache($solicitudTecnicaId);
    public function getLogCambiosCache($solicitudTecnicaId);
    public function saveLogComunicacionLegadoCache($solicitudTecnicaId);
    public function getLogComunicacionLegadoCache($solicitudTecnicaId);
    public function validarOtActiva($numRequerimiento);
    public function consultaCapacidad($request);
}
