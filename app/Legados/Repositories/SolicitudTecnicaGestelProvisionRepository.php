<?php namespace Legados\Repositories;
use Legados\Repositories\SolicitudTecnicaRepositoryInterface;
use Legados\models\SolicitudTecnicaGestelProvision as SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\EnvioLegado;
use Ofsc\Capacity;

class SolicitudTecnicaGestelProvisionRepository implements SolicitudTecnicaRepositoryInterface
{
    public function sanearCoordenadasCms($solicitud)
    {
    }

    public function saveLogCache($solicitudTecnicaId)
    {
    }

    public function getLogCache($solicitudTecnicaId)
    {
    }

    private function getLogSt($solicitudTecnicaId)
    {
    }
    public function saveLogCambiosCache($solicitudTecnicaId)
    {
    }

    public function getLogCambiosCache($solicitudTecnicaId)
    {
    }

    private function getLogCambiosSt($solicitudTecnicaId)
    {
    }

    public function saveLogComunicacionLegadoCache($solicitudTecnicaId)
    {
    }

    public function getLogComunicacionLegadoCache($solicitudTecnicaId)
    {
    }

    private function getLogComunicacionLegadoSt($solicitudTecnicaId)
    {
    }

    public function validarOtActiva($numRequerimiento)
    {
    }

    public function consultaCapacidad($request)
    {
    }

    public function updateCampos($solicitudTecnica, $solicitud = [])
    {
        $solicitudTecnicaGestelProvision = $solicitudTecnica->gestelProvision;
        foreach (SolicitudTecnicaGestelProvision::$rules as $key => $value) {
            if (array_key_exists($key, $solicitud)) {
                $solicitudTecnicaGestelProvision[$key] = $solicitud[$key];
            }
        }
        $solicitudTecnica->gestelProvision()->save($solicitudTecnicaGestelProvision);
    }
}
