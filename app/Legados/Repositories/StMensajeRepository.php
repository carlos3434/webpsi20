<?php
namespace Legados\Repositories;

use Legados\Repositories\StOutboundRepositoryInterface;
use Legados\models\SolicitudTecnica as St;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaGestion;
use Legados\models\SolicitudTecnicaMovimientoDetalle as MovimientoDetalle;
use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;
use Ofsc\Inbound;
use Ofsc\Activity;
use Ofsc\Outbound;
use Legados\helpers\InboundHelper;
use Log;

class StMensajeRepository implements StOutboundRepositoryInterface
{
    public function sendingToa($mensaje, $request, $return, $horaLlegada = null)
    {
        $messageId = $mensaje->message_id;
        $data = array(
            'message_id' => $messageId,
            'status' => 'sent'
        );
        
        array_push($return, $data);
        $mensajeexistente = \Mensaje::where("message_id", "=", $messageId)->first();
        \Mensaje::crear(null, (array) $mensaje);

        $queue = \Config::get("keysqueue.{$mensaje->subject}");
        if (is_null($mensajeexistente)) {
            if (!is_null($queue)) {
                $push = \Queue::push('NotificacionController', array($mensaje), $queue);
            } else {
                $push = \Queue::push('NotificacionController', array($mensaje), 'outbound_notification');
            }
            
        }
        //(4) REGISTRO TOA-PSI
        
        $this->registrarLog($mensaje, $horaLlegada, '4.1', 1);
        $this->registrarLog($mensaje, null, '4.2', 1);
        return $return;
    }
    public function deliveryToa($mensaje, $inputs)
    {
        $outbound = new Outbound;
        $request =
            ['message'=>
                [
                    'message_id' => $mensaje->message_id,
                    'status' => 'delivered'
                ]
            ];
        $message = \DB::table("mensajes")
            ->select("updated_at")
            ->where("message_id", "=", $mensaje->message_id)
            ->get();

        if (isset($message[0])) {
            //$tiempoActual = strtotime(date("Y-m-d H:i:s"));
            //$tiempoUpdatedAt = strtotime($message[0]->updated_at);
            $datetime1 = new \DateTime(date("Y-m-d H:i:s"));
            $datetime2 = new \DateTime($message[0]->updated_at);
            $interval = $datetime1->diff($datetime2);
            if ($interval->format('%i') > 29) {
                $update = ['estado' => 'failed'];
                \Mensaje::where('message_id', $mensaje->message_id)
                ->update($update);
                $response = $outbound->setMessageStatus($request);
                return 1;
            }
        }
       
        $response = $outbound->setMessageStatus($request);

        if (!$response->error) {
            $code = $response->data->message_response->result->code;

            if ($code=='OK') {
                $update = ['estado' => 'delivered'];
                \Mensaje::where('message_id', $mensaje->message_id)
                ->update($update);

                //(5) REGISTRO PSI-TOA
                //$transaccionesGoRepo = new TransaccionesGoRepo();
                //$transaccionesGoRepo->registrarLogsFlujo($inputs["st"], 5, 2, null, $mensaje->message_id);
                return 1;
            }

            if ($code == "NOT FOUND") {
                $update = ['estado' => 'not_found'];
                \Mensaje::where('message_id', $mensaje->message_id)
                ->update($update);
                return 1;
            }
        }
        return 0;
    }

    public function registrarLog($mensaje, $fechaLlegada = null, $flujo, $tipo)
    {
        $transaccionesGoRepo = new TransaccionesGoRepo();
        $objMensajeBody = null;
        $ahora = \date("Y-m-d G:i:s");

        //Parseando a array porsiacaso sea un objeto
        $mensaje = (array)$mensaje;

        //Flag para registrar el mensaje si por algun motivo no se puede guardar en transaccion go detalle.
        $mensajeOk = true;
        
        if ( isset($mensaje['body']) ) {

            $mensajeBody = $mensaje['body'];
            $mensajeId = $mensaje['message_id'];
            $mensajeSendto = $mensaje['send_to'];

            try {
                $objMensajeBody = simplexml_load_string(
                    preg_replace(
                        "[\n|\r|\n\r|\t]",
                        "",
                        str_replace(["<![CDATA[","]]>"," "], "", $mensaje['body'])
                    )
                );
            } catch (Exception $e) {
                $mensajeOk = false;
            }

            if ($objMensajeBody !== null) {

                //Definiendo id_solicitud_tecnica
                $st = null;

                if (isset($objMensajeBody->xa_identificador_st)) {

                    $st = $objMensajeBody->xa_identificador_st;


                } else if (isset($objMensajeBody->IDENTIFICADOR_ST)) {

                    $st = $objMensajeBody->IDENTIFICADOR_ST;


                } else if (isset($objMensajeBody->activity_properties)) {

                    if (isset($objMensajeBody->activity_properties->xa_identificador_st)) {

                        $st = $objMensajeBody->activity_properties->xa_identificador_st;


                    } else {
                        $mensajeOk = false;
                    }

                } else {
                    $mensajeOk = false;
                }


                if( $st !== null ) {

                    $usuario = null;

                    if (isset($objMensajeBody->usuario)){
                        $usuario = $objMensajeBody->usuario;
                    }
                
                    $datos = [
                        "id_solicitud_tecnica" => $st,
                        "fechaLlegada" => $fechaLlegada,
                        "flujo" => $flujo,
                        "subject" => $mensaje['subject'],
                        "tipo" => $tipo,
                        "mensaje_id" => $mensajeId,
                        "send_to" => $mensajeSendto,
                        "usuario" => $usuario
                    ];

                    $transaccionesGoRepo->registrarLogsFlujo($datos);

                } else {
                    $mensajeOk = false;
                }

            }

            //Guardando en log el mensaje no procesado
            if( $mensajeOk == false ) {
                Log::useDailyFiles(storage_path().'/logs/test_flujo_4.log');
                Log::info([preg_replace("[\n|\r|\n\r|\t]", "", str_replace(["<![CDATA[","]]>"," "], "", $mensaje['body']))]);
            }
        }
        
    }

    public function updateProceso($mensaje, $inputs)
    {

    }
}
