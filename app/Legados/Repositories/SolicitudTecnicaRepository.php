<?php namespace Legados\Repositories;
use Legados\Repositories\SolicitudTecnicaRepositoryInterface;
use Legados\models\SolicitudTecnicaCms as STCms;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\EnvioLegado;
use Ofsc\Capacity;

class SolicitudTecnicaRepository implements SolicitudTecnicaRepositoryInterface
{
    public function sanearCoordenadasCms($solicitud)
    {
        
        if ($solicitud->coordx_cliente == "" || $solicitud->coordy_cliente == "") {
            $rangookcliente = false;
        } else {
            $rangookcliente = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_cliente, $solicitud->coordy_cliente);
        }

        if ($solicitud->coordx_direccion_tap == "" || $solicitud->coordy_direccion_tap == "") {
            $rangookfftt = false;
        } else {
            $rangookfftt = \FuncionesMatematicas::validarRangoXYLima($solicitud->coordx_direccion_tap, $solicitud->coordy_direccion_tap);
        }
        if ($rangookcliente) {
            if (!$rangookfftt) {
                $solicitud->coordx_direccion_tap = $solicitud->coordx_cliente;
                $solicitud->coordy_direccion_tap = $solicitud->coordy_cliente;

            }
            return $solicitud;
        }
        if ($rangookfftt) {
            $solicitud->coordx_cliente = $solicitud->coordx_direccion_tap;
            $solicitud->coordy_cliente = $solicitud->coordy_direccion_tap;

            return $solicitud;
        }
        /*
        ** Consulto a la BD brindada por el Equipo de Averias
        */
        $coordenadascliente = \DB::table("webpsi_coc.tb_planta_catv")
            ->where("Servicio", $solicitud->cod_servicio)
            ->first();
        if (is_null($coordenadascliente)) {
            $coordenadascliente = null;
        }
        if (!is_null($coordenadascliente)) {
            $rangookfftt = \FuncionesMatematicas::validarRangoXYLima($coordenadascliente->numcoo_x, $coordenadascliente->numcoo_y);
            if ($rangookfftt) {
                $solicitud->coordy_cliente = $coordenadascliente->numcoo_y;
                $solicitud->coordx_cliente = $coordenadascliente->numcoo_x;
                $solicitud->coordx_direccion_tap = $coordenadascliente->numcoo_x;
                $solicitud->coordy_direccion_tap = $coordenadascliente->numcoo_y;
                
                return $solicitud;
            }
        }
        $zonal = $solicitud->zonal;
        $nodo = $solicitud->cod_nodo;
        $troba = ($solicitud->cod_troba == "")? $solicitud->cod_plano : $solicitud->cod_troba;
        $tap = $solicitud->cod_tap;
        $amplificador = $solicitud->cod_amplificador;
        /*
        ** Consulto de Coordenadas de Troba
        */
        $coordenadasfftt = \GeoTrobaPunto::where(
            [
                "zonal" => $zonal,
                "nodo" => $nodo,
                "troba" => $troba
            ]
        )->first();
        if (!is_null($coordenadasfftt)) {
            $rangonodo = \FuncionesMatematicas::validarRangoXYLima($coordenadasfftt->coord_x, $coordenadasfftt->coord_y);
            if ($rangonodo) {
                $solicitud->coordy_cliente = $coordenadasfftt->coord_y;
                $solicitud->coordx_cliente = $coordenadasfftt->coord_x;
                $solicitud->coordx_direccion_tap = $coordenadasfftt->coord_x;
                $solicitud->coordy_direccion_tap = $coordenadasfftt->coord_y;

                return $solicitud;
            }
        }
        /*
        ** Consulto de Coordenadas de Tap
        */
        $coordenadasfftt = \GeoTap::where(
            [
                "zonal" => $zonal,
                "nodo" => $nodo,
                "troba" => $troba,
                "tap" => $tap,
                "amplificador" => $amplificador,
                "orden" => 1
            ]
        )->first();
        if (!is_null($coordenadasfftt)) {
            $rangotap = \FuncionesMatematicas::validarRangoXYLima($coordenadasfftt->coord_x, $coordenadasfftt->coord_y);
            if ($rangotap) {
                $solicitud->coordy_cliente = $coordenadasfftt->coord_y;
                $solicitud->coordx_cliente = $coordenadasfftt->coord_x;
                $solicitud->coordx_direccion_tap = $coordenadasfftt->coord_x;
                $solicitud->coordy_direccion_tap = $coordenadasfftt->coord_y;

                return $solicitud;
            }
        }
        return $solicitud;
    }

    public function saveLogCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        \Cache::forget("logst{$solicitudTecnicaId}");
        \Cache::remember(
            "logst{$id}",
            1*60*60,
            function () use ($id) {
                return $this->getLogSt($id);
            }
        );
    }

    public function getLogCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        $logst = \Cache::get("logst{$id}");
        if (!$logst) {
            $logst = \Cache::remember(
                "logst{$id}",
                1*60*60,
                function () use ($id) {
                    return $this->getLogSt($id);
                }
            );
        }
        return $logst;
    }

    private function getLogSt($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        $logrequest = [];
        $log = STLogRecepcion::select(
            "solicitud_tecnica_log_recepcion.*",
            "le.message AS detalle_error"
        )->where(["solicitud_tecnica_id" => $id])
                ->leftJoin(
                    "lego_errores AS le",
                    "le.code",
                    "=",
                    "solicitud_tecnica_log_recepcion.code_error"
                )->groupBy("id")
            ->orderBy("id", "DESC")
            ->get()
            ->toArray();
        return $log;
    }
    public function saveLogCambiosCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        \Cache::forget("logcambiosst{$solicitudTecnicaId}");
        \Cache::remember(
            "logcambiosst{$id}",
            1*60*60,
            function () use ($id) {
                return $this->getLogCambiosSt($id);
            }
        );
    }

    public function getLogCambiosCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        $logst = \Cache::get("logcambiosst{$id}");
        if (!$logst) {
            $logst = \Cache::remember(
                "logcambiosst{$id}",
                24*60*60,
                function () use ($id) {
                    return $this->getLogCambiosSt($id);
                }
            );
        }
        return $logst;
    }

    private function getLogCambiosSt($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        return \DB::table('solicitud_tecnica_log as l')
                ->leftjoin(
                    'usuarios as u',
                    'l.usuario_created_at',
                    '=',
                    'u.id'
                )->select(
                    'l.id',
                    'antes',
                    'despues',
                    'u.usuario',
                    'l.created_at'
                )
            ->where('solicitud_tecnica_id', $id)
            ->orderBy('l.created_at', 'desc')
            ->get();
    }

    public function saveLogComunicacionLegadoCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        \Cache::forget("logcomunicacionlegadost{$id}");
        \Cache::remember(
            "logcomunicacionlegadost{$id}",
            24*60*60,
            function () use ($id) {
                return $this->getLogComunicacionLegadoSt($id);
            }
        );
    }

    public function getLogComunicacionLegadoCache($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        $logst = \Cache::get("logcomunicacionlegadost{$id}");
        if (!$logst) {
            $logst = \Cache::remember(
                "logcomunicacionlegadost{$id}",
                24*60*60,
                function () use ($id) {
                    return $this->getLogComunicacionLegadoSt($id);
                }
            );
        }
        return $logst;
    }

    private function getLogComunicacionLegadoSt($solicitudTecnicaId)
    {
        $id = $solicitudTecnicaId;
        return EnvioLegado::select("solicitud_tecnica_id", "request", "response", "accion", "u.nombre", "u.apellido", "envio_legado.created_at", "envio_legado.id")
                ->leftjoin("usuarios as u", "u.id", "=", "envio_legado.usuario_created_at")
                ->where(["solicitud_tecnica_id" => $id])
                ->orderBy("envio_legado.id", "DESC")
                ->get()
                ->toArray();
    }

    public function validarOtActiva($numRequerimiento)
    {
        $existe = false;
        $num_requerimiento = Ultimo::where('num_requerimiento', $numRequerimiento)
            ->where("estado_aseguramiento", "<", 6)
            ->where("estado_st", "=", 1)
            ->first();
        if (!is_null($num_requerimiento)) {
            $existe = true;
        } else {
            $num_requerimiento = Ultimo::where("num_requerimiento", $numRequerimiento)
                ->where("estado_ofsc_id", "=", 5)
                ->first();
            if (!is_null($num_requerimiento)) {
                $existe = false;
            } else {
                $existe = true;
            }
        }
        return $existe;
    }

    public function consultaCapacidad($request)
    {
        $objquiebre = \Quiebre::find($request->quiebre_id);
        $workType = "";
        $fecha = $request->fecha;
        if (!is_null($objquiebre)) {
            $objquiebreGrupo = $quiebre->quiebregrupos;
            $actividadTipo = \ActividadTipo::find($request->actividad_tipo_id);
            if (!is_null($actividadTipo)) {
                $workType = $actividadTipo->apocope;
            }
            $workZone = $request->microzona;
            $data=[
                'fecha'          => $fecha,
                'time_slot'      => '',
                'worktype_label' => $workType,
                'quiebre'        => $quiebre->apocope,
                'zona_trabajo'   => $workZone,
            ];
            $objcapacity = new Capacity();
            $response = $objcapacity->getCapacity($data);
            $movimiento = [];
            if (isset($response->error) && $response->error===false
                && isset($response->data->capacity)) {
                $capacidad = $response->data->capacity;
                $timeSlot = $response->data->time_slot_info;
            } else {
                
            }
        }
        
    }
}
