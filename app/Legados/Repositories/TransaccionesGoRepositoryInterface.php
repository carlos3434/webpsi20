<?php

namespace Legados\Repositories;

interface TransaccionesGoRepositoryInterface {
    public function registrarLogsFlujo($datos);
}
