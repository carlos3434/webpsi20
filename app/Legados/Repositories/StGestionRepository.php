<?php
namespace Legados\Repositories;

use Legados\Repositories\StGestionRepositoryInterface;
use Legados\models\SolicitudTecnica as St;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaGestion;
use Legados\models\SolicitudTecnicaMovimientoDetalle as MovimientoDetalle;
use Ofsc\Inbound;
use Ofsc\Activity;
use Legados\helpers\InboundHelper;
use Legados\models\SolicitudCaida;

class StGestionRepository implements StGestionRepositoryInterface
{
    public function envioOfsc($solicitudTecnicaId, $inputs)
    {
        $aid = $resourceId = null;
        $msjError = $msjErrorOfsc = '';
        $capacidad = true;
        $tipoEnvioOfsc = 1;
        $horario = 0;
        $tramaarrays = [];
        $inventarios = [];
        $actividadTipoId    = $inputs["actividad_tipo_id"];
        $actividadId        = $inputs["actividad_id"];
        $tematicos          = isset($inputs["tematicos"]) ? $inputs["tematicos"] : [];
        $observacionToa    = isset($inputs["observacion_toa"]) ? $inputs["observacion_toa"] : "";
        $solicitudTecnicaId = isset($inputs["solicitud_tecnica_id"]) ? $inputs["solicitud_tecnica_id"] : 0;
        // $ffttcobre          = isset($inputs["ffttcobre"]) ? $inputs["ffttcobre"] : [];
        $agdsla             = $inputs["agdsla"];
        $hf                 = $inputs["hf"];
        $quiebreId          = $inputs["quiebre"];

        $solicitudTecnica = St::find($solicitudTecnicaId);
        $ultimo = $solicitudTecnica->ultimo;
        if ($ultimo->tipo_legado == 1) {
            $requerimiento = $solicitudTecnica->cms;
        } elseif ($ultimo->tipo_legado == 2) {
            if ($ultimo->actividad_id == 2) {
                $requerimiento = $solicitudTecnica->gestelprovision;
            } else {
                $requerimiento = $solicitudTecnica->gestelaveria;
            }
        }
        
        $quiebre = \Quiebre::find($quiebreId);
        if (is_null($quiebre)) {
            $respuesta = [
                'rst'                   => 2,
                'solicitud_tecnica_id'  => $solicitudTecnica->id,
                'msj'                   => "Tipo de Actividad Sin Quiebre"
            ];
            return $respuesta;
        }

        $quiebreGrupo = $quiebre->quiebregrupos;
        $actividadTipo = \ActividadTipo::find($actividadTipoId);
        if (is_null($actividadTipo)) {
            $respuesta = [
                'rst'                   => 2,
                'solicitud_tecnica_id'  => $solicitudTecnica->id,
                'msj'                   => "Tipo de Actividad No Definida"
            ];
            return $respuesta;
        }

        $parametros = [
            'fechaAgenda'       => '',
            'external_id'       => '',
            'timeSlot'          => '',
            'intervalo_slot'    => ''
        ];
        
        list(
            $parametros['fechaAgenda'],
            $parametros['external_id'],
            $parametros['timeSlot'],
            $parametros['intervalo_slot']
        ) = explode("||", $hf);

        $diaSemana = date("N", strtotime($parametros['fechaAgenda']));

        if (isset($inputs["XA_IND_NOT_TIDY"])) {
            $parametros['XA_IND_NOT_TIDY'] = $inputs["XA_IND_NOT_TIDY"];
        }

        $parametros['tipoEnvio'] = $agdsla;
        if ($parametros['tipoEnvio'] == 'sla' || $parametros['tipoEnvio'] == 'agendasla') {
            $tipoEnvioOfsc = 2;
            $horario = 0;
        } else {
            if (isset($inputs["cupos"]) && $ultimo->tipo_legado == 1) { // solo cms
                $horario = \Horario::where('horario', '=', $parametros['timeSlot'])->first()->id;
                $capacidad = $requerimiento->getValidaCupos($inputs["cupos"], $parametros);
            }
        }

        $actuacion = [];
        if ($capacidad) {
            $parametros['nenvio']       = isset($inputs["nenvio"]) ? $inputs["nenvio"] : $ultimo->nenvio+1;
            $parametros['quiebre']      = $quiebre->apocope;
            $parametros['quiebreGrupo'] = $quiebreGrupo->nombre;
            $parametros['workType']     = $actividadTipo->label;
            $parametros['duration']     = $actividadTipo->duracion;
            $parametros['sla']          = $actividadTipo->sla;
            $parametros['actividad']    = $actividadId;
            $parametros['tipo_legado']  = $ultimo->tipo_legado;
            $parametros['workZone']     = $requerimiento->getWorkZone();

            $prefijo_go = ( $observacionToa != "" ) ? ( $ultimo->xa_observation != "" ? "," : "" ) . "GO:" : "";
            $ultimo->xa_observation.= $prefijo_go . $observacionToa;
            $requerimiento->xa_observation = substr(\Helpers::swapString($ultimo->xa_observation), 0, 49);

            if ($ultimo->tipo_legado == 1) {
                /*
                * Cancelamos cuando se trata de un
                * predevuelto
                */
                if ($ultimo->estado_aseguramiento == 3) {
                    $actuacion['estado_aseguramiento'] = 1;
                    $api = new Activity();
                    $response = $api->cancelActivity($ultimo->aid, []);
                }
                $inventarios = [];
                //$inventarios=$requerimiento->getMateriales($ultimo->actividad_id);
                $tramaarrays['componentes'] = $requerimiento->getComponentes();
                $response = InboundHelper::envioOrdenOfsc($requerimiento, $parametros, $tramaarrays, $inventarios);
            } elseif ($ultimo->tipo_legado == 2) {
                $inventarios = [];
                //$inventarios=$requerimiento->getMateriales();
                if ($ultimo->actividad_id == 2) {
                    $tramaarrays["ffttcobre"] = $requerimiento->ffttcobre;
                    $tramaarrays["direccioncliente"] = $requerimiento->direccioncliente;
                    $response = InboundHelper::envioOrdenOfscProvision($requerimiento, $parametros, $tramaarrays, $inventarios);
                }
                if ($ultimo->actividad_id == 1) {
                    $response = InboundHelper::envioOrdenOfscAveria($requerimiento, $parametros, $tramaarrays, $inventarios);
                }
            }

            $actuacion['tipo_envio_ofsc']   = $tipoEnvioOfsc;
            $actuacion['actividad_id']      = $actividadId;
            $actuacion['actividad_tipo_id'] = $actividadTipoId;
            $actuacion['quiebre_id']        = $quiebreId;
            $actuacion['nenvio']            = $parametros['nenvio'];
            $actuacion['fecha_agenda']      = $parametros['fechaAgenda'];
            $actuacion['dia_id']            = $diaSemana;
            $actuacion['horario_id']        = $horario;
            $actuacion['intervalo']         = $parametros['intervalo_slot'];
            $actuacion['motivo_ofsc_id']    = null;
            $actuacion['submotivo_ofsc_id'] = null;
            //$movimiento['idultimo']          = $ultimo->id;

            if($response===false) {
                
                $msjError="Error coneccion TOA";
                $rst = 2;
                
                $solicitud_caida = new SolicitudCaida;
                $solicitud_caida::where('solicitud_tecnica_id','=',$solicitudTecnicaId);
                $array['created_at'] = date("Y-m-d G:i:s");
                $array['usuario_created_at'] = \Auth::user()->id;
                $solicitud_caida->update($array);

                $rst = 2;
                $actuacion['estado_ofsc_id'] = 8;//no enviado
                $actuacion['xa_observation'] = $observacionToa."|".$msjError;

            }else{
                if (isset($response->data->data->commands->command->appointment->aid)) {
                    $aid = $response->data->data->commands->command->appointment->aid;
                    if (isset($response->data->data->commands->command->external_id)) {
                        $resourceId = $response->data->data->commands->command->external_id;
                    }
                    $msjError = "Envio a Correcto a OFSC";
                    $rst=1;

                    //actualiza ultimo y movimiento
                    $actuacion['aid']               = $aid;
                    $actuacion['estado_ofsc_id']    = 1;//pendiente
                    $actuacion['estado_aseguramiento'] = 1;
                    $actuacion['resource_id']       = $resourceId;
                    $actuacion['xa_observation']    = $observacionToa;

                    $bucket = \Bucket::where('bucket_ofsc', $resourceId)->first();
                    $actuacion['bucket_id'] = isset($bucket->id) ? $bucket->id : "";
                } else {
                    if (isset($response->data->data->commands->command->appointment->report->message)) {
                        if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                            $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
                        } else {
                            $mensajes=$response->data->data->commands->command->appointment->report->message;
                            foreach ($mensajes as $key => $value) {
                                if (isset($value->result)) {
                                    if ($value->result=="error" || $value->result=="warning") {
                                        $msjErrorOfsc=$value->description;
                                    }
                                }
                            }
                        }
                    }
                    $rst = 2;
                    $msjError = "Envio a OFSC: FAIL|".$msjErrorOfsc;
                    //$codError='ERR22';//go debe reenviar
                    $actuacion['estado_ofsc_id'] = 8;//no enviado
                    $actuacion['xa_observation'] = $observacionToa."|".$msjError;
                }
                
                //borrado logico de tabla solicitudes_caidas
                $solicitud_caida = new SolicitudCaida;
                $sol=$solicitud_caida::where('solicitud_tecnica_id','=',$solicitudTecnicaId);
                $sol->delete();
            }

            $actuacion["tipo_legado"] = $ultimo->tipo_legado;
            $actuacion["actividad_id"] = $ultimo->actividad_id;
            $inputs['tipodetalle'] = 5;
            $inputs["comentario"] = $observacionToa;
            $movimientook = $this->registrarMovimiento($solicitudTecnica, $actuacion, $inputs);
            if (!$movimientook) {
                $rst = 2;
                $msjError.= ". Error al Registrar los Movimientos de Gestion";
            }
        } else {
            $rst = 2;
            $msjError = 'Se agotaron los cupos para la Fecha y Horario seleccionado';
        }

        $respuesta = [
            'rst'                   => $rst,
            //'id_solicitud_tecnica'  => $idSolicitudTecnica,
            'solicitud_tecnica_id'  => $solicitudTecnica->id,
            'msj'                   => $msjError
        ];


        return $respuesta;
    }

    public function stopOrden($solicitudTecnicaId, $inputs)
    {
        $response = ["rst" => 1, "error" => "", "msj" => "OK"];
        $movimiento = [];
        $comentario = $inputs["comentario"];
        $tipoDevolucion = $inputs["tipoDevolucion"];
        $tipodetalle = null;
        $tematicos = $inputs["tematicos"];
        $motivodevolucion = $inputs["motivodevolucion"];
        $motivoliquidacion = $inputs["motivoliquidacion"];
        $submotivoDevolucion = $inputs["submotivoDevolucion"];

        if ($tipoDevolucion == "C") {
            $tipodetalle = 3;
        } elseif ($tipoDevolucion == "T") {
            $tipodetalle = 4;
        }

        $solicitud = St::with("ultimo")->find($solicitudTecnicaId);
        if (!is_null($solicitud)) {
            $ultimo = $solicitud["ultimo"];
            if (!is_null($ultimo)) {
                if ($ultimo->actividad_id == 1) {
                    $tipodetalle = 6;
                }
                if ($ultimo->estado_aseguramiento == 4 || $ultimo->estado_aseguramiento == 3) {
                    if ($motivodevolucion !="") {
                        $estado = $ultimo->estado_aseguramiento == 3 ? 4 : 6;
                        $objmotivoofsc = \MotivoOfsc::find($motivodevolucion);
                        if (!is_null($objmotivoofsc)) {
                            $movimiento['motivo_ofsc_id'] = $objmotivoofsc->id;
                        }
                    }

                    //submotivos
                    $submotivoofsc = null;
                    if ($submotivoDevolucion!='') {
                        $submotivoofsc = $submotivoDevolucion;
                    }
                    $movimiento['submotivo_ofsc_id'] = $submotivoofsc;

                }
                $movimiento['xa_observation'] = $ultimo->xa_observation." | ".$comentario;
                $objinbound = new Inbound;
                $responsetoa = $objinbound->updateActivity($ultimo->num_requerimiento, [], [], "notdone_activity");

                if (isset($responsetoa->result) && $responsetoa->result=="success") {
                    $movimiento['estado_ofsc_id'] = 4;
                    $movimiento['estado_aseguramiento'] = 5;
                    $tecnico = \Tecnico::find($ultimo->tecnico_id);
                    if (!is_null($tecnico)) {
                        $celular = $tecnico->celular;
                        $mensaje = "Req. ".$ultimo->num_requerimiento." Puede continuar con la siguiente orden";
                        \Sms::enviar($celular, $mensaje, '1');
                    }
                    $responsepsi["msj"] = "Cierre posterior exitoso";
                    if ($ultimo->actividad_id == 2) {
                        $response["msj"] = "Operación exitosa";
                    }
                    $movimiento['xa_observation'] .= " | ".$response["msj"];
                } else {
                    $tecnico = \Tecnico::find($ultimo->tecnico_id);
                    if (!is_null($tecnico)) {
                        $celular = $tecnico->celular;
                        $mensaje = "Req. ".$ultimo->num_requerimiento."No se pudo cerrar la orden en TOA, comuniquese con su Despacho";
                        \Sms::enviar($celular, $mensaje, '1');
                    }
                    $errorToa = isset($responsetoa->errorMsg)? $responsetoa->errorMsg : ($responsetoa->description)? $responsetoa->description : 'Error no Catalogado de TOA';
                    $response["rst"] = 2;
                    $response["msj"] = "No se pudo cerrar la orden en TOA. Error TOA : ".$errorToa;
                    $response["error"] = isset($responsetoa->code) ? $responsetoa->code : 'Error no Catalogado de TOA';
                    $movimiento['xa_observation'] .= " | ".$response["msj"];
                }
                $inputs['tipodetalle'] = $tipodetalle;
                $movimientook = $this->registrarMovimiento($solicitud, $movimiento, $inputs);
                if ($movimientook) {
                    return $response;
                }
                $response["rst"] = 2;
                $response["msj"] = "Se Stopeo Correctamente. Error al Registrar los Movimientos de Gestion";
                $response["error"] = "";
                return $response;
            }
            $response["rst"] = 2;
            $response["msj"] = "Error al buscar la Ultima Gestion de la Solicitud Tecnica";
            $response["error"] = "";
            return $response;
        }
        $response["rst"] = 2;
        $response["msj"] = "Error al buscar la Solicitud Tecnica";
        $response["error"] = "";
        return $response;
    }

    public function resetformulariotoa($solicitudTecnicaId, $inputs)
    {
        $solicitud = St::where('id', $solicitudTecnicaId)
                            ->orderBy("updated_at", "DESC")
                            ->first();
  
        $comentario = $inputs["comentario"];
        $tematicos = $inputs["tematicos"];
        $tipoDevolucion = $inputs["tipoDevolucion"];
        $tipodetalle = null;
        $properties = [];
        $response = ["rst" => 1, "error" => "", "msj" => ""];
        
        $ultimo = $solicitud->ultimo;
        if (!is_null($solicitud)) {
            if (!is_null($ultimo)) {
                $objinbound = new Inbound;
                $properties["A_IND_PRUEBA"] = 2;
                $properties["A_IND_REQ_PRUEBA"] = 2;
                if ($ultimo->estado_aseguramiento == 3) { // pre devolucin
                    $properties['A_IND_PRE_CIERRE_DEV'] = 3;
                    if ($ultimo->actividad_id == 1) {
                        $properties['A_NOT_DONE_AREA'] = "";
                        $properties['A_NOT_DONE_REASON_REPAIR'] = "";
                    } elseif ($ultimo->actividad_id == 2) {
                        $properties['A_NOT_DONE_TYPE_INSTALL'] = "";
                        $properties['A_NOT_DONE_REASON_INSTALL'] = "";
                    }
                    $properties['A_OBSERVATION'] = "";

                    if ($tipoDevolucion == "C") {
                        $tipodetalle = 3;
                    } elseif ($tipoDevolucion == "T") {
                        $tipodetalle = 4;
                    }
                } elseif ($ultimo->estado_aseguramiento == 4) { // pre completar
                    $properties['A_IND_PRE_CIERRE_COM'] = "";
                    $properties['A_RECEIVE_PERSON_NAME'] = "";
                    $properties['A_RECEIVE_PERSON_ID'] = "";
                    if ($ultimo->tipo_liquidacion == 1) {
                        $properties['A_IND_CASA_CERRADA'] = "";
                        $properties['A_CH_COMPLETE_CAUSA_REP_CBL'] = "";
                        $properties['A_CH_COMPLETE_REP_CBL'] = "";
                        $properties['A_CH_OBSERVATION'] = "";
                        $movimiento['tipo_liquidacion'] = null;
                    }
                    if ($ultimo->actividad_id == 1) {
                        $properties['A_COMPLETE_CAUSA_REP_STB'] = "";
                        $properties['A_COMPLETE_REP_STB'] = "";
                        $properties['A_COMPLETE_CAUSA_REP_ADSL'] = "";
                        $properties['A_COMPLETE_REP_ADSL'] = "";
                        $properties['A_COMPLETE_CAUSA_REP_SAT'] = "";
                        $properties['A_COMPLETE_REP_SAT'] = "";
                        $properties['A_COMPLETE_CAUSA_REP_CBL'] = "";
                        $properties['A_COMPLETE_REP_CBL'] = "";
                    } elseif ($ultimo->actividad_id == 2) {
                        $properties['A_COMPLETE_REASON_INSTALL'] = "";
                        $properties['A_COMPLETE_REP_CBL'] = "";
                        $properties['A_IND_CASA_CERRADA'] = "";
                    }
                    $properties['A_OBSERVATION'] = "";
                }
                if ($ultimo->actividad_id == 1) {
                    $tipodetalle = 6;
                }

                $properties['A_OBSERVATION'] = $comentario;
                $movimiento['xa_observation'] = $comentario;
                $responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties);

                if (isset($responseToa->result) && $responseToa->result=="success") {
                    $tecnico = \Tecnico::find($ultimo->tecnico_id);
                    if (!is_null($tecnico)) {
                        $celular = $tecnico->celular;
                        $mensaje = "";
                        if ($ultimo->estado_aseguramiento == 3) {
                            $mensaje = "Req. ".$ultimo->num_requerimiento.", No se acepta la Devolución, verifica la instalación";
                        }
                        if ($ultimo->estado_aseguramiento == 4) {
                            $mensaje = "Req. ".$ultimo->num_requerimiento.", No se acepta el Cierre";
                        }
                        
                        \Sms::enviar($celular, $mensaje, '1');
                        \Sms::enviar($celular, $comentario, '1');
                    }
                    $response["msj"] = "Formulario Reseteado con Exito";
                    $movimiento['xa_observation'] .= " | ".$response["msj"];
                } else {
                    $tecnico = \Tecnico::find($ultimo->tecnico_id);
                    if (!is_null($tecnico)) {
                        $celular = $tecnico->celular;
                        $mensaje = "Req. ".$ultimo->num_requerimiento.", Error en TOA, comuniquese con el Despacho";
                        \Sms::enviar($celular, $mensaje, '1');
                    }
                    if (isset($responseToa->errorMsg)) {
                        $errorToa = $responseToa->errorMsg;
                    } elseif (isset($responseToa->description)) {
                        $errorToa = $responseToa->description;
                    } else {
                        $errorToa = 'Error no Catalogado de TOA';
                    }

                    $response["rst"] = 2;
                    $response["msj"] = "Error TOA : ".$errorToa;
                    $response["error"] = isset($responseToa->code) ? $responseToa->code : 'Error no Catalogado de TOA';
                    $movimiento['xa_observation'] .= " | ".$response["msj"];
                }

                $movimiento['estado_aseguramiento'] = 1;
                $inputs['tipodetalle'] = $tipodetalle;
                $movimientook = $this->registrarMovimiento($solicitud, $movimiento, $inputs);
                if ($movimientook) {
                    return $response;
                }

                $response["rst"] = 2;
                $response["msj"] = "Se Stopeo Correctamente. Error al Registrar los Movimientos de Gestion";
                $response["error"] = "0013";
                return $response;
            } else {
                $response["rst"] = 2;
                $response["msj"] = "Error al buscar la Ultima Gestion de la Solicitud Tecnica";
                $response["error"] = "0014";
                return $response;
            }
        }

        $response["rst"] = 2;
        $response["msj"] = "Error al buscar la Solicitud Tecnica";
        $response["error"] = "0013";
        return $response;
    }

    public function registrarMovimiento($solicitud, $movimiento, $inputs)
    {

        try {
            $ultimo = $solicitud->ultimo;
            //$ultimo_array = [];
            foreach (Ultimo::$rules as $key => $value) {
                if (array_key_exists($key, $movimiento)) {
                    $ultimo[$key] = $movimiento[$key];
                }
            }
            $solicitud->ultimo()->save($ultimo);

            $ultimoMovimiento = $solicitud->ultimoMovimiento->replicate();
            foreach (Movimiento::$rules as $key => $value) {
                if (array_key_exists($key, $movimiento)) {
                    $ultimoMovimiento[$key] = $movimiento[$key];
                }
            }
            $ultimoMovimiento->save();
            if (isset($inputs["tematicos"])) {
                if (count($inputs['tematicos']) > 0) {
                    $solicitudTecnicaGestion = new SolicitudTecnicaGestion();
                    $data = [
                        "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                        "comentarios" => $inputs['comentario'],
                        "tipo" => $inputs['tipodetalle']
                    ];
                    $save = $solicitudTecnicaGestion->generarMovimientoconTematico($inputs['tematicos'], [], $data);
                }
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    

    public function getPendientesOfsc($bucket = "", $fecha = "")
    {
        $objactivity = new Activity;
        $fecha = ($fecha == "")? date("Y-m-d") : $fecha;
        $fin = date("Y-m-d", strtotime("{$fecha} +7 days"));
        $actividades = $objactivity->getActivities($bucket, $fecha, $fin);
        $resultado = [];
        if (isset($actividades->data)) {
            foreach ($actividades->data as $key => $value) {
                if ($value["status"]!="cancel") {
                    $resultado[] = $value;
                }
            }
        }
        return $resultado;
    }

    public function liquidacionToa($solicitud, $inputs = [])
    {
        $objinbound = new Inbound;
        $ultimo = $solicitud->ultimo;
        $observacion = isset($inputs["observacion"])? $inputs["observacion"] : "";
        $motivoofsc = isset($inputs["motivoofsc"])? $inputs["motivoofsc"] : $ultimo->motivoOfsc;
        
        $properties = [];
        $actuacion = [];
        $response = [];
        //$properties["A_OBSERVATION"] = $ultimo->xa_observation." | ".$observacion;
        $properties["A_IND_PRE_CIERRE_COM"] = 2;
        if ($ultimo->actividad_id == 1) { // Averia
            if ($motivoofsc->tipo_tecnologia == 1) {// CATV
                $properties["A_COMPLETE_CAUSA_REP_CBL"] = $motivoofsc->codigo_ofsc;
            } elseif ($motivoofsc->tipo_tecnologia == 2) {// SPEEDY
                $properties["A_COMPLETE_CAUSA_REP_ADSL"] = $motivoofsc->codigo_ofsc;
            } elseif ($motivoofsc->tipo_tecnologia == 3) {// BASICA
                $properties["A_COMPLETE_CAUSA_REP_STB"] = $motivoofsc->codigo_ofsc;
            }
        } elseif ($ultimo->actividad_id == 2) {
            $properties["A_COMPLETE_REASON_INSTALL"] = $motivoofsc->codigo_ofsc;
        }

        $actuacion['xa_observation'] = $ultimo->xa_observation." | ".$observacion;
        $responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties, [], "complete_activity");
        if (isset($responseToa->result) && $responseToa->result == "success") {
            $actuacion['estado_ofsc_id'] = 6;
            $actuacion['estado_aseguramiento'] = isset($inputs["estado_aseguramiento"])? $inputs["estado_aseguramiento"] : 6;
            $response = ["rst" => 1];
        } else {
            $error = isset($responseToa->errorMsg)? $responseToa->errorMsg:'error desconocido en TOA';
            $response["rst"] = 2;
            $response["msj"] = $error;
            $actuacion['xa_observation'] .= " | ".$error;
        }
        $this->registrarMovimiento($solicitud, $actuacion, $inputs);
        return $response;
    }

    public function registrarMovimientoDetalle($solicitud, $datosMovimiento)
    {
        try {
            $estadoOfscId = 2;//pendiente
            if ($datosMovimiento['subject'] == 'TOA_OUTBOUND_AUT_SUSPEND' || $datos_movimiento['subject'] == 'TOA_OUTBOUND_AUT_COORDENADAS') {
                $estadoOfscId = 1;//iniciada
            }

            $movimiento_ultimo = Movimiento::where('solicitud_tecnica_id', '=', $solicitud->id)
                                        ->where(
                                            function ($q) use ($estadoOfscId, $datosMovimiento) {
                                                $q->where('estado_ofsc_id', '=', $estadoOfscId);
                                                if ($datosMovimiento['subject'] == 'TOA_OUTBOUND_AUT_SUSPEND') {
                                                    $q->orwhere('estado_ofsc_id', '=', 2);
                                                }
                                            }
                                        )
                                            ->orderBy('updated_at', 'desc')
                                            ->first();
            unset($datosMovimiento['subject']);

            if ($movimiento_ultimo) {
                $ultimo = $solicitud->ultimo;
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $datos_movimiento)) {
                        $ultimo[$key] = $datosMovimiento[$key];
                    }
                }
                $solicitud->ultimo()->save($ultimo);
                
                $movimiento_ultimo->update($datosMovimiento);
                $datosMovimiento['tipo'] = 10;
                $datosMovimiento['solicitud_tecnica_movimiento_id'] = $movimiento_ultimo->id;
                $movimiento_detalle = MovimientoDetalle::create($datosMovimiento);
            }
            return true;

        } catch (Exception $e) {
            return false;
        }

    }
}