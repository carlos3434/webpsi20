<?php namespace Legados\Repositories;

interface StOutboundRepositoryInterface
{
    public function sendingToa($mensaje, $request, $return);
    public function deliveryToa($mensaje, $inputs);
    public function updateProceso($mensaje, $inputs);
}
