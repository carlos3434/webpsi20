<?php namespace Legados\Repositories;

interface StGestionRepositoryInterface
{
    public function envioOfsc($solicitud, $inputs);
    public function stopOrden($solicitud, $inputs);
    public function resetformulariotoa($solicitud, $inputs);
    public function registrarMovimiento($solicitud, $movimiento, $inputs);
    public function liquidacionToa($solicitud, $inputs);
}
