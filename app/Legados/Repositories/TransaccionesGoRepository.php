<?php

namespace Legados\Repositories;

use Legados\Repositories\StLogsRepositoryInterface;

class TransaccionesGoRepository implements TransaccionesGoRepositoryInterface
{

    /*

    $flujo

    1-> CMS -> PSI (Llegada de solicitudes)
    2-> CMS <- PSI (Respuesta a CMS del estado de la solicitud)
    3-> PSI -> TOA (Inbound)
    4-> PSI <- TOA (Outbound)
    6-> CMS <- PSI (Cierre)

    ----------------------------------

    $tipo

    1 -> Sincrono

    2 -> Asincrono

    */

    public function registrarLogsFlujo($datos) {

        date_default_timezone_set('America/Lima');

        //print_r($datos);
        //exit;
        $id_solicitud_tecnica = strval($datos["id_solicitud_tecnica"]);
        //echo 2; exit;

        $flujo = isset($datos["flujo"]) ? $datos["flujo"] : null;
        $tipo = isset($datos["tipo"]) ? $datos["tipo"] : null;
        
        $mensaje_id = isset($datos["mensaje_id"]) ? $datos["mensaje_id"] : null;
        $send_to = isset($datos["send_to"]) ? $datos["send_to"] : null;
        $subject = isset($datos["subject"]) ? $datos["subject"] : null;
        $fechaLlegada = isset($datos["fechaLlegada"]) ? $datos["fechaLlegada"] : null;
        $usuario = isset($datos["usuario"]) ? $datos["usuario"] : null;
        $tipo_operacion = isset($datos["tipo_operacion"]) ? $datos["tipo_operacion"] : null;

        //Consiguiendo el tipo de operacion
        if($tipo_operacion === null) {
            $st_cms = \DB::table("solicitud_tecnica")->select(["tipo_operacion"])->where("id_solicitud_tecnica", $id_solicitud_tecnica)->first();

            if($st_cms) {
                $tipo_operacion = $st_cms->tipo_operacion;
            } 
        }

        //Evaluando si es averia o provision        
        $actividad_id = null;

        switch ( $tipo_operacion ) {
            case 'AVE_IND':
                $actividad_id = 1;
                break;

            case 'RUT_PRV':
            case 'ALT_PRV':
                $actividad_id = 2;
                break;
        }

        $cabecera = \DB::table('transaccion_go')
            ->where('flujo', $flujo)
            ->where('actividad_id', $actividad_id)
            //->increment('cantidad', 1);
            ->update([
                'cantidad' => \DB::raw('cantidad + 1'),
                'updated_at' => ($fechaLlegada !== null) ? $fechaLlegada : date('Y-m-d G:i:s')
            ]);

        if (!$cabecera) {
            $cabecera_id = \DB::table("transaccion_go")->insertGetId([
                "flujo" => $flujo,
                "tipo" => $tipo,
                "actividad_id" => $actividad_id,
                "cantidad" => 1,
                "created_at" => ($fechaLlegada !== null) ? $fechaLlegada : date('Y-m-d G:i:s'),
                "updated_at" => ($fechaLlegada !== null) ? $fechaLlegada : date('Y-m-d G:i:s')
            ]);
        }

        $detalle = [
            "transaccion_go_id" => \DB::table('transaccion_go')->select('id')->where('flujo', $flujo)->where('actividad_id', $actividad_id)->pluck('id'),
            "id_solicitud_tecnica" => $id_solicitud_tecnica,
            "flujo" => $flujo,
            "mensaje_id" => $mensaje_id,
            "created_at" => ($fechaLlegada !== null) ? $fechaLlegada : date('Y-m-d G:i:s'),
            "send_to" => $send_to,
            "subject" => $subject,
            "usuario_toa" => $usuario
        ];

        \DB::table("transaccion_go_detalle")->insert($detalle);
    }
}