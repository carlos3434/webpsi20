<?php
namespace Legados\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MensajesCsvContador extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'msj_csv_contador';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera csv de contadores ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        date_default_timezone_set('America/Lima');



        $mensajes = \DB::table("mensajes")->select("estado","subject")->whereRaw( "date(created_at) = curdate() and hour(created_at) = ".(date("G")-1) )->get();

        $contenido = "Asunto,Estado,Contador\n";

        $a_contadores = [];

        foreach ($mensajes as $mensaje) {

            if( isset( $a_contadores[ $mensaje->subject."-".$mensaje->estado ] ) ) {

                $a_contadores[$mensaje->subject."-".$mensaje->estado]["contador"]++;
            
            } else {
                $a_contadores[$mensaje->subject."-".$mensaje->estado] = [
                    "contador" => 1,
                    "subject" => $mensaje->subject,
                    "estado" => $mensaje->estado
                ];
            }

        }

        foreach ($a_contadores as $a_contador) {
            $contenido .= $a_contador["subject"].",".$a_contador["estado"].",".$a_contador["contador"]."\n";
        }

        $ruta = "/mensajes_csv_contador/reporte_" . date("Y-m-d G:i:s") .".csv";

        file_put_contents( public_path() . $ruta, $contenido);
    }
}
