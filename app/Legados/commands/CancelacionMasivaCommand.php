<?php 
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as ST;
use Ofsc\Activity;


class CancelacionMasivaCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:cancelacionmasiva';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un csv de las STs en general.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
       \Auth::loginUsingId(697);//proceso automatico
    $solicitudes = Ultimo::where("estado_aseguramiento", "=", 9)->where("estado_ofsc_id", "=", 1)->where("actividad_id", "=", 1)->get();
    $api = new Activity();
                   
    foreach ($solicitudes as $key => $value) {
        $movimiento = [];
         $response = $api->cancelActivity($value->aid, []);
            if (isset($response->data->data->commands->command->appointment->aid)) {
                $aid = $response->data->data->commands->command->appointment->aid;
                if (isset($response->data->data->commands->command->external_id)) {
                    $resourceId = $response->data->data->commands->command->external_id;
                }
                $movimiento['estado_ofsc_id']    = 5;//pendiente
                echo "cancele";
            } else {
                if (isset($response->data->data->commands->command->appointment->report->message)) {
                    if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                        $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
                    } else {
                        $mensajes=$response->data->data->commands->command->appointment->report->message;
                        foreach ($mensajes as $key => $value) {
                            if (isset($value->result)) {
                                if ($value->result=="error" || $value->result=="warning") {
                                    $msjErrorOfsc=$value->description;
                                }
                            }
                        }
                    }
                }
                echo "no cancele";
            }
        $solicitudTecnica = ST::find($value->solicitud_tecnica_id);
        $ultimo = $solicitudTecnica->ultimo;
                      
                      foreach (Ultimo::$rules as $key => $value) {
                          if (array_key_exists($key, $movimiento)) {
                              $ultimo[$key] = $movimiento[$key];
                          }
                      }
                      $solicitudTecnica->ultimo()->save($ultimo);


                      $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                      foreach (Movimiento::$rules as $key => $value) {
                          if (array_key_exists($key, $movimiento)) {
                              $ultimoMovimiento[$key] = $movimiento[$key];
                          }
                      }
                      $ultimoMovimiento->save();
    }
    \Auth::logout();

    }
}
