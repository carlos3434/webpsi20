<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaLogRecepcion as Log;
use Legados\models\EnvioLegado as LogLego;
use Legados\models\SolicitudTecnica as ST;
use Configuracion\models\Parametro;
use DB;


class DescargaMasivaSolicitudesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:descargamasivasolicitudes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lee un json con las tramas de las solicitudes y emula el input desde el wsdl de generar_st del servidor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $array['tipo']=5; //
        $array['estado']=1;
        $array['proveniencia']=2; //CMS
        $parametros = Parametro::getParametroValidacion($array);

        $date = date("Y-m-d");
        $fechaCreacion[0] = strtotime('-7 day', strtotime($date));
        $fechaCreacion[0] = date('Y-m-d', $fechaCreacion[0]);
        $fechaCreacion[1] = date("Y-m-d");


        $datos = Ultimo::from('solicitud_tecnica as st')
                ->select(
                    'stlr.trama'
                )
                ->join(
                    'solicitud_tecnica_log_recepcion as stlr',
                    'stlr.solicitud_tecnica_id',
                    '=',
                    'st.id'
                )
                ->join(
                    'solicitud_tecnica_cms as stc',
                    'stc.solicitud_tecnica_id',
                    '=',
                    'st.id'
                )
                ->whereBetween(DB::raw("date(stlr.created_at)"), $fechaCreacion);

        foreach ($parametros as $key => $value) {
            $trama = explode("||", $value->trama);
            $detalle = explode("||", $value->detalle);

            for ($i = 0; $i < count($trama); $i++) {
                echo $trama[$i].": ".$detalle[$i]."\n";
                $valuesIn = array();
                $valuesIn = explode(",", $detalle[$i]);
                $datos->whereIn("stc.{$trama[$i]}", $valuesIn);
            }
        }
        $datos = $datos->get();
        // print_r($datos);
        // print_r(DB::getQueryLog());
        if (count($datos) > 0) {
            $array = [];
            foreach ($datos as $value2) {
                $array[] = json_decode($value2->trama, true);
            }

            $fileName = "datasolicitudes.json";
            $rutaFolder = public_path()."/json/solicitudes_masiva";
            $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
            $rutaFile = $rutaFolder."/".$fileName;
            if ($existeFolder === true) {
                if (\File::exists($rutaFile)) {
                    unlink($rutaFile);
                }
                \File::put($rutaFile, json_encode($array));
                chmod($rutaFile, 0777);
                echo "Json Generado con solicitudes, número de st: ".count($datos);
            }
        } else {
            echo "No se encontraron registros";
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            // array('bucket_ofsc', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
