<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;
use Ofsc\Activity;
use Helpers;

class CruceLiquidadasCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    //php artisan ofsc:cruce_liquidadas
    protected $name = 'ofsc:cruce_liquidadas';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Crea csv de cruce de liquidadas';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function fire()
    {   
        $time_start = microtime(true);
        date_default_timezone_set('America/Lima');
        echo "----------------------------------------\nInicio: " . date("Y-m-d G:i:s") . "\n";
        
        $rutaFolder = public_path()."/csv/cruce_liquidadas";
        $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
        
        if ( $existeFolder === true ) {
            $created_at=date("Y-m-d G:i:s");
            $fileName = 'Cruceliq_'.date("Y").'_'.date("m").'_'.date("d").'_'.date("G").'_'.
            date("i").'_'.date("s").".csv";
            $rutaFile = $rutaFolder."/".$fileName;

            if (!\File::exists($rutaFile)) {
                
                $activity  = new Activity();
                $from      = date("Y-m-d");
                $to        = date("Y-m-d");

                $data=DB::select("SELECT DISTINCT tmp3.num_requerimiento,
                DATE_FORMAT(tmp3.fch_liquidada_cms,'%d/%m/%y') AS fch_liquidada_cms,
                (SELECT MAX(t.`fec_completado`) FROM psi_backup.liquidado_toa t 
                WHERE tmp3.`num_requerimiento`=t.`num_requerimiento`) AS fch_liquidada_toa FROM 
                (SELECT tmp2.num_requerimiento,tmp2.fch_liquidada_cms FROM (
                    SELECT tmp.num_requerimiento,tmp.fecha_liquidacion AS fch_liquidada_cms,
                    toa.fec_completado AS fch_liquidada_toa FROM (
                        SELECT cms.`codigoreq` AS num_requerimiento,DATE(cms.`fecha_liquidacion`)
                        AS fecha_liquidacion
                        FROM psi_backup.aver_liq_catv_pais AS cms 
                        INNER JOIN psi_backup.liquidado_toa AS toa ON cms.`codigoreq`=toa.`num_requerimiento`
                    )tmp 
                    LEFT JOIN psi_backup.liquidado_toa toa ON tmp.num_requerimiento=toa.`num_requerimiento` AND
                    CONCAT(SUBSTR(tmp.`fecha_liquidacion`,9,2),SUBSTR(tmp.`fecha_liquidacion`,6,2),SUBSTR(tmp.`fecha_liquidacion`,1,4))=
                    CONCAT(SUBSTR(toa.`fec_completado`,1,2),SUBSTR(toa.`fec_completado`,4,2),SUBSTR(toa.`fec_completado`,7,4))
                )tmp2 
                WHERE IFNULL(tmp2.fch_liquidada_toa,'')='')tmp3
                INNER JOIN psi_backup.liquidado_toa t ON tmp3.`num_requerimiento`=t.`num_requerimiento`;");

                $cabeceras = ["num_requerimiento","fch_liquidada_cms","fch_liquidada_toa"];
                $contenido = json_decode(json_encode($data),true);
                \Helpers::createExcel($contenido, $rutaFile, 'CSV', $cabeceras);

            }
        }

        echo "\n file ".$fileName." creado exitosamente! \n";
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo "\nFin: " . date("Y-m-d G:i:s");
        echo "\nTiempo de ejecución: " . $execution_time . " Seg. \n----------------------------------------\n";
    }

}
