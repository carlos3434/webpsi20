<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;
use Ofsc\Activity;
use Helpers;

class XlsSolicitudesProceso_7diasCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    //php artisan ofsc:xls_solicitudes_proceso_7dias
    protected $name = 'ofsc:xls_solicitudes_proceso_7dias';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Crea xls de solicitudes que se encuentran en proceso 
    ( rango fechas: - 7 dias (Hoy) + 7 dias )';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function fire()
    {
        $time_start = microtime(true);
        date_default_timezone_set('America/Lima');
        echo "----------------------------------------\nInicio: " . date("Y-m-d G:i:s") . "\n";
        

        $created_at=date("Y-m-d G:i:s");

        $activity  = new Activity();
        $from      = date("Y-m-d"/*, strtotime("- 7 days")*/);
        $to        = date("Y-m-d"/*, strtotime("+ 7 days")*/);

        $buckets=DB::table('bucket')
                ->select('bucket_ofsc')
                ->whereRaw("estado=1 and type='BK'")
                ->distinct()
                ->get();

        $cabecera=[];
        $truncate=DB::statement('TRUNCATE TABLE reporte_solicitudesenproceso_toa7dias');
        $rango_fechas=$from.' al '.$to.' (7 dias) ';
        foreach ($buckets as $key => $value) {
            $bucket=$value->bucket_ofsc;
            $actividades = $activity->getActivities($bucket, $from, $to);
            if (isset($actividades->data)) {
                if (count($actividades->data) > 0) {
                    
                    $data = $actividades->data;
                    foreach ($data[0] as $key2 => $value2) {
                        if(!in_array($key2, $cabecera)) {
                            $cabecera [] = $key2;
                        }
                    }
                    $cabecera [] = "bucket";
                    $cabecera [] = "estado_actual";
                    $cabecera [] = "fecha_liquidacion_toa";

                    foreach ($data as $k => $val) {
                        $v=(implode("|", $val)).'|'.$bucket;
                        $valor=explode("|", $v);
                        $content_schdle_sist= $this->buscarEnTabla($val['appt_number']);

                        $valor[23] = $content_schdle_sist['estado_actual_cms'];
                        $valor[24] = $content_schdle_sist['fecha_liquidacion_cms'];

                        $valor[1] = str_replace(',', '', $valor[1]);
                        $valor[5] = str_replace(',', '', $valor[5]);
                        $valor[6] = str_replace(',', '', $valor[6]);
                        $valor[7] = str_replace(',', '', $valor[7]);

                        DB::insert(
                        'INSERT into reporte_solicitudesenproceso_toa7dias(
                        id,name,status,resource_id,appt_number,coordx,coordy,address,date,
                        start_time,end_time,time_slot,time_of_booking,sla_window_start,sla_window_end,service_window_start,service_window_end,xa_appintment_scheduler,a_asignacion_manual_flag,xa_work_zone_key,a_control,xa_identificator_st,bucket,
                        estado_actual_cms,fecha_liquidacion_cms,rango_fechas,created_at) 
                        values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                            [
                            $valor[0],$valor[1],$valor[2],$valor[3],$valor[4],$valor[5],
                            $valor[6],$valor[7],$valor[8],$valor[9],$valor[10],$valor[11],
                            $valor[12],$valor[13],$valor[14],$valor[15],$valor[16],$valor[17],
                            $valor[18],$valor[19],$valor[20],$valor[21],$valor[22],$valor[23],
                            $valor[24],$rango_fechas,$created_at]
                        );
                    }
                }
            }   
        }

        echo "\n Proceso terminado correctamente! \n";
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo "\nFin: " . date("Y-m-d G:i:s");
        echo "\nTiempo de ejecución: " . $execution_time . " Seg. \n----------------------------------------\n";
    }


    public function buscarEnTabla($num_requerimiento) {

        $tablas= ['schedulle_sistemas.prov_pen_catv_pais',
                  'schedulle_sistemas.aver_pen_catv_pais',
                  'schedulle_sistemas.prov_liq_catv_pais',
                  'schedulle_sistemas.aver_liq_catv_pais'];

        $valor = ['estado_actual_cms'=> 'no_encontrada', 'fecha_liquidacion_cms' =>''];
        $encontrado = "no se encontro  \n";
        
        for ($i=0; $i<4; $i++) {
            $columna = 'codigo_req';
            if($tablas[$i] == 'schedulle_sistemas.aver_liq_catv_pais') {
                $columna = 'codigoreq';
            }

            $query_resultado = \DB::table($tablas[$i])
                                ->where($columna, $num_requerimiento)
                                ->first();
            $tipo2 = substr($tablas[$i], 24, 3);

            if($query_resultado){
                if ($i == 0 || $i == 1){
                    $valor['estado_actual_cms'] = 'pendiente';
                    $valor['fecha_liquidacion_cms'] = '';
                }
                elseif ($i == 2 || $i == 3) {
                    $valor['estado_actual_cms'] = 'liquidada';
                    $valor['fecha_liquidacion_cms'] = $query_resultado->fecha_liquidacion;
                }

            } 
            
        }
        
        return $valor;
    }
}
