<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GestionesStPorDiaCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:gestionesstdia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un csv de las STs en general.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //-----------------------------------------------------------------------------------
        $ayer = date("Y-m-d", strtotime("-1 day", strtotime( date("Y-m-d") ) ) );
        //$ayer = "2018-01-24";
        echo "Reporte del dia: ".$ayer."\n";

        $reporte_db = \DB::select('

        SELECT
            st_custom.*,

            st_m.created_at as mov_fecha_registro,

            st_m.id AS mov_id,

            mov_detalle.id AS mov_detalle_id,

            e_a.nombre AS mov_situacion,

            e_ofsc.nombre AS mov_estado_toa,

            REPLACE(REPLACE(REPLACE(REPLACE(m_ofsc.descripcion,CHAR(9),""),CHAR(10),""),CHAR(13),""),"/","") AS mov_motivo,
            REPLACE(REPLACE(REPLACE(REPLACE(sub_m_ofsc.descripcion,CHAR(9),""),CHAR(10),""),CHAR(13),""),"/","") AS mov_sub_motivo,

            tecnico.nombre_tecnico AS mov_nombre_tecnico,

            tecnico.carnet AS mov_carnet_tecnico,

            mov_detalle.usuario_autorizacion AS mov_usuario_toa,

            mov_detalle.fecha_autorizacion AS mov_fecha_autorizacion,

            tipo_autorizacion.descripcion AS mov_tipo_autorizacion,

            replace( replace( mov_detalle.campo1, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico,
            replace( replace( mov_detalle.campo2, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n1,
            replace( replace( mov_detalle.campo3, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n2,
            replace( replace( mov_detalle.campo4, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n3,

            bk.nombre AS mov_bucket,

            mov_detalle.coordx_tecnico AS mov_coord_x_tecnico,
            mov_detalle.coordy_tecnico AS mov_coord_y_tecnico,

            st_m.xa_observation AS mov_observacion_tecnico_toa

            FROM (
                SELECT
                    st_cms.id_solicitud_tecnica AS solicitud_tecnica,

                    st_cms.num_requerimiento AS codigo_requerimiento,

                    st_cms.cod_cliente  AS codigo_cliente_cms,

                    st_cms.cod_servicio  AS codigo_servicio_cms,

                    CONCAT( st_cms.fecha_registro_requerimiento,  " ",  st_cms.hora_registro_requerimiento )  AS fecha_registro_cms,

                    st_cms.created_at  AS fecha_llegada_psi,

                    (
                        SELECT created_at FROM solicitud_tecnica_movimiento
                        WHERE  solicitud_tecnica_id = st_cms.solicitud_tecnica_id
                        AND estado_ofsc_id = 2
                        ORDER BY created_at ASC
                        LIMIT 1
                    ) AS fecha_llegada_toa,

                    (
                        SELECT created_at FROM solicitud_tecnica_movimiento
                        WHERE  solicitud_tecnica_id = st_cms.solicitud_tecnica_id
                        AND (estado_ofsc_id = 6 OR estado_ofsc_id = 4)
                        ORDER BY created_at DESC
                        LIMIT 1
                    ) AS fecha_liquidacion_toa,

                    st_u.fecha_agenda AS fecha_agenda,

                    estado_aseguramiento.nombre AS situacion_psi,

                    IF (
                        st_u.estado_st = 1,
                        "OK",
                        IF (
                            st_u.estado_st = 2,
                            "TRABAR LEGADO",
                            "ERROR TRAMA"
                        )
                    )  AS estado_solicitud,

                    IF(
                        st_u.estado_st = 1,
                        "Agenda",
                        IF(
                            st_u.estado_st = 2,
                            "SLA",
                            ""
                        )
                    ) AS tipo_agenda,

                    st_cms.coordx_cliente AS coord_x_cliente,

                    st_cms.coordy_cliente AS coord_y_cliente,

                    st_u.solicitud_tecnica_id,

                    st_u.tipo_legado AS legado,

                    st_u.actividad_id AS actividad

                FROM solicitud_tecnica_cms AS st_cms

                INNER JOIN solicitud_tecnica_ultimo AS st_u ON st_u.solicitud_tecnica_id = st_cms.solicitud_tecnica_id

                LEFT JOIN estados_aseguramiento AS estado_aseguramiento ON st_u.estado_aseguramiento = estado_aseguramiento.id

                WHERE DATE(st_u.updated_at) = "'.$ayer.'"

            ) AS st_custom

            LEFT JOIN solicitud_tecnica_movimiento AS st_m ON st_m.solicitud_tecnica_id = st_custom.solicitud_tecnica_id

            LEFT JOIN estados_ofsc AS e_ofsc ON e_ofsc.id = st_m.estado_ofsc_id

            LEFT JOIN motivos_ofsc AS m_ofsc ON m_ofsc.id = st_m.motivo_ofsc_id

            LEFT JOIN submotivos_ofsc AS sub_m_ofsc ON sub_m_ofsc.id = st_m.submotivo_ofsc_id

            LEFT JOIN solicitud_tecnica_movimiento_detalle AS mov_detalle ON mov_detalle.solicitud_tecnica_movimiento_id = st_m.id

            LEFT JOIN bucket AS bk ON bk.id = st_m.bucket_id

            LEFT JOIN tipo_autorizacion ON tipo_autorizacion.id = mov_detalle.tipo_autorizacion_id

            LEFT JOIN estados_aseguramiento AS e_a ON e_a.id = st_m.estado_aseguramiento

            LEFT JOIN tecnicos AS tecnico ON st_m.tecnico_id = tecnico.id

            ORDER BY solicitud_tecnica, st_m.created_at DESC,  mov_detalle.created_at DESC
        ');

        // Aqui se manejaria la data de la cabecera

        $padres = [];

        $v_tipo_legado = [ 1 => "CMS", 2 => "GESTEL" ];
        $v_actividad = [ 1 => "AVERIA", 2 => "PROVISION" ];

        foreach ($reporte_db as $solicitud) {

            $solicitud_array = json_decode( json_encode($solicitud), true );

            if( !isset($padres[$solicitud->legado."_".$solicitud->actividad]) ) {

                $ruta_csv = "/reportes_st_diarios/reporte_st_" .
                            $v_tipo_legado[ $solicitud->legado ] .
                            "_" .
                            $v_actividad[ $solicitud->actividad ] .
                            "_" .
                            $ayer .
                            ".csv";

                $padres[ $solicitud->legado."_".$solicitud->actividad ] = [

                    "legado" => $solicitud->legado,
                    "actividad_id" => $solicitud->actividad,
                    "cantidad" => [],
                    "fecha" => $ayer,
                    "created_at" => $ayer,
                    "csv_location" => $ruta_csv,
                    "contenido_csv" => implode( ",", array_keys($solicitud_array) ) . "\n"
                ];
            }

            $padres[$solicitud->legado."_".$solicitud->actividad]["cantidad"][$solicitud->solicitud_tecnica] = 0;
            $padres[$solicitud->legado."_".$solicitud->actividad]["contenido_csv"] .= implode( ",", $solicitud_array ) . "\n";
        }

        foreach ($padres as &$padre) {

            if ( !file_exists( public_path() . $padre["csv_location"] ) ) {
                file_put_contents( public_path() . $padre["csv_location"], $padre["contenido_csv"] );
            }

            unset($padre["contenido_csv"]);

            $padre["cantidad"] = sizeof($padre["cantidad"]);

            $padre["id"] = \DB::table("comandos_gestiones_stpordia")->insertGetId($padre);

        }

    }
}