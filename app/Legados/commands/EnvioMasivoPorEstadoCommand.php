<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\Repositories\SolicitudTecnicaRepository as STRepo;

class EnvioMasivoPorEstadoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legado:enviomasivoporestado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lee un json con las tramas de las solicitudes y emula el input desde el wsdl de generar_st del servidor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $estado = !is_null($this->option('estado'))? $this->option("estado") : 8;
        $fechaEnvio = !is_null($this->option('fecha_envio'))? $this->option('estado') : date("Y-m-d");

        $i = 0;
        $solicitudes = Ultimo::where(["estado_ofsc_id" => $estado, "estado_st" => 1])->get();
        foreach ($solicitudes as $key => $value) {
            $objst = $value->solicitudTecnica;
            if (!is_null($objst)) {
                $objcms = $objst->cms;
                if (!is_null($objcms)) {
                    $objrepo = new STRepo;
                    $request = new \stdClass;
                    $request->quiebre_id = $value->quiebre_id;
                    $request->actividad_tipo_id = $value->actividad_tipo_id;
                    $request->microzona = $objcms->getWorkZone();
                    
                }
            }
            
        }
        echo "\n se han procesado {$i} solicitudes";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('estado', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('fecha_envio', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
