<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\EnvioLegado;
use Legados\STCmsApi;

class EnvioMasivoLegadoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'lego:reenviolegado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reenviar a legado cada vez que haiga un error de data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \Auth::loginUsingId(697);//proceso automatico
        $objApiST = new STCmsApi;
        $accion = !is_null($this->option('accion'))? $this->option('accion') : "";
        $solicitud = !is_null($this->option('solicitud'))? $this->option('solicitud') : "";
        $coincidencia = !is_null($this->option('coincidencia'))? $this->option('coincidencia') : "";
        $fechaInicio = !is_null($this->option('fecha_inicio'))? $this->option('fecha_inicio') : date("Y-m-d H:i:s");
        $fechaFin = !is_null($this->option('fecha_fin'))? $this->option('fecha_fin') : date("Y-m-d H:i:s");
        $envioLegado = EnvioLegado::from('envio_legado as el')
                ->select(
                    'el.*'
                )->leftJoin("solicitud_tecnica AS st", "st.id", "=", "el.solicitud_tecnica_id")
                ->whereBetween("el.created_at", [$fechaInicio, $fechaFin]);
        if ($accion != "") {
            $envioLegado->where("el.accion", "=", $accion);
        }
        if ($coincidencia != "") {
            $envioLegado->whereRaw("el.response LIKE '%".$coincidencia."%' ");
        }
        if ($solicitud != "") {
            $envioLegado->where("st.id_solicitud_tecnica", "=", $solicitud);
        }
        $envioLegado = $envioLegado->orderBy("el.id", "DESC")->get();
        $intentos = [];
        if (!empty($envioLegado)) {
            foreach ($envioLegado as $key => $value) {
                $request = json_decode($value['request']);
                switch ($value['accion']) {
                    case 'RptaEnvioSolicitudTecnica':
                        $data = [
                            'id_solicitud_tecnica' => $request->NumeroSolicitud,
                            'id_respuesta' => $request->IndicadorRespuesta,
                            'observacion' => $request->Observacion,
                            'solicitud_tecnica_id' => $value['solicitud_tecnica_id']
                        ];
                        $objApiST->responderST($data);
                        echo "st === ".$request->NumeroSolicitud."\n";
                        $intentos[] = $request->NumeroSolicitud;
                        break;
                    case 'CierreSolicitudTecnica':
                        $cierre = json_decode(json_encode($request), true);
                        $cierre['componentes'] = $cierre['componente'];
                        unset($cierre['componente']);
                        $cierre['solicitud_tecnica_id'] = $value['solicitud_tecnica_id'];
                        $objApiST->cierre($cierre);
                        echo "st === ".$cierre['id_solicitud_tecnica']."\n";
                        $intentos[] = $cierre['id_solicitud_tecnica'];
                        break;
                    default:
                        break;
                }
            }
        }
        \Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_enviolego_intentos.log');
        \Log::info([$intentos]);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('accion', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('fecha_inicio', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('fecha_fin', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('coincidencia', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('solicitud', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
