<?php
namespace Legados\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ActualizarSubjectHistorico extends Command
{

	protected $name = 'legado:actualizarsubject';


	protected $description = '';	

	public function __construct()
    {
        parent::__construct();
    }


    public function fire()
    {

    	echo "iniciando  \n";
    	$transacionesSolicitudesId = \DB::table('psi_backup.transaccion_go_detalle as tgd')
    									->join('psi.mensajes as m', 'm.message_id', '=', 'tgd.mensaje_id')
    									->whereRaw("tgd.subject IS NULL")
    									->whereIn('tgd.flujo', ['4.1', '4.2', '4.3'])
    									//->lists('mensaje_id', 'id');
    									->limit(1500)
    									->get(['tgd.id', 'm.subject']);


      	//dd($transacionesSolicitudesId);
    	foreach ($transacionesSolicitudesId as $key => $transaccion) {
    		

    		/*BD::statement("CREATE TEMPORARY TABLE IF NOT EXIST temp_table  AS ( 
    			SELECT `id`,  `subject` from `psi_backup`.`transaccion_go_detalle` as tgd 
    			left join   `psi`.`mensajes` as m on  `tgd`.`mensaje_id`  =   `m`.`message_id` 
    			where  `tgd`.`flujo`  in ('4.1', '4.2', '4.3')) 
    			where  `tgd`.`subject` is not null
    			limit 10000");*/

    				//update psi_backup.transaccion_go_detalle   set subject = m.subject from  psi_backup.transaccion_go_detalle tgd  leftjoin psi.mensajes m on m.message_id  = tgd.mensaje_id;  

			//$subject = \DB::table('mensajes')->where('message_id', $transaccion->mensaje_id)->pluck('subject');
			\DB::table('psi_backup.transaccion_go_detalle')->where('id', $transaccion->id)->update(['subject' => $transaccion->subject]);
				
			echo $key . " \n";
			echo $transaccion->id."  \n";
			break;


		}

    	echo "fin :) \n";

    }



    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('fecha', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}