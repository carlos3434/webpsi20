<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;

class ObtenerSolicitudesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:obtenersolicitudes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generar json de solicitudes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $bucket_ofsc = $this->option('bucket_ofsc');
        $buckets = !is_null($bucket_ofsc)? \Bucket::where("bucket_ofsc", "=", $bucket_ofsc)->get() : \Bucket::all();
        $fechaInicio = !is_null($this->option('fecha_inicio'))? $this->option('fecha_inicio') : date("Y-m-d");
        $fechaFin = !is_null($this->option('fecha_fin'))? $this->option('fecha_fin') : date("Y-m-d");
        foreach ($buckets as $key => $value) {
            $datos = Ultimo::from('solicitud_tecnica_ultimo as stu')
                        ->select(
                            'stlr.trama',
                            'stu.bucket_id',
                            'b.bucket_ofsc'
                        )
                        ->join(
                            'solicitud_tecnica_log_recepcion as stlr',
                            'stlr.solicitud_tecnica_id',
                            '=',
                            'stu.solicitud_tecnica_id'
                        )
                        ->join(
                            'bucket as b',
                            'b.id',
                            '=',
                            'stu.bucket_id'
                        )
                        ->whereRaw("DATE(stlr.created_at) BETWEEN '{$fechaInicio}' AND '{$fechaFin}' ")
                        ->where('b.bucket_ofsc', '=', $value->bucket_ofsc)
                        ->where("stu.estado_ofsc_id", "=", 1)
                        ->where("stu.estado_st", "=", 1)
                        ->get();
            $array = [];
            foreach ($datos as $value2) {
                $array[] = json_decode($value2->trama, true);
            }
                
            $fileName = "datasolicitudes.json";
            $rutaFolder = public_path()."/json/solicitudes/{$value->bucket_ofsc}";
            $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
            $rutaFile = $rutaFolder."/".$fileName;
            if ($existeFolder === true) {
                if (\File::exists($rutaFile)) {
                    unlink($rutaFile);
                }
                \File::put($rutaFile, json_encode($array));
                chmod($rutaFile, 0777);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('fecha', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('fecha_inicio', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('fecha_fin', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('bucket_ofsc', null, InputOption::VALUE_OPTIONAL, 'An example option.', null)
        );
    }
}
