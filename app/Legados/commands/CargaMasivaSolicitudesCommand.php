<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaLogRecepcion as Log;
use Legados\models\EnvioLegado as LogLego;
use Legados\models\SolicitudTecnica as ST;

class CargaMasivaSolicitudesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:cargamasivasolicitudes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lee un json con las tramas de las solicitudes y emula el input desde el wsdl de generar_st del servidor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $bucket_ofsc = $this->option('bucket_ofsc');
        $buckets = !is_null($bucket_ofsc)? \Bucket::where("bucket_ofsc", "=", $bucket_ofsc)->get() : \Bucket::all();
        $json = !is_null($this->option('json'))? $this->option('bucket_ofsc') : true;

        $i = 0;
        if ($json) {
            /*foreach ($buckets as $key => $value) {
                $pathsolicitudes = public_path("json/solicitudes/");
                $rutaFile = $pathsolicitudes.$value->bucket_ofsc."/datasolicitudes.json"; //datasolicitudes.json";
                if (\File::exists($rutaFile)) {
                    $solicitudes = json_decode(\File::get($rutaFile), true);

                    $wsOptArray = [
                        "trace" => 1,
                        "exception" => 0,
                        "connection_timeout" => 500000,
                    ];
                    $opts = array('http' => array('protocol_version' => '1.0'));
                    $wsOptArray["stream_context"] = stream_context_create($opts);
                    $wsdl = \Config::get("legado.solicitudtecnica.wsdl");//\Config::get("legado.solicitudtecnica.wsdl");
                    $soapClient = new \SoapClient(
                        $wsdl,
                        $wsOptArray
                    );
                    $client = $soapClient;

                    print_r($client->__getFunctions());

                    
                    foreach ($solicitudes as $value2) {
                        // print_r($value);
                        $idst = $value2["id_solicitud_tecnica"];
                        echo "procesando ST = {$idst}\n",
                        
                        $objst = ST::where("id_solicitud_tecnica", "=", $idst)->first();
                        if (!is_null($objst)) {
                            try {
                                Movimiento::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                Ultimo::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                Log::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                LogLego::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                ST::where(["id" => $objst->id])->delete();
                            } catch (Exception $e) {
                                continue;
                            }
                            
                        }
                        $response = $client->__soapCall("generar", ["generar" => $value2]);
                        print_r($response);
                        $i++;
                    }
                }
            }*/
        } else {
            $to = !is_null($this->option('to'))? $this->option('to') : date("Y-m-d")." 00:00:00";
            $from = !is_null($this->option('from'))? $this->option('from') : date('Y-m-d H:i:s', strtotime($to . " -3 hours"));
            echo "from = ".$from."\n";
            echo "to = ".$to."\n";
            

            $singestionpsi = \DB::select("SELECT 
                    r.id_solicitud_tecnica, 
                    IF(c.id IS NULL, 'NO', 'OK') tipo,
                    IF(u.id IS NULL, 'NO', 'OK') tabla_ultimo,
                    IF(st.id IS NULL, 'NO', 'OK') tabla_st,
                    r.created_at 
                FROM solicitud_tecnica_log_recepcion r
                    LEFT JOIN solicitud_tecnica_cms c
                    ON r.id_solicitud_tecnica = c.id_solicitud_tecnica
                    LEFT JOIN solicitud_tecnica_ultimo u
                    ON u.solicitud_tecnica_id = c.solicitud_tecnica_id
                    LEFT JOIN solicitud_tecnica st
                    ON st.id_solicitud_tecnica = r.id_solicitud_tecnica
                WHERE r.created_at BETWEEN '{$from}' AND '{$to}'
                HAVING tipo = 'NO'");
            $idST = [];
            foreach ($singestionpsi as $key => $value) {
                $idST[] = $value->id_solicitud_tecnica;
            }

            $logs = Log::select("trama", "id", "id_solicitud_tecnica")->whereIn("id_solicitud_tecnica", $idST)->get();
            $wsOptArray = [
                "trace" => 1,
                "exception" => 0,
                "connection_timeout" => 500000,
            ];
            $opts = array('http' => array('protocol_version' => '1.0'));
            $wsOptArray["stream_context"] = stream_context_create($opts);
            $wsdl = \Config::get("legado.solicitudtecnica.wsdl");//\Config::get("legado.solicitudtecnica.wsdl");
            $soapClient = new \SoapClient(
                $wsdl,
                $wsOptArray
            );
            $client = $soapClient;

            print_r($client->__getFunctions());
            foreach ($logs as $key => $value) {
                echo "procesando ST : ".$value->id_solicitud_tecnica."\n";
                if ($i == 0) {
                    $response = $client->__soapCall("generar", ["generar" => json_decode($value->trama, true)]);
                    print_r($response);
                }
                
                
                $i++;
            }

        }

        echo "\n se han procesado {$i} solicitudes";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('bucket_ofsc', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array("json", null, InputOption::VALUE_OPTIONAL, 'An example option', null),
            array("from", null, InputOption::VALUE_OPTIONAL, 'An example option', null),
            array("to", null, InputOption::VALUE_OPTIONAL, 'An example option', null)
        );
    }
}
