<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaLogRecepcion as Log;
use Legados\models\EnvioLegado as LogLego;
use Legados\models\SolicitudTecnica as ST;


class CargaMasivaStEnvLegadoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:cargmasiva_st_enviolegado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lee un json con las tramas de las solicitudes(envio legado-2) y emula el input desde el wsdl de generar_st del servidor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {   
        date_default_timezone_set('America/Lima');
        echo "----------------------------------------\nInicio: " . date("Y-m-d G:i:s") . "\n";
        $fileName=date("Y").'_'.date("m").'_'.date("d").'_'.(date("G")-1).".json";
        echo "file: ".$fileName. "\n\n";;
        $time_start = microtime(true);
        $rutaFile = public_path()."/json/envio_psi_test/".$fileName;
        if (\File::exists($rutaFile)) {
            $solicitudes = json_decode(\File::get($rutaFile), true);

            $wsOptArray = [
                    "trace" => 1,
                    "exception" => 0,
                    "connection_timeout" => 500000,
                ];
                $opts = array('http' => array('protocol_version' => '1.0'));
                $wsOptArray["stream_context"] = stream_context_create($opts);
                $wsdl = \Config::get("legado.solicitudtecnica.wsdl");//\Config::get("legado.solicitudtecnica.wsdl");
                $soapClient = new \SoapClient(
                    $wsdl,
                    $wsOptArray
                );
                $client = $soapClient;

                print_r($client->__getFunctions());

                
                foreach ($solicitudes as $value2) {
                    // print_r($value);
                    $idst = $value2["id_solicitud_tecnica"];
                    echo "procesando ST = {$idst}\n",
                    
                    $objst = ST::where("id_solicitud_tecnica", "=", $idst)->first();
                    if (!is_null($objst)) {
                        try {
                            Movimiento::where(["solicitud_tecnica_id" => $objst->id])->delete();
                            Ultimo::where(["solicitud_tecnica_id" => $objst->id])->delete();
                            Log::where(["solicitud_tecnica_id" => $objst->id])->delete();
                            LogLego::where(["solicitud_tecnica_id" => $objst->id])->delete();
                            ST::where(["id" => $objst->id])->delete();
                        } catch (Exception $e) {
                            continue;
                        }
                        
                    }
                    $response = $client->__soapCall("generar", ["generar" => $value2]);
                    print_r($response);
                }
        }else{
            echo "MENSAJE: file '".$fileName."' no existe! \n";
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo "\nFin: " . date("Y-m-d G:i:s");
        echo "\nTiempo de ejecución: " . $execution_time . " Seg. \n----------------------------------------\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('bucket_ofsc', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
