<?php 
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as ST;
use Legados\models\SolicitudTecnicaCms as STCms;
use Ofsc\Activity;


class LiquidacionCmsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:liquidacioncms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'envia cierre o devolucion segun se elija de una ST Cms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
       $tipoLiquidacion = !is_null($this->option('tipo_liquidacion'))? $this->option('tipo_liquidacion') : "DEVOLUCION";
       $idSt = !is_null($this->option('idst'))? $this->option('idst') : "";
       \Auth::loginUsingId(697);//proceso automatico
        $stcms = STCms::where("id_solicitud_tecnica", $idSt)->first();
        if (!is_null($stcms)) {
            $stcms->devolucionCms();
        } else {
            echo "no existe ST Cms";
        }
        \Auth::logout();

    }

    protected function getOptions()
    {
        return array(
            array('tipo_liquidacion', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('idst', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
