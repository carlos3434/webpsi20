<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;

class EliminarStCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:eliminar_st';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'eliminar solicitudes tecnicas( CMS-provision )';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        date_default_timezone_set('America/Lima');
        echo "----------------------------------------\nInicio: " . date("Y-m-d G:i:s") . "\n\n";
        $time_start = microtime(true);

        // solicitud_tecnica
        $insert_st=DB::statement("INSERT IGNORE psi_backup.`historico_solicitud_tecnica`
        SELECT st.*,NOW() AS load_date
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN solicitud_tecnica st ON stu.`solicitud_tecnica_id`=st.id
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND stu.`num_requerimiento` NOT IN
        //('8317993','8318001','8318043','8318100','8318110','8278376','8307988','8307622',
        //'8307577','8307566','8307548','8307487','8436403');");

        if ($insert_st==1) {

            echo "1.- Tabla solicitud_tecnica registrado! \n";
            $delete_st=DB::statement("DELETE FROM solicitud_tecnica
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_st==1) {
                echo "1.- Tabla solicitud_tecnica registrado y eliminado! \n";
            }
        }

        //solicitud_tecnica_cms
        $insert_stcms=DB::statement("INSERT IGNORE 
        psi_backup.`historico_solicitud_tecnica_cms`
        SELECT stcms.*,NOW() AS load_date 
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN solicitud_tecnica_cms stcms ON stu.`solicitud_tecnica_id`=
        stcms.`solicitud_tecnica_id`
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_stcms==1) {

            echo "2.- Tabla solicitud_tecnica_cms registrado! \n";
            $delete_stcms=DB::statement("DELETE FROM solicitud_tecnica_cms
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica_cms` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_stcms==1) {
                echo "2.- Tabla solicitud_tecnica_cms registrado y eliminado! \n";
            }
        }

        //solicitud_tecnica_movimiento
        $insert_stm=DB::statement("INSERT IGNORE 
        psi_backup.`historico_solicitud_tecnica_movimiento`
        SELECT stm.*,NOW() AS load_date  
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN solicitud_tecnica_movimiento stm ON stu.`solicitud_tecnica_id`=
        stm.`solicitud_tecnica_id`
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_stm==1) {

            echo "3.- Tabla solicitud_tecnica_movimiento registrado! \n";
            $delete_stm=DB::statement("DELETE FROM solicitud_tecnica_movimiento
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica_movimiento` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_stm==1) {
                echo "3.- Tabla solicitud_tecnica_movimiento registrado y eliminado! \n";
            }
        }

        //solicitud_tecnica_ultimo
        $insert_stu=DB::statement("INSERT IGNORE 
        psi_backup.`historico_solicitud_tecnica_ultimo`
        SELECT *,NOW() AS load_date   
        FROM solicitud_tecnica_ultimo 
        WHERE `tipo_legado`=1 AND `actividad_id`=2 AND DATE(created_at) < '2018-01-14'; ");//AND
        //`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110','8278376',
        //'8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_stu==1) {

            echo "4.- Tabla solicitud_tecnica_ultimo registrado! \n";
            $delete_stu=DB::statement("DELETE FROM solicitud_tecnica_ultimo
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica_ultimo` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_stu==1) {
                echo "4.- Tabla solicitud_tecnica_ultimo registrado y eliminado! \n";
            }
        }

        //componentes_cms
        $insert_comcms=DB::statement("INSERT IGNORE 
        psi_backup.`historico_componente_cms`
        SELECT com.*,NOW() AS load_date  
        FROM solicitud_tecnica_ultimo stu
        INNER JOIN solicitud_tecnica_cms stcms ON stu.`solicitud_tecnica_id`=
        stcms.`solicitud_tecnica_id`
        INNER JOIN componente_cms com ON stcms.id=com.solicitud_tecnica_cms_id
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_comcms==1) {

            echo "5.- Tabla componente_cms registrado! \n";
            $delete_comcms=DB::statement("DELETE FROM componente_cms
            WHERE id IN (SELECT id FROM psi_backup.`historico_componente_cms` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_comcms==1) {
                echo "5.- Tabla componente_cms registrado y eliminado! \n";
            }
        }

        //componente_operacion
        $insert_comop=DB::statement("INSERT IGNORE 
        psi_backup.`historico_componente_operacion`
        SELECT com.*,NOW() AS load_date 
        FROM solicitud_tecnica_ultimo stu
        INNER JOIN solicitud_tecnica st ON stu.`solicitud_tecnica_id`=st.id 
        INNER JOIN componente_operacion com ON st.`id_solicitud_tecnica`=com.id_solicitud_tecnica
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_comop==1) {

            echo "6.- Tabla componente_operacion registrado! \n";
            $delete_comop=DB::statement("DELETE FROM componente_operacion
            WHERE id IN (SELECT id FROM psi_backup.`historico_componente_operacion` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_comop==1) {
                echo "6.- Tabla componente_operacion registrado y eliminado! \n";
            }
        }

        //envio_legado
        $insert_envleg=DB::statement("INSERT IGNORE 
        psi_backup.`historico_envio_legado`
        SELECT env.*,NOW() AS load_date  
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN envio_legado env ON stu.`solicitud_tecnica_id`=env.`solicitud_tecnica_id`
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';"); //AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_envleg==1) {

            echo "7.- Tabla envio_legado registrado! \n";
            $delete_envleg=DB::statement("DELETE FROM envio_legado
            WHERE id IN (SELECT id FROM psi_backup.`historico_envio_legado` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_envleg==1) {
                echo "7.- Tabla envio_legado registrado y eliminado! \n";
            }
        }

        //solicitud_tecnica_log_recepcion
        $insert_stlr=DB::statement("INSERT IGNORE 
        psi_backup.`historico_solicitud_tecnica_log_recepcion`
        SELECT stlr.*,NOW() AS load_date  
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN solicitud_tecnica_log_recepcion stlr ON stu.`solicitud_tecnica_id`=
        stlr.`solicitud_tecnica_id`
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_stlr==1) {

            echo "8.- Tabla solicitud_tecnica_log_recepcion registrado! \n";
            $delete_stlr=DB::statement("DELETE FROM solicitud_tecnica_log_recepcion
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica_log_recepcion` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_stlr==1) {
                echo "8.- Tabla solicitud_tecnica_log_recepcion registrado y eliminado! \n";
            }
        }

        //solicitud_tecnica_log
        $insert_stl=DB::statement("INSERT IGNORE 
        psi_backup.`historico_solicitud_tecnica_log`
        SELECT stl.*,NOW() AS load_date
        FROM solicitud_tecnica_ultimo stu 
        INNER JOIN solicitud_tecnica_log stl ON stu.`solicitud_tecnica_id`=
        stl.`solicitud_tecnica_id`
        WHERE stu.`tipo_legado`=1 AND stu.`actividad_id`=2 AND DATE(stu.created_at) < '2018-01-14';");//AND
        //stu.`num_requerimiento` NOT IN ('8317993','8318001','8318043','8318100','8318110',
        //'8278376','8307988','8307622','8307577','8307566','8307548','8307487','8436403');");

        if ($insert_stl==1) {

            echo "9.- Tabla solicitud_tecnica_log registrado! \n";
            $delete_stl=DB::statement("DELETE FROM solicitud_tecnica_log
            WHERE id IN (SELECT id FROM psi_backup.`historico_solicitud_tecnica_log` 
            WHERE DATE(load_date)=CURDATE());");

            if ($delete_stl==1) {
                echo "9.- Tabla solicitud_tecnica_log registrado y eliminado! \n";
            }
        }
        
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo "\nFin: " . date("Y-m-d G:i:s");
        echo "\nTiempo de ejecución: " . $execution_time . " Seg. \n----------------------------------------\n";
    }
}
