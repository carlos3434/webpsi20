<?php
namespace Legados\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AlertaSmsTransaccionesGo extends Command
{
	/**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legado:alertasmstransacciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
    	$averias_transacciones = \DB::table('transaccion_go')
    					->where('actividad_id', 1)
    					->orderBy('flujo', 'asc')
    					->get(['flujo', 'cantidad', 'updated_at']);
    	$celulares_por_hora = ['944978719', '953690299'];
        $celulares_por_diezminutos = ['987092235'];
    	
        $fecha_actual = strtotime(date('Y-m-d H:i:s'));

        //$fecha_transaccion = strtotime('2018-02-10 10:12:15');
        //$diferencia = $fecha_actual - $fecha_transaccion;
        $minuto_actual = date('i');

        if(self::validarTipoEnvio($minuto_actual)) {
            echo "envio por hora  \n";
            $respuesta = self::enviarPorHora($fecha_actual, $averias_transacciones);
            self::envioSms($celulares_por_hora, $respuesta['mensaje_completo'], 1);
            if ($respuesta['estado_envio']) {
                echo "envio por horam y minutos usuarios   \n";
                self::envioSms($celulares_por_diezminutos, $respuesta['mensaje_simple'], 1);
            }
        } else {
            $respuesta = self::enviarPorDiezMinutos($fecha_actual, $averias_transacciones);

            if ($respuesta['estado_envio']) {
                echo "envio por 10 minuto  \n";
                foreach ($celulares_por_hora as $celular) {
                    $celulares_por_diezminutos[] = $celular;
                }
                self::envioSms($celulares_por_diezminutos, $respuesta['mensaje_simple'], 1);
            }
            else {
                echo "No se envio Sms :(  \n";
            }
        }
   
        echo "Sms enviado :)  \n";
    	return;
    }


    public function validarTipoEnvio($minuto) {
        $minuto = substr( $minuto, 0, 1);
        $value = ($minuto == '0') ? true : false;
        return $value;
    }


    public function enviarPorHora($fecha_actual, $averias_transacciones) {
        $estado_envio = false;
        $mensaje_completo = '';
        $mensaje_simple = '';
        foreach ($averias_transacciones as $transaccion) {
            $fecha_transaccion = strtotime($transaccion->updated_at);
            if (($fecha_actual - $fecha_transaccion) > 600) {
                if ($transaccion->flujo != 5) {
                    $estado_envio = true;
                    $fecha_actualizacion = substr($transaccion->updated_at, 11, 10);
                    $mensaje_simple .= $transaccion->flujo.",  ".$transaccion->cantidad. ",  ".$fecha_actualizacion."\n";
                }
            }
            if ($transaccion->flujo != 5) {
                $fecha_actualizacion = substr($transaccion->updated_at, 11, 10);
                $mensaje_completo .= $transaccion->flujo.",  ".$transaccion->cantidad. ",  ".$fecha_actualizacion."\n";
            }
        }

        return array('mensaje_completo' => $mensaje_completo, 'estado_envio' => $estado_envio, 'mensaje_simple' => $mensaje_simple);

    }


    public function enviarPorDiezMinutos($fecha_actual, $averias_transacciones) {
        $estado_envio = false;
        $mensaje_simple = '';
        foreach ($averias_transacciones as $transaccion) {
            $fecha_transaccion = strtotime($transaccion->updated_at);
            if (($fecha_actual - $fecha_transaccion) > 600) {
                if ($transaccion->flujo != 5) {
                    $estado_envio = true;
                    $fecha_actualizacion = substr($transaccion->updated_at, 11, 10);
                    $mensaje_simple .= $transaccion->flujo.",  ".$transaccion->cantidad. ",  ".$fecha_actualizacion."\n";
                }
            }
        }

        return array('estado_envio' => $estado_envio, 'mensaje_simple' => $mensaje_simple);
    }


    public function envioSms($celulares, $mensaje) {
        print_r($celulares);
        foreach ($celulares as $celular) {
            \Sms::enviarSincrono($celular, $mensaje, 1);
        }
        
        return ;
    }
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('fecha', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}