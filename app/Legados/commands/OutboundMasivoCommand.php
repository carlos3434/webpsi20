<?php 
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as ST;
use Ofsc\Activity;

class OutboundMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:outboundmasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un csv de las STs en general.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
       $subject = !is_null($this->option('subject'))? $this->option("subject") : "";
       $fecha = !is_null($this->option('fecha'))? $this->option("fecha") : date("Y-m-d");
       \Auth::loginUsingId(697);//proceso automatico
        if ($subject!="") {
            $mensajes = \Mensaje::where(["subject" => $subject])->whereRaw("DATE(created_at) = '$fecha' ")->get();
            foreach ($mensajes as $key => $value) {
                $objdesprogramar = new \Services\models\Outbound\DesProgramar;
                $objdesprogramar->recibir($value["body"]);
                $return = $objdesprogramar->procesar();
                var_dump($return);
            }
        }
        \Auth::logout();

    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('subject', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('fecha', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
