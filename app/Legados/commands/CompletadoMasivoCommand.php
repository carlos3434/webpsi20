<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as Solicitud;
use Legados\models\SolicitudTecnicaCms as SolicitudCms;
use Legados\Repositories\SolicitudTecnicaRepository as STRepo;
use Legados\Repositories\StGestionRepository as StGestion;
use Legados\models\ComponenteOperacion;

class CompletadoMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legado:completadomasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $hoy = $fecha = !is_null($this->option('fecha'))? $this->option("fecha") : date("Y-m-d");

        $i = 0;
        $rango = [
            0 => [$fecha." 06:00:00", $fecha." 09:00:00"],
            1 => [$fecha." 09:00:00", $fecha." 12:00:00"],
            2 => [$fecha." 12:00:00", $fecha." 15:00:00"]
        ];
        $contador = 0;
        foreach ($rango as $key => $value) {
            $mensajes = \DB::table("mensajes")->where("subject", "TOA_OUTBOUND_COMPLETED")->whereIn("estado", ["sending", "failed"])->whereBetween("created_at", $value)->get();
            $solicitudes = [];
            $solicitudesMensajes = [];
            foreach ($mensajes as $key2 => $value2) {
                $body = str_replace(["<![CDATA[","]]>"], "", $value2->body);
                if (\Helpers::isValidXml($body)) {
                    $body = (simplexml_load_string($body));
                } else {
                    $body = "";
                }
                
                if ($body!="") {
                    $idSt = "";
                    if (isset($body->xa_identificador_st)) {
                        $idSt = (trim($body->xa_identificador_st));
                    } elseif (isset($body->activity_properties->xa_identificador_st)) {
                        $idSt = (trim($body->activity_properties->xa_identificador_st));
                    } elseif ($body->IDENTIFICADOR_ST) {
                        $idSt =(trim($body->IDENTIFICADOR_ST));
                    }

                    if (isset($body->activity_properties->appt_number)) {
                        $value2->codactu =
                            trim($body->activity_properties->appt_number);
                    }
                    if (isset($body->activity_properties->external_id)) {
                        $value2->bucket =
                            trim($body->activity_properties->external_id);
                    }
                    //armando el datetime cierre
                    if (isset($body->activity_properties->ETA)) {
                        $value2->starttime = $hoy.' '.trim($body->activity_properties->ETA);
                    }
                    if (isset($body->activity_properties->eta_end_time)) {
                        $value2->endtime= $hoy.' '.trim(
                            $body->activity_properties->eta_end_time
                        );
                    }

                    if (isset($body->appt_number)) {
                        $value2->codactu= trim(strtoupper($body->appt_number));
                    }
                    if (isset($body->external_id)) {
                        $value2->bucket= trim(strtoupper($body->external_id));
                    }
                    //armando el datetime incio
                    if (isset($body->eta_start_time)) {
                        $value2->starttime= $hoy.' '.trim($body->eta_start_time);
                    }
                    //armando el datetime cierre
                    if (isset($body->eta_end_time)) {
                        $value2->endtime= $hoy.' '.trim($body->eta_end_time);
                    }
                    $respuesta = [];
                    foreach ($body->activity_properties->property as $key3 => $value3) {
                        $respuesta[(string)$value3->label] = (string)$value3->value;
                    }
                    $solicitudes[$idSt] = $idSt;
                    $value2->body = $body;
                    $value2->respuesta = $respuesta;
                    $solicitudesMensajes[$idSt] = $value2;
                }
            }
            $ultimo = \DB::table("solicitud_tecnica_ultimo")
                ->select("solicitud_tecnica_ultimo.*", "st.id_solicitud_tecnica AS solicitud")
                ->leftJoin("solicitud_tecnica AS st", "st.id", "=", "solicitud_tecnica_ultimo.solicitud_tecnica_id")
                ->where("estado_ofsc_id", "=", 2)
                ->where("estado_aseguramiento", "=", 4)
                ->where("actividad_id", "=", 1)
                ->whereIn("st.id_solicitud_tecnica", $solicitudes)
                ->get();

            foreach ($ultimo as $key2 => $value2) {
                $solicitud = $value2->solicitud;
                if (isset($solicitudesMensajes[$solicitud])) {
                    $movimientoReciente = (array)$value2;
                    $mensaje = $solicitudesMensajes[$solicitud];
                    $respuesta = $mensaje->respuesta;
                    $respuesta["actividad_id"] = $movimientoReciente["actividad_id"];
                    $respuesta["estado_ofsc_id"] = $movimientoReciente["estado_ofsc_id"];
                    $tecnico = $this->getCarnetTmp($mensaje->bucket);
                    $motivo = $this->getMotivo($respuesta);
                    $submotivo = null;
                    if (!is_null($motivo)) {
                        $submotivo = $this->getSubmotivo($respuesta, $motivo->id);
                    }
                    
                    unset($movimientoReciente["id"]);
                    unset($movimientoReciente["num_requerimiento"]);
                    unset($movimientoReciente["num_orden_trabajo"]);
                    unset($movimientoReciente["num_orden_servicio"]);
                    unset($movimientoReciente["liquidate"]);
                    unset($movimientoReciente["contador_error_liquidate"]);
                    unset($movimientoReciente["tipo_legado"]);
                    unset($movimientoReciente["nenvio"]);
                    unset($movimientoReciente["tel1"]);
                    unset($movimientoReciente["tel2"]);
                    unset($movimientoReciente["contacto"]);
                    unset($movimientoReciente["direccion"]);
                    unset($movimientoReciente["freeview"]);
                    unset($movimientoReciente["descuento"]);
                    unset($movimientoReciente["solicitud"]);

                    /*if ($value2->actividad_id == 1) {
                        if ($motivo->id != 454) { // diferente de OTROS
                            $where = [
                                'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id : null,
                                'actividad_id'          => $value2->actividad_id,
                                'tipo_legado'           => $value2->tipo_legado,
                                'area_transferencia'    => $motivo->codigo_legado,
                                'estado'                => 1
                            ];
                            $transferencia = $this->getContrataTransferencia($where);
                            $solicitudupdate = [
                                'codigo_contrata' => isset($transferencia->contrata_transferencia)?  $transferencia->contrata_transferencia : "",
                                'valor1' => $motivo->codigo_legado,
                            ];
                        }
                    } elseif ($value2->actividad_id == 2) {
                        $where = [
                            'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id : null,
                            'actividad_id'          => $value2->actividad_id,
                            'tipo_legado'           => $value2->tipo_legado,
                            'area_transferencia'    => $motivo->codigo_legado,
                            'estado'                => 1
                        ];
                        $codigoContrata = "";
                        $descripcionContrata = "";
                        if (!is_null($tecnico)) {
                            $objempresa = \Empresa::find($tecnico->empresa_id);
                            if (!is_null($objempresa)) {
                                $codigoContrata = $objempresa->cms;
                                $descripcionContrata = $objempresa->nombre;
                            }
                        }
                        //$transferencia = $this->getContrataTransferencia($where);
                        $solicitudupdate = [
                            'codigo_contrata' => $codigoContrata,
                            'desc_contrata' => $descripcionContrata,
                            'valor1' => $motivo->codigo_legado,
                        ];
                        $objsolicitudcms = SolicitudCms::where("solicitud_tecnica_id", $value2->solicitud_tecnica_id)->first();
                        if (!is_null($objsolicitudcms)) {
                            $objsolicitudcms->update($solicitudupdate);
                        }
                    }*/
                    echo "Solicitud : $solicitud\n";
                    $actuacion = [
                        "codactu" => $mensaje->codactu,
                        "estado" => $value2->estado_ofsc_id,
                        "solicitud_tecnica_id" => $value2->solicitud_tecnica_id,
                        "st" => $solicitud,
                        "actividad_id" => $value2->actividad_id,
                        "submotivo_ofsc_id" => $submotivo->id
                    ];


                    $solicitudCms = [
                            'valor1' => \Config::get("legado.area_transferencia"),
                            'valor2' => $motivo->codigo_legado,
                            'valor3' => $submotivo->codigo_legado,
                        ];
                        $this->setAveriaComponentes($solicitudCms, $solicitud);

                    $movimientoReciente["estado_ofsc_id"] = 6;
                    $movimientoReciente["estado_aseguramiento"] = 5;
                    $movimientoReciente["xa_observation"] = "regularizacion de movimientos";
                    $movimientoReciente["created_at"] = $mensaje->created_at;
                    $movimientoReciente["usuario_created_at"] = 952;
                    $movimientoReciente["a_receive_person_id"] = isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '';
                     $movimientoReciente["a_receive_person_name"] = isset($respuesta['A_RECEIVE_PERSON_NAME']) ?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '';

                    $movimientoReciente["start_time"] = $mensaje->starttime.":00";
                    $movimientoReciente["end_time"] = $mensaje->endtime.":00";

                    \DB::table("solicitud_tecnica_movimiento")->insert($movimientoReciente);
                    $update = [
                        "estado_ofsc_id" => 6,
                        "estado_aseguramiento" => 5,
                        "xa_observation" => "regularizacion de movimientos",
                        "updated_at" => $mensaje->created_at,
                        "usuario_updated_at" => 952,
                        "motivo_ofsc_id" => (!is_null($motivo))? $motivo->id : null,
                        "submotivo_ofsc_id" => (!is_null($submotivo))? $submotivo->id : null,
                        "start_time" => $mensaje->starttime.":00",
                        "end_time" => $mensaje->endtime.":00",
                        'a_receive_person_id'   => isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '',
                        'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME']) ?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
                    ];
                    \DB::table("solicitud_tecnica_ultimo")->where("solicitud_tecnica_id", $value2->solicitud_tecnica_id)->update($update);
                    $this->completarOfsc($actuacion);
                    $contador++;
                    
                }
                //dd();
            }
        }
        echo "\n----se han procesado $contador solicitudes pendientes a inciadas\n";
        dd();

    }
    public function getMotivo($respuesta)
    {
        $where["estado"] = 1;
        if (isset($respuesta['estado_ofsc_id']) &&
            ($respuesta['estado_ofsc_id'] == 4 || $respuesta['estado_ofsc_id'] == 6)) {
            $where["actividad_id"] = $respuesta["actividad_id"];
        }
        $propiedades = \PropiedadOfsc::get(['tipo'=>'M','estado'=>1]);
        $motivo = new \stdClass;
        $motivo->id = null;
        $motivo->codigo_ofsc = null;
        $motivo->tipo_tratamiento_legado = null;
        $motivo->codigo_legado = '';
        $motivo->actividad_id = null;
        $motivo->estado_ofsc_id = null;
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $where["tipo_tecnologia"] = $propiedad->tipo_tecnologia;
                $where["codigo_ofsc"] = trim($respuesta[$propiedad->label]);
                $obj = \MotivoOfsc::get($where);
                if (count($obj) > 0) {
                    if (!is_null($obj[0])) {
                        $motivo = $obj[0];
                    }
                }
            }
        }
        return $motivo;
    }

    public function getSubmotivo($respuesta, $motivoId)
    {
        $subMotivo = new \stdClass;
        $subMotivo->id =null;
        $subMotivo->codigo_legado = '';

        $propiedades = \PropiedadOfsc::get(['tipo'=>'S','estado'=>1]);
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $rst = \SubmotivoOfsc::get([
                    'codigo_ofsc'=>trim($respuesta[$propiedad->label]),
                    'motivo_ofsc_id'=>$motivoId,
                ]);
                if (count($rst)>0) {
                    $subMotivo=$rst[0];
                }
            }
        }

        return $subMotivo;
    }

    public function completarOfsc($actuacion = [])
    {
        if ($actuacion['submotivo_ofsc_id'] == '1071') { // submotivo (CMS, AVERIA)
            $condicion = [
                'codactu' => $actuacion["codactu"],
                'estado'  => $actuacion['estado_ofsc_id'],
                'razon'   => 'submotivo_ofsc_id',
                'valor'   => '32',
                'aid'     => $actuacion->aid,
                'st'      => $actuacion->st
            ];
                            $visitas = Movimiento::getcontarvisita($condicion);
                            if ($visitas['tipo_envio_ofsc'] == 2 && $visitas['visitas'] < 3) {
                                
                                $agenda['agdsla'] = "sla";

                                $agenda['hf'] = $date .'||'.$this->bucket.'||||';
                                $agenda['nenvio'] = $visitas['visitas'] + 1;
                                $agenda['quiebre'] = $visitas['quiebre_id'];
                                $agenda['solicitud'] = $visitas['id_solicitud_tecnica'];
                                $agenda['actividad_tipo_id'] = $visitas['actividad_tipo_id'];
                                $agenda['actividad_id'] = $visitas['actividad_id'];
                                $agenda['solicitud_tecnica_id'] = $actuacion['solicitud_tecnica_id'];
                                \Input::replace($agenda);
                                \Auth::loginUsingId(952);
                                $objbandeja = new \Legados\controllers\BandejaLegadoController;
                                $objbandeja->_bandejaLegadoController->postEnvioofsc();
                                \Auth::logout();
                            } else {
                                if ($actuacion['actividad_id'] == 1) {
                                    $solicitudCms = SolicitudCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();
                                    if (count($solicitudCms) > 0) {
                                        $solicitudCms->liquidacionCms();
                                    }
                                }
                            }
                        } else {
                            if ($actuacion['actividad_id'] == 1) {//solo averias
                                $solicitudCms = SolicitudCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();
                                if (count($solicitudCms) > 0) {
                                    $solicitudCms->liquidacionCms();
                                }
                            } elseif ($actuacion['actividad_id'] == 2) {
                                $solicitudCms = SolicitudCms::where("solicitud_tecnica_id", $actuacion["solicitud_tecnica_id"])->first();
                                if (count($solicitudCms) > 0) {
                                    $solicitudCms->liquidacionCms();
                                }
                            }
                        }
    }


    public function getCarnetTmp($bucket)
    {
        return \Tecnico::where('carnet_tmp', $bucket)->first();
    }
    public function getContrataTransferencia($where)
    {
        return \ContrataTransferencia::where($where)->first();
    }
    public function setAveriaComponentes($averias, $solicitud)
    {
        ComponenteOperacion::setAveriaComponente($averias, $solicitud);
    }
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('fecha', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
