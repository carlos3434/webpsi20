<?php
namespace Legados\commands;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AltersReporteGestionesSt extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'alrters_reportes_st_x_dia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta alters para el reporte de ST por día.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        
        /*
        \DB::statement("
            alter table solicitud_tecnica_movimiento_detalle add tipo_autorizacion_id int (11) DEFAULT NULL;
            alter table solicitud_tecnica_movimiento_detalle add fecha_autorizacion datetime DEFAULT NULL;
            alter table solicitud_tecnica_movimiento_detalle add usuario_autorizacion varchar (200) DEFAULT NULL;
        ");
        */

        echo "\nAñadiendo columnas a solicitud_tecnica_ultimo....";
        \DB::statement("ALTER TABLE solicitud_tecnica_ultimo ADD COLUMN end_date date NULL AFTER end_time;");
        echo "OK.";


        echo "\nAñadiendo columnas a solicitud_tecnica_movimiento....";
        \DB::statement("ALTER TABLE solicitud_tecnica_movimiento ADD COLUMN end_date  date NULL AFTER end_time;");
        echo "OK.";

        /*echo "\nAñadiendo columnas a solicitud_tecnica_ultimo....";

        \DB::statement("alter table solicitud_tecnica_ultimo add tipo_autorizacion_id int (11) DEFAULT NULL;");
        \DB::statement("alter table solicitud_tecnica_ultimo add fecha_autorizacion datetime DEFAULT NULL;");
        \DB::statement("alter table solicitud_tecnica_ultimo add usuario_autorizacion varchar (200) DEFAULT NULL;");

        echo "OK.";

        echo "\n\n\nAñadiendo columnas a solicitud_tecnica_movimiento....";

        \DB::statement("alter table solicitud_tecnica_movimiento add tipo_autorizacion_id int (11) DEFAULT NULL;");
        \DB::statement("alter table solicitud_tecnica_movimiento add fecha_autorizacion datetime DEFAULT NULL;");
        \DB::statement("alter table solicitud_tecnica_movimiento add usuario_autorizacion varchar (200) DEFAULT NULL;");
        
        echo "OK.\n\n";*/
    }
}
