<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as ST;
use Legados\models\EnvioLegado as LogLegado;
use Legados\Repositories\StGestionRepository as StGestion;
use Ofsc\Activity;

class CruceToaGoMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legados:crucetoagomasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un csv de las STs en general.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $from = !is_null($this->option('from'))? $this->option('from') : date("Y-m-d");
        $to = !is_null($this->option('to'))? $this->option('to') : date("Y-m-d", strtotime("+ 7 days"));
        $bucket_ofsc = $this->option('bucket');
        $buckets = !is_null($bucket_ofsc)? \Bucket::where("bucket_ofsc", "=", $bucket_ofsc)->get() : \Bucket::all();
        $status = !is_null($this->option('status'))? $this->option('status') : "pending";
        // pendientes de toa
        $reqtoa = [];
        $objactivity = new Activity;
        $transferencias = [];
        foreach ($buckets as $key => $value) {
            $actividades = $objactivity->getActivities($value->bucket_ofsc, $from, $to);
            $pendientes = [];
            $cierres = [];
            foreach ($actividades->data as $key2 => $value2) {
                if ($value2["status"] == "pending" && $value2["date"]!="" && $value2["date"]!="3000-01-01") {
                    $pendientes[] = $value2["appt_number"];
                }
                if ($value2["status"] == "notdone") {
                    $transferencias[] = $value2["appt_number"];
                }
                if ($value2["status"] == "complete") {
                    $cierres[] = $value2["appt_number"];
                }
            }
            /*$pendientespsi = Ultimo::whereNotIn("num_requerimiento", $pendientes)
                ->where("estado_ofsc_id", "=", 1)
                ->where("estado_st", "=", 1)
                ->where("estado_aseguramiento", "=", 1)
                ->where("bucket_id", "=", $value->id)
                ->whereRaw("DATE(updated_at) = '{$from}' ")
                ->get();
            $cantidadpendientes = count($pendientespsi);
            echo "bucket = {$value->bucket_ofsc} \n";
            echo "cantidad pendientes = {$cantidadpendientes} \n";
            if ($cantidadpendientes > 0) {
                print_r($pendientespsi->toArray());
            }
            echo "\n ---------------- \n";*/
        }

        switch ($status) {
            case 'notdone':
                $this->cruceNoRealizada($transferencias);
                break;
                
            default:
                # code...
                break;
        }

    }

    public function cruceNoRealizada($requerimientos = [])
    {
        $norealizadas = Ultimo::whereIn("num_requerimiento", $requerimientos)
            ->where("estado_ofsc_id", "=", 4)
            ->where("estado_aseguramiento", "=", 6)
            ->where("motivo_ofsc_id", "=", 454)
            ->where("submotivo_ofsc_id", "=", 1347)
            ->where("estado_st", "=", 1)
            ->get();

        print_r($norealizadas);
        dd();
        foreach ($norealizadas as $key => $value) {
            $solicitud = $value->solicitudTecnica;
            $movimiento = ["estado_aseguramiento" => 5, "xa_observation" => "Cambio Estado Automatico GO"];
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitud, $movimiento, []);
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('from', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('to', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('bucket', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('status', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
