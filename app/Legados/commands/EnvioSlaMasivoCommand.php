<?php
namespace Legados\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\Repositories\SolicitudTecnicaRepository as STRepo;
use Legados\Repositories\StGestionRepository as StGestion;

class EnvioSlaMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'legado:enviosla';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio por SLA de los no Enviados a TOA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fecha = !is_null($this->option('fecha'))? $this->option("fecha") : date("Y-m-d");
        \Auth::loginUsingId(\Config::get("legado.user_logs.toa"));
        $i = 0;
        $solicitudes = Ultimo::where(["estado_ofsc_id" => 8, "estado_st" => 1])
            ->whereRaw("actividad_tipo_id IS NOT NULL")
            ->whereRaw("DATE(created_at) = '{$fecha}' ")
            ->get();
        foreach ($solicitudes as $key => $value) {
            $objst = $value->solicitudTecnica;
            $objcms = $objst->cms;

            $arrayvalidacion=array();
            $arrayvalidacion['tipo']=2; //GOTOA
            $arrayvalidacion['estado']=1;
            $arrayvalidacion['proveniencia']=2; //CMS

            if ($objcms->validaGoToa($arrayvalidacion)) {
                $objcms->actividad_tipo_id = $value->actividad_tipo_id;
                $objcms->quiebre_id = $value->quiebre_id;
                $objcms->getWorkZone();
                $capacidad = $objcms->capacityOfsc();

                if (isset($capacidad["external_id"])) {
                    $hf = date("Y-m-d")."||".$capacidad["external_id"]."||0||0-0";
                    $inputs = [
                        'actividad_tipo_id'     => $value->actividad_tipo_id,
                        'actividad_id'          => $value->actividad_id,
                        'tematicos'             => [],
                        'observacion_toa'       => "",
                        'solicitud_tecnica_id'  => $value->solicitud_tecnica_id,
                        // 'ffttcobre'             => \Input::has("ffttcobre") ? \Input::get("ffttcobre") : [],
                        'agdsla'                => "sla",
                        'hf'                    => $hf,
                        'quiebre'               => $value->quiebre_id
                    ];

                    echo $value->num_requerimiento."\n";
                    print_r($inputs);
                    echo "---------------\n";
                    $solicitudGestion = new StGestion;
                    $response = $solicitudGestion->envioOfsc($value->solicitud_tecnica_id, $inputs);
                    print_r($response);
                    echo "---------\n";
                }
            }

            //$objst = $value->solicitudTecnica;
            $i++;
        }
        echo "\n se han procesado {$i} solicitudes";
        \Auth::logout();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('bucket_ofsc', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('fecha', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }
}
