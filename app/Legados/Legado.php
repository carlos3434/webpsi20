<?php

namespace Legados;

use Legados\models\EnvioLegado;
use Legados\Repositories\SolicitudTecnicaRepository as STRepository;

/**
* clase padre de configuracion para consumir web service
*/
abstract class Legado
{
    protected $_wsdl;
    protected $_client;
    protected $_header;
    protected $_userId;

    /**
     * funcion que inicia el enlace entre OFSC y la webpsi
     * mediante el SOAP, en case falle retorna false
     * y se guarda en la tabla de errores
     */
    public function __construct()
    {
        try {
            $wsOptArray = [
                "trace" => 1,
                "exception" => 0,
                "connection_timeout" => 500000,
            ];
            $opts = array('http' => array('protocol_version' => '1.0'));
            $wsOptArray["stream_context"] = stream_context_create($opts);
            $soapClient = new \SoapClient(
                $this->_wsdl,
                $wsOptArray
            );
            $this->_client = $soapClient;
        } catch (Exception $e) {
            $objerror = new \ErrorController;
            $objerror->saveError($e);
        }
    }

    protected function header($elementos = [])
    {
        $urlheader = "http://telefonica.com/globalIntegration/header";
        $headerbody = [
            "country" => "PE",
            "lang" => "es",
            "entity" => "TDP",
            "system" => "GO",
            "subsystem" => "PSI",
            "originator" => "PE:TDP:GO:PSI",
            "sender" => "OracleServiceBus",
            "userId" => $this->_userId,
            "operation" => $elementos["operation"],
            "destination" => "PE:CMS:CMS:CMS",
            "execId" => "a2aa1107-e75c-44ed-90fa-2f96faf01e86",
            "timestamp" => "2017-01-05T17:52:16.547-05:00"
        ];
        $this->_header = new \SoapHeader($urlheader, "HeaderIn", $headerbody);
        $this->_client->__setSoapHeaders($this->_header);
    }

    /**
     * ejecuta una accion (metodo) de una determinada api
     * se le envia un array ($setArray) como parametro
     * puede guardarse un log en Base de datos o un log temporal
     *
     * @param String $action
     * @param String $setArray
     * @param Double $save
     * @return type $response
     */
    protected function doAction($elementos = array())
    {
        $response = new \stdClass();
        $response->error = false;
        $response->errorMsg ='';
        $objcomunicacion = null;

        if ($this->_client===false || $this->_client===null) {
            return false;
        }
        try {
            $request = $elementos['body'];
            $this->header($elementos);
            $result = $this->_client->__soapCall(
                $elementos['action'],
                ["request"=>$request]
            );
            $response->data = $result;
            return $response;
        } catch (\SoapFault $fault) {
            $response->error = true;
            $response->obj = $fault;
            $response->errorCode = $fault->faultcode;
            $response->errorString = $fault->faultstring;
            $objError = $fault->getMessage();
            $obj = null;
            $response->errorMsg = $response->errorString;
            if (isset($objError->ServerException)) {
                $obj = $objError->ServerException;
            } else if (isset($objError->ClientException)) {
                $obj = $objError->ClientException;
            }
            if (!is_null($obj)) {
                $response->exceptionCategory = $obj->exceptionCategory;
                $response->exceptionCode = $obj->exceptionCode;
                $response->exceptionMsg = $obj->exceptionMsg;
                $response->exceptionDetail = $obj->exceptionDetail;
                $response->exceptionSeverity = $obj->exceptionDetail;
                $response->errorMsg = $obj->exceptionMsg;
            }
            
            if (isset($fault->detail)) {
                //$response->errorMsg = $fault->detail;
            }
            return $response;
        } catch (\Exception $error) {
            $response->error = true;
            $objError = $error->getMessage();
            $response->errorMsg = "";
            $obj = null;
            if (isset($objError->ServerException)) {
                $obj = $objError->ServerException;
            } else if (isset($objError->ClientException)) {
                $obj = $objError->ClientException;
            }
            if (!is_null($obj)) {
                $response->exceptionCategory = $obj->exceptionCategory;
                $response->exceptionCode = $obj->exceptionCode;
                $response->exceptionMsg = $obj->exceptionMsg;
                $response->exceptionDetail = $obj->exceptionDetail;
                $response->exceptionSeverity = $obj->exceptionDetail;
            }
            return $response;
        }
    }

    /**
     * @param $request array
     * @param $response array
     */
    protected function tracer($elementos = [], $response = [])
    {
        $envio = new EnvioLegado;
        //var_dump($this->_client->__getLastRequest());//xml
        //var_dump($this->_client->__getLastRequestHeaders());//string
        //var_dump($this->_client->__getLastResponse());//xml
        //var_dump($this->_client->__getLastResponseHeaders());//string
        

        $reqheader=explode(" ", str_replace("\r\n", " ", $this->_client->__getLastRequestHeaders()));
        $host=null;
        $urls=\Config::get("legado.url_lego");

        foreach ($urls as $key => $value) {
            if (array_search($value, $reqheader)) {
                $host=$key;
                break;
            }
        }

        $log = [
            "lastrequestheaders" => $this->_client->__getLastRequestHeaders(),
            "lastrequest" => $this->_client->__getLastRequest(),
            "lastresponseheaders" => $this->_client->__getLastResponseHeaders(),
            "latresponse" => $this->_client->__getLastResponse(),
            "host" => $host
        ];

        $envio->host = $host;
        $envio->response_header=$this->_client->__getLastResponseHeaders();
        $envio->request_header=$this->_client->__getLastRequestHeaders();
        $envio->request = json_encode($elementos["body"]);
        $envio->response = json_encode($response);
        $envio->accion = $elementos["action"];
        $envio->solicitud_tecnica_id = $elementos["solicitud_tecnica_id"];

        if (isset($response->exceptionCode)) {
            $envio->code_error = json_encode($response->exceptionCode);
        }
        if (isset($response->errorString)) {
            $envio->descripcion_error = $response->errorString; //errorMsg
        }
        $envio->save();

        $objimplements = new STRepository;
        $objimplements->saveLogComunicacionLegadoCache($elementos["solicitud_tecnica_id"]);
        return;
    }

    public function setWsdl($wsdl)
    {
        $this->_wsdl = $wsdl;
    }

    public function getWsdl()
    {
        return $this->_wsdl;
    }
}
