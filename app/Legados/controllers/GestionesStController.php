<?php namespace Legados\controllers;

use Illuminate\Support\Collection;
use Input;
use Response;
use DB;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaLogRecepcion as Log;
use Legados\models\EnvioLegado as LogLego;
use Legados\models\SolicitudTecnica as ST;

class GestionesStController extends \BaseController
{
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    public function postDescargarjson()
    {
        $buckets = Input::get('buckes');
        $estadosOfsc = Input::get('estados_ofsc');
        $estadoSt = Input::get('slct_estado_st');
        $actividad = Input::get('slct_actividad');
        $actividadTipo = Input::get('slct_actividad_tipo_id');
        $fechaCreacion = explode(" - ", Input::get('created_at'));
        $datos = Ultimo::from('solicitud_tecnica_ultimo as stu')
                ->select(
                    'stlr.trama',
                    'stu.bucket_id',
                    'b.bucket_ofsc'
                )
                ->join(
                    'solicitud_tecnica_log_recepcion as stlr',
                    'stlr.solicitud_tecnica_id',
                    '=',
                    'stu.solicitud_tecnica_id'
                )
                ->join(
                    'bucket as b',
                    'b.id',
                    '=',
                    'stu.bucket_id'
                )
                ->whereBetween(DB::raw("date(stlr.created_at)"), $fechaCreacion);
        if (count($buckets) > 0) {
            $datos->whereIn('b.id', $buckets);
        }
        if (count($estadosOfsc) > 0) {
            $datos->whereIn('stu.estado_ofsc_id', $estadosOfsc);
        }
        if (count($estadoSt) > 0) {
            $datos->whereIn('stu.estado_st', $estadoSt);
        }
        if (count($actividad) > 0) {
            $datos->whereIn('stu.actividad_id', $actividad);
        }
        if (count($actividadTipo) > 0) {
            $datos->whereIn('stu.actividad_tipo_id', $actividadTipo);
        }
        $datos = $datos->get();
        if (count($datos) > 0) {
            $array = [];
            foreach ($datos as $value2) {
                $array[] = json_decode($value2->trama, true);
            }

            $fileName = "datasolicitudes.json";
            $rutaFolder = public_path()."/json/solicitudes";
            $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
            $rutaFile = $rutaFolder."/".$fileName;
            if ($existeFolder === true) {
                if (\File::exists($rutaFile)) {
                    unlink($rutaFile);
                }
                \File::put($rutaFile, json_encode($array));
                chmod($rutaFile, 0777);
                return Response::download($rutaFile);
            }
        } else {
            return [
                "rst" => 2,
                "msj" => "No se encontraron datos"
            ];
        }
    }

    public function postCargarjson()
    {
        $rutaFolder = public_path()."/json/solicitudes/";
        $fileName = "datasolicitudes.json";

        $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
        $rutaFile = $rutaFolder."/".$fileName;
        if ($existeFolder === true) {
            if (\File::exists($rutaFile)) {
                unlink($rutaFile);
            }
            Input::file('file_json')->move($rutaFolder, $fileName);
            if (\File::exists($rutaFile)) {
                chmod($rutaFile, 0777);
                $solicitudes = json_decode(\File::get($rutaFile), true);
                if (count($solicitudes) > 0) {
                    $wsOptArray = [
                        "trace" => 1,
                        "exception" => 0,
                        "connection_timeout" => 500000,
                    ];
                    $opts = array('http' => array('protocol_version' => '1.0'));
                    $wsOptArray["stream_context"] = stream_context_create($opts);
                    $wsdl = \Config::get("legado.solicitudtecnica.wsdl");
                    $soapClient = new \SoapClient(
                        $wsdl,
                        $wsOptArray
                    );
                    $client = $soapClient;
                    foreach ($solicitudes as $value2) {
                        $idst = $value2["id_solicitud_tecnica"];
                        $objst = ST::where("id_solicitud_tecnica", "=", $idst)->first();
                        if (!is_null($objst)) {
                            try {
                                Movimiento::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                Ultimo::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                Log::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                LogLego::where(["solicitud_tecnica_id" => $objst->id])->delete();
                                ST::where(["id" => $objst->id])->delete();
                            } catch (Exception $e) {
                                $errorController = new \ErrorController();
                                $errorController->saveError($e);
                                continue;
                            }
                            
                        }
                        $response = $client->__soapCall("generar", ["generar" => $value2]);
                    }
                }
            }
        }
        return;
    }

    //Funcion para cargar los datos en el datatable
    public function postCargar()
    {
        $fcha_inicio=Input::get("fecha_inicio");
        $fcha_fin=Input::get("fecha_fin");
        $fechas="'".$fcha_inicio."' AND '".$fcha_fin."'";
        $tipo_legado=Input::get("tipo_legado");
        $actividad=Input::get("actividad");
        $bucket=Input::get("buckets");

        /*Filtros*/
        $where='';
        if (isset($tipo_legado)) {$where.=' and g.legado in ('.implode(',',$tipo_legado).')';}
        if (isset($actividad)) {$where.=' and g.actividad_id in ('.implode(',',$actividad).')';}

        $order = ["column" => Input::get('column'),"dir" => Input::get('dir')];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        /*consulta*/
        $select=['g.id','a.nombre as actividad',
        DB::raw("IF(g.legado=1,'Cms',IF(g.legado=2,'Gestel','')) AS legado"),
        'g.cantidad','g.fecha', 'csv_location'];

        $data2= DB::table('comandos_gestiones_stpordia as g')
                ->Join("actividades AS a", "a.id", "=", "g.actividad_id")
                ->select($select)
                ->whereRaw('DATE(g.fecha) between '.$fechas.$where)
                ->orderBy('g.fecha', 'desc');

        $column = "id";
        $dir = "desc";

        $data2 = $data2
                ->orderBy($column, $dir)
                ->paginate($grilla["perPage"]);

        $data = $data2->toArray();

        $col = new Collection([
            'recordsTotal'=> $data2->getTotal(),
            'recordsFiltered'=> $data2->getTotal(),
        ]);

        return $col->merge($data);
    }

    public function postExcel() {

        $id = Input::get("id");

        //consulta
        $reporte=DB::select("
            SELECT 
                estadosolicitud,
                solicitud_tecnica,
                requerimiento,
                nombre_tecnico,
                carnet_tecnico,
                bucket,
                estado,
                fecregistropsi,
                fecha_agendado,
                fecha_iniciado,
                fecha_ult_mov,
                coordx_cliente,
                coordy_cliente,
                coordx_tecnico_inicio,
                coordy_tecnico_inicio,
                cod_cliente,
                apointment_toa,
                tipo_operacion,
                tipo_requerimiento,
                usuario_valida_coord,
                fecha_cierre,
                tiempo_iniciado_a_cierre,
                motivo,
                submotivo,
                fecha_registro_csm,
                fecha_llegada_toa,
                codigo_servicio_cms,
                fecha_liquidacion_toa,
                fin_actividad,
                fftt_nodo,
                fftt_troba,
                fftt_plano,
                tematicos_1,
                tematicos_2,
                tematicos_3,
                tematicos_4,
                observacion
           FROM comandos_detgestiones_stpordia
           WHERE gestpordia_id=".$id."
           ORDER BY solicitud_tecnica ASC;");

        if (Input::get("tipo") == 'Excel') {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=GestionesporDia.xls');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');

            if (count($reporte) > 0) {
                $n = 1;
                foreach ($reporte as $data) {
                //Encabezado
                    if ($n == 1) {
                        foreach ($data as $key => $val) {
                            echo $key . "\t";
                        }
                        echo $val . "\r\n";
                    }
                    //Datos
                    foreach ($data as $val) {
                        $val = str_replace(
                        array("\r\n", "\n", "\n\n", "\t", "\r"),
                        array("", "", "", "", ""),
                        $val
                        );
                        echo $val . "\t";
                    }
                    echo "\r\n";
                    $n++;
                }
            }else{$val="Mensaje:No existen registros!";echo $val . "\r\n";}
        } else {return Response::json(array('rst' => 1, 'datos' => $reporte));}
    }

    public function postListar()
    {
        $tipo = Input::get('tipo');
        $data = [];
        if ($tipo == 'tipo_legado') {
            $data[0] = array('id' => 1,'nombre'=>'Cms');
            $data[1] = array('id' => 2,'nombre'=>'Gestel');
        }

        if ($tipo == 'actividad') {
            $data[0] = array('id' => 1,'nombre'=>'Averia');
            $data[1] = array('id' => 2,'nombre'=>'Provision');
        }
    return Response::json(["rst" => 1, "datos" => $data]);
    }

    public function postCargarimagen()
    {
        $rutaFolder = public_path()."/lib/jstree-3.2.1/";
        $fileName = Input::file('imagen')->getClientOriginalName();
        $existeFolder = \FileHelper::validaCrearFolder($rutaFolder);
        if ($existeFolder === true) {
            Input::file('imagen')->move($rutaFolder, $fileName);
            $rutaFile = $rutaFolder."/".$fileName;
            if (\File::exists($rutaFile)) {
                chmod($rutaFile, 0777);
            }
        }
        return;
    }
}