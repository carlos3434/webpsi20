<?php

namespace Legados\controllers;

use Legados\STGestelApi;
use Legados\STCmsApi;
use Legados\models\SolicitudTecnica;
use Nodo;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaLog;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as MovimientoDetalle;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\ComponenteCms;
use Legados\models\ComponenteGestel;
use Legados\models\SolicitudTecnicaGestion as Gestion;


use Legados\helpers\InboundHelper;
use Legados\helpers\SolicitudTecnicaHelper;
use Legados\helpers\ActivityHelper;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\EnvioLegado;
use Legados\models\ComponenteOperacion;
use Legados\models\MotivosDevolucion;
use Legados\models\MotivosLiquidacion;
use Legados\models\Tematicos;

use Legados\Repositories\SolicitudTecnicaRepository as STRepository;
use Legados\Repositories\StGestionRepository;
use Repositories\ListadoLegoRepository;

use Ofsc\Capacity;
use Ofsc\Inbound;
use Ofsc\Activity;

//use Intervention\Image\Facades\Image;

class BandejaLegadoController extends \BaseController
{
    protected $repository;
    public function __construct(ListadoLegoRepository $listadoLegoRepository)
    {
        $this->repository = $listadoLegoRepository;
    }

    /**
     * cierra una solicitud tecnica
     */
    public function validarRedisUsuario($idSolicitudTecnica)
    {
        $key = \Redis::get($idSolicitudTecnica);
      
        if (is_null($key)) {
            $response =['estado' => true, 'msj' => 'El tiempo para atender la solicitud ha expirado.'];
        } else {
            $key_data = json_decode($key, true);
            $response = ['estado' => false];
            if ($key_data['idusuario'] != \Auth::id()) {
                $response = ['estado' => true, 'msj' => 'La solicitud esta siendo atendida por otro usuario.'];
            }
        }

        return $response;
    }

    public function postCierre()
    {
        $solicitudTecnicaId = \Input::get("solicitud_tecnica_id", 0);
        $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitudTecnicaId)->first();
        $actividadid = \Input::get("actividad_id", 0);
  
        /*if ($actividadid == 2) {
            $validacion_redis =  $this->validarRedisUsuario($solicitudCms->id_solicitud_tecnica);
            if ($validacion_redis['estado']) {
                return \Response::json([
                    'rst' => 2,
                    'msj' => $validacion_redis['msj']
                ]);
            }
        }*/
        $codactu = \Input::get('codactu');
        $tipoPersona = \Input::get('tipo_persona', 1); // 0 es tecnico, 1 es despachador
        $tipoCierre = \Input::get("tipo_cierre", 0); // 1 es devuelto, 2 es liquidacion, 3 es retorno
        $tematicos = json_decode(\Input::get("tematicos"), true);
        $tipoDevolucion = \Input::get("tipodevolucion", "");
        $motivodevolucion = \Input::get("motivosdevolucion", "");
        $submotivoDevolucion = \Input::get("submotivosdevolucion");
        $motivoliquidacion = \Input::get("motivosliquidacion", "");
        
        $enviosincrono = \Input::get("enviosincrono", false);
        $observacion = \Input::get("comentario-tematico", "");
        $tipoLegado = \Input::get("tipo_legado", 0);
        
        $tecnico = \Tecnico::find(\Input::get("tecnico_id", 0));
        $response = [];
        $mensaje = "";

        if (!is_null($tecnico)) {
            $celular = $tecnico->celular;
        } else {
            $celular = "";
        }

        if ($tipoCierre == 1) { // devuelta
            if ($motivodevolucion == "") {
                $motivoofsc = \MotivoOfsc::find(\Input::get("motivo_ofsc_id", 0));
            } else {
                $motivoofsc = \MotivoOfsc::find($motivodevolucion);
            }
            if (is_null($motivoofsc)) {
                return \Response::json(
                    ['rst' => 2,
                        'msj' => "No existe un codigo de devolucion o transferencia para legados"
                    ]
                );
            }

            $responseToa["rst"] = 1;
            if ($tipoPersona != 0) {
                $submotivoofsc=null;
                if ($submotivoDevolucion!='') {
                    $submotivoofsc=$submotivoDevolucion;
                }
                $responseToa = $this->devolucion($motivoofsc, $submotivoofsc, false, $solicitudTecnicaId, $observacion, $tematicos);
            }
            
            if ($responseToa["rst"] == 1) {
                if (count($solicitudCms) > 0) {
                    $solicitudCms->devolucionCms();
                }

                $mensaje = "Req. ".$codactu.", Devolución Aceptada, puede Continuar con la Siguiente Orden";
                if ($celular!="") {
                    \Sms::enviar($celular, $mensaje, '1');
                }
                $response = ["rst" => 1, "msj" => "Devuelto a Legados Exitoso"];
                return \Response::json($response);
            }
            if ($responseToa["rst"] == 2) {
                $mensaje = "Req. ".$codactu.", No se pudo cerrar la orden en TOA, comuniquese con su Despacho";
                if ($celular!="") {
                    \Sms::enviar($celular, $mensaje, '1');
                }
                $response = ["rst" => 2, "msj" => "Devuelta a TOA Fallido"];
                return \Response::json($response);
            }
        }

        if ($tipoCierre == 2) { // cierre -> por el tecnico, o despacho
            $motivoofsc = \MotivoOfsc::find(\Input::get("motivo_ofsc_id", 0));
            if (is_null($motivoofsc)) {
                return \Response::json(
                    ['rst' => 2,
                         'msj' => "No existe un codigo de cierre para legados"
                    ]
                );
            }

            $responseLegado["rst"] = 1;
            if ($tipoPersona != 0) {
                $responseLegado = $this->liquidacion($motivoofsc, $solicitudTecnicaId, $observacion, $tematicos); // liquidamos (TOA)
            }

            if ($responseLegado["rst"] == 1) {
                $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitudTecnicaId)->first();
                if (count($solicitudCms) > 0) {
                    $solicitudCms->liquidacionCms();
                }

                $mensaje = "Req. ".$codactu.", Cierre Aceptada, puede Continuar con la Siguiente Orden";
                if ($celular!="") {
                    \Sms::enviar($celular, $mensaje, '1');
                }
                $response = ["rst" => 1, "msj" => "Cierre a Legados Exitoso"];
                return \Response::json($response);
            }
            if ($responseLegado["rst"] == 2) {
                $mensaje = "Req. ".$codactu.", No se pudo cerrar la orden en TOA, comuniquese con su Despacho";
                if ($celular!="") {
                    \Sms::enviar($celular, $mensaje, '1');
                }
                return \Response::json($responseLegado);
            }
        }

        if ($tipoCierre == 3) { // retorno
            // $objstcierre = new STCierre($codactu, $tematicos, ["tipo" => 3]);
        }
        return;

    }

    public function devolucion(
        $motivoofsc = null,
        $submotivodevolucion = null,
        $stopautomatico = false,
        $solicitud_tecnica_id,
        $observacion,
        $tematicos
    ) {
        if (!is_null($motivoofsc)) {
            $solicitudTecnicaGestion = new Gestion;
            $objinbound = new Inbound;

            $tipoDevolucion = \Input::get("tipodevolucion", "");
            $tipo = $tipoDevolucion == "C" ? 3 : 4;

            $solicitudTecnica = SolicitudTecnica::where('id', $solicitud_tecnica_id)
                                ->orderBy("updated_at", "DESC")
                                ->first();
            $ultimo = $solicitudTecnica->ultimo;

            $properties = [];
            $properties["A_OBSERVATION"] = substr($ultimo->xa_observation." | ".$observacion, 0, 49);
            $properties["A_IND_PRE_CIERRE_DEV"] = 2;

            if ($ultimo->actividad_id == 1) {
                $properties["A_NOT_DONE_AREA"] = $motivoofsc->codigo_ofsc;
            } elseif ($ultimo->actividad_id == 2) {
                $properties["A_NOT_DONE_REASON_INSTALL"] = $motivoofsc->codigo_ofsc;
            }

            $actuacion = [
                'xa_observation'   => $ultimo->xa_observation." | ".$observacion,
                'tel1'      => \Input::get('tel1'),
                'tel2'      => \Input::get('tel2'),
                'contacto'  => \Input::get('contacto'),
                'direccion' => \Input::get('direccion'),
                'freeview'  => \Input::get('chk_freeview'),
                'descuento' => \Input::get('chk_descuento'),
            ];

            $responseToa = new \stdClass;
            $responseToa->result = "success";
            $responseToa->code = 0;
            /*$responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties, [], "notdone_activity");*/

            
            //$actuacion['estado_aseguramiento'] = 6;
            $actuacion['motivo_ofsc_id'] = $motivoofsc->id;
            $actuacion['submotivo_ofsc_id'] = $submotivodevolucion;
            if (isset($responseToa->result)
                && $responseToa->result == "success"
                && $responseToa->code == 0) {
                $actuacion['estado_ofsc_id'] = 4;
                $actuacion['estado_aseguramiento'] = $stopautomatico ? 5 : 6;
                $response = ["rst" => 1, "msj" => ""];
            } else {
                $errorMsj = isset($responseToa->errorMsg) ?
                            $responseToa->errorMsg : ($responseToa->description)? $responseToa->description : 'error desconocido en TOA';
                $actuacion['xa_observation'] .= " | ".$errorMsj;
                $response["rst"] = 2;
                $response["msj"] = $errorMsj;
            }

            foreach (Ultimo::$rules as $key => $value) {
                if (array_key_exists($key, $actuacion)) {
                    $ultimo[$key] = $actuacion[$key];
                }
            }
            $solicitudTecnica->ultimo()->save($ultimo);

            $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
            foreach (Movimiento::$rules as $key => $value) {
                if (array_key_exists($key, $actuacion)) {
                    $ultimoMovimiento[$key] = $actuacion[$key];
                }
            }
            $ultimoMovimiento->save();

            if (count($tematicos) > 0) {
                $data = [
                    "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                    "comentarios" => $observacion,
                    "tipo" => $tipo // 3 devolucion comercial y 4 dev. tecnica
                ];
                $save = $solicitudTecnicaGestion->generarMovimientoconTematico(
                    $tematicos,
                    [],
                    $data
                );
            }
            return $response;
        }
    }

    public function liquidacion($motivoofsc, $solicitud_tecnica_id, $observacion, $tematicos, $estado_aseguguramiento = 6)
    {
        $solicitudTecnicaGestion = new Gestion;
        $objinbound = new Inbound;

        $solicitudTecnica = SolicitudTecnica::where('id', $solicitud_tecnica_id)
                            ->orderBy("updated_at", "DESC")
                            ->first();
        $ultimo = $solicitudTecnica->ultimo;
        
        $properties = [];
        $properties["A_OBSERVATION"] = substr($ultimo->xa_observation." | ".$observacion, 0, 49);
        $properties["A_IND_PRE_CIERRE_COM"] = 2;
        if ($ultimo->actividad_id == 1) { // Averia
            if ($motivoofsc->tipo_tecnologia == 1) {// CATV
                $properties["A_COMPLETE_CAUSA_REP_CBL"] = $motivoofsc->codigo_ofsc;
            } elseif ($motivoofsc->tipo_tecnologia == 2) {// SPEEDY
                $properties["A_COMPLETE_CAUSA_REP_ADSL"] = $motivoofsc->codigo_ofsc;
            } elseif ($motivoofsc->tipo_tecnologia == 3) {// BASICA
                $properties["A_COMPLETE_CAUSA_REP_STB"] = $motivoofsc->codigo_ofsc;
            }
        } elseif ($ultimo->actividad_id == 2) {
            $properties["A_COMPLETE_REASON_INSTALL"] = $motivoofsc->codigo_ofsc;
        }

        $actuacion['xa_observation'] = $ultimo->xa_observation." | ".$observacion;
        $responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties, [], "complete_activity");
        if (isset($responseToa->result) && $responseToa->result == "success") {
            $actuacion['estado_ofsc_id'] = 6;
            $actuacion['estado_aseguramiento'] = $estado_aseguguramiento;
            $response = ["rst" => 1];
        } else {
            $error = isset($responseToa->errorMsg)? $responseToa->errorMsg: isset($responseToa->description) ? $responseToa->description : 'error desconocido en TOA';
            $response["rst"] = 2;
            $response["msj"] = $error;
            $actuacion['xa_observation'] .= " | ".$error;
        }

        foreach (Ultimo::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimo[$key] = $actuacion[$key];
            }
        }
        $solicitudTecnica->ultimo()->save($ultimo);

        $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
        foreach (Movimiento::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimoMovimiento[$key] = $actuacion[$key];
            }
        }
        $ultimoMovimiento->save();

        if (count($tematicos) > 0) {
            $data = [
                "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                "comentarios" => $observacion,
                "tipo" => 6
            ];
            $save = $solicitudTecnicaGestion->generarMovimientoconTematico(
                $tematicos,
                [],
                $data
            );
        }
        return $response;
    }

    /**
     * tipo_busqueda    1: busqueda individual, 2busqueda general
     * valor            cuando es busqueda individual, valor a buscar
     * tipo             cuando es busqueda individual, criterio a buscar
     */
    public function postListar()
    {
        $descargar = false;
        if (\Input::has("accion")) {
            $descargar = true;
        }

        if (!$descargar) {
            $reglas = array(
                'valor'          => 'required_if:tipo_busqueda,1',
                'tipo'           => 'required_if:tipo_busqueda,1|in:id_solicitud_tecnica,num_requerimiento,orden_trabajo,peticion'
            );
            $mensaje = array(
                'required'      => 'no se ingreso :attribute',
                'required_if'   => 'en la busqueda individual se requiere :attribute de busqueda',
            );
            $validator = \Validator::make(\Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return \Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages()->all()[0]
                    )
                );
            }
        }

        /*change keys to report data*/
        if (\Input::has("accion")) {
            $inputAll = \Input::all();
                //Input::clear();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                    $inputAll[$key] = $val;
            }
            \Input::merge($inputAll);
        }
        /*change keys to report data end*/

        $tipoBusqueda = \Input::get("tipo_busqueda");
        $response = [
            'rst' => 1,
            "recordsTotal" => 0,
            "draw" => 1,
            "recordsFiltered" => 0,
            "data" => []
        ];
        $gestion = new Gestion;
        try {
            if ($tipoBusqueda == "1") {
                $valor = \Input::get("valor");
                $tipo = \Input::get("tipo");
                $opcionestipo = [
                    "id_solicitud_tecnica" => [
                        "cms" => "id_solicitud_tecnica",
                        "gestelprovision" => "id_solicitud_tecnica",
                        "gestelaveria" => "id_solicitud_tecnica"
                    ],
                    "num_requerimiento" => [
                        "cms" => "solicitud_tecnica_cms.num_requerimiento",
                        "gestelprovision" => "solicitud_tecnica_gestel_provision.peticion",
                        "gestelaveria" => "solicitud_tecnica_gestel_averia.cod_averia"
                    ],
                    "orden_trabajo" => [
                        "cms" => "solicitud_tecnica_cms.orden_trabajo",
                        "gestelprovision" => "solicitud_tecnica_gestel_provision.numero_orden_servicio"
                        ],
                    "peticion" => [
                        "cms" => "solicitud_tecnica_cms.peticion",
                        "gestelprovision" => "solicitud_tecnica_gestel_provision.peticion",
                        "gestelaveria" => "solicitud_tecnica_gestel_averia.numero_orden_reparacion",
                    ]
                ];

                $solicitudes = $gestion->busquedaIndividual($valor, $opcionestipo[$tipo]);
                if ($descargar) {
                    $solicitudes = json_decode(json_encode($solicitudes), true);
                    return \Helpers::exportArrayToExcel($solicitudes, "SolicitudesTecnicas", ["actividad_id","estado_ofsc_id","actividad_tipo_id","quiebre_id","tipo_legado","estado_st","estado_aseguramiento","solicitud_tecnica_id", "estado_ofsc2"]);
                }

                $response["recordsTotal"] = count($solicitudes);
                $response["recordsFiltered"] = count($solicitudes);
                $response["data"] = $solicitudes;

                return \Response::json($response);
            } else {
                
                $filtros = ["campos" => [
                    ],
                "arreglosEqual" => [
                    "u.actividad_tipo_id"
                            => \Input::has('actividad_tipo_id')? \Input::get('actividad_tipo_id') : [],
                    "zonal" => \Input::has('zonal')? \Input::get('zonal') : [],
                    "cod_nodo" => \Input::has('cod_nodo')? \Input::get('cod_nodo') : [],
                    "tipo_operacion"
                            => \Input::has('tipo_operacion')? \Input::get('tipo_operacion') : [],
                    "u.estado_st"
                        => \Input::has('estado_st')? \Input::get('estado_st') : [],
                    "u.estado_aseguramiento"
                        => \Input::has('estado_aseguramiento')? \Input::get('estado_aseguramiento') : [],
                    "cod_troba"
                        => \Input::has('cod_troba')? \Input::get('cod_troba') : [],
                    "u.estado_ofsc_id"
                        => \Input::has('estado_ofsc')? \Input::get('estado_ofsc') : [],
                    "u.tipo_envio_ofsc"
                        => \Input::has('tipo_envio_ofsc')? \Input::get('tipo_envio_ofsc') : [],
                    "mo.estado_ofsc"
                        => \Input::has('tipo_devolucion_filtro')? \Input::get('tipo_devolucion_filtro') : [],
                    "u.quiebre_id"
                        => \Input::has('quiebre')? \Input::get('quiebre') : [],
                    "u.actividad_id"
                        => \Input::has('actividad')? \Input::get('actividad') : [],
                    "u.tipo_legado"
                        => \Input::has('tipo_legado')? ( is_array(\Input::get('tipo_legado'))? \Input::get('tipo_legado') : [\Input::get('tipo_legado')]): [],
                    "u.motivo_ofsc_id"
                        => \Input::has('motivo_devolucion_f')? \Input::get('motivo_devolucion_f') : [],
                    "u.parametro_id"
                        => \Input::has('parametro')? \Input::get('parametro') : [],
                    "u.workzone"
                        => \Input::has('workzone')? \Input::get('workzone') : [],
                    "u.bucket_id"
                        => \Input::has('bucket')? \Input::get('bucket') : [],
                    "u.usuario_gestionando"
                        => \Input::has('asignado')? array(\Auth::id()) : [],
                ],
                "arreglosNotEqual" => [
                    "id_solicitud_tecnica"
                        => \Session::get('tipoPersona') != 3 ? $this->repository->solicitudes() : [],
                ],
                "fechas" => [
                    "created_at" => explode(" - ", \Input::get("created_at", "")),
                    "fecha_agenda" => explode(" - ", \Input::get("fecha_agenda", "")),
                    ],

                ];

                 /*$paginado = [ "start" => \Input::get("start", 0),
                    "length" => \Input::get("length", 10),
                    "draw" => \Input::get("draw", 1)
                ];*/
                //$columns = \Input::get("columns");
                $order = [
                    "column" => \Input::get('column'),
                    "dir" => \Input::get('dir')
                ];

                $grilla['perPage'] = \Input::has('per_page') ? (int) \Input::get('per_page') : null;
                //$grilla["paginado"] = $paginado;
                $grilla["order"] = $order;
                $solicitudes = $gestion->busquedaGeneral($filtros, $grilla, $descargar);
                if ($descargar) {
                    $solicitudes = json_decode(json_encode($solicitudes), true);
                    return \Helpers::exportArrayToExcel($solicitudes, "SolicitudesTecnicas", ["actividad_id","estado_ofsc_id","actividad_tipo_id","quiebre_id","tipo_legado","estado_st","estado_aseguramiento","solicitud_tecnica_id", "estado_ofsc2"]);
                }
                return $solicitudes;

            }
        } catch (Exception $e) {
            return \Response::json($response);
        }
    }

    public function postDatasolicitud()
    {
        if (\Input::ajax()) {
            $tipoLegado = \Input::get("tipo_legado");
            $actividadId = \Input::get('actividad_id');
            $solicitudTecnicaId = \Input::get('solicitud_tecnica_id');
            $objgestion = new Gestion();

            $solicitud = null;
            if ($tipoLegado == "1") {
                return $objgestion->getSelectSolicitudCms($solicitudTecnicaId);
            }
            if ($tipoLegado == "2") {
                if ($actividadId == "1") { // averia
                    return $objgestion->getSelectSolicitudGestelAveria($solicitudTecnicaId);
                }
                if ($actividadId == "2") { // provision
                    return $objgestion->getSelectSolicitudGestelProvision($solicitudTecnicaId);
                }
            }
        }
    }
    public function postDetalleSolicitud()
    {
        $componentes = [];
        $logcambios = [];
        $movimientos = [];
        $comunicacionlego = [];
        $logrecepcion = [];
        //$ubicaciontecnico = [];
        $operaciones = [];
        //$imagenes = [];
        $tipoLegado = \Input::get("tipo_legado", 1);
        $objimpl = new STRepository;
        if (\Input::get('id')) {
            if (\Input::has("tipo_legado")) {
                switch ($tipoLegado) {
                    case '1':
                        $componentes = ComponenteCms::select(
                            "componente_cms.*",
                            "u.nombre",
                            "u.apellido"
                        )
                        ->leftJoin(
                            "solicitud_tecnica_cms AS stc",
                            "stc.id",
                            "=",
                            "componente_cms.solicitud_tecnica_cms_id"
                        )
                        ->leftjoin(
                            "usuarios as u",
                            "u.id",
                            "=",
                            "componente_cms.usuario_created_at"
                        )
                        ->where(
                            "stc.solicitud_tecnica_id",
                            "=",
                            \Input::get("id")
                        )
                        ->whereRaw("deleted_at IS NULL")
                        ->orderBy(
                            "id",
                            "DESC"
                        )
                        ->get();
                        break;
                    case '2':
                        $query = ComponenteGestel::select(
                            "componente_gestel.*",
                            "componente_gestel.desc_equipo as descripcion",
                            "componente_gestel.codigo_equipo as componente_cod",
                            \DB::raw(" '' as numero_requerimiento"),
                            \DB::raw(" '' as numero_servicio"),
                            "u.nombre",
                            "u.apellido"
                        );
                        if (\Input::get("actividad_id") == 1 ||
                            \Input::get("actividad_id") == 3) { // averia
                            $query->leftJoin(
                                "solicitud_tecnica_gestel_averia as stg",
                                "stg.id",
                                "=",
                                "componente_gestel.solicitud_tecnica_gestel_id"
                            );
                        } else {
                            $query->leftJoin(
                                "solicitud_tecnica_gestel_provision as stg",
                                "stg.id",
                                "=",
                                "componente_gestel.solicitud_tecnica_gestel_id"
                            );
                        }
                        $componentes = $query->leftjoin(
                            "usuarios as u",
                            "u.id",
                            "=",
                            "componente_gestel.usuario_created_at"
                        )
                        ->where(
                            "stg.solicitud_tecnica_id",
                            "=",
                            \Input::get("id")
                        )
                        ->where(
                            "componente_gestel.actividad_id",
                            "=",
                            \Input::get("actividad_id")
                        )
                        ->whereRaw("deleted_at IS NULL")
                        ->orderBy(
                            "id",
                            "DESC"
                        )
                        ->get();
                        break;
                    default:
                        # code...
                        break;
                }
            }
            
            $movimientos = Movimiento::from("solicitud_tecnica_movimiento as stm")->with('detalle', 'detalle.tecnico')
                ->where(["stm.solicitud_tecnica_id"  => \Input::get("id")])
                ->select(
                    \DB::raw("CONCAT(u.nombre,' ',u.apellido) AS usuario"),
                    "stm.created_at",
                    "stm.id",
                    "stm.sistema_externo_id",
                    "stm.coordx_tecnico",
                    "stm.coordy_tecnico",
                    "stm.estado_ofsc_id",
                    "stm.tipo_envio_ofsc",
                    "stm.intervalo",
                    \DB::raw("IFNULL(ea.nombre, '') as estado_aseguramiento_nombre"),
                    "ea.id as estado_aseguramiento",
                    "eo.nombre as estado",
                    \DB::raw("IFNULL(stm.fecha_agenda, '') as fecha_agenda"),
                    \DB::raw("IFNULL(stm.resource_id, '') as resource_id"),
                    \DB::raw("IFNULL(mo.descripcion, '') as motivo"),
                    \DB::raw("IFNULL(so.descripcion, '') as submotivo"),
                    "stm.xa_note as observacion",
                    "stm.xa_observation as observaciontoa",
                    "stu.actividad_id as actividad",
                    \DB::raw("CONCAT(t.nombres,' ',t.ape_paterno,' ',t.ape_materno) AS tecnico"),
                    \DB::raw(
                        "(SELECT COUNT(*) as cant_detalle FROM solicitud_tecnica_movimiento_detalle 
                    WHERE solicitud_tecnica_movimiento_id=stm.id AND IFNULL(campo1,'')<>''AND (tipo IN (3,4,5,6))
                    ) as cantdetalle"
                    )
                )
                ->leftjoin("usuarios as u", "u.id", "=", "stm.usuario_created_at")
                ->leftjoin("estados_ofsc as eo", "eo.id", "=", "stm.estado_ofsc_id")
                ->leftjoin("motivos_ofsc as mo", "mo.id", "=", "stm.motivo_ofsc_id")
                ->leftjoin("submotivos_ofsc as so", "so.id", "=", "stm.submotivo_ofsc_id")
                ->leftJoin("estados_aseguramiento as ea", "ea.id", "=", "stm.estado_aseguramiento")
                ->leftJoin("tecnicos as t", "t.id", "=", "stm.tecnico_id")
                ->leftjoin("solicitud_tecnica_ultimo as stu","stu.solicitud_tecnica_id","=","stm.solicitud_tecnica_id")
                ->orderBy("stm.id", "DESC")
                ->get();

            $logcambios = $objimpl->getLogCambiosCache(\Input::get("id"));
            foreach ($logcambios as $key => $value) {
                $antes = json_decode($value->antes, true);
                $htmlantes = "";
                foreach ($antes as $key2 => $value2) {
                    $htmlantes.="<b>$key2</b>  =>  ".$value2."<br>";
                }
                $value->htmlantes = $htmlantes;

                $despues = json_decode($value->despues, true);
                $htmldespues = "";
                foreach ($despues as $key2 => $value2) {
                    if (!is_array($value2)) {
                        $htmldespues.="<b>$key2</b>  =>  ".$value2."<br>";
                    }
                }
                $value->htmldespues = $htmldespues;

                $logcambios[$key] = $value;
            }
            $enviolegado = $objimpl->getLogComunicacionLegadoCache(\Input::get("id"));
            foreach ($enviolegado as $key => $value) {
                $xmlrequest = \Array2XML::createXML('psi', json_decode($value["request"], true));
                $value["xml_request"] = $xmlrequest->saveXML();

                $xmlresponse = \Array2XML::createXML('psi', json_decode($value["response"], true));
                $value["xml_response"] = $xmlresponse->saveXML();

                $comunicacionlego[$value["accion"]][] = $value;
            }

            $logrecepcion = $objimpl->getLogCache(\Input::get("id"));
            foreach ($logrecepcion as $key => $value) {
                $xmltrama = \Array2XML::createXML('generar', json_decode($value["trama"], true));
                $value["xmltrama"] = $xmltrama->saveXML();
                $logrecepcion[$key] = $value;
            }
            $operacion = ComponenteOperacion::getComponenteOperacion(\Input::get("idcms"), 1);
            foreach ($operacion as $key => $value) {
                $operaciones[$value->operacion][] = $value;
            }
        }
        return \Response::json(
            array(
                'rst' => 1,
                'componentes' => $componentes,
                'logcambios'  => $logcambios,
                'movimientos' => $movimientos,
                'comunicacionlego' => $comunicacionlego,
                'logrecepcion' => $logrecepcion,
                //'ubicaciontecnico' => $ubicaciontecnico,
                'operaciones' => $operaciones,
                //'imagenes' => $imagenes,
            )
        );
    }
    /**
     * solo para cms
     */
    public function postGuardar()
    {
        $cms = SolicitudTecnicaCms::find(\Input::get("id"));
        $ultimo = Ultimo::where('solicitud_tecnica_id', \Input::get("solicitud_tecnica_id"))->first();

        if (!is_null($ultimo)) {
            if ($ultimo->estado_ofsc_id!=1 && $ultimo->estado_ofsc_id!=8) {
                return \Response::json(
                    array(
                        'rst' => 2,
                        'msj' => "La orden debe estar en pendiente o no enviada para poder actualizarse"
                    )
                );
            }
        } else {
            return \Response::json(
                array(
                        'rst' => 2,
                        'msj' => "Error"
                    )
            );
        }
        $only = \Input::only(array_keys(SolicitudTecnicaCms::$rules));
        //actualizar
        $except = [
            'comunicacionlego',
            'logrecepcion',
            'movimientos',
            'componentes',
            'imagenes',
            'logcambios',
            'operaciones'
        ];
        //comparar campos
        $cambios =[];
        foreach ($only as $key => $value) {
            if ($value != $cms[$key]) {
                array_push($cambios, [$key=>$value]);
            }
        }
        return \Response::json(['rst' => 1,'msj' => $msj ]);
        /*
        $resultado = $cms->update($only);

        $msj = '';
        if ($resultado) {
            $msj = 'Se actualizaron los datos ';
        } else {
            return \Response::json(['rst' => 1,'msj' => 'Error al actualizar campos' ]);
        }

        if ($ultimo->estado_ofsc_id!=1) {
            //si no es pendiente no deberia actualizar en toa
            return \Response::json(['rst' => 1,'msj' => $msj ]);
        }*/
        if (count($cambios)>0) {
            foreach ($cambios as $key => $value) {
                var_dump($value);
            }
        }
        $result = null;
        $propiedades = [];
        $appointment = [];
        if (!is_null($obj)) {
            if (isset($datamapa["coordx_cliente"])) {
                if ($obj->coordx_cliente != $datamapa["coordx_cliente"]) {
                    $appointment['coordx'] = floatval($datamapa["coordx_cliente"]);
                }
            }

            if (isset($datamapa["coordy_cliente"])) {
                if ($obj->coordy_cliente != $datamapa["coordy_cliente"]) {
                    $appointment['coordy'] = floatval($datamapa["coordy_cliente"]);
                }
            }

            if (isset($datadespues["telefono1"])) {
                if ($datadespues["telefono1"]!=$obj->telefono1) {
                    $appointment["phone"] = $datadespues["telefono1"];
                    $propiedades['XA_CONTACT_PHONE_NUMBER_2'] = $datadespues["telefono1"];
                }
            }

            if (isset($datadespues["telefono2"])) {
                if ($datadespues["telefono2"]!=$obj->telefono1) {
                    $appointment["cell"] = $datadespues["telefono2"];
                    $propiedades['XA_CONTACT_PHONE_NUMBER_3'] = $datadespues["telefono2"];
                }
            }
            
            $direccionantes = $obj->full_address;
            
            
            $direcciondespues = $obj->full_address;
            $objinbound = new Inbound();

            if ($direccionantes!= $direcciondespues) {
                $appointment['address'] = $direcciondespues;
            }

            if (count($appointment) > 0 || count($propiedades) > 0) {
                $response = $objinbound->updateActivity($obj->num_requerimiento, $propiedades, $appointment);
            }
        }

        return \Response::json(
            array(
                'rst' => 1,
                'solicitud' => $id_solicitud_tecnica
            )
        );
    }

    public function postCapacityMultiple()
    {
        $tipoLegado = \Input::get("tipo_legado");
        $actividadId = \Input::get("actividad_id");
        $ffttcobre = \Input::get("ffttcobre", "");
        $reglas = [];
        if ($tipoLegado == 1) {
            $reglas = array(
                'quiebre'  => 'required|Integer',
                'actividad_tipo_id' => 'required|exists:actividades_tipos,id',
                'troba' => 'required|alpha_num',
                //'plano' => 'required|alpha_num',
                'nodo' => 'required|alpha_num',
            );
        }
        if ($tipoLegado == 2) {
            $reglas = array(
                'quiebre'  => 'required|Integer',
                'actividad_tipo_id' => 'required|exists:actividades_tipos,id',
                //'plano' => 'required|alpha_num',
                // 'nodo' => 'required|alpha_num',
            );
        }

        $mensaje = array(
            'required'      => 'esta solicitud no tiene :attribute',
            'exists'       => ':attribute no se encontro en el sistema',
            'alpha_num'       => ':attribute debe ser alfanumerico',
            'Integer'       => ':attribute Solo debe ser entero'
        );
        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);

        if ($validator->fails()) {
            return \Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages()->all()[0]
                )
            );
        }


        $quiebre = \Quiebre::find(\Input::get('quiebre'));
        $quiebreGrupo = $quiebre->quiebregrupos;
        $actividadTipo = \ActividadTipo::find(\Input::get('actividad_tipo_id'));
        $microzona = "";
        if ($tipoLegado == 1) {
            if (\Input::get('nodo')!='') {
                if (\Input::get('troba')!='') {
                    $microzona = \Input::get('nodo')."_".\Input::get('troba');
                } elseif (\Input::get('plano')!='') {
                    $microzona = \Input::get('nodo')."_".\Input::get('plano');
                }
            }
            if ($microzona=="") { //GEOCERCA
                if (\Input::get('solicitud')) {
                    $solicitudTecnica = SolicitudTecnica::where(
                        'id_solicitud_tecnica',
                        \Input::get('solicitud')
                    )->first();
                    $requerimiento = $solicitudTecnica->cms;
                    $microzona=$requerimiento->getGeozona($actividadId);
                }
            }
        }
        if ($tipoLegado == 2) {
            if ($actividadId == 1) {
                if (\Input::get('cabecera_mdf') != "") {
                    if (\Input::get('cod_armario') != "") {
                            $workZone = \Input::get('cabecera_mdf')."_".\Input::get('cod_armario');
                    } elseif (\Input::get('cable') != "") {
                            $workZone = \Input::get('cabecera_mdf')."_".\Input::get('cable');
                    }
                }
            } elseif ($actividadId == 2) {
                $solicitudGestelProvision = new SolicitudTecnicaGestelProvision();
                $microzona = $solicitudGestelProvision->getWorkZone($ffttcobre);
            }
        }
        
        $fechaARecorrer = $fechaInicial = date('Y-m-d');

        $msjAct = '';
        $rst = 2;
        $capacityDuration = '';
        $capacityObject = [];
        $slotObject = [];
        $capacityObjectToday = [];
        $capacityData = null;
        $response = [];
        $timeSlot = 'AM_PM';

        $bucketid=[];

        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            && $quiebre!='') {

            $franja=explode("_", $timeSlot);
            
            for ($i=0; $i < 7; $i++) {
                $workType = $actividadTipo->label;
                $workZone = $microzona;
                foreach ($franja as $key => $value) {
                    $llave = $fechaARecorrer.'|'.$value.'|'.$workType.'|'.$quiebre->apocope.'|'.$workZone;
                    $rst = \Redis::get($llave);

                    $capacidad = json_decode($rst, true);
                    $response['capacity'][] = json_decode($rst, true);
                    if (isset($capacidad["location"])) {
                    //} elseif (\App::environment('local'))
                    } else {
                        //conultar la capacidad directamente a oracle
                        $data=[
                                'fecha'          => $fechaARecorrer,
                                'time_slot'      => '',
                                'worktype_label' => $workType,
                                'quiebre'        => $quiebre->apocope,
                                'zona_trabajo'   => $workZone,
                            ];
                        $capacity = new Capacity();
                        
                        $response2 = $capacity->getCapacity($data);
                        if (isset($response2->error) && $response2->error===false
                            && isset($response2->data->capacity)) {

                            $capacidad = $response2->data->capacity;
                            $timeSlot = $response2->data->time_slot_info;
                            foreach ($capacidad as $key => $val) {
                                if (isset($val->work_skill)) {
                                    $val->{"time_slot_info"} = $timeSlot;
                                    $response['capacity'][] = json_decode(json_encode($val), true);
                                    $clave=$val->date.'|'.
                                           $val->time_slot.'|'.
                                           $workType.'|'.
                                           $quiebre->apocope.'|'.
                                           $workZone;
                                    \Redis::set($clave, json_encode($val));
                                    \Redis::expire($clave, 3600*24);//24 horas -> 3600s
                                }
                            }
                        }
                    }
                }
                $fechaARecorrer = date("Y-m-d", strtotime($fechaARecorrer . " + 1 days"));

            }
            
            if (count($response['capacity']) > 0) {
                $capacityData = $response;
                $timeSlotData = [];
                $capacityDuration = $actividadTipo->duracion;
                foreach ($capacityData['capacity'] as $key => $val) {
                    if (isset($val['location'])) {
                        $search = array_search($val['location'], $bucketid);
                        if (false!==$search) {
                            1;
                        } else {
                            $bucketid[]=$val['location'];
                        }
                    }
                    if (isset($val['time_slot'])) {
                        $capacityObject["{$val['date']}|{$val['time_slot']}"] =
                            $val;
                    }
                    if (isset($val['time_slot'])
                        && $val['date'] == $fechaInicial) {
                        $capacityObjectToday[] = $val;
                        // timeSlot today
                        if (isset($val['time_slot_info'])) {
                            $timeSlotData = $val['time_slot_info'];
                        }
                    }
                }
                if (isset($timeSlotData[0])
                    && is_array($timeSlotData)) {
                    foreach ($timeSlotData as $key => $val) {
                        $slotObject["{$val['name']}|{$val['label']}"] = $val;
                    }
                } elseif (count($timeSlotData) > 0
                    && is_object($timeSlotData)) {
                    $val = $timeSlotData;
                    $slotObject["{$val['name']}|{$val['label']}"] = $val;
                }
                $rst = 1;
            } else {
                $msjAct = '(C) No hay capacidad en TOA. ';
            }

        } else {
                $msjAct = 'Esta actividad no esta '
                    . 'definida para enviar a TOA. ';
        }

        $a = [
            'capacity' => $capacityObject,
            'timeslot' => $slotObject,
            'today' => [
                'capacity' => $capacityObjectToday,
                'time_slot_info' => isset($timeSlotData)?
                $timeSlotData : []
            ],
            'location'=>$bucketid,
            'duracion' => $capacityDuration,
            'msj' => $msjAct,
            'rst' => $rst
        ];
        if (\Input::has('json')) {
            $a = \Response::json($a);
        }
        return $a;
    }

    public function postEnvioofsc()
    {
        $solicitudTecnicaId = \Input::get("solicitud_tecnica_id", 0);
        $inputs = [
            'actividad_tipo_id'     => \Input::get('actividad_tipo_id'),
            'actividad_id'          => \Input::get('actividad_id'),
            'tematicos'             => \Input::get("tematicos", []),
            'observacion_toa'       => \Input::get("observacion_toa", ""),
            'solicitud_tecnica_id'  => \Input::get("solicitud_tecnica_id", 0),
            // 'ffttcobre'             => \Input::has("ffttcobre") ? \Input::get("ffttcobre") : [],
            'agdsla'                => \Input::get('agdsla'),
            'hf'                    => \Input::get('hf'),
            'quiebre'               => \Input::get('quiebre')
        ];

        if (\Input::has("XA_IND_NOT_TIDY")) {
            $inputs['XA_IND_NOT_TIDY'] = \Input::get('XA_IND_NOT_TIDY');
        }

        if (\Input::has("cupos")) {
            $inputs['cupos'] = \Input::get('cupos');
        }

        if (\Input::has("nenvio")) {
            $inputs['nenvio'] = \Input::get('nenvio');
        }

        $solicitudGestion = new StGestionRepository();
        $response = $solicitudGestion->envioOfsc($solicitudTecnicaId, $inputs);
        return $response;
    }

    public function postEnviarsms()
    {
        $numeros = \Input::get("numeros_telefonicos", "");
        $mensaje = \Input::get("contenido_sms", "");
        $mensaje = trim($mensaje);
        if (strlen($mensaje) > 140) {
            return \Response::json(
                array(
                    'rst' => 2,
                    'msj' => "Mensaje debe tener como maximo 140 caracteres"
                )
            );
        }
        if (strlen($mensaje) == 0) {
            return \Response::json(
                array(
                    'rst' => 2,
                    'msj' => "Mensaje no puede esta vacio"
                )
            );
        }
        if ($numeros !="") {
            $numeros = explode(",", $numeros);
            foreach ($numeros as $key => $value) {
                if (strlen($value) > 0) {
                    $celular = substr($value, -9);
                    \Sms::enviar($celular, $mensaje, '1');

                }
            }
        }

        return \Response::json(
            array(
                'rst' => 1,
                'msj' => 'Mensaje Enviado Satisfactoriamente',
                
            )
        );
    }

    public function postStoporden()
    {
        $solicitudTecnicaId = \Input::get("solicitud_tecnica_id", 0);
        $inputs = [
            'comentario'            => \Input::get("comentario", ""),
            'tipoDevolucion'        => \Input::get("tipo_devolucion", ""),
            'tematicos'             => is_array(\Input::get("tematicos")) ?
                                       \Input::get("tematicos") : json_decode(\Input::get("tematicos"), true),
            'motivodevolucion'      => \Input::get("motivosdevolucion", ""),
            'motivoliquidacion'     => \Input::get("motivosliquidacion", ""),
            'submotivoDevolucion'   => \Input::get("submotivosdevolucion", "")
        ];
        $solicitudGestion = new StGestionRepository();
        $response = $solicitudGestion->stopOrden($solicitudTecnicaId, $inputs);
        return $response;
    }

    public function postResetformulariotoa()
    {
        $solicitudTecnicaId = \Input::get("solicitud_tecnica_id", 0);
        $inputs = [
            'comentario'        => \Input::get("comentario", ""),
            'tipoDevolucion'    => \Input::get("tipo_devolucion", ""),
            'tematicos'         => \Input::get("tematicos")
        ];
        $solicitudGestion = new StGestionRepository();
        $response = $solicitudGestion->resetformulariotoa($solicitudTecnicaId, $inputs);
        return $response;
    }

    public function postReenviooperaciones()
    {
        $inputs = [];
        $inputs_updt = [];
        $response = "";
        $accion = "";
        
        $id = \Input::get("id");
        $estado = \Input::get("estado".$id);
        $solicitud_tecnica_id = \Input::get("solicitud_tecnica_id");
        
        $objimpl = new STRepository;
        $enviolegado = $objimpl->getLogComunicacionLegadoCache($solicitud_tecnica_id);
        foreach ($enviolegado as $key => $value) {
            if ($value["id"] == $id) {
                $inputs = json_decode(
                    str_replace(
                        'componentes_cod',
                        'componente_cod',
                        str_replace(
                            'componente',
                            'componentes',
                            $value["request"]
                        )
                    ),
                    true
                );
                $accion=$value["accion"];
            }
        }
        $inputs['solicitud_tecnica_id'] = $solicitud_tecnica_id;

        if ($estado == 1) {
            foreach ($inputs as $key => $val) {
                if ($key != 'componentes') {
                    if ($key == 'solicitud_tecnica_id') {
                        $inputs_updt[$key]=$val;
                    } else {
                        if ($key == 'IndicadorRespuesta') {
                            $c=0;
                            $c=$c+1;
                            $cont='_'.$c;
                            $data=\Input::get($key.$cont);
                            $inputs_updt[$key]=strtoupper($data);
                        } else {
                            $data=\Input::get($key);
                            $inputs_updt[$key]=strtoupper($data);
                        }
                    }
                } else {
                    if ($key == 'componentes') {
                        $componente=$val;
                        $comp=[];
                        foreach ($componente as $key => $val) {
                            $cont='_'.($key+1);
                            foreach ($val as $k => $v) {
                                $data=\Input::get($k.$cont);
                                $comp[$k]=strtoupper($data);
                            }
                            $inputs_updt['componentes'][$key]=$comp;
                        }
                    }
                }
            }
            $inputs=$inputs_updt;
        }
        
        \Input::replace($inputs);
        switch ($accion) {
            case 'asignacionDecos':
                $response = \Helpers::ruta('componentelegado/activar', 'POST', $inputs, false);
                break;
            case 'desasignacionDecos':
                $response = \Helpers::ruta('componentelegado/desasignar', 'POST', $inputs, false);
                break;
            case 'cambioAveria':
                $response = \Helpers::ruta('componentelegado/cambioaveria', 'POST', $inputs, false);
                break;
            case 'revertirAveria':
                $response = \Helpers::ruta('componentelegado/revertiraveria', 'POST', $inputs, false);
                break;
            case 'reposicion':
                $response = \Helpers::ruta('componentelegado/reposicion', 'POST', $inputs, false);
                break;
            case 'revertirReposicion':
                $response = \Helpers::ruta('componentelegado/revertireposicion', 'POST', $inputs, false);
                break;
            case 'refresh':
                $response = \Helpers::ruta('componentelegado/refresh', 'POST', $inputs, false);
                break;
            case 'CierreSTProvision'://gestel
                $response = \Helpers::ruta('bandejalegado/reenviarlegado', 'POST', $inputs, false);
                break;
            case 'CierreSTAverias'://gestel
                $response = \Helpers::ruta('bandejalegado/reenviarlegado', 'POST', $inputs, false);
                break;
            case 'CierreSolicitudTecnica'://cms
                //$response = \Helpers::ruta('bandejalegado/reenviarlegado', 'POST', $inputs, false);
                $solicitud = SolicitudTecnica::find($solicitud_tecnica_id);
                $ultimo = $solicitud->ultimo;
                $estado_ofsc = $ultimo->estado_ofsc_id;
                $tipo_legado = $ultimo->tipo_legado;
                $actividad = $ultimo->actividad_id;
                $tipo_operacion='';

                if ($tipo_legado == 1 && $estado_ofsc != "") {
                    
                    if ($estado_ofsc == 6) {
                        //liquidacion
                        if ($actividad == 1) {
                            $tipo_operacion='ACI_LIQ';
                        } else {
                            $tipo_operacion='PCI_LIQ';
                        }
                    } else {
                        //devolucion
                        if ($actividad == 1) {
                            $tipo_operacion='ACI_DEV';
                        } else {
                            $tipo_operacion='PCI_DEV';
                        }
                    }

                    $api = new STCmsApi;
                    $inputs['tipo_operacion']=$tipo_operacion;
                    $response = $api->cierre($inputs);
                } else {
                    $response["rst"] = 2;
                    $response["msj"] = "Estado OFSC no cumple para enviar a Legados";
                }

                break;
            case 'RptaEnvioSolicitudTecnica'://
                $api = new STCmsApi;
                $request = [
                    'id_solicitud_tecnica' => $inputs['NumeroSolicitud'],
                    'id_respuesta' => $inputs['IndicadorRespuesta'],
                    'observacion' => $inputs['Observacion'],
                    'solicitud_tecnica_id' => $inputs['solicitud_tecnica_id']
                ];
                $response = $api->responderST($request);
                break;
            default:
                break;
        }
        return \Response::json(
            array(
                'rst' => isset($response->rst)? $response->rst : 1,
                'msj' => isset($response->msj)? $response->msj : "Envio Trama a Legado con Exito"
            )
        );
    }

    public function getMotivosdevolucion()
    {
        return \MotivoOfsc::select(
            "motivos_ofsc.*",
            "motivos_ofsc.estado_ofsc as tipo"
        )->where(["estado" => 1, "estado_ofsc_id" => 4])
        ->whereRaw("motivos_ofsc.codigo_legado IS NOT NULL")
        ->whereRaw("motivos_ofsc.tipo_legado IS NOT NULL")
        ->whereRaw("motivos_ofsc.estado_ofsc IS NOT NULL")
        ->whereRaw("motivos_ofsc.actividad_id IS NOT NULL")
        ->remember(24)
        ->get();
    }

    public function getTematicos()
    {
        //$data = Tematicos::where(["estado" => 1])->remember(24)->get();
        $data = Tematicos::where(["estado" => 1])->get();
        $tematicos = [];
        foreach ($data as $key => $value) {
            $tematicos[$value->parent][] = $value;
        }

        return $tematicos;
    }
    /**
     * obtener las imagenes de una actividad en toa
     */
    public function postImagenes()
    {
        $reglas = array(
            'aid'  => 'required|Integer',
            'tipo' => 'required|in:general,inicio,trabajo,pre_trabajo',
        );
        if (\App::environment('local')) {
            $aid = 47662;
        } else {
            $aid = \Input::get("aid");
        }
        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'Integer'       => ':attribute Solo debe ser entero'
        );
        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);

        if ($validator->fails()) {
            return \Response::json(
                [
                'rst' => 2,
                'msj' => $validator->messages()
                ]
            );
        }
        //$aid = "16132";
        $activity = new Activity();
        $listaImagenToa = \Config::get("ofsc.image.". \Input::get('tipo'));
        $labels = $imagenes = [];
        $pathRelativa = 'imagenes/';
        $i=1;
        $path = storage_path($pathRelativa);

        //cruzar con las imagenes de configurcion
        foreach ($listaImagenToa as $key => $value) {
            //nombre del archivo
            $filename  = "{$aid}-{$key}.jpg";
            //buscar en configuracon
            //validar si archivo existe, antes de consultar a toa
            if (\File::exists($path.$filename)) {
                $imagenes[] = [
                    'class'     => ($i==1) ? 'item active' : 'item',
                    'src'       => $pathRelativa.$filename,
                    'nombre'    => $listaImagenToa[$key]
                ];
                $i++;
            } else {
                $response = $activity->getFile($aid, $key);
                //validar resultado de toa
                if (isset($response->result_code) && $response->result_code == 0) {
                    $binFile = implode('', ((array) $response->file_data));
                    //crear imagen en webpsi
                    if (\File::put($path.$filename, $binFile)) {
                        $imagenes[] = [
                            'class'     => ($i==1) ? 'item active' : 'item',
                            'src'       => $pathRelativa.$filename,
                            'nombre'    => $listaImagenToa[$key]
                        ];
                        $i++;
                    }
                }
            }
        }

        $resonse =[
            'rst'=>1,
            'mensaje'=>count($imagenes),
            'imagenes' =>$imagenes
        ];
        return \Response::json($resonse);
    }

    public function postGuardartematico()
    {
        $solicitud_tecnica_id=\Input::get("solicitud_tecnica_id", "");
        $solicitudTecnica = SolicitudTecnica::find(\Input::get("solicitud_tecnica_id"));
        /*$validacion_redis = $this->validarRedisUsuario($solicitudTecnica->id_solicitud_tecnica);
        if ($validacion_redis['estado']) {
            return \Response::json([
                'rst' => 2,
                'msj' => $validacion_redis['msj']
            ]);
        }*/

        $tematicos = \Input::get("tematicos");
        $motivoDevolucion = \Input::get("motivoDevolucion", "");
        $submotivoDevolucion = \Input::get("submotivoDevolucion");
        $tel1 = \Input::get("tel1", "");
        $tel2 = \Input::get("tel2", "");
        $contacto = \Input::get("contacto", "");
        $direccion =\Input::get("direccion", "");
        $chk_freeview=\Input::get("chk_freeview", "");
        $chk_descuento=\Input::get("chk_descuento", "");
        $comentarios = \Input::get("comentario", "");
        $actividad_id = \Input::get("actividad_id");
        $tipoDevolucion = \Input::get("tipoDevolucion");
        $tipodevolucionst = \Input::get("tipo_devolucion", "");

        $response  = ["rst" => 1, "msj" => ""];
        $textchkfreeview = "";
        $textchkdescuento = "";
        if ($chk_freeview) {
            $textchkfreeview = "Freeview";
        }
        if ($chk_descuento) {
            $textchkdescuento = "Descuento";
        }
        $comentarioArmado= "Tel1 : ".$tel1.","."Tel2 : ".$tel2.","."Contacto : ".$contacto.","."Direccion :".$direccion.",".$textchkfreeview.",".$textchkdescuento.", Motivo : ".$motivoDevolucion;
        
        $ultimo = $solicitudTecnica->ultimo;

        if (!is_null($ultimo)) {
            if ($motivoDevolucion <> "") {
                $motivoofsc = \MotivoOfsc::find($motivoDevolucion);
                $submotivo=null;
                if ($submotivoDevolucion!='') {
                    $submotivo=$submotivoDevolucion;
                }
                $actuacion = [
                    'tel1'          => $tel1,
                    'tel2'          => $tel2,
                    'contacto'      => $contacto,
                    'direccion'     => $direccion,
                    'freeview'      => $chk_freeview,
                    'descuento'     => $chk_descuento,
                    'motivo_ofsc_id'=> $motivoofsc->id,
                    'submotivo_ofsc_id'=>$submotivo
                ];
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                       
                        $ultimo[$key] = $actuacion[$key];
                    }
                }
                $solicitudTecnica->ultimo()->save($ultimo);

                /*$ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                foreach (Movimiento::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimoMovimiento[$key] = $actuacion[$key];
                    }
                }
                $ultimoMovimiento->save();*/
            }

            if (count($tematicos) > 0) {
                $tipo=0;
                //Averia
                if ($actividad_id==1) {
                    $tipo=6;
                }
                
                //Provision
                if ($actividad_id==2) {
                    if ($tipodevolucionst!="") {
                        if ($tipodevolucionst == "C") {
                            $tipo = 3;
                        }
                        if ($tipodevolucionst == "T") {
                            $tipo = 4;
                        }
                    } else {
                        if ($tipoDevolucion=='Comercial') {
                            $tipo=3;
                        }
                        if ($tipoDevolucion=='Tecnico') {
                            $tipo=4;
                        }
                    }
                }

                $data = [
                    "solicitud_tecnica_id"  => $ultimo->solicitud_tecnica_id,
                    "tipo"                  => $tipo,
                    "comentarios"           => $comentarios,
                    "observacion_form"      => $comentarioArmado
                ];
                $solicitudTecnicaGestion = new Gestion();
                $save = $solicitudTecnicaGestion->generarMovimientoconTematico(
                    $tematicos,
                    [],
                    $data
                );

                $response["msj"] = "Tematico Registrado";
            }
        } else {
            $response["rst"] = 2;
            $response["msj"] = "Error en Data de PSI";
        }
        return \Response::json($response);
    }

    public function postReenviarlegado()
    {
        $tematicos = \Input::get("tematicos");
        $comentario = \Input::get("comentario", "");
        $solicitud = SolicitudTecnica::find(\Input::get("solicitud_tecnica_id", 0));
        $movimiento = $solicitud->ultimoMovimiento;
        $ultimo = $solicitud->ultimo;
        $observacionToa = $solicitud->ultimo->xa_observation;
        $comentario = $comentario.' TOA: '.$observacionToa;
        $comentario = substr($comentario, 0, 99);
        $response  = ["rst" => 1, "msj" => ""];
        if (!is_null($solicitud)) {

            $estado_ofsc_id = $ultimo->estado_ofsc_id;
            $motivo_ofsc_id = $ultimo->motivo_ofsc_id;

            $objmotivoofsc = \MotivoOfsc::find($motivo_ofsc_id);
            if (!is_null($objmotivoofsc) or 1==1) {
                if ($ultimo->tipo_legado == 1 && $estado_ofsc_id != "") {
                    $solicitudCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", \Input::get("solicitud_tecnica_id"))->first();
                    if ($estado_ofsc_id == 6) {
                        $solicitudCms->liquidacionCms();
                    } else {
                        $solicitudCms->devolucionCms();
                    }
                } elseif ($ultimo->tipo_legado == 2 && $estado_ofsc_id != "") {
                    if ($ultimo->actividad_id == 1) {
                        $solicitudGestelAveria = SolicitudTecnicaGestelAveria::where("solicitud_tecnica_id", \Input::get("solicitud_tecnica_id"))->first();
                        if ($estado_ofsc_id == 6) {
                            $solicitudGestelAveria->liquidacionGestelAveria();
                        } else {
                            $solicitudGestelAveria->devolucionGestelAveria();
                        }
                    } else {
                        $solicitudGestelProvision = SolicitudTecnicaGestelProvision::where("solicitud_tecnica_id", \Input::get("solicitud_tecnica_id"))->first();
                        if ($estado_ofsc_id == 6) {
                            $solicitudGestelProvision->liquidacionGestelProvicion();
                        } else {
                            $solicitudGestelProvision->devolucionGestelProvicion();
                        }
                    }
                } else {
                    $response["rst"] = 2;
                    $response["msj"] = "Estado OFSC no cumple para enviar a Legados";
                }

                $ultimo->estado_aseguramiento = 6;
                $ultimo->save();
                $response["msj"] = "Actividad Enviada a Legado";
            } else {
                $response["rst"] = 2;
                $response["msj"] = "No se tiene un motivo o codigo para enviar a Legado";
            }
        } else {
            $response["rst"] = 2;
            $response["msj"] = "Error en Data de PSI";
        }
        return \Response::json($response);
    }

    public function postActivarprecierre()
    {
        $tematicos = \Input::get("tematicos");
        $comentarios = \Input::get("comentario", "");
        $solicitud = SolicitudTecnica::find(\Input::get("solicitud_tecnica_id", 0));
        if (!is_null($solicitud)) {
            $ultimo = $solicitud->ultimo;
            if ($ultimo->estado_ofsc_id == 2) {
                $objinbound = new Inbound;
                $properties = [];
                $properties["A_IND_PRUEBA"] = 2;
                $properties["A_IND_REQ_PRUEBA"] = 2;
                $responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties, [], "update_activity");
                if (isset($responseToa->result) && $responseToa->result == "success") {
                    $solicitudTecnicaGestion = new Gestion();
                    if (count($tematicos) > 0) {
                        $data = [
                            "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                            "tipo" => 6,
                            "comentarios" => $comentarios
                        ];
                        $save = $solicitudTecnicaGestion->generarMovimientoconTematico(
                            $tematicos,
                            [],
                            $data
                        );
                    }
                    $tecnico = \Tecnico::find($ultimo->tecnico_id);
                    $celular = (is_null($tecnico)) ? "" : $tecnico->celular;
                    $mensaje = "Se activo el formulario de precompletado en TOA";
                    if ($celular!="") {
                        $mensaje = 'Req. '.$ultimo->num_requerimiento.', '.$mensaje;
                        \Sms::enviar($celular, $mensaje, '1');
                    }
                    return \Response::json(
                        [
                        "rst" => 1,
                        "msj" => $mensaje
                        ]
                    );
                } else {
                    $error = isset($responseToa->errorMsg)? $responseToa->errorMsg:'error desconocido en TOA';
                    return \Response::json(
                        [
                        "rst" => 2,
                        "msj" => $error
                        ]
                    );
                }
            }
        }
    }
    public function postForzarvalidacioncoordenadas()
    {
        $codactu = \Input::get("codactu", "");
        $aid = \Input::get("aid", 0);
        $tecnico = \Tecnico::find(\Input::get("tecnico_id", 0));
        $properties["A_ESTADO_VALIDACION_COORDENADAS"]='5';
        $response = ["rst" => 1, "msj" => ""];
        
        $inbound = new Inbound();
        $responseToa = $inbound->updateActivity($codactu, $properties);
        if (isset($responseToa->result) && $responseToa->result == "success") {
            $mensaje = "Req. ".$codactu." : Validacion de Coordenadas Forzadas por el Despacho con Exito";
            $comentarios = "Validacion de Coordenadas Forzadas. ".\Input::get("comentario", "");
            $response["msj"] = "Se ha forzado la Validacion de Coordenadas con Exito";
        } else {
            $mensaje = "Req. ".$codactu." : Validacion de Coordenadas Forzadas por el Despacho ha Fallado";
            $comentarios = "Validacion de Coordenadas Forzadas Fallidas. ".\Input::get("comentario", "");
            $response["rst"] = 2;
            $response["msj"] = "Ha fallado la Validacion de Coordenadas";
        }
        if (!is_null($tecnico)) {
            \Sms::enviarSincrono($tecnico->celular, $mensaje, '1', '');
        }
        //insertar registro de transaccion
        $movimiento = Movimiento::where('aid', $aid)->latest()->first();
        $detalle = new MovimientoDetalle;
        if ($movimiento) {
            $ultimodetalle = MovimientoDetalle::where('solicitud_tecnica_movimiento_id', $movimiento->id)->latest()->first();
            $detalle["solicitud_tecnica_movimiento_id"] = $movimiento->id;
            $detalle["comentarios"] = $comentarios;
            $detalle["sms"] = $mensaje;
            $detalle["tipo"] = 1;
            $detalle->save();
        }
        return \Response::json($response);
    }

    public function postGuardardatosgo()
    {
        $response = ["rst" => 1, "msj" => ""];
        $solicitud = SolicitudTecnica::find(\Input::get("solicitud_tecnica_id", 0));
        $contrata = \Input::get("contrata", "");
        $quiebreid = \Input::get("quiebre_id");
        $actividadtipoid = \Input::get("actividad_tipo_id", null);
        $nodo = \Input::get("nodo", null);
        $troba = \Input::get("troba", null);

        $objempresa = null;
        if (!is_null($solicitud)) {
            $objultimo = $solicitud->ultimo;
            $estadoofscid = $objultimo->estado_ofsc_id;
            $tipolegado = $objultimo->tipo_legado;
            $actividadid = $objultimo->actividad_id;
            if ($estadoofscid  == 1 || $estadoofscid  == 8) {
                if ($objultimo->tipo_legado == 1) {
                    if ($contrata!="") {
                        $obj = $solicitud->cms;
                        $obj->codigo_contrata = $contrata;
                        if (!is_null($quiebreid) && $quiebreid!="") {
                            $obj->quiebre_id = $quiebreid;
                        }
                        if (!is_null($nodo)) {
                            $obj->cod_nodo=$nodo;
                            $objnodo = Nodo::where('nodo', '=', $nodo)->first();
                            if (!is_null($objnodo)) {
                                $desc_nodo=$objnodo->nombre;
                                $obj->desc_nodo=$desc_nodo;
                            }
                        }
                        if (!is_null($troba)) {
                            $obj->cod_troba=$troba;
                        }
                        $solicitud->cms()->save($obj);
                    }
                }
                if ($objultimo->tipo_legado == 2) {
                    if ($actividadid == 1) {
                        if ($contrata!="") {
                            $obj = $solicitud->gestelaveria;
                            $obj->contrata = $contrata;
                            if (!is_null($quiebreid)) {
                                $obj->quiebre_id = $quiebreid;
                            }
                            $solicitud->gestelaveria()->save($obj);
                        }
                    }
                    if ($actividadid == 2) {
                        if ($contrata!="") {
                            $obj = $solicitud->gestelprovision;
                            $obj->contrata = $contrata;
                            if (!is_null($quiebreid)) {
                                $obj->quiebre_id = $quiebreid;
                            }
                            $solicitud->gestelprovision()->save($obj);
                        }
                    }
                }
                if (!is_null($quiebreid) && $quiebreid!="") {
                    $objultimo->quiebre_id = $quiebreid;
                }
                if (!is_null($actividadtipoid)) {
                    $objultimo->actividad_tipo_id = $actividadtipoid;
                }
            
                if ($objultimo->tipo_legado == 1) {
                    //actualiza y mando trama validando
                    $arrayvalidacion=array();
                    $arrayvalidacion['tipo']=2; //GOTOA
                    $arrayvalidacion['estado']=1; //1
                    $arrayvalidacion['proveniencia']=2; //CMS
                    if ($solicitud->cms->validarParam([], $arrayvalidacion)) {
                        $objultimo->estado_aseguramiento = 1;
                    } else {
                        $objultimo->estado_aseguramiento = 8;
                    }
                }
                $objultimo->estado_st = 1;
                
                $solicitud->ultimo()->save($objultimo);
                $response["msj"] = "Datos de GO Actualizados";
            } else {
                $response["rst"] = 2;
                $response["msj"] = "El requerimiento debe estar como Pendiente o No Enviado a TOA para actualizar";
            }
            
        } else {
            $response["rst"] = 2;
            $response["msj"] = "Error al consultar DATA de ST";
        }
        return \Response::json($response);
    }

    public function postVisor()
    {
        $rangoFechas = \Input::get('rangoFechas');
        $fechaInicio = new \DateTime($rangoFechas[0]);
        $fechaFin = new \DateTime($rangoFechas[1]);
        $dias = $fechaInicio->diff($fechaFin);

        $estados = \DB::table('estados_aseguramiento_ofsc as eao')
                    ->select(
                        'eao.estado_ofsc_id',
                        'eo.nombre',
                        \DB::raw('count(eao.estado_ofsc_id) as countrow'),
                        'eo.color',
                        'eo.color_letra'
                    )
                    ->join('estados_ofsc as eo', 'eo.id', '=', 'eao.estado_ofsc_id')
                    ->groupBy('eao.estado_ofsc_id')
                    ->get();

        $query = "select eao.id, 
                eao.estado_ofsc_id,
                eao.estado_aseguramiento_id,
                ea.nombre,";

        for ($i = 0; $i <= ($dias->days); $i++) {
            $query .= "sum(
                            if(date(stu.created_at) = 
                                date(date_sub('".$rangoFechas[1]."', INTERVAL ". $i ." DAY)), 
                                1, 0
                            )
                        ) as f$i,";
        }

        $query .= "count(stu.id) total,
                sum(if(stu.estado_st = 0, 1, 0)) estado_0,
                sum(if(stu.estado_st = 1, 1, 0)) estado_1,
                sum(if(stu.estado_st = 2, 1, 0)) estado_2
                from estados_aseguramiento_ofsc eao
                inner join estados_aseguramiento ea 
                on ea.id = eao.estado_aseguramiento_id
                left join solicitud_tecnica_ultimo stu
                on stu.estado_aseguramiento = eao.estado_aseguramiento_id 
                and stu.estado_ofsc_id = eao.estado_ofsc_id 
                and date(stu.created_at) 
                BETWEEN date('".$rangoFechas[0]."') AND date('".$rangoFechas[1]."')
                group by eao.estado_ofsc_id, eao.estado_aseguramiento_id";

   
        $datos = \DB::select($query);

        return [
            'estados' => $estados,
            'datos' => $datos,
            'dias' => $dias->days
        ];
    }

    public function postReenviarrespuesta()
    {
        $solicitud = \Input::get("solicitud");
        $trama = \DB::table('solicitud_tecnica_log_recepcion as st')
                    ->where('st.id_solicitud_tecnica', '=', $solicitud)
                    ->orderBy('st.id', 'desc')->first();
        if (!is_null($trama)) {
            $demo = [
                'id_solicitud_tecnica' => $trama->id_solicitud_tecnica,
                'id_respuesta' => 1,
                'observacion' => "OK",
                'solicitud_tecnica_id' => $trama->solicitud_tecnica_id
            ];
            //print_r($demo);
            $objApiST = new STCmsApi('GESTEL Legado');
            $objApiST->responderST($demo);
            return \Response::json(["rst" => 1]);
        }
        return \Response::json(["rst" => 2]);
    }

    public function postUpdatecoords()
    {
        $actuacion = [];
        /*$solicitudTecnica = SolicitudTecnica::where('id', \Input::get('solicitud_tecnica_id'))
                            ->orderBy("updated_at", "DESC")
                            ->first();
        $ultimo = $solicitudTecnica->ultimo;*/

        $objinbound = new Inbound;
        $appointment = (array)json_decode(\Input::get('appointment'));
        $responseToa = $objinbound->updateActivity(\Input::get('num_requermiento'), [], $appointment);

        if (isset($responseToa->result) && $responseToa->result == "success") {
            /*$actuacion['coordx_tecnico'] = $appointment['coordx'];
            $actuacion['coordy_tecnico'] = $appointment['coordy'];*/
            $solicitudTecnicaCms= SolicitudTecnicaCms::where('solicitud_tecnica_id', \Input::get('solicitud_tecnica_id'))->first();
            $solicitudTecnicaCms['coordx_cliente'] = $appointment['coordx'];
            $solicitudTecnicaCms['coordy_cliente'] = $appointment['coordy'];
            $solicitudTecnicaCms->save();

            $movimiento = Movimiento::where('solicitud_tecnica_id', \Input::get('solicitud_tecnica_id'))->latest()->first();
            if ($movimiento) {
                $detalle = new MovimientoDetalle();
                $detalle['coordx_tecnico']        = $appointment['coordx'];
                $detalle['coordy_tecnico']        = $appointment['coordy'];
                $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                $detalle['tipo']                  = 9; //guardar de cordenadas
                $detalle['sms']                   = 'Coordenadas Actualizadas Correctamente';
                $movimiento->detalle()->save($detalle);
            }
            $response = ["rst" => 1];
        } else {
            $error = isset($responseToa->errorMsg)? $responseToa->errorMsg:'Error al actualizar las coordenadas';
            $response["rst"] = 2;
            $response["msj"] = $error;
            /*$actuacion['coordx_tecnico'] = $appointment['coordx'];
            $actuacion['coordy_tecnico'] = $appointment['coordy'];
            $actuacion['xa_observation'] = $error;*/
        }
        
        /*foreach (Ultimo::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimo[$key] = $actuacion[$key];
            }
        }
        $solicitudTecnica->ultimo()->save($ultimo);

        $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
        foreach (Movimiento::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimoMovimiento[$key] = $actuacion[$key];
            }
        }
        $ultimoMovimiento->save();*/

        return \Response::json($response);
    }

    public function postMovimientos()
    {
        $movimientos = [];
        $objimpl = new STRepository;
        $movimientos = Movimiento::from("solicitud_tecnica_movimiento as stm")->with('detalle', 'detalle.tecnico')
                ->where(["stm.solicitud_tecnica_id"  => \Input::get("id")])
                ->select(
                    \DB::raw("CONCAT(u.nombre,' ',u.apellido) AS usuario"),
                    "stm.created_at",
                    "stm.id",
                    "stm.sistema_externo_id",
                    "stm.coordx_tecnico",
                    "stm.coordy_tecnico",
                    "stm.estado_ofsc_id",
                    "stm.tipo_envio_ofsc",
                    "stm.intervalo",
                    \DB::raw("IFNULL(ea.nombre, '') as estado_aseguramiento_nombre"),
                    "ea.id as estado_aseguramiento",
                    "eo.nombre as estado",
                    \DB::raw("IFNULL(stm.fecha_agenda, '') as fecha_agenda"),
                    \DB::raw("IFNULL(stm.resource_id, '') as resource_id"),
                    \DB::raw("IFNULL(mo.descripcion, '') as motivo"),
                    \DB::raw("IFNULL(so.descripcion, '') as submotivo"),
                    "stm.xa_note as observacion",
                    "stm.xa_observation as observaciontoa",
                    "stm.actividad_id as actividad",
                    \DB::raw("CONCAT(t.nombres,' ',t.ape_paterno,' ',t.ape_materno) AS tecnico"),
                    \DB::raw("(SELECT COUNT(*) as cant_detalle FROM solicitud_tecnica_movimiento_detalle 
                    WHERE solicitud_tecnica_movimiento_id=stm.id AND IFNULL(campo1,'')<>''AND (tipo IN (3,4,5,6))
                    ) as cantdetalle")
                )
                ->leftjoin("usuarios as u", "u.id", "=", "stm.usuario_created_at")
                ->leftjoin("estados_ofsc as eo", "eo.id", "=", "stm.estado_ofsc_id")
                ->leftjoin("motivos_ofsc as mo", "mo.id", "=", "stm.motivo_ofsc_id")
                ->leftjoin("submotivos_ofsc as so", "so.id", "=", "stm.submotivo_ofsc_id")
                ->leftJoin("estados_aseguramiento as ea", "ea.id", "=", "stm.estado_aseguramiento")
                ->leftJoin("tecnicos as t", "t.id", "=", "stm.tecnico_id")
                ->orderBy("stm.id", "DESC")
                ->get();
        return \Response::json(
            array(
                'rst' => 1,
                'movimientos' => $movimientos,
            )
        );
    }
    
    public function postLogrepcion()
    {
        $logcambios = [];
        $objimpl = new STRepository;
        $logcambios = $objimpl->getLogCambiosCache(\Input::get("id"));
        foreach ($logcambios as $key => $value) {
            $antes = json_decode($value->antes, true);
            $htmlantes = "";
            foreach ($antes as $key2 => $value2) {
                $htmlantes.="<b>$key2</b>  =>  ".$value2."<br>";
            }
            $value->htmlantes = $htmlantes;

            $despues = json_decode($value->despues, true);
            $htmldespues = "";
            foreach ($despues as $key2 => $value2) {
                if (!is_array($value2)) {
                    $htmldespues.="<b>$key2</b>  =>  ".$value2."<br>";
                }
            }
            $value->htmldespues = $htmldespues;

            $logcambios[$key] = $value;
        }
        return \Response::json(
            array(
                'rst' => 1,
                'logcambios' => $logrecepcion,
            )
        );
    }

    public function postComunicacionlego()
    {
        $comunicacionlego = [];
        $objimpl = new STRepository;
        $enviolegado = $objimpl->getLogComunicacionLegadoCache(\Input::get("id"));
        foreach ($enviolegado as $key => $value) {
            $xmlrequest = \Array2XML::createXML('psi', json_decode($value["request"], true));
            $value["xml_request"] = $xmlrequest->saveXML();

            $xmlresponse = \Array2XML::createXML('psi', json_decode($value["response"], true));
            $value["xml_response"] = $xmlresponse->saveXML();

            $comunicacionlego[$value["accion"]][] = $value;
        }
        return \Response::json(
            array(
                'rst' => 1,
                'comunicacionlego' => $comunicacionlego,
            )
        );

    }

    public function postLogcambios()
    {
        $logcambios = [];
        $objimpl = new STRepository;
        $logcambios = $objimpl->getLogCambiosCache(\Input::get("id"));
        foreach ($logcambios as $key => $value) {
            $antes = json_decode($value->antes, true);
            $htmlantes = "";
            foreach ($antes as $key2 => $value2) {
                $htmlantes.="<b>$key2</b>  =>  ".$value2."<br>";
            }
            $value->htmlantes = $htmlantes;

            $despues = json_decode($value->despues, true);
            $htmldespues = "";
            foreach ($despues as $key2 => $value2) {
                if (!is_array($value2)) {
                    $htmldespues.="<b>$key2</b>  =>  ".$value2."<br>";
                }
            }
            $value->htmldespues = $htmldespues;

            $logcambios[$key] = $value;
        }

        return \Response::json(
            array(
                'rst' => 1,
                'logcambios' => $logcambios,
            )
        );
    }
}
