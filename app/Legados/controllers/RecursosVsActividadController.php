<?php namespace Legados\controllers;

use Illuminate\Support\Collection;
use Input;
use Response;
use DB;
use Ofsc\Resources;
use Ofsc\Activity;
use models\Bucket;
use Legados\models\TecnicoRuta;
use Legados\models\TecnicoRutaCantidad;
use Legados\models\TecnicoBucketDisponible;

class RecursosVsActividadController extends \BaseController
{
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */

    public function postListarrecursos()
    {    
       $bucketFiltros = Input::get("bucket");
        $o_resource = new Resources();
        $buckets     = \Bucket::where('type', '=', 'BK')
          ->where('estado', '=', '1')
          ->get()->toArray();

          
        $recursos = [];
        $recursosFiltros = [];
        if(Input::has('bucket')) {           
            for ($i=0;$i<count($bucketFiltros);$i++) {
                $recursosFiltros[$i] = json_decode(json_encode($o_resource->getResources($bucketFiltros[$i])), true);             
                if (!isset($recursosFiltros[$i]['data']['result_code'])) {                    
                    $recursos[$i] = $recursosFiltros[$i]['data'];                    
                }
            }

            if (count($recursos) > 0) {              //

                if ( is_array($recursos) ) {
                    TecnicoBucketDisponible::truncate();            
                    foreach ($recursos as $value) {                        
                        foreach ($value as $value2) {                     
                            //$value2['bucket'] = $key;                        
                            $objactividadtecnico = new TecnicoBucketDisponible;
                            $objactividadtecnico->fill($value2);                     
                            $objactividadtecnico->save(); 
                        }
                    }
                }
            }
        } else {
            foreach ($buckets as $bucket) {              
                
                //$recursos = $o_resource->getResources($bucket['bucket_ofsc']);
                $recursos=json_decode(json_encode($o_resource->getResources($bucket['bucket_ofsc'])), true);
                if (!isset($recursosFiltros['data']['result_code'])) {                
                    $recursos = $recursos['data'];
                }
                //print_r($recursos);        
            }

            if (count($recursos) > 0) {
              //

                if ( is_array($recursos) ) {
                    TecnicoBucketDisponible::truncate();            
                    foreach ($recursos as $value) {
                        //print_r($value);
                        foreach ($value as $value2) {                     
                          //$value2['bucket'] = $key;                        
                              $objactividadtecnico = new TecnicoBucketDisponible;
                              $objactividadtecnico->fill($value);                     
                              $objactividadtecnico->save(); 
                        }
                    }
                }
            }
            
        }
        

        //return Response::json(["rst" => 1, "data" => $dataRecurso]);
    }

    public function postListartecnico()
    {
        
        if (Input::has("accion")) {            
            
        } else {
            $bucketFiltros = Input::get("buckett");    
            $activity = new Activity();
            $from     = date("Y-m-d");
            $to       = date("Y-m-d", strtotime("+ 7 days"));
            $buckets  = \Bucket::where('type', '=', 'BK')
              ->where('estado', '=', '1')
              ->get()->toArray();
            $dataActividades2 = [];

            if (Input::has('buckett')) {               
                for ($i=0;$i<count($bucketFiltros);$i++){
                    $actividades = $activity->getActivities($bucketFiltros[$i], $from, $to);                
                    if (isset($actividades->data) && count($actividades->data) > 0) {
                        $dataActividades2[$bucketFiltros[$i]] = $actividades->data;
                    }
                }
            } else {               
                foreach ($buckets as $bucket) {
                    $actividades = $activity->getActivities($bucket['bucket_ofsc'], $from, $to);                
                    if (isset($actividades->data) && count($actividades->data) > 0) {
                        $dataActividades2[$bucket['bucket_ofsc']] = $actividades->data;
                    }
                }
            }

            if (count($dataActividades2) > 0) {                
                TecnicoRuta::truncate();                
                foreach ($dataActividades2 as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                      $value2['bucket'] = $key;                      
                      $objactividadtecnico = new TecnicoRuta;
                      $objactividadtecnico->fill($value2);                     
                      $objactividadtecnico->save(); 
                    }
                }
            }
        }
        $query = " SELECT
            rtr.bucket, 
            te.nombre_tecnico,
            rtr.resource_id,
            ar.fecha_activacion,
            IF (DATE(rtr.date) ='3000-01-01','NO PROGRAMADO','PROGRAMDO') AS programado,
            (CASE 
            WHEN rtr.status='pending' THEN 'pendiente' 
            WHEN  rtr.status='started' THEN 'iniciada'  
            WHEN  rtr.status='notdone' THEN 'norealizada' 
            WHEN  rtr.status='complete' THEN 'completada' 
            WHEN  rtr.status='suspended' THEN 'suspendido' 
            WHEN  rtr.status='cancelled' THEN 'cancelado' END) AS estado,
            rtr.name AS cliente,
            rtr.address AS direccion
            FROM rpt_tecnico_ruta rtr 
            INNER JOIN tecnicos  te ON rtr.resource_id=te.carnet
            LEFT JOIN activacion_route ar ON rtr.resource_id=ar.carnet_id ORDER BY  rtr.resource_id ";          
        $data = DB::select($query);
            if (Input::get("accion")) {
                return \Helpers::exportArrayToExcel($data, "Rutas_no_activadas", []);
            } else {               
                return Response::json(["rst" => 1, "data" => $data]);
            }
    }

    public function postListarcantidad() {

        if (Input::has("accion")) {
            
        } else {
            $bucketFiltros = Input::get("bucketc");
            $activity = new Activity();
            $from     = date("Y-m-d");
            $to       = date("Y-m-d", strtotime("+ 7 days"));
            $buckets  = \Bucket::where('type', '=', 'BK')
              ->where('estado', '=', '1')
              ->get()->toArray();
            $dataActividades = [];            if(Input::has('bucketc')) {
                for ($i=0;$i<count($bucketFiltros);$i++){
                    $actividades = $activity->getActivities($bucketFiltros[$i], $from, $to);
                    if (isset($actividades->data) && count($actividades->data) > 0) {
                        $dataActividades[$bucketFiltros[$i]] = $actividades->data;
                    }
                }
            } else {
                foreach ($buckets as $bucket) {
                    $actividades = $activity->getActivities($bucket['bucket_ofsc'], $from, $to);
                    if (isset($actividades->data) && count($actividades->data) > 0) {
                        $dataActividades[$bucket['bucket_ofsc']] = $actividades->data;
                    }
                }
            }

            if (count($dataActividades) > 0) {
                TecnicoRutaCantidad::truncate();
                foreach ($dataActividades as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                      $value2['bucket'] = $key;
                      $objactividadtecnico = new TecnicoRutaCantidad;
                      $objactividadtecnico->fill($value2);
                      $objactividadtecnico->save(); 
                    }
                }
            }
        }
        $query = "  SELECT
                    rtcr.bucket,
                    resource_id,
                    te.nombre_tecnico,
                    COUNT(*) AS cantidad,
                    COUNT(CASE WHEN DATE <> '3000-01-01' THEN ' programada'  END) AS programada,
                    COUNT(CASE WHEN DATE = '3000-01-01' THEN ' noprogramada'  END) AS noprogramada
                    FROM rpt_tecnico_ruta_cantidad  rtcr
                    INNER JOIN tecnicos  te ON rtcr.resource_id=te.carnet
                    GROUP BY resource_id ORDER BY rtcr.bucket";
        $data = DB::select($query);

        if (Input::get("accion")) {            
            return \Helpers::exportArrayToExcel($data, "Cantidad_Actividad", []);
        } else {
            return Response::json(["rst" => 1, "data" => $data]);
        }
    }    
}