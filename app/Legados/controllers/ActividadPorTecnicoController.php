<?php
namespace Legados\controllers;
use Input;
use Response;
use DB;
use Legados\models\ActividadTecnico;
use Ofsc\Resources;
//use models\Bucket;
use Ofsc\Activity;

class ActividadPorTecnicoController extends \BaseController {
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postCargar()
    {
      $hoy = date("Y-m-d");
      $fecha_inicio = Input::get("fecha_inicio");

      if ( $fecha_inicio == $hoy) {
        //dd("hoy");
          $dataActividades = [];
          $bucketTecnicos = [];
          if (Input::get("tipo") == 'excel') {
          } else { 
            $activity   = new Activity();
            $from       = date("Y-m-d");
            $to         = date("Y-m-d", strtotime("+ 7 days"));      
            $bucket     = \Bucket::where('type', '=', 'BK')->where('estado', '=', '1')->get()->toArray();   

              foreach ($bucket as $key => $value) {            
                  //get activities by bucket      
                  $actividades = $activity->getActivities($value['bucket_ofsc'], $from, $to);
                  if (isset($actividades->data) && count($actividades->data) > 0) {     
                    $dataActividades[$value['bucket_ofsc']] = $actividades->data;                                        
                  }else{ //get tecnicos by bucket to filter with carnet_id of activation routes
                    $resource = new Resources();                 
                    $bucketTecnicos['buckets_toa'] = $resource->getResources($value['bucket_ofsc'])->data;                                                
                  }
              }                                    
                            
              if (count($dataActividades) > 0) {
                ActividadTecnico::truncate();
                foreach ($dataActividades as $key => $value) {
                  foreach ($value as $key2 => $value2) {
                      $value2['bucket'] = $key;                      
                      $objactividadtecnico = new ActividadTecnico;
                      $objactividadtecnico->fill($value2);                     
                      $objactividadtecnico->save();      
                  }
                }
              } else {
                $activaciones = \ActivacionRoute::all()->toArray();
                if($activaciones){
                  foreach ($activaciones as $key => $value) {
                    foreach ($bucketTecnicos['buckets_toa'] as $index => $val) {
                        if($value['carnet_id']==$val['id']){                        
                          $objactividadtecnico = new ActividadTecnico;
                          $objactividadtecnico->bucket = $val['parent_id'];
                          $objactividadtecnico->name = $val["name"];
                          $objactividadtecnico->status = $val["status"];
                          $objactividadtecnico->resource_id = $val["id"];
                          $objactividadtecnico->save();
                          break;      
                        }
                    }
                  }                                   
                }
            }

          }
         
          $buckets = Input::get("bucketss");     
          $bucket = [];
          for ( $i=0; $i < count($buckets); $i++ ) { 
              array_push($bucket, "'".strtoupper($buckets[$i])."'");
          }
          $where = "";
          if (isset($buckets)) {$where.=' and UPPER(t1.bucket) in ('.implode(',',$bucket).')';}
          $query = "SELECT t1.bucket, 
          IFNULL(t1.resource_id,'') as resource_id, 
          IFNULL(t1.nombre_tecnico,'') as nombre_tecnico, 
          IFNULL(t1.fecha_inicio_primeraActividad,'') as fecha_inicio_primeraActividad, 
          IFNULL(t1.fecha_fin_primeraActividad,'') as fecha_fin_primeraActividad,
          IFNULL(fecha_fin_ultimaActividad,'') as fecha_fin_ultimaActividad,
          REPLACE(ctoh.coordx, '.', ',') AS coordx,
          REPLACE(ctoh.coordy, '.', ',') AS coordy, 
          IFNULL(t1.cantidad,'') as cantidad,  IFNULL(t1.pendiente,'') as pendiente,  IFNULL(t1.iniciada,'') as iniciada, IFNULL(t1.norealizada,'') as norealizada, IFNULL(t1.completada,'') as completada,
          IFNULL(t1.suspendida,'') as suspendida, IFNULL(t1.cancelada,'') as cancelada, IFNULL(ar.carnet_id,'') as carnet_id,IFNULL(ar.fecha_activacion,'') as fecha_activacion 
          FROM 
            (SELECT
              act.bucket,
              act.resource_id,
              t.nombre_tecnico,
              MIN(IF(STATUS='complete', start_time,NOW()) ) AS fecha_inicio_primeraActividad ,
              MIN(IF(STATUS='complete', end_time,'no ha completado actividad') )AS fecha_fin_primeraActividad , 
              MAX(CASE WHEN STATUS='complete' THEN end_time  END ) AS fecha_fin_ultimaActividad,
              REPLACE(act.coordx, '.', ',') AS coordx,
              REPLACE(act.coordy, '.', ',') AS coordy,       
              COUNT(*) AS cantidad,
              COUNT(CASE WHEN STATUS='pending' THEN 'pendiente' END) AS pendiente,
              COUNT(CASE WHEN STATUS='started' THEN 'iniciada' END) AS iniciada,
              COUNT(CASE WHEN STATUS='notdone' THEN 'norealizada' END) AS norealizada,
              COUNT(CASE WHEN STATUS='complete' THEN 'completada' END) AS completada ,
              COUNT(CASE WHEN STATUS='suspended' THEN 'completada' END) AS suspendida ,
              COUNT(CASE WHEN STATUS='cancelled' THEN 'completada' END) AS cancelada
              FROM actividad_tecnico act
              INNER JOIN `tecnicos` t ON act.resource_id=t.carnet WHERE STATUS <> 'cancelled' AND DATE <>'3000-01-01' GROUP BY act.resource_id
            ) t1 
          LEFT JOIN activacion_route ar ON (CONVERT(ar.carnet_id USING utf8) COLLATE utf8_spanish_ci=t1.`resource_id`)  
          LEFT JOIN coordenada_tecnico_ofsc_historico ctoh ON (CONVERT(ctoh.carnet USING utf8) COLLATE utf8_spanish_ci=t1.`resource_id`) WHERE DATE(ctoh.created_at)=DATE(NOW()) 
          $where GROUP BY t1.resource_id 
          ORDER BY t1.bucket";
      } else {
        $query = " SELECT
          IFNULL(act.bucket,'') as bucket,
          IFNULL(act.resource_id,'') as resource_id,
          IFNULL(t.nombre_tecnico,'') as nombre_tecnico,
          MIN(IF(STATUS='complete', start_time,NOW()) ) AS fecha_inicio_primeraActividad ,
          MIN(IF(STATUS='complete', end_time,'no ha completado actividad') )AS fecha_fin_primeraActividad , 
          MAX(CASE WHEN STATUS='complete' THEN end_time  END ) AS fecha_fin_ultimaActividad,
          REPLACE(act.coordx, '.', ',') AS coordx,
          REPLACE(act.coordy, '.', ',') AS coordy,       
          COUNT(*) AS cantidad,
          COUNT(CASE WHEN STATUS='pending' THEN 'pendiente' END) AS pendiente,
          COUNT(CASE WHEN STATUS='started' THEN 'iniciada' END) AS iniciada,
          COUNT(CASE WHEN STATUS='notdone' THEN 'norealizada' END) AS norealizada,
          COUNT(CASE WHEN STATUS='complete' THEN 'completada' END) AS completada ,
          COUNT(CASE WHEN STATUS='suspended' THEN 'completada' END) AS suspendida ,
          COUNT(CASE WHEN STATUS='cancelled' THEN 'completada' END) AS cancelada
          FROM actividad_tecnico_historico act 
          INNER JOIN `tecnicos` t ON act.resource_id=t.carnet
          WHERE STATUS <> 'cancelled' AND DATE <>'3000-01-01'  AND DATE(act.created_at)='$fecha_inicio'
          GROUP BY act.resource_id ORDER BY act.bucket  ";
      }
        $data = DB::select($query);                 
        $resul=json_decode(json_encode($data),true);
        if ( Input::get("tipo") == 'excel' ) {
            return \Helpers::exportArrayToExcel($resul, 'ActividadPorTecnico');
        } else {
            return Response::json(array("rst" => 1,'data' => $data));
        }        
    }
    public function postListar() {

        $tipo=Input::get('tipo');
        if ( $tipo == 'slct_bucketss' ) {
            $data = DB::select(" SELECT  bucket_ofsc as id,UPPER(bucket_ofsc) as nombre FROM bucket WHERE estado=1;");
        }

        if ( $tipo == 'actividades' ) {
            $data = DB::select(" SELECT id,UPPER(nombre) as nombre FROM actividades WHERE estado=1;");
        }
                 
        return Response::json(["rst" => 1, "datos" => $data]);
    }


}

