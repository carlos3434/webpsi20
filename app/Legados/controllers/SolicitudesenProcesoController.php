<?php namespace Legados\controllers;

use Illuminate\Support\Collection;
use Input;
use Response;
use DB;
use Ofsc\Activity;
use Helpers;

class SolicitudesenProcesoController extends \BaseController
{
    //Funcion para cargar los datos en el datatable
    public function postCargar()
    {
        $fcha_inicio=Input::get("fecha_inicio");
        $fcha_fin=Input::get("fecha_fin");
        $fechas="'".$fcha_inicio."' AND '".$fcha_fin."'";
        $buckets=Input::get("buckets");
        $tab=Input::get("tab");

        $order = ["column" => Input::get('column'),"dir" => Input::get('dir')];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        /*consulta*/
        $select=['bucket','appt_number','xa_identificator_st','name','status','date','a_control',
        'estado_actual_cms','fecha_liquidacion_cms','rango_fechas',
        DB::raw("created_at AS fecha_creacion")];

        /*Filtros*/
        $where='';
        $bucket=[];
        $buckets=explode(',', $buckets);
        for ($i=0; $i < count($buckets); $i++) { 
            array_push($bucket, "'".strtoupper($buckets[$i])."'");
        }

        if ($buckets[0]!='null') {
            $where=" and bucket in (".implode(',',$bucket).")";
        }

        $tabla='';
        if($tab == 2) {
            $tabla="reporte_solicitudesenproceso_toa30dias";
        }else {
            $tabla="reporte_solicitudesenproceso_toa7dias";
        }

        $data2= DB::table($tabla)
                ->select($select)
                ->whereRaw("(date between ".$fechas." or date='3000-01-01')".$where)
                ->orderBy('date', 'desc');

        $column = "id";
        $dir = "desc";

        $data2 = $data2
                ->orderBy($column, $dir)
                ->paginate($grilla["perPage"]);

        $data = $data2->toArray();

        $col = new Collection([
            'recordsTotal'=> $data2->getTotal(),
            'recordsFiltered'=> $data2->getTotal(),
        ]);

        return $col->merge($data);
    }

    public function postExcel() {

        $fcha_inicio=Input::get("fecha_inicio");
        $fcha_fin=Input::get("fecha_fin");
        $fechas="'".$fcha_inicio."' AND '".$fcha_fin."'";
        $buckets=Input::get("buckets");
        $tab=Input::get("tab");
        //consulta
        /*Filtros*/
        $where='';
        $bucket=[];
        $buckets=explode(',', $buckets);
        for ($i=0; $i < count($buckets); $i++) { 
            array_push($bucket, "'".strtoupper($buckets[$i])."'");
        }

        if ($buckets[0]!='null') {
            $where=" and bucket in (".implode(',',$bucket).")";
        }

        $select=["id","name","status","resource_id","appt_number","coordx","coordy",
        "address","date","start_time","end_time","time_slot","time_of_booking",
        "sla_window_start","sla_window_end","service_window_start","service_window_end",
        "xa_appintment_scheduler","a_asignacion_manual_flag","xa_work_zone_key","a_control",
        "bucket",DB::raw("xa_identificator_st AS solicitud_tecnica"),"estado_actual_cms",
        "fecha_liquidacion_cms"];

        $tabla='';$filename='';
        if($tab ==2 ) {
            $tabla="reporte_solicitudesenproceso_toa30dias";
            $filename='TOA(30dias)_'.date("Y").'_'.date("m").'_'.date("d").'_'.date("G").
            '_'.date("i").'_'.date("s").'.xls';
        }else {
            $tabla="reporte_solicitudesenproceso_toa7dias";
            $filename='TOA(7dias)_'.date("Y").'_'.date("m").'_'.date("d").'_'.date("G").
            '_'.date("i").'_'.date("s").'.xls';
        }

        $reporte= DB::table($tabla)
                ->select($select)
                ->whereRaw("(date between ".$fechas." or date='3000-01-01')")
                ->orderBy('date', 'desc')
                ->get();

        $reporte=json_decode(json_encode($reporte),true);
        if (Input::get("tipo") == 'Excel') {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename);
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');

            if (count($reporte) > 0) {
                $n = 1;
                foreach ($reporte as $data) {
                //Encabezado
                    if ($n == 1) {
                        foreach ($data as $key => $val) {
                            echo $key . "\t";
                        }
                        echo $val . "\r\n";
                    }
                    //Datos
                    foreach ($data as $val) {
                        $val = str_replace(
                        array("\r\n", "\n", "\n\n", "\t", "\r"),
                        array("", "", "", "", ""),
                        $val
                        );
                        echo $val . "\t";
                    }
                    echo "\r\n";
                    $n++;
                }
            }else{$val="Mensaje:No existen registros!";echo $val . "\r\n";}
        } else {return Response::json(array('rst' => 1, 'datos' => $reporte));}
    }

    public function postListar(){
        $tipo=Input::get('tipo');
        $data = [];

        if($tipo=='buckets'){
            $data=DB::table('bucket')
            ->select("bucket_ofsc AS id","nombre")
            ->whereRaw("estado=1 AND TYPE='BK'")
            ->get();
        }

        if($tipo=='tipo'){
            $data[0]=array('id' => 1,'nombre'=>'7 Dias');
            $data[1]=array('id' => 2,'nombre'=>'30 Dias');
        }

     return Response::json(["rst" => 1, "datos" => $data]);
    }

}