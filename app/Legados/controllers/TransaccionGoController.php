<?php

namespace Legados\controllers;

use DB;
use Response;
use Input;
use File;

class TransaccionGoController extends \BaseController {

    public function postDescargarexcel() {
        date_default_timezone_set('America/Lima');

        $txtFecha = Input::get("txt_rangofecha", null);

        $diasTemporales = [];

        if ($txtFecha != null && $txtFecha != "") {

            $txtFechaArray = explode(" - ", $txtFecha);
            $inicio = $txtFechaArray[0];
            $fin = $txtFechaArray[1];

            $datetime1 = date_create($inicio.' 00:00:00');
            $datetime2 = date_create($fin.' 23:59:59');


            //Obteniendo el rango de fechas elegidas
            $diaTemporal = $inicio;
            $diaFin =   date (
                            'Y-m-d' ,
                            strtotime (
                                '+1 day',
                                strtotime (
                                    $fin
                                )
                            )
                        );

            while ( $diaTemporal != $diaFin) {
                $diasTemporales[] = $diaTemporal;

                $diaTemporal = date (
                    'Y-m-d' ,
                    strtotime (
                        '+1 day',
                        strtotime (
                            $diaTemporal
                        )
                    )
                );

            }

            //Evaluando el casuisticas del query
            $today = date_create();

            $r = 0;

            if ($today >= $datetime1 && $today <= $datetime2) {
                $r = 1;
            }

            if($r) {
                $transaccionGoDetalle = DB::table("psi_backup.transaccion_go_detalle")
                ->whereBetween("created_at", [$inicio.' 00:00:00' , $fin.' 23:59:59'])
                ->union(
                    DB::table("transaccion_go_detalle")
                );
            } else {
                $transaccionGoDetalle = DB::table("psi_backup.transaccion_go_detalle")
                ->whereBetween("created_at", [$inicio.' 00:00:00' , $fin.' 23:59:59']);
            }

        } else {
            $diasTemporales[] = date("Y-m-d");

            $transaccionGoDetalle = DB::table("transaccion_go_detalle");

        }

        $tipo = Input::get("tipo", "tgo");

        if ($tipo == "tgo") {
            return $this->datosDescargaTGO($transaccionGoDetalle);
        } else if ($tipo == "e") {
            return $this->datosEstadisticaTGO($transaccionGoDetalle, $diasTemporales);
        }

    }


    public function datosDescargaTGO($transaccionGoDetalle) {

        $transaccionGoDetalle = $transaccionGoDetalle->get();

        if( $transaccionGoDetalle !== null ) {

            $contenido = "id,transaccion_go_id,id_solicitud_tecnica,created_at,flujo,mensaje_id,send_to,usuario,tipo_usuario,subject\n";

            foreach ( $transaccionGoDetalle as $tgd ) {

                $tipo_usuario = "";

                if($tgd->subject == "NOT_LEG"){
                    if ($tgd->usuario_toa == "Routing") {
                        $tipo_usuario = "Automatico";
                    } else {
                        $tipo_usuario = "Manual";
                    }
                }
                
                $contenido .= 
                    $tgd->id.",".
                    $tgd->transaccion_go_id.",".
                    $tgd->id_solicitud_tecnica.",".
                    $tgd->created_at.",".
                    $tgd->flujo.",".
                    $tgd->mensaje_id.",".
                    $tgd->send_to.",".
                    $tgd->usuario_toa.",".
                    $tipo_usuario.",".
                    $tgd->subject."\n";

            }

        }
        
        date_default_timezone_set('America/Lima');

        $rutaCsv = "/transaccion_go/transaccion_go_descarga.json";
        file_put_contents( public_path() . $rutaCsv, $contenido );

        $headers = array('Content-Type: application/csv',);

        return Response::download( public_path() . $rutaCsv, 'transaccion_go_' . date("Y-m-d G:i:s") . '.csv', $headers);
    }

    function dateDiffObjectToSeconds($dateObject) {

        $segundos = (INT)$dateObject->format("%s");

        $minutos = (INT)$dateObject->format("%i");

        $horas = (INT)$dateObject->format("%h");

        $dias = (INT)$dateObject->format("%d");

        return $segundos + ($minutos * 60) + ($horas * 60 * 60) + ($dias * 24 * 60 * 60);
        
    }

    public function datosEstadisticaTGO($transaccionGoDetalle, $diasTemporales) {
        date_default_timezone_set('America/Lima');
        $transaccionGoDetalle = $transaccionGoDetalle->whereIn("flujo", ["4.1","4.2","4.3"])
        ->get();

        $mensajes = [];

        foreach ($transaccionGoDetalle as $registro) {

            $llave = $registro->mensaje_id;

            if ( !isset( $mensajes[$llave] ) ) {

                $mensajes[$llave] = [
                    "flujo" => [
                        $registro->flujo => $registro->created_at
                    ],
                    "subject" => $registro->subject,
                    "send_to" => $registro->send_to,
                    "send_to_2" =>  date ( 'Y-m-d H:i:s' , strtotime ( '-5 hour -5 minutes' , strtotime ( $registro->send_to ) ) ),
                    "usuario_toa" => $registro->usuario_toa
                ];

            } else {

                $mensajes[$llave]["flujo"][$registro->flujo] = $registro->created_at;

            }

            
        }

        $mensajesProcesados = [];

        foreach ($mensajes as $key => &$mensaje) {
            
            if (isset($mensaje["flujo"]["4.1"]) && isset($mensaje["flujo"]["4.3"])) {


                //Tiempo de proceso 4.3 - 4.1
                $time1 = date_create($mensaje["flujo"]["4.1"]);
                $time3 = date_create($mensaje["flujo"]["4.3"]);
                $tiempoProceso1 = date_diff($time3, $time1);
                $mensaje["tiempo_proceso_1"] = $this->dateDiffObjectToSeconds($tiempoProceso1);


                //Tiempo de proceso con SEND TO
                $send_to_2 = date_create($mensaje["send_to_2"]);

                $tiempoProceso2 = date_diff($time3, $send_to_2);
                $mensaje["tiempo_proceso_2"] = $this->dateDiffObjectToSeconds($tiempoProceso2);

                $mensaje["hora"] = (INT)$time1->format("H");
                $mensaje["fecha"] = $time1->format("Y-m-d");
                $mensaje["mensaje_id"] = $key;

                $mensajesProcesados[$key] = $mensaje;

            }
        }

        //echo "<pre>";
        //print_r($mensajes);
        //exit;

        $horas = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];

        $mensajesFormateados = [];

        foreach ($diasTemporales as $key => $diaTemporal) {

            foreach ($horas as $hora) {

                $mensajesFormateados[$diaTemporal." - ".$hora] = [];

                foreach ($mensajesProcesados as $mensajeProcesado) {
                    //echo "dia temporal[".$diaTemporal."] fecha[".$mensajeProcesado["fecha"]."]";

                    if ($mensajeProcesado["hora"] == $hora && $diaTemporal == $mensajeProcesado["fecha"]) {
                        $mensajesFormateados[$diaTemporal." - ".$hora][] = $this->separaNotLeg($mensajeProcesado);
                    }

                }
            }
        }

        /*echo "<pre>";
        print_r($mensajesFormateados);
        exit;*/

        $excel_datos = "Hora,Subject,Tiempo promedio(4.3-4.1),A(<10s),B(10s - 29s),C(30s - 59s),D(1m - 2:59m),E(>3m),,Tiempo promedio(send_to),A(<10s),B(10s - 29s),C(30s - 59s),D(1m - 2:59m),E(>3m),, TOTAL\n";

        //Recorriendo mensajes ordenados por hora
        foreach ($mensajesFormateados as $fecha => &$mensajeFormateado) {

            $datosEstadisticos = [];

            //Recorriendo mensajes de una hora determinada
            foreach ($mensajeFormateado as $mensajesXHora) {

                $calificacionContador_1 = $this->evaluaTiempoProceso($mensajesXHora["tiempo_proceso_1"]);

                $calificacionContador_2 = $this->evaluaTiempoProceso($mensajesXHora["tiempo_proceso_2"]);

                if (!isset($datosEstadisticos[$mensajesXHora["subject"]])) {

                    $datosEstadisticos[$mensajesXHora["subject"]] = [
                        "tiempo_proceso_1" => [$mensajesXHora["tiempo_proceso_1"]],
                        "A1" => 0,
                        "B1" => 0,
                        "C1" => 0,
                        "D1" => 0,
                        "E1" => 0,
                        "tiempo_proceso_2" => [$mensajesXHora["tiempo_proceso_2"]],
                        "A2" => 0,
                        "B2" => 0,
                        "C2" => 0,
                        "D2" => 0,
                        "E2" => 0,
                        "total" => 1
                    ];
                
                } else {
                    $datosEstadisticos[$mensajesXHora["subject"]]["tiempo_proceso_1"][] = $mensajesXHora["tiempo_proceso_1"];
                    $datosEstadisticos[$mensajesXHora["subject"]]["tiempo_proceso_2"][] = $mensajesXHora["tiempo_proceso_2"];
                    $datosEstadisticos[$mensajesXHora["subject"]]["total"]++ ;
                }

                $datosEstadisticos[$mensajesXHora["subject"]][$calificacionContador_1."1"]++;
                $datosEstadisticos[$mensajesXHora["subject"]][$calificacionContador_2."2"]++;

            }

            $mensajesFormateados[$fecha] = $datosEstadisticos;

            foreach ($datosEstadisticos as $subject => $datos) {
                $excel_datos .=
                $fecha.",".
                $subject.",".
                number_format(array_sum($datos["tiempo_proceso_1"])/ sizeof($datos["tiempo_proceso_1"]), 2, '.', '').",".
                $datos["A1"].",".
                $datos["B1"].",".
                $datos["C1"].",".
                $datos["D1"].",".
                $datos["E1"].",".
                ",".
                number_format(array_sum($datos["tiempo_proceso_2"])/ sizeof($datos["tiempo_proceso_2"]), 2, '.', '').",".
                $datos["A2"].",".
                $datos["B2"].",".
                $datos["C2"].",".
                $datos["D2"].",".
                $datos["E2"].",".
                ",".
                $datos["total"]."\n";
            }

        }

        $rutaCsv = "/transaccion_go/transaccion_go_estadistica.json";
        file_put_contents( public_path() . $rutaCsv, $excel_datos );

        $headers = array('Content-Type: application/csv',);

        return Response::download( public_path() . $rutaCsv, 'transaccion_estadistica' . date("Y-m-d G:i:s") . '.csv', $headers);
    }

    public function separaNotLeg($mensajeProcesado){
        if($mensajeProcesado["subject"] == "NOT_LEG") {
            if($mensajeProcesado["usuario_toa"] == "Routing" ) {
                $mensajeProcesado["subject"] = "NOT_LEG_ROUTING";
            }
        }

        return $mensajeProcesado;
    }

    public function evaluaTiempoProceso($tiempo) {
        if ($tiempo < 10) {
            return "A";
        } else if ($tiempo >= 10 &&  $tiempo < 30 ) {
            return "B";
        } else if ( $tiempo >= 30 && $tiempo < 60  ) {
            return "C";
        } else if ( $tiempo >= 60 && $tiempo < 180 ) {
            return "D";
        } else {
            return "E";
        }
    }

    public function descargarExcelHistorico() {
        date_default_timezone_set('America/Lima');

        $transaccionGoDetalle = DB::table("psi_backup.transaccion_go_detalle")->get();

        if( $transaccionGoDetalle ) {

            $contenido = "id,transaccion_go_id,id_solicitud_tecnica,created_at,flujo,mensaje_id,send_to,subject\n";

            foreach ( $transaccionGoDetalle as $tgd ) {
                
                $contenido .= 
                    $tgd->id.",".
                    $tgd->transaccion_go_id.",".
                    $tgd->id_solicitud_tecnica.",".
                    $tgd->created_at.",".
                    $tgd->flujo.",".
                    $tgd->mensaje_id.",".
                    $tgd->send_to.",".
                    $tgd->subject."\n";

            }

        }


        $rutaCsv = "/transaccion_go/transaccion_go_descarga.json";
        file_put_contents( public_path() . $rutaCsv, $contenido );

        $headers = array('Content-Type: application/csv',);

        return Response::download( public_path() . $rutaCsv, 'transaccion_historico' . date("Y-m-d G:i:s") . '.csv', $headers);
    } 

}