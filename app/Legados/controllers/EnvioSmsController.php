<?php namespace Legados\controllers;

class EnvioSmsController extends \BaseController
{

    public function enviar($job, $data)
    {
        echo "dentro de sms  \n";
        print_r($data);
        if (is_array($data["mensaje"]) && isset($data["mensaje"][0])) {
            $data["mensaje"] = $data["mensaje"][0];
        }
        if (\App::environment('local')) {
            exec("perl ".base_path()."/enviar_sms.pl ".$data['celular']." '".$data['mensaje']."' ".$data['id_user']." ".$data['estado_envio']);
        } else {
            exec("perl ".base_path()."/../enviar_sms.pl ".$data['celular']." '".$data['mensaje']."' ".$data['id_user']." ".$data['estado_envio']);
        }
        $job->delete();
    }
}
