<?php

namespace Legados\controllers;

use Legados\models\SolicitudTecnicaCms;

class TramaController extends BandejaLegadoController
{
    public function postActualizarordencms()
    {
        $estado=false;$msj="";
        $form=\Input::get('form',null);
        $id=\Input::get('id',null);
        $nombres=\Input::get('nombres',null);
        $paterno=\Input::get('paterno',null);
        $materno=\Input::get('materno',null);
        $zonal=\Input::get('zonal',null);
        $contrata=\Input::get('contrata',null);
        $tiporeq=\Input::get('tiporeq',null);
        $motigogen=\Input::get('motigogen',null);
        $fchinstalacion=\Input::get('fchinstalacion',null);
        $hrainstalacion=\Input::get('hrainstalacion',null);
        $telefono1=\Input::get('telefono1',null);
        $telefono2=\Input::get('telefono2',null);
        $data=[];
        $cant=0;
        try {
                if($form=='requerimiento'){

                    $solicitud_cms=SolicitudTecnicaCms::find($id);
                   
                    //nombres
                    $nombre='';
                    if($nombres!=''){
                        $nombre=$nombres;
                        $solicitud_cms->nom_cliente=$nombres;
                    }else{
                        $nombre=$solicitud_cms->nom_cliente;
                    }

                    //ape paterno
                    $ape_paterno='';
                    if($paterno!=''){
                        $ape_paterno=$paterno;
                        $solicitud_cms->ape_paterno=$paterno;
                    }else{
                        $ape_paterno=$solicitud_cms->ape_materno;
                    }

                    //ape materno
                    $ape_materno='';
                    if($materno!=''){
                        $ape_materno=$materno;
                        $solicitud_cms->ape_materno=$materno;
                    }else{
                        $ape_materno=$solicitud_cms->ape_materno;
                    }
                    
                    //zonal
                    $codzonal='';
                    if($zonal!=''){
                        $codzonal=$zonal;
                        $solicitud_cms->zonal=$zonal;
                    }else{
                        $codzonal=$solicitud_cms->zonal;
                    }

                    //contrata
                    $codcontrata='';$desc_contrata='';
                    if($contrata!=''){
                        $codcontrata=$contrata;
                        $cont=\DB::table('contratas_cms')
                            ->where('codctr','=',$contrata)
                            ->get();
                        $desc_contrata=$cont[0]->desnomctr;

                        $solicitud_cms->codigo_contrata=$contrata;
                        $solicitud_cms->desc_contrata=$desc_contrata;
                    }else{
                        $codcontrata=$solicitud_cms->codigo_contrata;
                        $desc_contrata=$solicitud_cms->desc_contrata;
                    }

                    //requerimiento
                    $codrequerimiento='';$desc_requerimiento='';
                    if($tiporeq!=''){
                        $codrequerimiento=$tiporeq;
                        $treq=\DB::table('gtm_averias')
                            ->where('tipo_req','=',$tiporeq)
                            ->get();
                    
                        if(count($treq)>0){
                            $desc_requerimiento=$treq[0]->desc_req;
                        }else{
                            $treq=\DB::table('webpsi_coc.prov_catv_tiposact')
                                ->where('tipo_req','=',$tiporeq)
                                ->get();
                            $desc_requerimiento=$treq[0]->des_req;
                        }
                        
                        $solicitud_cms->tipo_requerimiento=$codrequerimiento;
                        $solicitud_cms->desc_tipo_requerimiento=$desc_requerimiento;
                    }else{
                        $codrequerimiento=$solicitud_cms->tipo_requerimiento;
                        $desc_requerimiento=$solicitud_cms->desc_tipo_requerimiento;
                    }

                    //motivo
                    $codmotivo='';$desc_motivo='';
                    if($motigogen!=''){
                        $codmotivo=$motigogen;
                        $motgen=\DB::table('gtm_averias')
                            ->where('motivo','=',$motigogen)
                            ->get();
                    
                        if(count($motgen)>0){
                            $desc_motivo=$motgen[0]->desc_motivo;
                        }else{
                            $motgen=\DB::table('webpsi_coc.prov_catv_tiposact')
                                ->where('motivo','=',$motigogen)
                                ->get();
                            $desc_motivo=$motgen[0]->des_motivo;
                        }

                        $solicitud_cms->cod_motivo_generacion=$codmotivo;
                        $solicitud_cms->desc_motivo_generacion=$desc_motivo;
                    }else{
                        $codmotivo=$solicitud_cms->cod_motivo_generacion;
                        $desc_motivo=$solicitud_cms->desc_motivo_generacion;
                    }

                    //fecha instalacion
                    $fecha_instalacion='';
                    if($fchinstalacion!=''){
                        $fecha_instalacion=$fchinstalacion;
                        $solicitud_cms->fecha_instalacion_alta=$fecha_instalacion;
                    }else{
                        $fecha_instalacion=$solicitud_cms->fecha_instalacion_alta;
                    }

                    //hora instalacion
                    $hora_instalacion='';
                    if($hrainstalacion!=''){
                        $hora_instalacion=$hrainstalacion;
                        $solicitud_cms->hora_instalacion_alta=$hora_instalacion;
                    }else{
                        $hora_instalacion=$solicitud_cms->hora_instalacion_alta;
                    }

                    $solicitud_cms->save();
                    $estado=true;
                    $msj="Registro actualizado correctamente!";
                    $data=array('nombres' => $nombre,'paterno'=>$ape_paterno,
                                'materno'=>$ape_materno,'zonal'=>$codzonal,'codcontrata'=>$codcontrata,
                                'desccontrata'=>$desc_contrata,'codrequerimiento'=>$codrequerimiento,
                                'descrequerimiento'=>$desc_requerimiento,'codmotivo'=>$codmotivo,
                                'descmotivo'=>$desc_motivo,'fecha_instalacion'=>$fecha_instalacion,
                                'hora_instalacion'=>$hora_instalacion);
                }

                if($form=='datos'){
                    $solicitud_cms=SolicitudTecnicaCms::find($id);

                    //telefono1
                    $telf1='';
                    if($telefono1!=''){
                        $telf1=$telefono1;
                        $solicitud_cms->telefono1=$telefono1;
                    }else{
                        $telf1=$solicitud_cms->telefono1;
                    }

                    //telefono2
                    $telf2='';
                    if($telefono2!=''){
                        $telf2=$telefono2;
                        $solicitud_cms->telefono2=$telefono2;
                    }else{
                        $telf2=$solicitud_cms->telefono2;
                    }

                    $solicitud_cms->save();
                    $estado=true;
                    $msj="Registro actualizado correctamente!";
                    $data=array('telefono1' => $telf1,'telefono2'=>$telf2);
                }

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return \Response::json(array('estado'=>$estado,'msj'=>$msj,'form'=>$form,'campos'=>$data));
    }
}