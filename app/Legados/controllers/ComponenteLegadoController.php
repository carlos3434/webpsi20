<?php namespace Legados\controllers;

use Legados\models\ComponenteCms;
use Legados\Componente as ComponenteDeco;
use Legados\models\ComponenteOperacion;
use Legados\models\SolicitudTecnicaCms as Cms;

class ComponenteLegadoController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter(
            'csrf',
            ['only' => ['getCargar']]
        );
    }

    public function getCargar()
    {
        if (!\Auth::id()) {
            $id = 1159;
            $test = str_replace(' ', '/', \Input::get('usuario'));
            if (\Hash::check($id, $test)) {
                \Auth::loginUsingId(1159);
            } else {
                return \Response::json(
                    array(
                        'rst' => 2,
                        'msj' => 'Tecnico no existente'
                    )
                );
            }
        }
        
        $deco  = array();
        $tarjeta = array();
        $modem = array();
        $lineavoip = array();
        $equipotelefonico = array();
        $codactu = \Input::get('codactu');
        $tipost = \Input::get('tipost');
        $tipolegado = \Input::get('tipolegado'); // 1 CMS, 2 GESTEL
        $idsolicitudtecnica = \Input::get('idsolicitudtecnica');

        $cms = Cms::select(
            'id',
            'solicitud_tecnica_id',
            'id_solicitud_tecnica',
            'num_requerimiento',
            'ind_origen_requerimiento'
        )
                ->where("id_solicitud_tecnica", $idsolicitudtecnica)
                ->first();

        if (count($cms) > 0) {
            $componentes = ComponenteCms::select(
                'componente_cms.numero_requerimiento',
                'componente_cms.numero_servicio',
                'componente_cms.numero_serie',
                \DB::raw('if (componente_cms.codigo_material = "" or componente_cms.codigo_material is null, ct.codmat, componente_cms.codigo_material) as codigo_material'),
                'componente_cms.id',
                'componente_cms.descripcion',
                'componente_cms.componente_cod',
                'componente_cms.flag',
                'componente_cms.marca',
                'componente_cms.modelo',
                \DB::raw('case 
                                    when componente_cms.descripcion like "%DECO%" then 0 
                                    when componente_cms.descripcion like "%TARJETA%" then 1 
                                    when componente_cms.descripcion like "%MODEM%" then 2 
                                    when componente_cms.descripcion like "%LINEA VOIP%" then 3 
                                    when componente_cms.descripcion like "%EQUIPO TELEFONICO%" then 4 
                                    else 5
                                    end as tipo'),
                \DB::raw('if (componente_cms.descripcion like "%TAR%", 1, 0) as tarjeta')
            )->leftJoin(
                'componente_test as ct',
                'ct.codcomp',
                '=',
                'componente_cms.componente_cod'
            )
                            ->where('componente_cms.solicitud_tecnica_cms_id', $cms->id)
                            ->whereNull('componente_cms.deleted_at')
                            ->orderBy('componente_cms.id', 'desc')
                            ->get();
        } else {
            $componentes = [];
        }

        $i = 1;
        foreach ($componentes as $componente) {
            $componente->solicitud_tecnica_id = $cms->solicitud_tecnica_id;
            $componente->id_solicitud_tecnica = $cms->id_solicitud_tecnica;
            $componente->num_requerimiento = $cms->num_requerimiento;
            $componente->ind_origen_requerimiento = $cms->ind_origen_requerimiento;
            $componente->codigo_material = strlen($componente->codigo_material) == 8 ?
                $componente->codigo_material : '0'.$componente->codigo_material;
            $componente->color = "#e2fbf1";
            if ($i%2 == 0) {
                $componente->color = "#fff8d0";
            }
            // $componente->color = "#" . dechex(rand(0, 0xFFFFFF));
            if ($componente->tipo == 0) {
                array_push($deco, $componente);
                $i++;
            } elseif ($componente->tipo == 1) {
                array_push($tarjeta, $componente);
                // $i++;
            } elseif ($componente->tipo == 2) {
                array_push($modem, $componente);
            } elseif ($componente->tipo == 3) {
                array_push($lineavoip, $componente);
            } elseif ($componente->tipo == 4) {
                array_push($equipotelefonico, $componente);
            }
        }

        $datos = array (
            'decos'             => $deco,
            'tarjetas'          => $tarjeta,
            'modems'            => $modem,
            'lineavoip'         => $lineavoip,
            'equipotelefonico'  => $equipotelefonico,
            'componentes'       => $componentes,
        );
        return $datos;
    }

    public function postListarrevertiraveria()
    {
        $componentes = array();
        $idSolicitudTecnica = \Input::get('id_solicitud_tecnica');
        $codReq = \Input::get('codactu');

        $query = "SELECT stc.solicitud_tecnica_id, stc.id_solicitud_tecnica, stc.num_requerimiento, 
            stc.ind_origen_requerimiento, cc.numero_requerimiento, cc.numero_servicio, cc.codigo_material,
            cc.numero_serie, cc.id, cc.descripcion, cc.componente_cod, co.id as componte_operacion_id, 
            case when cc.descripcion like '%DECO%' then 0 when cc.descripcion like '%TARJETA%' then 1 end as tipo 
            FROM componente_operacion as co 
            inner join (
                select max(t0.id) as id from componente_operacion t0 
                where t0.operacion='cambioAveria' and t0.indicador=1 and t0.indicador_procesamiento=1
                and t0.codreq='".$codReq."' group by t0.componente_id
            ) r on r.id = co.id
            inner join componente_cms as cc 
            on cc.id = co.componente_id 
            inner join solicitud_tecnica_cms as stc 
            on stc.id = cc.solicitud_tecnica_cms_id 
            WHERE co.operacion = 'cambioAveria' 
            and co.indicador = '1' and co.indicador_procesamiento = '1' 
            and co.codreq = '".$codReq."' and cc.deleted_at IS NULL 
            GROUP BY co.componente_id";
        $componentesObj = \DB::select($query);
        foreach ($componentesObj as $componente) {
            $key = 'cambioaveria_'.$componente->solicitud_tecnica_id.'_'.$componente->id;
            if (\Redis::get($key)) {
                array_push($componentes, $componente);
            }
        }

        return [
            'componentes'   => $componentes,
        ];
    }

    public function postListarrevertirreposicion()
    {
        $componentes = array();
        $idSolicitudTecnica = \Input::get('id_solicitud_tecnica');
        $codReq = \Input::get('codactu');

        $query = "SELECT stc.solicitud_tecnica_id, stc.id_solicitud_tecnica, stc.num_requerimiento, 
            stc.ind_origen_requerimiento, cc.numero_requerimiento, cc.numero_servicio, cc.codigo_material,
            cc.numero_serie, cc.id, cc.descripcion, cc.componente_cod, co.id as componte_operacion_id, 
            case when cc.descripcion like '%DECO%' then 0 when cc.descripcion like '%TARJETA%' then 1 end as tipo 
            FROM componente_operacion as co 
            inner join (
                select max(t0.id) as id from componente_operacion t0 
                where t0.operacion='reposicion' and t0.indicador=1 and t0.indicador_procesamiento=1
                and t0.codreq='".$codReq."' group by t0.componente_id
            ) r on r.id = co.id
            inner join componente_cms as cc 
            on cc.id = co.componente_id 
            inner join solicitud_tecnica_cms as stc 
            on stc.id = cc.solicitud_tecnica_cms_id 
            WHERE co.operacion = 'reposicion' 
            and co.indicador = '1' and co.indicador_procesamiento = '1' 
            and co.codreq = '".$codReq."' and cc.deleted_at IS NULL 
            GROUP BY co.componente_id";
        $componentesObj = \DB::select($query);

        foreach ($componentesObj as $componente) {
            $key = 'reposicion_'.$componente->solicitud_tecnica_id.'_'.$componente->id;
            if (\Redis::get($key)) {
                array_push($componentes, $componente);
            }
        }

        return [
            'componentes'   => $componentes,
        ];
    }

    public function postActivar() // asignacionDecos
    {
        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq', ''),
            'numcompxreq'           => \Input::get('numcompxreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codtar'                => \Input::get('codtar', ''),
            'numtar'                => \Input::get('numtar', ''),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
         ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        $componente = new ComponenteDeco();
        $respuesta = $componente->asignacionDecos($peticion, '');
        
        $flag = 2;
        $msj = 'Error de Envio de activacion';

        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Activacion Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }
        
        if (\Input::get('deco_componente_id') && \Input::get('tarjeta_componente_id')) {
            if ($flag == 1) {
                $componentes = ComponenteCms::find(\Input::get('deco_componente_id'));
                $componentes['numero_serie'] = \Input::get('numser', '');
                $componentes['codigo_material'] = \Input::get('codmat', '');
                $componentes['flag'] = 1;
                $componentes->save();

                $componentes = ComponenteCms::find(\Input::get('tarjeta_componente_id'));
                $componentes['numero_serie'] = \Input::get('numtar', '');
                $componentes['codigo_material'] = \Input::get('codtar', '');
                $componentes['flag'] = 1;
                $componentes->save();
            }
        }

        $respuesta->operacion='asignacionDecos';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('deco_componente_id', '');
        $respuesta->telefono_origen =\Input::get('telefono_origen', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $respuesta->numserpar =\Input::get('numtar', '');
        $respuesta->codmatpar =\Input::get('codtar', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postDesasignar()
    {


        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq', ''),
            'codmat'                => \Input::get('codmat'), //deco
            'numser'                => \Input::get('numser'), //deco
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }
        

        $componente = new ComponenteDeco;
        $respuesta=$componente->desasignacionDecos($peticion, '');

        $flag = 2;
        $msj = 'Error envio de Desasignar';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Desasignar Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        $respuesta->operacion='desasignacionDecos';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('deco_componente_id', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postCambioaveria()
    {
        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'numcompxreq'           => \Input::get('numcompxreq'),
            'numcompxsrv'           => \Input::get('numcompxsrv'), // numero_servicio
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'numserold'             => \Input::get('numserold'),
            'codmatpar'             => \Input::get('codmatpar'),
            'numserpar'             => \Input::get('numserpar'),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
         ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj,
                        'descripcion' => \Input::get('descripcion', ''),
                        'numserold' => \Input::get('numserold', '')
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        $componente = new ComponenteDeco;
        $respuesta = $componente->cambioAveria($peticion, '');
        $flag = 2;
        $msj = 'Error envio de Cambio por Averia';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Cambio por Averia Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        if (\Input::get('componente_id')) {
            if ($flag == 1) {
                $trama = json_encode(\Input::all());
                $key = 'cambioaveria_'.\Input::get('solicitud_tecnica_id').'_'.\Input::get('componente_id');
                \Redis::set($key, $trama);
                \Redis::expire($key, 8*60*60);
                $componentes = ComponenteCms::find(\Input::get('componente_id'));
                $componentes['numero_serie'] = \Input::get('numser', '');
                $componentes['codigo_material'] = \Input::get('codmat', '');
                $componentes->save();
            }
        }

        $respuesta->operacion='cambioAveria';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('componente_id', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $respuesta->numserold =\Input::get('numserold', '');
        $respuesta->codmatold =\Input::get('codmatold', '');
        $respuesta->numserpar =\Input::get('numserpar', '');
        $respuesta->codmatpar =\Input::get('codmatpar', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj,
                'descripcion' => \Input::get('descripcion', ''),
                'numserold' => \Input::get('numserold', '')
            )
        );
    }

    public function postRevertiraveria()
    {
        $key = 'cambioaveria_'.\Input::get('solicitud_tecnica_id').'_'.\Input::get('componente_id');
        if (\Redis::get($key)) {
            $trama = json_decode(\Redis::get($key), true);
        } else {
            return \Response::json(
                array(
                    'rst' => 2,
                    'msj' => 'No se realizo cambio de averia para este componente'
                )
            );
        }

        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codmatpar'             => (isset($trama["codmatpar"]) ? $trama["codmatpar"] : ''),
            'numserpar'             => (isset($trama["numserpar"]) ? $trama["numserpar"] : ''),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        // \Config::set("legado.wsdl.RegistroDecos", \Config::get("legado.wsdl.RegistroDecos_test"));
        $componente = new ComponenteDeco;
        $respuesta=$componente->revertirAveria($peticion, '');
        $flag = 2;
        $msj = 'Error envio de Revertir Averia';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Revertir Averia Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        if (\Input::get('componente_id')) {
            if ($flag == 1) {
                \Redis::del($key);
                $componentes = ComponenteCms::find(\Input::get('componente_id'));
                $componentes['numero_serie'] = (isset($trama["numserold"]) ? $trama["numserold"] : '');
                $componentes['codigo_material'] = (isset($trama["codmatold"]) ? $trama["codmatold"] : '');
                $componentes->save();
            }
        }

        $respuesta->operacion='revertirAveria';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('componente_id', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $respuesta->numserpar =isset($trama["numserpar"]) ? $trama["numserpar"] : '';
        $respuesta->codmatpar =isset($trama["codmatpar"]) ? $trama["codmatpar"] : '';
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postReposicion()
    {
        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'numcompxreq'           => \Input::get('numcompxreq'),
            'numcompxsrv'           => \Input::get('numcompxsrv'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'numserold'             => \Input::get('numserold'),
            'codmatpar'             => \Input::get('codmatpar'),
            'numserpar'             => \Input::get('numserpar'),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        // \Config::set("legado.wsdl.RegistroDecos", \Config::get("legado.wsdl.RegistroDecos_test"));
        $componente = new ComponenteDeco;
        $respuesta=$componente->reposicion($peticion, '');
        $flag = 2;
        $msj = 'Error envio de Reposicion';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Reposicion Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        if (\Input::get('componente_id')) {
            if ($flag == 1) {
                $trama = json_encode(\Input::all());
                $key = 'reposicion_'.\Input::get('solicitud_tecnica_id').'_'.\Input::get('componente_id');
                \Redis::set($key, $trama);
                \Redis::expire($key, 8*60*60);

                $componentes = ComponenteCms::find(\Input::get('componente_id'));
                $componentes['numero_serie'] = \Input::get('numser', '');
                $componentes['codigo_material'] = \Input::get('codmat', '');
                $componentes->save();
            }
        }
        
        $respuesta->operacion='reposicion';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('componente_id', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $respuesta->numserold =\Input::get('numserold', '');
        $respuesta->codmatold =\Input::get('codmatold', '');
        $respuesta->numserpar =\Input::get('numserpar', '');
        $respuesta->codmatpar =\Input::get('codmatpar', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postRevertireposicion()
    {
        $key = 'reposicion_'.\Input::get('solicitud_tecnica_id').'_'.\Input::get('componente_id');
        if (\Redis::get($key)) {
            $trama = json_decode(\Redis::get($key), true);
        } else {
            return \Response::json(
                array(
                    'rst' => 2,
                    'msj' => 'No se realizo reposicion para este componente'
                )
            );
        }

        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        // \Config::set("legado.wsdl.RegistroDecos", \Config::get("legado.wsdl.RegistroDecos_test"));
        $componente = new ComponenteDeco;
        $respuesta=$componente->revertirReposicion($peticion, '');
        $flag = 2;
        $msj = 'Error envio de Revertir Reposicion';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Revertir Reposicion Enviada, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        if (\Input::get('componente_id')) {
            if ($flag == 1) {
                \Redis::del($key);
                $componentes = ComponenteCms::find(\Input::get('componente_id'));
                $componentes['numero_serie'] = (isset($trama["numserold"]) ? $trama["numserold"] : '');
                $componentes['codigo_material'] = (isset($trama["codmatold"]) ? $trama["codmatold"] : '');
                $componentes->save();
            }
        }

        $respuesta->operacion='revertirReposicion';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('componente_id', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postRefresh()
    {
        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codtar'                => \Input::get('codtar'),
            'codact'                => \Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        if (strlen($peticion['codmat'])  == 11 && isset($peticion['codmat'])) {
            $codificacion_respuesta = $this->analizarCodigoMaterial($peticion['codmat']);

            if ($codificacion_respuesta['rspt']  == 2) {
                
                $msj = 'Error, no se encontro un equivalente al codigo de material.';
                return \Response::json(
                    array(
                        'rst' => $codificacion_respuesta['rspt'],
                        'msj' => $msj
                    )
                );
            }
            $peticion['codmat'] =  $codificacion_respuesta['nuevo_codigo_material'];
        }

        $componente = new ComponenteDeco;
        $respuesta = $componente->refresh($peticion, '');

        $flag = 2;
        $msj = 'Error envio de Refresh';
        if ($respuesta->error == false) {
            if ($respuesta->data->indicador=='-1') {
                $flag = 2;
                $msj = $respuesta->data->observacion;
            } else {
                $flag = 1;
                $msj = 'Refresh Enviado, la confirmación de la 
                    operación llegará a su celular en breve';
            }
        } elseif ($respuesta->error == true) {
            if (isset($respuesta->error->obj->detail->ClientException->exceptionDetail)) {
                $respuesta->observacion = $respuesta->error->obj->detail->ClientException->exceptionDetail;
            } else {
                $respuesta->observacion = 'Problema en el servidor de CMS';
            }
            $flag = 2;
            $msj = $respuesta->observacion;
        }

        $respuesta->operacion='refresh';
        $respuesta->id_solicitud_tecnica=\Input::get('id_solicitud_tecnica', '');
        $respuesta->componente_id=\Input::get('deco_componente_id', '');
        $respuesta->telefono_origen =\Input::get('telefono_origen', '');
        $respuesta->interface =\Input::get('interface', '1');
        $respuesta->codreq =\Input::get('codreq', '');
        $respuesta->numser =\Input::get('numser', '');
        $respuesta->codmat =\Input::get('codmat', '');
        $respuesta->codmatpar =\Input::get('codtar', '');
        $ComponenteOperacion = new ComponenteOperacion;
        $operacion=$ComponenteOperacion->guardar($respuesta);
        
        return \Response::json(
            array(
                'rst' => $flag,
                'msj' => $msj
            )
        );
    }

    public function postModem()
    {
        $equipos = \Input::get("equipos");
        $msj = "Series: ";
        foreach ($equipos as $equipo) {
            $componente = ComponenteCms::find($equipo["id"]);
            $componente->numero_serie = $equipo["numero_serie"];
            // $componente->marca = $equipo["marca"];
            // $componente->modelo = $equipo["modelo"];
            $componente->flag = 1;
            $componente->save();
            $msj .= $equipo["numero_serie"].", ";
        }

        return \Response::json([
            'rst' => 1,
            'msj' => $msj." registrados"
        ]);
    }

    public function getTarjetas()
    {
        $datos = [
            [
                'codtar' => '07870141',
                'descripcion' => 'INSTALAR TARJETA DIGITAL'
            ]
        ];
        return $datos;
    }

    public function postComponentoperacion()
    {
        return ComponenteOperacion::getComponenteOperacion(\Input::get('codactu'));
    }

    public function analizarCodigoMaterial($codificacion_respuesta)
    {
        
        $rspt = 1;
        $nuevo_codigo_material = '';
        $valor_codificador = \DB::select(
            \DB::raw('SELECT de.codmat_equivalencia 
                from decodificadores_equivalencia as de  
                where de.codmat  = "'.$codificacion_respuesta.'"
            limit 1')
        );
        if (count($valor_codificador)  == 0) {
            $rspt = 2;
        } else {
            $nuevo_codigo_material = $valor_codificador[0]->codmat_equivalencia;
        }
        $resultado = ['rspt' => $rspt, 'nuevo_codigo_material' => $nuevo_codigo_material];
        return $resultado;
    }

    public function getModemmarcamodelo()
    {
        $marca = [
            [
                'codigo' => '1209',
                'nombre' => 'SMART HOOK'
            ],
            [
                'codigo' => '3100',
                'nombre' => 'DOCTOC FLY'
            ]
        ];

        $modelo = [
            [
                'codigo' => '1111',
                'nombre' => 'CUADRADO'
            ],
            [
                'codigo' => '1111',
                'nombre' => 'RECTANGULO'
            ]
        ];

        return [
            'marca' => $marca,
            'modelo' => $modelo,
        ];
    }

    public function getEquipotelefonicomarcamodelo()
    {
        $marca = [
            [
                'codigo' => '5555',
                'nombre' => 'NOKEO'
            ],
            [
                'codigo' => '6532',
                'nombre' => 'SAMSONG'
            ]
        ];

        $modelo = [
            [
                'codigo' => '9091',
                'nombre' => 'XP-134'
            ],
            [
                'codigo' => '4324',
                'nombre' => 'TAR-GZ'
            ]
        ];
        
        return [
            'marca' => $marca,
            'modelo' => $modelo,
        ];
    }
}
