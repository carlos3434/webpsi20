<?php

namespace Legados\controllers;

use Legados\models\LogEnvioMasivo;
use Legados\models\SolicitudCaida;
use Legados\Repositories\StGestionRepository;
use Illuminate\Support\Collection;
use DB;
use Response;
use Input;
use File;


class EnvioMasivoStController extends \BaseController {

    public function postListarsolicitudes() {

        $order = ["column" => Input::get('column'),"dir" => Input::get('dir')];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        /*consulta*/
        $select=['st_u.id','st.id_solicitud_tecnica AS solicitud_tecnica',
        'st_u.num_requerimiento AS requerimiento','est.nombre AS estado_ofsc',
        DB::raw("IF(st_c.flujo=3,'PSI-TOA','')  AS nombre_flujo"),'st_c.codigo_error',
        'st_c.error','st_c.created_at AS fecha_creacion','st_c.estado',
        DB::raw("IF(st_u.tipo_legado=1,'CMS',IF(st_u.tipo_legado=2,'GESTEL',''))  AS legado"),
        DB::raw("IF(st_u.actividad_id=1,'AVERIA',IF(st_u.actividad_id=2,'PROVISION','')) 
        AS actividad"),"st_c.intentos","st_c.observacion"];

        /*Filtros*/
        $codigo_error='ERR27';
        $data2= DB::table('solicitud_tecnica_caida AS st_c')
                ->select($select)
                ->leftjoin('solicitud_tecnica AS st','st.id','=','st_c.solicitud_tecnica_id')
                ->leftjoin('solicitud_tecnica_ultimo AS st_u','st_c.solicitud_tecnica_id','=',
                'st_u.solicitud_tecnica_id')
                ->leftjoin('estados_ofsc AS est','st_u.estado_ofsc_id','=','est.id')
                ->whereRaw("st_c.codigo_error='".$codigo_error."' and st_c.estado=1 and 
                    IFNULL(st_c.deleted_at,'')='' ");

        $column = "st.id_solicitud_tecnica";
        $dir = "asc";

        $data2 = $data2
                ->orderBy($column, $dir)
                ->paginate($grilla["perPage"]);

        $data = $data2->toArray();

        $col = new Collection([
            'recordsTotal'=> $data2->getTotal(),
            'recordsFiltered'=> $data2->getTotal(),
        ]);

        return $col->merge($data);
    }

    public function postLogenviomasivo() {

        /*consulta*/
        $select=['log_mas.id',DB::raw("IF(log_mas.flujo=3,'PSI-TOA','')  AS nombre_flujo"),
        'log_mas.codigo_error','log_mas.error','log_mas.cantidad',
        DB::raw("UPPER(CONCAT(u.apellido,', ',u.nombre)) AS usuario"),
        'log_mas.created_at as fecha_creacion'];

        /*Filtros*/
        $codigo_error='ERR27';
        $data_log= DB::table('log_envio_masivo AS log_mas')
                ->select($select)
                ->leftjoin('usuarios AS u','log_mas.usuario_id','=','u.id')
                ->orderBy('log_mas.id', 'desc')
                ->get();


        $data= DB::table('solicitud_tecnica_caida')
                ->select([DB::raw('count(*) as cantidad')])
                ->whereRaw("codigo_error='".$codigo_error."' and estado=1 and 
                    IFNULL(deleted_at,'')=''")
                ->get();

        $cantidad=$data[0]->cantidad;

        return \Response::json(array('log_envio_masivo'=>$data_log,'cantidad'=>$cantidad));
    }

    public function postEnviomasivoofsc(){

        $hoy = date("Y-m-d");
        $codigo_error='ERR27';
        $inputs = [];

        $select_log=['flujo','codigo_error','error'];
        $log=DB::table('solicitud_tecnica_caida')
                    ->select($select_log)
                    ->whereRaw("estado=1 AND codigo_error='".$codigo_error."'")
                    ->first();

        $flujo = $log->flujo;
        $error = $log->error;

        $select=['st_c.solicitud_tecnica_id','st.id_solicitud_tecnica AS solicitud_tecnica',
        'st_u.num_requerimiento AS requerimiento','st_u.tipo_legado','st_u.actividad_id AS actividad',
        'st_u.actividad_tipo_id AS actividad_tipo','st_u.quiebre_id AS quiebre',
        'st_u.tipo_envio_ofsc AS tipo_envio',
        DB::raw("'Reenvio TOA - Error coneccion TOA' AS observacion_toa"),
        'st_u.quiebre_id','q.apocope','st_u.actividad_tipo_id','act_t.label as workType',
        'st_cms.cod_nodo as campo1','st_cms.cod_troba as campo2','st_cms.cod_plano as campo3'];
        $solicitud_cms=DB::table('solicitud_tecnica_caida AS st_c')
                    ->select($select)
                    ->join('solicitud_tecnica AS st','st_c.solicitud_tecnica_id','=','st.id')
                    ->join('solicitud_tecnica_ultimo AS st_u','st_u.solicitud_tecnica_id','=',
                    'st_c.solicitud_tecnica_id')
                    ->join('quiebres AS q','q.id','=','st_u.quiebre_id')
                    ->join('actividades_tipos AS act_t','act_t.id','=','st_u.actividad_tipo_id')
                    ->join('solicitud_tecnica_cms AS st_cms','st_cms.solicitud_tecnica_id','=',
                    'st_c.solicitud_tecnica_id')
                    ->whereRaw("st_c.estado=1 AND IFNULL(st_c.deleted_at,'')='' AND st_u.tipo_legado=1 AND codigo_error='".$codigo_error."'");

        unset($select[13]);$select[13] = 'st_gstave.cabecera_mdf as campo1';
        unset($select[14]);$select[14] = 'st_gstave.cod_armario as campo2';
        unset($select[15]);$select[15] = 'st_gstave.cable as campo3';
        $solicitud_gestel_averia=DB::table('solicitud_tecnica_caida AS st_c')
                    ->select($select)
                    ->join('solicitud_tecnica AS st','st_c.solicitud_tecnica_id','=','st.id')
                    ->join('solicitud_tecnica_ultimo AS st_u','st_u.solicitud_tecnica_id','=',
                    'st_c.solicitud_tecnica_id')
                    ->join('quiebres AS q','q.id','=','st_u.quiebre_id')
                    ->join('actividades_tipos AS act_t','act_t.id','=','st_u.actividad_tipo_id')
                    ->join('solicitud_tecnica_gestel_averia AS st_gstave',
                    'st_gstave.solicitud_tecnica_id','=','st_c.solicitud_tecnica_id')
                    ->whereRaw("st_c.estado=1 AND IFNULL(st_c.deleted_at,'')='' AND st_u.tipo_legado=2 AND st_u.actividad_id=1 AND 
                        codigo_error='".$codigo_error."'");

        unset($select[13]);$select[13] = 'st_gstprov_ffttcob.cabecera_mdf as campo1';
        unset($select[14]);$select[14] = 'st_gstprov_ffttcob.cod_armario as campo2';
        unset($select[15]);$select[15] = 'st_gstprov_ffttcob.cable as campo3';
        $solicitud_gestel_provision=DB::table('solicitud_tecnica_caida AS st_c')
                    ->select($select)
                    ->join('solicitud_tecnica AS st','st_c.solicitud_tecnica_id','=','st.id')
                    ->join('solicitud_tecnica_ultimo AS st_u','st_u.solicitud_tecnica_id','=',
                    'st_c.solicitud_tecnica_id')
                    ->join('quiebres AS q','q.id','=','st_u.quiebre_id')
                    ->join('actividades_tipos AS act_t','act_t.id','=','st_u.actividad_tipo_id')
                    ->join('solicitud_tecnica_gestel_provision AS st_gstprov',
                    'st_gstprov.solicitud_tecnica_id','=','st_c.solicitud_tecnica_id')
                    ->join('gestel_provision_ffttcobre AS st_gstprov_ffttcob',
                    'st_gstprov.id','=','st_gstprov_ffttcob.solicitud_tecnica_gestel_provision_id')
                    ->whereRaw("st_c.estado=1 AND IFNULL(st_c.deleted_at,'')='' AND
                        st_u.tipo_legado=2 AND st_u.actividad_id=2 AND 
                        codigo_error='".$codigo_error."'");

        $solicitudes = $solicitud_cms->union($solicitud_gestel_averia)
                                     ->union($solicitud_gestel_provision);
        $array=[];
        $cant_envio=0;
        foreach ($solicitudes->get() as $key => $value) {

            $array['campo1'] = $value->campo1;
            $array['campo2'] = $value->campo2;
            $array['campo3'] = $value->campo3;
            $workZone = $this->workZone($array);

            $llave = $hoy.'|AM|'.$value->workType.'|'.$value->apocope.'|'.$workZone;
            $rst = \Redis::get($llave);
            $capacidad = json_decode($rst, true);
            
            if (!isset($capacidad["location"])) {
                $llave = $hoy.'|PM|'.$value->workType.'|'.$value->apocope.'|'.$workZone;
                $rst = \Redis::get($llave);
                $capacidad = json_decode($rst, true);
            } 

            $solicitud_tecnica_id = $value->solicitud_tecnica_id;
            $solicitud_tecnica = $value->solicitud_tecnica;
            $requerimiento = $value->requerimiento;
            $tipo_legado = $value->tipo_legado;
            $actividad = $value->actividad;
            $actividad_tipo = $value->actividad_tipo;
            $tipo_envio = 'sla';
            $hf = $hoy.'||'.$capacidad["location"].'||||';
            $observacion_toa = $value->observacion_toa;
            $quiebre = $value->quiebre;

            if (isset($capacidad["location"])) {
                
                $inputs = [
                    'actividad_tipo_id'     => $actividad_tipo,
                    'actividad_id'          => $actividad,
                    'tematicos'             => [],
                    'observacion_toa'       => $observacion_toa,
                    'solicitud_tecnica_id'  => $solicitud_tecnica_id,
                    'agdsla'                => $tipo_envio,
                    'hf'                    => $hf,
                    'quiebre'               => $quiebre
                ];

                $solicitudGestion = new StGestionRepository();
                $response = $solicitudGestion->envioOfsc($solicitud_tecnica_id, $inputs);
                if($response['rst']==2 and $response['msj']=="Error coneccion TOA"){

                    $msjError = "Error coneccion TOA";
                    $codError = 'ERR27';//go debe reenviar
                    $observacion = $codError.' | '.$msjError;
                    
                    $obj = new SolicitudCaida;
                    $obj_st=$obj::where('solicitud_tecnica_id','=',$solicitud_tecnica_id)->first();
                    $obj_st->observacion = $observacion;
                    $obj_st->intentos=(($obj_st->intentos)+1);
                    $obj_st->save();

                    $obj_log = new LogEnvioMasivo;
                    $obj_log->flujo = $flujo; 
                    $obj_log->codigo_error = $codigo_error; 
                    $obj_log->error = $error; 
                    $obj_log->cantidad = $cant_envio; 
                    $obj_log->usuario_id = \Auth::user()->id;
                    $obj_log->save();

                    return;

                }elseif ($response['rst']==1 and $response['msj']=="Envio a Correcto a OFSC") {
                    $cant_envio++;
                }
            }else{
                $msjError = "Consulta CAPACIDAD a TOA: FAIL";
                $codError = 'ERR12';//go debe reenviar
                $observacion = $codError.' | '.$msjError;

                $obj = new SolicitudCaida;
                $obj_st=$obj::where('solicitud_tecnica_id','=',$solicitud_tecnica_id)->first();
                $obj_st->observacion = $observacion;
                $obj_st->intentos=(($obj_st->intentos)+1);
                $obj_st->save();
            }
        }

        $obj_log = new LogEnvioMasivo;
        $obj_log->flujo = $flujo; 
        $obj_log->codigo_error = $codigo_error; 
        $obj_log->error = $error; 
        $obj_log->cantidad = $cant_envio; 
        $obj_log->usuario_id = \Auth::user()->id;
        $obj_log->save(); 

        $estado = true;
        $msg = 'Envio Masivo Finalizado Correctamente!';

        return \Response::json(array('estado'=>$estado,'msg'=>$msg));
    }

    public function workZone($array){

        $workZone = null;
            if ($array['campo1'] != '') {
                if ($array['campo2']  != "") {
                    $workZone = $array['campo1']."_".$array['campo2'];
                } elseif ($this->cod_plano  != "") {
                    $workZone = $array['campo1']."_".$array['campo3'];
                }
            }

        return  $workZone;   
    }                 

}
