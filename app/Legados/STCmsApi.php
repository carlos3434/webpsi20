<?php
namespace Legados;

use Legados\Legado;
//use Legados\models\TiempoComunicacionLegado as TiempoLog;
use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;

class STCmsApi extends Legado
{
    public function __construct($userId = 'CMS Legado')
    {
        $this->_wsdl = \Config::get("legado.wsdl.respuestast");
        $this->_userId = $userId;
        parent::__construct();
    }

    public function cierre($peticion)
    {
        if (count($peticion['componentes'])==0) {
            $componentes = [
                'numero_requerimiento'=>'',
                'numero_ot'=>'',
                'numero_servicio'=>'',
                'componente_cod'=>'',
                'descripcion'=>'',
                'codigo_material'=>'',
                'tipo_adquisicion'=>'',
                'tipo_procedencia'=>'',
                'marca'=>'',
                'modelo'=>'',
                'componente_tipo'=>'',
                'numero_serie'=>'',
                'casid'=>'',
                'estado'=>'',
                'cadena1'=>'',
                'cadena2'=>'',
                'cadena3'=>'',
                'cadena4'=>'',
                'cadena5'=>''
            ];
        } else {
            $componentes = $peticion['componentes'];
        }
        $body = [
            'tipo_operacion'           => $peticion['tipo_operacion'],
            'id_solicitud_tecnica'     => $peticion['id_solicitud_tecnica'],
            'fecha_envio'              => $peticion['fecha_envio'],
            'hora_envio'               => $peticion['hora_envio'],
            'codigo_liquidacion'       => $peticion['codigo_liquidacion'],
            'codigo_tecnico1'          => $peticion['codigo_tecnico1'],
            'codigo_tecnico2'          => $peticion['codigo_tecnico2'],
            'fecha_inicio'             => $peticion['fecha_inicio'],
            'hora_inicio'              => $peticion['hora_inicio'],
            'fecha_fin'                => $peticion['fecha_fin'],
            'hora_fin'                 => $peticion['hora_fin'],
            'fecha_liquida'            => $peticion['fecha_liquida'],
            'hora_liquida'             => $peticion['hora_liquida'],
            'nombre_contacto'          => $peticion['nombre_contacto'],
            'parentesco_contacto'      => $peticion['parentesco_contacto'],
            'dni_contacto'             => $peticion['dni_contacto'],
            'codigo_contrata'          => $peticion['codigo_contrata'],
            'observacion'              => $peticion['observacion'],
            'campo1'                   => $peticion['campo1'],
            'campo2'                   => $peticion['campo2'],
            'campo3'                   => $peticion['campo3'],
            'campo4'                   => $peticion['campo4'],
            'campo5'                   => $peticion['campo5'],
            'componente'               => $componentes
        ];

        $elementos = [
            'action'      => 'CierreSolicitudTecnica',
            'body'      => $body,
            'operation' => 'CierreSolicitudTecnica',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local')) {
            //print_r($elementos);
            $response = new \stdClass;
            $response->rst=true;
            $response->msj="Prueba: Enviado correctamente!";
            //return;
        }else{
            $response = $this->doAction($elementos);
        }
        $this->tracer($elementos, $response);

        //(6) REGISTRO PSI-CMS

        $transaccionesGoRepo = new TransaccionesGoRepo();
        $transaccionesGoRepo->registrarLogsFlujo([
            "id_solicitud_tecnica" => $peticion["id_solicitud_tecnica"],
            "flujo" => 6,
            "tipo" => 1
        ]);

        return $response;
    }

    public function responderST($respuesta = [])
    {
        $body = [
            "NumeroSolicitud" => $respuesta["id_solicitud_tecnica"],
            "IndicadorRespuesta" => $respuesta["id_respuesta"],
            "Observacion" => $respuesta["observacion"]
        ];
        $elementos= [
            'action' => 'RptaEnvioSolicitudTecnica',
            'body' => $body,
            'operation' => 'RespuestaDeEnvio',
            'solicitud_tecnica_id' => $respuesta["solicitud_tecnica_id"],
        ];
        if (\App::environment('local')) {
            $response = new \stdClass;
            $response->error = false;
            $response->errorMsg = "prueba";
            $response->exceptionCode = new \stdClass;
            $response->errorString = "Error de Prueba!!!";
            $response->exceptionDetail = "Prueba";
            $response->data = new \stdClass;
            $response->data->descripcion = "Ok";
            $response->rst=1;
            $response->msj="Prueba: Enviado correctamente!";
            //print_r($elementos);
        } else {
            $response = $this->doAction($elementos);
        }
        $this->tracer($elementos, $response);

        //(2) REGISTRO CMS-PSI
        $transaccionesGoRepo = new TransaccionesGoRepo();
        $transaccionesGoRepo->registrarLogsFlujo([
            "id_solicitud_tecnica" => $respuesta["id_solicitud_tecnica"],
            "flujo" => 2,
            "tipo" => 2
        ]);
        return $response;
    }
}
