<?php namespace Legados\models;
class RetornoLegado extends \Eloquent {
	protected $fillable = ['id_solicitud_tecnica','host','accion','request','error_code','msj_error'];
    public $table = 'retorno_legado';


    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }
}