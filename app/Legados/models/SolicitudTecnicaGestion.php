<?php
namespace Legados\models;

use Legados\models\SolicitudTecnicaCms as StCms;
use Legados\models\SolicitudTecnicaGestelProvision as StGestelP;
use Legados\models\SolicitudTecnicaGestelAveria as StGestelA;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as MovimientoDetalle;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\helpers\SolicitudTecnicaHelper;
use Illuminate\Support\Collection;
use Ofsc\Inbound;

class SolicitudTecnicaGestion
{
    use \SolicitudTecnicaTrait;

    public function generarMovimientoconTematico($tematicos = [], $movimiento = [], $data = [])
    {
        $movimientoReciente = Movimiento::where(["solicitud_tecnica_id" => $data["solicitud_tecnica_id"]])
            ->orderBy("id", "DESC")->first();

        if (!is_null($movimientoReciente) && count($tematicos) > 0) {
            $objdetalle = new MovimientoDetalle;
            $objdetalle["solicitud_tecnica_movimiento_id"] = $movimientoReciente->id;
            $objdetalle["tipo"] = (isset($data["tipo"]))? $data["tipo"] : null;
            $objdetalle["comentarios"] = (isset($data["comentarios"]))? $data["comentarios"] : "";
            $objdetalle["campo1"] = (isset($tematicos[0])) ? $tematicos[0]["nombre"] : "";
            $objdetalle["campo2"] = (isset($tematicos[1])) ? $tematicos[1]["nombre"] : "";
            $objdetalle["campo3"] = (isset($tematicos[2])) ? $tematicos[2]["nombre"] : "";
            $objdetalle["campo4"] = (isset($tematicos[3])) ? $tematicos[3]["nombre"] : "";
            $objdetalle["observacion_form"] = (isset($data["observacion_form"]))? $data["observacion_form"] : null;
            $objdetalle->save();
        }
    }

    public function bodyBusqueda($descargar = false)
    {
        $solicitudesCms = StCms::select(
            "solicitud_tecnica_cms.id_solicitud_tecnica",
            "bu.bucket_ofsc AS bucket",
            "solicitud_tecnica_cms.orden_trabajo",
            "solicitud_tecnica_cms.created_at",
            "solicitud_tecnica_cms.cod_troba",
            "solicitud_tecnica_cms.zonal",
            "solicitud_tecnica_cms.tipo_operacion",
            "solicitud_tecnica_cms.cod_nodo",
            "solicitud_tecnica_cms.telefono1 as telf1",
            "solicitud_tecnica_cms.telefono2 as telf2",
            "solicitud_tecnica_cms.peticion as numero_peticiion",
            "solicitud_tecnica_cms.coordx_cliente AS x_cliente",
            "solicitud_tecnica_cms.coordy_cliente AS y_cliente",
            \DB::raw(" '' AS cabecera_mdf"),
            "u.num_requerimiento",
            "u.estado_ofsc_id",
            "eo.nombre as estado_ofsc",
            "u.estado_st",
            "act.nombre as actividad",
            "u.actividad_id",
            "u.actividad_tipo_id",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo"),
            'q.nombre as quiebre',
            "u.quiebre_id",
            \DB::raw("(CASE WHEN (u.tipo_legado = '1') THEN 'CMS' ELSE 'GESTEL' END) AS tipo_legados"),
            "u.tipo_legado",
            "u.fecha_agenda",
            "u.estado_aseguramiento",
            "u.solicitud_tecnica_id",
            "u.updated_at",
            "u.motivo_ofsc_id",
            "u.sistema_externo_id",
            "q.apocope as quiebres",
            "eo.nombre as estado_ofsc2",
            "eo.name as estado_ofsc_etiqueta",
            "ea.nombre as estado_aseguramiento_nombre",
            "eo.color as color_ofsc",
            "eo.color_letra",
            "mo.estado_ofsc as tipodevolucion",
            "mo.descripcion as motivo_ofsc_descripcion",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo_nombre"),
            \DB::raw("IFNULL(p.nombre, '') as parametro"),
            \DB::raw("IFNULL(p.tipo, '') as parametro_tipo"),
            "u.workzone"
        )->getUltimoCms()
        ->getMotivoofsc()
        ->getEstadoofsc()
        ->getQuiebre()
        ->GetActividades()
        ->getActividadTipo()
        ->getBucket()
        ->getEstadoaseguramiento()
        ->getParametro();

        $solicitudesGestelProvision = StGestelP::select(
            "solicitud_tecnica_gestel_provision.id_solicitud_tecnica",
            "bu.bucket_ofsc AS bucket",
            \DB::raw(" '' AS orden_trabajo"),
            "solicitud_tecnica_gestel_provision.created_at",
            \DB::raw(" '' AS cod_troba"),
            "solicitud_tecnica_gestel_provision.zonal",
            "solicitud_tecnica_gestel_provision.tipo_operacion",
            \DB::raw(" IFNULL(solicitud_tecnica_gestel_provision_ffttcobre.cabecera_mdf, '') AS cod_nodo"),
            \DB::raw(" '' AS telf1"),
            \DB::raw(" '' AS telf2"),
            "solicitud_tecnica_gestel_provision.peticion as numero_peticiion",
            \DB::raw(" '' AS x_cliente"),
            \DB::raw(" '' AS y_cliente"),
            "solicitud_tecnica_gestel_provision_ffttcobre.cabecera_mdf",
            "u.num_requerimiento",
            "u.estado_ofsc_id",
            "eo.nombre as estado_ofsc",
            "u.estado_st",
            "act.nombre as actividad",
            "u.actividad_id",
            "u.actividad_tipo_id",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo"),
            'q.nombre as quiebre',
            "u.quiebre_id",
            \DB::raw("(CASE WHEN (u.tipo_legado = '1') THEN 'CMS' ELSE 'GESTEL' END) AS tipo_legados"),
            "u.tipo_legado",
            "u.fecha_agenda",
            "u.estado_aseguramiento",
            "u.solicitud_tecnica_id",
            "u.updated_at",
            "u.motivo_ofsc_id",
            "u.sistema_externo_id",
            "q.apocope as quiebres",
            "eo.nombre as estado_ofsc2",
            "eo.name as estado_ofsc_etiqueta",
            "ea.nombre as estado_aseguramiento_nombre",
            "eo.color as color_ofsc",
            "eo.color_letra",
            "mo.estado_ofsc as tipodevolucion",
            "mo.descripcion as motivo_ofsc_descripcion",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo_nombre"),
            \DB::raw("IFNULL(p.nombre, '') as parametro"),
            \DB::raw("IFNULL(p.tipo, '') as parametro_tipo"),
            \DB::raw("'' as workzone")
        )->getUltimoGestelProvision()
        ->getMotivoofsc()
        ->getEstadoofsc()
        ->getQuiebre()
        ->GetActividades()
        ->getActividadTipo()
        ->getBucket()
        ->getEstadoaseguramiento()
        ->getParametro()
        ->leftJoin(
            "solicitud_tecnica_gestel_provision_ffttcobre",
            "solicitud_tecnica_gestel_provision_ffttcobre.solicitud_tecnica_gestel_provision_id",
            "=",
            "solicitud_tecnica_gestel_provision.id"
        );

        $solicitudesGestelAveria =StGestelA::select(
            "solicitud_tecnica_gestel_averia.id_solicitud_tecnica",
            "bu.bucket_ofsc AS bucket",
            \DB::raw(" '' AS orden_trabajo"),
            "solicitud_tecnica_gestel_averia.created_at",
            \DB::raw(" '' AS cod_troba"),
            \DB::raw(" '' AS zonal"),
            "solicitud_tecnica_gestel_averia.tipo_operacion",
            \DB::raw(" '' AS cod_nodo"),
            \DB::raw(" '' AS telf1"),
            \DB::raw(" '' AS telf2"),
            \DB::raw(" '' AS numero_peticiion"),
            \DB::raw(" '' AS x_cliente"),
            \DB::raw(" '' AS y_cliente"),
            \DB::raw(" '' AS cabecera_mdf"),
            "u.num_requerimiento",
            "u.estado_ofsc_id",
            "eo.nombre as estado_ofsc",
            "u.estado_st",
            "act.nombre as actividad",
            "u.actividad_id",
            "u.actividad_tipo_id",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo"),
            'q.nombre as quiebre',
            "u.quiebre_id",
            \DB::raw("(CASE WHEN (u.tipo_legado = '1') THEN 'CMS' ELSE 'GESTEL' END) AS tipo_legados"),
            "u.tipo_legado",
            "u.fecha_agenda",
            "u.estado_aseguramiento",
            "u.solicitud_tecnica_id",
            "u.updated_at",
            "u.motivo_ofsc_id",
            "u.sistema_externo_id",
            "q.apocope as quiebres",
            "eo.nombre as estado_ofsc2",
            "eo.name as estado_ofsc_etiqueta",
            "ea.nombre as estado_aseguramiento_nombre",
            "eo.color as color_ofsc",
            "eo.color_letra",
            "mo.estado_ofsc as tipodevolucion",
            "mo.descripcion as motivo_ofsc_descripcion",
            \DB::raw("IFNULL(at.nombre, '') as actividad_tipo_nombre"),
            \DB::raw("IFNULL(p.nombre, '') as parametro"),
            \DB::raw("IFNULL(p.tipo, '') as parametro_tipo"),
            \DB::raw("'' as workzone")
        )->getUltimoGestelAveria()
        ->getMotivoofsc()
        ->getEstadoofsc()
        ->getQuiebre()
        ->GetActividades()
        ->getActividadTipo()
        ->getBucket()
        ->getEstadoaseguramiento()
        ->getParametro();

        if ($descargar) {
            $solicitudesCms->getMovimientos()->getMovimientosDetalle()->groupBy('u.id');
            $solicitudesGestelProvision->getMovimientos()->getMovimientosDetalle()->groupBy('u.id');
            $solicitudesGestelAveria->getMovimientos()->getMovimientosDetalle()->groupBy('u.id');
        }

        $solicitudbody = [
            "cms" => $solicitudesCms,
            "gestelprovision" => $solicitudesGestelProvision,
            "gestelaveria" => $solicitudesGestelAveria];
        return $solicitudbody;
    }
    public function busquedaIndividual($valor = "", $campo = "")
    {
        $bodyBusqueda = $this->bodyBusqueda();
        $solicitudesCms = null;
        $solicitudesGestelProvision = null;
        $sollicitudesGestelAveria = null;

        /*if(\Auth::user()->perfil_id==13) { //gestor devueltas
            $motivousuario=\DB::table('usuario_motivo_submotivo_ofsc')
                    ->select(
                        \DB::RAW('GROUP_CONCAT(DISTINCT motivo_ofsc_id) as motivos'),
                        \DB::RAW('GROUP_CONCAT(DISTINCT submotivo_ofsc_id) as submotivos')
                        )
                    ->where('usuario_id',\Auth::id())
                    ->first();
        }*/
        foreach ($bodyBusqueda as $key => $value) {

            if (!is_null($value)) {
                if (isset($campo[$key]) && !is_null($campo[$key])) {
                     $value->where($campo[$key], $valor);
                }
                if ($key == "cms") {
                    $solicitudesCms = $value;
                }
                if ($key == "gestelprovision") {
                    $solicitudesGestelProvision = $value;
                }
                if ($key == "gestelaveria") {
                    $sollicitudesGestelAveria = $value;
                }
                if(isset($motivousuario)) {
                    if($motivousuario->submotivos!==null) {
                        
                        $submotivo=array_merge(explode(",", $motivousuario->submotivos), [null]);
                        $value->Where(
                            function ($query) use ($key, $submotivo) {
                                foreach ($submotivo as $valor) {
                                    $query->orWhere('u.submotivo_ofsc_id', "=", $valor);
                                }
                            }
                        );
                    }
                    if($motivousuario->motivos!==null){
                       $motivo=array_merge(explode(",", $motivousuario->motivos), [null]);
                        $value->Where(
                            function ($query) use ($key, $motivo) {
                                foreach ($motivo as $valor) {
                                    $query->orWhere('u.motivo_ofsc_id', "=", $valor);
                                }
                            }
                        ); 
                    }
                }
                

                if(\Session::get("perfilId") == 13 || \Session::get("perfilId") == 14 || \Session::get("perfilId") == 15) {

                    $value->whereIn('estado_aseguramiento',[3,5]);
                    $value->where("u.actividad_id", "=", 2);

                    if (\Session::get("perfilId")==13 ) {
                        $value->where("mo.estado_ofsc", 'C');
                    }
                    $posiblesmotivos = [103, 467, 115];
                    if (\Session::get("perfilId") == 14 ) {
                        $value->where("mo.estado_ofsc", 'T')
                            ->whereNotIn("u.motivo_ofsc_id", $posiblesmotivos);
                    }
                    if (\Session::get("perfilId") == 15) {
                        $value->where("mo.estado_ofsc", 'T')
                            ->whereIn("u.motivo_ofsc_id", $posiblesmotivos);
                    }
                    $solicitudesredis = \Redis::lrange("solicitudes", 0, 1000);
                    $ignoradas = [];
                    foreach ($solicitudesredis as $key2 => $value2) {
                        $solicituddata = json_decode(\Redis::get($value2), true);
                        if (count($solicituddata) > 0) {
                            if ($solicituddata["idusuario"]!=\Auth::id()) {
                                $ignoradas[] = "{$value2}";
                            }
                        }
                    }
                    if (count($ignoradas) > 0) {
                        $value->whereNotIn("id_solicitud_tecnica", $ignoradas);
                    }
                }

                if(\Session::get("perfilId") == 16 ) {
                    $value->where("u.actividad_id", "=", 2);
                }
            }
        }

        $solicitudes = $solicitudesCms->union($solicitudesGestelProvision->getQuery());
        if (!is_null($sollicitudesGestelAveria)) {
            $solicitudes->union($sollicitudesGestelAveria->getQuery());
        }

        if(\Session::get("perfilId") == 13 || \Session::get("perfilId") == 14 || \Session::get("perfilId") == 15) {
            $solicitudes->orderBy("estado_aseguramiento", "ASC")->orderBy("updated_at", "ASC");

        } else {
            $solicitudes->orderBy("solicitud_tecnica_id", "DESC");
        }
        
        
        return $solicitudes->get();
        
    }

    public function busquedaGeneral($filtros, $grilla, $descargar = false)
    {
        $where = [];
        $whereIn = [];
        $whereNotIn = [];
        $whereBetween = [];
        $bodyBusqueda = $this->bodyBusqueda($descargar);
        $solicitudesCms = (isset($bodyBusqueda["cms"])) ? $bodyBusqueda["cms"] : null;
        $solicitudesGestelProvision = (isset($bodyBusqueda["gestelprovision"])) ? $bodyBusqueda["gestelprovision"] : null;
        $solicitudesGestelAveria = (isset($bodyBusqueda["gestelaveria"])) ? $bodyBusqueda["gestelaveria"] : null;

        if (\Session::get("perfilId")==13 ||
            \Session::get("perfilId")==14 ||
            \Session::get("perfilId") == 15) {
            $filtros["arreglosEqual"]["u.estado_ofsc_id"] = [2,4];
            $filtros["arreglosEqual"]["u.estado_aseguramiento"] = [3, 5];
            $filtros["arreglosEqual"]["u.actividad_id"] = [2];
            if (\Session::get("perfilId") == 13) {
                $filtros["arreglosEqual"]["mo.estado_ofsc"] = ["C"];
            }
            if (\Session::get("perfilId") == 14 || \Session::get("perfilId") == 15) {
                $filtros["arreglosEqual"]["mo.estado_ofsc"] = ["T"];
            }

            if (\Session::get("perfilId") == 14) {
                $filtros["arreglosNotEqual"]["u.motivo_ofsc_id"] =  [103, 467, 115];
            }
            if (\Session::get("perfilId") == 15) {
                if (count($filtros["arreglosEqual"]["u.motivo_ofsc_id"]) <= 0) {
                    $filtros["arreglosEqual"]["u.motivo_ofsc_id"] =  [103, 467, 115];
                }
            }
            $solicitudesredis = \Redis::lrange("solicitudes", 0, 1000);

            $ignoradas = [];
            foreach ($solicitudesredis as $key => $value) {
                $stdata = \Redis::get($value);
                if (!is_null($stdata)) {
                    $solicituddata = json_decode($stdata, true);
                    if (count($solicituddata) > 0) {
                        if ($solicituddata["idusuario"]!=\Auth::id()) {
                            $ignoradas[] = "{$value}";
                        }
                    }
                }
            }
            if (count($ignoradas) > 0) {
                $filtros["arreglosNotEqual"]["id_solicitud_tecnica"] = $ignoradas;
            }
        }
        foreach ($filtros as $key => $value) {
            switch ($key) {
                case 'campos':
                    foreach ($value as $key2 => $value2) {
                        if ($value2!="") {
                            $where[$key2] = $value2;
                        }
                    }
                    break;
                case 'arreglosEqual':
                    foreach ($value as $key2 => $value2) {
                        if (count($value2) > 0) {
                            $whereIn[$key2] = $value2;
                        }
                    }
                    break;
                case 'arreglosNotEqual':
                    foreach ($value as $key2 => $value2) {
                        if (count($value2) > 0) {
                            $whereNotIn[$key2] = $value2;
                        }
                    }
                    break;
                case 'fechas':
                    foreach ($value as $key2 => $value2) {
                        if (count($value2) == 2) {
                            $whereBetween[$key2] = $value2;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        if (count($where) > 0) {
            $solicitudesCms->where($where);
        }
        foreach ($whereIn as $key => $value) {
            if ($key!="cabecera_mdf") {
                $solicitudesCms->where(
                    function ($query) use ($key, $value) {
                        foreach ($value as $valor) {
                            $query->orWhere($key, "=", $valor);
                        }
                    }
                );
            } else {
                $solicitudesCms->havingRaw("$key='$value[0]'");
            }
            if ($key!="cod_nodo" && $key!="cod_troba" && $key!="cabecera_mdf") {
                $solicitudesGestelAveria->Where(
                    function ($query) use ($key, $value) {
                        foreach ($value as $valor) {
                            $query->orWhere($key, "=", $valor);
                        }
                    }
                );
            } else {
                $solicitudesGestelAveria->havingRaw("$key='$value[0]'");
            }

            if ($key!="cod_nodo" && $key!="cod_troba") {
                $solicitudesGestelProvision->Where(
                    function ($query) use ($key, $value) {
                        foreach ($value as $valor) {
                            $query->orWhere($key, "=", $valor);
                        }
                    }
                );
            } else {
                $solicitudesGestelProvision->havingRaw("$key='$value[0]'");
            }
        }

        foreach ($whereNotIn as $key => $value) {
            $solicitudesCms->Where(
                function ($query) use ($key, $value) {
                    $value = (is_array($value)) ? $value : array($value);
                    foreach ($value as $valor) {
                        $query->Where($key, "<>", $valor);
                    }
                }
            );
            $solicitudesGestelAveria->Where(
                function ($query) use ($key, $value) {
                    $value = (is_array($value)) ? $value : array($value);
                    foreach ($value as $valor) {
                        $query->Where($key, "<>", $valor);
                    }
                }
            );
            $solicitudesGestelProvision->Where(
                function ($query) use ($key, $value) {
                    $value = (is_array($value)) ? $value : array($value);
                    foreach ($value as $valor) {
                        $query->Where($key, "<>", $valor);
                    }
                }
            );
        }
        foreach ($whereBetween as $key => $value) {
            list($inicio,$fin) = $value;
            $inicio .=' 00:00:01';
            $fin .=' 23:23:59';
            if ($key=='created_at') {
                $solicitudesCms->whereBetween('solicitud_tecnica_cms.'.$key, [$inicio,$fin]);
                $solicitudesGestelAveria->whereBetween('solicitud_tecnica_gestel_averia.'.$key, [$inicio,$fin]);
                $solicitudesGestelProvision->whereBetween('solicitud_tecnica_gestel_provision.'.$key, [$inicio,$fin]);
            } else {
                $solicitudesCms->whereBetween($key, [$inicio,$fin]);
                $solicitudesGestelAveria->whereBetween($key, [$inicio,$fin]);
                $solicitudesGestelProvision->whereBetween($key, [$inicio,$fin]);
            }
        }
        if (!is_null($solicitudesCms)) {
            $solicitudesCms->whereRaw("u.num_requerimiento IS NOT NULL");
        }

        if (!is_null($solicitudesGestelProvision)) {
            //$solicitudesGestelProvision->whereRaw("u.num_requerimiento IS NOT NULL");
        }
        //motivos ofsc permitidos
        
        $rst=$solicitudesCms
        ->union($solicitudesGestelAveria->getQuery())
        ->union($solicitudesGestelProvision->getQuery());
        if ($descargar) {
            return $solicitudesCms->get();
        }
        $column = "created_at";
        $dir = "desc";
        $solicitudes = \DB::table(\DB::raw('(' . $rst->toSql() . ')  as solicitudes'))->mergeBindings($rst->getQuery());
        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }

        if(\Session::get("perfilId") == 16 ) {
            $solicitudes = $solicitudes->where("actividad_id", "=", 2);
        }

        if (\Session::get("perfilId")==13 ||
            \Session::get("perfilId")==14 ||
            \Session::get("perfilId") == 15) {
            $solicitudes = $solicitudes->orderBy("estado_aseguramiento", "ASC")
                ->orderBy("updated_at", "ASC");
        } else {
            $solicitudes = $solicitudes->orderBy($column, $dir);
        }

        if(\Input::has('asignado')){     
            $solicitudes = $solicitudes->paginate(1);              
        }else{
            $solicitudes = $solicitudes->paginate($grilla["perPage"]);            
        }

        $data = $solicitudes->toArray();

        $col = new Collection([
            'recordsTotal'=> \Input::has('asignado') ? 1 : $solicitudes->getTotal(),
            'recordsFiltered'=> \Input::has('asignado') ? 1 :$solicitudes->getTotal(),
        ]);

        return $col->merge($data);
    }

    public function getSelectSolicitudCms($solicitudTecnicaId = 0)
    {
        
        $solicitud = StCms::select(
            "solicitud_tecnica_cms.*",
            \DB::raw(" '' AS cabecera_mdf"),
            \DB::raw("IFNULL(u.aid,'') as aid"),
            \DB::raw("IFNULL(u.xa_note,'') as xa_note"),
            \DB::raw("IFNULL(u.xa_observation,'') as xa_observation"),
            \DB::raw("IFNULL(solicitud_tecnica_cms.usuario_updated_at, '') as usuario_updated_at"),
            \DB::raw("IFNULL(u.a_receive_person_id, '') as a_receive_person_id"),
            \DB::raw("IFNULL(u.a_receive_person_name, '') as a_receive_person_name"),
            \DB::raw("IFNULL(u.estado_ofsc_id,'') as estado_ofsc_id"),
            \DB::raw("IFNULL(u.estado_st,'') as estado_st"),
            \DB::raw("IFNULL(u.actividad_id,'') as actividad_id"),
            \DB::raw("IFNULL(u.actividad_tipo_id,'') as actividad_tipo_id"),
            \DB::raw("IFNULL(u.quiebre_id,'') as quiebre_id"),
            \DB::raw("IFNULL(u.tipo_legado,'') as tipo_legado"),
            \DB::raw("IFNULL(u.fecha_agenda,'') as fecha_agenda"),
            \DB::raw("IFNULL(u.estado_aseguramiento,'') as estado_aseguramiento"),
            \DB::raw("IFNULL(u.solicitud_tecnica_id,'') as solicitud_tecnica_id"),
            \DB::raw("IFNULL(u.resource_id,'') as resource_id"),
            \DB::raw("IFNULL(u.contador_error_liquidate,'') as contador_error_liquidate"),
            \DB::raw("IFNULL(u.motivo_ofsc_id,'') as motivo_ofsc_id"),
            \DB::raw("IFNULL(u.submotivo_ofsc_id,'') as submotivo_ofsc_id"),
            \DB::raw("IFNULL(u.tel1,'') as tel1"),
            \DB::raw("IFNULL(u.tel2,'') as tel2"),
            \DB::raw("IFNULL(u.contacto,'') as contacto"),
            \DB::raw("IFNULL(u.direccion,'') as direccion"),
            \DB::raw("IFNULL(u.freeview,'') as freeview"),
            \DB::raw("IFNULL(u.descuento,'') as descuento"),
            \DB::raw("IFNULL(eo.nombre,'') as estado_ofsc"),
            \DB::raw("IFNULL(eo.name,'') as estado_ofsc_etiqueta"),
            \DB::raw("IFNULL(eo.color,'') as color_ofsc"),
            \DB::raw("IFNULL(eo.color_letra,'') as color_letra"),
            \DB::raw("IFNULL(t.carnet,'') as carnettecnico"),
            \DB::raw("IFNULL(t.nombre_tecnico,'') as nombretecnico"),
            \DB::raw("IFNULL(t.celular,'') as celulartecnico"),
            \DB::raw("IFNULL(t.id,'') as tecnico_id"),
            \DB::raw("IFNULL(mo.descripcion,'') as motivoofsc"),
            \DB::raw("IFNULL(so.descripcion,'') as submotivoofsc"),
            \DB::raw("IFNULL(mo.estado_ofsc,'') as tipodevolucion"),
            \DB::raw("IFNULL(q.nombre,'') as quiebre "),
            \DB::raw(" 'cms' as nombrelegado"),
            \DB::raw("min(sm.created_at) as fechaenviotoa")
        )->getUltimoCms()
        ->getQuiebre()
        ->getEstadoofsc()
        ->getTecnico()
        ->getMotivoofsc()
        ->getSubmotivoofsc()
        ->getFechaEnvioToa()
        ->where(
            'u.solicitud_tecnica_id',
            '=',
            $solicitudTecnicaId
        )->first();

        return $solicitud;
    }

    public function getSelectSolicitudGestelProvision($solicitudTecnicaId = 0)
    {
        
        $solicitud = StGestelP::with('direccioncliente')
            ->with('dominiogestel')
            ->with('productosadquiridos')
            ->with('fftt')
            ->with('ffttcobre')
            ->with('ffttadsl')
            ->with('ffttpin')
            ->select(
                "solicitud_tecnica_gestel_provision.*",
                "solicitud_tecnica_gestel_provision.peticion as num_requerimiento",
                \DB::raw(" '' AS cabecera_mdf"),
                "u.aid",
                "u.estado_ofsc_id",
                "u.estado_st",
                "u.actividad_id",
                "u.actividad_tipo_id",
                "u.quiebre_id",
                "u.tipo_legado",
                "u.fecha_agenda",
                "u.estado_aseguramiento",
                "u.solicitud_tecnica_id",
                "u.resource_id",
                "u.contador_error_liquidate",
                "u.motivo_ofsc_id",
                "u.tel1",
                "u.tel2",
                "u.contacto",
                "u.direccion",
                "u.freeview",
                "u.descuento",
                "eo.nombre as estado_ofsc",
                "eo.name as estado_ofsc_etiqueta",
                "eo.color as color_ofsc",
                "t.carnet as carnettecnico",
                "t.nombre_tecnico as nombretecnico",
                "t.celular as celulartecnico",
                "t.id as tecnico_id",
                "mo.descripcion as motivoofsc",
                "so.descripcion as submotivoofsc",
                "mo.estado_ofsc as tipodevolucion",
                "q.nombre as quiebre",
                \DB::raw(" '' AS cod_nodo"),
                \DB::raw(" '' AS empresa_id"),
                \DB::raw(" 'gestel' as nombrelegado")
            )->getUltimoGestelProvision()
        ->getQuiebre()
        ->getEstadoofsc()
        ->getTecnico()
        ->getMotivoofsc()
        ->getSubmotivoofsc()
        ->where(
            'solicitud_tecnica_gestel_provision.solicitud_tecnica_id',
            $solicitudTecnicaId
        )->first();

        return $solicitud;
    }

    public function getSelectSolicitudGestelAveria($solicitudTecnicaId = 0)
    {
        $solicitud = StGestelA::with('dominiogestel')
            ->with('productosadquiridos')
            ->select(
                "solicitud_tecnica_gestel_averia.*",
                "solicitud_tecnica_gestel_averia.id_solicitud_tecnica",
                \DB::raw(" '' AS orden_trabajo"),
                "solicitud_tecnica_gestel_averia.created_at",
                \DB::raw(" '' AS cod_troba"),
                "solicitud_tecnica_gestel_averia.zonal",
                "solicitud_tecnica_gestel_averia.tipo_operacion",
                \DB::raw(" '' AS cod_nodo"),
                "u.num_requerimiento",
                "u.estado_ofsc_id",
                "u.estado_st",
                "u.actividad_id",
                "u.actividad_tipo_id",
                "u.quiebre_id",
                "u.tipo_legado",
                "u.fecha_agenda",
                "u.estado_aseguramiento",
                "u.solicitud_tecnica_id",
                "u.tel1",
                "u.tel2",
                "u.contacto",
                "u.direccion",
                "u.freeview",
                "u.descuento",
                "eo.nombre as estado_ofsc",
                "eo.name as estado_ofsc_etiqueta",
                "eo.color as color_ofsc",
                "mo.estado_ofsc as tipodevolucion",
                \DB::raw(" 'gestel' as nombrelegado")
            )->getUltimoGestelAveria()
        ->getQuiebre()
        ->getEstadoofsc()
        ->getTecnico()
        ->getMotivoofsc()
        ->getSubmotivoofsc()
        ->where(
            'solicitud_tecnica_gestel_averia.solicitud_tecnica_id',
            $solicitudTecnicaId
        )->first();

        return $solicitud;
    }

    public function devolucionOfsc($datos = []) 
    {
        $objinbound = new Inbound;
        $solicitudTecnica = SolicitudTecnica::find($datos["solicitud_tecnica_id"]);
        $ultimo = $solicitudTecnica->ultimo;
        $tecnico = $ultimo->tecnico;
        $celular = isset($tecnico->celular) ? $tecnico->celular : "";

        $tipo = $datos["tipo_motivo"] == "C" ? 3 : 4;

        $properties = [];
        $properties["A_OBSERVATION"] = $datos["observacion_gestion"];
        $properties["A_IND_PRE_CIERRE_DEV"] = 2;

        if ($ultimo->actividad_id == 1) {
            $properties["A_NOT_DONE_AREA"] = $datos["codigo_ofsc"];
        } elseif ($ultimo->actividad_id == 2) {
            $properties["A_NOT_DONE_REASON_INSTALL"] = $datos["codigo_ofsc"];
        }

        $actuacion = [
            'xa_observation'    => $ultimo->xa_observation." | ".$datos["observacion_gestion"],
            'tel1'              => $datos["telefono1_contacto"],
            'tel2'              => $datos["telefono2_contacto"],
            'contacto'          => $datos["persona_contacto"],
            'direccion'         => $datos["direccion_contacto"],
            'freeview'          => $datos["freeview"],
            'descuento'         => $datos["descuento"],
        ];

        $responseToa = $objinbound->updateActivity(
            $ultimo->num_requerimiento, $properties, [], "notdone_activity"
        );

        $actuacion['motivo_ofsc_id'] = $datos["motivo_ofsc_id"];
        $actuacion['submotivo_ofsc_id'] = $datos["submotivo_ofsc_id"];
        if (isset($responseToa->result)
            && $responseToa->result == "success"
            && $responseToa->code == 0) {
            $actuacion['estado_ofsc_id'] = 4;
            $actuacion['estado_aseguramiento'] = 6;
            $response["rst"] = 1;
            $response["msj"] = "";
            $response["error"] = "";
        } else {
            $errorMsj = isset($responseToa->errorMsg) ?
                        $responseToa->errorMsg : $responseToa->description ? 
                        $responseToa->description : 'error desconocido en TOA';

            $actuacion['xa_observation'] .= " | ".$errorMsj;
            $response["rst"] = 2;
            $response["msj"] = $errorMsj;
            $response["error"] = isset($responsetoa->code) ? $responsetoa->code : 'Error no Catalogado de TOA';
        }

        foreach (Ultimo::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimo[$key] = $actuacion[$key];
            }
        }
        $solicitudTecnica->ultimo()->save($ultimo);

        $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
        foreach (Movimiento::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimoMovimiento[$key] = $actuacion[$key];
            }
        }
        $ultimoMovimiento->save();

        if (count($datos["tematicos"]) > 0) {
            $data = [
                "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                "comentarios" => $datos["observacion_gestion"],
                "tipo" => $tipo
            ];
            $save = $this->generarMovimientoconTematico(
                $datos["tematicos"],
                [],
                $data
            );
        }

        if ($celular != "" && $response["rst"] == 1) {
            $sms = "Req. ".$ultimo->num_requerimiento.", Devolución Aceptada, puede Continuar con la Siguiente Orden";
            \Sms::enviarSincrono($celular, $sms, '1');
        }

        return $response;
    }

    public function liquidacionOfsc($datos = []) 
    {
        $objinbound = new Inbound;
        $solicitudTecnica = SolicitudTecnica::find($datos["solicitud_tecnica_id"]);
        $ultimo = $solicitudTecnica->ultimo;
        $tecnico = $ultimo->tecnico;
        $celular = isset($tecnico->celular) ? $tecnico->celular : "";
        
        $properties = [];
        $properties["A_OBSERVATION"] = $datos["observacion_gestion"];
        $properties["A_IND_PRE_CIERRE_COM"] = 2;
        if ($ultimo->actividad_id == 1) { // Averia
            if ($datos["tipo_tecnologia"] == 1) {// CATV
                $properties["A_COMPLETE_CAUSA_REP_CBL"] = $datos["codigo_ofsc"];
            } elseif ($datos["tipo_tecnologia"] == 2) {// SPEEDY
                $properties["A_COMPLETE_CAUSA_REP_ADSL"] = $datos["codigo_ofsc"];
            } elseif ($datos["tipo_tecnologia"] == 3) {// BASICA
                $properties["A_COMPLETE_CAUSA_REP_STB"] = $datos["codigo_ofsc"];
            }
        } elseif ($ultimo->actividad_id == 2) {
            $properties["A_COMPLETE_REASON_INSTALL"] = $datos["codigo_ofsc"];
        }

        $actuacion['xa_observation'] = $ultimo->xa_observation." | ".$datos["observacion_gestion"];
        $responseToa = $objinbound->updateActivity($ultimo->num_requerimiento, $properties, [], "complete_activity");
        if (isset($responseToa->result) && $responseToa->result == "success") {
            $actuacion['estado_ofsc_id'] = 6;
            $actuacion['estado_aseguramiento'] = 6;
            $response["rst"] = 1;
            $response["msj"] = "";
            $response["error"] = "";
        } else {
            $errorMsj = isset($responseToa->errorMsg) ?
                        $responseToa->errorMsg : $responseToa->description ? 
                        $responseToa->description : 'error desconocido en TOA';
            $actuacion['xa_observation'] .= " | ".$errorMsj;
            $response["rst"] = 2;
            $response["msj"] = $errorMsj;
            $response["error"] = isset($responseToa->code) ? $responseToa->code : 'Error no Catalogado de TOA';
        }

        foreach (Ultimo::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimo[$key] = $actuacion[$key];
            }
        }
        $solicitudTecnica->ultimo()->save($ultimo);

        $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
        foreach (Movimiento::$rules as $key => $value) {
            if (array_key_exists($key, $actuacion)) {
                $ultimoMovimiento[$key] = $actuacion[$key];
            }
        }
        $ultimoMovimiento->save();

        if (count($datos["tematicos"]) > 0) {
            $data = [
                "solicitud_tecnica_id" => $ultimo->solicitud_tecnica_id,
                "comentarios" => $datos["observacion_gestion"],
                "tipo" => 6
            ];
            $save = $this->generarMovimientoconTematico(
                $datos["tematicos"],
                [],
                $data
            );
        }

        if ($celular != "" && $response["rst"] == 1) {
            $sms = "Req. ".$ultimo->num_requerimiento.", Cierre Aceptada, puede Continuar con la Siguiente Orden";
            \Sms::enviarSincrono($celular, $sms, '1');
        }

        return $response;
    }
}
