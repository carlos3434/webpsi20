<?php namespace Legados\models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ComponenteCms extends \Eloquent
{

    use SoftDeletingTrait;

    protected $table = "componente_cms";

    protected $dates = ['deleted_at'];

    protected $guarded =[];
    public static $rules = [
        'solicitud_tecnica_cms_id' => 'between:1,11',
        'numero_requerimiento' => 'required|numeric|max:999999999999999',
        'numero_ot' => 'required|numeric|max:999999999999999',
        'numero_servicio' => 'numeric|max:999999999999999',
        'componente_cod' => 'numeric|max:999999999999999',
        //'descripcion' => 'regex:/^([1-9a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,100})$/i',
        'codigo_material' => 'alpha_num|max:8',
        'tipo_adquisicion' => 'alpha_num|max:1',
        'tipo_procedencia' => 'alpha_num|max:5',
        'marca' => 'regex:/^([1-9a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,50})$/i',
        'modelo' => 'regex:/^([1-9a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,50})$/i',
        'componente_tipo' => 'alpha_num|max:5',
        'numero_serie' => 'alpha_num|max:32',
        'casid' => 'alpha_num|max:20',
        'estado' => 'alpha_num|max:1',
    ];


    const NONE = 0;
    const OK = 1;
    const FAIL = 2;

    public static function boot()
    {
        parent::boot();

        static::updating(function ($table)
        {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function ($table)
        {
            $table->usuario_created_at = \Auth::id();
        });
    }
    public function solicitudTecnicaCms()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnicaCms');
    }
}
