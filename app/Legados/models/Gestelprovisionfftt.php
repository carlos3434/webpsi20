<?php 

namespace Legados\models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Gestelprovisionfftt extends \Eloquent
{

    use SoftDeletingTrait;

    protected $table = "gestel_provision_fftt";

    protected $dates = ['deleted_at'];

    protected $guarded =[];

    public static function boot()
    {
        parent::boot();

        static::updating(function ($table)
        {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function ($table)
        {
            $table->usuario_created_at = \Auth::id();
        });
    }
    public function solicitudTecnicaGestelProvision()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnicaGestelProvision');
    }
}
