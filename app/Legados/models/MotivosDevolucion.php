<?php

namespace Legados\models;

class MotivosDevolucion extends \Eloquent
{

    public $table = "motivos_devolucion";

    protected $fillable = [
        'motivo_ofsc_id',
        'codigo_motivo',
        'descripcion_motivo',
        'estado',
        'tipo_legado'
    ];
}
