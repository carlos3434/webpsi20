<?php namespace Legados\models;

class ActividadTecnicoHistorico extends \Eloquent
{
    protected $guarded =[];
    //protected $fillable = ['aid'];
    protected $table = "actividad_tecnico_historico";

    
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
