<?php namespace Legados\models;

class SolicitudTecnicaGestelProvisionFFTTCobre extends \Eloquent
{
    
    protected $table = "solicitud_tecnica_gestel_provision_ffttcobre";


    protected $guarded =[];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
