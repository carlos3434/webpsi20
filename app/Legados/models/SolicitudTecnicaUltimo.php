<?php namespace Legados\models;

class SolicitudTecnicaUltimo extends \Eloquent
{
    protected $guarded =[];
    //protected $fillable = ['aid'];
    protected $table = "solicitud_tecnica_ultimo";

    public static $rules = [
        'aid'                               => 'required',
        'bucket_id'                         => 'required',
        'num_requerimiento'                 => 'required',
        'num_orden_trabajo'                 => 'required',
        'num_orden_servicio'                => 'required',
        'xa_note'                           => 'required',
        'xa_observation'                    => 'required',
        'a_receive_person_id'               => 'required',
        'a_receive_person_name'             => 'required',
        'start_time'                        => 'required',
        'end_time'                          => 'required',
        'end_date'                          => 'required',
        'resource_id'                       => 'required',
        'tipo_envio_ofsc'                   => 'required',
        'coordx_tecnico'                    => 'required',
        'estado_aseguramiento'              => 'required',
        'coordy_tecnico'                    => 'required',
        'liquidate'                         => 'required',
        'contador_error_liquidate'          => 'required',
        'estado_legado'                     => 'required',
        'estado_st'                         => 'required',
        'solicitud_tecnica_id'              => 'required',
        'estado_ofsc_id'                    => 'required',
        'motivo_ofsc_id'                    => 'required',
        'submotivo_ofsc_id'                 => 'required',
        'actividad_id'                      => 'required',
        'actividad_tipo_id'                 => 'required',
        'quiebre_id'                        => 'required',
        'tipo_legado'                       => 'required',
        'nenvio'                            => 'required',
        'horario_id'                        => 'required',
        'dia_id'                            => 'required',
        'fecha_agenda'                      => 'required',
        'tecnico_id'                        => 'required',
        'sistema_externo_id'                => 'required',
        'time_of_assignment'                => 'required',
        'time_of_booking'                   => 'required',
        'duration'                          => 'required',
        'intervalo'                         => 'required',
        'delivery_windows_start'            => 'required',
        'delivery_windows_end'              => 'required',
        'tel1'                              => 'required',
        'tel2'                              => 'required',
        'contacto'                          => 'required',
        'direccion'                         => 'required',
        'freeview'                          => 'required',
        'descuento'                         => 'required',
        'parametro_id'                      => 'required',
        'tipo_liquidacion'                  => ''
        //'tipo_autorizacion_id'              => '',
        //'usuario_autorizacion'              => '',
        //'fecha_autorizacion'                => ''

    ];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });

        static::created(
            function ($table) {
                if ($table->estado_ofsc_id == 1) {
                    $psi = \PsiInformacion::find(1);
                    $psi->fecha_psi_toa = date('Y-m-d H:i:s');
                    $psi->save();
                }
            }
        );

        static::updated(
            function ($table) {
                if ($table->estado_ofsc_id == 4 || $table->estado_ofsc_id == 6) {
                    $psi = \PsiInformacion::find(1);
                    $psi->fecha_toa_psi = date('Y-m-d H:i:s');
                    $psi->save();
                }
            }
        );
    }
    /**
     * relationship to SolicitudTecnica
     */
    public function solicitudTecnica()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnica');
    }

    public function tecnico()
    {
        return $this->belongsTo('Tecnico');
    }

    public function motivoOfsc()
    {
        return $this->belongsTo('MotivoOfsc');
    }

    public function submotivoOfsc()
    {
        return $this->belongsTo('SubmotivoOfsc');
    }

    public function bucket()
    {
        return $this->belongsTo('Bucket');
    }

    public function scopeGetCms($query)
    {
        return $query->join(
            'solicitud_tecnica_cms as cms',
            'cms.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica_ultimo.solicitud_tecnica_id'
        );
    }
    public function scopePendienteWebAseguramiento($query)
    {
        return $query->where('estado_aseguramiento', 3);
    }
    public function scopeOkWebAseguramiento($query)
    {
        return $query->where('estado_aseguramiento', 1);
    }
    public function scopeErrorWebAseguramiento($query)
    {
        return $query->where('estado_aseguramiento', 2);
    }
    public function scopeSinErrorTrama($query)
    {
        return $query->where('estado_st', '<>', 0);
    }
    /**
     * relationship to Quiebre
     */
    public function quiebre()
    {
        return $this->hasOne('\Quiebre');
    }
    /**
     * relationship to estadoOFSC
     */
    public function estadoOfsc()
    {
        return $this->belongsTo('\EstadoOfsc');
    }
}
