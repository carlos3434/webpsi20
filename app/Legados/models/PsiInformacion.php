<?php

namespace Legados\models;

class PsiInformacion extends \Eloquent
{

    public $table = "psi_informacion2";

    protected $fillable = [
        'chart',
        'solicitud_ok',
        'solicitud_no_procesada',
        'solicitud_error_trama',
        'solicitud_error_trama',
        'created_at',
        'updated_at'
    ];
}
