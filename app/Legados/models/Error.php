<?php

namespace Legados\models;

class Error extends \Eloquent
{

    public $table = "lego_errores";

    protected $fillable = [
        'code', 'message', 'id'
        , 'usuario_created_at', 'usuario_updated_at'
        , 'usuario_deleted_at'
    ]; 

    public static $rules = [
        'code' => 'alpha_num|max:90',
        'message' => 'max:240',
        'usuario_created_at' => 'numeric',
        'usuario_updated_at' => 'numeric',
        'usuario_deleted_at' => 'numeric',
    ];

}
