<?php

namespace Legados\models;

class EnvioLegado extends \Eloquent
{
    use \DataViewer;
    public $table = "envio_legado";

    protected $fillable = [
        'solicitud_tecnica_id', 'accion',
        'request', 'response','code_error','descripcion_error',
        'usuario_created_at', 'usuario_updated_at'
    ];

    protected $appends = [
        'xml_request',
        'xml_response'
    ];
    public $userAuth = 697;

    public static function boot() {
        parent::boot();

        static::updating(function($table) {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function($table) {
            $table->usuario_created_at = \Auth::id();
        });

        static::created(function($trama) {
            if ($trama->accion == 'RptaEnvioSolicitudTecnica') {
                $psi = \PsiInformacion::find(1);
                $psi->fecha_psi_cms = date('Y-m-d H:i:s');
                $psi->save();               
            } else if ($trama->accion == 'CierreSolicitudTecnica') {
                $psi = \PsiInformacion::find(1);
                $psi->fecha_psi_cierre_cms = date('Y-m-d H:i:s');
                $psi->fecha_cms_cierre_psi = date('Y-m-d H:i:s');
                $psi->save();
            }          
        });
    }
    public function getXmlRequestAttribute()
    {
        $xmlrequest = \Array2XML::createXML('psi', json_decode($this->request, true));
        return $xmlrequest->saveXML();
    }
    public function getXmlResponseAttribute()
    {
        $xmlresponse = \Array2XML::createXML('psi', json_decode($this->response, true));
        return $xmlresponse->saveXML();
    }
    public function usuarioCreated()
    {
        return $this->belongsTo('Usuario','usuario_created_at');
    }
}