<?php

namespace Legados\models;

class MotivosLiquidacion extends \Eloquent
{

    public $table = "motivos_liquidacion";

    protected $fillable = [
        'motivo_ofsc_id',
        'codigo_liquidacion',
        'descripcion_liquidacion',
        'estado',
        'actividad_id'
    ];
}
