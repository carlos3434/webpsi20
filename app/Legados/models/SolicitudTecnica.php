<?php namespace Legados\models;
/**
 * 
 */
use Toolbox\ToolboxApi;
class SolicitudTecnica extends \Eloquent
{
    
    protected $table = "solicitud_tecnica";


    protected $guarded =[];
    public static function boot() {
        parent::boot();

        static::updating(function($table) {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function cms()
    {
        return $this->hasOne('Legados\models\SolicitudTecnicaCms');
    }
    public function gestelAveria()
    {
        return $this->hasOne('Legados\models\SolicitudTecnicaGestelAveria');
    }
    public function gestelProvision()
    {
        return $this->hasOne('Legados\models\SolicitudTecnicaGestelProvision');
    }

    public function ultimoMovimiento()
    {
        return $this->hasOne('Legados\models\SolicitudTecnicaMovimiento')->latest();
    }
    public function movimientos()
    {
        return $this->hasMany('Legados\models\SolicitudTecnicaMovimiento');
    }
    public function ultimo()
    {
        return $this->hasOne('Legados\models\SolicitudTecnicaUltimo');
    }
    public function log()
    {
        return $this->hasMany('Legados\models\SolicitudTecnicaLogRecepcion');
    }

    /**
     * Scope para obtener  movimiento.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetMovimientos($query)
    {
        return $query->join(
            'solicitud_tecnica_movimiento as m',
            'm.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica.id'
        );
    }
    /**
     * Scope para obtener ultimo.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetUltimo($query)
    {
        return $query->join(
            'solicitud_tecnica_ultimo as u',
            'u.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica.id'
        );
    }

    public function envioToolbox($tipoGestion)
    {
        $elementos = $this->setTramaEnvioToolbox($tipoGestion);
        $this->enviarToolbox($elementos);
    }

    public function setTramaEnvioToolbox($tipoGestion)
    {
        $solicitud = $this;
        $ultimo = $this->ultimo;
        $logRecepcion = $this->log()->orderBy('solicitud_tecnica_log_recepcion.id', 'desc')->first();

        $datetime_ini[0] = $datetime_ini[1] = null;
        if (!is_null($ultimo->start_time)) {
            $datetime_ini = explode(" ", $ultimo->start_time);
        }

        $xmlString = "";
        if (isset($logRecepcion->trama)) {
            $xml = \Array2XML::createXML('GO_ST', json_decode($logRecepcion->trama, true));
            $xmlString = $xml->saveXML();
            $xmlString = "<![CDATA[".str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $xmlString)."]]>";
        }

        $elementos = [
            'solicitud_tecnica_id'      => $ultimo->solicitud_tecnica_id,
            'tipo_gestion'              => $tipoGestion,
            'solicitud_tecnica'         => $solicitud->id_solicitud_tecnica,
            'num_requerimiento'         => $ultimo->num_requerimiento,
            'usuario'                   => 'toolbox',
            'tipo_motivo'               => isset($ultimo->motivoOfsc->estado_ofsc) ? $ultimo->motivoOfsc->estado_ofsc : NULL,
            'motivo_ofsc'               => isset($ultimo->motivoOfsc->codigo_ofsc) ? $ultimo->motivoOfsc->codigo_ofsc : NULL,
            'submotivo_ofsc'            => isset($ultimo->submotivoOfsc->codigo_ofsc) ? $ultimo->submotivoOfsc->codigo_ofsc : NULL,
            'codigo_tecnico'            => isset($ultimo->tecnico->carnet) ? $ultimo->tecnico->carnet : NULL,
            'desc_tecnico'              => isset($ultimo->tecnico->nombre_tecnico) ? $ultimo->tecnico->nombre_tecnico : NULL,
            'bucket'                    => isset($ultimo->bucket->bucket_ofsc) ? $ultimo->bucket->bucket_ofsc : NULL,
            'fecha_envio_go'            => date('d/m/Y'),
            'hora_envio_go'             => date('H:i:s'),
            'fecha_inicio_actividad'    => \Helpers::formatoFecha($datetime_ini[0]),
            'hora_inicio_actividad'     => $datetime_ini[1],
            'fecha_fin_actividad'       => \Helpers::formatoFecha($datetime_ini[0]),
            'hora_fin_actividad'        => $ultimo->end_time,
            'observacion_toa'           => \Helpers::swapString($ultimo->xa_observation),
            'imagenes'                  => [],
            'data_solicitud_tecnica'    => $xmlString
        ];
        return $elementos;
    }

    public function enviarToolbox($elementos)
    {
        print_r($elementos);
        $toolboxApi = new ToolboxApi();
        $toolboxApi->envio($elementos);
    }
}