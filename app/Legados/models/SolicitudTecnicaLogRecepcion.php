<?php namespace Legados\models;

class SolicitudTecnicaLogRecepcion extends \Eloquent
{
    protected $table = "solicitud_tecnica_log_recepcion";
    protected $guarded =[];
    public static function boot()
    {
        parent::boot();

        static::saving(function ($table) {
            if (\Auth::id()) {
                $table->usuario_created_at = \Auth::id();
            }
        });
        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });

        static::created(function($user) {
            $psi = \PsiInformacion::find(1);
            $psi->fecha_cms_psi = date('Y-m-d H:i:s');
            $psi->save();
        });
    }
}