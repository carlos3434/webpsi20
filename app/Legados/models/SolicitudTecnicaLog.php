<?php

namespace Legados\models;


class SolicitudTecnicaLog extends \Eloquent
{
    protected $guarded =[];
    protected $table = "solicitud_tecnica_log";

    public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating( function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
