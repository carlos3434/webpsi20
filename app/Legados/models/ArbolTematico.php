<?php
namespace Legados\models;

class ArbolTematico extends \Eloquent
{
    private $_elementos = array();

    public function get(){
        $datosP = \DB::select("select * from tematicos where `is_completado`=1");
        //dd($datosP);
        $this->_elementos["padres"] = $this->_elementos["hijos"] = array();

        if (count($datosP) > 0) {
            foreach ($datosP as $elemento) {
                # code...
                //var_dump($elemento);
                if ($elemento->parent == 0) {
                    array_push($this->_elementos["padres"], $elemento);
                } else {
                    array_push($this->_elementos["hijos"], $elemento);
                }
            }
            //dd($this->_elementos["hijos"]);
        }

        return $this->_elementos;
    }

    // recursividad
    public function bucle($rows, $parent_id = ""){
        //var_dump($rows);
        $rama = array();
        $html = "";

        if (!empty($rows)) {
            $html .= "<ul>";
            $rows2 = json_decode(json_encode($rows), true);
            //var_dump($rows);
            foreach ($rows2 as $key => $row) {
                //var_dump($row["parent"]);
                //var_dump($key);
                //print_r($row[$key]->parent." | ".$parent_id."\n");
                //$key = isset($row[$key]->parent) ? $row[$key]->parent : 0;
                //var_dump($key);
                //if (!is_null($row)) {
                $key = isset($row['parent']) ? $row['parent'] : $row['parent'];
                    if ($key== $parent_id) {
                        //var_dump($row["parent"]);
                        $hijos = self::bucle($rows, $row['id']);

                        $array = array(
                            "id"=>$row['id'],
                            "title"=>$row['nombre'],
                            "status" => $row['estado'],
                            "nodes"=>$hijos
                        );
                        $html .= "<li>";
                        $html .= "<spam>".$row['nombre']."</spam>";
    //                    $html .= self::bucle($rows, $row[$key]->recurso_id);
                        $html .= "</li>";

                        $rama[] = $array;

                        
                    }
                //}
            }
            $html .= "</ul>";
        }

        return $rama;
    }
}