<?php namespace Legados\models;

use Legados\models\ComponenteGestel;
use Ofsc\Capacity;
use Legados\models\SolicitudTecnica;
use Legados\STGestelApi;
use Configuracion\models\Parametro;

class SolicitudTecnicaGestelAveria extends \Eloquent
{
    use \SolicitudTecnicaTrait;
    protected $guarded =[];
    protected $table = "solicitud_tecnica_gestel_averia";

    public static $rules = [
         'tipo_operacion' => 'required|in:ALT_AVE_BLI,ALT_AVE_BPR,ALT_AVE_SPE,ALT_AVE_TUP',
         'id_solicitud_tecnica' => 'required|between:1,11|regex:/^[0-9a-zA-Z]+$/',
         'zonal'=> 'required|between:1,3',
         'ciudad'=> 'between:1,5',
         'inscripcion'=> 'between:0,999999999999.99|numeric',
         'cod_clase'=> 'between:1,3',
         'cod_promocion'=> 'between:1,3',
         'desc_cod_promocion'=> 'between:1,40',
         'cod_cliente'=> 'between:0,9999999999.99|numeric',
         'cuenta'=> 'between:0,9999999999.99|numeric',
         'codigo_pc'=> 'between:0,9999999999.99|numeric',
         'fecha_ini_act_abonado'=> 'between:1,10',
         'cod_averia'=> 'between:1,10',
         'correlativo_llamada'=> 'between:0,9999999999.99|numeric',
         'estado_averia'=> 'between:1,1',
         'desc_estado_averia'=> 'between:1,40',
         'averia_reiterada'=> 'between:0,99999|numeric',
         'cod_defecto'=> 'between:1,3',
         'desc_cod_defecto'=> 'between:1,50',
         'fecha_registro'=> 'between:1,19',
         'observacion_registro'=> 'between:1,40',
         'fecha_vencimiento'=> 'between:1,19',
         'area'=> 'between:1,10',
         'desc_area'=> 'between:1,40',
         'area_destino'=> 'between:1,10',
         'desc_destino'=> 'between:1,40',
         'cod_diagnostico'=> 'between:1,2',
         'desc_diagnostico'=> 'between:1,40',
         'fecha_diagnostico'=> 'between:1,16',
         'observacion_diagnostico'=> 'between:1,40',
         'tipo_reclamo'=> 'between:1,1',
         'desc_tipo_reclamo'=> 'between:1,40',
         'desc_categoria'=> 'between:1,10',
         'nombre_contacto'=> 'between:1,40',
         'telefono_contacto'=> 'between:1,10',
         'telefono_referencia'=> 'between:1,10',
         'cod_zoc'=> 'between:1,2',
         'desc_zoc'=> 'between:1,25',
         'cod_rural'=> 'between:1,1',
         'con_serv_telefonico'=> 'between:1,1',
         'contrata'=> 'between:1,2',
         'desc_contrata'=> 'between:1,20',
         'desc_ult_liquidada'=> 'between:1,50',
         'fecha_ult_reclamo'=> 'between:1,19',
         'centro_ejecucion_ficticio_real'=> 'between:1,2',
         'valor1'=> 'between:1,20',
         'valor2'=> 'between:1,20',
         'valor3'=> 'between:1,20',
         'valor4'=> 'between:1,20',
         'valor5'=> 'between:1,20',
         'cod_plan'=> 'between:1,3',
         'desc_plan'=> 'between:1,40',
         'tipo_plan'=> 'between:1,3',
         'cod_camp'=> 'between:1,4',
         'desc_camp'=> 'between:1,40',
         'desc_tipo_modem'=> 'between:1,30',
         'cod_modalidad'=> 'between:1,2',
         'desc_modalidad'=> 'between:1,30',
         'cod_serv_dslam'=> 'between:1,4',
         'desc_serv_dslam'=> 'between:1,40',
         'tipser'=> 'between:1,3',
         'desc_tipser'=> 'between:1,30',
         'cod_speedy'=> 'between:1,5',
         'cod_anterior_speedy'=> 'between:1,5',
         'velocidad_speedy'=> 'between:1,10',
         'unidad_bajada'=> 'between:1,2',
         'promocion'=> 'between:1,3',
         'desc_promocion'=> 'between:1,40',
         'cod_isp'=> 'between:1,3',
         'desc_isp'=> 'between:1,40',
         'vpi_modem'=> 'between:1,5',
         'vci_modem'=> 'between:1,5',
         'fecha_instalacion_router'=> 'between:1,19',
         'tipo_doc'=> 'between:1,1',
         'desc_tipo_doc'=> 'between:1,30',
         'numero_doc'=> 'between:1,15',
         'nombres'=> 'between:1,30',
         'ape_paterno'=> 'between:1,60',
         'ape_materno'=> 'between:1,30',
         'cuc'=> 'between:1,9',
         'segmento_cliente'=> 'between:1,2',
         'desc_segmento'=> 'between:1,30',
         'subsegmento_cliente'=> 'between:1,2',
         'desc_subsegmento_cliente'=> 'between:1,30',
         'direccion_completa'=> 'between:1,50',
         'sector'=> 'between:1,4',
         'manzana'=> 'between:1,4',
         'ubigeo'=> 'between:1,6',
         'desc_departamento'=> 'between:1,40',
         'desc_provincia'=> 'between:1,40',
         'desc_distrito'=> 'between:1,40',
         'cod_distrito'=> 'between:1,6',
         'datos_ref_direccion'=> 'between:1,80',
         'coordx'=> 'between:1,20',
         'coordy'=> 'between:1,20',
         'tipo_rango'=> 'between:1,2',
         'cod_zonal_tel'=> 'between:1,3',
         'telefono'=> 'between:1,10',
         'estanum'=> 'between:1,2',
         'tecnologia_linea'=> 'between:1,1',
         'descripcion'=> 'between:1,30',
         'tipo_linea'=> 'between:1,5',
         'tipo_tecnologia'=> 'between:1,4',
         'cabecera_conmutacion'=> 'between:1,3',
         'central'=> 'between:1,5',
         'circuito'=> 'between:1,15',
         'ubilic'=> 'between:1,8',
         'tccto'=> 'between:1,2',
         'operadora_portabilidad'=> 'between:1,4',
         'cabecera_mdf'=> 'between:1,20',
         'desc_mdf'=> 'between:1,20',
         'direccion_mdf'=> 'between:1,60',
         'centro_ejecucion'=> 'between:1,2',
         'cod_armario'=> 'between:1,5',
         'direccion_armario'=> 'between:1,70',
         'ubigeo_armario'=> 'between:1,6',
         'cable'=> 'between:1,4',
         'par_alimentador'=> 'between:1,5',
         'borne_alimentador'=> 'between:1,5',
         'estado_par_alimentador'=> 'between:1,2',
         'caja'=> 'between:1,4',
         'direccion_caja'=> 'between:1,30',
         'ubigeo_caja'=> 'between:1,6',
         'coordx_fftt'=> 'between:1,30',
         'coordy_fftt'=> 'between:1,30',
         'sector_fftt'=> 'between:1,4',
         'par_dist_directo'=> 'between:1,5',
         'estado_par_dist_directo'=> 'between:1,2',
         'borne_dist_directo'=> 'between:1,5',
         'par_dedicado'=> 'between:1,5',
         'cabecera_xdsl'=> 'between:1,3',
         'par_xdsl'=> 'between:1,5',
         'estado_par_adsl'=> 'between:1,2',
         'posicion_adsl'=> 'between:1,8',
         'armario_adsl'=> 'between:1,5',
         'id_servicio_voip'=> 'between:1,1',
         'identificador_tid_adsl'=> 'between:1,20',
         'fecha_adsl'=> 'between:1,16',
         'armario_centro_gestion'=> 'between:1,10',
         'shelf_cg'=> 'between:1,10',
         'slot_cg'=> 'between:1,20',
         'puerto_cg'=> 'between:1,10',
         'tipo_dslam'=> 'between:1,2',
         'tipo_tarjeta'=> 'between:1,1',
         'tecnologia_adsl'=> 'between:1,5',
         'nombre_msan'=> 'between:1,8',
         'host_id'=> 'between:1,40',
         'id_msan'=> 'between:1,10',
         'cdslam'=> 'between:1,6',
         'mantenimiento'=> 'between:1,1',
         'llamada_subsecuente'=> 'numeric|between:0,99999',
         'numero_orden_reparacion'=> 'numeric|between:0,999999999',
         'estado_boleta'=> 'between:1,1',
         'cod_causa'=> 'between:1,3',
         'desc_causa'=> 'between:1,40',
         'prioridad'=> 'between:1,1',
         'fuente'=> 'between:1,1',
         'desc_fuente'=> 'between:1,40',
         'observacion_hd'=> 'between:1,120',
         'observacion_cac'=> 'between:1,120',
         'telefono_contacto_prediag'=> 'between:1,10',
         'nombre_contacto_prediag'=> 'between:1,40',
         'tecnologia'=> 'between:1,2',
         'cod_unico_setem'=> 'between:1,9',
         'id_inalambrica'=> 'between:1,10',
         'cod_titularidad'=> 'between:1,1',
         'cod_origen'=> 'between:1,1',
         'desc_origen'=> 'between:1,25',
         'cod_ruta'=> 'between:1,3',
         'cod_clasificacion'=> 'between:1,3',
         'sabovip'=> 'between:1,1',
         'garantia'=> 'between:1,1',
         'cod_ult_averia'=> 'between:1,3',
         'id_telefono'=> 'between:1,9'
    ];
    public static $rules_retorno = [
        'tipo_operacion' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,20})$/i',
        'solicitud_tecnica' => 'required|regex:/^([a-zA-Z]{1}[0-9]{10})/',
        'fecha_envio' => 'required|regex:/^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})/',
        'hora_envio' => 'required|regex:/^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})/',
        'cod_envio' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,100})$/i',
        'descripcion' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,100})$/i'
    ];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });

        static::updated(function ($table) {
            \Event::fire('stcms.updated', $table);
        });
    }

    public function componentes()
    {
        return $this->hasMany('Legados\models\ComponenteGestel', 'solicitud_tecnica_gestel_id');
    }
    public function dominio()
    {
        return $this->hasMany('Legados\models\Gesteldominio', 'solicitud_tecnica_gestel_id');
    }
    public function dominiogestel()
    {
        return $this->hasMany('Legados\models\Gesteldominio', 'solicitud_tecnica_gestel_id')
                    ->where('gestel_dominio.actividad_id',1);
    }
    public function productos()
    {
        return $this->hasMany('Legados\models\Gestelproductos', 'solicitud_tecnica_gestel_id');
    }
    public function productosadquiridos()
    {
        return $this->hasMany('Legados\models\Gestelproductos', 'solicitud_tecnica_gestel_id')
                    ->where('gestel_productos_adquiridos.actividad_id',1);
    }
    public function solicitudTecnica()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnica');
    }
    /**
     * valida si la actividad esta configurada dentro de la parametrizacion
     * para trabajar en go,
     * @param $proveniencia: puede ser CMS, GESTEL
     */
    public function validarParam($array = [])
    {
        $parametros = Parametro::getParametroValidacion($array);
        $filtro='0';
        $valida=true; //si no hay $parametros;
        $this->mensaje='';
        $e=1;
        foreach ($parametros as $key => $value) {
            $valida=true;
            $trama=explode("||", $value->trama);
            $detalle=explode("||", $value->detalle);

            for ($i=0; $i <count($trama); $i++) {
                $subdetalle=explode(",", $detalle[$i]);
                if ($trama[$i]!='' && isset($this->$trama[$i])) {
                    if (array_search($this->$trama[$i], $subdetalle)===false) {
                        $valida=false;
                        $this->mensaje.= $e.'.- El campo "'.$trama[$i].'": ['.$this->$trama[$i].'] no está entre: ['.$detalle[$i].'] || ';
                        $e++;
                    }
                    if ($valida==false) {
                        break;
                    }
                }
            }
            if ($valida===true) {
                $this->mensaje='';
                break;
            }
        }
        return $valida;
    }
    public function getFullNameAttribute()
    {
        return "$this->ape_paterno $this->ape_materno, $this->nombres";
    }
    /*
    public function getFechaRegReqAttribute()
    {
        if (strlen($this->fecha_registro_requerimiento) > 0) {
            return \DateTime::CreateFromFormat('d/m/Y', $this->fecha_registro_requerimiento);
        }
    } */
    public function getTimeOfBookingAttribute()
    {   
        $date=trim(str_replace("/", "-", $this->fecha_registro));
        if(\DateTime::createFromFormat('Y-m-d H:i:s', $date)){
            return $date;
        } 
    }
    /*
    public function getFullAddressAttribute()
    {
        return "$this->tipo_via $this->nombre_via, $this->numero_via".
        "$this->piso $this->interior $this->manzana $this->lote $this->etapa".
        "$this->sector_servicio $this->nombre_playa $this->kilometro ".
        "$this->tipo_urbanizacion $this->desc_urbanizacion";
    }
    */
    //latitud X -77.027738 , longitud Y -12.123496
    public function setQuadrant()
    {
        $cellsize = \Config::get("validacion.cellsize"); //kilometers
        $pivotY =  \Config::get("validacion.pivot.longitude");//y
        $pivotX= \Config::get("validacion.pivot.latitude");//x
        $xgrid = round((1000 / 9) * cos($this->coordx * pi() / 180) * ($this->coordy - $pivotY) / $cellsize);
        $ygrid = round((1000 / 9) * ($this->coordx - $pivotX) / $cellsize);
        return $xgrid.":".$ygrid;
    }

    public function getWorkZone()
    {
        $workZone = null;
        $mdf=$armario=$cable=null;

        $mdf=$this->cabecera_mdf;
        $armario=$this->cod_armario;
        $cable=$this->cable;

        if ($mdf != "") {
            if ($armario != "") {
                    $workZone = $mdf."_".$armario;
            } elseif ($cable != "") {
                    $workZone = $mdf."_".$cable;
            }
        }

        $this->workZone = $workZone;
        return $workZone;
    }

    /**
     * metodo que consulta la capacidad almacenada en redis,
     * en su defecto en toa caso contrario devuelve texto de error
     */
    public function capacityOfsc()
    {
        $quiebre = \Quiebre::find($this->quiebre_id);

        $quiebreGrupo = $quiebre->quiebregrupos;
        $actividadTipo = \ActividadTipo::find($this->actividad_tipo_id);
        $hoy = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));

        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            && !is_null($quiebre)) {
            $workType = $actividadTipo->label;
            $llave = $hoy.'|AM|'.$workType.'|'.$quiebre->apocope.'|'.$this->workZone;
            $rst = \Redis::get($llave);
            $capacidad = json_decode($rst, true);
            
            if (!isset($capacidad["location"])) {
                $llave = $hoy.'|PM|'.$workType.'|'.$quiebre->apocope.'|'.$this->workZone;
                $rst = \Redis::get($llave);
                $capacidad = json_decode($rst, true);
            }
            
            if (isset($capacidad["location"])) {
                return [
                    'external_id'   =>$capacidad["location"],
                    'quiebre'       =>$quiebre->apocope,
                    'quiebreGrupo'  =>$quiebreGrupo->nombre,
                    'workType'      =>$workType,
                    'workZone'      =>$this->workZone,
                    'duration'      =>$actividadTipo->duracion,
                    'sla'           =>$actividadTipo->sla
                ];
            } else {
                //conultar la capacidad directamente a oracle
                // \Config::set("ofsc.auth.company", "telefonica-pe.test");
                $data=[
                        'fecha'          => $hoy,
                        'time_slot'      => '',
                        'worktype_label' => $workType,
                        'quiebre'        => $quiebre->apocope,
                        'zona_trabajo'   => $this->workZone,
                    ];
                $capacity = new Capacity();
                
                $response = $capacity->getCapacity($data);
                if (isset($response->error) && $response->error===false
                    && isset($response->data->capacity)) {

                    $capacidad = $response->data->capacity;
                    $timeSlot = $response->data->time_slot_info;
                    foreach ($capacidad as $key => $val) {
                        if (isset($val->location)) {
                            $val->{"time_slot_info"} = $timeSlot;
                            $clave=$val->date.'|'.
                                    $val->time_slot.'|'.
                                    $workType.'|'.
                                    $quiebre->apocope.'|'.
                                    $this->workZone;
                            \Redis::set($clave, json_encode($val));

                            return [
                                'external_id'   =>$val->location,
                                'quiebre'       =>$quiebre->apocope,
                                'quiebreGrupo'  =>$quiebreGrupo->nombre,
                                'workType'      =>$workType,
                                'workZone'      =>$this->workZone,
                                'duration'      =>$actividadTipo->duracion,
                                'sla'           =>$actividadTipo->sla
                            ];
                        }
                    }
                } elseif (is_string($response)) {
                    return $response;
                }
                return "SIN BUCKET";
            }
        } else {
            return "SIN ACTIVIDAD TIPO";
        }
    }

    public function getQuiebreId()
    {
        $quiebreId=null;

        switch ($this->area) {
            case 'CRIT':
                $quiebreId=19;
                $this->quiebre_id = $quiebreId;
                return $quiebreId;
            case 'PAGE':
                $quiebreId=53;
                $this->quiebre_id = $quiebreId;
                return $quiebreId;
            default:
                break;
        }
        //02, 03, 05, 06, 07 y 08
        switch ($this->segmento_cliente) {
            case '2':
            case '3':
            case '5':
            case '6':
            case '7':
            case '8':
                $quiebreId=54;
                $this->quiebre_id = $quiebreId;
                return $quiebreId;
            default:
                break;
        }

        $this->quiebre_id = $quiebreId;
        return $quiebreId;
    }
    public function getActividadTipoId()
    {
        $actividadTipoId=3; //falta definir
        $this->actividad_tipo_id = $actividadTipoId;

        return $actividadTipoId;
    }
    public function getActividadId()
    {
        return 1;
    }

    /**
     *
     */
    public function parsearFechas()
    {
        if (strlen($this->fecha_envio) > 0) {
            $this->fecha_envio = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_envio);
        }
        if (strlen($this->fecha_registro_requerimiento) > 0) {
            $this->fecha_registro_requerimiento = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_registro_requerimiento);
        }
        if (strlen($this->fecha_asignacion) > 0) {
            $this->fecha_asignacion = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_asignacion);
        }
        if (strlen($this->fecha_instalacion_alta) > 0) {
            $this->fecha_instalacion_alta = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_instalacion_alta);
        }
    }

    public function liquidacionGestelAveria()
    {
        $this->setTipoOperacion("LIQUIDACION");
        $reservados = $this->setTramaComponentes();
        $elementos = $this->setTrama($reservados);
        $this->enviarLegado($elementos);
    }

    public function devolucionGestelAveria()
    {
        $this->setTipoOperacion("DEVOLUCION");
        $reservados = $this->setTramaComponentes();
        $elementos = $this->setTrama($reservados);
        $this->enviarLegado($elementos);
    }

    public function setTipoOperacion($operacion)
    {
        $operacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                            ->where('tipo_legado', 2)
                            ->where('tipo', 0)
                            ->where('actividad_id', 1)
                            ->whereRaw('tipo_operacion like "%'.$this->tipo_operacion.'%"')
                            ->first();
        $this->tipo_operacion = "";
        if (!is_null($operacionObj)) {
            $this->tipo_operacion = $operacionObj->codigo;
        }
    }

    public function setTramaComponentes()
    {
        $componentesArray = [];
        $reservadosArray = [];
        foreach ($this->componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => $value->tipo_equipo,
                'codigo_equipo'         => $value->codigo_equipo,
                'modelo'                => $value->modelo,
                'marca'                 => $value->marca,
                'mantenimiento'         => $value->mantenimiento,
                'serie'                 => $value->serie,
                'codigo_barra'          => $value->codigo_barra,
                'codigo_material'       => $value->codigo_material,
                'cantidad'              => $value->cantidad,
                'descr_unidad_medida'   => $value->descr_unidad_medida,
                'codigo_repuesto'       => "",
                'numero_cabina'         => $value->numero_cabina,
            ];

            $reservadosArray[] = [
                'reservado1'    => $value->reservado1,
                'reservado2'    => $value->reservado2,
                'reservado3'    => $value->reservado3,
                'reservado4'    => $value->reservado4,
                'reservado5'    => $value->reservado5,
            ];
        }
        $this->componentes = $componentesArray;
        return $reservadosArray;
    }

    public function setTrama($requestReservado)
    {
        $fechaProgramacion =    '2017-07-18 14:40';
        $fechaEmision =         '2017-07-18 14:40';
        $fechaLiquid =          '2017-07-18 14:40:00';
        $fechaSal =             '2017-07-18 14:40';
        $fechaDestino =         '2017-07-18 14:40';

        $elementos = [
            'solicitud_tecnica_id'      => $this->solicitud_tecnica_id,
            'tipo_operacion'            => $this->tipo_operacion,
            'numero_solicitud_tecnica'  => $this->id_solicitud_tecnica,
            'fec_hora_programacion'     => $fechaProgramacion,
            'contrata'                  => $this->contrata,
            'tecnico'                   => isset($this->solicitudTecnica->ultimo->tecnico->carnet) ? 
                                           $this->solicitudTecnica->ultimo->tecnico->carnet : NULL,
            'tecnico2'                  => '',
            'tecnico3'                  => '',
            'ncompro'                   => "",
            'fecha_emision'             => $fechaEmision,
            'codigo_franqueo'           => "",
            't_liquid'                  => $fechaLiquid,
            'hora_inicio'               => '',
            'hora_fin'                  => '',
            'codigo_liquidacion'        => "",
            'codigo_destino'            => "",
            'codigo_version'            => "",
            'codigo_detalle'            => "",
            'fecha_sal'                 => $fechaSal,
            'fecha_destino'             => $fechaDestino,
            'nomcon_liq'                => "",
            'telecon_liq'               => "",
            'sintoma'                   => "",
            'obs_ultima_liq'            => $this->solicitudTecnica->ultimo->xa_observation,
            'dato1'                     => $this->valor1,
            'dato2'                     => $this->valor2,
            'dato3'                     => $this->valor3,
            'dato4'                     => $this->valor4,
            'dato5'                     => $this->valor5,
            'datosEquipo'               => $this->componentes,
            'reservado'                 => $requestReservado,
        ];

        return $elementos;
    }

    public function enviarLegado($elementos)
    {
        // dd(json_encode($elementos));
        $objStapi = new STGestelApi;
        $response = $objStapi->cierreAveria($elementos);
    }

    public function getMateriales()
    {
        $inventarios=[];

        /*$empresa=\Empresa::where('cms', $this->codigo_contrata)
                 ->first();*/
        //if (!is_null($empresa)) {
            $material=\MaterialActividad::where('actividad_id', 1)
                    //->where('empresa_id', $empresa->id)
                    ->where('tipo_legado', 2)
                    ->with('material')
                    ->get();
            foreach ($material as $key => $value) {
                $invarray=array();
                $invarray['invtype_label']=$value->material['codmat'];
                $invarray['quantity']=$value->cantidad;
                $invarray['XI_BRAND']="";
                $invarray['XI_MODEL']="";
                array_push($inventarios, $invarray);
            }
        //}
        return $inventarios;
    }
}
