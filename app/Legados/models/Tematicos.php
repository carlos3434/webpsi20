<?php

namespace Legados\models;

class Tematicos extends \Eloquent
{

    public $table = "tematicos";
    use \SoftDeletingTrait;

    protected $fillable = [
        'actividad_id',
        'motivo_ofsc_id',
        'nombre',
        'descripcion',
        'tipo_legado',
        'is_no_realizado',
        'is_pendiente',
        'is_completado',
        'is_pre_liquidado',
        'is_pre_devuelto',
        'estado',
        'parent',
        'tipo',
        'usuario_created_at',
        'usuario_updated_at'
    ];

    public static $rules = [
        'nombre' => 'required|between:1,100',
        'parent' => 'required|numeric',
        'tipo_legado' => 'required|numeric|in:1,2',
        'actividad_id' => 'required|numeric|in:1,2'
    ];
    public $userAuth = 697;

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = \Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = \Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = \Auth::id();
            }
        );
    }
}
