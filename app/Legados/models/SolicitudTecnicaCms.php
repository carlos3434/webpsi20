<?php namespace Legados\models;

use Legados\models\ComponenteCms;
use Ofsc\Capacity;
use Legados\models\SolicitudTecnica;
use Legados\helpers\SolicitudTecnicaHelper;
use Legados\STCmsApi;
use Configuracion\models\Parametro;

class SolicitudTecnicaCms extends \Eloquent
{
    use \SolicitudTecnicaTrait;

    protected $msj_error_content = [];

    protected $guarded =[];
    protected $table = "solicitud_tecnica_cms";

    public static $rules = [
         'tipo_operacion' => 'required|in:ALT_PRV,RUT_PRV,AVE_IND',
         'id_solicitud_tecnica' => 'required|between:1,11|regex:/^[0-9a-zA-Z]+$/',
         'fecha_envio' => 'required|size:10|date_format:d/m/Y',
         'hora_envio' => 'required|size:8',
         'zonal' => 'required|between:1,20|regex:/^[0-9a-zA-Z]+$/',
         'oficina' => 'required|between:1,3|regex:/^[0-9a-zA-Z]+$/',
         'orden_trabajo' => 'required|between:1,15|regex:/^([0-9])/',
         'estado' => '',//'required|size:1',
         'desc_estado' => '',//'required|between:1,30',
         'num_requerimiento' => 'required|between:1,15|alpha_num',
         'ind_origen_requerimiento' => 'required|size:1',
         'tipo_requerimiento' => 'required|between:1,10',
         'desc_tipo_requerimiento' => 'required|between:1,100',
         'cod_motivo_generacion' => 'required|between:1,10',
         'desc_motivo_generacion' => 'required|between:1,100',

         'fecha_registro_requerimiento' => 'required|size:10|date_format:d/m/Y',
         'hora_registro_requerimiento' => 'required|size:8',
         'fecha_asignacion' => 'size:10|date_format:d/m/Y',
         'hora_asignacion' => 'size:8',
         //'fecha_instalacion_alta' => 'required_if:tipo_operacion,RUT_PRV|required_if:tipo_operacion,AVE_IND|size:10|date_format:d/m/Y',
         'fecha_instalacion_alta' => 'required_if:tipo_operacion,RUT_PRV|size:10|date_format:d/m/Y',
         //'hora_instalacion_alta' => 'required_if:tipo_operacion,RUT_PRV|required_if:tipo_operacion,AVE_IND|size:8',
         'hora_instalacion_alta' => 'required_if:tipo_operacion,RUT_PRV|size:8',
         'codigo_contrata' => 'required|between:1,5|regex:/^([0-9])/',
         'desc_contrata' => 'between:1,50',
         'codigo_tecnico' => 'between:1,12',
         'desc_tecnico' => 'between:1,30|regex:/^([-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
         'peticion' => 'required_if:tipo_paquete,DUO|required_if:tipo_paquete,TRIO|between:1,15|regex:/^([0-9])/',
         'doc_identidad' => '',//'required|between:8,20',
         'coordx_cliente' => 'numeric|between:-999999999.99999999999999,999999999.99999999999999',
         'coordy_cliente' => 'numeric|between:-999999999.99999999999999,999999999.99999999999999',
         'coordx_direccion_tap' => 'numeric|between:-999999999.99999999999999,999999999.99999999999999',
         'coordy_direccion_tap' => 'numeric|between:-999999999.99999999999999,999999999.99999999999999',
         'tipo_portado' => 'between:1,3',
         'observacion' => 'between:0,250',
         'cod_servicio' => 'required_if:tipo_operacion,RUT_PRV|required_if:tipo_operacion,AVE_IND|numeric|between:0,999999999999999|regex:/^([0-9])/',
         'clase_servicio' => 'required|between:0,4',
         'desc_clase_servicio' => 'required|between:0,100',
         'categoria_servicio' => 'required|between:1,2',
         'desc_categoria_servicio' => 'required|between:1,100',
         'cod_departamento' => 'required|between:1,3',
         'desc_departamento' => '',//'required|between:1,100',
         'cod_provincia' => '',//'required|between:1,3',
         'desc_provincia' => '',//'required|between:1,100',
         'cod_distrito' => '',//'required|between:1,3',
         'desc_distrito' => '',//'required|between:1,100',
         'tipo_via' => '',//'between:1,2',
         'nombre_via' => '',//'required|between:1,30',
         'numero_via' => '',//'required|between:1,5',
         'piso' => 'between:1,2',
         'interior' => 'between:1,5',
         'manzana' => 'between:1,5',
         'lote' => 'between:1,5',
         'etapa' => 'between:1,3',
         'sector_servicio' => 'between:1,5',
         'nombre_playa' => 'between:1,50',
         'kilometro' => 'numeric|between:0,9999.9',
         'tipo_urbanizacion' => 'between:1,4',
         'desc_urbanizacion' => 'between:1,30',
         'desc_referencia' => 'between:1,80',
         'telefono1' => 'between:1,20',
         'telefono2' => 'between:1,20',

         'cod_cliente' => '',//'required|numeric|between:0,999999999999999|regex:/^([0-9])/',
         'condicion' => 'between:1,30',
         'ape_paterno' => 'between:1,35',
         'ape_materno' => 'between:1,35',
         'nom_cliente' => '',//'required|between:1,30',
         'ind_vip' => 'size:1',
         'situacion' => 'between:1,10',
         'desc_situacion' => 'between:1,100',
         'categoria_cliente' => '',//'required|between:1,2',
         'desc_categoria_cliente' => '',//'required|between:1,100',
         'segmento' => 'required|between:1,2',
         'desc_segmento' => 'required|between:1,50',
         'cod_jefatura' => 'between:1,3',
         'cod_nodo' => 'required|between:1,2',
         'desc_nodo' => 'between:1,100',
         'cod_plano' => 'between:1,4',
         'desc_plano' => 'between:1,100',
         'cod_troba' => 'required|between:1,4',
         'sector_troba' => 'between:1,50',
         'cod_amplificador' => 'between:1,50',
         'cod_tap' => 'between:1,4',
         'borne' => 'between:1,4',
         'cod_cmts' => 'between:1,4',
         'desc_cmts' => 'between:1,40',
         'tap_via' => 'between:1,2',
         'tap_nombre_via' => 'between:1,30',
         'tap_numero' => 'between:1,5',
         'tap_piso' => 'between:1,2',
         'tap_interior' => 'between:1,5',
         'tap_manzana' => 'between:1,5',
         'tap_lote' => 'between:1,5',
         'sector_tecnico' => 'between:1,5',
         'referencia' => 'between:1,100',
         'id_tematico' => 'numeric|between:0,99999|regex:/^([0-9])/',
         'desc_tematico' => 'between:1,100',
         'id_bbk' => 'numeric|between:0,9999|regex:/^([0-9])/',
         'tipo_paquete' => '|between:1,50',
         'velocidad_internet_servicio' => '',//'numeric|between:0,99999',
         'velocidad_internet_requerimiento' => '',//'numeric|between:0,99999',

         'escenario_cms' => 'size:1',
         'escenario_gestel' => 'between:1,3',
         'ind_calidad_venta' => 'size:1',
         'tipo_senal_troba' => 'in:1,2,3',
         'ind_duo' => 'size:1',
         'tipo_linea' => 'between:1,4',
         'tipo_tecnologia' => 'between:1,4',
         'telefono_voip' => 'between:1,8',
         'promocion_linea' => 'between:1,3',
         'desc_linea' => 'between:1,40',
         'nodo_id' => 'between:1,9|regex:/^[0-9a-zA-Z]+$/',
         'valor1' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',
         'valor2' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',
         'valor3' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',
         'valor4' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',
         'valor5' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',

    ];
    public static $rules_retorno = [
        'tipo_operacion' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,20})$/i',
        'solicitud_tecnica' => 'required|regex:/^([a-zA-Z]{1}[0-9]{10})/',
        'fecha_envio' => 'required|size:10|date_format:d/m/Y',
        'hora_envio' => 'required|size:8',
        'cod_envio' => 'required',
        'descripcion' => 'required'
    ];
    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = \Auth::id();
            }
        );
        static::creating(
            function ($table) {
                $table->usuario_created_at = \Auth::id();
            }
        );

        static::updated(
            function ($table) {
                \Event::fire('stcms.updated', $table);
            }
        );
    }

    public function componentes()
    {
        return $this->hasMany('Legados\models\ComponenteCms');
    }
    public function solicitudTecnica()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnica');
    }
    /**
     * valida si la actividad esta configurada dentro de la parametrizacion
     * para trabajar en go,
     * @param $proveniencia: puede ser CMS, GESTEL
     */
    public function validaCmsGo($param = [])
    {
        $detalle="";
        if (isset($param["filtrodetalle"])) {
            $detalle="|".$param["filtrodetalle"];
        }

        $parametros = \Cache::get('listParametroCms'.$detalle);

        if (!isset($param["tipo"])) {
            $param["tipo"] = 1;
        }
        if (!$parametros) {
            $parametros = \Cache::remember(
                'listParametroCms'.$detalle,
                24*60*60,
                function () use ($param) {
                    return Parametro::getParametroValidacion($param);
                }
            );
        }
        return $this->validarParam($parametros, $param);
    }

    public function validaMasiva($param = [])
    {
        $detalle="";
        if (isset($param["filtrodetalle"])) {
            $detalle="|".$param["filtrodetalle"];
        }

        $parametros = \Cache::get('listParametroCmsMasiva'.$detalle);
        if (!isset($param["tipo"])) {
            $param["tipo"] = 4;
        }
        if (!$parametros) {
            $parametros = \Cache::remember(
                'listParametroCmsMasiva'.$detalle,
                24*60*60,
                function () use ($param) {
                    return Parametro::getParametroValidacion($param);
                }
            );
        }
        return $this->validarParam($parametros, $param);

    }

    public function validaGoToa($param = [])
    {
        $detalle="";
        if (isset($param["filtrodetalle"])) {
            $detalle="|".$param["filtrodetalle"];
        }

        $parametros = \Cache::get('listParametrosGoToa'.$detalle);

        if (!isset($param["tipo"])) {
            $param["tipo"] = 2;
        }

        if (!$parametros) {
            $parametros = \Cache::remember(
                'listParametrosGoToa'.$detalle,
                24*60*60,
                function () use ($param) {
                    return Parametro::getParametroValidacion($param);
                }
            );
        }
        return $this->validarParam($parametros, $param);

    }
    public function validarParam($parametros = [], $array = [])
    {
        //$parametros = \Parametro::getParametroValidacion($array);

        $filtro = '0';
        $valida = true;
        $this->mensaje = '<div style="max-height: 200px; overflow-y: scroll">';

        if ($array["tipo"] == 4 && count($parametros)<=0) {
            return false;
        }

        foreach ($parametros as $key => $value) {

            $nombreParametro = isset($value->nombre_parametro)? $value->nombre_parametro : "";
            $this->mensaje.='<strong>Parametro: ' . $nombreParametro . '</strong><br>';

            $valida = true;
            $trama = explode("||", $value->trama);
            $detalle = explode("||", $value->detalle);

            for ($i = 0; $i < count($trama); $i++) {
                if (isset($detalle[$i])) {
                    $subdetalle = explode(",", $detalle[$i]);

                    if ($trama[$i] != '' && isset($this->{$trama[$i]})) {

                        if (array_search($this->{$trama[$i]}, $subdetalle) === false) {
                            $valida = false;
                            $this->mensaje.=' El campo "'.$trama[$i].'": ['.$this->{$trama[$i]}.'], no está entre: ['.$detalle[$i].']';

                        }
                        if ($valida == false) {
                            break;
                        }
                    }
                }

            }

            $this->mensaje .= '<br>----------------------------------------------------<br>';

            if ($valida === true) {
                $this->mensaje = '';
                break;
            }
        }

        if (!($valida === true)) {
            $this->mensaje .= '</div>';
        } else {
            $this->mensaje = '';
        }

        return $valida;
    }
    public function getFullNameAttribute()
    {
        return "$this->ape_paterno $this->ape_materno, $this->nom_cliente";
    }

    public function getFechaRegReqAttribute()
    {
        if (strlen($this->fecha_registro_requerimiento) > 0) {
            return \DateTime::CreateFromFormat('d/m/Y', $this->fecha_registro_requerimiento);
        }
    }
    public function getTimeOfBookingAttribute()
    {
        if ($this->fecha_reg_req == null) {
            $fecha=$this->fecha_registro_requerimiento;
        } else {
            $fecha=$this->fecha_reg_req->format('Y-m-d');
        }
        return $fecha." ".$this->hora_registro_requerimiento;
    }
    public function getFullAddressAttribute()
    {
        return "$this->tipo_via $this->nombre_via $this->numero_via".
        " PISO $this->piso INT $this->interior MNZ $this->manzana LT $this->lote".
        " $this->tipo_urbanizacion: $this->desc_urbanizacion";
    }
    public function getFullFfttAttribute()
    {
        if ($this->cod_troba=='') {
            return "$this->cod_nodo|$this->cod_plano|$this->cod_amplificador|$this->cod_tap|$this->borne";
        }
        return "$this->cod_nodo|$this->cod_troba|$this->cod_amplificador|$this->cod_tap|$this->borne";
    }
    //latitud X -77.027738 , longitud Y -12.123496
    public function setQuadrant()
    {
        $cellsize = \Config::get("validacion.cellsize"); //kilometers
        $pivotY =  \Config::get("validacion.pivot.longitude");//y
        $pivotX= \Config::get("validacion.pivot.latitude");//x
        $xgrid = round((1000 / 9) * cos($this->coordx_cliente * pi() / 180) * ($this->coordy_cliente - $pivotY) / $cellsize);
        $ygrid = round((1000 / 9) * ($this->coordx_cliente - $pivotX) / $cellsize);
        return $xgrid.":".$ygrid;
    }
    public function getWorkZone()
    {
        $workZone = null;
        if ($this->cod_nodo !='') {
            if ($this->cod_troba  != "") {
                $workZone = $this->cod_nodo."_".$this->cod_troba;
            } elseif ($this->cod_plano  != "") {
                $workZone = $this->cod_nodo."_".$this->cod_plano;
            }
        }
        $this->workZone = $workZone;
        return $workZone;
    }
    /**
     * metodo que consulta la capacidad almacenada en redis,
     * en su defecto en toa caso contrario devuelve texto de error
     */
    public function capacityOfsc($quiebreId = null)
    {
        if (is_null($quiebreId)) {
            $quiebreId = $this->quiebre_id;
        }
        $quiebre = \Quiebre::find($quiebreId);
        
        $quiebreGrupo = $quiebre->quiebregrupos;
        $actividadTipo = \ActividadTipo::find($this->getActividadTipoId());
        //$hoy = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));
        $hoy = date("Y-m-d");

        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            && !is_null($quiebre)) {
            $workType = $actividadTipo->label;
            $llave = $hoy.'|AM|'.$workType.'|'.$quiebre->apocope.'|'.$this->getWorkZone();
            $rst = \Redis::get($llave);
            $capacidad = json_decode($rst, true);
            
            if (!isset($capacidad["location"])) {
                $llave = $hoy.'|PM|'.$workType.'|'.$quiebre->apocope.'|'.$this->getWorkZone();
                $rst = \Redis::get($llave);
                $capacidad = json_decode($rst, true);
            }
            
            if (isset($capacidad["location"])) {
                return [
                    'external_id'   =>$capacidad["location"],
                    'quiebre'       =>$quiebre->apocope,
                    'quiebreGrupo'  =>$quiebreGrupo->nombre,
                    'workType'      =>$workType,
                    'workZone'      =>$this->getWorkZone(),
                    'duration'      =>$actividadTipo->duracion,
                    'sla'           =>$actividadTipo->sla
                ];
            } else {
                //conultar la capacidad directamente a oracle
                $data=[
                        'fecha'          => $hoy,
                        'time_slot'      => '',
                        'worktype_label' => $workType,
                        'quiebre'        => $quiebre->apocope,
                        'zona_trabajo'   => $this->getWorkZone()
                    ];
                $capacity = new Capacity();
                
                $response = $capacity->getCapacity($data);
                if (isset($response->error) && $response->error===false
                    && isset($response->data->capacity)) {

                    $capacidad = $response->data->capacity;
                    $timeSlot = $response->data->time_slot_info;
                    foreach ($capacidad as $key => $val) {
                        if (isset($val->location)) {
                            $val->{"time_slot_info"} = $timeSlot;
                            $clave=$val->date.'|'.
                                    $val->time_slot.'|'.
                                    $workType.'|'.
                                    $quiebre->apocope.'|'.
                                    $this->getWorkZone();
                            \Redis::set($clave, json_encode($val));

                            return [
                                'external_id'   =>$val->location,
                                'quiebre'       =>$quiebre->apocope,
                                'quiebreGrupo'  =>$quiebreGrupo->nombre,
                                'workType'      =>$workType,
                                'workZone'      =>$this->getWorkZone(),
                                'duration'      =>$actividadTipo->duracion,
                                'sla'           =>$actividadTipo->sla
                            ];
                        }
                    }
                } elseif (is_string($response)) {
                    return $response;
                }
                return "SIN BUCKET";
            }
        } else {
            return "SIN ACTIVIDAD TIPO";
        }
    }

    /*
    * Validacion para Quiebre PENDMOV1
    * tipo_paquete = 'TRCM'
    * combinacion de tipo requerimiento + motivo exista
    * en tabla webpsi_coc.prov_catv_tiposact y quiebre marcado como "MOVISTAR UNO"
    */

    public function getMsjErrorContent()
    {
        return $this->msj_error_content;
    }

    public function getMsjActividadError()
    {
        return $this->msj_actividad_error;
    }

    public function getQuiebreId2($actividadId)
    {
        $quiebre_id = SolicitudTecnicaHelper::getQuiebreByBD($actividadId, $this);
        $this->quiebre_id = $quiebre_id;
        return $quiebre_id;
    }

    public function getQuiebreId($actividadId)
    {
        $quiebreId = null;

        $msj_error_content = [
            'fallo' => "Quiebre",
            'tipos' => []
        ];

        if ($actividadId == 1) {// averia

            $msj_error_content['actividad'] = "Avería";
            // PREMIUM
            $q_premium = SolicitudTecnicaHelper::getQuiebrePremium($this);
            $quiebreId = $q_premium["quiebre_id"];

            if (!is_null($quiebreId)) {
                $this->quiebre_id = $quiebreId;
                return $quiebreId;
            } else {
                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre Premium",
                    'msj' => "",
                    'quiebres' => $q_premium["msj_error_log"]
                ];
            }
            
            // QUIEBRE DIF MASIVO
            $q_diferenciadomasivo = SolicitudTecnicaHelper::getDiferenciadoMasivo($this);
            if (!is_null($q_diferenciadomasivo)) {
                $this->quiebre_id = $q_diferenciadomasivo;
                return $q_diferenciadomasivo;
            }
            // QUIEBRE DIFERENCIADO
            $q_diferenciado = SolicitudTecnicaHelper::getQuiebreDiferenciado($this);
            $quiebreId = $q_diferenciado["quiebre_id"];

            if (!is_null($quiebreId)) {

                $this->quiebre_id = $quiebreId;
                return $quiebreId;

            } else {
                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre diferenciado",
                    'msj' => "",
                    'quiebres' => $q_diferenciado["msj_error_log"]
                ];
            }

            // PENDMOV1: validando por clase de servicio
            $q_movistaruno = SolicitudTecnicaHelper::getQuiebreMovistarUno($this);
            $quiebreId = $q_movistaruno["quiebre_id"];

            if (!is_null($quiebreId)) {
                $this->quiebre_id = $quiebreId;
                return $quiebreId;
            } else {
                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre Movistar Uno",
                    'msj' => "",
                    'quiebres' => $q_movistaruno["msj_error_log"]
                ];
            }

            //ASIGNADO_A_CRITICOS
            $contratascritico = ["343", "344", "345", "346"];
            if (in_array($this->codigo_contrata, $contratascritico)) {

                $this->quiebre_id = 19;
                return 19;

            } else {
                $msj_error_content['tipos'][] = [
                    'nombre' => "ASIGNADO A CRITICOS",
                    'msj' => "Código contrata esperado (343, 34, 345 ó 346); Recibido: " . $this->codigo_contrata.'.',
                    'quiebres' => []
                ];
            }

            // POST_DIGIT
            $q_post_digit = SolicitudTecnicaHelper::getQuiebrePostDigitalizacion($this);
            $quiebreId = $q_post_digit["quiebre_id"];

            if (!is_null($quiebreId)) {

                $this->quiebre_id = $quiebreId;
                return $quiebreId;

            } else {

                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre post-digitalización",
                    'msj' => "",
                    'quiebres' => $q_post_digit["msj_error_log"]
                ];

            }

        }


        if ($actividadId == 2) {// provision
            $msj_error_content['actividad'] = "Provisión";

            // PENDMOV1: validando por clase de servicio
            $q_movistarUno = SolicitudTecnicaHelper::getQuiebreMovistarUno($this);
            $quiebreId = $q_movistarUno["quiebre_id"];

            if (!is_null($quiebreId)) {

                $this->quiebre_id = $quiebreId;
                return $quiebreId;

            } else {

                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre Movistar Uno",
                    'msj' => "",
                    'quiebres' => $q_movistarUno["msj_error_log"]
                ];

            }
            

            // PREMIUM
            $q_premium = SolicitudTecnicaHelper::getQuiebrePremium($this);
            $quiebreId = $q_premium["quiebre_id"];

            if (!is_null($quiebreId)) {

                $this->quiebre_id = $quiebreId;
                return $quiebreId;

            } else {

                $msj_error_content['tipos'][] = [
                    'nombre' => "Quiebre Premium",
                    'msj' => "",
                    'quiebres' => $q_premium["msj_error_log"]
                ];

            }


            // DUO_HFC
            $combinaciones = ["SE-V257", "SE-V259", "SL-V258"];
            $tmp = $this->tipo_requerimiento."-".$this->cod_motivo_generacion;

            if (in_array($tmp, $combinaciones)) {

                $this->quiebre_id = 7;
                return 7;

            } else {

                $msj_error_content['tipos'][] = [
                    'nombre' => "DUO HFC",
                    'msj' => "Combinación Tipo Requerimiento y motivo esperado (SE-V257, SE-V259, SL-V258); Recibido: " . $tmp,
                    'quiebres' => []
                ];

            }

            //MONO_CATV_DIGITAL

        }

        $this->msj_error_content = $msj_error_content;

        $this->quiebre_id = $quiebreId;
            
        return $quiebreId;
    }
    public function getActividadTipoId()
    {
        $actividadTipoId = null;

        $msj_error_content = [
            'fallo' => "Tipo de Actividad",
            'tipos' => []
        ];

        if ($this->tipo_operacion == "AVE_IND") { // averia
            $msj_error_content['actividad'] = "Avería";

            if (($this->clase_servicio == "26" ||
                  $this->clase_servicio == "27" ||
                  $this->clase_servicio == "33" ||
                  $this->clase_servicio == "38") || $this->ind_duo == "DUO" || $this->tipo_tecnologia == "VOIP") {
                $actividadTipoId = 6;
            } else {
                $actividadTipoId = 4;
            }

        } else {
            $msj_error_content['tipos'][] = [
                'nombre' => "Reparacion Movistar uno | Reparación CATV",
                'msj' => "Tipo de operación esperado (AVE_IND); Recibido: " . $this->tipo_operacion . ".",
                'quiebres' => []
            ];
        }

        if ($this->tipo_operacion == "RUT_PRV" || $this->tipo_operacion == "ALT_PRV") { // provision
            $msj_error_content['actividad'] = "Provisión";

            $existe = \DB::table("webpsi_coc.prov_catv_tiposact")
                ->where(
                    ["tipo_req" => $this->tipo_requerimiento,
                    "motivo" => $this->cod_motivo_generacion]
                )
                ->first();

            if (!is_null($existe)) {

                $actividadTipoId = $existe->actividad_toa;

            } else {
                $msj_error_content['tipos'][] = [
                    'nombre' => "Asignacióń de tipo de actividad por Base de datos",
                    'msj' => "No se encontró registro en la tabla de Tipos de actividades de provisiones de Cable y TV con tipo de requirimiento (" .$this->tipo_requerimiento. ") y motivo (" .$this->cod_motivo_generacion. ").",
                    'quiebres' => []
                ];
            }
            
        } else {
            $msj_error_content['tipos'][] = [
                'nombre' => "Asignacióń de tipo de actividad por Base de datos",
                'msj' => "Tipo de operación esperado (AVE_IND); Recibido: " . $this->tipo_operacion . ".",
                'quiebres' => []
            ];
        }

        $this->msj_error_content = $msj_error_content;

        $this->actividad_tipo_id = $actividadTipoId;
        
        return $actividadTipoId;
    }
    public function getActividadId()
    {
        switch ($this->tipo_operacion) {
            case 'AVE_IND':
                return 1;
                break;
            case 'RUT_PRV':
            case 'ALT_PRV':
                return 2;
                break;
        }
    }

    public static function getDatosCms($solicitudTecnicaId = '')
    {
        $ultimoMov = SolicitudTecnicaCms::select()
                    ->getUltimoCms()
                    ->getTecnico()
                    ->where(
                        'solicitud_tecnica_cms.solicitud_tecnica_id',
                        '=',
                        $solicitudTecnicaId
                    )
                    ->orderBy('solicitud_tecnica_cms.id', 'desc')
                    ->first();

        if ($ultimoMov->solicitud_tecnica_id!='') {
            $solicitudTecnicaCms = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitudTecnicaId)->orderBy('id', 'desc')->first();
            $componentes = $solicitudTecnicaCms->componentes()->whereRaw("deleted_at IS NULL")->get();
            $valida=1;
            foreach ($componentes as $key => $value) {
                $result = strpos($value->descripcion, 'DECO');
                if ($result !== false) {
                    if ($value->estado_operacion!=1) {
                        $valida=0;
                        break;
                    }
                }
            }

            return array(
                'rpta'=>$valida,
                'componentes'=>$componentes,
                'ultimov'=>$ultimoMov
            );
        } else {
            return array(
                'rpta'=>0
            );
        }
    }
    /**
     *
     */
    public function parsearFechas()
    {
        if (strlen($this->fecha_envio) > 0) {
            $this->fecha_envio = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_envio);
        }
        if (strlen($this->fecha_registro_requerimiento) > 0) {
            $this->fecha_registro_requerimiento = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_registro_requerimiento);
        }
        if (strlen($this->fecha_asignacion) > 0) {
            $this->fecha_asignacion = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_asignacion);
        }
        if (strlen($this->fecha_instalacion_alta) > 0) {
            $this->fecha_instalacion_alta = \DateTime::CreateFromFormat('d/m/Y', $this->fecha_instalacion_alta);
        }
    }

    public function liquidacionCms()
    {
        $this->setTipoOperacion("LIQUIDACION");
        $this->setTramaComponentesCms();
        $elementos = $this->setTramaCms();
        $this->enviarLegado($elementos);
    }

    public function devolucionCms()
    {
        $this->setTipoOperacion("DEVOLUCION");
        $this->setTramaComponentesCms();
        $elementos = $this->setTramaCms();
        $this->enviarLegado($elementos);
    }

    public function setTipoOperacion($operacion)
    {
        if ($this->solicitudTecnica->ultimo->actividad_id == 1) {
            $this->tipo_operacion = $operacion == "LIQUIDACION" ? "ACI_LIQ" : "ACI_DEV";
        } else {
            $this->tipo_operacion = $operacion == "LIQUIDACION" ? "PCI_LIQ" : "PCI_DEV";
        }
    }

    public function setTramaComponentesCms()
    {
        $componentesArray = [];
        foreach ($this->componentes as $key => $value) {
            $componentesArray[] = [
                'numero_requerimiento' => $value->numero_requerimiento,
                'numero_ot'            => $value->numero_ot,
                'numero_servicio'      => $value->numero_servicio,
                'componente_cod'       => $value->componente_cod,
                'marca'                => $value->marca,
                'modelo'               => $value->modelo,
                'tipo_equipo'          => $value->componente_tipo,
                'numero_serie'         => $value->numero_serie,
                'casid'                => $value->casid,
                'cadena1'              => $value->valor1,
                'cadena2'              => $value->valor2,
                'cadena3'              => $value->valor3,
                'cadena4'              => $value->valor4,
                'cadena5'              => $value->valor5,
            ];
        }
        $this->componentes = $componentesArray;
    }

    public function setTramaCms()
    {
        $ultimo = $this->solicitudTecnica->ultimo;
        $datetime_ini[0] = $datetime_ini[1] = null;
        if (!is_null($ultimo->start_time)) {
            $datetime_ini = explode(" ", $ultimo->start_time);
        }

        $fechaFin = is_null($ultimo->end_date) ?
            \Helpers::formatoFecha($datetime_ini[0]) :
            \Helpers::formatoFecha($ultimo->end_date);

        $nuevaHora = strtotime('+1 minute', strtotime($ultimo->end_time));
        $nuevaHora = date('H:i:s', $nuevaHora);

        $codigo_liquidacion = "";
        switch ($this->tipo_operacion) {
            case 'ACI_LIQ':
                $codigo_liquidacion = "LT05";
                break;
            case 'ACI_DEV':
                $codigo_liquidacion = isset($ultimo->submotivoOfsc->codigo_legado)?
                    $ultimo->submotivoOfsc->codigo_legado : null;
                break;
            case 'PCI_LIQ':
                $codigo_liquidacion = "LT01";
                break;
            case 'PCI_DEV':
                $codigo_liquidacion = isset($ultimo->motivoOfsc->codigo_legado) ?
                    $ultimo->motivoOfsc->codigo_legado : null;
                break;
        }

        $elementos = [
            "solicitud_tecnica_id"  => $ultimo->solicitud_tecnica_id,
            "tipo_operacion"        => $this->tipo_operacion,
            "id_solicitud_tecnica"  => $this->solicitudTecnica->id_solicitud_tecnica,
            "fecha_envio"           => \Helpers::formatoFecha($this->fecha_envio),
            "hora_envio"            => $this->hora_envio,
            "codigo_liquidacion"    => $codigo_liquidacion,
            "codigo_tecnico1"       => isset($ultimo->tecnico->carnet) ?
                $ultimo->tecnico->carnet : null,
            "codigo_tecnico2"       => '',
            "fecha_inicio"          => \Helpers::formatoFecha($datetime_ini[0]),
            "hora_inicio"           => $datetime_ini[1],
            "fecha_fin"             => $fechaFin,
            "hora_fin"              => $ultimo->end_time,
            "fecha_liquida"         => $fechaFin,
            "hora_liquida"          => $nuevaHora,
            "nombre_contacto"       => (is_null($ultimo->a_receive_person_name) || $ultimo->a_receive_person_name == "") ?
                                       $this->nom_cliente : $ultimo->a_receive_person_name,
            "parentesco_contacto"   => '',
            "dni_contacto"          => '',
            "codigo_contrata"       => $this->codigo_contrata,
            "observacion"           => substr(\Helpers::swapString($ultimo->xa_observation), 0, 99),
            "campo1"                => $this->valor1,
            "campo2"                => $this->valor2,
            "campo3"                => $this->valor3,
            "campo4"                => $this->valor4,
            "campo5"                => $this->valor5,
            "componentes"           => $this->componentes
        ];

        return $elementos;
    }

    public function enviarLegado($elementos)
    {
        // dd(json_encode($elementos));
        $objStapi = new STCmsApi;
        $response = $objStapi->cierre($elementos);
        return $response;
    }

    public function getGeozona($actividad_id)
    {
        //$actividad_id=1;
        $query=\GeoProyectoCapa::listargeocerca($actividad_id);
        $bucket=null;
    
        foreach ($query as $key => $value) {
            $coord=$value->coordenadas;
            $coord=str_replace(",", " ", $coord);
            $polygon=explode("|", $coord);
            array_push($polygon, $polygon[0]); //PARA CERRAR POLIGONO
    
            $pointLocation = new \pointLocation();
            $points = $this->coordx_cliente." ".$this->coordy_cliente;
    
            if ($pointLocation->pointInPolygon($points, $polygon)!=="outside"
                && $value->detalle!=="") {
                $bucket=$value->detalle;
                break;
            }
            
        }
        $this->workZone=$bucket;
        return $bucket;

    }
    public function getValidaCupos($cupos, $parametros)
    {
        $diaSemana=date("N", strtotime($parametros['fechaAgenda']));

        if (\Session::get('tipoPersona') !== null) {
            $tipopersonaid=\Session::get('tipoPersona');
        } else if (\Auth::user()) {
            $tipopersonaid= \Auth::user()->tipo_persona_id;
        } else {
            $tipopersonaid=3;
        }
       
        $TipoPersona =\TipoPersona::where('id', '=', $tipopersonaid)
                       ->First();
        $ocupados = \Agenda::postHorarios(
            '2',
            array(
               $parametros['fechaAgenda'],
               $diaSemana,
               $diaSemana,
               $diaSemana,
               $parametros['external_id'],
               $tipopersonaid,
               '7',
               $parametros['fechaAgenda'],
               $parametros['fechaAgenda'],
               $this->codigo_contrata,
               $this->zonal,
               \Input::get('quiebre'),
               $parametros['fechaAgenda'],
               $parametros['timeSlot']
               )
        );
        if ($ocupados) {
            $libre = $cupos-$ocupados[0]->ocupado;
            if ($libre<=0) {
                return false;
            }
        }
        return true;
    }

    public function getMateriales($actividad_id)
    {
        $inventarios=[];

        /*$empresa=\Empresa::where('cms', $this->codigo_contrata)
                 ->first();*/
        //if (!is_null($empresa)) {
            $material=\MaterialActividad::where('actividad_id', $actividad_id)
                    //->where('empresa_id', $empresa->id)
                    ->where('tipo_legado', 1)
                    ->with('material')
                    ->get();
        foreach ($material as $key => $value) {
            $invarray=array();
            $invarray['invtype_label']=$value->material['codmat'];
            $invarray['quantity']=$value->cantidad;
            $invarray['XI_BRAND']="";
            $invarray['XI_MODEL']="";
            array_push($inventarios, $invarray);
        }
        //}
        return $inventarios;
    }

    public function getComponentes()
    {
        return ComponenteCms::where(
            'solicitud_tecnica_cms_id',
            $this->id
        )->get();
    }
}
