<?php

namespace Legados\models;
use Legados\models\ComponenteCms;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Sms;

class ComponenteOperacion extends \Eloquent
{

    public $table = "componente_operacion";

    public function guardar ($response)
    {
        $array=array();
        $data = new \stdClass();
        if (isset($response->data)) {
            $data = $response->data;
        }

        if(isset($response->id_solicitud_tecnica))
            $array['id_solicitud_tecnica']=$response->id_solicitud_tecnica;
        if(isset($response->componente_id))
            $array['componente_id']=$response->componente_id;
        if(isset($response->codreq))
            $array['codreq']=$response->codreq;
        if(isset($response->operacion))
            $array['operacion']=$response->operacion;
        if(isset($response->telefono_origen))
            $array['telefono_origen']=$response->telefono_origen;
        if(isset($response->interface))
            $array['interface']=$response->interface;
        if(isset($response->observacion))
            $array['observacion']=$response->observacion;
        if(isset($response->numser))
            $array['numser']=$response->numser;
        if(isset($response->codmat))
            $array['codmat']=$response->codmat;
        if(isset($response->numserold))
            $array['numserold']=$response->numserold;
        if(isset($response->codmatold))
            $array['codmatold']=$response->codmatold;
        if(isset($response->numserpar))
            $array['numserpar']=$response->numserpar;
        if(isset($response->codmatpar))
            $array['codmatpar']=$response->codmatpar;
        if(isset($data->codreq))
            $array['codreq']=$data->codreq;
        if(isset($data->indorigreq))
            $array['indorigreq']=$data->indorigreq;
        if(isset($data->numcompxreq))
            $array['numcompxreq']=$data->numcompxreq;
        if(isset($data->numcompxsrv))
            $array['numcompxsrv']=$data->numcompxsrv;
        if(isset($data->indicador))
            $array['indicador']=$data->indicador;
        if(isset($data->idreg))
            $array['idreg']=$data->idreg;
        if(isset($data->observacion))
            $array['observacion']=$data->observacion;

        $array['created_at'] = date('Y-m-d H:i:s');
        $array['usuario_created_at'] = 697;
        \DB::table('componente_operacion')->insert($array);
    }

   public function RespuestaOperacion ($request)
    {
        $operacion = ComponenteOperacion::where('idreg','=',$request->indicador_operacion)
                    ->orderBy('id','desc')
                    ->first();
        if($operacion) {
            $operacion['indicador_procesamiento'] = $request->indicador_procesamiento; 
            $operacion['codigo_error_interno'] = $request->codigo_error_interno; 
            $operacion['codigo_error_envio'] = $request->codigo_error_envio; 
            $operacion['observacion_respuesta'] = $request->observacion;
            $operacion['usuario_updated_at'] = '697';
            $operacion->save();

            $celular = $operacion->telefono_origen;
            if (is_null($celular) or $celular = "") {
                $ultimo = Ultimo::where('num_requerimiento', $operacion->codreq)
                        ->leftJoin('tecnicos as t', 't.id', '=', 'tecnico_id')
                        ->orderBy('solicitud_tecnica_ultimo.id', 'desc')->first();
                $celular = $ultimo["celular"];
            }

            if ($request->indicador_procesamiento == 1) {
                Sms::enviar($celular, "Operacion exitosa, Requerimiento: {$operacion->codreq}, Operacion: {$operacion->operacion}", '1');
            } else {
                Sms::enviar($celular, "Error de operacion IWY, Requerimiento: {$operacion->codreq}, Operacion: {$operacion->operacion}", '1');
            }

            return array(
                'result_code'  =>  1,
                'error_msg'    => 'OK'
                );
        } else {

            return array(
                'result_code'  =>  0,
                'error_msg'    => 'NO OK'
                );
        }
    }

    public static function getComponenteOperacion($value, $tipo = 0)
    {
        $parametro = 'cc.num_requerimiento';
        if ($tipo == 1) {
            $parametro = 'cc.solicitud_tecnica_cms_id';
        }
        return \DB::table('componente_cms as cc')
                ->join(
                    'componente_operacion as co', 
                    'co.componente_id', '=', 'cc.id'
                )
                ->select(
                    'cc.descripcion',
                    'cc.numero_serie',
                    'cc.codigo_material',
                    \DB::raw('
                        case 
                            when co.interface = 0 then "USSD" 
                            when co.interface = 1 then "WEBPSI" 
                            when co.interface = 2 then "OFFICETRACK" 
                            else ""
                        end as interface'),
                    'co.operacion',
                    \DB::raw('ifnull(co.observacion, "") as observacion'),
                    \DB::raw('IF(co.indicador = 1, "Se envio operacion", "Error de envio") as indicador'),
                    'co.operacion',
                    \DB::raw('
                        case 
                            when co.indicador_procesamiento = 0 then "Fallido" 
                            when co.indicador_procesamiento = 1 then "Ok" 
                            else ""
                        end as estado_operacion'),
                    \DB::raw('IFNULL(co.observacion_respuesta, "") as observacion_respuesta'),
                    'co.created_at',
                    \DB::raw('ifnull(co.numser, "") as numser'),
                    \DB::raw('ifnull(co.codmat, "") as codmat'),
                    \DB::raw('ifnull(co.numserold, "") as numserold'),
                    \DB::raw('ifnull(co.codmatold, "") as codmatold'),
                    \DB::raw('ifnull(co.numserpar, "") as numserpar'),
                    \DB::raw('ifnull(co.codmatpar, "") as codmatpar')
                )
                ->where($parametro, $value)
                ->orderBy('co.id', 'desc')
                ->get();
    }

    public static function setAveriaComponente($averias = [], $idSolicitudTecnica)
    {
        $objOperacion = ComponenteOperacion::where("id_solicitud_tecnica", $idSolicitudTecnica)
                        ->whereRaw("(operacion = 'cambioAveria' or operacion = 'reposicion')")
                        ->get();
                        
        if (count($objOperacion) <= 0) {
            return;
        }

        if (count($averias) > 0) {
            foreach ($objOperacion as $value) {
                $componente = ComponenteCms::find($value->componente_id);
                if (!is_null($componente)) {
                    $componente['valor1'] = (isset($averias['valor2'])) ? $averias['valor2'] : '';
                    $componente['valor2'] = (isset($averias['valor3'])) ? $averias['valor3'] : '';
                    $componente->save();
                }
            }
        }
    }
}
