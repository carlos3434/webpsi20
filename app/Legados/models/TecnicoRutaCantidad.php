<?php namespace Legados\models;

class TecnicoRutaCantidad extends \Eloquent
{
    protected $guarded =[];
    //protected $fillable = ['aid'];
    protected $table = "rpt_tecnico_ruta_cantidad";
     
    public static function boot()
    {
        parent::boot();
        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
