<?php namespace Legados\models;

use Legados\models\ComponenteCms;
use Legados\models\Gestelprovisiondireccioncliente;
use Legados\models\Gestelprovisionfftt;
use Ofsc\Capacity;
use Legados\models\SolicitudTecnica;
use Legados\STGestelApi;
use Configuracion\models\Parametro;

class SolicitudTecnicaGestelProvision extends \Eloquent
{
    use \SolicitudTecnicaTrait;
    protected $guarded =[];
    protected $table = "solicitud_tecnica_gestel_provision";
    /*
    ALTA-PROVISION  LINEA
    ALTA-PROVISION-SPEEDY
    ALTA-PROVISION-TUP

    RUTINA-PROVISION  LINEA
    RUTINA -PROVISION-SPEEDY
    RUTINA -PROVISION-TUP
    */
    public static $rules =[
        'tipo_operacion' => 'required|in:ALT_PRV_LIN,ALT_PRV_SPE,ALT_PRV_TUP,RUT_PRV_LIN,RUT_PRV_SPE,RUT_PRV_TUP',
        'id_solicitud_tecnica' => 'required|between:1,11|regex:/^[0-9a-zA-Z]+$/',
        'zonal' => 'required|between:1,3|regex:/^[0-9a-zA-Z]+$/',
        'ciudad' => 'between:1,5|regex:/^[0-9a-zA-Z]+$/',
        'desc_ciudad' => 'between:1,20|regex:/^[0-9a-zA-Z]+$/',
        'inscripcion' => 'numeric|between:0,999999999999.99|regex:/^([0-9])/',

        'solicitud' =>'numeric|between:0,999999999|regex:/^([0-9])/',
        'numero_orden_servicio' =>'numeric|between:0,999999999|regex:/^([0-9])/',
        'fecha_solicitud' =>'size:10|date_format:d/m/Y',
        'fecha_formulacion' =>'size:10|date_format:d/m/Y',
        'fecha_programacion' =>'size:10|date_format:d/m/Y',
        'fecha_inicio' =>'size:10|date_format:d/m/Y',
        'estado_solicitud' =>'between:1,1',
        'desc_estado_solicitud' =>'between:1,30',
        'cod_servicio' =>'between:1,4|regex:/^[0-9a-zA-Z]+$/',
        'desc_servicio' =>'between:1,30',
        'cod_movimiento' =>'between:1,1|regex:/^[0-9a-zA-Z]+$/',
        'desc_movimiento' =>'between:1,20',
        'tipo_pedido' =>'between:1,3|regex:/^[0-9a-zA-Z]+$/',
        'desc_tipo_pedido' =>'between:1,25',
        'cod_agencia' =>'numeric|between:0,99999|regex:/^[0-9a-zA-Z]+$/',
        'desc_agencia' =>'between:1,30',
        'cod_clase' =>'between:1,3|regex:/^[0-9a-zA-Z]+$/',
        'desc_clase' =>'between:1,40',
        'cod_promocion' =>'between:1,3',
        'desc_promocion' =>'between:1,40',
        'cod_plan' =>'between:1,3',
        'desc_plan' =>'between:1,40',

        'tipo_paquete' => 'between:1,4',
        'escenario_gestel' => 'between:1,12',
        'escenario_cms' => 'between:1,12',
        'ind_envio' => 'between:1,1',
        'estado_planta_int' => 'between:1,1',
        'fecha_ejecucion_pin' => 'size:10|date_format:d/m/Y',
        'fecha_ejecucion_mdf' => 'size:10|date_format:d/m/Y',
        'contrata' => 'between:1,2',
        'desc_categoria' => 'between:1,999999999|regex:/^[0-9a-zA-Z]+$/',
        'ind_portin_portout' => 'between:1,1',
        'peticion' => 'numeric|between:0,9999999999.99|regex:/^([0-9])/',
        'agrupacion' => 'numeric|between:0,99999.99|regex:/^([0-9])/',
        'fecha_registro_peticion' => 'between:1,23',
        'usuario_registra_atis' => 'between:1,16',
        'cliente' => 'numeric|between:0,9999999999.99|regex:/^([0-9])/',
        'cuenta' => 'numeric|between:0,9999999999.99|regex:/^([0-9])/',
        'pc' => 'numeric|between:0,9999999999.99|regex:/^([0-9])/',

        'cod_titularidad' => 'between:1,999999999',
        'cod_unico_setem' => 'between:1,9',
        'tecnologia' => 'between:1,2',
        'id_inalambrica' => 'between:0,999999999|numeric',
        'numero_veces_reprog' => 'between:0,999999999|numeric',
        'licencia_municipal' => 'between:1,40',

        'codmot' => 'between:0,99999|numeric',
        'desobs' => 'between:1,150',
        'fecha_hora_devolucion' => 'between:1,16',
        'registro_pai' => 'between:1,19',
        'estado_pai' => 'between:1,1',

        'req_cms' => 'between:1,15|alpha_num',
        'estado_req_cms' => 'between:1,4',

        'valor1' => 'between:0,20|regex:/^[0-9a-zA-Z]+$/',
        'valor2' => 'between:0,20|regex:/^[0-9a-zA-Z]+$/',
        'valor3' => 'between:0,20|regex:/^[0-9a-zA-Z]+$/',
        'valor4' => 'between:0,20|regex:/^[0-9a-zA-Z]+$/',
        'valor5' => 'between:0,20|regex:/^[0-9a-zA-Z]+$/',

        's_cod_plan' => 'between:1,3',
        's_desc_plan' => 'between:1,40',
        's_tipo_plan' => 'between:1,3',
        's_cod_camp' => 'between:1,4',
        's_desc_camp' => 'between:1,40',
        's_desc_tipo_modem' => 'between:1,30',
        's_cod_modalidad' => 'between:1,2',
        's_desc_modalidad' => 'between:1,30',
        's_cod_servicio_dslam' => 'between:1,4',
        's_desc_servicio_dslam' => 'between:1,40',
        's_tipser' => 'between:1,3',
        's_desc_tipser' => 'between:1,30',

        'cod_speedy' => 'between:1,5',
        'cod_anterior_speedy' => 'between:1,5',
        'velocidad_speedy' => 'between:1,999999999',
        'unidad_bajada' => 'between:1,2',
        'promocion' => 'between:1,3',
        'desc_promocion' => 'between:1,40',
        'cod_isp' => 'between:1,3',
        'desc_isp' => 'between:1,40',
        'vpi_modem' => 'between:1,5',
        'vci_modem' => 'between:1,5',
        'fecha_instalacion_router' => 'between:1,19',

        'tipo_doc' => 'between:1,1',
        'desc_tipo_doc' => 'between:1,30',
        'numero_doc' => 'between:1,15',
        'nombres' => 'between:1,30',
        'ape_paterno' => 'between:1,60',
        'ape_materno' => 'between:1,30',
        'cuc' => 'between:1,9',
        'segmento_cliente' => 'between:1,2',
        'desc_segmento' => 'between:1,30',
        'subsegmento_cliente' => 'between:1,2',
        'desc_subsegmento_cliente' => 'between:1,30',

        'nombres_contacto' => 'between:1,64',
        'ape_pater_contacto' => 'between:1,40',
        'ape_mater_contacto' => 'between:1,40',
        'telefono_contacto' => 'between:1,20',
        'telefono_referencia' => 'numeric|between:0,999999999',
        'celular' => 'numeric|between:0,999999999'
    ];
    public static $rules_retorno = [
        'tipo_operacion' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,20})$/i',
        'solicitud_tecnica' => 'required|regex:/^([a-zA-Z]{1}[0-9]{10})/',
        'fecha_envio' => 'required|regex:/^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})/',
        'hora_envio' => 'required|regex:/^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})/',
        'cod_envio' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,9999999990})$/i',
        'descripcion' => 'required|regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,9999999990})$/i'
    ];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });

        static::updated(function ($table) {
            //\Event::fire('stgestel_provision.updated', $table);
        });
    }

    public function componentes()
    {
        return $this->hasMany('Legados\models\ComponenteGestel', 'solicitud_tecnica_gestel_id');
    }
    public function direccioncliente()
    {
        return $this->hasMany('Legados\models\Gestelprovisiondireccioncliente', 'solicitud_tecnica_gestel_provision_id');
    }
    public function fftt()
    {
        return $this->hasMany('Legados\models\Gestelprovisionfftt', 'solicitud_tecnica_gestel_provision_id');
    }
    public function ffttpin()
    {
        return $this->hasMany('Legados\models\Gestelprovisionffttpin', 'solicitud_tecnica_gestel_provision_id');
    }
    public function ffttcobre()
    {
        return $this->hasMany('Legados\models\Gestelprovisionffttcobre', 'solicitud_tecnica_gestel_provision_id');
    }
    public function ffttadsl()
    {
        return $this->hasMany('Legados\models\Gestelprovisionffttadsl', 'solicitud_tecnica_gestel_provision_id');
    }
    public function dominio()
    {
        return $this->hasMany('Legados\models\Gesteldominio', 'solicitud_tecnica_gestel_id');
    }
    public function dominiogestel()
    {
        return $this->hasMany('Legados\models\Gesteldominio', 'solicitud_tecnica_gestel_id')
                    ->where('gestel_dominio.actividad_id',2);
    }
    public function productos()
    {
        return $this->hasMany('Legados\models\Gestelproductos', 'solicitud_tecnica_gestel_id');
    }
    public function productosadquiridos()
    {
        return $this->hasMany('Legados\models\Gestelproductos', 'solicitud_tecnica_gestel_id')
                    ->where('gestel_productos_adquiridos.actividad_id',2);
    }
    public function solicitudTecnica()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnica');
    }
    /**
     * valida si la actividad esta configurada dentro de la parametrizacion
     * para trabajar en go,
     * @param $proveniencia: puede ser CMS, GESTEL
     */
    public function validarParam($array = [])
    {
        $parametros = Parametro::getParametroValidacion($array);
        $filtro='0';
        $valida=true; //si no hay $parametros;
        $this->mensaje='';
        $e=1;
        foreach ($parametros as $key => $value) {
            $valida=true;
            $trama=explode("||", $value->trama);
            $detalle=explode("||", $value->detalle);

            for ($i=0; $i <count($trama); $i++) {
                $subdetalle=explode(",", $detalle[$i]);
                if ($trama[$i]!='' && isset($this->$trama[$i])) {
                    if (array_search($this->$trama[$i], $subdetalle)===false) {
                        $valida=false;
                        $this->mensaje.=$e.'.- El campo "'.$trama[$i].'": ['.$this->$trama[$i].'] no está entre: ['.$detalle[$i].'] || ';
                        $e++;
                    }
                    if ($valida==false) {
                        break;
                    }
                }
            }
            if ($valida===true) {
                $this->mensaje='';
                break;
            }
        }
        return $valida;
    }
    public function getFullNameAttribute()
    {
    //cliente
        return "$this->ape_paterno $this->ape_materno, $this->nombres";
    }
    public function getFullContactAttribute()
    {
    //contacts
        return "$this->ape_pater_contacto $this->ape_mater_contacto, $this->nombres_contacto";
    }
/*
    public function getFechaRegReqAttribute()
    {
        if (strlen($this->fecha_registro_requerimiento) > 0) {
            return \DateTime::CreateFromFormat('d/m/Y', $this->fecha_registro_requerimiento);
        }
    } */
    public function getTimeOfBookingAttribute()
    {
        $fecha = "";
        if ($this->fecha_registro_peticion!= null) {
            $fecha=substr($this->fecha_registro_peticion, 0, 19);
            $d=\DateTime::createFromFormat('Y-m-d H:i:s', $fecha);
            if(!$d) 
                $fecha="";
        }
        return $fecha;
    }
    /*public function getFullAddressAttribute()
    {
        return "$this->tipo_via $this->nombre_via, $this->numero_via".
        "$this->piso $this->interior $this->manzana $this->lote $this->etapa".
        "$this->sector_servicio $this->nombre_playa $this->kilometro ".
        "$this->tipo_urbanizacion $this->desc_urbanizacion";
    }*/
    //latitud X -77.027738 , longitud Y -12.123496
    public function setQuadrant($citax, $citay)
    {
        if ($citax!=0 and $citay!=0) {
            $cellsize = \Config::get("validacion.cellsize"); //kilometers
            $pivotY =  \Config::get("validacion.pivot.longitude");//y
            $pivotX= \Config::get("validacion.pivot.latitude");//x
            $xgrid = round((1000 / 9) * cos($citax * pi() / 180) * ($citay - $pivotY) / $cellsize);
            $ygrid = round((1000 / 9) * ($citax - $pivotX) / $cellsize);
            return $xgrid.":".$ygrid;
        } else {
            return 0;
        }
    }

    public function getWorkZone($ffttcobreObj=[])
    {
        $ffttcobreObj=$this->ffttcobre;
        $workZone = null;
        $mdf=$armario=$cable=null;
        
        foreach ($ffttcobreObj as $key => $value) {
           if($mdf==null and $value['cabecera_mdf']!=''){
                $mdf=$value['cabecera_mdf'];
                $armario=$value['cod_armario'];
                $cable=$value['cable'];
                break;
           }
        }

        if ($mdf !='') {
            if ($armario != "") {
                $workZone = $mdf."_".$armario;
            } elseif ($cable != "") {
                $workZone = $mdf."_".$cable;
            }
        }

        $this->workZone = $workZone;
        return $workZone;
    }
    /**
     * metodo que consulta la capacidad almacenada en redis,
     * en su defecto en toa caso contrario devuelve texto de error
     */
    public function capacityOfsc()
    {
        $quiebre = \Quiebre::find($this->quiebre_id);

        $quiebreGrupo = $quiebre->quiebregrupos;
        $actividadTipo = \ActividadTipo::find($this->actividad_tipo_id);
        $hoy = date("Y-m-d", strtotime(date("Y-m-d") . ' +1 day'));

        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            && !is_null($quiebre)) {
            $workType = $actividadTipo->label;
            $llave = $hoy.'|AM|'.$workType.'|'.$quiebre->apocope.'|'.$this->workZone;
            $rst = \Redis::get($llave);
            $capacidad = json_decode($rst, true);
            
            if (!isset($capacidad["location"])) {
                $llave = $hoy.'|PM|'.$workType.'|'.$quiebre->apocope.'|'.$this->workZone;
                $rst = \Redis::get($llave);
                $capacidad = json_decode($rst, true);
            }
            
            if (isset($capacidad["location"])) {
                return [
                    'external_id'   =>$capacidad["location"],
                    'quiebre'       =>$quiebre->apocope,
                    'quiebreGrupo'  =>$quiebreGrupo->nombre,
                    'workType'      =>$workType,
                    'workZone'      =>$this->workZone,
                    'duration'      =>$actividadTipo->duracion,
                    'sla'           =>$actividadTipo->sla
                ];
            } else {
                //conultar la capacidad directamente a oracle
                // \Config::set("ofsc.auth.company", "telefonica-pe.test");
                $data=[
                        'fecha'          => $hoy,
                        'time_slot'      => '',
                        'worktype_label' => $workType,
                        'quiebre'        => $quiebre->apocope,
                        'zona_trabajo'   => $this->workZone,
                    ];
                $capacity = new Capacity();
                
                $response = $capacity->getCapacity($data);
                if (isset($response->error) && $response->error===false
                    && isset($response->data->capacity)) {

                    $capacidad = $response->data->capacity;
                    $timeSlot = $response->data->time_slot_info;
                    foreach ($capacidad as $key => $val) {
                        if (isset($val->location)) {
                            $val->{"time_slot_info"} = $timeSlot;
                            $clave=$val->date.'|'.
                                    $val->time_slot.'|'.
                                    $workType.'|'.
                                    $quiebre->apocope.'|'.
                                    $this->workZone;
                            \Redis::set($clave, json_encode($val));

                            return [
                                'external_id'   =>$val->location,
                                'quiebre'       =>$quiebre->apocope,
                                'quiebreGrupo'  =>$quiebreGrupo->nombre,
                                'workType'      =>$workType,
                                'workZone'      =>$this->workZone,
                                'duration'      =>$actividadTipo->duracion,
                                'sla'           =>$actividadTipo->sla
                            ];
                        }
                    }
                } elseif (is_string($response)) {
                    return $response;
                }
                return "SIN BUCKET";
            }
        } else {
            return "SIN ACTIVIDAD TIPO";
        }
    }

    public function getQuiebreId($ffttcobreObj)
    {
        $quiebreId=null;
        $mdf=null;
        
        foreach ($ffttcobreObj as $key => $value) {
           if($mdf==null and $value['cabecera_mdf']!=''){
                $mdf=$value['cabecera_mdf'];
                break;
           }
        }

        switch ($mdf) {
            case 'LL':
            case 'SP':
            case 'LM':
                    $result = \DB::table("webpsi_coc.prov_bas_tiposact")
                        ->where(["codser" => $this->cod_servicio,
                            "prefijo" => $this->tipo_pedido,
                            "movimiento" => $this->cod_movimiento])
                        ->first();
                    if (!is_null($result)) {
                        $quiebreId= 25;
                        $this->quiebre_id = $quiebreId;
                        return $quiebreId;
                    }
                break;    
            default:
                break;
        }
    
        if($this->ind_portin_portout=='I')
            $quiebreId=11;
        
        $this->quiebre_id = $quiebreId;
        return $quiebreId;

    }
    public function getActividadTipoId()
    {
        $actividadTipoId = null;
        $result = \DB::table("webpsi_coc.prov_bas_tiposact")
            ->where(["codser" => $this->cod_servicio,
                "prefijo" => $this->tipo_pedido,
                "movimiento" => $this->cod_movimiento])
            ->first();
        if (!is_null($result)) {
            $actividadTipoId = $result->actividad_toa;
        }

        $this->actividad_tipo_id = $actividadTipoId;
        return $actividadTipoId;
    }
    public function getActividadId()
    {
        return 2;
    }

    /**
     *
     */
    public function parsearFechas()
    {
        $datetime = 'd/m/Y H:i:s';
        $date = 'd/m/Y';

        if (strlen($this->fecha_solicitud) > 0) {
            $this->fecha_solicitud = \DateTime::CreateFromFormat($date, $this->fecha_solicitud);
        }
        if (strlen($this->fecha_formulacion) > 0) {
            $this->fecha_formulacion = \DateTime::CreateFromFormat($date, $this->fecha_formulacion);
        }
        if (strlen($this->fecha_programacion) > 0) {
            $this->fecha_programacion = \DateTime::CreateFromFormat($date, $this->fecha_programacion);
        }
        if (strlen($this->fecha_inicio) > 0) {
            $this->fecha_inicio = \DateTime::CreateFromFormat($date, $this->fecha_inicio);
        }
        if (strlen($this->fecha_ejecucion_pin) > 0) {
            $this->fecha_ejecucion_pin = \DateTime::CreateFromFormat($date, $this->fecha_ejecucion_pin);
        }
        if (strlen($this->fecha_ejecucion_mdf) > 0) {
            $this->fecha_ejecucion_mdf = \DateTime::CreateFromFormat($date, $this->fecha_ejecucion_mdf);
        }
        if (strlen($this->fecha_registro_peticion) > 0) {
            $this->fecha_registro_peticion = \DateTime::CreateFromFormat($datetime, $this->fecha_registro_peticion);
        }
        if (strlen($this->registro_pai) > 0) {
            $this->registro_pai = \DateTime::CreateFromFormat($datetime, $this->registro_pai);
        }
        if (strlen($this->fecha_hora_devolucion) > 0) {
            $this->fecha_hora_devolucion = \DateTime::CreateFromFormat($datetime, $this->fecha_hora_devolucion);
        }
        if (strlen($this->fecha_instalacion_router) > 0) {
            $this->fecha_instalacion_router = \DateTime::CreateFromFormat($datetime, $this->fecha_instalacion_router);
        }

    }

    public function liquidacionGestelProvicion()
    {
        $this->setTipoOperacion("LIQUIDACION");
        $this->setTramaComponentes();
        $elementos = $this->setTrama();
        $this->enviarLegado($elementos);
    }

    public function devolucionGestelProvicion()
    {
        $this->setTipoOperacion("DEVOLUCION");
        $this->setTramaComponentes();
        $elementos = $this->setTrama();
        $this->enviarLegado($elementos);
    }

    public function setTipoOperacion($operacion)
    {
        $operacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                        ->where('tipo_legado', 2)
                        ->where('tipo', 0)
                        ->where('actividad_id', 2)
                        ->whereRaw('tipo_operacion like "%'.$this->tipo_operacion.'%"')
                        ->first();
        $this->tipo_operacion = "";
        if (!is_null($operacionObj)) {
            $this->tipo_operacion = $operacionObj->codigo;
        }
    }

    public function setTramaComponentes()
    {
        $componentesArray = [];
        foreach ($this->componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => $value->tipo_equipo,
                'codigo_equipo'         => $value->codigo_equipo,
                'descripcion_equipo'    => $value->descripcion_equipo,
                'cod_material'          => $value->cod_material,
                'cantidad_material'     => $value->cantidad_material,
                'unidad_medida'         => $value->unidad_medida,
                'nro_serie'             => $value->nro_serie,
                'codigo_barras'         => $value->codigo_barras,
                'tipo_transaccion'      => $value->tipo_transaccion,
                'marca_equipo'          => $value->marca_equipo,
                'modelo_equipo'         => $value->modelo_equipo,
                'mantenimiento_datos'   => $value->mantenimiento_datos,
                'numero_cabina'         => $value->numero_cabina,
                'dato1'                 => $value->dato1,
                'dato2'                 => $value->dato2,
                'dato3'                 => $value->dato3,
                'dato4'                 => $value->dato4,
                'dato5'                 => $value->dato5,
            ];
        }
        $this->componentes = $componentesArray;
    }

    public function setTrama()
    {
        $fechaProgramacion =    '19-07-17';
        $fechaEmision =         '19-07-17';
        $fechaEjecucion =       '19-07-17';
        $horaInicio =           '15:25';
        $horaFin =              '16:40';
        $fechaDesplazamiento =  '12:10';
        $fechaDevolucion =      '12:10 19-07-17';

        $elementos = [
            'solicitud_tecnica_id'      => $this->solicitud_tecnica_id,
            'tipo_operacion'            => $this->tipo_operacion,
            'num_solicitud_tecnica'     => $this->id_solicitud_tecnica,
            'id_solicitud_tecnica'      => $this->id_solicitud_tecnica,
            'fecha_programacion'        => $fechaProgramacion,
            'contrata'                  => $this->contrata,
            'tecnico1'                  => isset($this->solicitudTecnica->ultimo->tecnico->carnet) ? 
                                           $this->solicitudTecnica->ultimo->tecnico->carnet : NULL,
            'tecnico2'                  => '',
            'fecha_emision_boleta'      => $fechaEmision,
            'nombre_persona_recibe'     => $this->nombres." ".$this->ape_paterno." ".$this->ape_materno,
            'numero_doc_identidad'      => $this->numero_doc,
            'observacion'               => $this->solicitudTecnica->ultimo->xa_observation,
            'parentesco'                => "",
            'indicador_fraude'          => "",
            'anexos'                    => "",
            'fecha_ejecucion'           => $fechaEjecucion,
            'hora_inicio'               => $horaInicio,
            'hora_fin'                  => $horaFin,
            'desplazamiento'            => $fechaDesplazamiento,
            'escenario'                 => "",
            'cabina'                    => "",
            'ambiente'                  => "",
            'cod_mot'                   => "",
            'des_obs'                   => $this->solicitudTecnica->ultimo->xa_observation,
            'fecha_hora_devolucion'     => $fechaDevolucion,
            'dato1'                     => $this->valor1,
            'dato2'                     => $this->valor2,
            'dato3'                     => $this->valor3,
            'dato4'                     => $this->valor4,
            'dato5'                     => $this->valor5,
            'DatosMateriales'           => $this->componentes
        ];

        return $elementos;
    }

    public function enviarLegado($elementos)
    {
        // dd(json_encode($elementos));
        $objStapi = new STGestelApi;
        $response = $objStapi->cierreAveria($elementos);
    }

    public function getMateriales()
    {
        $inventarios=[];

        /*$empresa=\Empresa::where('cms', $this->codigo_contrata)
                 ->first();*/
        //if (!is_null($empresa)) {
            $material=\MaterialActividad::where('actividad_id', 2)
                    //->where('empresa_id', $empresa->id)
                    ->where('tipo_legado', 2)
                    ->with('material')
                    ->get();
            foreach ($material as $key => $value) {
                $invarray=array();
                $invarray['invtype_label']=$value->material['codmat'];
                $invarray['quantity']=$value->cantidad;
                $invarray['XI_BRAND']="";
                $invarray['XI_MODEL']="";
                array_push($inventarios, $invarray);
            }
        //}
        return $inventarios;
    }
}
