<?php namespace Legados\models;

use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnica;

class SolicitudTecnicaMovimiento extends \Eloquent
{
    protected $guarded =[];
    protected $table = "solicitud_tecnica_movimiento";

    public static $rules = [
        'bucket_id'                     => 'required',
        'xa_note'                       => 'required',
        'xa_observation'                => 'required',
        'a_receive_person_id'           => 'required',
        'a_receive_person_name'         => 'required',
        'start_time'                    => 'required',
        'end_time'                      => 'required',
        'end_date'                      => 'required',
        'resource_id'                   => 'required',
        'tipo_envio_ofsc'               => 'required',
        'coordx_tecnico'                => 'required',
        'estado_aseguramiento'          => 'required',
        'coordy_tecnico'                => 'required',
        'estado_legado'                 => 'required',
        'estado_st'                     => 'required',
        'solicitiud_tecnica_id'         => 'required',
        'estado_ofsc_id'                => 'required',
        'motivo_ofsc_id'                => 'required',
        'submotivo_ofsc_id'             => 'required',
        'actividad_id'                  => 'required',
        'actividad_tipo_id'             => 'required',
        'quiebre_id'                    => 'required',
        'horario_id'                    => 'required',
        'dia_id'                        => 'required',
        'fecha_agenda'                  => 'required',
        'tecnico_id'                    => 'required',
        'sistema_externo_id'            => 'required',
        'time_of_assignment'            => 'required',
        'time_of_booking'               => 'required',
        'duration'                      => 'required',
        'intervalo'                     => 'required',
        'delivery__window_start'        => 'required',
        'delivery__window_end'          => 'required',
        //'tipo_autorizacion_id'          => '',
        //'usuario_autorizacion'          => '',
        //'fecha_autorizacion'            => ''
    ];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
    /**
     * relationship to SolicitudTecnica
     */
    public function solicitudTecnica()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnica');
    }
    public function detalle()
    {
        return $this->hasMany('Legados\models\SolicitudTecnicaMovimientoDetalle')
                    ->leftjoin(
                        'usuarios as u',
                        'solicitud_tecnica_movimiento_detalle.usuario_created_at',
                        '=',
                        'u.id'
                    )
                    ->select('solicitud_tecnica_movimiento_detalle.*', 'u.usuario as usuariodetalle', 'u.apellido', 'u.nombre');
    }
    public static function getcontarvisita($condicion)
    {
        $ultimo = SolicitudTecnica::getUltimo()->where('num_requerimiento', $condicion['codactu']);
        if (!is_null($condicion["st"])) {
            $ultimo->where("solicitud_tecnica.id_solicitud_tecnica", $condicion["st"]);
        }
        $ultimo = $ultimo->orderBy('u.updated_at', 'desc')
                    ->first();

        $visitas = SolicitudTecnicaMovimiento::where('solicitud_tecnica_id', $ultimo->solicitud_tecnica_id)
                ->where('estado_ofsc_id', $condicion['estado'])
                ->where($condicion['razon'], $condicion['valor'])
                ->count();

        return [
            'tipo_envio_ofsc'       => $ultimo->tipo_envio_ofsc,
            'quiebre_id'            => $ultimo->quiebre_id,
            'id_solicitud_tecnica'  => $ultimo->id_solicitud_tecnica,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id,
            'actividad_tipo_id'     => $ultimo->actividad_tipo_id,
            'actividad_id'          => $ultimo->actividad_id,
            'nenvio'                => $ultimo->nenvio,
            'visitas'               => $visitas
        ];
    }
}
