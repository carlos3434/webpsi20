<?php namespace Legados\models;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
class SolicitudCaida extends \Eloquent 
{
	use SoftDeletingTrait;

    protected $table ="solicitud_tecnica_caida";
    protected $fillable = [];
	protected $dates = ['deleted_at'];

	public static function boot()
    {
        parent::boot();

        static::saving(function ($table)
        {
            $table->usuario_created_at = \Auth::id();
        });

        static::updating(function ($table)
        {
            $table->usuario_updated_at = \Auth::id();
        });

        static::deleted(function ($table)
        {
            $table->usuario_deleted_at = \Auth::id();
        });
    }

}