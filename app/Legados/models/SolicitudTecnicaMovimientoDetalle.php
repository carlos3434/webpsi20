<?php namespace Legados\models;

class SolicitudTecnicaMovimientoDetalle extends \Eloquent
{
    protected $guarded =[];
    protected $table = "solicitud_tecnica_movimiento_detalle";

    public static $rules = [];
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
    /**
     * relationship to SolicitudTecnicaUltimo
     */
    public function ultimo()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnicaUltimo');
    }

    public function tecnico()
    {
        return $this->hasOne('Tecnico', 'id', 'tecnico_id');
    }

}
