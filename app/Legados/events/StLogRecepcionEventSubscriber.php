<?php namespace Legados\events;

use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\PsiInformacion as PsiInformacion;

class StLogRecepcionEventSubscriber
{
 
  /**
   * When a solicitud tecnica cms is created
   */
  public function onCreate($event)
  {
  }
 
  /**
   * When a solicitud tecnica cms is updated
   */
  public function onUpdate($event)
   {
    $id = $event->solicitud_tecnica_id;
    $logst = \Cache::get("logst{$id}");
    if (!$logst) {
      $logst = \Cache::remember(
        "logst{$id}",
          24*60*60,
          function () use ($id) {
            return STLogRecepcion::select(
              "solicitud_tecnica_log_recepcion.*",
              "le.message AS detalle_error"
              )->where(["solicitud_tecnica_id" => $id])
                ->leftJoin(
                    "lego_errores AS le",
                    "le.code",
                    "=",
                    "solicitud_tecnica_log_recepcion.code_error"
            )->groupBy("id")
            ->orderBy("id", "DESC")->get();      
          }
      );
    }
  }

  public function onRegisterPsiInfo($datos)
  {
    $hora_actual = date('Y-m-d H');
    $minuto_actual = date('i');
    if ($minuto_actual < 29) {
      $fechaInicio = $hora_actual. ':00:00';
      $fechaFin = $hora_actual. ':29:00';
    } else {
      $fechaInicio = $hora_actual. ':30:00';
      $fechaFin = $hora_actual. ':59:00';
    }
    $registroPsi = PsiInformacion::where('fecha_inicio', '=', $fechaInicio)
                  ->where('fecha_fin', '=', $fechaFin)
                  ->first();
    if ($registroPsi) {
      //actualizar
      $data = [
        'fecha_inicio' => $fechaInicio,
        'fecha_fin' => $fechaFin,
        'fecha_fin' => $fechaFin,
        'solicitud_ok' => 0,
        'chart'=> $datos['0'],
        'solicitud_error_trama' => 0,
        'solicitud_no_procesada' => 0,
        'solicitud_envio_legado' => 0
      ];
      $registroPsi::update($data);

    } else {
      $data = [
        'fecha_inicio' => $fechaInicio,
        'fecha_fin' => $fechaFin,
        'fecha_fin' => $fechaFin,
        'chart'=> $datos['0'],
        'solicitud_ok' => 0,
        'solicitud_error_trama' => 0,
        'solicitud_no_procesada' => 0,
        'solicitud_envio_legado' => 0
      ];
      PsiInformacion::create($data);
    }
  }
 
  /**
   * Register the listeners for the subscriber.
   *
   * @param  Illuminate\Events\Dispatcher  $events
   * @return array
   */
  public function subscribe($events)
  {
 
    $events->listen('stlogrecepcion.updated', 'StLogRecepcionEventSubscriber@onUpdate');
    $events->listen('stlogrecepcion.registerPsiInfo', 'Legados\events\StLogRecepcionEventSubscriber@onRegisterPsiInfo');
  }
 
}