<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * 
 */
class ProyectoEstado extends \Eloquent
{

    public $table = "proyectos_estados";

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    //protected $fillable = array('*');
    protected $guarded = array();

    /**
     * GestionEdificio relationship
     */
    public function gestionEdificio()
    {
        return $this->hasMany('GestionEdificio');
    }

    public function getSiguienteAnteriorEstado($proyectoEstadoId)
    {
        $result = \DB::table('proyectos_estados')
                ->select(
                    'id', 'siguiente', 'anterior', 
                    'proyecto_tipo_id as proyectoTipoId'
                )
                ->where('id', $proyectoEstadoId)
                ->where('estado', '1')
                ->orderby('id', 'desc')
                ->first();
        return $result;
    }

    /**
     * usar: ProyectoEstado::getListaRelacionConTipo(true)
     * @param boolean $coleccion
     * @return array
     */
    public static function getListaRelacionConTipo($coleccion = false)
    {
        $result = \DB::table('proyectos_estados AS pe')
                ->select(
                    'pe.id AS estado_id', 'pe.nombre AS estado', 
                    'proyecto_tipo_id AS tipo_id', 'pt.nombre AS tipo'
                )
                ->join(
                    'proyecto_tipo AS pt', 'pe.proyecto_tipo_id', '=', 'pt.id'
                )
                ->get();
        if ($coleccion === true) {
            $coleccion = \Illuminate\Support\Collection::make($result);
            $result = $coleccion->keyBy('estado_id');
        }
        return $result;
    }

}
