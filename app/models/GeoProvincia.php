<?php 
class GeoProvincia extends Eloquent
{
    public static function postDepartamentoFiltro()
    {
        $r  =   DB::table('geo_provincia')
          ->select(
              'departamento as id',
              DB::raw('departamento as nombre')
          )
         ->groupBy('departamento')
         ->get();
        return $r;
    }

    public static function postProvinciaFiltro($array)
    {
        $r  =   DB::table('geo_provincia')
                  ->select(
                      'provincia as id',
                      DB::raw("provincia as nombre ")
                      //DB::raw("CONCAT('D',departamento) as relation")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->groupBy('provincia')
                  ->get();
        return $r;
    }

    public static function postProvinciaCoord($array)
    {
        $r  =   DB::table('geo_provincia')
                  ->select(
                      'provincia as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(departamento,'-',provincia) as detalle")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->orderBy('provincia', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }

     public static function postCoord($array)
    {
        $r  =   DB::table('geo_provincia')
                  ->select(
                      'provincia as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(departamento,'-',provincia) as detalle")
                  )
                  ->where('departamento', '=', $array[0])
                  ->where('provincia', '=', $array[1])
                  ->orderBy('provincia', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }
  
}
