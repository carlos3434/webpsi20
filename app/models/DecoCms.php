<?php 
use Legados\Componente;
class DecoCms {
    public static $rules_refresh = [

         'indorigreq' => 'required|in:A,I',
         'codmat' => 'required|numeric',
         'numser' => 'required|numeric',
    ];
    /**
     * ObtenerPorRango Function
     * Use test soap service
     * @param object $request
     * @return object $employees
     */
    public function refresh($request){

        $peticion = [
            'codreq'                => '10000000',
            'indorigreq'            => $request->indorigreq,
            'codmat'                => $request->codmat,
            'numser'                => $request->numser,
            'codtar'                => '07870141',
            'codact'                => Config::get("legado.user_operation"),
            'solicitud_tecnica_id'  => 0
        ];
        $validator = Validator::make((array)$request, self::$rules_refresh);
        if ($validator->fails()) {
            throw new SoapFault('SOAP-ENV:Client', $validator->messages()->all()[0]);
        }
        $response= new stdClass();
        $componente = new Componente;
        $respuesta = $componente->refresh($peticion);
        if ($respuesta->error==false && isset($respuesta->data) ){

            $response->error = false;
            $response->errorMsg = '';
            $response->codreq = $respuesta->data->codreq;
            $response->indorigreq = $respuesta->data->indorigreq;
            $response->indicador = $respuesta->data->indicador;
            $response->idreg = $respuesta->data->idreg;
            $response->observacion = $respuesta->data->observacion;
        } else {

            $respuesta->error = true;
            $respuesta->errorMsg = '';
            
        }

        return $response;
    }
}