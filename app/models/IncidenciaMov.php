<?php

class IncidenciaMov extends \Eloquent {
	public $table = 'incidencia_mov';
	protected $fillable = ['incidencia_id','observacion','estado_validacion','sistema_id','asociado_id','estado','fecha_entrega','usuario_responsable_id','nivel_impacto'];

	 public static function boot()
  	{
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
  	}

}