<?php

class QuiebreGrupoUsuario extends \Eloquent {
	protected $fillable = [];
	protected $table = "quiebre_grupo_usuario";
	
	public function getAllUsersbyquiebregrupo(){
		$query = DB::table('quiebre_grupo_usuario as qgu')
				->join('usuarios as u','u.id','=','qgu.usuario_id')
				->Leftjoin('tipo_persona as tp','tp.id','=','u.tipo_persona_id')	
                ->select(
                	'u.id as usuario_id',
                    DB::raw('CONCAT_WS(u.nombre,u.apellido," ") as usuario'),
                    'u.celular',
                    'u.dni',
                    'tp.nombre'                    
                )
                ->whereRaw('(select count(*) as existe from usuario_area where area_id=51 and usuario_id=qgu.usuario_id) = 0')
                ->where('qgu.quiebre_grupo_id',10)
                ->where('qgu.estado', 1)
                ->get();
        return $query;
	}
}