<?php

class ZonaPremiumCatv extends \Eloquent {
  use DataViewer;
	public $table = 'zona_premium_catv';
  
	protected $fillable = ['zona','nodo','troba','contrata','fecha_registro','estado','usuario_created_at',
	'usuario_updated_at'];

    public static function boot()
    {
       parent::boot();

    }
}