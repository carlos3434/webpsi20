<?php

class UsuarioCantSolicitudes extends \Eloquent
{
	use DataViewer;
	public $table = 'usuario_cant_solicitudes';
	
	public function user(){
		return $this->belongsTo('Usuario');
	}

    
}