<?php 
class GeoDireccionCoord extends Eloquent
{
    protected $table = "geo_direccion_coord";

    protected $guarded =[];
    
    protected $dates = ['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function direccion()
    {
        return $this->belongsTo('GeoDireccion');
    }

}