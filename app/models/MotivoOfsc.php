<?php

class MotivoOfsc extends Base
{

    public $table = 'motivos_ofsc';
    public static $where =
        ['id', 'codigo_ofsc', 'estado_ofsc', 'descripcion', 'estado', 'tipo_tratamiento_legado', 'tipo_legado', 'tipo_tecnologia', 'codigo_legado', 'actividad_id'];
    public static $selec =
        ['id', 'codigo_ofsc', 'estado_ofsc', 'descripcion', 'estado', 'tipo_tratamiento_legado', 'tipo_legado', 'tipo_tecnologia', 'codigo_legado', 'actividad_id', 'estado_ofsc_id'];

//    public function __construct()
//    {
//        $this->table = "motivos_ofsc";
//    }

    public static function postMotivoTipo()
    {
        $tipo=Input::get('motivo_cancel');
        $actividad=Input::get('actividad_id');

        $r  =   DB::table('motivos_ofsc')
                ->select(
                    'codigo_ofsc',
                    DB::raw('concat(id,"|",codigo_ofsc) as id'),
                    DB::raw('descripcion as nombre')
                )
                  ->where('estado', '=', '1')
                  ->where('estado_ofsc', '=', $tipo)
                  ->where('actividad_id', '=', $actividad)
                  ->orderBy('descripcion', 'asc')
                  ->get();
        return $r;
    }

    public function getMotivoOfsc() // Lista de MotivosOfc añadido
    {
        return DB::table('motivos_ofsc AS m')
                ->leftJoin(
                    'actividades AS a',
                    'a.id', '=', 'm.actividad_id'
                )
                ->leftJoin(
                    'estados_ofsc as eo',
                    'eo.id', '=', 'm.estado_ofsc_id'
                )
                ->select(
                        'm.id',
                        'm.codigo_ofsc',
                        'codigo_legado',
                        'descripcion',
                        'm.estado',
                        'a.nombre as actividad',
                        DB::raw('case 
                                when m.estado_ofsc = "T" then "TECNICO" 
                                when m.estado_ofsc = "C" then "COMERCIAL" 
                                when m.estado_ofsc = "S" then "SOPORTE DE CAMPO" 
                                else ""
                                end as estado_ofsc'),
                        DB::raw('case 
                                when tipo_legado = 1 then "CMS" 
                                when tipo_legado = 2 then "GESTEL" 
                                else ""
                                end as tipo_legado'),
                        'eo.nombre as nombre_ofsc',
                        DB::raw('if(tipo_tratamiento_legado = 1, "AUTOMATICO", "SIGUE FLUJO") as tipo_tratamiento_legado_descripcion')
                )
                ->get();
    }


}
