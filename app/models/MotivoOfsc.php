<?php

class MotivoOfsc extends Base
{

    public $table = 'motivos_ofsc';
    public static $where = 
        ['id', 'codigo_ofsc', 'estado_ofsc', 'descripcion', 'estado'];
    public static $selec = 
        ['id', 'codigo_ofsc', 'estado_ofsc', 'descripcion', 'estado'];

//    public function __construct()
//    {
//        $this->table = "motivos_ofsc";
//    }

    public static function postMotivoTipo()
    {
        $tipo=Input::get('motivo_cancel');
        $actividad=Input::get('actividad_id');

        $r  =   DB::table('motivos_ofsc')
                ->select(
                    'codigo_ofsc',
                     DB::raw('concat(id,"|",codigo_ofsc) as id'),
                     DB::raw('descripcion as nombre')
                )
                  ->where('estado', '=', '1')
                  ->where('estado_ofsc', '=',$tipo)
                  ->where('actividad_id', '=',$actividad)
                  ->orderBy('descripcion', 'asc')
                  ->get();
        return $r;
    }

}