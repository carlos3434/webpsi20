<?php

class AsistenciaTecnicoOfscDetalle extends \Eloquent
{
    protected $table = "asistencia_tecnico_ofsc_detalle";
    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = Auth::id();
            }
        );
    }
    /*public function setEstado()
    {
        $horaactual = date("Y-m-d H:i:s");

        $tiempoactual = strtotime($horaactual);
        $tiempoot = strtotime($this->fecha_ultimo_officetrack);
        $tiempotoa = strtotime($this->fecha_ultimo_ofsc);

        if ($tiempoot > $tiempotoa) {
            $tiemposegundos = $tiempoactual - $tiempoot;
        } else {
            $tiemposegundos = $tiempoactual - $tiempotoa;
        }
        $tiemporango  = [1200, 7200, 14400];
        if ($tiemposegundos > $tiemporango[2]) {
            $this->estado = 4;
        } else if ($tiemposegundos > $tiemporango[1]) {
            $this->estado = 3;
        } else if ($tiemposegundos > $tiemporango[0]) {
            $this->estado = 2;
        } else if ($tiemposegundos > $tiemporango[0]) {
            $this->estado = 1;
        }
    }*/
}
