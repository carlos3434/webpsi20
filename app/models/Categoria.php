<?php

class Categoria extends Eloquent
{

    public $table = 'categorias';

    public function ubicacion()
    {
        return $this->belongsTo('Ubicacion');
    }

}
