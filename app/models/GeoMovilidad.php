<?php 
class GeoMovilidad extends Eloquent
{
    public static function postGeoMovilidadAll()
    {
       $r  =   DB::table('geo_movilidad')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r;
    }

    public static function postProyectoFiltro($array)
    {
        $r  =   DB::table('geo_movilidad')
                  ->select(
                      'proyecto as id',
                      DB::raw("proyecto as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('proyecto')
                  ->get();
        return $r;
    }

    public static function postMovilFiltro($array)
    {
        $r  =   DB::table('geo_movilidad')
                  ->select(
                      'movil as id',
                      DB::raw("movil as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("proyecto", $array['proyecto'])
                  ->groupBy('movil')
                  ->get();
        return $r;
    }

  
}
