<?php
use Illuminate\Database\Eloquent;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EnvioOfscQuery
 *
 * @author Administrador
 */
class EnvioOfscQuery extends \Eloquent 
{
    //put your code here
    public $table="envio_ofsc_query";
    protected $guarded = array('id');
    protected $modelClassName;
    
    public function create(array $attributes)
    {
        return call_user_func_array(
            "{$this->modelClassName}::create", 
            array($attributes)
        );
    }
}
