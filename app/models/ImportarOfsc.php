<?php
//models
class ImportarOfsc extends \Eloquent
{

     public static function getInsertarMessage(array $data )
     {
        $usuario=Auth::user()->id;

        $import = "INSERT INTO rep_ofsc_messages (queue_id, 
                   appt_id, mqid, time, local_time, inv_id, scenario_step, 
                   msid, mstype, scenario_name, mfid, address, method, 
                   result, result_desc, message_ext_id, duration, 
                   timefrom, timeto, message_data, trigger_name, 
                   time_delivered_start, time_delivered_end, recipient, 
                   appt_ext_id, cust_ext_id,created_at,usuario_created_at) 
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
                   ?, ?, ?,?,?,?,?,?,?,?,NOW(),$usuario)";
        return DB::insert($import, $data);

     }

     public static function getInsertarMessageText(array $data)
     {
        $usuario=Auth::user()->id;
        
        $import = "INSERT INTO rep_ofsc_messagestext (mc_mqid,
                        mcsubject,mcbody,created_at,usuario_created_at) 
                        VALUES (?, ?, ?,NOW(),$usuario)";
        return DB::insert($import, $data);

     }


}
