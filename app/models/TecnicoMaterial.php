<?php 

class TecnicoMaterial extends Eloquent
{
    protected $guarded =[];
    protected $table = "tecnico_material";
    
    public static function boot()
    {
        parent::boot();

        static::saving(
            function ($table) {
            $table->usuario_created_at = \Auth::id();
            }
        );
        static::updating(
            function ($table) {
            $table->usuario_updated_at = \Auth::id();
            }
        );
    }

    public function material()
    {
        return $this->belongsTo('Material');
    }
    public function tecnico()
    {
        return $this->belongsTo('Tecnico');
    }

    public function movimiento()
    {
        return $this->hasMany('TecnicoMaterialMovimiento');
    }

    public static function savematerial($array)
    {
        return TecnicoMaterial::insert($array);
    }
}