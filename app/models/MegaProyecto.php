<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MegaProyecto
 *
 * @author Administrador
 */
class MegaProyecto extends \Eloquent{
    //put your code here
    public $table = "gestiones_edificios_megaproyecto";
    
    public static function agregar($data, $idMega ,$idProyecto, $parent = 0){
        if($parent > 0){
            $mega = MegaProyecto::find($idMega);
            $mega->energia_estado = $data["energia_estado"];
            $mega->fo_estado = $data["fo_estado"];
            $mega->troba_nueva = $data["troba_nueva"];
            $mega->troba_fecha_fin = $data["troba_fecha_fin"];
            $mega->troba_estado = $data["troba_estado"];
            //$mega->usuario_created_at = Auth::id();
            $mega->usuario_updated_at = Auth::id();
            $mega->save();
            
            $data['proyecto_edificio_id'] = $idProyecto;
            $data['parent'] = $parent;
            
            DB::table('gestiones_edificios_megaproyecto')->insert($data);
        }else{
            $data['proyecto_edificio_id'] = $idProyecto;
            $data['usuario_created_at'] = Auth::id();
            //$data['usuario_updated_at'] = Auth::id();
            //print_r($data);
            DB::table('gestiones_edificios_megaproyecto')->insert($data);
            MegaProyecto::agregar($data, DB::getPdo()->lastInsertId(), 
                    $idProyecto, DB::getPdo()->lastInsertId());
        }
        
    }
}
