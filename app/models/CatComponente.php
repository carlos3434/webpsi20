<?php

class CatComponente extends Eloquent
{

    public $table = 'cat_componentes';

    /**
     * Obtener los componentes de una gestion o actuacion.
     *
     * @var gestionId string
     * @var codactu string
     * @return componentes array
     */
    public static function getComponente( $gestionId = null, $codactu = null ){
        //try {
            $componentes = array();
            if (isset($gestionId) && $gestionId!='') {
                # code...
                $componentes = DB::table('componente_gestion AS cg')
                        ->join(
                            'cat_componentes AS cp', 
                            'cp.cod_componente', '=', 'cg.componente_id'
                        )
                        ->select(
                            'cp.cod_componente AS id', 
                            'cp.desc_componente AS nombre',
                             DB::raw(
                                 '"" as serie'
                             ),
                             DB::raw(
                                 '"" as tarjeta'
                             ),
                             DB::raw(
                                 '"" as accion'
                             ),
                             DB::raw(
                                 '"" as rst'
                             ),
                             DB::raw(
                                 'IF(cp.desc_componente LIKE "%MODEM%", 1, 0) as modem'
                             )
                        )
                        ->where(
                            'cg.gestion_id', 
                            '=', $gestionId
                        )
                        ->whereRaw(
                                 '(cp.abreviado IN ("DECHD", "DECDI", "DECPV")
                                    OR cp.desc_componente LIKE "%MODEM%")'
                             
                        )                        // ->whereIn(
                        //     'cp.abreviado', 
                        //     ['DECHD', 'DECDI', 'DECPV']
                        // )
                        ->orderBy('cp.desc_componente')
                        ->get();
            }
            if (count($componentes) > 0) {
                return $componentes;
            }
            if (isset($codactu) && $codactu!='') {
                $componentes = DB::table('cat_componentes AS cp')
                        ->join(
                            'schedulle_sistemas.prov_pen_catv_pais_componentes '
                            . 'AS pp', 
                            'cp.cod_componente', 
                            '=', 
                            'pp.codigo_de_componente'
                        )
                        ->select(
                            'cp.cod_componente AS id', 
                            'cp.desc_componente AS nombre',
                             DB::raw(
                                 '"" as serie'
                             ),
                             DB::raw(
                                 '"" as tarjeta'
                             ),
                             DB::raw(
                                 '"" as accion'
                             ),
                             DB::raw(
                                 '"" as rst'
                             )
                        )
                        ->where(
                            'pp.codigo_req', '=', $codactu
                        )
                        ->orderBy('cp.desc_componente')
                        ->get();
            }

            if (count($componentes) > 0) {
                return $componentes;
            }
            return $componentes;
        /*} catch (Exception $e) {
            
        }*/
    }

}
