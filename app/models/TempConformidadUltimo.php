<?php

class TempConformidadUltimo extends \Eloquent {
	use DataViewer;
	public $table = 'temp_conformidad_ultimo';

	protected $fillable = 
    [   'temporal_conformidad_id',
        'codcli',
        'motivo_id',
        'submotivo_id',
        'estado_id',
        'estado_pretemporal_id',
        'observacion',
        'pre_temporal_solucionado_id'
    ];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }
}