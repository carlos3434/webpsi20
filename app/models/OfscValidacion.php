<?php 

/**
*  Modelo que realiza las validaciones de las ordenes a 
*  enviar a Ofsc
*/
class OfscValidacion extends  \Eloquent
{	
	/** 
	* Funcion que valida el Envio Masivo
	*/
	public function validaEnvioMasivoOfsc ($ordenes = array())
	{
	    $ordenes = $this->validaContrata($ordenes);
	     echo "actividades provisiones son contrata 245 ".count($ordenes)."\n";
	            
	     $ordenes = $this->validaArea2($ordenes);
	       echo "actividades provisiones no son actividad area2 igual a PAI ".count($ordenes)."\n";

	       $ordenes = $this->validaProvPendienteCatv( $ordenes);
	      echo "actividades provisiones son pendiente catv ".count($ordenes)."\n";

	       $ordenes = $this->validarProvLiquidadas($ordenes);
	        echo "actividades provisiones no estan liquidadas".count($ordenes)."\n";
	        return $ordenes;
	}
	/*
	* Funcion que valida que la provision este en pendientes
	*/
	public function validaProvPendienteCatv( $ordenes = array())
	{
		$resultado = array();
		// extrayendo solo actividades que son provision
		
		    if (count($ordenes) > 0) {
		       $in = array();
		      foreach ($ordenes as $key => $value) {
		   	$in[] = $value->codactu;
		      }
		        $query = DB::table("schedulle_sistemas.prov_pen_catv_pais")
		        	->select("codigo_req")
		    	->whereIn("codigo_req", $in)
			->get();

		  if (count ($query) > 0) {
		  	$query_temp = array();
		  	foreach ($query as $key => $value) {
		  		$query_temp[$value->codigo_req] = $value;
		  	}
		  	//var_dump($query);
		  	$query = $query_temp;
		  	foreach ($ordenes as $key => $value) {
		  		if (isset($query[$value->codactu])){
		  			$resultado[] = $value;
		  		}
		  	}
		       }
		}
		
		return $resultado;
	}

	/*
	* Funcion que valida que la provision no este liquidada
	*/
	public  function validarProvLiquidadas ($ordenes = array())
	{
		$resultado = array();

		if (count ($ordenes) > 0) {
			$in = array();
			foreach ($ordenes as $key => $value) {
				$in[] = $value->codactu;
			}
			$query = DB::table("webpsi_coc.prov_liqui_macro")
					->select("numreq", "edoxot")
					->where("edoxot", "=", "Q")
					->whereIn("numreq", $in)
					->get();

			if (count ($query) > 0) {
				$query_temp = array();
				foreach ($query as $key => $value) {
					$query_temp[$value->numreq] = $value;
				}
				$query = $query_temp;
				foreach ($ordenes as $key => $value) {
					if (isset($query[$value->numreq])){
						unset($ordenes[$key]);
					}
				}
				$resultado = $ordenes;
			} else {
				$resultado = $ordenes;
			}
		} else {
			$resultado = $ordenes;
		}

		return $resultado;
	}

	public  function validaContrata ($ordenes = array())
	{
		$resultado  = array();
		foreach ($ordenes as $key => $value) {
			if ($value->contrata !== "245")
				unset($ordenes[$key]);
		}
		$resultado = $ordenes;
		return $resultado;
	}

	public  function validaArea2 ($ordenes = array())
	{
		$resultado = array();
		foreach ($ordenes as $key => $value) {
			if ( $value->area2 === "PAI")
				unset($ordenes[$key]);
		}
		$resultado = $ordenes;

		return $resultado;
	}

	public static function quiebrePasadoOfsc ($codactu = 0, $quiebrePasado = 0)
	{
		if ($codactu >0)
		{
			// Verifico si el quiebre pasado pertenecia
			// a la configuracion de ordenes enviadas a OFSC
			$config = DB::table("config_valores")->where(["id" => 9])->get();
			if (count($config) > 0)
			{
				
				$config = $config[0];
				$quiebres = $config->valores;
				$quiebres = explode("|", $quiebres);
				foreach ($quiebres as $key => $value) {
					if ($value == $quiebrePasado)
						return ["rst" => 1]; //encontro coincidencia de quiebre
				}
				return ["rst" => 2]; // no encontro coincidencia de quiebre
			} else {
				return ["rst" =>3]; // no existe registros para config con quiebres
			}
		} else {
			return ["rst" => 4]; // no se envio el codigo de actuacion
		}
	}
}