<?php

class ActividadOfscMovimiento extends \Eloquent
{
	public function __construct()
    {
         $this->table = "actividades_ofsc_movimientos";
    }

    public function crear($data = [])
    {
		$objAOM = new ActividadOfscMovimiento;

    	foreach ($data as $key => $value) {
    		$objAOM[$key] = $value;
    	}

        $objAOM->save();
        return $objAOM->id;
    }

    public function getCambios ($dataNueva = [], $dataAnterior = [])
    {
    	$cambios = [];
    	$i = 0;
    	foreach ($dataAnterior as $key => $value) {
    		if (isset($dataNueva[$value["appt_number"]])){
    			$ordenNueva = $dataNueva[$value["appt_number"]];
    		
	    		if (isset($ordenNueva))
	    		{	
	    			$cambio = [
	    				"codactu" => $key,
	    				"resource_id_anterior" => $value["resource_id"],
	    				"resource_id_nuevo" => $ordenNueva["resource_id"],
	    				"start_time_anterior" => $value["start_time"],
	    				"start_time_nuevo" => $ordenNueva["start_time"],
	    				"end_time_anterior" => $value["end_time"],
	    				"end_time_nuevo" => $ordenNueva["end_time"],
	    				"time_of_booking_anterior" => $value["time_of_booking"],
	    				"time_of_booking_nuevo" => $ordenNueva["time_of_booking"],
	    				"mensaje" => "",
	    				"nodo" => "",
	    				"quiebre_id" => 0,
	    				"empresa_id" => 0
	    			];

	    			if ($ordenNueva["resource_id"]!==$value["resource_id"]) {
	    				$cambio["mensaje"] = "Se hizo un cambio de resource_id: ".$value["resource_id"]." a ".$ordenNueva["resource_id"];
	    				$cambio["tipo"] = 1;
	    				$cambios[] = $cambio;
	    			}
	    			if ($ordenNueva["start_time"]!==$value["start_time"]) {
	    				$cambio["mensaje"] = "Fecha Inicio a cambiado de ".$value["start_time"]." a ".$ordenNueva["start_time"];
	    				$cambio["tipo"] = 2;
	    				$cambios[] = $cambio; 
	    			}

	    			if ($ordenNueva["end_time"]!==$value["end_time"]) {
	    				$cambio["mensaje"] = "Fecha Fin a cambiado de ".$value["end_time"]." a ".$ordenNueva["end_time"];
	    				$cambio["tipo"] = 3;
	    				$cambios[] = $cambio; 
	    			}

	    			if ($ordenNueva["time_of_booking"]!==$value["time_of_booking"]) {
	    				$cambio["mensaje"] = "Fecha de Agenda a cambiado de ".$value["time_of_booking"]." a ".$ordenNueva["time_of_booking"];
	    				$cambio["tipo"] = 4;
	    				$cambios[] = $cambio; 
	    			}
	    		}
    		}
    	}
    	return $cambios;
    }

    public function setCambios ($data = [], $json = [])
    {
    	$hoy = date("Y-m-d");
    	$dataAnterior = [];
    	$dataNueva = [];

    	$bucketIds = ["LARI_INS_MOVISTAR_UNO"];

    	foreach ($json as $key => $value) {
            if (isset($value["status"])) {
                if ($value["status"] === "pending"){
                    $dataAnterior[$value["appt_number"]] = $value;
                }
            }
    	}

    	foreach ($data as $key => $value) {
    		if ($value["status"] === "pending")
    			$dataNueva[$value["appt_number"]] = $value;
    	}

    	$cambios = $this->getCambios($dataNueva, $dataAnterior);
    	foreach ($cambios as $key => $value) {
    		$idNuevo = $this->crear($value);
    		$this->updateGestion($value, $idNuevo, $value["tipo"]);
    	}
    }
    public function updateGestion ($orden = [], $idNuevo, 
    	$tipo = 0)
    {
    	$bucketIds = ["LARI_INS_MOVISTAR_UNO"];
    	//echo "tipo === ".$tipo."\n";

    	if (isset($orden["codactu"])) {
    		$resourceId = $orden["resource_id_nuevo"];
    		$gestionDetalle = 
    			GestionDetalle::select("gestion_id", "mdf")
    			->where("codactu", "=", $orden["codactu"])
    			->get();

    		if (isset($gestionDetalle[0])) {
    			$update = []; 
    			$where = [];

    			$gestionId = $gestionDetalle[0]->gestion_id;
    			$where["gestion_id"] = $gestionId;

    			$nodo = $gestionDetalle[0]->mdf;
    			$tecnico = Tecnico::select("id")
    				->where("carnet", "=", $resourceId)
    				->get();

    			print_r($tecnico);

    			$movimiento = GestionMovimiento::where($where)
    				->first();

    			// id usuario TOA = 952
    			if (isset($movimiento)) {

    				$ultimo = UltimoMovimiento::where($where)
    					->get();
    				switch ($tipo) {
    					case 1:
    						// valido si el resource anterior ha sido un bucket
    						if (isset($tecnico[0]))
    						{
    							$update["tecnico_id"] = 
    								$tecnico[0]->id;
    							$update["estado_id"] = 2;

    						} else {
    							$update["tecnico_id"] = 
    								null;
    							$update["estado_id"] = 7;
    						}
    						$update["resource_id"] = 
    							$orden["resource_id_nuevo"];
    						break;
    					case 2:
    						// cambio de start time
    						$update["f_inicio"] = 
    						$orden["start_time_nuevo"];
    						break;

    					case 3:
    						$update["f_cierre"] = 
    						$orden["end_time_nuevo"];
    						break;

    					case 4:
    						$update["fecha_agenda"] = 
    						date("Y-m-d", 
    							strtotime($orden["time_of_booking_nuevo"])
    						);
    						break;

    					default:
    						# code...
    						break;
    				}

    			$update["usuario_updated_at"] = 952;

    			UltimoMovimiento::where($where)
    				->update($update);
    				
    			$where["id"] = $movimiento->id;

    			if (isset($update["f_inicio"]))
    				unset($update["f_inicio"]);

    			if (isset($update["f_cierre"]))
    				unset($update["f_cierre"]);

    			if (isset($update["resource_id_nuevo"]))
    				unset($update["resource_id_nuevo"]);

    			GestionMovimiento::where($where)
    				->update($update);

    			$update = ["quiebre_id" => $movimiento->quiebre_id,
    				"empresa_id" => $movimiento->empresa_id,
    				"nodo" => $nodo];
    			ActividadOfscMovimiento::where(["id" => $idNuevo])
    				->update($update);
    			}
    		}
    	}
    }
}