<?php
use Legados\models\SolicitudTecnicaUltimo as Ultimo;

class Tecnico extends \Eloquent
{
    public $table = "tecnicos";

    /**
     * Celula relationship
     */
    public function celulas()
    {
        return $this->belongsToMany('Celula');
    }
    /**
     * Empresa relationship
     */
    public function empresas()
    {
        return $this->belongsTo('Empresa');
    }
    
    public function ultimomovimiento()
    {
        return $this->hasMany('UltimoMovimiento');
    }

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = Auth::id();
            }
        );

        /*static::updated(
            function ($table) {
                \Event::fire('tecnico.updated', $table);
            }
        );*/
    }
    /**
     * obtener celulas por tecnico, para cargar en mantenimiento de tecnicos
     */
    public static function getCelulas($tecnicoId)
    {
        $celulas = DB::table('celula_tecnico as ct')
                    ->join(
                        'celulas as c',
                        'ct.celula_id',
                        '=',
                        'c.id'
                    )
                    ->select(
                        'c.id',
                        'c.nombre',
                        DB::raw(
                            'IFNULL(ct.officetrack,0) as officetrack'
                        )
                    )
                    ->where('c.estado', '=', 1)
                    ->where('ct.estado', '=', 1)
                    ->where('ct.tecnico_id', '=', $tecnicoId)
                    ->get();
        return $celulas;
    }
    public static function getTecnico($empresaId, $celulaId)
    {
        $query = "SELECT id, nombre_tecnico, ape_paterno, ape_materno, carnet,
                         dni , IFNULL(g.grupos,0) grupos
                  FROM tecnicos t
                  LEFT JOIN (
                            SELECT  cg.tecnico_id, GROUP_CONCAT(cg.id) grupos
                            FROM celula_grupos cg where  celula_id=?
                            GROUP BY cg.tecnico_id
                  ) g ON t.id=g.tecnico_id
                  WHERE t.carnet <> '' AND t.estado=1 AND empresa_id =?
                        ";

        return DB::select($query, array($celulaId,$empresaId));

    }

    public static function getTecnicotmp($carnettmp)
    {
        return DB::table('tecnicos as t')
                ->leftjoin('celula_tecnico as ct', 't.id', '=', 'ct.tecnico_id')
                ->select(
                    't.id',
                    't.nombre_tecnico',
                    't.carnet_tmp',
                    'ct.celula_id'
                )
                ->where('t.estado', '1')
                ->where(
                    't.carnet_tmp',
                    '=',
                    $carnettmp
                )
                ->groupby('t.id')
                ->get();
    }

    public static function getEmpresa($carnettmp)
    {
        return DB::table('tecnicos as t')
                ->leftjoin('empresas as e', 'e.id', '=', 't.empresa_id')
                ->select(
                    't.id',
                    't.nombre_tecnico',
                    't.carnet_tmp',
                    'e.cms as contrata_cms',
                    'e.gestel as contrata_gestel'
                )
                ->where('t.estado', '1')
                ->where('t.carnet_tmp', '=', $carnettmp)
                ->first();
    }

    public static function getEstadoOfficetrack()
    {
        $estado = DB::table('celula_tecnico')
                    ->select('officetrack')
                    ->where('tecnico_id', '=', Input::get('tecnico_id'))
                    ->where('celula_id', '=', Input::get('celula_id'))
                    ->where('estado', '=', '1')
                    ->first();
        if (isset($estado->officetrack)) {
            return $estado->officetrack;
        }
        return 0;
    }

    public static function getTecnicosOfficetrackAll()
    {
        return DB::table('tecnicos as t')
                ->join('celula_tecnico as ct', 't.id', '=', 'ct.tecnico_id')
                ->select(
                    't.id',
                    't.nombre_tecnico',
                    't.carnet',
                    't.carnet_tmp',
                    'ct.celula_id'
                )
                ->where('t.estado', '1')
                ->where('ct.officetrack', '1')
                ->groupby('t.id')
                ->get();
    }

    public static function getAllTecnicos()
    {
        $query = DB::table('tecnicos as t')
                ->leftJoin(
                    'empresas as e',
                    function ($join) {
                        $join->on(
                            't.empresa_id',
                            '=',
                            'e.id'
                        );
                    }
                )
                ->leftJoin(
                    'celula_tecnico as ct',
                    'ct.tecnico_id',
                    '=',
                    't.id'
                )
                ->leftJoin(
                    'celulas as c',
                    'c.id',
                    '=',
                    'ct.celula_id'
                )
                ->leftJoin(
                    'bucket as b',
                    'b.id',
                    '=',
                    't.bucket_id'
                )
                ->select(
                    't.id',
                    't.nombres',
                    't.ape_paterno',
                    't.ape_materno',
                    't.celular',
                    't.ninguno',
                    't.estado',
                    't.empresa_id',
                    't.marca',
                    't.modelo',
                    't.version',
                    't.observacion',
                    'e.nombre as empresa',
                    'b.id as bucket_id',
                    DB::raw("IFNULL(t.fecha_inicio_toa,'') as fecha_inicio_toa"),
                    DB::raw("IFNULL(t.cargo_tecnico,'') as cargo_tecnico"),
                    DB::raw("IFNULL(t.supervisor,'') as supervisor"),
                    DB::raw("IFNULL(t.imei,'') as imei"),
                    DB::raw("IFNULL(t.imsi,'') as imsi"),
                    DB::raw("IFNULL(b.nombre, 'SinBucket') AS bucket"),
                    DB::raw(
                        'ifnull(t.dni,"") as dni,
                        ifnull(t.carnet,"") as carnet,
                        ifnull(t.carnet_tmp,"") as carnet_tmp,
                        GROUP_CONCAT(c.nombre SEPARATOR "  ||*||  ") as celula'
                    )
                );

        if (Auth::user()->perfil_id != 8) {
            $userId = Auth::user()->id;
            $query->whereIn('t.empresa_id', function($query2) use ($userId) {
                $query2->select('t1.empresa_id')
                      ->from('empresa_usuario as t1')
                      ->where('t1.usuario_id', '=', $userId)
                      ->where('t1.estado', '=', 1);

            });
        }

        if (Input::has("bucket")) {
            $query->whereIn('t.bucket_id', Input::get("bucket"));
        }

        if (Input::has("empresa")) {
            $query->whereIn('t.empresa_id', Input::get("empresa"));
        }

        $resultado = $query->groupBy('t.id')
                          ->orderBy('t.nombres', 'asc')
                          ->get();
        return $resultado;
    }
    public static function asisTecnicos($fecha, $tecnicos)
    {
        $ot = Config::get("wpsi.schema.officetrack");
        $asistencia = DB::table('asistencia_entradas')->get();
        $sql ='';
        foreach ($asistencia as $asis) {
            $entrada = $asis->entrada;
            $id = $asis->id;

            $sql .= " ,(
                    select MAX(fecha_asistencia)
                    from $ot.asistencia_tecnico at
                    WHERE at.numero_tecnico = t.carnet_tmp
                    and at.id_entrada = " . $id . "
                    and at.fecha_asistencia >= '$fecha'
                    and at.fecha_asistencia < DATE_ADD('$fecha',INTERVAL 1 DAY)
                    ) ". $entrada;
        }

        $sql = "SELECT
                    t.carnet_tmp carnet
                    ,c.nombre celula
                    ,CONCAT_WS(' ',t.ape_paterno, t.ape_materno, t.nombres)
                        nombre
                    $sql,
                    IFNULL(lo.estado,'Inactivo') estado , lo.t
                FROM tecnicos t
                INNER JOIN celula_tecnico ct ON t.id=ct.tecnico_id
                INNER JOIN celulas c ON ct.celula_id=c.id
                LEFT JOIN (
                    SELECT  l.id, l.EmployeeNum carnet,
                        DATE_FORMAT(l.TIMESTAMP, '%Y-%m-%d %H:%i:%s') t,
                            IF(
                                DATE_ADD(
                                    DATE_FORMAT(
                                        l.TIMESTAMP, '%Y-%m-%d %H:%i:%s'
                                    )
                                    ,INTERVAL 1 HOUR
                                ) >= NOW(),
                                'Activo',
                                'Inactivo'
                            ) estado
                    FROM $ot.locations l
                    INNER JOIN (SELECT MAX(id) id
                                FROM $ot.locations
                                WHERE DATE(TIMESTAMP)='$fecha'
                                GROUP BY EmployeeNum
                    ) mx ON l.id = mx.id
                ) lo ON t.carnet_tmp = lo.carnet
                WHERE t.estado=1 AND ct.estado = 1 /*AND ct.officetrack = 1*/
                AND t.id IN ($tecnicos) ";

        $reporte = DB::select($sql);

        return $reporte;
    }

    public static function asisTecnicoSRango(
        $fechaIni,
        $fechaFin,
        $tecnicos
    ) {
        $ot = Config::get("wpsi.schema.officetrack");
        $sql = "";
        $ini = new DateTime($fechaIni);
        $fin = new DateTime($fechaFin);
        $interval = $ini->diff($fin);
        $cantDias = (int) $interval->format('%R%a');
        for ($i = 0; $i<= $cantDias; $i++) {
            $fechaNew = date(
                'Y-m-d',
                strtotime(
                    '+'.$i.' days',
                    strtotime($fechaIni)
                )
            );

            $sql.=" ,(
                    select GROUP_CONCAT(tipo_entrada,'(',fasis, ')' , '<br>')
                    from (
                        select  MIN(at.fecha_asistencia) fasis,
                                SUBSTRING(ae.entrada,1,1) tipo_entrada,
                                at.numero_tecnico
                        from $ot.asistencia_tecnico at
                        join $ot.asistencia_entradas ae
                                on ae.id = at.id_entrada
                        where  DATE(at.fecha_asistencia)  = '$fechaNew'
                        group by at.numero_tecnico, at.id_entrada
                    ) q
                    where q.numero_tecnico =  t.carnet
                    ) '$fechaNew' ";

        }

        $sql = "SELECT
                    t.carnet
                    ,t.nombre_tecnico
                    $sql
                from tecnicos t
                INNER JOIN celula_tecnico ct ON t.id=ct.tecnico_id
                WHERE t.estado=1 AND ct.estado = 1 AND ct.officetrack = 1
                AND t.id IN ($tecnicos)";

        $reporte = DB::select($sql);
        return $reporte;
    }

    public function get()
    {
        $elementos = array();

        $queryP = Empresa::select(
                    'empresas.id as empresa_id',
                    DB::raw('concat("empresa_", empresas.id) as id'),
                    DB::raw("'0' as parent_id"),
                    'empresas.nombre as nombre_recurso'
                )
                ->join(
                    'tecnicos as t',
                    't.empresa_id', '=', 'empresas.id'
                )
                ->join(
                    'bucket as b',
                    'b.id', '=', 't.bucket_id'
                );

        if (Auth::user()->perfil_id == 8) {
            $queryP->where('empresas.estado', '=', 1);
        } else {
            $queryP->join(
                'empresa_usuario as eu',
                'eu.empresa_id', '=', 'empresas.id'
            )
            ->where('empresas.estado', 1)
            ->where('eu.estado', 1)
            ->where('eu.usuario_id', Auth::user()->id);
        }
        $datosP = $queryP->orderBy('empresas.nombre')
                    ->groupBy('empresas.id')
                    ->get();

        // ******************************************************************************
        $queryH1 = Empresa::select(
                    'empresas.id as empresa_id',
                    't.bucket_id as bucket_id',
                    DB::raw('concat("bucket_", b.id) as id'),
                    DB::raw('concat("empresa_", t.empresa_id) as parent_id'),
                    'b.nombre as nombre_recurso'
                )
                ->join(
                    'tecnicos as t',
                    't.empresa_id', '=', 'empresas.id'
                )
                ->join(
                    'bucket as b',
                    'b.id', '=', 't.bucket_id'
                );

        if (Auth::user()->perfil_id == 8) {
            $queryH1->where('empresas.estado', 1);
        } else {
            $queryH1->join(
                'empresa_usuario as eu',
                'eu.empresa_id', '=', 'empresas.id'
            )
            ->where('empresas.estado', 1)
            ->where('eu.estado', 1)
            ->where('eu.usuario_id', Auth::user()->id);
        }

        $datosH1 = $queryH1->whereRaw('t.bucket_id is not null and t.bucket_id != 0')
                    ->groupBy('t.bucket_id')
                    ->get();

        // ******************************************************************************

        /*$queryH1 = Tecnico::select(
                    DB::raw('concat("bucket_", b.id) as id'),
                    DB::raw('concat("empresa_", tecnicos.empresa_id) as parent_id'),
                    'b.nombre as nombre_recurso'
                )
                ->join(
                    'empresas as e',
                    'e.id', '=', 'tecnicos.empresa_id'
                )
                ->join(
                    'bucket as b',
                    'b.id', '=', 'tecnicos.bucket_id'
                );

        if (Auth::user()->perfil_id == 8) {
            $queryH1->where('tecnicos.estado', '=', 1);
        } else {
            $queryH1->join(
                'empresa_usuario as eu',
                'eu.empresa_id', '=', 'tecnicos.empresa_id'
            )
            ->where('tecnicos.estado', 1)
            ->where('eu.usuario_id', Auth::user()->id)
            ->where('eu.estado', 1);
        }

        $datosH1 = $queryH1->whereRaw('e.estado = 1 and tecnicos.bucket_id is not null and tecnicos.bucket_id != 0')
                    ->groupBy('b.id')
                    ->get();*/



        // ******************************************************************************
        $datosH2 = Empresa::select(
                    DB::raw('concat("tecnico_", t.id) as id'),
                    DB::raw('concat("bucket_", b.id) as parent_id'),
                    DB::raw('concat(t.carnet_tmp, " | ", t.nombres, " ", t.ape_paterno, " ", t.ape_materno) as nombre_recurso')
                )
                ->join(
                    'tecnicos as t',
                    't.empresa_id', '=', 'empresas.id'
                )
                ->join(
                    'bucket as b',
                    'b.id', '=', 't.bucket_id'
                );

        if (Auth::user()->perfil_id == 8) {
            $datosH2->where('empresas.estado', 1);
        } else {
            $datosH2->join(
                'empresa_usuario as eu',
                'eu.empresa_id', '=', 'empresas.id'
            )
            ->where('empresas.estado', 1)
            ->where('eu.estado', 1)
            ->where('eu.usuario_id', Auth::user()->id);
        }

        $datosH2 = $datosH2->whereRaw('t.bucket_id is not null and t.bucket_id != 0')
                    ->get();

        // ******************************************************************************
    
        /*$datosH2 = DB::select('select concat("tecnico_", t.id) id, concat("bucket_", b.id) parent_id, 
                                concat(t.carnet_tmp, " | ", t.nombres, " ", t.ape_paterno, " ", t.ape_materno) nombre_recurso from tecnicos t 
                                inner join empresas e
                                on e.id = t.empresa_id
                                inner join bucket b  
                                on b.id = t.bucket_id
                                where e.estado = 1 and t.bucket_id is not null and t.bucket_id != 0;');*/
        $elementos["padres"] = $elementos["hijos"] = array();


        if (count($datosP) > 0) {
            foreach ($datosP as $elemento) {
                array_push($elementos["padres"], $elemento);
            }

            foreach ($datosH1 as $elemento1) {
                array_push($elementos["hijos"], $elemento1);
            }

            foreach ($datosH2 as $elemento2) {
                array_push($elementos["hijos"], $elemento2);
            }
        }

        return $elementos;
    }

    public function getEstadistico()
    {
        /*$empresas = DB::select('select e.id empresa_id, e.nombre, count(stu.id) total
                    from solicitud_tecnica_ultimo stu
                    inner join bucket b
                    on b.id = stu.bucket_id
                    inner join empresa_bucket eb
                    on eb.bucket_id = b.id
                    inner join empresas e 
                    on e.id = eb.empresa_id
                    where stu.estado_ofsc_id = 1 and date(stu.updated_at) = date(now())
                    group by eb.empresa_id;');*/

        $empresas = Ultimo::select(
                        'e.id as empresa_id', 
                        'e.nombre',
                        DB::raw('count(solicitud_tecnica_ultimo.id) as total')
                    )
                    ->join(
                        'bucket as b',
                        'b.id', '=', 'solicitud_tecnica_ultimo.bucket_id'
                    )
                    ->join(
                        'empresa_bucket as eb',
                        'eb.bucket_id', '=', 'b.id'
                    )
                    ->join(
                        'empresas as e ',
                        'e.id', '=', 'eb.empresa_id'
                    )
                    ->where('solicitud_tecnica_ultimo.estado_ofsc_id', 1)
                    ->whereRaw('date(solicitud_tecnica_ultimo.updated_at) = date(now())')
                    ->groupBy('eb.empresa_id')
                    ->get();

        $bukets = DB::select('select e.id empresa_id, b.id bucket_id, b.nombre, count(stu.id) total
                    from solicitud_tecnica_ultimo stu
                    inner join bucket b
                    on b.id = stu.bucket_id
                    inner join empresa_bucket eb
                    on eb.bucket_id = b.id
                    inner join empresas e 
                    on e.id = eb.empresa_id
                    where stu.estado_ofsc_id = 1 and date(stu.updated_at) = date(now())
                    group by eb.bucket_id;');

        return [
            "empresas" => $empresas,
            "bukets" => $bukets
        ];
    }
}