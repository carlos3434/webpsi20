<?php

class GeoTroba extends Eloquent
{

    public static function postGeoTrobaAll()
    {
        $r = DB::table('geo_troba')
                ->select(
                    'zonal as id', DB::raw('zonal as nombre')
                )
                ->groupBy('zonal')
                ->get();
        return $r;
    }

    public static function postNodoFiltro($array)
    {
        $r = DB::table('geo_troba')
                ->select(
                    'nodo as id', DB::raw("nodo as nombre ")
                )
                ->whereIn("zonal", $array['zonal'])
                ->groupBy('nodo')
                ->get();
        return $r;
    }

    public static function postTrobaFiltro($array)
    {
        $r = DB::table('geo_troba')
                ->select(
                    'troba as id', DB::raw("troba as nombre ")
                )
                ->whereIn("zonal", $array['zonal'])
                ->whereIn("nodo", $array['nodo'])
                ->groupBy('troba')
                ->get();
        return $r;
    }

    /**
     * {YA NO SE USA, DESPUES CAMBIO MODELO BBDD}
     * @deprecated since version number 
     * @param type $zonal
     * @param type $nodo
     * @param type $troba
     * @return mix return string if throw exception and array if successfull
     */
    public function listarTrobasArea($zonal, $nodo, $troba)
    {
        try {
            $t = ['zonal' => $zonal, 'nodo' => $nodo, 'troba' => $troba];
            
            $data = DB::table('geo_troba')
                ->select(
                    'id', 
                    DB::raw('troba AS campo'), 
                    //'troba AS campo', 
                    'coord_x', 
                    'coord_y'
                )
                ->where($t)
                ->where('id', '<>', '')
                ->orderBy('troba', 'orden')
                ->get();
            
            return $data;
        } catch (Exception $x) {
            Log::error($x);
            return "";
        }
    }
    
    /**
     * {YA NO SE USA, DESPUES CAMBIO MODELO BBDD}
     * @param string $detalle tipo: "LIM|CA|R035"
     * @return array
     * @deprecated since version number
     */
    public function listarCoordenadasPorZonaNodoTroba($detalle)
    {
        $trobaId = 8;
        return DB::table('geo_tabla_detalle AS gtd')
            ->select('detalle', 'coord')
            ->join(
                'geo_tablas AS gt', 
                function($join) use ($trobaId) {
                $join->on('gt.id', '=', 'gtd.tabla_id')
                    ->where('gt.id', '=', $trobaId);
                }
            )
//            ->join(
//                "geo_figuras AS gf", 
//                "gf.id", "=", "gt.figura_id"
//            )
            ->where('gtd.detalle', '=', $detalle)
            ->where('gtd.estado', '=', 1)
            ->first();
//        $sql = "SELECT detalle, coord 
//            FROM geo_tabla_detalle gtd 
//            INNER JOIN geo_tablas gt
//                    ON gt.id = gtd.tabla_id AND gt.id = 8
//            INNER JOIN geo_figuras gf
//                    ON gf.id = gt.figura_id
//            WHERE gtd.detalle = '$detalle' AND gtd.estado = 1;";
//        return DB::select($sql);
    } 

}
