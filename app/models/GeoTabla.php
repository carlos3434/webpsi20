<?php

class GeoTabla extends Eloquent
{


    public static function postElementoAll()
    {
        $r = DB::table('geo_tablas')
            ->select(
                'nombre',
                DB::raw(
                    'concat(id,"_",figura_id
                      ,"_",nombre) as id'
                )
            )
            ->where('estado', '=', '1')
            ->orderBy('nombre', 'asc')
            ->get();
        return $r;
    }

        public static function postElementoPunto()
    {
        $r = DB::table('geo_tablas')
            ->select(
                'id','nombre', 'etiqueta'
            )
            ->where('estado', '=', '1')
            ->orderBy('nombre', 'asc')
            ->get();
        return $r;
    }

    public static function insertTabla($nombre, $etiqueta, $figura)
    {
        $id = Auth::id();
        $tableId = DB::table("geo_tablas")
            ->insertGetId(
                [
                    "nombre" => $nombre,
                    "etiqueta" => $etiqueta,
                    "figura_id" => $figura,
                    "estado" => 1,
                    "usuario_created_at" => $id,
                    "created_at" => date("Y-m-d h:i:s")
                ]
            );
        return $tableId;
    }

    public static function getCampos($id)
    {
        $tabla = DB::table("geo_tablas as t")
            ->join("geo_tabla_campos as tc", "tc.tabla_id", "=", "t.id")
            ->join("geo_campos as c", "tc.campo_id", "=", "c.id")
            ->join("geo_figuras as f", "t.figura_id", "=", "f.id")
            ->select("c.nombre as nombre_campo", "f.nombre as nombre_figura", "tc.orden")
            ->where("t.id", "=", $id)
            ->get();
        return $tabla;
    }

    public static function deleteTable($id)
    {
        try {
            DB::beginTransaction();
            DB::table("geo_tabla_detalle")
                ->where("tabla_id", "=", $id)
                ->delete();

            DB::table("geo_tabla_campos")
                ->where("tabla_id", "=", $id)
                ->delete();

            DB::table("geo_tablas")
                ->where("id", "=", $id)
                ->update();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
