<?php 
class GestionCasuistica extends Eloquent
{

      protected $table = 'gestiones_casuisticas';

    /**
     * buscar gestiones si son agendadas (antes de hoy),
     * que no sean liquidadas, ni canceladas
     */
    public static function Listar($usuarioId)
    {   
        if ($usuarioId > 0) {
            $datos = DB::table('proyecto_tipo as pt')
                    ->rightJoin('proyecto_tipo_usuario as ptu', 'pt.id', '=', 'ptu.proyecto_tipo')
                    ->select('pt.id', 'pt.nombre')
                    ->where('pt.estado', '1')
                    ->where('ptu.usuarios_id', '=', $usuarioId)
                    ->where('pt.visible', '=', 1)
                    ->orderBy('orden')
                    ->get();
        } else {
           $datos = DB::table('proyecto_tipo')
                    ->select('id', 'nombre')
                    ->where('estado', '1')
                    ->orderBy('orden')
                    ->get(); 
        }
        
        return  array(
                    'rst'=>1,
                    'datos'=>$datos
                );
    }
    
    public static function ListarDetalle()
    {
        $datos = DB::table('casuistica_proyecto_estado AS gecd')
                    ->join(
                        'casuisticas AS gcd',
                        'gecd.casuistica_id', '=', 'gcd.id'
                    )
                    ->select(
                        'gcd.id', 'gcd.nombre',
                        DB::raw(
                            'GROUP_CONCAT(
                                DISTINCT(
                                    CONCAT(
                                        "M",
                                        gcd.proyecto_tipo_id
                                    ) 
                                )
                            ) AS relation'
                        )
                    )
                    ->where('gecd.estado', '1')
                    ->where('gcd.estado', '1')
                    ->groupBy('gcd.id')
                    ->orderBy('gcd.nombre')
                    ->get();

        return  array(
                    'rst'=>1,
                    'datos'=>$datos
                );
    }

    public static function ListarEstado()
    {
        $datos = DB::table('casuistica_proyecto_estado AS gecd')
                    ->join(
                        'casuisticas AS gcd',
                        'gecd.casuistica_id', '=', 'gcd.id'
                    )
                    ->join(
                        'proyectos_estados AS ge',
                        'ge.id', '=', 'gecd.proyecto_estado_id'
                    )
                    ->select(
                        'ge.id', 'ge.nombre',
                        DB::raw(
                            'CONCAT(
                                GROUP_CONCAT(
                                    DISTINCT(
                                        CONCAT(
                                        "M",
                                        gcd.proyecto_tipo_id
                                        )
                                    ) 
                                        SEPARATOR "|,|"
                                ),"|,|",
                                GROUP_CONCAT(
                                    DISTINCT(
                                        CONCAT(
                                        "S",
                                        gecd.casuistica_id
                                        )
                                    ) 
                                        SEPARATOR "|,|"
                                )
                            ) AS relation'
                        )
                    )
                    ->where('ge.estado', '1')
                    ->where('gecd.estado', '1')
                    ->where('gcd.estado', '1')
                    ->groupBy('ge.id')
                    ->orderBy('ge.nombre')
                    ->get();

        return  array(
                    'rst'=>1,
                    'datos'=>$datos
                );
    }
    
        
    public static function getSiguienteProyectoEstado(
        $proyectoEstadoId, 
        $casuisticaId
    )
    {
        $datos = DB::table('casuistica_proyecto_estado')
            ->select('siguiente')
            ->where('proyecto_estado_id', $proyectoEstadoId)
            ->where('casuistica_id', $casuisticaId)
            ->first();

        return  $datos; 
    }
}
