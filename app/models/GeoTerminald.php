<?php
class GeoTerminald extends Eloquent
{
    public static function postGeoTerminaldAll()
    {
        $r  =   DB::table('geo_terminald')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r;
    }

    public static function postMdfFiltro($array)
    {
        $r  =   DB::table('geo_terminald')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
    }

    public static function postCableFiltro($array)
    {
        $r  =   DB::table('geo_terminald')
                  ->select(
                      'cable as id',
                      DB::raw("cable as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->groupBy('cable')
                  ->get();
        return $r;
    }

    public static function postTerminalFiltro($array)
    {
        $r  =   DB::table('geo_terminald')
                  ->select(
                      'terminald as id',
                      DB::raw("terminald as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->whereIn("cable", $array['cable'])
                  ->groupBy('terminald')
                  ->get();
        return $r;
    }

}