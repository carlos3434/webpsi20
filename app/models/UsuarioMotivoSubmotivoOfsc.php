<?php
class UsuarioMotivoSubmotivoOfsc extends \Eloquent
{
    public $table = "usuario_motivo_submotivo_ofsc";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = \Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = \Auth::id();
            }
        );
    }
}
