<?php

class Agenda extends \Base
{
    public static function postHorarios($tipo=0,$array=array())
    {
        $sql = "SELECT IF(b.fecha_agenda IS NULL, DATE_ADD(?,
                        INTERVAL IF(a.diaId < ?, 7-?+a.diaId, a.diaId-?)
                        DAY), b.fecha_agenda) fecha,
                        a.*,
                        IF(b.ocupado IS NULL, 0, b.ocupado) ocupado
                FROM ( SELECT fd.id_horario as horarioId,fd.id_dias as diaId,d.nombre, 
                       h.horario 'hora', h.hora_inicio AS horaIni, 
                       h.hora_fin as horaFin, fd.cupo, fd.estado FROM filtro_cupo f
                       LEFT JOIN filtrocupo_detalle as fd
                        ON f.id=fd.id_filtro_cupo
                       JOIN dias d ON fd.id_dias=d.id
                       JOIN horarios h ON fd.id_horario=h.id
                       WHERE f.id_bucket=?
                        and id_tipo_persona=? AND id_horario_tipo=?
                        and fd.estado=1 
                       ORDER BY id_horario, id_dias 
                ) a
                LEFT JOIN 
                ( SELECT stu.fecha_agenda,stu.quiebre_id, stc.codigo_contrata, 
                  stc.zonal,stu.dia_id as diaId, stu.horario_id as horarioId, count(*) ocupado
                  FROM solicitud_tecnica_cms stc
                  INNER JOIN solicitud_tecnica_ultimo stu
                   ON stc.solicitud_tecnica_id=stu.solicitud_tecnica_id
                  WHERE stu.fecha_agenda BETWEEN ? AND ?
                    and stc.codigo_contrata=? and stc.zonal=? 
                    and stu.quiebre_id=?
                  GROUP BY stu.fecha_agenda, stu.dia_id, stu.horario_id
                ) b
                ON a.diaId=b.diaId and a.horarioId=b.horarioId
                ORDER BY hora, fecha";

        if($tipo!=1){
           $sql="SELECT ocupado from (".$sql.")c WHERE fecha=? and hora=? and estado=1 " ;
        }
        $horarios= DB::select(
            $sql,$array
        );

        return $horarios;
    }

}
