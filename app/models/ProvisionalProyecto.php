<?php
/**
 * Description of ProvisionalProyecto
 *
 * @author Administrador
 */
class ProvisionalProyecto extends \Eloquent
{
    public $table = "provisional_proyecto_edificio";
    
    public function get($id)
    {
        
    }
    
    public static function agregar($data, $idProvisional ,$idProyecto, 
        $parent = 0)
    {
        if ($parent > 0) {
            $provisional = ProvisionalProyecto::find($idProvisional);
            $provisional->fecentrega = $data["fecentrega"];
            $provisional->fecrespuesta = $data["fecrespuesta"];
            $provisional->fectermino = $data["fectermino"];
            $provisional->fecliquidacion = $data["fecliquidacion"];
            $provisional->estado_proyecto = $data["estado_proyecto"];
            $provisional->ejecutor_proyecto = $data["ejecutor_proyecto"];
            $provisional->estado_provisional = $data["estado_provisional"];
            $provisional->usuario_created_at = Auth::user()->id;

            $provisional->save();
            $data['idproyectoedificio'] = $idProyecto;
            $data['parent'] = $parent;
                DB::table('provisional_proyecto_edificio')
                            ->insert($data);
        } else {
            $data['idproyectoedificio'] = $idProyecto;
            $data['usuario_created_at'] = Auth::user()->id;
            //print_r($data);
            DB::table('provisional_proyecto_edificio')->insert($data);
            ProvisionalProyecto::agregar(
                $data, DB::getPdo()->lastInsertId(), 
                $idProyecto, DB::getPdo()->lastInsertId()
            );
        }
    }
    /**
     * GestionEdificio relationship
     */
    public function gestionEdificio()
    {
        return $this->hasMany('GestionEdificio');
    }
}
