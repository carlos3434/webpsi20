<?php

class QuiebreGrupo extends \Eloquent
{
    protected $table = "quiebre_grupos";
    protected $guarded =[];
    /**
     * Usuario relationship
     */
    public function quiebres()
    {
        return $this->hasMany('Quiebre');
    }

    /**
     * Usuario relationship
     */
    public function usuarios()
    {
        return $this->belongsToMany('Usuario');
    }

}
