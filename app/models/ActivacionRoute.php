<?php

class ActivacionRoute extends \Eloquent {
	protected $fillable = [];
    
    protected $table ="activacion_route";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = Auth::id();
            }
        );
    }
}