<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
 * 
 */
 class Casuistica extends \Eloquent
{
    public $table="casuisticas";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    //protected $fillable = array('*');
    protected $guarded = array();

    /**
     * GestionEdificio relationship
     */
    public function gestionEdificio()
    {
        return $this->hasMany('GestionEdificio');
    }

}
