<?php
//models
class OfscLegados extends \Eloquent
{

     public static function getCrearTxtLegados()
     {
        $usuario=Auth::user()->id;
        $files=Input::get('files');

        $import = "SELECT gd.orden_trabajo, t.carnet_tmp, gd.codactu, 
                    gd.inscripcion, t.nombre_tecnico, t.celular, 
                    um.fecha_registro, um.updated_at 
                    FROM psi.gestiones_detalles gd
                    INNER JOIN psi.ultimos_movimientos um
                    ON gd.gestion_id=um.gestion_id
                    INNER JOIN psi.tecnicos t
                    ON um.tecnico_id=t.id
                    WHERE um.actividad_id='2' and estado_ofsc_id='6' 
                    and um.quiebre_id='25' AND DATE(um.updated_at)=CURDATE()
                    INTO OUTFILE '".$files."'
                    FIELDS TERMINATED BY ',' ENCLOSED BY '' 
                    LINES TERMINATED BY '\r\n' ";

//        var_dump($import);

        return DB::statement($import);

     }

}
