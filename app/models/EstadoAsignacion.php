<?php

class EstadoAsignacion extends \Eloquent {
    public $table = 'estados_asignacion';
    protected $fillable = ['nombre','estado_ofsc_id','estado_aseguramiento_id','estado'];

    public static $rules = [
      'nombre'    => 'Required',
      'estadoofsc'=> 'Required',
      'estadost'  => 'Required',
      'estado'    => 'Required'
    ];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }   
}