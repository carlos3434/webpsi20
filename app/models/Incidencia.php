<?php

class Incidencia extends \Eloquent {
    use DataViewer;
	public $table = 'incidencia';
	protected $fillable = ['id','nombre','tipo','actividad_id','sistema_id','empresa_id','asociado_id','tipo_legado','detalle','tipificacion','num_requerimiento','nivel_impacto','fecha_validacion','prioridad','estado_validacion','observacion','estado','fecha_entrega','usuario_responsable_id','flag_eliminado'];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

    public function empresa()
    {
        return $this->hasOne('Empresa');
    }

	public function getIncidenciaById(){
		 $query=  DB::table('incidencia as i')
                        ->select(
                        	'i.id',
                        	 DB::raw("LPAD(i.id,5,0) as incidenciat"),
                            'i.actividad_id',
                            'i.empresa_id',
                            'i.sistema_id',
                            'i.tipo_legado',
                            'i.nivel_impacto',
                            'i.prioridad',
                            'i.observacion',
                            'i.estado_validacion',
                            'i.num_requerimiento',
                            'i.estado',
                            'i.tipificacion',
                            'i.tipo',
                            'i.created_at as fecha_registro',
                            'i.usuario_responsable_id',
                            'i.fecha_entrega',
                            'i.asociado_id',
                            'i.cod_remedi',
                            'i.sistema_id',
                            'i.detalle',
                            DB::raw('(select GROUP_CONCAT(image) from incidencia_img where incidencia_id=i.id and estado=1) as imagenes')
                        )
                        ->where(function($query){
                        		$query->where('i.id', Input::get('incidencia_id'));
                        })                       
                        ->get();
        return (count($query)>0) ? $query : false;		 
	}

    public function visor(){
        $count = "SELECT actividad_id,CASE actividad_id WHEN 1 THEN 'AVERIA' WHEN 2 THEN 'PROVISION' END AS actividad_name,COUNT(DISTINCT(sistema_id) ) as row_count from incidencia where flag_eliminado=0 group by actividad_id";
        $count1 = DB::select($count);

        $query ="SELECT i.actividad_id,CASE i.actividad_id WHEN 1 THEN 'AVERIA' WHEN 2 THEN 'PROVISION' END AS actividad,
                CASE i.sistema_id  WHEN 1 THEN 'PSI' WHEN 2 THEN 'PSI-TOA' WHEN 3 THEN 'CMS' WHEN 4 THEN 'Gestel' WHEN 5 THEN 'TOA MOVIL' WHEN 6 THEN 'TOA MANAGE' WHEN 7 THEN 'CMS-PSI' WHEN 8 THEN 'CMS-PSI-TOA'  END AS sistema,
                count(i1.id) as f1,count(i2.id) as f2,count(i3.id) as f3 ,count(i4.id) as f4,count(i5.id) as f5,count(i6.id) as f6,count(i7.id) as f7,count(i8.id) as f8,count(i.id) as total 
                FROM incidencia i
                LEFT JOIN incidencia i1 ON i1.id = i.id AND i1.tipo=2 AND i1.estado=4 
                LEFT JOIN incidencia i2 ON i2.id = i.id AND i2.tipo=2 AND i2.estado IN (1,5) 
                LEFT JOIN incidencia i3 ON i3.id = i.id AND i3.tipo=2 AND i3.estado=2
                LEFT JOIN incidencia i4 ON i4.id = i.id AND i4.tipo=2 AND i4.estado=3 
                LEFT JOIN incidencia i5 ON i5.id = i.id AND i5.tipo IN (1,3) AND i5.estado=4
                LEFT JOIN incidencia i6 ON i6.id = i.id AND i6.tipo IN (1,3) AND i6.estado IN (1,5)
                LEFT JOIN incidencia i7 ON i7.id = i.id AND i7.tipo IN (1,3) AND i7.estado=2
                LEFT JOIN incidencia i8 ON i8.id = i.id AND i8.tipo IN (1,3) AND i8.estado=3 
                WHERE i.tipo IN (1,2,3) and i.flag_eliminado=0 
                GROUP BY i.actividad_id,i.sistema_id";
        $data = DB::select($query);

        return array(
                'data' => $data,
                'count' => $count1
            );             
    }

    public function responsables(){
        $sistema = "SELECT id as sistema_id,nombre FROM sistema WHERE estado=1";
        $c_sistema = DB::select($sistema);

        $actividad = "SELECT actividad_id,CASE actividad_id WHEN 1 THEN 'AVERIA' WHEN 2 THEN 'PROVISION' END AS actividad_name,COUNT(DISTINCT(usuario_responsable_id) ) as row_count from incidencia where flag_eliminado=0 group by actividad_id";
        $c_actividad = DB::select($actividad);

        $array_tipo = ['INCIDENCIA'=>'2','OM'=>'1,3'];
        $left='';$x=0;$count='';
        foreach ($array_tipo as $key => $value) {
            for($i=0;$i<count($c_sistema)+1;$i++){
                $x+=1;
                if($i == count($c_sistema)){
                    $left.= "LEFT JOIN incidencia i$x ON i$x.id = i.id AND i$x.tipo IN ($value) AND i$x.estado IN (1,5) ";
                }else{
                    $sistema_id=$c_sistema[$i]->sistema_id;
                    $left.= "LEFT JOIN incidencia i$x ON i$x.id = i.id AND i$x.tipo IN ($value) AND i$x.sistema_id=$sistema_id AND i$x.estado IN (1,5) ";                    
                }
                $count.=",COUNT(i$x.id) as f$x";     
            }            
        }

        $query ="SELECT  i.actividad_id,CASE i.actividad_id WHEN 1 THEN 'AVERIA' WHEN 2 THEN 'PROVISION' END AS actividad,
                UCASE(CONCAT_WS(' ',u.nombre,u.apellido)) as responsable $count                
                FROM incidencia i 
                LEFT JOIN usuarios as u ON u.id=i.usuario_responsable_id 
                $left 
                GROUP BY i.actividad_id,i.usuario_responsable_id order by i.actividad_id ASC,i.usuario_responsable_id DESC";
        $data = DB::select($query);

        return array(
                'sistema' => $c_sistema,
                'actividad' => $c_actividad,
                'data' => $data
            );             
    }
}