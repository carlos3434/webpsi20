<?php 
class GeoArmarioPunto extends Eloquent
{

    public static function postGeoArmarioPuntoAll()
    {
        $r  =   DB::table('geo_armariopunto')
            ->select(
                'zonal as id',
                DB::raw('zonal as nombre')
            )
         ->groupBy('zonal')
          ->get();
        return $r;
    }

    public static function postMdfFiltro($array)
    {
        $r  =   DB::table('geo_armariopunto')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
    }

    public static function postArmarioPuntoFiltro($array)
    {
        $r  =   DB::table('geo_armariopunto')
                  ->select(
                      'armario as id',
                      DB::raw("armario as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->groupBy('armario')
                  ->get();
        return $r;
    }

  
}
