<?php

class UsuarioSolicitudTecnica extends \Eloquent
{
	public function __construct()
    {
         $this->table = "usuario_solicitud_tecnica";
    }

    public function getUsuario()
    {
    	$this->belongsTo('Usuario');
    }    
}