<?php

class ParametroCriterio extends \Base
{
	public $table = 'parametro_criterio';

	protected $fillable = ['parametro_id','criterios_id','detalle', 'estado', 'updated_at', 'created_at'];

	public function criterio(){
		return $this->hasOne('Criterios', 'id', 'criterios_id');
	}
}