<?php

class UsuarioHorario extends \Eloquent {
	protected $fillable = ['usuario_id','dia_id','estado'];
	public $table = 'usuario_horario';


	public function getAll(){
			$query=  DB::table('usuario_horario as uh')
                        ->select(
                        	'uh.id',
                            'uh.usuario_id',
                            'uh.dia_id',
                            'uh.estado'
                        )
                        ->where(function($query){
                                $query->where('estado',1);
                            }
                        )                        
                        ->get();
        return (count($query)>0) ? $query : false;
	}
}