<?php
//models
class TmpProvision extends \Eloquent
{

    public static function getTmpUpdate()
    {
        $query = "SELECT gd.id 'gdid',um.id 'umid',um.aid,
        um.gestion_id,tmp.codigo_req,tmp.veloc_adsl,
        tmp.fono1,tmp.telefono,tmp.fonos_contacto,
        tmp.fecha_Reg, tmp.orden 
                from webpsi_coc.tmp_provision tmp
                INNER JOIN ultimos_movimientos um
                on tmp.codigo_req=um.codactu
                INNER JOIN gestiones_detalles gd
                on um.gestion_id=gd.gestion_id
                where um.aid is not null
                and um.update_toa=0 and um.estado_ofsc_id not in (5,6) " ;

        return DB::select($query);
    }

}
