<?php
    /**
     * Registro de las accciones del webService.
     * @return Response
     */

class EnvioOfscTemporal extends \Eloquent
{

    public $table = 'envios_ofsc_temporal';

    public function registrarAccionWebservice($accion, $dataReq, $contenidoResp,
                                             $usuario) 
    {
            DB::table('envios_ofsc_temporal')->insert(
                array(
                 'accion'             => $accion,
                 'enviado'            => $dataReq,
                 'respuesta'          => $contenidoResp,
                 'usuario_created_at' => $usuario,
                 'created_at' => DB::raw('NOW()')

                )
            );

    }

}