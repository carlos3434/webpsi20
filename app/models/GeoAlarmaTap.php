<?php

class GeoAlarmaTap extends \Eloquent
{

    public $table = 'geo_alarma_tap';

    /**
     * 
     * @param type $zonal
     * @return string
     */
    public function trobasAlarma($zonal)
    {
        try {
            $sql = "SELECT 
                        a.zonal, a.nodo, a.troba, 
                        COUNT(a.codcli) n, b.troba alarma, 
                        b.color, b.opacidad, b.blink
                    FROM 
                        geo_alarma_tap a 
                            INNER JOIN geo_troba_alertadas b 
                            ON a.nodo=b.nodo AND a.troba=b.troba
                    WHERE 
                        a.troba <> '' ";
            if (!empty($zonal)) {
                $sql .= " AND a.zonal='$zonal' ";
            }
            $sql .= "GROUP BY 
                        a.zonal, a.nodo, a.troba
                    ORDER BY 
                        color, a.nodo, a.troba, n";

            $data = DB::select($sql);
//            $model = DB::table('geo_alarma_tap')
//                ->select(
//                    'geo_alarma_tap.zonal',
//                    'geo_alarma_tap.nodo',
//                    'geo_alarma_tap.troba',
//                    DB::raw('COUNT(geo_alarma_tap.codcli) AS n'), 
//                    'geo_troba_alertadas.troba AS alarma', 
//                    'geo_troba_alertadas.color', 
//                    'geo_troba_alertadas.opacidad', 
//                    'geo_troba_alertadas.blink'
//                )
//                ->join('geo_troba_alertadas', 
//                  'geo_troba_alertadas.nodo', '=', 'geo_alarma_tap.nodo')
//                ->join('geo_troba_alertadas', 
//                  'geo_troba_alertadas.troba', '=', 'geo_alarma_tap.troba')
//                ->where('geo_alarma_tap.troba', '<>', '')
//                ->get();
        } catch (Exception $x) {
            Log::error($x);
            $data = '';
        }
        return $data;
    }

    /**
     * {YA NO SE USA, DESPUES CAMBIO MODELO BBDD}
     * @param type $zonal
     * @param type $nodo
     * @param type $troba
     * @return mix return string if throw exception and array if successfull
     * @deprecated since version number
     */
    public function tapAlarmaClientes($zonal, $nodo, $troba)
    {
        try {
            $query = "SELECT 
                        a.codcli, a.nombres, a.ipaddress, 
                        a.interface, a.detalle, a.amplificador, a.tap, 
                        b.coord_x, b.coord_y
                    FROM
                        geo_alarma_tap a 
                            LEFT JOIN geo_tap b
                                ON a.zonal=b.zonal 
                                AND a.nodo=b.nodo 
                                AND a.troba=b.troba 
                                AND a.amplificador=b.amplificador 
                                AND a.tap=b.tap
                    WHERE
                        a.zonal=? 
                        AND a.nodo=?  
                        AND a.troba=?";
            $data = DB::select($query, array($zonal, $nodo, $troba));
        } catch (Exception $x) {
            Log::error($x);
            $data = '';
        }
        return $data;
    }

    /**
     * {YA NO SE USA, DESPUES CAMBIO MODELO BBDD} 
     * @param type $zonal
     * @param type $nodo
     * @param type $troba
     * @return string
     * @deprecated since version number
     */
    public function ampAlarmaClientes($zonal, $nodo, $troba)
    {
        try {
            $query = "SELECT 
                        amplificador, coord_x, coord_y
                    FROM 
                        geo_amplificador 
                    WHERE 
                        zonal=? 
                        AND nodo=? 
                        AND troba=?";
            $data = DB::select($query, array($zonal, $nodo, $troba));
        } catch (Exception $x) {
            Log::error($x);
            $data = '';
        }
        return $data;
    }

    /**
     * [SIN USO]
     * @return mix return string if throw exception and array if successfull
     */
    public function tapAlarma()
    {         
        try {
            $query = "SELECT 
                        b.coord_x, b.coord_y, 
                        a.zonal, a.nodo, a.troba, 
                        a.amplificador, a.tap
                    FROM
                        geo_alarma_tap a, 
                        geo_tap b
                    WHERE
                        a.zonal=b.zonal 
                        AND a.nodo=b.nodo 
                        AND a.troba=b.troba 
                        AND a.amplificador=b.amplificador 
                        AND a.tap=b.tap";
            $data = DB::select($query);
        } catch (Exception $x) {
            Log::error($x);
            $data = '';
        }
        return $data;
    }
}
