<?php
/**
*   modelo para los estados que se registra en 'proyectos edificios'
*/
class GestionEdificioMegaproyecto extends \Eloquent
{
    public $table="gestiones_edificios_megaproyecto";

    public static function agregar($datos)
    {
        return DB::table('gestiones_edificios_megaproyecto')
            ->insert(
                array(
                    'proyecto_edificio_id' => 
                        $datos['proyecto_edificio_id'],
                    'energia_estado' => $datos['energia_estado'],
                    'fo_estado' => $datos['fo_estado'],
                    'troba_nueva' => $datos['troba_nueva'],
                    'troba_fin' => $datos['fecha_fin'],
                    'troba_estado' => $datos['troba_estado'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'usuario_created_at' => Auth::id()
                )
            );
    }
}
