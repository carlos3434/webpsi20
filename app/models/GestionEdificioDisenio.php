<?php 
class GestionEdificioDisenio extends Eloquent
{
    public $table = 'gestion_edificio_disenio';
    
    public static function getDato($proyectoEdificioId)
    {
        $result = DB::table('gestion_edificio_disenio AS ged')
                ->select(
                    'id','pregunta_relacion_id AS preguntaRelacionId',
                    'respuesta', 'sustento'
                    // , 'proyecto_edificio_id AS proyectoEdificioId'
                )
                ->where('ged.proyecto_edificio_id', '=', $proyectoEdificioId)
                ->orderBy('ged.id', 'ASC')
                ->get();
        //return $result;  
        $coleccion = \Illuminate\Support\Collection::make($result);
        return $coleccion->keyBy('preguntaRelacionId');
    }
    
    public static function crear($dataMultiArray)
    {
        return DB::table('gestion_edificio_disenio')->insert(
            $dataMultiArray
        );   
    }

    public static function actualizarMultiple($dataMultiArray)
    {
        $result = [];
        foreach ($dataMultiArray as $k => $v ) {
            $result = DB::table('gestion_edificio_disenio')
            ->where('id', $k)
            ->update(
                $v
            );
        }
    }
    
    public static function actualizar($proyectoEdificioId, $preguntaId, $dataArray)
    {
            return GestionEdificioDisenio::where(
                'pregunta_relacion_id', $preguntaId
            )
            ->where('proyecto_edificio_id', $proyectoEdificioId)
            ->update(
                $dataArray
            );
    }
    
    public static function existePorEdificio($proyectoEdificioId, $preguntaId)
    {
        return DB::table('gestion_edificio_disenio')
            ->where('proyecto_edificio_id', '=', $proyectoEdificioId)
            ->where('pregunta_relacion_id', '=', $preguntaId)
            ->count();
    }
    
}