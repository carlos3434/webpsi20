<?php

class Geofftt extends Eloquent
{
    public static function  getFFTT( $x, $y, $cant, $tabla)
    {
        $sql = "SELECT 
                        *,
             ROUND( 3959000 * ACOS( 
                                    COS( RADIANS(?) )  *
                                    COS( RADIANS( coord_y ) ) *
                                    COS( 
                                        RADIANS( coord_x ) - RADIANS(?)
                                        ) +
                                    SIN( RADIANS(?) ) *
                                    SIN( RADIANS( coord_y ) )
                                )
                ) AS distance
                FROM $tabla
                ORDER BY distance
                LIMIT 0 , ?";

                /*HAVING distance < '%s'*/
        return DB::select($sql, array((float)$y,(float)$x,(float)$y, $cant));
    }

    public function getMdf($mdf)
    {
        $data = DB::table('geo_mdf')
                ->select('coord_x', 'coord_y')
                ->where('mdf', $mdf)
                ->orderBy('orden')
                ->get();
        return $data;
    }
    
    public function getArmario($data)
    {
        $data = DB::table('geo_armariopoligono')
                ->select('coord_x', 'coord_y')
                ->where('mdf', $data['mdf'])
                ->where('armario', $data['armario'])
                ->orderBy('orden')
                ->get();
        return $data;
    }
    
    public function getNodo($nodo)
    {
        $data = DB::table('geo_nodopoligono')
                ->select('coord_x', 'coord_y')
                ->where('nodo', $nodo)
                ->orderBy('orden')
                ->get();
        return $data;
    }
    
    public static function getTroba($data)
    {
        $data = DB::table('geo_troba')
                ->select('coord_x', 'coord_y')
                ->where('nodo', $data['nodo'])
                ->where('troba', $data['troba'])
                ->orderBy('orden')
                ->get();
        return $data;
    }

    public static function getTap($data)
    {
        $data = DB::table('geo_tap')
                ->select(DB::raw('"tap" as tipo') , 'coord_x', 'coord_y')
                ->where('nodo', $data['nodo'])
                ->where('troba', $data['troba'])
                ->where('amplificador', $data['amplificador'])
                ->where('tap', $data['tap'])
                ->orderBy('orden')
                ->get();
        return $data;
    }

    public static function getTerminald($data)
    {
        $data = DB::table('geo_terminald')
                ->select(DB::raw('"terminald" as tipo') , 'coord_x', 'coord_y')
                ->where('mdf', $data['mdf'])
                ->where('cable', $data['cable'])
                ->where('terminald', $data['terminald'])
                ->get();
                //var_dump($data);
        return $data;
    }

    public static function getTerminalf($data)
    {
        $data = DB::table('geo_terminalf')
                ->select(DB::raw('"terminalf" as tipo') , 'coord_x', 'coord_y')
                ->where('mdf', $data['mdf'])
                ->where('armario', $data['armario'])
                ->where('terminalf', $data['terminalf'])
                ->get();
        return $data;
    }

    public static function getTrobaUsu()
    {
        $data = DB::table('geo_troba as gt')
                ->join(
                    'zonales as z',
                    'z.abreviatura', '=', 'gt.zonal'
                )
                ->join(
                    'usuario_zonal as uz',
                    'uz.zonal_id', '=', 'z.id'
                )
                ->where('uz.usuario_id', '=', Auth::id())
                ->select(
                    'gt.zonal', 
                    'gt.nodo', 
                    'gt.troba', 
                    'gt.coord_x', 
                    'gt.coord_y',
                    DB::raw('gt.troba as id, gt.troba as nombre ')
                )
                ->groupBy('gt.troba')
                ->orderBy('gt.troba')
                ->orderBy('gt.id')
                ->get();
        return $data;
    }

}
