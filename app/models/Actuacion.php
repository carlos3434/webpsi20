<?php 

/**
 * 
 */
class Actuacion
{
    public $codactu;
    public $mdf;
    public $actividadTipoId;
    public $empresa;
    public $actividad;
    public $contrata;
    public $area2;
    public $numreq;
    public $fftt;
    public $quiebre;
    public $grupoEnvio;

    function __construct($actividad)
    {
        $this->codactu = $actividad->codactu;
        $this->mdf = $actividad->mdf;
        $this->actividadTipoId = $actividad->actividad_tipo_id;
        $this->empresa = $actividad->empresa;
        $this->actividad = $actividad->actividad;
        $this->contrata = $actividad->contrata;
        $this->area2 = $actividad->area2;
        $this->numreq = $actividad->numreq;
        $this->fftt = $actividad->fftt;
        $this->quiebre = $actividad->quiebre;
    }
    public function validarContrata(){
        switch ($this->empresa) {
            case "LARI":
                if ($this->contrata!="245") {
                    return false;
                }
            break;
        }
        return true;
    }
    public function validarPAI(){
        if ($this->area2!="PAI") {
            return true;
        }
        return false;
    }
    public function validarProvPendienteCatv(){
        $act = DB::table("schedulle_sistemas.prov_pen_catv_pais")
                 ->select("codigo_req")
                 ->where("codigo_req", "=", $this->codactu)
                 ->first();
        if (!isset($act)) {
            return false;
        }
        return true;
    }
    public function validarLiquidada(){
        $act=null;
        if ($this->actividad == 1) {
            $act = $this->getLiquidadaAveria();
        } else if ($this->actividad == 2) {
            $act = $this->getLiquidadaProvision();
        }
        if (!isset($act)) {
            return true;
        }
        return false;
    }
    public function getLiquidadaAveria(){
        return DB::table("webpsi_coc.aver_pend_macro")
                ->select("numreq", "edoxot")
                ->where(
                    ["edoxot" => "Q",
                    "numreq" => $this->codactu]
                )
                ->first();
    }
    public function getLiquidadaProvision(){
        return DB::table("schedulle_sistemas.prov_liq_catv_pais")
                ->select("codigo_req")
                ->where(
                    ["estado_ot" => "Q",
                    "codigo_req" => $this->codactu]
                )
                ->first();
    }
}