<?php

class ProyectoReporte extends \Eloquent
{

    //protected $table = "actividades";

    public function __construct()
    {
        $this->table = "gestiones_edificios";
    }

    /**
     * Quiebre relationship
     */
    public static function getCargar(
        $rfechas = '', $rSegmento = '', $rTipo = '')
    {
        $select = [
            DB::raw(
                'IF(LENGTH(pe.parent)>0, CONCAT(pe.parent,pe.subitem), pe.id) '
                . 'AS ITEM'
            ),
            //'pe.id AS ITEM', 'pe.parent AS MEGAPROYECTO', 
            'pe.created_at AS FECHA_REGISTRO', 
            'pe.updated_at AS FECHA_ACTUALIZA', 'pe.proyecto_estado AS ESTADO', 
            'bandeja', DB::raw(
                "CONCAT_WS('|',IF(LENGTH(foto_uno)>0,'SI','NO'), "
                . "IF(LENGTH(foto_dos)>0,'SI','NO'), "
                . "IF(LENGTH(foto_tres)>0,'SI','NO')) AS 'FOTOS_1|2|3'"
            ),  'coord_y', 'coord_x', 'departamento', 'provincia', 'distrito',
            'pe.segmento', 'pe.tipo_proyecto', 'pe.tipo_cchh AS TIPO_URB_CCHH',
            'cchh AS NOMBRE_URB_CCHH', 'pe.tipo_via', 'pe.manzana', 'pe.lote',
            'pe.direccion', 'pe.numero', 'pe.nombre_proyecto', 
            'pe.numero_block AS BLOCKS', 'pe.numero_piso AS PISOS', 
            'pe.numero_departamentos AS DPTOS', 
            'pe.numero_departamentos_habitados AS DPTOS_HAB', 'avance', 
            'pe.seguimiento', 'pe.fecha_termino', 'pe.tipo_infraestructura',
            /*'pe.bitacora',*/
            'pe.observacion', 'pe.nombre_constructora', 'pe.ruc_constructora', 
            DB::raw(
                '(CONCAT_WS("|",persona_contacto, 
                persona_contacto_cel, pe.email, pe.persona_contactodos, 
                persona_contactodos_cel, pagina_web_dos))  AS CONTACTOS'
            ),
/*
            DB::raw(
                'TRIM(BOTH "|" FROM (CONCAT_WS("|",persona_contacto, 
                persona_contacto_cel, pe.email, pe.persona_contactodos, 
                persona_contactodos_cel, pagina_web_dos)) ) AS CONTACTOS'
            ),
            'persona_contacto AS CTO_1',  
            DB::raw(
                'SUBSTRING_INDEX(persona_contacto_cel,"|",1) AS CTO_1_TEL_1'
            ),
            DB::raw(
                'SUBSTRING_INDEX(persona_contacto_cel,"|",-1) AS CTO_1_TEL_2'
            ),
            'pe.email AS CTO_1_EMAIL', 'pe.persona_contactodos AS CTO_2', 
            DB::raw(
                'SUBSTRING_INDEX(persona_contactodos_cel,"|",1) AS CTO_2_TEL_1'
            ),
            DB::raw(
                'SUBSTRING_INDEX(persona_contactodos_cel,"|",-1) AS CTO_2_TEL_2'
            ),
            'pagina_web_dos AS CTO_2_EMAIL',
*/
            'pe.armario AS ARMARIO_GIS', 'pe.zona_competencia', 'mdf_gis', 
            'pe.ura_gis', 'troba_gis', 'troba_bidireccional', 'zona_gis', 
            'eecc_gis',
            'u.usuario AS USUARIO_REGISTRO', 'uu.usuario AS USUARIO_ACTUALIZA', 
            'pe.fuente_identificacion', 'mes', 'ticket'
        ];

    $r = DB::table('proyecto_edificios AS pe')
        ->leftJoin('usuarios AS u', 'u.id', '=', 'pe.usuario_created_at')
        ->leftJoin('usuarios AS uu', 'uu.id', '=', 'pe.usuario_updated_at')
        ->select($select)
        ->where(
            function($query) use ($rfechas) {
                if (is_array($rfechas)) {
                    $query->whereBetween('pe.created_at', $rfechas);     
                }
            }
        )
        ->where(
            function($query) use ($rSegmento) {
                if (is_array($rSegmento)) {
                    $query->whereIn('pe.segmento', $rSegmento);     
                }
            }
        )  
        ->where(
            function($query) use ($rTipo) {
                if (is_array($rTipo)) {
                    $query->whereIn('pe.tipo_proyecto', $rTipo);     
                }
            }
        )
        ->whereRaw('deleted_at IS NULL')  
        ->orderBy(DB::raw('CAST(pe.id AS UNSIGNED)'), 'desc')
        ->get();
        return $r;
    }

}
