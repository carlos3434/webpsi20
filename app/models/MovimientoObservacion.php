<?php 
class MovimientoObservacion extends Eloquent
{

    public $table = 'movimientos_observaciones';


    public function getObservaciones()
    {
        $codactu = Input::get('codactu');
        $query = "select GROUP_CONCAT(mo.observacion) observacion, gm.aid
                    from gestiones_movimientos gm 
                    left join movimientos_observaciones mo
                    on mo.gestion_movimiento_id = gm.id
                    where gm.gestion_id = (
                    select gd.gestion_id from gestiones_detalles gd where gd.codactu = ?)";
        return $datos = DB::select($query,[$codactu]);
    }
}
