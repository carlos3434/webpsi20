<?php
//models
class PermisoEventos extends \Eloquent
{

    public static function getCargarPersonas()
    {
        $query = "SELECT u.id,u.apellido, u.nombre, u.dni, '1' as 'tipo_persona'
            , IFNULL(e.nombre,'') as empresa, '' as carnet,  '' as celula,
            if(ev.estado=1,'Con Permisos', 'Sin Permisos') as detalle
            FROM usuarios u
            LEFT JOIN eventos ev
            on u.id=ev.persona_id and ev.tipo_persona=1 and ev.estado=1
            LEFT JOIN empresas e ON u.empresa_id=e.id
            where u.estado=1  GROUP BY u.id ";

        $res = DB::select($query);

        return $res;
    }

    public static function getCargarTecnicos()
    {
        $query = "SELECT t.id, concat(ape_paterno,' ',ape_materno) as apellido,
         nombres as nombre, dni, '2' as 'tipo_persona', c.nombre as celula,
         e.nombre as empresa, IFNULL(t.carnet_tmp,'') as carnet,
         if(ev.estado=1,'Con Permisos', 'Sin Permisos') as detalle
         FROM tecnicos t
         LEFT JOIN eventos ev
         on t.id=ev.persona_id AND ev.tipo_persona=2 AND ev.estado=1
         LEFT JOIN empresas e ON t.empresa_id=e.id
         JOIN celula_tecnico ct ON ct.tecnico_id = t.id
         JOIN celulas as c ON c.id = ct.celula_id
         where t.estado=1 AND ct.estado = 1
         GROUP BY t.id";

        $res = DB::select($query);

        return $res;
    }

     public static function getAgregarEvento(array $data )
     {
        $sql = "INSERT INTO eventos (evento_id,persona_id,tipo_persona,
                tipo_evento,estado,created_at,usuario_created_at)
                values(?,?,?,?,'1',now(),?)
                ON DUPLICATE KEY UPDATE updated_at=now(),usuario_updated_at=?,
                estado='1'";
        return DB::insert($sql, $data);
        // echo var_dump($data);

     }

     public static function getDesactivarpermisos(array $datas)
     {
        $sql = "UPDATE eventos set estado=0, updated_at=now(),
                usuario_updated_at=? where persona_id=? and tipo_persona=? ";
        return DB::update($sql, $datas);

     }


}
