<?php

class SolicitudTecnicaRecepcion extends \Eloquent {
  use DataViewer;
	public $table = 'solicitud_tecnica_log_recepcion';

 //id_solicitud_tecnica,code_error, descripcion_error,usuario_updated_at,usuario_created_at 

	protected $fillable = ['descripcion_error','id_solicitud_tecnica'];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }
    //********
    public function getAll(){

			$query=  DB::table('solicitud_tecnica_log_recepcion as t')
                        ->select(
                        	't.id_solicitud_tecnica',
                        	't.code_error',
                          't.descripcion_error',
                          't.usuario_updated_at',
                          't.usuario_created_at'
                        ) ->limit(5)
                        ->get();
              
        return (count($query)>0) ? $query : false;
	}


}