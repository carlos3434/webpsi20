<?php 

class TipoMasiva extends Eloquent
{
    protected $guarded =[];
    protected $table = "tipo_masiva";

    public static $where =['id', 'nombre'];
    public static $selec =['id', 'nombre'];
}