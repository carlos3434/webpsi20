<?php

use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaCms;

class Parametro extends Eloquent
{

    protected $guarded =[];
    public $table = 'parametro';

    protected $fillable = ['nombre','estado','proveniencia','tipo','bucket', 'quiebre_id', 'updated_at', 'created_at', 'usuario_updated_at', 'usuario_created_at'];
    
    public static $where =['id','nombre','estado','proveniencia','tipo','bucket'];
    public static $selec =['id','nombre','estado','proveniencia','tipo','bucket'];

    public function tipo_masiva()
    {
        return $this->belongsTo('TipoMasiva');
    }

    public static function getGuardar()
    {
        $name='usuario_updated_at';
        $id=0;
        $tipo=0;
        if (Input::get('id') && Input::get('id')!='') {
            $id=Input::get('id');
            $parametro=Parametro::find($id);
            $proveniencia=$parametro->proveniencia;
            $tipo=$parametro->tipo;
        } else {
            $parametro = new Parametro();
            $name='usuario_created_at';
        }
            
        if (Input::get('nombre')) {
            $parametro['nombre'] = Input::get('nombre');
        }
            

        if (!Input::get('nombre') && count(Input::get('estado'))==0) {
            $parametro['nombre']='Sin Nombre_'.date('Ymd_His');
        }
            
        $parametro[$name] =  Auth::id();

        if (Input::get('tipo')) {
            $parametro['tipo'] = Input::get('tipo');
        }

        if (Input::get('proveniencia')) {
            $parametro['proveniencia'] = Input::get('proveniencia');
        }
        /*if (Input::get('tipo')==4) {
            $parametro['proveniencia'] = 0;
        }
        if (isset($proveniencia)) {
            $parametro['proveniencia'] = $proveniencia;
        }*/
        if (Input::get('bucket')) {
            $parametro['bucket'] = Input::get('bucket');
        }
        if (Input::get('tipo')==4 || $tipo==4) {
            $parametro['bucket'] = "";
        }
        if (Input::get('tipo_masiva_id') && (Input::get('tipo')==4 || $tipo==4)) {
            $parametro['tipo_masiva_id'] =Input::get('tipo_masiva_id');
        }
        if (count(Input::get('estado'))>0) {
            $parametro['estado'] = Input::get('estado');
        }
        $result=$parametro->save();
        $parametro = Parametro::find($parametro->id);
            
        if ($result) {
            $valores=Input::all();
            if (Input::get('Parametro_Criterio')) { //se guardaran criterios

                $checks=Input::get('check', []);
                $criterios=[];
                foreach ($checks as $key => $value) {
                    $criterios[]=explode("|", $value)[0];
                }
                    DB::table('parametro_criterio')
                    ->where('parametro_id', $id)
                    ->whereNotIn('criterios_id', $criterios)
                    ->update(
                        array(
                        'estado' => 0,
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    );
                    $cache=[];
                    foreach ($valores as $key => $value) {
                        if (substr($key, 0, 8)=='criterio') {
                            if (in_array(substr($key, 8, 8), $criterios)) {
                                if(substr($key, 8, 8)==11){ //nodo
                                    $cache[]=$value;
                                } 
                                $detalle=implode($value, ",");
                                $sql = "INSERT INTO parametro_criterio (parametro_id,criterios_id,detalle,created_at)
                                values(".$parametro->id.",".substr($key, 8, 8).",'".$detalle."',now())
                                ON DUPLICATE KEY UPDATE updated_at=now(),estado=1,detalle='".$detalle."' ";
                                 DB::insert($sql);
                            }
                        }
                    }
            }
        }

        if($parametro->tipo==1){
            if(count($cache)==0){
                $buscarnodos=DB::table('parametro_criterio')
                    ->where('parametro_id', $id)
                    ->where('criterios_id', 11)
                    ->select('detalle')
                    ->first();
                if(!is_null($buscarnodos))
                    $cache[]=explode(",", $buscarnodos->detalle);
            }
            foreach ($cache as $key => $value) {
                foreach ($value as $keys => $values) {
                    //var_dump("listParametroCms|".$values);
                    Cache::forget("listParametroCms|".$values);    
                }
            }
        }

        if($parametro->tipo==2){
            if(count($cache)==0){
                $buscarnodos=DB::table('parametro_criterio')
                    ->where('parametro_id', $id)
                    ->where('criterios_id', 11)
                    ->select('detalle')
                    ->first();
                if(!is_null($buscarnodos))
                    $cache[]=explode(",", $buscarnodos->detalle);
            }
            foreach ($cache as $key => $value) {
                foreach ($value as $keys => $values) {
                    //var_dump("listParametrosGoToa|".$values);
                    Cache::forget("listParametrosGoToa|".$values);    
                }
            }
        }

        return $parametro ;
    }

    public static function getBuscar()
    {
            $listafiltro =  DB::table('parametro as p')
                            ->leftjoin(
                                'parametro_criterio as pc',
                                'p.id',
                                '=',
                                'pc.parametro_id'
                            )
                            ->leftjoin(
                                'criterios as c',
                                'c.id',
                                '=',
                                'pc.criterios_id'
                            )
                            ->select(
                                'p.id',
                                'p.nombre',
                                'p.tipo',
                                'p.proveniencia',
                                'p.bucket',
                                'pc.criterios_id',
                                'c.nombre as criterio',
                                'c.nombre_trama',
                                'c.controlador as control',
                                'pc.detalle',
                                'p.estado',
                                'pc.estado as criterio_activo'
                            );
                            //->where('pc.estado', '1');

            if (Input::get('id')) {
                $listafiltro->where('p.id', Input::get('id'));
            }
            if (Input::get('estado')) {
                $listafiltro->where('p.estado', Input::get('estado'));
            }
            if (Input::get('proveniencia')) {
                $listafiltro->where('p.proveniencia', Input::get('proveniencia'));
            }
            $listafiltro->orderBy('p.id');
            $listafiltro=$listafiltro->get();

            return $listafiltro;
    
    }

    public static function getParametroValidacion($data = [])
    {
        if ($data!=[]) {
            Input::replace($data);
        }
        $nombrecampo='nombre_trama';
        if (Input::get('proveniencia')) {
            $proveniencia=Input::get('proveniencia');
            if ($proveniencia==1) {
                $nombrecampo='nombre_gestion';
            }
            if ($proveniencia==3) {
                $nombrecampo='nombre_gestel_prov';
            }
            if ($proveniencia==4) {
                $nombrecampo='nombre_gestel_ave';
            }
        }
            
        if (Input::get('campo')) {
            $nombrecampo=Input::get('campo');
        }
        try {
            DB::statement("SET GLOBAL group_concat_max_len=4096");
        } catch (Exception $e) {
            $error = new ErrorController;
            $error->saveError($e);
        }
        
        $listafiltro =  DB::table('parametro as p')
                        ->leftjoin(
                            'parametro_criterio as pc',
                            'p.id',
                            '=',
                            'pc.parametro_id'
                        )
                        ->leftjoin(
                            'criterios as c',
                            'c.id',
                            '=',
                            'pc.criterios_id'
                        )
                            ->select(
                                'p.nombre AS nombre_parametro',
                                'pc.parametro_id as filtro',
                                'p.tipo',
                                'p.proveniencia',
                                'p.bucket',
                                DB::RAW('GROUP_CONCAT('.$nombrecampo.' SEPARATOR "||") as trama'),
                                DB::RAW('GROUP_CONCAT(detalle SEPARATOR "||") as detalle')
                            )
                            ->where('pc.estado', 1);

        if (Input::get('id')) {
            $listafiltro->where('p.id', Input::get('id'));
        }
        if (Input::get('tipo')) {
            $listafiltro->where('p.tipo', Input::get('tipo'));
        }
        if (Input::get('estado')) {
            $listafiltro->where('p.estado', Input::get('estado'));
        }
        if (Input::get('proveniencia')) {
            $listafiltro->where('p.proveniencia', Input::get('proveniencia'));
        }
        if (Input::get('bucket')) {
            $listafiltro->where('p.bucket', Input::get('bucket'));
        }
        $listafiltro->groupBy('p.id');
        $listafiltro->orderBy('p.id');

        $listafiltro=$listafiltro->get();

        return $listafiltro;
    
    }

    public static function getParametroQuery($data = [])
    {
        if ($data!=[]) {
            Input::replace($data);
        }
        $nombrecampo='nombre_trama';
        if (Input::get('proveniencia')) {
            $proveniencia=Input::get('proveniencia');
            if ($proveniencia==1) {
                $nombrecampo='nombre_gestion';
            }
            if ($proveniencia==3) {
                $nombrecampo='nombre_gestel_prov';
            }
            if ($proveniencia==4) {
                $nombrecampo='nombre_gestel_ave';
            }
        }
            
        if (Input::get('campo')) {
            $nombrecampo=Input::get('campo');
        }

        $listafiltro
            = 'select p.nombre as nombre_parametro, pc.parametro_id as filtro, p.tipo, p.proveniencia, p.bucket, GROUP_CONCAT('.$nombrecampo.' SEPARATOR "||") as trama, GROUP_CONCAT(detalle SEPARATOR "||") as detalle 
                from parametro as p 
                left join parametro_criterio as pc on p.id = pc.parametro_id 
                left join criterios as c on c.id = pc.criterios_id 
                where pc.estado = 1';

        if (Input::get('id')) {
            $listafiltro.=' and p.id in ('.Input::get('id').')';
        }
        if (Input::get('tipo')) {
            $listafiltro.=' and p.tipo in ('.Input::get('tipo').')';
        }
        if (Input::get('estado')) {
            $listafiltro.=' and p.estado in ('.Input::get('estado').')';
        }
        if (Input::get('proveniencia')) {
            $listafiltro.=' and p.proveniencia in ('.Input::get('proveniencia').')';
        }
        if (Input::get('bucket')) {
            $listafiltro.=' and p.bucket in ('.Input::get('bucket').')';
        }

        $listafiltro.= 'group by p.id order by p.id asc';

        if (Input::get('filtrodetalle')) {
            $filtrodetalle=Input::get('filtrodetalle');
            $listafiltro='select * from ('.$listafiltro.')a 
                        where detalle like "%'.$filtrodetalle.'%" ';
        }
        DB::select("SET GLOBAL group_concat_max_len=4096");

        $listafiltro=DB::select($listafiltro);

        return $listafiltro;
    }

    public static function getGuardarPlano($campos)
    {
        $name='usuario_created_at';
        $idfiltro=0;
        if (Input::get('idfiltro') && Input::get('idfiltro')!='') {
            $parametro=Parametro::find(Input::get('idfiltro'));
            $idfiltro=Input::get('idfiltro');
        } else {
            $parametro = new Parametro();
            $name='usuario_updated_at';
        }
        if (Input::get('nombre')) {
            $parametro['nombre'] = Input::get('nombre');
        }
        $parametro[$name] =  Auth::id();

        if (Input::get('proveniencia')) {
            $parametro['proveniencia'] = Input::get('proveniencia');
        }
            
        if (count(Input::get('estado'))>0) {
            $parametro['estado'] = Input::get('estado');
        }
        $result=$parametro->save();
            
        if ($result) {
            $tmpcriterios =  DB::table('tmp_criterios')
                                ->select(
                                    DB::RAW($campos)
                                )->get();
            if (count($tmpcriterios)>0) {
                DB::table('parametro_criterio')
                        ->where('parametro_id', $idfiltro)
                        ->update(
                            array('estado' => 0,
                            'updated_at'=> date('Y-m-d H:i:s')
                            )
                        );
                    $campos=explode(",", $campos);

                foreach ($campos as $key => $campo) {
                        $criteriodetalle=array();
                    foreach ($tmpcriterios as $key => $criterio) {
                        if ($criterio->$campo!='') {
                            array_push($criteriodetalle, $criterio->$campo);
                        }
                    }
                    $control=strtolower($campo);
                    $getcriterio=json_decode(Criterios::get(array('controlador'=>$control)));
                    if (count($getcriterio)>0) {
                        $idcriterio=$getcriterio[0]->id;
                        $detalle=implode(",", $criteriodetalle);
                        $sql = "INSERT INTO parametro_criterio (parametro_id,criterios_id,detalle,created_at)
                                values(".$parametro['attributes']['id'].",".$idcriterio.",'".$detalle."',now())
                                ON DUPLICATE KEY UPDATE updated_at=now(),estado=1,detalle='".$detalle."' ";
                        DB::insert($sql);
                    }
                }
            }
        }

        return $result ;
    }

    public function parametroCriterio()
    {
        return $this->hasMany('ParametroCriterio');
    }

    public static function getFiltrarST($id, $proveniencia)
    {
        $masivast=Masivast::where('parametro_id', $id)->get();
        $response = ["rst" => 1, "msj" => "", "data" => []];
        $hoy = date("Y-m-d");

        if (count($masivast)>0) {
            $response["rst"] = 2;
            return $response;
        }
        $array['id']= $id;
        $array['estado']= 1;
        $parametrizacion = Parametro::getParametroValidacion($array);
        if($proveniencia==2) {
            foreach ($parametrizacion as $key => $value) {
                $trama=explode("||", $value->trama);
                $detalle=explode("||", $value->detalle);
                $stmasivas=SolicitudTecnicaCms::select(
                    DB::RAW($id. ' as parametro_id'),
                    'u.solicitud_tecnica_id'
                )->getUltimoCms();
                foreach ($trama as $keys => $valor) {
                    if ($valor!='' && $detalle[$keys]!='') {
                        $stmasivas->whereIn(DB::RAW($valor), explode(",", $detalle[$keys]));
                    }
                }
                $stmasivas=$stmasivas->whereIn("u.estado_ofsc_id", [1])
                    ->where("estado_aseguramiento", "=", 1)
                    ->where("u.estado_st", "=", 1)
                    ->whereRaw("DATE(u.updated_at) = '{$hoy}'")
                    ->get();
                $stmasivas = $stmasivas->toArray();
                if (count($stmasivas) > 0) {
                    Masivast::insert($stmasivas);
                }
                
                $envio = ["parametro" => $id,
                        "st"        => $stmasivas];
                $response["data"] = $envio;
                $count = count($stmasivas);
            $response["msj"] = "Registro Actualizado. Se enviaran a cancelar {$count}   solicitudes";
            }
        } else {
            $response["rst"] = 3;
        }

        return $response;
    }
}
