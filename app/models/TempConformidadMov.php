<?php

class TempConformidadMov extends \Eloquent {
	use DataViewer;
	public $table = 'temp_conformidad_mov';

	protected $fillable = 
    [   'temporal_conformidad_id',
        'codcli', 
        'motivo_id',
        'submotivo_id',
        'estado_id',
        'estado_pretemporal_id',
        'observacion',
        'pre_temporal_solucionado_id',
        'usuario_created_at'
    ];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

     public static function crearMovimiento()
    {
        $datos=array();
        $datos=Input::all();
        $ticket=Input::get('id');
        DB::beginTransaction();

        try {
            //guardar movimiento
            $mov = [];
            $mov['temporal_conformidad_id'] = $datos['id'];
            $mov['codcli'] = $datos['codcli'];
            $mov['motivo_id'] = $datos['motivo'];
            $mov['submotivo_id'] = $datos['submotivo'];
            $mov['estado_id'] = $datos['estado'];
            $mov['estado_pretemporal_id'] = $datos['estado_pretemporal_id'];
            $mov['observacion'] = $datos['observacion'];
            $mov['pre_temporal_solucionado_id'] = $datos['solucionado'];
            TempConformidadMov::create($mov);

            //actualiza estado de ticket
            $temp_conformidad  = TemporalConformidad::find($datos['id']);
            $temp_conformidad->estado_pretemporal_id = $datos["estado_pretemporal_id"];
            $temp_conformidad->usuario_updated_at =	Auth::id();
            $temp_conformidad->updated_at =	date('Y-m-d H:i:s', time());
            $temp_conformidad->save();

            //Obtener ultimo movimiento antes de grabar la gestion
            $ultmov = DB::table('temp_conformidad_ultimo')
            ->where('temporal_conformidad_id', $datos['id'])
            ->first();
            
            if ($ultmov == NULL) {
            	$ult['temporal_conformidad_id'] = $datos['id'];
            	$ult['estado_pretemporal_id'] = 1;
            	TempConformidadUltimo::create($ult);            	
            }

            if($ticket){
	            DB::table('temp_conformidad_ultimo')
	                ->where('temporal_conformidad_id', $ticket)
	                ->update(
	                    array(
	                        'codcli'                        =>$datos['codcli'],
	                        'motivo_id'                     =>$datos['motivo'], //$datos['motivo_id']
	                        'submotivo_id'                  =>$datos['submotivo'], //$datos['submotivo_id']
	                        'estado_id'                     =>$datos['estado'], //$datos['estado_id']
	                        'estado_pretemporal_id'         =>$datos["estado_pretemporal_id"],
	                        'observacion'                   =>$datos["observacion"],
	                        'pre_temporal_solucionado_id'   =>$datos["solucionado"], //$datos["solucionado_id"]
	                        'usuario_updated_at'            => Auth::id(),
	                        'updated_at'                    => date('Y-m-d H:i:s')
	                    )
	                );            	
            }

            DB::commit();
            return 1;
        } catch (\Exception $exc) {
            DB::rollback();
            $errorController = new ErrorController();
            $errorController->saveError($exc);     
            return 0;
        }
    }

    public static function listar_movimientos($id)
    {
        $data = DB::table('temp_conformidad_mov as ptm')
        ->join('motivos as m', 'ptm.motivo_id', '=', 'm.id')
        ->join('submotivos as sm', 'ptm.submotivo_id', '=', 'sm.id')
        ->join('estados as e', 'ptm.estado_id', '=', 'e.id')
        ->join('estados_pre_temporales as ept', 'ptm.estado_pretemporal_id', '=', 'ept.id')
        ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
        ->Leftjoin('pre_temporales_solucionado as pts', 'ptm.pre_temporal_solucionado_id', '=', 'pts.id')
        ->select(
            'ptm.id as id',
            'm.nombre as motivo',
            'sm.nombre as submotivo',
            'e.nombre as estado',
            'ept.nombre as estado_pretemporal',
            'ptm.observacion as observacion',
            DB::raw('DATE_FORMAT(ptm.created_at,"%d-%m-%y %H:%i:%s") as fecha'),
            DB::raw("CONCAT(u.nombre,' ',u.apellido) as usuario"),
            DB::raw('IFNULL(CONCAT(pts.producto," - ", pts.accion), " ") as solucionado')
        )
        ->where('ptm.temporal_conformidad_id', '=', $id)
        ->orderBy('ptm.created_at', 'desc')
        ->get();

        return $data;
    }
}