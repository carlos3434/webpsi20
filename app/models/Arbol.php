<?php
class Arbol extends \Eloquent
{
    private $_elementos = array();

    public function get(){
        $datosP = DB::select("select * from arbol_recursos");
        //dd($datosP);
        $this->_elementos["padres"] = $this->_elementos["hijos"] = array();

        if (count($datosP) > 0) {
            foreach ($datosP as $elemento) {
                # code...
                //var_dump($elemento);
                if ($elemento->parent_id == "0") {
                    array_push($this->_elementos["padres"], $elemento);
                } else {
                    array_push($this->_elementos["hijos"], $elemento);
                }
            }
            //dd($this->_elementos["hijos"]);
        }

        return $this->_elementos;
    }

    // recursividad
    public function bucle($rows = array(), $parent_id = ""){
        $rama = array();
        $html = "";
        if (!empty($rows)) {
            $html .= "<ul>";
            foreach ($rows as $row) {
                if ($row->parent_id == $parent_id) {
                    $hijos = self::bucle($rows, $row->recurso_id);
                    $array = array(
                        "id"=>$row->recurso_id,
                        "title"=>$row->nombre_recurso,
                        "status" => $row->estado,
                        "nodes"=>$hijos
                    );
                    $html .= "<li>";
                    $html .= "<spam>".$row->nombre_recurso."</spam>";
//                    $html .= self::bucle($rows, $row->recurso_id);
                    $html .= "</li>";

                    $rama[] = $array;
                }
            }
            $html .= "</ul>";
        }

        return $rama;
    }
}