<?php
class Arbol extends \Eloquent
{
    private $_elementos = array();

    public function get(){
        $datosP = DB::select("select * from arbol_recursos");
        $this->_elementos["padres"] = $this->_elementos["hijos"] = array();
        if (count($datosP) > 0) {
            foreach ($datosP as $elemento) {
                if ($elemento->parent_id == "0") {
                    array_push($this->_elementos["padres"], $elemento);
                } else {
                    array_push($this->_elementos["hijos"], $elemento);
                }
            }
        }

        return $this->_elementos;
    }

    // recursividad
    public function bucle($rows = array(), $parent_id = "")
    {
        $html = "";
        if (!empty($rows)) {
            $html .= "<ul>";
            foreach ($rows as $row) {
                if ($row->parent_id == $parent_id) {
                    $html .= "<li>";
                    $html .= "<span id='".$row->nombre_recurso."'>".$row->nombre_recurso."</span>";
                        $html .= self::bucle($rows, $row->recurso_id);
                    $html .= "</li>";
                }
            }
            $html .= "</ul>";
        }
        return $html;
    }
}