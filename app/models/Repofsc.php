<?php
//models
class Repofsc extends \Eloquent
{
   
    public static function getCargarRepoMessage()
    {
        $fecha=Input::get('fecha_timefrom');
        $fecha=explode(" - ", $fecha);

        $Reporte=  DB::table('rep_ofsc_messages')
                    ->select('queue_id','appt_ext_id', 'appt_id',
                        'mqid','scenario_step','result','result_desc',
                        'timefrom')
                    ->whereBetween(DB::raw("DATE(timefrom)"),$fecha)
                    ->orderBy('queue_id','asc')
                    ->get();

        return $Reporte;

    }

    public static function getCargarRepoMessagestext()
    {
        $idmensaje=Input::get('idmensaje');

        $Reporte=  DB::table('rep_ofsc_messagestext')
                    ->select('mc_mqid','mcsubject', 'mcbody')
                    ->where('mc_mqid','=',$idmensaje)
                    ->Limit('1')
                    ->get();

        return $Reporte;
    }

    public static function getCargarExcel()
    {
        $fecha=Input::get('fecha_timefrom');
        $fecha=explode(" - ", $fecha);

        $reporte=  DB::table('rep_ofsc_messages as rm')
                    ->leftjoin('rep_ofsc_messagestext as rt', 'rm.mqid', '=', 'rt.mc_mqid')
                    ->select('queue_id','appt_ext_id', 'appt_id',
                        'mqid','scenario_step','rt.mcsubject','rt.mcbody','result','result_desc',
                        'timefrom')
                    ->whereBetween(DB::raw("DATE(timefrom)"),$fecha)
                    ->orderBy('queue_id','asc')
                    ->get();

        return $reporte;
        
    }


}
