<?php
use \Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaCms;
use Ofsc\Inbound;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as Detalle;

class Publicmap extends Eloquent
{
    /**
     * @param filtro_fec array(0,1) , el primer parametro es un simbolo =,<,>,<>
     * el segundo es el valor de la fecha:
     * <>'', ='', <'', >''
     */

    protected  $sql = "SELECT  
        g.id_atc, g.id,gd.codactu,q.apocope AS quiebre,
        e.nombre AS empresa,IFNULL(gm.fecha_agenda,'') fecha_agenda,
        IFNULL(t.nombre_tecnico,'') AS tecnico,es.nombre AS estado,
        a.nombre AS actividad,gd.fecha_registro,
    IFNULL( 
        IF(a.id IN (1,3),
            (SELECT 1 
            FROM schedulle_sistemas.pen_pais_total ppt 
            WHERE ppt.averia=gd.codactu 
            LIMIT 1),
            (SELECT 1 
            FROM schedulle_sistemas.tmp_gaudi_total tgt 
            WHERE tgt.DATA17=gd.codactu 
            LIMIT 1)
        ),
        '0'
    ) AS existe,
    IFNULL(
        ta.paso,
        IF(g.n_evento=1,'1','0')
    ) AS transmision,
    IFNULL(
        (SELECT estado
        FROM webpsi_officetrack.paso_tres pt
        WHERE pt.task_id=ta.id
        LIMIT 1),
        ''
    ) cierre_estado,e.id AS empresa_id,
    IFNULL(t.id,'') AS tecnico_id,gm.coordinado,
    IFNULL(gm.celula_id,'') AS celula_id,es.id AS estado_id,
    q.id AS quiebre_id,q.quiebre_grupo_id,a.id AS actividad_id,
    g.nombre_cliente_critico,g.celular_cliente_critico,
    g.telefono_cliente_critico, gd.zonal_id,
    gd.tipo_averia tipoactu,
    gd.horas_averia,
    
    gd.ciudad,
    gd.inscripcion,gd.mdf,
    gd.observacion,
    gd.segmento,gd.area,
    gd.direccion_instalacion,
    gd.codigo_distrito,gd.nombre_cliente,
    gd.orden_trabajo,gd.veloc_adsl,
    gd.clase_servicio_catv,
    gd.codmotivo_req_catv,
    gd.total_averias_cable,
    gd.total_averias_cobre,
    gd.total_averias,gd.fftt,gd.llave,
    gd.dir_terminal,gd.fonos_contacto,
    gd.contrata,gd.zonal,
    IFNULL(gd.wu_nagendas,'0') wu_nagendas,
    IFNULL(gd.wu_nmovimientos,'0') wu_nmovimientos,
    gd.wu_fecha_ult_agenda,
    gd.total_llamadas_tecnicas,
    gd.total_llamadas_seguimiento,
    gd.llamadastec15dias,gd.llamadastec30dias,
    gd.lejano,gd.distrito,gd.eecc_zona,
    gd.zona_movistar_uno,
    IFNULL(gd.paquete,'') AS paquete,
    gd.data_multiproducto,gd.averia_m1,
    gd.fecha_data_fuente,gd.telefono_codclientecms,
    gd.rango_dias,gd.sms1,gd.sms2,gd.area2,gd.microzona,
    gd.tipo_actuacion,IFNULL(gm.horario_id,'') AS horario_id, 
    IFNULL(h.horario,'') hora_agenda,IFNULL(gm.dia_id,'') AS dia_id,
    CONCAT( 
        IF( IFNULL(gm.fecha_agenda,'')='',
            '',CONCAT(gm.fecha_agenda,' / ')
        ),
        IFNULL(h.horario,'')
    ) AS fh_agenda, gd.x, gd.y 
    FROM gestiones g
    INNER JOIN gestiones_detalles gd 
        ON g.id=gd.gestion_id
    INNER JOIN gestiones_movimientos gm 
        ON (g.id=gm.gestion_id AND 
            gm.id IN (  SELECT MAX(gm2.id)
                        FROM gestiones_movimientos gm2
                        WHERE gm2.gestion_id=g.id
                     )
           )
    INNER JOIN actividades a ON a.id=g.actividad_id
    INNER JOIN quiebres q ON q.id=gd.quiebre_id
    INNER JOIN empresas e ON e.id=gm.empresa_id
    INNER JOIN estados es ON es.id=gm.estado_id
    LEFT JOIN tecnicos t ON t.id=gm.tecnico_id
    LEFT JOIN horarios h ON h.id=gm.horario_id
    LEFT JOIN webpsi_officetrack.tareas ta 
        ON (ta.task_id=g.id AND 
            ta.id IN (  SELECT MAX(ta2.id)
                        FROM webpsi_officetrack.tareas ta2
                        WHERE ta2.task_id=g.id
                     )
           )
    /*LEFT JOIN clientes cl ON cl.codigo=gd.inscripcion*/
    WHERE 
        es.id=2 ";
    /**
     * mostrar ruta de tecnico y gestion con fecha de agenda igual a hoy
     */
    public function getRutaTecnico($carnet, $gestionIid, $filtroFec = array())
    {

        try {
            if ($carnet != '') {
                $tecnico = DB::table('tecnicos')
                    ->where('estado', '=', 1)
                    ->where('carnet', '=', $carnet)
                    ->orWhere('carnet_tmp', '=', $carnet)
                    ->first(
                        array(
                            'id',
                            'nombre_tecnico'
                        )
                    );
            }
            $sql = $this->sql;
                    //AND gm.fecha_agenda='$fecAgenda'";
            if ($carnet != '') {
                $sql .= " AND t.id=$tecnico->id  ";
            }
            if ($gestionIid != '') {
                $sql .= " AND g.id = $gestionIid ";
            }
            if (empty($filtroFec)) {//si esta vacio
                $fecAgenda = date("Y-m-d");
                $sql .=" AND gm.fecha_agenda='$fecAgenda'";
            } else {
                $sql .=" AND gm.fecha_agenda".
                        $filtroFec["condicion"]."'".$filtroFec["valor"]."'";
            }

            $result = DB::select($sql);

            return $result;
        } catch (Exception $exc) {
            return "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getRutaOFSC($codactu)
    {

        try {
            $sql = "SELECT g.id_atc, g.id,gd.codactu, gd.tipo_actuacion, gd.tipo_averia tipoactu,
                    gd.x, gd.y, a.nombre actividad, es.nombre estado, gm.coordinado,
                    g.nombre_cliente_critico, gd.direccion_instalacion, gd.fftt,
                    IFNULL(gm.fecha_agenda,'') fecha_agenda, gd.inscripcion, q.apocope AS quiebre,
                    CONCAT( 
        IF( IFNULL(gm.fecha_agenda,'')='',
            '',CONCAT(gm.fecha_agenda,' / ')
        ),
        IFNULL(h.horario,'')
        ) AS fh_agenda
                    FROM gestiones g
                    INNER JOIN gestiones_detalles gd  ON g.id=gd.gestion_id
                    INNER JOIN actividades a ON a.id=g.actividad_id
                    INNER JOIN gestiones_movimientos gm 
                        ON (g.id=gm.gestion_id AND 
                        gm.id IN (  SELECT MAX(gm2.id)
                                    FROM gestiones_movimientos gm2
                                    WHERE gm2.gestion_id=g.id
                                         )
                    )
                    INNER JOIN estados es ON es.id=gm.estado_id
                    INNER JOIN quiebres q ON q.id=gd.quiebre_id
                    LEFT JOIN horarios h ON h.id=gm.horario_id
                    WHERE gd.codactu = $codactu";
                    /*
            if ($codactu != '') {
                $sql .= " AND gd.codactu = $codactu ";
            }*/

            $result = DB::select($sql);

            return $result;
        } catch (Exception $exc) {
            return "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getRutaLegado($codactu, $solicitud)
    {
        try {    
            $result = SolicitudTecnica::select(
                      DB::RAW("'' AS 'id_atc'"),
                      DB::RAW("solicitud_tecnica.id"),
                      DB::RAW("solicitud_tecnica.id_solicitud_tecnica"),
                      DB::RAW("stu.num_requerimiento as codactu"),
                      DB::RAW("'' AS 'tipo_actuacion'"),
                      DB::RAW("at.label AS 'tipoactu'"),
                      DB::RAW("coordx_direccion_tap AS x"),
                      DB::RAW("coordy_direccion_tap AS y"),
                      DB::RAW("a.nombre AS actividad"),
                      DB::RAW("eo.nombre estado"),
                      DB::RAW("'' AS coordinado"),
                      DB::RAW("CONCAT(nom_cliente,' ',ape_paterno,' ',ape_materno) AS 'nombre_cliente_critico'") ,
                      DB::RAW("CONCAT(tipo_via,'B',nombre_via,' ',numero_via,' PISO',piso,' INT',interior,' MNZ',manzana,' LT',lote,' tipo_urbanizacion:', desc_urbanizacion)
                                        AS 'direccion_instalacion'"), 
                      DB::RAW("concat(if(cod_nodo='',cod_plano,cod_nodo),'|',cod_troba,'|',cod_amplificador,'|',cod_tap,'|',borne) AS fftt"),
                      DB::RAW("IFNULL(fecha_agenda,'') fecha_agenda"), 
                      DB::RAW("'' AS inscripcion"), 
                      DB::RAW("q.apocope AS quiebre"),
                      DB::RAW("CONCAT( 
                                    IF( IFNULL(fecha_agenda,'')='',
                                        '',CONCAT(fecha_agenda,' / ')
                                    ),
                                    IFNULL(h.horario,'')
                                ) AS fh_agenda ")
                            )
                        ->join('solicitud_tecnica_cms as stc',
                                'solicitud_tecnica.id', '=', 'stc.solicitud_tecnica_id')
                        ->join('solicitud_tecnica_ultimo as stu',
                                'solicitud_tecnica.id', '=', 'stu.solicitud_tecnica_id')
                        ->join('actividades as a',
                                'a.id', '=', 'stu.actividad_id')
                        ->join('actividades_tipos as at',
                                'at.id', '=', 'stu.actividad_tipo_id')
                        ->join('estados_ofsc as eo',
                                'eo.id', '=', 'stu.estado_ofsc_id')
                        ->join('quiebres as q',
                                'q.id', '=', 'stu.quiebre_id')
                        ->leftjoin('horarios as h',
                                'h.id', '=', 'stu.horario_id')
                        ->where("solicitud_tecnica.id_solicitud_tecnica",$solicitud)
                        ->where("stu.num_requerimiento",$codactu)
                        ->get();

            return $result;
        } catch (Exception $exc) {
            return "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getSolicitudTecnica($codactu, $solicitud)
    {
        //try {    
            $result = SolicitudTecnicaCms::select(
                      '*',
                      DB::RAW('solicitud_tecnica_cms.id as cmsid'),
                      DB::RAW("at.label AS 'tipoactu'"),
                      DB::RAW("a.nombre AS actividad"),
                      DB::RAW("eo.nombre estado"),
                      DB::RAW("IFNULL(fecha_agenda,'') fecha_agenda"), 
                      DB::RAW("q.apocope AS quiebre"),
                      DB::RAW("CONCAT( 
                                    IF( IFNULL(fecha_agenda,'')='',
                                        '',CONCAT(fecha_agenda,' / ')
                                    ),
                                    IFNULL(h.horario,'')
                                ) AS fh_agenda ")
                            )
                    ->getUltimoCms()
                    ->leftjoin('actividades as a',
                                'a.id', '=', 'u.actividad_id')
                    ->leftjoin('actividades_tipos as at',
                            'at.id', '=', 'u.actividad_tipo_id')
                    ->leftjoin('estados_ofsc as eo',
                            'eo.id', '=', 'u.estado_ofsc_id')
                    ->leftjoin('quiebres as q',
                            'q.id', '=', 'u.quiebre_id')
                    ->leftjoin('horarios as h',
                            'h.id', '=', 'u.horario_id')
                    ->where('solicitud_tecnica_cms.num_requerimiento',$codactu);

                if($solicitud!=""){
                   $result->where('id_solicitud_tecnica',$solicitud);
                }
                    
                $st=$result->first();
                if($st) {
                    $x = $st->coordx_cliente;
                    $y = $st->coordy_cliente;
                    $data= [
                        'id_atc' => '',
                        'id' => $st->cmsid,
                        'codactu' => $st->num_requerimiento,
                        'tipo_actuacion' => '',
                        'tipoactu' => $st->tipoactu,
                        'x' => $x,
                        'y' => $y,
                        'actividad' =>$st->actividad,
                        'estado' => $st->estado,
                        'coordinado' => 'null',
                        'nombre_cliente_critico' => $st->fullName,
                        'direccion_instalacion' => $st->fullAddress,
                        'fftt' => $st->fullfftt,
                        'fecha_agenda' => $st->fecha_agenda,
                        'inscripcion' => '',
                        'quiebre' => $st->quiebre,
                        'fh_agenda' => $st->fh_agenda,
                        'tipo_legado' => $st->tipo_legado,
                        'solicitud_tecnica_id' => $st->solicitud_tecnica_id,
                        'id_solicitud_tecnica' => $st->id_solicitud_tecnica,
                    ];
        
                    $cant = 5;
                    $tabla = 'psi.geo_tap';
                    $mapa["data"] = $data;
                    $mapa["fftt"] = array();
    
                    if (count($mapa["data"]) > 0) {
                        $mapa["fftt"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                    } 
                    return $mapa;
                } else {
                    return null;
                }

            //return $result;
       /* } catch (Exception $exc) {
            return "<h2>Error: No se encontraron datos</h2>";
        }*/
    }

    public function getTap($codactu)
    {

        try {
            $sql = "SELECT xtap, ytap, xterminal, yerminal, xtroba, ytroba
                    FROM `webpsi_coc`.`tb_lineas_servicio_total` WHERE ";
                    if ( isset($codactu) ) {
                        $codactu = trim($codactu);
                        $sql .= " codclicms=? ";
                        $result = DB::select($sql, array($codclicms));
                    }

            $result = DB::select($sql);

            return $result;
        } catch (Exception $exc) {
            return "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function postObtenerCoord()
    {
        $rst=1;
        $msj="";
        $solicitud=Input::get('solicitud','');
        $codactu=Input::get('codactu','');
        $cms=SolicitudTecnicaCms::where('id_solicitud_tecnica',$solicitud)
                            ->where('num_requerimiento',$codactu)
                            ->first();
        if(is_null($cms)){
            $rst=2;
            $msj='No se Encuentra Solicitud Tecnica "'.$solicitud.
                    '", y Requerimiento "'.$codactu.'"';
        } 

        return Response::json(
                        array(
                            'rst' => $rst, 
                            'datos' => $cms,
                            'msj' => $msj
                        )
                    );
    }

    public function postGuardarCoord()
    {
        $id=Input::get('id','');
        $stid=Input::get('stid','');
        $codactu=Input::get('codactu','');
        $solicitud=Input::get('Solicitud','');
        $cms=SolicitudTecnicaCms::find($id);
        $rst=1;
        $msj='Coordenadas Actualizadas Correctamente';

        if (!is_null($cms)) {
            $cms['coordx_cliente']=Input::get('coordx','');
            $cms['coordy_cliente']=Input::get('coordy','');
            $cms->save();

            $inbound = new Inbound;
            $appointment = [];
            $appointment["coordx"] = $cms['coordx_cliente'];
            $appointment["coordy"] = $cms['coordy_cliente'];
            $responseToa = $inbound->updateActivity($codactu, [],$appointment);

            
            if (isset($responseToa->result) && $responseToa->result != "success") {
                $rst=2;
                $msj=$responseToa->description;
            } else {
                //insertar registro de transaccion
                $movimiento = Movimiento::where('solicitud_tecnica_id', $stid)->latest()->first();
    
                Auth::loginUsingId(697);
                if ($movimiento) {
                    $detalle = new Detalle();
                    $detalle['coordx_tecnico']        = $cms['coordx_cliente'];
                    $detalle['coordy_tecnico']        = $cms['coordy_cliente'];
                    $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                    $detalle['tipo']                  = 9; //guardar de cordenadas
                    $detalle['sms']                   = $msj;
                    $movimiento->detalle()->save($detalle);
                }
                Auth::logout();
            }

            $publicmap = new Publicmap();
            $stmapa = $publicmap->getSolicitudTecnica($codactu, $solicitud);

            $mapa=json_decode(json_encode($stmapa));

            return Response::json(array(
                        'rst' => $rst,
                        'datos'=> $mapa,
                        'msj' => $msj
                    ));
        }
    }

    public function postValidarcoord()
    {
            $codactu=Input::get('codactu');
            $solicitud_id=Input::get('solicitud_id');
            $dateini=date("Y-m-d H:i:s");

            $data=[
               'coord_x1'=> Input::get('coordx'), //coord de actividad
               'coord_y1'=> Input::get('coordy'), //coord de actividad lat 1
               'coord_x2'=> Input::get('coordxtec'), // coord de tecnicos
               'coord_y2'=> Input::get('coordytec') //coord de tecnico lat 2
            ];

            $respuesta = GeoValidacion::getValidadistancia($data);
            if ($respuesta['rpta']==1) {//coordenadas validadas
                $properties["A_ESTADO_VALIDACION_COORDENADAS"]='2';
                $rst=1;
                $mensaje = "Req: ".$codactu.": Validación de Coordenadas OK. ";
            } else {//coordenadas invalidas
                $properties["A_ESTADO_VALIDACION_COORDENADAS"]='1';
                $rst=2;
                $mensaje = "Req: ".$codactu.": Fuera de Rango. A ".
                            $respuesta['metros']." metros del Requerimiento. ";
            }
            
            $inbound = new Inbound();
            $respuestatoa=$inbound->updateActivity($codactu, $properties);
            $datefin=date("Y-m-d H:i:s");
            if(!isset($respuestatoa->result) || $respuestatoa->result!=='success') {
                $mensaje.="\n Falló Actualización en TOA. ";
                $rst=2;
            }
            //insertar registro de transaccion
            $movimiento = Movimiento::where('solicitud_tecnica_id', $solicitud_id)->latest()->first();

            Auth::loginUsingId(697);
            if ($movimiento) {
                $detalle = new Detalle();
                $detalle['coordx_tecnico']        = $data['coord_x2'];
                $detalle['coordy_tecnico']        = $data['coord_y2'];
                $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                $detalle['tipo']                  = 8; // validacion de cordenadas
                $detalle['distancia']             = $respuesta['metros'];
                $detalle['sms']                   = $mensaje;
                $detalle['fecha_inicio']          = $dateini;
                $detalle['fecha_fin']             = $datefin;
                $movimiento->detalle()->save($detalle);
            }
            Auth::logout();
            return Response::json(array(
                        'rst' => $rst,
                        'msj' => $mensaje,
                        'toa' => $respuestatoa
                    ));
    }
}
