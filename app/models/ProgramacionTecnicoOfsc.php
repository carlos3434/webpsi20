<?php

class ProgramacionTecnicoOfsc extends \Eloquent {
	public function get ($filtros = array())
	        {
	            $select = [DB::raw("MAX(gm.id) as gestion_movimiento_id"), "t.nombre_tecnico", "q.nombre as nombre_quiebre", 
	            "t.carnet", "gm.estado_ofsc_id", "gm.estado_id",  "gm.fecha_agenda", "gm.horario_id", "gm.updated_at  as ultima_actualizacion", 
	            "gd.codactu as codactu", "eo.name as estado_ofsc_name", "gm.y_ofsc", "gm.x_ofsc", "eo.nombre as estado_ofsc", 
	            "e.nombre as empresa", "gd.x as x_orden", "gd.y as y_orden"];
	            $response = GestionMovimiento::from("gestiones_movimientos as gm");
	            $response = $response->orderBy("gestion_movimiento_id", "desc");
	           $response = $response->groupBy("gm.id");
	            $response->select($select);
	            foreach ($filtros as $key => $value) {
	                switch ($key) {
	                    case 'empresa':
	                        if ($value!=="")
	                            $response->whereIn("gm.empresa_id", $value);
	                        break;
	                    case 'fecha_agenda':
	                        $fecha = explode(" - ", $value);
	                        $response->whereBetween("fecha_agenda", $fecha);
	                        break;
	                    case 'quiebre':
	                        if ($value!=="")
	                            $response->whereIn("q.id", $value);
	                        break;
	                    default:
	                        # code...
	                        break;
	                }
	            }
	            $response->leftJoin("tecnicos as t", "t.id", "=", "gm.tecnico_id")
	                                ->leftJoin("quiebres as q", "q.id", "=", "gm.quiebre_id")
	                                ->leftJoin("gestiones_detalles as gd", "gd.gestion_id", "=", "gm.gestion_id" )
	                                ->leftJoin("estados_ofsc as eo", "eo.id", "=", "gm.estado_ofsc_id")
	                                ->leftJoin("empresas as e", "e.id", "=", "gm.empresa_id")
	                                ->whereIn("gm.horario_id", [52,51]);
	            $response = $response->get();
	            $data = json_decode(json_encode($response), true);
	            return $data;
	        }
}