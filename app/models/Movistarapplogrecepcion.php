<?php 

class Movistarapplogrecepcion extends \Eloquent
{
    protected $guarded =[];
    protected $table = "movistarapp_log_recepcion";
    
    public static $rules = [
         'solicitud' => 'required|between:1,11|regex:/^[0-9a-zA-Z]+$/',
         'location' => 'between:1,100',
         'date' => 'size:10|date_format:Y-m-d',
         'time_slot' => 'in:AM,PM',
         'observation' => 'between:0,100'
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
    }

    public static function getErrorByMessage($message = "")
    {
        $respuesta = ["error_code" => "", "desc_error" => ""];
        $error = Movistarapperror::find(8);
        if ($message!="") {
            $coincidencia = strpos($message, "obligatorio");
            if ($coincidencia) {
                $error = Movistarapperror::find(10);
                return $error->code_error;
            }
            $coincidencia = strpos($message, "debe tener entre");
            if ($coincidencia) {
                $error = Movistarapperror::find(11);
                return $error->code_error;
            }
            $coincidencia = strpos($message, "formato");
            if ($coincidencia) {
                $error = Movistarapperror::find(12);
                return $error->code_error;
            }
            $coincidencia = strpos($message, "inválido");
            if ($coincidencia) {
                $error = Movistarapperror::find(2);
                return $error->code_error;
            }
            $coincidencia = strpos($message, "contener");
            if ($coincidencia) {
                $error = Movistarapperror::find(13);
                return $error->code_error;
            } else {
                return $error->code_error;
            }
        } else {
            return $error->code_error;
        }
    }

    public static function tracer( $request = [], $response = [], $e=null )
    {
        try {
            $envio = new Movistarapplogrecepcion();
            $envio['request'] = json_encode($request);
            $envio['response'] =json_encode($response);
            $envio['accion'] =$response->accion;
            $envio['error'] =$response->error;
            if (isset($response->code_error)) {
                $envio['code_error'] =$response->code_error;
            }
            if (isset($response->message)) {
                $envio['message'] =$response->message;
                if (isset($e)) {
                    $envio['message'] =$response->message." | ".$e->getCode().":".$e->getMessage()." | ". $e->getFile()." (".$e->getLine().")";
                }
                if (isset($response->message_detalle)) {
                    $envio['message'] =$response->message." | ".$response->message_detalle;
                }
            }
            $envio->save();
        } catch (Exception $e) {
            
        }

    }
}