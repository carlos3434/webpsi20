<?php

class TablaDetalle extends \Eloquent
{

    public $table = "geo_tabla_detalles";

    public static function Campos()
    {
        $tablaId = Input::get('tabla_id');
        $sql = " SELECT GROUP_CONCAT(tc.campo_id ORDER BY orden SEPARATOR '|')
               campo, count(tabla_id) cant,
               GROUP_CONCAT(c.nombre ORDER BY tc.orden SEPARATOR '|') c_detalle
                FROM geo_tabla_campos tc
                INNER JOIN geo_campos c
                on tc.campo_id=c.id
                WHERE tc.tabla_id= $tablaId
                AND tc.estado=1
                GROUP BY tc.tabla_id";

        $r = DB::select($sql);
        if (count($r)>0) {
            return $r[0];
        }
        return [];
    }

    public static function CamposDinamicos()
    {
        $tablaId = Input::get('tabla_id');
        $id = Input::get('c');
        $idr = $id - 1;
        $idrf = $idr - 1;
        $t = Input::get('t');

        $campo = "";
        $campoDos = "";
        $campoTres = "";
        $relacion = "";
        $relation = "";

        if ($id == 1) {
            $campo = "SUBSTRING_INDEX(detalle,'|',1) c$id";
        } elseif ($id == $t) {
            $relation = ", relation";
            $campoDos = "SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idr)
                        , '|',-1
                    ) c$idr";

            if ($idrf == 1) {
                $campoTres = ",SUBSTRING_INDEX(detalle,'|',1) c$idrf";
            } elseif ($idrf > 1) {
                $campoTres = ",SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idrf)
                        , '|',-1
                    ) c$idrf";
            }

            $campo = "SUBSTRING_INDEX(detalle,'|',-1) c$id," 
                . $campoDos . $campoTres;
        } else {
            $relation = ", relation";

            if ($idr == 1) {
                $campoDos = "SUBSTRING_INDEX(detalle,'|',1) c$idr";
            } else {
                $campoDos = "   SUBSTRING_INDEX( 
                                SUBSTRING_INDEX(detalle,'|',$idr)
                                , '|',-1
                            ) c$idr";
            }

            if ($idrf == 1) {
                $campoTres = ",SUBSTRING_INDEX(detalle,'|',1) c$idrf";
            } elseif ($idrf > 1) {
                $campoTres = ",SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idrf)
                        , '|',-1
                    ) c$idrf";
            }

            $campo = "SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$id)
                        , '|',-1
                    ) c$id," . $campoDos . $campoTres;
        }

        if ($relation != '') {
            $aux = "";
            if ($idrf >= 1) {
                $aux = ",'|,|C$idrf',
                        GROUP_CONCAT( DISTINCT(c$idrf) SEPARATOR '|,|C$idrf')";
            }
            $relation = " , CONCAT(
                            'C$idr',
                            GROUP_CONCAT( DISTINCT(c$idr) SEPARATOR '|,|C$idr')
                            $aux
                        ) relation";
        }

        $sql = "SET group_concat_max_len := @@max_allowed_packet;";

        DB::select($sql);

        $sql = "  SELECT c$id as id,c$id as nombre $relation
                FROM (
                    SELECT 
                    $campo
                    FROM geo_tabla_detalle
                    WHERE tabla_id= $tablaId
                ) f 
                GROUP BY c$id
                ORDER BY c$id";

        $r = DB::select($sql);
        return $r;
    }

    public static function CamposDetalle()
    {
        $tablaId = Input::get('tabla_id');
        $id = Input::get('c');
        $idr = $id - 1;
        $t = Input::get('t');
        $where = "";
        $final = 0;
        $selcampo = "";
        $coords = array();

        if (Input::get('idfinal')) {
            $final = Input::get('idfinal');
        }


        $campo = array();

        for ($i = 1; $i <= $id; $i++) {
            if ($i == 1) {
                $campo[] = "SUBSTRING_INDEX(detalle,'|',1) c$i";
            } elseif ($i == $id AND $i == $t) {
                $campo[] = "SUBSTRING_INDEX(detalle,'|',-1) c$i";
            } else {
                $campo[] = "SUBSTRING_INDEX( 
                            SUBSTRING_INDEX(detalle,'|',$i)
                            , '|',-1
                        ) c$i";
            }
        }

        $campos = implode(",", $campo);
        if ($final == 1) {
            $campos.= ' , id,  detalle, coord, dato ';
            $selcampo.=' , id as tabla_detalle_id, detalle, coord, dato ';
        }

        if (Input::has('valor')) {
            if (Input::get('idtipo') == 1) {

                $valor = Input::get('valor');
                for ($i = 0; $i < count($valor); $i++) {
                    $where.= " AND (";
                    for ($j = 0; $j < count($valor[$i]); $j++) {
                        if ($i == 0) {
                            if ($j == 0) {
                                $where.= " detalle LIKE '" . $valor[$i][$j] . "|%' ";
                            } else {
                                $where.= " OR detalle LIKE '" . $valor[$i][$j] . "|%'";
                            }
                        } else {
                            if ($final == 1 && $i == count($valor) - 1) {
                                if ($j == 0) {
                                    $where.= " detalle LIKE '%|" . $valor[$i][$j] . "' ";
                                } else {
                                    $where.= " OR detalle LIKE '%|" . $valor[$i][$j] . "'";
                                }
                            } else {
                                if ($j == 0) {
                                    $where.= " detalle LIKE '%|" . $valor[$i][$j] . "|%' ";
                                } else {
                                   $where.= " OR detalle LIKE '%|" . $valor[$i][$j] . "|%'";
                                }
                            }
                        }
                    }
                    $where.=") ";
                }
            } else {
                $valores = Input::get('valor');
                $zonal=Input::get('zonal','LIM');
                $arrayValor = array();
                $nodo = '';
                $troba='';
                $tap='';
                $amp='';
                foreach ($valores as $val) {
                    if (isset($val["valor"])) {
                        $arrayValor[] = $val["valor"];
                        if ($val["tipo"] == "nodo") {
                            //Obtener coordenadas del poligono NODO
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getNodo($val["valor"]);
                            $nodo=$val["valor"];
                        } elseif ($val["tipo"] == "troba") {
                            $troba = $val["valor"];
                            $data['nodo'] = $nodo;
                            $data['troba'] = $troba;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getTroba($data);
                        } elseif ($val["tipo"] == "amplificador") {
                            $amp=$val["valor"];
                            $sql = "SELECT 'amplificador' AS tipo,
                                IFNULL(coord_x, '') AS coord_x,
                                IFNULL(coord_y, '') AS coord_y
                            FROM
                                `geo_amplificador`
                            WHERE
                                `troba` = '".$troba."' AND `nodo` = '".$nodo."' and amplificador = '".$amp."'";

                            $coords = DB::select($sql);
                        } elseif ($val["tipo"] == "tap") {
                            $tap = $val["valor"];
                            $data['nodo'] = $nodo;
                            $data['troba'] = $troba;
                            $data['amplificador'] = $amp;
                            $data['tap'] = $tap;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getTap($data);
                        } elseif ($val["tipo"] == "mdf") { //T. DIREC
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getMdf($val["valor"]);
                            $mdf=$val["valor"];
                        } elseif ($val["tipo"] == "cable") {
                            $cable=$val["valor"];
                            //$coords = [];
                        } elseif ($val["tipo"] == "armario") {
                            $armario=$val["valor"];
                            $data['mdf'] = $mdf;
                            $data['armario'] = $armario;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getArmario($data);
                        } elseif ($val["tipo"] == "terminald") {
                            $terminald = $val["valor"];
                            $data['mdf'] = $mdf;
                            $data['cable'] = $cable;
                            $data['terminald'] = $terminald;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getTerminald($data);
                        } elseif ($val["tipo"] == "terminalf") {
                            $terminalf = $val["valor"];
                            $data['mdf'] = $mdf;
                            $data['armario'] = $armario;
                            $data['terminalf'] = $terminalf;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getTerminalf($data);
                        }
                    }
                }
                $valor = implode("|", $arrayValor);
                $where = " AND detalle LIKE '%$valor%' ";
            }
        }

        $filtroestadouno='';
        $filtroestadodos='';

        $sql = "  SELECT c$id as id,c$id as nombre $selcampo $filtroestadouno
                FROM (
                    SELECT 
                    $campos
                    FROM geo_tabla_detalle
                    WHERE tabla_id= $tablaId
                    $where
                ) f 
                $filtroestadodos
                GROUP BY c$id
                ORDER BY c$id";
      //var_dump($sql);
        $r = DB::select($sql);
        return array("datos"=>$r,"coords"=>$coords);
    }
    
    /**
     * 
     * @param string $tabla lista de codigos en: 'geo_tablas'
     * @param string $detalle  tipo: "LIM|CA|R035|X..."
     * @param boolean $unRegistro si es TRUE retorna array bidimensional, 
     * sino array simple
     * @return array
     */
    public function coordenadasPorTabla($tabla, $detalle, $unRegistro = false)
    {
        $model = DB::table('geo_tabla_detalle AS gtd')
                ->join(
                    'geo_tablas AS gt', function($join) use ($tabla) {
                        $join->on('gt.id', '=', 'gtd.tabla_id')
                        ->where('gt.id', '=', $tabla);
                    }
                )
                ->where('gtd.detalle', 'like', "$detalle%")
                ->where('gtd.estado', '=', 1);
        if ($unRegistro) {
            return $model->select(
                'detalle', 
                'coord'
            )
                ->first();
        } else {
            return $model->select(
                'detalle', 
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',1) AS coord_x"),
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',-1) AS coord_y")
            )
                ->get();
        }
    }

    /**
     * 
     * @param string $zonal
     * @param string $nodo
     * @param string $troba
     * @return array
     */
    public function coordenadasGeoAlarmaTap($zonal, $nodo, $troba)
    {
        $model = DB::table('geo_alarma_tap AS gat')
            ->select(
                'gat.codcli', 
                'gat.nombres',
                'gat.ipaddress',
                'gat.interface',
                'gat.amplificador',
                'gat.tap',
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',1) AS coord_x"),
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',-1) AS coord_y")
            )
            ->leftJoin(
                'geo_tabla_detalle AS gtd', function($join)
                {
                $join->on('gat.tdetalle', '=', 'gtd.detalle')
                    ->where('gtd.tabla_id', '=', 5)
                    ->where('gtd.estado', '=', 1);
                }
            )
            ->where('gat.zonal', '=', $zonal)
            ->where('gat.nodo', '=', $nodo)
            ->where('gat.troba', '=', $troba);
         
        return $model->get();
    }

    public static function CoordsTap()
    {
        $troba = '';
        $nodo = '';
        $amp = '';
        $tap = '';
        $valores = Input::get('valor');
        foreach ($valores as $valor) {
            if ($valor["tipo"] == 'nodo') {
                $nodo = $valor["valor"];
            } elseif ($valor["tipo"] == 'troba') {
                $troba = $valor["valor"];
            } elseif ($valor["tipo"] == 'amplificador') {
                $amp = $valor["valor"];
            } elseif ($valor["tipo"] == 'tap') {
                $tap = $valor["valor"];
            }
        }
        $sql = "SELECT 
                    IFNULL(coord_x, '') AS coord_x,
                    IFNULL(coord_y, '') AS coord_y
                FROM
                    `geo_tap`
                WHERE
                    `troba` = '".$troba."' AND `nodo` = '".$nodo."' and amplificador = '".$amp."' AND tap = '".$tap."'";
        $rs = DB::select($sql);

        return $rs;
    }

}
