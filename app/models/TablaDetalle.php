<?php

class TablaDetalle extends \Eloquent
{

    public $table = "geo_tabla_detalles";

    public static function Campos()
    {
        $tablaId = Input::get('tabla_id');
        $sql = " SELECT GROUP_CONCAT(tc.campo_id ORDER BY orden SEPARATOR '|')
               campo, count(tabla_id) cant,
               GROUP_CONCAT(c.nombre ORDER BY tc.orden SEPARATOR '|') c_detalle
                FROM geo_tabla_campos tc
                INNER JOIN geo_campos c
                on tc.campo_id=c.id
                WHERE tc.tabla_id= $tablaId
                AND tc.estado=1
                GROUP BY tc.tabla_id";

        $r = DB::select($sql);
        return $r[0];
    }

    public static function CamposDinamicos()
    {
        $tablaId = Input::get('tabla_id');
        $id = Input::get('c');
        $idr = $id - 1;
        $idrf = $idr - 1;
        $t = Input::get('t');

        $campo = "";
        $campoDos = "";
        $campoTres = "";
        $relacion = "";
        $relation = "";

        if ($id == 1) {
            $campo = "SUBSTRING_INDEX(detalle,'|',1) c$id";
        } elseif ($id == $t) {
            $relation = ", relation";
            $campoDos = "SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idr)
                        , '|',-1
                    ) c$idr";

            if ($idrf == 1) {
                $campoTres = ",SUBSTRING_INDEX(detalle,'|',1) c$idrf";
            } elseif ($idrf > 1) {
                $campoTres = ",SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idrf)
                        , '|',-1
                    ) c$idrf";
            }

            $campo = "SUBSTRING_INDEX(detalle,'|',-1) c$id," 
                . $campoDos . $campoTres;
        } else {
            $relation = ", relation";

            if ($idr == 1) {
                $campoDos = "SUBSTRING_INDEX(detalle,'|',1) c$idr";
            } else {
                $campoDos = "   SUBSTRING_INDEX( 
                                SUBSTRING_INDEX(detalle,'|',$idr)
                                , '|',-1
                            ) c$idr";
            }

            if ($idrf == 1) {
                $campoTres = ",SUBSTRING_INDEX(detalle,'|',1) c$idrf";
            } elseif ($idrf > 1) {
                $campoTres = ",SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$idrf)
                        , '|',-1
                    ) c$idrf";
            }

            $campo = "SUBSTRING_INDEX( 
                        SUBSTRING_INDEX(detalle,'|',$id)
                        , '|',-1
                    ) c$id," . $campoDos . $campoTres;
        }

        if ($relation != '') {
            $aux = "";
            if ($idrf >= 1) {
                $aux = ",'|,|C$idrf',
                        GROUP_CONCAT( DISTINCT(c$idrf) SEPARATOR '|,|C$idrf')";
            }
            $relation = " , CONCAT(
                            'C$idr',
                            GROUP_CONCAT( DISTINCT(c$idr) SEPARATOR '|,|C$idr')
                            $aux
                        ) relation";
        }

        $sql = "SET group_concat_max_len := @@max_allowed_packet;";

        DB::select($sql);

        $sql = "  SELECT c$id as id,c$id as nombre $relation
                FROM (
                    SELECT 
                    $campo
                    FROM geo_tabla_detalle
                    WHERE tabla_id= $tablaId
                ) f 
                GROUP BY c$id
                ORDER BY c$id";

        $r = DB::select($sql);
        //echo $sql;

        return $r;
    }

    public static function CamposDetalle()
    {
        $tablaId = Input::get('tabla_id');
        $id = Input::get('c');
        $idr = $id - 1;
        $t = Input::get('t');
        $where = "";
        $final = 0;
        $selcampo = "";
        $coords = array();

        if (Input::get('idfinal')) {
            $final = Input::get('idfinal');
        }


        $campo = array();

        for ($i = 1; $i <= $id; $i++) {
            if ($i == 1) {
                $campo[] = "SUBSTRING_INDEX(detalle,'|',1) c$i";
            } elseif ($i == $id AND $i == $t) {
                $campo[] = "SUBSTRING_INDEX(detalle,'|',-1) c$i";
            } else {
                $campo[] = "SUBSTRING_INDEX( 
                            SUBSTRING_INDEX(detalle,'|',$i)
                            , '|',-1
                        ) c$i";
            }
        }

        $campos = implode(",", $campo);
        if ($final == 1) {
            $campos.= ' , id,  detalle, coord, dato ';
            $selcampo.=' , id as tabla_detalle_id, detalle, coord, dato ';
        }

        if (Input::has('valor')) {
            if (Input::get('idtipo') == 1) {

            $valor = Input::get('valor');
            for ($i = 0; $i < count($valor); $i++) {
                $where.= " AND (";
                for ($j = 0; $j < count($valor[$i]); $j++) {
                    if ($i == 0) {
                    if ($j == 0) {
                        $where.= " detalle LIKE '" . $valor[$i][$j] . "|%' ";
                    } else {
                        $where.= " OR detalle LIKE '" . $valor[$i][$j] . "|%'";
                    }
                    } else {
                if ($final == 1 && $i == count($valor) - 1) {
                    if ($j == 0) {
                        $where.= " detalle LIKE '%|" . $valor[$i][$j] . "' ";
                    } else {
                        $where.= " OR detalle LIKE '%|" . $valor[$i][$j] . "'";
                    }
                } else {
                    if ($j == 0) {
                        $where.= " detalle LIKE '%|" . $valor[$i][$j] . "|%' ";
                    } else {
                       $where.= " OR detalle LIKE '%|" . $valor[$i][$j] . "|%'";
                    }
                }
                    }
                }
        $where.=") ";
            }
            } else {
                $valores = Input::get('valor');
                $arrayValor = array();
                $nodo = '';
                $troba='';
                foreach ($valores as $val){
                    if(isset($val["valor"])) {
                        $arrayValor[] = $val["valor"];
                        if($val["tipo"] == "nodo"){
                            //Obtener coordenadas del poligono NODO
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getNodo($val["valor"]);
                            $nodo=$val["valor"];
                        }elseif($val["tipo"] == "troba"){
                            $troba = $val["valor"];
                            $data['nodo'] = $nodo;
                            $data['troba'] = $troba;
                            $geofftt = new Geofftt();
                            $coords = $geofftt->getTroba($data);
                        }elseif($val["tipo"] == "amplificador"){
                            $sql = "SELECT 
                                IFNULL(coord_x, '') AS coord_x,
                                IFNULL(coord_y, '') AS coord_y
                            FROM
                                `geo_amplificador`
                            WHERE
                                `troba` = '".$troba."' AND `nodo` = '".$nodo."' and amplificador = '".$val["valor"]."'";

                            $coords = DB::select($sql);
                        }
                    }
                }
                $valor = implode("|", $arrayValor);
                $where = " AND detalle LIKE '%$valor%' ";
            }
        }

        $filtroestadouno='';
        $filtroestadodos=''; /*
        if (isset(Input::get('modulo')) && Input::get('modulo')=='fftt') {
             if (Input::get('codactu')){
            $codactu=Input::get('codactu');
               if($id==1){
                $filtroestadouno=", if(gd.zonal=f.c$id,1,0) estado ";
               } else {
                $filtroestadouno=
                ", if(SUBSTRING_INDEX(SUBSTRING_INDEX(fftt,'|',$id-1),'|',-1)=
                    if(LENGTH(c$id)=1,CONCAT('0',f.c$id),f.c$id),1,0) estado ";
               }
                $filtroestadodos="JOIN gestiones_detalles gd
                                  where gestion_id='$codactu' ";
             }
        }*/
        
        $sql = "  SELECT c$id as id,c$id as nombre $selcampo $filtroestadouno
                FROM (
                    SELECT 
                    $campos
                    FROM geo_tabla_detalle
                    WHERE tabla_id= $tablaId
                    $where
                ) f 
                $filtroestadodos
                GROUP BY c$id
                ORDER BY c$id";

        //echo $sql;
        $r = DB::select($sql);

        return array("datos"=>$r,"coords"=>$coords);
        //print_r( $sql);
    }
    
    /**
     * 
     * @param string $tabla lista de codigos en: 'geo_tablas'
     * @param string $detalle  tipo: "LIM|CA|R035|X..."
     * @param boolean $unRegistro si es TRUE retorna array bidimensional, 
     * sino array simple
     * @return array
     */
    public function coordenadasPorTabla($tabla, $detalle, $unRegistro = false)
    {
        $model = DB::table('geo_tabla_detalle AS gtd')
//                ->select(
//                    'detalle', 
//                    'coord'
//                )
                ->join(
                    'geo_tablas AS gt', function($join) use ($tabla) {
                        $join->on('gt.id', '=', 'gtd.tabla_id')
                        ->where('gt.id', '=', $tabla);
                    }
                )
//            ->join("geo_figuras AS gf", "gf.id", "=", "gt.figura_id")
                ->where('gtd.detalle', 'like', "$detalle%")
                ->where('gtd.estado', '=', 1);
        //echo $model->toSql();return [];        
        if ($unRegistro) {
            return $model->select(
                'detalle', 
                'coord'
            )
                ->first();
        } else {
            return $model->select(
                'detalle', 
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',1) AS coord_x"),
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',-1) AS coord_y")
            )
                ->get();
        }
    }

    /**
     * 
     * @param string $zonal
     * @param string $nodo
     * @param string $troba
     * @return array
     */
    public function coordenadasGeoAlarmaTap($zonal, $nodo, $troba)
    {
        $model = DB::table('geo_alarma_tap AS gat')
            ->select(
                'gat.codcli', 
                'gat.nombres',
                'gat.ipaddress',
                'gat.interface',
                //'gat.detalle',
                'gat.amplificador',
                'gat.tap',
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',1) AS coord_x"),
                DB::raw("SUBSTRING_INDEX(gtd.coord,',',-1) AS coord_y")
            )
//            ->leftJoin(
//                'geo_tabla_detalle AS b', 'gat.tdetalle', '=', 'gtd.detalle'
//            )
            ->leftJoin(
                'geo_tabla_detalle AS gtd', function($join)
                {
                $join->on('gat.tdetalle', '=', 'gtd.detalle')
                    ->where('gtd.tabla_id', '=', 5)
                    ->where('gtd.estado', '=', 1);
                }
            )
            ->where('gat.zonal', '=', $zonal)
            ->where('gat.nodo', '=', $nodo)
            ->where('gat.troba', '=', $troba);
         
//        $sub = DB::table('geo_alarma_tap AS gat')
//            ->select(
//                DB::raw("CONCAT_WS('|',zonal,nodo,troba,amplificador,tap)")
//            )
//            ->whereRaw(
//                "gat.zonal='$zonal' AND gat.nodo='$nodo' AND 
//                gat.troba='$troba'"
//            );       
//        $model = DB::table('geo_tabla_detalle AS gtd')
//            ->select('detalle', 'coord')
//            ->join(
//                'geo_tablas AS gt', function($join) {
//                    $join->on('gt.id', '=', 'gtd.tabla_id')
//                    ->where('gt.id', '=', 5);
//                }
//            )
//            ->whereRaw("gtd.detalle IN ({$sub->toSql()})")
//            ->where('gtd.estado', '=', 1);
        return $model->get();
        //dd($model->toSql());//->get();
    }

    public static function CoordsTap(){
        $troba = '';
        $nodo = '';
        $amp = '';
        $tap = '';
        $valores = Input::get('valor');
        foreach ($valores as $valor){
            if($valor["tipo"] == 'nodo'){
                $nodo = $valor["valor"];
            }elseif($valor["tipo"] == 'troba'){
                $troba = $valor["valor"];
            }elseif($valor["tipo"] == 'amplificador'){
                $amp = $valor["valor"];
            }elseif($valor["tipo"] == 'tap'){
                $tap = $valor["valor"];
            }
        }
        $sql = "SELECT 
                    IFNULL(coord_x, '') AS coord_x,
                    IFNULL(coord_y, '') AS coord_y
                FROM
                    `geo_tap`
                WHERE
                    `troba` = '".$troba."' AND `nodo` = '".$nodo."' and amplificador = '".$amp."' AND tap = '".$tap."'";
        $rs = DB::select($sql);

        return $rs;
    }

}
