<?php
class GeoTablaCampos extends Eloquent {

    public function campos()
    {
        return $this->belongsTo('GeoCampos');
    }

}