<?php
class GeoTap extends Eloquent
{
    public $table = "geo_tap";

    public static function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            
            $result =   DB::table('geo_tap')
              ->select(
                  'tap as id',
                  'tap as nombre',
                  DB::raw('GROUP_CONCAT(DISTINCT CONCAT("A",amplificador) SEPARATOR "|,|") as relation')
                )
                    ->groupBy('tap')
                    ->orderBy('id', 'asc')
                    ->get();

            return $result;
        }
    }

    public static function filtrar ($filtro) {

    }

    /*public static function postGeoTapAll()
    {
        $r  =   DB::table('geo_tap')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r;
    }

    public static function postNodoFiltro($array)
    {
        $r  =   DB::table('geo_tap')
                  ->select(
                      'nodo as id',
                      DB::raw("nodo as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('nodo')
                  ->get();
        return $r;
    }

    public static function postTrobaFiltro($array)
    {
        $r  =   DB::table('geo_tap')
                  ->select(
                      'troba as id',
                      DB::raw("troba as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("nodo", $array['nodo'])
                  ->groupBy('troba')
                  ->get();
        return $r;
    }

    public static function postAmplificadorFiltro($array)
    {
        $r  =   DB::table('geo_tap')
                  ->select(
                      'amplificador as id',
                      DB::raw("amplificador as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("nodo", $array['nodo'])
                  ->whereIn("troba", $array['troba'])
                  ->groupBy('amplificador')
                  ->get();
        return $r;
    }

    public static function postTapFiltro($array)
    {
        $r  =   DB::table('geo_tap')
                  ->select(
                      'tap as id',
                      DB::raw("tap as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("nodo", $array['nodo'])
                  ->whereIn("troba", $array['troba'])
                  ->whereIn("amplificador", $array['amplificador'])
                  ->groupBy('tap')
                  ->get();
        return $r;
    }*/
}