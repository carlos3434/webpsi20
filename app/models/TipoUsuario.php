<?php

class TipoUsuario extends Eloquent
{

    public $table = "tipos_usuarios";

    public function usuario()
    {
        return $this->belongsTo('Usuario');
    }

}