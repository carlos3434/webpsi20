<?php 

class Material extends Eloquent
{
    protected $guarded =[];
    protected $table = "material";
    
    public static function boot()
    {
        parent::boot();

        static::saving(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
    }

    
}