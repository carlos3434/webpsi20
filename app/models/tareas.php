<?php
class tareas { 

	public static $rules_refresh = [
	        'asunto' => 'required',
	        'actividad' => 'required',
	        'serieDeco' => 'required',
	        'telefonoOrigen' => 'required'
    ];

    function insert($request)
    {   
        Log::useDailyFiles(storage_path().'/logs/tareas.log');
        Log::info([$request]);
             
    	$validator = Validator::make((array)$request, self::$rules_refresh);
        if ($validator->fails()) {
        	throw new SoapFault('SOAP-ENV:Client', $validator->messages()->all()[0]);
        	return;
		}

        $result = 1;   
        $envio = ["request" => $request];
        Queue::push('GenerarUssd', $envio);
        return $result;
    }
}