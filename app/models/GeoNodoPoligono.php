<?php 
class GeoNodoPoligono extends Eloquent
{

    public static function getNodozonal($zonal)
    {
        $r  =   DB::table('geo_nodopoligono')
                  ->select(
                      'nodo as id',
                      DB::raw(
                          'nodo as nombre'
                      )
                  )
                  ->where(
                      function($query) use($zonal) {
                          $query->where('zonal', '=', $zonal);
                      }
                  )
                  ->groupBy('nodo')
                  ->orderBy('nodo', 'asc')
                  ->get();
        return $r;
    }

    public static function getCoordNodo($nodo)
    {
        $r  =   DB::table('geo_nodopoligono')
                  ->select(
                      'nodo as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng ")
                  )
                  ->whereIn("nodo", $nodo)
                  ->orderBy('nodo', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }

    public static function postGeoNodoPoligonoAll()
    {
        $r  =   DB::table('geo_nodopoligono')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r;
    }

    public static function postNodoPoligonoFiltro($array)
    {
        $r  =   DB::table('geo_nodopoligono')
                  ->select(
                      'nodo as id',
                      DB::raw("nodo as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('nodo')
                  ->get();
        return $r;
    }

    

  
}
