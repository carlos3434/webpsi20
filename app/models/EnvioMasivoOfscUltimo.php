<?php
use Ofsc\Inbound;

/**
*   modelo para los estados que se registra en 'envios masivos'
*/
class EnvioMasivoOfscUltimo extends \Eloquent
{
    protected $table = "envio_masivo_ofsc_ultimo";
    private $select = array();

    public function __construct()
    {
        $this->select = ["emou.codactu", "emou.aid", "emou.empresa",
                        "emou.mdf as mdf", "emou.mensaje", "emou.numintentos",
                        "emou.estado",  "act.nombre as tipo_actividad_id",
                        "um.estado_ofsc_id as estado_ofsc_id",
                        "eo.nombre as estado_ofsc","act.nombre as actividad_id",
        DB::raw("(CASE emo.tipo_masivo WHEN 'MasivoArtisan' THEN 'Actividades Temporales' WHEN 'MasivoBandeja' THEN 'Envios desde Bandeja' WHEN 'Componentes' THEN 'Actualización de Componentes' WHEN 'MasivoLiquidado' THEN 'Actividades Liquidadas' END) as tipo_masivo"),
        DB::raw("if( emou.updated_at = '0000-00-00 00:00:00', emou.fec_created, emou.updated_at)  as fec_movimiento"),
        "est.nombre as estado_psi"];
    }
    public function get($bodySql = array())
    {
        $result = array();
        $orderBy = (isset($bodySql["orderBy"]))? $bodySql["orderBy"] : 'fec_movimiento';
        $orderDir = (isset($bodySql["orderDir"]))? $bodySql["orderDir"] : 'desc';
        $length = (isset($bodySql["length"]))? $bodySql["length"] : 10;
        $start = (isset($bodySql["start"]))? $bodySql["start"] : 0;
        $filtros = (isset($bodySql["filtros"]))? $bodySql["filtros"] : array();

		$result["datos"] = EnvioMasivoOfscUltimo::from("envio_masivo_ofsc_ultimo as emou")->orderBy($orderBy, $orderDir)
                                            ->orderBy("um.id", "desc");
		$result["datos"]
			->select($this->select);
			
		$result["datos"]
			->leftJoin("ultimos_movimientos as um", "um.gestion_id", "=", "emou.id_gestion")
                                           // ->leftJoin("gestiones_movimientos as gm", "gm.gestion_id", "=", "emou.id_gestion")
			->leftJoin("estados_ofsc as eo", "eo.id", "=", "um.estado_ofsc_id")
			->leftJoin("estados as est", "est.id", "=", "um.estado_id")
			->leftJoin("envio_masivo_ofsc as emo", "emo.id", "=", "emou.id_ultimo_masivo")
			->leftJoin("actividades_tipos as act", "act.id", "=", "emou.tipo_actividad_id")
			;
		if (count($filtros) > 0) {
			if  ($filtros["codactu"]>0) {
				$result["datos"]->where("emou.codactu", "=",$filtros["codactu"] );
			} else {
				foreach ($filtros as $key => $value) {
					switch ($key) {
						case 'empresa':
							if($value!==""){
								$result["datos"]
								->leftJoin("empresas as e", "e.nombre", "=", "emou.empresa")
								->whereIn("e.id", $value);
							}
							break;
						case 'estado':
							if ($value!=="") {
								$result["datos"]->where("emou.estado", "=", $value);
							}
							break;
						case 'estado_psi':
							if ($value!=="") {
								$result["datos"]->whereIn("est.id",$value);
							}
							break;
						case 'estado_ofsc':
							if ($value!=="") {
								$result["datos"]->whereIn("eo.id", $value);
							}
							break;
						case 'fec_movimiento':
							if ($value!="") {
								$fecha = explode(" - ", $value);
								$fecha[1].=" 23:59:59";
								$result["datos"]->whereRaw("IF(emou.updated_at ='0000-00-00 00:00:00', emou.fec_created, emou.updated_at) BETWEEN '".$fecha[0]."' AND '".$fecha[1]."' ");
							}
							break;
						case 'nodo':
							if ($value!="") {
								$result["datos"]->whereIn("emou.mdf", $value);
							}
							break;
						case 'tipo_actividad_id':
							if ($value!="") {
								$result["datos"]->whereIn("emou.tipo_actividad_id", $value);
							}
							break;
						case 'tipo_masivo':
							if ($value!="") {
								$result["datos"]->where("emo.tipo_masivo", $value);
							}
							break;
						default:
							# code...
							break;
					}
				}
			}
		}
		$r = $result["datos"];
		if (isset($bodySql["download_excel"])) {
			if($bodySql["download_excel"]){
				$datos  = $r->get();
				$result["datos"] =  json_decode(json_encode($datos), true);
				
				return $result;
			}
		}
		
		$result["total"] = count($r->get());
		$datos = $r->limit($length)->offset($start)->get();
		$result["datos"] =  json_decode(json_encode($datos), true);
		
        return $result;
    }
}