<?php

class Bucket extends \Eloquent
{
    protected $table = "bucket";
    protected $fillable = ['id', 'bucket_ofsc', 'parent_id', 'nombre', 'status', 'type', 'estado'];
}
