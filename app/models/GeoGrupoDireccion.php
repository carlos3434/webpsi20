<?php 
class GeoGrupoDireccion extends Eloquent
{
    protected $table = "geo_grupo_direccion";

    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function direcciones()
    {
        return $this->hasMany('GeoDireccion');
    }

}