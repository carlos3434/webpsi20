<?php 
class GeocercaProyecto extends Eloquent
{
    protected $table = "geocerca_proyecto";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
            $table->usuario_updated_at = \Auth::id();
            }
        );
        static::creating(
            function ($table) {
            $table->usuario_created_at = \Auth::id();
            }
        );
    }

    public function geocercazonas()
    {
        return $this->hasMany('GeocercaProyectoZonas');
    }

    public static function guardar(array $data)
    {
        $query = new GeocercaProyecto;
        $query->nombre = $data['nombre'];
        $query->save();

        return $query;
    }

    public static function listar()
    {
       $r  =   GeocercaProyecto::with("geocercazonas")
                ->select(
                    'id',
                    'nombre',
                    'estado',
                    DB::raw('estado_publico as publico'),
                    DB::raw('DATE(created_at) as created')
                )
                //->where('estado', '=','1')
                ->orderBy('id', 'asc')
                ->get();
        return $r;
    }

    public static function actualizar($id)
    { 
        $array=array();
        $array=Input::all();
        unset($array['idproyecto']);
        unset($array['token']);

        return GeocercaProyecto::where('id', $id)
                    ->update($array); 
    }

}