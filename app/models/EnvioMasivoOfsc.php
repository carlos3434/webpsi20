<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Ofsc\Inbound;
use Ofsc\Activity;

/**
*   modelo para los estados que se registra en 'envios masivos'
*/
class EnvioMasivoOfsc extends \Eloquent
{
    public $table="envio_masivo_ofsc";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $guarded = array();

    public function envioOrdenOfsc($job, $actuacion)
    {
        Auth::loginUsingId($actuacion['authId']);
        list($nodoOrMdf, $troba)=explode("|", $actuacion['fftt']);
        $workZone = $nodoOrMdf.'_'.$troba;

        $hoy = date("Y-m-d");
        $actuArray = array();
        $error = $bucket = "";

        $actividadTipo= ActividadTipo::find($actuacion['actividadTipoId']);
        if ( isset($actividadTipo->label) && isset($actividadTipo->duracion) ) {
            $workType = $actividadTipo->label;
            $key = $hoy.'|AM|'.$workType.'|'.$actuacion['quiebre'].'|'.$workZone;
            $valor = Redis::get($key);
            $capacidad = json_decode($valor, true);
            if (isset($capacidad)) {
                $bucket = $capacidad['location'];
            } else {
                $error = "No se encontro bucket";
            }
        } else {
            $error = "No se encontro tipo de actividad";
        }
        $insert =  [
            'envio_masivo_ofsc_grupo_id' => $actuacion['grupoEnvio'],
            'codactu' => $actuacion['codactu'],
            'tipo_actividad_id' => $actuacion['actividadTipoId'],
            'empresa' => $actuacion['empresa'],
            'accion' => 'inbound_interface',
            'aid' => '',
            'mdf' => $actuacion['mdf'],
            'tipo_masivo'=>'EnvioMasivo',
            'tipo_actividad_id' =>$actuacion['actividadTipoId'],
            'mensaje' =>$error,
            'estado'=>0,
            'bucket'=>$bucket
        ];
        if ($bucket == '') {
            EnvioMasivoOfsc::create($insert);
            Auth::logout();
            $job->delete();
            return;
        }
        //inicio envio 
        $sla = true;
        $estadoComponente=0;
        $telefonosContacto=[];
        $save = array(
            "rst" => 2,
            "msj" => "Bucket:" . $bucket . " | Sin reultados.",
            "error" => "",
            "gestion_id" => 0
        );
        $actu = Gestion::getCargar($actuacion['codactu']);
        if (!isset($actu['datos'][0])) {
            //no se encontro actuacion
            $insert["estado"] = 0;
            $insert["mensaje"] = "no se encontro actuacion";
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);
            Auth::logout();
            $job->delete();
            return;
        }

        $actuObj = $actu["datos"][0];
        $actuArray = (array) $actuObj;
        $parametros=[
            'tipoEnvio' => 'sla',
            'nenvio' => 1,
            'fechaAgenda' => '',
            'timeSlot' => '',
            'external_id' => $bucket
        ];
        $response = InboundHelper::envioOrdenOfsc(
            $parametros,
            $actuObj,
            $sla,
            $estadoComponente,
            $telefonosContacto
        );

        if (isset($response->data->data->commands->command->appointment->aid)) {
            $aid = $response->data->data->commands->command->appointment->aid;

            //actualizando telefono de contacto
            $actuArray['fonos_contacto']=implode("|", $telefonosContacto);
            $actuArray['noajax'] = "ok";
            $actuArray['gestion_id'] = $actuObj->id;
            $actuArray['fecha_agenda']='';
            $actuArray['x'] = $actuArray['coord_x'];
            $actuArray['y'] = $actuArray['coord_y'];
            $actuArray['estado_agendamiento'] = "3-0";
            $actuArray['motivo'] = 2;
            $actuArray['submotivo'] = 18;
            $actuArray['estado'] = 7;
            $actuArray['aid'] = $aid;
            $actuArray['estado_ofsc_id'] = 1;
            $actuArray['envio_ofsc']=2;
            $actuArray['programado']=1;
            $actuArray['componente']=$estadoComponente;

            if (Input::has('desprogramar') AND 
                Input::get('desprogramar')=='ok') {
                $actuArray['programado']=0;
            }
            Input::replace($actuArray);
            $errorContr = new ErrorController();
            $objGestionMov = new GestionMovimientoController($errorContr);
            $objGestionMov->postRegistrar($actuArray);

            //Registro de envío masivo OK
            $insert["aid"] = $aid;
            $insert["estado"] = 1;
            $insert["mensaje"] = "Registro y envio a OFSC correcto.";
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);

        } else {
            $msjErrorOfsc='';
            if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
            }
            //Registro de envío masivo ERROR
            $insert["estado"] = 0;
            $insert["mensaje"] = $msjErrorOfsc;
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);
        }
    
        Auth::logout();
        $job->delete();
        return;
    }
    public function envioOrdenOfscTest($job, $actuacion)
    {
        Auth::loginUsingId($actuacion['authId']);
        list($nodoOrMdf, $troba)=explode("|", $actuacion['fftt']);
        $workZone = $nodoOrMdf.'_'.$troba;

        $hoy = date("Y-m-d");
        $actuArray = array();
        $error = $bucket = "";

        $actividadTipo= ActividadTipo::find($actuacion['actividadTipoId']);
        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)) {
            $workType = $actividadTipo->label;
            $key = $hoy.'|AM|'.$workType.'|'.$actuacion['quiebre'].'|'.$workZone;
            $valor = Redis::get($key);
            $capacidad = json_decode($valor, true);
            if (isset($capacidad)) {
                $bucket = $capacidad['location'];
            } else {
                $error = "No se encontro bucket";
            }
        } else {
            $error = "No se encontro tipo de actividad";
        }
        $insert =  [
            'envio_masivo_ofsc_grupo_id' => $actuacion['grupoEnvio'],
            'codactu' => $actuacion['codactu'],
            'tipo_actividad_id' => $actuacion['actividadTipoId'],
            'empresa' => $actuacion['empresa'],
            'accion' => 'inbound_interface',
            'aid' => '',
            'mdf' => $actuacion['mdf'],
            'tipo_masivo'=>'EnvioMasivoTest',
            'tipo_actividad_id' =>$actuacion['actividadTipoId'],
            'mensaje' =>$error,
            'estado'=>0,
            'bucket'=>$bucket
        ];
        if ($bucket == '') {
            EnvioMasivoOfsc::create($insert);
            Auth::logout();
            $job->delete();
            return;
        }
        //inicio envio
        $save = array(
            "rst" => 2,
            "msj" => "Bucket:" . $bucket . " | Sin reultados.",
            "error" => "",
            "gestion_id" => 0
        );
        $actu = Gestion::getCargar($actuacion['codactu']);
        if (!isset($actu['datos'][0])) {
            //no se encontro actuacion
            $error='no se encontro actuacion';
            $insert["estado"] = 0;
            $insert["mensaje"] = $error;
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);
            Auth::logout();
            $job->delete();
            return;
        }

        $actuObj = $actu["datos"][0];
        $actuArray = (array) $actuObj;
        $parametros=[
            'tipoEnvio' => 'sla',
            'nenvio' => 1,
            'fechaAgenda' => '',
            'timeSlot' => '',
            'external_id' => $bucket
        ];
        $sla=true;
        $estCompo=0;
        $tel=[];
        //envio a test o train
        Config::set("ofsc.auth.company", 'telefonica-pe.test');
        
        $response = InboundHelper::envioOrdenOfsc(
            $parametros,
            $actuObj,
            $sla, //sla
            $estCompo, //estado componentes
            $tel //telefono de contacto
        );
        if (isset($response->data->data->commands->command->appointment->aid)) {
            $aid = $response->data->data->commands->command->appointment->aid;
            //Registro de envío masivo OK
            $insert["aid"] = $aid;
            $insert["estado"] = 1;
            $insert["mensaje"] = "Registro y envio a OFSC correcto.";
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);
        } else {
            $msjErrorOfsc='';
            if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
            }
            //Registro de envío masivo ERROR
            $insert["estado"] = 0;
            $insert["mensaje"] = $msjErrorOfsc;
            $insert["bucket"] = $bucket;
            EnvioMasivoOfsc::create($insert);

        }

    
        Auth::logout();
        $job->delete();
            return;
    }
    public function liquidarOrden($job, $actuacion)
    {
        $activity = new Activity();
        $response = $activity->getActivity($actuacion['aid']);
        if (isset($response->error) && $response->error==true) {
            echo $response->errorMsg;
            $job->delete();
            return;
        }
        Auth::loginUsingId($actuacion['authId']);
        $aid = $actuacion['aid'];
        $codactu = $actuacion['codactu'];
        $actividadTipoId = $actuacion['actividad_tipo_id'];
        $envioMasivo = [
            "envio_masivo_ofsc_grupo_id" => $actuacion['grupoEnvio'],
            "codactu" => $codactu,
            "mdf" => '',
            "tipo_actividad_id" => $actividadTipoId,
            "aid" => $aid,
            "tipo_masivo" => "MasivoLiquidado",
            "empresa" => ''
        ];
        switch ($response->data['status']) {
            case 'pending':
                $actividad['XA_NOTE']='Se liquido en Legados';
                $inbound = new Inbound();
                $response = $inbound->cancelActivity($aid, $codactu, $actividad);
                if (isset(
                    $response->data->data->commands->command->appointment
                        ->report->message
                )) {

                    $resultCode = $response->data->data->commands->command
                                    ->appointment->report->message;

                    if (isset($resultCode->result) && $resultCode->result=='error') {
                        $estado = 0;
                        $mensaje =  $resultCode->description;
                    } else {
                        for ($y = 0; $y < count($resultCode); $y++) {
                            if ($resultCode[$y]->type=='cancel'
                                && $resultCode[$y]->result=='success' ) {

                                DB::table('ultimos_movimientos')
                                    ->where('aid', $aid)
                                    ->where('codactu', $codactu)
                                    ->update(['liquidado_ofsc' => 1]);
                                $estado = 1;
                                $mensaje = 'Envio de cancelacion exitosa';
                            }
                        }
                    }
                }
                break;
            case 'complete':
                DB::table('ultimos_movimientos')
                    ->where('aid', $aid)
                    ->where('codactu', $codactu)
                    ->update(['liquidado_ofsc' => 1]);
                $estado = 1;
                $mensaje =  'cancelado en TOA';
                break;
            case 'started':
            case 'notdone':
            case 'cancelled':
            case 'suspended':
                $estado = 0;
                $mensaje =  'Estado de la actividad no valido';
                break;
            default:
                $estado = 0;
                $mensaje =  'No se pudo obtener el estado';
                break;
        }
        $envioMasivo["estado"] = $estado;
        $envioMasivo["mensaje"] = $mensaje;
        EnvioMasivoOfsc::create($envioMasivo);
        Auth::logout();
        $job->delete();
        return;
    }
    public function cancelarOrden($job, $actuacion)
    {
        Auth::loginUsingId($actuacion['authId']);
        $aid = $actuacion['aid'];
        $codactu = $actuacion['codactu'];
        $actividadTipoId = $actuacion['actividad_tipo_id'];
        $envioMasivo = [
            "envio_masivo_ofsc_grupo_id" => $actuacion['grupoEnvio'],
            "codactu" => $codactu,
            "mdf" => '',
            "tipo_actividad_id" => $actividadTipoId,
            "aid" => $aid,
            "tipo_masivo" => "MasivoCancelado",
            "empresa" => ''
        ];
        $inbound = new Inbound();
        $properties["XA_NOTE"] = "Cancelado por cambio de quiebre";
        $response = $inbound->cancelActivity(0, $codactu, $properties);
        $estado=0;
        $mensaje = "Error en el envio de cancel.";
        if (isset(
            $response->data->data->commands->command->appointment->report->message
        )
        ) {
            $result = $response->data->data->commands->command->appointment->report->message;
            if (isset($result->result) && $result->result=='error') {
                $mensaje=$result->description;
            } else {
                //dd($result);
                foreach ($result as $key => $value) {
                    $mensaje = $value->description;
                    if ($value->type=='cancel' && $value->result=='success') {
                        $estado = 1;
                        $mensaje = "Envio de Cancelacion a OFSC correcto.";
                        break;
                    }
                }
                $estado = 1;
                $mensaje = "Envio de Cancelacion a OFSC correcto.";
            }
            
        }
        $envioMasivo["estado"] = $estado;
        $envioMasivo["mensaje"] = $mensaje;
        EnvioMasivoOfsc::create($envioMasivo);
        Auth::logout();
        $job->delete();
        return;
    }
}