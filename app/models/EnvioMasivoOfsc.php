<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Ofsc\Inbound;
use Ofsc\Activity;
use Ofsc\Capacity;

/**
*   modelo para los estados que se registra en 'envios masivos'
*/
class EnvioMasivoOfsc extends \Eloquent
{
    public $table="envio_masivo_ofsc";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $guarded = array();

    public static function getCargar($rfiltros = array())
    {
        $select = ['e.codactu', 'e.aid', 'eg.nombre as grupo', 'e.mdf',
                    'a.nombre','e.mensaje','e.estado', 'e.envio_masivo_at'];

        $r = DB::table('envio_masivo_ofsc as e')
            ->join(
                'actividades_tipos as a',
                'e.tipo_actividad_id', '=', 'a.id'
            )->join(
                'envio_masivo_ofsc_grupo as eg',
                'envio_masivo_ofsc_grupo_id', '=', 'eg.id'
            );
        if (count($rfiltros) > 0) {
            if ($rfiltros['fechas']!=null) {
                if (is_array($rfiltros['fechas'])) {
                    $r = $r->whereBetween(
                        'e.created_at', $rfiltros['fechas']
                    );
                }
            }
            if ($rfiltros["estado"]!=null) {
                if (is_array($rfiltros['estado'])) {
                   $r = $r->where('e.estado', $rfiltros['estado']);
                }
            }
            if ($rfiltros["tipo"]!=null) {
                if (is_array($rfiltros['tipo'])) {
                   $r = $r->whereIn($rTipo, $rfiltros['tipo']);
                }
            }
        }
        $r = $r->select($select) ->get();
        return $r;
    }

    public static function envioOrdenOfsc($orden = null,
        $isMasivoBandeja = false, $index, $grupoEnvioId, $idUsuario = 0)
    {
        $resultado = array();
        $codactu = $orden["codactu"];
        $mdf = $orden["mdf"];
        $actividadTipoId = $orden["actividad_tipo_id"];
        $empresa = $orden["empresa"];
        $nombreGrupo = date("Y-m-d H:i:s");
        $dataGrupo = array();
        $hoy = date("Y-m-d");
        $hoyall = date("Y-m-d H:i:s");
        $actuArray = array();
        $resultado["correctos"] = 0;
        $resultado["incorrectos"] = 0;

        $error = $bucket = "";
        $insert =  [
            'envio_masivo_ofsc_grupo_id' => $grupoEnvioId,
            'codactu' => $codactu,
            'tipo_actividad_id' => $actividadTipoId,
            'empresa' => $empresa,
            'aid' => '',
            'mdf' => $mdf,
            "created_at" => date("Y-m-d H:i:s"),
            "usuario_created_at" => \Config::get("wpsi.ofsc.user_logs.actividades_temporales") 
        ];

        $tipoEnvioMasivo = "MasivoArtisan";
        if ($isMasivoBandeja == true) {
            $tipoEnvioMasivo = "MasivoBandeja";
        }
        $insert["tipo_masivo"] = $tipoEnvioMasivo;

        $actividadTipo= ActividadTipo::find($actividadTipoId);
        if ( isset($actividadTipo->label) && isset($actividadTipo->duracion) ) {

            $capacity = new Capacity();
            $data=[
                'fecha'          => [$hoy],
                'time_slot'      => '',
                'work_skill'     => $actividadTipo->label,
                'mdf'            => $mdf,
            ];
            $response = $capacity->getCapacity($data);
            //validar si la consulta de la capacidad no tiene errores
            if (isset($response->error) && $response->error===false ) {
                if(isset($response->data->capacity))
                    $bucket = $response->data->capacity[0]->location;
            } else {
                $error = "No se encontro bucket";
            }
        } else {
            $error = "No se encontro tipo de actividad";
        }
        $insert["tipo_actividad_id"] = $actividadTipoId;
        $insert["mensaje"] = $error;
        $insert["estado"] = 0;
        $insert["bucket"] = $bucket;

        if ($bucket == '') {
            $emo = EnvioMasivoOfsc::create(
                $insert
            );
            $insert["id_ultimo_masivo"] = $emo->id;
            EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
            return $resultado;
        }
        
        $datos = [];
        $rand = rand(0, 1);
        $arrayslot = ['AM', 'PM'];
        $arrayhora = [['9', '13'], ['13', '18']];
        $datos['fecha'] = $hoy; //,strtotime('+1 day')
        $datos['bucket'] = $bucket; //BK_TDPROUTE
        $datos['slot'] = $arrayslot[$rand]; //time slot
        $datos['horas'] = $arrayhora[$rand];
        $datos['codactu'] = $codactu;
        $fono1                  ='';
        $fono2                  ='';
        $fono3                  ='';
        $fono4                  ='';
        $fono5                  ='';
        $datos['slaini'] = date('Y-m-d H:i');

        $sla = true;
        $slaInicio = "0000-00-00 00:00";

        //retorno de transaccion
        $save = array(
            "rst" => 2,
            "msj" => "Bucket:" . $bucket . " | Sin reultados.",
            "error" => "",
            "gestion_id" => 0
        );

        $inbound = new Inbound();

        $gestionId = "";
        $codactu = $datos['codactu']; //"35268199"; //
        $date = $datos['fecha']; //date("Y-m-d"); //
        $bucket = $datos['bucket']; //"BK_PRUEBAS_TOA";
        $tini = trim($datos['horas'][0]); //"09:00"
        $tend = trim($datos['horas'][1]); //"13:00"
        $tobooking = date("Y-m-d H:i");
        $slot = $datos['slot']; //"AM"
        $cuadrante = "Something";
        $techTp = "NULL";
        $techBb = "NULL";
        $techTv = "NULL";
        $wzKey = "";
        //$wtLabel = "";
        //$wType = "";
        $accTech = "COAXIAL";
        $businessType = "";
        $phone = array(0=>"", 1=>"",2=>"",3=>"",4=>"");
        $estadoComponente = 0;
        //Datos de la actuacion
        $actu = $orden;
        //Existe la actuacion
        $codigoactuacion = $codactu;
        if ( isset($codigoactuacion)) {
            $input = [  "buscar" => $orden["codactu"],
                        "tipo" => "gd.averia",
                        "bandeja" => 1,
                        "isSoloActuaciones" => '0',
                     ];
            Input::replace($input);

            $orden = Gestion::getCargar($orden["codactu"]);
            if (isset($orden["datos"][0])) {
                $orden = $orden["datos"][0];
            } else {
                $insert["estado"] = 0;
                $insert["mensaje"] = "No existe data de la actividad";
                $emo = EnvioMasivoOfsc::create($insert);
                $insert["id_ultimo_masivo"] = $emo->id;
                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
                return $resultado;
            }
            $actuObj = $orden;
            $actuArray = json_decode(json_encode($orden), true);

            $actuArray['noajax'] = "ok";
            //Motivo/Submotivo
            $actuArray['gestion_id'] = $actuObj->id;
            $actuArray['motivo'] = 1;
            $actuArray['submotivo'] = 1;
            $actuArray['estado'] = 2;

            //GestionID
            $gestionId = $actuObj->id;

            if (trim($actuArray['actividad_tipo_id']) == '') {
                $insert["mensaje"] = "No cuenta con actividad_tipo_id";
                $insert["mdf"] = $orden->mdf;
                $insert["tipo_actividad_id"] =$orden->actividad_tipo_id;
                $insert["bucket"] = $bucket;
                $insert["estado"] = 0;
                //Registro de envío masivo ERROR
                $emo =  EnvioMasivoOfsc::create($insert);
                $insert["id_ultimo_masivo"] = $emo->id;
        
                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
                return $resultado;
            }
            //Quiebre grupo
            $actuArray['quiebre_grupo_id'] = $orden->quiebre_grupo_id;
            $quiebreGrupo = DB::table('quiebre_grupos')
                    ->select('nombre')
                    ->where('id', '=', $actuArray['quiebre_grupo_id'])
                    ->first();

            //Negocio por texto
            if (strpos($actuArray['tipo_averia'], "catv") > 0) {
                $businessType = "CATV";
                $preDiagnosis='TV_';
            }

            if (strpos($actuArray['tipo_averia'], "adsl") > 0) {
                $businessType = "ADSL";
                $preDiagnosis='SP_';
            }

            if (strpos($actuArray['tipo_averia'], "bas") > 0) {
                $businessType = "BASICA";
                $preDiagnosis='SB_';
            }

            //Telefonos de contacto
            //$phone = array("0000000", "0000000"); // Inicializa Telefono
            //Telefonos de contacto
            $phoneArray = explode("|", $actuArray['fonos_contacto']);
            if (isset($phoneArray[0]) AND trim($phoneArray[0]) != '') {
                $phone[0] = trim($phoneArray[0]);
            } elseif (isset($phoneArray[0]) AND isset($phoneArray[1]) AND
                    trim($phoneArray[1]) != '') {
                $phone[0] = trim($phoneArray[1]);
            }

            if (isset($phoneArray[1]) AND trim($phoneArray[1]) != '') {
                $phone[1] = trim($phoneArray[1]);
            }
            //Datos XY Web
            $actuArray['x'] = $actuArray['coord_x'];
            $actuArray['y'] = $actuArray['coord_y'];

            //Facilidades técnicas
            $ffttController = new \FfttController();
            $val = new stdClass();
            $val->fftt = $actuArray['fftt'];
            $val->tipoactu = $actuArray['tipo_averia'];
            $ffttArray = $ffttController->getExplodefftt($val);

            if ($ffttArray["tipo"] == 'catv') {
                $wzKey = $ffttArray["nodo"] . "_" . $ffttArray["troba"];
            } else {
                $wzKey = $ffttArray["mdf"] . "_" . $ffttArray["armario"];
            }

            //Parche: datos no concuerdan -> enumeration
            if (strtoupper($actuArray['segmento']) == "NO-VIP" OR
                    trim($actuArray['segmento']) == '') {
                $actuArray['segmento'] = "N";
            } elseif (strtoupper($actuArray['segmento']) == 'VIP') {
                if ($businessType == "CATV") {
                    $actuArray['segmento'] = "S";
                } elseif ($businessType == "BASICA") {
                    $actuArray['segmento'] = "D";
                }
            }
            //Componentes
            $dataComp = array();
            $equipment = "";
            $compGestion = new ComponenteGestion();
            //Componentes agrupados
            $dataComp = $compGestion->getComponenteFaltanteGrupal($codactu);
            if (isset($dataComp["data"]) and count($dataComp["data"]) > 0) {
                $equipment = $compGestion->getArrayformat($dataComp["data"]);
                $estadoComponente = 1;
            }
            $agendasWebUnificada = InboundHelper::getElementXaWebUnificada(
                $actuArray['wu_nagendas'], $actuArray['wu_nmovimientos'], 
                $actuArray['wu_fecha_ult_agenda']
            );
            //Homologacion de campos
            $cuadrante=GeoValidacion::getquadrant($actuArray['coord_x'], $actuArray['coord_y']);
            
            $ofscCliente= InboundHelper::getCliente(
                $datos['bucket'], strtolower($actuObj->actividad), $date,
                "update_activity", $bucket, $tini, $tend, $actuArray['codactu'],
                $actuArray['inscripcion'], $actividadTipo->label, $slot, 
                $tobooking, $actividadTipo->duracion, 
                $actuArray['nombre_cliente'], $phone[0], "", $phone[1],
                $actuArray['direccion_instalacion'], "", "", "", "1", 
                (int) $actividadTipo->duracion, "19", $actuArray['coord_x'],
                $actuArray['coord_y']
            );

            $ofscActivity= InboundHelper::getActividad(
                $actuArray['fecha_registro'], "PSI", $actuArray['segmento'],
                "", $actuArray['nombre_cliente'], "", $phone[2], $phone[3],
                $phone[4], "", $actuArray['codigo_distrito'], 
                $actuArray['distrito'], $actuArray['zonal'], $cuadrante, "",
                $wzKey, "", "", $actividadTipo->nombre, "TEL", $idUsuario, 
                $actuArray['codactu'], $actuArray['orden_trabajo'], "", "",
                "", "", "", $agendasWebUnificada, $actuArray['area'], 
                $actuArray['paquete'], $actuArray['empresa'], 
                $quiebreGrupo->nombre, $actuArray['quiebre'], $businessType, "",
                "", $equipment, $actuArray['observacion'], $techTp, $techBb,
                $techTv, $accTech, "0", "", "",
                $actuArray['clase_servicio_catv'], $actuArray['mdf'], "", "", "",
                $actuArray['dir_terminal'], "", $actuArray['tipo_actuacion'],
                "NULL", $actuArray['veloc_adsl'], $actuArray['tipo_servicio'],
                "", $actuArray['sms1'], "", $actuArray['total_averias'], 1
            );

            $dataOfsc = array_merge($ofscCliente, $ofscActivity);

            $dataOfsc['XA_TOTAL_REPAIRS'] = InboundHelper::getElementXaTotalRepairs(
                $actuArray['total_averias_cable'], $actuArray['total_averias_cobre'], 
                $actuArray['total_averias']
            );
            //FAcilidades TEcnicas
            $dataOfsc = InboundHelper::getFFTTXML($ffttArray, $actuArray, $dataOfsc, $codactu);
           
            //Envio por SLA
            if ($datos['slaini'] != '') {
                $sla = true;
                $slaInicio = strtotime($datos['slaini']);

                $slaDay = $actividadTipo->sla * 3600;

                $slaFin = $slaInicio + $slaDay;

                $dataOfsc["sla_window_start"] = date("Y-m-d H:i", $slaInicio);
                $dataOfsc["sla_window_end"] = date("Y-m-d H:i", $slaFin);

                $dataOfsc["date"] = date("Y-m-d", $slaInicio);
            }
            //Campos adicionales para averia
            $dataOfsc["XA_DIAGNOSIS"]  = InboundHelper::getXaDiagnosisXML(
                $actuArray, $actuObj, $preDiagnosis
            );
            //tipo req motivo req
            $diagnosis = explode('|', $actuArray['codmotivo_req_catv']);
            if (isset($diagnosis[0])) {
                $dataOfsc["XA_REQUIREMENT_TYPE"] = $diagnosis[0];
            }
            if (isset($diagnosis[1])) {
                $dataOfsc["XA_REQUIREMENT_REASON"] = $diagnosis[1];
            }

            /**
             * Guardar en tablas de gestion
             * - gestiones
             * - gestiones_detalles
             * - gestiones_movimientos
             * - ultimos_movimientos
             */
            $actuArray['fecha_agenda'] = $date;
            $horario = Horario::where('horario', '=', $slot)->first();

            if ($horario != NULL AND $horario != '') {
                $actuArray['horario_id'] = $horario->id;
                $actuArray['dia_id'] = date("N", strtotime($date));
            }

            $actuArray['estado_agendamiento'] = "1-1";

            unset($actuArray["tecnico"]);
            unset($actuArray["tecnico_id"]);
            unset($actuArray["celula_id"]);

            if ($datos['slaini'] != '') {
                $actuArray['horario_id'] = 49;
            }

            $resultado["dataofsc"] = $dataOfsc;
            
            //Crear actividad en OFSC
            if ($dataOfsc['date'] == 0) {
                $dataOfsc['date'] = date("Y-m-d", strtotime(Input::get('slaini')));
            }

            $response = $inbound->createActivity($dataOfsc, $sla);
            $resultado["response"] = $response;
            $report = $response->data
                    ->data
                    ->commands
                    ->command
                    ->appointment
                    ->report;

            $resultBool = true;

            /**
             * $report->message:
             *
             * El mensaje de respuesta puede ser un arreglo
             * o un único mensaje.
             * Se valida la respuesta para cada mensaje recibido.
             */
            if (is_array($report->message)) {
                foreach ($report->message as $val) {
                    if ($val->result == 'warning') {
                        $save["error"][] = $val->description;
                    }
                    if ($val->result == 'error') {
                        $save["error"][] = $val->description;
                    }
                }
            } else {
                if ($report->message->result == 'error') {
                    $resultBool = false;
                    $save["error"][] = $report->message->description;
                }
            }

            //Retorno OK
            if ($resultBool) {
                //Appointment id
                $aid = $response->data
                        ->data
                        ->commands
                        ->command
                        ->appointment
                        ->aid;
                
                unset($actuArray["fecha_agenda"]);
                unset($actuArray["horario_id"]);
                unset($actuArray["dia_id"]);
                $actuArray['estado_agendamiento'] = "3-0";
                $actuArray['motivo'] = 2;
                $actuArray['submotivo'] = 18;
                $actuArray['estado'] = 7;
                $actuArray['aid'] = $aid;
                $actuArray['estado_ofsc_id'] = 1;

                $actuArray['envio_ofsc']=2;
                $actuArray['programado']=1;
                $actuArray['componente']=$estadoComponente;
                if (Input::has('desprogramar') AND 
                    Input::get('desprogramar')=='ok') {
                    $actuArray['programado']=0;
                }
                if ($sla==false) {
                    $actuArray['envio_ofsc']=1;
                }
                Input::replace($actuArray);
            
                $errorController = new ErrorController();
                $objGestionMovimiento = new GestionMovimientoController($errorController);

//dd($actuArray['celular_cliente_critico']);
                $save = $objGestionMovimiento->postRegistrar($actuArray);
                $save["msj"] = "Bucket:" . $bucket . " | Registro y envio a OFSC correcto.";
                $save["aid"] = $aid;

                //Registro de envío masivo OK
                $insert["accion"] = "inbound_interface";
                $insert["aid"] = $aid;
                $insert["estado"] = 1;
                $insert["mensaje"] = $save["msj"];
                $emo = EnvioMasivoOfsc::create($insert);
                unset($insert["accion"]);
                $insert["id_ultimo_masivo"] = $emo->id;
                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
                unset($insert["id_ultimo_masivo"]);
                return $resultado;
            }
        } else {
            //Registro de envío masivo ERROR
            $insert["mensaje"] = $save["msj"];
            $insert["estado"] = 0;
            $insert["bucket"] = $bucket;
            $emo = EnvioMasivoOfsc::create($insert);
            $insert["id_ultimo_masivo"] = $emo->id;
            EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
            return $resultado;
        }
        return $resultado;
    }

    public function numIntentosMasivo ($codactu = 0, $accion = "") 
    {
        $numIntentos = 1;
        if ($codactu > 0) {
            $ultimoEnvio = DB::table("envio_masivo_ofsc")
                ->select("numintentos")
                ->where("codactu", "=", $codactu)
                ->where("accion", "=", $accion)
                ->orderBy("id", "desc")
                ->get();

            if (count($ultimoEnvio) > 0) {
                $numIntentos = $ultimoEnvio[0]->numintentos + 1;
            }
        }
        return $numIntentos;
    }
    public static function envioLiquidadoOfsc($aid, $codactu, $mdf,
        $actividadTipoId, $empresa, $idMasivoOfscGrupo)
    {
        $activity = new Activity();
        $inbound = new Inbound();
        //dd($aid);
        $response = $activity->getActivity($aid);
        if (isset($response->data)){
            if (isset($response->data["status"]) && $response->data["status"] != '') {
                $estadoofscDos = DB::table('estados_ofsc AS e')
                    ->where('e.name', '=', $response->data["status"])
                    ->first();
                $estadoofscDos = is_null($estadoofscDos)?
                    ['id' => 0]  : $estadoofscDos;
            } else {
                $estadoofscDos = array("id" => 0);
            }
        } else {
             $estadoofscDos = array("id" => 0);
        }
        $arrayOfsc = $estadoofscDos;

        $insert = ["envio_masivo_ofsc_grupo_id" => $idMasivoOfscGrupo, 
        "codactu" => $codactu, "mdf" => $mdf, "tipo_actividad_id" => $actividadTipoId,
        "aid" => $aid, "tipo_masivo" => "MasivoLiquidado", "empresa" => $empresa, 
        "created_at" => date("Y-m-d H:i:s"), 
         "usuario_created_at" => \Config::get("wpsi.ofsc.user_logs.liquidado_masivo") ];

        if (count($estadoofscDos) == 1 && isset($arrayOfsc->name)) {
            if ($arrayOfsc->name == "pending") {
                $actividad=array();
                $actividad['XA_NOTE']='Se liquido en Legados';
                $response = $inbound->cancelActivity($aid, $codactu, $actividad);

                if (isset(
                    $response->data->data->commands->command->appointment
                             ->report->message
                )) {

                    $resultCode = $response->data->data->commands
                                    ->command->appointment->report
                                    ->message;

                    if (isset($resultCode->result) 
                        && $resultCode->result=='error') {
                        $insert["estado"] = 0;
                        $insert["mensaje"] =  'No se pudo cancelar la actividad';

                        $emo = EnvioMasivoOfsc::create($insert );
                        $insert["id_ultimo_masivo"] = $emo->id;
                        EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);

                        /*DB::table('envio_masivo_ofsc')
                            ->insert(
                                [
                                    'envio_masivo_ofsc_grupo_id' => 
                                    $idMasivoOfscGrupo,
                                    'bucket' => '',
                                    'codactu' => $codactu,
                                    'mdf' => $mdf,
                                    'tipo_actividad_id' => 
                                    $actividadTipoId,
                                    'aid' => $aid,
                                    'mensaje' => 
                                    'No se pudo cancelar la actividad',
                                    'envio_masivo_at' => '',
                                    'tipo_masivo' => 'MasivoLiquidado',
                                    "empresa" => $empresa,
                                    'estado' => 0,
                                    'created_at' => date("Y-m-d H:i:s")
                                ]
                            ); */
                    } else {
                        for ($y = 0; $y < count($resultCode); $y++) {
                            if ($resultCode[$y]->type=='cancel' 
                                && $resultCode[$y]->result=='success' ) {

                                DB::table('ultimos_movimientos')
                                    ->where('aid', $aid)
                                    ->where('codactu', $codactu)
                                    ->update(['liquidado_ofsc' => 1]);

                                $insert["estado"] = 1;
                                $nsert["mensaje"] = 'Envio de cancelacion exitosa';

                                $emo = EnvioMasivoOfsc::create($insert );
                                $insert["id_ultimo_masivo"] = $emo->id;
                                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);

                               /* DB::table('envio_masivo_ofsc')
                                    ->insert(
                                        [
                                            'envio_masivo_ofsc_grupo_id' => 
                                            $idMasivoOfscGrupo,
                                            'bucket' => '',
                                            'codactu' => $codactu,
                                            'mdf' => $mdf,
                                            'tipo_actividad_id' => 
                                            $actividadTipoId,
                                            'aid' => $aid,
                                            'mensaje' => 
                                            'Envio de cancelacion exitosa',
                                            'envio_masivo_at' => '',
                                            'tipo_masivo' => 'MasivoLiquidado',
                                            'estado' => 1,
                                            'created_at' => date("Y-m-d H:i:s"),
                                        ]
                                    );
                                    
                                DB::table('envio_masivo_ofsc')->insert(
                                    [
                                        'envio_masivo_ofsc_grupo_id' =>
                                        $idMasivoOfscGrupo,
                                        'bucket' => '',
                                        'codactu' => $codactu,
                                        'mdf' => $mdf,
                                        'tipo_actividad_id' =>
                                        $actividadTipoId,
                                        'aid' => $aid,
                                        'mensaje' =>
                                        'Envio de cancelacion exitosa',
                                        'envio_masivo_at' => '',
                                        'tipo_masivo' => 'MasivoLiquidado',
                                        "empresa" => $empresa,
                                        'estado' => 1,
                                        'created_at' => date("Y-m-d H:i:s"),
                                    ]
                                );*/
                            }
                        }
                    }
                }
            } elseif ($arrayOfsc->name == "started" 
                or $arrayOfsc->name == "notdone") {
                $response = $activity->completeActivity($aid);
                $valResponse = $response;
                if (isset($valResponse->status)) {
                    if ($valResponse->status == "cancelled") {
                        DB::table('ultimos_movimientos')
                            ->where('aid', $aid)
                            ->where('codactu', $codactu)
                            ->update(['liquidado_ofsc' => 1]);

                            $insert["estado"] = 1;
                            $insert["mensaje"] =  'Envio de cancelacion exitosa';

                            $emo = EnvioMasivoOfsc::create($insert );
                                $insert["id_ultimo_masivo"] = $emo->id;
                                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);

                       /*DB::table('envio_masivo_ofsc')
                            ->insert(
                                [
                                    'envio_masivo_ofsc_grupo_id' => 
                                    $idMasivoOfscGrupo,
                                    'bucket' => '',
                                    'codactu' => $codactu,
                                    'mdf' => $mdf,
                                    'tipo_actividad_id' => 
                                    $actividadTipoId,
                                    'aid' => $aid,
                                    'mensaje' => 
                                    'Envio de cancelacion exitosa',
                                    'envio_masivo_at' => '',
                                    'tipo_masivo' => 'MasivoLiquidado',
                                        "empresa" => $empresa,
                                    'estado' => 1,
                                    'created_at' => date("Y-m-d H:i:s"),
                                ]
                            );*/
                    } else {
                        $insert["estado"] = 0;
                            $insert["mensaje"] =  'No se pudo cancelar la actividad';

                            $emo = EnvioMasivoOfsc::create($insert );
                                $insert["id_ultimo_masivo"] = $emo->id;
                                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);

                        /*DB::table('envio_masivo_ofsc')->insert(
                            [
                            'envio_masivo_ofsc_grupo_id' => 
                            $idMasivoOfscGrupo,
                            'bucket' => '',
                            'codactu' => $codactu,
                            'mdf' => $mdf,
                            'tipo_actividad_id' => 
                            $actividadTipoId,
                            'aid' => $aid,
                            'mensaje' => 
                            'No se pudo cancelar la actividad',
                            'envio_masivo_at' => '',
                            'tipo_masivo' => 'MasivoLiquidado',
                                "empresa" => $empresa,
                            'estado' => 0,
                            'created_at' => date("Y-m-d H:i:s"),
                            ]
                        );*/
                    }
                } else {
                    $insert["estado"] = 0;
                            $insert["mensaje"] =  'No se pudo obtener el status del completeActivity';

                            $emo = EnvioMasivoOfsc::create($insert );
                                $insert["id_ultimo_masivo"] = $emo->id;
                                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);

                  /*  DB::table('envio_masivo_ofsc')->insert(
                            [
                            'envio_masivo_ofsc_grupo_id' => 
                            $idMasivoOfscGrupo,
                            'bucket' => '',
                            'codactu' => $codactu,
                            'mdf' => $mdf,
                            'tipo_actividad_id' => 
                            $actividadTipoId,
                            'aid' => $aid,
                            'mensaje' => 
                            'No se pudo obtener el status del completeActivity',
                            'envio_masivo_at' => '',
                            'tipo_masivo' => 'MasivoLiquidado',
                                "empresa" => $empresa,
                            'estado' => 0,
                            'created_at' => date("Y-m-d H:i:s"),
                            ]
                        ); */
                }
                
            } elseif ($arrayOfsc->name == "complete") {
                 DB::table('ultimos_movimientos')
                    ->where('aid', $aid)
                    ->where('codactu', $codactu)
                    ->update(['liquidado_ofsc' => 1]);

                $insert["estado"] = 1;
                $insert["mensaje"] =  'cancelado en TOA';

                $emo = EnvioMasivoOfsc::create($insert);
                $insert["id_ultimo_masivo"] = $emo->id;
                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
                
               /* DB::table('envio_masivo_ofsc')->insert(
                    [
                    'envio_masivo_ofsc_grupo_id' =>$idMasivoOfscGrupo,
                    'bucket' => '',
                    'codactu' => $codactu,
                    'mdf' => $mdf,
                    'tipo_actividad_id' =>$actividadTipoId,
                    'aid' => $aid,
                    'mensaje' => 
                    'Estado de la actividad no valido',
                    'envio_masivo_at' => '',
                    'tipo_masivo' => 'MasivoLiquidado',
                    "empresa" => $empresa,
                    'estado' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                    ]
                );*/
            } else {
                $insert["estado"] = 0;
                $insert["mensaje"] =  'Estado de la actividad no valido';

                $emo = EnvioMasivoOfsc::create($insert );
                $insert["id_ultimo_masivo"] = $emo->id;
                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);
            }
        } else {
             $insert["estado"] = 0;
                            $insert["mensaje"] =  'No se pudo obtener el estado';

                            $emo = EnvioMasivoOfsc::create($insert );
                                $insert["id_ultimo_masivo"] = $emo->id;
                                EnvioMasivoOfscUltimo::setUltimoEnvioMasivo($insert);


            /*DB::table('envio_masivo_ofsc')->insert(
                [
                'envio_masivo_ofsc_grupo_id' => 
                $idMasivoOfscGrupo,
                'bucket' => '',
                'codactu' => $codactu,
                'mdf' => $mdf,
                'tipo_actividad_id' => 
                $actividadTipoId,
                'aid' => $aid,
                'mensaje' => 
                'No se pudo obtener el estado',
                'envio_masivo_at' => '',
                'tipo_masivo' => 'MasivoLiquidado',
                "empresa" => $empresa,
                'estado' => 0,
                'created_at' => date("Y-m-d H:i:s"),
                ]
            );*/
        }
    }
}