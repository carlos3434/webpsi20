<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
*   modelo para los estados que se registra en 'proyectos edificios'
*/
class GestionEdificio extends \Eloquent
{
    public $table="gestiones_edificios";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    
    protected $guarded = array();

    public function proyectoEdificio()
    {
        return $this->belongsTo('ProyectoEdificio');
    }

    public function proyectoTipo()
    {
        return $this->belongsTo('ProyectoTipo');
    }

    public function casuistica()
    {
        return $this->belongsTo('Casuistica');
    }
    public function proyectoEstado()
    {
        return $this->belongsTo('ProyectoEstado');
    }
}
