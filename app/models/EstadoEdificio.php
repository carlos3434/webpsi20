<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
*   modelo para los estados que se registra en 'proyectos edificios'
*/
class EstadoEdificio extends \Eloquent
{
    public $table="estado_edificios";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    

    /**
     * ProyectoEdificio relationship
     */
    public function proyectoEdificio()
    {
        return $this->hasMany('ProyectoEdificio');
    }
}
