<?php

class TempConformidadAveria extends \Eloquent {
	use DataViewer;
	public $table = 'temp_conformidad_averia';

	protected $fillable = 
    [   'id','temporal_conformidad_id', 'codactu', 'estado_legado',
        'fecha_registro_legado', 'fecha_liquidacion', 'cod_liquidacion',
        'detalle_liquidacion',
        'codcli',
        'nombre_cliente',
        'area',
        'contrata',
        'zonal',
        'proceso',
        'mdf',
        'observacion'
    ];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

}