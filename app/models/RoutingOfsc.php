<?php

class RoutingOfsc
{	
	private $radioTierra;
	private $numAgrupar;

	public function __construct()
	{
		$this->radioTierra = 6372795.477598;
		$this->numAgrupar = 3;
	}
	public function numMaximoAgrupados($total = 0, $divisor = 1)
	{
	 	$response = [];
	 	$response["numMaximo"] = (int)($total / $divisor);
	 	$response["resto"] = $total - ($divisor*$response["numMaximo"]);
	}

	public function geoDistancia($puntoInicio= [], $puntoFin=[])
	{
		 $longitudA = $puntoInicio["longitud"];
		 $longitudB = $puntoFin["longitud"];
		 $latitudA = $puntoInicio["latitud"];
		 $latitudB = $puntoFin["latitud"];

		 $distancia = (number_format(6371 * ACOS(COS(deg2rad($latitudA)) * 
                     COS(deg2rad( $latitudB )) * 
                     COS(deg2rad( $longitudB )  - deg2rad($longitudA)) + 
                     SIN(deg2rad($latitudA)) * SIN(deg2rad( $latitudB) )), 2, '.', ''))
                     *1000 ;

		 return abs($distancia);
	}
	public function obtenerDistancia ( $puntoInicio = [], $puntoFin = [])
	{
		$lat1= $puntoInicio["latitud"];
		$lat2 = $puntoFin["latitud"];
		$long1 = $puntoInicio["longitud"];
		$long2 = $puntoFin["longitud"];
		$km = 111.302;
		$degtorad = 0.01745329;//1 Grado = 0.01745329 Radianes
		$radtodeg = 57.29577951;//1 Radian = 57.29577951 Grados
		$dlong = ($long1 - $long2); 
		$dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
		$dd = acos($dvalue) * $radtodeg; 
		if (is_nan($dd))
			return 0;
		else
		 	return abs($dd * $km);
	}

	public function obtenerRutaTrayecto ($posiciones = [], $numTotal = 0)
	{
		$rutaTrayecto = [];
		$z = 0;
		for($i = 0; $i < $numTotal; $i++)
		{
			$trayecto = [];
			$trayecto[0] = $i;
				
			for($j = $i+1; $j < $numTotal; $j++)
			{
				$trayecto[1] = $j;
				for($k = $j+1; $k < $numTotal; $k++)
				{
					$trayecto[2] = $k;
					$z++;
					$rutaTrayecto[$z] = $trayecto;
				}
			}
		}
		return $rutaTrayecto;
	}

	public function obtenerDistanciaTrayecto ($posiciones =[], $rutaTrayecto = []){
		$distanciaTrayecto = [];
		foreach ($rutaTrayecto as $key => $value) {
			$i = $value[0];
			$j = $value[1];
			$k = $value[2];

			$puntoA = $posiciones[$i];
			$puntoB = $posiciones[$j];
			$puntoC = $posiciones[$k];

			$distanciaABC = FuncionesMatematicas::geoDistanciaKM($puntoA, $puntoB) 
			+ FuncionesMatematicas::geoDistanciaKM($puntoB, $puntoC);

			$distanciaBCA = FuncionesMatematicas::geoDistanciaKM($puntoB, $puntoC) 
			+ FuncionesMatematicas::geoDistanciaKM($puntoC, $puntoA);

			$distanciaCAB = FuncionesMatematicas::geoDistanciaKM($puntoC, $puntoA) 
			+ FuncionesMatematicas::geoDistanciaKM($puntoA, $puntoB);

			$opt = [0 => $i."-".$j."-".$k,
					1 => $j."-".$k."-".$i,
					2 => $k."-".$i."-".$j];

			$distanciaTmp[0] = $distanciaABC;
			$distanciaTmp[1] = $distanciaBCA;
			$distanciaTmp[2] = $distanciaCAB;
					
			asort($distanciaTmp);
			reset($distanciaTmp);

			$keyTmp = key($distanciaTmp);
			$distanciaTrayecto[$opt[$keyTmp]] = $distanciaTmp[$keyTmp]; 
		}
		asort($distanciaTrayecto);
		$distanciaResultado = [];
		foreach ($distanciaTrayecto as $key => $value) {
			$indices = explode("-", $key);
			$distanciaResultado[] = ["ruta" => $key, "distancia" => $value,
			"rutadistancia" => $posiciones[$indices[0]]["codactu"]."-".$posiciones[$indices[1]]["codactu"]."-".$posiciones[$indices[2]]["codactu"]];
		}
		return $distanciaResultado;
	}
	public function obtenerRutaUnica ($distanciaTrayecto = [])
	{
		$rutasUnicas = [];
		$numTotal = count($distanciaTrayecto);

		for ($i = 0; $i < $numTotal; $i++)
		{
			if (isset($distanciaTrayecto[$i])){
				$ruta1 = explode("-",$distanciaTrayecto[$i]["ruta"]);
			
				if (count($ruta1) > 0)
				{
					for ($j = $i+1; $j < $numTotal; $j++)
					{
						if (isset($distanciaTrayecto[$j]))
						{
							$ruta2 = explode("-", $distanciaTrayecto[$j]["ruta"]);
							foreach ($ruta2 as $key => $value)
							{
								if (in_array($value, $ruta1))
								{
									unset($distanciaTrayecto[$j]);
								}
							}
						}
					}
				}
			}
		}

		return $distanciaTrayecto;
	}
	public function posiblesRutas($posiciones = [])
	{
		$rutas = [];
		$z = 0;

		if (count($posiciones) > 0)
		{
			$numTotal = count($posiciones);
			$rutaTrayecto = $this->obtenerRutaTrayecto ($posiciones, $numTotal);
			$distanciaTrayecto = $this->obtenerDistanciaTrayecto ($posiciones, $rutaTrayecto);
			
			$rutas["trayecto"] = $rutaTrayecto;
			$rutas["distancias"] = $distanciaTrayecto;
			$rutas["rutasunicas"] = $this->obtenerRutaUnica($distanciaTrayecto);
		}
		return $rutas;
	}

}