<?php
class Error extends \Base
{
    public $table = "errores";

    public static $where =[
                        'id', 'code','file','line', 'message', 'comentario',
                        'trace','usuario_id', 'date', 'estado', 'url'
                        ];
    public static $selec =[
                        'id', 'code','file','line', 'message', 'comentario',
                        'trace','usuario_id', 'date', 'estado', 'url'
                        ];
    public static function get(array $data =array())
    {
        return parent::get($data);
    }
    public static function boot() {
        parent::boot();

        static::updating(function($table) {
            $table->usuario_updated_at = Auth::id() ? Auth::id() : 0;
        });

        static::saving(function($table) {
            $table->usuario_created_at = Auth::id() ? Auth::id() : 0;
        });
        /*static::deleting(function($table) {
            $table->deleted_by = Session::get('usuario');
        });*/
}
}
