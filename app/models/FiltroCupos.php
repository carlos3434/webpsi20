<?php

class FiltroCupos extends \Base
{

    public $table = 'filtro_cupo';
    
    public static $where =['id','id_bucket','id_tipo_persona','id_horario_tipo'];
    public static $selec =['id','id_bucket','id_tipo_persona','id_horario_tipo'];

    public static function getGuardar()
    {
            $name='usuario_created_at';
            $idbucket=Input::get('bucket','');
            $idpersona=Input::get('persona');
            $idhorario=Input::get('horario');

            if (Input::get('idfiltro') && Input::get('idfiltro')!=0){
                $filtrocupos=FiltroCupos::find(Input::get('idfiltro'));
            }
            else {
                $filtrocupos = new FiltroCupos();
                $name='usuario_updated_at';
                $filtrocupos['id_bucket'] = $idbucket;
                $filtrocupos['id_tipo_persona'] = $idpersona;
                $filtrocupos['id_horario_tipo'] = $idhorario;
            }

            $filtrocupos['estado'] = 1;
            $filtrocupos[$name] =  Auth::id();
            $result=$filtrocupos->save();
            
            if($result){
                $cupos=Input::all();
                    /*DB::table('filtrocupo_detalle as fd')
                    ->where('fd.id_filtro_cupo', $filtrocupos['attributes']['id'])
                    ->update(
                        array('fd.estado' => 0,
                        'fd.updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); */
                foreach ($cupos as $key => $value) {
                    if(substr($key, 0,4)=='cupo'){
                        $detalle=explode("_", $key);
                        $sql = "INSERT INTO filtrocupo_detalle (id_filtro_cupo,id_horario,id_dias,cupo,created_at)
                            values(".$filtrocupos['attributes']['id'].",".$detalle[1].",'".$detalle[2]."','".$value."',now())
                                ON DUPLICATE KEY UPDATE updated_at=now(),estado=1,cupo='".$value."' ";
                        DB::insert($sql);
                    }
                }                
            } 

            return  $result;
    }

    public static function getBuscar()
     {
            $idbucket=Input::get('bucket');
            $idpersona=Input::get('persona');//Session::get('tipoPersona');
            $idhorario=Input::get('horario');
            $cupos =  DB::table('filtro_cupo as f')
                ->leftjoin(
                    'filtrocupo_detalle as fd',
                    'f.id', '=', 'fd.id_filtro_cupo'
                    )
                ->select('f.id', 'id_horario','id_dias','cupo', 
                    DB::raw('concat(id_horario,id_dias) as keyarray'))
                ->where('f.id_bucket', $idbucket)
                ->where('id_tipo_persona', $idpersona)
                ->where('id_horario_tipo', $idhorario)
                ->where('fd.estado', 1)
                ->Orderby('id_dias')
                ->get();
                
            $cupopersona=DB::table('tipo_persona')
                ->select('id', 'porcent')
                ->where('id', $idpersona)
                ->first();

            $horario=DB::table('horarios')
                ->select('id', 'horario')
                ->where('horario_tipo_id', $idhorario)
                ->get();

            $dia=array(1,2,3,4,5,6,7); // de LUN - DOM
            $orden=array();
            $hoy=date("Y-m-d");
            $i=0;
            $capa=array();
            foreach ($horario as $key => $valhora) {
                foreach ($dia as $key_ => $valdia) {
                    $ix=$valhora->id.$valdia;
                    $orden[$ix]=0;
                }
                //$capa[$i] = json_decode(Redis::get($hoy.'|'.$valhora->horario.'|A_PROV_INS_M1|PENDMOV1|LL_R011'),true);
                //$i++;
                //$capa[$i] = json_decode(Redis::get($hoy.'|'.$valhora->horario.'|A_AVE_REP_M1|PENDMOV1|LL_R011'),true);
            }
            //capacidad
            /*$capacidad=0;
            foreach ($capa as $key => $value) {
                if($value['location']==$idbucket){
                    $capacidad= $capacidad+$value['quota'];
                }
            } */
            $capacidad=26;
            $capacidad=(int) ($capacidad*$cupopersona->porcent/100);
            $cupoorden=array();
            foreach ($orden as $key => $valorden) {
                foreach ($cupos as $key_ => $value) {
                    if($key==$value->keyarray)
                    {
                       $orden[$key]=$value->cupo; 
                    }
                }
            }
            $idfiltro=0;
            if(count($cupos)>0)
            $idfiltro=$cupos[0]->id;
        
            return Response::json(
                array(
                'rst' => 1,
                'idfiltro'=> $idfiltro,
                'datos' => $orden,
                'horario' => $horario,
                'capacidad' =>$capacidad,
                'dia'=>$dia
                )
            );
     }

     public static function getGuardarPlano()
    {
            $name='usuario_created_at';

            /*$totales =  DB::table('tmp_cupos')
                        ->select(DB::RAW('lunes,martes,miercoles,jueves,viernes,
                            sabado,domingo') 
                          )
                        ->get();*/
            $tmp_cupos =  DB::table('tmp_cupos')
                                ->select(
                                    DB::RAW('bucket,horario,lunes,martes,miercoles,jueves,viernes,sabado,domingo') 
                                  )
                                ->get();

            $validar=0;
            foreach ($tmp_cupos as $key => $value) {
                foreach ($value as $key => $valuedos) {
                  if($key!='bucket' && $key!='horario') {
                    if($valuedos>100) {
                        return Response::json(
                        array(
                        'rst' => 2,
                        'msj' => 'La capacidad por Rango Horario no debe 
                        pasar el 100%',
                        ));
                    }
                  }
                }
            }

            $idpersona=Input::get('persona');

            $dias=array(1=>'lunes',2=>'martes',3=>'miercoles',4=>'jueves',5=>'viernes',6=>'sabado',7=>'domingo');

            foreach ($tmp_cupos as $key => $value) {
                $horario=json_decode(Horario::get(array('horario'=>$value->horario)));
                if(count($horario)>0) {
                    $filtroid=json_decode(FiltroCupos::get(
                        array('id_bucket'=>$value->bucket, 
                              'id_tipo_persona'=>$idpersona,
                              'id_tipo_horario'=>$horario[0]->horario_tipo_id)));

                    if(count($filtroid)>0){
                        $filtrocupos=FiltroCupos::find($filtroid[0]->id);
                    } else {
                        $filtrocupos = new FiltroCupos();
                        $name='usuario_updated_at';
                        $filtrocupos['id_bucket'] = $value->bucket;
                        $filtrocupos['id_tipo_persona'] = $idpersona;
                        $filtrocupos['id_horario_tipo'] = $horario[0]->horario_tipo_id;
                    }

                    $filtrocupos['estado'] = 1;
                    $filtrocupos[$name] =  Auth::id();
                    $result=$filtrocupos->save();

                    if ($result) {
                          foreach ($dias as $key => $dia) {
                            
                              $sql = "INSERT INTO filtrocupo_detalle (id_filtro_cupo,id_horario,id_dias,cupo,created_at)
                                    values(".$filtrocupos->id.",".$horario[0]->id.",'".$key."','".$value->$dia."',now())
                                ON DUPLICATE KEY UPDATE updated_at=now(),estado=1,cupo='".$value->$dia."' ";
                                 DB::insert($sql);

                          }
                        
                    }

                }              
              
            }                    
            if($result) {
                return Response::json(
                        array(
                        'rst' => 1,
                        'msj' => 'Registro realizado correctamente',
                        ));
            }
            else {  Response::json(
                        array(
                        'rst' => 2,
                        'msj' => 'Ocurrio una interrupción en el proceso',
                        ));
            }
    }
}

