<?php 
class GeoclienteAnPunto extends Eloquent
{

    public static function postGeoclienteAnPuntoAll()
    {
        $r  =   DB::table('geo_clienteanpunto')
        ->select(
            'zonal as id',
            DB::raw('zonal as nombre')
        )
         ->groupBy('zonal')
          ->get();
        return $r;
    }

    public static function postTipoFiltro($array)
    {
        $r  =   DB::table('geo_clienteanpunto')
                  ->select(
                      'tipo as id',
                      DB::raw("tipo as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('tipo')
                  ->get();
        return $r;
    }

    public static function postOrigenFiltro($array)
    {
        $r  =   DB::table('geo_clienteanpunto')
                  ->select(
                      'origen as id',
                      DB::raw("origen as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("tipo", $array['tipo'])
                  ->groupBy('origen')
                  ->get();
        return $r;
    }

    public static function postClienteFiltro($array)
    {
        $r  =   DB::table('geo_clienteanpunto')
                  ->select(
                      'cliente as id',
                      DB::raw("cliente as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("tipo", $array['tipo'])
                  ->whereIn("origen", $array['origen'])
                  ->groupBy('cliente')
                  ->get();
        return $r;
    }
  
}
