<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * 
 */
class ProyectoEdificioBitacora extends \Eloquent
{

    use SoftDeletingTrait;

    //protected $dates = ['deleted_at'];
    //protected $guarded = [];
    private $_rules = array(
        'descripcion' => 'required|min:3',
        'fecha_observacion' => 'required|date',
        //'usuario_created_at' => 'required|integer',
        'proyecto_edificio_id' => 'required|integer',
    );
    private $_errors;

    public function __construct()
    {
        $this->dates = ['deleted_at'];
        $this->table = 'proyecto_edificios_bitacora';
        //$this->guarded = array();
    }

    public function validate($data)
    {
        $v = Validator::make($data, $this->_rules);
        // return $v->passes();
        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->_errors = $v->messages();
            return false;
        }
        // validation pass
        return true;
    }
    
    public function errors()
    {
        return $this->_errors;
    }

    /**
     * ProyectoEdificio relationship
     */
    public function proyectoEdificio()
    {
        return $this->hasMany('ProyectoEdificio');
    }

//    /**
//     * 
//     * @param array $attributes
//     * @return object
//     */
//    public static function create($attributes)
//    {
//        
//        return ProyectoEdificioBitacora::create($attributes);
//    }

    /**
     * Sino existe proyecto_id retorna listado completo
     * @param integer $proyectoId
     * @return object
     */
    public function listWithUser($proyectoId = 0)
    {
        // 'usuario',
        return ProyectoEdificioBitacora::select(
            DB::raw(
                '( select @rownum := @rownum + 1 from 
                ( select @rownum := 0 ) d2 ) AS fila'
            ),
            'fecha_observacion', 
            'descripcion', 
            DB::raw('CONCAT_WS(", ",apellido,nombre) AS usuario')
        )
        ->orderBy('proyecto_edificios_bitacora.created_at', 'DESC')
        ->join(
            'usuarios', 'usuarios.id', '=', 
            'proyecto_edificios_bitacora.usuario_created_at'
        )
        ->where(
            function ($query) use ($proyectoId) {
                if ($proyectoId) {
                    $query->where('proyecto_edificio_id', $proyectoId);
                }
            }
        )
        ->get();
    }

}
