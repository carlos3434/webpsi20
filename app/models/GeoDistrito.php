<?php 
class GeoDistrito extends Eloquent
{
    public static function postGeoDistritoAll()
    {
        $r  =   DB::table('geo_distrito')
        ->select(
            'departamento as id',
            DB::raw('departamento as nombre')
        )
         ->groupBy('departamento')
          ->get();
        return $r;
    }

    public static function postProvinciaFiltro($array)
    {
        $r  =   DB::table('geo_distrito')
                  ->select(
                      'provincia as id',
                      DB::raw("provincia as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->groupBy('provincia')
                  ->get();
        return $r;
    }
    
    public static function postDistritoFiltro($array)
    {
        $r  =   DB::table('geo_distrito')
                  ->select(
                      'distrito as id',
                      DB::raw("distrito as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->groupBy('distrito')
                  ->get();
        return $r;
    }
}
