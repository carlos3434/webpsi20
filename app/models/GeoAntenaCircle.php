<?php 
class GeoAntenaCircle extends Eloquent
{
    public static function postGeoAntenaCircleAll()
    {
        $r  =   DB::table('geo_antenacircle')
          ->select(
              'departamento as id',
              DB::raw('departamento as nombre')
          )
         /* ->where(
              function($query) use($array) {
                  $query->where('elemento_id', '=', $array['elementoid']);
              }
          )*/
          ->groupBy('departamento')
          ->get();
        return $r;
    }

    public static function postProvinciaFiltro($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'provincia as id',
                      DB::raw("provincia as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->groupBy('provincia')
                  ->get();
        return $r;
    }

    public static function postDistritoFiltro($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'distrito as id',
                      DB::raw("distrito as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->groupBy('distrito')
                  ->get();
        return $r;
    }

    public static function postTecnologiaFiltro($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'tecnologia as id',
                      DB::raw("tecnologia as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->whereIn("distrito", $array['distrito'])
                  ->groupBy('tecnologia')
                  ->get();
        return $r;
    }
    
    public static function postAntenaFiltro($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'estacion as id',
                      DB::raw("estacion as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->whereIn("distrito", $array['distrito'])
                  ->whereIn("tecnologia", $array['tecnologia'])
                  ->groupBy('estacion')
                  ->get();
        return $r;
    }

    public static function postAntenacircleCoord($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'estacion as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("banda "),
                      DB::raw("radio "),
                      DB::raw("concat(departamento,'-',provincia,'-',
                        distrito,'-',tecnologia,'-',estacion) as detalle")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->whereIn("distrito", $array['distrito'])
                  ->whereIn("tecnologia", $array['tecnologia'])
                  ->whereIn("estacion", $array['antenacircle'])
                  ->orderBy('estacion', 'asc')
                  ->get();
        return $r;
    }

     public static function postCoord($array)
    {
        $r  =   DB::table('geo_antenacircle')
                  ->select(
                      'estacion as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("banda "),
                      DB::raw("radio "),
                      DB::raw("concat(departamento,'-',provincia,'-',
                        distrito,'-',tecnologia,'-',estacion) as detalle")
                  )
                  ->where("departamento", '=', $array[0])
                  ->where("provincia", '=', $array[1])
                  ->where("distrito", '=', $array[2])
                  ->where("tecnologia", '=', $array[3])
                  ->where("estacion", '=', $array[4])
                  ->orderBy('estacion', 'asc')
                  ->get();
        return $r;
    }
}
