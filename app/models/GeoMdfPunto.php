<?php 
class GeoMdfPunto extends Eloquent
{
    public static function postGeoMdfPuntoAll()
    {
        $r  =   DB::table('geo_mdfpunto')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r;
    }

    public static function postMdfPuntoFiltro($array)
    {
        $r  =   DB::table('geo_mdfpunto')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
    }

     public static function postMdfPuntoCoord($array)
    {
        $r  =   DB::table('geo_mdfpunto')
                  ->select(
                      'mdf as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(zonal,'-',mdf) as detalle")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdfpunto'])
                  ->orderBy('mdf', 'asc')
                  ->get();
        return $r;
    }

     public static function postCoord($array)
    {
        $r  =   DB::table('geo_mdfpunto')
                  ->select(
                      'mdf as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(zonal,'-',mdf) as detalle")
                  )
                  ->where('zonal', '=', $array[0])
                  ->where('mdf', '=', $array[1])
                  ->orderBy('mdf', 'asc')
                  ->get();
        return $r;
    }



  
}
