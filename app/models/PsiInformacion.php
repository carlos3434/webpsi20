<?php

class PsiInformacion extends \Eloquent
{
    protected $table = "psi_informacion";

    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

}
