<?php 
class GeoAmplificador extends Eloquent
{


    public static function postZonalFiltro()
    {
        $r  =   DB::table('geo_amplificador')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
          ->groupBy('zonal')
          ->get();
        return $r;
    }

    public static function postNodoFiltro($array)
    {
        $r  =   DB::table('geo_amplificador')
                  ->select(
                      'nodo as id',
                      DB::raw("nodo as nombre ")
                     // DB::raw("CONCAT('Z',zonal) as relation")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('nodo')
                  ->get();
        return $r;
    }

    public static function postTrobaFiltro($array)
    {
        $r  =   DB::table('geo_amplificador')
                  ->select(
                      'troba as id',
                      DB::raw("troba as nombre ")
                     //DB::raw("CONCAT('Z',zonal,'|,|','N',nodo) as relation")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("nodo", $array['nodo'])
                  ->groupBy('troba')
                  ->get();
        return $r;
    }

    public static function postAmplificadorFiltro($array)
    {
        $r  =   DB::table('geo_amplificador')
                  ->select(
                      'amplificador as id',
                      DB::raw("amplificador as nombre ")
                      //DB::raw(
                          //"CONCAT('Z',zonal,'|,|','N',nodo,'|,|','T',troba) 
                        //as relation"
                      //)
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("nodo", $array['nodo'])
                  ->whereIn("troba", $array['troba'])
                  ->groupBy('amplificador')
                  ->get();
        return $r;
    }
    /*
    public static function postProvinciaCoord($array)
    {
        $r  =   DB::table('geo_provincia')
                  ->select(
                      'provincia as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(departamento,'-',provincia) as detalle")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->orderBy('provincia', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }

     public static function postCoord($array)
    {
        $r  =   DB::table('geo_provincia')
                  ->select(
                      'provincia as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng "),
                      DB::raw("concat(departamento,'-',provincia) as detalle")
                  )
                  ->where('departamento', '=', $array[0])
                  ->where('provincia', '=', $array[1])
                  ->orderBy('provincia', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }*/


  
}