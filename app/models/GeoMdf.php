<?php 
class GeoMdf extends Eloquent
{

    public static function getMdfzonal($zonal)
    {
        $r  =   DB::table('geo_mdf')
                  ->select(
                      'MDF as id',
                      DB::raw(
                          'MDF as nombre'
                      )
                  )
                  ->where(
                      function($query) use($zonal) {
                          $query->where('zonal', '=', $zonal);
                      }
                  )
                  ->groupBy('MDF')
                  ->orderBy('MDF', 'asc')
                  ->get();
        return $r;
    }

    public static function getCoordMdf($mdf)
    {
        $r  =   DB::table('geo_mdf')
                  ->select(
                      'MDF as id',
                      DB::raw("coord_y as lat "),
                      DB::raw("coord_x as lng ")
                  )
                  ->whereIn("mdf", $mdf)
                  ->orderBy('mdf', 'asc')
                  ->orderBy('orden', 'asc')
                  ->get();
        return $r;
    }

    public static function postGeoMdfAll()
    {
        $r  =   DB::table('geo_mdf')
        ->select(
            'zonal as id',
            DB::raw('zonal as nombre')
        )
         ->groupBy('zonal')
          ->get();
        return $r;
    }

    public static function postMdfFiltro($array)
    {
        $r  =   DB::table('geo_mdf')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
    }

  
}
