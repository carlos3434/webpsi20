<?php 

class GeocercaProyectoZonas extends Eloquent
{
    protected $guarded =[];
    protected $table = "geocerca_proyecto_zonas";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
            $table->usuario_updated_at = \Auth::id();
            }
        );
        static::creating(
            function ($table) {
            $table->usuario_created_at = \Auth::id();
            }
        );
    }

    public function tipo_geozona()
    {
        return $this->belongsTo('TipoGeozona');
    }

    public static function guardar($id, $mapa)
    {  
        $referencia='';
        foreach ($mapa as $index => $value) {
            if ($value) {
                if (!isset($value['tipo_zona'])) {
                  $referencia.=$value['detalle'].','; //MODIFICAR REFERENCIAS 
                } else {
                  $query = new GeocercaProyectoZonas();
                  $query['geocerca_proyecto_id']= $id;
                  $query['detalle']             = $value['detalle'];
                  $query['coordenadas']         = $value['coordenadas'];
                  $query['tipo_geozona_id']     = $value['tipo_zona'];
                  $query['referencia']          = $referencia;
                  $query['borde']               = $value['borde'];
                  $query['grosorlinea']         = $value['grosorlinea'];
                  $query['fondo']               = $value['fondo'];
                  $query['opacidad']            = $value['opacidad'];
                  $query['figura_id']           = $value['figura_id'];
                  $query['averia']              = $value['averia'];
                  $query['provision']           = $value['provision'];
                  if($value['actividad_tipo_id']!="") {
                    $query['actividad_tipo_id']=
                            implode(",", $value['actividad_tipo_id']);
                  }
                  if($value['quiebre_id']!="") {
                    $query['quiebre_id']=
                            implode(",", $value['quiebre_id']);
                  }
                  $query->save();

                  $referencia='';
                }
            }
        }   
    }

    public static function listar()
    {
        $id=Input::get('id');

        return GeocercaProyectoZonas::with('tipo_geozona')
                ->where('geocerca_proyecto_id', $id)
                ->where('estado', 1)
                ->get();
    }

    public static function actualizar()
    { 
        $array=array();
        $array=Input::all();
        $id=$array['id'];
        unset($array['id']);
        unset($array['token']);

        if(Input::get('actividad_id')){
          $a=Input::get('actividad_id');
          if(in_array("1", $a)) {
              $array['averia']=1;
          } else {
              $array['averia']=0;
          }
          if(in_array("2", $a)) {
              $array['provision']=1;
          } else {
              $array['provision']=0;
          }
        }
        unset($array['actividad_id']);

        if(Input::get('actividad_tipo_id')){
          $array['actividad_tipo_id']=
                  implode(",", Input::get('actividad_tipo_id'));
        }
        if(Input::get('quiebre_id')){
          $array['quiebre_id']=
                  implode(",", Input::get('quiebre_id'));
        }

        return GeocercaProyectoZonas::where('id', $id)
                    ->update($array); 
    }
}