<?php 
class ProyectoPreguntaRelacion extends Eloquent
{
    public $table = 'proyecto_pregunta_relacion';

    public static function getPreguntaAgrupada()
    {
        $result = DB::table('proyecto_pregunta AS pp')
                ->select(
                    DB::raw(
                        "ppg1.nombre AS 'grupo', ppg2.nombre AS 'seccion', "
                        . "ppg3.nombre AS 'seccion_hijo'"
                    ),
                    DB::raw(
                        'GROUP_CONCAT(
                            pp.pregunta ORDER BY ppr.orden ASC SEPARATOR "||"
                        ) AS preguntas'
                    ),
                    DB::raw(
                        'GROUP_CONCAT(
                            ppr.id ORDER BY ppr.orden ASC SEPARATOR "||"
                        ) AS ids'
                    ),
                    DB::raw(
                        'GROUP_CONCAT(
                            respuesta_tipo,estado_sustento
                            ORDER BY ppr.orden ASC SEPARATOR "||"
                        ) AS caracteristica'
                    ),  
                    //'pp.pregunta','ppr.id','estado_sustento','respuesta_tipo',
                    'ppr.pregunta_grupoparent_id AS igrupo', 
                    'ppr.pregunta_grupo_id AS iseccionpadre',
                    'ppr.pregunta_gruposection_id AS iseccionhijo'
                )
                ->join(
                    'proyecto_pregunta_relacion AS ppr', 'pp.id', 
                    '=', 
                    'ppr.pregunta_id'
                )
                ->leftJoin(
                    'proyecto_pregunta_grupo AS ppg3', function($join){
                        $join->on(
                            'ppr.pregunta_gruposection_id', '=', 'ppg3.id'
                        );
                        $join->where('ppg3.estado', '=', '1');
                    }
                )
                ->join(
                    'proyecto_pregunta_grupo AS ppg2', function($join){
                        $join->on('ppr.pregunta_grupo_id', '=', 'ppg2.id');
                        $join->where('ppg2.estado', '=', '1');
                    }
                )
                ->leftJoin(
                    'proyecto_pregunta_grupo AS ppg1', function($join){
                        $join->on(
                            'ppr.pregunta_grupoparent_id', '=', 'ppg1.id'
                        );
                        $join->where('ppg1.estado', '=', '1');
                        $join->where('ppg1.parent', '=', 0);
                    }
                )
                ->where('pp.estado', '=', '1')
                ->groupBy('ppr.pregunta_grupoparent_id')
                ->groupBy('ppr.pregunta_grupo_id')
                ->groupBy('ppr.pregunta_gruposection_id')
                //->orderBy('ppr.pregunta_grupoparent_id', 'asc')
                ->orderBy('ppg1.orden', 'ASC')
                ->get();
        return $result;      
//        $coleccion = \Illuminate\Support\Collection::make($result);
//        return $coleccion->keyBy('grupo');
    }
    
}