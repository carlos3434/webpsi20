<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
*   modelo para los tipos que se registra en 'proyectos edificios'
*/
class TipoProyecto extends \Eloquent
{
    public $table="tipo_proyectos";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    
    /**
     * ProyectoEdificio relationship
     */
    public function proyectoEdificio()
    {
        return $this->hasMany('ProyectoEdificio');
    }
}
