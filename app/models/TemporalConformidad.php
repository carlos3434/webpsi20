<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;


class TemporalConformidad extends Eloquent
{

    public $table = 'temporal_conformidad';
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $fillable = 
    [   
        'id', 
        'quiebre_id', 
        'actividad_id', 
        'estado_pretemporal_id', 
        'tipo_atencion',
        'tipo_fuente', 
        'evento_masiva', 
        'usuario_id', 
        'fecha_asignacion_usuario', 
        'gestion',
        'codactu', 
        'codcli',
        'actividad_tipo_id',
        'cliente_nombre',
        'cliente_celular',
        'cliente_telefono',
        'cliente_correo',
        'cliente_dni',
        'contacto_nombre',
        'contacto_telefono',
        'contacto_correo',
        'contacto_dni',
        'embajador_nombre',
        'embajador_correo',
        'embajador_celular',
        'embajador_dni',
        'comentario',
        'fecha_registro_legado',
        'validado',
        'fh_reg104',
        'fh_reg2l',
        'cod_multigestion',
        'telf_gestion',
        'telf_entrante',
        'operador',
        'motivo_call',
        'cod_servicio',
        'producto',
        'created_at'
    ];

   /* public static function boot() 
    {
        parent::boot();

        static::updating( function($table) {
            $table->usuario_updated_at = Auth::id();
        });
        static::creating( function($table) {
            if (1014!=Auth::id()) {
                $table->embajador_nombre = Session::get('full_name');
                $table->embajador_dni = Session::get('dni');
                $table->embajador_celular = Session::get('celular');
                $table->embajador_correo = Session::get('email');
            }
            $table->usuario_created_at = Auth::id();
        });
    }*/


    public static function  buscarTicket($ticket) 
    {
        $datos= array();
        $array = explode('E', $ticket);
        if (count($array)>1) {
            $ticket_id = $array[1];
            $ticket = (int)$ticket_id;
        }
        $datos = DB::table('temporal_conformidad as p')->where('p.id', $ticket)
                ->select(
                    "p.id",
                    "p.codactu",
                    "p.codcli",
                    "p.cliente_nombre",
                    "p.cliente_celular",
                    "p.cliente_telefono",
                    "p.cliente_correo",
                    "p.cliente_dni",
                    "p.contacto_nombre",
                    "p.contacto_celular",
                    "p.contacto_telefono",
                    "p.contacto_correo",
                    "p.contacto_dni",
                    "p.embajador_nombre",
                    "p.embajador_celular",
                    "p.embajador_correo",
                    "p.embajador_dni",
                    "p.comentario",
                    "p.created_at",
                    "p.validado",
                    "p.estado_pretemporal_id",
                    "p.actividad_tipo_id",
                    "p.evento_masiva_id",
                    "p.gestion",
                    DB::raw('IFNULL(em.nombre,"") as evento'),
                    DB::raw("CONCAT('E',p.id) as ticket"),
                    'at.apocope as tipo_averia',
                    'ep.nombre as estado',
                    'pa.nombre as atencion',
                    'q.nombre as quiebre',
                    DB::raw('IFNULL(e.id,"") as estado_id'),
                    DB::raw('IFNULL(pf.nombre,"") as fuente'),
                    'pta.estado_legado','pta.fecha_registro_legado',
                    'pta.nombre_cliente','pta.zonal','pta.contrata','pta.area',
                    'pta.mdf','pta.observacion','pta.fecha_liquidacion',
                    'pta.cod_liquidacion','pta.detalle_liquidacion'
                )
                ->join('actividades_tipos as at', 'at.id', '=', 'p.actividad_tipo_id')
                ->join('quiebres as q', 'q.id', '=', 'p.quiebre_id')
                ->join('estados_pre_temporales as ep', 'ep.id', '=', 'p.estado_pretemporal_id')
                ->Leftjoin('temp_conformidad_ultimo as ptu', 'ptu.temporal_conformidad_id', '=', 'p.id')
                ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
                ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'p.tipo_atencion')
                ->Leftjoin('temp_conformidad_averia as pta', 'p.id', '=', 'pta.temporal_conformidad_id')
                ->Leftjoin('pre_temporales_rrss as pf', 'pf.id', '=', 'p.tipo_fuente')
                ->Leftjoin('eventos_masiva as em','em.id','=','p.evento_masiva_id')
                ->get();

        if(count($datos)>0) {
            return $datos[0];
        }
    }



}    
