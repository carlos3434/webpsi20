<?php

class EnvioOfscUltimo extends Eloquent
{

    public $table = 'envios_ofsc_ultimo';

    public static function get($where = array())
    {
        if (count($where) > 0 ) {
            return DB::table("envios_ofsc_ultimo")->where($where)->get();
        } else {
            return DB::table("envios_ofsc_ultimo")->get();
        }
    }
}