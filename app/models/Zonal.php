<?php

class Zonal extends Base
{

    public $table = 'zonales';
    public static $where = ['id', 'nombre', 'abreviatura', 'estado'];
    public static $selec = ['id', 'nombre', 'abreviatura']; //'estado'

    /**
     * Usuario relationship
     */
    public function usuarios()
    {
        return $this->belongsToMany('Usuario');
    }
    /**
     * ProyectoEdificio relationship
     */
    public function proyectoEdificio()
    {
        return $this->hasMany('ProyectoEdificio');
    }
    public static function getZonal()
    {
        $z = DB::table('zonales')
                ->select(
                    'nombre', DB::raw('CONCAT(abreviatura,"|",id) as id')
                )
                ->where('estado', '=', '1')
                ->where(
                    function($query) {
                    if (Input::get('usuario')) {
                        $query->whereRaw(
                            ' id IN (SELECT zonal_id
                            FROM usuario_zonal
                            WHERE usuario_id="'
                            . Auth::id() . '"
                            AND estado=1
                            )'
                        );
                    }
                    }
                )
                ->get();
        return $z;
    }

    public static function getZonalM()
    {
        $z = DB::table('zonales')
                ->select('nombre', 'id')
                ->where('estado', '=', '1')
                ->where(
                    function($query) {
                    if (Input::get('usuario')) {
                        $query->whereRaw(
                            ' id IN (SELECT zonal_id
                            FROM usuario_zonal
                            WHERE usuario_id="' . Auth::id() . '"
                            AND estado=1
                            )'
                        );
                    }
                    }
                )
                ->get();
        return $z;
    }

    public static function getZonalUsuario($usuarioId)
    {
        $z = DB::table('zonales as z')
                ->join('usuario_zonal as uz', 'z.id', '=', 'uz.zonal_id')
                /* ->leftJoin(
                  'usuario_zonal as uz', function($join) use ($usuarioId)
                  {
                  $join->on('z.id', '=', 'uz.zonal_id')
                  ->on('uz.usuario_id', '=', DB::raw($usuarioId));
                  }
                  ) */
                ->select(
                    'z.nombre', 'z.id', 'uz.estado', 'uz.pertenece'
                )
                ->where('z.estado', '=', '1')
                ->where('uz.estado', '=', '1')
                ->where('uz.usuario_id', '=', $usuarioId)
                ->get();
        return $z;
    }

    public static function getZonalAbreviatura($usuarioId = null)
    {
        $z = DB::table('zonales')
                ->select(
                    'nombre', 'abreviatura AS id'
                )
                ->where('estado', '=', '1')
                ->where(
                    function($query) use($usuarioId) {
                    if (isset($usuarioId)) {
                        $query->whereRaw(
                            " id IN (SELECT zonal_id
                            FROM usuario_zonal
                            WHERE usuario_id='$usuarioId'
                            AND estado=1)"
                        );
                    }
                    }
                )
                ->get();
        return $z;
    }

    public static function getZonalCoordenadas()
    {
        $z = Zonal::select(
            'nombre',
            'abreviatura',
            'coord_x',
            'coord_y'
        )
        ->where('estado', '=', '1')
        ->get();
        $arreglo = array();
        foreach ($z as $val) {
            $contenido = array();
            $contenido['coordx'] = $val['coord_x'];
            $contenido['coordy'] = $val['coord_y'];
            $contenido['nombre'] = $val['nombre'];
            $contenido['abreviatura'] = $val['abreviatura'];
            $arreglo[$val['abreviatura']] = $contenido;
        }
        return $arreglo;
    }

}
