<?php

class Emergencia extends \Eloquent
{
    protected $guarded =[];
    protected $table = "emergencias";
    /**
     * relationship to EmergenciaImagen
     */
    public function emergenciaImagen()
    {
        return $this->hasMany('EmergenciaImagen');
    }
    /**
     * Scope para obtener  imagenes.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetImagenes($query)
    {
        return $query->join(
            'emergencia_imagen as i',
            'i.emergencia_id',
            '=',
            'emergencias.id'
        );
    }
}
