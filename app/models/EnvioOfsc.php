<?php

class EnvioOfsc extends \Eloquent
{


    public $table = 'envios_ofsc';


    /**
     * @param string $accion        accion
     * @param string $dataReq       data requerida
     * @param string $contenidoResp respuesta WS
     * @param string $respuestaEstadoWs status final de la WS
     */
    public function registrarAccionWebservice($accion, $dataReq, $contenidoResp,
                                             $usuario, $dataOfsc = array())
    {
            $empresa = "";
            $nodo = "";
            $useFrom = 0;
            $tipoAgenda = 0;
            $usuario = 0;
            $codactu = 0;
            $actividadTipoId = 0;
            $numIntentos = 1;
            $estadoOfsc = 0;
            $aid = 0;

            if (isset($dataOfsc["aid"])) {
              $aid = $dataOfsc["aid"];
            }

            if (isset($dataOfsc["usuario"])) {
              $usuario = $dataOfsc["usuario"];
            } else if (Auth::user()!==null) {
              //$usuario = Auth::user()->id;
            }

            if (count($dataOfsc) > 0) {
              switch ($accion) {
                case 'inbound_interface':
                    $codactu = $dataOfsc["appt_number"];
                    $tipoAgenda = $dataOfsc["isSla"];
                    if (isset($dataOfsc["use_from"]))
                      $useFrom = $dataOfsc["use_from"];
                    $orden = Gestion::getCargar($codactu, true);
                    if ($orden["rst"] == 1) {
                        $orden = $orden["datos"][0];
                        $orden = get_object_vars($orden);

                        $empresa = "";
                        if (isset($orden["empresa"]))
                            $empresa = $orden["empresa"];
                          $nodo = $orden["mdf"];
                          $actividadTipoId = $orden["actividad_tipo_id"];
                    }
                    break;
                case 'get_capacity':
                    break;

                default:
                    break;
              }
            }
            if ($codactu ===0) {

            } else {
                $envio = DB::table("envios_ofsc")
                         ->select("numintentos")
                         ->where("codactu", "=", $codactu)
                         ->orderBy("id", "desc")
                         ->get();

              if (count($envio) > 0) {
                $numIntentos = $envio[0]->numintentos + 1;
              }
              $ultimo_movimiento = DB::table("ultimos_movimientos")
                          ->select("estado_ofsc_id")
                          ->where("codactu", "=", $codactu)
                          ->orderBy("id", "desc")
                          ->get();
              if ( count($ultimo_movimiento)  > 0) {
                $estadoOfsc = $ultimo_movimiento[0]->estado_ofsc_id;
                if ($estadoOfsc === null)
                  $estadoOfsc = 0;
              }
            }

            $insert = [
                   'accion'             => $accion,
                   'aid'                    => $aid,
                   'contrata'          => $empresa,
                   'nodo'                 => $nodo,
                   'codactu'          => $codactu,
                   'tipo_actividad_id' => $actividadTipoId,
                   'tipo_envio'       => $tipoAgenda,
                   'enviado'            => $dataReq,
                   'estado_ofsc'    => $estadoOfsc,
                   'respuesta'          => $contenidoResp,
                   'usuario_created_at' => $usuario,
                   'use_from' => $useFrom,
                   'created_at' => DB::raw('NOW()')
          ];
          $this->insertEnvioOfsc($insert);

    }//end registrarAccionWebservice()

    public function insertEnvioOfsc ($insert= array())
    {
        if (count($insert) > 0) {
          DB::table('envios_ofsc')->insert($insert);
        }
    }
    public function getEnviosOfsc ($codactu = 0)
    {
      if ($codactu > 0) {
        return DB::table("envios_ofsc as eo")
                ->select(
                    "codactu", "tgo.nombre as gestion_ofsc",
                    "contrata as empresa", "nodo", "aid",
                    DB::raw(
                        "(CASE tipo_envio WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA' END) as tipoAgenda "
                    ),
                    "created_at as fecha_envio", "eo.accion as accion"
                )
            ->leftJoin("tipo_gestion_ofsc as tgo", "tgo.accion", "=", "eo.accion")
            ->where("codactu", "=", $codactu)
            ->orderBy("eo.id", "desc")
            ->get();
      } else {
        return array();
      }
    }

    public  static function getOrdenes()
    {
//<<<<<<< HEAD
            $consulta = array();
             if (Input::has("bandeja")) {
                    $resultado = array();
                    $fechaRegistro = explode(" - ",Input::get("fecha_registro"));
                    if (Input::has("download_excel")) { 
                        $consulta = DB::table("envios_ofsc_ultimo as eou")
                                        ->select(
                                                DB::raw("codactu as CodigoRequerimiento, IFNULL(eou.fec_updated, eou.fec_created) as fecha_envio, eou.aid as AID,
                                                    at.nombre as TipoActividad, e.nombre as Empresa, n.nombre as Nodo,
                                                    (CASE eou.tipo_envio WHEN 0 THEN '' WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA - Libre' END) AS Agenda, CONCAT (u.nombre, u.apellido, ' ') as Usuario ")
                                            );
                    }
                    else {
                        $consulta = DB::table("envios_ofsc_ultimo as eou")
                                    ->select(
                                        DB::raw("DISTINCT eou.codactu, eou.aid as aid, 
                                          eou.empresa, IFNULL(eou.fec_updated, eou.fec_created) as fecha_envio,
                                        at.nombre as nombre_tipo_actividad, at.actividad_id, u.usuario as loginusuario, e.nombre as empresa,
                                        n.nombre as nombre_nodo, (CASE eou.tipo_envio WHEN 0 THEN '' WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA - Libre' END) AS nombre_agenda, 
                                        eof.nombre as estado_ofsc, eou.numintentos, um.estado_ofsc_id " )
                                        );
                    }
                    $between = " BETWEEN '".$fechaRegistro [0]."' AND '".$fechaRegistro [1]."' ";
                    $consulta->whereRaw(" IF ( ISNULL(eou.fec_updated) ,  eou.fec_created ".$between." ,  eou.fec_updated ".$between.")")->leftJoin( 'usuarios as u',  'u.id', '=', 'eou.user_updated')
                                    ->leftJoin( 'empresas as e',  'e.nombre', '=', 'eou.empresa')
                                    ->leftJoin('ultimos_movimientos as um', 'um.id', '=', 'eou.id_ultimo_movimiento')
                                    ->leftJoin( 'webpsi_fftt.nodos_eecc_regiones as n',  'n.NODO', '=', 'eou.nodo')
                                    ->leftJoin( 'actividades_tipos as at',  'at.id', '=', 'eou.tipo_actividad_id')
                                    ->leftJoin( 'actividades as ac',  'at.actividad_id', '=', 'ac.id')
                                    ->leftJoin('estados_ofsc as eof', 'um.estado_ofsc_id', '=', 'eof.id');
                    if (Input::get("codigo_actuacion")!="") {
                        $consulta->where("eou.codactu", "=", Input::get("codigo_actuacion"));
                    } else {
                        if (Input::has("estado_ofsc")) {
                          $estado_ofsc = Input::get("estado_ofsc");
                          if ($estado_ofsc!=="") {
                            $consulta->where("gm.estado_ofsc_id", "=", $estado_ofsc);
                          }
                        }
                        if (Input::has("empresa")) {
                            $empresa = Input::get("empresa");
                            if ($empresa!=="null") {
                                if (Input::has("download_excel")) {
                                    $consulta->whereRaw(" e.id IN ( $empresa) ");
                                } else {
                                    $consulta->whereIn("e.id", $empresa);
                                }
                            }
                            
                        }
                        if (Input::has("nodo")) {
                            $nodo = Input::get("nodo");

                        if (Input::has("download_excel")) {
                            if ($nodo!=="null") {
                                $consulta->whereRaw(" eou.nodo IN ($nodo)");
                            }
                        } else {
                            $consulta->whereIn("eou.nodo", Input::get("nodo"));
                        }
                    }
                    if (Input::has("actividad")) {
                        $actividad = Input::get("actividad");
                        if (Input::has("download_excel")) {
                            if ($actividad!=="null")
                                $consulta->whereRaw(" ac.id IN ( $actividad) ");
                        } else {
                            $consulta->whereIn("ac.id", $actividad);
                        }
                        $actividad_tipo = Input::get("actividad_tipo");
                         if (Input::has("actividad_tipo")) {
                            if (Input::has("download_excel")) {
                                if ($actividad_tipo!=="null")
                                    $consulta->whereRaw(" eou.tipo_actividad_id IN ($actividad_tipo) ");
                            } else {
                                $consulta->whereIn("eou.tipo_actividad_id", $actividad_tipo);
                            }
                         }
                    }
                    if (Input::get("tipo_envio")!="") {
                        $consulta->where("eou.tipo_envio", "=", Input::get("tipo_envio"));
                    }
                }
                if (Input::has("download_excel")) {
                    $resultados["datos"] = $consulta->get();
                } else {
                    $resultados["total"] = count($consulta->get());
                    if ( Input::get('draw') ) {
                        if ( Input::get('order') ) {
                            $inorder=Input::get('order');
                            $incolumns=Input::get('columns');
                            $campoOrderBy = $incolumns[ $inorder[0]['column'] ]['name'];
                            $orderDir  = $inorder[0]['dir'];
                            $resultados["datos"] = 
                                        $consulta->limit(Input::get("length"))
                                                ->offset(Input::get("start"))
                                                ->orderBy($campoOrderBy, $orderDir)
                                                ->get();
                        }
                    } else {
                        $resultados["datos"] = $consulta
                                              ->limit(Input::get("length"))
                                              ->offset(Input::get("start"))
                                              ->get();
                    }
                }

                return $resultados;
        } else {
            $fecha= explode(" - ", Input::get('fecha'));

            $query = "SELECT e.accion, e.enviado, e.respuesta ,
                        e.created_at, u.usuario
                      FROM envios_ofsc e
                      LEFT JOIN usuarios u
                      ON u.id = e.usuario_created_at
                      WHERE date(e.created_at) BETWEEN '" . $fecha[0] . "'
                      AND '" . $fecha[1] . "'";
            $consulta = DB::select($query);
            $envios["data"] = $consulta;
        }
        return $consulta;
    }

}
