<?php 
class GeoDireccion extends Eloquent
{
    protected $table = "geo_direccion";

    protected $guarded =[];
    
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    public function coordenadas()
    {
        return $this->hasMany('GeoDireccionCoord','geo_direccion_id');
    }

    public function grupodireccion()
    {
        return $this->belongsTo('GeoGrupoDireccion');
    }

}