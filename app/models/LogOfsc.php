<?php
class LogOfsc extends Eloquent
{

    public $table = "log_ofsc";

    public static function getTypeLog()
    {
        $r  =   DB::table('log_ofsc')
          ->select(
              'type as id',
              DB::raw(
                  'type as nombre'
              )
          )
                  ->orderBy('type', 'asc')
                  ->groupBy('type')
                  ->get();
        return $r;
    }

    public static function getCargarLog($filtro)
    {

        $query ="Select id,process,type,body,created_at as created_at 
                from log_ofsc where 1 ". $filtro." order by id desc limit 1";

        $res = DB::select($query);
        $result = array();
        $i=0;
        foreach ($res as $key => $value) {
            $result[$i]['id'] = $value->id;
            $result[$i]['process'] = $value->process;
            $result[$i]['type'] = $value->type;
            $result[$i]['body'] = $value->body;
            $result[$i]['created_at'] = $value->created_at;
            $i++;
        }
       // print_r($result);
         $dataasignaciondeldia=array();
        foreach ($result as $index => $value) {
                $dataasignacion=array();
                $dataasignacion['id']=$value['id'];
                $dataasignacion['process']=$value['process'];
                $dataasignacion['type']=$value['type'];
               // $dataasignacion['body']=$value['body'];
                if (strlen($value['body'])<=240 and strlen($value['body'])>=1) {
                  $dataasignacion['body']=serialize($value['body']);
                } else {
                  $dataasignacion['body']=$value['body'];
                }
                $dataasignacion['created_at']=$value['created_at'];

                array_push($dataasignaciondeldia, $dataasignacion);
        }

        //print_r( $dataasignaciondeldia);
        return $dataasignaciondeldia;
    }

}
