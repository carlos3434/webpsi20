<?php 
class Nodo extends Eloquent
{

    public $table = 'geo_nodopunto';

    public static function getNodoAll()
    {
        $r  =   DB::table('geo_nodopunto')
          ->select(
              'nodo as id',
              DB::raw(
                  'CONCAT(nodo,": ",nombre,"->",zonal) as nombre'
              )
          )
          ->where('tecnologia','COAXIAL')
                  ->orderBy('nodo', 'asc')
                  ->get();
        return $r;
    }

}
