<?php 
class Nodo extends Eloquent
{

    public $table = 'webpsi_fftt.nodos_eecc_regiones';

    public static function getNodoAll()
    {
        $r  =   DB::table('webpsi_fftt.nodos_eecc_regiones')
          ->select(
              'NODO as id',
              DB::raw(
                  'CONCAT(NODO,": ",NOMBRE,"->",PROVINCIA,"->",DPTO) as nombre'
              )
          )
                  ->orderBy('NODO', 'asc')
                  ->get();
        return $r;
    }

}
