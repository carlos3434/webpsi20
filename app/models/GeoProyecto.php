<?php 
class GeoProyecto extends Eloquent
{

    public static function getidProyecto($proyecto)
    {
         $query = " select id from geo_proyecto where nombre='".$proyecto."' 
                       order by id desc limit 1  ";

        $res = DB::select($query);

        return $res;
    }

    public static function getAgregarProyecto(array $data)
    {
        $sql = "insert into geo_proyecto(nombre,usuario_created_at,created_at)
                 values(?,?,now())";
        return DB::insert($sql, $data);
    }


    public static function getCargarProyectos()
    {
       $r  =   DB::table('geo_proyecto')
        ->select(
            'id',
            DB::raw('nombre'),
            DB::raw('estado'),
            DB::raw('estado_publico as publico'),
            DB::raw('date(created_at) as created_at')
        )
         ->orderBy('id', 'desc')
          ->get();
        return $r;
    }

    public static function getUpdateProyecto(array $data )
    {

        DB::table('geo_proyecto')
                    ->where('id', $data[0])
                    ->update(
                        array(
                        $data[2]=> $data[1],
                        'usuario_updated_at'=> $data[3],
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); 

          return '1';
    }

    public static function getUpdateProyectoCapa(array $data )
    {
          DB::table('geo_proyecto')
                    ->where('id', $data[0])
                    ->update(
                        array(
                        'usuario_updated_at'=> $data[1],
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); 

          return '1';
    }

    public static function getUpdateProyectoCapaElem(array $datas )
    {
          DB::table('geo_proyecto')
          ->join('geo_proyecto_capa', 'geo_proyecto_capa.proyecto_id', '=', 'geo_proyecto.id')
          ->where('geo_proyecto_capa.id', $datas[0])
          ->update(
              array(
              'geo_proyecto.usuario_updated_at'=> $datas[1],
              'geo_proyecto.updated_at'=> date('Y-m-d H:i:s'),
              )
          ); 

          return '1';;
    }

    // proyectos publicos
    public static function getListarProyectos($usuario)
    {

        $query = "select gp.id as proyecto_id, gp.nombre as nombre, 
                  gp.estado as estado, estado_publico,
                  gp.usuario_created_at,GROUP_CONCAT(gpc.id) as id 
                  from geo_proyecto gp
                  inner join geo_proyecto_capa gpc 
                  on gp.id=gpc.proyecto_id
                  where gp.estado=1 and (estado_publico=1 or 
                  usuario_created_at='".$usuario."')
                  GROUP BY gp.id ";

        $res = DB::select($query);

        return $res;
    }

}
