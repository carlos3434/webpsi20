<?php 
class GeoProyecto extends Eloquent
{
    protected $table = "geo_proyecto";
    protected $guarded =[];
    
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }

    /*public static function getidProyecto($proyecto)
    {
         $query = " select id from geo_proyecto where nombre='".$proyecto."' 
                       and estado=1 order by id desc limit 1  ";

        $res = DB::select($query);

        return $res;
    }*/

    public static function agregar(array $data)
    {
        $query = new GeoProyecto($data);
        $query->save();

        return $query;
    }


    public static function cargar()
    {
       $r  =   GeoProyecto::select(
                    'id',
                    'nombre',
                    'estado',
                    DB::raw('estado_publico as publico'),
                    DB::raw('DATE(created_at) as created')
                )
                ->where('estado', '=','1')
                ->orderBy('id', 'desc')
                ->get();
        return $r;
    }

    public static function actualizar(array $data,$id )
    { 
         return GeoProyecto::where('id', $id)
                    ->update(
                        $data
                    ); 
    }
/*
    public static function getUpdateProyectoCapa(array $data )
    {
          DB::table('geo_proyecto')
                    ->where('id', $data[0])
                    ->update(
                        array(
                        'usuario_updated_at'=> $data[1],
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); 

          return '1';
    }

    public static function getUpdateProyectoCapaElem(array $datas )
    {
          DB::table('geo_proyecto')
          ->join('geo_proyecto_capa', 'geo_proyecto_capa.proyecto_id', '=', 'geo_proyecto.id')
          ->where('geo_proyecto_capa.id', $datas[0])
          ->update(
              array(
              'geo_proyecto.usuario_updated_at'=> $datas[1],
              'geo_proyecto.updated_at'=> date('Y-m-d H:i:s'),
              )
          ); 

          return '1';;
    }*/

    // proyectos publicos
    public static function listar($usuario)
    {
      if(Input::get('lista') && Input::get('lista')==1){
        $complete='gp.id as id';
      } else {
        $complete='GROUP_CONCAT(gpc.id) as id';
      }
        
        $query = "select gp.id as proyecto_id, gp.nombre as nombre, 
                  gp.estado as estado, estado_publico,
                  gp.usuario_created_at, ".$complete." 
                  from geo_proyecto gp
                  inner join geo_proyecto_capa gpc 
                  on gp.id=gpc.proyecto_id
                  where gp.estado=1 and (estado_publico=1 or 
                  usuario_created_at='".$usuario."')
                  GROUP BY gp.id ";

        $res = DB::select($query);

        return $res;
    }

}
