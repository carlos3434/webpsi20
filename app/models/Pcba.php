<?php

use Legados\models\EnvioLegado;

class Pcba extends Eloquent
{
    public function obtenerprueba()
    {
        $url = \Config::get("legado.wsdl.pcba");

        $CodArea = Input::get('codarea','');
        $Telefono = Input::get('telefono',''); // '2796403';

        $url.="?CodArea=".$CodArea."&Telefono=".$Telefono;

        $xml = new DOMDocument();
        $xml->load($url);
        $xmlTest = $xml->saveXML();
        $xmlObj = simplexml_load_string($xmlTest);

        $elementos = [
            'action'               => 'PCBA',
            'telefono'             => $CodArea.$Telefono
        ];

        $this->tracer($elementos, $xmlObj);

        return $xmlObj;
    }

    protected function tracer($elementos = [], $response = [])
    {
        $userId = \Auth::id();
        $envio = new EnvioLegado;
        $envio->request = json_encode($elementos);
        $envio->response = json_encode($response);
        $envio->accion = $elementos["action"];
        $envio->solicitud_tecnica_id = $elementos["telefono"];
        $envio->save();
        return;
    }

}
