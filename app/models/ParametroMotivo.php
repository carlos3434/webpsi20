<?php

class ParametroMotivo extends \Eloquent
{
	public $table = "parametro_motivo";

	public function parametroMotivoDetalle()
    {
        return $this->hasMany('ParametroMotivoDetalle');
    }
}