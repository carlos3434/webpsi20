<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Base implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait, DataViewer;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'usuarios';
    protected $guarded =[];
    public static $where =[
                        'id', 'nombre','apellido','usuario', 'password',
                        'dni','sexo', 'imagen', 'estado'
                        ];
    public static $selec =[
                        'id', 'nombre','apellido','usuario', 'password',
                        'dni','sexo', 'imagen', 'estado'
                        ];

    public static function boot() 
    {
        parent::boot();

        static::created(function($table){
            Cache::forget('usuario_list');
        });

        static::updated(function($table){
            Event::fire('user.updated', $table);
            Cache::forget('usuario_list');
        });
    }
    /**
     * ejemplo Auth::user()->full_name
     */
    public function getFullNameAttribute()
    {
        return $this->apellido .' '. $this->nombre;
    }
    public static function get(array $data =array())
    {

        //recorrer la consulta
        $personas = parent::get($data);

        foreach ($personas as $key => $value) {
            if ($key=='password') {
                $personas[$key]['password']='';
            }
        }

        return $personas;
    }
    public static function getPerfil()
    {
        return Session::get('perfilId');
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public $hidden = array('password', 'remember_token');
    /**
     * SubModulo relationship
     */
    public function submodulos()
    {
        return $this->belongsToMany('Submodulo');
    }

    /**
     * Empresa relationship
     */
    public function area()
    {
        return $this->belongsTo('Area');
    }
    /**
     * Empresa relationship
     */
    public function empresa()
    {
        return $this->belongsTo('Empresa');
    }

    /**
     * Usuario relationship
     */
    public function empresas()
    {
        return $this->belongsToMany('Empresa');
    }
    /**
     * Zonal relationship
     */
    public function zonales()
    {
        return $this->belongsToMany('Zonal');
    }
    /**
     * QuiebreGrupo relationship
     */
    public function quiebregrupos()
    {
        return $this->belongsToMany('QuiebreGrupo');
    }
    public function getSubmodulos($usuarioId)
    {
        //subconsulta
        $sql = DB::table('submodulo_usuario as su')
        ->join(
            'submodulos as s',
            'su.submodulo_id', '=', 's.id'
        )
        ->join(
            'modulos as m',
            's.modulo_id', '=', 'm.id'
        )
        ->select(
            DB::raw(
                "
            CONCAT(m.id, '-',
            GROUP_CONCAT(s.id)) as info"
            )
        )
        ->whereRaw("su.usuario_id=$usuarioId AND su.estado=1 AND m.estado=1")

        ->groupBy('m.id');
        //consulta
        $submodulos = DB::table(DB::raw("(".$sql->toSql().") as o"))
                ->select(
                    DB::raw("GROUP_CONCAT( info SEPARATOR '|'  ) as DATA ")
                )
               ->get();

        return $submodulos;

    }

    public function getSubmodulosParent($usuarioId)
    {

        $parents = array();
        $submodulos = array();
        $query = DB::table("submodulo_usuario as su")
                    ->rightJoin("usuarios as u", "su.usuario_id", "=", "u.id")
                    ->rightJoin("submodulos as s", "s.id", "=", "su.submodulo_id")
                    ->select("s.modulo_id", "s.id", "parent")
                    ->where("s.parent", ">", "0")
                    ->where("su.estado", "=", "1")
                    ->where("su.usuario_id", "=", $usuarioId)
                    ->get();

        foreach($query as $key => $value){
            $submodulos[$value->modulo_id][] = $value;
            $parents[$value->parent] = $value->parent;
        }

        if(count($parents) > 0){
            $parents = DB::table("submodulos")
                         ->select("id", "nombre", "modulo_id")
                         ->whereIn("id", $parents)
                         ->get();
            $parents_temp = array();
            foreach($parents as $key => $value){
                $parents_temp[$value->id] = $value;
            }
            $parents = $parents_temp;
        }

        return array("submodulos" => $submodulos, "parents" => $parents);
    }
    public function getPrivilegios($usuarioId, $idModulo)
    {

        return $submodulos = DB::table('submodulo_usuario as su')
        ->join(
            'submodulos as s',
            'su.submodulo_id', '=', 's.id'
        )
        ->join(
            'modulos as m',
            's.modulo_id', '=', 'm.id'
        )
        ->select(
            'su.usuario_id',
            'su.submodulo_id',
            's.nombre',
            //'su.consultar',
            'su.agregar',
            'su.editar',
            'su.eliminar'
        )
        ->where('su.usuario_id', $usuarioId)
        ->where('m.id', $idModulo)
        ->where('su.estado', 1)
        ->where('m.estado', 1)
        ->orderBy('nombre', 'asc')
        ->get();

    }

    public function getbandejap($usuarioId)
    {
        return DB::table('bandeja_permisos as bp')
            ->join(
                'bandeja_list as bl',
                'bl.id', '=', 'bp.vbandeja_id'
            )
            ->select(
                'bp.*', 'bl.nombre', 'bl.descripcion', 'bl.columna', 'bl.option', 'bl.option2'
            )
            ->where('bp.usuario_id', $usuarioId)
            ->get();
    }

    public function getAllUsuarios($a="", $b="")
    {
          
    }

    public function getAllUsers()
    {
            $usuario = Auth::id();
            $perfilId = Session::get('perfilId');
            $usuarios =  DB::table('usuarios AS u')
                        ->join(
                            'empresas as e',
                            'u.empresa_id', '=', 'e.id'
                        )
                        ->join(
                            'perfiles as p',
                            'u.perfil_id', '=', 'p.id'
                        )
                        ->join(
                            'areas as a',
                            'u.area_id', '=', 'a.id'
                        )
                        ->select(
                                'u.id',
                                DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) as nombre')
                        )
                        ->whereRaw('(u.perfil_id=8 or u.area_id=52)')
                        ->where('u.estado',1);
            $usuarios = $usuarios->get();
            return $usuarios;
    }

}
