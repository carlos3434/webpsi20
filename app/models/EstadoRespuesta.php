<?php

use Ofsc\Outbound;
use Ofsc\Activity;
use Ofsc\Capacity;
/**
*
*/
class EstadoRespuesta
{
    protected $_gestionMovimientoController;
    protected $_bandejaController;

    private $body;
    private $subject;
    private $codactu;
    private $bucket;
    private $data;
    private $aid;
    private $idmotivo;
    private $idsubmotivo;
    private $starttime;
    private $endtime;
    private $number;
    private $text;

    function __construct(
                    GestionMovimientoController $gestionMovimientoController,
                    BandejaController $bandejaController)
    {
        $this->_gestionMovimientoController = $gestionMovimientoController;
        $this->_bandejaController = $bandejaController;
    }
    public function getGestion()
    {
        Input::replace([]); 
        $actividades = Gestion::getCargar($this->codactu);
        $cantidad= count($actividades["datos"]);
        if ($cantidad>0) {
            return (array) $actividades["datos"][$cantidad-1];
        } else {
            return [];
        }
    }
    public function recibir($data)
    {
        $this->setData($data);
        $body = str_replace(["<![CDATA[","]]>"], "", $data['body']);
        if (Helpers::isValidXml($body)) {
            $this->setBody(simplexml_load_string($body));
        } else {
            $this->setBody("");
        }
        //capturando la fecha de envio mensaje
        $date = new DateTime( date("Y-m-d") );
        $hoy = $date->format('Y-m-d');

        $this->setSubject(strtoupper(trim($data['subject'])));

        if ($this->subject=='TOA_OUTBOUND_NOTDONE' ||
            $this->subject=='TOA_OUTBOUND_SUSPEND') {
            if ( isset($this->body->activity_properties->appt_number) )
                $this->codactu =
                    trim($this->body->activity_properties->appt_number);
            if ( isset($this->body->activity_properties->external_id) )
                $this->bucket =
                    trim($this->body->activity_properties->external_id);
            //armando el datetime cierre
            if ( isset($this->body->activity_properties->eta_end_time) )
                $this->endtime=
                        $hoy.' '.trim(
                            $this->body->activity_properties->eta_end_time
                        );
        } elseif ( $this->subject == 'UNSCHEDULE' ||
                   $this->subject == 'TOA_OUTBOUND_COMPLETED' ||
                   $this->subject == 'WO_INIT' ||
                   $this->subject == 'WO_CANCEL' ) {

            if ( isset($this->body->appt_number) )
                $this->codactu= trim(strtoupper($this->body->appt_number));
            if ( isset($this->body->external_id) )
                $this->bucket= trim(strtoupper($this->body->external_id));
            //armando el datetime incio
            if ( isset($this->body->eta_start_time) )
                $this->starttime= $hoy.' '.trim($this->body->eta_start_time);
            //armando el datetime cierre
            if ( isset($this->body->eta_end_time) )
                $this->endtime= $hoy.' '.trim($this->body->eta_end_time);
        } elseif ( $this->subject == 'INT_SMS' ) {
            if ( isset($this->body->number) )
                $this->number= trim($this->body->number);
            if ( isset($this->body->text) )
                $this->text= $this->body->text ;
            return;
        } elseif ($this->subject=='TOA_VALIDATE_COORDS') {
            if ( isset($this->body->validation_properties->aid) )
                $this->setAid(trim($this->body->validation_properties->aid));
        }

        if ( isset($this->body->aid) ) {
            $this->setAid(trim($this->body->aid));
        } elseif ( isset($this->body->activity_properties->aid) ) {
            $this->setAid(trim($this->body->activity_properties->aid));
        }
    }
    public function responder()
    {
        $date = date("Y-m-d");
        $body=$this->body;

        $respuesta =array();
        if ($this->subject == 'TOA_OUTBOUND_COMPLETED') {
            foreach ($body->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $actuacion=$this->completarOfsc($respuesta);
            if ($actuacion['rst']==1) {
                $respuesta=$this->sendeMessageToa();
                if ($this->idsubmotivo=='32' && $respuesta==1) {
                    // para completado: 
                    //Otros problemas
                    //Inefectiva
                    //Casa Cerrada
                    $visitas=GestionMovimiento::getcontarvisita($actuacion['id']);
                    //si se completo actividad, se debe reenviar a ofsc al mismo
                    //tecnico sin fecha de cita y sla
                    if ($actuacion['envio_ofsc']==2 && $visitas<3) {
                        $tecnico =  $this->getCarnetTmp();

                        $agenda = [];
                        $agenda['agdsla']="sla";
                        $agenda['codactu'] =$this->codactu;
                        $agenda['empresa_id']=$actuacion['empresa_id'];
                        if (count($tecnico)>0) {
                            $agenda['tenicoreagenda']=$tecnico[0]->id;
                            $agenda['celulareagenda']=$tecnico[0]->celula_id;
                        }
                        $agenda['hf']=$date .'||'.$this->bucket.'||0||0-0';
                        $agenda['slaini']=$date.' 12:00';
                        $agenda['tipoprograma']='noprogramado';
                        //no programado: sin fecha de cita
                        $agenda['desprogramar']='ok';
                        $agenda['nenvio']=$visitas+1;
                        Input::replace($agenda);
                        Auth::loginUsingId(952);
                        $devolver=$this->_bandejaController->postEnvioofsc();
                        Auth::logout();
                    }
                }
            }
            //prueba Modem WS
            $this->pruebaModem($actuacion['gestion_id']);

        } elseif ($this->subject == 'TOA_OUTBOUND_SUSPEND') {
            foreach ($body->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $rst=$this->suspenderOfsc($respuesta);
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        } elseif ($this->subject == 'TOA_OUTBOUND_NOTDONE') {
            foreach ($body->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $actuacion=$this->norealizadoOfsc($respuesta);
            if ($actuacion['rst']==1) {
                $respuesta=$this->sendeMessageToa();
                if ($this->idmotivo=='35' && $respuesta==1) {
                    // para no realizado: cliente ausente
                    //$actuacion = $this->getGestion();
                    $visitas=GestionMovimiento::getcontarvisita($actuacion['id']);
                    //si no se realizo la actividad, se debe reenviar a ofsc
                    //al mismo tecnico sin fecha de cifa con sla
                    if ($actuacion['envio_ofsc']==2 && $visitas<3) { 
                        $tecnico =  $this->getCarnetTmp();
                        $agenda = [];
                        $agenda['agdsla']="sla";
                        $agenda['codactu'] =$this->codactu;
                        $agenda['empresa_id']=$actuacion['empresa_id'];

                        if (count($tecnico)>0) {
                            $agenda['tenicoreagenda']=$tecnico[0]->id;
                            $agenda['celulareagenda']=$tecnico[0]->celula_id;
                        }
                        $agenda['hf']=$date . '||'.$this->bucket.'||0||0-0';
                        $agenda['slaini']=$date.' 12:00';
                        //no programado: sin fecha de cita
                        $agenda['tipoprograma']='noprogramado';
                        $agenda['nenvio']=$visitas+1;
                        $agenda['desprogramar']='ok';
                        Input::replace($agenda);
                        Auth::loginUsingId(952);
                        $devolver=$this->_bandejaController->postEnvioofsc();
                        Auth::logout();
                    }
                }
            }
        } elseif ($this->subject == 'WO_CANCEL') {
            foreach ($body->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $rst=$this->cancelarOfsc($respuesta);
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        } elseif ($this->subject == 'WO_INIT') {
            foreach ($body->activity_properties->property as $key => $value) {
                $respuesta[(string)$value->label]=(string)$value->value;
            }
            $rst=$this->iniciarOfsc($respuesta);
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        } elseif ($this->subject == 'UNSCHEDULE') {
            $rst=$this->desprogramarOfsc();
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        } elseif ($this->subject == 'INT_SMS') {
            $rst=$this->envioSmsOfsc();
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        } elseif ($this->subject == 'TOA_VALIDATE_COORDS') {
            $rst=$this->validarDistanciaOfsc();
            if ($rst==1) {
                $this->sendeMessageToa();
            }
        }
        return true;
    }

    public function pruebaModem($gestion_id = null)
    {
        //consulta del webservice Modem
        $id = $gestion_id;
        $catalogo = CatalogoDecos::select('mac')
                                ->where('gestion_id', '=', $id)
                                ->where('mac', '!=', '')->first();
        if ($catalogo && isset($catalogo->mac)) {
            $resultadoWS = CatalogoDecos::consultarModem($catalogo->mac);
            //guardar resultado de WS
            $modem = new Modem;
            $modem->gestion_id = $id;
            $modem->mac = $catalogo->mac;
            $modem->prueba = $resultadoWS;
            $modem->save();
        }
    }
    /**
     * valida la distancia entrea las coordenadas de tecnico y actividad
     * retorna 2 cuando el tecnico esta dentro de la distancia permitida
     * retorna 1 en caso contrario
     */
    public function validarDistanciaOfsc()
    {
        $location=array('x'=>0,'y'=>0);
        if ( isset($this->body->validation_properties->appt_coordinates->acoord_x) )
            $this->acoord_x =
                trim($this->body->validation_properties->appt_coordinates->acoord_x);
        if ( isset($this->body->validation_properties->appt_coordinates->acoord_y) )
            $this->acoord_y =
                trim($this->body->validation_properties->appt_coordinates->acoord_y);

        if ( isset($this->body->external_id) ) {
            $this->bucket=trim($this->body->external_id);
            $location=$this->getLocationCarnet();
        }
     
        if(count($location)!=2 || $location['x']=='0' || $location['y']=='0') {
            if ( isset($this->body->validation_properties->resource_coordinates) ) {
                $Coordtecnico = $this->body->validation_properties->resource_coordinates;
                $Coordtecnico = str_replace( ["lat:", "lng:"], "",trim( $Coordtecnico ) );
                $array= explode(",", $Coordtecnico);
                if (count($array)==2) {
                    $location['x']=$array['1'];
                    $location['y']=$array['0'];
                }
            }

        }

        if ( isset($this->acoord_x) && $this->acoord_x !='' &&
             isset($this->acoord_y) && $this->acoord_y !='' &&
             count($location)==2  &&  $location['x']!='' && $location['y']!='' &&
             isset($this->aid) && $this->aid!='' ) {

            $data=[
               'coord_x1'=>$this->acoord_x, //coord de actividad
               'coord_y1'=>$this->acoord_y, //coord de actividad lat 1
               'coord_x2'=> $location['x'], // coord de tecnicos
               'coord_y2'=> $location['y'] //coord de tecnico lat 2
            ];
            $respuesta = GeoValidacion::getValidadistancia($data);
            if ($respuesta['rpta']==1) {
               $actividad["A_ESTADO_VALIDACION_COORDENADAS"]='2';
            } else {
               $actividad["A_ESTADO_VALIDACION_COORDENADAS"]='1';
            }
            $activity = new Activity();
            $response = $activity->updateActivity($this->aid, $actividad);
            return 1;
        }
        return 2;
    }

     public function sendeMessageToa()
     {
        $outbound = new Outbound();
        $request=['message'=>
                    [
                        'message_id'=>$this->data['message_id'],
                        'status'=>'delivered'
                        ]
                    ];

        $response=$outbound->setMessageStatus($request);

        if (!$response->error) {
            $code = $response->data->message_response->result->code;
            if ($code=='OK') {
                $sql ="UPDATE mensajes SET estado='delivered'
                        WHERE message_id=?";
                DB::update($sql, array($this->data['message_id']));
                return 1;
            }
        }
        return 0;
     }

    public function desprogramarOfsc()
    {
        $actuacion = $this->getGestion();
        if (count($actuacion)>0) {
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=7;
            $actuacion['motivo']=2;
            $actuacion['submotivo']=18;
            $actuacion['noajax']='ok';

            unset($actuacion["fecha_agenda"]);
            unset($actuacion["horario_id"]);
            unset($actuacion["dia_id"]);
            unset($actuacion["tecnico"]);
            unset($actuacion["tecnico_id"]);
            unset($actuacion["celula_id"]);

            $actuacion['programado']=0;
            $actuacion['estado_ofsc_id']=7;

            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $this->_gestionMovimientoController->postRegistrar();
            if (trim($actuacion['envio_ofsc'])=='1') return;
            $date = date("Y-m-d", strtotime("+ 1 days"));
            $timeSlot =$bucket= '';
            $actividadTipo=ActividadTipo::find($actuacion['actividad_tipo_id']);
 
            if ( isset($actividadTipo->label) && isset($actividadTipo->duracion)
            ) {
                $capacity = new Capacity();
                $data=[
                    'fecha'          => [$date],
                    'time_slot'      => $timeSlot,
                    'work_skill'     => $actividadTipo->label,
                    'mdf'            => $actuacion['mdf']
                ];
                $response = $capacity->getCapacity($data);
                if (isset($response->error) && $response->error===false && 
                    isset($response->data->capacity[0]->location) ) {
                    $bucket = $response->data->capacity[0]->location;
                }
            }
            if ($bucket=='') $bucket = $this->bucket;

            $actuacion['codactu']=$this->codactu;
            //$actuacion['fono1']="000000";
            //$actuacion['fono2']="000000";
            //2-> sla, 1 ->agenda
            if (trim($actuacion['envio_ofsc'])=='2' || 
                trim($actuacion['envio_ofsc'])=='3' || 
                trim($actuacion['envio_ofsc'])=='0'  ) {
                $actuacion['agdsla']='sla';
                $actuacion['hf'] = $date."||".$bucket."||AM|| - ";
            } else if (trim($actuacion['envio_ofsc'])=='1') {
                $actuacion['agdsla']='agenda';
                $actuacion['hf'] = $date."||".$bucket."||AM|| - ";
            }
            $actuacion['slaini']=date("Y-m-d", strtotime("+ 1 days"));

            Input::replace($actuacion);

            $this->_bandejaController->postEnvioofsc();

            Auth::logout();
            return 1;
        }
        return 0;
    }

    public function completarOfsc($respuesta)
    {
        $actuacion = $this->getGestion();
        $tecnico =  $this->getCarnetTmp();
        $location =  $this->getLocationCarnet();

        if (count($actuacion)>0) {
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=6;
            $actuacion['motivo']=3;
            $actuacion['submotivo']=12;
            $actuacion['noajax']='ok';
            $this->idsubmotivo=$this->getSubmotivoId($respuesta);
            $actuacion['motivo_ofsc_id']=$this->getMotivoId($respuesta);
            $actuacion['submotivo_ofsc_id']=$this->idsubmotivo;
            $actuacion['f_cierre']=$this->endtime;
            $actuacion['x_ofsc']=$location['x'];
            $actuacion['y_ofsc']=$location['y'];
            $actuacion['estado_ofsc_id']=6;
            $actuacion['resource_id']=$this->bucket;

            unset($actuacion["tecnico"]);
            unset($actuacion["tecnico_id"]);
            unset($actuacion["celula_id"]);
            unset($actuacion["celula"]);

            if (count($tecnico)>0) {
                $actuacion['tecnico']=$tecnico[0]->id;
                $actuacion['celula']=$tecnico[0]->celula_id;
            }

            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $this->_gestionMovimientoController->postRegistrar();
            Auth::logout();
            $actuacion['rst']=1;
            return $actuacion;
        }
        return ['rst'=>0];
    }

    public function cancelarOfsc($respuesta)
    {
        $actuacion = $this->getGestion();
        $tecnico =  $this->getCarnetTmp(); 
        if (count($actuacion)>0) {

            $actuacion['estado_agendamiento']="3-0";
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=4;
            $actuacion['motivo']=9;
            $actuacion['submotivo']=3;
            $actuacion['noajax']='ok';
            $actuacion['observacion2']='';
            if ( isset($respuesta['XA_NOTE'])) {
                $actuacion['observacion2']=$respuesta['XA_NOTE'];
            }
            $actuacion['motivo_ofsc_id']=$this->getMotivoId($respuesta);

            $actuacion['programado']=0;
            $actuacion['estado_ofsc_id']=5;
            $actuacion['aid']=$this->aid;
            $actuacion['resource_id']=$this->bucket;

            unset($actuacion["tecnico"]);
            unset($actuacion["tecnico_id"]);
            unset($actuacion["celula_id"]);
            unset($actuacion["celula"]);

            if (count($tecnico)>0) {
                $actuacion['tecnico']=$tecnico[0]->id;
                $actuacion['celula']=$tecnico[0]->celula_id;
            }

            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $save = $this->_gestionMovimientoController->postRegistrar();

            Auth::logout();
            return 1;
        }
        return 0;
    }

    public function iniciarOfsc($motivo)
    {
        $actuacion = $this->getGestion();
        $tecnico =  $this->getCarnetTmp();
        $location =  $this->getLocationCarnet();

        if (count($actuacion)>0) {
            $actuacion['estado_agendamiento']="3-0";
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=13;
            $actuacion['motivo']=7;
            $actuacion['submotivo']=11;
            $actuacion['noajax']='ok';
            $actuacion['resource_id']=$this->bucket;

            if (count($tecnico)>0) {
               $actuacion['tecnico']=$tecnico[0]->id;
               $actuacion['celula']=$tecnico[0]->celula_id;
            }
            $actuacion['noajax']='ok';
            $actuacion['fonos_contacto'] =$actuacion['fonos_contacto'];
            $actuacion['observacion2']=$motivo['A_COMMENT_TECHNICIAN'];
            $actuacion['f_inicio']=$this->starttime;
            $actuacion['x_ofsc']=$location['x'];
            $actuacion['y_ofsc']=$location['y'];

            $actuacion['aid']=$this->aid;
            $actuacion['programado']=1;
            $actuacion['estado_ofsc_id']=2;

            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $this->_gestionMovimientoController->postRegistrar();
            Auth::logout();
            //actualizar link en ofsc
            $activity = new Activity();
            $actividad['XA_ACTIVACION_EQ']=
                Config::get("wpsi.prueba.activacionrefresh").$actuacion["id"].
                '/'.$this->body->external_id;
            $actividad['XA_PRUEBAS_CABLEMODEM_LINKHTTP']=
                Config::get("wpsi.prueba.cablemodem");
            $activity->updateActivity($this->aid, $actividad);

            return 1;
        }
        return 0;
    }
    public function suspenderOfsc($respuesta)
    {
        $actuacion = $this->getGestion();
        $tecnico =  $this->getCarnetTmp();

        if (count($actuacion)>0) {

            $actuacion['estado_agendamiento']="3-0";
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=7;
            $actuacion['motivo']=2;
            $actuacion['submotivo']=18;
            $actuacion['noajax']='ok';
            $actuacion['motivo_ofsc_id']=$this->getMotivoId($respuesta);
            $actuacion['submotivo_ofsc_id']=$this->getSubmotivoId($respuesta);
            $actuacion['f_cierre']=$this->endtime;
            $actuacion['estado_ofsc_id']=3;
            $actuacion['resource_id']=$this->bucket;
            unset($actuacion["fecha_agenda"]);
            unset($actuacion["horario_id"]);
            unset($actuacion["dia_id"]);
            unset($actuacion["tecnico"]);
            unset($actuacion["tecnico_id"]);
            unset($actuacion["celula_id"]);
            unset($actuacion["celula"]);

            if (count($tecnico)>0) {
                $actuacion['tecnico']=$tecnico[0]->id;
                $actuacion['celula']=$tecnico[0]->celula_id;
            }
            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $save = $this->_gestionMovimientoController->postRegistrar();
            Auth::logout();
            return 1;
        }
        return 0;
    }

    public function norealizadoOfsc($respuesta)
    {
        $actuacion = $this->getGestion();
        $tecnico =  $this->getCarnetTmp();

        if (count($actuacion)>0) {
            $actuacion['estado_agendamiento']="3-0";
            $actuacion['gestion_id'] = $actuacion['id'];
            $actuacion['estado']=5;
            $actuacion['motivo']=4;
            $actuacion['submotivo']=6;
            $actuacion['noajax']='ok';
            $this->idmotivo=$this->getMotivoId($respuesta);
            $actuacion['motivo_ofsc_id']=$this->idmotivo;
            $actuacion['observacion2'] = '';
            $actuacion['resource_id']=$this->bucket;
            if ( isset($respuesta['XA_NOTE'])) {
                $actuacion['observacion2']=$respuesta['XA_NOTE'];
            }
            $actuacion['f_cierre']=$this->endtime;
            $actuacion['estado_ofsc_id']=4;

            unset($actuacion["fecha_agenda"]);
            unset($actuacion["horario_id"]);
            unset($actuacion["dia_id"]);
            unset($actuacion["tecnico"]);
            unset($actuacion["tecnico_id"]);
            unset($actuacion["celula"]);
            unset($actuacion["celula_id"]);

            if (count($tecnico)>0) {
                $actuacion['tecnico']=$tecnico[0]->id;
                $actuacion['celula']=$tecnico[0]->celula_id;
            }

            Auth::loginUsingId(952);
            Input::replace($actuacion);
            $this->_gestionMovimientoController->postRegistrar();
            Auth::logout();
            $actuacion["rst"] =1;
            return $actuacion;
        }
        return ['rst'=>0];
    }

    public function envioSmsOfsc()
    {
        //Sms::enviar('953669813', $this->text, '397');
        return 1;
        /*$datos=array();
        $datos['telefonoOrigen']=substr($this->number, '-9');
        $datos['mensaje']=$this->text;

        if (is_numeric($datos['telefonoOrigen']) &&
            strlen($datos['telefonoOrigen'])==9) {

            Auth::loginUsingId(952);
            Input::replace($datos);
            $save = $this->_apiController->postGetvalidacion();

            Auth::logout();
            return 1;
        }
        return 0;*/
    }

    public function getMotivoId($respuesta) 
    {
        $propiedades = PropiedadOfsc::get(['tipo'=>'M','estado'=>1]);

        $motivoId=null;

        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $rst = MotivoOfsc::get(
                    [ 'codigo_ofsc'=>trim($respuesta[$propiedad->label]) ]
                );
                if (count($rst)>0) {
                    $motivoId=$rst[0]->id;
                }
            }
        }

        return $motivoId;
    }

    public function getSubmotivoId($respuesta) 
    {
        $submotivoId=null;
        $propiedades = PropiedadOfsc::get(['tipo'=>'S','estado'=>1]);
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $rst = SubmotivoOfsc::get(
                    [ 'codigo_ofsc'=>trim($respuesta[$propiedad->label]) ]
                );
                if (count($rst)>0) {
                    $submotivoId=$rst[0]->id;
                }
            }
        }

        return $submotivoId;
    }

    public function getCarnetTmp()
    {
        return Tecnico::getTecnicotmp($this->bucket);
    }

    public function getLocationCarnet()
    {
        $xml = (array) simplexml_load_file( public_path().'/xml/tecnicos.xml');
        $xml = json_decode(json_encode(  $xml ), 1);
        $x = $y = 0;
        if (array_key_exists($this->bucket, $xml ) ) {
            if ( isset( $xml[$this->bucket]['x'] ))
                $x = $xml[$this->bucket]['x'];

            if ( isset( $xml[$this->bucket]['y'] ))
                $y = $xml[$this->bucket]['y'];
        }
        return ['x'=>$x , 'y'=>$y];
    }

    public function getBody()
    {
        return $this->body;
    }
    public  function setBody($body)
    {
        $this->body=$body;
    }

    public function getSubject()
    {
        return $this->subject;
    }
    public  function setSubject($subject)
    {
        $this->subject=$subject;
    }
    public function getCodactu()
    {
        return $this->codactu;
    }
    public  function setCodactu($codactu)
    {
        $this->codactu=$codactu;
    }

    public function getBucket()
    {
        return $this->bucket;
    }
    public  function setBucket($bucket)
    {
       $this->bucket=$bucket;
    }

    public function getData()
    {
        return $this->data;
    }
    public  function setData($data)
    {
        $this->data=$data;
    }

    public function getAid()
    {
        return $this->aid;
    }
    public  function setAid($aid)
    {
        $this->aid=$aid;
    }
}