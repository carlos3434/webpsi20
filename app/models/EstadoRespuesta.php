<?php
use Illuminate\Support\Collection;
use Ofsc\Outbound;
use Ofsc\Inbound;
// use Legados\helpers\ActivityHelper;
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaGestion;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as Detalle;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\controllers\BandejaLegadoController;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\TipoAutorizacion as TipoAutorizacion;
use Legados\CableModem;
use Legados\models\ComponenteOperacion;
use Legados\Repositories\StGestionRepository as StGestion;
use Services\models\Outbound as OutboundService;
use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;

class EstadoRespuesta
{
    protected $_bandejaLegadoController;

    private $body;
    private $subject;
    private $codactu;
    private $bucket;
    private $data;
    private $aid;
    private $idmotivo;
    private $idsubmotivo;
    private $starttime;
    private $endtime;
    private $enddate;
    private $number;
    private $text;
    private $st;
    private $numOrdenTrabajo;
    private $numOrdenServicio;

    public function __construct(BandejaLegadoController $bandejaLegadoController)
    {
        $this->_bandejaLegadoController = $bandejaLegadoController;
    }

    public function recibir($data)
    {

        $this->setData($data);
        $body = str_replace(["<![CDATA[","]]>"], "", $data['body']);
        if (Helpers::isValidXml($body)) {
            $this->setBody(simplexml_load_string($body));
        } else {
            $this->setBody("");
        }
        //capturando la fecha de envio mensaje

     
        $date = new DateTime(date("Y-m-d"));
        $hoy = $date->format('Y-m-d');

        $this->setSubject(strtoupper(trim($data['subject'])));

        if ($this->subject=='TOA_OUTBOUND_NOTDONE' ||
            $this->subject=='TOA_OUTBOUND_SUSPEND' ||
            $this->subject=='TOA_OUTBOUND_PRE_NOTDONE') {
            if (isset($this->body->activity_properties->appt_number)) {
                $this->codactu =
                    trim($this->body->activity_properties->appt_number);
            }
            if (isset($this->body->activity_properties->external_id)) {
                $this->bucket =
                    trim($this->body->activity_properties->external_id);
            }
            //armando el datetime cierre
            if (isset($this->body->activity_properties->ETA)) {
                $this->starttime = $hoy.' '.trim($this->body->activity_properties->ETA);
            }
            if (isset($this->body->activity_properties->eta_end_time)) {
                $this->endtime = $this->body->activity_properties->eta_end_time;
            }
            if (isset($this->body->activity_properties->date)) {
                $this->enddate = $this->body->activity_properties->date;
            }
        } elseif ($this->subject == 'UNSCHEDULE' ||
                   $this->subject == 'TOA_OUTBOUND_COMPLETED' ||
                   $this->subject == 'WO_INIT' ||
                   $this->subject == 'WO_CANCEL' ||
                   $this->subject == 'WO_PRE_COMPLETED' ||
                   $this->subject == 'UNSCHEDULE_START') {
            if (isset($this->body->appt_number)) {
                $this->codactu= trim(strtoupper($this->body->appt_number));
            }
            if (isset($this->body->external_id)) {
                $this->bucket= trim(strtoupper($this->body->external_id));
            }
            //armando el datetime incio
            if (isset($this->body->eta_start_time)) {
                $this->starttime= $hoy.' '.trim($this->body->eta_start_time);
            }
            //armando el datetime cierre
            if (isset($this->body->eta_end_time)) {
                $this->endtime = $this->body->eta_end_time;
            }
            if (isset($this->body->date)) {
                $this->enddate= $this->body->date;
            }
        } elseif ($this->subject == 'INT_SMS') {
            if (isset($this->body->number)) {
                $this->number= trim($this->body->number);
            }
            if (isset($this->body->text)) {
                $this->text= $this->body->text ;
            }
            return;
        } elseif ($this->subject=='TOA_VALIDATE_COORDS') {
            if (isset($this->body->validation_properties->aid)) {
                $this->setAid(trim($this->body->validation_properties->aid));
            }
            if (isset($this->body->appt_number)) {
                $this->codactu= trim(strtoupper($this->body->appt_number));
            }
            if (isset($this->body->external_id)) {
                $this->bucket= trim(strtoupper($this->body->external_id));
            }
        } elseif ($this->subject=='NOT_LEG' ||
                  $this->subject=='PRUEBA' ||
                  $this->subject=='TOA_OUTBOUND_AUT_SUSPEND' ||
                  $this->subject=='TOA_OUTBOUND_AUT_COORDENADAS' ||
                  $this->subject=='TOA_OUTBOUND_AUT_TEST_MODEM' ||
                  $this->subject=='TOA_OUTBOUND_AUT_TEST_MODEM' ||
                  $this->subject=='WO_PRE_CLOSEHOME') {
            if (isset($this->body->external_id)) {
                $this->setBucket(trim($this->body->external_id));
            }
            if (isset($this->body->appt_number)) {
                $this->setCodactu(trim(strtoupper($this->body->appt_number)));
            }
        } else if ($this->subject == 'TOA_ACTIVATE_ROUTE') {
            if (isset($this->body->external_id)) {
                $this->setBucket(trim($this->body->external_id));
            }
            if (isset($this->body->r_activate_route_time)) {
                $this->setActivateRouteTime(trim($this->body->r_activate_route_time));
            }
        }

        if (isset($this->body->aid)) {
            $this->setAid(trim($this->body->aid));
        } elseif (isset($this->body->activity_properties->aid)) {
            $this->setAid(trim($this->body->activity_properties->aid));
        }
        if (isset($this->body->xa_identificador_st)) {
            $this->setSt(trim($this->body->xa_identificador_st));
        } elseif (isset($this->body->activity_properties->xa_identificador_st)) {
            $this->setSt(trim($this->body->activity_properties->xa_identificador_st));
        } elseif ($this->body->IDENTIFICADOR_ST) {
            $this->setSt(trim($this->body->IDENTIFICADOR_ST));
        }

        if (isset($this->body->XA_NUMBER_WORK_ORDER)) {
            $this->setNumOrdenTrabajo(trim($this->body->XA_NUMBER_WORK_ORDER));
            if (is_null($this->body->XA_NUMBER_WORK_ORDER) ||
                $this->body->XA_NUMBER_WORK_ORDER == "") {
                $this->setNumOrdenTrabajo("");
            }
        } elseif (isset($this->body->activity_properties->XA_NUMBER_WORK_ORDER)) {
            $this->setNumOrdenTrabajo(trim($this->body->activity_properties->XA_NUMBER_WORK_ORDER));
            if (is_null($this->body->activity_properties->XA_NUMBER_WORK_ORDER) ||
                $this->body->activity_properties->XA_NUMBER_WORK_ORDER == "") {
                $this->setNumOrdenTrabajo("");
            }
        }

        if (isset($this->body->XA_NUMBER_SERVICE_ORDER)) {
            $this->setNumOrdenServicio(trim($this->body->XA_NUMBER_SERVICE_ORDER));
            if (is_null($this->body->XA_NUMBER_SERVICE_ORDER) ||
                $this->body->XA_NUMBER_SERVICE_ORDER == "") {
                $this->setNumOrdenServicio("");
            }
        } elseif (isset($this->body->activity_properties->XA_NUMBER_SERVICE_ORDER)) {
            $this->setNumOrdenServicio(trim($this->body->activity_properties->XA_NUMBER_SERVICE_ORDER));
            if (is_null($this->body->activity_properties->XA_NUMBER_SERVICE_ORDER) ||
                $this->body->activity_properties->XA_NUMBER_SERVICE_ORDER == "") {
                $this->setNumOrdenServicio("");
            }
        }

        if (isset($this->body->time_authorization)) {
            $this->setTimeAutorization($this->body->time_authorization);
        }
        if (isset($this->body->user_name)) {
            $this->setUserAutorization($this->body->user_name);
        }
    }
    public function responder()
    {
        $date = date("Y-m-d");
        $body = $this->body;
        $respuesta =array();
        $inventario =array();
        switch ($this->subject) {
            case 'TOA_OUTBOUND_COMPLETED':
                $objServiceCompletar = new Services\models\Outbound\Completar;
                $objServiceCompletar->recibir($this->getData());
                $objServiceCompletar->procesar(); 
                break;
            case 'TOA_OUTBOUND_PRE_NOTDONE':
                $objServicePreDevolver = new Services\models\Outbound\PreDevolver;
                $objServicePreDevolver->recibir($this->getData());
                $actuacion = $objServicePreDevolver->procesar();
                if ($actuacion["rst"] == 1 && Config::get("toolbox.switch") == 1) {
                    $objServicePreDevolver->toolbox($actuacion);
                }
                break;
            case 'WO_PRE_COMPLETED':
                $objPreCompletarService = new Services\models\Outbound\PreCompletar;
                $objPreCompletarService->recibir($this->getData());
                $actuacion = $objPreCompletarService->procesar();
                if (Config::get("toolbox.switch") == 1 && $actuacion["rst"] == 1) {
                    $objPreCompletarService->toolbox($actuacion);
                }
                break;
            case 'TOA_OUTBOUND_NOTDONE':
                $objServiceNoRealizado = new Services\models\Outbound\NoRealizada;
                $objServiceNoRealizado->recibir($this->getData());
                $objServiceNoRealizado->procesar();               
                break;
            case 'WO_CANCEL':
                $objServiceCancelar = new Services\models\Outbound\Cancelar;
                $objServiceCancelar->recibir($this->getData());
                $objServiceCancelar->procesar();
                break;
            case 'WO_INIT':
                $objServiceIniciar = new Services\models\Outbound\Iniciar;
                $objServiceIniciar->recibir($this->getData());
                $objServiceIniciar->procesar();
                break;
            case 'TOA_VALIDATE_COORDS':
                $objServiceValCoords = new Services\models\Outbound\ValidarCoordenadas;
                $objServiceValCoords->recibir($this->getData());
                $objServiceValCoords->procesar();
                break;
            case 'NOT_LEG':
                $objAsignarTecnicoService = new Services\models\Outbound\AsignarTecnico;
                $objAsignarTecnicoService->recibir($this->getData());
                $objAsignarTecnicoService->procesar();
                break;
            case 'PRUEBA':
                //filtrar pruebas por quiebre
                //consultar pruebas cable modem
                $rst = $this->getPruebasCableModem();
                break;
            case 'TOA_ACTIVATE_ROUTE':
                $objServiceActRoute = new Services\models\Outbound\ActivacionRuta;
                $objServiceActRoute->recibir($this->getData());
                $objServiceActRoute->procesar();
                break;
            case 'TOA_OUTBOUND_SUSPEND':
                $objSuspenderService = new Services\models\Outbound\Suspender;
                $objSuspenderService->recibir($this->getData());
                $objSuspenderService->procesar();
                break;
            case 'TOA_OUTBOUND_AUT_SUSPEND':
            case 'TOA_OUTBOUND_AUT_COORDENADAS':
            case 'TOA_OUTBOUND_AUT_TEST_MODEM':
            case 'TOA_OUTBOUND_AUT_COMPLETED':
                $objSuspenderService = new Services\models\Outbound\AutorizacionSolicitud;
                $objSuspenderService->recibir($this->getData());
                $objSuspenderService->procesar();
                break;
            case 'UNSCHEDULE':
                $objDesprogramarService = new Services\models\Outbound\DesProgramar;
                $objDesprogramarService->recibir($this->getData());
                $objDesprogramarService->procesar();
                break;
            case 'UNSCHEDULE_START':
                $rst=$this->desprogramarIniciarOfsc();
                break;
            case 'INT_SMS':
                $objSmsService = new Services\models\Outbound\Sms;
                $objSmsService->recibir($this->getData());
                $objSmsService->procesar();
                break;
            case 'WO_PRE_CLOSEHOME':
                $objCasaCerradaService = new Services\models\Outbound\PreCasaCerrada;
                $objCasaCerradaService->recibir($this->getData());
                $objCasaCerradaService->procesar();
        }
        return true;
    }

    /**
     * valida la distancia entrea las coordenadas de tecnico y actividad
     * retorna 2 cuando el tecnico esta dentro de la distancia permitida
     * retorna 1 en caso contrario
     */
    /*public function validarDistanciaOfsc()
    {
        $location=array('x'=>'','y'=>'');
        if (isset($this->body->validation_properties->appt_coordinates->acoord_x)) {
            $this->acoord_x =
                trim($this->body->validation_properties->appt_coordinates->acoord_x);
        }
        if (isset($this->body->validation_properties->appt_coordinates->acoord_y)) {
            $this->acoord_y =
                trim($this->body->validation_properties->appt_coordinates->acoord_y);
        }

        if (isset($this->body->validation_properties->resource_coordinates)) {
            $Coordtecnico = $this->body->validation_properties->resource_coordinates;
            $Coordtecnico = str_replace(["lat:", "lng:"], "", trim($Coordtecnico));
            $array= explode(",", $Coordtecnico);
            if (count($array)==2) {
                $location['x']=$array['1'];
                $location['y']=$array['0'];
            }
        }

        if (isset($this->acoord_x) && $this->acoord_x !='' && isset($this->acoord_y) && $this->acoord_y !='' && $location['x']!='' && $location['y']!='' ) {

            $data=[
               'coord_x1'=>$this->acoord_x, //coord de actividad
               'coord_y1'=>$this->acoord_y, //coord de actividad lat 1
               'coord_x2'=> $location['x'], // coord de tecnicos
               'coord_y2'=> $location['y'] //coord de tecnico lat 2
            ];
            
            $respuesta = GeoValidacion::getValidadistancia($data);
            if ($respuesta['rpta']==1) {
                //coordenadas validadas
                $properties["A_ESTADO_VALIDACION_COORDENADAS"]='2';
                $mensaje = "Req: $this->codactu : Coordenadas OK";
            } else {
                //coordenadas invalidas
                $properties["A_ESTADO_VALIDACION_COORDENADAS"]='1';
                $mensaje = 'Req: '.$this->codactu.' : Coordenadas incorrectas. A '.$respuesta['metros'].' metros del Req';
            }
            
            $inbound = new Inbound();
            $inbound->updateActivity($this->codactu, $properties);

            $tecnico =  $this->getCarnetTmp();
            if ($tecnico) {
                Sms::enviarSincrono($tecnico->celular, $mensaje, '1', '');
            }
            //insertar registro de transaccion
            $movimiento = Movimiento::where('aid', $this->aid)->latest()->first();

            if ($movimiento) {
                $detalle = new Detalle();
                $detalle['coordx_tecnico']        = $data['coord_x2'];
                $detalle['coordy_tecnico']        = $data['coord_y2'];
                $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                $detalle['tipo']                  = 1; // validacion de cordenadas
                $detalle['distancia']             = $respuesta['metros'];
                $detalle['sms']                   = $mensaje;
                Auth::loginUsingId(952);
                $movimiento->detalle()->save($detalle);
                Auth::logout();
            }
            return 1;
        }
        return 2;
    }*/

    public function validarDistanciaOfsc()
    {
        $solicitudTecnica=SolicitudTecnicaCms::getUltimoCms()
                ->where('solicitud_tecnica_cms.num_requerimiento', $this->codactu)
                ->where('u.aid', $this->aid)
                ->first();

        if (is_null($solicitudTecnica)) {
            return 2;
        }

        $location=array('x'=>'','y'=>'');
        if (isset($this->body->validation_properties->appt_coordinates->acoord_x)) {
            $this->acoord_x =
                trim($this->body->validation_properties->appt_coordinates->acoord_x);
        }
        if (isset($this->body->validation_properties->appt_coordinates->acoord_y)) {
            $this->acoord_y =
                trim($this->body->validation_properties->appt_coordinates->acoord_y);
        }

        if (isset($this->body->validation_properties->resource_coordinates)) {
            $Coordtecnico = $this->body->validation_properties->resource_coordinates;
            $Coordtecnico = str_replace(["lat:", "lng:"], "", trim($Coordtecnico));
            $array= explode(",", $Coordtecnico);
            if (count($array)==2) {
                $location['x']=$array['1'];
                $location['y']=$array['0'];
            }
        }

        if ($solicitudTecnica->actividad_id==2) {// provision
            if ($location['x']!='' && $location['y']!='') {
                $solicitudTecnicaCms= SolicitudTecnicaCms::where('solicitud_tecnica_id', $solicitudTecnica->solicitud_tecnica_id)->first();
                $solicitudTecnicaCms['coordx_cliente'] = $location['x'];
                $solicitudTecnicaCms['coordy_cliente'] = $location['y'];
                $solicitudTecnicaCms->save();
    
                 $movimiento = Movimiento::where('solicitud_tecnica_id', $solicitudTecnica->solicitud_tecnica_id)
                ->latest()->first();
        
                Auth::loginUsingId(952);
                if ($movimiento) {
                    $detalle = new Detalle();
                    $detalle['coordx_tecnico']        = $location['x'];
                    $detalle['coordy_tecnico']        = $location['y'];
                    $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                    $detalle['tipo']                  = 9; //guardar de cordenadas
                    $detalle['sms']                   = 'Coordenadas Actualizadas Correctamente';
                    $movimiento->detalle()->save($detalle);
                }
                Auth::logout();
                return 1;
            }
        } else {
            if (isset($this->acoord_x) && $this->acoord_x !='' && isset($this->acoord_y) && $this->acoord_y !='' && $location['x']!='' && $location['y']!='') {
                $data=[
                'coord_x1'=>$this->acoord_x, //coord de actividad
                'coord_y1'=>$this->acoord_y, //coord de actividad lat 1
                'coord_x2'=> $location['x'], // coord de tecnicos
                'coord_y2'=> $location['y'] //coord de tecnico lat 2
                ];
                
                $respuesta = GeoValidacion::getValidadistancia($data);

                if ($respuesta['rpta']==1) {
                    //coordenadas validadas
                    $properties["A_ESTADO_VALIDACION_COORDENADAS"]='2';
                    $mensaje = "Req: $this->codactu : Coordenadas OK";
                } else {
                    //coordenadas invalidas
                    $properties["A_ESTADO_VALIDACION_COORDENADAS"]='1';
                    $mensaje = 'Req: '.$this->codactu.' : Coordenadas incorrectas. A '.$respuesta['metros'].' metros del Req';
                }
                
                $inbound = new Inbound();
                $inbound->updateActivity($this->codactu, $properties);
    
                $tecnico =  $this->getCarnetTmp();
                if ($tecnico) {
                    Sms::enviarSincrono($tecnico->celular, $mensaje, '1', '');
                }
                //insertar registro de transaccion
                $movimiento = Movimiento::where('aid', $this->aid)->latest()->first();
    
                if ($movimiento) {
                    $detalle = new Detalle();
                    $detalle['coordx_tecnico']        = $data['coord_x2'];
                    $detalle['coordy_tecnico']        = $data['coord_y2'];
                    $detalle['tecnico_id']            = $movimiento['tecnico_id'];
                    $detalle['tipo']                  = 1; // validacion de cordenadas
                    $detalle['distancia']             = $respuesta['metros'];
                    $detalle['sms']                   = $mensaje;
                    Auth::loginUsingId(952);
                    $movimiento->detalle()->save($detalle);
                    Auth::logout();
                }
                return 1;
            }
        }

        return 2;
    }
    public function completarOfsc($respuesta, $inventario = [])
    {

        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $respuesta['estado_ofsc_id'] = 6;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        $objmotivoofsc = $this->getMotivo($respuesta);
        $objsubmotivoofsc = $this->getSubmotivo($respuesta, $objmotivoofsc->id);
        $tecnico =  $this->getCarnetTmp();

        $actuacion = array(
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ?
                                       $respuesta['A_OBSERVATION']:'',
            'a_receive_person_id'   => isset($respuesta['A_RECEIVE_PERSON_ID']) ?
                                       $respuesta['A_RECEIVE_PERSON_ID'] : '',
            'a_receive_person_name' => isset($respuesta['A_RECEIVE_PERSON_NAME']) ?
                                       $respuesta['A_RECEIVE_PERSON_NAME'] : '',
            'estado_ofsc_id'        => 6,
            'motivo_ofsc_id'        => $objmotivoofsc->id,
            'submotivo_ofsc_id'     => $objsubmotivoofsc->id,
            'end_time'              => $this->endtime,
            'end_date'              => $this->enddate,
            'estado_aseguramiento'  => 6,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";

            $this->updateMateriales($inventario, $tecnico->id);
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    // if ($ultimo->actividad_id == 1) {
                        $solicitud = [
                            'valor1' => Config::get("legado.area_transferencia"),
                            'valor2' => $objmotivoofsc->codigo_legado,
                            'valor3' => $objsubmotivoofsc->codigo_legado,
                        ];
                        $this->setAveriaComponentes($solicitud);
                    /*} elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }*/
                        if ($ultimo->actividad_id == 2) {
                            if ($ultimo->estado_ofsc_id == 6) {
                                if ($ultimo->estado_aseguramiento == 5) {
                                    $actuacion['rst'] = 1;
                                    return $actuacion;
                                }
                            }
                        }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }

            // ActivityHelper::getImagenOfsc($ultimo->aid);
        }

        if ($ultimo->estado_aseguramiento != 6) {
            try {
                Auth::loginUsingId(952);
                $objgestion = new StGestion;
                $objgestion->registrarMovimiento($solicitudTecnica, $actuacion, []);

                switch ($ultimo->tipo_legado) {
                    case '1':
                        $solicitudTecnicaCms = $solicitudTecnica->cms;
                        foreach (SolicitudTecnicaCms::$rules as $key => $value) {
                            if (array_key_exists($key, $solicitud)) {
                                $solicitudTecnicaCms[$key] = $solicitud[$key];
                            }
                        }
                        $solicitudTecnica->cms()->save($solicitudTecnicaCms);
                        break;
                    case '2':
                        if ($ultimo->actividad_id == 1) {
                            $solicitudTecnicaGestelAveria = $solicitudTecnica->gestelAveria;
                            foreach (SolicitudTecnicaGestelAveria::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelAveria[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelAveria->save();
                            break;
                        } elseif ($ultimo->actividad_id == 2) {
                            $solicitudTecnicaGestelProvision = $solicitudTecnica->gestelProvision;
                            foreach (SolicitudTecnicaGestelProvision::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelProvision[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelProvision->save();
                            break;
                        }
                        break;
                }

                Auth::logout();
                $actuacion['rst'] = 1;
            } catch (Exception $e) {
                $actuacion['rst'] = 0;
            }
        } else {
            $actuacion['rst'] = 1;
            $actuacion['automatico'] = 0;
        }
        return $actuacion;
    }

    public function norealizadoOfsc($respuesta, $inventario = [])
    {
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }
        $respuesta['estado_ofsc_id'] = 4;
        $respuesta['tipo_legado'] = $ultimo->tipo_legado;
        $respuesta['actividad_id'] = $ultimo->actividad_id;
        $tecnico =  $this->getCarnetTmp();
        $objmotivoofsc = $this->getMotivo($respuesta);
        $objsubmotivoofsc = $this->getSubmotivo($respuesta, $objmotivoofsc->id);

        $estado_aseguramiento = $ultimo->estado_aseguramiento == 7 ? 7 : 6;
        $actuacion = array(
            'xa_observation'        => isset($respuesta['A_OBSERVATION']) ? $respuesta['A_OBSERVATION'] : '',
            'motivo_ofsc_id'        => $objmotivoofsc->id,
            'submotivo_ofsc_id'     => $objsubmotivoofsc->id,
            'estado_ofsc_id'        => 4,
            'end_time'              => $this->endtime,
            'end_date'              => $this->enddate,
            'aid'                   => $this->aid,
            'estado_aseguramiento'  => $estado_aseguramiento,
            'tipo_legado'           => $ultimo->tipo_legado,
            'actividad_id'          => $ultimo->actividad_id,
            'solicitud_tecnica_id'  => $ultimo->solicitud_tecnica_id
        );

        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
            $actuacion['coordx_tecnico'] = $tecnico->coord_x;
            $actuacion['coordy_tecnico'] = $tecnico->coord_y;
            $actuacion['celular'] = isset($tecnico->celular) ? $tecnico->celular : "";
            $this->updateMateriales($inventario, $tecnico->id);
        }

        $solicitud = [];
        if (count($ultimo) > 0) {
            switch ($ultimo->tipo_legado) {
                case '1':
                    if ($ultimo->actividad_id == 1) {
                        if ($objmotivoofsc->id != 454) { // diferente de OTROS
                            $where = [
                                'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id : null,
                                'actividad_id'          => $ultimo->actividad_id,
                                'tipo_legado'           => $ultimo->tipo_legado,
                                'area_transferencia'    => $objmotivoofsc->codigo_legado,
                                'estado'                => 1
                            ];
                            $transferencia = $this->getContrataTransferencia($where);
                            $solicitud = [
                                'codigo_contrata' => isset($transferencia->contrata_transferencia)? $transferencia->contrata_transferencia : "",
                                'valor1' => $objmotivoofsc->codigo_legado,
                            ];
                        }
                    } elseif ($ultimo->actividad_id == 2) {
                        $where = [
                            'empresa_id'            => (!is_null($tecnico)) ? $tecnico->empresa_id: null,
                            'actividad_id'          => $ultimo->actividad_id,
                            'tipo_legado'           => $ultimo->tipo_legado,
                            'area_transferencia'    => $objmotivoofsc->codigo_legado,
                            'estado'                => 1
                        ];
                        $codigoContrata = "";
                        $descripcionContrata = "";
                        if (!is_null($tecnico)) {
                            $objempresa = Empresa::find($tecnico->empresa_id);
                            if (!is_null($objempresa)) {
                                $codigoContrata = $objempresa->cms;
                                $descripcionContrata = $objempresa->nombre;
                            }
                        }
                        //$transferencia = $this->getContrataTransferencia($where);
                        $solicitud = [
                            'codigo_contrata' => $codigoContrata,
                            'desc_contrata' => $descripcionContrata,
                            'valor1' => $objmotivoofsc->codigo_legado,
                        ];
                    }
                    break;
                case '2':
                    if ($ultimo->actividad_id == 1) {
                        # code...
                    } elseif ($ultimo->actividad_id == 2) {
                        # code...
                    }
                    break;
            }
            // ActivityHelper::getImagenOfsc($ultimo->aid);
        }

        if ($ultimo->estado_aseguramiento != 5 && $ultimo->estado_aseguramiento != 6) {
            Auth::loginUsingId(952);
            try {
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimo[$key] = $actuacion[$key];
                    }
                }
                $solicitudTecnica->ultimo()->save($ultimo);
                $objmovimientoreciente = $solicitudTecnica->ultimoMovimiento;
                if (!is_null($objmovimientoreciente)) {
                    $ultimoMovimiento = $objmovimientoreciente->replicate();
                    foreach (Movimiento::$rules as $key => $value) {
                        if (array_key_exists($key, $actuacion)) {
                            $ultimoMovimiento[$key] = $actuacion[$key];
                        }
                    }
                    $ultimoMovimiento->save();
                }

                switch ($ultimo->tipo_legado) {
                    case '1':
                        $solicitudTecnicaCms = $solicitudTecnica->cms;
                        foreach (SolicitudTecnicaCms::$rules as $key => $value) {
                            if (array_key_exists($key, $solicitud)) {
                                $solicitudTecnicaCms[$key] = $solicitud[$key];
                            }
                        }
                        $solicitudTecnica->cms()->save($solicitudTecnicaCms);
                        break;
                    case '2':
                        if ($ultimo->actividad_id == 1) {
                            $solicitudTecnicaGestelAveria = $solicitudTecnica->gestelAveria;
                            foreach (SolicitudTecnicaGestelAveria::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelAveria[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelAveria->save();
                            break;
                        } elseif ($ultimo->actividad_id == 2) {
                            $solicitudTecnicaGestelProvision = $solicitudTecnica->gestelProvision;
                            foreach (SolicitudTecnicaGestelProvision::$rules as $key => $value) {
                                if (array_key_exists($key, $solicitud)) {
                                    $solicitudTecnicaGestelProvision[$key] = $solicitud[$key];
                                }
                            }
                            $solicitudTecnicaGestelProvision->save();
                            break;
                        }
                        break;
                }

                $actuacion['rst'] = 1;
            } catch (Exception $e) {
                $actuacion['rst'] = 0;
            }
            Auth::logout();
        } else {
            $actuacion['rst'] = 1;
            $actuacion['automatico'] = 0;
        }
        
        return $actuacion;
    }
    public function desprogramarIniciarOfsc()
    {
        return 1;
        $tecnico =  $this->getCarnetTmp();
        $solicitudTecnica = $this->getSolicitudTecnica();
        if (count($solicitudTecnica) == 0) {
            return $actuacion['rst'] = 0;
        }
        $ultimo = $solicitudTecnica->ultimo;
        if (count($ultimo) == 0) {
            return $actuacion['rst'] = 0;
        }

        $actuacion = array(
            'estado_ofsc_id'    => 9,
            'resource_id'       => $this->bucket,
            'tipo_legado'       => $ultimo->tipo_legado,
            'actividad_id'      => $ultimo->actividad_id
        );
        if ($tecnico) {
            $actuacion['tecnico_id'] = $tecnico->id;
        }
        Auth::loginUsingId(952);
        try {
            $inbound = new Inbound();
            $responseToa = $inbound->updateActivity($ultimo->num_requerimiento, [], [], "cancel_activity");
            if (isset($responseToa->result) && $responseToa->result=="success") {
                $actuacion["estado_ofsc_id"] = 4;
            } else {
                $actuacion["xa_observation"] = $ultimo->xa_observation."| Error ".$responseToa->description;
            }
            $ultimo = $solicitudTecnica->ultimo;
            foreach (Ultimo::$rules as $key => $value) {
                if (array_key_exists($key, $actuacion)) {
                    $ultimo[$key] = $actuacion[$key];
                }
            }
            $solicitudTecnica->ultimo()->save($ultimo);

            $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
            foreach (Movimiento::$rules as $key => $value) {
                if (array_key_exists($key, $actuacion)) {
                    $ultimoMovimiento[$key] = $actuacion[$key];
                }
            }
            $ultimoMovimiento->save();

            $actuacion['rst'] = 1;
        } catch (Exception $e) {
            $actuacion['rst'] = 0;
        }
        if ($ultimo==null) {
            return 1;
        }
        $actuacion['actividad_id']=$ultimo->actividad_id;
        $actuacion['actividad_tipo_id']=$ultimo->actividad_tipo_id;
        $quiebre = Quiebre::find($ultimo->quiebre_id);
        $actuacion['quiebre']=(!is_null($quiebre))? $quiebre->apocope : ""; //quiebre apocope
        $actuacion['envio_ofsc']=$ultimo->tipo_envio_ofsc;
        $actuacion['nenvio']=1;


        if ($ultimo->tipo_legado==1) {//cms
            // $detallesolicitud = SolicitudTecnicaCms::where("solicitud_tecnica_id", $ultimo->solicitud_tecnica_id)
            //                     ->first();
            $detallesolicitud = $solicitudTecnica->cms;
            if ($detallesolicitud->cod_troba!='') {
                $actuacion['fftt'] = $detallesolicitud->cod_nodo."|".$detallesolicitud->cod_troba;
            } else {
                $actuacion['fftt'] = $detallesolicitud->cod_nodo."|".$detallesolicitud->cod_plano;
            }
            $actuacion['solicitud']=$detallesolicitud->id_solicitud_tecnica;
        } else {
            $actuacion['fftt']=null;
        }

        if (trim($actuacion['envio_ofsc'])=='1') {
            return 1;
        }


        $date = date("Y-m-d", strtotime("+ 1 days"));
        $bucket= '';
        $bucket=$this->consultarCapacidad($actuacion, $date);

        if ($bucket == '') {
            $bucket = $this->bucket;
        }

        //2-> sla, 1 ->agenda
        if (trim($ultimo->tipo_envio_ofsc)=='2') {
            $actuacion['agdsla']='sla';
            $actuacion['hf'] = $date."||".$bucket."||AM|| - ";
        } else if (trim($actuacion['envio_ofsc'])=='1') {
            $actuacion['agdsla']='agenda';
            $actuacion['hf'] = $date."||".$bucket."||AM||09-13";
        }
        $actuacion['quiebre']=$ultimo->quiebre_id; //quiebre_id
        $actuacion['solicitud_tecnica_id']=$ultimo->solicitud_tecnica_id;
        Input::replace($actuacion);
        //Auth::loginUsingId(952);
        $this->_bandejaLegadoController->postEnvioofsc();
        Auth::logout();
        return 1;
    }
    /**
     * pruebas cable modem
     */
    public function getPruebasCableModem()
    {
        $tecnico =  $this->getCarnetTmp();
        $solicitudActiva = $this->getSolicitudTecnica();
        if (count($solicitudActiva) == 0) {
            return $actuacion['rst'] = 0;
        }
        $celular = '953669813';
        $codCliente = "";
        $codServicio = "";
        $objsolicitud = null;
        if ($tecnico) {
            $celular = $tecnico->celular;
        }

        if ($solicitudActiva->tipo_legado == 1) {
            $objsolicitud = $solicitudActiva->cms;
            if (!is_null($objsolicitud)) {
                $codCliente = $objsolicitud->cod_cliente;
                $codServicio = $objsolicitud->cod_servicio;
            }
        }
        if ($solicitudActiva->tipo_legado == 2) {
            if ($solicitudActiva->actividad_id == 1) {
                $objsolicitud = $solicitudActiva->gestelAveria;
            }
            if ($solicitudActiva->actividad_id == 2) {
                $objsolicitud = $solicitudActiva->gestelProvision;
            }
        }
        //buscar mac de modem: por codcli y cod_serv
        //$codactu='45611239';
        //$solicitudCms = SolicitudTecnicaCms::where('num_requerimiento',$codactu)->first();

        if (is_null($objsolicitud)) {
            $mensaje="Req:".$this->codactu." no se encontro requerimiento";
            Sms::enviarSincrono($celular, $mensaje, '1', '');
            return ['rst'=>1];
        }
        
        $sql= "SELECT mac2 FROM status_movistarx WHERE idclientecrm=? AND codserv=? ";
        //$modem = DB::select($sql,[$this->codactu]);
        $col = new Collection(DB::select($sql, [$codCliente, $codServicio]));
        if (!$col->first()) {
            $mensaje="Req:".$this->codactu." no se encontro mac asociado";
            Sms::enviarSincrono($celular, $mensaje, '1', '');
            return ['rst'=>1];
        }

        
        //obtener pruebas
        $api = new CableModem();
        $response = $api->getPruebas($col->first()->mac2);
        /*
        0#USPWR|53.8|DSPWR|-12.7|USSNR|27.7|DSSNR|37.5#rf-error
        */
        //$response="0#USPWR|53.8|DSPWR|-12.7|USSNR|27.7|DSSNR|37.5#rf-error";
        list($error,$mediciones) = explode('#', $response);

        $datos = explode('|', $mediciones);

        $pwr = [
            'USPWR' => ['min' => 37.0,'max' => 55.0],
            'DSPWR' => ['min' => -5.0,'max' => 10.0],
            'USSNR' => ['min' => 27.0,'max' => 99],
            'DSSNR' => ['min' => 30.0,'max' => 99]
        ];
        $i=$j=0;
        foreach ($datos as $key => $value) {
            if ($key==1 || $key==3 || $key==5 || $key==7) {
                continue;
            }
            $valor = (float) $datos[$key+1];

            if ($pwr[$value]['min'] <= $valor && $valor <= $pwr[$value]['max']) {
                $pwr[$value]['prueba'] = true;
                $i++;
            } elseif ($valor=='') {
                $pwr[$value]['prueba'] = '';
                $j++;
            } else {
                $pwr[$value]['prueba'] = false;
            }

        }
        $inbound = new Inbound();

        if ($i==4) {
            // actualizar propiedad en toa, para que el tenico pueda completar la orden
            $properties["A_IND_PRUEBA"]='2';//ok, mostrar boton completar
            $inbound->updateActivity($this->codactu, $properties);
            
            //enviar mensaje al tecnico que se verifico correctamente
            $mensaje="Req:".$this->codactu." pruebas de cable ok";
            Sms::enviarSincrono($celular, $mensaje, '1', '');
            return ['rst'=>1];
        }
        if ($j==4) {
            //enviar mensaje al tecnico que la mac no tiene resultado de pruebas
            $mensaje="Req:".$this->codactu." pruebas de cable modem vacias";
            Sms::enviarSincrono($celular, $mensaje, '1', '');
            return ['rst'=>1];
        }
        $properties["A_IND_PRUEBA"]='3';//incorrecto
        $inbound->updateActivity($this->codactu, $properties);
        //envir  mensaje que no es valido las pruebas
        $mensaje="Req:".$this->codactu." pruebas de cable invalidas";
        Sms::enviarSincrono($celular, $mensaje, '1', '');
        return ['rst'=>1];
    }

    public function activacionRutaTecnico()
    {
        $tecnico =  $this->getBucket();
        $fecha_activacion = $this->getActivateRouteTime();
        if ($tecnico && $fecha_activacion) {
            $obj = new ActivacionRoute();
            $obj->fecha_activacion = $fecha_activacion;
            $obj->carnet_id = $tecnico;
            $obj->save();
        } else {
            
        }
        return ["rst" => 1];
    }

    public function consultarCapacidad($actuacion, $date)
    {
        $bucket='';
        $actividadTipo= ActividadTipo::find($actuacion['actividad_tipo_id']);
        list($nodoOrMdf, $troba)=explode("|", $actuacion['fftt']);
        $quiebre = $actuacion['quiebre'];
        $zonaTrabajo = $nodoOrMdf.'_'.$troba;
        if (isset($actividadTipo->label) && isset($actividadTipo->duracion)
            ) {
            $rst = Redis::get($date.'|AM|'.$actividadTipo->label.'|'.$quiebre.'|'.$zonaTrabajo);
            $capacidad = json_decode($rst, true);
            if (isset($capacidad['location'])) {
                $bucket = $capacidad['location'];
            }
        }
        return $bucket;
    }

    public function autorizacionSolicitud()
    {
        try {
            $tiposAutorizaciones = Cache::get('tipo_autorizacion');
            $tiposAutorizaciones = Cache::remember('tipo_autorizacion', 24*60*60, function () {
                return json_encode(TipoAutorizacion::lists('id', 'subject'));
            });
            $objgestion = new StGestion;
            $solicitudTecnica = $this->getSolicitudTecnica();
            $usuarioAutorizacion = $this->getUserAutorization();
            $tiempoAutorizacion = $this->getTimeAutorization();
            $tiposAutorizaciones = json_decode($tiposAutorizaciones, true);
            $tipoAutorizacion = $tiposAutorizaciones[$this->subject];
            $inputs = [];
            $movimiento = array(
                        'tipo_autorizacion_id' => $tipoAutorizacion,
                        'usuario_autorizacion' => $usuarioAutorizacion,
                        'fecha_autorizacion' => $tiempoAutorizacion,
                        'subject' => $this->subject
                    );
            
            $objgestion->registrarMovimientoDetalle($solicitudTecnica, $movimiento);
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function getMotivo($respuesta)
    {
        $where["estado"] = 1;
        if (isset($respuesta['estado_ofsc_id']) &&
            ($respuesta['estado_ofsc_id'] == 4 || $respuesta['estado_ofsc_id'] == 6)) {
            $where["actividad_id"] = $respuesta["actividad_id"];
        }
        $propiedades = PropiedadOfsc::get(['tipo'=>'M','estado'=>1]);
        $motivo = new \stdClass;
        $motivo->id = null;
        $motivo->codigo_ofsc = null;
        $motivo->tipo_tratamiento_legado = null;
        $motivo->codigo_legado = '';
        $motivo->actividad_id = null;
        $motivo->estado_ofsc_id = null;
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $where["tipo_tecnologia"] = $propiedad->tipo_tecnologia;
                $where["codigo_ofsc"] = trim($respuesta[$propiedad->label]);
                $obj = MotivoOfsc::get($where);
                if (count($obj) > 0) {
                    if (!is_null($obj[0])) {
                        $motivo = $obj[0];
                    }
                }
            }
        }
        return $motivo;
    }

    public function getSubmotivo($respuesta, $motivoId)
    {
        $subMotivo = new \stdClass;
        $subMotivo->id =null;
        $subMotivo->codigo_legado = '';

        $propiedades = PropiedadOfsc::get(['tipo'=>'S','estado'=>1]);
        foreach ($propiedades as $propiedad) {
            if (array_key_exists($propiedad->label, $respuesta)) {
                $rst = SubmotivoOfsc::get([
                    'codigo_ofsc'=>trim($respuesta[$propiedad->label]),
                    'motivo_ofsc_id'=>$motivoId,
                ]);
                if (count($rst)>0) {
                    $subMotivo=$rst[0];
                }
            }
        }

        return $subMotivo;
    }

    public function setAveriaComponentes($averias)
    {
        ComponenteOperacion::setAveriaComponente($averias, $this->st);
    }

    public function getContrataTransferencia($where)
    {
        return ContrataTransferencia::where($where)->first();
    }

    public function getCarnetTmp()
    {
        return Tecnico::where('carnet_tmp', $this->bucket)->first();
    }

    public function getEmpresa()
    {
        return Tecnico::getEmpresa($this->bucket);
    }

    public function getUltimo()
    {
        $ultimo = Ultimo::where("num_requerimiento", $this->codactu)
            ->leftJoin(
                "solicitud_tecnica as st",
                "st.id",
                "=",
                "solicitud_tecnica_ultimo.solicitud_tecnica_id"
            );
        /*if (!is_null($this->aid)) {
            $ultimo->where("aid", "=", $this->aid);
        }*/
        if (!is_null($this->st)) {
            $ultimo->where("st.id_solicitud_tecnica", "=", $this->st);
        }
        $ultimo->orderBy("solicitud_tecnica_ultimo.updated_at", "DESC");
        return $ultimo->first();
    }

    public function getSolicitudTecnica()
    {
        $solicitud = null;
        if ($this->numOrdenTrabajo == "" && $this->numOrdenServicio == "") {
            $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $this->st)
                            ->orderBy("updated_at", "DESC")->first();
        } else {
            $solicitud = SolicitudTecnica::select("solicitud_tecnica.*")
                            ->leftJoin(
                                "solicitud_tecnica_ultimo as stu",
                                "stu.solicitud_tecnica_id",
                                "=",
                                "solicitud_tecnica.id"
                            )
                            ->where("id_solicitud_tecnica", $this->st)
                            ->where("num_requerimiento", $this->codactu);
            if (!is_null($this->numOrdenTrabajo) && $this->numOrdenTrabajo !="") {
                $solicitud = $solicitud->where("num_orden_trabajo", '=', $this->numOrdenTrabajo);
                $solicitud->where("num_orden_trabajo", '=', $this->numOrdenTrabajo);
            }
            if (!is_null($this->numOrdenServicio) && $this->numOrdenServicio !="" && (int)$this->numOrdenServicio != 0) {
                $solicitud = $solicitud->where('num_orden_servicio', '=', $this->numOrdenServicio);
            }

            $solicitud = $solicitud->where("stu.estado_st", "=", 1)
                ->orderBy("solicitud_tecnica.updated_at", "desc")
                ->first();
        }
        Log::useDailyFiles(storage_path().'/logs/estadorespuesta_st.log');
        $request = ["st" => $this->st, "ot" => $this->numOrdenTrabajo, "os" => $this->numOrdenServicio, "codactu" => $this->codactu];
            Log::info([$request]);
        return $solicitud;
    }

    public function updateMateriales($inventario, $tecnico_id)
    {
    //en el mensaje no viene serie, debe acordarse luego si se agregará o no
        foreach ($inventario as $key => $value) {
            $material=Material::where('codmat', (string) $value->invtype)
                      ->first();
            if ($material) {
                $datos['material_id']=$material->id;
                $datos['tecnico_id']=$tecnico_id;
                $datos['opcion']=2;

                $tecmaterial=TecnicoMaterial:: where('material_id', $datos['material_id'])
                        ->where('tecnico_id', $datos['tecnico_id'])
                        ->where('estado', 1)
                        ->first();

                if ($tecmaterial) {
                    $datos['cantidad']=floatval($tecmaterial->cantidad)-floatval($value->quantity);
                    $tecmaterial->cantidad=$datos['cantidad'];
                    //$tecmaterial->save();

                    $movimientoObj= new TecnicoMaterialMovimiento($datos);
                    $tecmaterial->movimiento()->saveMany([$movimientoObj]);
                    $tecmaterial->save();
                }
            }

        }
    }

    public function getBody()
    {
        return $this->body;
    }
    public function setBody($body)
    {
        $this->body=$body;
    }

    public function getSubject()
    {
        return $this->subject;
    }
    public function setSubject($subject)
    {
        $this->subject=$subject;
    }
    public function getCodactu()
    {
        return $this->codactu;
    }
    public function setCodactu($codactu)
    {
        $this->codactu=$codactu;
    }

    public function getBucket()
    {
        return $this->bucket;
    }
    public function setBucket($bucket)
    {
        $this->bucket=$bucket;
    }

    public function getData()
    {
        return $this->data;
    }
    public function setData($data)
    {
        $this->data=$data;
    }

    public function getAid()
    {
        return $this->aid;
    }
    public function setAid($aid)
    {
        $this->aid=$aid;
    }
    public function setSt($st)
    {
        $this->st=$st;
    }
    public function setNumOrdenTrabajo($order)
    {
        $this->numOrdenTrabajo = $order;
    }
    public function setNumOrdenServicio($servicio)
    {
        $this->numOrdenServicio = $servicio;
    }
    public function setActivateRouteTime($time)
    {
        $this->activateTime = $time;
    }
    public function getActivateRouteTime()
    {
        return $this->activateTime;
    }
    public function setTimeAutorization($time_authorization)
    {
        $this->timeAutorization = $time_authorization;
    }
    public function getTimeAutorization()
    {
        return $this->timeAutorization;
    }
    public function setUserAutorization($user_name)
    {
        $this->userAutorization = $user_name;
    }
    public function getUserAutorization()
    {
        return $this->userAutorization;
    }
}
