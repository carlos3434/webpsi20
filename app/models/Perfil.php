<?php

class Perfil extends \Eloquent
{

    public $table = "perfiles";
    use \SoftDeletingTrait;

    public static $rules = [
        'nombre' => 'required|between:1,200'
    ];
    public $userAuth = 697;

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = \Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = \Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = \Auth::id();
            }
        );
    }
    public function opciones()
    {
        return $this->hasMany('PerfilOpcion');
    }
}
