<?php

class Opcion extends \Eloquent
{
	use DataViewer;
    public $table = "opcion";

    public function criterios()
    {
        return $this->hasMany('OpcionCriterio');
    }
}
