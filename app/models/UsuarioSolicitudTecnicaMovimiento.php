<?php

class UsuarioSolicitudTecnicaMovimiento extends \Eloquent
{
	public function __construct()
    {
         $this->table = "usuario_solicitud_tecnica_movimiento";
    }

    public function getUsuario()
    {
    	$this->belongsTo('Usuario');
    }

    
}