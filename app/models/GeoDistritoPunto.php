<?php 
class GeoDistritoPunto extends Eloquent
{

    public static function postGeoDistritoPuntoAll()
    {
        $r  =   DB::table('geo_distritopunto')
        ->select(
            'departamento as id',
            DB::raw('departamento as nombre')
        )
         ->groupBy('departamento')
          ->get();
        return $r;
    }

    public static function postProvinciaFiltro($array)
    {
        $r  =   DB::table('geo_distritopunto')
                  ->select(
                      'provincia as id',
                      DB::raw("provincia as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->groupBy('provincia')
                  ->get();
        return $r;
    }
    
    public static function postDistritoFiltro($array)
    {
        $r  =   DB::table('geo_distritopunto')
                  ->select(
                      'distrito as id',
                      DB::raw("distrito as nombre ")
                  )
                  ->whereIn("departamento", $array['departamento'])
                  ->whereIn("provincia", $array['provincia'])
                  ->groupBy('distrito')
                  ->get();
        return $r;
    }

  
}
