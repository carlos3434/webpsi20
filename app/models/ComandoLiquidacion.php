<?php

class ComandoLiquidacion extends \Eloquent
{
    //protected $table = "actividades";

    public function __construct()
    {
         $this->table = "comandos_liquidacion";
    }

    protected $fillable = ['nombre', 
                           'fecha_inicio_comando', 
                           'fecha_final_comando', 
                           'dias_hora_inicio', 
                           'dias_hora_fin', 
                           'estado', 
                           'orden', 
                           'tipo_fecha', 
                           'fecha_inicio_busqueda', 
                           'fecha_final_busqueda', 
                           'estado_programacion'];

    public function dias()
    {
        return $this->hasMany('ComandoDia324', 'comando_id', 'id');
    }

    public function quiebres()
    {
        return $this->hasMany('ComandoQuiebre', 'comando_id', 'id');
    }

    public function atenciones()
    {
        return $this->hasMany('ComandoPretemporalRrss', 'comando_id', 'id');
    }

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = \Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = \Auth::id();
            }
        );
    }

}
