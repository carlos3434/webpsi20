<?php

class Toa extends \Eloquent
{
    //protected $table = "actividades";
    public static function ValidaPermiso()
    {
        // if ( !isset(Auth::user()->id) )
        //     return 1;

        $codactu=Input::get('codactu');

        $sql="  SELECT c.nombre,cv.valores,cv.campo,cv.relacion,
                        cv.relacion_temporal,cv.campo_temporal
                FROM config c
                INNER JOIN config_valores cv
                    ON cv.config_id=c.id AND cv.estado=1
                WHERE c.id=3
                AND c.estado=1";
        $datos=DB::select($sql);

        $where=''; // Inicializando filtros
        $relacion=''; // Inicializando relacion
        $wheret='';
        $relaciont='';
        $idUser = Auth::user()->id?Auth::user()->id:'';

        //$idEstaticos = array(8,10,823,769,767,743,11,918,467,932,832,933,828,934,935,936,937,938);

        //if ( (count($datos)>0 AND date("d-m-Y") >= "13-07-2016") OR (count($datos)>0 AND in_array($idUser, $idEstaticos)) ) {
        if ( count($datos)>0 ) {
            foreach ($datos as $key => $value) {
                if (trim($value->relacion)!='') {
                    if (strpos($relacion, $value->relacion)===FALSE) {
                        $relacion.=" ".$value->relacion." ";
                    }
                }
                if (trim($value->relacion_temporal)!='') {
                    if (strpos($relaciont, $value->relacion_temporal)===FALSE) {
                        $relaciont.=" ".$value->relacion_temporal." ";
                    }
                }
                if (strpos($value->valores, "|")!==FALSE) {
                    $det=explode("|", $value->valores);
                    $where.=" AND ( ";
                    $wheret.=" AND ( ";
                    for ($i=0;$i<count($det);$i++) {
                        if (strpos($value->campo, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo);
                            $where.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $where.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $where=substr($where, 0, -3);
                            $where.=") OR ";
                        } else {
                            $where.=$value->campo."='".$det[$i]."' OR ";
                        }

                        if (strpos($value->campo_temporal, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo_temporal);
                            $wheret.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $wheret.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $wheret=substr($wheret, 0, -3);
                            $wheret.=") OR ";
                        } else {
                            $wheret.=$value->campo_temporal.
                                    "='".$det[$i]."' OR ";
                        }
                    }

                    $where=substr($where, 0, -3);
                    $wheret=substr($wheret, 0, -3);
                    $where.=") ";
                    $wheret.=") ";
                } else {
                    if (strpos($value->campo, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo);
                        $where.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $where.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $where=substr($where, 0, -3);
                        $where.=") ";
                    } else {
                        $where.=" AND ".$value->campo."='".$value->valores."' ";
                    }

                    if (strpos($value->campo_temporal, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo_temporal);
                        $wheret.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $wheret.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $wheret=substr($wheret, 0, -3);
                        $wheret.=") ";
                    } else {
                        $wheret.=" AND ".$value->campo_temporal.
                                "='".$value->valores."' ";
                    }
                }
            }

            $sqlf=" SELECT count(um.codactu) cant
                    FROM ultimos_movimientos um
                    $relacion
                    WHERE um.codactu='$codactu'
                    $where ";
            $datosf= DB::select($sqlf);
            
            if ( $datosf[0]->cant==0 ) {
                $sqlf=" SELECT count(t.averia) cant
                        FROM webpsi_coc.averias_criticos_final t
                        $relaciont
                        WHERE averia='$codactu'
                        $wheret ";
                $sqlf= str_replace("actividadt", "1", $sqlf);
                $datosf= DB::select($sqlf);

                if ( $datosf[0]->cant==0 ) {
                    $sqlf=" SELECT count(t.codigo_req) cant
                            FROM webpsi_coc.tmp_provision t
                            $relaciont
                            WHERE codigo_req='$codactu'
                            $wheret ";
                    $sqlf= str_replace("actividadt", "2", $sqlf);
                    $datosf= DB::select($sqlf);
                }
            }

            // *****************************************************************
            $redis  = Redis::connection();
            try {
                Redis::ping();
                Redis::select(1);
            } catch (Exception $e) {}
            if ( !isset($e) ) {
                // tipoValidacion es 1, entonces es TOA
                // tipoValidacion es 0, No es TOA
                $tipoValidacion = 0;
                if ($datosf[0]->cant > 0) {
                    $tipoValidacion = 1;
                }
                $validacion = '';
                if (Redis::get($codactu)) {
                    $fechaActual = strtotime(date("Y-m-d H:i:s"));
                    $contenidoArray = explode("|", Redis::get($codactu));
                    $usuario = Usuario::find($contenidoArray[1]);
                    $nomUsuario = $usuario->nombre." ".$usuario->apellido;
                    $validacion = 'view'."|".$nomUsuario."|".$tipoValidacion;
                    return $validacion;
                    // $fechaOpenModal = strtotime($contenidoArray[0]);
                    // $difFecha = ($fechaActual-$fechaOpenModal)/60;
                    // if ($difFecha < 10) {
                    //     // cargamos modal, pero quitamos la opcion de Registrar
                    //     $usuario = Usuario::find($contenidoArray[1]);
                    //     $nomUsuario = $usuario->nombre." ".$usuario->apellido;
                    //     $validacion = 'view'."|".$nomUsuario."|".$tipoValidacion;
                    //     return $validacion;
                    // } else {
                    //     Redis::del($codactu);
                    // }
                }

                // *********************************************************
                $contenido = date("Y-m-d H:i:s")."|".Auth::user()->id;
                Redis::set($codactu, $contenido);
                Redis::expire($codactu, 1*60);
                // *********************************************************
            }
            // *****************************************************************

            return $datosf[0]->cant;
        } else {
            // *****************************************************************
            $redis  = Redis::connection();
            try {
                Redis::ping();
                Redis::select(1);
            } catch (Exception $e) {}
            if ( !isset($e) ) {
                $validacion = '';
                if (Redis::get($codactu)) {
                    $fechaActual = strtotime(date("Y-m-d H:i:s"));
                    $contenidoArray = explode("|", Redis::get($codactu));
                    $usuario = Usuario::find($contenidoArray[1]);
                    $nomUsuario = $usuario->nombre." ".$usuario->apellido;
                    $validacion = 'view'."|".$nomUsuario."|0";
                    return $validacion;
                }

                // *********************************************************
                $contenido = date("Y-m-d H:i:s")."|".Auth::user()->id;
                Redis::set($codactu, $contenido);
                Redis::expire($codactu, 1*60);
                // *********************************************************
            }
            // *****************************************************************
            return 0;
        }
    }

    public static function ValidaPermisoProceso()
    {
        $sql="  SELECT c.nombre,cv.valores,cv.campo,cv.relacion,
                        cv.relacion_temporal,cv.campo_temporal
                FROM config c
                INNER JOIN config_valores cv
                    ON cv.config_id=c.id AND cv.estado=1
                WHERE c.nombre='toa'
                AND c.estado=1";
        $datos=DB::select($sql);

        $where=''; // Inicializando filtros
        $relacion=''; // Inicializando relacion
        $wheret='';
        $relaciont='';
        $df=array();

        if ( count($datos)>0 ) {

            foreach ($datos as $key => $value) {
                if (trim($value->relacion)!='') {
                    if (strpos($relacion, $value->relacion)===FALSE) {
                        $relacion.=" ".$value->relacion." ";
                    }
                }
                if (trim($value->relacion_temporal)!='') {
                    if (strpos($relaciont, $value->relacion_temporal)===FALSE) {
                        $relaciont.=" ".$value->relacion_temporal." ";
                    }
                }

                if (strpos($value->valores, "|")!==FALSE) {
                    $det=explode("|", $value->valores);
                    $where.=" AND ( ";
                    $wheret.=" AND ( ";
                    for ($i=0;$i<count($det);$i++) {
                        if (strpos($value->campo, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo);
                            $where.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $where.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $where=substr($where, 0, -3);
                            $where.=") OR ";
                        } else {
                            $where.=$value->campo."='".$det[$i]."' OR ";
                        }

                        if (strpos($value->campo_temporal, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo_temporal);
                            $wheret.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $wheret.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $wheret=substr($wheret, 0, -3);
                            $wheret.=") OR ";
                        } else {
                            $wheret.=$value->campo_temporal.
                                    "='".$det[$i]."' OR ";
                        }
                    }
                    $where=substr($where, 0, -3);
                    $wheret=substr($wheret, 0, -3);
                    $where.=") ";
                    $wheret.=") ";
                } else {
                    if (strpos($value->campo, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo);
                        $where.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $where.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $where=substr($where, 0, -3);
                        $where.=") ";
                    } else {
                        $where.=" AND ".$value->campo."='".$value->valores."' ";
                    }

                    if (strpos($value->campo_temporal, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo_temporal);
                        $wheret.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $wheret.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $wheret=substr($wheret, 0, -3);
                        $wheret.=") ";
                    } else {
                        $wheret.=" AND ".$value->campo_temporal.
                                "='".$value->valores."' ";
                    }
                }
            }

            
            $sqlfa=" SELECT averia codactu,mdf,actividad_tipo_id, 
                    eecc_final as empresa, 2 as actividad, contrata
                    FROM webpsi_coc.averias_criticos_final t
                    $relaciont
                    WHERE averia NOT IN ( SELECT codactu FROM ultimos_movimientos )
                    $wheret
                    ORDER BY mdf,actividad_tipo_id";

            $sqlf= str_replace("actividadt", "1", $sqlfa);
            $datosA= DB::select($sqlf);
            Toa::logEnvioMasivoQuery($sqlf, 1);
            
            $sqlfp=" SELECT t.codigo_req as codactu, mdf, actividad_tipo_id,
                    eecc_final as empresa, 1 as actividad, contrata, area2,
                    codigo_req as numreq
                    FROM webpsi_coc.tmp_provision t
                    $relaciont
                    WHERE codigo_req NOT IN ( SELECT codactu FROM ultimos_movimientos )
                    $wheret
                    ORDER BY mdf,actividad_tipo_id";

            $sqlf= str_replace("actividadt", "2", $sqlfp);
            $datosP= DB::select($sqlf);
            Toa::logEnvioMasivoQuery($sqlf, 2);

            $df= array_merge($datosA, $datosP);
            return $df;
        } else {
            return $df;
        }
    }

    public static function ValidaPermisoEnvioLiquidados()
    {
        $sql="SELECT c.nombre,cv.valores,cv.campo,cv.relacion,
                        cv.relacion_temporal,cv.campo_temporal
                FROM config c
                INNER JOIN config_valores cv
                    ON cv.config_id=c.id AND cv.estado=1
                WHERE c.id=4
                AND c.estado=1";
        $datos=DB::select($sql);

        $where=''; // Inicializando filtros
        $relacion=''; // Inicializando relacion
        $wheret='';
        $relaciont='';
        $valores = array();

        if ( count($datos)>0 ) {

            foreach ($datos as $key => $value) {
                if (trim($value->relacion)!='') {
                    if (strpos($relacion, $value->relacion)===FALSE) {
                        $relacion.=" ".$value->relacion." ";
                    }
                }
                if (trim($value->relacion_temporal)!='') {
                    if (strpos($relaciont, $value->relacion_temporal)===FALSE) {
                        $relaciont.=" ".$value->relacion_temporal." ";
                    }
                }

                if (strpos($value->valores, "|")!==FALSE) {
                    $det=explode("|", $value->valores);
                    $where.=" AND ( ";
                    $wheret.=" AND ( ";
                    for ($i=0;$i<count($det);$i++) {
                        if (strpos($value->campo, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo);
                            $where.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $where.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $where=substr($where, 0, -3);
                            $where.=") OR ";
                        } else {
                            $where.=$value->campo."='".$det[$i]."' OR ";
                        }

                        if (strpos($value->campo_temporal, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo_temporal);
                            $wheret.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $wheret.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $wheret=substr($wheret, 0, -3);
                            $wheret.=") OR ";
                        } else {
                            $wheret.=$value->campo_temporal.
                                    "='".$det[$i]."' OR ";
                        }
                    }
                    $where=substr($where, 0, -3);
                    $wheret=substr($wheret, 0, -3);
                    $where.=") ";
                    $wheret.=") ";
                } else {
                    if (strpos($value->campo, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo);
                        $where.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $where.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $where=substr($where, 0, -3);
                        $where.=") ";
                    } else {
                        $where.=" AND ".$value->campo."='".$value->valores."' ";
                    }

                    if (strpos($value->campo_temporal, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo_temporal);
                        $wheret.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $wheret.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $wheret=substr($wheret, 0, -3);
                        $wheret.=") ";
                    } else {
                        $wheret.=" AND ".$value->campo_temporal.
                                "='".$value->valores."' ";
                    }
                }
            }

            $query = "select um.aid, um.codactu, um.actividad_tipo_id, um.mdf , e.nombre as empresa
                      from ultimos_movimientos um
                      LEFT JOIN empresas e ON um.empresa_id = e.id
                      $relacion
                      where (um.estado_id = 15 or um.estado_id = 6)
                      and um.aid IS NOT NULL
                      and um.liquidado_ofsc = 0

                      $where";
            //dd($query);
            $valores = DB::select($query);
            return $valores;
        } else {
            return $valores;
        }
    }

    public static function ObtenerPendientes()
    {
        $sql="  SELECT c.nombre,cv.valores,cv.campo,cv.relacion,
                        cv.relacion_temporal,cv.campo_temporal
                FROM config c
                INNER JOIN config_valores cv
                    ON cv.config_id=c.id AND cv.estado=1
                WHERE c.id=3
                AND c.estado=1";
        $datos=DB::select($sql);

        $where=''; // Inicializando filtros
        $relacion=''; // Inicializando relacion
        $wheret='';
        $relaciont='';
        $df=array();

        if ( count($datos)>0 ) {

            foreach ($datos as $key => $value) {
                if (trim($value->relacion)!='') {
                    if (strpos($relacion, $value->relacion)===FALSE) {
                        $relacion.=" ".$value->relacion." ";
                    }
                }
                if (trim($value->relacion_temporal)!='') {
                    if (strpos($relaciont, $value->relacion_temporal)===FALSE) {
                        $relaciont.=" ".$value->relacion_temporal." ";
                    }
                }

                if (strpos($value->valores, "|")!==FALSE) {
                    $det=explode("|", $value->valores);
                    $where.=" AND ( ";
                    $wheret.=" AND ( ";
                    for ($i=0;$i<count($det);$i++) {
                        if (strpos($value->campo, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo);
                            $where.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $where.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $where=substr($where, 0, -3);
                            $where.=") OR ";
                        } else {
                            $where.=$value->campo."='".$det[$i]."' OR ";
                        }

                        if (strpos($value->campo_temporal, "*")!==FALSE) {
                            $detval=explode("*", $det[$i]);
                            $detcam=explode("*", $value->campo_temporal);
                            $wheret.="(";
                            for ($j=0;$j<count($detval);$j++) {
                                $wheret.=" ".$detcam[$j]."='".
                                        $detval[$j]."' AND";
                            }
                            $wheret=substr($wheret, 0, -3);
                            $wheret.=") OR ";
                        } else {
                            $wheret.=$value->campo_temporal.
                                    "='".$det[$i]."' OR ";
                        }
                    }
                    $where=substr($where, 0, -3);
                    $wheret=substr($wheret, 0, -3);
                    $where.=") ";
                    $wheret.=") ";
                } else {
                    if (strpos($value->campo, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo);
                        $where.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $where.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $where=substr($where, 0, -3);
                        $where.=") ";
                    } else {
                        $where.=" AND ".$value->campo."='".$value->valores."' ";
                    }

                    if (strpos($value->campo_temporal, "*")!==FALSE) {
                        $detval=explode("*", $value->valores);
                        $detcam=explode("*", $value->campo_temporal);
                        $wheret.=" AND (";
                        for ($i=0;$i<count($detval);$i++) {
                            $wheret.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                        }
                        $wheret=substr($wheret, 0, -3);
                        $wheret.=") ";
                    } else {
                        $wheret.=" AND ".$value->campo_temporal.
                                "='".$value->valores."' ";
                    }
                }
            }

            /*
            $sqlfa=" SELECT codactu,aid,actividad_id
                    FROM ultimos_movimientos um
                    $relaciont
                    WHERE estado_ofsc_id=1 and actividad_id=1
                    $wheret
                    ORDER BY codactu";

            $sqlf1= $sqlfa; //str_replace("actividadt", "1", $sqlfa);
            $datosA= DB::select($sqlf1);
            */
            $sqlfp=" SELECT codactu,aid,actividad_id
                    FROM ultimos_movimientos um
                    JOIN  webpsi_coc.tmp_provision p ON um.codactu = p.codigo_req 
                    WHERE p.quiebre='PENDMOV1' AND p.mdf IN ('LL','LM','SP')
                    AND p.origen='pen_prov_catv' AND p.contrata!='245'
                    AND estado_ofsc_id=1 and actividad_id=2
                    ORDER BY codactu";
                return 
            //$sqlf=  $sqlfp; //str_replace("actividadt", "2", $sqlfp);
            $datosP= DB::select($sqlfp);
            //$df= array_merge($datosA, $datosP);
            return $datosP;
            //return $sqlf1.'             '.$sqlf;
        } else {
            return $df;
        }
    }

    public static function logEnvioMasivoQuery ($query = "", $tipo = 0){
        DB::table("envio_ofsc_query")->insert(array("query" => $query, "orden" => $tipo));
    }
}
