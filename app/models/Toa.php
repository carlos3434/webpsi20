<?php
class Toa extends \Eloquent
{
    protected $relaciont;
    protected $relacion;
    protected $wheret;
    protected $where;

    /**
     * id
     * 1: envio masivo
     * 2: cancelado masivo
     * 3: validacion de bandeja
     * 4: liquidacion masiva a toa
     * 5: envio masivo test
     */
    public function obtenerConfiguracion($id)
    {
        $sql="  SELECT c.nombre,cv.valores,cv.campo,cv.relacion,
                        cv.relacion_temporal,cv.campo_temporal
                FROM config c
                INNER JOIN config_valores cv
                    ON cv.config_id=c.id AND cv.estado=1
                WHERE c.id=?
                AND c.estado=1";
        $datos=DB::select($sql,[$id]);
        if (count($datos)==0) return false;
        $where=''; // Inicializando filtros
        $relacion=''; // Inicializando relacion
        $wheret='';
        $relaciont='';

        foreach ($datos as $key => $value) {
            if (trim($value->relacion)!='') {
                if (strpos($relacion, $value->relacion)===FALSE) {
                    $relacion.=" ".$value->relacion." ";
                }
            }
            if (trim($value->relacion_temporal)!='') {
                if (strpos($relaciont, $value->relacion_temporal)===FALSE) {
                    $relaciont.=" ".$value->relacion_temporal." ";
                }
            }
            if (strpos($value->valores, "|")!==FALSE) {
                $det=explode("|", $value->valores);
                $where.=" AND ( ";
                $wheret.=" AND ( ";
                for ($i=0;$i<count($det);$i++) {
                    if (strpos($value->campo, "*")!==FALSE) {
                        $detval=explode("*", $det[$i]);
                        $detcam=explode("*", $value->campo);
                        $where.="(";
                        for ($j=0;$j<count($detval);$j++) {
                            $where.=" ".$detcam[$j]."='".
                                    $detval[$j]."' AND";
                        }
                        $where=substr($where, 0, -3);
                        $where.=") OR ";
                    } else {
                        $where.=$value->campo."='".$det[$i]."' OR ";
                    }

                    if (strpos($value->campo_temporal, "*")!==FALSE) {
                        $detval=explode("*", $det[$i]);
                        $detcam=explode("*", $value->campo_temporal);
                        $wheret.="(";
                        for ($j=0;$j<count($detval);$j++) {
                            $wheret.=" ".$detcam[$j]."='".
                                    $detval[$j]."' AND";
                        }
                        $wheret=substr($wheret, 0, -3);
                        $wheret.=") OR ";
                    } else {
                        $wheret.=$value->campo_temporal.
                                "='".$det[$i]."' OR ";
                    }
                }
                $where=substr($where, 0, -3);
                $wheret=substr($wheret, 0, -3);
                $where.=") ";
                $wheret.=") ";
            } else {
                if (strpos($value->campo, "*")!==FALSE) {
                    $detval=explode("*", $value->valores);
                    $detcam=explode("*", $value->campo);
                    $where.=" AND (";
                    for ($i=0;$i<count($detval);$i++) {
                        $where.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                    }
                    $where=substr($where, 0, -3);
                    $where.=") ";
                } else {
                    $where.=" AND ".$value->campo."='".$value->valores."' ";
                }
                if (strpos($value->campo_temporal, "*")!==FALSE) {
                    $detval=explode("*", $value->valores);
                    $detcam=explode("*", $value->campo_temporal);
                    $wheret.=" AND (";
                    for ($i=0;$i<count($detval);$i++) {
                        $wheret.=" ".$detcam[$i]."='".$detval[$i]."' AND";
                    }
                    $wheret=substr($wheret, 0, -3);
                    $wheret.=") ";
                } else {
                    $wheret.=" AND ".$value->campo_temporal.
                            "='".$value->valores."' ";
                }
            }
        }
        $this->where = $where;
        $this->wheret = $wheret;
        $this->relaciont = $relaciont;
        $this->relacion = $relacion;
        return true;
    }
    /**
     * retorna 1 cuando tiene permiso de envio a toa, cero de caso contrario
     */
    public function validacionIndividual($codactu)
    {
        $sqlf=" SELECT count(um.codactu) cant
                FROM ultimos_movimientos um
                $this->relacion
                WHERE um.codactu='$codactu'
                $this->where ";
        $datosf= DB::select($sqlf);
        
        if ( $datosf[0]->cant==0 ) {
            $sqlf=" SELECT count(t.averia) cant
                    FROM webpsi_coc.averias_criticos_final t
                    $this->relaciont
                    WHERE averia='$codactu'
                    $this->wheret ";
            $sqlf= str_replace("actividadt", "1", $sqlf);
            $datosf= DB::select($sqlf);

            if ( $datosf[0]->cant==0 ) {
                $sqlf=" SELECT count(t.codigo_req) cant
                        FROM webpsi_coc.tmp_provision t
                        $this->relaciont
                        WHERE codigo_req='$codactu'
                        $this->wheret ";
                $sqlf= str_replace("actividadt", "2", $sqlf);
                $datosf= DB::select($sqlf);
            }
        }
        $tipoValidacion = 0;
        if ($datosf[0]->cant > 0) {
            $tipoValidacion = 1;
        }
        return $tipoValidacion;
    }
    /**
     * filtro de envio a toa
     */
    public function envioMasivoLiquidados()
    {
        $sql = "SELECT um.aid, um.codactu, um.actividad_tipo_id
                FROM ultimos_movimientos um
                $this->relacion
                WHERE (um.estado_id = 15 OR um.estado_id = 6)
                AND um.aid IS NOT NULL
                AND um.liquidado_ofsc = 0
                $this->where";
        return DB::select($sql);
    }
    /**
     * filtro de envio a toa
     */
    public function envioMasivo()
    {

        $sqlfa=" SELECT averia codactu,mdf,actividad_tipo_id, 
                    eecc_final as empresa, 1 as actividad, contrata,
                    averia as numreq, area2, fftt, quiebre
                    FROM webpsi_coc.averias_criticos_final t
                    $this->relaciont
                    WHERE averia NOT IN ( SELECT codactu FROM ultimos_movimientos )
                    $this->wheret
                    ORDER BY mdf,actividad_tipo_id";
                    
        $sqlf= str_replace("actividadt", "1", $sqlfa);
        $datosA= DB::select($sqlf);
        
        $sqlfp=" SELECT t.codigo_req as codactu, mdf, actividad_tipo_id,
                eecc_final as empresa, 2 as actividad, contrata, area2,
                codigo_req as numreq, fftt, quiebre
                FROM webpsi_coc.tmp_provision t
                $this->relaciont
                WHERE codigo_req NOT IN ( SELECT codactu FROM ultimos_movimientos )
                $this->wheret
                ORDER BY mdf,actividad_tipo_id";

        $sqlf= str_replace("actividadt", "2", $sqlfp);
        $datosP= DB::select($sqlf);
        $actuaciones= array_merge($datosA, $datosP);
        return $actuaciones;

    }
    public function envioMasivoCancelado()
    {
        $sql = "SELECT um.aid, um.codactu, um.actividad_tipo_id, um.actividad_id
                FROM ultimos_movimientos um
                JOIN  webpsi_coc.tmp_provision p ON um.codactu = p.codigo_req 
                AND p.origen='pen_prov_catv' AND p.contrata!='245'
                $this->relacion
                WHERE (um.estado_id = 1 OR um.estado_id = 7)
                AND um.aid IS NOT NULL
                $this->where";
                //print_r($sql); exit();
        return DB::select($sql);
    }

    public static function enviomasivogestion($parametros){

        $sqlA=array();
        foreach ($parametros as $key => $value) {
            $trama=explode("||", $value->trama);
            $detalle=explode("||", $value->detalle);
            /*
            //averia
            $sqlave=DB::table('webpsi_coc.averias_criticos_final as t')
                ->join(
                    'empresas as e',
                    'e.nombre', '=', 't.eecc_final'
                )
                ->join(
                    'quiebres as q',
                    'q.apocope', '=', 't.quiebre'
                )
                ->select(
                    DB::RAW('averia as codactu'),
                    'mdf',
                    'actividad_tipo_id',
                    'eecc_final as empresa',
                    DB::RAW('1 as actividad'),
                    'contrata',
                    DB::RAW('averia as numreq'),
                    'area2',
                    'fftt',
                    'quiebre'
                )
                ->whereNotIN('averia',function ($query) {
                        $query->select(DB::raw('codactu'))
                      ->from('ultimos_movimientos');
                });

            foreach ($trama as $keys => $valor) {
                if($valor!='vacio'){
                $sqlave->whereIn(DB::RAW($valor),explode(",", $detalle[$keys]));
                }
            }
            //$sqlave->limit(1);
            $sqlave=$sqlave->get();
            array_push($sqlA, $sqlave); */

            //provision
            $sqlpro=DB::table('webpsi_coc.tmp_provision as t')
                ->join(
                    'empresas as e',
                    'e.nombre', '=', 't.eecc_final'
                )
                ->join(
                    'quiebres as q',
                    'q.apocope', '=', 't.quiebre'
                )
                ->select(
                    DB::RAW('t.codigo_req as codactu'),
                    'mdf',
                    'actividad_tipo_id',
                    'eecc_final as empresa',
                    DB::RAW('2 as actividad'),
                    'contrata',
                    'area2',
                    DB::RAW('codigo_req as numreq'),
                    'fftt',
                    'quiebre'
                )
                ->whereNotIN('codigo_req',function ($query) {
                        $query->select(DB::raw('codactu'))
                      ->from('ultimos_movimientos');
                });

            foreach ($trama as $keys => $valor) {
                if($valor!=''){
                $sqlpro->whereIn(DB::RAW($valor),explode(",", $detalle[$keys]));
                }
            }
            //$sqlpro->limit(30);
            $sqlpro=$sqlpro->get();
            array_push($sqlA, $sqlpro);
        }

        $actuaciones=array();
        foreach ($sqlA as $key => $value) {
            foreach ($value as $keys => $valor) {
                array_push($actuaciones, $valor);
            }
        }

        return $actuaciones;
    }
    public static function enviomasivoLiquidado($parametros){

        $sqlA=array();
        foreach ($parametros as $key => $value) {
            $trama=explode("||", $value->trama);
            $detalle=explode("||", $value->detalle);
            /*
            //averia
            $sqlave=DB::table('webpsi_coc.averias_criticos_final as t')
                ->join(
                    'empresas as e',
                    'e.nombre', '=', 't.eecc_final'
                )
                ->join(
                    'quiebres as q',
                    'q.apocope', '=', 't.quiebre'
                )
                ->select(
                    DB::RAW('averia as codactu'),
                    'mdf',
                    'actividad_tipo_id',
                    'eecc_final as empresa',
                    DB::RAW('1 as actividad'),
                    'contrata',
                    DB::RAW('averia as numreq'),
                    'area2',
                    'fftt',
                    'quiebre'
                )
                ->whereNotIN('averia',function ($query) {
                        $query->select(DB::raw('codactu'))
                      ->from('ultimos_movimientos');
                });

            foreach ($trama as $keys => $valor) {
                if($valor!='vacio'){
                $sqlave->whereIn(DB::RAW($valor),explode(",", $detalle[$keys]));
                }
            }
            //$sqlave->limit(1);
            $sqlave=$sqlave->get();
            array_push($sqlA, $sqlave); */

            //provision
            $sqlpro=DB::table('ultimos_movimientos as um')
                ->join(
                    'empresas as e',
                    'e.id', '=', 'um.empresa_id'
                )
                ->join(
                    'quiebres as q',
                    'q.id', '=', 'um.quiebre_id'
                )
                ->select(
                    'um.codactu',
                    'aid',
                    'mdf',
                    'actividad_tipo_id',
                    DB::RAW('e.nombre as empresa'),
                    DB::RAW('"2" as actividad'),
                    'contrata',
                    'area2',
                    DB::RAW('um.codactu as numreq'),
                    'fftt',
                    DB::RAW('q.apocope as quiebre')
                )
                ->orWhere(function ($query) {
                    $query->where('um.estado_id', 15)
                          ->OrWhere('um.estado_id', 14)
                          ->OrWhere('um.estado_id', 18)
                          ->OrWhere('um.estado_id', 6);
                })
                ->where('um.liquidado_ofsc',0)
                ->where('um.estado_ofsc_id',1)
                ->whereNotNull('um.aid');

            foreach ($trama as $keys => $valor) {
                if($valor!='vacio'){
                $sqlpro->whereIn(DB::RAW($valor),explode(",", $detalle[$keys]));
                }
            }
            //$sqlpro->limit(30);
            $sqlpro=$sqlpro->get();
            array_push($sqlA, $sqlpro);
        }

        $actuaciones=array();
        foreach ($sqlA as $key => $value) {
            foreach ($value as $keys => $valor) {
                array_push($actuaciones, $valor);
            }
        }

        return $actuaciones;
    }
}
