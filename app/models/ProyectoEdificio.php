<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * 
 */
class ProyectoEdificio extends \Eloquent
{

    //public $table = 'proyecto_edificios';

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    //protected $fillable = array('*');
    protected $guarded = [];

    public function __construct()
    {
        $this->dates = ['deleted_at'];
        $this->table = 'proyecto_edificios';
        $this->guarded = array();
    }

    /**
     * GestionEdificio relationship
     */
    public function gestionEdificio()
    {
        return $this->hasMany('GestionEdificio');
    }

}
