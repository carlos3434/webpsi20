<?php

class Ubicacion extends Eloquent
{

    public $table = 'ubicaciones';

    public function categorias()
    {
        return $this->hasMany('Categoria');
    }

}
