<?php

class AyudaImagen extends \Eloquent
{
    protected $guarded =[];
    protected $table = "ayuda_imagen";
    /**
     * relationship to Emergencia
     */
    public function ayuda()
    {
        return $this->belongsTo('Ayuda');
    }
}