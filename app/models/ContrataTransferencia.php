<?php

class ContrataTransferencia extends \Eloquent
{
	public $table = "contrata_transferencia";

	public function actividad()
	{
		return $this->belongsTo("Actividad");
	}

	public function empresa()
	{
		return $this->belongsTo("Empresa");
	}
}