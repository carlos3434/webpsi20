<?php

class EmpresaBucket extends \Eloquent
{
    protected $table = "empresa_bucket";

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

}
