<?php

class UploadLog extends \Eloquent {
	use DataViewer;
	public $table = 'uploads_log';
	protected $fillable = ['file_accept','file_refused','number_acept','number_refused','type','estado','number_exist'];

	public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

}