<?php

class GeoValidacion extends Eloquent
{
    public static function  getValidadistancia( $data )
    {
        //en metros // calculo de distancia entre dos coordenadas (x,y)
        $distancia =(number_format(6371 * ACOS(COS(deg2rad($data['coord_y1'])) * 
                     COS(deg2rad( $data['coord_y2'] )) * 
                     COS(deg2rad( $data['coord_x2'] )  - deg2rad($data['coord_x1'])) + 
                     SIN(deg2rad($data['coord_y1'])) * SIN(deg2rad( $data['coord_y2']) )), 2, '.', ''))
                     *1000 ;

        $select=[DB::RAW('UPPER(c.nombre) AS criterio'),'op_oc.detalle','op_oc.valor'];
        $opcion=\DB::table("opcion_outbound_criterio as op_oc")
                    ->select($select)
                    ->join("opcion_outbound as op_o","op_oc.opcion_outbound_id","=","op_o.id")
                    ->join("criterios as c","op_oc.criterio_id","=","c.id")
                    ->whereRaw("op_o.accion=1 AND op_o.tipo_outbound_id=10 AND op_o.id=1")
                    ->get();
        
        $actividad=[];
        for ($i=0; $i <count($opcion) ; $i++) { 
            if($opcion[$i]->criterio=="ACTIVIDADES"){
                $key=explode('|', $opcion[$i]->detalle);
                $value=explode('|', $opcion[$i]->valor);
                for ($j=0; $j < count($key); $j++) { 
                    $actividad[$key[$j]]=$value[$j];
                }
            }
        }
        
        $distmax=$actividad[$data['actividad_id']];
        if($distancia<=$distmax)
            return array('rpta'=>'1',
                         'metros'=>$distancia,
                         'maximo'=>$distmax);
        else 
            return array('rpta'=>'0',
                         'metros'=>$distancia,
                         'maximo'=>$distmax);
    }

     public static function  getquadrant($Longitude=0,$Latitude=0)
    {  //latitud -77.027738 y longitud -12.123496

        if($Longitude!=0 or $Latitude!=0) {
            $cellsize = Config::get("validacion.cellsize"); //kilometers
            $Longitudepivot =  Config::get("validacion.pivot.longitude");
            $Latitudepivot= Config::get("validacion.pivot.latitude");
    
            //quadrantId = Xgrid:Ygrid (ejemplo: -72:23)
            //Xgrid = round((1000 / 9) * cos(Latitude * pi / 180) * (Longitude - Longitude_Pivot) / CellSize)
            //Ygrid = round((1000 / 9) * (Latitude - Latitude_Pivot) / CellSize)
    
            $xgrid = round((1000 / 9) * cos($Latitude * pi() / 180) * ($Longitude - $Longitudepivot) / $cellsize);
            $ygrid = round((1000 / 9) * ($Latitude - $Latitudepivot) / $cellsize);
            
            $quadrantId=$xgrid.":".$ygrid; 
    
            return $quadrantId;
        } else {
            return "something";
        }
    }


}
