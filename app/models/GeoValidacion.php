<?php

class GeoValidacion extends Eloquent
{
    public static function  getValidadistancia( $data )
    {
        //en metros // calculo de distancia entre dos coordenadas (x,y)
        $distancia =(number_format(6371 * ACOS(COS(deg2rad($data['coord_y1'])) * 
                     COS(deg2rad( $data['coord_y2'] )) * 
                     COS(deg2rad( $data['coord_x2'] )  - deg2rad($data['coord_x1'])) + 
                     SIN(deg2rad($data['coord_y1'])) * SIN(deg2rad( $data['coord_y2']) )), 2, '.', ''))
                     *1000 ;

        $distmax=Config::get("validacion.distancia");

        if($distancia<=$distmax)
            return array('rpta'=>'1',
                         'metros'=>$distancia,
                         'maximo'=>$distmax);
        else 
            return array('rpta'=>'0',
                         'metros'=>$distancia,
                         'maximo'=>$distmax);
    }

     public static function  getquadrant($Longitude=0,$Latitude=0)
    {  //latitud -77.027738 y longitud -12.123496

        if($Longitude!=0 or $Latitude!=0) {
            $cellsize = Config::get("validacion.cellsize"); //kilometers
            $Longitudepivot =  Config::get("validacion.pivot.longitude");
            $Latitudepivot= Config::get("validacion.pivot.latitude");
    
            //quadrantId = Xgrid:Ygrid (ejemplo: -72:23)
            //Xgrid = round((1000 / 9) * cos(Latitude * pi / 180) * (Longitude - Longitude_Pivot) / CellSize)
            //Ygrid = round((1000 / 9) * (Latitude - Latitude_Pivot) / CellSize)
    
            $xgrid = round((1000 / 9) * cos($Latitude * pi() / 180) * ($Longitude - $Longitudepivot) / $cellsize);
            $ygrid = round((1000 / 9) * ($Latitude - $Latitudepivot) / $cellsize);
            
            $quadrantId=$xgrid.":".$ygrid; 
    
            return $quadrantId;
        } else {
            return "something";
        }
    }


}
