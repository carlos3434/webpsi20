<?php

class Officetrack extends \Eloquent
{

    public function getPasouno($gestionId)
    {
        $sql = "SELECT 
                    pu.x, pu.y, pu.casa_img1, 
                    pu.casa_img2, pu.casa_img3,
                    IFNULL(observacion,'') observacion
                FROM
                    webpsi_officetrack.tareas t, 
                    webpsi_officetrack.paso_uno pu 
                WHERE
                    t.id=pu.task_id 
                    AND t.task_id = $gestionId";
        
        $result = DB::select($sql);        
        return $result;
    }

    public function getPasodos($gestionId)
    {
        $sql = "SELECT 
                    pu.motivo, pu.observacion, 
                    pu.problema_img1, pu.problema_img2, 
                    pu.modem_img1, pu.modem_img2, 
                    pu.tap_img1, pu.tap_img2, 
                    pu.tv_img1, pu.tv_img2
                FROM
                    webpsi_officetrack.tareas t, 
                    webpsi_officetrack.paso_dos pu 
                WHERE
                    t.id=pu.task_id 
                    AND t.task_id = $gestionId";
        
        $result = DB::select($sql);
        return $result;
    }

    public function getPasotres($gestionId)
    {
        $sql = "SELECT 
                    pu.estado, pu.observacion, 
                    pu.final_img1, pu.final_img2, 
                    pu.firma_img
                FROM
                    webpsi_officetrack.tareas t, 
                    webpsi_officetrack.paso_tres pu 
                WHERE
                    t.id=pu.task_id 
                    AND t.task_id = $gestionId";
        
        $result = DB::select($sql);        
        return $result;
    }

    public function registrar( $trama, $response)
    {

        $db = "webpsi_officetrack";
        $table = "wsenvios";
        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $fecha = date("Y-m-d H:i:s");
            DB::insert(
                "INSERT INTO $db.$table
                (
                    fecha, trama, response
                )
                VALUES
                (
                   ?, ?, ?
                )", 
                array($fecha, $trama, $response)
            );

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Trama enviada hacia OT";
            return $result;
        /*} catch (PDOException $error) {
            //Rollback
            DB::rollback();
            $this->saveError($error);
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }*/
    }

    public function registrarTarea($task_id, $cod_tecnico, $paso, 
                                    $fec_recepcion, $cliente, $fec_agenda){
        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $sql = "INSERT INTO webpsi_officetrack.tareas 
                    (
                        task_id, cod_tecnico, 
                        paso, fecha_recepcion, 
                        cliente, fecha_agenda
                    ) 
                    VALUES 
                    (
                       ?, ?, 
                       ?, ?, 
                       ?, ?
                    )";

            $valArray = array(
                $task_id, $cod_tecnico,
                $paso, $fec_recepcion,
                $cliente, $fec_agenda
            );
            DB::insert($sql, $valArray);
            DB::commit();

            //Ultimo ID registrado
            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.tareas
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $id = $lastId[0]->id;
            DB::commit();


            $result["estado"] = true;
            $result["msg"] = "Tarea registrada correctamente";
            $result["id"] = $id;
            return $result;
        /*} catch (PDOException $error) {
            DB::rollback();
            $this->saveError($error);
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }*/
    }

    /*update*/
    public static function registrarNuevaTarea($data = array()){
        //try {
            $envio = DB::table("webpsi_officetrack.tareas")->insert($data);
            DB::commit();

            //Ultimo ID registrado
            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.tareas
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $id = $lastId[0]->id;
            DB::commit();

            $result["id"] = $id;
            $result["msg"] = 'Tarea registrada correctamente';
            //DB::getPdo()->lastInsertId()  DB::getQueryLog()
            $result["estado"] = $envio;// 1 
            return $result;
       /*} catch (PDOException $error) {
            $this->saveError($error);
            $result["id"] = 'error';
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }*/
    }
    /*update*/

    public function save_img_curl($img64, $gestion_id, $tarea_id, $pos, $nimg) {
        //try {
            //Guardar Imagen de inicio
            //$url="http://10.226.44.222:7020/imagen/tareas";
//            $url = "http://localhost/webpsi20/public/imagen/tareas";
            $acceso="\$PSI20\$";
            $clave="\$1st3m@\$";
            $hashg=hash('sha256',$acceso.$clave.$gestion_id.$tarea_id.$pos);
            $postData=array(
                'img'           =>"$img64",
                'gestion_id'    =>"$gestion_id",
                'tarea_id'      =>"$tarea_id",
                'pos'           =>"$pos",
                'nimg'          =>"$nimg",
                'hashg'         =>"$hashg"
            );
            //insertar registro de imagen enviada por curl
            $img = "P0".$pos."/g".$gestion_id."/i".$tarea_id."_".$nimg.".jpg";
            //Iniciar transaccion
            //$this->_mysql->beginTransaction();
            $sql = "INSERT INTO webpsi_officetrack.registro_imagenes
                    (
                        name,  
                        name64, 
                        conteo
                    ) 
                    VALUES 
                    (
                       ?,
                       ?,
                       ?
                    )";

            $valArray = array(
                $img,
                $img64,
                1
            );
            DB::insert($sql,$valArray);

            //Ultimo ID registrado
            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.registro_imagenes 
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $id = $lastId[0]->id;
            //$this->_mysql->commit();

            $result = Helpers::ruta(
                'imagen/tareas',
                'POST',
                $postData, false
            );

            if ($result=="Finalizado") {
                $sql = "DELETE FROM webpsi_officetrack.registro_imagenes 
                    WHERE id = ? ";

                $valArray = array(
                    $id
                );
                $reg = DB::delete($sql,$valArray);
            }
            return $result;

        /*} catch (Exception $exc) {
            $this->saveError($exc);
        }*/
    }

    public function registrarPasoUno($task_id="", $x="", $y="", $observacion="", $img=array())
    {

        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.paso_uno 
                    (
                        task_id, x, y, observacion, casa_img1, casa_img2, casa_img3
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array($task_id, $x, $y, $observacion, $img[1], $img[2], $img[3]);
            DB::insert($sql, $valArray);

            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.paso_uno
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $result["id"] = $lastId;
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso uno registrado correctamente";
            return $result;
        /*} catch (PDOException $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $this->saveError($error);
            return $result;
        }*/
    }

    public function registrarPasoDos($TaskNumber, $tap_img=array(),
                              $modem_img=array(), $tv_img=array(),
                              $problema_img=array(), $motivo, $observacion="123")
    {

        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.paso_dos 
                    (
                        task_id, tap_img1, tap_img2, tap_img3,
                        modem_img1, modem_img2, modem_img3,
                        tv_img1, tv_img2, tv_img3,
                        problema_img1, problema_img2, problema_img3,
                        motivo, observacion
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?,
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?
                    )";

            $valArray = array(
                $TaskNumber,
                $tap_img[1],
                $tap_img[2],
                $tap_img[3],
                $modem_img[1],
                $modem_img[2],
                $modem_img[3],
                $tv_img[1],
                $tv_img[2],
                $tv_img[3],
                $problema_img[1],
                $problema_img[2],
                $problema_img[3],
                $motivo,
                $observacion
            );
            DB::insert($sql,$valArray);

            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.paso_dos
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $result["id"] = $lastId;

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso dos registrado correctamente";
            return $result;
        /*} catch (PDOException $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $this->saveError($error);
            return $result;
        }*/
    }

    public function buscarMateriales($materiales,$id = ''){
        //try {
            //Iniciar transaccion
            DB::beginTransaction();

            $sql = "SELECT 
                        id, material
                    FROM
                        webpsi_officetrack.catalogo_materiales
                    WHERE
                        id IS NOT NULL ";

            $valArray = array();

            if ( $id != "" )
            {
                $sql .= " AND id = ?";
                $valArray[] = $id;
            }
            if ( $materiales != "" )
            {
                $sql .= " AND material = ?";
                $valArray[] = $materiales;
            }

            $stmt = DB::select($sql,$valArray);

            $reporte = array();

            foreach($stmt as $material){
                $reporte[] = (array)$material;
            }
            /*while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            } */

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        /*} catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }*/
    }

    public function registrarDetalleMateriales($paso_tres_id, $material_id, $utilizado, $stock, $fecha_registro, $serie){
        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar decos
            $sql = "INSERT INTO webpsi_officetrack.detalle_materiales 
                    (
                        paso_tres_id, material_id, 
                        utilizado, stock, fecha_registro,
                        serie
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array(
                $paso_tres_id,
                $material_id,
                $utilizado,
                $stock,
                $fecha_registro,
                $serie
            );
//            $stmt = $this->_mysql->prepare($sql);
//            $stmt->execute($valArray);

            DB::insert($sql, $valArray);
            
            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.detalle_materiales
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $result["id"] = $lastId;


            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Material registrado correctamente";
            return $result;
        /*} catch (PDOException $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();

            return $result;
        }*/
    }

    public function registrarPasoTres($TaskNumber, $estado,
                              $observacion, $tvadicional,
                              $final_img=array(), $firma_img, $boleta_img)
    {

        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.paso_tres 
                    (
                        task_id, estado, estado_codigo,
                        observacion, tvadicional, final_img1,
                        final_img2, final_img3,
                        firma_img, boleta_img,
                        fecha_registro
                    ) 
                    VALUES 
                    (
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?,
                       ?, ?,
                       ?
                    )";

            $estadon = $estado;
            $estadoc = "";

            $estadoarray = explode("||", $estado);
            if (count($estadoarray) > 1) {
                $estadoc = $estadoarray[0];
                $estadon = $estadoarray[1];
            }

            $valArray = array(
                $TaskNumber, $estadon, $estadoc,
                $observacion, $tvadicional, $final_img[1],
                $final_img[2], $final_img[3],
                $firma_img, $boleta_img,
                $date
            );
            $stmt = DB::insert($sql, $valArray);

            $queryLastInserted = "SELECT
                                      id
                                  FROM webpsi_officetrack.paso_tres
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $reporte["id"] = $lastId;

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso tres registrado correctamente";
            $result["data"] = $reporte;
            return $result;
        /*} catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $this->saveError($error);
            return $result;
        }*/
    }

    public function cambiarDireccion($gestion_id, $carnet,
                                     $x, $y, $direccion, $referencia){

        //try {

            //Iniciar transaccion
            DB::beginTransaction();

            //ID de tecnico
            $sql = "SELECT id FROM {$this->_db}.tecnicos WHERE carnet_tmp=?";
            $this->_data = array($carnet);
            $data = DB::select($sql,array($carnet));

            $tecnico_id = $data[0]->id;

            //INSERT en cambios_direcciones
            $sql = "INSERT INTO {$this->_db}.cambios_direcciones 
                   (gestion_id, tipo_usuario, usuario_id,
                   coord_x, coord_y, direccion, referencia)
                   VALUES (?, ?, ?, ?, ?, ?, ?)";
            $this->_data = array(
                $gestion_id, 'tec', $tecnico_id,
                $x, $y, $direccion, $referencia
            );
            DB::insert($sql,$this->_data);

            //UPDATE ultimos_movimientos
            $sql = "UPDATE {$this->_db}.ultimos_movimientos 
                   SET x = ?, y = ? 
                   WHERE gestion_id = ?";
            $this->_data = array(
                $x, $y, $gestion_id
            );
            DB::insert($sql,$this->_data);

            //UPDATE gestiones_detalles
            $sql = "UPDATE {$this->_db}.gestiones_detalles 
                   SET x = ?, y = ? 
                   WHERE gestion_id = ?";
            $this->_data = array(
                $x, $y, $gestion_id
            );
            DB::update($sql,$this->_data);


            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Direccion actualizada";
            //$result["id"] = $id;
            return $result;
        /*} catch (Exception $error) {
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }*/
        
    }

    public function asistencia($id_entrada, $fecha_asistencia, $direccion, 
                                $coor_x, $coor_y, $descripcion, $nombre_tecnico,
                                $numero_tecnico){
        //try {
            DB::beginTransaction();

            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.asistencia_tecnico 
                    (
                        id_entrada, fecha_asistencia,
                        direccion, coor_x,
                        coor_y, descripcion,
                        nombre_tecnico, numero_tecnico
                    ) 
                    VALUES 
                    (
                       ?, ?, 
                       ?, ?, 
                       ?, ?, 
                       ?, ?
                    )";

            $valArray = array(
                $id_entrada,
                $fecha_asistencia,
                $direccion,
                $coor_x,
                $coor_y,
                $descripcion,
                $nombre_tecnico,
                $numero_tecnico
            );
            DB::insert($sql,$valArray);

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso uno registrado correctamente";
            return $result;
        /*} catch (PDOException $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $this->saveError($error);

            return $result;
        }*/
    }

    private function saveError($exc)
    {
        if (is_array($exc) && isset($exc["errorInfo"]) && count($exc)==3) {
            list($code,$line,$message) = array_shift($exc);
            $error["code"] = $code;
            $error["file"] = 'mysql';
            $error["line"] = $line;
            $error["message"] = $message;
            $error["trace"] = '';
        } else if (is_array($exc)) {
            $error["code"] = isset($exc["code"]) ? $exc["code"] : '';
            $error["file"] = isset($exc["file"]) ? $exc["file"] : '';
            $error["line"] = isset($exc["line"]) ? $exc["line"] : '';
            $error["message"] = isset($exc["message"]) ? $exc["message"] : '';
            $error["trace"] = isset($exc["trace"]) ? $exc["trace"] : '';
        } elseif(is_object($exc) ){
            $error["code"] = $exc->getCode();
            $error["file"] = $exc->getFile();
            $error["line"] = $exc->getLine();
            $error["message"] = $exc->getMessage();
            $error["trace"] = $exc->getTraceAsString();
        } else {
            $error["code"] = 'code';
            $error["file"] = 'file';
            $error["line"] = 'line';
            $error["message"] = 'message';
            $error["trace"] = 'trace';
        }
        $u ='697';
        $error["usuario_id"] = isset(Auth::user()->id) ? Auth::user()->id : $u;
        $error["date"] = date("Y-m-d H:i:s");
        $error["url"] = Request::url();
        DB::table('errores')->insert(
            array($error)
        );
    }
}
