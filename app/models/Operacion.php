<?php

class Operacion extends \Base
{
    public $table = "tipo_operacion";

    public static $where =['id', 'codigo', 'nombre'];
    public static $selec =['id', 'codigo', 'nombre'];

     public static function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            
            $result =   DB::table('tipo_operacion')
            				->select('codigo as id',
            					'nombre')
            				->get();

            return $result;
        }
    }

}
