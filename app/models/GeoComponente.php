<?php

class GeoComponente extends Eloquent
{
    public static function  getFFTT( $x, $y, $cant, $tabla, $distancia)
    {
        $sql = "SELECT 
                        *,
             ROUND( 3959000 * ACOS( 
                                    COS( RADIANS(?) )  *
                                    COS( RADIANS( coord_y ) ) *
                                    COS( 
                                        RADIANS( coord_x ) - RADIANS(?)
                                        ) +
                                    SIN( RADIANS(?) ) *
                                    SIN( RADIANS( coord_y ) )
                                )
                ) AS distance
                FROM $tabla
                WHERE (ROUND( 3959000 * ACOS( 
                                    COS( RADIANS($y) )  *
                                    COS( RADIANS( coord_y ) ) *
                                    COS( 
                                        RADIANS( coord_x ) - RADIANS($x)
                                        ) +
                                    SIN( RADIANS($y) ) *
                                    SIN( RADIANS( coord_y ) )
                                )
                )) < $distancia
                ORDER BY distance
                LIMIT 0 , ?";

                /*HAVING distance < '%s'*/
        return DB::select($sql, array((float)$y,(float)$x,(float)$y, $cant));
    }


}
