<?php 
class GeoArmarioPoligono extends Eloquent
{

    public static function postArmarioPoligonoAll()
    {
        $r  =   DB::table('geo_armariopoligono')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
          ->get();
        return $r;
    }

     public static function postMdfFiltro($array)
     {
        $r  =   DB::table('geo_armariopoligono')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
     }

     public static function postArmarioFiltro($array)
     {
        $r  =   DB::table('geo_armariopoligono')
                  ->select(
                      'armario as id',
                      DB::raw("armario as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->groupBy('armario')
                  ->get();
        return $r;
     }

  
}
