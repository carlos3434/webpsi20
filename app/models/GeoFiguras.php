<?php
class GeoFiguras extends Eloquent
{
    protected $table = 'geo_figuras';


    static function listar()
    {
        $campos = DB::table('geo_figuras')
            ->select('id', 'nombre', 'estado')
            ->where('estado', '=', 1)
            ->get();
        return $campos;
    }
}