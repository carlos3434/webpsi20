<?php 
class GeoDependencia extends Eloquent
{

    public $table = 'geo_dependencias';

    public static function postDependenciaAll($array)
    {
        $r  =   DB::table('geo_dependencias')
          ->select(
              'id as id',
              DB::raw('campo'),
              DB::raw('orden'),
              DB::raw('etiqueta'),
              DB::raw('target')
          )
          ->where(
              function($query) use($array) {
                  $query->where('elemento_id', '=', $array['elementoid']);
              }
          )
          ->orderBy('orden', 'asc')
          ->get();
        return $r;
    }

  
}
