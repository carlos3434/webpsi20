<?php

class UsuarioExoneracion extends \Eloquent {
	protected $fillable = ['usuario_id','fecha_inicio','fecha_fin','observacion'];
	public $table = 'usuario_exoneracion';


	public function getAll(){
			$query=  DB::table('usuario_exoneracion as ue')
                        ->select(
                        	'ue.fecha_inicio',
                            'ue.fecha_fin',
                          	'ue.id',
                          	'ue.observacion',
                          	'ue.estado',
                          	'ue.usuario_id'
                        )
                        ->where(function($query){
                        		$query->where('ue.usuario_id',Input::get('usuario_id'));
                            }
                        )                        
                        ->get();
        return (count($query)>0) ? $query : false;
	}
}