<?php
class GeoCampos extends Eloquent
{
    protected $table = 'geo_campos';

    /**
     * Geo_Tabla_Campos relationship
     */
    public function geoTablaCampos()
    {
        return $this->belongsToMany('GeoTablaCampos');
    }

    static function listar()
    {
        $campos = DB::table('geo_campos')
            ->select('id', 'nombre') // al agregar estado. el combo sale check en los de estado=1
            ->where('estado','=',1)
            ->get();
        return $campos;
    }

    public static function insertarCampos($idTabla, $campos)
    {
        try
        {
            DB::beginTransaction();
            $lastId = $campos[count($campos) - 1]["id"];
            foreach($campos as $campo)
            {
                if($lastId == $campo["id"]) $target = 1;
                else $target = 0;

                DB::table("geo_tabla_campos")
                    ->insert(
                        [
                            "tabla_id"  => $idTabla,
                            "campo_id"  => $campo["id"],
                            "orden"     => $campo["index"],
                            "target"    => $target,
                            "estado"    => 1,
                            "usuario_created_at" => Auth::id(),
                            "created_at" => date("Y-m-d h:i:s")
                        ]
                    );

                DB::commit();
            }
        }catch (Exception $x)
        {
            DB::rollback();
        }
    }
}