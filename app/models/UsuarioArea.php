<?php

class UsuarioArea extends \Eloquent {
    use DataViewer;
    protected $fillable = ['usuario_id','area_id','estado'];
    public $table = 'usuario_area';

	public function getAll(){
			$query=  DB::table('usuario_area as ua')
						->join('usuarios as u',
                           'ua.usuario_id',
                           '=',
                           'u.id'
                        )
                        ->join('areas as a',
                        	'a.id',
                        	'=',
                        	'ua.area_id'
                        )
                        ->Leftjoin('grupos as g', 'g.id', '=', 'ua.grupo_id')
                        ->Leftjoin('quiebres as q', 'q.id', '=', 'ua.quiebre_id')
                        ->select(
                        	'ua.id as usuario_area_id',
                            'u.id as id',
                            DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) AS nombre'),
                            'u.nombre as nombrep', 
                            'u.apellido',
                            'a.nombre as area', 
                            'ua.estado as ua_estado',
                            DB::raw('IFNULL(g.nombre, "") as grupo'),
                            DB::raw('IFNULL(q.nombre, "") as quiebre')
                        )
                        ->where(function($query){
                                if(Input::has('grupo')){
                                    $query->whereRaw('(ua.grupo_id = 0 or ua.grupo_id="" or ua.grupo_id IS NULL)');
                                }
                            }
                        )                        
                        ->get();
        return (count($query)>0) ? $query : false;
	}

    public function getAllAvailable(){
         $query = DB::table('usuario_area as ua')
                    ->Leftjoin('usuario_horario as uh', 'ua.usuario_id', '=', 'uh.usuario_id')
                    ->join('usuarios as u','ua.usuario_id','=','u.id')
                    ->select(
                        'ua.usuario_id as id',
                        DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) AS nombre'),
                        DB::raw(
                            '((SELECT cantidad FROM pre_temporales_admin where nombre="maxasig") -(SELECT COUNT(id) from pre_temporales  where  estado_pretemporal_id=1 and usuario_id=ua.usuario_id )) as libres'
                        )
                    )
                    ->where(function ($query){
                        $query->where('ua.estado',1);
                        $query->where('ua.area_id',51);
                        $query->whereRaw('uh.dia_id=  (WEEKDAY(CURDATE()) + 1)');         
                    })                    
                    ->groupBy('ua.usuario_id')
                    ->having('libres', '>', 0)
                    ->get();
        /*end active and valuable users*/
        return (count($query)>0) ? $query : false;
    }

    public function getAsignacionesbyUser(){
        
        /*header*/
        $cabecera = [];
        $date = strtotime("-7 day");
        $datefin = strtotime("+1 day");
        $quiebre = '';
        if(Input::get('quiebre_id')){
            $quiebre = " AND p.quiebre_id IN (".implode(',', Input::get('quiebre_id')).")";
        }

        $fechaIni =date('Y-m-d', $date);
        $fechaFin =date('Y-m-d',$datefin);
        $f_fecha = " DATE(p.fecha_asignacion_usuario) BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "' $quiebre";         

        if(Input::get('fecha')){
            $fecha = Input::get('fecha');
            list($fechaIni, $fechaFin) = explode(" - ", $fecha);
            $f_fecha = " DATE(p.fecha_asignacion_usuario) BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "' $quiebre";            
            $fechaIni_ = strtotime($fechaIni);
            $fechaFin_ = strtotime($fechaFin);
        }else{
            $fechaIni_ = strtotime("-7 day");
            $fechaFin_ = strtotime($fechaFin);        
        }

        $fecha = date_create($fechaIni);
        $n = 0;$left = "";$count = "";$left2 = "";$atendidos = "";
        for ($i = $fechaIni_; $i <= $fechaFin_; $i += 86400) {
            $n++;
            $count.=",COUNT(p$n.id) as a$n";
            $atendidos.=",COUNT(pa$n.id) as at$n";
            $left.=" LEFT JOIN pre_temporales p$n ON p$n.id=p.id and DATE(p.fecha_asignacion_usuario)= STR_TO_DATE('" . date("Y-m-d", $i) . "','%Y-%m-%d') $quiebre";
            $left2.=" LEFT JOIN pre_temporales pa$n ON pa$n.id=p.id and DATE(p.fecha_asignacion_usuario)= STR_TO_DATE('" . date("Y-m-d", $i) . "','%Y-%m-%d') AND p.estado_pretemporal_id!=1 $quiebre";
            array_push($cabecera, date_format($fecha, 'Y-m-d'));
            date_add($fecha, date_interval_create_from_date_string('1 days'));
        }
        array_push($cabecera, 'total');
        /*end header*/

        /*body*/
        $sql = "SELECT CONCAT_WS(' ',u.nombre,u.apellido)as usuario, COUNT(p.id) as total $count $atendidos
                FROM usuario_area ua 
                INNER JOIN usuarios u ON u.id=ua.usuario_id 
                LEFT JOIN pre_temporales p ON p.usuario_id=ua.usuario_id and $f_fecha   
                $left 
                $left2 
                GROUP BY ua.usuario_id";
        /*end body*/

        $data['cabecera'] = $cabecera;
        $data['data'] = (array)DB::select($sql);
        return $data;

    }

}