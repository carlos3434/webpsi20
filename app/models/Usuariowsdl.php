<?php 

class Usuariowsdl extends \Eloquent
{
    protected $table = "usuario_wsdl";
    
    public static function getAuth($request)
    {
        if(!Usuariowsdl::validateDate($request->user->now)){
            return 0;
        }

        $usuario=Usuariowsdl::where('login',$request->user->login)
        ->where('company',$request->user->company)
        ->first();

        if($usuario){
            if(md5($request->user->now.$usuario->password)==$request->user->auth_string) {
                return $usuario;
            }
        }
        return 0;

    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        $fecha=date_format($d, 'Y-m-d');
        return $d && $d->format($format) == $date && $fecha==date('Y-m-d');
    }

}