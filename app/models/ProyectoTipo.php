<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
 * 
 */
 class ProyectoTipo extends \Eloquent
{
    public $table="proyecto_tipo";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    //protected $fillable = array('*');
    protected $guarded = array();

    /**
     * GestionEdificio relationship
     */
    public function gestionEdificio()
    {
        return $this->hasMany('GestionEdificio');
    }

}
