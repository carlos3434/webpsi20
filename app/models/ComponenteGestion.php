<?php

class ComponenteGestion extends Eloquent
{

    protected $table = 'componente_gestion';
    protected $guarded =[];
    
    /**
     * Obtiene componentes de una orden gestionada
     * 
     * @param type $gestionId
     * @return boolean
     */
    public function getComponenteId($gestionId){
        try {
            $sql = "SELECT
                    cp.cod_componente AS idEquipo,
                    cp.desc_componente AS tipoEquipo,
                    count(*) AS cantidad
                FROM
                    psi.cat_componentes AS cp
                        INNER JOIN psi.componente_gestion AS cg
                            ON cp.cod_componente = cg.componente_id
                WHERE
                    cg.gestion_id = $gestionId
                group by cp.cod_componente";
            
            $result["data"] = DB::select($sql);
            $result["error"] = false;
            return $result;
        } catch (Exception $exc) {
            $result["data"] = array();
            $result["error"] = $exc->getMessage();
            return $result;
        }
    }
    
    /**
     * Obtiene componentes de una orden temporal
     * 
     * @param type $actu
     * @return boolean
     */
    public function getComponenteTmp($actu){
        try {
            $sql = "SELECT
                        cp.cod_componente AS idEquipo,
                        cp.desc_componente AS tipoEquipo
                    FROM
                        psi.cat_componentes AS cp
                        INNER JOIN 
                            schedulle_sistemas.prov_pen_catv_pais_componentes 
                            AS pp
                                ON cp.cod_componente = pp.codigo_de_componente
                    WHERE
                        pp.codigo_req = $actu";
            
            $result["data"] = DB::select($sql);
            $result["error"] = false;
            return $result;
        } catch (Exception $exc) {
            $result["data"] = array();
            $result["error"] = $exc->getMessage();
            return $result;
        }
    }
    
    /**
     * Graba componentes catv
     * 
     * @param type $gestionId
     * @param type $data
     * @return boolean
     */
    public function saveComponente($gestionId, $data){
            $user  = Auth::id() ? Auth::id() : 952;
            foreach ($data as $val) {
                DB::table($this->table)->insert(
                    array(
                        'gestion_id' => $gestionId,
                        'componente_id' => $val->idEquipo,
                        'created_at' => date("Y-m-d H:i:s"),
                        'usuario_created_at' => $user,
                    )
                );
            }
            $result["data"] = array();
            $result["error"] = false;
            return $result;
    }

    /**
     * Obtiene componentes de una orden temporal
     * 
     * @return boolean
     */


    public function getCodReqComponenteFaltante2(){
        try {
            $sql = "SELECT DISTINCT(sp.codigo_req) codigo_req, gd.aid, 
                  gd.gestion_id, gd.mdf, gd.actividad_tipo_id, em.nombre as empresa,
                  at.label, a.nombre actividad
                  FROM schedulle_sistemas.prov_pen_catv_pais_componentes sp
                  INNER JOIN psi.gestiones_detalles gd
                  ON gd.codactu = sp.codigo_req AND gd.componente = 0
                  INNER JOIN actividades_tipos at
                  ON at.id = gd.actividad_tipo_id
                  AND gd.aid IS NOT NULL
                  INNER JOIN actividades a
                  ON a.id = at.actividad_id
                  INNER JOIN psi.gestiones_movimientos gm
                  ON gm.aid = gd.aid AND gm.estado_ofsc_id IN (1,2)
                  LEFT JOIN psi.empresas em ON gm.empresa_id = em.id";

            $result["data"] = DB::select($sql);
            $result["error"] = false;
            return $result;
        } catch (Exception $exc) {
            $result["data"] = array();
            $result["error"] = $exc->getMessage();
            return $result;
        }
    }

    public function getArrayformat($array)
    {
        $equipment = "";
        $equipment .= "<XA_EQUIPMENT>";
        foreach ($array as $value) {
            $equipment .= "<Equipo>";
            $equipment .= "<IdEquipo>".$value->idEquipo."</IdEquipo>";
            $equipment .= "<TipoEquipo>".$value->tipoEquipo."</TipoEquipo>";
            $equipment .= "<Cantidad>".$value->cantidad."</Cantidad>";
            $equipment .= "</Equipo>";
        }
        $equipment .= "</XA_EQUIPMENT>";

        return $equipment;
    }

    /**
    * Si no encuentra componente por REQUERIMIENTO (codactu) busque
    * por ORDEN DE TRABAJO (ot)
    *
    */
    public function getComponenteFaltanteGrupal($codigo_req)
    {
      try {
          $result["data"] = DB::table('schedulle_sistemas.prov_pen_catv_pais_componentes as sp')
                          ->join('cat_componentes as cc', 'cc.cod_componente', '=', 'sp.codigo_de_componente')
                          ->select('sp.codigo_req', 'cc.cod_componente as idEquipo', 'cc.desc_componente as tipoEquipo', DB::raw('COUNT(*) as cantidad'))
                          ->where('sp.codigo_req', '=', $codigo_req)
                          ->groupBy('cc.cod_componente')
                          ->get();

            if (count($result["data"]) <=0) {
              $requerimientoDetalle = GestionDetalle::where("codactu", "=", $codigo_req)->get();
              if (isset($requerimientoDetalle[0]))
                $ordenTabajo = $requerimientoDetalle[0];
              else
                $ordenTabajo = [];

              if (count ($ordenTabajo) > 0 ) {
                $result["data"] = DB::table("schedulle_sistemas.prov_pen_cms_catv_pais_componentes as cp")
                ->leftJoin("cat_componentes as cc", "cc.cod_componente", "=", 
                  "cp.codcomp")
                ->select("cp.codordtrab as orden", "cc.cod_componente as idEquipo",
                  "cc.desc_componente as tipoEquipo", DB::raw("COUNT(*) as cantidad"))
                ->where("cp.codordtrab", "=", $ordenTabajo->orden_trabajo)
                ->groupBy('cc.cod_componente')
                ->get();
              }
              
            }
          $result["error"] = false;
          return $result;
      } catch (Exception $exc) {
          $result["data"] = array();
          $result["error"] = $exc->getMessage();
          return $result;
      }
    }
}
