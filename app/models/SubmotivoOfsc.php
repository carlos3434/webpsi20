<?php

class SubmotivoOfsc extends Base
{
    public static $where =['id', 'codigo_ofsc', 'estado'];
    public static $selec =['id', 'codigo_ofsc', 'estado'];
    
    public function __construct()
    {
         $this->table = "submotivos_ofsc";
    }
}
