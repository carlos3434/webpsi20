<?php

class SubmotivoOfsc extends Base
{
    public static $where =['id', 'codigo_ofsc', 'estado', 'motivo_ofsc_id', 'codigo_legado'];
    public static $selec =['id', 'codigo_ofsc', 'estado', 'motivo_ofsc_id', 'codigo_legado'];
    
    public function __construct()
    {
         $this->table = "submotivos_ofsc";
    }

    public function getSubMotivoOfsc() // Lista de MotivosOfc añadido
    {
        $submotivoOfsc = Auth::id();
        $perfilId = Session::get('perfilId');
        $submotivos_ofsc =  DB::table('submotivos_ofsc AS sm')
                ->join(
                    'motivos_ofsc AS m', 'm.id', '=', 'motivo_ofsc_id'
                )
                ->leftJoin(
                    'actividades AS a',
                    'a.id', '=', 'm.actividad_id'
                )
                ->leftJoin(
                    'estados_ofsc as eo',
                    'eo.id', '=', 'm.estado_ofsc_id'
                )
                ->select(
                    'm.descripcion as descripcion_motivoofsc',
                    'm.codigo_ofsc as codigoofsc_motivoofsc',
                    'm.codigo_legado as codigo_legado_motivoofsc',

                    'a.nombre as actividad',
                    DB::raw('case 
                            when m.estado_ofsc = "T" then "TECNICO" 
                            when m.estado_ofsc = "C" then "COMERCIAL" 
                            when m.estado_ofsc = "S" then "SOPORTE DE CAMPO" 
                            else ""
                            end as estado_ofsc'),
                    DB::raw('case 
                            when tipo_legado = 1 then "CMS" 
                            when tipo_legado = 2 then "GESTEL" 
                            else ""
                            end as tipo_legado'),
                    'eo.nombre as nombre_ofsc',
                    DB::raw('if(tipo_tratamiento_legado = 1, "AUTOMATICO", "SIGUE FLUJO") as tipo_tratamiento_legado_descripcion'),

                    'm.estado as estado_motivoofsc',
                    'sm.codigo_ofsc as codigoofsc_submotivoofsc',
                    'sm.codigo_legado as codigo_legado_submotivoofsc',
                    'sm.descripcion as descripcion_submotivoofsc',
                    'sm.estado as estado_submotivoofsc'
                );

        $submotivos_ofsc = $submotivos_ofsc->get();

        return $submotivos_ofsc;
    }



}
