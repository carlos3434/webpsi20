<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
 *  Mensaje
 */
class Mensaje extends \Eloquent
{
    public $table="mensajes";
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    public static function crear( $id, $data )
    {
        $body= isset($data['body'])? $data['body']: '';
        $message = new Mensaje;
        $message['app_host'] = isset($data['app_host'])? $data['app_host'] : '';
        $message['app_port'] = isset($data['app_port'])? $data['app_port']: '';
        $message['app_url'] = isset($data['app_url'])? $data['app_url']: '';
        $message['message_id'] = isset($data['message_id'])? $data['message_id']: '';
        $message['company_id'] = isset($data['company_id'])? $data['company_id']: '';
        $message['address'] = isset($data['address'])? $data['address']: '';
        $message['send_to'] = isset($data['send_to'])? $data['send_to']: '';
        $message['subject'] = isset($data['subject'])? $data['subject']: '';
        $message['estado'] = 'sent';
        $message['body'] = trim($body);
        $message['job_id'] = $id;
        $message['url_recibido'] = isset($data['url_recibido'])? $data['url_recibido']: '';
        $message->save();
        return $message->id;
    }

    public static function drop($idMensaje)
    {
        $mensaje = Mensaje::Where('message_id', $idMensaje)->first();

        if (isset($mensaje)) {
            $mensaje->delete();
        } else {
            return  array(
                    'code'=>"NOT FOUND",//OK, NOT found, ERROR
                    'desc'=>''
                    );
        }
        return  array(
                'code'=>"OK",
                'desc'=>''
                );
    }

    public static function getStatus($idMensaje)
    {
        $mensaje = Mensaje::Where('message_id', $idMensaje)->first();
        if (isset($mensaje)) {
            return  array(
                    'code'=>"OK",
                    'desc'=>$mensaje->estado
                    );
        } else {
            return  array(
                    'code'=>"NOT FOUND",//OK, NOT found, ERROR
                    'desc'=>''
                    );
        }
    }
}
