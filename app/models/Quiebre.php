<?php

class Quiebre extends \Eloquent
{
    protected $table = "quiebres";
    protected $guarded =[];
    /**
     * Celula relationship
     */
    public function celulas()
    {
        return $this->belongsToMany('Celula');
    }
    /**
     * Actividad relationship
     */
    public function actividades()
    {
        return $this->belongsToMany('Actividad');
    }
    /**
     * Actividad relationship
     */
    public function motivos()
    {
        return $this->belongsToMany('Motivo');
    }
    /**
     * QuiebreGrupo relationship
     */
    public function quiebregrupos()
    {
        return $this->belongsTo('QuiebreGrupo','quiebre_grupo_id','id');
    }
    public function solicitudTecnicaUltimo()
    {
        return $this->belongsTo('Legados\models\SolicitudTecnicaUltimo');
    }
    public static function getQuiebresAllOfficeTrack()
    {
        $query = "  SELECT q.id, q.nombre, q.apocope
                    FROM quiebres q
                    INNER JOIN actividad_quiebre aq ON  q.id=aq.quiebre_id
                    INNER JOIN actividades a ON aq.actividad_id=a.id
                    WHERE q.estado=1 AND aq.estado=1
                    AND a.estado=1 
                    GROUP BY q.id
                    ORDER BY q.nombre";

        $res = DB::select($query);

        return $res;
    }

    public static function Listbygroup(){
        $quiebres=  DB::table('quiebres')
                            ->select('id', 'nombre')
                            ->where('estado', '=', '1')
                            ->where('quiebre_grupo_id',10)
                            ->orderBy('nombre')
                            ->get();
        return $quiebres;
    }

    public function parametros() {
        return $this->hasMany('Parametro','quiebre_id','id');
    }
}
