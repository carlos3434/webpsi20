<?php
class GeoTerminalf extends Eloquent
{
    public static function postGeoTerminalfAll()
    {
        $r  =   DB::table('geo_terminalf')
          ->select(
              'zonal as id',
              DB::raw('zonal as nombre')
          )
         ->groupBy('zonal')
         ->get();
        return $r; 
    }

    public static function postMdfFiltro($array)
    {
        $r  =   DB::table('geo_terminalf')
                  ->select(
                      'mdf as id',
                      DB::raw("mdf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->groupBy('mdf')
                  ->get();
        return $r;
    }

    public static function postArmarioFiltro($array)
    {
        $r  =   DB::table('geo_terminalf')
                  ->select(
                      'armario as id',
                      DB::raw("armario as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->groupBy('armario')
                  ->get();
        return $r;
    }

    public static function postTerminalfFiltro($array)
    {
        $r  =   DB::table('geo_terminalf')
                  ->select(
                      'terminalf as id',
                      DB::raw("terminalf as nombre ")
                  )
                  ->whereIn("zonal", $array['zonal'])
                  ->whereIn("mdf", $array['mdf'])
                  ->whereIn("armario", $array['armario'])
                  ->groupBy('terminalf')
                  ->get();
        return $r;
    }
}