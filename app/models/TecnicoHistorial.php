<?php

class TecnicoHistorial extends \Eloquent
{
    public $table = "tecnicos_historial";

    public static function boot()
    {
        parent::boot();

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );
    }
}