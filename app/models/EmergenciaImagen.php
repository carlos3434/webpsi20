<?php

class EmergenciaImagen extends \Eloquent
{
    protected $guarded =[];
    protected $table = "emergencia_imagen";
    /**
     * relationship to Emergencia
     */
    public function emergencia()
    {
        return $this->belongsTo('Emergencia');
    }
}
