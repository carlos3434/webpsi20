<?php 
class EstadoOfsc extends Eloquent
{

    protected $table = 'estados_ofsc';
    protected $guarded =[];
    public static $where =['id', 'nombre', 'estado','name'];
    public static $selec =['id', 'nombre', 'estado','name'];

    public function ultimo()
    {
        return $this->hasMany('Legados\models\SolicitudTecnicaUltimo');
    }
    public function getNameCortoAttribute(){
        return substr($this->name , 0, 5);
    }
    public static function Listar()
    {
        $r = DB::table('estados_ofsc')
                        ->select(
                            'id', 
                            'nombre' 
                        )
                        ->where(
                            function($query)
                            {
                                if ( Input::has('estado') ) {
                                    $query->where(
                                        'estado', '=', Input::has('estado')
                                    );
                                }
                            }
                        )
                        ->get();
        return $r;
    }

}
