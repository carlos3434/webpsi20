<?php

class TecnicoLogCambio extends \Eloquent {
	protected $fillable = [];
    
    protected $table ="tecnico_log_cambios";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );

    }
}