<?php 
class GeoProyectoCapaElemento extends Eloquent
{

     public static function getAgregarCapaElemento($values)
     {
        $sql = "insert into geo_proyecto_capa_elemento(capa_id,detalle,
                tabla_detalle_id,orden,
                borde,grosorlinea,fondo,opacidad,tabla_id,figura_id)
                values ".$values;
        return DB::insert($sql);
     }

    public static function getUpdateElemento(array $data )
    {
        DB::table('geo_proyecto_capa_elemento')
                    ->where('id', $data[0])
                    ->update(
                        array(
                        'borde' => $data[1],
                        'grosorlinea'=> $data[2],
                        'fondo' => $data[3],
                        'opacidad' => $data[4]
                        )
                    );

        DB::table('geo_proyecto')
                    ->where('id', $data[5])
                    ->update(
                        array(
                        'usuario_updated_at'=> $data[6],
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); 

          return '1';
    }

    public static function getDeleteElemento(array $data )
    {

       DB::table('geo_proyecto_capa_elemento')
       ->where('id', '=', $data[0])->delete();

        DB::table('geo_proyecto')
                    ->where('id', $data[1])
                    ->update(
                        array(
                        'usuario_updated_at'=> $data[2],
                        'updated_at'=> date('Y-m-d H:i:s')
                        )
                    ); 

          return '1';
    }


}
