<?php 
class GeoProyectoCapaElemento extends Eloquent
{
    protected $table = "geo_proyecto_capa_elemento";
    protected $guarded =[];

     public static function agregar($values)
     {  
        $query = new GeoProyectoCapaElemento($values);
        $query->save();
        return $query;
     }

    public static function actualizar()
    {
        $array=Input::all();
        unset($array['id']);

        GeoProyectoCapaElemento::where('id', Input::get('id'))
                                ->update($array); 

          return '1';
    }

}
