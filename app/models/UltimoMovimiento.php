<?php

class UltimoMovimiento extends Eloquent
{

    public $table = 'ultimos_movimientos';
    
    /**
     * Actualiza direccion y 
     * mantiene la anterior en campos "previo"
     * 
     * @param String $x Longitud
     * @param String $y Latitud
     * @param String $direccion direccion
     * @param Integer $gestionId ID de gestion
     * @return boolean
     */
    public static function actualizar_direccion($x, $y, $direccion, $gestionId)
    {
        $setStr = "";
        if (trim($x)!='' and trim($y)!='') {
            $setStr .= "x_previo = x, y_previo = y,";
        }
        
        if (trim($direccion)!='') {
            $setStr .= "direccion_previo = direccion_instalacion,";
        }
        
        $setStr = substr($setStr, 0, strlen($setStr)-1);
        
        try {
            $sql = "UPDATE 
                        ultimos_movimientos 
                    SET 
                        $setStr 
                    WHERE 
                        gestion_id = ?";            
            $result["data"] = DB::update($sql, array($gestionId));
            $result["estado"] = true;
            return $result;
        } catch (Exception $exc) {
            $result["data"] = array();
            $result["estado"] = false;
            return $result;
        }
    }

    public static function actualizar_movimiento($codactu, $datos)
    {
        $datos["direccion"] = $datos['address'];
        $datos["telefono"] = $datos['phone'];
        $datos["celular"] = $datos['XA_CONTACT_PHONE_NUMBER_2'];
        $datos["x"] = strval($datos['coordx']);
        $datos["y"] = strval($datos['coordy']);
        DB::table('gestiones_detalles')
            ->where('codactu', $codactu)
            ->update(
                array(
                    //'x'=>$datos['x'], //en actualizar_direccion
                    //'y'=>$datos['y'],
                    'fonos_contacto'=>$datos['telefono']."|".$datos['celular'],
                    'direccion_instalacion'=>$datos['direccion'],
                    'fftt'=>$datos["fftt"],
                    'usuario_updated_at' => Auth::user()->id,
                    'updated_at' => date('Y-m-d h:i:s', time())
                )
            );
            
        DB::table('ultimos_movimientos')
            ->where('codactu', $codactu)
            ->update(
                array(
                    //'x'=>$datos['x'],
                    //'y'=>$datos['y'],
                    'fonos_contacto'=>$datos['telefono']."|".$datos['celular'],
                    'direccion_instalacion'=>$datos['direccion'],
                    'fftt'=>$datos["fftt"],
                    'usuario_updated_at' => Auth::user()->id,
                    'updated_at' => date('Y-m-d h:i:s', time())
                )
            );
    }

}
