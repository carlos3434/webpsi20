<?php

class UltimoMovimiento extends Eloquent
{

    protected $table = 'ultimos_movimientos';
    
    /**
     * Actualiza direccion y 
     * mantiene la anterior en campos "previo"
     * 
     * @param String $x Longitud
     * @param String $y Latitud
     * @param String $direccion direccion
     * @param Integer $gestionId ID de gestion
     * @return boolean
     */
    
    public static $where =['id', 'codactu','id_solicitud_tecnica'];
    public static $selec =['id', 'codactu','id_solicitud_tecnica'];

    public function horario()
    {
        return $this->belongsTo('Horario');
    }
    public function estadoOfsc()
    {
        return $this->belongsTo('EstadoOfsc');
    }
    public function tecnico()
    {
        return $this->belongsTo('Tecnico');
    }
    public static function actualizar_direccion($x, $y, $direccion, $gestionId)
    {
        $setStr = "";
        if (trim($x)!='' and trim($y)!='') {
            $setStr .= "x_previo = x, y_previo = y,";
        }
        
        if (trim($direccion)!='') {
            $setStr .= "direccion_previo = direccion_instalacion,";
        }
        
        $setStr = substr($setStr, 0, strlen($setStr)-1);
        
        try {
            $sql = "UPDATE 
                        ultimos_movimientos 
                    SET 
                        $setStr 
                    WHERE 
                        gestion_id = ?";            
            $result["data"] = DB::update($sql, array($gestionId));
            $result["estado"] = true;
            return $result;
        } catch (Exception $exc) {
            $result["data"] = array();
            $result["estado"] = false;
            return $result;
        }
    }

    public static function actualizar_movimiento()
    {
        $datos=array();
        $datos=Input::all();
        $datos['fonos_contacto']=Input::get('fono2','')."|".
        Input::get('fono3','')."|".Input::get('fono4','');
        $codactu=Input::get('codactu');

        DB::table('gestiones_detalles')
            ->where('codactu', $codactu)
            ->update(
                array(
                    //'x'=>$datos['x'], //en actualizar_direccion
                    //'y'=>$datos['y'],
                    'nombre_cliente' => $datos['nombre_contacto'],
                    'telefono'=>$datos['phone'],
                    'fono1'=>$datos['cell'],
                    'fonos_contacto'=>$datos['fonos_contacto'],
                    'direccion_instalacion'=>$datos['address'],
                    'fftt'=>$datos["fftt"],
                    'observacion'=>$datos["observacion"],
                    'usuario_updated_at' => Auth::id(),
                    'updated_at' => date('Y-m-d h:i:s', time())
                )
            );
            //falta observacion
        DB::table('ultimos_movimientos')
            ->where('codactu', $codactu)
            ->update(
                array(
                    //'x'=>$datos['x'],
                    //'y'=>$datos['y'],
                    'nombre_cliente_critico' => $datos['nombre_contacto'],
                    'telefono'=>$datos['phone'],
                    'fono1'=>$datos['cell'],
                    'fonos_contacto'=>$datos['fonos_contacto'],
                    'direccion_instalacion'=>$datos['address'],
                    'fftt'=>$datos["fftt"],
                    'observacion'=>$datos["observacion"],
                    'usuario_updated_at' => Auth::id(),
                    'updated_at' => date('Y-m-d h:i:s', time())
                )
            );
    }

}
