<?php

class Ayuda extends \Eloquent
{
    protected $guarded =[];
    protected $table = "ayudas";
    /**
     * columnas para el DataViewer
     */
    public static $columns = [
        'id',
        'zona',
        'x',
        'y',
        'n_voluntarios',
        'objetivo',
        'organizacion',
        'color',
        'created_at',
        'updated_at',
        'usuario_created_at',
        'usuario_updated_at',
    ];
    /**
     * columnas para la creacion
     */
    public static $create = [
        'zona'=>'',
        'x'=>'',
        'y'=>'',
        'n_voluntarios'=>'',
        'objetivo'=>'',
        'organizacion'=>'',
        'color'=>'',
        'created_at'=>'',
        'updated_at'=>'',
        'usuario_created_at',
        'usuario_updated_at'=>''
    ];
    /**
     * columnas para la actualizacion
     */
    public static $update = [
        'zona'=>'',
        'x'=>'',
        'y'=>'',
        'n_voluntarios'=>'',
        'objetivo'=>'',
        'organizacion'=>'',
        'color'=>'',
        'created_at'=>'',
        'updated_at'=>'',
        'usuario_created_at',
        'usuario_updated_at'=>''
    ];
    /**
     * relationship to EmergenciaImagen
     */
    public function ayudaImagen()
    {
        return $this->hasMany('AyudaImagen');
    }
    /**
     * Scope para obtener  imagenes.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetImagenes($query)
    {
        return $query->join(
            'ayuda_imagen as i',
            'i.ayuda_id',
            '=',
            'ayudas.id'
        );
    }
    public function scopeSearchPaginateAndOrder($query)
    {
        $request = app()->make('request');

        $v = Validator::make($request->only([
            'column', 'direction', 'per_page','search'
        ]), [
            'column' => 'required|alpha_dash|in:'.implode(',', Ayuda::$columns),
            'direction' => 'required|in:asc,desc',
            'per_page' => 'integer|min:1',
            'search' => 'max:255'
        ]);

        if($v->fails()) {
            return $v->messages();
            //throw new \Illuminate\Validation\ValidationException($v);
        }
//dd($request->get('per_page'));
        return $query
            ->select(Ayuda::$columns)
            ->orderBy($request->get('column'), $request->get('direction'))
            ->where(function($query) use ($request) {
                if($request->has('search')) {
                    $query->where('zona', 'LIKE', '%'.$request->get('search').'%');
                    $query->orWhere('objetivo', 'LIKE', '%'.$request->get('search').'%');
                    $query->orWhere('organizacion', 'LIKE', '%'.$request->get('search').'%');
                }
            })
            ->paginate($request->get('per_page'));
    }
}