<?php 
class GeoProyectoCapa extends Eloquent
{

    public static function getAgregarCapa($values)
    {
        $sql = "insert into geo_proyecto_capa (nombre,proyecto_id)
                values ".$values;
        return DB::insert($sql);
    }

    public static function getCargarCapa($proyectoid)
    {
       
         $sql = "select gpc.id as id,gpc.nombre as nombre,gp.id as idproyecto,
               gp.nombre as proyecto
                from geo_proyecto_capa gpc
               INNER JOIN geo_proyecto gp
               on gpc.proyecto_id=gp.id where gpc.estado=1";
        if ($proyectoid!='') {
         $sql .= "  and gp.id='".$proyectoid."' ";
        }
               
        $res = DB::select($sql);

        return $res;
        
    }

    public static function getListarCapasDetalle($capaid)
    {
       
        $sql = "select ge.id as id,ge.figura_id,ge.tabla_id,ge.tabla_detalle_id,
                 ge.detalle, coord, dato, ge.borde,ge.grosorlinea,
                 if(figura_id='3',SUBSTR(ge.fondo,2),ge.fondo) as fondo,
                 ge.opacidad, ge.orden 
                 from geo_proyecto_capa_elemento ge
                 INNER JOIN geo_proyecto_capa gc
                 on gc.id=ge.capa_id 
                 INNER JOIN geo_tabla_detalle td
                 ON ge.tabla_detalle_id=td.id
                 where ge.capa_id =".$capaid;
               //where gp.nombre='proyectotest'";
               
        $res = DB::select($sql);

        return $res;
        
    }

    public static function getDeleteCapa(array $datas)
    {

        DB::table('geo_proyecto_capa')
        ->join('geo_proyecto', 'geo_proyecto_capa.proyecto_id', '=', 'geo_proyecto.id')
        ->where('geo_proyecto_capa.id', $datas[0])
        ->update(
            array(
            'geo_proyecto_capa.estado'=> '0',
            'geo_proyecto.usuario_updated_at'=> $datas[1],
            'geo_proyecto.updated_at'=> date('Y-m-d H:i:s'),
            )
        ); 

          return '1';
    }

    public static function getUpdatecapa(array $datas)
    {

        DB::table('geo_proyecto')
        ->join('geo_proyecto_capa', 'geo_proyecto_capa.proyecto_id', '=', 'geo_proyecto.id')
        ->join('geo_proyecto_capa_elemento', 'geo_proyecto_capa_elemento.capa_id', '=', 'geo_proyecto_capa.id')
                ->where('geo_proyecto_capa.id', $datas[0])
                ->update(
                    array(
                    'geo_proyecto_capa_elemento.borde'=> $datas[1],
                    'geo_proyecto_capa_elemento.fondo'=> $datas[2],
                    'geo_proyecto_capa_elemento.grosorlinea'=> $datas[3],
                    'geo_proyecto_capa_elemento.opacidad'=> $datas[4],
                    'geo_proyecto.usuario_updated_at'=> $datas[5],
                    'geo_proyecto.updated_at'=> date('Y-m-d H:i:s'),
                    )
                ); 

          return '1';
    }
}
