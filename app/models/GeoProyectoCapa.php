<?php 
class GeoProyectoCapa extends Eloquent
{
    protected $table = "geo_proyecto_capa";
    protected $guarded =[];
    
    public static function getAgregarCapa($values)
    {
      $query = new GeoProyectoCapa($values);
      $query->save();    
      return $query;
    }

    public static function cargar($proyectoid)
    {
        $where='';
        if ($proyectoid!='') {
         $where .= "  and gp.id='".$proyectoid."' ";
        }
        DB::select("SET GLOBAL group_concat_max_len=4096");

         $sql = "select gpc.id as id,gpc.nombre as nombre,gp.id as idproyecto,
               gp.nombre as proyecto,GROUP_CONCAT(ge.detalle SEPARATOR ',') AS elementos,
               ge.figura_id
                from geo_proyecto_capa gpc
               INNER JOIN geo_proyecto gp
               on gpc.proyecto_id=gp.id 
               LEFT JOIN geo_proyecto_capa_elemento ge
               on gpc.id=ge.capa_id
               where gpc.estado=1 and ge.estado=1 ".$where."
                GROUP BY gpc.id";
        
        $res = DB::select($sql);

        return $res;
        
    }

    public static function listardetalle($capaid)
    {
       
     $sql = "select ge.id as id,ge.figura_id,ge.tabla_id,
                 ge.detalle, coord, dato, ge.borde,ge.grosorlinea,
                 fondo,
                 ge.opacidad, ge.orden, gp.nombre as proyecto, gc.nombre as capa 
                 from geo_proyecto_capa_elemento ge
                 INNER JOIN geo_proyecto_capa gc
                 on gc.id=ge.capa_id 
                 INNER JOIN geo_proyecto gp
                 on gp.id =gc.proyecto_id
                 LEFT JOIN geo_tabla_detalle td
                 ON ge.detalle=td.detalle and td.tabla_id=ge.tabla_id
                 where ge.estado=1 and ge.capa_id =".$capaid;
               
        $res = DB::select($sql);

        return $res;

       /* $result=GeoProyectoCapaElemento::where('capa_id', $capaid)
           ->select(
              'geo_proyecto_capa_elemento.id as id','figura_id',
              'geo_proyecto_capa_elemento.tabla_id',
              'tabla_detalle_id', 'geo_proyecto_capa_elemento.detalle', 
              'coord', 'dato', 'borde',
              'grosorlinea',
              DB::RAW("if(figura_id='3',SUBSTR(fondo,2),fondo) as fondo"),
              'opacidad', 'geo_proyecto_capa_elemento.orden', 
              DB::RAW('gp.nombre as proyecto'), 
              DB::RAW('gc.nombre as capa')
            )
           ->join('geo_proyecto_capa as gc',
               'gc.id','=','geo_proyecto_capa_elemento.capa_id'
               )
           ->join('geo_proyecto as gp',
               'gp.id','=','gc.proyecto_id'
           )
            ->leftJoin('geo_tabla_detalle as td', function($q) {
                $q->on('geo_proyecto_capa_elemento.detalle', '=', 'td.detalle');
                $q->where('td.tabla_id', '=', 'geo_proyecto_capa_elemento.tabla_id', 'and');
            })
           ->get();

        return $result;*/
        
    }

    public static function actualizar()
    {

        $array=Input::all(); 
        unset($array['id']);
        unset($array['token']);

        $query=GeoProyectoCapa::where('id', Input::get('id'))
                    ->update(
                        $array
                    ); 

        return '1';
    }

    public static function listargeocerca($actividad_id)
    {
       
        $sql = "select gp.id, gpz.id as zona_id, 
              coordenadas, detalle from geocerca_proyecto gp
              INNER JOIN geocerca_proyecto_zonas gpz
              ON gpz.geocerca_proyecto_id=gp.id
              where gp.estado=1 and gpz.estado=1 and tipo_geozona_id<>4";
              //VALIDAR EL TIPO DE GEO ZONA A IGNORAR
              if ($actividad_id==1) {
                $sql.=" and averia=1";
              } else {
                $sql.=" and provision=1";
              }

               
        $res = DB::select($sql);

        return $res;
    }

}
