<?php

class CoordenadaTecnicoOfscHistorico extends \Eloquent
{
    protected $table ="coordenada_tecnico_ofsc_historico";

    public static function boot()
    {
        parent::boot();

        static::updating(
            function ($table) {
                $table->usuario_updated_at = Auth::id();
            }
        );

        static::saving(
            function ($table) {
                $table->usuario_created_at = Auth::id();
            }
        );

        static::deleted(
            function ($table) {
                $table->usuario_deleted_at = Auth::id();
            }
        );
    }
}

