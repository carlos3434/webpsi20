<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new ProcesoOfscEnvioMasivoCommand);
Artisan::add(new ObtenerArchivoOfscCommand);
Artisan::add(new ProcesoOfscCancelMasivoCommand);
Artisan::add(new ProcesoOfscEnvioComponentesCommand);
Artisan::add(new ObtenerCurlOfscCommand);
Artisan::add(new ProcesoOfscEnvioLocalizacionCommand);
Artisan::add(new ProcesoOfscEnvioLiquidadosCommand);
Artisan::add(new OfscParaLiquidarCommand);
Artisan::add(new ObtenerRecursosOfscCommand);
Artisan::add(new ActividadesOfsc);
Artisan::add(new ActividadesOfscHistorico);
Artisan::add(new ProcesoActividadOfscCommand);
Artisan::add(new ResourcesOfsc);
Artisan::add(new ProcesoOfscUpdateTmpCommand);
Artisan::add(new ResourceLocationUpdate);
Artisan::add(new RouterActivityUpdate);