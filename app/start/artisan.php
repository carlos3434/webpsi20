<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new ProcesoOfscEnvioMasivoCommand);
Artisan::add(new ObtenerArchivoOfscCommand);
Artisan::add(new ProcesoOfscCancelMasivoCommand);
Artisan::add(new ProcesoOfscEnvioComponentesCommand);
Artisan::add(new ObtenerCurlOfscCommand);
Artisan::add(new ProcesoOfscEnvioLocalizacionCommand);
Artisan::add(new ProcesoOfscEnvioLiquidadosCommand);
Artisan::add(new ObtenerRecursosOfscCommand);
Artisan::add(new ActividadesOfscCommand);
Artisan::add(new ActividadesOfscHistoricoCommand);
Artisan::add(new ProcesoActividadOfscCommand);
Artisan::add(new ResourcesOfscCommand);
Artisan::add(new ProcesoOfscUpdateTmpCommand);
Artisan::add(new ResourceLocationUpdate);
Artisan::add(new RouterActivityUpdate);
Artisan::add(new ProcesoResultadoPruebasModemCommand);
Artisan::add(new OfscObtenerCapacidadCommand);
Artisan::add(new ProcesoOfscConexionCommand);
Artisan::add(new TestOfscMasivoCommand);
Artisan::add(new ActualizarPreTemporalesCommand);
Artisan::add(new RefreshCacheAssetsCommand);
Artisan::add(new ProcesoPsiConsultaActividadesToaCommand);
Artisan::add(new ReporteMovimientoCommand);
Artisan::add(new CargaAutomaticCommand);
Artisan::add(new ProcesoActualizaEstadoSTUltimoSistCommand);
Artisan::add(new EnvioAtentoCommand);
Artisan::add(new ProcesoGenerarMovimientosTemp);
Artisan::add(new CotEliminarRepetidos);
Artisan::add(new EnvioLegadoErrorCommand);
include app_path().'/Legados/routes/command.php';
include app_path().'/Logs/routes/command.php';