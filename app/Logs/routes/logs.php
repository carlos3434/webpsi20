<?php 

	Route::controller('enviolegadolog', 'Logs\controllers\EnvioLegadoLogController');
	Route::controller('solicitudtecnicalogrec', 'Logs\controllers\SolicitudTecnicaLogRecController');
	Route::controller('componenteoperacion', 'Logs\controllers\ComponenteOperacionController');
	Route::controller('dashboard', 'Logs\controllers\DashboardController');
	Route::controller('enviosofsc', 'Logs\controllers\EnviosOfscController');