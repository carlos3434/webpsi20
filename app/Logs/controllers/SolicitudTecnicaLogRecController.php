<?php
namespace Logs\controllers;
use Input;
use Response;
use DB;
use Illuminate\Support\Collection;
use Legados\models\SolicitudTecnicaLogRecepcion;

class SolicitudTecnicaLogRecController extends \BaseController {
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postListar()
    {   
        $fecha = Input::get("fecha", '');
        $formFecha = explode(',', $fecha);
        $solicitudTecnica = Input::get("solicitudTecnica", '');
        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;
        $solicitudTecnicaLogREc = SolicitudTecnicaLogRecepcion::select('solicitud_tecnica_log_recepcion.id_solicitud_tecnica',
                'solicitud_tecnica_log_recepcion.trama',
                'solicitud_tecnica_log_recepcion.code_error',
                'solicitud_tecnica_log_recepcion.descripcion_error',
                'usu.nombre',
                'solicitud_tecnica_log_recepcion.created_at')
            ->join('usuarios AS usu', 'solicitud_tecnica_log_recepcion.usuario_created_at', '=', 'usu.id');
        if (Input::get('tipo_busqueda') == 1) { 
            if ( $solicitudTecnica != '' ) {
                $solicitudTecnicaLogREc->whereRaw(" solicitud_tecnica_log_recepcion.id_solicitud_tecnica = '".$solicitudTecnica."'");
            }
        } else if (Input::get('tipo_busqueda') == 2) {
            if ( $fecha != '' ) {
                $solicitudTecnicaLogREc->whereRaw(" date(solicitud_tecnica_log_recepcion.created_at) BETWEEN '". $formFecha[0]."' and '".$formFecha[1]."'");
            } 
        }           
        if ( \Input::get("accion") ) {
            $solicitud = $solicitudTecnicaLogREc->get();
            $solicitudTecnicaLogREcrpt = json_decode(json_encode($solicitud), true);
            return \Helpers::exportArrayToExcel($solicitudTecnicaLogREcrpt, "solicitud_tecnica_log_recepcion", []);
        }
        $column = "created_at";
        $dir = "desc";

        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        } 
        $solicitudTecnicaLogREc = $solicitudTecnicaLogREc->orderBy($column, $dir)
                        ->paginate($grilla["perPage"]);
        $data = $solicitudTecnicaLogREc->toArray();
        $col = new Collection([
            'recordsTotal'=> $solicitudTecnicaLogREc->getTotal(),
            'recordsFiltered'=> $solicitudTecnicaLogREc->getTotal(), 
        ]);
        return $col->merge($data);
    }

    public function postMostarrespuesta()
    {
        $trama = Input::get('trama');
        $xmlTrama = \Array2XML::createXML('SolicitudTecnicaLogRec', json_decode( json_encode( $trama ), true ));           
        $request = $xmlTrama->saveXML();
        return [
            "request" => $request,
        ];
    }
   


}

