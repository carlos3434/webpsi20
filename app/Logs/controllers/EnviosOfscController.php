<?php namespace Logs\controllers;

use Ofsc\Inbound;
use Repositories\EnvioMasivoOfscRepository;
use Logs\Repositories\EnvioOfscRepository;
use Ofsc\Activity;
use Logs\models\EnvioOfsc;

class EnviosOfscController extends \BaseController
{
    protected $_errorController;
    protected $_bandejaController;
    protected $_gestionMovimientoController;
    protected $_envioMasivoOfscRepo;
    protected $_envioOfsc;

    public function __construct(
            \ErrorController $errorController,
            \BandejaController $bandejaController,
            EnvioMasivoOfscRepository $envioMasivoOfscRepo,
            \GestionMovimientoController $gestionMovimientoController,
            EnvioOfscRepository $envioOfsc
    )
    {
        $this->_errorController = $errorController;
        $this->_bandejaController = $bandejaController;
        $this->_gestionMovimientoController = $gestionMovimientoController;
        $this->_envioMasivoOfscRepo = $envioMasivoOfscRepo;
        $this->_envioOfsc = $envioOfsc;
    }

    public function postListarmasivos()
    {
        return $this->getListarmasivos();
    }

    public static function detalleEnvioOfsc($codactu)
    {
        if ($codactu > 0) {
            $envioofsc = new EnvioOfsc();
            return $envioofsc->getEnviosOfsc($codactu);
        }
    }
    
    public function getListarmasivos()
    {
        if (!\Input::has("isSoloActuaciones")) {
            $start = \Input::get('start');
            $length = \Input::get('length');
            $columns = \Input::get('columns');
            $orderColumn = [];
            $orderDir = [];
            foreach (\Input::get('order') as $order) {
                $orderColumn = $columns[$order['column']]['data'];
                $orderDir = $order['dir'];
            }
            $datos = \Input::all();
            if (\Input::has("bandeja") && \Input::get("bandeja")!=1) {
                $envioMasivo = [];
                $emou = new EnvioMasivoOfscUltimo();
                $filtros = [
                    "codactu" => \Input::get("codactuacion", '0'),
                    "estado" => \Input::get("estado_masivo", ""),
                    "empresa" => \Input::get("empresa_masivo", "") ,
                    "fecha" => \Input::get("fecha_registro_masivo", ""),
                    "estado_psi" => \Input::get("estado_psi", ""),
                    "estado_ofsc" => \Input::get("estado_ofsc_masivo", ""),
                    "fec_movimiento" => \Input::get("fecha_movimiento", ""),
                    "nodo" => \Input::get("nodo_masivo", ""),
                    "tipo_actividad_id" =>
                                    \Input::get("actividad_tipo_masivo", ""),
                    "tipo_masivo" => \Input::get("tipo_masivo", "")
                ];
                $bodySql = [
                    "filtros" => $filtros,
                    "length" => $length,
                    "start"     => $start,
                    "orderBy" => $orderColumn,
                    "orderDir"   => $orderDir,
                    "download_excel" =>
                                \Input::has("download_excel") ? true : false,
                ];
                $envioMasivo = $emou->get($bodySql);
            } else {
                $envioMasivo = $this->_envioMasivoOfscRepo->getRows(
                    $datos, $orderColumn, $orderDir, $start, $length
                );
            }
            $extra['recordsTotal'] = $envioMasivo['total'];
            $extra['recordsFiltered'] = $envioMasivo['total'];
            $extra['draw'] = \Input::get('draw');
                if (\Input::has("bandeja")) {
                    $data =  $envioMasivo["datos"];
                } else {
                    $data = $envioMasivo['datos']->toArray();
                }
            $output = [];
            $modal='enviomasivoModal';
            foreach ($data as $k => $v) {
                $statusClass = ($v['estado'] == 0 ) ? 'danger' : 'success';
                $r = [];
                $r = $v;
                $r['accion'] = '';
                $r['estado'] = \Helpers::getEstadoHtml($statusClass);
                array_push($output, $r);
            }
            return \Helpers::getResponseJson($output, '', $extra);
        } else {
            $datos = \DB::table("envio_masivo_ofsc as emo")
                    ->select(
                        "emo.codactu",
                        "eou.id_ultimo_envio",
                        "emo.tipo_actividad_id as actividad_tipo_id",
                        "eo.estado_ofsc"
                    )
                    ->leftJoin(
                        "envios_ofsc_ultimo as eou",
                        "eou.codactu", "=", "emo.codactu"
                    )
                    ->leftJoin(
                        "envios_ofsc as eo",
                        "eo.id", "=", "eou.id_ultimo_envio"
                    )
                   ->orderBy("emo.id", "desc")
                    ->get();
            return \Response::json(
                [
                    "rst" => 1,
                    "datos" => $datos
                ]
            );
        }
    }

    public function postReporteofsc()
    {
        $consulta =  $this->_envioOfsc->getOrdenes();
        return $consulta;
    }
    /**
    * POST AJAX que recibe masivo desde la Bandeja De Gestion
    */
    public function postMasivoofsc()
    {
        return Response::json(['pendiente']);
    }

    public function postListardetalle()
    {
        $ve = \Input::get('e');
        $vr = \Input::get('r');
        if (json_decode($ve)=='') {
            $veDos=$ve;
        } else {
            $veDos=json_decode($ve);
        }
        if (json_decode($vr)=='') {
            $vrDos=$vr;
        } else {
            $vrDos=json_decode($vr);
        }
        $data=[];
        $data["enviado"]= $veDos;
        $data["respuesta"] =$vrDos;
        return \View::make('admin.reporte.form.ofscmodal', $data);
    }
}