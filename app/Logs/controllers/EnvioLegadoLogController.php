<?php namespace Logs\controllers;

use Input;
use Response;
use DB;
use Illuminate\Support\Collection;
//use Logs\models\EnvioLegadoLog;
use Legados\models\EnvioLegado;

class EnvioLegadoLogController extends \BaseController
{
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postListar()
    {
        $fecha = Input::get("fecha", '');
        
        $solicitudTecnica = Input::get("solicitudTecnica", '');
        $acciones = Input::has("saccion")? Input::get("saccion") : [];

        if (\Input::get("accion")) {
            $inputAll = Input::all();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                    $inputAll[$key] = $val;
            }
            $solicitudTecnica = (isset($inputAll["solicitudTecnica"]))? $inputAll["solicitudTecnica"] : "";
            $fecha = isset($inputAll["fecha"])? $inputAll["fecha"] : "";
        }

        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;
        $envioLegadoLog = DB::table('envio_legado')->select(
            'st.id_solicitud_tecnica as solicitud',
            'envio_legado.host',
            'envio_legado.id',
            'envio_legado.accion',
            'envio_legado.created_at',
            DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(envio_legado.request_header,CHAR(9),''),CHAR(10),''),CHAR(13),''),'/','') as request_header"),
            'envio_legado.request',
            DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(envio_legado.response_header,CHAR(9),''),CHAR(10),''),CHAR(13),''),'/','') as response_header"),
            'envio_legado.response',
            'envio_legado.code_error',
            'envio_legado.descripcion_error'
        )->join(
            'solicitud_tecnica AS st',
            'envio_legado.solicitud_tecnica_id',
            '=',
            'st.id'
        );



        if (Input::get('tipo_busqueda') == 1) {
            if ($solicitudTecnica != '') {
                $envioLegadoLog->where("st.id_solicitud_tecnica", "=", $solicitudTecnica);
            }
        } else if (Input::get('tipo_busqueda') == 2) {
            if ($fecha != '') {
                $formFecha = explode(',', $fecha);
                $envioLegadoLog->whereRaw(" date(envio_legado.created_at) BETWEEN '". $formFecha[0]."' and '".$formFecha[1]."'");
            }

            if (count($acciones) > 0) {
                $envioLegadoLog = $envioLegadoLog->whereIn("accion", $acciones);
            }
        }

        if (\Input::get("accion")) {
            $resultado = $envioLegadoLog->get();
            $excel = [];
            foreach ($resultado as $key => $value) {
                /*$request = json_decode($value->request, true);
                $objxml = \Array2XML::createXML('envio_legado_log', $request);
                $objxml->saveXML();
                $request = $objxml->saveXML();
                $request2=simplexml_load_string($request);
                echo "<pre>";
                print_r($request2);
                echo "</pre>";*/
                $excel[] = [
                    "fecregistro" => $value->created_at,
                    "solicitud" => $value->solicitud,
                    "accion"    => $value->accion,
                    "request_header" => $value->request_header,
                    "request"   => $value->request,
                    "response_header" => $value->response_header,
                    "response"  => $value->response
                ];
            }
            return \Helpers::exportArrayToExcel($excel, "Envio_legado_log", []);
        }

        $column = "envio_legado.created_at";
        $dir = "desc";

        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }

        $envioLegadoLog = $envioLegadoLog->orderBy($column, $dir)
                        ->paginate($grilla["perPage"]);
        $data = $envioLegadoLog->toArray();
        $col = new Collection([
            'recordsTotal'=> $envioLegadoLog->getTotal(),
            'recordsFiltered'=> $envioLegadoLog->getTotal(),
        ]);
        return $col->merge($data);
    }

    public function postMostarrespuesta()
    {
        $id = Input::get("id");
        $Logs=EnvioLegado::select(
            'envio_legado.request_header',
            'envio_legado.response',
            'envio_legado.response_header',
            'envio_legado.request'
        )
        ->whereRaw(" envio_legado.id = '".$id."'")
        ->get();

        $logss=json_decode(json_encode($Logs), true);
        //print_r($logss[0]['request']);
        $envio = Input::get('envio');
        $respuesta = Input::get('respuesta');
        $cabeceraEnvio = $logss[0]['request_header'];
        $cabeceraRespuesta = $logss[0]['response_header'];
        $xmlHeaderRequest = \Array2XML::createXML('envio_legado_log', $logss[0]['request']);
        $xmlHeaderResponse = \Array2XML::createXML('envio_legado_log', $logss[0]['request']);
        $request = $logss[0]["xml_request"];
        $response = $logss[0]["xml_response"];
        $headerRequest = $xmlHeaderRequest->saveXML();
        $headerResponse = $xmlHeaderResponse->saveXML();
        return [
            "request" => $request,
            "response" => $response,
            "headerRequest" => $cabeceraEnvio,
            "headerResponse" => $cabeceraRespuesta,
        ];
    }
}

