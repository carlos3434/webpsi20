<?php
namespace Logs\controllers;
use Input;
use Response;
use DB;
use Illuminate\Support\Collection;
use Legados\models\ComponenteOperacion;

class ComponenteOperacionController extends \BaseController {
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postListar()
    {   
        $fecha = Input::get("fecha", '');
        $formFecha = explode(',', $fecha);
        $operacion = Input::has("operacion")? Input::get("operacion") : '';
        $solicitudTecnica = Input::get("solicitudTecnica", '');
        if (Input::has("accion")) {
            $inputAll = Input::all();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                    $inputAll[$key] = $val;
            }
            $operacion = isset($inputAll["operacion"])? $inputAll["operacion"] : '';
            $solicitudTecnica = isset($inputAll["solicitudTecnica"])? $inputAll["solicitudTecnica"] : "";
        }
        
        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;
        $componenteOperacion = ComponenteOperacion::select(
            'componente_operacion.id_solicitud_tecnica',
            'componente_operacion.codreq',
            'componente_operacion.observacion',
            'componente_operacion.operacion',
            DB::raw("IF(indicador_procesamiento=1,'Proceso','Sin proceso') as proceso"),
            'cc.descripcion',
            'componente_operacion.created_at as fecha_log',
            'componente_operacion.updated_at as fecha_respuesta'
        )
            ->join('componente_cms AS cc', 'componente_operacion.componente_id', '=', 'cc.id');
        if (Input::get('tipo_busqueda') == 1) { 
            if ( $solicitudTecnica != '' ) {
            $componenteOperacion->whereRaw(" componente_operacion.id_solicitud_tecnica = '".$solicitudTecnica."'");
            } 
        } else if (Input::get('tipo_busqueda') == 2) {
            if ($fecha != '') {
                $componenteOperacion->whereRaw(" date(componente_operacion.created_at) BETWEEN '". $formFecha[0] ."' and '". $formFecha[1] ."'");
            }
            if ( $operacion != '' ) {
                $componenteOperacion->whereIn('componente_operacion.operacion', $operacion);
            } 
        }
               
        if (Input::get("accion")) {
            $componenteOperacionrpt = json_decode(json_encode($componenteOperacion->get()), true);
            return \Helpers::exportArrayToExcel($componenteOperacionrpt, "componente_operacion_log", []);
        }
        $column = "created_at";
        $dir = "desc";

        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }
        $componenteOperacion = $componenteOperacion->orderBy($column, $dir)
                        ->paginate($grilla["perPage"]);
        $data = $componenteOperacion->toArray();
        $col = new Collection([
            'recordsTotal'=> $componenteOperacion->getTotal(),
            'recordsFiltered'=> $componenteOperacion->getTotal(),
        ]);
        return $col->merge($data);
    }

    public function postMostarrespuesta()
    {
        $envio=Input::get('envio');
        $respuesta=Input::get('respuesta');
        $xmlRequest = \Array2XML::createXML('envio_legado_log', json_decode($envio, true));        
        $xmlResponse = \Array2XML::createXML('envio_legado_log', json_decode($respuesta, true));
        $request = $xmlRequest->saveXML();
        $response = $xmlResponse->saveXML();
        return [
            "request" => $request,
            "response" => $response,
        ];
    }

    public function postTipooperacion() 
    {
        $tipoOperacion=['revertirReposicion'=>'Revertir Reposicion',
        'revertirAveria'=>'Revertir Averia',
        'reposicion'=>'Reposicion',
        'refresh'=>'Refresh',
        'desasignacionDecos'=>'Desasignacion Decos',
        'cambioAveria'=>'Cambio Averia',
        'asignacionDecos'=>'Asignacion Decos'];
        return [
            "data"=>$tipoOperacion,
        ];
    } 
}

