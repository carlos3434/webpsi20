<?php
namespace Logs\controllers;
use Input;
use Response;
use DB;
use Illuminate\Support\Collection;
use Legados\models\SolicitudTecnicaLogRecepcion;

class DashboardController extends \BaseController {

    public function postListarcabeceras()
    {   

        $select=[DB::raw("IF(flujo=1,'CMS-PSI (1)', IF(flujo=2,'PSI-CMS (2)', IF(flujo=3,'PSI-TOA (3)', IF(flujo=4.1,'TOA-PSI (4.1)', IF(flujo=4.2,'TOA-PSI (4.2)', IF(flujo=4.3,'PSI-TOA (4.3)',  IF(flujo=6,'PSI-CMS (6)',''))))))) AS nomb_flujo"),
            DB::raw("IF(actividad_id=1,'AVERIA',IF(actividad_id,'PROVISION','')) 
            AS actividad"),
            "cantidad",
            DB::raw("date(updated_at) AS fecha_ultimo"),
            DB::raw("time(updated_at) AS tiempo_ultimo"),
            DB::raw("updated_at AS fecha_tiempo")];

        $data=DB::table('transaccion_go')
            ->select($select)
            ->orderBy('actividad_id', 'ASC')
            ->orderBy('flujo', 'ASC')
            ->get();

        return $data;
    }

}

