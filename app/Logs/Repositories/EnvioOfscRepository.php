<?php namespace Logs\Repositories;
use Illuminate\Support\Collection;
class EnvioOfscRepository implements EnvioOfscRepositoryInterface
{
    public function getOrdenes()
    {
            $consulta = array();
             if (\Input::has("bandeja")) {
                    $resultado = array();
                    $fechaRegistro = explode(" - ",\Input::get("fecha_registro"));

                    if (\Input::has("download_excel")) { 
                        $consulta = \DB::table("envios_ofsc_ultimo as eou")
                                        ->select(
                                                \DB::raw("codactu as CodigoRequerimiento, IFNULL(eou.fec_updated, eou.fec_created) as fecha_envio, eou.aid as AID,
                                                    at.nombre as TipoActividad, e.nombre as Empresa, n.nombre as Nodo,
                                                    (CASE eou.tipo_envio WHEN 0 THEN '' WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA - Libre' END) AS Agenda, CONCAT (u.nombre, u.apellido, ' ') as Usuario ")
                                            );
                    } else {
                        $consulta = \DB::table("envios_ofsc_ultimo as eou")
                                    ->select(
                                        \DB::raw("DISTINCT eou.codactu, eou.aid as aid, 
                                          eou.empresa, IFNULL(eou.fec_updated, eou.fec_created) as fecha_envio,
                                        at.nombre as nombre_tipo_actividad, at.actividad_id, u.usuario as loginusuario, e.nombre as empresa,
                                        n.nombre as nombre_nodo, (CASE eou.tipo_envio WHEN 0 THEN '' WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA - Libre' END) AS nombre_agenda, 
                                        eof.nombre as estado_ofsc, eou.numintentos, um.estado_ofsc_id " )
                                        );
                    }

                    $between = " BETWEEN '".$fechaRegistro [0]."' AND '".$fechaRegistro [1]."' ";
                    $consulta->whereRaw(" IF ( ISNULL(eou.fec_updated) ,  eou.fec_created ".$between." ,  eou.fec_updated ".$between.")")->leftJoin( 'usuarios as u',  'u.id', '=', 'eou.user_updated')
                                    ->leftJoin( 'empresas as e',  'e.nombre', '=', 'eou.empresa')
                                    ->leftJoin('ultimos_movimientos as um', 'um.id', '=', 'eou.id_ultimo_movimiento')
                                    ->leftJoin( 'webpsi_fftt.nodos_eecc_regiones as n',  'n.NODO', '=', 'eou.nodo')
                                    ->leftJoin( 'actividades_tipos as at',  'at.id', '=', 'eou.tipo_actividad_id')
                                    ->leftJoin( 'actividades as ac',  'at.actividad_id', '=', 'ac.id')
                                    ->leftJoin('estados_ofsc as eof', 'um.estado_ofsc_id', '=', 'eof.id');

                    if (\Input::get("codigo_actuacion")!="") {
                        $consulta->where("eou.codactu", "=", \Input::get("codigo_actuacion"));
                    } else {
                        if (\Input::has("estado_ofsc")) {
                          $estado_ofsc = \Input::get("estado_ofsc");
                          if ($estado_ofsc!=="") {
                            $consulta->where("gm.estado_ofsc_id", "=", $estado_ofsc);
                          }
                        }
                        if (\Input::has("empresa")) {
                            $empresa = \Input::get("empresa");
                            if ($empresa!=="null") {
                                if (\Input::has("download_excel")) {
                                    $consulta->whereRaw(" e.id IN ( $empresa) ");
                                } else {
                                    $consulta->whereIn("e.id", $empresa);
                                }
                            }
                            
                        }

                        if (\Input::has("nodo")) {
                          $nodo = \Input::get("nodo");
                          if (\Input::has("download_excel")) {
                            if ($nodo!=="null") {
                              $consulta->whereRaw(" eou.nodo IN ($nodo)");
                            }
                          } else {
                            $consulta->whereIn("eou.nodo", \Input::get("nodo"));
                          }
                        }

                        if (\Input::has("actividad")) {
                          $actividad = \Input::get("actividad");
                          if (\Input::has("download_excel")) {
                                if ($actividad!=="null")
                                    $consulta->whereRaw(" ac.id IN ( $actividad) ");
                          } else {
                                $consulta->whereIn("ac.id", $actividad);
                          }
                          $actividad_tipo = \Input::get("actividad_tipo");
                          if (\Input::has("actividad_tipo")) {
                            if (\Input::has("download_excel")) {
                              if ($actividad_tipo!=="null")
                                $consulta->whereRaw(" eou.tipo_actividad_id IN ($actividad_tipo) ");
                            } else {
                              $consulta->whereIn("eou.tipo_actividad_id", $actividad_tipo);
                            }
                          }
                        }
                        if (\Input::get("tipo_envio")!="") {
                            $consulta->where("eou.tipo_envio", "=", \Input::get("tipo_envio"));
                        }
                    }

                    if (\Input::has("download_excel")) {
                        $resultados["datos"] = $consulta->get();
                    } else {
                        $resultados["total"] = count($consulta->get());
                        if ( \Input::get('draw') ) {
                            if ( \Input::get('order') ) {
                                $inorder=\Input::get('order');
                                $incolumns=\Input::get('columns');
                                $campoOrderBy = $incolumns[ $inorder[0]['column'] ]['name'];
                                $orderDir  = $inorder[0]['dir'];
                                $resultados["datos"] = 
                                            $consulta->limit(\Input::get("length"))
                                                    ->offset(\Input::get("start"))
                                                    ->orderBy($campoOrderBy, $orderDir)
                                                    ->get();
                            }
                        } else {
                            $resultados["datos"] = $consulta
                                                  ->limit(\Input::get("length"))
                                                  ->offset(\Input::get("start"))
                                                  ->get();
                        }
                    }

                return $resultados;
        } else {
            $fecha= explode(" - ", \Input::get('fecha_calen'));
            if($fecha){
                $query = \DB::table('envios_ofsc as e')
                      ->select('e.accion', 'e.enviado', 'e.respuesta', 'e.created_at', 'u.usuario','e.codactu')
                      ->leftJoin('usuarios as u',  'u.id', '=', 'e.usuario_created_at')
                      ->where(function($query) use($fecha){
                            if(\Input::get('fecha')){
                                $query->whereRaw('DATE(e.created_at) BETWEEN "'.$fecha[0].'" AND "'.$fecha[1].'"');                                
                            }

                            if(\Input::get('tipoaccion')){
                                $query->whereIn('e.accion',\Input::get('tipoaccion'));
                            }
                      });

                if (\Input::has('column')) {
                    $query->orderBy(\Input::get('column'), \Input::get('dir'));
                }
                
                $perPage = \Input::has('per_page') ? (int)\Input::get('per_page') : null;
                $result = $query->paginate($perPage);       
                $data = $result->toArray(); 
                $col = new Collection([
                    'recordsTotal'=>$result->getTotal(),
                    'recordsFiltered'=>$result->getTotal()
                ]);
                return $col->merge($data);
            }
        }
    }

}
