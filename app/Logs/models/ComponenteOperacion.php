<?php namespace Logs\models;

class ComponenteOperacion extends \Eloquent
{       
    protected $guarded =[];
    //protected $fillable = ['aid'];
    protected $table = "componente_operacion as co";

    
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
