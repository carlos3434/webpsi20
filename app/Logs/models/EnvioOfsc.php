<?php namespace Logs\models;

use Illuminate\Support\Collection;
class EnvioOfsc extends \Eloquent
{


    public $table = 'envios_ofsc';


    /**
     * @param string $accion        accion
     * @param string $dataReq       data requerida
     * @param string $contenidoResp respuesta WS
     * @param string $respuestaEstadoWs status final de la WS
     */
    public function registrarAccionWebservice($accion, $dataReq, $contenidoResp,
                                             $usuario, $dataOfsc = array())
    {
            $empresa = "";
            $nodo = "";
            $useFrom = 0;
            $tipoAgenda = 0;
            $usuario = 0;
            $codactu = 0;
            $actividadTipoId = 0;
            $numIntentos = 1;
            $estadoOfsc = 0;
            $aid = 0;

            if (isset($dataOfsc["aid"])) {
              $aid = $dataOfsc["aid"];
            }

            if (isset($dataOfsc["usuario"])) {
              $usuario = $dataOfsc["usuario"];
            }

            if (count($dataOfsc) > 0) {
              switch ($accion) {
                case 'inbound_interface':
                    $codactu = $dataOfsc["appt_number"];
                    $tipoAgenda = $dataOfsc["isSla"];
                    if (isset($dataOfsc["use_from"]))
                      $useFrom = $dataOfsc["use_from"];
                    $orden = Gestion::getCargar($codactu, true);
                    if ($orden["rst"] == 1) {
                        $orden = $orden["datos"][0];
                        $orden = get_object_vars($orden);

                        $empresa = "";
                        if (isset($orden["empresa"]))
                            $empresa = $orden["empresa"];
                          $nodo = $orden["mdf"];
                          $actividadTipoId = $orden["actividad_tipo_id"];
                    }
                    break;
                case 'get_capacity':
                    break;

                default:
                    break;
              }
            }
            if ($codactu ===0) {

            } else {
                $envio = DB::table("envios_ofsc")
                         ->select("numintentos")
                         ->where("codactu", "=", $codactu)
                         ->orderBy("id", "desc")
                         ->get();

              if (count($envio) > 0) {
                $numIntentos = $envio[0]->numintentos + 1;
              }
              $ultimo_movimiento = DB::table("ultimos_movimientos")
                          ->select("estado_ofsc_id")
                          ->where("codactu", "=", $codactu)
                          ->orderBy("id", "desc")
                          ->get();
              if ( count($ultimo_movimiento)  > 0) {
                $estadoOfsc = $ultimo_movimiento[0]->estado_ofsc_id;
                if ($estadoOfsc === null)
                  $estadoOfsc = 0;
              }
            }

            $insert = [
                   'accion'             => $accion,
                   'aid'                    => $aid,
                   'contrata'          => $empresa,
                   'nodo'                 => $nodo,
                   'codactu'          => $codactu,
                   'tipo_actividad_id' => $actividadTipoId,
                   'tipo_envio'       => $tipoAgenda,
                   'enviado'            => $dataReq,
                   'estado_ofsc'    => $estadoOfsc,
                   'respuesta'          => $contenidoResp,
                   'usuario_created_at' => $usuario,
                   'use_from' => $useFrom,
                   'created_at' => DB::raw('NOW()')
          ];
          $this->insertEnvioOfsc($insert);

    }//end registrarAccionWebservice()

    public function insertEnvioOfsc ($insert= array())
    {
        if (count($insert) > 0) {
          DB::table('envios_ofsc')->insert($insert);
        }
    }
    public function getEnviosOfsc ($codactu = 0)
    {
      if ($codactu > 0) {
        return DB::table("envios_ofsc as eo")
                ->select(
                    "codactu", "tgo.nombre as gestion_ofsc",
                    "contrata as empresa", "nodo", "aid",
                    DB::raw(
                        "(CASE tipo_envio WHEN 1 THEN 'Agenda' WHEN 2 THEN 'SLA' END) as tipoAgenda "
                    ),
                    "created_at as fecha_envio", "eo.accion as accion"
                )
            ->leftJoin("tipo_gestion_ofsc as tgo", "tgo.accion", "=", "eo.accion")
            ->where("codactu", "=", $codactu)
            ->orderBy("eo.id", "desc")
            ->get();
      } else {
        return array();
      }
    }
}
