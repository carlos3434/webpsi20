<?php namespace Logs\models;

class SolicitudTecnicaLogRec extends \Eloquent
{   
    
    protected $guarded =[];
    //protected $fillable = ['aid'];
    protected $table = "solicitud_tecnica_log_recepcion as stlr";

    
    public static function boot()
    {
        parent::boot();

        static::updating(function ($table) {
            $table->usuario_updated_at = \Auth::id();
        });
        static::creating(function ($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
}
