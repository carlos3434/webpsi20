<?php

namespace Logs;
use Illuminate\Support\ServiceProvider;
use Logs\Repositories;

class LogServiceProvider extends ServiceProvider {

  /**
   * Register
   */
    public function register()
    {
        $this->app->bind('Logs\Repositories\EnvioOfscRepositoryInterface', 'Logs\Repositories\EnvioOfscRepository');
    }
}
