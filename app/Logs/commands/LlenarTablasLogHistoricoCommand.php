<?php
namespace Logs\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Logs\models\ComponenteOperacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LlenarTablasLogHistoricoCommand extends Command
{

    protected $name = 'logs:llenarTablasLogHistorico';

    protected $description = 'Limpia logs(componente operacion, envio legado y solicitud tecnica log recepcion) mayor a 1 mes.';


    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        echo "----------------------------------------\nInicio: " . date("Y-d-m H:m:s") . "\n----------------------------------------";
        $time_start = microtime(true);

        date_default_timezone_set('America/Lima');

        $dia = date("d");
        $mes = date("m");
        $anio = date("Y");
        $hora = date("G");
        $minuto = date("i");
        $segundo = date("s");

        $tabla_compoperacion='historico_compoperacion_'.$dia.$mes.$anio.$hora.$minuto.$segundo;
        $tabla_envlegado='historico_envlegado_'.$dia.$mes.$anio.$hora.$minuto.$segundo;
        $tabla_soltecnicarec='historico_soltecnicarec_'.$dia.$mes.$anio.$hora.$minuto.$segundo;



        //1.- componente operacion

        echo "\n\n1.-) Procesando Registros en historico_compoperacion: \n";

        $min_date_1 = DB::table("componente_operacion")->selectRaw("MIN( created_at ) as min_date")->first()->min_date;

        echo "     Registro mas antiguo: " . $min_date_1 . "\n\n";

        
        $data_compoperacion = DB::table('componente_operacion')
                                ->whereRaw("MONTH( created_at ) = MONTH('".$min_date_1."') AND YEAR( created_at ) = YEAR('".$min_date_1."')")
                                ->get();
        
        if (count($data_compoperacion) > 0) {

            //crear tbl
            DB::statement("CREATE TABLE psi_backup.".$tabla_compoperacion." LIKE psi.componente_operacion;");

            $r_hc = 0;

            foreach ($data_compoperacion as $value) {

                //insert
                DB::insert(
                    "INSERT INTO psi_backup.".$tabla_compoperacion." (id,componente_id,id_solicitud_tecnica,codreq,created_at,updated_at,usuario_created_at,usuario_updated_at,componente_cod,indorigreq,numcompxreq,numcompxsrv,indicador,observacion,idreg,operacion,indicador_procesamiento,codigo_error_interno,codigo_error_envio,observacion_respuesta,telefono_origen,estado_operacion,interface,numser,codmat,numserold,codmatold,numserpar,codmatpar) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    [
                        $value->id,$value->componente_id,$value->id_solicitud_tecnica,
                        $value->codreq,$value->created_at,$value->updated_at,
                        $value->usuario_created_at,$value->usuario_updated_at,$value->componente_cod,
                        $value->indorigreq,$value->numcompxreq,$value->numcompxsrv,
                        $value->indicador,$value->observacion,$value->idreg,
                        $value->operacion,$value->indicador_procesamiento,
                        $value->codigo_error_interno,$value->codigo_error_envio,
                        $value->observacion_respuesta,$value->telefono_origen,
                        $value->estado_operacion,$value->interface,$value->numser,
                        $value->codmat,$value->numserold,$value->codmatold,
                        $value->numserpar,$value->codmatpar
                    ]
                );

                $r_hc++;

                echo "[".$value->id."]";

            }

            echo "\n\nTotal: " . $r_hc . "\n";
            echo "----------------------------------------\n";
            
            //delete
            DB::statement("DELETE FROM componente_operacion where MONTH( created_at ) = MONTH('".$min_date_1."') AND YEAR( created_at ) = YEAR('".$min_date_1."');");
        }

        //2.- envio legado

        echo "\n\n2.-) Procesando Registros en historico_envlegado: \n";

        $min_date_2 = DB::table("envio_legado")->selectRaw("MIN( created_at ) as min_date")->first()->min_date;

        echo "     Registro mas antiguo: " . $min_date_2 . "\n\n";


        $data_envlegado = DB::table('envio_legado')
                            ->whereRaw("MONTH( created_at ) = MONTH('".$min_date_2."') AND YEAR( created_at ) = YEAR('".$min_date_2."')")
                            ->get();

        if (count($data_envlegado) > 0) {

            //crear tbl
            DB::statement("CREATE TABLE psi_backup.".$tabla_envlegado." LIKE psi.envio_legado;");

            $r_hel = 0;

            foreach ($data_envlegado as $value) {

                DB::insert(
                    "INSERT INTO psi_backup.".$tabla_envlegado." (id,solicitud_tecnica_id,host,accion,request,response,request_header,response_header,created_at,updated_at,deleted_at,usuario_created_at,usuario_updated_at,descripcion_error,code_error) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    [
                        $value->id,$value->solicitud_tecnica_id,$value->host,
                        $value->accion,$value->request,$value->response,
                        $value->request_header,$value->response_header,$value->created_at,
                        $value->updated_at,$value->deleted_at,$value->usuario_created_at,
                        $value->usuario_updated_at,$value->descripcion_error,$value->code_error
                    ]
                );

                $r_hel++;
                echo "[".$value->id."]";
            }

            echo "\n\nTotal: " . $r_hel . "\n";
            echo "----------------------------------------\n";
            //delete
            DB::statement("DELETE FROM envio_legado where MONTH( created_at ) = MONTH('".$min_date_2."') AND YEAR( created_at ) = YEAR('".$min_date_2."');");
        }

        //3.- solicitud tecnica recepcion
        echo "\n\n3.-) Procesando Registros en historico_soltecnicarec: \n";

        $min_date_3 = DB::table("solicitud_tecnica_log_recepcion")->selectRaw("MIN( created_at ) as min_date")->first()->min_date;

        echo "     Registro mas antiguo: " . $min_date_3 . "\n\n";

        $data_soltecnicarec = DB::table('solicitud_tecnica_log_recepcion')
                                ->whereRaw("MONTH( created_at ) = MONTH('".$min_date_3."') AND YEAR( created_at ) = YEAR('".$min_date_3."')")
                                ->get();

        if (count($data_soltecnicarec) > 0) {

            //crear tbl
            DB::statement("CREATE TABLE psi_backup.".$tabla_soltecnicarec." LIKE psi.solicitud_tecnica_log_recepcion;");

            $r_hstr = 0;

            foreach ($data_soltecnicarec as $value) {

                DB::insert(
                    "INSERT INTO psi_backup.".$tabla_soltecnicarec." (id, id_solicitud_tecnica,trama,code_error,descripcion_error,solicitud_tecnica_id,                usuario_updated_at,created_at,usuario_created_at,updated_at) values(?,?,?,?,?,?,?,?,?,?)",
                    [
                        $value->id,$value->id_solicitud_tecnica,$value->trama,
                        $value->code_error,$value->descripcion_error,$value->solicitud_tecnica_id,
                        $value->usuario_updated_at,$value->created_at,$value->usuario_created_at,
                        $value->updated_at
                    ]
                );

                $r_hstr++;
                echo "[".$value->id."]";

            }

            echo "\n\nTotal: " . $r_hstr . "\n";
            echo "----------------------------------------\n";
            //delete
            DB::statement("DELETE FROM solicitud_tecnica_log_recepcion where MONTH( created_at ) = MONTH('".$min_date_3."') AND YEAR( created_at ) = YEAR('".$min_date_3."');");
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo "\nFin: " . date("Y-d-m H:m:s") . "\n";

        echo "\nTiempo de ejecución: " . $execution_time . "\n\n----------------------------------------\n";

    }
}
