<?php
namespace Logs\commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LimpiarTransaccionGoCommand extends Command
{

    protected $name = 'logs:limpiarTGO';

    protected $description = 'Limpia la tabla de transacciones go detalle y las guarda en psi_backup';


    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $detalles =  DB::table("transaccion_go_detalle")->select("transaccion_go_id","id_solicitud_tecnica","created_at","flujo","mensaje_id","send_to","subject","usuario_toa")->get();
        $i = 0;

        echo sizeof($detalles)."\n";

        foreach ($detalles as &$detalle) {

            $detalle = (array)$detalle;
            //echo $i++."\n";

        }

        $conjuntos = array_chunk($detalles, 500);

        foreach ($conjuntos as $conjunto) {
            DB::table("psi_backup.transaccion_go_detalle")->insert((array)$conjunto);
        }

        DB::statement("truncate table transaccion_go_detalle");
        DB::statement("truncate table transaccion_go");

        echo "Finish\n";
    }
}