<?php

return [
    "componentes" => [
        "wsdl" => "http://10.226.44.222:7020/componente.wsdl",
        "authorized" => [
            "ejecucion_componente" => true
        ],
    ],
    "solicitudtecnica" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica.wsdl",
        "authorized" => [
            "generar" => true,
            "consultar" => true,
            "retornar" => true
        ]
    ],
    "solicitudtecnica_gestel_averia" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica_gestel_averia.wsdl",
        "authorized" => [
            "generar" => true
        ]
    ],
    "solicitudtecnica_gestel_provision" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica_gestel_provision.wsdl",
        "authorized" => [
            "generar" => true
        ]
    ],
    "ussd" => [
        "wsdl" => "http://10.226.44.222:7020/ussd.wsdl",
        "authorized" => [
            "consultar" => true
        ]
    ],    
    // webservices del area sistemas de telefonica
    "wsdl" => [
        "respuestast" =>      "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnica.wsdl",
        "respuestast_test" => "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnica_test.wsdl",
        "respuestast_desarrollo" => "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnica_desarrollo.wsdl",
        "RegistroDecos"      => "http://10.226.44.222:7020/legado/ws/RegistroDecos/RegistroDecos/WSDL/RegistroDecos.wsdl",
        "RegistroDecos_test" => "http://10.226.44.222:7020/legado/ws/RegistroDecos/RegistroDecos/WSDL/RegistroDecos_test.wsdl",
        "RegistroDecos_desarrollo" => "http://10.226.44.222:7020/legado/ws/RegistroDecos/RegistroDecos/WSDL/RegistroDecos_desarrollo.wsdl",
        "analizador" =>      "http://10.226.4.239/webunificada/modulos/analizador/PruebaElectricaService.php?wsdl",
        "cable_modem" =>      "http://10.123.200.207:5620/wsCCM1.php?wsdl",
        "assia" =>      "http://172.28.13.121:8080/dslexpresse/services/pe_data.pe_dataHttpSoap11Endpoint",
        "respuestast_gestel" =>      "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnicaGestel.wsdl",
        "respuestast_gestel_test" =>      "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnicaGestel_test.wsdl",
        "respuestast_gestel_desarrollo" =>      "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnicaGestel_desarrollo.wsdl",
        "pcba" => "http://172.28.13.119/amfphp/services/ws_pdm_request.php",
        "mac" =>"http://190.234.74.6:9571/cmts/actmac.php",
    ],
    "url_lego" => [
        "Produccion"=>"10.226.6.141:7777",
        "Test"=>"10.226.4.41:10001",
        "Desarrollo"=>"10.226.4.108:8011"
    ],
    "user_logs" => [
      "genera_solicitud" => 1185,
      "genera_solicitud_gestel" => 1186,
      "activa_deco" => 0,
      "refresh" => 0,
      "deasignar_deco" => 0,
      "cambiar_deco_por_averia" => 0,
      "revertir_deco_por_averia" => 0,
      "reponer_deco" => 0,
      "revertir_reponer_deco" => 0
   ],
   "appmovistar" => [
        "wsdl" => "http://10.226.44.222:7020/stappmovistar.wsdl",
        "authorized" => [
            "agendar" => true,
            "capacidad" => true,
            "cancelar" => true
        ]
    ],
   /*
    * 0: envio manual por bandeja legado
      1: envio automatico a toa
    */
   "web_aseguramiento" => 1,
   "user_operation" => "JESCALA5",
   "area_transferencia" => "REDCLTE"
];
