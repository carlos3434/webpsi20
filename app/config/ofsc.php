<?php
/**
 * Variables "globales" configuracion
 * 
 * auth: login y pass en base64
 */
//define("TOAURL", "https://telefonica-pe.test.toadirect.com");
//define("TOAURLSOAP", "https://api.etadirect.com/soap/");
define("TOAURLSOAP", "https://legacy-api.etadirect.com/soap/");
define("TOAURLRestV1", "https://api.etadirect.com/rest/ofscCore/v1/");

return [
    "tipo" => [
        "capacity" => "soap",
        "activity" => "soap",//soap|rest
        "resource" => "soap",//soap|rest
        "inbound"  => "soap",
        "outbound" => "soap",
        "location" => "soap",
    ],
    "wsdl" => [
        "capacity" => TOAURLSOAP . "capacity/?wsdl",
        "activity" => TOAURLSOAP . "activity/v2/?wsdl",
        "resource" => TOAURLSOAP . "resource-management/?wsdl",
        "inbound" => TOAURLSOAP . "inbound/?wsdl",
        "outbound" => TOAURLSOAP . "outbound/?wsdl",
        "location" => TOAURLSOAP . "location/?wsdl",
        "history" => ""
    ],
    "auth" => [
        "company" => "telefonica-pe",//instanceId
        "login" => "soap",
        "pass" => "arg3ntina"
    ],
    'oAuth2' => [
        "client_id" => 'ZTFiMWRiODI5MzJmMGQxZjhhYjZiYTQwMmEwZTk1MGQxNzZmMmU1NWM2MTkwZDE2MmQ2NDZkMGZmNDg2MWNlMDp0ZWxlZm9uaWNhLXBlMi50ZXN0',
        "client_secret" => '8cbf8d02a30ee42246ee9129418b7e80d36004f7fd9fddf7a500f75195e32389',
        "authorize_endpoint" => 'https://api.etadirect.com/rest/oauthTokenService/v1/authorize', // s/u
        "token_endpoint" => 'https://api.etadirect.com/rest/oauthTokenService/v1/token'
    ],
    "outbound" => [
        "wsdl" => "http://10.226.44.222:7020/toa_outbound",
        "company" => "telefonica-pe.test",
        "login" => "soap",
        "pass" => "arg3ntina",
        "diferenciaSegundos" => 1800, //segundos de diferencia
        "authorized" => [
            "send_message" => true,
            "drop_message" => true,
            "get_message_status" => true
        ]
    ],
    "rest" => [
        /* %1$d: activityId */
        /* %2$d: linkedActivityId */
        /* %3$d: linkType */
        /* USE: sprintf(Config::get("ofscrestful.rest.activity.get"), activityId) */
        "activity" => [
            'endpoint' => TOAURLRestV1 . 'activities',
            // POST
            'bulkUpdate' => TOAURLRestV1 . 'activities/custom-actions/bulkUpdate',
            'cancel' => TOAURLRestV1 . 'activities/%1$d/custom-actions/cancel',
            'complete' => TOAURLRestV1 . 'activities/%1$d/custom-actions/complete',
            'createLink' => TOAURLRestV1 . 'activities/%1$d/linkedActivities',
            'create' => TOAURLRestV1 . 'activities',
            // DELETE
            'delete' => TOAURLRestV1 . 'activities/%1$d',
            'deleteLink' => TOAURLRestV1 . 'activities/{activityId}/linkedActivities/%2$d/linkTypes/%3$d',
            'deleteAllLinks' => TOAURLRestV1 . 'activities/%1$d/linkedActivities',
            'deleteRequiredInventories' => TOAURLRestV1 . 'activities/%1$d/requiredInventories',
            'deleteResourcePreferences' => TOAURLRestV1 . 'activities/%1$d/resourcePreferences',
            // GET
            'get' => TOAURLRestV1 . 'activities/%1$d',
            'getFile' => TOAURLRestV1 . 'activities/%1$d/%2$s',
            'getLinkDetails' => TOAURLRestV1 . 'activities/%1$d/linkedActivities/%2$d/linkTypes/%3$d',
            'getLink' => TOAURLRestV1 . 'activities/%1$d/linkedActivities',
            'getCustomerInventories' => TOAURLRestV1 . 'activities/%1$d/customerInventories',
            'getRequiredInventories' => TOAURLRestV1 . 'activities/%1$d/requiredInventories',
            'getResourcePreferences' => TOAURLRestV1 . 'activities/%1$d/resourcePreferences',
            // POST
            'move' => TOAURLRestV1 . 'activities/%1$d/custom-actions/move',
            'notDone' => TOAURLRestV1 . 'activities/%1$d/custom-actions/notDone',
            // PUT
            'replaceLink' => TOAURLRestV1 . 'activities/%1$d/linkedActivities/%2$d/linkTypes/%3$d',
            'setRequiredInventories' => TOAURLRestV1 . 'activities/%1$d/requiredInventories',
            'setResourcePreferences' => TOAURLRestV1 . 'activities/%1$d/resourcePreferences',
            // POST
            'start' => TOAURLRestV1 . 'activities/%1$d/custom-actions/start',
            'suspend' => TOAURLRestV1 . 'activities/%1$d/custom-actions/suspend',
            // PATCH
            'update' => TOAURLRestV1 . 'activities/%1$d',
        ],
        "dailyExtract" => [
            'endpoint' => TOAURLRestV1 . 'folders/dailyExtract/folders',
            // GET
            'DownloadDailyExtractFile' => TOAURLRestV1 . 'folders/dailyExtract/folders/%1$s/files/%2$s',
            'getDailyExtractDates' => TOAURLRestV1 . 'folders/dailyExtract/folders',
            'getListDailyExtractFilesForDate' => TOAURLRestV1 . 'folders/dailyExtract/folders/%1$s/files',
        ],
        "event" => [
            'endpoint' => TOAURLRestV1 . 'events',
            // POST
            'createSubscription' => TOAURLRestV1 . 'events/subscriptions',
            // GET
            'get' => TOAURLRestV1 . 'events',
            'getSubscriptionDetailInfo' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
        ],
        "inventory" => [
            'endpoint' => TOAURLRestV1 . 'inventories',
            // POST
            'create' => TOAURLRestV1 . 'events/subscriptions',
            'deinstall' => TOAURLRestV1 . 'events/subscriptions',
            // DELETE
            'delete' => TOAURLRestV1 . 'events',
            // GET
            'get' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
            // POST
            'install' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
            'undoDeinstall' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
            'UndoInstall' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
            // PATCH
            'update' => TOAURLRestV1 . 'events/subscriptions/{subscriptionId}',
        ],
        "user" => [
            'endpoint' => TOAURLRestV1 . 'users',
            // POST
            'addCollaborationGroups' => TOAURLRestV1 . 'users/%1$s/collaborationGroups',
            // PUT
            'create' => TOAURLRestV1 . 'users/%1$s',
            // DELETE
            'deleteCollaborationGroups' => TOAURLRestV1 . 'users/%1$s/collaborationGroups',
            'delete' => TOAURLRestV1 . 'users/%1$s',
            // GET
            'getCollaborationGroups' => TOAURLRestV1 . 'users/%1$s/collaborationGroups',
            'get' => TOAURLRestV1 . 'users/%1$s',
            'gets' => TOAURLRestV1 . 'users',
            'getsLimit' => TOAURLRestV1 . 'users/?limit=%1$d&offset=%2$d',
            // PATCH
            'update' => TOAURLRestV1 . 'users/%1$s',
        ],
        "resource" => [
            //GET
            'gets' => TOAURLRestV1 . 'resources',
            'getsLimit' => TOAURLRestV1 . 'resources/?limit=%1$d&offset=%2$d'
        ]
    ]
];