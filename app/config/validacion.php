<?php
/**
 * Variables "globales" configuracion
 *
 *
 */
/*
return array(
    "distancia" => "1000" //en metros 
); */


return [
    "distancia" => "600", //en metros = 1km
    "cellsize" => "1.5", // en KM
    "pivot" => [
        "latitude" => -12.109129,
        "longitude" => -77.016123
    ],
    "lari-ate" => [
        "latitude" => -12.059542,
        "longitude" => -76.970035
    ]
];
