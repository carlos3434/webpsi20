<?php
/**
 * Variables "globales" configuracion
 */

return array(
    "webservice" =>[
        'decocms' => 'http://10.226.44.222:7020/deco_cms.wsdl'
    ],
    "map" => array(
        'key' => 'AIzaSyC_m4ilxD-gh685xBy0970wNt-SPJe76Wc'
    ),
    "db" => array(
        "tmp_averia" => "webpsi_coc.averias_criticos_final",
        "tmp_provision" => "webpsi_coc.tmp_provision",
    ),
    "ot" => array(
        "url" => "http://10.226.157.232/test/integracion/office_track.php"
    ),
    "geo" => array(
        "public" => array(
            "maptec" => "http://psiweb.ddns.net:7020/publicmap/rutatecnico/",
            "mapord" => "http://psiweb.ddns.net:7020/publicmap/ordentecnico/",
            "mapofsc" => "http://psiweb.ddns.net:7020/publicmap/ordentecnicoofsc/",
            "maptap" => "http://psiweb.ddns.net:7020/publicmap/ordentapofsc/",
            "maptaplego" => "http://psiweb.ddns.net:7020/publicmap/ordentaplego/",
            "mapterminal" => "http://psiweb.ddns.net:7020/publicmap/ordenterminalofsc/",
            "mapvalida" => "http://psiweb.ddns.net:7020/publicmap/ordenvalidar/",
            "mapupdate" => "http://psiweb.ddns.net:7020/publicmap/ordenactualizar/"
        ),
        "default" => array(
            "lat" => -12.115132,
            "lng" => -77.062628
        )
    ),
    "modem" => array(
        "mac" => "http://psiweb.ddns.net:7020/cable/cablemodem/",
    ),
    "schema" => array(
        "psi" => "psi",
        "unificada" => "webunificada",
        "officetrack" => "webpsi_officetrack",
        "fftt" => "webpsi_fftt",
        "criticos" => "webpsi_criticos",
        "coc" => "webpsi_coc"
    ),
    "sms" => array(
        //"url" => "http://10.226.44.222/webpsi/sms_enviar_individual_ajax.php"
        "url" => "http://10.226.44.223/noc/sms.php"//novus
    ),
    "permisos" => array(
        "key" => "7dc78e66cd43431445177f0200bdb8b9"
    ),
    "proxy" => array(
        "host" => "10.226.157.232" ,
        "port" => "8590"
    ),
    "prueba" => array(
        "cablemodem" => "http://psiweb.ddns.net:7020/prueba/cablemodem",
        "activacionrefresh" => "http://psiweb.ddns.net:7020/deco/activacionrefresh/"
    ),
    "ofsc" => array(
        "user_logs" => array(
                "actividades_temporales" => 966,
                "update_componentes" => 967,
                "liquidado_masivo" => 968
        )
    ),
    "legado" => array(
      "operaciones" => "http://psiweb.ddns.net:7020/componentes.operaciones/"
    ),
    "urlfolder" => array(
        "historico_actividades" => "http://psiweb.ddns.net:7020/json/actividades/",
        "historico_tecnicos" => "http://psiweb.ddns.net:7020/json/tecnicos/",
        "reportes" => "http://psiweb.ddns.net:7020/json/reportes/",
        ),
    "tiempos" => [
        'tiempo_cierre_solicitud' => 600
    ],
);
