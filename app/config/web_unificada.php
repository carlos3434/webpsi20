<?php

return [
    "componentes" => [
        "wsdl" => "http://10.226.44.222:7020/componente.wsdl",
        "authorized" => [
            "ejecucion_componente" => true
        ],
    ],
    "solicitudtecnica" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica.wsdl",
        "authorized" => [
            "generar" => true,
            "consultar" => true,
            "retornar" => true
        ]
    ],
    "solicitudtecnica_gestel_averia" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica_gestel_averia.wsdl",
        "authorized" => [
            "generar" => true
        ]
    ],
    "solicitudtecnica_gestel_provision" => [
        "wsdl" => "http://10.226.44.222:7020/solicitudtecnica_gestel_provision.wsdl",
        "authorized" => [
            "generar" => true
        ]
    ],
    // webservices del area sistemas de telefonica
    "wsdl" => [
        "respuestast" =>      "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnica.wsdl",
        "respuestast_test" => "http://10.226.44.222:7020/legado/ws/RespCierreSt/RptaCierreSolicitudTecnica/WSDL/RptaCierreSolicitudTecnica_test.wsdl",
        "RegistroDecos"      => "http://10.226.44.222:7020/legado/ws/RegistroDecos/RegistroDecos/WSDL/RegistroDecos.wsdl",
        "RegistroDecos_test" => "http://10.226.44.222:7020/legado/ws/RegistroDecos/RegistroDecos/WSDL/RegistroDecos_test.wsdl"
    ],
    "user_logs" => [
      "genera_solicitud" => 1022,
      "activa_deco" => 0,
      "refresh" => 0,
      "deasignar_deco" => 0,
      "cambiar_deco_por_averia" => 0,
      "revertir_deco_por_averia" => 0,
      "reponer_deco" => 0,
      "revertir_reponer_deco" => 0
   ],
   "web_aseguramiento" => 0,
   "user_operation" => "JESCALA5"
];
