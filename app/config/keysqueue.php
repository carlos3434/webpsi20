<?php

return array(
   "TOA_OUTBOUND_COMPLETED" => "outbound_completed",
   "TOA_OUTBOUND_NOTDONE" => "outbound_notdone",
   "TOA_OUTBOUND_SUSPEND" => "outbound_suspend",
   "UNSCHEDULE" => "outbound_unschedule",
   "NOT_LEG" => "outbound_notleg",
   "WO_PRE_CLOSEHOME" => "outbound_pre_closehome",
   "WO_PRE_COMPLETED" => "outbound_precompleted",
   "WO_CANCEL" => "outbound_cancel",
   "WO_INIT" => "outbound_init",
   "TOA_VALIDATE_COORDS" => "outbound_validate_coords",
   "TOA_ACTIVATE_ROUTE" => "outbound_activate_route"
);
