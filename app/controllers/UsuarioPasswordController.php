<?php

class UsuarioPasswordController extends BaseController
{
    /*
     * Obtenemos la última fecha y hora que se realizó al cambiar
     * la contraseña
     */

    public function postObtenerultimafecha()
    {
        if (Request::ajax()) {
            $query = "SELECT `fecha`, `estado`,
                    DATEDIFF(NOW(), DATE(`fecha`)) AS difFecha
                    FROM `usuario_password`
                    WHERE `usuario` = '" . Session::get('nomusuario') . "'
                    ORDER BY id DESC
                    LIMIT 1";

            $detaAuditoria = DB::select($query);
            return Response::json(array('data' => $detaAuditoria));
        }
    }

}
