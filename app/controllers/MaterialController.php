<?php
class MaterialController extends \BaseController
{
    public function __construct(
            ErrorController $errorController
        )
    {
        $this->_errorController = $errorController;
    }

    public function postListar()
    {
                $material = Material::select(
                    'id',
                    'deslarmat as nombre',
                    DB::raw('CONCAT("T",tipmat) as relation'))
                ->get();
            
            return Response::json(array('rst'=>1,'datos'=>$material));
    }

}