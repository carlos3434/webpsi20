<?php
use Intervention\Image\Facades\Image;

class ImagenController extends BaseController
{

    protected $_errorController;

    public function __construct(ErrorController $errorController)
    {
        $this->_errorController = $errorController;
    }

    /**
     * The image cached for 10 minutes, resize generate for parameter $dimension
     * <br> How to use:
     * <br> http://web.dev/images/{$dirname}/{$dimension}/{$filename}
     * <br> Example:
     * <br> http://web.dev/images/edificio/360x240/indice.jpg
     * <br>
     * @param String $dirname partial PATH into public/img/
     * @param String $dimension use template "{width}x{height}"
     * @param String $filename only fullname with extension
     * @return Image
     * @uses Intervention\Image\Facades\Image plugin
     * @version 20160721-2
     */
    public function getImage($dirname, $dimension, $filename)
    {
        if (strpos($dimension, 'x') === false) {
            return 'Formato Dimension: anchura x altura, ej: 320x240. ';
        }
        $size = explode('x', $dimension);
        $values = Helpers::validateImagen($dirname, $filename);
        if ($values['rst'] == 2) {
            return $values['msj'];
        } elseif (count($size) != 2 || !is_numeric($size[0]) 
            || !is_numeric($size[1])) {
            return 'Dimension: no cumple, usar el formato, ej: 320x240';
        }
        list($extension, $mime, $path) = array_values($values);

        $cacheImage = Image::cache(
            function($image) use ($path, $size) {
                return $image->make($path)->resize($size[0], $size[1]);
            }, 10
        );

        $response = Response::make(
            $cacheImage, 200, ['Content-Type' => $mime[$extension]]
        );
        /*
         * -> FOR USE: $file->getMimeType();
         * $file = new Symfony\Component\HttpFoundation\File\File($publicPath);
         * -> 1RO
         * $img = Image::make($path)->resize($size[0], $size[1]);
         * ob_end_clean();
         * return $img->response('jpg');
         * -> 2DO
         * $response = Response::make(File::get($path), 200);
         * $response->header('Content-type', 'image');
         * ...
        */
        ob_end_clean(); // Fix Buffer controller
        return $response;
    }

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    public function postTareas()
    {
        try {
            $acceso = "\$PSI20\$";
            $clave = "\$1st3m@\$";
            $imagen = Input::get('img');
            $gestionId = Input::get('gestion_id');
            $tareaId = Input::get('tarea_id');
            $pos = Input::get('pos');
            $nimg = Input::get('nimg');
            $hashg = Input::get('hashg');
            $sHash = $acceso . $clave . $gestionId . $tareaId . $pos;
            $hash = hash('sha256', $sHash);

            $dirpUno = 'img/officetrack/p01/g' . $gestionId . '/';
            $dirpDos = 'img/officetrack/p02/g' . $gestionId . '/';
            $dirpTres = 'img/officetrack/p03/g' . $gestionId . '/';
            $dirf = '';
            if ($hash == $hashg) {
                if (!is_dir('img/')) {
                    mkdir('img/');
                }
                if (!is_dir('img/officetrack/')) {
                    mkdir('img/officetrack/');
                }
                if (!is_dir('img/officetrack/p01/')) {
                    mkdir('img/officetrack/p01/');
                }
                if (!is_dir('img/officetrack/p02/')) {
                    mkdir('img/officetrack/p02/');
                }
                if (!is_dir('img/officetrack/p03/')) {
                    mkdir('img/officetrack/p03/');
                }
                if (!is_dir($dirpUno)) {
                    //mkdir($dirpUno, 0777, true);
                    //chmod($dirpUno, 0777);
                    mkdir($dirpUno);
                }

                if (!is_dir($dirpDos)) {
                    //mkdir($dirpDos, 0777, true);
                    //chmod($dirpDos, 0777);
                    mkdir($dirpDos);
                }

                if (!is_dir($dirpTres)) {
                    //mkdir($dirpTres, 0777, true);
                    //chmod($dirpTres, 0777);
                    mkdir($dirpTres);
                }

                if ($pos == 1) {
                    $dirf = $dirpUno;
                } elseif ($pos == 2) {
                    $dirf = $dirpDos;
                } elseif ($pos == 3) {
                    $dirf = $dirpTres;
                }

                //for( $i=0;$i<count($imagen);$i++ ){
                if ($imagen != '') {
                    $this->base64_to_jpeg(
                        $imagen, $dirf . "i" . $tareaId . "_" . $nimg . ".jpg"
                    );
                }

                if (
                    file_exists($dirf . "i" . $tareaId . "_" . $nimg . ".jpg")
                ) {
                    echo "Finalizado";
                } else {
                    echo "Error:0003";
                }
                //}
            } else {
                echo "Error:0002";
            }
        } catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            echo "Error:0001";
        }
    }

    public function postImagen()
    {
        //try {
            $paso = Input::get('paso');
            $url = Input::get('url');
            $imagen = Input::get('imagen');
            $id = Input::get('id');

            $pos = strpos($url, "/", 4);

            if ($paso != '' and $url != '' and $imagen != '' and $id != '') {
                $dirp = 'img/officetrack/' . substr($url, 0, $pos) . '/';

                $dirf = '';
                if (!is_dir($dirp)) {
                    mkdir($dirp);
                }

                $this->base64_to_jpeg($imagen, 'img/officetrack/' . $url);

                if (file_exists('img/officetrack/' . $url)) {
                    if ($paso == 1) {
                        DB::update(
                            'UPDATE webpsi_officetrack.paso_uno SET '
                            . 'casa_img1="",casa_img2="",casa_img3="" '
                            . 'WHERE id=?', array($id)
                        );
                    } elseif ($paso == 2) {
                        DB::update(
                            'UPDATE webpsi_officetrack.paso_dos '
                            . 'SET WHERE id=?', array($id)
                        );
                    } elseif ($paso == 3) {
                        DB::update(
                            'UPDATE webpsi_officetrack.paso_tres '
                            . 'SET final_img1="",final_img2="",final_img3="",'
                            . 'boleta_img="",firma_img="" WHERE id=?',
                            array($id)
                        );
                    }
                }
            }
        /*} catch (Exception $exc) {
            return json_encode($exc);
        }*/
    }

    public function base64_to_jpeg($baseString, $outputFile)
    {
        $ifp = fopen($outputFile, "w+");
        fwrite($ifp, base64_decode($baseString));
        fclose($ifp);

        return $outputFile;
    }

}
