<?php

class GeoAlarmaTapController extends \BaseController
{

    /**
     * Valida sesion activa
     */
    public function __construct()
    {
        $this->beforeFilter('auth'); 
    }

    /**
     * @uses admin.georeferencia.alarma modulo
     * @return json
     */
    public function postAlarmaTrobas()
    {
        if (Request::ajax()) {
            $model = new GeoAlarmaTap();
            $data = $model->trobasAlarma(Input::get('zonal', ''));

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $data
                )
            );
        }
    }

    /**
     * @uses admin.georeferencia.alarma modulo
     * @return json
     */
    public function postAlarmaTrobasArea()
    {
        if (Request::ajax() && Input::has('zonal') && 
                Input::has('nodo') && Input::has('troba')) {
            $rst = 0;
            $dt = Input::get('zonal')
                . '|' .Input::get('nodo')
                . '|' .Input::get('troba');
            $model = new TablaDetalle();
            $data = $model->coordenadasPorTabla(8, $dt, true);
//            $model = new GeoTroba();
//            $data = $model->listarTrobasArea(
//                    Input::get('zonal'), 
//                    Input::get('nodo'), 
//                    Input::get('troba')
//                );
//            $data = $model->listarCoordenadasPorZonaNodoTroba(
//                    Input::get('zonal').'|'
//                    .Input::get('nodo').'|'
//                    .Input::get('troba')
//                );     
            if (count($data)) {
                //((new TablaDetalle())
                //->coordenadasGeoAlarmaTap('LIM','RI','R016'));
                $rst = 1;
            }
            return Response::json(
                array(
                    'rst' => $rst,
                    'datos' => $data
                )
            );
        }
    }

    /**
     * @uses admin.georeferencia.alarma modulo
     * @return json
     */
    public function postTapClientes()
    {
        if (Request::ajax() && Input::has('zonal') && 
                Input::has('nodo') && Input::has('troba')) {
            $rst = 0;
            $dt = Input::get('zonal')
                . '|' .Input::get('nodo')
                . '|' .Input::get('troba');
            $modela = new TablaDetalle();
            $amp = $modela->coordenadasPorTabla(1, $dt);
            $tap = $modela->coordenadasGeoAlarmaTap(
                Input::get('zonal'), 
                Input::get('nodo'), 
                Input::get('troba')
            );
//            $model = new GeoAlarmaTap();
//            $tap = $model
//                ->tapAlarmaClientes(
//                    Input::get('zonal'), 
//                    Input::get('nodo'), 
//                    Input::get('troba')
//                );
//            $amp = $model
//                ->ampAlarmaClientes(
//                    Input::get('zonal'), 
//                    Input::get('nodo'), 
//                    Input::get('troba')
//                );
            
            if (count($tap) || count($amp) ) {
                $rst = 1;
            }
            
            return Response::json(
                array(
                    'rst' => $rst,
                    'tap' => $tap,
                    'amp' => $amp
                )
            );
        }
    }

}
