<?php

class GeoAlarmaTapColorController extends \BaseController
{

    /**
     * Valida sesion activa
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     *
     * @return type
     */
    public function postListar()
    {
        if (Request::ajax()) {

            $data = GeoAlarmaTapColor::getGeoAlarmaTapColor();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $data
                )
            );
        }
    }

}
