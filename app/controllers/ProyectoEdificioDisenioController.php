<?php

/**
 * route: proyectoedificiodisenio
 */
class ProyectoEdificioDisenioController extends \BaseController
{
    protected $_errorController;
        
    public function __construct(ErrorController $errorController)
    {
        $this->_errorController = $errorController;
        $this->beforeFilter('auth');
    }
    
    public function postCargarPregunta()
    {
        if (Request::ajax()) {
            $datos = ProyectoPreguntaRelacion::getPreguntaAgrupada();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCrearDisenio()
    {
        if (Request::ajax()) {
            $input = Input::all();
            $fechaTiempo = date("Y-m-d H:i:s");
            $insertar = '';
            $proyectoEdificioId = $input['proyecto_edificio_id'];
            unset($input['proyecto_edificio_id']);
            $count = count($input);
            try{
                DB::beginTransaction();
                foreach ($input as $k => $v) {
                    if (empty($v)) {
                        continue;
                    }
                    $interno = array();
                    $interno['proyecto_edificio_id'] = $proyectoEdificioId;
                    $interno['pregunta_relacion_id'] = $k;
                    $interno['created_at'] = $fechaTiempo;
                    $interno['usuario_created_at'] = Auth::id();
                    $interno['respuesta'] = $v;
                    $interno['sustento'] = '';
                    if (is_array($v)) {
                        $interno['respuesta'] = isset($v[1])? $v[1] : '';
                        $interno['sustento'] = isset($v[0])? $v[0] : '';
                    }
                    if (
                        GestionEdificioDisenio::existePorEdificio(
                            $proyectoEdificioId, 
                            $k
                        )
                    ) {
                        GestionEdificioDisenio::actualizar(
                            $proyectoEdificioId, $k, $interno
                        );
                    } else {
                        GestionEdificioDisenio::crear($interno);
                    }
                }
                
                DB::commit();    
                $rst = 1;
                $msn = trans('main.success_register');
            } catch (Exception $ex) {
                DB::rollback();
                $this->_errorController->saveError($ex);
                $rst = 0;
                $msn = trans('main.error_operacion');
            }
            
            return Response::json(
                array(
                    'rst' => $rst,
                    'datos' => $insertar,
                    'test' => $input,
                    'count' => $count,
                    'msj' => $msn,
                )
            );
        }
    }
    
    public function postLeerDisenio()
    {
        $proyectoEdificioId = Input::get('proyecto_edificio_id');
        if (Request::ajax() && !empty($proyectoEdificioId) ) {
            $datos = GestionEdificioDisenio::getDato($proyectoEdificioId);
            
            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
        return Response::json(
            array(
                'rst' => 0,
                'datos' => ''
            )
        );
    }
}

//fin class
