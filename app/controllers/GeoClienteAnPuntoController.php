<?php
class GeoClienteAnPuntoController extends \BaseController
{
    public function postListar()
    {

        $r =GeoClienteAnPunto::postGeoClienteAnPuntoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoClienteAnPunto::postTipoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postTipo()
    {
        $array['zonal']=Input::get('zonal');
        $array['tipo']=Input::get('tipo');
        $r =GeoClienteAnPunto::postOrigenFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postOrigen()
    {
        $array['zonal']=Input::get('zonal');
        $array['tipo']=Input::get('tipo');
        $array['origen']=Input::get('origen');
        $r =GeoClienteAnPunto::postClienteFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

}

