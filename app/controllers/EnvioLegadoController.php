<?php
use Legados\models\EnvioLegado;
class EnvioLegadoController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postCargar(){
        
            $fecha = Input::get("fecha");
            $formFecha = explode(',', $fecha);
            $EnvioLegado_Host=Input::get("EnvioLegado_Host", null);
            $EnvioLegado_Accion=Input::get("EnvioLegado_Accion", null);

            $query=  EnvioLegado::from('envio_legado as el')   
                    
                    ->select('el.id as id',
                        'el.accion as accion',
                        'el.request as request',                           
                        'el.response as response',
                        'el.host as host',
                        'el.created_at as fech_creacion',
                        'u.usuario as usuario'
                            )

                    ->join('usuarios as u','el.usuario_created_at','=','u.id')
                    ->where(function($query) use ($EnvioLegado_Host, $EnvioLegado_Accion,$fecha,$formFecha){
                    if($EnvioLegado_Host != "" && $EnvioLegado_Host != null){
                        $query->where('host', $EnvioLegado_Host);
                    }
                    if($EnvioLegado_Accion != "" && $EnvioLegado_Accion != null){
                        $query->where('accion', $EnvioLegado_Accion);
                    }if($fecha!="" && $fecha != null){
                    $query->whereRaw(" date(el.created_at) BETWEEN '". $formFecha[0]."' and '".$formFecha[1]."'");
                    }
                 });          
                $datos=$query->searchPaginateAndOrder();                                
                return $datos;
                

    }
    //Funcion para trata los datos de json a xml
    public function postListardetalle()
    {
        $data = EnvioLegado::find(Input::get("id"));

        $xmlRequest = \Array2XML::createXML('envio_legado', json_decode($data->request, true));
        
        $xmlResponse = \Array2XML::createXML('envio_legado', json_decode($data->response, true));

        $request = $xmlRequest->saveXML();
        $response = $xmlResponse->saveXML();

        return [
            "request" => $request,
            "response" => $response,
        ];
    }
    
}
