<?php
class GeoTablaController extends \BaseController
{
    public function postListar()
    {

        $r =GeoTabla::postElementoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }

    public function postGenerartabla()
    {
        //debe insertar la tabla y obtener el tabla_id
        //insertar tabla_campos y obtener los idarray que son los id de los campos

        $figura=Input::get("figura"); //tipo figura: 2-poligono, 3 - punto
        $tabla= Input::get("tabla"); //nombre tabla
        $etiqueta = Input::get("etiqueta");
        $campos = Input::get("campos");

        $tablaid=GeoTabla::insertTabla($etiqueta,$tabla,$figura);
        GeoCampos::insertarCampos($tablaid,$campos);

        $nombres = array();
        $ids = array();

        foreach($campos as $campo){
            $nombres[] = $campo["name"];
            $ids[] = $campo["id"];
        }

        $idarray=implode("|", $ids);
        $array = $nombres;
        $groupby=array();
        $orden='';
        $radio=''; //radio
        $user=Auth::id(); //usuario
        $whereuno='where ';

        for ($i=0; $i<count($array); $i++) { //validar no vacios
            if ($i==0) {
                $whereuno.=$array[$i]."<>'' ";
            } else {
                $whereuno.=" and ".$array[$i]."<>'' ";
            }
        }

        $query = " select ".implode(",", $array)." from ".$tabla." ".
            $whereuno.
            " GROUP BY ".implode(",", $array).
            " ORDER BY ".implode(",", $array);

        $lista = DB::select($query);

        if ($lista) {

            if ($figura==2) { //poligono
                $orden=" ORDER BY orden asc ";
            }
            if ($figura==1) { //circulo
                $radio=', radio';
            }

            foreach ($lista as $key => $value) {

                $whereselect=''; //reinicia el where
                $insert='';
                for ($i=0; $i<count($array); $i++) {
                    if ($i==0) { //primero
                        $whereselect.= $array[$i]."='".$value->$array[$i]."' ";
                    } else {
                        $whereselect.= " and ".$array[$i]."='".$value->$array[$i]."' ";
                    }

                    if ($i==count($array)-1) { //ultimo
                        $insert.=$value->$array[$i];
                    } else {
                        $insert.=$value->$array[$i]."|";
                    }

                }

                $sqlcoord= "SELECT coord_y as lat, coord_x as lng ".$radio." from "
                    .$tabla.
                    " WHERE ".$whereselect."  ".$orden;

                $listacoord = DB::select($sqlcoord);

                $coordenadas= array();
                $dato='';
                foreach ($listacoord as $key => $valores) {
                    array_push($coordenadas, $valores->lng.','.$valores->lat);
                    if ($figura==1) {
                        $dato=$valores->radio;
                    }
                }
                if (count($coordenadas)>0) {
                    $coord=implode("|", $coordenadas);

                    $sqlmax = 'SET group_concat_max_len := @@max_allowed_packet;';
                    DB::insert($sqlmax);

                    $sqlinsert="insert into geo_tabla_detalle(tabla_id,campos,
                 detalle,coord,usuario_created_at,created_at,dato)
                 values(".$tablaid.",'".$idarray."','".$insert."','".$coord."',
                 '".$user."',now(),'".$dato."')";
                    DB::insert($sqlinsert);
                }

            }
        }

        return 1;
    }

    public function postBuscartabla(){
        if( Request::ajax() )
        {
            $id = Input::get("id");
            $tabla = GeoTabla::getCampos($id);
            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $tabla
                )
            );
        }
    }

    public function postEditartabla()
    {

        $idTabla = explode("_",Input::get("idTabla"));
        $idTabla = $idTabla[0];

        $result = GeoTabla::deleteTable($idTabla);

        if($result){
            $tabla = Helpers::ruta(
                'geotabla/generartabla',
                'POST',
                array('tabla' => Input::get('tabla'),
                        'figura'=>Input::get('figura'),
                        'etiqueta' => Input::get('etiqueta'),
                        'campos'=>Input::get('campos')
                ),
                false
            );
            if($tabla)
            {
                return Response::json(
                    array(
                        'rst' => 1,
                        'msj' => "Tabla editada correctamente"
                    )
                );
            }else{
                return Response::json(
                    array(
                        'rst' => 0,
                        'datos' => "No se pudo actualizar la tabla"
                    )
                );
            }
        }
    }
}

