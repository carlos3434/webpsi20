<?php

class ProvinciaController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            try {
                if(Input::get('parametros')){
                    $result = DB::table('provincia')
                                ->select(
                                    'provincia_id as id',
                                    'nombre',
                                    DB::RAW('CONCAT("D",departamento_id) AS relation')
                                    );

                    if(Input::get('valor')){
                            $result->WhereIn('departamento_id',Input::get('valor'));
                        }
                    $result->orderBy('nombre');
                    $result=$result->get();
                }
            } catch (Exception $error) {
               $this->error->handlerError($error);
            }
            return Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
