<?php
class DecodificadorEquivalenciaController extends \BaseController
{
    public function postList()
    {
        $datos = DecodificadorEquivalencia::orderBy('id', 'desc')
                ->get();

        return [
            'datos' => $datos
        ];
    }

    public function postInsert()
    {
        $motivoOfsc = new DecodificadorEquivalencia();
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->codmat = Input::get('codmat');
        $motivoOfsc->codmat_equivalencia = Input::get('codmat_equivalencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }

    public function postUpdate()
    {
        $motivoOfsc = DecodificadorEquivalencia::find(Input::get('id'));
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->codmat = Input::get('codmat');
        $motivoOfsc->codmat_equivalencia = Input::get('codmat_equivalencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }
}

