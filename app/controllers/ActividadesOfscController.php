<?php

use Ofsc\Activity;

class ActividadesOfscController extends BaseController
{
    /**
     * consulta de actividade por bucket
     */
    public function show($resourceId)
    {
        $pos = strpos(URL::previous(), 'train');
        $refresh = Input::get("refresh", false);
        if ($refresh) {
            $objactivity = new Activity;
            $from = date("Y-m-d");
            $to  = date("Y-m-d", strtotime("+ 7 days"));
            $bucket = $resourceId;
            $actividades = $objactivity->getActivities($bucket, $from, $to);
            if ($actividades->error) {
                $actividades = [];
            } else {
                $actividades = $actividades->data;
            }
            $this->load_activities();
            return json_encode($actividades);
        }
        if ($pos === false) {
            $hoy = date("Ymd");
            $file = public_path()."/json/actividades/".$resourceId."/".$hoy.".json";
            $json = '[]';
            if (File::exists($file)) {
                $json = File::get($file);
            }
            return $json;
        } else {
            $to = $from = date("Y-m-d");
            \Config::set("ofsc.auth.company", 'telefonica-pe.train');
            $activity = new Activity();
            $resultado = $activity->getActivities($resourceId, $from, $to);
            return Response::json($resultado->data);
        }
    }

    public function load_activities()
    {
        $activity     = new Activity();
        $objAOM     = new ActividadOfscMovimiento();
        $from         = date("Y-m-d");
        $to         = date("Y-m-d", strtotime("+ 7 days"));
        $fileName     = preg_replace("/-/", "", $from).".json";
        $buckets = OfscHelper::getFileResource();
        $bucketsId = OfscHelper::getIdBucket($buckets);        
       
        foreach ($bucketsId as $key => $value) {
            $rutaFolder = public_path()."/json/actividades/".$value;
            $existeFolder = FileHelper::validaCrearFolder($rutaFolder);
            if ($existeFolder === true) {
                $rutaFile = $rutaFolder."/".$fileName;
                $actividades = $activity->getActivities($value, $from, $to);

                if (isset($actividades->data)) {
                    if (count($actividades->data) > 0) {

                        $json = [];
                        if (File::exists($rutaFile)) {
                            $json = json_decode(
                                File::get($rutaFile), true
                            );                            
                           
                            unlink($rutaFile);
                        }
                        File::put($rutaFile, json_encode($actividades->data));
                        $dataNueva = [];
                        foreach ($actividades->data as $key2 => $value2) {
                            if (isset($value2["appt_number"]))
                                $dataNueva[] = $value2;
                        }
                        $objAOM->setCambios($dataNueva, $json);
                    }
                }
            }
        }
    }
}
