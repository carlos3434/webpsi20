<?php

use Ofsc\Ofsc;
use Ofsc\Activity;

class ActividadesOfscController extends BaseController
{

    public function __construct()
    {
        /*$this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );*/
    }
    /**
     * mostrar componente especifico
     */
    public function show($resourceId)
    { 
        $from = date("Y-m-d");
        $to = $from;
        $type = '';
        $filename = date("Ymd");//'train';
        if (Input::has('type')) {
            $type = Input::get('type');
        }  

        if (Input::has('dateFrom') && Input::has('dateTo')) {
            $from = Input::get('dateFrom');
            $to = Input::get('dateTo');
            // $xml = (array) simplexml_load_file(public_path().'/xml/actividadestrain.xml');
            $old = \Config::get("ofsc.auth.company");
            //if ($type == 'train') {
            \Config::set("ofsc.auth.company", 'telefonica-pe.train'); // .train
            //}
            $bucket = $resourceId;

            $rutaFolderActividades = public_path() . "/json" ."/actividades";
            $folder = preg_replace("/-/", "_", $bucket);
            $rutaFolder = $rutaFolderActividades."/".$folder;
            $rutaFile = $rutaFolder."/".preg_replace("/-/", "", $from)."t.json";

            $activity = new Activity();
            $resultado = $activity->getActivities($bucket,$from, $to);
            $actividad = $resultado->data;
            \Config::set("ofsc.auth.company", $old);   

            if (File::exists($rutaFile)) {
                if (unlink($rutaFile)) {
                    File::put($rutaFile,json_encode($actividad));    
                }
            } else {
                File::put($rutaFile,json_encode($actividad));
            }

            return Response::json($actividad);
        } elseif ($type == 'train') {
            // $filename = $filename . 't'
            $hoy = preg_replace("/-/", "", $filename);
            $file = public_path()."/json/actividades/".$resourceId."/".$hoy."t.json";
            if (!File::exists($file)) {
                $json = '[]';   
            } else {
                $json = File::get($file);
            }
        } 
        else {
            $filename = $from;
            $hoy = preg_replace("/-/", "", $filename);
            $file = public_path()."/json/actividades/".$resourceId."/".$hoy.".json";
            if (!File::exists($file)) {
                $json = '[]';   
            } else {
                $json = File::get($file);
            }
        }

        return $json;
    }

}
