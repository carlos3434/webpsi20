<?php
use Repositories\ListadoLegoRepositoryInterface;

Class ListadoLegoController extends \BaseController
{
    protected $repository;

    public function __construct(ListadoLegoRepositoryInterface $listadoLegoRepositoryInterface)
    {
        $this->repository = $listadoLegoRepositoryInterface;
    }

    public function postSelects()
    {
        $selects = json_decode(Input::get('selects'), true);
        $datos = json_decode(Input::get('datos'), true);
        $valores = array();
        foreach ($selects as $key => $value) {
            $string = '$this->repository->'.$value.'($datos[$key])';
            $valores[] = eval( "return $string;" );
        }
        
        return Response::json(
            array(
                'rst'  => 1,
                'data' => $valores,
            )
        );
    }
}



