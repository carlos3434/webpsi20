<?php
use Illuminate\Support\Collection;

class PerfilController extends \BaseController
{

    /**
     * Store a newly created resource in storage.
     * POST /perfil/cargar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $perfilId = Session::get('perfilId');
            if ($perfilId==8) {
                $perfiles = DB::table('perfiles')
                        ->select('id', 'nombre', 'estado')
                        ->get();
            } else {
                $perfiles = DB::table('perfiles')
                        ->select('id', 'nombre', 'estado')
                        ->where('id', '<>', 8)
                        ->get();
            }
            return Response::json(array('rst'=>1,'datos'=>$perfiles));
        }
    }
    /**
     * Store a newly created resource in storage.
     * POST /perfil/cargar
     *
     * @return Response
     */
    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            if (Input::has("opt")) {
                $idlogeado = Auth::user()->id;
                $order = [
                    "column" => Input::get('column'),
                    "dir" => Input::get('dir')
                ];

                $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
                $grilla["order"] = $order;
                $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

                $usuarios = Perfil::select(
                    "perfiles.*",
                    DB::raw("IF(perfiles.estado = 1, 'Activo', 'Inactivo') AS estadoperfil")
                );

                $search = Input::get("search", "");
                if ($search!="") {
                    $usuarios->whereRaw("(perfiles.nombre LIKE '%$search%')");
                }

                $column = "id";
                $dir = "desc";

                if (isset($grilla["order"])) {
                    $column = $grilla["order"]["column"];
                    $dir = $grilla["order"]["dir"];
                }
                $usuarios = $usuarios
                            ->orderBy($column, $dir)
                            ->paginate($grilla["perPage"]);
                $data = $usuarios->toArray();
                $col = new Collection([
                    'recordsTotal'=> $usuarios->getTotal(),
                    'recordsFiltered'=> $usuarios->getTotal(),
                ]);
                return $col->merge($data);
            } else {
                $perfilId = Session::get('perfilId');
                if ($perfilId==8) {
                    $perfiles = DB::table('perfiles')
                            ->select('id', 'nombre', 'estado')
                            ->get();
                } else {
                    $perfiles = DB::table('perfiles')
                            ->select('id', 'nombre', 'estado')
                            ->where('id', '<>', 8)
                            ->get();
                }
                return Response::json(array('rst'=>1,'datos'=>$perfiles));
            }
        }
    }

    public function getEditar()
    {
        $id = Input::get("id", 0);
        $cacheperfil = null;
        if ($id > 0) {
            $cacheperfil = Cache::get("perfil{$id}");
            if (!$cacheperfil) {
                $cacheperfil = Cache::remember(
                    "perfil{$id}",
                    24*60*60,
                    function () use ($id) {
                        $perfil = Perfil::with("opciones")->find($id);
                        foreach ($perfil->opciones as $key => $value) {
                            $opciones = Opcion::with("criterios")->find($value->opcion_id);
                            $criterios = $opciones->criterios;
                            foreach ($criterios as $key2 => $value2) {
                                $criterios[$key2]->obj = Criterios::find($value2->criterio_id);
                            }
                            unset($opciones["criterios"]);
                            $perfil->opciones[$key]->opcion = $opciones;
                            $perfil->opciones[$key]->criterios = $criterios;
                        }
                        return $perfil;
                    }
                );
            }
        }
        $perfil = $cacheperfil;
        if (!is_null($perfil)) {
            return  Response::json(["rst" => 1, "obj" => $perfil]);
        } else {
            return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
    }

    public function postGuardar()
    {
        $objvalidar = [
            "nombre" => Input::get("nombre", "")
        ];
        $validator = Validator::make($objvalidar, Perfil::$rules);
        if ($validator->fails()) {
            $msjError = $validator->messages()->all()[0];
            return  Response::json(["rst" => 2, "msj" => $msjError]);
        }
        $obj = ["id" => Input::get("id", 0),
            "nombre" => Input::get("nombre"),
            "opcionesperfil" => Input::has("opcion_perfil") ? Input::get("opcion_perfil") : []
        ];

        try {
            if ($obj["id"] > 0) {
                $objperfil = Perfil::find($obj["id"]);
            } else {
                $objperfil = new Perfil;
            }
            $objperfil->nombre = $obj["nombre"];
            $objperfil->save();

            PerfilOpcion::where(["perfil_id" => $objperfil->id])->delete();
            $insert = [];
            foreach ($obj["opcionesperfil"] as $key => $value) {
                $insert[] = ["perfil_id" => $objperfil->id, "opcion_id" => $value];
            }
            if (count($insert) > 0) {
                PerfilOpcion::insert($insert);
            }
            Cache::forget("perfil{$objperfil->id}");
            return Response::json(["rst" => 1, "msj" => "Perfil Guardado con Exito", "obj" => $objperfil]);
        } catch (Exception $e) {
            return Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
        
    }

    public function postEliminar()
    {
        $id = Input::get("id", 0);
        try {
            $perfil = Perfil::find($id);
            if (!is_null($perfil)) {
                $perfil->usuario_deleted_at = Auth::id();
                $perfil->save();
                $perfil->delete();
                Cache::forget("perfil{$id}");
                return Response::json(["rst" => 1, "msj" => "Perfil Eliminado con Exito"]);
            } else {
                return Response::json(["rst" => 2, "msj" => "Error al Eliminar"]);
            }
        } catch (Exception $e) {
            return Response::json(["rst" => 2, "msj" => "Error!!!"]);
        }
    }

    public function getCambiarestado()
    {
        $id = Input::get("id", 0);
        $estado = Input::get("estado");
        $perfil = Perfil::find($id);
        if (!is_null($perfil)) {
            if ($estado == 0) {
                $perfil->estado = 1;
            } else {
                $perfil->estado = 0;
            }
            $perfil->save();
            Cache::forget("perfil{$id}");
            return  Response::json(["rst" => 1, "obj" => $perfil]);
        } else {
            return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
    }
}
