<?php

class ImportarOfscController extends \BaseController
{

    protected $_errorController;

    public function __construct(ErrorController $errorController)
    {
        $this->_errorController = $errorController;
    }

    public function getImportarfiles() 
    {
        $array=Input::get('files');
        $usuario=Auth::id();

        foreach ($array as $key => $value) { 
          echo "Analizando: ". $value. " ... \n";
          $ficheros=scandir($value);
          //var_dump($ficheros);
          foreach ($ficheros as $key => $filecsv) { 
          
            if(stristr($filecsv, '.csv')){

              if(file_exists($value.'/'.$filecsv) ) {
                $handle = fopen($value.'/'.$filecsv, "r");
                
                if( $handle && $filecsv=='telefonica-pe_messages.csv') {
                  $i = 0;
                  echo "Importando Data de: ".$filecsv." ...  ";
                  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                   $i++;
                   if($i!=1){
                        $datos=ImportarOfsc::getInsertarMessage($data);
                   }
                  }
            
                  fclose($handle);
                  echo " OK \n ";
                }

                if( $handle && $filecsv=='telefonica-pe_message_text.csv') {
                  $i = 0;
                  echo "Importando Data de: ".$filecsv." ... ";
                  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                   $i++;
                   if($i!=1){
                        $datos=ImportarOfsc::getInsertarMessageText($data);
                   }
                  }
            
                  fclose($handle);
                  echo " OK \n ";
                }


              } 
            }
          }
        }

        if(count($array)>0)
        return "\n ------------  OK  ------------ \n";
        else
        return "\n ---- Sin Archivos por Descargar ---- \n";  
       // return Helpers::getResponseJson($output, '', $extra);
    }







}
