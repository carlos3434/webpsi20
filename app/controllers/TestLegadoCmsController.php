<?php
use Legados\STCmsApi;
use Legados\models\SolicitudTecnicaCms;
use Legados\Componente as ComponenteDeco;
use Legados\models\ComponenteOperacion;
use Legados\models\SolicitudTecnicaUltimo;

class TestLegadoCmsController extends BaseController
{
    public function getUpdatecms()
    {
        $solicitudCms = SolicitudTecnicaCms::get();
        foreach ($solicitudCms as $key => $value) {
            SolicitudTecnicaUltimo::where('solicitud_tecnica_id', $value["solicitud_tecnica_id"])
            ->update([
                'num_orden_trabajo' => $value["orden_trabajo"], 
                'num_orden_servicio' => $value["cod_servicio"]
            ]);
        }
    }

    public function postRespuesta()
    {
    	$solicitud = Input::get("id_solicitud_tecnica");
	    $trama = DB::table('solicitud_tecnica_log_recepcion as st')
                ->where('st.id_solicitud_tecnica', '=', $solicitud)
                ->orderBy('st.id', 'desc')->first();
		    
	    $respuesta = [
	        'id_solicitud_tecnica' => $trama->id_solicitud_tecnica,
	        'id_respuesta' => "1",
	        'observacion' => "Ok",
	        'solicitud_tecnica_id' => $trama->solicitud_tecnica_id
	    ];

	    print_r($respuesta);
	    \Config::set("legado.wsdl.respuestast", \Config::get("legado.wsdl.respuestast_desarrollo"));
	    $objApiST = new STCmsApi();
	    $objApiST->responderST($respuesta);
    }

    public function postActivar()
    {
        $peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq', ''),
            'numcompxreq'           => \Input::get('numcompxreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codtar'                => \Input::get('codtar', ''),
            'numtar'                => \Input::get('numtar', ''),
            'codact'                => \Input::get('codact'),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        print_r($peticion);

        \Config::set("legado.wsdl.RegistroDecos", \Config::get("legado.wsdl.RegistroDecos_desarrollo"));
        $componente = new ComponenteDeco();
        $respuesta = $componente->asignacionDecos($peticion, '');
        print_r($respuesta);
    }

    public function postRefresh()
    {
    	$peticion = [
            'codreq'                => \Input::get('codreq'),
            'indorigreq'            => \Input::get('indorigreq'),
            'codmat'                => \Input::get('codmat'),
            'numser'                => \Input::get('numser'),
            'codtar'                => \Input::get('codtar'),
            'codact'                => \Input::get('codact'),
            'solicitud_tecnica_id'  => \Input::get('solicitud_tecnica_id')
        ];

        print_r($peticion);

        \Config::set("legado.wsdl.RegistroDecos", \Config::get("legado.wsdl.RegistroDecos_desarrollo"));
        $componente = new ComponenteDeco;
        $respuesta = $componente->refresh($peticion, '');
        print_r($respuesta);
    }

    public function postCierre()
    {
    	// ACI_LIQ | ACI_DEV | PCI_LIQ | PCI_DEV
        $id_solicitud_tecnica = Input::get('id_solicitud_tecnica');
        $solicitud_tecnica_id = Input::get('solicitud_tecnica_id');
    	// $codactu = Input::get('codactu');

    	$tipoOperacion = Input::get('tipo_operacion', '');
    	$fecha_envio = Input::get('fecha_envio', '');
    	$hora_envio = Input::get('hora_envio', '');
        $codigo_liquidacion = Input::get('codigo_liquidacion');
    	$codigo_tecnico1 = Input::get('codigo_tecnico1', '');
    	$fecha_inicio = Input::get('fecha_inicio', '');
    	$hora_inicio = Input::get('hora_inicio', '');
    	$fecha_fin = Input::get('fecha_fin', '');
    	$hora_fin = Input::get('hora_fin', '');
    	$fecha_liquida = Input::get('fecha_liquida', '');
    	$hora_liquida = Input::get('hora_liquida', '');
    	$codigo_contrata = Input::get('codigo_contrata', '');
        $campo1 = Input::get('campo1', '');
        $campo2 = Input::get('campo2', 'REDCLTE');
        $campo3 = Input::get('campo3', '');

    	// $datasolicitud = SolicitudTecnicaCms::validaLego($codactu);
        $data = DB::table('solicitud_tecnica_log_recepcion')
                ->where('id_solicitud_tecnica', $id_solicitud_tecnica)
                ->orderBy('id', 'desc')
                ->first();
        $dataArray = json_decode($data->trama, true);

    	$solicitud = $dataArray;
        $componentes = $dataArray['componentes']['componente'];
        $componentesArray = [];

        foreach ($componentes as $key => $value) {
            $componentesArray[] = [
                'numero_requerimiento' => $value['numero_requerimiento'],
                'numero_ot'            => $value['numero_ot'],
                'numero_servicio'      => $value['numero_servicio'],
                'componente_cod'       => $value['componente_cod'],
                'marca'                => $value['marca'],
                'modelo'               => $value['modelo'],
                'tipo_equipo'          => $value['componente_tipo'],
                'numero_serie'         => $value['numero_serie'],
                'casid'                => $value['casid'],
                'cadena1'              => $value['valor1'],
                'cadena2'              => $value['valor2'],
                'cadena3'              => $value['valor3'],
                'cadena4'              => $value['valor4'],
                'cadena5'              => "",
            ];
        }

        if (!is_null($solicitud)) {
            $elementos = [
                "solicitud_tecnica_id"          => $solicitud_tecnica_id,
                "tipo_operacion"                => $tipoOperacion,
                "id_solicitud_tecnica"          => $solicitud['id_solicitud_tecnica'],
                "fecha_envio"                   => $fecha_envio,
                "hora_envio"                    => $hora_envio,
                "codigo_liquidacion"            => $codigo_liquidacion,
                "codigo_tecnico1"               => $codigo_tecnico1,
                "codigo_tecnico2"               => '',
                "fecha_inicio"                  => $fecha_inicio,
                "hora_inicio"                   => $hora_inicio,
                "fecha_fin"                     => $fecha_fin,
                "hora_fin"                      => $hora_fin,
                "fecha_liquida"                 => $fecha_liquida,
                "hora_liquida"                  => $hora_liquida,
                "nombre_contacto"               => $solicitud['nom_cliente'],
                "parentesco_contacto"           => '',
                "dni_contacto"                  => '',
                "codigo_contrata"               => $codigo_contrata,
                "observacion"                   => 'TEST CIERRE CMS',
                "campo1"                        => $campo1,
                "campo2"                        => $campo2,
                "campo3"                        => $campo3,
                "campo4"                        => $solicitud['valor4'],
                "campo5"                        => $solicitud['valor5'],
                "componentes"                   => $componentesArray
            ];

            print_r($elementos);
            \Config::set("legado.wsdl.respuestast", \Config::get("legado.wsdl.respuestast_desarrollo"));
            $objStapi = new STCmsApi;
            $response = $objStapi->cierre($elementos);
        }
    }
}