<?php

class OfscLegadosController extends \BaseController
{

    protected $_errorController;

    public function __construct(ErrorController $errorController)
    {
        $this->_errorController = $errorController;
    }

    public function getOfsccompletado() 
    {

        $datos=OfscLegados::getCrearTxtLegados();

        return $datos;

    }

}
