<?php

class BucketController extends BaseController
{
    public function postListar()
    {
        $datos = Bucket::where('estado', 1)->get();
        return [
            'rst' => 1,
            'datos' => $datos
        ];
    }

    public function postCargar()
    {
        $datos = Bucket::select('id','nombre')
        		->where('estado', 1)->get();
        return [
            'rst' => 1,
            'datos' => $datos
        ];
    }
}