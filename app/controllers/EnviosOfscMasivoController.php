<?php

class EnviosOfscMasivoController extends \BaseController
{
	protected $_errorController;
	public function __construct($_errorController = null)
	{
		$this->_errorController = $_errorController;
	}
	public function failed()
	    {
	        // Called when the job is failing...
	        Log::error('Job failed!');
	    }
    	public function fire($job, $data)
	    {
	        $id = $job->getJobId();
	        //try {
	            if ($data["tipo"] == 1) {
	            	$envioOrden = EnvioMasivoOfsc::envioOrdenOfsc( $data["orden"], $data["isMasivoBandeja"] , $data["i"], $data["grupoEnvioId"], $data["idUsuario"]);
	            } elseif ($data["tipo"] == 2) {
	            	$envioOrden = EnvioMasivoOfsc::envioLiquidadoOfsc( $data["aid"], $data["codactu"] , $data["mdf"], $data["actividad_tipo_id"], $data["empresa"], $data["idMasivoOfscGrupo"]);
	            }
	            $job->delete();
	        /*} catch (Exception $exec) {
	            //si los intentos de fallido es mayor de 1
	            echo "error";
	            if ($job->attempts() > 1) {
	                if ($this->_errorController == null) {
	                }else{
	                   $this->_errorController->saveError($exec);
	                }
	                $job->delete();
	                //registrar en log de errores
	            } else {
	                Log::error('Job failed!'.$exec);
	                //ejecutar la cola despues de 30 segundos
	                $job->release(30);
	            }
	        }*/
	    }
}