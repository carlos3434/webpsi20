
<?php
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use UsuarioSolicitudTecnicaMovimiento as UsuarioSolicitudMovimiento;
use UsuarioSolicitudTecnica as UsuarioSolicitudUltimo;
use Legados\models\SolicitudTecnica as SolicitudTecnica;
use Repositories\AsignacionRepositoryInterface as Asignacion;

class GestorDevueltasController extends BaseController
{
	protected $_asignacion;
    public function __construct(Asignacion $asig)
    {
        $this->_asignacion = $asig;
    }

	
	public function postListar(){
		$usuarios = $this->_asignacion->usuariosHabilitados();

		if(Input::get('accion')){
	           $data = json_decode(json_encode($usuarios), true);
	           return Helpers::exportArrayToCsv($data, 'Usuarios_devueltas');
	    }
		return $usuarios; 	
	}

	/*public function postGuardarbolsa(){
		$user_id = Input::get('user_id');
		$cantidad = Input::get('cantidad');
		$user_bolsa = UsuarioCantSolicitudes::where('usuario_id', '=', $user_id)->first();
		if($user_bolsa != null){
			$user_bolsa->cantidad = $cantidad;
			$user_bolsa->save();
		}
		else{
			//$data = ['usuario_id' => $user_id, 'cantidad' => $cantidad];
			//UsuarioCantSolicitudes::create($data);
			$user_bolsa  = new UsuarioCantSolicitudes;
			$user_bolsa->usuario_id = $user_id;
			$user_bolsa->cantidad = $cantidad;
			$user_bolsa->save();
		}
		return Response::json(
            array(
            'rst'=>1,
            'msj'=>'Registro actualizado correctamente',
            )
        );

	}*/

	public function postCambiarestado(){	
		$action = $this->_asignacion->changeState();
		return Response::json(
            array(
            'rst'=>($action) ? 1 : 0,
            'msj'=>'Registro actualizado correctamente',
            )
        );
	}

	public function postListarusuarios(){
		if (Request::ajax()) {
			$usuarios = Usuario::where(['perfil_id' => 13, 'estado' => 1])->get();
		}
		return Response::json(['datos' => $usuarios, 'rst' => 1]);
	}

	public function postReasignar(){
		if (Request::ajax()) {
			$asignados = $this->_asignacion->asignar();
			return Response::json(['asignados'=>$asignados['asignados'],'pendientes'=>$asignados['restante']]);
		}
	}
}