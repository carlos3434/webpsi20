<?php
use Ofsc\Inbound;
use Ofsc\Capacity;
use Ofsc\Activity;

class BandejaController extends \BaseController
{
    protected $_visorgpsController;
    protected $_errorController;
    protected $_gestionMovimientoController;
    protected $_gestionController;

    public function __construct(
        VisorgpsController $visorgpsController,
        ErrorController $errorController,
        GestionMovimientoController $gestionMovimientoController,
        GestionController $gestionController
    )
    {
        $this->beforeFilter('auth');
        $this->_visorgpsController = $visorgpsController ;
        $this->_errorController = $errorController;
        $this->_gestionMovimientoController = $gestionMovimientoController;
        $this->_gestionController = $gestionController;
    }

    /**
     * muestra los detalles de una actividad
     */
    public function postDetallebandeja()
    {
        $aid = Input::get('aid');
        $activity =  new Activity();
        $response = $activity->getActivity($aid);
        if (isset($response->error) && $response->error===false) {
            return Response::json($response->data);
        } else {
            return Response::json($response);
        }
        return [];
    }

    /**
     * @deprecated since version number
     */
    public function postEstadoofsc()
    {
        $aid = Input::get('aid');
        $activity =  new Activity();
        $response = $activity->getActivity($aid);
        if (isset($response->error) && $response->error===false
            && isset($response->data['status']) ) {

            $estadoofsc = DB::table('estados_ofsc AS e')
                        ->where('e.name', '=', $response->data['status'])
                        ->first();
            $estadoofsc = is_null($estadoofsc)? ['id' => 0] : $estadoofsc;
            return Response::json($estadoofsc);
        }
        return Response::json(["id" => 0]);
    }
    /**
     * Actualiza propiedades en ofsc
     * 
     */
    public function postUpdateofsc()
    {
        $activityId = Input::get('aid');
        
        $codactu = Input::get('codactu');
        if (Input::has('fftt'))  $fftt=Input::get('fftt', null);
        $ft = explode("|", $fftt);

        $actividad=array(); // solo propiedades para enviar a TOA
        if (isset($ft['0']) && isset($ft['1']) )
            $actividad['XA_WORK_ZONE_KEY']=$ft['0'].'|'.$ft['1'];
        else $actividad['XA_WORK_ZONE_KEY']=0;

        $actividad['address']=Input::get('direccion');
        $actividad['coordx']=floatval(Input::get('coord_x'));
        $actividad['coordy']=floatval(Input::get('coord_y'));
        $actividad['phone']=Input::get('telefono');
        $actividad['XA_CONTACT_PHONE_NUMBER_2']=Input::get('celular');
        //$actividad['XA_CONTACT_PHONE_NUMBER_3']=Input::get('celular');
        //$actividad['XA_CONTACT_PHONE_NUMBER_4']=Input::get('celular');
        //$actividad['XA_CONTACT_NAME']=Input::get('celular');
        //$actividad['email']=Input::get('celular');
        if (Input::has('tipoft')) {
            if (Input::get('tipoft')=='tap') { // aqui me quede
                if (isset($ft['0']))  $actividad['XA_HFC_NODE']=$ft['0'];
                else $actividad['XA_HFC_NODE']=0;
                if (isset($ft['1']))  $actividad['XA_HFC_TROBA']=$ft['1'];
                else $actividad['XA_HFC_TROBA']=0;
                if (isset($ft['2']))  $actividad['XA_HFC_AMPLIFIER']=$ft['2'];
                else $actividad['XA_HFC_AMPLIFIER']=0;
                if (isset($ft['3']))  $actividad['XA_HFC_TAP']=$ft['3'];
                else $actividad['XA_HFC_TAP']=0;
                if (isset($ft['4']))  $actividad['XA_HFC_BORNE']=$ft['4'];
                else $actividad['XA_HFC_BORNE']=0;
            } elseif (Input::get('tipoft')=='td') { //t directo
                if (isset($ft['0']))  $actividad['XA_MDF']=$ft['0'];
                else $actividad['XA_MDF']=0;
                if (isset($ft['1']))  $actividad['XA_CABLE']=$ft['1'];
                else $actividad['XA_CABLE']=0;
                if (isset($ft['2']))  $actividad['XA_BOX']=$ft['2'];
                else $actividad['XA_BOX']=0;
                if (isset($ft['3']))  $actividad['XA_BORNE']=$ft['3'];
                else $actividad['XA_BORNE']=0;
            } else {
                if (isset($ft['0']))  $actividad['XA_MDF']=$ft['0'];
                else $actividad['XA_MDF']=0;
                if (isset($ft['1']))  $actividad['XA_CABINET']=$ft['1'];
                else $actividad['XA_CABINET']=0;
                if (isset($ft['2']))  $actividad['XA_BOX']=$ft['2'];
                else $actividad['XA_BOX']=0;
                if (isset($ft['3']))  $actividad['XA_BORNE']=$ft['3'];
                else $actividad['XA_BORNE']=0;
            }
        }

        $activity = new Activity();
        $response = $activity->updateActivity($activityId, $actividad);
        if (isset($response->error) && $response->error===false
            && isset($response->data['status']) ) {

            $actividad['codactu']=$codactu; //keys que no se envia a TOA
            $actividad['fftt']=$fftt;
            Input::replace($actividad);
            $save = $this->_gestionController->postActualizarfftt();

            $actualizarxy=array();
            $actualizarxy['actu']=$codactu;
            $actualizarxy['lat']=$actividad['coordy'];
            $actualizarxy['lng']=$actividad['coordx'];
            Input::replace($actualizarxy);
            $savexy = json_decode($this->_gestionController->postActualizaxy());

            if($savexy->rst==1) {
               $return["rst"] = 1;
               $return["msj"] = "Actualización correcta.";
               $return['result_code'] = 0; 
            } else { 
                $return["rst"] = $savexy->rst; 
                $return["msj"] = $savexy->msj;
            }

            if (Input::has('fftt')) {
                $return["fftt"] = Input::get('fftt');
            }
        } elseif (isset($response->error) && isset($response->errorMsg) ) {
            $return["rst"] = 2;
            $return['error_msg']=$response->errorMsg;
        } else {
            $return["rst"] = 2;

        }
        //var_dump(Response::json($return));
        return Response::json($return);
    }

    public function postCancelarofsc()
    {
       
        $codactu = Input::get('codactu');

        $actuarray = Gestion::getCargar($codactu);
        $actuacion=$actuarray["datos"][0];

        $activityId = $actuacion->aid;

        $actividad=array();
        if(Input::get('nota')) {
           $usuario=Usuario::find(Auth::user()->id);
           $actividad['XA_NOTE'] = Input::get('nota').' '.$usuario['attributes']["usuario"];
        }
        $save=array();
        $save["aid"]=$activityId;

        $inbound = new Inbound();
        $response = $inbound->cancelActivity($activityId, $codactu, $actividad);
        if (isset($response->data->data->commands->command->appointment->report->message)) {
            $resultCode = $response->data->data->commands->command->appointment->report->message;
            if (isset($resultCode->result) && $resultCode->result=='error') {
                $save["rst"] = 2;
                $save['error_msg']=$resultCode->description;
            } else {
                for ($y = 0; $y < count($resultCode); $y++) {
                    if ($resultCode[$y]->type=='cancel' && $resultCode[$y]->result=='success' ) {
                        $save["gestion_id"] = $actuacion->id;
                        $save["rst"] = 1;
                        $save["msj"] = "Envio de Cancelacion a OFSC correcto.";
                        $save['result_code']=$resultCode[$y]->code;
                    }
                }
            }
        } 
        return json_encode($save);
    }

    public function postIniciarofsc()
    {
        $activityId = Input::get('aid');
        $codactu = Input::get('codactu');
        $save["aid"]=$activityId;

        $activity = new Activity();
        $response = $activity->startActivity($activityId);
        return json_encode($response);
    }

    public function postCompletarofsc()
    {
        $activityId = Input::get('aid');
        $codactu = Input::get('codactu');
        $save["aid"]=$activityId;

        $activity = new Activity();
        $response = $activity->completeActivity($activityId);

        return json_encode($response);
    }

    public function postEnvioofsc()
    {
        //cuando es sla el timeslot =0 , horas=0-0
        //ya que es una actividad que se puede realizar en cualquier momento
        $datos=array();
        $datos['hf']            =explode("||", Input::get('hf'));
        $datos['fecha']         =$datos['hf'][0];
        $datos['bucket']        =$datos['hf'][1];
        $datos['slot']          =$datos['hf'][2]; //time slot
        $datos['horas']         =explode("-", $datos['hf'][3]);
        $datos['empresa_id']    =Input::get('empresa_id');
        $datos['codactu']       =Input::get('codactu');
        $obsToa                 =Input::get('observacion_toa', '');
        $fonoUno             =Input::get('fono1', '');
        $fonoDos             =Input::get('fono2', '');
        $fonoTres            =Input::get('fono3', '');
        $fonoCuatro          =Input::get('fono4', '');
        $fonoCinco           =Input::get('fono5', '');

        $sla = false;
        $slaInicio = "0000-00-00 00:00";

        //retorno de transaccion
        $save = array(
            "rst" => 2,
            "msj" => "Sin reultados.",
            "error" => "",
            "gestion_id" => 0
        );

        $inbound = new Inbound();

        $gestionId = "";
        $codactu = $datos['codactu']; //"35268199"; //
        $date = $datos['fecha']; //date("Y-m-d"); //
        $bucket = $datos['bucket']; //"BK_PRUEBAS_TOA";
        $tini = trim($datos['horas'][0]); //"09:00"
        $tend = trim($datos['horas'][1]); //"13:00"
        $tobooking = date("Y-m-d H:i");
        $slot = $datos['slot']; //"AM"
        $cuadrante = "Something";
        $techTp = "NULL";
        $techBb = "NULL";
        $techTv = "NULL";
        $wzKey = "";
        $wtLabel = "";
        $wType = "";
        $accTech = "COAXIAL";
        $businessType = "";

        $phone = array(0=>"", 1=>"",2=>"",3=>"",4=>"");

        //Datos de la actuacion
        $actu = Gestion::getCargar($codactu);

        //Existe la actuacion
        if (isset($actu["datos"][0])) {
            $estadoComponente = 0;
            $actuObj = $actu["datos"][0];
            $actuArray = (array) $actuObj;

            $actuArray['noajax'] = "ok";

            //Motivo/Submotivo
            $actuArray['gestion_id'] = $actuObj->id;
            $actuArray['motivo'] = 1;
            $actuArray['submotivo'] = 1;
            $actuArray['estado'] = 2;

            //Fecha agenda
            $actuArray['fecha_agenda']=$date;
            //dd($actuArray);
            //GestionID
            $gestionId = $actuObj->id;

            //Tipo de actividad: tabla "actividaes_tipos"
            $actTipo = DB::table('actividades_tipos')
                    ->select('nombre', 'label', 'sla', 'duracion')
                    ->where('id', '=', $actuArray['actividad_tipo_id'])
                    ->first();

            //Duracion en minutos
            $duration = $actTipo->duracion;

            //Quiebre grupo
            $quiebreGrupo = DB::table('quiebre_grupos')
                    ->select('nombre')
                    ->where('id', '=', $actuArray['quiebre_grupo_id'])
                    ->first();

            //Negocio por texto
            $preDiagnosis='';
            if (strpos($actuArray['tipo_averia'], "catv") > 0) {
                $businessType = "CATV";
                $preDiagnosis='TV_';
            }

            if (strpos($actuArray['tipo_averia'], "adsl") > 0) {
                $businessType = "ADSL";
                $preDiagnosis='SP_';
            }

            if (strpos($actuArray['tipo_averia'], "bas") > 0) {
                $businessType = "BASICA";
                $preDiagnosis='SB_';
            }

            $wtLabel = $actTipo->label;
            $wType = $actTipo->nombre;

            $telefono=$actuArray['telefono_cliente_critico']; //telefono
            $cell=$actuArray['celular_cliente_critico']; //fono1

            $phonestring = str_replace(" ", "", $actuArray['fonos_contacto']);
            $phone = explode("|", $phonestring);
            $countphone= count($phone);

            if ($fonoUno!='' || $countphone<1)
                $phone[0] = $fonoUno;
            if ($fonoDos!='' || $countphone<2)
                $phone[1] = $fonoDos;
            if ($fonoTres!='' || $countphone<3)
                $phone[2] = $fonoTres;
            if ($fonoCuatro!='' || $countphone<4)
                $phone[3] = $fonoCuatro;
            if ($fonoCinco!='' || $countphone<5)
                $phone[4] = $fonoCinco;

            //Datos XY Web
            $actuArray['x'] = $actuArray['coord_x'];
            $actuArray['y'] = $actuArray['coord_y'];

            //Facilidades técnicas
            $ffttController = new \FfttController();
            $val = new stdClass();
            $val->fftt = $actuArray['fftt'];
            $val->tipoactu = $actuArray['tipo_averia'];
            $ffttArray = $ffttController->getExplodefftt($val);

            if ($ffttArray["tipo"]=='catv') {
                $wzKey = $ffttArray["nodo"] . "_" . $ffttArray["troba"];
            } else {
                $wzKey = $ffttArray["mdf"] . "_" . $ffttArray["armario"];
            }

            //Parche: datos no concuerdan -> enumeration
            if (strtoupper($actuArray['segmento']) == "NO-VIP"
                OR trim($actuArray['segmento'])=='') {
                $actuArray['segmento'] = "N";
            } elseif ( strtoupper($actuArray['segmento'])=='VIP') {
                if ( $businessType == "CATV") {
                    $actuArray['segmento'] = "S";
                } elseif ( $businessType=="BASICA") {
                    $actuArray['segmento'] = "D";
                }
            }

            //Componentes
            $dataComp = array();
            $equipment = "";
            $compGestion = new ComponenteGestion();
            if (trim($gestionId) != "") {
                $dataComp = $compGestion->getComponenteId($gestionId);
            } else {
                $dataComp = $compGestion->getComponenteFaltanteGrupal($codactu);
            }
            if (isset($dataComp["data"]) and count($dataComp["data"]) > 0) {
               $equipment = $compGestion->getArrayformat($dataComp["data"]);
               $estadoComponente = 1;
            }
            $agendasWebUnificada = InboundHelper::getElementXaWebUnificada(
                $actuArray['wu_nagendas'], $actuArray['wu_nmovimientos'], 
                $actuArray['wu_fecha_ult_agenda']
            );

            $nenvio=1;
            if (Input::get('nenvio')) {
                    $nenvio =Input::get('nenvio');
            }

            if ($obsToa != "") {
                $movimiento = new MovimientoObservacion;
                $observaciones = $movimiento->getObservaciones();

                if ($observaciones[0]->observacion != '') {
                    $obsToa = $obsToa." | ".$observaciones[0]->observacion;
                }
                
                // $actuArray['observacion'] = $actuArray['observacion']." | ".$obsToa;
            }
            /* 
            * Condicional que compara si orden es enviada por agenda o por SLA 
            * Agenda = CLI, SLA = TEL
            */
            
            $xaAppointmentScheduler = "CLI";
            if (Input::get('agdsla')=='sla' ){
                 $xaAppointmentScheduler = "TEL";
            }

             $dataOfsc1= InboundHelper::getCliente(
                 $datos['bucket'], strtolower($actuObj->actividad), $date, "update_activity",
                 $bucket, $tini, $tend, $actuArray['codactu'], $actuArray['inscripcion'], $wtLabel, $slot, $tobooking, $duration,
                 $actuArray['nombre_cliente'], $telefono, "", $cell, $actuArray['direccion_instalacion'], "", "", "", "1", 
                 (int) $duration, "19", $actuArray['coord_x'], $actuArray['coord_y']
             );
            $cuadrante=GeoValidacion::getquadrant($actuArray['coord_x'], $actuArray['coord_y']);

            $dataOfsc2= InboundHelper::getActividad(
                $actuArray['fecha_registro'], "PSI", $actuArray['segmento'], "",
                $actuArray['nombre_cliente'], $phone[0], $phone[1], $phone[2], $phone[3],
                "", $actuArray['codigo_distrito'], $actuArray['distrito'], $actuArray['zonal'],
                $cuadrante, "", $wzKey, "", "", $wType, $xaAppointmentScheduler, Auth::user()->id, $actuArray['codactu'], $actuArray['orden_trabajo'],
                "", "", "", "", "", $agendasWebUnificada, $actuArray['area'], $actuArray['paquete'], $actuArray['empresa'],
                $quiebreGrupo->nombre, $actuArray['quiebre'], $businessType, "", "", $equipment, $obsToa,
                $techTp, $techBb, $techTv, $accTech, "0", "", "", $actuArray['clase_servicio_catv'], "", "", "", "", $actuArray['dir_terminal'],
                "", $actuArray['tipo_actuacion'], "NULL", $actuArray['veloc_adsl'], $actuArray['tipo_servicio'], "", $actuArray['sms1'],
                "", $actuArray['total_averias'], $nenvio
            );

            $dataOfsc = array_merge($dataOfsc1, $dataOfsc2);
            $dataOfsc = InboundHelper::getUbigeoXML($dataOfsc, $actuArray);
            $dataOfsc['XA_TOTAL_REPAIRS'] = InboundHelper::getElementXaTotalRepairs(
                $actuArray['total_averias_cable'], $actuArray['total_averias_cobre'], 
                $actuArray['total_averias']
            );
            // Facilidades Tecnicas
            $dataOfsc = InboundHelper::getFFTTXML($ffttArray, $actuArray, $dataOfsc, $codactu);
            //Envio por Agendamiento
            $horario= Horario::where('horario', '=', $slot)->first();
            
            if ( $horario!=NULL AND $horario!='' ) {
                $actuArray['horario_id']= $horario->id;
                $actuArray['dia_id']= date("N", strtotime($date));
            }
            $actuArray['estado_agendamiento']="1-1";
            //Envio por SLA
            // La fecha del modal viene "0", aqui lo redeclara con fecha de hoy
            // "fecha_agenda" se redeclara nuevamente
            if (Input::get('agdsla')=='sla' /*AND Input::get('slaini')!=''*/) {
                $sla = true;
                //$slaInicio = strtotime(Input::get('slaini'));
                //$slaDay = $actTipo->sla * 3600;
                //$slaFin = $slaInicio + $slaDay;

                $dataOfsc["sla_window_start"] = date("Y-m-d H:i");
                $datos['slaini'] = date('Y-m-d H:i', strtotime($date));
                $slaInicio = strtotime($datos['slaini']);
                $slaDay = $actTipo->sla * 3600;
                $slaFin = $slaInicio + $slaDay;
                $dataOfsc["sla_window_end"] = date("Y-m-d H:i", $slaFin);
                //
                // viene de models/EstadoRespuesta.php
                $dataOfsc["date"] = date("Y-m-d", $slaInicio);
                if (Input::get('tipoprograma') AND
                    Input::get('tipoprograma')=='noprogramado') {
                    $dataOfsc["date"] ='';
                }
                //Si SLA la fecha_agenda es el limite
                $actuArray['fecha_agenda'] = date("Y-m-d", $slaFin);
                $actuArray['horario_id'] = 49;//Horario libre
                $actuArray['dia_id'] = date(
                    "N", strtotime($actuArray['fecha_agenda'])
                );
            }
            //echo "date === ".$actuArray["date"];
            //Campos adicionales para averia
             $dataOfsc["XA_DIAGNOSIS"]  = InboundHelper::getXaDiagnosisXML($actuArray, $actuObj, $preDiagnosis);
            //tipo req motivo req
            $dataOfsc = InboundHelper::gettXaRequirimentTypeReason($actuArray, $dataOfsc);

            /**
             * Guardar en tablas de gestion
             * - gestiones
             * - gestiones_detalles
             * - gestiones_movimientos
             * - ultimos_movimientos
             */
            unset($actuArray["tecnico"]);
            unset($actuArray["tecnico_id"]);
            unset($actuArray["celula_id"]);

            if (Input::get('tenicoreagenda')) {
                $actuArray['tecnico']=Input::get('tenicoreagenda');
                $actuArray['celula']=Input::get('celulareagenda');
            }

            //actualizando telefono de contacto
            //$actuArray['fonos_contacto']=implode("|", $phone);

            //Crear actividad en OFSC
            $response = $inbound->createActivity($dataOfsc, $sla);
            $report = $response->data
                ->data
                ->commands
                ->command
                ->appointment
                ->report;

                $resultBool = true;

                /**
                 * $report->message:
                 *
                 * El mensaje de respuesta puede ser un arreglo
                 * o un único mensaje.
                 * Se valida la respuesta para cada mensaje recibido.
                 */
                $errores = [];
                if (is_array($report->message)) {
                    foreach ($report->message as $val) {
                        if ($val->result == 'warning') {
                            $save["error"][] = $val->description;
                            $errores[$val->code] = $val->description;
                        }
                        if ($val->result == 'error') {
                            $save["error"][] = $val->description;
                            $errores[$val->code] = $val->description;
                        }
                    }
                } else {
                    if ($report->message->result == 'error') {
                        $resultBool = false;
                        $save["error"][] = $report->message->description;
                        $errores[$report->message->code] = $report->message->description;
                    }
                }

                //Retorno OK
               if ($resultBool) {
                    //Appointment id
                    $aid = $response->data
                        ->data
                        ->commands
                        ->command
                        ->appointment
                        ->aid;

                    unset($actuArray["fecha_agenda"]);
                    unset($actuArray["horario_id"]);
                    unset($actuArray["dia_id"]);
                    $actuArray['estado_agendamiento']="3-0";
                    $actuArray['motivo'] = 2;
                    $actuArray['submotivo'] = 18;
                    $actuArray['estado'] = 7;
                    $actuArray['aid'] = $aid;
                    $actuArray['estado_ofsc_id'] = 1;

                    $actuArray['envio_ofsc']=2;
                    $actuArray['programado']=1;
                    $actuArray['componente']=$estadoComponente;
                    if (Input::has('desprogramar') AND
                       Input::get('desprogramar')=='ok') {
                        $actuArray['programado']=0;
                    }
                    if ($sla==false) {
                        $actuArray['envio_ofsc']=1;
                    }

                    Input::replace($actuArray);
                    $save = $this->_gestionMovimientoController->postRegistrar();
                    $save["msj"] = "Registro y envío a OFSC correcto.";
                    $save["aid"]=$aid;
                    $save["errorCreateActivity"]= $errores;
               }

                //Retorno ERROR
                if (!$resultBool) {
                    $errArray['type'] = $report->message->type;
                    $errArray['code'] = $report->message->code;
                    $errArray['desc'] = $report->message->description;

                    //Input::replace($actuArray);
                    //$save = $this->_gestionMovimientoController->postRegistrar();

                    $save["rst"] = 2;
                    $save["msj"] = "No se pudo enviar a OFSC. "
                                    . $report->message->description;
                    $save["errorCreateActivity"]= $errores;
                }

        }

        return json_encode($save);
    }
    /**
     * Metodo que recibe cinco parametros y segun ellos invoca al Helper
     * de capacity
     * POST bandeja/capacity
     * 
     * @param tipo              String: indica si es 'agenda' o 'sla'
     * @param fecha             Date: indica la fecha a consultar la capacidad
     * @param timeslot          string: indica la franja horaria
     * @param mdf               string: mdf
     * @param actividad_tipo_id int: mdf
     * @param periodo string: 7 dias iniciando en el param fecha (deprecated)
     * 
     * @return Response         
     */
    public function postCapacity()
    {
        $ayer=date("Y-m-d", strtotime(date('Y-m-d')." - 1 days"));
        $required='';
        $tipo = Input::get('tipo', '');
        if ($tipo=='agenda') {
            $required = 'Required|';
        }
        $reglas = array(
            'actividad_tipo_id' => 'Required|Integer|Max:10',
            'mdf'               => 'Required|AlphaNum|Between:2,5',
            'fecha'             => $required.'date_format:"Y-m-d"|after:'.$ayer,
            'timeSlot'          => $required.'AlphaNum|Max:15',
            'tipo'              => 'Alpha|Max:10',
        );
        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'Alpha'         => ':attribute Solo debe ser Texto',
            'AlphaNum'      => ':attribute Solo debe ser alfanumerico',
            'Integer'       => ':attribute Solo debe ser entero',
        );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                )
            );
        }
        $tipo = Input::get('tipo', '');
        $mdf  = Input::get('mdf');
        $actividadTipoId  = Input::get('actividad_tipo_id');
        $timeSlot = '';

        if ($tipo =='agenda') {
            $fecha = Input::get('fecha');
            $timeSlot  = Input::get('timeSlot');
        } else {
            $fecha= date('Y-m-d');
        }
        $actividadTipo= ActividadTipo::find($actividadTipoId);
        if ( isset($actividadTipo->label) && isset($actividadTipo->duracion) ) {
            //construir array para consultar la capacidad en OFSC
            $capacity = new Capacity();
            $data=[
                'fecha'          => [$fecha],
                'time_slot'      => $timeSlot,
                'work_skill'     => $actividadTipo->label,
                'mdf'            => $mdf,
            ];
            $response = $capacity->getCapacity($data);
            //validar si la consulta de la capacidad no tiene errores
            if (isset($response->error) && $response->error===false ) {
                return Response::json(
                    [
                    'rst'=>1,
                    'datos'=>$response,
                    'duracion'=>$actividadTipo->duracion
                    ]
                );
            }
            return Response::json(
                [
                'rst'=>2,
                'msj' => 'No se encontro bucket',
                ]
            );
        }
        return Response::json(
            [
            'rst' => 2,
            'msj' => 'No se encontro tipo de actividad',
            ]
        );
    }
    /**
     * Recepciona datos de Bandeja Controller
     *
     * @return type
     */
    public function postRecepccion()
    {
        $data=array();
        $valida=array();

        $dataOfficetrack=Input::all();
        $dataGestion=Input::all();
        $dataGestionPendiente=Input::all();

        $dataGestion['dia_id']=Input::get('dia_id', '');
        $dataGestion['fecha_agenda']=Input::get('fecha_agenda', '');

        if ( trim($dataGestion["fecha_agenda"]) !='' and
            trim($dataGestion['dia_id']) !='' and
            $dataGestion['dia_id']!=
            date("N", strtotime(date($dataGestion["fecha_agenda"]))) ) {
            $exc['code']='0000';
            $exc['file']='BandejaController.php';
            $exc['line']='26';
            $dataGestion['codactu']=Input::get('codactu', '');
            $dataGestion['estado_id']=Input::get('estado_id', '');
            $dataGestion['submotivo_id']=Input::get('submotivo_id', '');
            $dataGestion['motivo_id']=Input::get('motivo_id', '');
            $dataGestion['quiebre_id']=Input::get('quiebre_id', '');
            $dataGestion['actividad_id']=Input::get('actividad_id', '');
            $exc['message']=$dataGestion['codactu'].', No cuadran dias =>'.
                            $dataGestion['dia_id'].' == '.
                        date("N", strtotime(date($dataGestion["fecha_agenda"])));

            $exc['trace']=$dataGestion['dia_id']."|".
                $dataGestion['fecha_agenda']."|".$dataGestion['estado_id']."|".
                $dataGestion['submotivo_id']."|".$dataGestion['motivo_id']."|".
                $dataGestion['quiebre_id']."|".$dataGestion['actividad_id'];

            $this->_errorController->saveError($exc);
            return Response::json(
                array(
                    'rst'=>2,
                    'msj'=>'Ocurrio una interrupción en el registro de la información.',
                    'codactu'=>$dataGestion['codactu']
                )
            );
        }

        /**
         * Validacion Técnico en un solo horario y fecha
         *
         * Datos requeridos:
         * tecnico:12
         * horario_id:19
         * dia_id:5
         * fecha_agenda:2015-05-08
         * estado_agendamiento: 1-1
         */
        $tecAsignadoBool = false;
        $resultAsignado  = array();
        if ($dataGestion["estado_agendamiento"]=='1-1') {
            if ( trim($dataGestion["tecnico"])==='' ||
                 trim($dataGestion["horario_id"])==='' ||
                 trim($dataGestion["dia_id"])==='' ||
                 trim($dataGestion["fecha_agenda"])===''
             ) {
                return array(
                            'rst'=>2,
                            'msj'=>'Se requiere fecha de agenda'
                        );;
            }
            //No validar para tecnico NINGUNO
            $tecArray = DB::table('tecnicos')
                        ->where('id', '=', $dataGestion["tecnico"])
                        ->where('estado', '=', '1')
                        ->select(
                            'ninguno'
                        )
                        ->get();
            if (isset($tecArray[0]) and $tecArray[0]->ninguno==0) {
                $dataValidaCupo = new stdClass();
                $dataValidaCupo->tecnico_id     = $dataGestion["tecnico"];
                $dataValidaCupo->horario_id     = $dataGestion["horario_id"];
                $dataValidaCupo->dia_id         = $dataGestion["dia_id"];
                $dataValidaCupo->fecha_agenda   = $dataGestion["fecha_agenda"];

                $asignado=GestionMovimiento::getTecnicoHorario($dataValidaCupo);

                if (is_array($asignado) and count($asignado["asignado"])>0) {
                    $ordenAgenda = $asignado["asignado"][0];
                    $tecAsignadoBool = true;
                    $resultAsignado = array(
                            'rst'=>2,
                            'msj'=>'El técnico seleccionado ya tiene una orden '
                                   . 'agendada para el horario asignado',
                            'codactu'=>$ordenAgenda->codactu
                        );
                }

                //Respuesta tecnico con agenda asignada
                if ($tecAsignadoBool) {
                    return $resultAsignado;
                }
            }
        }

        /**
         * Para estados: Cancelado y Pendiente, evaluar si
         * la orden está asignada a un técnico y dejarla
         * sin efecto. 2015-06-25
         *
         * Obtener ultimo movimiento antes de grabar la gestion
         */
        $getOtoff = "";
        if ( $dataGestion["estado"]==5 or $dataGestion["estado"]==7) {
            if ( isset($dataGestion["gestion_id"])
                and $dataGestion["gestion_id"]>0 ) {
                $ultimov = DB::table('ultimos_movimientos')
                        ->where('gestion_id', $dataGestion["gestion_id"])
                        ->first();
                $ultimov = Helpers::stdToArray($ultimov);

                /**
                 * Valida si la orden tiene:
                 * - horario
                 * - dia
                 * - celula
                 * - tecnico
                 */
                if ($ultimov["horario_id"] > 0
                    and $ultimov["dia_id"] > 0
                    and $ultimov["celula_id"] > 0
                    and $ultimov["tecnico_id"] > 0) {

                    $ultimov["estado"]              = "";
                    $ultimov["actividad"]           = "";
                    $ultimov["duration"]            = 1;
                    $ultimov["quiebre"]             = "";
                    $ultimov["eecc_final"]          = "";
                    $ultimov["cr_observacion"]      = "";
                    $ultimov["carnet"]              = "";
                    $ultimov["velocidad"]           = "";
                    $ultimov["paquete"]             = "";
                    $ultimov['fecha_agenda']        = "";
                    $ultimov['hora_agenda']         = "";
                    $ultimov["estado_agendamiento"] = "1-1";
                    $ultimov["coordinado2"]         = "0";

                    //Envio a OT
                    $savedata["otdata"] = $ultimov;
                    $rot = Helpers::ruta(
                        'officetrack/enviartarea',
                        'POST',
                        $savedata,
                        false
                    );

                    //Respuesta OT
                    //$getOtoff = $rot->officetrack;
                }

            }
        }

        $getOtoff = "";
        DB::beginTransaction();

        $rgmDos['sql']='';
        $rgmDos['estofic']='';
        if ( isset($dataGestion["gestion_id"]) and $dataGestion["gestion_id"]>0
            AND $dataGestion["estado_officetrack"]==0
        ) {
            $ultimov = DB::table('ultimos_movimientos')
                        ->where('gestion_id', $dataGestion["gestion_id"])
                        ->first();
            $ultimov = Helpers::stdToArray($ultimov);

            $sql="  SELECT ct.officetrack
                    FROM tecnicos t
                    INNER JOIN celula_tecnico ct ON t.id=ct.tecnico_id
                    WHERE ct.tecnico_id='".$ultimov["tecnico_id"]."'
                    AND ct.celula_id='".$ultimov["celula_id"]."'
                    AND ct.estado=1";

            $tecnicoinfo = DB::select($sql);
            $tecnicoinfo = Helpers::stdToArray($tecnicoinfo);
            if (count($tecnicoinfo)>0) {
                $rgmDos['estofic']=$tecnicoinfo[0]['officetrack'];
            }
            $rgmDos['sql']=$sql;
            /**
             * Valida si la orden tiene:
             * - horario
             * - dia
             * - celula
             * - tecnico
             */
            if ($ultimov["horario_id"] > 0
                and $ultimov["dia_id"] > 0
                and $ultimov["celula_id"] > 0
                and $ultimov["tecnico_id"] > 0
                and $rgmDos['estofic']==1) {

                $ultimov["estado"]              = "";
                $ultimov["actividad"]           = "";
                $ultimov["duration"]            = 1;
                $ultimov["quiebre"]             = "";
                $ultimov["eecc_final"]          = "";
                $ultimov["cr_observacion"]      = "";
                $ultimov["carnet"]              = "";
                $ultimov["velocidad"]           = "";
                $ultimov["paquete"]             = "";
                $ultimov['fecha_agenda']        = "";
                $ultimov['hora_agenda']         = "";
                $ultimov["estado_agendamiento"] = "0-0";
                $ultimov["coordinado2"]         = "0";

                //Envio a OT
                $savedata["otdata"] = $ultimov;
                $rot = Helpers::ruta(
                    'officetrack/enviartarea',
                    'POST',
                    $savedata,
                    false
                );

                //Registrar Pendiente
                $dataGestionPendiente['estado_agendamiento']='0-0';
                $dataGestionPendiente['motivo']='2';
                $dataGestionPendiente['submotivo']='18';
                $dataGestionPendiente['estado']='7';
                $dataGestionPendiente['horario_id']='';
                $dataGestionPendiente['dia_id']='';
                $dataGestionPendiente['fecha_agenda']='7';
                $dataGestionPendiente['tecnico']='';
                //$dataGestionPendiente['usuario_sistema']='sistema';//697
                if (isset($dataGestion["fecha_registro_psi"]))
                    $dataGestionPendiente['fecha_psi'] = $dataGestion["fecha_registro_psi"];

                $rgm = Helpers::ruta(
                    'gestion_movimiento/crear',
                    'POST',
                    $dataGestionPendiente,
                    false
                );

                //Respuesta OT
                //$getOtoff = $rot->officetrack;
            }

        }
        if (isset($dataGestion["fecha_registro_psi"]))
            $dataGestion['fecha_psi'] = $dataGestion["fecha_registro_psi"];
        $rgm = Helpers::ruta(
            'gestion_movimiento/crear',
            'POST',
            $dataGestion,
            false
        );
        $rgm= Helpers::stdToArray($rgm);
        //$rgm["sql"]=$rgmDos["sql"];
        $rgm['estofic']=$rgmDos['estofic'];

        //Registra o actualiza XY del cliente
        if (isset($dataGestion['inscripcion']) &&
            isset($dataGestion['nombre_cliente_critico'])&&
            isset($dataGestion['x']) &&
            isset($dataGestion['y']) &&
            isset($dataGestion['direccion_instalacion']) ) {
            # code...
            $dataXyCliente = array(
                'codigo' => $dataGestion['inscripcion'],
                'nombre' => $dataGestion['nombre_cliente_critico'],
                'coord_x' => $dataGestion['x'],
                'coord_y' => $dataGestion['y'],
                'direccion' => $dataGestion['direccion_instalacion'],
                'estado' => 1
            );

            if ($dataGestion["cliente_xy_insert"]==1) {
                $query = DB::table('clientes')->insert($dataXyCliente);
            } else {
                $query = DB::table('clientes')
                    ->where('codigo', $dataGestion['inscripcion'])
                    ->update($dataXyCliente);
            }
        }

        $rvalida="0";
        if ( Input::get('tecnico') and Input::get('tecnico')!='' ) {
            /*$dataGestion["estado_agendamiento"]!='2-0' and*/ 
            $valida=array();
            //Indica si cumple con el envio a officetrack acitividad + quiebre
            $valida["actividad_id"]=$dataOfficetrack["actividad_id"];
            $valida["quiebre_id"]=$dataOfficetrack["quiebre_id"];
            //El estado del tecnico de officetrack
            $valida["tecnico_id"]=$dataOfficetrack["tecnico"];
            $valida["celula_id"]=$dataOfficetrack["celula"];
            //El estado de Agendamiento para officetrack
            $valida["motivo_id"]=$dataOfficetrack["motivo"];
            $valida["submotivo_id"]=$dataOfficetrack["submotivo"];
            $valida["estado_id"]=$dataOfficetrack["estado"];
            //El evento indica si anteriormente ya se realizÃ³ una transacciÃ³n OT
            $valida["transmision"]=$dataOfficetrack["transmision"];

            $rvalida =  Helpers::ruta(
                'officetrack/validar',
                'POST',
                $valida,
                false
            );
        }

        // true indica que se enviara a officetrack
        $dataGestion['officetrack_envio']=$rvalida;
        if ( $rvalida=="1" and $rgm['rst']=="1" ) {
            $dataOfficetrack['gestion_id']=$rgm['gestion_id'];
            /*if ( !isset($dataOfficetrack['id_gestion']) ) {
                $idGestion=Gestion::getGenerarID();
                $dataOfficetrack['gestion_id']=$idGestion;
                $dataGestion['gestion_id_officetrack']=$idGestion;
            }*/
            $tecnico = Tecnico::find($dataOfficetrack['tecnico']);
            $dataOfficetrack['carnet']=$tecnico['carnet_tmp'];

            $estado = Estado::find($dataOfficetrack['estado']);
            $dataOfficetrack['estado']=$estado['nombre'];

            $horarioTipo = HorarioTipo::find($dataOfficetrack['horario_tipo']);
            $dataOfficetrack['duration']=$horarioTipo['minutos'];


            $velocidad=array('','','');
            if (trim($dataOfficetrack['paquete'])!='') {
                $velocidad=explode("|", $dataOfficetrack['paquete']);
            }

            $dataOfficetrack['velocidad']=$velocidad[2];
            $dataOfficetrack['eecc_final']=$dataOfficetrack['empresa_id'];
            $dataOfficetrack['cr_observacion']=$dataOfficetrack['observacion2'];

            $savedata["otdata"] = $dataOfficetrack;
            $rot = Helpers::ruta(
                'officetrack/enviartarea',
                'POST',
                $savedata,
                false
            );

            $rot= Helpers::stdToArray($rot);
            if ( $rot['officetrack']=="OK" ) { //registrara normalmente
                DB::commit();
                $rgm['msj']='Registro realizado correctamente con Officetrack';
                $rgm['estado_agendamiento']=$dataGestion['estado_agendamiento'];
                $rgm['tecnico']=$dataGestion['tecnico'];

                if ($dataGestion["estado_agendamiento"]!='1-1'
                    AND $dataGestion["tecnico"]!='') {
                    //$url="http://psiweb.ddns.net:2230/webpsi/sms_enviar_individual_ajax.php";
                    $url = Config::get("wpsi.sms.url");
                    $tecnicoinfo = DB::table('tecnicos')
                                    ->where('id', $dataGestion["tecnico"])
                                    ->first();
                    $postData=array(
                        'enviar_sms' =>1,
                        'celular'    =>substr($tecnicoinfo->celular, -9),
                        //'celular' =>'996475583', // poner celular para probar
                        'iduser'     =>Auth::user()->id,
                        'mensaje'    =>"La actuación: ".$dataGestion['codactu'].
    " ha sido eliminada, favor de sincronizar para actualizar. PSI-OFFICETRACK"
                    );
                    $rgm['mensaje']="Llego";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    //Retorno
                    $result = curl_exec($ch);
                    curl_close($ch);
                } else {
                    $rgm['mensaje']="No Llego :(";
                }
                return Response::json(
                    $rgm
                );
            } else {
                DB::rollback();
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>'No se pudo realizar el envio : '.
                                $rot['officetrack'].
                                '; Intente nuevamente el envio a officetrack',
                        'codactu'=>$dataGestion['codactu']
                    )
                );
            }
        } elseif ( $rgm['rst']=="1" ) {
            DB::commit();
            $rgm['msj']='Registro realizado correctamente';
            return Response::json(
                $rgm
            );
        //ocurrio un error de try catch
        } elseif ( $rgm['rst']=="2" &&
            (is_array($rgm['err']) || is_object($rgm['err'])) ) {
            DB::rollback();
            $rgm['err']['message']=serialize($rgm);  
            $rgm['message']=serialize($rgm);
            $this->_errorController->saveError($rgm['err']);

            return Response::json(
                array(
                    'rst'=>2,
                    'msj'=>$rgm['msj']
                )
            );
        } else {
            DB::rollback();

            return Response::json(
                array(
                    'rst'=>2,
                    'msj'=>$rgm['msj']
                )
            );
        }
         /*else { // registrarÃ¡ directo sin officetrack
            $rgm = Helpers::ruta(
                'gestion_movimiento/crear',
                'POST',
                $dataGestion,
                false
            );

            $rgm= Helpers::stdToArray($rgm);
            $rgm['msj']='Registro realizado correctamente';
            return Response::json(
                $rgm
            );
        }*/


    }

    public function postExtraerxy()
    {
        $coord = array("coord"=>array("x"=>"", "y"=>""), "rst"=>0, "ins"=>0);
        $array = new stdClass();
        $array->tipoactu = Input::get('tipoactu');
        $array->fftt = Input::get('fftt');

        //Buscar xy en tabla clientes
        $xy = DB::table('clientes')
                ->where('codigo', '=', Input::get('cod_cliente'))
                ->where('estado', '=', 1)
                ->get();
        if (count($xy) > 0) {
            foreach ($xy as $key=>$val) {
                $coord["coord"]["x"] = $val->coord_x;
                $coord["coord"]["y"] = $val->coord_y;
            }
            $coord["rst"] = 1;
        } else {
            $data["data"][]=$array;
            $xy=$this->_visorgpsController->getActuCoord($data);

            if (count($xy) > 0) {
                foreach ($xy as $key=>$val) {
                    $coord["coord"]["x"] = $val->x;
                    $coord["coord"]["y"] = $val->y;
                }
                $coord["rst"] = 1;
                $coord["ins"] = 1;
            }
        }
        return json_encode($coord);
    }

    public function postActivity()
    {
        $codactuacion = Input::get('codactuacion');
        try {
            $sql = DB::table("ultimos_movimientos")
                    ->where("codactu", $codactuacion)
                    ->get();
            if (count($sql) > 0) {
                $activity =  new Activities();
                $response = $activity->getActivity($sql[0]->aid);
                $resultCode = $activity->getHttpResponseCode($response);
                if (empty($resultCode)) {
                    $htmlResponse = Helpers::HTMLDetalleOFSC($response);
                    $data = array(
                        'rst'=>1,
                        'datos'=>$response,
                        'html' => $htmlResponse,
                        'msj'=>"ok"
                    );  
                } else {
                    $data = array(
                        'rst'=>2,
                        'datos'=> $response,
                        'msj'=>"Error OFSC ".$resultCode
                    );
                }
                return Response::json($data);
            } else {
                return Response::json(
                    array(
                                'rst'=>2,
                                'datos'=> array(),
                                'msj'=>"No existe registro en Ultimos Movimientos"
                            )
                );
            }
        } catch (Exception $ex) {
            $this->_errorController->saveError($ex);
            return Response::json(
                array(
                        'rst'=>2,
                        'datos'=>array(),
                        'msj'=>"error"
                    )
            );
        }
    }

   /**
     * fecha, mdf, actividad_tipo_id, timeSlot, tipoEnvio, json
     * POST bandeja/capacity-multiple
     * 
     * @param tipo              String: indica si es 'agenda' o 'sla'
     * @param fecha             Date: indica la fecha a consultar la capacidad
     * @param timeslot          string: indica la franja horaria
     * @param mdf               string: mdf
     * @param actividad_tipo_id int: mdf
     * @param periodo string: 7 dias iniciando en el param fecha (deprecated)
     * 
     * @return Response         
     */
    public function postCapacityMultiple()
    {
        if (Input::has('fecha') && Input::get('fecha') != '' 
            && strlen(Input::get('fecha'))== 10) {
            $fechaInicial = Input::get('fecha');     
        } else {
            $fechaInicial = date('Y-m-d');
        }
        // list($mes, $dia, $anio) = explode('-', $fecha); 
        $msjAct = '';
        $rst = 2;
        $capacityDuration = '';
        $capacityObject = [];
        $slotObject = [];
        $capacityObjectToday = [];
        $capacityData = null;
        if (Input::has('mdf') && Input::has('actividad_tipo_id')) {
            $mdf  = Input::get('mdf');
            $actividadTipoId  = Input::get('actividad_tipo_id');
            $timeSlot = Input::get('timeSlot', '');
            $multiple = [
                $fechaInicial,
                date("Y-m-d", strtotime($fechaInicial . " + 1 days")),
                date("Y-m-d", strtotime($fechaInicial . " + 2 days")),
                date("Y-m-d", strtotime($fechaInicial . " + 3 days")),
                date("Y-m-d", strtotime($fechaInicial . " + 4 days")),
                date("Y-m-d", strtotime($fechaInicial . " + 5 days")),
                date("Y-m-d", strtotime($fechaInicial . " + 6 days")),
            ];
            $actividadTipo= ActividadTipo::find($actividadTipoId);
            
            if (isset($actividadTipo->label) 
                && isset($actividadTipo->duracion)) {
                //construir array para consultar la capacidad en OFSC
                $capacity = new Ofsc\Capacity();
                $data=[
                    'fecha'          => $multiple,
                    'time_slot'      => $timeSlot,
                    'work_skill'     => $actividadTipo->label,
                    'mdf'            => $mdf,
                ];
                $response = $capacity->getCapacity($data);
                //validar si la consulta de la capacidad no tiene errores
                if (isset($response->error) && $response->error===false 
                    && isset($response->data->capacity)) {
                    $capacityData = $response->data;
                    $capacityDuration = $actividadTipo->duracion; 
                    foreach ($capacityData->capacity as $key => $val) {
                        if (isset($val->time_slot)) {
                            $capacityObject["{$val->date}|{$val->time_slot}"] = 
                                $val;
                        }
                        if (isset($val->time_slot) 
                            && $val->date == $fechaInicial) {
                            $capacityObjectToday[] = $val;
                        }
                    }
                    if (isset($capacityData->time_slot_info) 
                        && is_array($capacityData->time_slot_info)) {
                        foreach ($capacityData->time_slot_info 
                            as $key => $val) {
                            $slotObject["{$val->name}|{$val->label}"] = $val;
                        }   
                    } elseif (isset($capacityData->time_slot_info) 
                        && is_object($capacityData->time_slot_info)) {
                        $val = $capacityData->time_slot_info;
                        $slotObject["{$val->name}|{$val->label}"] = $val;   
                    }
                    $rst = 1;
                } else {
                    $msjAct = '(C) No hay capacidad en TOA. ';
                }
            } elseif (!isset($actividadTipo->label)) {
                $msjAct .= 'Esta actividad no esta '
                    . 'definida para enviar a TOA. ';
            } else { // 
                $msjAct .= '(C) No hay capacidad en TOA. ';
            }
        } 
        $a = [
            'capacity' => $capacityObject,
            'timeslot' => $slotObject,
            'raw' => $capacityData,
            'today' => [
                'capacity' => $capacityObjectToday, 
                'time_slot_info' => isset($capacityData->time_slot_info)? 
                $capacityData->time_slot_info : []
            ],
            'duracion' => $capacityDuration,
            'msj' => $msjAct,
            'rst' => $rst
        ];
        if(Input::has('json')) {
            $a = Response::json($a);
        }
        return $a;
    }

    /**
     * 
     * @return type
     */
    public function postSavelocalstorage()
    {
        if (Request::ajax()) {
            $localStorageData = json_decode(Input::get("localStorageBusqueda"), true);
            $dataResponse = array();
            foreach ($localStorageData as $key => $value) {
                if ($value['estado_ofsc'] === "" || $value['estado_ofsc'] === "Pendiente")
                    $value['seleccionado'] = true;
                else
                    $value['seleccionado'] = false;
                $dataResponse[$value['codactu']] = array("codactu" => $value["codactu"],
                                "estado_ofsc" => $value["estado_ofsc"],
                                "actividad_tipo_id" => $value["actividad_tipo_id"],
                                "mdf" => $value["mdf"],
                                "empresa" => $value["empresa"],
                                "seleccionado" => $value["seleccionado"]);
            }
           return Response::json(
               array(
                        'rst'=>1,
                        'datos'=>$dataResponse,
                    )
           );
        }
    }

    public function postDetalleactivity()
    {
        $aid = Input::get('aid');
        if(!empty($aid)) {

            $ultimo = DB::table('ultimos_movimientos as um')
                ->leftJoin('gestiones_movimientos as gm', 'um.gestion_id', '=', 'gm.gestion_id')
                ->leftJoin('quiebres as q', 'um.quiebre_id', '=', 'q.id')
                ->leftJoin('estados as e', 'um.estado_id', '=', 'e.id')
                ->leftJoin('empresas as ea', 'um.empresa_id', '=', 'ea.id')
                ->leftJoin('motivos as m', 'um.motivo_id', '=', 'm.id')
                ->leftJoin('submotivos as sm', 'um.submotivo_id', '=', 'sm.id')
                ->leftJoin('estados_ofsc as eo', 'um.estado_ofsc_id', '=', 'eo.id')
                ->leftJoin('tecnicos as t', 'um.tecnico_id', '=', 't.id')
                ->select(
                    'um.codactu', 'q.nombre as quiebre_nombre', 'e.nombre as estado_nombre', 'ea.nombre as empresa_nombre',
                    'm.nombre as motivo_nombre', 'sm.nombre as submotivo_nombre', 'eo.nombre as estado_ofsc', 'um.direccion_instalacion',
                    'um.observacion', 'um.x', 'um.y', 'gm.x_ofsc', 'gm.y_ofsc', DB::raw('ifnull(x_inicio,NULL) as x_inicio'), DB::raw('ifnull(y_inicio,NULL) as y_inicio'), DB::raw('ifnull(x_fin,NULL) as x_fin'), DB::raw('ifnull(y_fin,NULL) as y_fin'), 't.nombre_tecnico', 't.celular', 't.carnet', 'um.nombre_cliente',
                    'um.fonos_contacto', 'um.fecha_agenda'
                )
                ->where('um.aid', $aid)
                ->first();

            $url = 'gestion_movimiento/cargar';
            $method = 'POST';

            $data = Helpers::ruta($url, $method, ['codactu' => $ultimo->codactu]);
            $data = json_decode($data);
            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $data->datos,
                    'ultimo' => $ultimo
                )
            );
        }
        return Response::json(
            array(
                'rst' => 0,
                'datos' => null,
                'ultimo' => null
            )
        );
    }
    
    public function postActivityOfsc()
    {
        $aImage = [
            'XA_DONE_PIC', 'XA_CABLE_PIC',   
            'XA_START_PIC_1', 'XA_START_PIC_2', 
            'XA_DONE_PIC_ADD_1', 'XA_DONE_PIC_ADD_2', 'XA_DONE_PIC_ADD_3', 
            'XA_DONE_PIC_ADD_4', 'XA_DONE_PIC_ADD_5', 'XA_DONE_PIC_ADD_6', 
            'XA_DONE_PIC_ADD_7', 'XA_DONE_PIC_ADD_8'
        ];
        $aImageResponse = [];
        $aid = Input::get('aid');
        $activity =  new Activity();
        $response = $activity->getActivity($aid);
        
        if (isset($response->error) && $response->error===false) {
            if (isset($response->data['status'])) {
                $estadoofsc = DB::table('estados_ofsc AS e')
                            ->where('e.name', '=', $response->data['status'])
                            ->first();
                $data['estadoofsc'] = is_null($estadoofsc)? 
                    ['id' => 0] : $estadoofsc;
            } 
            for ($a = 0; $a < count($aImage); $a++) {
                if (isset($response->data[$aImage[$a]])) {
                    $aImageResponse[] = $aImage[$a];
                }
            }
            $data['imagenofsc'] = $aImageResponse;
            return Response::json($data);
        }
        return Response::json(
            ['estadoofsc' => ['id' => 0], 'imagenofsc' => []]
        );
    }

    public function postUpdatetmpofsc()  //se actualiza desde tmp a TOA
    {
       $response =TmpProvision::getTmpUpdate();
       $actuacion=json_decode(json_encode($response), true);
       $actualizado=array();
       //var_dump($actuacion);
       foreach ($actuacion as $key => $value) {
           $actividad=array();
           $data=array();

           $ultimovimiento=UltimoMovimiento::find($value['umid']);
           $gestiondetalle=GestionDetalle::find($value['gdid']);
           $gestiones=Gestion::find($value['gestion_id']);

           if ($value['veloc_adsl']!=0) {
              $actividad['XA_ADSL_SPEED']=
                $gestiondetalle['veloc_adsl']=
                    $ultimovimiento['veloc_adsl']=$value['veloc_adsl'];
            }
            if ($value['fono1']!=0) { 
                $gestiones['celular_cliente_critico']=
                 $gestiondetalle['fono1']=
                   $ultimovimiento['celular_cliente_critico']=
                     $ultimovimiento['fono1']=$value['fono1'];
            } 
            if ($value['telefono']!=0) { 
                $data['phone']=
                  $gestiones['telefono_cliente_critico']=
                    $gestiondetalle['telefono']=
                     $ultimovimiento['telefono_cliente_critico']=
                      $ultimovimiento['telefono']=$value['telefono'];
            } 
            if ($value['orden']!=0) { 
                $actividad['XA_NUMBER_SERVICE_ORDER']=
                $gestiondetalle['orden_trabajo']=
                     $ultimovimiento['orden_trabajo']=$value['orden'];
            } 
           if ($value['fonos_contacto']!='0|0' && $value['fonos_contacto']!='' ) { //0|0 y |
              $telefonos=explode("|",$value['fonos_contacto']); 
              $phone=array('','');
           
              if ($telefonos[0]!=0) { 
              $actividad['XA_CONTACT_PHONE_NUMBER_2']=$telefonos[0];
              $phone[0]=$telefonos[0];
              }
              if ($telefonos[1]!=0) {
              $actividad['XA_CONTACT_PHONE_NUMBER_3']=$telefonos[1];
              $phone[1]=$telefonos[1];
              }
            
              $gestiondetalle['fonos_contacto']=
                        $ultimovimiento['fonos_contacto']=implode('|', $phone);
           }
           if ($value['fecha_Reg']!='') {
              $data['time_of_booking']=
                    $gestiondetalle['fecha_registro']=
                    $ultimovimiento['fecha_registro']=$value['fecha_Reg'];
            }

            if(count($actividad)>0 || count($data)>0){
                $inbound = new Inbound();
                $response = $inbound->updateActivity($value['codigo_req'], $actividad,$data);
                //var_dump($response);
                if (isset($response->result) && $response->result=="success") {
                    $return["rst"] = 1;
                    $return["msj"] = "Actualización correcta.";
                    $return['result_code'] = 0; 
                    $ultimovimiento['update_toa']=1;
                    $ultimovimiento['usuario_updated_at']=964; //PROGRAM. EdWARD
                    $gestiondetalle['usuario_updated_at']=964; //964 en prod.
                    $gestiones['usuario_updated_at']=964;
                    $ultimovimiento->save();
                    $gestiondetalle->save();
                    $gestiones->save();
                    array_push($actualizado, $value['codigo_req']); 
                } elseif (isset($response->result) && $response->result=="error" ) {
                    $return["rst"] = 2;
                    $return["msj"] = 'error';
                    $return['result_code'] = $response->code; 
                    $return['error_msg']=$response->description;
                } else {
                    $return["rst"] = 2;
                    $return["msj"] = 'error';
           
                }
           } 
           //var_dump($return);
       }
       return $actualizado;
    }

}
