<?php

use Ofsc\Resources;

class RecursoOfscController extends BaseController
{
    public function getRecursos()
    {
        $Resource = new Resources();

        $recursos = $Resource->getResources();

        dd($recursos);
    }

    public function getRegistrarrecursos()
    {
        $recursos = new Resources();
        $response = $recursos->getResources();
        if (isset($response->error) && $response->error === false) {
            $totalRegistro = $response->totalResults;
            $array = $response->data;
        } else {
            return Response::json($response);
        }

        $intervalo = 100;
        $arrayIteracion = explode('.', ($totalRegistro / $intervalo));
        $numIteracion = $arrayIteracion[0];
        if ($arrayIteracion[1] > 0) {
            $numIteracion++;
        }
        $contador = 0;
        $arrayParentTecnico = array();

        $sql = "INSERT INTO arbol_recursos(parent_id, recurso_id, nombre_recurso, vehiculo, estado) VALUES";

        for ($i = 1; $i <= $numIteracion; $i++) {
            $recursosTotal = $recursos->getResourceslimit($i * $intervalo, ($i * $intervalo) - $intervalo);
            $arrayTotal = $recursosTotal->data;
            foreach ($arrayTotal as $rec) {
                $rec = (array)$rec;
                if ($rec["name"]) {
                    $arrayResourceId[$contador] = $rec["resourceId"];
                    $arrayStatus[$contador] = $rec["status"];

                    if($arrayStatus[$contador] == 'active'){
                        $arrayStatus[$contador] = 1;
                    }else{
                        $arrayStatus[$contador] = 0;
                    }

                    $arrayParentResourceId[$contador] = "0";
                    if (isset($rec["parentResourceId"])) {
                        $arrayParentResourceId[$contador] = $rec["parentResourceId"];
                    }
                    $arrayResourceType[$contador] = "";
                    if (isset($rec["resourceType"])) {
                        $arrayResourceType[$contador] = $rec["resourceType"];
                    }
                    $arrayName[$contador] = "";
                    if (isset($rec["name"])) {
                        $arrayName[$contador] = $rec["name"];
                    }
                    $arrayVeh[$contador] = "";
                    if (isset($rec["XR_VEHICLE"])) {
                        $arrayVeh[$contador] = $rec["XR_VEHICLE"];
                    }
                    $sql .= "('" . $arrayParentResourceId[$contador] . "', '" . $arrayResourceId[$contador] . "', '" . $arrayName[$contador] . "', '" . $arrayVeh[$contador] . "', '" . $arrayStatus[$contador] . "'),";
                    $contador++;
                }
            }
        }
        $sql = substr_replace($sql, "", -1);

        DB::insert($sql);
        return "OK";
    }

    public function getTree()
    {
        $names = Redis::get('name');
        $names = str_replace('"', "", $names);
        $names = str_replace('[', "", $names);
        $names = str_replace(']', "", $names);
        $names = explode(',', $names);

        $parent = Redis::get('parentResourceId');
        $parent = str_replace('"', "", $parent);
        $parent = str_replace('[', "", $parent);
        $parent = str_replace(']', "", $parent);
        $parent = explode(',', $parent);

        $id = Redis::get('resourceId');
        $id = str_replace('"', "", $id);
        $id = str_replace('[', "", $id);
        $id = str_replace(']', "", $id);
        $id = explode(',', $id);

        $tree = array();

        foreach ($id as $i => $val) {
            if (empty($parent[$i])) {
                //dd($parent);
                $sons = $this->getSons($id[$i], $parent);
                $arraySons = array();
                foreach ($sons as $idSon) {
                    $arraySons[] = $id[$idSon];
                }

                $tree[] = array("id" => $id[$i], 'children' => $arraySons);
            }
        }
        dd($tree);
    }

    private function getSons($id, $array = array())
    {
        $result = array();
        foreach ($array as $i => $val) {
            if ($id == $val) {
                $result[] = $i;
            }
        }
        return $result;
    }

    public function getArbol()
    {
        $tree = new Arbol;
        $elementos = $tree->get();
        $padres = $elementos["padres"];
        $hijos = $elementos["hijos"];


        $arbol = array();
        $html = "";
        $html .= "<ul>";
        foreach ($padres as $padre) {
            $hijos = $tree->bucle($hijos, $padre->recurso_id);
            $array = array(
                "id" => $padre->recurso_id,
                "title" => $padre->nombre_recurso,
                "status" => $padre->estado,
                "nodes" => $hijos
            );
            $html .= "<li>";
            $html .= "<spam>" . $padre->nombre_recurso . "</spam>";
            $tree = new Arbol;
//                $html .= $tree->bucle($hijos, $padre->recurso_id);
            $html .= "</li>";
            $arbol[] = $array;
        }
        $html .= "</ul>";

        return $arbol;
    }

    public function getComborecursos()
    {
        $sql = "SELECT t4.recurso_id as id, t4.nombre_recurso as nombre
                FROM arbol_recursos AS t1
                LEFT JOIN arbol_recursos AS t2 ON t2.parent_id = t1.recurso_id
                LEFT JOIN arbol_recursos AS t3 ON t3.parent_id = t2.recurso_id
                LEFT JOIN arbol_recursos AS t4 ON t4.parent_id = t3.recurso_id
                WHERE t1.recurso_id = 'GR_LIMA'";

        $recursos = DB::select($sql);

        return $recursos;
    }
}