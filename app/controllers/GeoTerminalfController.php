<?php
class GeoTerminalfController extends \BaseController
{
    public function postListar()
    {
      
        $r =GeoTerminalf::postGeoTerminalfAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoTerminalf::postMdfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postMdf()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $r =GeoTerminalf::postArmarioFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postArmario()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $array['armario']=Input::get('armario');
        $r =GeoTerminalf::postTerminalfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }



}

