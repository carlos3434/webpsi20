<?php

class IncidenciaReqController extends \BaseController {


	public function postCreate(){
		if (Request::ajax()) {						
			$data = [];
			$data['tipo_valor']=Input::get('tipo_valor');
			$data['incidencia_id']=Input::get('incidencia_id');
			$data['valor']=Input::get('valor');		
			$data['descripcion']=Input::get('observacion');
			$data['estado']=Input::get('estado');	
			$rst = IncidenciaReq::create($data);

			return Response::json(
	            array(
	            	'incidencia_id' => Input::get('incidencia_id'),
	            	'rst' => ($rst) ? 1 : 0,
	              	'msj' => 'Registrado Correctamente',
	            )
            );
		}
	}

	public function postListar(){
			$objClass = new IncidenciaReq();
			return Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );	
	}

	public function postCambiarestado(){
		if (Request::ajax()) {
			$update = IncidenciaReq::findOrFail(Input::get('requerimiendo_id'));
			$update->estado = Input::get('estado_id');
			$update->usuario_updated_at = Auth::id();
			$update->updated_at = date('Y-m-d H:i:s');
			$update->save();
			return Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}

	/**
	 * Display a listing of the resource.
	 * GET /incidenciareq
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /incidenciareq/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /incidenciareq
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /incidenciareq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /incidenciareq/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /incidenciareq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /incidenciareq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}