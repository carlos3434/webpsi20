<?php

class TipoMasivaController extends BaseController
{
    public function postListar()
    {
        $masiva=TipoMasiva::select('id','nombre')
                ->where('estado',1)->get();
        return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$masiva
                )
            );
    }
}