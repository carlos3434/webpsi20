<?php

use Ofsc\Activity;

class ActividadesRoutingOfscController extends BaseController
{
    /**
     * mostrar componente especifico
     */
    public function show($resourceId)
    { 
        $from = date("Y-m-d");
        $filename = $from;
        $hoy = preg_replace("/-/", "", $filename);
        $file = public_path()."/json/actividades/".$resourceId."/".$hoy.".json";
        if (!File::exists($file)) {
            $json = '[]';   
        } else {
            $json = File::get($file);
        }
            
        $json = json_decode($json, true);
        $data = [];
        $objRouting = new RoutingOfsc;
        $rutas = [];
        $jsonTmp = [];
        $actividades = [];
        $coordMedia = ["longitud" => [], "latitud" => []];
        $colores = ['#00FF00','#00FFFF','#0000FF','#000080',
                    '#008000','#008080','#800000','#800080',
                    '#808000','#808080','#C0C0C0','#FF00FF',
                    '#FF0000','#FF8800','#FFFF00','#FFFFFF',
                    '#80FF00'];

        $abcisas = []; $ordenadas = [];
        foreach ($json as $key => $value)
        {
            $abcisas[$key] = $value["coordx"];
            $ordenadas[$key] = $value["coordy"];
            $actividades[]  = $value["appt_number"];    
        }
        //array_multisort($abcisas, SORT_DESC, $json);
        array_multisort($ordenadas, SORT_DESC, $json);
        $jsonTmp = $json;
        $jsonTmp2 = [];
        $from = strtotime($from);

        for ($i = 0; $i < count($jsonTmp); $i++) {
            $fecha      = strtotime($jsonTmp[$i]["date"]);
            $codactu    = $jsonTmp[$i]["appt_number"];
            $coordx     = $jsonTmp[$i]["coordx"];
            $coordy     = $jsonTmp[$i]["coordy"];

            if ($fecha === $from){
                foreach ($actividades as $key2 => $value2)
                {
                    if ($value2 === $codactu)
                    {
                        $data[] = ["codactu" => $codactu,
                            "longitud" => $coordx,
                            "latitud" => $coordy
                            ];
                    }
                } 
            } else {
                unset($jsonTmp[$i]);
            }
            $coordMedia["longitud"][] = $coordx;
            $coordMedia["latitud"][]=$coordy;
        }

        $coordMedia["longitud"] = 
            FuncionesMatematicas::mediaAritmetica($coordMedia["longitud"]);
        $coordMedia["latitud"] = 
            FuncionesMatematicas::mediaAritmetica($coordMedia["latitud"]);

        $rutas = $objRouting->posiblesRutas($data);
        $i = 0;
        foreach ($rutas["rutasunicas"] as $key => $value)
        {
            $ruta = $value["rutadistancia"];
            $ruta = explode("-", $ruta);
            $color = FuncionesMatematicas::randomHexadecimal(1118481, 15658734);
            foreach ($ruta as $key2 => $value2)
            {
                foreach ($jsonTmp as $key3 => $value3)
                {
                    if ($value3["appt_number"] === $value2)
                    {
                        if (isset($colores[$i]))
                            $jsonTmp[$key3]["color"] = $colores[$i];
                        else
                            $jsonTmp[$key3]["color"] = $color;
                    }
                }
            }
            $i++;
        }

        $rutaFile = public_path()."/json/routing/rutamascorta.json";
        File::put($rutaFile,json_encode($rutas["rutasunicas"]));
        return $jsonTmp;
    }
}
