<?php
class FormularioInternoOTController extends \BaseController
{

    /**
     * validarcion de formulario de offitrack
     */
    public function validarEntrada($form, &$simpleXml){
        //Captura de formulario
        if (isset($form) and ! empty($form)) {
            //Cadena vacia
            try {
                if ($form === "") {
                    throw new Exception("Cadena vac&iacute;a.");
                }
            } catch (Exception $exc) {
                //Registrar en DB lo capturado
                $this->_errorController->saveError($exc);
            }
            //Captura de datos
            $simpleXml = simplexml_load_string($form);
            return true;
        } else {
            $simpleXml = 'Error, el xml recibido es incorrecto.';
            return false;
        }
    }


    public function postPruebaselectricas()
    {
        $xmlResponse = "<Response>"
                        . "<Message>"
                            . "<Text>[msg]</Text>"
                            . "<Icon>Warning/Critical/Info</Icon>"
                            . "<ButtonText>OK</ButtonText>"
                        . "</Message>"
                        . "<ReturnValue>"
                            . "<ShortText>[shortText]</ShortText>"
                            . "<LongText>[longText]</LongText>"
                            . "<Value>[value]</Value>"
                            . "<Action></Action>"
                        . "</ReturnValue>"
                     . "</Response>";

        $data = Input::get('Data');
        $type = Input::get('type','');

        if (($this->validarEntrada($data,$simpleXml)) 
            && $type == 'speedy_basica') {

            $rpta = '';

            foreach ($simpleXml->Fields->Field as $key => $val) {
                $id = $val->Id;
                if ($id == 'nro_telefono') {
                    $telefono = trim($val->Value);
                }
                if ($id == 'observaciones') {
                    $observaciones = trim($val->Value);
                }
            }
            $data = Input::all();
            $data['telefono']=$telefono;
            $data['form']= true;
            Input::replace($data);

            $result = Api::getWS();
            if ($result['rst'] == 1) {
                $rpta = ' Pruebas electricas: '.$result['msj'].'\n';
            }

            $results = Api::getWSClearView();
            if ($results['rst'] == 1) {
                $rpta .= ' Clearview: '.$results['msj'].'\n';
            }

            //respuesta officetrack
            $searchArray = array('[msg]', '[shortText]', '[value]', '[longText]');
            $replaceArray = array(
                'Respuesta',
                'Se procesó la consulta',
                '0',
                $rpta
            );
            $xmlResponse = str_replace($searchArray, $replaceArray, $xmlResponse);
            return $xmlResponse;

        } else {
            return $simpleXml;
        } 

    }
}