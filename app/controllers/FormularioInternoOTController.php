<?php

class FormularioInternoOTController extends \BaseController
{

    /**
     * validarcion de formulario de offitrack
     */
    public function validarEntrada($form, &$simpleXml){
        //Captura de formulario
        if (isset($form) and ! empty($form)) {
            //Cadena vacia
            try {
                if ($form === "") {
                    throw new Exception("Cadena vac&iacute;a.");
                }
            } catch (Exception $exc) {
                //Registrar en DB lo capturado
                $this->_errorController->saveError($exc);
            }
            //Captura de datos
            $simpleXml = simplexml_load_string($form);
            return true;
        } else {
            $simpleXml = 'Error, el xml recibido es incorrecto.';
            return false;
        }
    }


    public function postPruebaselectricas()
    {
        $xmlResponse= "<Response>"
                        . "<Message>"
                            . "<Text>[msg]</Text>"
                            . "<Icon>Warning/Critical/Info</Icon>"
                            . "<ButtonText>OK</ButtonText>"
                        . "</Message>"
                        . "<ReturnValue>"
                            . "<ShortText>[shortText]</ShortText>"
                            . "<LongText>[longText]</LongText>"
                            . "<Value>[value]</Value>"
                            . "<Action></Action>"
                        . "</ReturnValue>"
                     . "</Response>";

        $data = Input::get('Data');
        $type = Input::get('type','');

        if (($this->validarEntrada($data,$simpleXml)) 
            && $type == 'speedy_basica') {

            $rpta = '';

            foreach ($simpleXml->Fields->Field as $key => $val) {
                $id = $val->Id;
                if ($id == 'nro_telefono') {
                    $telefono = trim($val->Value);
                }
                if ($id == 'observaciones') {
                    $observaciones = trim($val->Value);
                }
            }
            $data = Input::all();
            $data['telefono']=$telefono;
            $data['form']= true;
            Input::replace($data);

            $result = Api::getWS();
            if ($result['rst'] == 1) {
                $rpta = ' Pruebas electricas: '.$result['msj'].'\n';
            }

            $results = Api::getWSClearView();
            if ($results['rst'] == 1) {
                $rpta .= ' Clearview: '.$results['msj'].'\n';
            }

            //respuesta officetrack
            $searchArray = array('[msg]', '[shortText]', '[value]', '[longText]');
            $replaceArray = array(
                'Respuesta',
                'Se procesó la consulta',
                '0',
                $rpta
            );
            return str_replace($searchArray, $replaceArray, $xmlResponse);

        } else {
            return $simpleXml;
        } 

    } 

    public function postOperaciondeco()
    {
        $xmlResponse= "<Response>"
                        . "<Message>"
                            . "<Text>[msg]</Text>"
                            . "<Icon>Warning/Critical/Info</Icon>"
                            . "<ButtonText>OK</ButtonText>"
                        . "</Message>"
                        . "<ReturnValue>"
                            . "<ShortText>[shortText]</ShortText>"
                            . "<LongText>[longText]</LongText>"
                            . "<Value>[value]</Value>"
                            . "<Action></Action>"
                        . "</ReturnValue>"
                     . "</Response>";

        $datos=array();

        $data = Input::get('Data','');
        $type = trim(Input::get('type',''));

       // $fo = fopen("FormularioInternoOT.txt", "w+");
       //     fwrite($fo, serialize($data) . "\r\n");
       //     fwrite($fo, serialize($type) . "\r\n");
       //     fclose($fo);

        if (($this->validarEntrada($data,$simpleXml)) 
            && $type == 'operaciondeco') {

            $datos["telefonoOrigen"]="";
            if($simpleXml->Employee->EmployeeNumber){
                $tecnico=Tecnico::Where("carnet_tmp",$simpleXml->Employee->EmployeeNumber)->First();
                if($tecnico){
                    $datos["telefonoOrigen"]=trim($tecnico->celular);
                }
            }

            foreach ($simpleXml->Fields->Field as $key => $val) {
                $id = $val->Id;
                if ($id == 'actividad_tipo_id') {
                    $datos["actividad_id"] = trim($val->ValueBehind);
                }
                if ($id == 'accion') {
                    $datos["accion"] = strtolower(trim($val->Value));
                }
                if ($id == 'num_requerimiento') {
                    $datos["num_requerimiento"] = trim($val->Value);
                }
                if ($id == 'ser_deco') {
                    $datos["numser"] = trim($val->Value);
                }
                if ($id == 'cod_mat') {
                    $datos["codmat"] = trim($val->Value);
                }
                if ($id == 'ser_tar') {
                    $datos["numtar"] = trim($val->Value);
                }
            }
            $datos["interfaz"] ='1';

            $response = json_decode(Helpers::ruta(
                    'api/operation', 'POST',$datos
                ));
            //respuesta officetrack
            $searchArray = array('[msg]', '[shortText]', '[value]', '[longText]');
            $replaceArray = array(
                'Respuesta',
                'Se procesó la consulta',
                '0',
                $response->msj
            );
            return str_replace($searchArray, $replaceArray, $xmlResponse);
            
        } else {
            return $simpleXml;
        } 

    }

}