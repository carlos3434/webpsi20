<?php

use yajra\Datatables\Datatables;

class MensajeController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function postListar()
    {
        $posts = Mensaje::select(
            array(
                'id'
                , 'app_host'
                , 'app_port'
                , 'app_url'
                , 'message_id'
                , 'company_id'
                , 'send_to'
                , 'address'
                , 'subject'
                , 'body'
                , 'job_id'
                , 'created_at'
                , 'updated_at'
                , 'deleted_at'
                , 'estado'
            )
        )->withTrashed();

        $rfechas = Input::has('txt_rangofecha') ?
                Input::get('txt_rangofecha') :
                null;
        return Datatables::of($posts)
                        ->addColumn(
                            'action', function ($message) {
                            return '<a data-toggle="modal" '
                                    . 'data-target="#mensajeDetalleModal" '
                                    . 'href="#detalle-' . $message->id . '" '
                                    . 'class="btn btn-primary">'
                                    . '<i class="fa fa-eye"></i></a>';
                            }
                        )
                        ->filter(
                            function ($query) use ($rfechas) {
                                if ($rfechas) {
                                    //dd($rfechas);
                                    $fechas = explode(' - ', $rfechas);
                                    $fechas = array(
                                            $fechas[0].' 00:00:01',
                                            $fechas[1].' 23:59:59'
                                        );

                                    $query->whereBetween('created_at', $fechas);
                                }
                            }
                        )
                        ->make(true);          
    }
}