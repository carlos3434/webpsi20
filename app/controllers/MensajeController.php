<?php

use yajra\Datatables\Datatables;

class MensajeController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function postListar()
    {
        $posts = Mensaje::select(
            array(
                'id'
                , 'app_host'
                , 'app_port'
                , 'app_url'
                , 'message_id'
                , 'company_id'
                , 'address'
                , 'subject'
                , 'body'
                , 'job_id'
                , 'created_at'
                , 'updated_at'
                , 'deleted_at'
                , 'estado'
            )
        )->withTrashed();

        $rfechas = Input::get('txt_rangofecha', null);

        $msg_id = Input::get('id_msg', null);
        $asunto = Input::get('asunto', null);
        $estado = Input::get('estado', null);

        return Datatables::of($posts)
                        ->addColumn(
                            'action', function ($message) {
                            return '<a data-toggle="modal" '
                                    . 'data-target="#mensajeDetalleModal" '
                                    . 'href="#detalle-' . $message->id . '" '
                                    . 'class="btn btn-primary">'
                                    . '<i class="fa fa-eye"></i></a>';
                            }
                        )
                        ->filter(
                            function ($query) use ($rfechas, $msg_id, $asunto, $estado) {
                                if ($rfechas) {
                                    //dd($rfechas);
                                    $fechas = explode(' - ', $rfechas);
                                    $fechas = array(
                                            $fechas[0].' 00:00:01',
                                            $fechas[1].' 23:59:59'
                                        );


                                    $query->whereBetween('created_at', $fechas);
                                }

                                if($msg_id != "" && $msg_id != null ){
                                    $query->where('message_id', $msg_id);
                                }

                                if($asunto != "" && $asunto != null ){
                                    $query->whereIn('subject', $asunto);
                                }

                                if($estado != "" && $estado != null ){
                                    $query->whereIn('estado', $estado);
                                }
                            }
                        )
                        ->make(true);          
    }
}