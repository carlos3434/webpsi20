<?php
use Legados\STGestelApi;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\TipoOperacion;

class TestLegadoController extends BaseController
{
    public function getCierreaveria()
    {
        $contrata = Input::get('contrata');
        $tecnico1 = Input::get('tecnico1');
        $solicitudid = Input::get('solicitud');
        $tipoOperacion = Input::get('tipooperacion', '');
        $operacion = Input::get('operacion', '');
        $sintoma = Input::get('sintoma', '');

        $st = SolicitudTecnicaGestelAveria::where('id_solicitud_tecnica',$solicitudid)->first();
        $st->contrata=$contrata;
        $st->save();
        $fechaProgramacion =    '2017-07-18 14:40';
        $fechaEmision =         '2017-07-18 14:40';
        $fechaLiquid =          '2017-07-18 14:40:00';
        $fechaSal =             '2017-07-18 14:40';
        $fechaDestino =         '2017-07-18 14:40';

        $sql = "select 
                    p.solicitud_tecnica_id,
                    p.tipo_operacion,
                    p.id_solicitud_tecnica 'numero_solicitud_tecnica',
                    '2017-05-29' as 'fec_hora_programacion',
                    p.contrata 'contrata',
                    '' as 'tecnico2',
                    '' as 'tecnico3',
                    '' as 'ncompro',
                    '2017-05-29' as 'fecha_emision',
                    '' as 'codigo_franqueo',
                    '' as 't_liquid',
                    '10:00:00' as 'hora_inicio',
                    '10:00:00' as 'hora_fin',
                    '' as 'codigo_liquidacion',
                    '' as 'codigo_destino',
                    '' as 'codigo_version',
                    '' as 'codigo_detalle',
                    '2017-05-29' as 'fecha_sal',
                    '2017-05-29' as 'fecha_destino',
                    '' as 'nomcon_loq',
                    '' as 'telecon_liq',
                    '' as 'sintoma',
                    '' as 'obs_ultima_liq',
                    p.valor1 'dato1',
                    p.valor2 'dato2',
                    p.valor3 'dato3',
                    p.valor4 'dato4',
                    p.valor5 'dato5'
                    from solicitud_tecnica_gestel_averia p 
                    where p.id_solicitud_tecnica = ? order by p.id desc limit 1";

        $solicitud = DB::select($sql, array($solicitudid));

        $sql2 = "select 
                p.solicitud_tecnica_id,
                'ACI_LIQ_BLI' as 'tipo_operacion',
                p.id_solicitud_tecnica 'numero_solicitud_tecnica',
                '2017-05-29' as 'fec_hora_programacion',
                p.contrata 'contrata',
                '' as 'tecnico2',
                '' as 'tecnico3',
                '' as 'ncompro',
                '2017-05-29' as 'fecha_emision',
                '001' as 'codigo_franqueo',
                '2017-05-30' as 't_liquid',
                '10:00:00' as 'hora_inicio',
                '10:00:00' as 'hora_fin',
                '12' as 'codigo_liquidacion',
                '03' as 'codigo_destino',
                '01' as 'codigo_version',
                '' as 'codigo_detalle',
                '2017-05-29' as 'fecha_sal',
                '2017-05-29' as 'fecha_destino',
                '' as 'nomcon_loq',
                '' as 'telecon_liq',
                '' as 'sintoma',
                '' as 'obs_ultima_liq',
                p.valor1 'dato1',
                p.valor2 'dato2',
                p.valor3 'dato3',
                p.valor4 'dato4',
                p.valor5 'dato5',
                cg.tipo_equipo 'tipo_equipo',
                cg.codigo_equipo 'codigo_equipo',
                cg.modelo 'modelo',
                cg.mantenimiento 'mantenimiento',
                cg.numero_serie 'serie',
                cg.marca 'marca',
                cg.codbarr 'codigo_barra',
                cg.cod_material 'codigo_material',
                '1'  as 'cantidad',
                ''  as 'descr_unidad_medida',
                '' as 'codigo_repuesto',
                cg.numero_cabina 'numero_cabina',
                cg.valor1 'reservado1',
                cg.valor2 'reservado2',
                cg.valor3 'reservado3',
                cg.valor4 'reservado4',
                ''  as 'reservado5'
                from solicitud_tecnica_gestel_averia p
                left join componente_gestel cg 
                on cg.solicitud_tecnica_gestel_id = p.id and cg.actividad_id = 1 and cg.deleted_at is null
                where p.id_solicitud_tecnica = ?";

        $componentes = DB::select($sql2, array($solicitudid));
            
        if ($tipoOperacion == "") {
            $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                        ->where('tipo_legado', 2)
                        ->where('tipo', 0)
                        ->whereRaw('tipo_operacion like "%'.$solicitud[0]->tipo_operacion.'%"')
                        ->first();

            $tipoOperacion = "ACI_LIQ_BLI";
            if (!is_null($tipoOperacionObj)) {
                $tipoOperacion = $tipoOperacionObj->codigo;
            }
        }
        print_r($solicitud[0]->tipo_operacion." | ".$tipoOperacion);

        $componentesArray = [];
        $reservadoArray = [];
        foreach ($componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => ($value->tipo_equipo == null or 
                                            $value->tipo_equipo == "")?
                                            'TELEFONO':$value->tipo_equipo,
                'codigo_equipo'         => ($value->codigo_equipo == null or 
                                            $value->codigo_equipo == "")?
                                            '31':$value->codigo_equipo,
                'modelo'                => ($value->modelo == null or 
                                            $value->modelo == "")?
                                            '1601':$value->modelo,
                'marca'                 => ($value->marca == null or 
                                            $value->marca == "")?
                                            '16':$value->marca,
                'mantenimiento'         => $value->mantenimiento,
                'serie'                 => ($value->serie == null or 
                                            $value->serie == "")?
                                            '1234567890':$value->serie,
                'codigo_barra'          => ($value->codigo_barra == null or 
                                            $value->codigo_barra == "")?
                                            '1321231432':$value->codigo_barra,
                'codigo_material'       => ($value->codigo_material == null or 
                                            $value->codigo_material == "")?
                                            '0001':$value->codigo_material,
                'cantidad'              => ($value->cantidad == null or 
                                            $value->cantidad == "")?
                                            '1':$value->cantidad,
                'descr_unidad_medida'   => ($value->descr_unidad_medida == null or 
                                            $value->descr_unidad_medida == "")?
                                            'UND':$value->descr_unidad_medida,
                'codigo_repuesto'       => "",
                'numero_cabina'         => ($value->numero_cabina == null or 
                                            $value->numero_cabina == "")?
                                            '103':$value->numero_cabina,
            ];

            $reservadoArray[] = [
                'reservado1'    => $value->reservado1,
                'reservado2'    => $value->reservado2,
                'reservado3'    => $value->reservado3,
                'reservado4'    => $value->reservado4,
                'reservado5'    => $value->reservado5,
            ];
        }

        $elementos = [
            'solicitud_tecnica_id'      => $solicitud[0]->solicitud_tecnica_id,
            'tipo_operacion'            => $tipoOperacion,
            'numero_solicitud_tecnica'  => $solicitud[0]->numero_solicitud_tecnica,
            'fec_hora_programacion'     => $fechaProgramacion,
            'contrata'                  => ($solicitud[0]->contrata == null or 
                                            $solicitud[0]->contrata == "")?
                                            'AB':$solicitud[0]->contrata,
            'tecnico'                   => $tecnico1,
            'tecnico2'                  => $solicitud[0]->tecnico2,
            'tecnico3'                  => $solicitud[0]->tecnico3,
            // 'observacion_mov'           => 'AVERIA PROGRAMADA AUTOMATICAMENTE',
            // 'descrip_obs_des'           => '',
            'ncompro'                   => ($solicitud[0]->ncompro == null or 
                                            $solicitud[0]->ncompro == "")?
                                            '34234234':$solicitud[0]->ncompro,
            'fecha_emision'             => $fechaEmision,
            'codigo_franqueo'           => ($solicitud[0]->codigo_franqueo == null or 
                                            $solicitud[0]->codigo_franqueo == "")?
                                            '001':$solicitud[0]->codigo_franqueo,
            // 'observacion'               => '',
            't_liquid'                  => $fechaLiquid,
            'hora_inicio'               => '',
            'hora_fin'                  => '',
            // 'falla_reportada'           => '02',
            'codigo_liquidacion'        => ($solicitud[0]->codigo_liquidacion == null or 
                                            $solicitud[0]->codigo_liquidacion == "")?
                                            '12':$solicitud[0]->codigo_liquidacion,
            'codigo_destino'            => 'LIQU',
            'codigo_version'            => '01',
            'codigo_detalle'            => ($solicitud[0]->codigo_detalle == null or 
                                            $solicitud[0]->codigo_detalle == "")?
                                            '01':$solicitud[0]->codigo_detalle,
            'fecha_sal'                 => $fechaSal,
            'fecha_destino'             => $fechaDestino,
            'nomcon_liq'                => 'PRUEBA',
            'telecon_liq'               => '61590402',
            'sintoma'                   => $sintoma,
            'obs_ultima_liq'            => 'TEST',

            // 'causa'                     => '501',
            // 'codigo_categoria'          => 'C',
            // 'tipo_averia'               => 'P',
            // 'Fecha_vencimiento_GICS'    => '',
            // 'telfic'                    => '',
            // 'observacion_102'           => 'Pruebas',
            // 'codigo_boleta'             => '0',
            // 'estado_boleta'             => '1',
            // 'detalle_contacto'          => '',
            // 'fecha_liquidacion'         => '10-07-2017',
            // 'codigo_tecnico_red'        => '178918',
            // 'tipo_ruta'                 => 'A',
            // 'motivo'                    => '02',
            // 'repuesto'                  => '0011',
            
            'dato1'                     => $solicitud[0]->dato1,
            'dato2'                     => $solicitud[0]->dato2,
            'dato3'                     => $solicitud[0]->dato3,
            'dato4'                     => $solicitud[0]->dato4,
            'dato5'                     => $solicitud[0]->dato5,
            'datosEquipo'               => $componentesArray,
            'reservado'                 => $reservadoArray,
        ];
        // dd(json_encode($elementos));

        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
        $objStapi = new STGestelApi("GESTEL Legado");
        $response = $objStapi->cierreAveria($elementos);
        //print_r($response);
    }

    public function getCierreprovision()
    {
        $contrata = Input::get('contrata');
        $tecnico1 = Input::get('tecnico1');
        $solicitudid = Input::get('solicitud');
        $tipoOperacion = Input::get('tipooperacion', '');
        $operacion = Input::get('operacion', '');

        $st = SolicitudTecnicaGestelProvision::where('id_solicitud_tecnica',$solicitudid)->first();
        $st->contrata=$contrata;
        $st->save();

        $fechaProgramacion =    '19-07-17';
        $fechaEmision =         '19-07-17';
        $fechaEjecucion =       '19-07-17';
        $horaInicio =           '15:25';
        $horaFin =              '16:40';
        $fechaDesplazamiento =  '12:10';
        $fechaDevolucion =      '12:10 19-07-17';

        $sql = "select 
                    p.solicitud_tecnica_id,
                    p.tipo_operacion,
                    p.id_solicitud_tecnica 'num_solicitud_tecnica',
                    p.id_solicitud_tecnica,
                    '2017-05-25' as 'fecha_programacion',
                    p.contrata,
                    '' as 'tecnico2',
                    '2017-05-29' as 'fecha_emision_boleta',
                    'JEE WITH VUEjs' as 'nombre_persona_recibe',
                    '10101010' as 'numero_doc_identidad',
                    'TEST' as 'observacion',
                    '0' as 'parentesco',
                    'N' as 'indicador_fraude',
                    '' as 'anexos',
                    '2017-05-29' as 'fecha_ejecucion',
                    '10:00' as 'hora_inicio',
                    '10:00' as 'hora_fin',
                    '12:00' as 'desplazamiento',
                    '1' as 'escenario',
                    'N' as 'cabina',
                    '' as 'ambiente',
                    '' as 'cod_mot',
                    '' as 'des_obs',
                    '2017-05-29' as 'fecha_hora_devolucion',
                    p.valor1 'dato1',
                    p.valor2 'dato2',
                    p.valor3 'dato3',
                    p.valor4 'dato4',
                    p.valor5 'dato5'
                    from solicitud_tecnica_gestel_provision p 
                    where p.id_solicitud_tecnica = ? order by p.id desc limit 1";

        $solicitud = DB::select($sql, array($solicitudid));

        $sql2 = "select 
                p.solicitud_tecnica_id,
                'PCI_LIQ_LIN' as 'tipo_operacion',
                p.id_solicitud_tecnica 'num_solicitud_tecnica',
                p.id_solicitud_tecnica,
                '2017-05-25' as 'fecha_programacion',
                p.contrata,
                '' as 'tecnico2',
                '2017-05-29' as 'fecha_emision_boleta',
                'JEE WITH VUEjs' as 'nombre_persona_recibe',
                '10101010' as 'numero_doc_identidad',
                'TEST' as 'observacion',
                '0' as 'parentesco',
                'N' as 'indicador_fraude',
                '' as 'anexos',
                '2017-05-29' as 'fecha_ejecucion',
                '10:00:00' as 'hora_inicio',
                '10:00:00' as 'hora_fin',
                '12:00' as 'desplazamiento',
                'O' as 'escenario',
                'N' as 'cabina',
                '' as 'ambiente',
                '' as 'cod_mot',
                '' as 'des_obs',
                '2017-05-29' as 'fecha_hora_devolucion',
                cg.tipo_equipo 'tipo_equipo',
                cg.codigo_equipo 'codigo_equipo',
                cg.desc_equipo 'descripcion_equipo',
                cg.cod_material 'cod_material',
                '1' as 'cantidad_material',
                'UND' as 'unidad_medida',
                cg.numero_serie 'nro_serie',
                cg.codbarr 'codigo_barras',
                '1' as 'tipo_transaccion',
                cg.marca 'marca_equipo',
                cg.modelo 'modelo_equipo',
                cg.mantenimiento 'mantenimiento_datos',
                cg.numero_cabina 'numero_cabina',
                cg.valor1 'dato1',
                cg.valor2 'dato2',
                cg.valor3 'dato3',
                cg.valor4 'dato4',
                ''  as 'dato5'
                from solicitud_tecnica_gestel_provision p
                left join componente_gestel cg 
                on cg.solicitud_tecnica_gestel_id = p.id and cg.actividad_id = 2 and cg.deleted_at is null
                where p.id_solicitud_tecnica = ?";

        $componentes = DB::select($sql2, array($solicitudid));
            
        if ($tipoOperacion == "") {
            $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                            ->where('tipo_legado', 2)
                            ->where('tipo', 0)
                            ->whereRaw('tipo_operacion like "%'.$solicitud[0]->tipo_operacion.'%"')
                            ->first();

            $tipoOperacion = "PCI_LIQ_SPE";
            if (!is_null($tipoOperacionObj)) {
                $tipoOperacion = $tipoOperacionObj->codigo;
            }
        }
        print_r($solicitud[0]->tipo_operacion." | ".$tipoOperacion);

        $componentesArray = [];
        foreach ($componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => ($value->tipo_equipo == null or 
                                            $value->tipo_equipo == "")?
                                            'TELEFONO':$value->tipo_equipo,
                'codigo_equipo'         => ($value->codigo_equipo == null or 
                                            $value->codigo_equipo == "")?
                                            '31':$value->codigo_equipo,
                'descripcion_equipo'    => ($value->descripcion_equipo == null or 
                                            $value->descripcion_equipo == "")?
                                            'TELEFONO FORMA':$value->descripcion_equipo,
                'cod_material'          => ($value->cod_material == null or 
                                            $value->cod_material == "")?
                                            '01506553':$value->cod_material,
                'cantidad_material'     => ($value->cantidad_material == null or 
                                            $value->cantidad_material == "")?
                                            '1':$value->cantidad_material,
                'unidad_medida'         => $value->unidad_medida,
                'nro_serie'             => ($value->nro_serie == null or 
                                            $value->nro_serie == "")?
                                            '1234567890':$value->nro_serie,
                'codigo_barras'         => $value->codigo_barras,
                'tipo_transaccion'      => ($value->tipo_transaccion == null or 
                                            $value->tipo_transaccion == "")?
                                            '1':$value->tipo_transaccion,
                'marca_equipo'          => $value->marca_equipo,
                'modelo_equipo'         => $value->modelo_equipo,
                'mantenimiento_datos'   => $value->mantenimiento_datos,
                'numero_cabina'         => $value->numero_cabina,
                'dato1'                 => $value->dato1,
                'dato2'                 => $value->dato2,
                'dato3'                 => $value->dato3,
                'dato4'                 => $value->dato4,
                'dato5'                 => $value->dato5,
            ];
        }

        $elementos = [
            'solicitud_tecnica_id'      => $solicitud[0]->solicitud_tecnica_id,
            'tipo_operacion'            => $tipoOperacion,
            'num_solicitud_tecnica'     => $solicitud[0]->num_solicitud_tecnica, // id_solicitud_tecnica
            'id_solicitud_tecnica'      => $solicitud[0]->id_solicitud_tecnica, // id_solicitud_tecnica
            'fecha_programacion'        => $fechaProgramacion,//$solicitud[0]->fecha_programacion, // fecha_programacion
            'contrata'                  => $solicitud[0]->contrata, // contrata
            'tecnico1'                  => $tecnico1,
            'tecnico2'                  => $solicitud[0]->tecnico2,
            'fecha_emision_boleta'      => $fechaEmision,//$solicitud[0]->fecha_emision_boleta,
            'nombre_persona_recibe'     => $solicitud[0]->nombre_persona_recibe, // nombres_contacto
            'numero_doc_identidad'      => $solicitud[0]->numero_doc_identidad, // numero_doc
            'observacion'               => $solicitud[0]->observacion, // desobs
            'parentesco'                => $solicitud[0]->parentesco,
            'indicador_fraude'          => $solicitud[0]->indicador_fraude,
            'anexos'                    => $solicitud[0]->anexos,
            'fecha_ejecucion'           => $fechaEjecucion,//$solicitud[0]->fecha_ejecucion,
            'hora_inicio'               => $horaInicio,//$solicitud[0]->hora_inicio,
            'hora_fin'                  => $horaFin,
            'desplazamiento'            => $fechaDesplazamiento,//$solicitud[0]->desplazamiento,
            'escenario'                 => $solicitud[0]->escenario, // escenario_gestel
            'cabina'                    => $solicitud[0]->cabina,
            'ambiente'                  => $solicitud[0]->ambiente,
            'cod_mot'                   => $solicitud[0]->cod_mot,
            'des_obs'                   => $solicitud[0]->des_obs,
            'fecha_hora_devolucion'     => $fechaDevolucion,//$solicitud[0]->fecha_hora_devolucion, // fecha_hora_devolucion
            'dato1'                     => $solicitud[0]->dato1, // valor1
            'dato2'                     => $solicitud[0]->dato2, // valor2
            'dato3'                     => $solicitud[0]->dato3, // valor3
            'dato4'                     => $solicitud[0]->dato4, // valor4
            'dato5'                     => $solicitud[0]->dato5, // valor5
            'DatosMateriales'           => $componentesArray
        ];
        // dd(json_encode($elementos));

        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
        $objStapi = new STGestelApi("GESTEL Legado");
        $response = $objStapi->cierreProvision($elementos);
        //print_r($response);
    }

    public function postCierreaveria()
    {
        $contrata = Input::get('contrata');
        $tecnico1 = Input::get('tecnico1');
        $solicitudid = Input::get('solicitud');
        $tipoOperacion = Input::get('tipooperacion', '');
        $operacion = Input::get('operacion', '');
        $sintoma = Input::get('sintoma', '');
        $codfra = Input::get('codfra', '003');

        $st = SolicitudTecnicaGestelAveria::where('id_solicitud_tecnica',$solicitudid)->first();
        $st->contrata=$contrata;
        $st->save();
        $fechaProgramacion =    '2017-07-18 14:40';
        $fechaEmision =         '2017-07-18 14:40';
        $fechaLiquid =          '2017-07-18 14:40:00';
        $fechaSal =             '2017-07-18 14:40';
        $fechaDestino =         '2017-07-18 14:40';

        $sql = "select 
                    p.solicitud_tecnica_id,
                    p.tipo_operacion,
                    p.id_solicitud_tecnica 'numero_solicitud_tecnica',
                    '2017-05-29' as 'fec_hora_programacion',
                    p.contrata 'contrata',
                    '' as 'tecnico2',
                    '' as 'tecnico3',
                    '' as 'ncompro',
                    '2017-05-29' as 'fecha_emision',
                    '' as 'codigo_franqueo',
                    '' as 't_liquid',
                    '10:00:00' as 'hora_inicio',
                    '10:00:00' as 'hora_fin',
                    '' as 'codigo_liquidacion',
                    '' as 'codigo_destino',
                    '' as 'codigo_version',
                    '' as 'codigo_detalle',
                    '2017-05-29' as 'fecha_sal',
                    '2017-05-29' as 'fecha_destino',
                    '' as 'nomcon_loq',
                    '' as 'telecon_liq',
                    '' as 'sintoma',
                    '' as 'obs_ultima_liq',
                    p.valor1 'dato1',
                    p.valor2 'dato2',
                    p.valor3 'dato3',
                    p.valor4 'dato4',
                    p.valor5 'dato5'
                    from solicitud_tecnica_gestel_averia p 
                    where p.id_solicitud_tecnica = ? order by p.id desc limit 1";

        $solicitud = DB::select($sql, array($solicitudid));

        $sql2 = "select 
                p.solicitud_tecnica_id,
                'ACI_LIQ_BLI' as 'tipo_operacion',
                p.id_solicitud_tecnica 'numero_solicitud_tecnica',
                '2017-05-29' as 'fec_hora_programacion',
                p.contrata 'contrata',
                '' as 'tecnico2',
                '' as 'tecnico3',
                '' as 'ncompro',
                '2017-05-29' as 'fecha_emision',
                '001' as 'codigo_franqueo',
                '2017-05-30' as 't_liquid',
                '10:00:00' as 'hora_inicio',
                '10:00:00' as 'hora_fin',
                '12' as 'codigo_liquidacion',
                '03' as 'codigo_destino',
                '01' as 'codigo_version',
                '' as 'codigo_detalle',
                '2017-05-29' as 'fecha_sal',
                '2017-05-29' as 'fecha_destino',
                '' as 'nomcon_loq',
                '' as 'telecon_liq',
                '' as 'sintoma',
                '' as 'obs_ultima_liq',
                p.valor1 'dato1',
                p.valor2 'dato2',
                p.valor3 'dato3',
                p.valor4 'dato4',
                p.valor5 'dato5',
                cg.tipo_equipo 'tipo_equipo',
                cg.codigo_equipo 'codigo_equipo',
                cg.modelo 'modelo',
                cg.mantenimiento 'mantenimiento',
                cg.numero_serie 'serie',
                cg.marca 'marca',
                cg.codbarr 'codigo_barra',
                cg.cod_material 'codigo_material',
                '1'  as 'cantidad',
                ''  as 'descr_unidad_medida',
                '' as 'codigo_repuesto',
                cg.numero_cabina 'numero_cabina',
                cg.valor1 'reservado1',
                cg.valor2 'reservado2',
                cg.valor3 'reservado3',
                cg.valor4 'reservado4',
                ''  as 'reservado5'
                from solicitud_tecnica_gestel_averia p
                left join componente_gestel cg 
                on cg.solicitud_tecnica_gestel_id = p.id and cg.actividad_id = 1 and cg.deleted_at is null
                where p.id_solicitud_tecnica = ?";

        $componentes = DB::select($sql2, array($solicitudid));
            
        if ($tipoOperacion == "") {
            $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                        ->where('tipo_legado', 2)
                        ->where('tipo', 0)
                        ->whereRaw('tipo_operacion like "%'.$solicitud[0]->tipo_operacion.'%"')
                        ->first();

            $tipoOperacion = "ACI_LIQ_BLI";
            if (!is_null($tipoOperacionObj)) {
                $tipoOperacion = $tipoOperacionObj->codigo;
            }
        }
        print_r($solicitud[0]->tipo_operacion." | ".$tipoOperacion);

        $componentesArray = [];
        $reservadoArray = [];
        foreach ($componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => ($value->tipo_equipo == null or 
                                            $value->tipo_equipo == "")?
                                            'TELEFONO':$value->tipo_equipo,
                'codigo_equipo'         => ($value->codigo_equipo == null or 
                                            $value->codigo_equipo == "")?
                                            '31':$value->codigo_equipo,
                'modelo'                => ($value->modelo == null or 
                                            $value->modelo == "")?
                                            '1601':$value->modelo,
                'marca'                 => ($value->marca == null or 
                                            $value->marca == "")?
                                            '16':$value->marca,
                'mantenimiento'         => $value->mantenimiento,
                'serie'                 => ($value->serie == null or 
                                            $value->serie == "")?
                                            '1234567890':$value->serie,
                'codigo_barra'          => ($value->codigo_barra == null or 
                                            $value->codigo_barra == "")?
                                            '1321231432':$value->codigo_barra,
                'codigo_material'       => ($value->codigo_material == null or 
                                            $value->codigo_material == "")?
                                            '0001':$value->codigo_material,
                'cantidad'              => ($value->cantidad == null or 
                                            $value->cantidad == "")?
                                            '1':$value->cantidad,
                'descr_unidad_medida'   => ($value->descr_unidad_medida == null or 
                                            $value->descr_unidad_medida == "")?
                                            'UND':$value->descr_unidad_medida,
                'codigo_repuesto'       => "",
                'numero_cabina'         => ($value->numero_cabina == null or 
                                            $value->numero_cabina == "")?
                                            '103':$value->numero_cabina,
            ];

            $reservadoArray[] = [
                'reservado1'    => $value->reservado1,
                'reservado2'    => $value->reservado2,
                'reservado3'    => $value->reservado3,
                'reservado4'    => $value->reservado4,
                'reservado5'    => $value->reservado5,
            ];
        }

        $elementos = [
            'solicitud_tecnica_id'      => $solicitud[0]->solicitud_tecnica_id,
            'tipo_operacion'            => $tipoOperacion,
            'numero_solicitud_tecnica'  => $solicitud[0]->numero_solicitud_tecnica,
            'fec_hora_programacion'     => $fechaProgramacion,
            'contrata'                  => ($solicitud[0]->contrata == null or 
                                            $solicitud[0]->contrata == "")?
                                            'AB':$solicitud[0]->contrata,
            'tecnico'                   => $tecnico1,
            'tecnico2'                  => $solicitud[0]->tecnico2,
            'tecnico3'                  => $solicitud[0]->tecnico3,
            // 'observacion_mov'           => 'AVERIA PROGRAMADA AUTOMATICAMENTE',
            // 'descrip_obs_des'           => '',
            'ncompro'                   => ($solicitud[0]->ncompro == null or 
                                            $solicitud[0]->ncompro == "")?
                                            '34234234':$solicitud[0]->ncompro,
            'fecha_emision'             => $fechaEmision,
            'codigo_franqueo'           => $codfra,
            // 'observacion'               => '',
            't_liquid'                  => $fechaLiquid,
            'hora_inicio'               => '',
            'hora_fin'                  => '',
            // 'falla_reportada'           => '02',
            'codigo_liquidacion'        => ($solicitud[0]->codigo_liquidacion == null or 
                                            $solicitud[0]->codigo_liquidacion == "")?
                                            '12':$solicitud[0]->codigo_liquidacion,
            'codigo_destino'            => 'LIQU',
            'codigo_version'            => '01',
            'codigo_detalle'            => ($solicitud[0]->codigo_detalle == null or 
                                            $solicitud[0]->codigo_detalle == "")?
                                            '01':$solicitud[0]->codigo_detalle,
            'fecha_sal'                 => $fechaSal,
            'fecha_destino'             => $fechaDestino,
            'nomcon_liq'                => 'PRUEBA',
            'telecon_liq'               => '61590402',
            'sintoma'                   =>  $sintoma,
            'obs_ultima_liq'            => 'TEST',

            // 'causa'                     => '501',
            // 'codigo_categoria'          => 'C',
            // 'tipo_averia'               => 'P',
            // 'Fecha_vencimiento_GICS'    => '',
            // 'telfic'                    => '',
            // 'observacion_102'           => 'Pruebas',
            // 'codigo_boleta'             => '0',
            // 'estado_boleta'             => '1',
            // 'detalle_contacto'          => '',
            // 'fecha_liquidacion'         => '10-07-2017',
            // 'codigo_tecnico_red'        => '178918',
            // 'tipo_ruta'                 => 'A',
            // 'motivo'                    => '02',
            // 'repuesto'                  => '0011',
            
            'dato1'                     => $solicitud[0]->dato1,
            'dato2'                     => $solicitud[0]->dato2,
            'dato3'                     => $solicitud[0]->dato3,
            'dato4'                     => $solicitud[0]->dato4,
            'dato5'                     => $solicitud[0]->dato5,
            'datosEquipo'               => $componentesArray,
            'reservado'                 => $reservadoArray,
        ];
        // dd(json_encode($elementos));

        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
        $objStapi = new STGestelApi("GESTEL Legado");
        $response = $objStapi->cierreAveria($elementos);
        //print_r($response);
    }

    public function postCierreprovision()
    {
        $contrata = Input::get('contrata');
        $tecnico1 = Input::get('tecnico1');
        $solicitudid = Input::get('solicitud');
        $tipoOperacion = Input::get('tipooperacion', '');
        $operacion = Input::get('operacion', '');

        $st = SolicitudTecnicaGestelProvision::where('id_solicitud_tecnica',$solicitudid)->first();
        $st->contrata=$contrata;
        $st->save();

        $fechaProgramacion =    '19-07-17';
        $fechaEmision =         '19-07-17';
        $fechaEjecucion =       '19-07-17';
        $horaInicio =           '15:25';
        $horaFin =              '16:40';
        $fechaDesplazamiento =  '12:10';
        $fechaDevolucion =      '12:10 19-07-17';

        $sql = "select 
                    p.solicitud_tecnica_id,
                    p.tipo_operacion,
                    p.id_solicitud_tecnica 'num_solicitud_tecnica',
                    p.id_solicitud_tecnica,
                    '2017-05-25' as 'fecha_programacion',
                    p.contrata,
                    '' as 'tecnico2',
                    '2017-05-29' as 'fecha_emision_boleta',
                    'JEE WITH VUEjs' as 'nombre_persona_recibe',
                    '10101010' as 'numero_doc_identidad',
                    'TEST' as 'observacion',
                    '0' as 'parentesco',
                    'N' as 'indicador_fraude',
                    '' as 'anexos',
                    '2017-05-29' as 'fecha_ejecucion',
                    '10:00' as 'hora_inicio',
                    '10:00' as 'hora_fin',
                    '12:00' as 'desplazamiento',
                    '1' as 'escenario',
                    'N' as 'cabina',
                    '' as 'ambiente',
                    '' as 'cod_mot',
                    '' as 'des_obs',
                    '2017-05-29' as 'fecha_hora_devolucion',
                    p.valor1 'dato1',
                    p.valor2 'dato2',
                    p.valor3 'dato3',
                    p.valor4 'dato4',
                    p.valor5 'dato5'
                    from solicitud_tecnica_gestel_provision p 
                    where p.id_solicitud_tecnica = ? order by p.id desc limit 1";

        $solicitud = DB::select($sql, array($solicitudid));

        $sql2 = "select 
                p.solicitud_tecnica_id,
                'PCI_LIQ_LIN' as 'tipo_operacion',
                p.id_solicitud_tecnica 'num_solicitud_tecnica',
                p.id_solicitud_tecnica,
                '2017-05-25' as 'fecha_programacion',
                p.contrata,
                '' as 'tecnico2',
                '2017-05-29' as 'fecha_emision_boleta',
                'JEE WITH VUEjs' as 'nombre_persona_recibe',
                '10101010' as 'numero_doc_identidad',
                'TEST' as 'observacion',
                '0' as 'parentesco',
                'N' as 'indicador_fraude',
                '' as 'anexos',
                '2017-05-29' as 'fecha_ejecucion',
                '10:00:00' as 'hora_inicio',
                '10:00:00' as 'hora_fin',
                '12:00' as 'desplazamiento',
                'O' as 'escenario',
                'N' as 'cabina',
                '' as 'ambiente',
                '' as 'cod_mot',
                '' as 'des_obs',
                '2017-05-29' as 'fecha_hora_devolucion',
                cg.tipo_equipo 'tipo_equipo',
                cg.codigo_equipo 'codigo_equipo',
                cg.desc_equipo 'descripcion_equipo',
                cg.cod_material 'cod_material',
                '1' as 'cantidad_material',
                'UND' as 'unidad_medida',
                cg.numero_serie 'nro_serie',
                cg.codbarr 'codigo_barras',
                '1' as 'tipo_transaccion',
                cg.marca 'marca_equipo',
                cg.modelo 'modelo_equipo',
                cg.mantenimiento 'mantenimiento_datos',
                cg.numero_cabina 'numero_cabina',
                cg.valor1 'dato1',
                cg.valor2 'dato2',
                cg.valor3 'dato3',
                cg.valor4 'dato4',
                ''  as 'dato5'
                from solicitud_tecnica_gestel_provision p
                left join componente_gestel cg 
                on cg.solicitud_tecnica_gestel_id = p.id and cg.actividad_id = 2 and cg.deleted_at is null
                where p.id_solicitud_tecnica = ?";

        $componentes = DB::select($sql2, array($solicitudid));
            
        if ($tipoOperacion == "") {
            $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $operacion)
                            ->where('tipo_legado', 2)
                            ->where('tipo', 0)
                            ->whereRaw('tipo_operacion like "%'.$solicitud[0]->tipo_operacion.'%"')
                            ->first();

            $tipoOperacion = "PCI_LIQ_SPE";
            if (!is_null($tipoOperacionObj)) {
                $tipoOperacion = $tipoOperacionObj->codigo;
            }
        }
        print_r($solicitud[0]->tipo_operacion." | ".$tipoOperacion);

        $componentesArray = [];
        foreach ($componentes as $key => $value) {
            $componentesArray[] = [
                'tipo_equipo'           => ($value->tipo_equipo == null or 
                                            $value->tipo_equipo == "")?
                                            'TELEFONO':$value->tipo_equipo,
                'codigo_equipo'         => ($value->codigo_equipo == null or 
                                            $value->codigo_equipo == "")?
                                            '31':$value->codigo_equipo,
                'descripcion_equipo'    => ($value->descripcion_equipo == null or 
                                            $value->descripcion_equipo == "")?
                                            'TELEFONO FORMA':$value->descripcion_equipo,
                'cod_material'          => ($value->cod_material == null or 
                                            $value->cod_material == "")?
                                            '01506553':$value->cod_material,
                'cantidad_material'     => ($value->cantidad_material == null or 
                                            $value->cantidad_material == "")?
                                            '1':$value->cantidad_material,
                'unidad_medida'         => $value->unidad_medida,
                'nro_serie'             => ($value->nro_serie == null or 
                                            $value->nro_serie == "")?
                                            '1234567890':$value->nro_serie,
                'codigo_barras'         => $value->codigo_barras,
                'tipo_transaccion'      => ($value->tipo_transaccion == null or 
                                            $value->tipo_transaccion == "")?
                                            '1':$value->tipo_transaccion,
                'marca_equipo'          => $value->marca_equipo,
                'modelo_equipo'         => $value->modelo_equipo,
                'mantenimiento_datos'   => $value->mantenimiento_datos,
                'numero_cabina'         => $value->numero_cabina,
                'dato1'                 => $value->dato1,
                'dato2'                 => $value->dato2,
                'dato3'                 => $value->dato3,
                'dato4'                 => $value->dato4,
                'dato5'                 => $value->dato5,
            ];
        }

        $elementos = [
            'solicitud_tecnica_id'      => $solicitud[0]->solicitud_tecnica_id,
            'tipo_operacion'            => $tipoOperacion,
            'num_solicitud_tecnica'     => $solicitud[0]->num_solicitud_tecnica, // id_solicitud_tecnica
            'id_solicitud_tecnica'      => $solicitud[0]->id_solicitud_tecnica, // id_solicitud_tecnica
            'fecha_programacion'        => $fechaProgramacion,//$solicitud[0]->fecha_programacion, // fecha_programacion
            'contrata'                  => $solicitud[0]->contrata, // contrata
            'tecnico1'                  => $tecnico1,
            'tecnico2'                  => $solicitud[0]->tecnico2,
            'fecha_emision_boleta'      => $fechaEmision,//$solicitud[0]->fecha_emision_boleta,
            'nombre_persona_recibe'     => $solicitud[0]->nombre_persona_recibe, // nombres_contacto
            'numero_doc_identidad'      => $solicitud[0]->numero_doc_identidad, // numero_doc
            'observacion'               => $solicitud[0]->observacion, // desobs
            'parentesco'                => $solicitud[0]->parentesco,
            'indicador_fraude'          => $solicitud[0]->indicador_fraude,
            'anexos'                    => $solicitud[0]->anexos,
            'fecha_ejecucion'           => $fechaEjecucion,//$solicitud[0]->fecha_ejecucion,
            'hora_inicio'               => $horaInicio,//$solicitud[0]->hora_inicio,
            'hora_fin'                  => $horaFin,
            'desplazamiento'            => $fechaDesplazamiento,//$solicitud[0]->desplazamiento,
            'escenario'                 => $solicitud[0]->escenario, // escenario_gestel
            'cabina'                    => $solicitud[0]->cabina,
            'ambiente'                  => $solicitud[0]->ambiente,
            'cod_mot'                   => $solicitud[0]->cod_mot,
            'des_obs'                   => $solicitud[0]->des_obs,
            'fecha_hora_devolucion'     => $fechaDevolucion,//$solicitud[0]->fecha_hora_devolucion, // fecha_hora_devolucion
            'dato1'                     => $solicitud[0]->dato1, // valor1
            'dato2'                     => $solicitud[0]->dato2, // valor2
            'dato3'                     => $solicitud[0]->dato3, // valor3
            'dato4'                     => $solicitud[0]->dato4, // valor4
            'dato5'                     => $solicitud[0]->dato5, // valor5
            'DatosMateriales'           => $componentesArray
        ];
        // dd(json_encode($elementos));

        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
        $objStapi = new STGestelApi("GESTEL Legado");
        $response = $objStapi->cierreProvision($elementos);
        //print_r($response);
    }
    public function postListar()
    {
        $currentPage = Input::get('currentPage');
        $perPage = Input::get('perPage');
        $filtro = Input::get('filtro');
        $filterSelected = Input::get('filterSelected');
        

        $queryTotal = DB::table('contacto_cms');
        $queryDatos = DB::table('contacto_cms');
        if ($filtro) {
            $datos = $queryDatos->whereRaw(''.$filterSelected.' like "%'.$filtro.'%"')->skip($currentPage)->take($perPage)->get();
            $count = $queryTotal->whereRaw(''.$filterSelected.' like "%'.$filtro.'%"')->count();
        } else {
            $datos = $queryDatos->skip($currentPage)->take($perPage)->get();
            $count = $queryTotal->count();
        }

        $response["total"] = $count;
        $response["datos"] = $datos;

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $response
            )
        );
    }
}