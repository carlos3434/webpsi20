<?php

use ComandoLiquidacion as Comando;

class ProgramadorMovimientoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function postListar(){
		$datos = Comando::with('dias')->orderBy('created_at', 'desc')->get();

		return Response::json([
			'rst' => 1,
			'datos' => $datos,
			'msj' => 'Comando creado con éxito'
			]);
	}

	public function postCambiarestado(){
		$id = Input::get('id');
		$estado = Input::get('estado');
		$comando = Comando::find($id);
		$comando->estado  = $estado;
		$comando->save();
		return Response::json([
			'rst' => 1,
			'msj' => 'Se actualizo con éxito'
			]);
	}

	public function postGuardarcomando(){
		
		$nombre = Input::get('nombre', '');
		$tipo_fecha = Input::get('tipo_fecha', 1);
		$estado = Input::get('estado', 1);
		$estado_programacion = Input::get('estado_programacion', 0);
		$dias_hora_inicio = Input::get('dias_hora_inicio', null);
		$dias_hora_fin = Input::get('dias_hora_fin', null);
		$dias = Input::get('dias', null);
		$quiebres = Input::get('quiebre', null);
		$atenciones = Input::get('atencion', null);
		$fechas_comando = Input::get('fechas_comando', null);
		$fechas_busqueda = Input::get('fechas_busqueda', null);
		$fechas_comando_arr = explode(' - ', $fechas_comando);
		$fechas_busqueda_arr = explode(' - ', $fechas_busqueda);
		$motivo = Input::get('motivo', null);
		$submotivo = Input::get('submotivo', null);

		$estado_programacion_val = 0;
		if ($estado_programacion !== 0) {
			$estado_programacion_val = 1;
		}

		$comando = new Comando();
		$comando->nombre = $nombre;
		$comando->tipo_fecha = $tipo_fecha;
		$comando->estado = $estado;
		$comando->estado_programacion = $estado_programacion_val;
		$comando->dias_hora_inicio = $dias_hora_inicio;
		$comando->dias_hora_fin = $dias_hora_fin;
		$comando->fecha_inicio_comando = $fechas_comando_arr[0];
		$comando->fecha_final_comando = $fechas_comando_arr[1];
		$comando->fecha_inicio_busqueda = $fechas_busqueda_arr[0];
		$comando->fecha_final_busqueda = $fechas_busqueda_arr[1];
			
		$comando->save();

		if(count($dias) > 0){
			foreach ($dias as $key => $dia) {
				$comando_dia = new ComandoDia();
				$comando_dia->comando_id = $comando->id;
				$comando_dia->dia = $dia;
				$comando_dia->save();
			}
		}

		if(count($quiebres) > 0){
			foreach ($quiebres as $key => $quiebre) {
				$comando_quiebre = new ComandoQuiebre();
				$comando_quiebre->comando_id = $comando->id;
				$comando_quiebre->quiebre_id = $quiebre;
				if ($estado_programacion == 0) {
					$comando_quiebre->orden = $key;
				}
				$comando_quiebre->save();
			}
		}

		if(count($atenciones) > 0){
			foreach ($atenciones as $key => $atencion) {
				$comando_atencion = new ComandoPretemporalRrss();
				$comando_atencion->comando_id = $comando->id;
				$comando_atencion->pretemporal_rrss_id = $atencion;
				$comando_atencion->save();
			}
		}
		
		return Response::json([
			'rst'=> 1,
			'msj' => 'Comando creado con éxito'
		]);
	}

	public function postObtenerdatos(){
		$id = Input::get('id');
		$comando = Comando::with('dias', 'quiebres', 'pretemporales_rrss')->find($id);
		return Response::json([
			'rst'=> 1,
			'msj' => 'Datos del comando retornados con éxito',
			'data' => $comando
		]);
	}
	

	public function postEditarcomando(){
		$id = Input::get('id', '');
		$nombre = Input::get('nombre', '');
		$tipo_fecha = Input::get('tipo_fecha', 1);
		$estado = Input::get('estado', 1);
		$estado_programacion = Input::get('estado_programacion', 0);
		$dias_hora_inicio = Input::get('dias_hora_inicio', null);
		$dias_hora_fin = Input::get('dias_hora_fin', null);
		$dias = Input::get('dias', null);
		$quiebres = Input::get('quiebre', null);
		$atenciones = Input::get('atencion', null);
		$fechas_comando = Input::get('fechas_comando', null);
		$fechas_busqueda = Input::get('fechas_busqueda', null);
		$fechas_comando_arr = explode(' - ', $fechas_comando);
		$fechas_busqueda_arr = explode(' - ', $fechas_busqueda);
		dd($fechas_comando_arr);

		if ($id != '') {
			
			$comando = Comando::find($id);
			$comando->nombre = $nombre;
			$comando->tipo_fecha = $tipo_fecha;
			$comando->estado = $estado;
			$comando->estado_programacion = $estado_programacion;
			$comando->dias_hora_inicio = $dias_hora_inicio;
			$comando->dias_hora_fin = $dias_hora_fin;
			$comando->fecha_inicio_comando = $fechas_comando_arr[0];
			$comando->fecha_final_comando = $fechas_comando_arr[1];
			$comando->fecha_inicio_busqueda = $fechas_busqueda_arr[0];
			$comando->fecha_final_busqueda = $fechas_busqueda_arr[1];
			$comando->save();

			$comando_dias = ComandoDia::where('comando_id', '=', $id)->get();
			$comando_dias_id_creados = [];
			$comandos_dias_index = [];

			if ( count($dias) > 0 ){
				if ( count($comando_dias) > 0 ){
					foreach ($comando_dias as $key => $comando_dia) {
						$comando_dias_id_creados[] = $comando_dia->dia;
						$comandos_dias_index[$comando_dia->dia] = $key;
					}
				}

				$crear_dias = array_diff($dias, $comando_dias_id_creados);
				$eliminar_dias = array_diff($comando_dias_id_creados, $dias);

				if ( count($crear_dias) > 0 ) {
					foreach ($crear_dias as $crear_dia) {
						$comando_dia = new ComandoDia();
						$comando_dia->comando_id = $id;
						$comando_dia->dia = $crear_dia;
						$comando_dia->save();
					}
				}
				else {
					if ( count($eliminar_dias) > 0 ) {
						foreach ($eliminar_dias as $eliminar_dia) {
							$comando_dias_key = $comandos_dias_index[$eliminar_dia];
							$_comando_dia = $comando_dias[$comando_dias_key];
							$_comando_dia->delete();
						}
					}
				}
			}
			else {
				foreach ($comando_dias as $key => $comando_dia) {
					$comando_dia->delete();
				}
			}

			$comando_quiebres = ComandoQuiebre::where('comando_id', '=', $id)->get();
			$comando_quiebres_id_creados = [];
			$comandos_quiebres_index = [];

			if ( count($quiebres) > 0 ) {
				if ( count($comando_quiebres) > 0 ) {
					foreach ($comando_quiebres as $key => $comando_quiebre) {
						$comando_quiebres_id_creados[] = $comando_quiebre->quiebre_id;
						$comandos_quiebres_index[$comando_quiebre->quiebre_id] = $key;
					}
				}

				$crear_quiebres = array_diff($quiebres, $comando_quiebres_id_creados);
				$eliminar_quiebres = array_diff($comando_quiebres_id_creados, $quiebres);

				if ( count($crear_quiebres) > 0 ){
					foreach ($crear_quiebres as $crear_quiebre) {
						$comando_quiebre = new ComandoQuiebre();
						$comando_quiebre->comando_id = $id;
						$comando_quiebre->quiebre_id = $crear_quiebre;
						$comando_quiebre->save();
					}
				}
				else {
					if ( count($eliminar_quiebres) > 0 ) {
						foreach ($eliminar_quiebres as $eliminar_quiebre) {
							$comando_quiebre_key = $comandos_quiebres_index[$eliminar_quiebre];
							$_comando_quiebre = $comando_quiebres[$comando_quiebre_key];
							$_comando_quiebre->delete();
						}
					}
				}

			}
			else {
				foreach ($comando_quiebres as $key => $comando_quiebre) {
					$comando_quiebre->delete();
				}
			}

			$comando_atenciones = ComandoPretemporalRrss::where('comando_id', '=', $id)->get();
			$comando_atenciones_id_creados = [];
			$comandos_atenciones_index = [];

			if( count($atenciones) > 0 ){
				if ( count($comando_atenciones) > 0 ) {
					foreach ($comando_atenciones as $key => $comando_atencion) {
						$comando_atenciones_id_creados[] = $comando_atencion->pretemporal_rrss_id;
						$comandos_atenciones_index[$comando_atencion->pretemporal_rrss_id] = $key;
					}
				}


				$crear_atenciones = array_diff($atenciones, $comando_atenciones_id_creados);
				$eliminar_atenciones = array_diff($comando_atenciones_id_creados, $atenciones);

				if( count($crear_atenciones) > 0) {
					foreach ($crear_atenciones as $key => $crear_atencion) {
						$comando_atencion = new ComandoPretemporalRrss();
						$comando_atencion->comando_id = $id;
						$comando_atencion->pretemporal_rrss_id = $crear_atencion;
						$comando_atencion->save();
					}
				}
				else {
					if ( count($eliminar_atenciones) > 0 ){
						foreach ($eliminar_atenciones as $eliminar_atencion) {
							$comando_atencion_key = $comandos_atenciones_index[$eliminar_atencion];
							$_comando_atencion = $comando_atenciones[$comando_atencion_key];
							$_comando_atencion->delete();
						}
					}
				}
			}
			else {
				foreach ($comando_atenciones as $key => $comando_atencion) {
					$comando_atencion->delete();
				}
			}


		}
		

		return \Response::json([
			'rst'=> 1,
			'msj' => 'Comando actualizado con éxito'
		]);
	}

}
