<?php
class TablaDetalleController extends \BaseController
{
    public function postCampos()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $r = TablaDetalle::Campos();
            $rs =   array(
                    'rst' => 1, 
                    'datos' => $r
                    );
            return Response::json($rs);

        }
    }

    public function postCamposdinamicos()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $r = TablaDetalle::CamposDinamicos();
            $rs =   array(
                    'rst' => 1, 
                    'datos' => $r
                    );
            return Response::json($rs);

        }
    }

    public function postCamposdetalle()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $r = TablaDetalle::CamposDetalle();
            $rs =   array(
                    'rst' => 1, 
                    'datos' => $r["datos"],
                    'coords' => $r["coords"]
                    );
            return Response::json($rs);

        }
    }

    public function getCoordstap()
    {
        if(Request::ajax()){
            $r = TablaDetalle::CoordsTap();
            $rs = array(
                'rst' => 1,
                'coords' => $r
            );

            return Response::json($rs);
        }
    }


}
