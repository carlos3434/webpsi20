<?php

class RedisCapacidadToaController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    public function getSelects()
    {
        $tipos_actividades = DB::table('actividades_tipos')
        ->select(
            'label', 
            'nombre' 
        )->where('estado', 1)->get();

        $quiebres = DB::table('quiebres')
        ->select(
            'apocope', 
            'nombre' 
        )->where('estado', 1)->get();

        $nodos = DB::table('geo_nodopunto')
        ->select(
            'nodo' 
        )->groupBy('nodo')->get();

        $trobas = DB::table('geo_trobapunto')
        ->select(
            'troba' 
        )->groupBy('troba')->get();

       return Response::json(["tipos_actividades" => $tipos_actividades, "quiebres" => $quiebres, "nodos" => $nodos, "trobas" => $trobas]);
    }

    public function getSearchcache(){
        $request = Input::all();

        $search = $request["search"];

        $data = Redis::command('KEYS', [$search]);

        $response = [];

        foreach ($data as $row) {
            $columns = explode("|", $row);

            $nodotroba = explode("_", $columns[4]);

            $response[] = [
                $columns[0],
                $columns[1],
                $columns[2],
                $columns[3],
                $nodotroba[0],
                $nodotroba[1],
                "<td><button key='".$row."' class='btn btn-danger btn-sm delete-cache'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td>"
            ];            
        }

        return Response::json(["data" => $response]);
    }

    public function getDeletecache(){
        $request = Input::all();
        $key = $request["key"];

        Redis::del($key);

        return Response::json("ok");
    }
}