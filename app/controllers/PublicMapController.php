<?php

class PublicMapController extends BaseController
{

    protected $_visorgps;

    public function __construct(VisorgpsController $visorgpsController)
    {
        $this->_visorgps = $visorgpsController;
    }

    public function getRutatecnico($carnet, $condicion = 'hoy')
    {

        try {
            $publicmap = new Publicmap();

            //Actuaciones gestionadas
            $hoy = date("Y-m-d");
            if ($condicion == 'hoy')
                $filtroFe = array("condicion" => "=", "valor" => $hoy);
            elseif ($condicion == 'pasados')
                $filtroFe = array("condicion" => "<", "valor" => $hoy);
            elseif ($condicion == 'futuros')
                $filtroFe = array("condicion" => ">", "valor" => $hoy);
            elseif ($condicion == 'todos')
                $filtroFe = array("condicion" => "<>", "valor" => "0000-00-00");

            $lista["data"] = $publicmap->getRutaTecnico($carnet, '', $filtroFe);

            $agenda["data"] = array();
            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                return View::make(
                    'public.ordenrutatecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getOrdentecnico($carnet, $gestionId)
    {

        try {
            $publicmap = new Publicmap();

            //Actuaciones gestionadas
            $lista["data"] = $publicmap->getRutaTecnico($carnet, $gestionId);

            $agenda["data"] = array();
            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);

                return View::make(
                    'public.ordenrutatecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getOrdentecnicoofsc($codactu)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no gestionado</h3>';

            $x = $lista["data"][0]->x;
            $y = $lista["data"][0]->y;
            $cant = 5;
           
            $temp1 = str_replace("-", "/", $lista["data"][0]->tipoactu);
            $temp2 = str_replace("_", "/", $temp1);
            $tipoactu = explode("/",$temp2);

            if(array_search("adsl", $tipoactu)){ 
                $tabla = 'psi.geo_armariopoligono';
            }
            if(array_search("bas", $tipoactu)) {
                $tabla = 'psi.geo_armariopoligono';
            }
            if(array_search("catv", $tipoactu)) {
                $tabla = 'psi.geo_tap';
            }

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                //obtengo los fftt
                $agenda["fftt"] = array();
                $agenda["fftt"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                $array = json_decode(json_encode($agenda), true);

                return View::make(
                    'public.ordenrutaofsc', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }
//primera version _old
    public function getOrdentapofsc_old($codactu)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no gestionado</h3>';

            $x = $lista["data"][0]->x;
            $y = $lista["data"][0]->y;
            $cant = 1;
            $tabla = 'psi.geo_tap';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                //obtengo los fftt
                $agenda2["data2"] = array();
                $agenda2["data2"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                $array2 = json_decode(json_encode($agenda2), true);

                return View::make(
                    'public.ordenrutaofsc', $array2, $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

     public function getOrdenterminalofsc($codactu)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no gestionado</h3>';

            $x = $lista["data"][0]->x;
            $y = $lista["data"][0]->y;
            $cant = 1;
            $tabla = 'psi.geo_armariopoligono';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                //obtengo los fftt
                $agenda2["data2"] = array();
                $agenda2["data2"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                $array2 = json_decode(json_encode($agenda2), true);

                return View::make(
                    'public.ordenrutaofsc', $array2, $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function showAddress($codigo)
    {
        $ubicacion = DB::table('ubicaciones')
                ->where('codigo', '=', $codigo)
                ->first(
                    array(
                        'nombre',
                        'descripcion',
                        'contacto',
                        'x',
                        'y',
                        'usuario_id',
                        'imagen'
                    )
                );

        //Si no se encuentran resultados
        if (is_null($ubicacion)) {
            return View::make('ubicaciones.noaddress');
        } else {
            return View::make(
                'ubicaciones.address', array('ubicacion' => $ubicacion)
            );
        }
    }

    public function getOrdentapofsc($codactu = FALSE)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no existe</h3>';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                return View::make(
                    'public.buscarcomponentestecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }
}
