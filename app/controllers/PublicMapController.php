<?php
use Legados\models\SolicitudTecnicaCms;
use Ofsc\Inbound;
class PublicMapController extends BaseController
{

    protected $_visorgps;

    public function __construct(VisorgpsController $visorgpsController)
    {
        $this->_visorgps = $visorgpsController;
    }

    public function getRutatecnico($carnet, $condicion = 'hoy')
    {

        try {
            $publicmap = new Publicmap();

            //Actuaciones gestionadas
            $hoy = date("Y-m-d");
            if ($condicion == 'hoy')
                $filtroFe = array("condicion" => "=", "valor" => $hoy);
            elseif ($condicion == 'pasados')
                $filtroFe = array("condicion" => "<", "valor" => $hoy);
            elseif ($condicion == 'futuros')
                $filtroFe = array("condicion" => ">", "valor" => $hoy);
            elseif ($condicion == 'todos')
                $filtroFe = array("condicion" => "<>", "valor" => "0000-00-00");

            $lista["data"] = $publicmap->getRutaTecnico($carnet, '', $filtroFe);

            $agenda["data"] = array();
            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                return View::make(
                    'public.ordenrutatecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getOrdentecnico($carnet, $gestionId)
    {

        try {
            $publicmap = new Publicmap();

            //Actuaciones gestionadas
            $lista["data"] = $publicmap->getRutaTecnico($carnet, $gestionId);

            $agenda["data"] = array();
            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);

                return View::make(
                    'public.ordenrutatecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getOrdentecnicoofsc($codactu,$solicitud=null)
    {
        return View::make('public.ordenrutaofsc')
        ->with(['codactu'=>$codactu,'solicitud'=>$solicitud]);
    }

    public function getOrdenvalidar($codactu,$solicitud=null)
    {
        return View::make('public.ordenvalidar')
        ->with(['codactu'=>$codactu,'solicitud'=>$solicitud]);
    }

    public function getOrdenactualizar($codactu,$solicitud=null)
    {
        return View::make('public.ordenactualizar')
        ->with(['codactu'=>$codactu,'solicitud'=>$solicitud]);
    }

    //primera version _old
    public function getOrdentapofsc_old($codactu)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no gestionado</h3>';

            $x = $lista["data"][0]->x;
            $y = $lista["data"][0]->y;
            $cant = 1;
            $tabla = 'psi.geo_tap';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                //obtengo los fftt
                $agenda2["data2"] = array();
                $agenda2["data2"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                $array2 = json_decode(json_encode($agenda2), true);

                return View::make(
                    'public.ordenrutaofsc', $array2, $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

     public function getOrdenterminalofsc($codactu)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no gestionado</h3>';

            $x = $lista["data"][0]->x;
            $y = $lista["data"][0]->y;
            $cant = 1;
            $tabla = 'psi.geo_armariopoligono';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                //obtengo los fftt
                $agenda2["data2"] = array();
                $agenda2["data2"] = Geofftt::getFFTT($x, $y, $cant, $tabla);
                $array2 = json_decode(json_encode($agenda2), true);

                return View::make(
                    'public.ordenrutaofsc', $array2, $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function showAddress($codigo)
    {
        $ubicacion = DB::table('ubicaciones')
                ->where('codigo', '=', $codigo)
                ->first(
                    array(
                        'nombre',
                        'descripcion',
                        'contacto',
                        'x',
                        'y',
                        'usuario_id',
                        'imagen'
                    )
                );

        //Si no se encuentran resultados
        if (is_null($ubicacion)) {
            return View::make('ubicaciones.noaddress');
        } else {
            return View::make(
                'ubicaciones.address', array('ubicacion' => $ubicacion)
            );
        }
    }

    public function getOrdentapofsc($codactu = FALSE)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaOFSC($codactu);
    
            if(empty($lista["data"])) return '<h3>Codigo no existe</h3>';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                return View::make(
                    'public.buscarcomponentestecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function getOrdentaplego($codactu, $solicitud)
    {
        try {
            $publicmap = new Publicmap();

            $lista["data"] = $publicmap->getRutaLegado($codactu, $solicitud);
    
            if(empty($lista["data"])) return '<h3>Codigo no existe</h3>';

            $agenda["data"] = array();

            foreach ($lista["data"] as $key => $val) {
                $buscar["data"] = array();
                //Ordenes sin XY
                if (trim($val->x) == '' or trim($val->y) == '') {
                    $buscar["data"][] = $val;
                    $orden = $this->_visorgps->getActuCoord($buscar);
                    $agenda["data"][] = $orden[0];
                } else {
                    //Ordenes con XY
                    $agenda["data"][] = $val;
                }
            }

            if (count($agenda["data"]) > 0) {
                $array = json_decode(json_encode($agenda), true);
                return View::make(
                    'public.buscarcomponentestecnico', $array
                );
            } else {
                return "<h3>Sin resultados</h3>";
            }
        } catch (Exception $exc) {
            echo "<h2>Error: No se encontraron datos</h2>";
        }
    }

    public function postOrdenstfftt()
    {
        try {
            $codactu=Input::get('codactu');
            $solicitud=Input::get('solicitud');
            $publicmap = new Publicmap();
            $rst=1;
            $msj="";
            $st = $publicmap->getSolicitudTecnica($codactu, $solicitud);
            if($st==null){
                $rst=2;
                $msj="No se encontraron datos";
            }
            $array = json_decode(json_encode($st), true);
            return Response::json(
                        array(
                            'rst' => $rst, 
                            'datos' => $array,
                            'msj' => $msj
                        )
                    );

        } catch (Exception $exc) {
            return Response::json(
                        array(
                            'rst' => 2, 
                            'datos' => [],
                            'msj' => 'No se encontraron datos'
                        )
                    );
        } 
    }

    public function postObtenercoordenadas()
    {
        $publicmap = new Publicmap();
        return $publicmap->postObtenerCoord();
    }
    
    public function postGuardarcoordenadas()
    {
        $publicmap = new Publicmap();
        return $publicmap->postGuardarCoord();
    }

    public function postValidarcoordenadas()
    {
        $publicmap = new Publicmap();
        return $publicmap->postValidarcoord();
    }
}
