<?php

class UploadLogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /uploadlog
	 *
	 * @return Response
	 */
	public function postCargar(){
		$query =  UploadLog::from('uploads_log as ul')
		  		->select(
		  			'ul.id',
		  			'ul.file_accept',
		  			'ul.file_refused',
		  			'ul.number_acept',
		  			'ul.number_refused',
		  			'ul.number_exist',
		  			'ul.created_at',
		  			DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) as responsable')
		  		)
				->leftjoin('usuarios as u','ul.usuario_created_at','=','u.id')
				 ->where(function($query){
                       	if(Input::get('fecha')){
                        	$fechaRegistro = Input::get('fecha');
                    		if (count($fechaRegistro) != 0) {
						        $fecha = explode(" - ", $fechaRegistro);
						        if (count($fecha) > 0) {
						            $startDate = date('Y-m-d  00:00:00', strtotime($fecha[0]));
						            $endDate = date('Y-m-d  23:59:59', strtotime($fecha[1]));
						            $query->whereBetween(
						                        'ul.created_at',
						                        array($fecha[0]. " 00:00:00",
						                                $fecha[1]. " 23:59:59")
						            );
						        }
						    }
                        }                        					 								
                        $query->where('ul.estado',1); 
                        if(Input::get('tipo') == 2){
                        	$query->where('ul.type',Input::get('tipo')); 
                        }else{
                        	$query->where('ul.type',1); 
                        }                       	
				 });
				$query=$query->orderBy(Input::get('column'),Input::get('dir'))
				->searchPaginateAndOrder();
        return $query; 	
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /uploadlog/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /uploadlog
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /uploadlog/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /uploadlog/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /uploadlog/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /uploadlog/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}