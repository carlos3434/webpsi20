<?php

class CriteriosController extends BaseController
{

    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
           
                $nombre='nombre_trama';
                $proveniencia=Input::get('proveniencia');
                if($proveniencia==1) $nombre='nombre_gestion';
                if($proveniencia==3) $nombre='nombre_gestel_prov';
                if($proveniencia==4) $nombre='nombre_gestel_ave';
                
                $result = Criterios::where('estado',1)
                          ->where($nombre,'<>','')
                          ->where('controlador','<>','');

                if(Input::get('masiva')==1) {
                  $result->where('estado_masiva',1);
                }
                  $result->orderBy('nombre','asc');
                $result=$result->get();
                
            
            return Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
