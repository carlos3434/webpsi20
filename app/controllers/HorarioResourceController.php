<?php
class HorarioResourceController extends \BaseController
{
    public function postList()
    {
        $datos = HorarioResource::select(
                    'd.id as dia_id',
                    'd.nombre as dia',
                    'horario_resource.id',
                    'horario_resource.rango_hora',
                    'horario_resource.hora_inicio',
                    'horario_resource.hora_fin',
                    'horario_resource.estado'
                )
                ->leftJoin('dias as d', 'd.id', '=', 'dia_id')
                ->orderBy('d.id')
                ->get();

        return [
            'datos' => $datos
        ];
    }

    /*public function postInsert()
    {
        $motivoOfsc = new HorarioResource();
        $motivoOfsc->dia_id = Input::get('dia_id');
        $motivoOfsc->rango_hora = Input::get('rango_hora');
        $motivoOfsc->hora_inicio = Input::get('hora_inicio');
        $motivoOfsc->hora_fin = Input::get('hora_fin');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }*/

    public function postUpdate()
    {
        $motivoOfsc = HorarioResource::find(Input::get('id'));
        $motivoOfsc->dia_id = Input::get('dia_id');
        $motivoOfsc->rango_hora = Input::get('rango_hora');
        $motivoOfsc->hora_inicio = Input::get('hora_inicio');
        $motivoOfsc->hora_fin = Input::get('hora_fin');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }
}