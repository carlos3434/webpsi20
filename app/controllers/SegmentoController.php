<?php

class SegmentoController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
            if(Input::get('parametros')){
                $segmento=DB::table('coc.segmento')
                                ->select(
                                    DB::RAW('cod_segmento as id'),
                                    'nom_segmento as nombre'
                                    )
                                ->where ('nom_segmento','<>','')
                                ->get();
            } else {
                $segmento = DB::table('segmentos')
                            ->select('nombre as id', 'nombre', 'id as codigo')
                            ->orderBy('nombre')
                            ->get();
            }

        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$segmento
                )
            );
        }
    }
 


}//fin class
