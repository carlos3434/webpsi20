<?php

class SegmentoController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
        
                $segmento = DB::table('segmentos')
                            ->select('nombre as id', 'nombre', 'id as codigo')
                            ->orderBy('nombre')
                            ->get();
        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$segmento
                )
            );
        }
    }
 


}//fin class
