<?php

class GeoReferenciaController extends BaseController
{

    public function __construct()
    {
        /*$this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );*/
    }

    public function postBuscar()
    {
        $calle = strtoupper(trim(addslashes($_REQUEST["calle"])));
        //$calle = explode(" ",$calle);

        $numero = trim(addslashes($_REQUEST["numero"]));
        $distr = $_REQUEST["distrito"];
        if (!is_array($_REQUEST["distrito"])) {
            $distr = array($_REQUEST["distrito"]);
        }
        foreach ($distr as $dist) {
            $distrito = trim(addslashes($dist));

            //Numeracion izquierda o derecha
            $izq = true;
            if ($_REQUEST["numero"] % 2 === 0) {
                $izq = false;
            }

            $data[] = $this->buscarDireccion(
                $distrito, $calle, $numero, $izq
            );
        }
        foreach ($data as $k => $v) {
            echo json_encode($v);
        }
    }

    private function buscarDireccion($distrito, $calle, $numero, $izq, $id = null)
    {
        $result = array();

        $dep = substr($distrito, 0, 2);
        $pro = substr($distrito, 2, 2);
        $dis = substr($distrito, 4, 2);

        try {
            //Iniciar transaccion
            //DB::beginTransaction();

            $reporte = array();
            $sql = "SELECT
                        COD_VIA, COD_TRAM, PKY_TRAMO,
                        NUM_CUADRA, NOM_VIA_TR,
                        COD_DPTO, COD_PROV, COD_DIST,
                        NUM_IZQ_IN, NUM_IZQ_FI, NUM_DER_IN, NUM_DER_FI,
                        XA, YA, XB, YB, XC, YC, XD, YD
                    FROM
                        geo_tramos
                    WHERE
                        NOM_VIA_TR LIKE '" . $calle . "%' ";

            if ($dep != "00" AND $dep != "") {
                $sql .= " AND COD_DPTO='$dep'";
            }
            if ($pro != "00" AND $pro != "") {
                $sql .= " AND COD_PROV='$pro'";
            }
            if ($dis != "00" AND $dis != "") {
                $sql .= " AND COD_DIST='$dis'";
            }

            //Numeracion izquierda o derecha
            if ($numero != "") {
                if ($izq) {
                    $sql .= " AND $numero BETWEEN NUM_IZQ_IN AND NUM_IZQ_FI";
                } else {
                    $sql .= " AND $numero BETWEEN NUM_DER_IN AND NUM_DER_FI";
                }
            }

            //Orden por coordenadas
            $sql .= " ORDER BY XA";
            //Datos para la busqueda
            $bind = DB::select($sql);


            foreach ($bind as $k => $v) {
                foreach (get_object_vars($v) as $key => $val) {
                    $data[$key] = utf8_encode($val);
                    $data["numero"] = $numero;
                    $data["SEARCH"] = $calle . " " . $numero;
                }
                //echo $data["XB"] ."+ ( (- ".$data["NUM_IZQ_FI"]." + ".
                //$numero.") * (- ".$data["XB"]." + ".$data["XA"].
                //") / (- ".$data["NUM_IZQ_FI"]." + ".$data["NUM_DER_IN"].") )";
                if ($data["NUM_IZQ_FI"] == 0 && $data["NUM_DER_IN"] == 0) {
                    $xDir = $data["XA"];
                    $yDir = $data["YA"];
                } else {
                    $xDir = $data["XB"] +
                        (
                            (-$data["NUM_IZQ_FI"] + $numero) *
                            (-$data["XB"] + $data["XA"]) /
                            (-$data["NUM_IZQ_FI"] + $data["NUM_DER_IN"])
                        );
                    $yDir = $data["YB"] +
                        (
                            (-$data["NUM_IZQ_FI"] + $numero) *
                            (-$data["YB"] + $data["YA"]) /
                            (-$data["NUM_IZQ_FI"] + $data["NUM_DER_IN"])
                        );
                }
                $data["XY"] = $yDir . "," . $xDir;
                $data["id"] = $id;
                $reporte[] = $data;
            }

            //DB::commit();
            /*if (!isset($data)) {
                return $reporte;
            }*/
            return $reporte;
        } catch (PDOException $error) {
            //DB::rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

    public function postBuscarmasiva()
    {
        /*error_reporting(E_ALL);
        ini_set("display_errors", 1);*/
        ini_set("memory_limit", "2048M");
        ini_set("max_execution_time", 0);
        DB::beginTransaction();
        //$file = fopen($_FILES["archivo"]["tmp_name"], "r");
        $notFound = array();
        $result = array();
        $contador = 0;

        $sql = "  SELECT  c.id,direccion,departamento,provincia,distrito
                ,u.nombre,u.coddep,u2.codpro,u3.coddis
                FROM planta_catv.clientes_m1 c
                LEFT JOIN ubigeos u 
                    ON (u.nombre=departamento 
                        AND u.codpro='00' 
                        AND u.coddis='00'
                        )
                LEFT JOIN ubigeos u2 
                    ON (u2.nombre=provincia 
                        AND u2.coddep=u.coddep 
                        AND u2.codpro!='00' 
                        AND u2.coddis='00'
                        )
                LEFT JOIN ubigeos u3 
                    ON (u3.nombre=distrito 
                        AND u3.coddep=u.coddep 
                        AND u3.codpro=u2.codpro 
                        AND u2.coddep!='00' 
                        AND u2.codpro!='00'
                        )
                WHERE c.estado=1
                limit 0, 25000";
        //echo $sql."<br>";
        $r = DB::select($sql);
        $first = reset($r);
        $last = end($r);
        //echo count($r);
        foreach ($r as $key => $value) {
            $address = $this->addressToArray($value->direccion);
            $calle = strtoupper(trim(addslashes($address["calle"])));
            //$calle = explode(" ",$calle);
            $numero = trim(addslashes($address["numero"]));

            $distrito = trim($value->coddep);
            $distrito .= trim($value->codpro);
            $distrito .= trim($value->coddis);

            $contador++;
            /*
            echo $value->direccion."<br>";
            echo $calle." | ".$numero."<br>";
            */

            //Numeracion izquierda o derecha
            $izq = true;
            if ($address["numero"] % 2 === 0) {
                $izq = false;
            }
            $data = array();

            if ($calle != '') {
                $data = $this->buscarDireccion(
                    $distrito, $calle, $numero, $izq, $value->id
                );
            }

            if (empty($data)) {
                $notFound[] = array($value->direccion, $numero, $value->id);
            } else {
                foreach ($data as $rutas) {
                    $result[] = $rutas;
                }
            }
        }

        $sqlInsert = "INSERT INTO planta_catv.backup_clientes
                      (
                        id_ref,
                        codcliente,
                        telefono,
                        paterno,
                        materno,
                        nombres,
                        direccion,
                        departamento,
                        provincia,
                        distrito,
                        nodotroba
                      )
                      SELECT id,
                            codcliente,
                            telefono,
                            paterno,
                            materno,
                            nombres,
                            direccion,
                            departamento,
                            provincia,
                            distrito,
                            nodotroba
                      FROM planta_catv.clientes_m1
                      WHERE estado = 1
                      ";

        $r = DB::insert($sqlInsert);

        $sqlDelete = "DELETE FROM planta_catv.clientes_m1
                      WHERE id IN (
                        SELECT id_ref
                        FROM planta_catv.backup_clientes
                      )";
        $r = DB::delete($sqlDelete);

        $sqlupdate = "UPDATE
                     planta_catv.clientes_m1 c
                    INNER JOIN (
                        SELECT id
                        FROM planta_catv.clientes_m1
                        WHERE estado=1
                        LIMIT 0,25000
                    ) f ON f.id=c.id
                    SET c.estado=0
                    WHERE c.estado=1";
        $r = DB::update($sqlupdate);

        DB::commit();
        /*
                while (!feof($file)) {
                    //set_time_limit(30);
                    $datos= fgets($file);
                    $address = $this->addressToArray($datos);
                    $calle = strtoupper(trim(addslashes($address["calle"])));
                    //$calle = explode(" ",$calle);
                    $numero = trim(addslashes($address["numero"]));
                    $distr = "150100";

                    $distrito = trim(addslashes($distr));
                    $contador++;

                    //Numeracion izquierda o derecha
                    $izq = true;
                    if ($address["numero"] % 2 === 0) {
                        $izq = false;
                    }
                    $data[] = $this->buscarDireccion(
                        $distrito, $calle, $numero, $izq
                    );
                    //$tempVar = $this->buscarDireccion($distrito, $calle, $numero, $izq);
                    if (empty($data[(count($data)-1)])) {
                        $notFound[] = array($datos, $numero);
                    }
                    else{
                        foreach ($data[(count($data)-1)] as $rutas) {
                            $result[] = $rutas;
                        }
                    }
                }
                fclose($file);*/
        /*foreach ($data as $val) {
        }*/
        $finalResult["data"] = $result;
        $finalResult["notFound"] = $notFound;
        if ($first) {
            $finalResult["firstId"] = $first->id;
            $finalResult["lastId"] = $last->id;
        }
        echo json_encode($finalResult);
    }

    public function postDescargarexcel()
    {
        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition: filename=ficheroExcel.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo $_POST['datos_a_enviar'];
    }

    private function addressToArray($string)
    {

        $string = str_replace(
            array(
                '\r\n', '\r', '\n', '\\r', '\\n', '\\r\\n', '\t', '\v', '"',
                '$', '<', '>', '&', '{', '}', '*', "'", '\\'
            ),
            array(''),
            $string
        );
        if (strlen($string) == 0)
            return array("calle" => "", "numero" => "");
        $flag = false;
        $adr = '';
        $arrayAddress = explode(" ", $string);
        if (count($arrayAddress) < 2)
            return array("calle" => "", "numero" => "");

        $number = "0";
        $backaddress = "";
        foreach ($arrayAddress as $address) {
            $res = array();
            if (!$flag) {
                $sql = "SELECT * FROM geo_combinaciones "
                    . "WHERE COMBINACIONES = '$address'";
                $res = DB::select($sql);
            }

            if (!empty($res)) {
                $flag = true;
            }

            if ($flag) {
                if (intval($address) || $address == "0") {
                    $number = $address;
                    break;
                }
                if ($address != $backaddress) {
                    $adr .= $address . " ";
                    $backaddress = $address;
                }
            }
        }
        return array("calle" => trim($adr), "numero" => $number);
    }

    public function postGenerarkml()
    {
        if (Input::get("coordinates")) {
            $coordinates = Input::get("coordinates");
            $contents = Input::get("contents");
            //dd($coordinates);
            $kml = new SimpleXMLElement('<kml/>');

            $document = $kml->addChild('Document');

            foreach ($coordinates as $key => $coordinate) {
                $placeMark = $document->addChild("Placemark");
                $placeMark->addChild("name", "Direccion");
                $placeMark->addChild("description", $contents[$key]);
                $point = $placeMark->addChild("Point");
                $point->addChild("coordinates", $coordinate["lng"] . "," . $coordinate["lat"]);
            }
            return json_encode(array("rst" => 1, "data" => $kml->asXML()));
        } else {
            return json_encode(array("rst" => 0, "data" => "No se ha realizado una busqueda"));
        }
    }

    public function postUploadkml()
    {
        $content = File::get(Input::file("kml")->getRealPath());
        $kml = simplexml_load_string($content);

        $polygon = true;
        $type = "polygon";
        if (isset($kml->Document->Placemark->Point)) {
            $polygon = false;
            $type = "point";
        }
        if ($polygon === true) {
            $content = array();
            foreach ($kml->Document->Placemark as $polygon) {
                $coordinates = array();
                $name = $polygon->name->__toString();
                $latLng = trim($polygon->Polygon->outerBoundaryIs->LinearRing->coordinates->__toString());
                $latLng = str_replace(",100", "", $latLng);
                $latLng = explode(',', $latLng);

                foreach ($latLng as $key => $coordinate) {
                    if ($key % 2 != 0) {
                        $coordinates[] = array("lat" => floatval($latLng[$key]), "lng" => floatval($latLng[$key - 1]));
                    }
                }
                $content[] = array("coordinates" => $coordinates, "name" => $name);

            }
            return json_encode(array("rst" => 1, "polygons" => $content, "type" => $type));
        } else {
            $coordinates = array();
            foreach ($kml->Document->Placemark as $points) {
                $content = $points->description;
                $latLng = explode(",", $points->Point->coordinates);
                $coordinates[] = array("lat" => $latLng[0], "lng" => $latLng[1], 'content' => $content[0]);
            }
            return json_encode(array("rst" => 1, "coordinates" => $coordinates, "type" => $type));
        }
    }

    public function postGeneratekmlpolygon()
    {
        $coordinates = Input::get("coordinates");
        $titles = Input::get("titles");

        $kml = new SimpleXMLElement('<kml/>');
        $document = $kml->addChild('Document');
        foreach ($coordinates as $key => $coordinate) {
            $coordinatesString = array();
            $placeMark = $document->addChild("Placemark");
            $placeMark->addChild("name", trim($titles[$key]));
            $polygon = $placeMark->addChild("Polygon");
            $outer = $polygon->addChild("outerBoundaryIs");
            $linear = $outer->addChild("LinearRing");
            foreach ($coordinate as $latlng) {
                $coordinatesString[] = $latlng["lng"] . "," . $latlng["lat"] . "" . ",100";
            }
            $linear->addChild("coordinates", implode(",", $coordinatesString));
        }
        return json_encode(array("rst" => 1, "data" => $kml->asXML()));
    }
}
