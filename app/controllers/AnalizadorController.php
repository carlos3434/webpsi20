<?php
use Legados\PruebaElectricaService;

class AnalizadorController extends \BaseController
{
    public function postObtener()
    {
        
        $rst=1;
        $msj="Realizado Correctamente"; 
        $listado= array();
        $api = new  PruebaElectricaService();
        
        $numero=trim(Input::get('telefono'));
        $response= $api->obtener($numero); //'15671606'

        if (isset($response['listaPrueba'])) {
            $listado = $response['listaPrueba'];
        } else {
            $rst= 2;
            $msj= $response['descripcion'];
        }
        
        //$rst= $api->obtenerHistorico('15671606');
        //$rst= $api->ejecutar('15671606','ProElectricaText');
        return \Response::json(
                array(
                    'rst' => $rst,
                    'msj' => $msj,
                    'pruebas' => $listado
                )
            );
    }

    public function postProbar()
    {
        
        $rst=1;
        $msj="Realizado Correctamente";
        $listado= array();
        $api = new  PruebaElectricaService();
        
        $numero=trim(Input::get('telefono'));
        $pruebas=trim(Input::get('pruebas'));
        try {
            $response= $api->ejecutar($numero,$pruebas);
        } catch (Exception $e) {
             $fo = fopen("errorejecutaranalizador.txt", "w+");
                 fwrite($fo, $e);
                 fclose($fo);
        }
        if (!isset($response['interprete'])) {
            $rst= 2;
             $msj='No se obtienen pruebas';
            if(isset($response['descripcion']))
                $msj= $response['descripcion'];
        }
        return \Response::json(
                array(
                    'rst' => $rst,
                    'msj' => $msj,
                    'resultado' => $response
                )
            );

    }

    public function postObteneryprobar()
    {
        
        $rst=1;
        $msj="Realizado Correctamente";
        $listado= array();
        $responseejecucion=array(
                    'rst' => 2,
                    'msj' => 'No se puede interpretar la respuesta'
                );

        $api = new  PruebaElectricaService();

        $telefono=trim(Input::get('telefono'));
        try {
            $response= $api->obtener($telefono);
        } catch (Exception $e) {}

        if (isset($response['listaPrueba'])) {
            $listado = $response['listaPrueba'];
        } else {
            $msj='No se obtienen pruebas';
            if(isset($response['descripcion']))
                $msj= $response['descripcion'];

            return \Response::json(
                array(
                    'rst' => 2,
                    'msj' => $msj
                )
            );
        }

        $pruebas=$listado[count($listado)-1]['descripcion'];
        try {
            $responseejecucion= $api->ejecutar($telefono,$pruebas);
        } catch (Exception $e) {
             $fo = fopen("errorejecutaranalizador.txt", "w+");
                 fwrite($fo, $e);
                 fclose($fo);
        }

       return $responseejecucion;

    }

}
