<?php

class CoordenadasTecnicasController extends \BaseController {
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postCargar(){
        $fcha_inicio=Input::get("fecha_inicio");
        $fcha_fin=Input::get("fecha_fin");
        $fechas="'".$fcha_inicio."' AND '".$fcha_fin."'";
        $empresas=Input::get("empresas");
        $buckets=Input::get("buckets");
        $actividades=Input::get("actividades");

        /*Filtros*/
        $where='';
        if (isset($empresas)) {$where.=' and t.empresa_id in ('.implode(',',$empresas).')';}
        
        $bucket=[];
        for ($i=0; $i < count($buckets); $i++) { 
            array_push($bucket, "'".strtoupper($buckets[$i])."'");
        }
        if (isset($buckets)) {$where.=' and UPPER(t.bucket) in ('.implode(',',$bucket).')';}
        if (isset($actividades)) {$where.=' and stm.actividad_id in ('.implode(',',$actividades).')';}

        /*consulta*/
        $select=['stmd.id as id','stmd.sms','stmd.distancia',DB::raw('IFNULL(UPPER(t.nombre_tecnico),"") 
        AS nombre_tecnico'),'st_cms.num_requerimiento','st_cms.solicitud_tecnica_id','st_cms.coordx_cliente',
        'st_cms.coordy_cliente',DB::raw('IFNULL(st_cms.nom_cliente,"") AS nom_cliente'),
        'stmd.coordx_tecnico','stmd.coordy_tecnico',DB::raw('IFNULL(t.carnet,"") AS carnet'),
        DB::raw('DATE(stmd.created_at) AS fecha_validacion'),DB::raw('IFNULL(stm.fecha_agenda,"") AS fecha_agenda'),
        DB::raw('UPPER(REPLACE(t.bucket,"_"," ")) as bucket'),"ac.nombre as actividad"];

        $data=  CoordenadasTecnicas::select($select)
                    ->leftjoin('tecnicos as t','stmd.tecnico_id','=','t.id')
                    ->leftjoin('solicitud_tecnica_movimiento as stm', 'stmd.solicitud_tecnica_movimiento_id','=','stm.id')
                    ->leftjoin('solicitud_tecnica_cms as st_cms','stm.solicitud_tecnica_id','=','st_cms.solicitud_tecnica_id')
                    ->leftjoin('actividades as ac','ac.id','=','stm.actividad_id')
                    ->where('stmd.tipo','=','1')
                    ->whereNotNull('stmd.sms')
                    ->whereRaw('DATE(stmd.created_at) between '.$fechas.$where)
                    ->orderBy('stmd.created_at','asc');

        $data=$data->searchPaginateAndOrder();
        return $data;
    }

    public function postExcel(){

        $fcha_inicio=Input::get("fch_inicio");
        $fcha_fin=Input::get("fch_fin");
        $fechas="'".$fcha_inicio."' AND '".$fcha_fin."'";
        $empresas=Input::get("empresas");
        $buckets=Input::get("buckets");
        $actividades=Input::get("actividades");

        //filtros
        $where='';
        if (isset($empresas) and $empresas<>'null') {$where.=' and t.empresa_id in ('.$empresas.')';}
        
        $bucket=[];
        $b=explode(',',$buckets);
        for ($i=0; $i < count($b); $i++) { 
            array_push($bucket, "'".strtoupper($b[$i])."'");
        }
        if (isset($buckets) and $buckets<>'null') {$where.=' and UPPER(t.bucket) in ('.implode(',',$bucket).')';}
        if (isset($actividades) and $actividades<>'null') {$where.=' and stm.actividad_id in ('.$actividades.')';}
        
        //consulta
        $reporte=DB::select("SELECT IFNULL(UPPER(t.nombre_tecnico),'') AS nombre_tecnico,stmd.sms,stmd.distancia,st_cms.num_requerimiento,st_cms.solicitud_tecnica_id,ac.nombre as actividad,UPPER(REPLACE(t.bucket,'_',' ')) as bucket,DATE(stmd.created_at) as fecha_validacion
            FROM solicitud_tecnica_movimiento_detalle as stmd
            LEFT JOIN tecnicos as t ON stmd.tecnico_id=t.id
            LEFT JOIN solicitud_tecnica_movimiento as stm on stmd.solicitud_tecnica_movimiento_id=stm.id
            LEFT JOIN solicitud_tecnica_cms as st_cms on stm.solicitud_tecnica_id=st_cms.solicitud_tecnica_id
            LEFT JOIN actividades ac on ac.id=stm.actividad_id
            WHERE stmd.tipo=1 AND IFNULL(stmd.sms,'')<>'' AND DATE(stmd.created_at) between ".$fechas.$where."
            ORDER BY stmd.created_at ASC");

            if (Input::get("tipo")=='Excel') {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=CoordenadasTecnicas.xls');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');

            if(count($reporte)>0){
                $n = 1;
                foreach ($reporte as $data) {
                //Encabezado
                    if ($n == 1) {
                        foreach ($data as $key => $val) {
                            echo $key . "\t";
                        }
                        echo $val . "\r\n";
                    }
                    //Datos
                    foreach ($data as $val) {
                        $val = str_replace(
                        array("\r\n", "\n", "\n\n", "\t", "\r"),
                        array("", "", "", "", ""),
                        $val
                        );
                        echo $val . "\t";
                    }
                    echo "\r\n";
                    $n++;
                }
            }else{$val="Mensaje:No existen registros!";echo $val . "\r\n";}
        } else {return Response::json(array('rst' => 1, 'datos' => $reporte));}
    }

    public function postListar(){

        $tipo=Input::get('tipo');
        if($tipo=='empresas'){
            $data=DB::select("SELECT  id,UPPER(nombre) as nombre FROM empresas WHERE estado=1;");
        }

        if($tipo=='actividades'){
            $data=DB::select("SELECT id,UPPER(nombre) as nombre FROM actividades WHERE estado=1;");
        }
                 
        return Response::json(["rst" => 1, "datos" => $data]);
    }





}

