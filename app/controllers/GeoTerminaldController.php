<?php
class GeoTerminaldController extends \BaseController
{
    public function postListar()
    {
       
        $r =GeoTerminald::postGeoTerminaldAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoTerminald::postMdfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postMdf()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $r =GeoTerminald::postCableFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postCable()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $array['cable']=Input::get('cable');
        $r =GeoTerminald::postTerminalFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


}

