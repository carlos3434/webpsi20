<?php

class AnalisisMensajesToaController extends \BaseController {
    public function postGenerar() {
        $buckets = Input::has("bucket")? Input::get("bucket") : [];
        $fecha = explode(" - ", Input::has("fecha")? Input::get("fecha") : []);
        $download = Input::get("flagdownload", false);
        $select = ["asistencia_tecnico_ofsc.*",
            DB::raw("(CONCAT(u.nombre, ' ', u.apellido)) AS usuario")];
        $asistencias = AsistenciaTecnicoOfsc::select($select)
            ->leftJoin("usuarios as u", "u.id", "=", "asistencia_tecnico_ofsc.usuario_created_at");
        
        $search = Input::get("search", "");
        if ($search!="") {
            $asistencias->whereRaw("(asistencia_tecnico_ofsc.bucket LIKE '%$search%')");
        }
        if (count($buckets) > 0) {
            $asistencias->whereIn("asistencia_tecnico_ofsc.bucket", $buckets);
        }
        if (count($fecha) > 0) {
            $asistencias->whereBetween("asistencia_tecnico_ofsc.fecha", $fecha);
        }

        if ($download) {
            $select = [ "ato.fecha",
                "ato.bucket",
                "asistencia_tecnico_ofsc_detalle.carnet",
                "asistencia_tecnico_ofsc_detalle.fecha_ultimo_officetrack",
                "asistencia_tecnico_ofsc_detalle.fecha_ultimo_ofsc",
                DB::raw("(CASE asistencia_tecnico_ofsc_detalle.asistio WHEN 0 THEN 'Ausente' WHEN 1 THEN 'Asistio' WHEN 2 THEN 'Vacaciones' WHEN 3 THEN 'Permiso' END) AS asistencia"),
                "asistencia_tecnico_ofsc_detalle.created_at AS fecregistro",
                "asistencia_tecnico_ofsc_detalle.updated_at AS fecmodificacion",
                DB::raw("(CASE asistencia_tecnico_ofsc_detalle.estado WHEN 1 THEN 'Menor a 20 minutos' WHEN 2 THEN 'Entre 20 minutos a 2 horas' WHEN 3 THEN 'Entre 2 horas a 4 horas' WHEN 4 THEN 'Mas de 4 horas' END) AS referenciaultimoxy")];
            $excel = AsistenciaTecnicoOfscDetalle::select($select)->leftJoin(
                "asistencia_tecnico_ofsc as ato",
                "ato.id",
                "=",
                "asistencia_tecnico_ofsc_detalle.asistencia_tecnico_ofsc_id"
            );
            if ($search!="") {
                $excel->whereRaw("(ato.bucket LIKE '%$search%')");
            }
            if (!is_null($buckets) && $buckets!="null") {
                if (count($buckets) > 0) {
                    $excel->whereIn("ato.bucket", $buckets);
                }
            }
            if (count($fecha) > 0) {
                $excel->whereBetween("ato.fecha", $fecha);
            }
            $excel   = $excel->get();
            $asistencias = json_decode(json_encode($excel), true);
            $txtnombrefile = "";
            if (count($buckets) > 0 && !is_null($buckets) && $buckets!="null") {
                $txtnombrefile = implode(",", $buckets);
            }
            return Helpers::exportArrayToExcel($asistencias, "AsistenciaTecnicos{$txtnombrefile}");
        }

        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];

        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;

        $column = "id";
        $dir = "desc";
        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }
        $asistencias = $asistencias
                    ->orderBy($column, $dir)
                    ->paginate($grilla["perPage"]);
        $data = $asistencias->toArray();
        $col = new Collection([
            'recordsTotal'=>$asistencias->getTotal(),
            'recordsFiltered'=>$asistencias->getTotal(),
        ]);
        return $col->merge($data);
    }
}