<?php

use Legados\models\SolicitudTecnicaCms;
use Legados\models\ComponenteCms;
use Ofsc\Activity;
use Ofsc\Inbound;
use Legados\STCmsApi;
use Legados\models\SolicitudTecnicaLogRecepcion as LogST;
use Legados\models\Error;

class SolicitudTecnicaGestelAveriaController extends BaseController
{
    public function getServer()
    {
        function generar($request)
        {
            // Desglosamos la solicitud tecnica y los componentes
            Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_averia_generar.log');
            Log::info([$request]);
            Auth::loginUsingId(\Config::get("legado.user_logs.genera_solicitud_gestel"));
            $log = new LogST;
            $log->id_solicitud_tecnica = $request->id_solicitud_tecnica;
            $log->trama = json_encode($request);
            $log->save();
            Auth::logout();
            $response=[
                'id_solicitud_tecnica' => $request->id_solicitud_tecnica,
                'id_respuesta'=>"1", 'observacion'=> "OK"
            ];
            $envio = ["request" => $request, "logId" => $log->id];
            Queue::push('GenerarSolicitudTecnicaGestelAveria', $envio, 'generar_st_averia');

            return $response;
        }
        try {
            $server = new SoapServer(Config::get('legado.solicitudtecnica_gestel_averia.wsdl'));
            $server->addFunction("generar");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}