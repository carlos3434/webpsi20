<?php
class GeoDireccionController extends \BaseController
{
    public function __construct(
            UploadController $uploadController, 
            ErrorController $errorController
        )
    {
        $this->_uploadController = $uploadController;
        $this->_errorController = $errorController;
    }
    public function postSave()
    {
        $grupo=Input::get('nombre');
        $direcciones=Input::get('dato');

        $rst=1;
        $msj='Grupo Registrado Correctamente';

        $reglas = array(
              'nombre'    => 'required|unique:geo_grupo_direccion'
        );
        $mensaje = array(
                'required'  => 'Nombre de Grupo es requerido',
                'unique'   => 'Nombre de Grupo ya está registrado',
            );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                )
            );
        }
        $grupodireccion = new GeoGrupoDireccion;
        $grupodireccion->nombre = $grupo;
        $grupodireccion->save();

        if($grupodireccion) {
            foreach ($direcciones as $key => $value) {
                $direccionObj= new GeoDireccion($value);
                unset($direccionObj['coordenadas']);
                $d=$grupodireccion->direcciones()->saveMany([$direccionObj]);

                if(isset($value['coordenadas'])) {
                    foreach ($value['coordenadas'] as $key => $valor) {
                        $direccion=GeoDireccion::find($d[0]->id);
                        $direccioncoordObj=new GeoDireccionCoord($valor);
                        $direccion->coordenadas()->saveMany([$direccioncoordObj]);
                    }
                } 
                //AQUI ME QUEDE
            }
        }
    
        return json_encode(array("rst" => $rst,
                                "msj" => $msj)
                            );
    }

    public function postUpdate()
    {
        $grupo=Input::get('grupo',0);
        $direcciones=Input::get('dato');

        $rst=2;
        $msj='Error al Actualizar Grupo';

        if($grupo) {
            $rst=1;
            $msj='Grupo Actualizado Correctamente';
            foreach ($direcciones as $key => $value) {
                $direccionObj=GeoDireccion::find($value['id']);
                if(isset($value['coordenadas'])) {
                    $coordenadas=$value['coordenadas'];
                    unset($value['coordenadas']);
                }
                $direccionObj['attributes']=array_merge($direccionObj['attributes'],$value);
                $direccionObj->update();
                
                foreach ($coordenadas as $key => $valor) {
                    $direccioncoordObj=GeoDireccionCoord::find($valor['id']);
                    $direccioncoordObj['attributes']=array_merge($direccioncoordObj['attributes'],$valor);
                    $direccioncoordObj->update();
                }
            }
        }
    
        return json_encode(array("rst" => $rst,
                                "msj" => $msj)
                            );
    }

    public function postUpdategrupo()
    {
        $grupo=Input::get('dato');

        $rst=1;
        $msj='Grupo Actualizado Correctamente';

        $direccionObj=GeoGrupoDireccion::find($grupo['id']);
        $direccionObj['attributes']=array_merge($direccionObj['attributes'],$grupo);
        $direccionObj->update();
    
        return json_encode(array("rst" => $rst,
                                "msj" => $msj)
                            );
    }

    public function postCargaplana()
    {
        $rst=1;
        $msj="";

        $upload = $this->_uploadController->postCargartmp(0, 0);
        $upload  = json_decode($upload);

        $peso=filesize($upload->file);
        $ext = pathinfo($upload->file, PATHINFO_EXTENSION );

        if($peso>42203) { 
            $rst=2;
            $msj.="El Archivo excede el limite: ". $peso. " Bytes. (Max: 92876)";
        }
        if($ext!=="csv") { 
            $rst=2; 
            $msj.= "Solo se permite Archivos CSV";
        }

        if($rst==2){
            return json_encode(array("rst" => $rst,
                                "msj" => $msj,
                                "csv" => "")
                            );
        }

        $handle = fopen($upload->file,'r');

        $csv=array();

        $claves=array("p", "pi", "pis", "piso", 
                                    "l", "lo", "lot", "lote", "lt",
                                    "i", "int", "inter", "interior",
                                    "o", "of", "ofi", "ofic", "oficina",
                                    "e", "ed", "edi", "edif", "edificio", 
                                    "d", "de","dep", "depar", "departamento", "dpto",
                                    "a", "ap", "apar", "apart", "apartamento", "apto",
                                    "pa", "pas","pje", "psje", "pasje", "pasaje",
                                    "c", "ca", "cal", "call", "calle", "cl",
                                    "cu", "cd", "cdra", "cdrs", "cuadra",
                                    "s", "sec", "st", "sect", "sctr", "sector",
                                    "m", "mz", "manz", "manzan", "mzn", "mnz", "manzana",
                                    "jr", "j", "jiron",
                                    "av", "avenida", "aven",
                                    "aa-hh", "aahh", "ah", "humano", "as", "cc", "bl", "li", "nn", "sn",
                                    "s/n", "b", "f", "g", "h", "l", "m", "n", "o", "q", "r", "s",
                                    "t", "u","v", "w", "x", "y", "z",
                                    "u", "ur", "urb", "urbanizacion", "urbanización",
                                    "pr", "li", "b", "std", "stand", "pto", "puesto", "almacen", "alm",
                                    "puerto", "pab", "pabellon", "par", "tor", "torre", "block", "bloque", "bloq");

        $calvesdos=array(
        "enero", "febrero", "marzo",
        "abril", "mayo", "junio", "julio", "agosto", "septiembre", 
        "setiembre", "octubre", "noviembre", "diciembre", "de");
        while ( ($data = fgetcsv($handle) ) !== FALSE ) {
                $variable=explode(" ", $data[0]);
                
                $address=$data[0];
                $borrar=array();
                foreach ($variable as $key => $value) {
                    $d=intval($value);
                    if($d!=0 && ($data[1]=="" || $data[1]==0 || !is_numeric($data[1]))){
                        if (isset($variable[$key-1]) && 
                            !in_array(strtolower(preg_replace('([^A-Za-z0-9])', 
                                '', $variable[$key-1])), $claves)){
                            //$data[1]=$d;
                            if (isset($variable[$key+1]) && 
                                !in_array(strtolower(preg_replace('([^A-Za-z0-9])', 
                                '', $variable[$key+1])), $calvesdos)){
                                $data[1]=$d;
                            } 
                            if(!isset($variable[$key+1])){
                                $data[1]=$d;
                            }
                        } 
                    } //limpiando direccion
                    if(strpos($value, ".")!==false) { array_push($borrar, $key); }

                    $variable[$key]=$value=preg_replace('([^A-Za-z0-9])','', $value);
                    if(in_array(strtolower(preg_replace('([^A-Za-z0-9])', 
                                '', $value)), $claves) ) {
                            array_push($borrar, $key);
                    }
                    if(preg_match('([^A-Za-z])',$value)==1 &&
                        preg_match('([^A-Za-z0-9])',$value)==0) { //combinacion numeroyletras o numeros
                        if(preg_match('([^0-9])',$value)==0) { //solo numeros
                            if (!isset($variable[$key+1]) ) {
                                array_push($borrar, $key);
                            } 
                            if (!isset($variable[$key-1]) ) {
                                array_push($borrar, $key);
                            }
                            if (isset($variable[$key+1]) &&
                                !in_array(strtolower($variable[$key+1]), $calvesdos)){
                                array_push($borrar, $key);
                            }
                        } else {
                            array_push($borrar, $key);
                        }
                    } else if (strlen($value)==1){
                            array_push($borrar, $key);
                    }

                    if($value=="") unset($variable[$key]);
                }

                foreach ($borrar as $key => $value) {
                    unset($variable[$value]);
                }

                $data[0]= implode(" ", $variable);

                $geotramo=new Tramo();
                $izq = true;
                if ( $data[1]%2 === 0 ) {
                    $izq = false;
                }
                
                if($data[1]!==0){
                    $data[0]=str_replace($data[1], "", $data[0]);
                }
    
                $tramos=$geotramo->getTramos("000000", $data[0], $data[1], $izq);
    
                $direccion['direccion_completa']=$address;
                $direccion['direccion']=$data[0];
                $direccion['numero']=$data[1];
                $direccion['nombre_dato']=$data[2];
                $direccion['dato']=$data[3];
                $direccion['coordenadas']=array();
    
    
                foreach ($tramos as $key => $value) {
                    $x=$y=0;
                    if($izq) {
                        $dist1=$data[1]-$value->NUM_IZQ_IN;
                        $dist2=$value->NUM_IZQ_FI-$data[1];             
                    } else {
                        $dist1=$data[1]-$value->NUM_DER_IN;
                        $dist2=$value->NUM_DER_FI-$data[1];
                    }                                    
                    if($dist2<$dist1) {
                        $x=$value->XB;
                        $y=$value->YB;
                    } else {
                        $x=$value->XA;
                        $y=$value->YA;
                    }
                    $coordenadas['coordx']=(Float) $x; 
                    $coordenadas['coordy']=(Float) $y;
                    $coordenadas['validacion']=0;
                    array_push($direccion['coordenadas'], $coordenadas);
    
                    if($key==1) break;
                }
                array_push($csv, $direccion);
        }
        unlink($upload->file);

        //return $csv;
        return json_encode(array("rst" => 1,
                                "msj" => "Archivo Cargado Correctamente",
                                "csv" => $csv)
                            );
    }

    public function postListargrupo()
    {
       $result=GeoGrupoDireccion::where('estado',1)->get(); 
       return $result;
    }

    public function postListardirecciones()
    {   
       $id=Input::get('grupo');
       $result=GeoDireccion::where('geo_grupo_direccion_id',$id)
                ->with('coordenadas')->get();
       return $result;
    }

}