<?php

use OfscRest\Users;

class UsuarioOfscController extends BaseController
{
    public function getUsuarios($offset)
    {
        $User = new Users();

        $usuarios = $User->getUsers(10, intval($offset));
        $usuarios = json_decode($usuarios);

        return Response::json(
            array(
                'rst' => 1,
                'total' => $usuarios->totalResults,
                'usuarios' => $usuarios->items
            )
        );
    }

    public function getUsuario($id)
    {
        $User = new Users();

        $usuario = $User->getUser($id);
        $usuario = json_decode($usuario);

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $usuario
            )
        );
    }

    public function postModificar()
    {
        $User = new Users();

        $login = Input::get('id');
        $datos = Input::get('datos');

        $user = $User->updateUser($login, $datos);

        $response = json_decode($user);

        if (isset($response->status) && $response->status == '400') {
            return Response::json(
                array(
                    'rst' => 0,
                    'msj' => $response->detail
                )
            );
        }

        return Response::json(
            array(
                'rst' => 1,
                'msj' => 'Usuario Modificado'
            )
        );
    }

    public function postRegistrar(){
        $datos = Input::get('data');
        $datos['language'] = 'en';
        $datos['timeZone'] = 'Peru';
        $datos['userType'] = 'SOAP';

        $User = new Users();

        $user = $User->createUser($datos['login'],$datos);

        $response = json_decode($user);

        if (isset($response->status) && $response->status == '400') {
            return Response::json(
                array(
                    'rst' => 0,
                    'msj' => $response->detail
                )
            );
        }

        return Response::json(
            array(
                'rst' => 1,
                'msj' => 'Usuario Creado'
            )
        );
    }
}