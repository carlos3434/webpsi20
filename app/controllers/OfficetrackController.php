<?php
use ClassOT\OfficeTrackGetSet;
use ClassOT\Error;
use ClassOT\CurlNovus;
use ClassOT\Validacion_activa;

class OfficetrackController extends \BaseController
{
    public function __construct(ErrorController $ErrorController)
    {
        $this->error = $ErrorController;
    }

    /**
     * Envio de una tarea hacia officetrack
     *
     * $data['fecha_agenda']            : Fecha de agendamiento
     * ** $data['hora_agenda']           : Hora de agendamiento (rango)
     * $data["gestion_id"]              : ID de gestion autogenerado
     * $data['codactu']                 : Averia, peticion o requerimiento
     * $data['fecha_registro']          : Fecha de registro de la actuacion
     * $data['nombre_cliente']          : Nombre del cliente
     * $data['direccion_instalacion']   : Direccion del cliente
     * $data['actividad']               : Averia o Provision
     * $data['codmotivo_req_catv']      : Codigo CMS
     * $data['orden_trabajo']           : Orden de trabajo
     * $data['fftt']                    : Facilidades tecnicas
     * $data['dir_terminal']            : Direccion de terminal
     * $data['inscripcion']             : Inscripcion o codigo de cliente
     * $data['mdf']                     : MDF
     * $data['segmento']                : Codigo de segmento
     * $data['clase_servicio_catv']     : Dato CMS
     * $data['total_averias']           : Total averias reportadas
     * $data['zonal']                   : Zonal (LIM, ARE, ...)
     * $data['llamadastec15dias']       : Numero de llamadas tecnicas 15 dias
     * ** $data['quiebre']               : Nombre de quiebre
     * $data['lejano']                  : Dato lejano
     * $data['distrito']                : Nombre de distrito
     * $data['averia_m1']               : Dato M1
     * $data['telefono_codclientecms']  : Dato CMS
     * $data['area2']                   : Dato tmp area2
     * ** $data['carnet']                : Carnet del tenico
     * ** $data['eecc_final']            : Empresa colaboradora asignada
     * ** $data['estado']                : Estado en web psi
     * $data['cr_observacion']          : Observacion
     * ** $data['velocidad']             : Indice 2 de explode("|", paquete)
     * * $data['duration']              : Duracion actividad en horas
     *
     * @param type $data Arreglo de datos de la orden
     * @return Array 'officetrack'=> "OK", "Error"
     */

    public function postEnviartarea($data = array())
    {
        if (
            Input::has('otdata')
            and is_array(Input::get('otdata'))
            and count(Input::get('otdata')) > 0
        ) {
            $data = Input::get('otdata');
        }

        //Url OT
        $url = Config::get("wpsi.ot.url");

        if ($data["estado_agendamiento"] != '1-1') {
            $tecnico = "";
            $data['carnet'] = "";
            $data['fecha_agenda'] = "";
            $data['hora_agenda'] = "";
        }

        $agendaArray = explode("-", $data['hora_agenda']);
        if (isset($agendaArray[1])) {
            $dueDate = date(
                "YmdHis",
                strtotime(
                    $data['fecha_agenda']
                    . " "
                    . $agendaArray[1] . ":00"
                )
            );
        } else {
            $dueDate = date("YmdHis", strtotime($data['fecha_registro']));
        }

        //Para horario libre
        if (isset($agendaArray[0]) and strtolower($agendaArray[0]) == 'libre') {
            $data["duration"] = 1439;
            $dueDate = date("YmdHis", strtotime($data['fecha_agenda'] . "23:59:59"));
        }

        //Ultimo movimiento
        $ultimo = DB::table("ultimos_movimientos")
            ->where('gestion_id', '=', $data["gestion_id"])
            ->get();

        //Seleccionar datos
        $envio['UserName'] = "";
        $envio['Password'] = "";
        $envio['Operation'] = "AutoSelect";
        $envio['TaskNumber'] = $data["gestion_id"];
        $envio['EmployeeNumber'] = $data['carnet'];
        $envio['DueDateAsYYYYMMDDHHMMSS'] = $dueDate;
        $envio['Duration'] = number_format(($data["duration"] / 60), 2);
        $envio['Notes'] = "";

        $envio['Description'] = $data["gestion_id"]
            . "-"
            . $data['codactu'];
        $envio['Status'] = "NewTask";
        $envio['CustomerName'] = $data['hora_agenda']
            . " / "
            . $data['nombre_cliente'];

        //Ultimos datos
        $x = 0;
        $y = 0;
        $quiebreId = 0;

        if (count($ultimo) > 0) {
            $x = $ultimo[0]->x;
            $y = $ultimo[0]->y;
            $quiebreId = $ultimo[0]->quiebre_id;
        }

        $envio['Location'] = array(
            "East" => $x,
            "North" => $y,
            "Address" => $data['direccion_instalacion']
        );
        //$envio['Data1']=trim(
        //               $data['fecha_agenda']
        //               . " "
        //               . $data['hora_agenda']
        //               );
        $envio['Data2'] = $data['codactu'];
        $envio['Data3'] = $data['fecha_registro'];
        $coordinado = "NO";
        if ($data["coordinado2"] > 0) {
            $coordinado = "SI";
        }
        $envio['Data4'] = $coordinado;
        //$envio['Data5'] = $data['direccion_instalacion'];
        $envio['Data6'] = $data["actividad"]
            . " - "
            . $data['codmotivo_req_catv'];
        $envio['Data7'] = $data['orden_trabajo'];
        $envio['Data8'] = $data['fftt'];
        $envio['Data9'] = $data['dir_terminal'];
        $envio['Data10'] = $data['inscripcion'];
        $envio['Data11'] = $data['mdf'];
        $envio['Data12'] = $data['segmento'];
        $envio['Data13'] = $data['clase_servicio_catv'];
        $envio['Data14'] = $data['total_averias'];
        $envio['Data15'] = $data['zonal'];
        $envio['Data16'] = $data['llamadastec15dias'];
        $envio['Data17'] = $data['quiebre'];
        $envio['Data18'] = $data['lejano'];
        $envio['Data19'] = $data['distrito'];
        $envio['Data20'] = $data['averia_m1'];
        $envio['Data21'] = $data['telefono_codclientecms'];
        $envio['Data22'] = $data['area2'];
        $envio['Data23'] = Config::get("wpsi.geo.public.mapord")
            . $data['carnet']
            . '/'
            . $data['gestion_id'];
        //tipo_servicio // ubicacion
        $envio['Data24'] = Config::get("wpsi.geo.public.maptec")
            . $data['carnet'];
        //tipo_actuacion// ruta del dia
        $envio['Data25'] = $data['eecc_final'];
        //Data26: La observacion (obs_dev, obs_102)
        if (count($ultimo) > 0) {
            $envio['Data26'] = $ultimo[0]->observacion;
        }
        //$envio['Data27']	=	$data['estado']; //Estado Webpsi
        $envio['Data28'] = $data['cr_observacion'];
        $envio['Data29'] = $data['velocidad'];

        $cantidadcomp = 0;
        $auxComponente = "";
        $conteocomp = 1;
        $arrayComponentefin = array();
        if (isset($data['componente_text'])) {
            $componente = implode("^^", $data['componente_text']);
            $arrayComponente = explode("^^", $componente);
            for ($i = 0; $i < count($arrayComponente); $i++) {
                if ($i == 0) {
                    $auxComponente = $arrayComponente[$i];
                }
                if ($arrayComponente[$i] != $auxComponente) {
                    array_push(
                        $arrayComponentefin,
                        $conteocomp
                        . ") "
                        . $auxComponente
                        . "("
                        . $cantidadcomp
                        . ")"
                    );
                    $auxComponente = $arrayComponente[$i];
                    $cantidadcomp = 0;
                    $conteocomp++;
                }
                $cantidadcomp++;
                //$arrayComponente[$i] = ($i + 1) . ") " . $arrayComponente[$i];
            }
            array_push(
                $arrayComponentefin,
                $conteocomp . ") " . $auxComponente . "(" . $cantidadcomp . ")"
            );
            $componente = implode("\n", $arrayComponentefin);
            $envio['Data30'] = $componente;
        }
        $envio['Options'] = "SendNotificationToMobile";

        /**
         * Validacion de distancia
         * Grupo-quiebre: Movistar 1 (11) y Exclusivo (7)
         * distancia 300 metros => 0.3 Km
         */
        $valorokArray = array(7, 11);
        $quiebreObj = DB::table('quiebres')
            ->where('id', $quiebreId)
            ->first();
        $quiebreGrupoId = $quiebreObj->quiebre_grupo_id;

        if (array_search($quiebreGrupoId, $valorokArray) !== false) {
            $envio["MaximalRadiusForEntries"] = 0.3;
            $envio["ProhibitEntriesOutsideRadius"] = '1';
        }

        //Array to json
        $cadena = json_encode($envio);

        //Data
        $postData = array(
            'cadena' => $cadena
        );
        $result = $this->enviarOfficeTrack($postData);
        return array('officetrack' => $result);
    }

    public function postValidar()
    {
        $r = 0;
        $dataValidar = array();
        $dataValidar = Input::all();

        $vo = array();
        $vo['actividad_id'] = $dataValidar['actividad_id'];
        $vo['quiebre_id'] = $dataValidar['quiebre_id'];

        $validaOfficetrack = Helpers::ruta(
            'gestion_movimiento/validaofficetrack', 'POST', $vo, false
        );

        $eo = array();
        $eo['tecnico_id'] = $dataValidar['tecnico_id'];
        $eo['celula_id'] = $dataValidar['celula_id'];

        $estadoOfficetrack = Helpers::ruta(
            'tecnico/estadoofficetrack', 'POST', $eo, false
        );

        $ea = array();
        $ea['motivo_id'] = $dataValidar['motivo_id'];
        $ea['submotivo_id'] = $dataValidar['submotivo_id'];
        $ea['estado_id'] = $dataValidar['estado_id'];

        $estadoAgendamiento = Helpers::ruta(
            'estado/estadoagendamiento', 'POST', $ea, false
        );

        if ($validaOfficetrack * 1 > 0 &&
            $estadoOfficetrack == '1' &&
            ($dataValidar['transmision'] != "0" ||
                $estadoAgendamiento == '11'
            )
        ) {
            $r = 1;
        }

        return $r;
    }

    public function postDetallepaso()
    {
        $dato = trim(Input::get("dato"));
        $datoArray = explode("-|", $dato);
        $dataArray = explode("_", $datoArray[1]);
        $gestionId = $dataArray[2];

        $officetrack = new Officetrack();

        $data = array();
        $view = "";
        if ($datoArray[0] == '0001') {
            $view = "detalle_uno";
            $data = $officetrack->getPasouno($gestionId);
        }

        if ($datoArray[0] == '0002') {
            $view = "detalle_dos";
            $data = $officetrack->getPasodos($gestionId);
        }

        if ($datoArray[0] == '0003') {
            $view = "detalle_tres";
            $data = $officetrack->getPasotres($gestionId);
        }

        if (!empty($data)) {
            return View::make(
                'admin.officetrack.' . $view, array('data' => $data[0])
            );
        } else {
            return "Sin resultados";
        }
    }

    public function getPasouno()
    {

    }

    public function getPasodos()
    {

    }

    public function getPasotres()
    {

    }

    public function postCargar()
    {
        $o = DB::table('webpsi_officetrack.tareas')
            ->where('task_id', '=', Input::get('task_id'))
            ->orderBy('fecha_recepcion', 'DESC')
            ->get();

        return Response::json(
            array(
                'rst' => 1,
                'msj' => "Tareas Cargadas",
                'datos' => $o
            )
        );
    }

    public function postCargardetalle()
    {
        $o = array();
        $id = Input::get('task_id');
        $paso = explode("-", strtolower(Input::get('paso')));
        if (substr($paso[1], 0, 6) == 'inicio') {
            //it.id,pu.x, pu.y, pu.observacion
            $query = 'SELECT it.nombre url,iti.nombre,it.fecha_creacion,pu.*
                    FROM webpsi_officetrack.imagenes_tareas it
                    INNER JOIN webpsi_officetrack.imagenes_tipo iti
                        ON iti.id=it.imagen_tipo_id
                    INNER JOIN webpsi_officetrack.paso_uno pu
                        ON it.tarea_id=pu.task_id
                    WHERE substr(it.nombre,1,3)="p01"
                    AND pu.task_id="' . $id . '"
                    ORDER BY url';
            //echo $query;
            $o = DB::select($query);
        } else if (substr($paso[1], 0, 11) == 'supervision') {
            //it.id,p.observacion,p.motivo
            $query = 'SELECT it.nombre url,iti.nombre,it.fecha_creacion,p.*
                    FROM webpsi_officetrack.imagenes_tareas it
                    INNER JOIN webpsi_officetrack.imagenes_tipo iti
                        ON iti.id=it.imagen_tipo_id
                    INNER JOIN webpsi_officetrack.paso_dos p
                        ON it.tarea_id=p.task_id
                    WHERE substr(it.nombre,1,3)="p02"
                    AND p.task_id="' . $id . '"
                    ORDER BY url';
            $o = DB::select($query);
        } else if (substr($paso[1], 0, 6) == 'cierre') {
            //it.id,p.observacion,p.estado
            $query = 'SELECT it.nombre url,iti.nombre,it.fecha_creacion,p.*
                    FROM webpsi_officetrack.imagenes_tareas it
                    INNER JOIN webpsi_officetrack.imagenes_tipo iti
                        ON iti.id=it.imagen_tipo_id
                    INNER JOIN webpsi_officetrack.paso_tres p
                        ON it.tarea_id=p.task_id
                    WHERE substr(it.nombre,1,3)="p03"
                    AND p.task_id="' . $id . '"
                    ORDER BY url';
            $o = DB::select($query);

            $ps = DB::table("webpsi_officetrack.tareas AS t")
                ->join("gestiones_detalles AS gd", "gd.gestion_id", "=", "t.task_id")
                ->join('quiebres AS q', 'gd.quiebre_id', '=', 'q.id')
                ->select('q.nombre')
                ->where("t.id", "=", $id)
                ->first();

            $adc = DB::table("webpsi_officetrack.tareas As t")
                ->join('webpsi_officetrack.paso_tres As p', "p.task_id", "=", "t.id")
                ->join("webpsi_officetrack.detalle_materiales As m", "m.paso_tres_id", "=", "p.id")
                ->join("webpsi_officetrack.catalogo_materiales As c", "c.id", "=", "m.material_id")
                ->select("c.material", "m.utilizado")
                ->where("t.id", "=", $id)
                ->get();

            $o[] = array("nombre" => "quiebre", "data" => $ps->nombre);
            $o[] = array("nombre" => "adicional", "data" => $adc);
        }

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $o
            )
        );
    }

    /**
     * Reenvio hacia Officetrack
     * @return type
     */
    public function postProcesarot()
    {
        //Obtener carnet de tecnico
        $data['carnet'] = "";
        $tecnico = DB::table('tecnicos')
            ->where('id', '=', Input::get("tecnico_id"))
            ->get();
        if (isset($tecnico[0])) {
            $data['carnet'] = $tecnico[0]->carnet_tmp;
        }
        $data["coordinado2"] = Input::get("coordinado2");
        $data["gestion_id"] = Input::get("gestion_id");
        //$data['carnet'] = "";
        $data['fecha_registro'] = Input::get("fecha_registro");

        $horaIni = "04:00:00";
        $horaFin = "06:00:00";
        $fechaHora = explode("/", Input::get("fh_agenda"));
        $data['fecha_agenda'] = trim($fechaHora[0]);
        $data['hora_agenda'] = trim($fechaHora[1]);
        $horas = explode("-", $data['hora_agenda']);
        if (isset($horas[0])) {
            $horaIni = trim($horas[0]) . ":00";
        }
        if (isset($horas[1])) {
            $horaFin = trim($horas[1]) . ":00";
        }

        //Duracion
        $toTime = strtotime("{$data['fecha_agenda']} $horaIni");
        $fromTime = strtotime("{$data['fecha_agenda']} $horaFin");
        $data["duration"] = round(abs($toTime - $fromTime) / 60, 2);

        $data['codactu'] = Input::get("codactu");
        $data['fecha_registro'] = Input::get("fecha_registro");
        $data['nombre_cliente'] = Input::get("nombre_cliente");
        $data['direccion_instalacion'] = Input::get("direccion_instalacion");
        $data["actividad"] = Input::get("actividad");

        $data['codmotivo_req_catv'] = Input::get("act_codmotivo_req_catv");
        $data['orden_trabajo'] = Input::get("orden_trabajo");
        $data['fftt'] = Input::get("fftt");
        $data['dir_terminal'] = Input::get("dir_terminal");
        $data['inscripcion'] = Input::get("inscripcion");
        $data['mdf'] = Input::get("mdf");
        $data['segmento'] = Input::get("segmento");
        $data['clase_servicio_catv'] = Input::get("clase_servicio_catv");
        $data['total_averias'] = Input::get("total_averias");
        $data['zonal'] = Input::get("zonal");
        $data['llamadastec15dias'] = Input::get("llamadastec15dias");
        $data['quiebre'] = Input::get("quiebre");
        $data['lejano'] = Input::get("lejano");
        $data['distrito'] = Input::get("distrito");
        $data['averia_m1'] = Input::get("averia_m1");
        $data['telefono_codclientecms'] = Input::get("telefono_codclientecms");
        $data['area2'] = Input::get("area2");
        $data['eecc_final'] = Input::get("eecc_final");
        $data["gestion_id"] = Input::get("gestion_id");
        $data['estado'] = Input::get("estado");
        $data['cr_observacion'] = Input::get("cr_observacion");
        $data['velocidad'] = Input::get("velocidad");
        $data["estado_agendamiento"] = "1-1";

        //Inicio componentes
        $arrComponentes = array();
        $cmp = Helpers::ruta(
            'cat_componente/cargar',
            'POST',
            array('codactu' => $data['codactu']), false
        );
        $cmp = Helpers::stdToArray($cmp);

        if ($cmp["rst"] == 1 and count($cmp["datos"]) > 0) {
            foreach ($cmp["datos"] as $val) {
                $arrComponentes[] = $val["nombre"];
            }
        }
        $data["componente_text"] = $arrComponentes;
        //Fin componentes

        $savedata["otdata"] = $data;

        $rot = Helpers::ruta(
            'officetrack/enviartarea', 'POST', $savedata, false
        );

        $rot = Helpers::stdToArray($rot);

        $user_id = 697;
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
        }

        if ($rot['officetrack'] == 'OK') {
            $query = "select GenerarReenvio(" . $data["gestion_id"] . "," . 
                $user_id. ",'" . 
                $data['cr_observacion'] . "')";
            $reenvio = DB::select($query);
        }
        return json_encode($rot);
    }

    private function enviarOfficeTrack($cadena)
    {
        //WebServices
        $otWsdl["tareas"]["ID"] = Config::get("ot.tareas.ID");
        $otWsdl["tareas"]["WSDL"] = Config::get("ot.tareas.WSDL");

        $otWsdl["localizacion"]["ID"] = Config::get("ot.localizacion.ID");
        $otWsdl["localizacion"]["WSDl"] = Config::get("ot.localizacion.WSDL");

        //Accesos
        $otAccess["UserName"] = Config::get("ot.UserName");
        $otAccess["Password"] = Config::get("ot.Password");

        $OfficeTrack = new OfficeTrackGetSet();
        //Existe la variable "cadena"
        $json = str_replace("&", "%26", $cadena['cadena']);
        if (isset($cadena) and !empty($json)) {
            try {
                //Validar cadena json
                $object = json_decode($json);
                if (is_object($object)) {
                    //Cadena json OK, agregar usuario y clave
                    $object->UserName = base64_decode($otAccess["UserName"]);
                    $object->Password = base64_decode($otAccess["Password"]);

                    //Convertir a XMl
                    $xml = $this->arrayToXml(
                        $object,
                        '<CreateOrUpdateTaskRequest></CreateOrUpdateTaskRequest>'
                    );

                    $OfficeTrack->set_wsdl($otWsdl['tareas']['WSDL']);
                    $tareas = $OfficeTrack->get_client();

                    $result = $tareas->CreateOrUpdateTask(array('Request' => $xml));
                    $otRes = $result->CreateOrUpdateTaskResult;
                    //Almacenar trama enviada hacia OT

                    $officetrackModel = new Officetrack();

                    $officetrackModel->registrar($json, $result->CreateOrUpdateTaskResult);

                    return $otRes;
                } else {
                    //Cadena KO
                    throw new Exception("Cadena Json no valida");
                }
            } catch (Exception $exc) {
                //Registrar error
                $this->error->saveError($exc);
            }
        }
    }

    private function arrayToXml($array, $rootElement = null, $xml = null)
    {
        $_xml = $xml;

        if ($_xml === null) {
            $_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root><root/>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) { //nested array
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            } elseif (is_object($v)) {
                $this->arrayToXml((array)$v, $k, $_xml->addChild($k));
            } else {
                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

    private function is_valid_xml($xml)
    {
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml);
        $errors = libxml_get_errors();
        return empty($errors);
    }

    public function getServer()
    {
        $Officetrack = new Officetrack();
        $Error       = new Error();

        $query_string = "";
        if ($_POST) {
            $kv = array();
            foreach ($_POST as $key => $value) {
                $kv[] = "$key=$value";
            }
            $query_string = join("&", $kv);
        } elseif ($_GET) {
            $kv = array();
            foreach ($_GET as $key => $value) {
                $kv[] = "$key=$value";
            }
            $query_string = join("&", $kv);
        }

        $fecha_registro = date('Y-m-d H:i:s');

        $sql = " INSERT INTO webpsi_officetrack.tareas_ot 
                              (tareas,fecha_registro) 
                        values (?,?) ";
        $arrayVal = [$query_string, $fecha_registro];
        //DB::insert($sql,$arrayVal);

        if (isset($_POST["forms"]) and !empty($_POST)) {
            $xml = trim($_POST["forms"]);

            //Cadena vacia
            try {
                if ($xml === "") {
                    throw new Exception("Cadena vac&iacute;a.");
                }
            } catch (Exception $exc) {
                //Registrar error
                $this->error->saveError($exc);
            }

            //XML valido
            try {
                if ($this->is_valid_xml($xml) === false) {
                    throw new Exception("Cadena XML no valida");
                }
            } catch (Exception $exc) {
                //Registrar error
                $this->error->saveError($exc);
            }

            $data['xml'] = $xml;

           // Queue::push('OfficetrackController@getProcesamientoColas',$data);
            $this->getProcesamientoColas($data);
            return  "_OK_";

        } else {
            echo "Nothing to do";
        }

    }

    public function getProcesamientoColas($data)
    {
        $Officetrack = new Officetrack();
        $Error = new Error();

        //try {
            //Captura de datos
            //$simpleXml=simplexml_load_file(public_path().'/ot_xml/inicio.xml');
            $simpleXml = simplexml_load_string($data['xml']);

//           $id = $job->getJobId();
            /**
             * Entradas y tipos
             * Asistencia:
             * Ingreso     21
             * Salida      22
             * Vacaciones 100
             * Enfermedad 102
             */
            $EventType = $simpleXml->EventType;
            $EntryType = $simpleXml->EntryType;
            $entryArray = array(21, 22, 100, 102);

            //Ubicacion
            $x = $simpleXml->EntryLocation->X;
            $y = $simpleXml->EntryLocation->Y;

            $EmployeeNumber = $simpleXml->Employee->EmployeeNumber;

            $TaskNumber = $simpleXml->Task->TaskNumber;
            $Status = $simpleXml->Task->Status;
            $Description = $simpleXml->Task->Description;
            $CustomerName = $simpleXml->Task->CustomerName;
            $Data1 = $simpleXml->Task->Data1;
            $Data2 = $simpleXml->Task->Data2;
            $Data3 = $simpleXml->Task->Data3;
            $Data4 = $simpleXml->Task->Data4;
            $Data6 = $simpleXml->Task->Data6;
            $Data7 = $simpleXml->Task->Data7;
            $Data8 = $simpleXml->Task->Data8;
            $Data10 = $simpleXml->Task->Data10;
            $Data11 = $simpleXml->Task->Data11;
            $Data12 = $simpleXml->Task->Data12;
            $Data13 = $simpleXml->Task->Data13;
            $Data14 = $simpleXml->Task->Data14;
            $Data15 = $simpleXml->Task->Data15;
            $Data16 = $simpleXml->Task->Data16;
            $Data17 = $simpleXml->Task->Data17;
            $Data18 = $simpleXml->Task->Data18;
            $Data19 = $simpleXml->Task->Data19;
            $Data20 = $simpleXml->Task->Data20;
            $Data21 = $simpleXml->Task->Data21;
            $Data22 = $simpleXml->Task->Data22;
            $Data23 = $simpleXml->Task->Data23;
            $Data24 = $simpleXml->Task->Data24;
            $Data25 = $simpleXml->Task->Data25;
            $Data26 = $simpleXml->Task->Data26;
            $Data27 = $simpleXml->Task->Data27;
            $Data28 = $simpleXml->Task->Data28;
            $StartDate = $simpleXml->Task->StartDate;
            $StartDateAge = $simpleXml->Task->StartDateAge;
            $StartDateFromEpoch = $simpleXml->Task->StartDateFromEpoch;
            $DueDate = $simpleXml->Task->DueDate;
            $DueDateAge = $simpleXml->Task->DueDateAge;
            $DueDateFromEpoch = $simpleXml->Task->DueDateFromEpoch;

            //Registrar solo si existen datos de formulario
            if (isset($simpleXml->Form->Name)) {

                $paso = trim($simpleXml->Form->Name);
                $paso_id = substr($paso, 0, 5);
                //Iniciar transaccion
                DB::beginTransaction();

                //Registrar datos de la tarea
                $date = date("Y-m-d H:i:s");
                $saveTask = array();

                if (trim($TaskNumber) != "") {

                    $insert["task_id"] = $TaskNumber;
                    $insert["cod_tecnico"] = $EmployeeNumber;
                    $insert["paso"] = $paso;
                    $insert["fecha_recepcion"] = $date;
                    $insert["cliente"] = $Data4;
                    $insert["fecha_agenda"] = $Description;

                    $saveTask = Officetrack::registrarNuevaTarea($insert);

                    if ($saveTask["estado"] === false) {
                        throw new Exception("Error al registrar tarea");
                    }
                }

                //Resultado de registro
                $doSave = array();

                /**
                 * Registrar datos Paso 1:
                 * 0001 => Comun
                 * 0009 => "Inicio Devoluciones"
                 */
                if ($paso_id === '0001-' or $paso_id === '0009-') {
                    $doSave = $this->pasoUno(
                                        $simpleXml,
                                        $saveTask,
                                        $x,
                                        $y,
                                        $TaskNumber,
                                        $date
                                    );
                }

                //Registrar datos Paso 2
                if ($paso_id === '0002-') {
                    $doSave = $this->pasoDos($simpleXml, $saveTask, $TaskNumber);
                }

                //Registrar Activacion_Decos
                if ($paso == 'Activacion_Decos(interno)') {
                    $this->activacionDecos($simpleXml, $TaskNumber, $EmployeeNumber);
                    $doSave["estado"] = true;
                }

                /* Activación o refresh de Deco */
                if ($paso == 'Refresh_Deco') {
                    $this->formularioDeco($simpleXml, $EmployeeNumber);
                    $doSave["estado"] = true;
                }

                /* Refresh de Deco por codigo cliente */
                if ($paso == 'Refresh Cliente') {
                    $this->refreshCliente($simpleXml, $EmployeeNumber);
                    $doSave["estado"] = true;
                }

                //Registrar datos Paso 3
                if ($paso_id === '0003-') {
                    $doSave = $this->pasoTres($simpleXml, $TaskNumber, $saveTask);
                }

                //Registrar Inicio - PEX
                if ($paso_id === '0004-') {
                    $doSave = $this->registroInicio(
                                        $simpleXml,
                                        $saveTask,
                                        $x,
                                        $y,
                                        $TaskNumber,
                                        $date
                                    );
                }

                //Registrar Cierre - PEX
                if ($paso_id === '0005-') {
                    $doSave = $this->registroCierre(
                                        $simpleXml,
                                        $saveTask,
                                        $TaskNumber, 
                                        $date
                                    );
                }
                /**
                 * Formulario cambio de direccion
                 * Devoluciones.
                 */
                if ($paso_id === '0008-') {
                    $doSave = $this->cambioDireccion($simpleXml, $TaskNumber);
                }

                //Formulario cierre devoluciones
                if ($paso_id === '0011-') {
                    $doSave = $this->cierreDevolucion($simpleXml, $TaskNumber, $saveTask);
                }

                if ($paso == 'Activacion_Decos') {
                    $this->activacion($simpleXml, $EmployeeNumber);
                    $doSave["estado"] = true;
                }

                //Validar correcto grabado del paso (1, 2, o 3)
                if ( isset($doSave["estado"]) && $doSave["estado"] === false) {
                    throw new Exception($doSave["msg"]);
                }
                //print_r($doSave);
                DB::commit();
                $result["estado"] = true;
                $result["msg"] = "Proyecto registrado correctamente";
                
            } //end if

            //Registrar Asistencia y entradas
            if ($EventType == 1 and in_array($EntryType, $entryArray)) {

                $fechaAsistencia = $simpleXml->EntryDate;
                $dia = substr($fechaAsistencia, 0, 2);
                $mes = substr($fechaAsistencia, 2, 2);
                $año = substr($fechaAsistencia, 4, 4);

                $hora = substr($fechaAsistencia, 8, 2);
                $min = substr($fechaAsistencia, 10, 2);
                $seg = substr($fechaAsistencia, 12, 2);

                $fecha = $año . "-" . $mes . "-" . $dia . " " . $hora . 
                ":" . $min . ":" . $seg;

                $Officetrack->asistencia(
                    $EntryType,
                    $fecha,
                    $simpleXml->EntryLocation->Address,
                    $x,
                    $y,
                    $simpleXml->Data,
                    $simpleXml->Employee->FirstName,
                    $simpleXml->Employee->EmployeeNumber
                );
            } //end registrar asistencia

//            $job->delete();

        /*} catch (Exception $error) {

            if ($job->attempts() > 1)  $job->delete();
            
            DB::rollBack();
            //Registrar error
            $Error->registrar(
                "err_queue",
                $error->getMessage(),
                $error->getFile() . "(" . $error->getLine() . ")"
            );
        }*/

    }


    protected function pasoUno($simpleXml, $saveTask, $x, $y, $TaskNumber, $date)
    {
        $Officetrack = new Officetrack();
        $casa_img = array(1 => "", 2 => "", 3 => "");

        if (isset($simpleXml->Files->File)) {
            $n = 1;
            foreach ($simpleXml->Files->File as $obj) {
                $casa_img[$n] = $obj->Data;
                $n++;
            }
        }

        $observacion = "";

        //try {
            //Agregar movimiento tecnico en sitio
            $sql = "SELECT psi.GenerarInicio($TaskNumber, '$x', '$y', '$date')";
            DB::select($sql);
        /*} catch (PDOException  $error) {
            //Registrar error
            $this->error->saveError($error);
        }*/

        //Generar imagenes
        $nimg = 1;
        foreach ($casa_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '8', '$nombre', now()) ";
                DB::insert($sql);

            }
        }

        return $Officetrack->registrarPasoUno($saveTask["id"], $x, $y, $observacion, $casa_img);
    }

    protected function pasoDos($simpleXml, $saveTask, $TaskNumber)
    {
        $Officetrack = new Officetrack();
        //Datos
        $motivo = "";
        $observaciones = "";

        if ($simpleXml->Form->Fields->Field->Id == 'Motivo Problema') {
            $motivo = $simpleXml->Form->Fields->Field->Value;
        }

        if ($simpleXml->Form->Fields->Field->Id == 'Observaciones') {
            $observaciones = $simpleXml->Form->Fields->Field->Value;
        }

        //Imagenes
        $tap_img = array(1 => "", 2 => "", 3 => "");
        $modem_img = array(1 => "", 2 => "", 3 => "");
        $tv_img = array(1 => "", 2 => "", 3 => "");
        $prob_img = array(1 => "", 2 => "", 3 => "");

        if (isset($simpleXml->Files->File)) {
            $nt = 1;
            $nm = 1;
            $nv = 1;
            $np = 1;
            foreach ($simpleXml->Files->File as $obj) {

                if ($obj->Id == "Tap") {
                    $tap_img[$nt] = $obj->Data;
                    $nt++;
                }

                if ($obj->Id == "Conexion Modem") {
                    $modem_img[$nm] = $obj->Data;
                    $nm++;
                }

                if ($obj->Id == "Conexion TV") {
                    $tv_img[$nv] = $obj->Data;
                    $nv++;
                }

                if ($obj->Id == "Problema") {
                    $prob_img[$np] = $obj->Data;
                    $np++;
                }

            }
        }

        //Generar imagenes paso 2
        $nimg = 2;
        foreach ($tap_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '2', '$nombre', now()) ";
                DB::insert($sql);

            }
        }

        foreach ($modem_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '3', '$nombre', now()) ";
                DB::insert($sql);
            }
        }

        foreach ($tv_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '4', '$nombre', now()) ";
                DB::insert($sql);

            }
        }

        foreach ($prob_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '5', '$nombre', now()) ";
                DB::insert($sql);
            }
        }

        return $Officetrack->registrarPasoDos(
            $saveTask["id"],
            $tap_img,
            $modem_img,
            $tv_img,
            $prob_img,
            $motivo,
            $observaciones
        );
    }

    protected function activacionDecos($simpleXml, $TaskNumber, $EmployeeNumber)
    {
        $fecha_registro = date("Y-m-d H:i:s");

        //Serie y Tarjeta
        $serie = "";
        $tarjeta = "";

        $parImpar = 0;
        foreach ($simpleXml->Form->Fields->Field as $key => $val) {

            if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie deco') {
                $serie = $val->Value;
            }

            if (substr(trim(strtolower($val->Id)), 0, 13) == 'serie tarjeta') {
                $tarjeta = $val->Value;
            }

            $parImpar++;

            $catalogoDecos = new CatalogoDecos;
            $catalogoDecos['gestion_id'] = $TaskNumber;
            $catalogoDecos['carnet'] = $EmployeeNumber;
            $catalogoDecos['serie'] = $serie;
            $catalogoDecos['tarjeta'] = $tarjeta;
            $catalogoDecos['cliente'] = 0;
            $catalogoDecos['fecha_registro'] = $fecha_registro;
            $catalogoDecos['accion'] = "activacion";
            $catalogoDecos['activo'] = 0;

            //Registrar Deco
            if ($parImpar % 2 === 0) {
                $saveDeco = $catalogoDecos->save();
                $serie = "";
                $tarjeta = "";
            }

        }
    }

    protected function formularioDeco($simpleXml, $EmployeeNumber)
    {
        $decoArray = array();
        $fecha_registro = date("Y-m-d H:i:s");

        foreach ($simpleXml->Form->Fields->Field as $key => $val) {

            if (substr(trim(strtolower($val->Id)), -5) == 'tarea') {
                $decoArray["tarea"] = $val->Value;
            }

            if (trim(strtolower($val->Id)) == 'accion') {
                $decoArray["accion"] = $val->Value;
            }

            if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie deco') {
                $decoArray["deco"][] = $val->Value;
            }

        }

        //Registro
        if (!empty($decoArray) and count($decoArray) > 0) {

            $catalogoDecos = new CatalogoDecos;
            $catalogoDecos['gestion_id'] = $decoArray["tarea"];
            $catalogoDecos['carnet'] = $EmployeeNumber;
            $catalogoDecos['tarjeta'] = "";
            $catalogoDecos['cliente'] = "";
            $catalogoDecos['fecha_registro'] = $fecha_registro;
            $catalogoDecos['accion'] = strtolower(trim($decoArray["accion"]));
            $catalogoDecos['activo'] = 0;

            foreach ($decoArray["deco"] as $val) {
                $catalogoDecos['serie'] = $val;
                $saveDeco = $catalogoDecos->save();
            }

        }
    }

    protected function refreshCliente($simpleXml, $EmployeeNumber)
    {
        $decoArray = array();
        $fecha_registro = date("Y-m-d H:i:s");
        $codigo_cliente = "";
        foreach ($simpleXml->Form->Fields->Field as $key => $val) {
            if (trim($val->Id) == 'Codigo de Cliente CMS') {
                $codigo_cliente = $val->Value;
            }
        }

        //Registro
        if ($codigo_cliente != '') {
            $catalogoDecos = new CatalogoDecos;
            $catalogoDecos['gestion_id'] = 0;
            $catalogoDecos['carnet'] = $EmployeeNumber;
            $catalogoDecos['serie'] = 0;
            $catalogoDecos['tarjeta'] = 0;
            $catalogoDecos['cliente'] = $codigo_cliente;
            $catalogoDecos['fecha_registro'] = $fecha_registro;
            $catalogoDecos['accion'] = "refresh";
            $catalogoDecos['activo'] = 0;
            $saveDeco = $catalogoDecos->save();

        }
    }

    protected function pasoTres($simpleXml, $TaskNumber, $saveTask)
    {
        $Officetrack = new Officetrack();
        //Datos
        $estado = "";
        $observaciones = "";
        $tvadicional = "";
        $materiales = array();

        //$fo = fopen("material_prueba.txt", "w+");
        foreach ($simpleXml->Form->Fields->Field as $key => $val) {
            if ($val->Id == 'Estado') {
                $estado = $val->Value;
            } elseif ($val->Id == 'Observaciones') {
                $observaciones = $val->Value;
            } else {

                //fwrite($fo, "Header:" . $val->Id . "\r\n");
                $arrMat = explode("_", $val->Id);
                $material = $arrMat[0];
                $material_id = 0;
                $materialArray = $Officetrack->buscarMateriales($material);
                foreach ($materialArray["data"] as $dbIndex => $dbField) {
                    $material_id = $dbField["id"];
                    //fwrite($fo, "Id:" . $material_id . "\r\n");
                }

                if ($material_id != 0) {
                    $materiales[$material_id][strtolower($arrMat[1])] = $val->Value;
                }
            }
        }
        //fclose($fo);

        $final_img = array(1 => "", 2 => "", 3 => "");
        $firma_img = "";
        $boleta_img = "";

        if (isset($simpleXml->Files->File)) {
            $n = 1;
            foreach ($simpleXml->Files->File as $obj) {

                if (trim($obj->Id) == "Trabajo Final") {
                    $final_img[$n] = $obj->Data;
                    $n++;
                }

                if (trim($obj->Id) == "Firma Cliente") {
                    $firma_img = $obj->Data;
                }
            }
        }


        $idPasoTres = 0;
        $fecha_registro = date("Y-m-d H:i:s");
        if (!empty($doSave)) {
            $idPasoTres = $doSave["data"]["id"];
        }

        //Registro de materiales
        if (!empty($materiales)) {
            $fecha_registro = date("Y-m-d H:i:s");
            foreach ($materiales as $matId => $val) {
                //Asignar valores
                if (isset($val["utilizado"])) {
                    if (trim($val["utilizado"]) == "") {
                        $val["utilizado"] = 0;
                    }
                } else {
                    $val["stock"] = 0;
                }

                if (isset($val["stock"])) {
                    if (trim($val["stock"]) == "") {
                        $val["stock"] = 0;
                    }
                } else {
                    $val["stock"] = 0;
                }

                //Registrar
                $regmat = $Officetrack->registrarDetalleMateriales(
                    $idPasoTres, 
                    $matId, 
                    $val["utilizado"], 
                    $val["stock"], 
                    $fecha_registro, 
                    ''
                );
            }
        }

        //Agregar movimiento tecnico en sitio
        $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estado', '$fecha_registro')";
        DB::select($sql);

        //Generar imagenes paso 3
        $nimg = 3;
        foreach ($final_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $index
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($index) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '6', '$nombre', now()) ";
                DB::insert($sql);
            }
        }

        if (trim($firma_img) != '') {
            $Officetrack->save_img_curl(
                $firma_img,
                $TaskNumber,
                $saveTask["id"],
                $nimg,
                '1'
            );

            $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
            $saveTask["id"] . "_1.jpg";

            $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                . " values (" . $saveTask["id"] . ", '7', '$nombre', now()) ";
            DB::insert($sql);
        }
        return $Officetrack->registrarPasoTres(
            $saveTask["id"],
            $estado,
            $observaciones,
            $tvadicional,
            $final_img,
            $firma_img,
            $boleta_img
        );
    }

    protected function registroInicio($simpleXml, $saveTask, $x, $y, $TaskNumber, $date)
    {
        $Officetrack = new Officetrack;
        $casa_img = array(1 => "", 2 => "", 3 => "");

        if (isset($simpleXml->Files->File)) {
            $n = 1;
            foreach ($simpleXml->Files->File as $obj) {
                $casa_img[$n] = $obj->Data;
                $n++;
            }
        }

        //Datos
        $problema = "";

        if ($simpleXml->Form->Fields->Field->Id == 'Problema') {
            $problema = $simpleXml->Form->Fields->Field->Value;
        }


        //Agregar movimiento tecnico en sitio
        $sql = "SELECT psi.GenerarInicio($TaskNumber, '$x', '$y', '$date')";
        DB::select($sql);

        return $Officetrack->registrarPasoUno(
            $saveTask["id"], $x, $y, $problema, $casa_img);
    }

    protected function registroCierre($simpleXml, $saveTask, $TaskNumber, $date)
    {
        $Officetrack = new Officetrack();

        //Datos
        $estado = "";
        $observaciones = "";
        $tvadicional = "";

        foreach ($simpleXml->Form->Fields->Field as $key => $val) {
            if ($val->Id == 'Estado') {
                $estado = $val->Value;
            } elseif ($val->Id == 'Observaciones') {
                $observaciones = $val->Value;
            }
        }

        $final_img = array(1 => "", 2 => "", 3 => "");
        $firma_img = "";
        $boleta_img = "";

        if (isset($simpleXml->Files->File)) {
            $n = 1;
            foreach ($simpleXml->Files->File as $obj) {

                if (trim($obj->Id) == "Foto Trabajo Final") {
                    $final_img[$n] = $obj->Data;
                    $n++;
                }

                if (trim($obj->Id) == "Firma Cliente") {
                    $firma_img = $obj->Data;
                }
            }
        }
        //Agregar movimiento tecnico en sitio
        $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estado', '$date')";

        DB::select($sql);

        return $Officetrack->registrarPasoTres(
            $saveTask["id"],
            $estado,
            $observaciones,
            $tvadicional,
            $final_img,
            $firma_img,
            $boleta_img
        );
    }

    protected function cambioDireccion($simpleXml, $TaskNumber)
    {
        //data
        $ubicacion = "";
        $direccion = "";
        $referencia = "";
        $nodo = "";
        $troba = "";
        $tap = "";
        $amplificador = "";
        $nuevo_x = "";
        $nuevo_y = "";

        $carnet = $simpleXml->Employee->EmployeeNumber;

        $actualiza_ubicacion = false;
        foreach ($simpleXml->Form->Fields->Field as $key => $val) {
            $id = $val->Id;
            $value = $val->Value;

            if ($id == 'actualiza_ubicacion' and $value == 'si') {
                $actualiza_ubicacion = true;
            }

            if ($id == 'ubicacion') {
                $ubicacion = $val->Value;
                $ubicacionArray = explode(",", trim($ubicacion));

                if (isset($ubicacionArray[0])) {
                    $nuevo_x = $ubicacionArray[0];
                }

                if (isset($ubicacionArray[1])) {
                    $nuevo_x = $ubicacionArray[1];
                }
            }

            if ($id == 'direccion') {
                $direccion = $val->Value;
            }

            if ($id == 'referencia') {
                $referencia = $val->Value;
            }

            if ($id == 'nodo') {
                $nodo = $val->Value;
            }

            if ($id == 'troba') {
                $troba = $val->Value;
            }

            if ($id == 'tap') {
                $tap = $val->Value;
            }

            if ($id == 'amplificador') {
                $amplificador = $val->Value;
            }
        }

        //Actualizar xy de direccion y reenviar al tecnico
        if ($actualiza_ubicacion) {
            //1. Updates tablas y guardar histórico
            $Officetrack = new Officetrack();
            return $upd = $Officetrack->cambiarDireccion(
                $TaskNumber,
                $carnet,
                $nuevo_x,
                $nuevo_y,
                $direccion,
                $referencia
            );

            //2. Reenvío al técnico

        }
    }

    protected function cierreDevolucion($simpleXml, $TaskNumber, $saveTask)
    {
        $Officetrack = new Officetrack();
        //Datos
        $estado = "";
        $observaciones = "";
        $tvadicional = "";
        $materiales = array();

        foreach ($simpleXml->Form->Fields->Field as $key => $val) {
            if ($val->Id == 'motivocierre') {
                $estado = $val->Value;
            } elseif ($val->Id == 'observaciones') {
                $observaciones = $val->Value;
            } elseif ($val->Id == 'TVadicional') {
                $tvadicional = $val->Value;
            } else {

                $arrMat = explode("_", $val->Id);
                $material = $arrMat[0];
                $material_id = 0;
                $materialArray = $Officetrack->buscarMateriales($material);

                foreach ($materialArray["data"] as $dbIndex => $dbField) {
                    $material_id = $dbField["id"];
                }
                if ($material_id != 0) {
                    $materiales[$material_id][strtolower($arrMat[1])] = (string)$val->Value;
                }
                $materiales[$material_id]['serie'] = '';

                //Caso de cable modem
                if ($arrMat[1] == 'modem') {
                    $materiales[$material_id]['utilizado'] = 1;
                    $materiales[$material_id]['stock'] = '';
                    $materiales[$material_id]['serie'] = (string)$val->Value;
                }

            }

        }
        $final_img = array(1 => "", 2 => "", 3 => "");
        $firma_img = "";
        $boleta_img = "";

        if (isset($simpleXml->Files->File)) {
            $n = 1;

            foreach ($simpleXml->Files->File as $obj) {

                if (trim($obj->Id) == "Trabajo Final") {
                    $final_img[$n] = $obj->Data;
                    $n++;
                }
                //Boleta
                if (trim($obj->Id) == "Boleta") {
                    $boleta_img = $obj->Data;
                }

                if (trim($obj->Id) == "Firma Cliente") {
                    $firma_img = $obj->Data;
                }

            }
        }

        $idPasoTres = 0;
        if (!empty($doSave)) {
            $idPasoTres = $doSave["data"]["id"];
        }

        $fecha_registro = date("Y-m-d H:i:s");
        //Registro de materiales
        if (!empty($materiales)) {
            foreach ($materiales as $matId => $val) {
                //Asignar valores
                if (trim($val["utilizado"]) == "") {
                    $val["utilizado"] = 0;
                }
                if (isset($val["stock"])) {
                    if (trim($val["stock"]) == "") {
                        $val["stock"] = 0;
                    }
                } else {
                    $val["stock"] = 0;
                }

                //Registrar
                $regmat = $Officetrack->registrarDetalleMateriales(
                    $idPasoTres, 
                    $matId, 
                    $val["utilizado"],
                    $val["stock"], 
                    $fecha_registro, $val["serie"]
                );
            }

        }

        //Agregar movimiento pre-liquidar
        $estadoarray = explode("||", $estado);
        $sql = "SELECT psi.GenerarCierre($TaskNumber, '$estadoarray[1]', '$fecha_registro')";
        DB::select($sql);
        

        //Generar imagenes paso 3
        $nimg = 3;
        $indice = 1;
        foreach ($final_img as $index => $val) {
            if (trim($val) != '') {
                $Officetrack->save_img_curl(
                    $val,
                    $TaskNumber,
                    $saveTask["id"],
                    $nimg,
                    $indice
                );

                $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . 
                $saveTask["id"] . "_" . ($indice) . ".jpg";

                $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                    . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                    . " values (" . $saveTask["id"] . ", '6', '$nombre', now()) ";
                DB::insert($sql);

                $indice++;
            }
        }

        if (trim($firma_img) != '') {
            $Officetrack->save_img_curl(
                $firma_img,
                $TaskNumber,
                $saveTask["id"],
                $nimg,
                $indice
            );

            $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . $saveTask["id"]
             . "_$indice.jpg";

            $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                . " values (" . $saveTask["id"] . ", '7', '$nombre', now()) ";
            DB::insert($sql);

            $indice++;
        }

        if (trim($boleta_img) != '') {
            $Officetrack->save_img_curl(
                $boleta_img,
                $TaskNumber,
                $saveTask["id"],
                $nimg,
                $indice
            );

            $nombre = "p0" . $nimg . "/g" . $TaskNumber . "/i" . $saveTask["id"]
             . "_$indice.jpg";

            $sql = "INSERT INTO webpsi_officetrack.imagenes_tareas "
                . " (tarea_id, imagen_tipo_id, nombre, fecha_creacion) "
                . " values (" . $saveTask["id"] . ", '9', '$nombre', now()) ";
            DB::insert($sql);
        }

        //Cantidad de visitas y reenvio
        /**
         * Validar devolucion por cliente ausente.
         */
        $strPos = false;
        if (isset($estadoarray[1])) {
            $strPos = strpos(strtolower($estado), 'ausente');
        }
        //$strPos = strpos(strtoupper($estado), 'DT02');
        if ($strPos !== false) {
            $CurlNovus = new CurlNovus();
            $visitaStr = $CurlNovus->calcular_visitas("$TaskNumber");
            $visitaObj = $visitaStr;

            //Datos de la gestion de la actuacion
            $taskStr = $CurlNovus->obtener_actu("$TaskNumber");
            $taskObj = $taskStr;
            //Reenviar al tecnico
            if ($visitaObj->rst == 1 and $visitaObj->cantidad > 0) {
                //Agregar movimiento "Agendado con tecnico"
                $sql = "SELECT psi.GenerarReasignacion($TaskNumber)";
                DB::select($sql);

                $reenviarStr = $CurlNovus->reasignar_trabajo("$TaskNumber");

            } else if ($visitaObj->rst == 1 and $visitaObj->cantidad == 0 
                        and $visitaObj->cantidad != '') {
                //invocar funcion de jorge, para cambiar de empresa -> retenciones
                //act movimiento gestinoes detalles, ult mov.

                //Para ciertos quiebres cambia a empresa "RETENCIONES"
                $quiebreArray = array(
                    12,
                );
                $quiebreId = $taskObj->datos->quiebre_id;

                if (array_search($quiebreId, $quiebreArray) !== false) {
                    $sql = "SELECT psi.ActEmpresa($TaskNumber)";
                    DB::select($sql);
                }

            }

        }

        /**
         * Para "Cliente NO DESEA" (DT06)
         * pasa a RETENCIONES
         */
        if (isset($estadoarray[0]) and $estadoarray[0] == 'DT06') {
            $sql = "SELECT psi.ActEmpresa($TaskNumber)";
            DB::select($sql);
        }

        return $Officetrack->registrarPasoTres(
            $saveTask["id"],
            $estado,
            $observaciones,
            $tvadicional,
            $final_img,
            $firma_img,
            $boleta_img
        );
    }

    protected function activacion($simpleXml, $EmployeeNumber)
    {
        $codactu='';
        if ($simpleXml->Form->Fields->Field[0]->Id == 'cod_cliente') {
            $codcliente = $simpleXml->Form->Fields->Field[0]->Value;
        }
        if ($simpleXml->Form->Fields->Field[1]->Id == 'requerimiento') {
            $codactu = $simpleXml->Form->Fields->Field[1]->Value;
        }

        if ( trim($codactu) != '') {
            $Validacion_activa = new Validacion_activa();
            $Validacion_activa->codactu = $codactu;
            $codactuArray = $Validacion_activa->buscar();

            foreach ($codactuArray["data"] as $dbIndex => $dbField) {
                $estado_ofsc_id = $dbField["estado_ofsc_id"];
                $carnet_tmp = $dbField["carnet_tmp"];
                $gestion_id = $dbField["gestion_id"];
            }


            if ($estado_ofsc_id == '2' && $carnet_tmp == $EmployeeNumber) {
                //insertar catalogo decos
                $fecha_registro = date("Y-m-d H:i:s");
                //Serie y Tarjeta
                $serie = "";
                $tarjeta = "";
                $parImpar = 0;

                foreach ($simpleXml->Form->Fields->Field as $key => $val) {

                    if (substr(trim(strtolower($val->Id)), 0, 10) == 'serie_deco') {
                        $serie = $val->Value;
                    }

                    if (substr(trim(strtolower($val->Id)), 0, 13) == 'serie_tarjeta') {
                        $tarjeta = $val->Value;
                    }
                                        
                    $catalogoDecos = new CatalogoDecos;
                    $catalogoDecos['gestion_id'] = $gestion_id;
                    $catalogoDecos['carnet'] = $EmployeeNumber;
                    $catalogoDecos['serie'] = $serie;
                    $catalogoDecos['tarjeta'] = $tarjeta;
                    $catalogoDecos['cliente'] = $codcliente;
                    $catalogoDecos['fecha_registro'] = $fecha_registro;
                    $catalogoDecos['accion'] = "activacion";
                    $catalogoDecos['activo'] = 0;
                    $parImpar++;

                    //Registrar Deco
                    if ($parImpar % 2 === 0 && $parImpar > 3) {
                        $saveDeco = $catalogoDecos->save();
                        $serie = "";
                        $tarjeta = "";
                    }

                }
                //fclose($fo);
            } else {
                $validacion_activa = new Validacion_activa();
                $validacion_activa->employee = $EmployeeNumber;
                $phoneArray = $validacion_activa->consultarphone();
                foreach ($phoneArray["data"] as $dbIndex => $dbField) {
                    $nombre = $dbField["ape_paterno"];
                    $phone = $dbField["celular"];
                }
                if ($estado_ofsc_id != '2') {
                    $msj = "Tecnico: " . $nombre . ", el requerimiento: " . $codactu .
                        " no está Iniciado ";
                } else {
                    $msj = "Tecnico: " . $nombre . ", usted no tiene permisos " .
                        "para el requerimiento: " . $codactu;
                }

                $CurlNovus = new CurlNovus();
                $notificarStr = $CurlNovus->notificar_noregistro($phone, $msj);
            }
        } else {
            $validacion_activa = new Validacion_activa();
            $validacion_activa->employee = $EmployeeNumber;
            $phoneArray = $validacion_activa->consultarphone();
            foreach ($phoneArray["data"] as $dbIndex => $dbField) {
                $nombre = $dbField["ape_paterno"];
                $phone = $dbField["celular"];
            }

            $msj = "Tecnico: " . $nombre . ", " .
                "No se encontraron registros, verifique los " .
                "datos ingresados e intente nuevamente.";

            $CurlNovus = new CurlNovus();
            $notificarStr = $CurlNovus->notificar_noregistro($phone, $msj);
        }
    }

    public function getTasks($start)
    {
        $query = 'SELECT * FROM webpsi_officetrack.tareas
                  ORDER BY id DESC
                  LIMIT ' . $start . ', 10';

        $count = 'SELECT COUNT(*) total FROM webpsi_officetrack.tareas';

        $rdata = DB::select($query);
        $rcount = DB::select($count);

        $datos["total"] = $rcount[0]->total;
        $datos["tareas"] = $rdata;

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }

    public function getErrors($start)
    {
        $query = 'SELECT * FROM webpsi_officetrack.errores
                  ORDER BY id DESC
                  LIMIT ' . $start . ', 10';

        $count = 'SELECT COUNT(*) total FROM webpsi_officetrack.errores';

        $rdata = DB::select($query);
        $rcount = DB::select($count);

        $datos["total"] = $rcount[0]->total;
        $datos["errors"] = $rdata;

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }
}
