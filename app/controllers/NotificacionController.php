<?php
use Legados\models\SolicitudTecnicaUltimo as SolicitudUltimoMovimiento;
use Legados\models\LegadoConfigVariable as LegadoConfigVariable;
use Legados\Repositories\StMensajeRepository as StMensajeRepo;


class NotificacionController extends \BaseController
{
    protected $_errorController;
    protected $_estadoRespuesta;

    public function __construct(
        ErrorController $errorController,
        EstadoRespuesta $estadoRespuesta
    ) {
        $this->_errorController = $errorController;
        $this->_estadoRespuesta = $estadoRespuesta;
    }

    public function failed()
    {
        // Called when the job is failing...
        Log::error('Job failed!');
    }

    public function fire($job, $data)
    {
        $time_start = microtime(true);
        //print_r($data);

        $id = $job->getJobId();
  
        for ($i=0; $i < count($data); $i++) {

            $this->_estadoRespuesta->recibir($data[$i]);
            $subject = $this->_estadoRespuesta->getSubject();

            if ($this->_estadoRespuesta->getCodactu() !='') {
                //  && $this->_estadoRespuesta->getAid()!=0
                $detalle = $this->_estadoRespuesta->responder();

            } elseif ($this->_estadoRespuesta->getSubject()=='INT_SMS'
                || $this->_estadoRespuesta->getSubject()=='TOA_VALIDATE_COORDS'
                || $this->_estadoRespuesta->getSubject()=='TOA_ACTIVATE_ROUTE') {
                $detalle = $this->_estadoRespuesta->responder();
            }


            if ($subject == 'TOA_OUTBOUND_PRE_NOTDONE') {

                $codActu = $this->_estadoRespuesta->getCodactu();
                $objsolicitud = $this->_estadoRespuesta->getSolicitudTecnica();

                if (!is_null($objsolicitud)) {
                    $solicitud_tecnica_id = $this->_estadoRespuesta->getSolicitudTecnica()->id;
                    $ultimo_movimiento = SolicitudUltimoMovimiento::where('solicitud_tecnica_id', $solicitud_tecnica_id)->first();

                    $fecha_actualizacion_old = $ultimo_movimiento->updated_at->format('Y-m-d H:i:s');
                    $codigo_ofsc = (!is_null($ultimo_movimiento->motivoOfsc))? $ultimo_movimiento->motivoOfsc->codigo_ofsc: '' ;

                    if ($codigo_ofsc != '') {
                        if ($ultimo_movimiento->motivoOfsc->tipo_tratamiento_legado != 1) {
                            $datos['codigo_ofsc'] = $codigo_ofsc;
                            $datos['fecha_actualizacion_old'] = $fecha_actualizacion_old;
                            $datos['solicitud_tecnica_id'] = $solicitud_tecnica_id;
                            $datos['codActu'] = $codActu;
                            $tiempo_cierre_st = Cache::get('tiempo_cierre_st');
                            if (!$tiempo_cierre_st) {
                                $tiempo_cierre_st = Cache::remember('tiempo_cierre_st', 24*60*60, function () {
                                    return $this->getTiempoAutomatico('auto_liquidacion');
                                });
                            }
                            Queue::later($tiempo_cierre_st, 'AutoCierreSolicitudController@cerrarSolicitud', $datos, 'autocierre_solicitud');
                        }
                    }
                } else {
                    $datosLog = ["subject" => $subject, "st" => $this->_estadoRespuesta->st];
                    Log::useDailyFiles(storage_path().'/logs/notificacion_nula.log');
                    Log::info([$datosLog]);
                }
                
            }

            if ($subject == 'WO_PRE_COMPLETED') {
                $solicitud_tecnica_id = $this->_estadoRespuesta->getSolicitudTecnica()->id;
                $st_ultimo = SolicitudUltimoMovimiento::where('solicitud_tecnica_id', $solicitud_tecnica_id)->first();
                $fecha_actualizacion_old = $st_ultimo->updated_at->format('Y-m-d H:i:s');
                if ($st_ultimo->actividad_id == 2) {
                    $datos['fecha_actualizacion_old'] = $fecha_actualizacion_old;
                    $datos['solicitud_tecnica_id'] = $solicitud_tecnica_id;

                    $tiempo_liquidacion_st = Cache::get('tiempo_liquidacion_st');
                    if (!$tiempo_liquidacion_st) {
                        $tiempo_liquidacion_st = Cache::remember(
                            'tiempo_liquidacion_st',
                            24*60*60,
                            function () {
                                return $this->getTiempoAutomatico('auto_liquidacion');
                            }
                        );
                    }
                    Queue::later($tiempo_liquidacion_st, 'AutoLiquidacionController@liquidarSolicitud', $datos, 'autoliquidacion_solicitud');
                }
                
            }

            /*$transaccionesGoRepo = new TransaccionesGoRepo();
            $transaccionesGoRepo->registrarLogsFlujo($log_m_body->xa_identificador_st, $flujo, 2, null, $mensaje->message_id, $mensaje->send_to, null, $horaLlegada);*/

        }

        $StMensajeRepo = new StMensajeRepo();
        $StMensajeRepo->registrarLog($data[0], null, '4.3', 1);

        $this->finish($time_start);
        $job->delete();
    }

    public function finish($time_start)
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        if ($execution_time > 62) {
            DB::table('errores')->insert([
                "date" => date("Y-m-d H:i:s"),
                "file" => realpath(dirname(__FILE__))."/".basename(__FILE__),
                "message" => "Proceso en cola 'outbound_notification' se está demorando mas de 60 segundos.(Tiempo de proceso: ".$execution_time." segundos)",
                "line" => "function fire()",
                "url" => "",
                "usuario_id" => 952
            ]);
        }
    }

    public function getTiempoAutomatico($nombre)
    {
        $tiempo_automatico_st = LegadoConfigVariable::where('nombre', '=', $nombre)->pluck('valor');
        return $tiempo_automatico_st;
    }
}
