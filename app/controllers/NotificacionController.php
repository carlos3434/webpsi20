<?php

class NotificacionController extends \BaseController
{
    protected $_errorController;
    protected $_estadoRespuesta;

    public function __construct(ErrorController $errorController,
        EstadoRespuesta $estadoRespuesta)
    {
        $this->_errorController = $errorController;
        $this->_estadoRespuesta = $estadoRespuesta;
    }
    public function failed()
    {
        // Called when the job is failing...
        Log::error('Job failed!');
    }
    public function fire($job, $data)
    {

        $id = $job->getJobId();
        
        // try {        
            for ( $i=0; $i < count($data); $i++ ) {
                
                // $idm=Mensaje::crear2($id, $data[$i]);
                Mensaje::crear($id, $data[$i]);
                // exit;
                $this->_estadoRespuesta->recibir($data[$i]);

                if ( $this->_estadoRespuesta->getCodactu()!='') {
                    //  && $this->_estadoRespuesta->getAid()!=0  
                    // no todas llegan aid
                    $detalle = $this->_estadoRespuesta->responder();

                } elseif ($this->_estadoRespuesta->getSubject()=='INT_SMS'
                    || $this->_estadoRespuesta->getSubject()=='TOA_VALIDATE_COORDS') {
                    $detalle = $this->_estadoRespuesta->responder();
                }




            }
        // DB::commit();
        $job->delete();



        // } catch (Exception $exec) {
        //     Mensaje::delete_u($idm);
            // echo " rooolll ";
            // echo $idm;
            // $job->delete();
            
            // echo "lplpxxxxx";
            // si los intentos de fallido es mayor de 1
            // if ($job->attempts() > 1) {
            //     Auth::loginUsingId(697);
            //     $this->_errorController->saveError($exec);
            //     Auth::logout();
            //     $job->delete();
            //     //registrar en log de errores
            // } else {
            //     //Log::error('Job failed!'.$exec);
            //     //ejecutar la cola despues de 30 segundos
            //     $job->release(30);
            // }

        // }
    }
}