<?php

class TecnicosOfscController extends BaseController
{

    /**
     * mostrar recurso especifico
     */
    public function show($resourceId)
    {
        $hoy = date("Y-m-d");
        $hoy = preg_replace("/-/", "", $hoy);
        $tecnicosFile = public_path()."/json/tecnicos/".$resourceId."/".$hoy.".json";
        if (!File::exists($tecnicosFile)) {
            
            $json= OfscHelper::getFileResource("PR");
            $tecnicosofsc = [];
            foreach ($json as $key => $value) {
                if ($value["parent_id"] == $resourceId) {
                    $tecnicosofsc[$value["id"]] = $value;
                }
            }
        } else {
            $json = File::get($tecnicosFile);
            $tecnicosofsc = json_decode($json, true);
        }
        $tiemporango  = [1200, 7200, 14400];
        foreach ($tecnicosofsc as $key => $value) {
            $horaactual = date("Y-m-d H:i:s");
            $tiempoactual = strtotime($horaactual);
            $tiempotecnico = isset($value["time"])? strtotime($value["time"]) : 100000;

            $tiemposegundos = $tiempoactual - $tiempotecnico;
            $tecnicosofsc[$key]["tiemposegundos"] = $tiemposegundos;
            if ($tiemposegundos <= $tiemporango[0]) {
                $tecnicosofsc[$key]["estado"] = 1;
            } else if ($tiemposegundos <= $tiemporango[1]) {
                $tecnicosofsc[$key]["estado"] = 2;
            } else if ($tiemposegundos <= $tiemporango[2]) {
                $tecnicosofsc[$key]["estado"] = 3;
            } else {
                $tecnicosofsc[$key]["estado"] = 4;
            }
        }
        return json_encode($tecnicosofsc);
    }

    public function postListadotecnico()
    {
        $hoy = date("Ymd");
        $buckets = Input::has("buckets")? Input::get("buckets") : [];
        if (count($buckets) == 0) {
            return json_encode([]);
        }
        $tecnicosofsc = [];
        foreach ($buckets as $key => $value) {
            $tecnicosFile = public_path()."/json/tecnicos/".$value."/".$hoy.".json";
            if (!File::exists($tecnicosFile)) {
                $tecnicosofsctmp = [];
                $json= OfscHelper::getFileResource("PR");
                
                foreach ($json as $key2 => $value2) {
                    if ($value2["parent_id"] == $value) {
                        $tecnicosofsctmp[$value2["id"]] = $value2;
                    }
                }
            } else {
                $json = File::get($tecnicosFile);
                $tecnicosofsctmp = json_decode($json, true);
            }
            $tiemporango  = [1200, 7200, 14400];
            foreach ($tecnicosofsctmp as $key2 => $value2) {
                $tecnicosofsctmp[$key2]["coord_x"] = isset($tecnicosofsctmp[$key2]["coord_x"])? $tecnicosofsctmp[$key2]["coord_x"] : 0;
                $tecnicosofsctmp[$key2]["coord_y"] = isset($tecnicosofsctmp[$key2]["coord_y"])? $tecnicosofsctmp[$key2]["coord_y"] : 0;
                $tecnicoPsi = Tecnico::where('carnet_tmp', $key2)
                    ->where('estado', 1)
                    ->first();
                if (!is_null($tecnicoPsi)) {
                    $tecnicosofsctmp[$key2]["datos"] = $tecnicoPsi;
                    $tecnicosofsctmp[$key2]["tecnico_id"] = $tecnicoPsi->id;
                } else {
                    $tecnicosofsctmp[$key2]["datos"] = null;
                    $tecnicosofsctmp[$key2]["tecnico_id"] = null;
                }

                $horaactual = date("Y-m-d H:i:s");
                $tiempoactual = strtotime($horaactual);
                $tiempotecnico = isset($value2["time"])? strtotime($value2["time"]) : 100000;
                $tiempotecnicotoa = isset($value2["timeToa"])? strtotime($value2["timeToa"]) : 100000;
                $tiempotecnicoot = isset($value2["timeOt"])? strtotime($value2["timeOt"]) : 100000;

                $tiemposegundos = $tiempoactual - $tiempotecnico;
                if ($tiemposegundos <= $tiemporango[0]) {
                    $tecnicosofsctmp[$key2]["estado"] = 1;
                } else if ($tiemposegundos <= $tiemporango[1]) {
                    $tecnicosofsctmp[$key2]["estado"] = 2;
                } else if ($tiemposegundos <= $tiemporango[2]) {
                    $tecnicosofsctmp[$key2]["estado"] = 3;
                } else {
                    $tecnicosofsctmp[$key2]["estado"] = 4;
                }

                $tiemposegundostoa = $tiempoactual - $tiempotecnicotoa;
                if ($tiemposegundostoa <= $tiemporango[0]) {
                    $tecnicosofsctmp[$key2]["estado_toa"] = 1;
                } else if ($tiemposegundostoa <= $tiemporango[1]) {
                    $tecnicosofsctmp[$key2]["estado_toa"] = 2;
                } else if ($tiemposegundostoa <= $tiemporango[2]) {
                    $tecnicosofsctmp[$key2]["estado_toa"] = 3;
                } else {
                    $tecnicosofsctmp[$key2]["estado_toa"] = 4;
                }

                $tiemposegundosot = $tiempoactual - $tiempotecnicoot;
                if ($tiemposegundosot <= $tiemporango[0]) {
                    $tecnicosofsctmp[$key2]["estado_ot"] = 1;
                } else if ($tiemposegundosot <= $tiemporango[1]) {
                    $tecnicosofsctmp[$key2]["estado_ot"] = 2;
                } else if ($tiemposegundosot <= $tiemporango[2]) {
                    $tecnicosofsctmp[$key2]["estado_ot"] = 3;
                } else {
                    $tecnicosofsctmp[$key2]["estado_ot"] = 4;
                }
                $tecnicosofsc[$value][$key2] = $tecnicosofsctmp[$key2];
            }
        }

        return json_encode($tecnicosofsc);
    }
}
