<?php

class TecnicosOfscController extends BaseController
{

    public function __construct()
    {
        /*$this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );*/
    }
    /**
     * mostrar componente especifico
     */
    public function show($resourceId)
    {
        // $xml = (array) simplexml_load_file(public_path().'/xml/tecnicos.xml');
        // return Response::json($xml);
        $hoy = date("Y-m-d");
        $hoy = preg_replace("/-/", "", $hoy);
        $json = File::get(public_path()."/json/tecnicos/".$resourceId."/".$hoy.".json");
        return $json;
    }
}
