<?php

class EmergenciasController extends BaseController
{
    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function getIndex(){
        $emergencias = Emergencia::with('EmergenciaImagen')->get();
        return Response::json( $emergencias );
    }

    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function postUpdate(){
        $id = Input::get('id');
        $data = Input::all();
        unset($data['id']);
        unset($data['emergencia_imagen']);
        //return Response::json([$data]);
        $rst= Emergencia::find( $id )->update( $data );
        if ($rst) {
            return Response::json( 
                [
                'rst'=>$rst,
                'msj'=>'se actualizo con exito'
                ] );
        }
        return Response::json( [
                'rst'=>$rst,
                'msj'=>'ocurrio un error al actualizar'
                ]  );
    }
}