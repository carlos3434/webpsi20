<?php

class GeoProyectoCapaController extends \BaseController
{

     public function postCargar()
     {
        $proyectoid=Input::get('id');
        $datos = GeoProyectoCapa::cargar($proyectoid);
        
       if (Request::ajax()) {
            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
     }

    public function postMapear()
    { 
       $capaid=Input::get('capaid');
       if (Request::ajax()) {
            $datos = GeoProyectoCapa::listardetalle($capaid);

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }

    public function postDelete()
    {

        //$idcapa=Input::get('idcapa');
        $datas=array(
                    Input::get('idcapa'),
                    Auth::id()
                    );

        
         if (Request::ajax()) {
            $datos = GeoProyectoCapa::eliminar($datas);

            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
    }

     public function postUpdatecapa()
     {

        $datos = GeoProyectoCapa::actualizar();

        if (Request::ajax()) {
            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
     }

    public function postAgregarcapa()
    {
        //$detalles=Input::get('detalles');
        $mapa=Input::get('mapa');
        $idpro=Input::get('idproyecto');
        $usuario=Auth::id();
        $rst=1;
        $msj='Proyecto Registrado Correctamente';

        $capas= array();

        foreach ($mapa as $index => $value) {
            if ($value) {
                $idcapa=array_search($value['capa'], $capas);
                if (!$idcapa) {
                    $arraycapa['proyecto_id'] = $idpro;
                    $arraycapa['nombre']      = $value['capa'];
                    $datoscapa=GeoProyectoCapa::getAgregarCapa($arraycapa);
                    $idcapa=$datoscapa->id;
                    $capas[$idcapa]=$value['capa'];
                }
                if ($idcapa>0) {
                    $value['capa_id']=$idcapa;
                    $value['tabla_id']=$value['elemento'];
                    unset($value['elemento']);
                    unset($value['capa']);
                    $delemento=GeoProyectoCapaElemento::agregar(
                                                  $value);
                } 
            }
        } 

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => $rst, 
                    'datos' => $idpro,
                    'msj' => $msj
                )
            );
        } 

    }
  //FALTA MODIFICAR CON NUEVAS TABLAS
    public function getGeocerca()
    {  
       $estado=0; 
       $id=Input::get('capas_id');
       if($id!='' or $id!=null){
            $ids=implode(',', $id);
       }else{
            $ids=0;
       }
       
       /*$data=DB::select("SELECT ge.capa_id,gc.proyecto_id,ge.averia,ge.provision,ge.id AS id,ge.figura_id,
        ge.tabla_id,ge.tabla_detalle_id,UPPER(IFNULL(ge.detalle,'')) AS detalle, coord, ge.coordenadas, ge.tipo_zona, 
        dato, ge.borde,ge.grosorlinea,IF(figura_id='3',SUBSTR(ge.fondo,2),ge.fondo) AS fondo,
        ge.opacidad, ge.orden, ge.averia, ge.provision,IF(ge.tipo_zona=1,'GEOCERCA',IF(ge.tipo_zona=2,'ZONA SOMBRA',
        IF(ge.tipo_zona=3,'ZONA PELIGROSA',IF(ge.tipo_zona=4,'ZONA TIPO 4',IF(ge.tipo_zona=5,'ZONA TIPO 5',
        IF(ge.tipo_zona=6,'ZONA TIPO 6',IF(ge.tipo_zona=7,'ZONA TIPO 7',IF(ge.tipo_zona=8,'ZONA TIPO 8',
        IF(ge.tipo_zona=9,'ZONA TIPO 9',IF(ge.tipo_zona=10,'ZONA TIPO 10','')))))))))) AS tipzona,
        UPPER(gp.nombre) AS proyecto, UPPER(gc.nombre) AS capa 
        FROM geo_proyecto_capa_elemento ge
        INNER JOIN geo_proyecto_capa gc ON gc.id=ge.capa_id 
        INNER JOIN geo_proyecto gp ON gp.id =gc.proyecto_id
        LEFT JOIN geo_tabla_detalle td ON ge.detalle=td.detalle AND td.tabla_id=ge.tabla_id
        WHERE tipo_zona=1 and ge.id in (".$ids.")");*/

        $data=DB::select("select *, IFNULL(t.nombre, 'Sin Tipo Zona') as tipzona, 
          UPPER(gp.nombre) AS proyecto, '' as capa, '' 
          as capa_id, coordenadas as coord from geocerca_proyecto_zonas gz
          INNER JOIN geocerca_proyecto gp
          on gp.id=gz.geocerca_proyecto_id
          LEFT JOIN tipo_geozona t
          ON gz.tipo_geozona_id=t.id
          where gz.id in (".$ids.") ");

        if(count($data)>0){
            $estado=1;
        }

       return \Response::json(
            array(
                "rst" => $estado,
                "geocerca" => $data
            )
        );
    }

    public function getProyectos()
    { 
       $estado=0;

       $data=GeocercaProyecto::where('estado',1)
              ->select("id", 
                       DB::RAW("UPPER(nombre) as nombre"))
              ->get();
       /*$data=DB::select("  SELECT  DISTINCT gp.`id`,UPPER(gp.`nombre`) AS nombre
          FROM  geo_proyecto_capa_elemento ce 
          INNER JOIN `geo_proyecto_capa` gc ON ce.`capa_id`=gc.`id`
          INNER JOIN `geo_proyecto` gp ON gc.`proyecto_id`=gp.`id`
          WHERE ce.tipo_zona=1 AND gp.`estado`=1;");*/

        if(count($data)>0){
            $estado=1;
        }

       return \Response::json(
            array(
                "rst" => $estado,
                "proyectos" => $data
            )
        );
    }

    public function getCapas()
    { 
       
       $id=Input::get('id');
       $estado=0;

       if($id!='' or $id!=null){
            $idproyecto=$id;
       }else{
            $idproyecto=0;
       }

      $data=GeocercaProyectoZonas::select("id", 
                              DB::RAW("TRIM(UPPER(detalle)) as nombre"))
              ->where('estado',1)
              ->where('geocerca_proyecto_id',$idproyecto)
              ->get();

       /*$data=DB::select("SELECT  DISTINCT ce.`id`,TRIM(UPPER(ce.detalle)) AS nombre
       FROM  geo_proyecto_capa_elemento ce 
       INNER JOIN `geo_proyecto_capa` gc ON ce.`capa_id`=gc.`id`
       WHERE ce.tipo_zona=1 AND gc.`estado`=1 AND gc.`proyecto_id`=".$idproyecto);*/

        if(count($data)>0){
            $estado=1;
        }

       return \Response::json(
            array(
                "rst" => $estado,
                "capas" => $data
            )
        );
    }
}
