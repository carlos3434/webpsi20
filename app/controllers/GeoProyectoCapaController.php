<?php

class GeoProyectoCapaController extends \BaseController
{

     public function postCargar()
     {
        $proyectoid=Input::get('id');
       if (Request::ajax()) {
            $datos = GeoProyectoCapa::getCargarCapa($proyectoid);

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
     }

    public function postMapear()
    { 
       $capaid=Input::get('capaid');
       if (Request::ajax()) {
            $datos = GeoProyectoCapa::getListarCapasDetalle($capaid);

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }

    public function postDelete()
    {

        //$idcapa=Input::get('idcapa');
        $datas=array(
                    Input::get('idcapa'),
                    Auth::user()->id
                    );

        
         if (Request::ajax()) {
            $datos = GeoProyectoCapa::getDeleteCapa($datas);

            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
    }

     public function postUpdatecapa()
     {

        $datas=array(
                    Input::get('capaid'),
                    Input::get('borde'),
                    Input::get('fondo'),
                    Input::get('grosorlinea'),
                    Input::get('opacidad'),
                    Auth::user()->id
                    );

        
         if (Request::ajax()) {
            $datos = GeoProyectoCapa::getUpdatecapa($datas);

            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
     }

    public function postAgregarcapa()
    {
        //$detalles=Input::get('detalles');
        $mapa=Input::get('mapa');
        $idpro=Input::get('idproyecto');
        $usuario=Auth::user()->id;

        $arraydetail= array();
        $i=0;
        $capas= array();
        $existe=0;
        $arraymapa= array();
        $y=0;

        foreach ($mapa as $index => $value) {
            if ($value) {
                if (in_array($value['capa'], $capas)) {
                    $existe++; 
                } else {
                array_push($capas, $value['capa']);
                    $arraydetail[$i]="('".$value['capa']."','".$idpro."')";
                    $i++;
                }

                $arraymapa[$y]="((select id from geo_proyecto_capa where 
                        nombre='".$value['capa']."' 
                        and proyecto_id='".$idpro."' 
                        order by id desc limit 1 ),
                        '".$value['subelemento']."',
                        '".$value['tabla_detalle_id']."',
                        '".$value['orden']."','".$value['borde']."',
                        '".$value['grosorlinea']."','".$value['fondo']."',
                        '".$value['opacidad']."',
                        '".$value['elemento']."',
                        '".$value['tipo']."')";
                $y++;
            }

        } 
         $valcap= implode(",", $arraydetail);
         $dcapa=GeoProyectoCapa::getAgregarCapa($valcap);
 
         if ($dcapa) {
            $valele= implode(",", $arraymapa);
            $delemento=GeoProyectoCapaElemento::getAgregarCapaElemento($valele);
            $datos=array(
                $idpro,
                Auth::user()->id
                );
            $upProyecto=GeoProyecto::getUpdateProyectoCapa($datos);
            $msj='Se Actualizo el Proyecto correctamente';
            $rst=1;
         } else {
             $msj='No se actualizo el Proyecto'; 
             $delemento=0;
             $rst=0;
         } 


            if ( Request::ajax() ) {
                return Response::json(
                    array(
                        'rst' => $rst, 
                        'datos' => $idpro,
                        'msj' => $msj
                    )
                );
            } 

    }

}
