<?php

class GeocercaProyectoController extends \BaseController
{
    public function postSave()
    {
      //INPUTS
      $mapa=Input::get('mapa');
      $proyecto['nombre']=Input::get('nombre');
      
      $idpro=0;
      $msj='Falló el registro del proyecto';
      $rst=2;

      //VALIDACIÓN   
      $reglas = array(
              'nombre'    => 'required|unique:geocerca_proyecto'
      );
      $mensaje = array(
              'required'  => 'Nombre de Proyecto es requerido',
              'unique'   => 'Nombre de Proyecto ya está registrado',
          );
      $validator = Validator::make(Input::all(), $reglas, $mensaje);
      if ($validator->fails()) {
          return Response::json(
              array(
              'rst' => 2,
              'msj' => $validator->messages(),
              )
          );
      }
      
      //PROYECTO
      $datos=GeocercaProyecto::guardar($proyecto);

      if ($datos->id) {
        $idpro=$datos->id;
        GeocercaProyectoZonas::guardar($datos->id, $mapa);
        $rst=1;
        $msj='Proyecto Registrado Correctamente';
      }
        
      if ( Request::ajax() ) {
          return Response::json(
              array(
                  'rst' => $rst, 
                  'datos' => $idpro,
                  'msj' => $msj
              )
          );
      } 
    }

    public function postListar()
    {
      $datos = GeocercaProyecto::listar();

      if (Request::ajax()) {
            return Response::json(array('rst'=>1,'datos'=>$datos));
      }
    }

    public function postUpdate()
    {
        
        if (Input::get('idproyecto')) {
          $id=Input::get('idproyecto');
        } else {
          $id=Input::get('id', 0);
        }

        //validacion    
        $reglas = array(
                'nombre'    => 'unique:geo_proyecto, "nombre", '.$id
        );
        $mensaje = array(
                'unique'   => 'Nombre de Proyecto ya está registrado',
            );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return Response::json(
                array(
                  'rst' => 2,
                  'msj' => $validator->messages(),
                )
            );
        }
        
        if (Request::ajax()) {
            $datos = GeocercaProyecto::actualizar($id);  
            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
        } 
    }


}