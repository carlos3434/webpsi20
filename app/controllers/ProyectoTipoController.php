<?php

class ProyectoTipoController extends \BaseController
{
    public function postCargarproyecto()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyectos')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCargarproyectotipo()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyecto_tipo')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postListar()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyecto_tipo as pt')
                    ->join('proyectos as p', 'p.id', '=', 'pt.proyecto_id')
                    ->select('pt.id', 'pt.proyecto_id', 'p.nombre as proyecto', 'pt.nombre', 'pt.estado')
                    ->orderBy('pt.nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCrear()
    {
        if (Request::ajax()) {

            DB::table('proyecto_tipo')
            ->insert(
                array('nombre' => Input::get('nombre'),
                'proyecto_id' => Input::get('proyecto_id'),
                'estado' => Input::get('estado'),
                'created_at' => date("Y-m-d H:m:s"),
                'usuario_created_at' => Auth::user()->id)
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente'
                )
            );
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            
            $datos = DB::table('proyecto_tipo')
                    ->select('id', 'proyecto_id', 'nombre', 'estado')
                    ->where('id', '=', Input::get('id'))
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            
            DB::table('proyecto_tipo')
            ->where('id', Input::get('id'))
            ->update(
                array('nombre' => Input::get('nombre'),
                    'proyecto_id' => Input::get('proyecto_id'),
                    'estado' => Input::get('estado'),
                    'updated_at' => date("Y-m-d H:m:s"),
                    'usuario_updated_at' => Auth::user()->id)
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Actualizacion realizado correctamente'
                )
            );
        }
    }

    public function postEstado()
    {
        if (Request::ajax()) {
            
            DB::table('proyecto_tipo')
            ->where('id', Input::get('id'))
            ->update(
                array('estado' => Input::get('estado'))
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Estado actualizado correctamente'
                )
            );
        }
    }

    // public function postBandeja()
    // {
    //     if (Request::ajax()) {
    //         $dinamico = "";
    //         $querype = "SELECT pe.id FROM proyecto_edificios pe WHERE pe.id = 4151";
    //         $datoid = DB::select($querype);

    //         $query = "SELECT id, nombre FROM proyectos_estados where estado = 1";
    //         $estados = DB::select($query);
    //         $i = 0;
    //         //die("dentro del controlador");
    //         //$demo = "SELECT * FROM papaya WHERE id = '$fff'";
    //         foreach($estados as $demo => $dato){
    //             $i++;
    //             $dinamico = $dinamico . 'IF('.$dato->id.' = t100.proyecto_estado_id, (CONCAT(IFNULL(t100.fecha_inicio,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.fecha_seguimiento,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.fecha_compromiso,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.fecha_termino,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.fecha_gestion,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.recepcion_expediente,'."'NULO'".'), '."'|'".', 
    //                 IFNULL(t100.fecha_teorica_final,'."'NULO'".'))),0) AS '.'estado'.$dato->id.','.'';
    //         }

    //         //die($dinamico);



    //         /*$query2 = 'SELECT 
    //                 '.$dinamico.' pe.id, t100.proyecto_estado_id
    //                 FROM proyecto_edificios pe
    //                 inner JOIN 
    //                 (SELECT ge.id idge, ge.created_at, ge.proyecto_estado_id, ge.fecha_inicio fecha_inicio, ge.fecha_seguimiento fecha_seguimiento,
    //                 ge.fecha_compromiso fecha_compromiso, ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion,
    //                 ge.recepcion_expediente recepcion_expediente, ge.fecha_teorica_final fecha_teorica_final, ge.proyecto_edificio_id id
    //                 FROM `gestiones_edificios` ge
    //                 where 
    //                 ge.id in (select Max(t50.id) from gestiones_edificios t50 
    //                 group by t50.proyecto_estado_id, t50.proyecto_edificio_id)) t100
    //                 on t100.id = pe.id
    //                 WHERE pe.id = 4151
    //                 order by t100.proyecto_estado_id asc';

    //         $datosdemo = DB::select($query2);*/

    //         $estado1 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 1
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado11 = DB::select($estado1);

    //         $estado2 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 2
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado22 = DB::select($estado2);

    //         $estado3 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 3
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado33 = DB::select($estado3);

    //         $estado4 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 4
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado44 = DB::select($estado4);

    //         $estado5 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 5
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado55 = DB::select($estado5);

    //         $estado6 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 6
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado66 = DB::select($estado6);

    //         $estado7 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 7
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado77 = DB::select($estado7);

    //         $estado8 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 8
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado88 = DB::select($estado8);

    //         $estado9 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 9
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado99 = DB::select($estado9);

    //         $estado10 = 'SELECT pe.id ,ge.id idge, ge.created_at, ge.proyecto_estado_id proyecto_estado_id, ge.fecha_inicio fecha_inicio,
    //                     ge.fecha_seguimiento fecha_seguimiento, ge.fecha_compromiso fecha_compromiso,
    //                     ge.fecha_termino fecha_termino, ge.fecha_gestion fecha_gestion, ge.recepcion_expediente recepcion_expediente,
    //                     ge.fecha_teorica_final fecha_teorica_final
    //                     FROM proyecto_edificios pe
    //                     left join `gestiones_edificios` ge
    //                     on ge.proyecto_edificio_id = pe.id and ge.id in (SELECT Max(t50.id) FROM gestiones_edificios t50 
    //                                                                      WHERE t50.proyecto_estado_id = 10
    //                                                                      GROUP BY t50.proyecto_edificio_id)';
    //         $estado1010 = DB::select($estado10);

    //         return Response::json(
    //             array(
    //                 'rst' => 1,
    //                 'datoid' => $datoid,
    //                 //'datodemo' => $datosdemo,
    //                 'estados' => $estados,
    //                 'estados1' => $estado11,
    //                 'estados2' => $estado22,
    //                 'estados3' => $estado33,
    //                 'estados4' => $estado44,
    //                 'estados5' => $estado55,
    //                 'estados6' => $estado66,
    //                 'estados7' => $estado77,
    //                 'estados8' => $estado88,
    //                 //'estados9' => $estado99,
    //                 // 'estados10' => $estado1010

    //             )
    //         );
    //     }

        
    // }
}