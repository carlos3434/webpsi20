<?php

class ProyectoTipoController extends \BaseController
{
    public function postCargarproyecto()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyectos')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCargarproyectotipo()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyecto_tipo')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postListar()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyecto_tipo as pt')
                    ->join('proyectos as p', 'p.id', '=', 'pt.proyecto_id')
                    ->select('pt.id', 'pt.proyecto_id', 'p.nombre as proyecto', 'pt.nombre', 'pt.estado')
                    ->orderBy('pt.nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCrear()
    {
        if (Request::ajax()) {

            DB::table('proyecto_tipo')
            ->insert(
                array('nombre' => Input::get('nombre'),
                'proyecto_id' => Input::get('proyecto_id'),
                'estado' => Input::get('estado'),
                'created_at' => date("Y-m-d H:m:s"),
                'usuario_created_at' => Auth::id())
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente'
                )
            );
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            
            $datos = DB::table('proyecto_tipo')
                    ->select('id', 'proyecto_id', 'nombre', 'estado')
                    ->where('id', '=', Input::get('id'))
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            
            DB::table('proyecto_tipo')
            ->where('id', Input::get('id'))
            ->update(
                array('nombre' => Input::get('nombre'),
                    'proyecto_id' => Input::get('proyecto_id'),
                    'estado' => Input::get('estado'),
                    'updated_at' => date("Y-m-d H:m:s"),
                    'usuario_updated_at' => Auth::id())
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Actualizacion realizado correctamente'
                )
            );
        }
    }

    public function postEstado()
    {
        if (Request::ajax()) {
            
            DB::table('proyecto_tipo')
            ->where('id', Input::get('id'))
            ->update(
                array('estado' => Input::get('estado'))
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Estado actualizado correctamente'
                )
            );
        }
    }
}