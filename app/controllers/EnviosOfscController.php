<?php
use Ofsc\Inbound;
use Repositories\EnvioMasivoOfscRepository;
use Ofsc\Activity;

class EnviosOfscController extends \BaseController
{
    protected $_errorController;
    protected $_bandejaController;
    protected $_gestionMovimientoController;
    protected $_envioMasivoOfscRepo;

    public function __construct(
            ErrorController $errorController,
            BandejaController $bandejaController,
            EnvioMasivoOfscRepository $envioMasivoOfscRepo,
            GestionMovimientoController $gestionMovimientoController
    )
    {
        $this->_errorController = $errorController;
        $this->_bandejaController = $bandejaController;
        $this->_gestionMovimientoController = $gestionMovimientoController;
        $this->_envioMasivoOfscRepo = $envioMasivoOfscRepo;
    }
    public function postListarmasivos()
    {
        return $this->getListarmasivos();
    }
    public static function detalleEnvioOfsc($codactu)
    {
        if ($codactu > 0) {
            $envioofsc = new EnvioOfsc();
            return $envioofsc->getEnviosOfsc($codactu);
        }
    }
    public function getListarmasivos()
    {
        if (!Input::has("isSoloActuaciones")) {
            $start = Input::get('start');
            $length = Input::get('length');
            $columns = Input::get('columns');
            $orderColumn = [];
            $orderDir = [];
            foreach (Input::get('order') as $order) {
                $orderColumn = $columns[$order['column']]['data'];
                $orderDir = $order['dir'];
            }
            $datos = Input::all();
            if (Input::has("bandeja") && Input::get("bandeja")!=1) {
                $envioMasivo = [];
                $emou = new EnvioMasivoOfscUltimo();
                $filtros = [
                    "codactu" => Input::get("codactuacion", '0'),
                    "estado" => Input::get("estado_masivo", ""),
                    "empresa" => Input::get("empresa_masivo", "") ,
                    "fecha" => Input::get("fecha_registro_masivo", ""),
                    "estado_psi" => Input::get("estado_psi", ""),
                    "estado_ofsc" => Input::get("estado_ofsc_masivo", ""),
                    "fec_movimiento" => Input::get("fecha_movimiento", ""),
                    "nodo" => Input::get("nodo_masivo", ""),
                    "tipo_actividad_id" =>
                                    Input::get("actividad_tipo_masivo", ""),
                    "tipo_masivo" => Input::get("tipo_masivo", "")
                ];
                $bodySql = [
                    "filtros" => $filtros,
                    "length" => $length,
                    "start"     => $start,
                    "orderBy" => $orderColumn,
                    "orderDir"   => $orderDir,
                    "download_excel" =>
                                Input::has("download_excel") ? true : false,
                ];
                $envioMasivo = $emou->get($bodySql);
            } else {
                $envioMasivo = $this->_envioMasivoOfscRepo->getRows(
                    $datos, $orderColumn, $orderDir, $start, $length
                );
            }
            $extra['recordsTotal'] = $envioMasivo['total'];
            $extra['recordsFiltered'] = $envioMasivo['total'];
            $extra['draw'] = Input::get('draw');
                if (Input::has("bandeja")) {
                    $data =  $envioMasivo["datos"];
                } else {
                    $data = $envioMasivo['datos']->toArray();
                }
            $output = [];
            $modal='enviomasivoModal';
            foreach ($data as $k => $v) {
                $statusClass = ($v['estado'] == 0 ) ? 'danger' : 'success';
                $r = [];
                $r = $v;
                $r['accion'] = '';
                $r['estado'] = Helpers::getEstadoHtml($statusClass);
                array_push($output, $r);
            }
            return Helpers::getResponseJson($output, '', $extra);
        } else {
            $datos = DB::table("envio_masivo_ofsc as emo")
                    ->select(
                        "emo.codactu",
                        "eou.id_ultimo_envio",
                        "emo.tipo_actividad_id as actividad_tipo_id",
                        "eo.estado_ofsc"
                    )
                    ->leftJoin(
                        "envios_ofsc_ultimo as eou",
                        "eou.codactu", "=", "emo.codactu"
                    )
                    ->leftJoin(
                        "envios_ofsc as eo",
                        "eo.id", "=", "eou.id_ultimo_envio"
                    )
                   ->orderBy("emo.id", "desc")
                    ->get();
            return Response::json(
                [
                    "rst" => 1,
                    "datos" => $datos
                ]
            );
        }
    }
    public function postReporteofsc()
    {
        $consulta = [];
        if (Input::has("bandeja")) {
            switch (Input::get("bandeja")) {
                case "ordenesofsc":
                    $consulta = EnvioOfsc::getOrdenes();
                    break;
                case "ordenesmasivoofsc":
                    $start = Input::get('start');
                    $length = Input::get('length');
                    $columns = Input::get('columns');
                    $orderColumn = "created_at";
                    $orderDir = " DESC";
                    $datos = Input::all();
                    $envioMasivo = [];
                    $emou = new EnvioMasivoOfscUltimo();
                    $filtros = [
                        "codactu" => Input::get("codactuacion", "0"),
                        "estado" => (Input::has("estado_masivo") 
                                                && Input::get("estado_masivo")!=="null")?
                                                Input::get("estado_masivo") :  "",
                        "empresa" => (Input::has("empresa_masivo") 
                                                    && Input::get("empresa_masivo") !=="null") ?
                                                Input::get("empresa_masivo") : "",
                        "fecha" => Input::get("fecha_registro_masivo", ''),
                        "estado_psi" => Input::get("estado_psi", ''),
                        "estado_ofsc" =>  Input::get("estado_ofsc_masivo", ''),
                        "fec_movimiento" => Input::get("fecha_movimiento", ''),
                        "nodo" => (Input::has("nodo_masivo") && 
                                                Input::get("nodo_masivo")!=="null")? 
                                                Input::ge("nodo_masivo") : "",
                        "tipo_actividad_id" =>
                                        Input::get("actividad_tipo_masivo", ''),
                        "tipo_masivo" => Input::get("tipo_masivo", '')
                    ];
                    $bodySql = [
                        "filtros" => $filtros,
                        "download_excel" =>
                            Input::has("download_excel") ? true : false,
                    ];
                    $envioMasivo = $emou->get($bodySql);
                    if (is_object($envioMasivo["datos"]))
                        $consulta ["datos"]  = $envioMasivo["datos"]->toArray();
                    else
                        $consulta ["datos"] = $envioMasivo["datos"];

                    break;
                default:
                    break;
            }
        } else {
            $consulta = EnvioOfsc::getOrdenes();
        }
        if (Request::ajax()) {
            if (Input::has("bandeja")) {
                $extra['recordsTotal'] = $consulta['total'];
                $extra['recordsFiltered'] = $consulta['total'];
                $extra['draw'] = Input::get('draw');
                $data = $consulta['datos'];

                return Response::json(
                    [
                        "status" => "success",
                        "mensaje" => "",
                        "data" => $data,
                        "recordsTotal" => $extra["recordsTotal"],
                        "recordsFiltered" => $extra["recordsFiltered"],
                        "draw" => $extra["draw"]
                    ]
                );
            } else {
                return Response::json(['rst' => 1, 'datos' => $consulta]);
            }
        } elseif (Input::has("download_excel") &&
                  count($consulta['datos']) > 0) {
            return Helpers::exportArrayToExcel(
                $consulta['datos'],
                Input::get("bandeja")
            );
        }
    }
    /**
    * POST AJAX que recibe masivo desde la Bandeja De Gestion
    */
    public function postMasivoofsc()
    {
        $dataActuaciones = json_decode(
            Input::get("codactuSeleccionados"),
            true
        );
        $response = $this->getProcesoenvioofsc(true, $dataActuaciones);
        return Response::json($response);
    }
    /**
     * Proceso que se programa y se ejecuta desde ProcesoToaCommand.php
     */
    public function getProcesoenvioofsc(
        $isMasivoBandeja = false,
        $dataActuaciones = array()
    )
    {
        $nombreGrupo = date("Y-m-d H:i:s");
        $actuacion = [];
        $grupoEnvioId = 0;
        $dataGrupo = [];
        $tablabien = ""; $tablamal = "";

        if ($isMasivoBandeja == false) {
            $actuacion = Toa::ValidaPermisoProceso();
            $idUsuario = 958;

            if (count($actuacion) > 0) {
            } else {
                return [
                    "mensaje" => "No hay data valida para enviar masivamente"
                ];
            }

        } else {
            if (Auth::user()->id != 743) {
                return [
                    "rst" => 2,
                    "msj" => "No tienes permiso para enviar masivos"
                ];
            }
            if (count($dataActuaciones) > 0 & $isMasivoBandeja === true) {
                foreach ($dataActuaciones as $key => $value) {
                    if (strlen(trim($value["seleccionado"])) == 0) {
                    } else {
                        $actuacion[] = $value;
                    }
                }
            }
            if (count($actuacion) == 0) {
                $actuacion["rst"] = 2;
                if (!isset($actuacion["msj"]))
                    $actuacion["msj"] = "No hay codigos de Actuacion";
                return $actuacion;
            }
        }
        $validacambio = "";
        $bucket = "";
        //Existen actuaciones para envío masivo
        if (count($actuacion) > 0) {
            $grupoEnvioId = DB::table('envio_masivo_ofsc_grupo')
                ->insertGetId(
                    [
                    'nombre' => $nombreGrupo,
                    'created_at' => date("Y-m-d H:i:s"),
                    'usuario_created_at' =>  \Config::get("wpsi.ofsc.user_logs.actividades_temporales") 
                    ]
                );
        }
        //echo "cantidad de ordenes".count($actuacion);
       //dd();
        if  ($isMasivoBandeja === false) {
            // Validaciones de Ordenes
            $provisiones = array();
            $averias = array();
            foreach ($actuacion as $key => $value) {
                echo $value->actividad;
                switch ($value->actividad) {
                    case "1":
                        $provisiones[] = $value;
                        //echo "estoy en provisiones";
                        break;
                    case "2":
                        $averias[] = $value;
                        break;
                    default:
                        # code...
                        break;
                }
            }
            echo "actividades generadas =".count($actuacion)."\n";
            echo "actividades provisiones".count($provisiones)."\n";

            $validacion = new OfscValidacion();
            $provisiones = $validacion->validaEnvioMasivoOfsc($provisiones);

            echo "actividades averias".count($averias);

            $actuacionesOK = array_merge($provisiones, $averias);
            if (count ($actuacionesOK )  > 0) {
                $actuacion = $actuacionesOK;
            } else {
                echo " No hay actuaciones validas para enviar";
                exit();
            }
             //dd();
        }


        foreach ($actuacion as $key => $value) {

             # code...{
            if (isset($value)) {
                $data = [];
                $data["tipo"] = 1;
                $data["orden"] = $value;
                $data["isMasivoBandeja"] = $isMasivoBandeja;
                $data["i"] = $key;
                $data["grupoEnvioId"] = $grupoEnvioId;
                $data["idUsuario"] = $idUsuario;
                $response[] = Queue::push('EnviosOfscMasivoController', $data);
                if ($isMasivoBandeja == false ) {
                    echo "\n Envio # ".$key." \n";
                    echo "Actuacion : ".$value->codactu."...\n";
                   // var_dump($data);
                }
            }
        }
        if ($isMasivoBandeja==false) {
        } else {
            return [
                "rst" => 1,
                "datos" => $response,
                "msj" => "Procesados : ".
                count($response)."<br>Revise el Reporte de".
                " Envios para el Estado de las Ordenes a OFSC"
            ];
        }
    }

    public function getProcesocancelarofsc()
    {
        if (!Input::has("codactu")){
            $actuacion = Toa::obtenerPendientes();
        }
        else{
            $actuacion = Gestion::getCargar(Input::get("codactu"));
            if ($actuacion["rst"] == 1){
                $actuacion = $actuacion["datos"];
                //$actuacion = $actuacion[0];
            } else {
                return ["rst" => 2];
            }

        }

        $save= [];
        $properties = [];
        if (Input::has("codactu"))
            $properties["XA_NOTE"] = "Cancelado por cambio de quiebre";
        for ($i = 0; $i < count($actuacion); $i++) {
            $save[$i]["aid"]=$actuacion[$i]->aid;
            $save[$i]["codactu"]=$actuacion[$i]->codactu;

            $inbound = new Inbound();
            $response = $inbound->cancelActivity(
                $actuacion[$i]->aid, $actuacion[$i]->codactu, $properties
            );
            if (isset(
                $response->data->data->commands->command->appointment->report
                         ->message
            )
                ) {
                $resultCode = $response->data->data->commands->command
                                ->appointment->report->message;
                if (isset($resultCode->result)
                    && $resultCode->result=='error'
                ) {
                    $save[$i]["rst"] = 2;
                    $save[$i]['error_msg']=$resultCode->description;
                } else {
                    for ($y = 0; $y < count($resultCode); $y++) {
                        if ($resultCode[$y]->type=='cancel' &&
                            $resultCode[$y]->result=='success'
                        ) {
                            $save[$i]["rst"] = 1;
                            $save[$i]["msj"] =
                                "Envio de Cancelacion a OFSC correcto.";
                            $save[$i]['result_code']=$resultCode[$y]->code;
                        }
                    }
                }
            }
        }
        return $save;
    }

    public function postListardetalle()
    {
        $ve = Input::get('e');
        $vr = Input::get('r');
        if (json_decode($ve)=='') {
            $veDos=$ve;
        } else {
            $veDos=json_decode($ve);
        }
        if (json_decode($vr)=='') {
            $vrDos=$vr;
        } else {
            $vrDos=json_decode($vr);
        }
        $data=[];
        $data["enviado"]= $veDos;
        $data["respuesta"] =$vrDos;
        return View::make('admin.reporte.form.ofscmodal', $data);
    }

    public function getEnviarcomponentesofsc()
    {
        $save = [];
        $response = "";
        $inbound = new Inbound();
        $compGestion = new ComponenteGestion();
        $dataCodReq = $compGestion->getCodReqComponenteFaltante2();

        if (count($dataCodReq["data"]) > 0) {

            // table envio_masivo_ofsc_grupo (insert)
            $dateActual = date("Y-m-d H:i:s");

            DB::table('envio_masivo_ofsc_grupo')
                ->insert(
                    [
                        'nombre' => $dateActual,
                        'created_at' => $dateActual
                    ]
                );

            $idMasivoOfscGrupo = DB::table('envio_masivo_ofsc_grupo')
                ->select('id')
                ->where('nombre', '=', $dateActual)
                ->get();

            foreach ($dataCodReq["data"] as $valCod) {
                $activityId = $valCod->aid;
                $codactu = $valCod->codigo_req;
                $gestionId = $valCod->gestion_id;
                $mdf = $valCod->mdf;
                $actividadTipoId = $valCod->actividad_tipo_id;
                $dataComp = $compGestion->getComponenteFaltanteGrupal($codactu);
                $empresa = $valCod->empresa;
                $label = $valCod->label;
                $actividad = $valCod->actividad;
                $resultformat = $compGestion->getArrayformat(
                    $dataComp["data"]
                );
                $data = [];
                $data["XA_EQUIPMENT"] = $resultformat;
                $response = $inbound->updateActivity($codactu, $data,array());
                
                if (isset($response->result)) {
                    if ($response->result=="success") {
                        // Update tabla gestiones_detalles
                        DB::table('gestiones_detalles')
                        ->where('codactu', '=', $codactu)
                        ->update(['componente' => 1]);


                        DB::table('envio_masivo_ofsc')
                        ->insert(
                            [
                            'envio_masivo_ofsc_grupo_id' =>
                            $idMasivoOfscGrupo[0]->id,
                            'bucket' => '',
                            'codactu' => $codactu,
                            'mdf' => $mdf,
                            'tipo_actividad_id' => $actividadTipoId,
                            'aid' => $activityId,
                            'mensaje' => 'Envio de Componentes exitoso',
                            'empresa' => $empresa,
                            'envio_masivo_at' => '',
                            'tipo_masivo' => 'Componentes',
                            'estado' => '1',
                            'created_at' => $dateActual,
                            "usuario_created_at" => \Config::get("wpsi.ofsc.user_logs.update_componentes") 
                            ]
                        );

                        $dataComp2 = $compGestion->getComponenteTmp($codactu);
                        if ($dataComp2["error"] === false) {
                            foreach ($dataComp2["data"] as $value) {
                                DB::table('componente_gestion')->insert(
                                    [
                                    'gestion_id' => $gestionId,
                                    'componente_id' => $value->idEquipo,
                                    'cantidad' => 1,
                                    'ejecuto' => 0,
                                    'estado' => 1,
                                    'created_at' => $dateActual,
                                    ]
                                );
                            }
                        }
                        
                        $save["rst"] = 1;
                        $save["msj"] = "Actualización correcta.";
                        $save['result_code']=0;
                    } else {
                        if ($response->result=="error" ) {
                            DB::table('envio_masivo_ofsc')
                            ->insert(
                                [
                                'envio_masivo_ofsc_grupo_id' =>
                                $idMasivoOfscGrupo[0]->id,
                                'bucket' => '',
                                'codactu' => $codactu,
                                'mdf' => $mdf,
                                'tipo_actividad_id' => $actividadTipoId,
                                'empresa' => $empresa,
                                'aid' => $activityId,
                                'mensaje' => 'Envio de Componentes fallido: '.$response->description,
                                'tipo_masivo' => 'Componentes',
                                'estado' => '0',
                                'created_at' => date("Y-m-d"),
                                 "usuario_created_at" => \Config::get("wpsi.ofsc.user_logs.update_componentes") 
                                ]
                            );
                            $save["rst"] = 2;
                            $save['error_msg']= $response->description;
                        }
                        
                    }
                    
                } else {

                    DB::table('envio_masivo_ofsc')
                    ->insert(
                        [
                        'envio_masivo_ofsc_grupo_id' =>
                        $idMasivoOfscGrupo[0]->id,
                        'bucket' => '',
                        'codactu' => $codactu,
                        'mdf' => $mdf,
                        'tipo_actividad_id' => $actividadTipoId,
                        'empresa' => $empresa,
                        'aid' => $activityId,
                        'mensaje' => 'No se obtuvo respuesta del API',
                        'tipo_masivo' => 'Componentes',
                        'estado' => '0',
                        'created_at' => date("Y-m-d"),
                         "usuario_created_at" => \Config::get("wpsi.ofsc.user_logs.update_componentes") 
                        ]
                    );
                    $save["rst"] = 2;
                    $save['error_msg']= 'error';
                }
            }
        } else {
            $save['msj'] = "No se encontraron datos";
        }
        return json_encode($save);
    }

    public function getEnviarliquidadoofsc()
    {
        $actuacion = Toa::ValidaPermisoEnvioLiquidados();
        $save= [];
        if (count($actuacion)>0) {
            $dateActual = date("Y-m-d H:i:s");

            DB::table('envio_masivo_ofsc_grupo')
                ->insert(
                    [

                        'nombre' => $dateActual,
                        'created_at' => $dateActual
                    ]
                );
            $idMasivoOfscGrupo = DB::table('envio_masivo_ofsc_grupo')
                                    ->select('id')
                                    ->where('nombre', '=', $dateActual)
                                    ->get();

            for ($i=0; $i<count($actuacion); $i++) {
                $data = [];
                $data["tipo"] = 2;
                $data["aid"] = $actuacion[$i]->aid;
                $data["codactu"] = $actuacion[$i]->codactu;
                $data["mdf"] = $actuacion[$i]->mdf;
                $data["actividad_tipo_id"] = $actuacion[$i]->actividad_tipo_id;
                $data["empresa"] = $actuacion[$i]->empresa;
                $data["idMasivoOfscGrupo"] = $idMasivoOfscGrupo[0]->id;
                $response[] = Queue::push('EnviosOfscMasivoController', $data);
                echo "\n Envio # ".$i." \n";
                echo "Actuacion : ".$data["codactu"]."...\n";
            }
            if (count($save) >= 0) {
                return "Ejecutando cola...";
            }
        }
        if (count($save) == 0) {
            return "No se encontraros datos";
        }
    }
}
