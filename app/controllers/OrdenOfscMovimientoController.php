<?php
class OrdenOfscMovimientoController extends BaseController
{
	public function postListar()
	{
		$result = [];
		$select = [
			"aom.*",
			"em.nombre as empresa",
			"q.nombre as quiebre"
		];
		$result["data"] = ActividadOfscMovimiento::from("actividades_ofsc_movimientos as aom")
			->orderBy('id', 'DESC')
			->select($select);
		$start = Input::get("start", 0);
		$length = Input::get("length", 10);
		$fecha = explode(" - ", Input::get("fecha_registro"));
		$fecha[1].=" 23:59:59";
		
		if (Request::ajax()) {
			$download = false;
			if (Input::has("download")) {
				$download = true;
			}
			$where = [];
			$whereIn = [];

			if (Input::has("empresa") 
				&& count (Input::get("empresa")) > 0)
			{
				$result["data"]
				->whereIn(
					"empresa_id", Input::get("empresa")
					);
			}

			if (Input::has("quiebre") 
				&& count (Input::get("quiebre")) > 0)
			{
				$result["data"]
				->whereIn(
					"quiebre_id", Input::get("quiebre")
					);
			}

			if (Input::has("actividad") 
				&& count (Input::get("actividad")) > 0)
			{
				$whereIn["actividad"] = Input::get("actividad");
			}

			if (Input::has("nodo") 
				&& count (Input::get("nodo")) > 0)
			{
				$result["data"]
				->whereIn(
					"nodo", Input::get("nodo")
					);
			}
			$data = $result["data"];
			$data = $data->leftJoin("empresas as em", "em.id", "=", "aom.empresa_id");
			$data = $data->leftJoin("quiebres as q", "q.id", "=", "aom.quiebre_id");
			$data = $data->whereBetween('aom.created_at', $fecha);
			$result["total"] = count($data->get());
			$result["data"] = $data->limit($length)->offset($start)->get();
			$result["recordsTotal"] = $result["total"];
            $result["recordsFiltered"] = $result["total"];
            $result["draw"] = Input::get("draw");
			return json_encode($result);
		}
	}
}