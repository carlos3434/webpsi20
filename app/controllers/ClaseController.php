<?php

class ClaseController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            try {
                if(Input::get('parametros')){
                    $result = DB::table('schedulle_sistemas.cat_clase_serv_catv')
                                ->select(
                                    'codclase as id',
                                    'descripcion as nombre'
                                    )
                                ->orderBy('descripcion')
                                ->get();
                }
            } catch (Exception $error) {
                $this->error->handlerError($error);
            }
            return Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
