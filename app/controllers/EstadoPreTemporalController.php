<?php

class EstadoPreTemporalController extends \BaseController
{
    public function postListar() {
        $data = DB::table('estados_pre_temporales')
        ->select(
            'id',
            'nombre'
        )
        ->where('estado', '=', '1')
        ->get();

        $datos = array(
            'rst' => 1,
            'datos' => $data
        );

        return Response::json($datos);
    }

}
