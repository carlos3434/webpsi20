<?php
class GeoTapController extends \BaseController
{
    public function postListar()
    {

        $r =GeoTap::postGeoTapAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoTap::postNodoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postNodo()
    {
        $array['zonal']=Input::get('zonal');
        $array['nodo']=Input::get('nodo');
        $r =GeoTap::postTrobaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postTroba()
    {
        $array['zonal']=Input::get('zonal');
        $array['nodo']=Input::get('nodo');
        $array['troba']=Input::get('troba');
        $r =GeoTap::postAmplificadorFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postAmplificador()
    {
        $array['zonal']=Input::get('zonal');
        $array['nodo']=Input::get('nodo');
        $array['troba']=Input::get('troba');
        $array['amplificador']=Input::get('amplificador');
        $r =GeoTap::postTapFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }



}

