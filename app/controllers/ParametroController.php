<?php

class ParametroController extends BaseController
{

    protected $_errorController;
    protected $_uploadController;

    public function __construct(
        ErrorController $errorController,
        UploadController $uploadController
    ) {
        $this->_errorController = $errorController;
        $this->_uploadController = $uploadController;
    }

    public function postSave()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            
            $parametros = Parametro::getGuardar();
            $msj='Registro realizado correctamente';
            $rst=1;
            if ($parametros->tipo==4 && $parametros->estado==1) {
                $result=Parametro::getFiltrarST(
                    $parametros->id,
                    $parametros->proveniencia
                );
                if ($result["rst"] == 2) {
                    $rst = 1;
                    $msj='¡ Esta Masiva esta en proceso !';
                }
                if ($result["rst"] == 3) {
                    $rst = 1;
                    $msj='¡ No se puede procesar esta Masiva !';
                }
                if ($result["rst"] == 1) {
                    $envio = $result["data"];
                    $msj = $result["msj"];
                    Queue::push('ProcesoMasiva', $envio, 'procesomasiva');
                }
            }
            if ($parametros->tipo==4 && $parametros->estado==0) {
                //eliminar los st que sean de este parametro de la tabla masivast
                $result=Masivast::where('parametro_id', $parametros->id)->delete();
                $msj = "Masiva Inactivada, Cancelacion Masiva Detenida";
            }
            Cache::forget("listParametrizacionCms");
            Cache::forget("listParametrizacionCmsMasiva");
            Cache::forget("listParametroCms");
            Cache::forget('listParametroCmsMasiva');
            return Response::json(
                array(
                'rst' => $rst,
                'msj' => $msj
                )
            );
        }
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            
            if (Input::get('id')) {
                $parametros = Parametro::getBuscar();
            } else {
                $parametros =
                Parametro::where('proveniencia', '<>', '1')
                            ->where("tipo", "<>", 3);

                if (Input::get('proveniencia')) {
                    $parametros->whereIn("proveniencia", Input::get('proveniencia'));
                }
                if (Input::get('bucket')) {
                    $parametros->whereIn("bucket", Input::get('bucket'));
                }

                if (Input::get('tipo')) {
                    $parametros->whereIn("tipo", Input::get('tipo'));
                }

                if (Input::get('tipo_masiva_id')) {
                    $parametros->whereIn("tipo_masiva_id", Input::get('tipo_masiva_id'));
                }

                if (Input::get('perfilId') && Input::get('perfilId')==17) {
                    $parametros->where("tipo", 4);
                }
                
                $parametros=$parametros->with("tipo_masiva")
                            ->get();
            }

            return Response::json(
                array(
                'rst' => 1,
                'datos' => $parametros,
                )
            );
        }
    }

    public function postBucket()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $buckets = OfscHelper::getFileResource();
            $array_bk=array();
            $i=0;
            foreach ($buckets as $key => $value) {
                 $array_bk[$i]['id']= $value['id'];
                 $array_bk[$i]['nombre']= $value['name'];
                 $i++;
            }

            return Response::json(
                array(
                'rst' => 1,
                'datos' => $array_bk,
                )
            );
        }
    }

    public function postCargaplana()
    {
    /*
        $upload = $this->_uploadController->postCargartmp(0, 0);

        $upload  = json_decode($upload);

        DB::table('tmp_criterios')->truncate();

        $data = Helpers::fileToJsonAddress($upload->file,0);
        $campos=$data[0];

        $query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE tmp_criterios FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES (".$campos.")", addslashes($upload->file));
        
        $total=DB::connection()->getpdo()->exec($query);

        $parametros = Parametro::getGuardarPlano($campos);

        unlink($upload->file);
        
        return Response::json(
                array(
                'rst' => 1,
                'msj' => 'Registro realizado correctamente',
                )
            );*/
        
    }
}
