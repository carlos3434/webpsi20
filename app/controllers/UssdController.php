<?php

class UssdController extends \BaseController {

	 public function getServer()
    {
        try {
            $options = ['cache_wsdl' => WSDL_CACHE_NONE];
            $url = Config::get('legado.ussd.wsdl');
            $server = new SoapServer($url, $options);
            $server->setClass( 'tareas' );
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }

}