<?php
class GeoDistritoPuntoController extends \BaseController
{
    public function postListar()
    {

        $r =GeoDistritoPunto::postGeoDistritoPuntoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


 public function postDepartamento()
 {
        $array['departamento']=Input::get('departamento');
        $r =GeoDistritoPunto::postProvinciaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
 }

    public function postProvincia()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $r =GeoDistritoPunto::postDistritoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


}

