<?php

class MotivoController extends \BaseController
{

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            if (Input::get('quiebre_id')) {
                $quiebreId2 = Input::get('quiebre_id');
                if(strpos(Input::get('quiebre_id'),',') !== false){
                    $quiebreId = explode(",",Input::get('quiebre_id'))[0];
                    $quiebreId2 = explode(",",Input::get('quiebre_id'))[1];
                }else{
                    $quiebreId = Input::get('quiebre_id');                    
                }              
                

                $orderBy = 'm.nombre';
                $select = "m.id, m.nombre, mq.estado";
                if (Input::get('interface')) {
                    $orderBy = 'm.created_at';
                    $select = "m.id, m.nombre";
                }
                
                $motivos = DB::table('motivos AS m')
                        ->leftjoin(
                            'estado_motivo_submotivo AS ems', 
                            'ems.motivo_id', 
                            '=', 
                            'm.id'
                        )
                        ->leftJoin(
                            'motivo_quiebre as mq', function($join) use (
                                $quiebreId,$quiebreId2
                                ) {
                            $join->on('mq.motivo_id', '=', 'm.id')->on(DB::raw("(`mq`.`quiebre_id` = $quiebreId OR `mq`.`quiebre_id` = $quiebreId2)"), DB::raw(''), DB::raw(''));
                              /*  ->where(
                                    'mq.quiebre_id', '=', 
                                    DB::raw($quiebreId)
                                )
                                ->orWhere( 
                                    'mq.quiebre_id', '=', 
                                    DB::raw($quiebreId2)
                                );*/
                            }
                        )
                        ->select(DB::raw($select))
                        ->where('m.estado', '=', 1)
                        //->where('ems.estado', '=', 1)
                        ->where(
                            function($query) {
                                if (Input::get('requerimiento') &&
                                        Input::get('mas')) {
                                    $query->whereRaw(
                                        'CONCAT(ems.req_tecnico,
                                                    "-",ems.req_horario
                                             ) 
                                            IN ("' 
                                        . Input::get('requerimiento') . '")'
                                    )
                                    ->where('mq.estado', 1);
                                } elseif (Input::get('requerimiento')) {
                                    $query->whereRaw(
                                        'CONCAT(ems.req_tecnico,
                                                    "-",ems.req_horario
                                             )="' 
                                        . Input::get('requerimiento') . '"'
                                    )
                                    ->where('mq.estado', 1);
                                }
                            }
                        )
                        ->groupBy('ems.motivo_id')
                        ->orderBy($orderBy)
                        ->get();
            } elseif (Input::get('parametros')) {
                    $first = DB::table('gtm_averias')
                            ->select(
                                    DB::RAW('distinct(motivo) as id'),
                                    DB::RAW('GROUP_CONCAT(
                                    DISTINCT(CONCAT("R",tipo_req)) 
                                        SEPARATOR "|,|") AS relation'),
                                    DB::RAW('concat("AVE-",motivo) as nombre')
                                    )
                            ->where('motivo','<>','')
                            ->groupBy('motivo');

                    $motivos = DB::table('webpsi_coc.prov_catv_tiposact')
                                ->select(
                                    DB::RAW('distinct(motivo) as id'),
                                    DB::RAW('GROUP_CONCAT(
                                    DISTINCT(CONCAT("R",tipo_req)) 
                                        SEPARATOR "|,|") AS relation'),
                                    DB::RAW('concat("PROV-",motivo) as nombre')
                                    );
                                
                        if(Input::get('valor')){
                            $motivos->WhereIn('tipo_req',Input::get('valor'));

                        }
                        $motivos->groupBy('motivo');
                        $motivos->union($first);
                        $motivos=$motivos->get();
            } else {
                $motivos = DB::table('motivos AS m')
                    ->leftjoin(
                        'estado_motivo_submotivo AS ems', 
                        'ems.motivo_id', 
                        '=', 
                        'm.id'
                    )
                    ->select(
                        'm.id', 'm.nombre'
                    )
                    ->where('m.estado', '=', '1')
                    //->where('ems.estado', '=', '1')
                    ->where(
                        function($query) {
                            if (Input::get('requerimiento') &&
                                    Input::get('mas')) {
                                $query->whereRaw(
                                    'CONCAT(ems.req_tecnico,
                                                "-",ems.req_horario
                                         ) 
                                        IN ("' . Input::get('requerimiento') 
                                    . '")'
                                );
                            } elseif (Input::get('requerimiento')) {
                                $query->whereRaw(
                                    'CONCAT(ems.req_tecnico,
                                                "-",ems.req_horario
                                         )="' . Input::get('requerimiento') 
                                    . '"'
                                );
                            }
                        }
                    )
                    ->groupBy('ems.motivo_id')
                    ->orderBy('m.nombre')
                    ->get();
            }

            return Response::json(array('rst' => 1, 'datos' => $motivos));
        }
    }

    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $motivos = Motivo::getMotivos();
            return Response::json(array('rst' => 1, 'datos' => $motivos));
        }
    }

    public function postCambiarestado()
    {
        if (Request::ajax()) {
            $motivoId = Input::get('id');
            $estado = Input::get('estado');
            Motivo::updateEstadoMotivo($motivoId, $estado);
            EstadoMotivoSubmotivo::updateEstadoPorMotivo($motivoId, $estado);
            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

    public function postCrear()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $required = 'required';
            $reglas = array(
                'nombre' => 'required',
            );

            $mensaje = array(
                'required' => ':attribute Es requerido',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            $motivos = new Motivo;
            $motivos['nombre'] = Input::get('nombre');
            $motivos['estado'] = Input::get('estado');
            $motivos['usuario_created_at'] = Auth::id();
            $motivos->save();

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente',
                )
            );
        }
    }

    /**
     * Update the specified resource in storage.
     * POST /motivo/editar
     *
     * @return Response
     */
    public function postEditar()
    {
        if (Request::ajax()) {
            $motivoId = Input::get('id');
            $regex = 'regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required = 'required';
            $reglas = array(
                'nombre' => 
                $required . '|' . $regex . '|unique:motivos,nombre,' .
                $motivoId,
            );

            $mensaje = array(
                'required' => ':attribute Es requerido',
                'regex' => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            $motivos = Motivo::find($motivoId);
            $motivos['nombre'] = Input::get('nombre');
            $motivos['estado'] = Input::get('estado');
            $motivos['usuario_created_at'] = Auth::id();
            $motivos->save();

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

}
