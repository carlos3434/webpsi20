<?php
use Legados\Assia;

class AssiaController extends \BaseController
{
	public function postAssia()
	{
		$assiaAPI = new Assia();
		$telefono = (float) Input::get('telefono');
        
		$response = $assiaAPI->diagnostico($telefono);
		$xml_response = htmlspecialchars(print_r($response, true));


        $xml_response = str_replace('&lt;','<' ,$xml_response );
        $xml_response = str_replace('&gt;','>' ,$xml_response );
        $xml_response = str_replace('&quot;','"' ,$xml_response );

        
        // DEBIDO A QUE VIENE OTRA ESTRUCTURA LA RESPUESTA DEL WS, SE VALIDAN ESTOS CODIGOS
        if (strpos($xml_response,"<ax241:value>17422</ax241:value>")!== false){
            return [
                'msj' => "id_: 17422?/ Desc: No se obtiene respuesta. Revisar linea o modem ?/ Telf:".$telefono
            ];
        }

        if (strpos($xml_response,"Could not find the physical address")!== false){
            return [
                'msj' => "Desc:EL NUMERO NO SE ENCUENTRA PROVICIONADO EN ASSIA / Telf:".$telefono
            ];
        }
        
        if (strpos($xml_response,"<ax241:statusCode>17101</ax241:statusCode>")!== false){
            
            return [
                'msj' => "id_: 17101?/ Desc: Failed to generate the Line Summary result for line... ?/ Telf:".$telefono
            ];
        }
        // ----------------
        $demo = str_replace('<ax241:entries xsi:type="ax241:ResponseDataEntry">','<entries>',$xml_response);



   
        $entries = explode("<ax241:entries xsi:type=\"ax241:ResponseDataEntry\">",$xml_response);
        $entriesClose = explode("</ax241:entries>",$entries[1]);
        $values = $entriesClose[0];

        $values = explode("<ax241:values xsi:type=\"ax241:ValueMappingBean\">",$values);

        $array = array();
        for($i=0;$i<count($values);$i++){
            //echo htmlspecialchars(print_r($values[$i], true)); ;
            $value = str_replace('</ax241:values>','',$values[$i]);
            array_push($array, $value);
        }
        $datosxml= array();
        for($i=1;$i<count($array);$i++){    
            //--- obtener key
            
            $Key = explode("<ax241:key>", $array[$i]);        
            $dataKey = explode("</ax241:key>",$Key[1]);
            $nameKey = $dataKey[0];

            //--- Obtener valor

            $Value = explode("<ax241:value>", $array[$i]);        
            $dataValue = explode("</ax241:value>",$Value[1]);
            $nameValue = $dataValue[0];


            $datosxml[$nameKey] = $nameValue;
            //echo("<script>console.log('Name Key: ".$nameKey." value:".$nameValue."');</script>");
            //echo print_r($array[$i], true); 
        }

        $Rpta="";
        if (array_key_exists('line.summary.action.recommendation', $datosxml)) 
            $Rpta = $Rpta."id_:".$datosxml['line.summary.action.recommendation']."? ";
        
        if (array_key_exists('line.summary.action.recommendation.message', $datosxml)) 
            $Rpta = $Rpta."/Desc:".$datosxml['line.summary.action.recommendation.message']." ?";

            $Rpta = $Rpta."/Telf:".$telefono;

        if (array_key_exists('lineinfo.service_product', $datosxml)) 
            $Rpta = $Rpta."/Serv Com:".$datosxml['lineinfo.service_product'];

        if (array_key_exists('profile.name', $datosxml)) {
            $Perfil = $datosxml['profile.name'];
            if (strpos($datosxml['profile.name'],":")!== false){
                $Perfil = explode(":", $datosxml['profile.name']);
                $Perfil = $Perfil[0];
            }
            $Rpta = $Rpta."/Perf:".$Perfil;
        }

        if (array_key_exists('diagnostics.ds.rate.synchronized', $datosxml))       
            $Rpta = $Rpta."/Sinc:".$datosxml['diagnostics.ds.rate.synchronized'];

        if (array_key_exists('diagnostics.us.rate.synchronized', $datosxml))
            $Rpta = $Rpta."_".$datosxml['diagnostics.us.rate.synchronized'];

        if (array_key_exists('diagnostics.ds.mabr.estimated2', $datosxml))
            $Rpta = $Rpta."/MABR:".$datosxml['diagnostics.ds.mabr.estimated2'];

        if (array_key_exists('diagnostics.us.mabr.estimated', $datosxml))
            $Rpta = $Rpta."_".$datosxml['diagnostics.us.mabr.estimated'];

        return [
            'msj' => $Rpta
        ];
	}
}