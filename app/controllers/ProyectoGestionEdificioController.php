<?php

use Repositories\ProyectoEdificioRepository;
use Repositories\GestionEdificioRepository;

class ProyectoGestionEdificioController extends \BaseController
{

    protected $_proyectoEdificioRepo;
    protected $_gestionEdificioRepo;
    protected $_errorController;

    public function __construct(
        ProyectoEdificioRepository $proyectoEdificioRepo, 
        GestionEdificioRepository $gestionEdificioRepo,
        ErrorController $errorController
    )
    {
        $this->_proyectoEdificioRepo = $proyectoEdificioRepo;
        $this->_gestionEdificioRepo = $gestionEdificioRepo;
        $this->_errorController = $errorController;

        $this->beforeFilter('auth');
    }

    /**
     * Para avanzar o retroceder un estado es necesario ingresar fecha_termino
     * Si se registra sin fecha_termino se mantiene el estado
     * SI no tiene ningun movimiento, iniciara con estado: iniciativa
     * @return array
     */
    public function postGestionar()
    {
        $update = [];
        $aEstado = '';
        $proyectoEdificioId = (Input::has('proyecto_edificio_id')) 
            ? Input::get('proyecto_edificio_id') : '';
        $input = Input::all();
        $input['usuario_created_at'] = Auth::user()->id;
        $proyectoestado = new ProyectoEstado();
        
        try {
            // INICIO -- validar-estado
            unset ($input['fecentrega']);
            unset ($input['fectermino']);
            unset ($input['fecliquidacion']);
            unset ($input['fecrespuesta']);
            unset ($input['estado_provisional']);
            unset ($input['ejecutor_proyecto']);
            unset ($input['is_provisional']); 
            unset ($input['estado_proyecto']);
            
            DB::beginTransaction();
            $ultimo = $this->_gestionEdificioRepo->
                getUltimoGestionesEdificios($proyectoEdificioId);
            if (isset($ultimo->proyectoestadoid) 
                && $input['fecha_termino'] != '') {
                // Obtener el estado anterior y siguiente segun: "ciclo_optimo"
                $aEstado = $proyectoestado
                    ->getSiguienteAnteriorEstado($ultimo->avance);
                /* // Hallar ESTADO por Casuistica // proyectoestadoid
                $aEstado = GestionCasuistica::getSiguienteProyectoEstado(
                    $ultimo->proyectoestadoid, 
                    $ultimo->casuisticaid
                );
                */
                if ($input['retorna'] == 0 && $aEstado->siguiente == 0) {
                    $estado = $ultimo->avance; //proyectoestadoid;
                } elseif ($input['retorna'] == 1 && $aEstado->anterior == 0) {
                    $estado = $ultimo->avance; //proyectoestadoid;
                } else {
                    $estado = $input['retorna'] == 1? 
                        $aEstado->anterior : $aEstado->siguiente;  
                }
            } elseif (isset($ultimo->proyectoestadoid) 
                && isset($input['fecha_termino']) 
                && $input['fecha_termino'] == '') {
                 $estado = $ultimo->avance; //proyectoestadoid;
                 //$ultimo->proyectoestadoid = $ultimo->avance;
            } else {
                $estado = 3; // siguiente estado
                $ultimo = (object) array('avance' => 4);
                //$ultimo->avance = 4; // Estado por defecto: iniciativa
//                return Response::json(
//                    array(
//                        'rst' => 0,
//                        'msj' => 'No existe PROYECTO_ESTADO. '
//                        . 'No se puede realizar la operación.'
//                    )
//                );
            }
            $input['proyecto_estado_id'] = $ultimo->avance; // $estado
            $input['avance'] = $estado;
            // FIN -- validar-estado
            // INICIO -- validar-casuistica - apunta a: NINGUNO
            if (!Input::has('casuistica_id')) {
                $ca = 0;
                // $input['proyecto_estado_id']
                switch ($input['proyecto_tipo_id']) { /* $estado */
                    case 1:
                        $ca = 76;
                        break;
                    case 2:
                        $ca = 77;
                        break;
                    case 3:
                        $ca = 78;
                        break;
                    case 4:
                        $ca = 79;
                        break;
                    case 6:
                       $ca = 80;
                        break;
                    case 7:
                       $ca = 81;
                        break;
                }
                $input['casuistica_id'] = $ca;
            }
            // FIN -- validar-casuistica
            $datos = $this->_gestionEdificioRepo->create($input);
            /*
            $where = array('proyecto_edificio_id' => $proyectoEdificioId);
            $datos = $this->_gestionEdificioRepo
                ->findGestionEdificioByColumn($where);
            $datos = $this->_gestionEdificioRepo->
                getGestionesEdificiosProyectos($proyectoEdificioId);
            */
            // Actualizar proyecto_edificios
            /* proyecto_estado_id */
            $update['proyecto_estado'] = 
                ProyectoEstado::find($input['avance'])->nombre;
            $update['casuistica'] = 
                Casuistica::find($input['casuistica_id'])->nombre;
            $update['proyecto_tipo'] = isset($aEstado->proyectoTipoId)?
                ProyectoTipo::find($aEstado->proyectoTipoId)->nombre : 
                ProyectoTipo::find($input['proyecto_tipo_id'])->nombre;
            $bUpdate = 
            $this->_proyectoEdificioRepo->update($proyectoEdificioId, $update);

//            if (Input::has('energia_estado')) {
//                GestionEdificioMegaproyecto::agregar($input);
//            }
            
            DB::commit();    
            $rst = 1;
            $msn = trans('main.success_gestion_movimiento');
        } catch (Exception $ex) {
            DB::rollback();
            $this->_errorController->saveError($ex);
            $rst = 0;
            $bUpdate =  array();
            $msn = trans('main.error_gestion_movimiento');
            $datos = '';
        }

        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $msn,
                'datos' => $datos,
                'update' => $bUpdate,
                'estado' => isset($update['proyecto_estado'])? 
                    $update['proyecto_estado'] : '',
            )
        );
    }
   
    /**
     * 
     * @return array
     */
    public function postGestionarProvisional()
    {
        $msn = trans('main.no_parametro');
        $rst = 0;
        $data = '';
        $bUpdate =  array();
        if (Input::has('is_provisional') && Input::has('fecentrega') && 
            Input::get('fecentrega')!= "") {
            try{
                DB::beginTransaction();
                $proyectoEdificioId = Input::get('proyecto_edificio_id');
                $provisionales = $this->_proyectoEdificioRepo
                    ->getProvisionales(
                        $proyectoEdificioId, Input::get('estado_proyecto')
                    );

                $data = array (
                    "fecentrega" => Input::get('fecentrega'),
                    "fecrespuesta" => Input::get('fecrespuesta'),
                    "fectermino" => Input::get('fectermino'),
                    "fecliquidacion" => Input::get('fecliquidacion'),
                    "estado_proyecto" => Input::get('estado_proyecto'),
                    "ejecutor_proyecto" => Input::get('ejecutor_proyecto'),
                    "estado_provisional" => Input::get('estado_provisional')
                );
                if (count($provisionales['datos']) > 0 ) {
                    ProvisionalProyecto::agregar(
                        $data, $provisionales['datos'][0]->id, 
                        $proyectoEdificioId, 
                        $provisionales['datos'][0]->id
                    );
                } else {
                    ProvisionalProyecto::agregar(
                        $data, 0, $proyectoEdificioId, 0
                    );
                }
                $estadoProvisional = Input::get('estado_provisional');
                if (!empty($estadoProvisional)) {
                    $update = [];
                    $update['estado_provisional'] = 
                        Input::get('estado_provisional');
                    $bUpdate = $this->_proyectoEdificioRepo->update(
                        $proyectoEdificioId, $update
                    );
                }
                DB::commit();    
                $rst = 1;
                $msn = 'Provisional Registrado con Exito';
            } catch (Exception $ex) {
                DB::rollback();
                $this->_errorController->saveError($ex);
                $rst = 0;
                $msn = trans('main.error_gestion_movimiento');
            }
        }
        
        return Response::json(
            array(
               'rst' => $rst,
                'datos' => $data,
                'msj' => $msn, 
                'update' => $bUpdate
            )
        );
    }
    
    public function postGestionarMegaProyecto()
    {
        $rst = 0;
        $datos = array();
        if (Request::ajax()) {
            //$datos = Input::all();
            if (Input::has('proyecto_edificio_id')) {
                $proyectoEdificioId = Input::get('proyecto_edificio_id');
                $megaproyectos = $this->_proyectoEdificioRepo
                ->getMegaproyectoGestiones($proyectoEdificioId);
                $data = array (
                    "energia_estado" => Input::get("energia_estado"),
                    "fo_estado" => Input::get("fo_estado"),
                    "troba_nueva" => Input::get("troba_nueva"),
                    "troba_fecha_fin" => Input::get("fecha_fin"),
                    "troba_estado" => Input::get("troba_estado")
                );
                if (count($megaproyectos['datos']) > 0) {
                    MegaProyecto::agregar(
                        $data, 
                        $megaproyectos['datos'][0]->id, 
                        $proyectoEdificioId, 
                        $megaproyectos['datos'][0]->id
                    );
                    $rst = 1;
                } else {
                    MegaProyecto::agregar($data, 0, $proyectoEdificioId, 0);
                    $rst = 1;
                }
                
            } else {
                
            }
            return Response::json(
                array(
                   'rst' => $rst,
                    'datos' => $datos,
                )
            );
        }
    }
    
    /**
     * 
     * @return type
     */
    public function postListargestiones()
    {
        $rst = 0;
        $datos = array();
        if (Input::has('proyecto_edificio_id')) {
            $datos = $this->_gestionEdificioRepo
                ->getGestionesEdificiosProyectos(
                    Input::get('proyecto_edificio_id')
                );
            $rst = 1;
        }
        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos,
            )
        );
    }
    
    public function postListarProvisionales()
    {
       $rst = 0;
        $datos = array();
        if (Input::has('proyecto_edificio_id')) {
            $datos = $this->_proyectoEdificioRepo
                ->getProvisionales(
                    Input::get('proyecto_edificio_id'),
                    Input::get('proyecto_estado'), Input::get('parent')
                );
            if (count($datos['datos']) > 0) {
                foreach ($datos['datos'] as $key => $value) {
                    $value->fila = count($datos['datos']) - $key;
                    $datos['datos'][$key] = $value;
                }
            }
            $rst = 1;
        }
        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos['datos'],
                'enviado' => Input::all()
            )
        );
    }
    
    public function postListarMegaProyecto()
    {
        $rst = 0;
        $datos = array();
        
        if (Input::has('proyecto_edificio_id')) {
            $datos = $this->_proyectoEdificioRepo
                ->getMegaproyectoGestiones(
                    Input::get('proyecto_edificio_id'), Input::get('parent')
                );
            if (count($datos['datos']) > 0) {
                foreach ($datos['datos'] as $key => $value) {
                    $value->fila = count($datos['datos']) - $key;
                    $datos['datos'][$key] = $value;
                }
            }
            $rst = 1;
        }
        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos['datos'],
                'enviado' => Input::all()
            )
        );
    }
    
    public function postListarPapelera()
    {
//        $rst = 0;
//        $datos = array();
        $datos = $this->_proyectoEdificioRepo->getRowsSoftDelete();
        $rst = 1;
        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos
            )
        );
    }
    
}
//fin class
