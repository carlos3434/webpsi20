<?php
class GeoAntenaCircleController extends \BaseController
{
    public function postListar()
    {
        $r =GeoAntenaCircle::postGeoAntenaCircleAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

       // echo $elementoid.' '.$origen;

    }

    public function postDepartamento()
    {
        $array['departamento']=Input::get('departamento');
        $r =GeoAntenaCircle::postProvinciaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postProvincia()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $r =GeoAntenaCircle::postDistritoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postDistrito()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $array['distrito']=Input::get('distrito');
        $r =GeoAntenaCircle::postTecnologiaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postTecnologia()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $array['distrito']=Input::get('distrito');
        $array['tecnologia']=Input::get('tecnologia');
        $r =GeoAntenaCircle::postAntenaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postAntenacircle()
    {
       $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $array['distrito']=Input::get('distrito');
        $array['tecnologia']=Input::get('tecnologia');
        $array['antenacircle']=Input::get('antenacircle');
        $r =GeoAntenaCircle::postAntenacircleCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postListarcoord()
    {
        $array=explode("-", Input::get('subelemento'));

        $r =GeoAntenaCircle::postCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        } 
    }


}

