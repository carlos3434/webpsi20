<?php
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;
use Ofsc\Activity;
use Legados\models\SolicitudTecnicaGestion as Gestion;

class StmovistarappController extends BaseController
{
    public function getServer()
    {
        function activity($request)
        {   
            try {
                $response=new stdClass();                
                $response->accion=1;
                $response->error=false;

                $loguin=Usuariowsdl::getAuth($request);
                if(!$loguin) {
                    $response->error=true;
                    $error = Movistarapperror::find(15);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }
                Auth::loginUsingId($loguin->usuario_id);

                $proveniencia=2;
                $intervalo=array('AM' => '09-13', 'PM'=>'13-18');

                $validator = Validator::make(json_decode(json_encode($request),true), Movistarapplogrecepcion::$rules);
                if ($validator->fails()) {
                    $msjError = $validator->messages()->all()[0];
                    $codError = Movistarapplogrecepcion::getErrorByMessage($msjError);
                    $response->error=true;
                    $response->code_error= $codError;
                    $response->message = $msjError;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                if($request->date=='' || 
                    $request->location=='' || 
                    !array_key_exists($request->time_slot, $intervalo)) {
                    $response->error=true;
                    $error = Movistarapperror::find(2);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                $inputs=array();
                $inputs['solicitud']=$request->solicitud;
                $inputs['hf']=$request->date."||".
                            $request->location."||".
                            $request->time_slot."||".
                            $intervalo[$request->time_slot];

                $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $request->solicitud)->getUltimo()->first();
                if(is_null($solicitud)) {
                    $response->error=true;
                    $error = Movistarapperror::find(3);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                if(is_null($solicitud->quiebre_id) ||
                   is_null($solicitud->actividad_id) || 
                   is_null($solicitud->actividad_tipo_id)){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                }
                if(intval($solicitud->estado_st)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado st: ".$error->descripcion_error;
                }
                if(intval($solicitud->estado_ofsc_id)!==1 && intval($solicitud->estado_ofsc_id)!==8){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado ofsc: ".$error->descripcion_error;
                    //validar que pasa si una actividad esta cancelada
                }
                if(intval($solicitud->estado_aseguramiento)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado aseguramiento: ".$error->descripcion_error;
                }

                $dteEnd=new DateTime("now");
                if(intval($solicitud->tipo_legado)==1) { //cms
                    $detallesolicitud = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                    $inputs['empresa_id']=$detallesolicitud['codigo_contrata'];
                    $inputs['fono1']=$detallesolicitud->telefono1;
                    $inputs['fono2']=$detallesolicitud->telefono2;

                    $dteStart= DateTime::CreateFromFormat('Y-m-d H:i:s', $detallesolicitud->fecha_registro_requerimiento.' '.$detallesolicitud->hora_registro_requerimiento);
                } else {
                    if($solicitud->actividad_id==2) {
                        $detallesolicitud = SolicitudTecnicaGestelProvision::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                        $inputs['empresa_id']=$detallesolicitud['contrata'];
                        $inputs['fono1']=$detallesolicitud->telefono_contacto;
                        $inputs['fono2']=$detallesolicitud->telefono_referencia;
                        $dteStart= DateTime::CreateFromFormat('Y-m-d H:i:s', $detallesolicitud->fecha_registro_peticion);
                        $proveniencia=3;

                    } else {
                        $detallesolicitud = SolicitudTecnicaGestelAveria::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                        $inputs['empresa_id']=$detallesolicitud['contrata'];
                        $inputs['fono1']=$detallesolicitud->telefono_contacto;
                        $inputs['fono2']=$detallesolicitud->telefono_referencia;
                        $dteStart= DateTime::CreateFromFormat('Y-m-d H:i:s', $detallesolicitud->fecha_registro);
                        $proveniencia=4;
                    }
                }

                $dteDiff  = $dteStart->diff($dteEnd);
                if($dteDiff->format('%R%a')<1){
                    $response->error=true;
                    $error = Movistarapperror::find(7);
                    $response->code_error= $error->code_error;
                    $response->message = "fecha registro: ".$error->descripcion_error;
                }

                if (!$response->error) {
                    $arrayvalidacion=array();
                    $arrayvalidacion['tipo']=2; //GOTOA
                    $arrayvalidacion['estado']=1;
                    $arrayvalidacion['proveniencia']=$proveniencia;
                    if (!$detallesolicitud->validarParam($arrayvalidacion)) {
                        $response->error=true;
                        $error = Movistarapperror::find(9);
                        $response->code_error= $error->code_error;
                        $response->message = $error->descripcion_error;
                    }
                }

                if(!$response->error){
                    $inputs['actividad_id']=$solicitud->actividad_id;
                    $inputs['actividad_tipo_id']=$solicitud->actividad_tipo_id;
                    $inputs['agdsla']="agenda";
                    $inputs['codactu']=$solicitud->num_requerimiento;
                    $inputs['fono3']="";
                    $inputs['fono4']="";
                    $inputs['fono5']="";
                    $inputs['observacion_toa']=$request->observation;
                    $inputs['quiebre']=$solicitud->quiebre_id;
                    $inputs['slaini']="";
                    $inputs['solicitud_tecnica_id']=$solicitud->solicitud_tecnica_id;
                    $inputs['estado_aseguramiento']=1;

                    \Config::set("ofsc.auth.company", "telefonica-pe.test");
                    $responseagenda = Helpers::ruta(
                        'bandejalegado/envioofsc',
                        'POST',
                        $inputs,
                        false
                    );
                    
                    if ($responseagenda->rst==1) {
                        $response=$request;//reinicia $response
                        unset($response->intervalo_tiempo);
                        $response->accion=1;
                        $response->error=false;
                    } else {
                        $response->error=true;
                        $error = Movistarapperror::find(8);
                        $response->code_error= $error->code_error;
                        $response->message = $error->descripcion_error;
                        $response->message_detalle=isset($responseagenda->msj) ? 
                                        $responseagenda->msj:""; 
                    }
                }

                Movistarapplogrecepcion::tracer($request, $response);
                Auth::logout();
                return $response;
            } catch (Exception $e) {
                $response=new stdClass();
                $response->error=true;
                $error = Movistarapperror::find(5);
                $response->code_error= $error->code_error;
                $response->message = $error->descripcion_error;
                $response->accion=1;
                Movistarapplogrecepcion::tracer($request, $response,$e);
                return $response;
            }
        }

        function capacity($request)
        {
            //falta validar antes de las 7 despues de las 9 
            try {
                $response=new stdClass();
                $response->error=false;
                $response->accion=2;

                $loguin=Usuariowsdl::getAuth($request);
                if(!$loguin) {
                    $response->error=true;
                    $error = Movistarapperror::find(15);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }
                Auth::loginUsingId($loguin->usuario_id);
                $proveniencia=2; //cms

                $validator = Validator::make(json_decode(json_encode($request),true), Movistarapplogrecepcion::$rules);
                if ($validator->fails()) {
                    $msjError = $validator->messages()->all()[0];
                    $codError = Movistarapplogrecepcion::getErrorByMessage($msjError);
                    $response->error=true;
                    $response->code_error= $codError;
                    $response->message = $msjError;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                $inputs=array();
                $inputs['solicitud']=$request->solicitud;
                    
                $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $request->solicitud)->getUltimo()->first();

                if(is_null($solicitud)) {
                    $response->error=true;
                    $error = Movistarapperror::find(3);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                if(is_null($solicitud->quiebre_id) ||
                    is_null($solicitud->actividad_id) || 
                    is_null($solicitud->actividad_tipo_id)){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                }
                if(intval($solicitud->estado_st)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado st: ".$error->descripcion_error;
                }
                if(intval($solicitud->estado_ofsc_id)!==1 && intval($solicitud->estado_ofsc_id)!==8){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado ofsc: ".$error->descripcion_error;
                }
                if(intval($solicitud->estado_aseguramiento)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado aseguramiento: ".$error->descripcion_error;
                }
    
                if(intval($solicitud->tipo_legado)==1) { //cms
                        $detallesolicitud = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                        $inputs['contrata']=$detallesolicitud['codigo_contrata'];
                        $inputs['nodo']=$detallesolicitud['cod_nodo'];
                        $inputs['plano']=$detallesolicitud['cod_plano'];
                        $inputs['troba']=$detallesolicitud['cod_troba'];
                        $inputs['zonal']=$detallesolicitud['zonal'];
                } else {
                    if($solicitud->actividad_id==2) {
                        $detallesolicitud = SolicitudTecnicaGestelProvision::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->ffttcobre()->first();
                        $inputs['ffttcobre']=$detallesolicitud->ffttcobre;
                        $inputs['contrata']=$detallesolicitud['contrata'];
                        $proveniencia=3;
                    } else {
                        $detallesolicitud = SolicitudTecnicaGestelAveria::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                        $inputs['contrata']=$detallesolicitud['contrata'];
                        $inputs['cabecera_mdf']=$detallesolicitud['cabecera_mdf'];
                        $inputs['cod_armario']=$detallesolicitud['cod_armario'];
                        $inputs['cable']=$detallesolicitud['cable'];
                        $inputs['zonal']=$detallesolicitud['zonal'];
                        $proveniencia=4;
                    }
                }
        
                if (!$response->error) {
                    $arrayvalidacion=array();
                    $arrayvalidacion['tipo']=2; //GOTOA
                    $arrayvalidacion['estado']=1;
                    $arrayvalidacion['proveniencia']=$proveniencia;
                    if (!$detallesolicitud->validarParam($arrayvalidacion)) {
                        $response->error=true;
                        $error = Movistarapperror::find(9);
                        $response->code_error= $error->code_error;
                        $response->message = $error->descripcion_error;
                    }
                }
                
                if (!$response->error) {
                    unset($response->error);
                    $inputs['actividad_id']=$solicitud->actividad_id;
                    $inputs['actividad_tipo_id']=$solicitud->actividad_tipo_id;
                    $inputs['codactu']=$solicitud->num_requerimiento;            
                    $inputs['json']=1;
                    $inputs['quiebre']=$solicitud->quiebre_id;
                    $inputs['tipo_legado']=$solicitud->tipo_legado;

                    \Config::set("ofsc.auth.company", "telefonica-pe.test");
                    $responsecapacidad = Helpers::ruta(
                            'bandejalegado/capacity-multiple',
                            'POST',
                            $inputs,
                            false
                        );
                    
                    if($responsecapacidad->rst==1 && count($responsecapacidad->capacity)>0){
                        $response->error=false;
                        $response->solicitud=$request->solicitud;
                        $fecha=date('Y-m-d');
                        $hora=date('H');
                        $diaSemana = date("N");
                        $diaMes = date("d");

                        //Aumentandole 7 dias
                        $ini = date("Y/n/" . $diaMes);                
                        $fchini = new DateTime($ini);
                        $fchini->add(new DateInterval('P6D'));
                        $fechaFin = $fchini->format('Y-m-d');

                        $tipo=7;
                        $tipopersonaid=3;
                        if(Auth::user()->tipo_persona_id>0)
                        $tipopersonaid=Auth::user()->tipo_persona_id;
                        
                        $TipoPersona=TipoPersona::where('id','=',$tipopersonaid)->First();
                        $duration=$responsecapacidad->duracion;
                        $duration=$responsecapacidad->duracion>0 ? $responsecapacidad->duracion:120;

                        $bucket_id=isset($responsecapacidad->location[0]) ? $responsecapacidad->location[0] : "";

                        $horarios=Agenda::postHorarios('1',array(
                            $fecha,
                            $diaSemana,
                            $diaSemana,
                            $diaSemana,
                            $bucket_id,
                            $tipopersonaid,
                            $tipo,
                            $fecha,
                            $fechaFin,
                            $detallesolicitud['codigo_contrata'],
                            $detallesolicitud['zonal'],
                            $solicitud->quiebre_id
                        ));
                        if(count($horarios)>0){
                            $arraycapacity=json_decode(json_encode($responsecapacidad->capacity), true);
                            $capacityList=array();
                            foreach ($horarios as $key => $data) {
                                if(array_key_exists($data->fecha.'|'.$data->hora, $responsecapacidad->capacity)){
                                    $value=$arraycapacity[$data->fecha.'|'.$data->hora];
                                    if ($hora>=7 && $hora<13 && $value['date']==$fecha && $value['time_slot']=='AM') {
                                        $arraycapacity[$data->fecha.'|'.$data->hora]['available']=0;
                                    } elseif ($hora>=13 && $value['date']==$fecha && ($value['time_slot']=='AM' || $value['time_slot']=='PM' )) {
                                            $arraycapacity[$data->fecha.'|'.$data->hora]['available']=0;
                                    } else {
                                        $cupototal=intval($value['quota']/$duration); 
                                        $cupoxtipopersona=round($cupototal*$TipoPersona->porcent/100);
                                        $cupoxhorarioxpersona=round($cupoxtipopersona*$data->cupo/100);
                                        $cantLibres = (int) ($cupoxhorarioxpersona-$data->ocupado);
                                        $arraycapacity[$data->fecha.'|'.$data->hora]['available']=$cantLibres;
                                        //$libres = $cantLibres."/".$cupoxhorarioxpersona;
                                    }
                                    $capacityList[]=json_decode(json_encode($arraycapacity[$data->fecha.'|'.$data->hora]));
                                }
                            }
                            $response->result=array(
                            'capacity_list' =>$capacityList);
                        } else {
                            $response->error=true;
                            $error = Movistarapperror::find(14);
                            $response->code_error= $error->code_error;
                            $response->message = $error->descripcion_error." ".$bucket_id;
                        }
                    } else {
                        $response->error=true;
                        $error = Movistarapperror::find(6);
                        $response->code_error= $error->code_error;
                        $response->message = $error->descripcion_error;
                        $response->message_detalle=isset($responsecapacidad->msj) ? 
                                        $responsecapacidad->msj:""; 
                    }
                }

                Movistarapplogrecepcion::tracer($request, $response);
                Auth::logout();
                return $response;
            } catch (Exception $e) {
                $response=new stdClass();
                $response->error=true;
                $error = Movistarapperror::find(5);
                $response->code_error= $error->code_error;
                $response->message = $error->descripcion_error;
                $response->accion=1;
                Movistarapplogrecepcion::tracer($request, $response,$e);
                return $response;
            }
        }

        function cancel($request)
        {
            try {
                $response=new stdClass();
                $response->error=false;
                $response->accion=3;

                $loguin=Usuariowsdl::getAuth($request);
                if(!$loguin) {
                    $response->error=true;
                    $error = Movistarapperror::find(15);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }
                Auth::loginUsingId($loguin->usuario_id);

                $validator = Validator::make(json_decode(json_encode($request),true), Movistarapplogrecepcion::$rules);
                if ($validator->fails()) {
                    $msjError = $validator->messages()->all()[0];
                    $codError = Movistarapplogrecepcion::getErrorByMessage($msjError);
                    $response->error=true;
                    $response->code_error= $codError;
                    $response->message = $msjError;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }

                $intervalo=array('AM' => '09-13', 'PM'=>'13-18');

                $inputs=array();
                $inputs['solicitud']=$request->solicitud;

                $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $request->solicitud)->getUltimo()->first();
                
                if(is_null($solicitud)) {
                    $response->error=true;
                    $error = Movistarapperror::find(3);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                    Movistarapplogrecepcion::tracer($request, $response);
                    return $response;
                }
                if(is_null($solicitud->quiebre_id) ||
                   is_null($solicitud->actividad_id) || 
                   is_null($solicitud->actividad_tipo_id)){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = $error->descripcion_error;
                }
                if(intval($solicitud->estado_st)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado st: ".$error->descripcion_error;
                }
    
                if(intval($solicitud->estado_ofsc_id)!==1) {
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado ofsc: ".$error->descripcion_error;
                }
                if(intval($solicitud->estado_aseguramiento)!==1){
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "estado aseguramiento: ".$error->descripcion_error;
                }

                $dteEnd=new DateTime("now");
                if(intval($solicitud->tipo_legado)==1) { //cms
                    $detallesolicitud = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solicitud->solicitud_tecnica_id)->first();
                    $inputs['empresa_id']=$detallesolicitud['codigo_contrata'];
                    $dteStart= DateTime::CreateFromFormat('Y-m-d H:i:s', $detallesolicitud->fecha_registro_requerimiento.' '.$detallesolicitud->hora_registro_requerimiento);

                    $dteDiff  = $dteStart->diff($dteEnd);
                    if($dteDiff->format('%R%a')<1){ // > a 24 horas
                        $response->error=true;
                        $error = Movistarapperror::find(7);
                        $response->code_error= $error->code_error;
                        $response->message = "fecha registro: ".$error->descripcion_error;
                    }
                } else {
                    $response->error=true;
                    $error = Movistarapperror::find(4);
                    $response->code_error= $error->code_error;
                    $response->message = "tipo legado: ".$error->descripcion_error;
                }

                if (!$response->error) {
                    $response=$request;
                    $response->accion=3;
                    $response->error=false;
                    unset($response->intervalo_tiempo);
    
                    $inputs['hf']=date("Y-m-d")."||".
                                $solicitud->resource_id."||||";
                    $inputs['actividad_id']=$solicitud->actividad_id;
                    $inputs['actividad_tipo_id']=$solicitud->actividad_tipo_id;
                    $inputs['agdsla']="sla";
                    $inputs['codactu']=$solicitud->num_requerimiento;            
                    $inputs['fono1']=$detallesolicitud->telefono1;
                    $inputs['fono2']=$detallesolicitud->telefono2;
                    $inputs['fono3']="";
                    $inputs['fono4']="";
                    $inputs['fono5']="";
                    $inputs['observacion_toa']=$request->observation;
                    $inputs['quiebre']=$solicitud->quiebre_id;
                    $inputs['slaini']="";
                    $inputs['solicitud_tecnica_id']=$solicitud->solicitud_tecnica_id;
                    $inputs['estado_aseguramiento']=1;
                    
                    \Config::set("ofsc.auth.company", "telefonica-pe.test");
                    $responseagenda = Helpers::ruta(
                        'bandejalegado/envioofsc',
                        'POST',
                        $inputs,
                        false
                    );
        
                    if($responseagenda->rst!=1) {
                        $response->error=true;
                        $error = Movistarapperror::find(1);
                        $response->code_error= $error->code_error;
                        $response->message = $error->descripcion_error;
                        $response->message_detalle==isset($responseagenda->msj) ? 
                                        $responseagenda->msj:""; 
                    }
                }
                Movistarapplogrecepcion::tracer($request, $response);
                Auth::logout();

                return $response;
            } catch (Exception $e) {
                $response=new stdClass();
                $response->error=true;
                $error = Movistarapperror::find(5);
                $response->code_error= $error->code_error;
                $response->message = $error->descripcion_error;
                $response->accion=1;
                Movistarapplogrecepcion::tracer($request, $response,$e);
                return $response;
            }
        }

        try {
            $server = new SoapServer(Config::get('legado.appmovistar.wsdl'), array('cache_wsdl' => WSDL_CACHE_NONE));
            $server->addFunction("activity");
            $server->addFunction("capacity");
            $server->addFunction("cancel");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}