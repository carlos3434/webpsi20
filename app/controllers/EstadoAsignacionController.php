<?php
use Repositories\EstadoAsigRepositoryInterface as EstadoAsig;

class EstadoAsignacionController extends \BaseController {

	protected $_estadoasig;
    public function __construct(EstadoAsig $estadoasig)
    {
        $this->_estadoasig = $estadoasig;
    }

	/**
	 * Display a listing of the resource.
	 * GET /estadoasignacion
	 *
	 * @return Response
	 */

	public function postCargar(){
		$data = $this->_estadoasig->listar();
        return $data; 	
	}

	public function postActualizar(){
		if (Request::ajax()) {
			$act = $this->_estadoasig->update();
			return Response::json(
                array(
                    'rst'    => ($act) ? 1 : 2,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}

	public function postCreate(){
		if (Request::ajax()) {	
	        if ($this->_estadoasig->validate->fails()) {
	            return Response::json(
	                array(
	                'rst' => 0,
	                'msj' => $this->_estadoasig->validate->messages(),
	                )
	            );
	        }			
			$rst = $this->_estadoasig->update();
			return Response::json(
                array(
                    'rst'    => ($rst) ? 1 : 0,
                    'msj'    => "Registrado Correctamente",
                )
            );	

		}
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /estadoasignacion/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /estadoasignacion
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /estadoasignacion/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /estadoasignacion/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /estadoasignacion/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /estadoasignacion/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}