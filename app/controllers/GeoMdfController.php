<?php
class GeoMdfController extends \BaseController
{
     public function postListar()
     {

       if (Input::get('modulo') && Input::get('modulo')=='geoplan') {
           $zonal = Input::get('zonal', 'LIM');
           $m =GeoMdf::getMdfzonal($zonal);
       } else {
           $m =GeoMdf::postGeoMdfAll();
       }
            
        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $m
                )
            );
        } 
        
     }

    public function postFiltrarcoord()
    {
        $id = Input::get('nodo');
        $tipo= Input::get('tipo');

        if ( $tipo[0]=='mdf' ) {
           $m =GeoMdf::getCoordMdf($id);
        }
        if ( $tipo[0]=='nodo' ) {
           $m =GeoNodoPoligono::getCoordNodo($id);
        }

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $m,
                    'seleccionado' => $id
                )
            );
        } 
        
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoMdf::postMdfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

}

