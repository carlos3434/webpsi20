<?php
class GeoProyectoCapaElementoController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter('auth');
    }
    public function postUpdate()
    { 
        $datos = GeoProyectoCapaElemento::actualizar();
         if (Request::ajax()) {
            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
    }

    public function postAgregarelemento()
    {
        //$detalles=Input::get('detalles');
        $mapa=Input::get('mapa');
        $idcapa=Input::get('id');
        $usuario=Auth::id();
        
        $arraymapa= array();

        foreach ($mapa as $index => $value) {
            if ($value) {
                  $value['capa_id']=$idcapa;
                  $value['tabla_id']=$value['elemento'];
                  unset($value['elemento']);
                  unset($value['capa']);

                  $delemento=GeoProyectoCapaElemento::agregar(
                                                  $value);
            }
        } 

            $msj='Se Actualizo el Proyecto correctamente';
            $rst=1;


            if ( Request::ajax() ) {
                return Response::json(
                    array(
                        'rst' => $rst, 
                        'datos' => $delemento,
                        'msj' => $msj
                    )
                );
            } 

    }

}

