<?php
class GeoProyectoCapaElementoController extends \BaseController
{
    public function postUpdate()
    {

        $datas=array(
                    Input::get('idelemento'),
                    Input::get('borde'),
                    Input::get('grosorlinea'),
                    Input::get('fondo'),
                    Input::get('opacidad'),
                    Input::get('idproyecto'),
                    Auth::user()->id
                    );
        
         if (Request::ajax()) {
            $datos = GeoProyectoCapaElemento::getUpdateElemento($datas);

            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
    }

    public function postAgregarelemento()
    {
        //$detalles=Input::get('detalles');
        $mapa=Input::get('mapa');
        $idcapa=Input::get('idcapa');
        $usuario=Auth::user()->id;

        
        $arraymapa= array();
        $y=0;

        foreach ($mapa as $index => $value) {

            if ($value) {
                $arraymapa[$y]="(".$idcapa.",
                        '".$value['subelemento']."',
                        '".$value['tabla_detalle_id']."',
                        '".$value['orden']."','".$value['borde']."',
                        '".$value['grosorlinea']."','".$value['fondo']."',
                        '".$value['opacidad']."',
                        '".$value['elemento']."',
                        '".$value['tipo']."')";
                $y++;
            }

        } 

            $valele= implode(",", $arraymapa);
            $delemento=GeoProyectoCapaElemento::getAgregarCapaElemento($valele);
            $datos=array(
                $idcapa,
                Auth::user()->id
                );
            $upProyecto=GeoProyecto::getUpdateProyectoCapaElem($datos);
            $msj='Se Actualizo el Proyecto correctamente';
            $rst=1;


            if ( Request::ajax() ) {
                return Response::json(
                    array(
                        'rst' => $rst, 
                        'datos' => $delemento,
                        'msj' => $msj
                    )
                );
            } 

    }

    public function postDelete()
    {

        $datas=array(
                    Input::get('idelemento'),
                    Input::get('idproyecto'),
                    Auth::user()->id
                    );
        
         if (Request::ajax()) {
            $datos = GeoProyectoCapaElemento::getDeleteElemento($datas);

            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
         } 
    }

}

