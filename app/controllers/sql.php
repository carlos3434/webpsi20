SELECT codactu, SUM(legados) AS enLegado,
         SUM(psi) AS enPSI, SUM(toa) AS enTOA
         , toa.*
        FROM
        (
        -- legados
            SELECT t.codigo_req AS codactu, '1' AS legados, '0' AS psi, '0' AS toa
 
            FROM schedulle_sistemas.prov_pen_catv_pais t
            LEFT JOIN `webpsi_coc`.`prov_catv_tiposact` p ON t.codigo_tipo_req=p.tipo_req AND t.codigo_motivo_req=p.motivo
            WHERE t.nodo IN ('LM' , 'LL' , 'SP' ) AND p.averia_m1='MOVISTAR UNO' AND fecha_registro >?--  AND t.contrata='245'
        --  filtrar las que esten liquidadas
        UNION
            -- psi
            SELECT psi.codactu, '0' AS legados, '1' AS psi, '0' AS toa
 
            FROM
            (
            SELECT  codigo_req AS codactu
            FROM  webpsi_coc.tmp_provision t1
            WHERE  t1.mdf IN ('LM' , 'LL' , 'SP' )
                AND t1.quiebre = 'PENDMOV1'
                AND t1.eecc_final  ='LARI'  --  and  t1.contrata='245'   and t1.area2!='PAI'
                AND   fecha_Reg >?
                AND   t1.codigo_req  NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' )
                 AND  t1.codigo_req  NOT IN (
                    SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais
                )
            UNION ALL
 
            SELECT    codactu
            FROM  psi.ultimos_movimientos t2
            WHERE t2.mdf IN ('LM' , 'LL' , 'SP' )
                AND t2.quiebre_id='25'
                AND t2.empresa_id = '5'  --  and t2.contrata='245' AND t2.area2!='PAI'
                AND (t2.estado_legado ='PENDIENTE'  OR t2.estado_legado IS NULL)
                AND (t2.tipo_averia='pen_prov_catv'  OR t2.tipo_averia  IS NULL )
                AND t2.estado_id NOT  IN  ('4','5','6','14','15','17' )
                AND t2.fecha_registro   >=?
                AND codactu NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' 
                )
                AND codactu  NOT IN (
                    SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais
                )
            ) psi
        UNION
        --  toa
            SELECT appt_number  AS codactu , '0' AS legados, '0' AS psi, '1' AS toa
 
            FROM actuaciones_toa_tmp
            WHERE STATUS!='suspended'
            GROUP BY aid
        )  AS  total   LEFT JOIN    actuaciones_toa_tmp AS  toa  ON  total.codactu = toa.appt_number
        WHERE   total.codactu!='0'
        GROUP BY  total.codactu
      