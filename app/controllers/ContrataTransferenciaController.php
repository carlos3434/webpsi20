<?php
class ContrataTransferenciaController extends \BaseController
{
    public function postList()
    {
        $datos = ContrataTransferencia::select(
                    'contrata_transferencia.id',
                    'contrata_transferencia.empresa_id',
                    'contrata_transferencia.actividad_id',
                    'contrata_transferencia.tipo_legado',
                    'contrata_transferencia.area_transferencia',
                    'contrata_transferencia.contrata_transferencia',
                    'contrata_transferencia.estado',
                    'e.nombre as empresa',
                    'a.nombre as actividad',
                    \DB::raw('if(contrata_transferencia.tipo_legado = 1, "CMS", "GESTEL") as tipo_legado_nombre')
                )
                ->leftJoin('empresas as e', 'e.id', '=', 'empresa_id')
                ->leftJoin('actividades as a', 'a.id', '=', 'actividad_id')
                ->get();

        return [
            'datos' => $datos
        ];
    }

    public function postInsert()
    {
        $motivoOfsc = new ContrataTransferencia();
        $motivoOfsc->empresa_id = Input::get('empresa_id');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->area_transferencia = Input::get('area_transferencia');
        $motivoOfsc->contrata_transferencia = Input::get('contrata_transferencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }

    public function postUpdate()
    {
        $motivoOfsc = ContrataTransferencia::find(Input::get('id'));
        $motivoOfsc->empresa_id = Input::get('empresa_id');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->area_transferencia = Input::get('area_transferencia');
        $motivoOfsc->contrata_transferencia = Input::get('contrata_transferencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }
}

