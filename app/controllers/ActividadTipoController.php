<?php

class ActividadTipoController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
          //  if ( Input::get('actividad_id') ) {
            //    $actividadId=Input::get('actividad_id');
                $r =  DB::table('actividades_tipos AS a')
                            ->select(
                                'a.id',
                                'a.nombre',
                                DB::raw(
                                    'CONCAT("A",actividad_id) as relation'
                                )
                            )
                            ->where('a.estado', '=', '1')
                          //  ->where('a.actividad_id', '=', $actividadId)
                            ->orderBy('a.nombre')
                            ->get();
       //     }
            /* else {
                $r = ActividadTipo::Listar();
            }*/
            return Response::json(array('rst' => 1, 'datos' => $r));
        }
    }

}
