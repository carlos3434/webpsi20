<?php

class ActividadTipoController extends BaseController
{
    /*ErrorController $errorController */
    public function __construct()
    {
        /*$this->error = $errorController;*/
    }

    public function postListar()
    {
        $r = DB::table('actividades_tipos')
                        ->select(
                            'id', 
                            'nombre' 
                        )
                        ->where(
                            function($query)
                            {
                                if ( Input::has('estado') ) {
                                    $query->where(
                                        'estado', '=', Input::has('estado')
                                    );
                                }
                            }
                        )
                        ->get();

        if (Request::ajax()) {
           /*//  if ( Input::get('actividad_id') ) {
            //    $actividadId=Input::get('actividad_id');
                $r =  DB::table('actividades_tipos AS a')
                            ->select(
                                'a.apocope as id',
                                'a.apocope as nombre',
                                DB::raw(
                                    'CONCAT("A",actividad_id) as relation'
                                ),
                                'a.apocope'
                            )

                            ->where( 
                                function($query){
                                    if(Input::has('apocope')){
                                        $query->where('a.apocope', '<>', '');                                
                                    }                                    
                                }
                            )

                          //  ->where('a.actividad_id', '=', $actividadId)
                            ->orderBy('a.nombre')
                            ->limit(4)
                            ->get();
       //     }*/
            /* else {
                $r = ActividadTipo::Listar();
            }*/
            return Response::json(array('rst' => 1, 'datos' => $r));
        }
    }

}
