<?php
use App\Http\Requests;
use Illuminate\Http\Request;
use Ofsc\Activity;

class HistoricoMapaTecnicoController extends BaseController
{
    /**
     * guadar actividades mostradas en el mapa de tecnicos
     */
    public function postCrear()
    {
        $dateFormat = date("dmYHis");
        $bucket = Input::get('resource_id');
        $nombreJson = Input::get('nombreJson');
        $entorno = Input::get('entorno');
        $rutaPrincipalReportes = public_path() . "/json" ."/reportes";
        $rutaFolderBucket = $rutaPrincipalReportes."/".$bucket;
        if ($entorno == 'TRAIN') {
            $rutaFolderBucket = $rutaPrincipalReportes."/".$bucket.$entorno;
        }
        $rutaFolderBucketActividad = $rutaFolderBucket."/actividades";
        $rutaFolderBucketTecnico = $rutaFolderBucket."/tecnicos";
        $rutaFileActividad = $rutaFolderBucketActividad."/".$dateFormat.".json";
        $rutaFileTecnico = $rutaFolderBucketTecnico."/".$dateFormat.".json";

        if (!File::exists($rutaPrincipalReportes)) {
            File::makeDirectory($rutaPrincipalReportes, $mode = 0777, true, true);
        }
        if (!File::exists($rutaFolderBucket)) {
            File::makeDirectory($rutaFolderBucket, $mode = 0777, true, true);
        }
        if (!File::exists($rutaFolderBucketActividad)) {
            File::makeDirectory($rutaFolderBucketActividad, $mode = 0777, true, true);
        }
        if (!File::exists($rutaFolderBucketTecnico)) {
            File::makeDirectory($rutaFolderBucketTecnico, $mode = 0777, true, true);
        }
        if (File::exists($rutaFileActividad) || File::exists($rutaFileTecnico)) {
            if (unlink($rutaFileActividad) || unlink($rutaFileTecnico)) {
                File::put($rutaFileActividad, json_encode(Input::get('actividades')));
                File::put($rutaFileTecnico, json_encode(Input::get('tecnicos')));
                DB::table('historico_mapa_tecnico')
                ->insert(
                    array(
                        'nombre' => $nombreJson,
                        'entorno' => $entorno,
                        'bucket' => $bucket,
                        'json_actividad' => $dateFormat.".json",
                        'json_tecnico' => $dateFormat.".json",
                        'created_at' => date("Y-m-d H:i:s")
                    )
                );
            }
        } else {
            File::put($rutaFileActividad, json_encode(Input::get('actividades')));
            File::put($rutaFileTecnico, json_encode(Input::get('tecnicos')));

            DB::table('historico_mapa_tecnico')
            ->insert(
                array(
                    'nombre' => $nombreJson,
                    'entorno' => $entorno,
                    'bucket' => $bucket,
                    'json_actividad' => $dateFormat.".json",
                    'json_tecnico' => $dateFormat.".json",
                    'created_at' => date("Y-m-d H:i:s")
                )
            );
        }
    }
    /**
     * listar los mapas guardados en el mapa de tecnicos
     */
    public function getListar()
    {
        $entorno = Input::get('entorno');
        $bucket = Input::get('resource_id');

        $query = "SELECT  id, nombre, created_at fecha,
                  json_actividad, json_tecnico
                  FROM historico_mapa_tecnico
                  WHERE entorno=? AND bucket=?";

        $datos = DB::select($query, [$entorno,$bucket]);

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }
    /**
     * descargar archivo kml de las actividades en oracle
     */
    public function postDescargarkml()
    {
        $resourceId=Input::get('resource_id');
        $pos = strpos(URL::previous(), 'train');
        if ( $pos === false ) {
            $hoy = date("Ymd");
            $file = public_path()."/json/actividades/".$resourceId."/".$hoy.".json";
            $actividad = '[]';
            if (File::exists($file)) {
                $actividad = File::get($file);
                $actividad = json_decode($actividad, true);
            }
        } else  {
            $to = $from = date("Y-m-d");
            \Config::set("ofsc.auth.company", 'telefonica-pe.train');
            $activity = new Activity();
            $resultado = $activity->getActivities($resourceId,$from, $to);
            $actividad = $resultado->data;
        }
        
        // Creates the Document.
        $kml = new DOMDocument('1.0', 'UTF-8');

        // Creates the root KML element and appends it to the root document.
        $node = $kml->createElementNS('http://earth.google.com/kml/2.1', 'kml');
        $parNode = $kml->appendChild($node);

        // Creates a KML Document element and append it to the KML element.
        $dnode = $kml->createElement('Document');
        $document = $parNode->appendChild($dnode);

        // Estilo del icono color negro del bucket
        $restStyleNode = $kml->createElement('Style');
        $restStyleNode->setAttribute('id', 'bucket');
        $restIconstyleNode = $kml->createElement('IconStyle');
        $restColor = $kml->createElement('color', 'ff000000');
        $restIconstyleNode->appendChild($restColor);
        $restStyleNode->appendChild($restIconstyleNode);

        $document->appendChild($restStyleNode);

        $resourceUno = '';
        foreach ($actividad as $key => $coordinate) {

            $resourceDos = $coordinate["resource_id"];
            if ( $resourceUno != $resourceDos) {
                $colorkml = HistoricoMapaTecnicoController::colorkml();
                $restStyleNode = $kml->createElement('Style');
                $restStyleNode->setAttribute('id', $resourceDos);
                $restIconstyleNode = $kml->createElement('IconStyle');
                $restColor = $kml->createElement('color', $colorkml);
                $restIconstyleNode->appendChild($restColor);
                $restStyleNode->appendChild($restIconstyleNode);
                $document->appendChild($restStyleNode);

                $resourceUno = $resourceDos;
            }
        }
        foreach ($actividad as $key => $coordinate) {
            $resource = $coordinate["resource_id"];
            // Creates a Placemark and append it to the Document.
            $node = $kml->createElement('Placemark');
            $placeNode = $document->appendChild($node);
            // Creates an id attribute and assign it the value of id column.
            $placeNode->setAttribute('id', 'placemark');
            // Create name, and description elements and assigns 
            //them the values of the name and address columns from the results.
            $nameNode = $kml->createElement('name', 'Direccion');
            $placeNode->appendChild($nameNode);
            $descNode = $kml->createElement('description', $coordinate['address']);
            $placeNode->appendChild($descNode);
            $styleUrl = $kml->createElement('styleUrl', $resource);
            if ($coordinate["resource_id"] == 'LARI_INS_MOVISTAR_UNO') {
                $styleUrl = $kml->createElement('styleUrl', '#bucket');
            }
            $placeNode->appendChild($styleUrl);
            // Creates a Point element.
            $pointNode = $kml->createElement('Point');
            $placeNode->appendChild($pointNode);

            // Creates a coordinates element and gives it the value of 
            //the lng and lat columns from the results.
            $coorStr = $coordinate["coordx"] . ','  . $coordinate["coordy"];
            $coorNode = $kml->createElement('coordinates', $coorStr);
            $pointNode->appendChild($coorNode);
        }
        $kmlOutput = $kml->saveXML();
        header('Content-type: application/vnd.google-earth.kml+xml');
        header("Content-disposition: inline; filename=test.kml");
        echo $kmlOutput;
    }

    public static function colorkml()
    {
        $number = rand(1118481, 15658734);
        $color = dechex($number); //color hexadecimal aleatorio
        //colores KML constan de 4 pares de digitos
        $par1 = 'ff';
        $par2 = substr($color, 0, -4);
        $par3 = substr($color, 2, -2);
        $par4 = substr($color, 4);

        $colorKML = $par1.$par4.$par3.$par2;
        return $colorKML;
    }
}