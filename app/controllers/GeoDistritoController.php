<?php
class GeoDistritoController extends \BaseController
{
    public function postListar()
    {
      
        $r =GeoDistrito::postGeoDistritoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postDepartamento()
    {
        $array['departamento']=Input::get('departamento');
        $r =GeoDistrito::postProvinciaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postProvincia()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $r =GeoDistrito::postDistritoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


}

