<?php

class CasuisticaController extends \BaseController
{
    public function postCargar()
    {
        if (Request::ajax()) {
            $datos = DB::table('casuisticas')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postListar()
    {
        if (Request::ajax()) {
            $datos = DB::table('casuisticas as c')
                ->join(
                    'proyecto_tipo as pt', 'pt.id', '=', 'c.proyecto_tipo_id'
                )
                ->select(
                    'c.id', 'c.nombre as casuistica', 'pt.nombre as tipo', 
                    'c.estado'
                )
                ->orderBy('c.nombre')
                ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCrear()
    {
        if (Request::ajax()) {
            DB::table('casuisticas')
            ->insert(
                array('nombre' => Input::get('nombre'),
                'proyecto_tipo_id' => Input::get('proyecto_tipo_id'),
                'estado' => Input::get('estado'),
                'created_at' => date("Y-m-d H:m:s"),
                'usuario_created_at' => Auth::id())
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente'
                )
            );
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            $datos = DB::table('casuisticas')
                    ->select('id', 'nombre', 'proyecto_tipo_id', 'estado')
                    ->where('id', '=', Input::get('id'))
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            DB::table('casuisticas')
            ->where('id', Input::get('id'))
            ->update(
                array('nombre' => Input::get('nombre'),
                    'proyecto_tipo_id' => Input::get('proyecto_tipo_id'),
                    'estado' => Input::get('estado'),
                    'updated_at' => date("Y-m-d H:m:s"),
                    'usuario_updated_at' => Auth::id())
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Actualizacion realizado correctamente'
                )
            );
        }
    }

    public function postEstado()
    {
        if (Request::ajax()) {
            DB::table('casuisticas')
            ->where('id', Input::get('id'))
            ->update(
                array('estado' => Input::get('estado'))
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Estado actualizado correctamente'
                )
            );
        }
    }
}