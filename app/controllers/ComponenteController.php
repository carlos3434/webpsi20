<?php

class ComponenteController extends \BaseController
{
    public function __construct()
    {

    }

    /**
     * Listar registro de geo_elementos
     * POST componente/listar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $componentes = GeoTabla::postElementoPunto();

            $s_ditancia = '<select class="form-control" name="distancia[]" 
                                disabled>
                                <option value="100">0.10 km.</option>
                                <option value="500">0.50 km.</option>
                                <option value="1000">1.00 km.</option>
                                <option value="5000">5.00 km.</option>
                                <option value="10000">10.00 km.</option>
                            </select>';

            $s_cantidad = '<select class="form-control" name="cantidad[]" 
                                disabled>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                            </select>';

            $tbody = ''; $con = 1;
            foreach ($componentes as $c) {
                $tbody .= '<tr>';
                $tbody .= '<td><input type="checkbox" name="componente[]" '
                        .'id="checkcomponente" value="'.$c->etiqueta.'"></td>';
                $tbody .= '<td>'.$c->nombre.'</td>';
                $tbody .= '<td>'.$s_ditancia.'</td>';
                $tbody .= '<td>'.$s_cantidad.'</td>';
                $tbody .= '</tr>';
                $con++;
            }

            return Response::json(array('rst' => 1, 'datos' => $tbody));
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            if(Input::has('componente')) {

                $componentes = Input::get('componente');
                $distancia = Input::get('distancia');
                $cantidad = Input::get('cantidad');

                $ubicacion = Input::get('ubicacion');
                if (empty($ubicacion)) {
                    return Response::json(
                        array(
                            'rst'=>0,
                            'datos'=>'Casilla de X,Y se encuentra vacía.'
                        )
                    );
                }
                $arr_ubicacion = explode(',', $ubicacion);

                if (count($arr_ubicacion) < 2) {
                    return Response::json(
                        array(
                            'rst'=>0,
                            'datos'=>'Formato incorrecto en la casilla de X,Y.'
                        )
                    );
                }
                $x = $arr_ubicacion[0];
                $y = $arr_ubicacion[1];

                $tbody = ''; $contendido = ''; $datos = '';

                for ($i=0, $lenth = count($componentes); $i < $lenth; $i++) { 
                    $tabla = 'geo_'.$componentes[$i];
                    try {
                        if(empty($cantidad[$i]) && empty($distancia[$i])) {
                            $cant = '100';
                            $dist = '10';
                        } else {
                            $cant = $cantidad[$i];
                            $dist = $distancia[$i];
                        }

                        $resultado = GeoComponente::getFFTT(
                            $x, $y, $cant, $tabla, $dist
                            );
                    } catch (Exception $e) {
                         return Response::json(
                            array(
                                'rst'=>0,
                                'datos'=>'Error en obtener la ubicación. Verifique la casilla X,Y'
                            )
                        );
                    }
                    
                    $datos[$componentes[$i]] = $resultado;
                    $con = 1;
                    if (count($resultado)>0) {

                        $html_tabla = '<table class="table table-condensed">
                        <tbody>
                            <tr><td colspan="3" style="text-align: center; '
                            .'color: #3c8dbc;"><b>'.ucwords($componentes[$i])
                            .'</b></td></tr>
                            <tr>
                                <td style="width: 40px"><b>#</b></td>
                                <td style="width: 10px"><b>Descripcion</b></td>
                                <td style="width: 40px"><b>Distancia</b></td>
                        </tr>';

                        foreach ($resultado as $r) {
                            if ($componentes[$i]!="provincia") {
                                $tbody .= '<tr>';
                                $tbody .= '<td>'.$con.'</td>';
                            }
                            
                            switch ($componentes[$i]) {
                                case 'amplificador':
                                    $tbody.="<td>".$r->zonal.' - '.
                                    $r->nodo.' - '.$r->troba.' - '.
                                    $r->amplificador."</td>";
                                    break;
                                case 'antenacircle':
                                    $tbody.="<td>".$r->departamento.' - '.
                                    $r->provincia.' - '.$r->distrito.' - '.
                                    $r->ubigeo."</td>";
                                    break;
                                case 'armariopoligono':
                                case 'armariopunto':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->mdf.' - '.$r->armario."</td>";
                                    break;
                                case 'mdf':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->mdf.' <br>'.$r->direccion."</td>";
                                    break;
                                case 'mdfpunto':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->mdf.' <br>'.$r->direccion."</td>";
                                    break;
                                case 'nodopoligono':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->nodo."</td>";
                                    break;
                                case 'tap':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->nodo.' - '.$r->troba.' - '.
                                    $r->amplificador .' - '.$r->tap .'<br>'.
                                    $r->direccion."</td>";
                                    break;
                                case 'terminalf':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->mdf.' - '.$r->armario.' - '.
                                    $r->terminalf .'<br>'.$r->direccion."</td>";
                                    break;
                                case 'terminald':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->mdf.' - '.$r->cable.' - '.
                                    $r->terminald .'<br>'.$r->direccion."</td>";
                                    break;
                                case 'provincia':
                                    $tbody.= "<td></td>";
                                    break;
                                case 'troba':
                                    $tbody.= "<td>".$r->zonal.' - '.
                                    $r->nodo.' - '.$r->troba."</td>";
                                    break;
                                default: $tbody.= "<td>No disponible</td>";
                                    break;
                            }
                            
                            //$tbody .= '<td>'.$descripcion.'</td>';
                            if ($componentes[$i]!="provincia") {
                                $tbody .= '<td>'.$r->distance.' m</td>';
                                $tbody .= '</tr>';
                            } else {
                                $titulo = '';$html_tabla = '';
                                $contendido = $componentes[$i].
                                ' : Ningun registro encontrado con los '.
                                'criterios de busqueda ingresados.<br>';
                            }
                            
                            $con++;
                        }
                        $titulo = '<b>'.ucwords($componentes[$i]).'</b>';
                        $html_tabla .= $tbody.'</tbody></table><br>';
                        $tbody = '';
                        $contendido .=  $html_tabla;
                    } else {
                        $contendido .= '<p><b>'.ucwords($componentes[$i]).
                        '</b> : Ningun registro encontrado con los criterios'.
                        ' de busqueda ingresados.<br></p>';
                    }


                }            

                return Response::json(
                    array(
                        'rst'=>1,
                        'tabla'=>$contendido,
                        'datos'=>$datos
                    )
                );
            } else {
                return Response::json(
                    array(
                        'rst'=>0,
                        'datos'=>'Selecciona un Componente'
                    )
                );
            }
        }
    }

    public function postBuscarcalle()
    {
        try {
            //Numeracion izquierda o derecha
            $izq = true;
            if ( Input::get("numero")%2 === 0 ) {
                $izq = false;
            }
            
            $tramo = new Tramo();
            
            $data = $tramo->getTramos(
                Input::get("distrito"), 
                Input::get("calle"), 
                Input::get("numero"), 
                $izq
            );
            
            return json_encode(
                array(
                    'rst' => 1, 
                    'datos' => $data
                )
            );
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
            
    }

}
