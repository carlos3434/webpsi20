<?php

class TipoGeoZonaController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        $result=TipoGeozona::where('estado', 1)->get();

        return Response::json(array('rst' => 1, 'datos' => $result));
    }
}
