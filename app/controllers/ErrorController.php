<?php

class ErrorController extends \BaseController
{

    /**
     * Valida sesion activa
     */

    public function __construct()
    {
        $this->beforeFilter('auth');
    }

    /**
     * 
     * @param array|Exception $exc
     */
    public function saveError($exc)
    {
        if (is_array($exc) && isset($exc["errorInfo"]) && count($exc)==3) {
            list($code,$line,$message) = array_shift($exc);
            $error["code"] = $code;
            $error["file"] = 'mysql';
            $error["line"] = $line;
            $error["message"] = $message;
            $error["trace"] = '';
        } else if (is_array($exc)) { 
            $error["code"] = isset($exc["code"]) ? $exc["code"] : '';
            $error["file"] = isset($exc["file"]) ? $exc["file"] : '';
            $error["line"] = isset($exc["line"]) ? $exc["line"] : '';
            $error["message"] = isset($exc["message"]) ? $exc["message"] : '';
            $error["trace"] = isset($exc["trace"]) ? $exc["trace"] : '';
        } elseif(is_object($exc) ){
            $error["code"] = $exc->getCode();
            $error["file"] = $exc->getFile();
            $error["line"] = $exc->getLine();
            $error["message"] = $exc->getMessage();
            $error["trace"] = $exc->getTraceAsString();
        } else {
            $error["code"] = 'code';
            $error["file"] = 'file';
            $error["line"] = 'line';
            $error["message"] = 'message';
            $error["trace"] = 'trace';
        }
        $error["usuario_id"] = Auth::id() ? Auth::id() : '697';
        $error["date"] = date("Y-m-d H:i:s");
        $error["url"] = Request::url();
        DB::table('errores')->insert(
            array($error)
        );
    }

    public function saveCustomError($custom)
    {
        DB::table('errores')->insert(
            array($custom)
        );
    }

    /**
     * 
     * @param Exception $error
     * @param string $code
     */
    public function handlerError($error, $code = '')
    {
        if (empty($code))
            $error["code"] = $error->getCode();
        else
            $error["code"] = $code;

        $error["file"] = $error->getFile();
        $error["line"] = $error->getLine();
        $error["message"] = $error->getMessage();
        $error["trace"] = $error->getTraceAsString();
        $error["usuario_id"] = Auth::id();
        $error["date"] = date("Y-m-d H:i:s");

        DB::table('errores')->insert(
            array($error)
        );
    }

    /**
     * 
     * @return integer
     */
    public function postCambiarestado()
    {
        $estado = Input::get('estado');
        $id = Input::get('id');
        $comentario = trim(Input::get('comentario'));
        if (Request::ajax() && $estado != '' && $id != '') {
            return DB::table('errores')
                            ->where('id', $id)
                            ->update(
                                array(
                                    'estado' => $estado
                                    , 'comentario' => $comentario
                                )
                            );
        }
    }

    public function postDetalle()
    {
        if (Request::ajax()) {
            $detalle = DB::table('errores')
                    ->select('message')
                    ->where('id', '=', Input::get('id'))
                    ->get();

            return Response::json(array('data' => $detalle));
        }
    }
    /**
     * mostrar listado de empresas a las que he sido afiliado,
     * el campo representante sera la persona que tiene representante_legal=1
     * el cargo se muestra de la tabla empresa_persona
     */
    public function getIndex(){
        $query = DB::table('errores as e');
        

        if (Input::has('sort')) {
            list($sortCol, $sortDir) = explode('|', Input::get('sort'));
            $query->orderBy($sortCol, $sortDir);
        } else {
            $query->orderBy('e.id', 'asc');
        }

        if (Input::has('filter')) {
            $filter=Input::get('filter');
            $query->where(function($q) use($filter) {
                $value = "%{$filter}%";
                $q->where('message', 'like', $value)
                    ->orWhere('url', 'like', $value)
                    ->orWhere('file', 'like', $value)
                    ->orWhere('trace', 'like', $value);
            });
        }

        $perPage = Input::has('per_page') ? (int) Input::get('per_page') : null;

        return Response::json($query->paginate($perPage));
    }
    public function getShow($ruc){
        return "s".$ruc;
    }
    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function postListar()
    {
        return Errores::searchPaginateAndOrder();
    }
    /**
     * Process datatables ajax request.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListar()
    {
        return Errores::searchPaginateAndOrder();

        $fechaIni = "";
        $fechaFin = "";
        
        set_time_limit(0);
        $posts = DB::table("errores")->select(
            array(
                'errores.id'
                , 'usuario AS nombre'
                , 'errores.code'
                , DB::raw('SUBSTRING_INDEX(errores.file, "/", -2) AS file')
                , 'errores.date'
                , 'errores.comentario'
                , 'errores.message'
                , 'errores.trace'
                ,'errores.estado'
                ,'errores.url'
            )
        )
            ->join('usuarios', 'usuarios.id', '=', 'errores.usuario_id');

        $rfechas = Input::has('txt_rangofecha') ?
                Input::get('txt_rangofecha') :
                '1';
        
        
        
        return Datatables::of($posts)
                        ->addColumn(
                            'actionstatus', function ($message) {
                            $color = 'btn-danger';
                            $texto = 'Diligencia';
                            if ($message->estado == 0) {
                                $color = 'btn-success';
                                $texto = 'Reparado';
                            }
                            return '<a data-toggle="modal" '
                                    . 'data-target="#errorComentarioModal" '
                                    . 'href="#diligencia-' . $message->id . '" '
                                    . 'class="btn btn-diligencia '.$color.'">'
                                    . $texto . '</a>';
                            }
                        )
                        ->addColumn(
                            'action', function ($message) {
                            return '<a data-toggle="modal" '
                                    . 'data-target="#myModal" '
                                    . 'href="#detalle-' . $message->id . '" '
                                    . 'class="btn-detalle btn btn-primary">'
                                    . '<i class="fa fa-eye"></i></a>';
                            }
                        )
                        ->filter(
                            function ($query) use ($rfechas) {
                                if ($rfechas != 1) {
                                    $fechas = explode(
                                        ' - ', 
                                        Input::get('txt_rangofecha')
                                    );
                                    $fechaIni = $fechas[0];
                                    $fechaFin = $fechas[1];
                                    //$query->whereBetween('date', $fechas);
                                    if ($fechaIni == $fechaFin) {
                                        $query->whereDate(
                                            'date', '=', $fechaIni
                                        );
                                    } else {
                                        $query->whereDate(
                                            'date', '>=', $fechaIni
                                        )
                                        ->whereDate(
                                            'date', '<=', $fechaFin
                                        );
                                    }
                                } else {
                                    $query->whereDate(
                                        'date', '=', date("Y-m-d")
                                    );
                                }
                            }
                        )
                        ->make(true);
    }

    public function postList()
    {
        $startRegistro = Input::get('startRegistro');
        $perPage = Input::get('perPage');
        $filtro = Input::get('filtro');
        $filterSelected = Input::get('filterSelected');

        $queryDatos = DB::table('errores as e')->select('e.*', 'u.usuario')->leftJoin('usuarios as u', 'u.id', '=', 'e.usuario_id');
        $queryTotal = DB::table('errores as e');
        if ($filtro) {
            $datos = $queryDatos->whereRaw(''.$filterSelected.' like "%'.$filtro.'%"')
                                ->skip($startRegistro)->take($perPage)
                                ->orderBy('e.id', 'desc')->get();
            $count = $queryTotal->leftJoin('usuarios as u', 'u.id', '=', 'e.usuario_id')
                                ->whereRaw(''.$filterSelected.' like "%'.$filtro.'%"')
                                ->count();
        } else {
            $datos = $queryDatos->skip($startRegistro)->take($perPage)
                                ->orderBy('e.id', 'desc')->get();
            $count = $queryTotal->count();
        }

        $trama["total"] = $count;
        $trama["datos"] = $datos;
        return Response::json([
            'rst' => 1,
            'trama' => $trama
        ]);
    }
}
