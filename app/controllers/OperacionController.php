<?php

class OperacionController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        if(Input::get('parametros')){
             $result=DB::table('tipo_operacion')
                                ->select(
                                    DB::RAW('codigo as id'),
                                    DB::RAW('concat( IF(tipo_legado=1,"CMS: ",
                                        IF(tipo_legado=2,"Gestel: ","")) ,
                                        nombre) as nombre')
                                    )
                                ->where ('codigo','<>','')
                                ->where ('tipo',null)
                                ->get();
        } else {
            $result=Operacion::postListar();
        }

        return Response::json(array('rst' => 1, 'datos' => $result));
    }

}
