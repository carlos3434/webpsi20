<?php
ini_set('memory_limit', '1024M');
class GestionController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
        $this->_errorController = $errorController;
    }

    public function postCargar()
    {
        if ( Request::ajax() ) {
            return Response::json(Gestion::getCargar());
        }
    }
    /**
     * Buscar codigo en tablas temporales y las gestionadas
     */
    public function postValidacodigo()
    {
        if (Input::has('codactu')) {
            $codactu= Input::get('codactu');
            try {
               $row = Gestion::getRowgestiones($codactu);
            } catch (Exception $e) {
                $this->_errorController->saveError($e);
                $msj='Ocurrió una interrupción en el registro del movimiento';
                return  array(
                    'rst'=>0,
                    'datos'=>$msj
                );
            }
            //si existe
            return Response::json(
                array('rst'=>$row) //return 1 o 0
            );
        }
        //vacio
        return Response::json(
            array('rst'=>0)
        );
    }

    public function postActualizarfftt()
    {
        if ( Request::ajax() ) {
          
            UltimoMovimiento::actualizar_movimiento();

            $result['rst'] = 1;

            return Response::json($result);
        }

    }
    
    /**
     * Actualiza coordenadas de una orden
     * @return type
     */
    public function postActualizaxy()
    {
        if ( Request::ajax() ) {
            $actu = Input::get('actu');
            $lat = Input::get('lat');
            $lng = Input::get('lng');
            
            $result = array();
            $result['rst'] = 2;
            $result['msj'] = 'Error al actualizar coordenadas: No existe Código de Actuación';

            try {
                //Iniciar transaccion
                DB::beginTransaction();
                
                $gestionDetalle = GestionDetalle::Where(
                    'codactu',
                    $actu
                )->first();

                if (count($gestionDetalle)>0 && $gestionDetalle->gestion_id!= null ) {
                    //Guardar direccion previa
                    UltimoMovimiento::actualizar_direccion(
                        $lng, $lat, '', $gestionDetalle->gestion_id
                    );
                                    
                    //Gestionada: gestiones_detalles
                    DB::table('gestiones_detalles')
                        ->where('codactu', $actu)
                        ->update(
                            array('x' => $lng, 'y' => $lat)
                        );
                    
                    //Ultimos movimientos
                    DB::table('ultimos_movimientos')
                        ->where('codactu', $actu)
                        ->update(
                            array('x' => $lng, 'y' => $lat)
                        );
                    
                    //Temporales
                    DB::table(Config::get("wpsi.db.tmp_averia"))
                        ->where('averia', $actu)
                        ->update(
                            array('xcoord' => $lng, 'ycoord' => $lat)
                        );
                    DB::table(Config::get("wpsi.db.tmp_provision"))
                        ->where('codigo_req', $actu)
                        ->update(
                            array('xcoord' => $lng, 'ycoord' => $lat)
                        );
                
                    //INSERT en cambios_direcciones
                
                    $sql = "INSERT INTO cambios_direcciones
                           (gestion_id, tipo_usuario, usuario_id,
                           coord_x, coord_y, direccion, referencia)
                           VALUES (?, ?, ?, ?, ?, ?, ?)";
                    $data = array(
                        $gestionDetalle->gestion_id, 'sys', Auth::id(),
                        $lng, $lat, '', ''
                    );
                    //DB::insert($sql, $data);
                    DB::commit();

                    //Update OK
                    $result['rst'] = 1;
                    $result['msj'] = 'Coordenadas actualizadas correctamente';
                } 
            } catch (PDOException $exc) {
                DB::rollback();
                
                $this->_errorController->saveError($exc);
                $result['rst'] = 2;
                $result['msj'] = 'Error al actualizar coordenadas';
            }
            
            return json_encode($result);
        }
    }

    public function postCargarcierre()
    {
        $idMovimiento = Input::get("buscar");

        $gestion = GestionMovimiento::find($idMovimiento);
        if (isset($gestion->motivo_id)) {
            $motivo = Motivo::find($gestion->motivo_id);
            $gestion->motivo = $motivo;
            if ($gestion->estado_id == 9) {
                echo json_encode(array("rst"=>1,"data"=>$gestion));
            } else {
                echo json_encode(array("rst"=>0,"data"=>"El movimiento no pertenece a un cierre"));
            }
        } else {
            echo json_encode(array("rst"=>0,"data"=>"El movimiento no pertenece a un cierre"));
        }
    }

    public function postValidarcierre()
    {
        $codigo = Input::get("codigo");
        $validado = Input::get("validado");
        $gestion = GestionMovimiento::find($codigo);
        $gestion->validado = $validado;
        $gestion->save();

        echo json_encode(array("rst" => 1, "msj" => "Se valido correctamente"));
    }

    /**
     * Buscar codigo ficticios en tablas gestionadas
     */
    public function getValidaficticio()
    {
        if (Input::has('tipo')) {
            $tipo= Input::get('tipo');
            try {
                $row = Gestion::getRowficticios($tipo);
                if ($row == 0) {
                    return Response::json(array('rst'=>0));
                }
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $msj='Ocurrió una interrupción en la consulta';
                return  array(
                    'rst'=>0,
                    'datos'=>$msj
                );
            }
            return Response::json(
                array('rst'=>1, 'datos'=>$row[0])
            );
        }
        return Response::json(
            array('rst'=>0)
        );
    }
}
