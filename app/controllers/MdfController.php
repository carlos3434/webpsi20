<?php
class MdfController extends \BaseController
{
    /**
     * Recepciona datos de Bandeja Controller
     * 
     * @return type
     */
    public function postListar()
    {

             if ( Input::get('bandeja')!=null OR Input::has('estado') ) {
                 $m =Mdf::getMdfAll();
             } else {
                 $zonal = Input::get('zonal', 'LIM');
                 $m=array();
                 if ( Input::get('tipo')=='rutina-catv-pais' ) {
                     $m =Mdf::getMdfCatv($zonal);
                 } else {
                     $m =Mdf::getMdfs($zonal);
                 }
             }

     
             if ( Request::ajax() ) {
                 return Response::json(
                     array(
                         'rst' => 1, 
                         'datos' => $m
                     )
                 );
             } 
        
    }



}

