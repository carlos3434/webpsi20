<?php
use Ofsc\Inbound;

class GestionMovimientoController extends BaseController
{

   protected $_errorController;
    /**
     * Valida sesion activa
     */
    public function __construct(ErrorController $ErrorController)
    {
        $this->beforeFilter('auth');
        $this->_errorController = $ErrorController;
    }

    public function postCargar()
    {
        if ( Request::ajax() ) {
            $cargar = GestionMovimiento::getGestionMovimiento();
            $html = "";
            if (Input::has("detalle")) {
                $html = Helpers::htmlGestionesMovimientos($cargar);
            }
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$cargar,
                    'html' => $html
                )
            );

        }
    }

    public function postCrearobs()
    {
        if ( Request::ajax() ) {
            //var_dump(Input::all());
            $codactu = Input::get('codactu');

            $inbound = new Inbound();

            $movimiento = new MovimientoObservacion;
            $datos = $movimiento->getObservaciones();

            $data["XA_NOTE"] = Input::get('observacion_o', '');

            if ($datos[0]->observacion != '') {
                $data["XA_NOTE"] = $datos[0]->observacion.', '.Input::get('observacion_o', '');
            }

            if ($datos[0]->aid != '') {
                $response = $inbound->updateActivity($codactu, $data, array());
                if (isset($response->result)) {
                    if ($response->result == "success") {
                        $movimientosObservaciones = new MovimientoObservacion;
                        $movimientosObservaciones['gestion_movimiento_id']=
                                        Input::get('gestion_movimiento_id');
                        $movimientosObservaciones['observacion_tipo_id']=
                                        Input::get('obs_tipo');
                        $movimientosObservaciones['observacion']=Input::get('observacion_o', '');

                        $movimientosObservaciones['usuario_created_at']=Auth::user()->id;
                        $movimientosObservaciones->save();

                        return Response::json(
                            array(
                                'rst'=>1,
                                'msj'=>'Registro realizado correctamente',
                                'codactu'=>Input::get('codactu')
                            )
                        );
                    } else {
                        return Response::json(
                            array(
                                'rst'=>2,
                                'msj'=>'No se pudo registrar observacion en TOA',
                                'codactu'=>Input::get('codactu')
                            )
                        );
                    }
                }
            } else {
                $movimientosObservaciones = new MovimientoObservacion;
                $movimientosObservaciones['gestion_movimiento_id']=
                                Input::get('gestion_movimiento_id');
                $movimientosObservaciones['observacion_tipo_id']=
                                Input::get('obs_tipo');
                $movimientosObservaciones['observacion']=Input::get('observacion_o', '');

                $movimientosObservaciones['usuario_created_at']=Auth::user()->id;
                $movimientosObservaciones->save();

                return Response::json(
                    array(
                        'rst'=>1,
                        'msj'=>'Registro realizado correctamente',
                        'codactu'=>Input::get('codactu')
                    )
                );
            }

            return Response::json(
                array(
                    'rst'=>2,
                    'msj'=>'No se pudo registrar observacion',
                    'codactu'=>$codactu
                )
            );
        }
    }

    public function postCrear()
    {
        if ( Request::ajax() or Input::get('noajax') ) {
            $gestionId="";
            $cupos = true;
            if ( Input::get('gestion_id') ) {
                $gestionId= Input::get('gestion_id');
                    $ultmov= DB::table('ultimos_movimientos')
                            ->where('gestion_id', '=', $gestionId)
                            ->first();
                    $ultimoMovimiento= UltimoMovimiento::find($ultmov->id);
                    
                if( Input::get('officetrack_envio')=='1' ){
                    $gestiones= Gestion::find(Input::get('gestion_id'));
                    $gestiones["n_evento"]=1;
                    $ultimoMovimiento["n_evento"]=1;
                    $gestiones->save();
                }

                try {
                    $gestionesMovimientos = new GestionMovimiento;
                    $gestionesMovimientos["quiebre_id"]=
                                Input::get('quiebre_id');
                    $gestionesMovimientos["gestion_id"]=
                                Input::get('gestion_id');
                    $gestionesMovimientos["empresa_id"]=
                                Input::get('empresa_id');
                    $gestionesMovimientos["zonal_id"]=
                                Input::get('zonal_id');
                    $gestionesMovimientos["estado_id"]=
                                Input::get('estado');
                    $gestionesMovimientos["motivo_id"]=
                                Input::get('motivo');
                    $gestionesMovimientos["submotivo_id"]=
                                Input::get('submotivo');
                    $gestionesMovimientos["observacion"]=
                                Input::get('observacion2');
                    $gestionesMovimientos["coordinado"]=
                                Input::get('coordinado2');


                    $ultimoMovimiento["quiebre_id"]=
                                Input::get('quiebre_id');
                    $ultimoMovimiento["empresa_m_id"]=
                                Input::get('empresa_id');
                    $ultimoMovimiento["zonal_id"]=
                                Input::get('zonal_id');
                    $ultimoMovimiento["estado_id"]=
                                Input::get('estado');
                    $ultimoMovimiento["motivo_id"]=
                                Input::get('motivo');
                    $ultimoMovimiento["submotivo_id"]=
                                Input::get('submotivo');
                    $ultimoMovimiento["observacion_m"]=
                                Input::get('observacion2');
                    $ultimoMovimiento["coordinado"]=
                                Input::get('coordinado2');

                    if ( Input::get('flag_tecnico') ) {
                    $ultimoMovimiento["flag_tecnico"]=
                                Input::get('flag_tecnico');
                    $gestionesMovimientos["flag_tecnico"]=
                                Input::get('flag_tecnico');
                    }            

                    if ( Input::get('horario_id') && Input::get('horario_id')!='' ) {
                    $gestionesMovimientos["horario_id"]=
                                Input::get('horario_id');
                    $gestionesMovimientos["dia_id"]=
                                Input::get('dia_id');
                    $gestionesMovimientos["fecha_agenda"]=
                                Input::get('fecha_agenda');

                    $ultimoMovimiento["horario_id"]=
                                Input::get('horario_id');
                    $ultimoMovimiento["dia_id"]=
                                Input::get('dia_id');
                    $ultimoMovimiento["fecha_agenda"]=
                                Input::get('fecha_agenda');
                    }

                    if ( Input::get('tecnico') && Input::get('tecnico')!='' ) {
                    $gestionesMovimientos["celula_id"]=
                                Input::get('celula');
                    $gestionesMovimientos["tecnico_id"]=
                                Input::get('tecnico');

                    $ultimoMovimiento["celula_id"]=
                                Input::get('celula');
                    $ultimoMovimiento["tecnico_id"]=
                                Input::get('tecnico');
                    }

                    if ( Input::get('fecha_consolidacion') && Input::get('fecha_consolidacion')!='' ) {
                    $gestionesMovimientos["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');

                    $ultimoMovimiento["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');
                    }
                    
                    //Origen del movimiento realizado
                    $gestionesMovimientos["submodulo_id"] = 3;
                    if ( Input::get('submodulo_id') !== null )
                    {
                        $gestionesMovimientos["submodulo_id"] = 
                                Input::get('submodulo_id');
                    }

                    if ( Input::get('usuario_sistema') !== null )
                    {
                        $gestionesMovimientos['usuario_created_at']=697;
                    }
                    $gestionesMovimientos['usuario_created_at']=Auth::user()->id;
                    $gestionesMovimientos->save();

                    if( substr(Input::get('estado_agendamiento'),-2)=='-1' ) {
                        $gestionesDetalles=GestionDetalle::where('gestion_id', '=', $gestionId)
                                           ->update(
                                                array(
                                                    'x' => Input::get('x'),
                                                    'y' => Input::get('y')
                                                )
                                            );

                        $ultimoMovimiento['x']=Input::get('x');
                        $ultimoMovimiento['y']=Input::get('y');
                    }

                    $ultimoMovimiento['usuario_updated_at']=Auth::user()->id;
                    if ( Input::get('usuario_sistema') !== null )
                    {
                        $ultimoMovimiento['usuario_updated_at']=697;
                    }
                    
                    $ultimoMovimiento->save();
                    
                    //Control de cupos
                    $gestionesMovimientos->estado_agendamiento 
                            = Input::get('estado_agendamiento');
                    $cupos = $this->controlarCupos($gestionesMovimientos);

                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                        return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro del movimiento',
                            'err'=> $exc
                        );
                }

                if ( Input::get('contacto') ) {
                    try {
                        $liquidados = new Liquidado();
                        $liquidados['gestion_id']=
                            Input::get('gestion_id');
                            
                        if(Input::get('feedback') && Input::get('feedback')!='')
                           $liquidados['feedback_liquidado_id']=
                            Input::get('feedback'); 
                        else $liquidados['feedback_liquidado_id']=10;

                        if(Input::get('solucion') && Input::get('solucion')!='')
                            $liquidados['solucion_comercial_id']=
                            Input::get('solucion');
                        else $liquidados['solucion_comercial_id']=17;

                        $liquidados['contacto']=
                            Input::get('contacto');
                        $liquidados['pruebas']=
                            Input::get('pruebas');
                        $liquidados['fecha_consolidacion']=
                            Input::get('fecha_consolidacion');
                        $liquidados['penalizable']=
                            Input::get('penalizable_obs');

                        $liquidados['usuario_created_at']=Auth::user()->id;
                        $liquidados->save();
                    } catch (Exception $exc) {
                        $this->_errorController->saveError($exc);
                        return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro de la liquidación',
                            'err'=> $exc
                        );
                    }
                }
                
            } else {
                $id="";
                try { 
                    $gestiones = new Gestion;
                    $ultimoMovimiento= new UltimoMovimiento;

                    if( Input::get('gestion_id_officetrack') ){
                        $id=Input::get('gestion_id_officetrack');
                        $gestiones['id']=$id;
                        $gestiones["n_evento"]=1;

                        $ultimoMovimiento["n_evento"]=1;
                    }

                    $gestiones["actividad_id"]=
                    Input::get('actividad_id');
                    $gestiones["nombre_cliente_critico"]=
                    Input::get('nombre_cliente_critico');
                    $gestiones["telefono_cliente_critico"]=
                    Input::get('telefono_cliente_critico');
                    $gestiones["celular_cliente_critico"]=
                    Input::get('celular_cliente_critico');

                    $gestiones['usuario_created_at']=Auth::user()->id;

                    $gestiones->save();

                    $ultimoMovimiento["actividad_id"]=
                    Input::get('actividad_id');
                    $ultimoMovimiento["nombre_cliente_critico"]=
                    Input::get('nombre_cliente_critico');
                    $ultimoMovimiento["telefono_cliente_critico"]=
                    Input::get('telefono_cliente_critico');
                    $ultimoMovimiento["celular_cliente_critico"]=
                    Input::get('celular_cliente_critico');

                    $ultimoMovimiento['usuario_created_at']=Auth::user()->id;

                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                    return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro de la gestion',
                            'err'=> $exc
                        );
                }

                try {
                    $gestionId= $gestiones->id;
                    $id=$gestiones->id;
                    $gestiones["id_atc"]="ATC_".date("Y")."_".$id;
                    $gestiones->save();

                    $ultimoMovimiento["id_atc"]="ATC_".date("Y")."_".$id;
                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                    return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro del ATC',
                            'err'=> $exc
                        );
                }

                try {

                    $gestionesDetalles = new GestionDetalle;
                    $gestionesDetalles["gestion_id"]=$id;
                    $gestionesDetalles["quiebre_id"]=
                            Input::get('quiebre_id');
                    $gestionesDetalles["empresa_id"]=
                            Input::get('empresa_id');
                    $gestionesDetalles["zonal_id"]=
                            Input::get('zonal_id');
                    $gestionesDetalles["codactu"]=
                            Input::get('codactu');
                    $gestionesDetalles["tipo_averia"]=
                            Input::get('tipo_averia');
                    $gestionesDetalles["horas_averia"]=
                            Input::get('horas_averia');
                    $gestionesDetalles["fecha_registro"]=
                            Input::get('fecha_registro');
                    $gestionesDetalles["ciudad"]=
                            Input::get('ciudad');
                    $gestionesDetalles["inscripcion"]=
                            Input::get('inscripcion');
                    $gestionesDetalles["fono1"]=
                            Input::get('fono1');
                    $gestionesDetalles["telefono"]=
                            Input::get('telefono');
                    $gestionesDetalles["mdf"]=
                            Input::get('mdf');
                    $gestionesDetalles["observacion"]=
                            Input::get('observacion');
                    $gestionesDetalles["segmento"]=
                            Input::get('segmento');
                    $gestionesDetalles["area"]=
                            Input::get('area');
                    $gestionesDetalles["direccion_instalacion"]=
                            Input::get('direccion_instalacion');
                    $gestionesDetalles["codigo_distrito"]=
                            Input::get('codigo_distrito');
                    $gestionesDetalles["nombre_cliente"]=
                            Input::get('nombre_cliente');
                    $gestionesDetalles["orden_trabajo"]=
                            Input::get('orden_trabajo');
                    $gestionesDetalles["veloc_adsl"]=
                            Input::get('veloc_adsl');
                    $gestionesDetalles["clase_servicio_catv"]=
                            Input::get('clase_servicio_catv');
                    $gestionesDetalles["codmotivo_req_catv"]=
                            Input::get('codmotivo_req_catv');
                    $gestionesDetalles["total_averias_cable"]=
                            Input::get('total_averias_cable');
                    $gestionesDetalles["total_averias_cobre"]=
                            Input::get('total_averias_cobre');
                    $gestionesDetalles["total_averias"]=
                            Input::get('total_averias');
                    $gestionesDetalles["fftt"]=
                            Input::get('fftt');
                    $gestionesDetalles["llave"]=
                            Input::get('llave');
                    $gestionesDetalles["dir_terminal"]=
                            Input::get('dir_terminal');
                    $gestionesDetalles["fonos_contacto"]=
                            Input::get('fonos_contacto');
                    $gestionesDetalles["contrata"]=
                            Input::get('contrata');
                    $gestionesDetalles["zonal"]=
                            Input::get('zonal');
                    $gestionesDetalles["wu_nagendas"]=
                            Input::get('wu_nagendas');
                    $gestionesDetalles["wu_nmovimientos"]=
                            Input::get('wu_nmovimientos');
                    $gestionesDetalles["wu_fecha_ult_agenda"]=
                            Input::get('wu_fecha_ult_agenda');
                    $gestionesDetalles["total_llamadas_tecnicas"]=
                            Input::get('total_llamadas_tecnicas');
                    $gestionesDetalles["total_llamadas_seguimiento"]=
                            Input::get('total_llamadas_seguimiento');
                    $gestionesDetalles["llamadastec15dias"]=
                            Input::get('llamadastec15dias');
                    $gestionesDetalles["llamadastec30dias"]=
                            Input::get('llamadastec30dias');
                    $gestionesDetalles["lejano"]=
                            Input::get('lejano');
                    $gestionesDetalles["distrito"]=
                            Input::get('distrito');
                    $gestionesDetalles["eecc_zona"]=
                            Input::get('eecc_zona');
                    $gestionesDetalles["zona_movistar_uno"]=
                            Input::get('zona_movistar_uno');
                    $gestionesDetalles["paquete"]=
                            Input::get('paquete');
                    $gestionesDetalles["data_multiproducto"]=
                            Input::get('data_multiproducto');
                    $gestionesDetalles["averia_m1"]=
                            Input::get('averia_m1');
                    $gestionesDetalles["fecha_data_fuente"]=
                            Input::get('fecha_data_fuente');
                    $gestionesDetalles["telefono_codclientecms"]=
                            Input::get('telefono_codclientecms');
                    $gestionesDetalles["rango_dias"]=
                            Input::get('rango_dias');
                    $gestionesDetalles["sms1"]=
                            Input::get('sms1');
                    $gestionesDetalles["sms2"]=
                            Input::get('sms2');
                    $gestionesDetalles["area2"]=
                            Input::get('area2');
                    $gestionesDetalles["microzona"]=
                            Input::get('microzona');
                    $gestionesDetalles["tipo_actuacion"]=
                            Input::get('tipo_actuacion');
                    $gestionesDetalles["actividad_tipo_id"]=
                            Input::get('actividad_tipo_id');

                    if (Input::get('fecha_registro_psi')) {
                        $gestionesDetalles["fecha_psi"]=
                                Input::get('fecha_registro_psi');
                    }

                    if( Input::get('x') and Input::get('y')/*substr(Input::get('estado_agendamiento'),-2)=='-1'*/ ) {
                        $gestionesDetalles["x"]=
                                Input::get('x');
                        $gestionesDetalles["y"]=
                                Input::get('y');

                        $ultimoMovimiento["x"]=
                                Input::get('x');
                        $ultimoMovimiento["y"]=
                                Input::get('y');
                    }

                    $gestionesDetalles['usuario_created_at']=Auth::user()->id;
                    $gestionesDetalles->save();

                    /**********************************************************/
                    $ffttExplode=explode("|", Input::get('fftt'));
                    $tipoAExplode=explode("-", Input::get('tipo_averia'));
                    if( count($tipoAExplode)==1 ){
                        $tipoAExplode=explode("_", Input::get('tipo_averia'));
                    }

                    $arrayproadsl=array(1,2,4);
                    $arrayprocatv=array(5,6,7,8,9);

                    $arrayaveradsl=array(1,2,3,14,15,16,17,9,18,19,20);
                    $arrayaverbas=array(1,2,3,10,11,12,4,9);
                    $arrayavercatv=array(5,13,6,7,8,9);


                    $sqlttff='INSERT INTO gestiones_fftt (gestion_id,fftt_tipo_id,nombre) VALUES (?,?,?)';
                    if ( in_array('aver', $tipoAExplode, true) AND count($ffttExplode)>1 ){
                        if ( in_array('adsl', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayaveradsl); $i++){
                                if (isset($ffttExplode[$i])) {
                                    $array=array($id,$arrayaveradsl[$i],trim($ffttExplode[$i]));

                                    if( trim($ffttExplode[$i])!='' ){
                                        DB::insert($sqlttff, $array);
                                    }
                                }
                            }
                        }
                        elseif ( in_array('bas', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayaverbas); $i++){
                                if (isset($ffttExplode[$i])) {
                                    $array=array($id,$arrayaverbas[$i],trim($ffttExplode[$i]));

                                    if( trim($ffttExplode[$i])!='' ){
                                        DB::insert($sqlttff, $array);
                                    }
                                }
                            }
                        }
                        elseif ( in_array('catv', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayavercatv); $i++){
                                if (isset($ffttExplode[$i])) {
                                    $array=array($id,$arrayavercatv[$i],trim($ffttExplode[$i]));

                                    if( trim($ffttExplode[$i])!='' ){
                                        DB::insert($sqlttff, $array);
                                    }
                                }
                            }
                        }
                    }
                    elseif ( in_array('prov', $tipoAExplode, true) AND count($ffttExplode)>1 ){
                        if ( in_array('adsl', $tipoAExplode, true) OR in_array('bas', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayproadsl); $i++){
                                if (isset($ffttExplode[$i])) {
                                    if( $i==1 AND strtoupper(substr(trim($ffttExplode[$i]),0,1))=='A' ){
                                        $array=array($id,$arrayproadsl[$i],trim($ffttExplode[$i]));
                                    }
                                    elseif($i==1 ){
                                        $array=array($id,3,trim($ffttExplode[$i]));
                                    }
                                    else{
                                        $array=array($id,$arrayproadsl[$i],trim($ffttExplode[$i]));
                                    }

                                    if( trim($ffttExplode[$i])!='' ){
                                        DB::insert($sqlttff, $array);
                                    }
                                }
                            }
                        }
                        elseif ( in_array('catv', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayprocatv); $i++){
                                if (isset($ffttExplode[$i])) {
                                    $array=array($id,$arrayprocatv[$i],trim($ffttExplode[$i]));

                                    if( trim($ffttExplode[$i])!='' ){
                                        DB::insert($sqlttff, $array);
                                    }
                                }
                            }
                        }
                    }
                    /**********************************************************/

                    $ultimoMovimiento["gestion_id"]=$id;
                    $ultimoMovimiento["quiebre_id"]=
                            Input::get('quiebre_id');
                    $ultimoMovimiento["empresa_id"]=
                            Input::get('empresa_id');
                    $ultimoMovimiento["zonal_id"]=
                            Input::get('zonal_id');
                    $ultimoMovimiento["codactu"]=
                            Input::get('codactu');
                    $ultimoMovimiento["tipo_averia"]=
                            Input::get('tipo_averia');
                    $ultimoMovimiento["horas_averia"]=
                            Input::get('horas_averia');
                    $ultimoMovimiento["fecha_registro"]=
                            Input::get('fecha_registro');
                    $ultimoMovimiento["ciudad"]=
                            Input::get('ciudad');
                    $ultimoMovimiento["inscripcion"]=
                            Input::get('inscripcion');
                    $ultimoMovimiento["fono1"]=
                            Input::get('fono1');
                    $ultimoMovimiento["telefono"]=
                            Input::get('telefono');
                    $ultimoMovimiento["mdf"]=
                            Input::get('mdf');
                    $ultimoMovimiento["observacion"]=
                            Input::get('observacion');
                    $ultimoMovimiento["segmento"]=
                            Input::get('segmento');
                    $ultimoMovimiento["area"]=
                            Input::get('area');
                    $ultimoMovimiento["direccion_instalacion"]=
                            Input::get('direccion_instalacion');
                    $ultimoMovimiento["codigo_distrito"]=
                            Input::get('codigo_distrito');
                    $ultimoMovimiento["nombre_cliente"]=
                            Input::get('nombre_cliente');
                    $ultimoMovimiento["orden_trabajo"]=
                            Input::get('orden_trabajo');
                    $ultimoMovimiento["veloc_adsl"]=
                            Input::get('veloc_adsl');
                    $ultimoMovimiento["clase_servicio_catv"]=
                            Input::get('clase_servicio_catv');
                    $ultimoMovimiento["codmotivo_req_catv"]=
                            Input::get('codmotivo_req_catv');
                    $ultimoMovimiento["total_averias_cable"]=
                            Input::get('total_averias_cable');
                    $ultimoMovimiento["total_averias_cobre"]=
                            Input::get('total_averias_cobre');
                    $ultimoMovimiento["total_averias"]=
                            Input::get('total_averias');
                    $ultimoMovimiento["fftt"]=
                            Input::get('fftt');
                    $ultimoMovimiento["llave"]=
                            Input::get('llave');
                    $ultimoMovimiento["dir_terminal"]=
                            Input::get('dir_terminal');
                    $ultimoMovimiento["fonos_contacto"]=
                            Input::get('fonos_contacto');
                    $ultimoMovimiento["contrata"]=
                            Input::get('contrata');
                    $ultimoMovimiento["zonal"]=
                            Input::get('zonal');
                    $ultimoMovimiento["wu_nagendas"]=
                            Input::get('wu_nagendas');
                    $ultimoMovimiento["wu_nmovimientos"]=
                            Input::get('wu_nmovimientos');
                    $ultimoMovimiento["wu_fecha_ult_agenda"]=
                            Input::get('wu_fecha_ult_agenda');
                    $ultimoMovimiento["total_llamadas_tecnicas"]=
                            Input::get('total_llamadas_tecnicas');
                    $ultimoMovimiento["total_llamadas_seguimiento"]=
                            Input::get('total_llamadas_seguimiento');
                    $ultimoMovimiento["llamadastec15dias"]=
                            Input::get('llamadastec15dias');
                    $ultimoMovimiento["llamadastec30dias"]=
                            Input::get('llamadastec30dias');
                    $ultimoMovimiento["lejano"]=
                            Input::get('lejano');
                    $ultimoMovimiento["distrito"]=
                            Input::get('distrito');
                    $ultimoMovimiento["eecc_zona"]=
                            Input::get('eecc_zona');
                    $ultimoMovimiento["zona_movistar_uno"]=
                            Input::get('zona_movistar_uno');
                    $ultimoMovimiento["paquete"]=
                            Input::get('paquete');
                    $ultimoMovimiento["data_multiproducto"]=
                            Input::get('data_multiproducto');
                    $ultimoMovimiento["averia_m1"]=
                            Input::get('averia_m1');
                    $ultimoMovimiento["fecha_data_fuente"]=
                            Input::get('fecha_data_fuente');
                    $ultimoMovimiento["telefono_codclientecms"]=
                            Input::get('telefono_codclientecms');
                    $ultimoMovimiento["rango_dias"]=
                            Input::get('rango_dias');
                    $ultimoMovimiento["sms1"]=
                            Input::get('sms1');
                    $ultimoMovimiento["sms2"]=
                            Input::get('sms2');
                    $ultimoMovimiento["area2"]=
                            Input::get('area2');
                    $ultimoMovimiento["microzona"]=
                            Input::get('microzona');
                    $ultimoMovimiento["tipo_actuacion"]=
                            Input::get('tipo_actuacion');
                    

                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                    return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro de la gestion detalle',
                            'err'=> $exc
                        );
                }

                try {
                    $gestionesMovimientos = new GestionMovimiento;
                    $gestionesMovimientos["gestion_id"]=$id;

                    $gestionesMovimientos["quiebre_id"]=
                                Input::get('quiebre_id');

                    $gestionesMovimientos["empresa_id"]=
                                Input::get('empresa_id');
                    $gestionesMovimientos["zonal_id"]=
                                Input::get('zonal_id');
                    $gestionesMovimientos["estado_id"]=
                                Input::get('estado');
                    $gestionesMovimientos["motivo_id"]=
                                Input::get('motivo');
                    $gestionesMovimientos["submotivo_id"]=
                                Input::get('submotivo');
                    $gestionesMovimientos["observacion"]=
                                Input::get('observacion2');
                    $gestionesMovimientos["coordinado"]=
                                Input::get('coordinado2');

                    $ultimoMovimiento["empresa_m_id"]=
                                Input::get('empresa_id');
                    $ultimoMovimiento["estado_id"]=
                                Input::get('estado');
                    $ultimoMovimiento["motivo_id"]=
                                Input::get('motivo');
                    $ultimoMovimiento["submotivo_id"]=
                                Input::get('submotivo');
                    $ultimoMovimiento["observacion_m"]=
                                Input::get('observacion2');
                    $ultimoMovimiento["coordinado"]=
                                Input::get('coordinado2');

                    if ( Input::get('flag_tecnico') ) {
                    $gestionesMovimientos["flag_tecnico"]=
                                Input::get('flag_tecnico');
                    $ultimoMovimiento["flag_tecnico"]=
                                Input::get('flag_tecnico');
                    }            

                    if ( Input::get('horario_id') && Input::get('horario_id')!='' ) {
                    $gestionesMovimientos["horario_id"]=
                                Input::get('horario_id');
                    $gestionesMovimientos["dia_id"]=
                                Input::get('dia_id');
                    $gestionesMovimientos["fecha_agenda"]=
                                Input::get('fecha_agenda');

                    $ultimoMovimiento["horario_id"]=
                                Input::get('horario_id');
                    $ultimoMovimiento["dia_id"]=
                                Input::get('dia_id');
                    $ultimoMovimiento["fecha_agenda"]=
                                Input::get('fecha_agenda');
                    }

                    if ( Input::get('tecnico') && Input::get('tecnico')!='' ) {
                    $gestionesMovimientos["celula_id"]=
                                Input::get('celula');
                    $gestionesMovimientos["tecnico_id"]=
                                Input::get('tecnico');

                    $ultimoMovimiento["celula_id"]=
                                Input::get('celula');
                    $ultimoMovimiento["tecnico_id"]=
                                Input::get('tecnico');
                    }

                    if ( Input::get('fecha_consolidacion') && Input::get('fecha_consolidacion')!='' ) {
                    $gestionesMovimientos["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');

                    $ultimoMovimiento["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');
                    }
                    
                    //Origen del movimiento realizado
                    $gestionesMovimientos["submodulo_id"] = 3;
                    if ( Input::get('submodulo_id') !== null )
                    {
                        $gestionesMovimientos["submodulo_id"] = 
                                Input::get('submodulo_id');
                    }
                    
                    //OFSC
                    $ultimoMovimiento["actividad_tipo_id"]=
                            Input::get('actividad_tipo_id');

                    $gestionesMovimientos['usuario_created_at']=Auth::user()->id;
                    $gestionesMovimientos->save();

                    $ultimoMovimiento['usuario_updated_at']=Auth::user()->id;
                    $ultimoMovimiento['updated_at']=date("Y-m-d H:i:s");
                    $ultimoMovimiento['usuario_created_at']=Auth::user()->id;
                    $ultimoMovimiento->save();
                    
                    //Control de cupos
                    $gestionesMovimientos->estado_agendamiento 
                            = Input::get('estado_agendamiento');
                    $cupos = $this->controlarCupos($gestionesMovimientos);
                                        
                } catch (Exception $exc) {
                    $this->_errorController->saveError($exc);
                    return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro del movimiento',
                            'err'=> $exc
                        );
                }

                if ( Input::get('contacto') ) {
                    try {
                        $liquidados = new Liquidado();
                        $liquidados['gestion_id']=$id;
                        if(Input::get('feedback') && Input::get('feedback')!='')
                           $liquidados['feedback_liquidado_id']=
                            Input::get('feedback'); 
                        else $liquidados['feedback_liquidado_id']=10;

                        if(Input::get('solucion') && Input::get('solucion')!='')
                            $liquidados['solucion_comercial_id']=
                            Input::get('solucion');
                        else $liquidados['solucion_comercial_id']=17;

                        $liquidados['contacto']=
                            Input::get('contacto');
                        $liquidados['pruebas']=
                            Input::get('pruebas');
                        $liquidados['fecha_consolidacion']=
                            Input::get('fecha_consolidacion');
                        $liquidados['penalizable']=
                            Input::get('penalizable_obs');

                            if ( Input::get('cumplimiento') ) {
                                $liquidados['cumplimiento_agenda']= Input::get('cumplimiento');
                            }

                        $liquidados['usuario_created_at']=Auth::user()->id;
                        $liquidados->save();
                    } catch (Exception $exc) {
                        $this->_errorController->saveError($exc);
                        return  array(
                            'rst'=>2,
                            'msj'=>'Ocurrió una interrupción en el registro de la gestion',
                            'err'=> $exc
                        );
                    }
                }
                
            /*
            print_r($gestiones);
            print_r($gestionesDetalles);
            print_r($gestionesMovimientos);
            */
            }

            if( Input::get('componente') ){
                $validar=DB::table('componente_gestion')
                            ->where('gestion_id','=',$gestionId)
                            ->get();

                if( count($validar)==0 ){
                    $componentes= Input::get('componente');
                    for($i=0; $i<count($componentes); $i++){
                        $comp=new ComponenteGestion;
                        $comp['componente_id']=$componentes[$i];
                        $comp['gestion_id']=$gestionId;
                        $comp['usuario_created_at']=Auth::user()->id;
                        $comp->save();
                    }
                }
            }
            
            if (!$cupos) {
                $err["file"] = __FILE__;
                $err["line"] = __LINE__;
                $err["message"] =  'No hay cupo';
                $err["trace"] =  __FUNCTION__ ;
                return  array(
                    'rst'=>2,
                    'msj'=>'No hay cupos disponibles para el horario'
                           . ' seleccionado. Vuelva a cargar los horarios'
                           . ' de agendamiento.',
                    'err' => $err /*'No hay cupo'*/
                );
            } else {
                return  array(
                    'rst'=>1,
                    'codactu'=>Input::get('codactu'),
                    'gestion_id'=>$gestionId
                );
            }
            
            
        }

    }

    public function postValidaofficetrack()
    {
        $cant= GestionMovimiento::getValidaOfficetrack();
        return $cant;
    }
    
    
    public function controlarCupos($data){
        
        $cupos = 0;
        $contar = 0;
        $orden = 0;
        $cupoBool = true;
        
        if (substr($data->estado_agendamiento, -2)=='-1' ) {
            $result = GestionMovimiento::getControlarCupos($data);
            
            //Cupos disponibles            
            foreach ($result["cupos"] as $val) {
                $cupos = $val->capacidad;
            }
            
            //Conteo y control
            
            foreach ($result["ocupado"] as $val) {
                $contar++;
                if ($data->id == $val->id) {
                    $orden = $contar;
                }
            }
            
            //Si orden > cupos: FALSE
            if ($orden > $cupos) {
                $cupoBool = false;
            }
            
        }
        
        return $cupoBool;
    }
    
    public function postRegistrar($input = array())
    {
        if (count($input) > 0) {
            Input::replace($input);
        }
        if (Auth::user() === null)
            $userCreatedAt = 697;
        else
            $userCreatedAt = Auth::user()->id;

        try {
            DB::beginTransaction();
            
            $returnArray = array(
                "rst" => 0,
                "msj" => "No se realizaron cambios",
                "act" => null,
                "error" => "",
                "gestion_id" => ""
            );

            if ( Request::ajax() or Input::get('noajax') ) {
                $gestionId="";
                $cupos = true;
                if ( Input::has('gestion_id') && Input::get("gestion_id")!=="" ) {
                    
                    $returnArray["act"] = "Nuevo movimiento creado";
                    
                    $gestionId= Input::get('gestion_id');
                    
                    $ultmov= DB::table('ultimos_movimientos')
                            ->where('gestion_id', '=', $gestionId)
                            ->first();
                    $ultimoMovimiento= UltimoMovimiento::find($ultmov->id);


                    $gestionesMovimientos = new GestionMovimiento;
                    $gestionesMovimientos["quiebre_id"]=
                                Input::get('quiebre_id');
                    $gestionesMovimientos["gestion_id"]=
                                Input::get('gestion_id');
                    $gestionesMovimientos["empresa_id"]=
                                Input::get('empresa_id');
                    $gestionesMovimientos["zonal_id"]=
                                Input::get('zonal_id');
                    $gestionesMovimientos["estado_id"]=
                                Input::get('estado');
                    $gestionesMovimientos["motivo_id"]=
                                Input::get('motivo');
                    $gestionesMovimientos["submotivo_id"]=
                                Input::get('submotivo');
                    $gestionesMovimientos["observacion"]=
                                Input::get('observacion2');
                    $gestionesMovimientos["coordinado"]=
                                Input::get('coordinado2');

                    $gestionesMovimientos['estado_ofsc_id']='0';
                    $gestionesMovimientos['y_ofsc']=Input::get('y_ofsc','0');
                    $gestionesMovimientos['x_ofsc']=Input::get('x_ofsc','0');
                    $gestionesMovimientos['usuario_created_at']=$userCreatedAt;

                    //ULTIMOS MOVIMIENTOS
                    $ultimoMovimiento["quiebre_id"]=
                                Input::get('quiebre_id');
                    $ultimoMovimiento["empresa_m_id"]=
                                Input::get('empresa_id');
                    $ultimoMovimiento["zonal_id"]=
                                Input::get('zonal_id');
                    $ultimoMovimiento["estado_id"]=
                                Input::get('estado');
                    $ultimoMovimiento["motivo_id"]=
                                Input::get('motivo');
                    $ultimoMovimiento["submotivo_id"]=
                                Input::get('submotivo');
                    $ultimoMovimiento["observacion_m"]=
                                Input::get('observacion2');
                    $ultimoMovimiento["coordinado"]=
                                Input::get('coordinado2');
                    if(Input::get('estado_ofsc_id')){
                        if(Input::get('estado_ofsc_id') == 6){
                            $ultimoMovimiento["x_fin"]=
                                Input::get('x_ofsc','0');
                            $ultimoMovimiento["y_fin"]=
                                Input::get('y_ofsc','0');
                        }else if(Input::get('estado_ofsc_id') == 2){
                            $ultimoMovimiento["x_inicio"]=
                                Input::get('x_ofsc','0');
                            $ultimoMovimiento["y_inicio"]=
                                Input::get('y_ofsc','0');
                        }
                    }

                    if (Input::get('fonos_contacto')) {
                        /*$gestionesDetalles["fonos_contacto"]=
                                Input::get('fonos_contacto');*/
                        $ultimoMovimiento["fonos_contacto"]=
                                    Input::get('fonos_contacto');
                    }

                    if ( Input::get('flag_tecnico') ) {
                        $ultimoMovimiento["flag_tecnico"]=
                                    Input::get('flag_tecnico');
                        $gestionesMovimientos["flag_tecnico"]=
                                    Input::get('flag_tecnico');
                    }            

                    if ( Input::get('horario_id') && Input::get('horario_id')!='' ) {
                        $gestionesMovimientos["horario_id"]=
                                    Input::get('horario_id');
                        $gestionesMovimientos["dia_id"]=
                                    Input::get('dia_id');
                        $gestionesMovimientos["fecha_agenda"]=
                                    Input::get('fecha_agenda');

                        $ultimoMovimiento["horario_id"]=
                                    Input::get('horario_id');
                        $ultimoMovimiento["dia_id"]=
                                    Input::get('dia_id');
                        $ultimoMovimiento["fecha_agenda"]=
                                    Input::get('fecha_agenda');
                    }

                    if ( Input::get('tecnico') && Input::get('tecnico')!='' ) {
                        $gestionesMovimientos["celula_id"]=
                                    Input::get('celula');
                        $gestionesMovimientos["tecnico_id"]=
                                    Input::get('tecnico');

                        $ultimoMovimiento["celula_id"]=
                                    Input::get('celula');
                        $ultimoMovimiento["tecnico_id"]=
                                    Input::get('tecnico');
                    }

                    if ( Input::get('fecha_consolidacion') && Input::get('fecha_consolidacion')!='' ) {
                        $gestionesMovimientos["fecha_consolidacion"]=
                                    Input::get('fecha_consolidacion');

                        $ultimoMovimiento["fecha_consolidacion"]=
                                    Input::get('fecha_consolidacion');
                    }

                    //Origen del movimiento realizado
                    $gestionesMovimientos["submodulo_id"] = 3;
                    if ( Input::get('submodulo_id') !== null )
                    {
                        $gestionesMovimientos["submodulo_id"] = 
                                Input::get('submodulo_id');
                    }

                    if (Input::has('motivo_ofsc_id')){
                        $gestionesMovimientos['motivo_ofsc_id']=Input::get('motivo_ofsc_id');
                        $ultimoMovimiento['motivo_ofsc_id']=Input::get('motivo_ofsc_id');
                    }

                    if (Input::has('f_inicio')){
                        $ultimoMovimiento['f_inicio']=Input::get('f_inicio');
                    }
                    if (Input::has('f_cierre')){
                        $ultimoMovimiento['f_cierre']=Input::get('f_cierre');
                    }

                    if (Input::has('submotivo_ofsc_id')){
                        $gestionesMovimientos['submotivo_ofsc_id']=Input::get('submotivo_ofsc_id');
                        $ultimoMovimiento['submotivo_ofsc_id']=Input::get('submotivo_ofsc_id');
                    }

                    if (Input::has('resource_id')){
                        $ultimoMovimiento['resource_id']=Input::get('resource_id');
                    }

                    $gestionesMovimientos['programado']=Input::get('programado',null);
                    $gestionesMovimientos['aid']=Input::get('aid',null);
                    $gestionesMovimientos['envio_ofsc']=Input::get('envio_ofsc',null);
                    $gestionesMovimientos['estado_ofsc_id']=
                        Input::get('estado_ofsc_id','0');
                    $gestionesMovimientos['y_ofsc']=Input::get('y_ofsc','0');
                    $gestionesMovimientos['x_ofsc']=Input::get('x_ofsc','0');
                    $gestionesMovimientos['usuario_created_at']=$userCreatedAt ;

                    $gestionesMovimientos['resource_id']=Input::get('resource_id',null);
                    $gestionesMovimientos['slot']=Input::get('slot',null);

                    $gestionesMovimientos->save();

                    if( substr(Input::get('estado_agendamiento'),-2)=='-1' ) {
                        GestionDetalle::where('gestion_id', '=', $gestionId)
                            ->update(
                                [
                                    'x' => Input::get('x'),
                                    'y' => Input::get('y'),
                                    'aid' => Input::get('aid',0)
                                ]
                            );

                        $ultimoMovimiento['x']=Input::get('x');
                        $ultimoMovimiento['y']=Input::get('y');
                    //ofsc, viene del modelo estadoRespuesta
                    } elseif ( Input::has('aid')) {
                        GestionDetalle::where('gestion_id', '=', $gestionId)
                            ->update([ 'aid' => Input::get('aid') ]);
                    }

                    $ultimoMovimiento['resource_id']=Input::get('resource_id',null);

                    $ultimoMovimiento['programado']=Input::get('programado',null);
                    $ultimoMovimiento['aid']=Input::get('aid',null);
                    $ultimoMovimiento['envio_ofsc']=Input::get('envio_ofsc',null);
                    $ultimoMovimiento['estado_ofsc_id']=
                        Input::get('estado_ofsc_id','0');
                    $ultimoMovimiento['usuario_updated_at']=$userCreatedAt ;
                    $ultimoMovimiento->save();

                } else {
                    $id="";
                    $returnArray["act"] = "Nuevo registro creado";
                    
                    $gestiones = new Gestion;
                    $ultimoMovimiento= new UltimoMovimiento;

                    $gestiones["actividad_id"]=
                    Input::get('actividad_id');
                    $gestiones["nombre_cliente_critico"]=
                    Input::get('nombre_cliente_critico');
                    $gestiones["telefono_cliente_critico"]=
                    Input::get('telefono_cliente_critico');
                    $gestiones["celular_cliente_critico"]=
                    Input::get('celular_cliente_critico');
                    $gestiones->save();
                    
                    $gestiones['usuario_created_at'] = $userCreatedAt;
                  
                    $ultimoMovimiento["actividad_id"]=
                    Input::get('actividad_id');
                    $ultimoMovimiento["nombre_cliente_critico"]=
                    Input::get('nombre_cliente_critico');
                    $ultimoMovimiento["telefono_cliente_critico"]=
                    Input::get('telefono_cliente_critico');
                    $ultimoMovimiento["celular_cliente_critico"]=
                    Input::get('celular_cliente_critico');


                    $ultimoMovimiento['usuario_created_at']=$userCreatedAt ;

                    $gestionId= $gestiones->id;
                    $id=$gestiones->id;
                    $gestiones["id_atc"]="ATC_".date("Y")."_".$id;
                    $gestiones->save();

                    $ultimoMovimiento["id_atc"]="ATC_".date("Y")."_".$id;
                

                    $gestionesDetalles = new GestionDetalle;
                    $gestionesDetalles["gestion_id"]=$id;
                    $gestionesDetalles["quiebre_id"]=
                            Input::get('quiebre_id');
                    $gestionesDetalles["empresa_id"]=
                            Input::get('empresa_id');
                    $gestionesDetalles["zonal_id"]=
                            Input::get('zonal_id');
                    $gestionesDetalles["codactu"]=
                            Input::get('codactu');
                    $gestionesDetalles["tipo_averia"]=
                            Input::get('tipo_averia');
                    $gestionesDetalles["horas_averia"]=
                            Input::get('horas_averia');
                    $gestionesDetalles["fecha_registro"]=
                            Input::get('fecha_registro');
                    $gestionesDetalles["ciudad"]=
                            Input::get('ciudad');
                    $gestionesDetalles["inscripcion"]=
                            Input::get('inscripcion');
                    $gestionesDetalles["fono1"]=
                            Input::get('fono1');
                    $gestionesDetalles["telefono"]=
                            Input::get('telefono');
                    $gestionesDetalles["mdf"]=
                            Input::get('mdf');
                    $gestionesDetalles["observacion"]=
                            Input::get('observacion');
                    $gestionesDetalles["segmento"]=
                            Input::get('segmento');
                    $gestionesDetalles["area"]=
                            Input::get('area');
                    $gestionesDetalles["direccion_instalacion"]=
                            Input::get('direccion_instalacion');
                    $gestionesDetalles["codigo_distrito"]=
                            Input::get('codigo_distrito');
                    $gestionesDetalles["nombre_cliente"]=
                            Input::get('nombre_cliente');
                    $gestionesDetalles["orden_trabajo"]=
                            Input::get('orden_trabajo');
                    $gestionesDetalles["veloc_adsl"]=
                            Input::get('veloc_adsl');
                    $gestionesDetalles["clase_servicio_catv"]=
                            Input::get('clase_servicio_catv');
                    $gestionesDetalles["codmotivo_req_catv"]=
                            Input::get('codmotivo_req_catv');
                    $gestionesDetalles["total_averias_cable"]=
                            Input::get('total_averias_cable');
                    $gestionesDetalles["total_averias_cobre"]=
                            Input::get('total_averias_cobre');
                    $gestionesDetalles["total_averias"]=
                            Input::get('total_averias');
                    $gestionesDetalles["fftt"]=
                            Input::get('fftt');
                    $gestionesDetalles["llave"]=
                            Input::get('llave');
                    $gestionesDetalles["dir_terminal"]=
                            Input::get('dir_terminal');
                    $gestionesDetalles["fonos_contacto"]=
                            Input::get('fonos_contacto');
                    $gestionesDetalles["contrata"]=
                            Input::get('contrata');
                    $gestionesDetalles["zonal"]=
                            Input::get('zonal');
                    $gestionesDetalles["wu_nagendas"]=
                            Input::get('wu_nagendas');
                    $gestionesDetalles["wu_nmovimientos"]=
                            Input::get('wu_nmovimientos');
                    $gestionesDetalles["wu_fecha_ult_agenda"]=
                            Input::get('wu_fecha_ult_agenda');
                    $gestionesDetalles["total_llamadas_tecnicas"]=
                            Input::get('total_llamadas_tecnicas');
                    $gestionesDetalles["total_llamadas_seguimiento"]=
                            Input::get('total_llamadas_seguimiento');
                    $gestionesDetalles["llamadastec15dias"]=
                            Input::get('llamadastec15dias');
                    $gestionesDetalles["llamadastec30dias"]=
                            Input::get('llamadastec30dias');
                    $gestionesDetalles["lejano"]=
                            Input::get('lejano');
                    $gestionesDetalles["distrito"]=
                            Input::get('distrito');
                    $gestionesDetalles["eecc_zona"]=
                            Input::get('eecc_zona');
                    $gestionesDetalles["zona_movistar_uno"]=
                            Input::get('zona_movistar_uno');
                    $gestionesDetalles["paquete"]=
                            Input::get('paquete');
                    $gestionesDetalles["data_multiproducto"]=
                            Input::get('data_multiproducto');
                    $gestionesDetalles["averia_m1"]=
                            Input::get('averia_m1');
                    $gestionesDetalles["fecha_data_fuente"]=
                            Input::get('fecha_data_fuente');
                    $gestionesDetalles["telefono_codclientecms"]=
                            Input::get('telefono_codclientecms');
                    $gestionesDetalles["rango_dias"]=
                            Input::get('rango_dias');
                    $gestionesDetalles["sms1"]=
                            Input::get('sms1');
                    $gestionesDetalles["sms2"]=
                            Input::get('sms2');
                    $gestionesDetalles["area2"]=
                            Input::get('area2');
                    $gestionesDetalles["microzona"]=
                            Input::get('microzona');
                    $gestionesDetalles["tipo_actuacion"]=
                            Input::get('tipo_actuacion');
                    $gestionesDetalles["actividad_tipo_id"]=
                            Input::get('actividad_tipo_id');
                    $gestionesDetalles["fecha_psi"]=
                            Input::get('fecha_registro_psi');

                    if( Input::get('x') and Input::get('y')/*substr(Input::get('estado_agendamiento'),-2)=='-1'*/ ) {
                        $gestionesDetalles["x"]=
                                Input::get('x');
                        $gestionesDetalles["y"]=
                                Input::get('y');

                        $ultimoMovimiento["x"]=
                                Input::get('x');
                        $ultimoMovimiento["y"]=
                                Input::get('y');
                    }
                    $gestionesDetalles['aid'] = Input::get('aid',null);
                    $gestionesDetalles['componente'] = Input::get('componente',0);
                    

                    $gestionesDetalles['usuario_created_at']=$userCreatedAt;
                    $gestionesDetalles->save();
                    

                    /**********************************************************/
                    $ffttExplode=explode("|", Input::get('fftt'));
                    $tipoAExplode=explode("-", Input::get('tipo_averia'));
                    if( count($tipoAExplode)==1 ){
                        $tipoAExplode=explode("_", Input::get('tipo_averia'));
                    }

                    $arrayproadsl=array(1,2,4);
                    $arrayprocatv=array(5,6,7,8,9);

                    $arrayaveradsl=array(1,2,3,14,15,16,17,9,18,19,20);
                    $arrayaverbas=array(1,2,3,10,11,12,4,9);
                    $arrayavercatv=array(5,13,6,7,8,9);


                    $sqlttff='INSERT INTO gestiones_fftt (gestion_id,fftt_tipo_id,nombre) VALUES (?,?,?)';
                    if ( in_array('aver', $tipoAExplode, true) AND count($ffttExplode)>1 ){
                        if ( in_array('adsl', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayaveradsl); $i++){
                                $array=array($id,$arrayaveradsl[$i],trim($ffttExplode[$i]));

                                if( trim($ffttExplode[$i])!='' ){
                                    DB::insert($sqlttff, $array);
                                }
                            }
                        }
                        elseif ( in_array('bas', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayaverbas); $i++){
                                $array=array($id,$arrayaverbas[$i],trim($ffttExplode[$i]));

                                if( trim($ffttExplode[$i])!='' ){
                                    DB::insert($sqlttff, $array);
                                }
                            }
                        }
                        elseif ( in_array('catv', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayavercatv); $i++){
                                $array=array($id,$arrayavercatv[$i],trim($ffttExplode[$i]));

                                if( trim($ffttExplode[$i])!='' ){
                                    DB::insert($sqlttff, $array);
                                }
                            }
                        }
                    }
                    elseif ( in_array('prov', $tipoAExplode, true) AND count($ffttExplode)>1 ){
                        if ( in_array('adsl', $tipoAExplode, true) OR in_array('bas', $tipoAExplode, true) ){
                            for ($i=0; $i<count($arrayproadsl); $i++) {
                                if( $i==1 AND strtoupper(substr(trim($ffttExplode[$i]),0,1))=='A' ){
                                    $array=array($id,$arrayproadsl[$i],trim($ffttExplode[$i]));
                                } elseif($i==1 ){
                                    $array=array($id,3,trim($ffttExplode[$i]));
                                } else{
                                    $array=array($id,$arrayproadsl[$i],trim($ffttExplode[$i]));
                                }

                                if( trim($ffttExplode[$i])!='' ){
                                    DB::insert($sqlttff, $array);
                                }
                            }
                        } elseif ( in_array('catv', $tipoAExplode, true) ){
                            for($i=0; $i<count($arrayprocatv); $i++){
                                $ffttExplodeTmp='';
                                if (isset($ffttExplode[$i])) {
                                    $ffttExplodeTmp = $ffttExplode[$i];
                                }
                                $array=array($id,$arrayprocatv[$i],trim($ffttExplodeTmp));

                                if( trim($ffttExplodeTmp)!='' ){
                                    DB::insert($sqlttff, $array);
                                }
                            }
                        }
                    }
                    /**********************************************************/
                    $ultimoMovimiento["gestion_id"]=$id;
                    $ultimoMovimiento["quiebre_id"]=
                            Input::get('quiebre_id');
                    $ultimoMovimiento["empresa_id"]=
                            Input::get('empresa_id');
                    $ultimoMovimiento["zonal_id"]=
                            Input::get('zonal_id');
                    $ultimoMovimiento["codactu"]=
                            Input::get('codactu');
                    $ultimoMovimiento["tipo_averia"]=
                            Input::get('tipo_averia');
                    $ultimoMovimiento["horas_averia"]=
                            Input::get('horas_averia');
                    $ultimoMovimiento["fecha_registro"]=
                            Input::get('fecha_registro');
                    $ultimoMovimiento["ciudad"]=
                            Input::get('ciudad');
                    $ultimoMovimiento["inscripcion"]=
                            Input::get('inscripcion');
                    $ultimoMovimiento["fono1"]=
                            Input::get('fono1');
                    $ultimoMovimiento["telefono"]=
                            Input::get('telefono');
                    $ultimoMovimiento["mdf"]=
                            Input::get('mdf');
                    $ultimoMovimiento["observacion"]=
                            Input::get('observacion');
                    $ultimoMovimiento["segmento"]=
                            Input::get('segmento');
                    $ultimoMovimiento["area"]=
                            Input::get('area');
                    $ultimoMovimiento["direccion_instalacion"]=
                            Input::get('direccion_instalacion');
                    $ultimoMovimiento["codigo_distrito"]=
                            Input::get('codigo_distrito');
                    $ultimoMovimiento["nombre_cliente"]=
                            Input::get('nombre_cliente');
                    $ultimoMovimiento["orden_trabajo"]=
                            Input::get('orden_trabajo');
                    $ultimoMovimiento["veloc_adsl"]=
                            Input::get('veloc_adsl');
                    $ultimoMovimiento["clase_servicio_catv"]=
                            Input::get('clase_servicio_catv');
                    $ultimoMovimiento["codmotivo_req_catv"]=
                            Input::get('codmotivo_req_catv');
                    $ultimoMovimiento["total_averias_cable"]=
                            Input::get('total_averias_cable');
                    $ultimoMovimiento["total_averias_cobre"]=
                            Input::get('total_averias_cobre');
                    $ultimoMovimiento["total_averias"]=
                            Input::get('total_averias');
                    $ultimoMovimiento["fftt"]=
                            Input::get('fftt');
                    $ultimoMovimiento["llave"]=
                            Input::get('llave');
                    $ultimoMovimiento["dir_terminal"]=
                            Input::get('dir_terminal');
                    $ultimoMovimiento["fonos_contacto"]=
                            Input::get('fonos_contacto');
                    $ultimoMovimiento["contrata"]=
                            Input::get('contrata');
                    $ultimoMovimiento["zonal"]=
                            Input::get('zonal');
                    $ultimoMovimiento["wu_nagendas"]=
                            Input::get('wu_nagendas');
                    $ultimoMovimiento["wu_nmovimientos"]=
                            Input::get('wu_nmovimientos');
                    $ultimoMovimiento["wu_fecha_ult_agenda"]=
                            Input::get('wu_fecha_ult_agenda');
                    $ultimoMovimiento["total_llamadas_tecnicas"]=
                            Input::get('total_llamadas_tecnicas');
                    $ultimoMovimiento["total_llamadas_seguimiento"]=
                            Input::get('total_llamadas_seguimiento');
                    $ultimoMovimiento["llamadastec15dias"]=
                            Input::get('llamadastec15dias');
                    $ultimoMovimiento["llamadastec30dias"]=
                            Input::get('llamadastec30dias');
                    $ultimoMovimiento["lejano"]=
                            Input::get('lejano');
                    $ultimoMovimiento["distrito"]=
                            Input::get('distrito');
                    $ultimoMovimiento["eecc_zona"]=
                            Input::get('eecc_zona');
                    $ultimoMovimiento["zona_movistar_uno"]=
                            Input::get('zona_movistar_uno');
                    $ultimoMovimiento["paquete"]=
                            Input::get('paquete');
                    $ultimoMovimiento["data_multiproducto"]=
                            Input::get('data_multiproducto');
                    $ultimoMovimiento["averia_m1"]=
                            Input::get('averia_m1');
                    $ultimoMovimiento["fecha_data_fuente"]=
                            Input::get('fecha_data_fuente');
                    $ultimoMovimiento["telefono_codclientecms"]=
                            Input::get('telefono_codclientecms');
                    $ultimoMovimiento["rango_dias"]=
                            Input::get('rango_dias');
                    $ultimoMovimiento["sms1"]=
                            Input::get('sms1');
                    $ultimoMovimiento["sms2"]=
                            Input::get('sms2');
                    $ultimoMovimiento["area2"]=
                            Input::get('area2');
                    $ultimoMovimiento["microzona"]=
                            Input::get('microzona');
                    $ultimoMovimiento["tipo_actuacion"]=
                            Input::get('tipo_actuacion');


                
                    $gestionesMovimientos = new GestionMovimiento;
                    $gestionesMovimientos["gestion_id"]=$id;

                    $gestionesMovimientos["quiebre_id"]=
                                Input::get('quiebre_id');

                    $gestionesMovimientos["empresa_id"]=
                                Input::get('empresa_id');
                    $gestionesMovimientos["zonal_id"]=
                                Input::get('zonal_id');
                    $gestionesMovimientos["estado_id"]=
                                Input::get('estado');
                    $gestionesMovimientos["motivo_id"]=
                                Input::get('motivo');
                    $gestionesMovimientos["submotivo_id"]=
                                Input::get('submotivo');
                    $gestionesMovimientos["observacion"]=
                                Input::get('observacion2');
                    $gestionesMovimientos["coordinado"]=
                                Input::get('coordinado2');

                    $ultimoMovimiento["empresa_m_id"]=
                                Input::get('empresa_id');
                    $ultimoMovimiento["estado_id"]=
                                Input::get('estado');
                    $ultimoMovimiento["motivo_id"]=
                                Input::get('motivo');
                    $ultimoMovimiento["submotivo_id"]=
                                Input::get('submotivo');
                    $ultimoMovimiento["observacion_m"]=
                                Input::get('observacion2');
                    $ultimoMovimiento["coordinado"]=
                                Input::get('coordinado2');

                    if ( Input::get('flag_tecnico') ) {
                        $gestionesMovimientos["flag_tecnico"]=
                                Input::get('flag_tecnico');
                        $ultimoMovimiento["flag_tecnico"]=
                                Input::get('flag_tecnico');
                    }

                    if ( Input::get('horario_id') &&
                         Input::get('horario_id')!='' ) {
                        $gestionesMovimientos["horario_id"]=
                                Input::get('horario_id');
                        $gestionesMovimientos["dia_id"]=
                                Input::get('dia_id');
                        $gestionesMovimientos["fecha_agenda"]=
                                Input::get('fecha_agenda');

                        $ultimoMovimiento["horario_id"]=
                                Input::get('horario_id');
                        $ultimoMovimiento["dia_id"]=
                                Input::get('dia_id');
                        $ultimoMovimiento["fecha_agenda"]=
                                Input::get('fecha_agenda');
                    }

                    if ( Input::get('tecnico') && Input::get('tecnico')!='' ) {
                        $gestionesMovimientos["celula_id"]=
                                Input::get('celula');
                        $gestionesMovimientos["tecnico_id"]=
                                Input::get('tecnico');

                        $ultimoMovimiento["celula_id"]=
                                Input::get('celula');
                        $ultimoMovimiento["tecnico_id"]=
                                Input::get('tecnico');
                    }

                    if ( Input::get('fecha_consolidacion') && Input::get('fecha_consolidacion')!='' ) {
                        $gestionesMovimientos["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');

                        $ultimoMovimiento["fecha_consolidacion"]=
                                Input::get('fecha_consolidacion');
                    }

                    //Origen del movimiento realizado
                    $gestionesMovimientos["submodulo_id"] = 3;
                    if ( Input::get('submodulo_id') !== null )
                    {
                        $gestionesMovimientos["submodulo_id"] = 
                                Input::get('submodulo_id');
                    }

                    //OFSC
                    $ultimoMovimiento["actividad_tipo_id"]=
                            Input::get('actividad_tipo_id');

                    $gestionesMovimientos['programado']=Input::get('programado',null);
                    $gestionesMovimientos['aid']=Input::get('aid',null);
                    $gestionesMovimientos['envio_ofsc']=Input::get('envio_ofsc',null);
                    $gestionesMovimientos['estado_ofsc_id']=
                        Input::get('estado_ofsc_id','0');
                    $gestionesMovimientos['y_ofsc']=Input::get('y_ofsc','0');
                    $gestionesMovimientos['x_ofsc']=Input::get('x_ofsc','0');
                    $gestionesMovimientos['usuario_created_at']=$userCreatedAt ;

                    $gestionesMovimientos->save();

                    $ultimoMovimiento['programado']=Input::get('programado',null);
                    $ultimoMovimiento['aid']=Input::get('aid',null);
                    $ultimoMovimiento['envio_ofsc']=Input::get('envio_ofsc',null);
                    $ultimoMovimiento['estado_ofsc_id']=
                        Input::get('estado_ofsc_id','0');
                    $ultimoMovimiento['usuario_updated_at']=$userCreatedAt ;
                    $ultimoMovimiento['updated_at']=date("Y-m-d H:i:s");
                    $ultimoMovimiento['usuario_created_at']=$userCreatedAt ;

                    $ultimoMovimiento->save();
                        
                    //Componentes
                    $compGestion = new ComponenteGestion();
                    $dataComp = $compGestion->getComponenteTmp(
                                Input::get("codactu")
                            );
                    
                    if (count($dataComp) > 0) {
                        $save = $compGestion->saveComponente(
                                $id, 
                                $dataComp["data"]
                            );
                    }

                }
                $returnArray["gestion_id"] = $gestionId;
                $returnArray['gestion_movimiento_id']=$gestionesMovimientos->id;
                
                DB::commit();
            
                $returnArray["rst"] = 1;
                $returnArray["msj"] = 'Registro realizado correctamente';
                $returnArray["error"] = "";
            }
            return $returnArray;
        } catch (Exception $exc) {
            DB::rollback();
            
            $returnArray["rst"] = 2;
            $returnArray["msj"] = 'Ocurrió un error en el registro';
            $returnArray["error"] = $exc->getMessage();
            $this->_errorController->saveError($exc);
            return $returnArray;
        }

    }

    public function postMovimientoobservaciones()
    {
        if (Request::ajax()) {
            $filtro=array('');

            if ( Input::get('quiebre') ) {
                $quiebre=implode(",", Input::get('quiebre'));
                $filtro[0].=" AND q.id IN (".$quiebre.")";
            }else{
                $filtro[0]='AND q.quiebre_grupo_id IN (  SELECT quiebre_grupo_id
                                    FROM quiebre_grupo_usuario
                                    WHERE estado=1
                                    AND usuario_id='.Auth::user()->id.'
                                )
                        AND q.id NOT IN (
                                SELECT quiebre_id
                                FROM quiebre_usuario_restringido
                                WHERE usuario_id="'.Auth::user()->id.'"
                                AND estado=1
                            )
                        AND gm.zonal_id IN (
                                SELECT zonal_id
                                FROM usuario_zonal
                                WHERE usuario_id="'.Auth::user()->id.'"
                                AND estado=1
                        )
                        ';
            }

            if ( Input::get('empresa') ) {
                $empresa=implode(",", Input::get('empresa'));
                $filtro[0].=" AND em.id IN (".$empresa.")";
            }

            $fechaAgenda=Input::get('fecha_agenda');
                                                
            $count = '  SELECT count(DISTINCT(gestion_id)) total
                        FROM gestiones_movimientos
                        WHERE fecha_agenda="'.$fechaAgenda.'"
                        AND motivo_id=1
                        AND DATE(created_at) BETWEEN DATE_SUB(CURDATE(),INTERVAL 10 DAY) AND CURRENT_DATE()';

        $query = 'SELECT gm.gestion_id,um.codactu, em.nombre empresa, q.nombre quiebre, um.fftt, ac.nombre actividad, 
                um.fecha_agenda Ult_Fecha, m.nombre Ult_Motivo, s.nombre Ult_SubMotivo, e.nombre Ult_Estado,
                CONCAT(t.ape_paterno,", ",t.ape_materno,",",t.nombres) Ult_tecnico,
                COUNT(DISTINCT(gm.id)) Nro_Mov_T,
                COUNT(DISTINCT(gm1.id)) Nro_Mov_MaIg,
                COUNT(DISTINCT(gm2.id)) Nro_Mov_Me,
                COUNT(DISTINCT( IF(gm1.motivo_id = 1, gm1.fecha_agenda, NULL))) Nro_Fecha_Agenda_Ma,
                COUNT(DISTINCT( IF(gm2.motivo_id = 1, gm2.fecha_agenda, NULL))) Nro_Fecha_Agenda_Me,
                COUNT(DISTINCT( 
                            IF( LOCATE("Inicio",wot1.paso)>0 
                                    AND wot1.fecha_recepcion 
                                        BETWEEN 
                                            SUBDATE(gm1.created_at,INTERVAL 2 MINUTE) 
                                            AND 
                                            ADDDATE(gm1.created_at,INTERVAL 2 MINUTE),
                                    CONCAT(gm1.fecha_agenda,"_",gm1.horario_id),
                                    NULL 
                            )               )
                        ) Nro_Inicio_MaIg,
                COUNT(DISTINCT( 
                            IF( LOCATE("Inicio",wot2.paso)>0 
                                    AND wot2.fecha_recepcion 
                                        BETWEEN 
                                            SUBDATE(gm2.created_at,INTERVAL 2 MINUTE) 
                                            AND 
                                            ADDDATE(gm2.created_at,INTERVAL 2 MINUTE),
                                    CONCAT(gm2.fecha_agenda,"_",gm2.horario_id),
                                    NULL 
                            )               )
                        ) Nro_Inicio_Me,
                COUNT(DISTINCT( 
                            IF( LOCATE("Cierre",wot1.paso)>0 
                                    AND wot1.fecha_recepcion 
                                        BETWEEN 
                                            SUBDATE(gm1.created_at,INTERVAL 2 MINUTE) 
                                            AND 
                                            ADDDATE(gm1.created_at,INTERVAL 2 MINUTE),
                                    CONCAT(gm1.fecha_agenda,"_",gm1.horario_id),
                                    NULL 
                            )               )
                        ) Nro_Cierre_MaIg,
                COUNT(DISTINCT( 
                            IF( LOCATE("Cierre",wot2.paso)>0 
                                    AND wot2.fecha_recepcion 
                                        BETWEEN 
                                            SUBDATE(gm2.created_at,INTERVAL 2 MINUTE) 
                                            AND 
                                            ADDDATE(gm2.created_at,INTERVAL 2 MINUTE),
                                    CONCAT(gm2.fecha_agenda,"_",gm2.horario_id),
                                    NULL 
                            )               )
                        ) Nro_Cierre_Me,
                COUNT(DISTINCT(mo.id)) Total_Obs,COUNT(DISTINCT(moma.id)) Total_Obs_MaIg,COUNT(DISTINCT(mome.id)) Total_Obs_Me,
                COUNT(DISTINCT(mo1.id)) Tp1,COUNT(DISTINCT(moma1.id)) Tp1_MaIg,COUNT(DISTINCT(mome1.id)) Tp1_Me,
                COUNT(DISTINCT(mo2.id)) Tp2,COUNT(DISTINCT(moma2.id)) Tp2_MaIg,COUNT(DISTINCT(mome2.id)) Tp2_Me,
                COUNT(DISTINCT(mo3.id)) Tp3,COUNT(DISTINCT(moma3.id)) Tp3_MaIg,COUNT(DISTINCT(mome3.id)) Tp3_Me,
                COUNT(DISTINCT(mo4.id)) Tp4,COUNT(DISTINCT(moma4.id)) Tp4_MaIg,COUNT(DISTINCT(mome4.id)) Tp4_Me,
                COUNT(DISTINCT(mo5.id)) Tp5,COUNT(DISTINCT(moma5.id)) Tp5_MaIg,COUNT(DISTINCT(mome5.id)) Tp5_Me,
                COUNT(DISTINCT(mo6.id)) Tp6,COUNT(DISTINCT(moma6.id)) Tp6_MaIg,COUNT(DISTINCT(mome6.id)) Tp6_Me,
                COUNT(DISTINCT(mo7.id)) Tp7,COUNT(DISTINCT(moma7.id)) Tp7_MaIg,COUNT(DISTINCT(mome7.id)) Tp7_Me
                FROM gestiones_movimientos gm
                INNER JOIN ultimos_movimientos um ON um.gestion_id=gm.gestion_id
                INNER JOIN (
                    SELECT gestion_id
                    FROM gestiones_movimientos
                    WHERE fecha_agenda="'.$fechaAgenda.'"
                    AND motivo_id=1
                    AND DATE(created_at) BETWEEN DATE_SUB(CURDATE(),INTERVAL 10 DAY) AND CURRENT_DATE()
                    GROUP BY gestion_id
                ) gf ON gf.gestion_id=gm.gestion_id
                INNER JOIN estados e ON e.id = um.estado_id 
                INNER JOIN motivos m ON m.id = um.motivo_id
                INNER JOIN submotivos s ON s.id = um.submotivo_id
                INNER JOIN empresas em ON em.id = um.empresa_id
                INNER JOIN quiebres q ON q.id = um.quiebre_id
                INNER JOIN actividades ac ON ac.id = um.actividad_id
                LEFT JOIN gestiones_movimientos gm1 ON gm1.id=gm.id AND DATE(gm1.created_at)>="'.$fechaAgenda.'"
                LEFT JOIN gestiones_movimientos gm2 ON gm2.id=gm.id AND DATE(gm2.created_at)<"'.$fechaAgenda.'"
                LEFT JOIN tecnicos t ON t.id = um.tecnico_id
                LEFT JOIN webpsi_officetrack.tareas wot1 ON wot1.task_id = gm1.gestion_id 
                LEFT JOIN webpsi_officetrack.tareas wot2 ON wot2.task_id = gm2.gestion_id 
                LEFT JOIN movimientos_observaciones mo ON mo.gestion_movimiento_id=gm.id
                LEFT JOIN movimientos_observaciones mo1 ON mo1.gestion_movimiento_id=gm.id AND mo1.observacion_tipo_id=1 
                LEFT JOIN movimientos_observaciones mo2 ON mo2.gestion_movimiento_id=gm.id AND mo2.observacion_tipo_id=2
                LEFT JOIN movimientos_observaciones mo3 ON mo3.gestion_movimiento_id=gm.id AND mo3.observacion_tipo_id=3
                LEFT JOIN movimientos_observaciones mo4 ON mo4.gestion_movimiento_id=gm.id AND mo4.observacion_tipo_id=4
                LEFT JOIN movimientos_observaciones mo5 ON mo5.gestion_movimiento_id=gm.id AND mo5.observacion_tipo_id=5
                LEFT JOIN movimientos_observaciones mo6 ON mo6.gestion_movimiento_id=gm.id AND mo6.observacion_tipo_id=6
                LEFT JOIN movimientos_observaciones mo7 ON mo7.gestion_movimiento_id=gm.id AND mo7.observacion_tipo_id=7
                LEFT JOIN movimientos_observaciones moma ON moma.gestion_movimiento_id=gm1.id
                LEFT JOIN movimientos_observaciones moma1 ON moma1.gestion_movimiento_id=gm1.id AND moma1.observacion_tipo_id=1 
                LEFT JOIN movimientos_observaciones moma2 ON moma2.gestion_movimiento_id=gm1.id AND moma2.observacion_tipo_id=2
                LEFT JOIN movimientos_observaciones moma3 ON moma3.gestion_movimiento_id=gm1.id AND moma3.observacion_tipo_id=3
                LEFT JOIN movimientos_observaciones moma4 ON moma4.gestion_movimiento_id=gm1.id AND moma4.observacion_tipo_id=4
                LEFT JOIN movimientos_observaciones moma5 ON moma5.gestion_movimiento_id=gm1.id AND moma5.observacion_tipo_id=5
                LEFT JOIN movimientos_observaciones moma6 ON moma6.gestion_movimiento_id=gm1.id AND moma6.observacion_tipo_id=6
                LEFT JOIN movimientos_observaciones moma7 ON moma7.gestion_movimiento_id=gm1.id AND moma7.observacion_tipo_id=7
                LEFT JOIN movimientos_observaciones mome ON mome.gestion_movimiento_id=gm2.id
                LEFT JOIN movimientos_observaciones mome1 ON mome1.gestion_movimiento_id=gm2.id AND mome1.observacion_tipo_id=1 
                LEFT JOIN movimientos_observaciones mome2 ON mome2.gestion_movimiento_id=gm2.id AND mome2.observacion_tipo_id=2
                LEFT JOIN movimientos_observaciones mome3 ON mome3.gestion_movimiento_id=gm2.id AND mome3.observacion_tipo_id=3
                LEFT JOIN movimientos_observaciones mome4 ON mome4.gestion_movimiento_id=gm2.id AND mome4.observacion_tipo_id=4
                LEFT JOIN movimientos_observaciones mome5 ON mome5.gestion_movimiento_id=gm2.id AND mome5.observacion_tipo_id=5
                LEFT JOIN movimientos_observaciones mome6 ON mome6.gestion_movimiento_id=gm2.id AND mome6.observacion_tipo_id=6
                LEFT JOIN movimientos_observaciones mome7 ON mome7.gestion_movimiento_id=gm2.id AND mome7.observacion_tipo_id=7
                WHERE DATE(gm.created_at) BETWEEN DATE_SUB(CURDATE(),INTERVAL 10 DAY) AND CURRENT_DATE()
                '.$filtro[0].'    
                GROUP BY gm.gestion_id
                ORDER BY Total_Obs DESC 
                LIMIT ' . Input::get('start') . ', ' . 
                Input::get('length') . '';

            $rcount = DB::select($count);
            
            $consulta = DB::select($query);
            
            $gestionmovimientos["draw"] = Input::get('draw');
            $gestionmovimientos["recordsTotal"] = $rcount[0]->total;
            $gestionmovimientos["recordsFiltered"] = $rcount[0]->total;

            $gestionmovimientos["data"] = $consulta;

            return Response::json($gestionmovimientos);
        }
    }


    /**
     * Actualización de  resource_id
     */
    public function getUpdate()
    {
        $xml = (array) simplexml_load_file('http://localhost:8080/webpsi20/public/xml/actividades.xml');
        $xml = json_decode(json_encode(  $xml ), 1);
        $in = array();
        //dd($xml);
        try {
            $con = array();  $err = 0; $no = 0;
            foreach ($xml as $key => $value) {
                $in [] = $value['id'];
            }
            $ultimo_movimientos = Gestion::getCargarGestion($in);
            
            $fix_xml = array();
            foreach ($xml as $key => $value) {
                $indice = explode('-', $key);
                $new_key = $indice[0];
                $fix_xml[$new_key] = $value;
            }

            foreach ($ultimo_movimientos as $key => $valor) {
                //busco en el xml los aid
                if (array_key_exists('actividad'.$valor['aid'], $fix_xml ) ) {
                    $update = FALSE;
                    $aid                 = $valor['aid'];
                    $estado_psi          = $valor['estado_ofsc'];
                    $resource_id_psi     = $valor['resource_id'];
                    $fecha_agenda_psi = $valor['fecha_agenda'];

                    $estado_ofsc_xml     = (String)$fix_xml['actividad'.$aid]['status'];
                    $resource_id_xml     = $fix_xml['actividad'.$aid]['resource_id'];
                    $time_of_booking_xml = (isset($fix_xml['actividad'.$aid]['time_of_booking']))?$fix_xml['actividad'.$aid]['time_of_booking']:'';
                    $slot_xml            = (isset($fix_xml['actividad'.$aid]['time_slot']))?$fix_xml['actividad'.$aid]['time_slot']:'';
                    $fecha_agenda_xml    = date("Y-m-d", strtotime($time_of_booking_xml));

                    //validacion estado_ofsc_id
                    if( $estado_ofsc_xml != $estado_psi) {
                        //estado no existe en PSI
                        if ($estado_ofsc_xml == 'deleted') {
                            $no++; continue;
                        }

                        $estado_ofsc_id = EstadoOfsc::select('id')
                                          ->where('name',$estado_ofsc_xml)
                                          ->get();
                        //estado_ofsc_id
                        if (count($estado_ofsc_id) > 0) {
                            $valor['estado_ofsc_id'] = $estado_ofsc_id->first()->id;
                            $update = TRUE;
                        } else {
                            print('Estado "'.$estado_ofsc_xml.'" no encontrado.');
                            $err ++;
                            continue;
                        }
                    } //end validacion estado

                    //validacion resource_id
                    if ( $resource_id_xml != $resource_id_psi) {
                        $valor['resource_id'] = $resource_id_xml;
                        $update = TRUE;
                    }

                    //validacion fecha_agenda
                    if ( $fecha_agenda_xml != $fecha_agenda_psi) {
                        $valor['fecha_agenda'] = $fecha_agenda_xml;
                        $update = TRUE;
                    }

                    //update
                    if ($update) {
                        $valor['noajax'] = true;
                        $valor['estado'] = $valor['estado_id'];
                        $valor['motivo'] = $valor['motivo_id'];
                        
                        //time_slot y horario_id
                        if (! empty($slot_xml)) {
                            $valor['slot'] = $slot_xml;
                            $valor['horario_id'] = ($slot_xml == 'AM')?51:52;
                        } else {
                            $slot = date("H", strtotime($time_of_booking_xml));
                            $valor['slot'] = ($slot < 12)?'AM':'PM';
                            $valor['horario_id'] = ($slot < 12)?51:52;
                        }

                        Input::replace($valor);
                        $r = $this->postRegistrar();
                        if ($r['rst'] == 1) array_push($con, $aid); //arreglo con los aid actualizados
                        if ($r['rst'] == 2) $err ++; //errores del modelo
                        //print_r($r); 
                        //if (count($con) == 1) break;
                    } else $no++; 
                    
                }//end array_key_exists
            }//end foreach
            print_r($con);
            print(' RESUMEN:'.count($con));
            print(' actualizaciones,  ');
            print($no.' no actualizadas,  ');
            print($err.' errores');
        } catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            return  'Ocurrio una error durante el proceso.';
        }
    }
}
