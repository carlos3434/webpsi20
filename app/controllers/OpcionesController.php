<?php
use Illuminate\Support\Collection;
class OpcionesController extends \BaseController {
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */
    //Funcion para cargar los datos en el datatable
    public function postCargar(){

        $order = ["column" => Input::get('column'),"dir" => Input::get('dir')];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        /*consulta*/
        $select=['o.id',"o.nombre as nombre",'o.created_at as fecha_creacion',
        DB::raw("IF(o.estado=1,'Activo','Inactivo') AS estado"),
        DB::raw("UPPER(to.nombre) as tipo"),'o.tipo as idtipo'];

        $data2= DB::table('opcion as o')
        ->join('tipo_opcion as to','o.tipo','=','to.id')
        ->select($select);

        $column = "id";
        $dir = "desc";

        $data2 = $data2
                ->orderBy($column, $dir)
                ->paginate($grilla["perPage"]);
        
        $data = $data2->toArray();
        
        $col = new Collection([
            'recordsTotal'=> $data2->getTotal(),
            'recordsFiltered'=> $data2->getTotal(),
        ]);
        
        return $col->merge($data);
    }

    public function postEstado()
    {
        $status=false;$msj="";
        $id=Input::get('id');
        $estado=Input::get('estado');
        $tipo=Input::get('tipo');

        try {
                if($tipo=='opcion'){
                    $update=DB::update("UPDATE opcion SET estado=".$estado." 
                    WHERE id=".$id);
                }

                if($tipo=='criterio'){
                    $update=DB::update("UPDATE criterios SET estado=".$estado." 
                    WHERE id=".$id);
                }
            
            $status=true;
            $msj="Estado actualizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$status=false;}

        
        return Response::json(array('status'=>$status,'msj'=>$msj,'tipo'=>$tipo));
    }

    public function postListar(){

        $input=Input::get('input');
        if($input=='tipoOpc'){
            $data=DB::select("SELECT id,nombre FROM tipo_opcion WHERE estado=1;");
        }

        if($input=='criterio'){
            $data=DB::select("SELECT id,nombre FROM criterios WHERE estado=1;");
        }

        return Response::json(["rst" => 1, "datos" => $data]);
    }

    public function postGuardaropciones(){

        $estado=false;$msj="";
        $nombre=Input::get('nombreOpc');
        $tipo=Input::get('tipoOpc');

        try {
                $insert=DB::insert("INSERT opcion(nombre,tipo,estado,created_at) values(?,?,1,NOW())",[$nombre,$tipo]);
                $estado=true;
                $msj="Registro realizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }

    public function postModificaropciones(){

        $estado=false;$msj="";
        $id=Input::get('opcionId');
        $nombre=Input::get('nombreOpc');
        $tipo=Input::get('tipoOpc');

        try {
                $update=DB::update("UPDATE opcion set nombre='".$nombre."',tipo='".$tipo."',updated_at=NOW()
                WHERE id=".$id);
                $estado=true;
                $msj="Registro actualizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }

    public function postCargarcriterios(){
        /*consulta*/
        $id=Input::get('opcionId_Crt');
        
        $data=DB::select("SELECT c.id,oc.id as idCrtOpc,o.nombre AS opcion,c.nombre AS criterio,
        IFNULL(c.nombre_gestion,'') AS campo,oc.valor,
        IFNULL(c.created_at,'') AS fecha_creacion,
        IF(c.estado=1,'Activo','Inactivo') AS estado,c.estado as idestado
        FROM opcion as o
        INNER JOIN opcion_criterio as oc on o.id=oc.opcion_id
        INNER JOIN criterios as c on c.id=oc.criterio_id
        where o.id=".$id);
       
        return Response::json(array('criterios'=>$data));
    }

    public function postCargarcamposcriterio(){
        /*consulta*/
        $id=Input::get('criterioCrt');
        $idCriterio=0;
        if($id==''){$idCriterio=0;}else{$idCriterio=$id;}
        $data=DB::select("SELECT * FROM criterios where id=".$idCriterio);
        return Response::json(array('criterios'=>$data));
    }

    public function postModificarvalor(){

        $estado=false;$msj="";
        //$idOpcion=Input::get('nombreOpc');
        $id=Input::get('Id_Crt_Opc');
        $valor=Input::get('valorCrt');

        try {
                $update=DB::update("UPDATE opcion_criterio set valor='".$valor."' WHERE id=".$id);
                $estado=true;
                $msj="Registro actualizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }

    public function postGuardarvalor(){

        $estado=false;$msj="";
        $opcion=Input::get('opcionId_Crt');
        $criterio=Input::get('criterioCrt');
        $valor=Input::get('valorCrt');

        try {
                $update=DB::update("INSERT opcion_criterio(opcion_id,criterio_id,valor) values(?,?,?)",
                [$opcion,$criterio,$valor]);
                $estado=true;
                $msj="Registro realizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }

    public function postCargarcriterio(){
        $order = ["column" => Input::get('column'),"dir" => Input::get('dir')];
        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        /*consulta*/
        $select=['c.id','c.nombre',
        DB::raw("IFNULL(c.nombre_gestion,'') AS campo"),'c.created_at as fecha_creacion',
        DB::raw("IF(c.estado=1,'Activo','Inactivo') AS estado")];

        $data2=  DB::table('criterios as c')->select($select);//Criterios::select($select);
        
        $column = "id";
        $dir = "desc";

        $data2 = $data2
                ->orderBy($column, $dir)
                ->paginate($grilla["perPage"]);

        $data = $data2->toArray();
        
        $col = new Collection([
            'recordsTotal'=> $data2->getTotal(),
            'recordsFiltered'=> $data2->getTotal(),
        ]);
        
        return $col->merge($data);
    }

    public function postGuardarcriterio(){

        $estado=false;$msj="";
        $nombre=Input::get('nombreCrit');
        $criterio=Input::get('campoCrit');

        try {
                $update=DB::update("INSERT criterios(nombre,nombre_gestion,estado,created_at) values(?,?,1,NOW())",
                [$nombre,$criterio]);
                $estado=true;
                $msj="Registro realizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }

    public function postModificarcriterio(){

        $estado=false;$msj="";
        //$idOpcion=Input::get('nombreOpc');
        $id=Input::get('criterioId');
        $nombre=Input::get('nombreCrit');
        $campo=Input::get('campoCrit');

        try {
                $update=DB::update("UPDATE criterios set nombre='".$nombre."',nombre_gestion='".$campo."',
                updated_at=NOW() WHERE id=".$id);
                $estado=true;
                $msj="Registro actualizado correctamente!";

        }catch(\Illuminate\Database\QueryException $ex){ $msj=$ex->getMessage();$estado=false;}
        
        return Response::json(array('estado'=>$estado,'msj'=>$msj));
    }


}

