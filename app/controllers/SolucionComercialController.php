<?php

class SolucionComercialController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
    }

    public function postListar()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $solucion = Cache::get('listarSolucionBandeja');
            if ( $solucion ) {
                $solucion = Cache::get('listarSolucionBandeja');
            } else {
                $solucion = Cache::remember('listarSolucionBandeja', 20, function(){
                    $datos = DB::table('soluciones_comerciales')
                            ->select('id', 'nombre')
                            ->where('estado', '=', 1)
                            ->orderBy('nombre')
                            ->get();

                    return $datos;
                });
            }
        } else {
            $solucion = DB::table('soluciones_comerciales')
                    ->select('id', 'nombre')
                    ->where('estado', '=', 1)
                    ->orderBy('nombre')
                    ->get();
        }

        return     Response::json(
            array(
                'rst'=>1,
                'datos'=>$solucion
            )
        );
    }

}
