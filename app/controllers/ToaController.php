<?php
use Ofsc\Activity;

class ToaController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
    }

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    public function postValidagestion()
    {
        if (Request::ajax()) {
            $permiso=Toa::ValidaPermiso();
            return Response::json(
                array(
                    'rst'=>1,
                    'permiso'=>$permiso
                )
            );
        }
    }

    /**
     * Obtiene imagenes enviadas por técnicos
     * aid, nimg, label, label2
     * - bandejamodal.detalle_ofsc_inicio: 
     *  aid=$aid&nimg=2&label=XA_START_PIC
     * - bandejamodal.detalle_ofsc_completo: 
     *  aid=$aid&nimg=8&label=XA_DONE_PIC_ADD&label2=XA_DONE_PIC
     *  La carpeta "public/images/historico" debe existir
     * @return type
     */
    public function postGetimage()
    {
        if(Input::has('toa')) {
            $response = $this->postGetimageOnlyForToa();
            return $response;
        } else if(!Input::has('label')) {
            return 'No existe label';
        }
        
        $activity = new Activity();

        $img = "";
        $binFile = array();
        $valFile = array();
        $url = 'images/historico/320x240/';
        $ext = '.jpg';
        $templateImg = '<div class="clearfix"></div><div class="col-md-12"><br>'
            . '<img alt="%1$s%3$s" src="%2$s%1$s" /></div>';
        
        if (Input::has('label2')) {
            $fn = Input::get('aid') . "-" . Input::get('label2') . $ext;
            $valFile[$fn] = Helpers::validateImagen('historico', $fn);
            if ($valFile[$fn]['rst'] == 2) {
                $response = $activity->getFile(
                    Input::get('aid'), Input::get('label2')
                );
                if (isset($response->result_code) 
                    && $response->result_code == 0 ) {
                    foreach  ($response->file_data as $val) {
                        $binFile[$fn] = $val;
                    }
                }    
            } else {
                $img =  sprintf($templateImg, $fn, $url, ' Fotografía trabajo');
            }
        }

        $aImage = Input::has('repo')? json_decode(Input::get('repo')) : [];
        foreach($aImage as $label) {
            if(strpos($label, Input::get('label')) !== false) {
                // si lo encuentra
                $fn = Input::get('aid') . "-" . $label . $ext;
                $valFile[$fn] = Helpers::validateImagen('historico', $fn);
                if ($valFile[$fn]['rst'] == 2) {
                    $response = $activity->getFile(Input::get('aid'), $label);    
                } else {
                    $img .=  sprintf($templateImg, $fn, $url, ' (*)');
                }
                if (isset($response->result_code) 
                    && $response->result_code == 0) {
                    $binFile[$fn] = implode('', ((array) $response->file_data));
                }
            }
        }
/*  
        for ($i=1; $i<=Input::get('nimg'); $i++) {
            $fn = Input::get('aid') . "-" 
                . Input::get('label') . "_" . $i . $ext;
            $valFile[$fn] = Helpers::validateImagen('historico', $fn);
            if ($valFile[$fn]['rst'] == 2) {
                $response = $activity->getFile(
                    Input::get('aid'), Input::get('label') . "_" . $i
                );
                if (isset($response->result_code) 
                    && $response->result_code == 0 ) {
                    foreach ($response->file_data as $val) {
//                        $img .= "<img src=\"data:image/jpeg;base64,"
//                            . base64_encode($val) ."\" />";
                        $binFile[$fn] = $val;
                    }
                }
            } else {
                $img .=  sprintf($templateImg, $fn, $url, ' (*)');
            }
        }
*/
        if(count($binFile) > 0) {
            foreach($binFile as $key => $val) {
                // la cadena no debe contener 'data:image/jpeg;base64,' y los
                // espacios en blanco ' ' cambiar por '+' [str_replace]
                //$data = $val; //base64_decode($val);
                $file = $valFile[$key]['aPath'];
                $success = file_put_contents($file, $val);
                $img .= $success === false ? 
                    "Unable to save the file in the repository: $key. " :
                    sprintf($templateImg, $key, $url, '');                
//                if ($success) {
//                    $explode = explode('-', $key);
//                    $a['codactu'] = $explode[0];
//                    $a['label'] = $explode[1];
//                    $a['imagen'] = $key;
//                    $success = DB::table("gestiones_imagen")->insert($a);
//                }
            }
        }
        if (strlen($img) == 0) {
            $img = '<img alt="ni toa ni local" src="img/no_image_256x256.png">';
        }

        return Response::json(
            array(
                'rst'=>1,
                'img'=>$img
            )
        );
    }
    
    /**
     * Obtiene imagenes enviadas por técnicos, usando TOA
     * @return json
     */
    public function postGetimageOnlyForToa()
    {
        $activity = new Activity();

        $img = "<img src=\"img/no_image_256x256.png\">";

        if(Input::has('label2')){
            $response = $activity->getFile(
                Input::get('aid'), Input::get('label2'));
            if (isset($response->result_code) && $response->result_code == 0 ) {
                foreach ($response->file_data as $val) {
                    $img = "<img title='Fotografía trabajo' "
                        . "src=\"data:image/jpeg;base64,"
                        . base64_encode($val) ."\" /><br><br>";
                }
            }
        }

        for ($i=1; $i<=Input::get('nimg'); $i++) {
            $response = $activity->getFile(
                Input::get('aid'), Input::get('label') . "_" . $i
            );

            if (isset($response->result_code) && $response->result_code == 0 ) {
                $img = str_replace(
                    "<img src=\"img/no_image_256x256.png\">",
                    "",
                    $img
                );
                foreach ($response->file_data as $val) {
                    $img .= "<img src=\"data:image/jpeg;base64,"
                        . base64_encode($val) ."\" />";
                }
            }
        }    
        
        return Response::json(
            array(
                'rst'=>1,
                'img'=>$img
            )
        );
    }

    /**
     * Eliminamos el codactu que se encuentra en Redis (Se da cuando se cierra
     * el modal)
     * POST toa/quitarvalidacion
     *
     * @return Response
     */
    public function postQuitarvalidacion()
    {
        if (Request::ajax()) {
            $redis  = Redis::connection();
            try {
                Redis::ping();
                Redis::select(1);
            } catch (Exception $e) {}
            if ( !isset($e) ) {
                $codactu = Input::get('codactu');
                $toaPermiso = Input::get('permiso');
                if (Redis::get($codactu)) {
                    if ($toaPermiso == "1" || $toaPermiso == "0") {
                        Redis::del($codactu);
                    }
                }
                return Response::json(
                    array(
                        'rst'=>1
                    )
                );
            }
            return Response::json(
                array(
                    'rst'=>1
                )
            );
        }
    }

}
