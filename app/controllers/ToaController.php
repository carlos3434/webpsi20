<?php
use Ofsc\Activity;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Configuracion\models\Parametro;

class ToaController extends BaseController
{

    public function __construct()
    {
        //$this->beforeFilter('auth'); // bloqueo de acceso
    }

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    public function postValidagestion()
    {
        //if (Request::ajax()) {
        $codactu=Input::get('codactu');
        $rst = Gestion::getActuacion($codactu);
        if (!isset($rst[0])) {
                return Response::json(
                    array(
                        'rst'=>1,
                        'permiso'=>0
                    )
                );
        }
        $razon='';
            
        $actuacion = new Actuacion($rst[0]);
        if ($actuacion->validarContrata()==false) {
            $razon=$actuacion->contrata;
        } elseif ($actuacion->validarPAI()==false) {
            $razon=$actuacion->area2;
        } elseif ($actuacion->validarProvPendienteCatv()==false) {
            $razon='no es pendiente';
        } elseif ($actuacion->validarLiquidada()==false) {
            $razon='liquidada';
        }
            //return Response::json($razon);
            $actividad =(array) $rst[0];
        try {
            list(,$troba) = explode('|', $actividad['fftt']);
            $actividad['cod_troba'] = $troba;
        } catch (Exception $e) {

        }

            $array['tipo']=2; //envio de psi a toa
            $array['estado']=1; //activo
            $array['proveniencia']=1; //1: gestión
            //$array['campo']='nombre_temporal';

            $parametros = Parametro::getParametroValidacion($array);
            $permiso = 0;
        foreach ($parametros as $key => $value) {
                $valida=true;
                $trama=explode("||", $value->trama);
                $detalle=explode("||", $value->detalle);

            for ($i=0; $i <count($trama); $i++) {
                    $subdetalle=explode(",", $detalle[$i]);
                if ($trama[$i]!='') {
                    $indice = array_search($actividad[$trama[$i]], $subdetalle, false);
                    if ($indice===false) {
                            $valida=false;
                            break;
                    }
                }
            }
            if ($valida===true) {
                    $permiso = 1;
                    break;
            }
        }

        if (Redis::get($codactu)) {
                $fechaActual = strtotime(date("Y-m-d H:i:s"));
                $contenidoArray = explode("|", Redis::get($codactu));
                $usuario = Usuario::find($contenidoArray[1]);
                $nomUsuario = $usuario->nombre." ".$usuario->apellido;
                $permiso = 'view'."|".$nomUsuario."|".$permiso;
        } else {
                $contenido = date("Y-m-d H:i:s")."|".Auth::id();
        }

            return Response::json(
                array(
                    'rst'=>1,
                    'permiso'=>$permiso,
                    'razon'=>$razon
                )
            );
       // }
    }

    public function postValidagestionst()
    {
        $timeexpire = 600; // en segundos
        $proveniencia=2; //CMS
        if (Request::ajax()) {

            if (Input::has("solicitud")) {
                $actividad = Input::get("solicitud");
                //$codactu = $actividad["num_requerimiento"];
                $idSolicitudTecnica = $actividad["id_solicitud_tecnica"];
                $tipoLegado = $actividad["tipo_legado"];
                $actividad["mdf"] = $actividad["cod_nodo"];
                $solicitudTecnicaId = $actividad["solicitud_tecnica_id"];
                /*
                if ($tipoLegado == 1) {
                    $empresa = Empresa::where("cms", "=", $actividad["codigo_contrata"])->first();
                    if (!is_null($empresa)) {
                        $actividad["empresa_id"] = $empresa->id;
                    } else {
                        $actividad["empresa_id"] = 0;
                    }
                } else {
                    if ($actividad['actividad_id']==2) {
                        $proveniencia=3;
                    } else {
                        $proveniencia=4;
                    }
                }
                $quiebre = Quiebre::find($actividad["quiebre_id"]);
                if (!is_null($quiebre)) {
                    $quiebre_grupo_id = $quiebre->quiebre_grupo_id;
                } else {
                    $quiebre_grupo_id = 0;
                }
                $actividad["quiebre_grupo_id"] = $quiebre_grupo_id;

                $array['tipo']=2; //envio de psi a toa
                $array['estado']=1; //activo
                $array['proveniencia']=$proveniencia; //cms, gest ave, gest prov

                $parametros = Parametro::getParametroValidacion($array);
                $permiso = false;
                foreach ($parametros as $key => $value) {
                    $valida=true;
                    $trama=explode("||", $value->trama);
                    $detalle=explode("||", $value->detalle);

                    for ($i=0; $i <count($trama); $i++) {
                        $subdetalle=explode(",", $detalle[$i]);
                        if ($trama[$i]!='') {
                            $indice = array_search($actividad[$trama[$i]], $subdetalle, false) ;
                            if ($indice===false) {
                                $valida=false;
                                break;
                            }
                        }
                    }
                    if ($valida===true) {
                        $permiso = true;
                        break;
                    }
                }
                $request['parametrizacion'] = $permiso; */
                $objultimo = Ultimo::where(["solicitud_tecnica_id" => $solicitudTecnicaId])->first();
                $idusuariogestionando = Auth::id();
                if (Redis::get($idSolicitudTecnica)) {
                    $acceso = json_decode(Redis::get($idSolicitudTecnica), true);
                    $request['gestionando'] = false;
                    if (Auth::id() != $acceso["idusuario"]) {
                        $request['nombreusuario'] = $acceso["nombreusuario"];
                        $request['gestionando'] = true;
                        $idusuariogestionando = $acceso["idusuario"];
                    }
                } else {
                    $request['gestionando'] = false;
                    $acceso = [
                        "hora"            => date("Y-m-d H:i:s"),
                        "idusuario"       => Auth::id(),
                        "nombreusuario"   => Session::get('full_name')
                    ];
                    Redis::lRem('solicitudes', 0, $idSolicitudTecnica);
                    Redis::lpush('solicitudes', $idSolicitudTecnica);//60 segundos
                    Redis::set($idSolicitudTecnica, json_encode($acceso));
                    Redis::expire($idSolicitudTecnica, $timeexpire);
                }
                try {
                    DB::table("solicitud_tecnica_ultimo")->where(["solicitud_tecnica_id" => $solicitudTecnicaId])->update(["usuario_gestionando" => $idusuariogestionando]);
                } catch (Exception $e) {
                    return Response::json(
                        array(
                            'rst'=> 2,
                            'acceso'=>$request,
                            'msj' => "ERROR en BD : Error al Guardar Usuario para Gestion"
                        )
                    );
                }

                return Response::json(
                    array(
                        'rst'=>1,
                        'acceso'=>$request
                    )
                );
            } else {

            }
        }
    }
    public function postValidaparametrizacion()
    {
        $timeexpire = 300; // en segundos
        $proveniencia=2; // CMS
        if (Request::ajax()) {

            if (Input::has("solicitud")) {
                $actividad = Input::get("solicitud");
                //$codactu = $actividad["num_requerimiento"];
                $idSolicitudTecnica = $actividad["id_solicitud_tecnica"];
                $tipoLegado = $actividad["tipo_legado"];
                $actividad["mdf"] = $actividad["cod_nodo"];
                if ($tipoLegado == 1) {
                    $empresa = Empresa::where("cms", "=", $actividad["codigo_contrata"])->first();
                    if (!is_null($empresa)) {
                        $actividad["empresa_id"] = $empresa->id;
                    } else {
                        $actividad["empresa_id"] = 0;
                    }
                } else {
                    if ($actividad['actividad_id']==2) {
                        $proveniencia=3;
                    } else {
                        $proveniencia=4;
                    }
                }
                $quiebre = Quiebre::find($actividad["quiebre_id"]);
                if (!is_null($quiebre)) {
                    $quiebre_grupo_id = $quiebre->quiebre_grupo_id;
                } else {
                    $quiebre_grupo_id = 0;
                }
                $actividad["quiebre_grupo_id"] = $quiebre_grupo_id;

                $array['tipo']=2; //envio de psi a toa
                $array['estado']=1; //activo
                $array['proveniencia']=$proveniencia; //cms, gest ave, gest prov

                $parametros = Parametro::getParametroValidacion($array);
                $permiso = false;
                if (count($parametros) > 0) {
                    foreach ($parametros as $key => $value) {
                        $valida=true;
                        $trama=explode("||", $value->trama);
                        $detalle=explode("||", $value->detalle);

                        for ($i=0; $i <count($trama); $i++) {
                            $subdetalle=explode(",", $detalle[$i]);
                            if ($trama[$i]!='') {
                                $indice = array_search($actividad[$trama[$i]], $subdetalle, false) ;
                                if ($indice===false) {
                                    $valida=false;
                                    break;
                                }
                            }
                        }
                        if ($valida===true) {
                            $permiso = true;
                            break;
                        }
                    }
                } else {
                    $permiso = true;
                }
                
                $request['parametrizacion'] = $permiso;

                return Response::json(
                    array(
                        'rst'=>1,
                        'acceso'=>$request
                    )
                );
            } else {

            }
        }
    }
    /**
     * Obtiene imagenes enviadas por técnicos
     * aid, nimg, label, label2, toa
     * aid, repo, label,
     * - bandejamodal.detalle_ofsc_inicio:
     *  aid=$aid&nimg=2&label=XA_START_PIC
     * - bandejamodal.detalle_ofsc_completo:
     *  aid=$aid&nimg=8&label=XA_DONE_PIC_ADD&label2=XA_DONE_PIC
     *  La carpeta "public/images/historico" debe existir
     * @return type
     */
    public function postGetimage()
    {
        $reglas = array(
            'aid' => 'Required|Integer',
        );
        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'Integer'       => ':attribute Solo debe ser entero',
        );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);

        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                'img'=> '<img alt="Null o indefinido" src="img/no_image_256x256.png">
                    <p class="text-center">El ID actividad ' . Input::get('aid') . '</p>'
                )
            );
        }

        if (Input::has('toa') && Input::has('label') && Input::has('nimg')) {
            // consulta directamente a TOA
            $response = $this->postGetimageOnlyForToa();
            return $response;
        }

        $aid = Input::get('aid');
        $aImageFromToa = Input::has('repo')? (array) json_decode(Input::get('repo')) : [];
        $aResult = [];
        $aResponse = [];

        $ext = '.jpg';
        $url = 'images/historico/320x240/';
        $templateImg = '<div class="clearfix"></div><div class="col-md-12"><br>'
            . '<img alt="%1$s%3$s" src="%2$s%1$s" /></div>';
        $path = public_path("img/historico/");
        $label = Input::get('label');
        $aListaTotal = \Config::get("ofsc.image.{$label}");

        // Consulta fotos en el repositorio
        if (Input::has('label') && Input::has('aid')
            && count($aImageFromToa) > 0) {
            $aResult = array_intersect_assoc($aImageFromToa, $aListaTotal);

            foreach ($aResult as $key => $val) {
                $filename = "{$aid}-{$key}{$ext}";
                if (File::exists($path . $filename)) {
                    $aResponse[] = sprintf(
                        $templateImg,
                        $filename,
                        $url,
                        " Imagen {$val}"
                    );
                    unset($aResult[$key]);
                }
            }
        }

        // Consulta fotos en TOA
        $activity = new Activity();

        foreach ($aResult as $llave => $etiqueta) {
            $filename = "{$aid}-{$llave}{$ext}";
            $response = $activity->getFile($aid, $llave);
            if (isset($response->result_code)
                && $response->result_code == 0) {
                $binFile = implode('', ((array) $response->file_data));
                $success = file_put_contents($path . $filename, $binFile);
                $aResponse[] = $success === false ?
                    "Unable to save the file in the repository: $filename. " :
                    sprintf($templateImg, $filename, $url, ' desde TOA');
            }
        }
            
        // Consulta fotos que solo existen en local
        if (count($aResponse) == 0) {
            foreach ($aListaTotal as $k => $v) {
                $filename = "{$aid}-{$k}{$ext}";
                if (File::exists($path . $filename)) {
                    $aResponse[] = sprintf(
                        $templateImg,
                        $filename,
                        $url,
                        " Solo existe en Local {$val}"
                    );
                }
            }
        }

        if (count($aResponse) > 0) {
            $img =  implode(" ", $aResponse);
            $rst = 1;
        } else {
            $img = '<img alt="ni toa ni local" src="img/no_image_256x256.png">';
            $rst = 2;
        }

        return Response::json(
            array(
                'rst'=>$rst,
                'img'=>$img
            )
        );
    }
    
    /**
     * Obtiene imagenes enviadas por técnicos, usando TOA
     * label, label2, aid, nimg
     * @return json
     */
    public function postGetimageOnlyForToa()
    {
        $activity = new Activity();

        $img = "<img src=\"img/no_image_256x256.png\">";

        if (Input::has('label2')) {
            $response = $activity->getFile(
                Input::get('aid'),
                Input::get('label2')
            );
            if (isset($response->result_code) && $response->result_code == 0) {
                foreach ($response->file_data as $val) {
                    $img = "<img title='Fotografía trabajo' "
                        . "src=\"data:image/jpeg;base64,"
                        . base64_encode($val) ."\" /><br><br>";
                }
            }
        }

        for ($i=1; $i<=Input::get('nimg'); $i++) {
            $response = $activity->getFile(
                Input::get('aid'),
                Input::get('label')."_".$i
            );

            if (isset($response->result_code) && $response->result_code == 0) {
                $img = str_replace(
                    "<img src=\"img/no_image_256x256.png\">",
                    "",
                    $img
                );
                foreach ($response->file_data as $val) {
                    $img .= "<img src=\"data:image/jpeg;base64,"
                        . base64_encode($val) ."\" />";
                }
            }
        }
        
        return Response::json(
            array(
                'rst'=>1,
                'img'=>$img
            )
        );
    }

    /**
     * Eliminamos el codactu que se encuentra en Redis (Se da cuando se cierra
     * el modal)
     * POST toa/quitarvalidacion
     *
     * @return Response
     */
    public function postQuitarvalidacion()
    {
        $timeexpire = 600;
        if (Request::ajax()) {
            
            $codactu = Input::get('codactu');
            //$toaPermiso = Input::get('permiso');
            if (Redis::get($codactu)) {
                $accesos = json_decode(Redis::get($codactu), true);
                $tiempocreacion = strtotime($accesos["hora"]);
                $tiempoahora = strtotime(date("Y-m-d H:i:s"));
                $diferencia = $tiempoahora - $tiempocreacion;
                //dd($tiempoahora - $tiempocreacion);

                if ($diferencia <=$timeexpire) {
                    if (Auth::id() == $accesos["idusuario"]) {
                        Redis::del($codactu);
                    }
                } else {
                    Redis::del($codactu);
                }
            }
            return Response::json(
                array(
                    'rst'=>1
                )
            );
        }
    }
        /**
     * Eliminamos el codactu que se encuentra en Redis (Se da cuando se cierra
     * el modal)
     * POST toa/quitarvalidacion
     *
     * @return Response
     */
    public function postQuitarvalidacionst()
    {
        $timeexpire = 600;
        if (Request::ajax()) {
            
            $idSolicitudTecnica = Input::get('id_solicitud_tecnica');
            $solicitudTecnicaId = Input::get("solicitud_tecnica_id");
            //$toaPermiso = Input::get('permiso');
            if (Redis::get($idSolicitudTecnica)) {
                $accesos = json_decode(Redis::get($idSolicitudTecnica), true);
                $tiempocreacion = strtotime($accesos["hora"]);
                $tiempoahora = strtotime(date("Y-m-d H:i:s"));
                $diferencia = $tiempoahora - $tiempocreacion;

                if ($diferencia <=$timeexpire) {
                    if (Auth::id() == $accesos["idusuario"]) {
                        Redis::del($idSolicitudTecnica);
                        Redis::lRem('solicitudes', 0, $idSolicitudTecnica);
                        DB::table("solicitud_tecnica_ultimo")->where(["solicitud_tecnica_id" => $solicitudTecnicaId])->update(["usuario_gestionando" => null]);
                    }
                } else {
                    Redis::del($idSolicitudTecnica);
                    Redis::lRem('solicitudes', 0, $idSolicitudTecnica);
                    DB::table("solicitud_tecnica_ultimo")->where(["solicitud_tecnica_id" => $solicitudTecnicaId])->update(["usuario_gestionando" => null]);
                }
            }
            $solicitudes = Redis::lrange('solicitudes', 0, 10);
            $datos=[];
            foreach ($solicitudes as $key => $value) {
                $datos[]= ['id'=>$key,'nombre'=>$value];
            }
            return Response::json(
                array(
                    'rst'=>1,
                    'solicitudes'=>$datos
                )
            );
        }
    }
}
