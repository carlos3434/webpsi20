<?php

use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;
use Legados\Repositories\StMensajeRepository as StMensaje;

class OutboundController extends \BaseController
{
    public function getServer()
    {
        function isTimestampIsoValid($timestamp)
        {
            if (preg_match(
                '/^'.
                '(\d{4})-(\d{2})-(\d{2})T'. // YYYY-MM-DDT ex: 2014-01-01T
                '(\d{2}):(\d{2}):(\d{2})'.  // HH-MM-SS  ex: 17:00:00
                '(Z|((-|\+)\d{2}:\d{2})|((-|\+)\d{2}\d{2}))'.  // Z or +01:00 or -01:00
                '$/',
                $timestamp,
                $parts
            ) == true) {
                try {
                    new \DateTime($timestamp);
                    return true;
                } catch (\Exception $e) {
                    return false;
                }
            } else {
                return false;
            }
        }
        function validarUsuario($user)
        {
            if (!isset($user->now) || !isset($user->login) ||
                 !isset($user->company) || !isset($user->auth_string) ) {
                Log::info('Log validarUsuario', array('user' => $user));
                return "is not authorized";
            }
            $toaNow = $user->now;
            //$toaNow = "2011-07-07T09:25:02+00:00";
            $psiNow= date("c");
            $login = $user->login;
            $company = $user->company;
            $authString = $user->auth_string;
            /*validar now*/
            if (!isTimestampIsoValid($toaNow)) {
                Log::info('Log isTimestampIsoValid', array('toaNow' => $toaNow));
                return "Parameter now is different of ISO 8601 format";
            }
            //diferencia de hora   $psiNow  $toaNow
            $datetimePsi = new DateTime($psiNow);
            //$datetimePsi->add(new DateInterval('PT5H'));
            //considerar la diferencia de hora quellega
            $datetimeToa = new DateTime($toaNow);
            $datetimeToa->sub(new DateInterval('PT5H'));

            $segPsi = strtotime($datetimePsi->format('Y-m-d H:i:s'));
            $segToa = strtotime($datetimeToa->format('Y-m-d H:i:s'));
            $diferencia = abs($segPsi-$segToa);

            if ($diferencia > (int) \Config::get("ofsc.outbound.diferenciaSegundos")) {
                Log::info('Log diferencia', array('diferencia' => $diferencia));
                return "the current time on the server and this difference exceeds";
            }
            /*validar company*/
            if ($company!= \Config::get("ofsc.outbound.company")) {
                Log::info('Log company', array('company' => $company));
                return "cannot be found in ETAdirect";
            }
            /*validar login acceso*/  // \Config::get("ofsc.outbound.login")
            if ($login!= \Config::get("ofsc.outbound.login")) {
                Log::info('Log login', array('login' => $login));
                return "cannot be found for this company";
            }
            /*validar login autorizacion*/
            $sendMessage =   \Config::get("ofsc.outbound.authorized.send_message");
            if ($sendMessage==false) {
                Log::info('Log sendMessage', array('sendMessage' => $sendMessage));
                return "is not authorized to use send_message method";
            }
            /*validar auth_string*/
            $authStringNow = md5($toaNow. md5(\Config::get("ofsc.outbound.pass")));

            if ($authString != $authStringNow) {
                Log::info('Log authString', array('authString' => $authString));
                //return "is not authorized".$authStringNow;
            }
            return 1;
        }
        function send_message($request)
        {
            $validacion="is not authorized";
            date_default_timezone_set('America/Lima');
            $horaLlegada = date('Y-m-d G:i:s');

            if (isset($request->user)) {
                $validacion = validarUsuario($request->user);

                if ($validacion<>1) {
                    return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
                }
            } else {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $return = array();
            if (!isset($request->messages->message)) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $message = $request->messages->message;
            //validar si es array u objeto
            //cuando llega un elemento llega como objecto
            if (is_object($message)) {
                $objstmensaje = new StMensaje;
                $return = $objstmensaje->sendingToa($message, $request, $return, $horaLlegada);
                return $return;
            }
            if (is_array($message)) {
                for ($i=0; $i < count($message); $i++) {
                    $objstmensaje = new StMensaje;
                    $return = $objstmensaje->sendingToa($message[$i], $request, $return, $horaLlegada);
                }
                return $return;
            }
        }
        function drop_message($request)
        {

            //se debe abortar el proceso (Pag 14 Outbound)
            $validacion="is not authorized";
            if (isset($request->user)) {
                $validacion = validarUsuario($request->user);
                if ($validacion<>1) {
                    return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
                }
            } else {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $return = array();

            if (!isset($request->messages->message)) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $message = $request->messages->message;
            if (is_object($message)) {
                $message = array($message);
            }
            if (is_array($message)) {

                for ($i=0; $i < count($message); $i++) {
                    $messageId =  $message[$i]->message_id;
                    //borrar los mensajes que se envian
                    try {
                        $respuesta = Mensaje::drop($messageId);
                    } catch (Exception $e) {
                        $respuesta = array(
                            'code'=>"ERROR",
                            'desc'=>""//$e->getMessage()
                        );
                    }
                    $data = array(
                        'message_id' => $messageId,
                        'result' => $respuesta
                    );
                    array_push($return, $data);
                }
                return $return;
            }
        }
        function get_message_status($request)
        {
            //
            $validacion="is not authorized";
            if (isset($request->user)) {
                $validacion = validarUsuario($request->user);
                if ($validacion<>1) {
                    return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
                }
            } else {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $return = array();
            if (!isset($request->messages->message)) {
                return new SoapFault('SOAP-ENV:Client', $validacion, 'Authentication module');
            }
            $message = $request->messages->message;
            if (is_object($message)) {
                $message = array($message);
            }

            if (is_array($message)) {

                for ($i=0; $i < count($message); $i++) {
                    $messageId =  $message[$i]->message_id;
                    //borrar los mensajes que se envian
                    try {
                        $respuesta = Mensaje::getStatus($messageId);
                    } catch (Exception $e) {
                        $respuesta = array(
                            'code'=>"ERROR",
                            'desc'=>""//$e->getMessage()
                        );
                    }
                    $data = array(
                        'message_id' => $messageId,
                        'result' => $respuesta
                    );
                    array_push($return, $data);
                }
                return $return;
            }
        }
        try {
            $server = new SoapServer(Config::get('ofsc.outbound.wsdl'));
            $server->addFunction("send_message");
            $server->addFunction("drop_message");
            $server->addFunction("get_message_status");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}