<?php

use Ofsc\Capacity;

class FiltroCuposController extends BaseController
{

   
    protected $_errorController;
    protected $_uploadController;

    public function __construct(
        ErrorController $errorController,
        UploadController $uploadController
    )
    {
        $this->_errorController = $errorController;
        $this->_uploadController = $uploadController;
    }

    public function postGuardar()
    {
        if (Request::ajax()) {
            
            $filtrocupos = FiltroCupos::getGuardar();
            $rst=2;
            $msj='Ocurrio una interrupción en el proceso';
            if($filtrocupos){
                $rst=1;
                $msj='Registro realizado correctamente';
            }
            return Response::json(
                array(
                'rst' => $rst,
                'msj' => $msj,
                )
            );
        }
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
             $buckets = OfscHelper::getFileResource();
             $array_bk=array();
             $i=0;
             foreach ($buckets as $key => $value) {
                 $array_bk[$i]['id']= $value['id'];
                 $array_bk[$i]['nombre']= $value['name'];
                 $i++;
             }

            return Response::json(
                array(
                'rst' => 1,
                'datos' => $array_bk,
                )
            );
        }
    }

    public function postCargarcupo()
    {
        
        if (Request::ajax()) {
            
            $result=FiltroCupos::getBuscar();
            
            return $result;
        }
    }

    public function postCargaplana()
    {
        $upload = $this->_uploadController->postCargartmp(0, 0);

        $upload  = json_decode($upload);

        DB::table('tmp_cupos')->truncate();

        $query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE tmp_cupos FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES (bucket,horario,lunes,martes,miercoles,jueves,viernes,sabado,domingo)", addslashes($upload->file));
        
        $total=DB::connection()->getpdo()->exec($query);

        $filtrocupo = FiltroCupos::getGuardarPlano();

        unlink($upload->file);
        
        return $filtrocupo;
        
    }


}
