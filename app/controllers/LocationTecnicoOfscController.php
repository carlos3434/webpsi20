<?php
use App\Http\Requests;
use Illuminate\Http\Request;

class LocationTecnicoOfscController extends BaseController
{

    public function __construct()
    {
        /*$this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );*/
    }
    public function index()
    {
        return ResourceLocation::all();
    }

    /**
     * mostrar componente especifico
     */
    public function show($idBucket)
    {
        
        $tecnicos = DB::table("resources_locations")
        ->where("bucket", $idBucket)
        ->get();
        return json_encode($tecnicos);
    }

    public function update($id){
        $input = Input::all();

        $resource = ResourceLocation::findOrFail($id);
        $resource->latitud = $input["latitud"];
        $resource->longitud = $input["longitud"];
        $resource->save();

        return Response::json($resource); //response()->json()
    }

      public function store(Request $request)
    {
        
    }
}