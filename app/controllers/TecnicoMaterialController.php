<?php
class TecnicoMaterialController extends \BaseController
{
    public function __construct(
            UploadController $uploadController, 
            ErrorController $errorController
        )
    {
        $this->_uploadController = $uploadController;
        $this->_errorController = $errorController;
    }

    public function postCargaplana()
    {
        $rst=1;
        $msj="";

        $upload = $this->_uploadController->postCargartmp(0, 0);
        $upload  = json_decode($upload);

        $peso=filesize($upload->file);
        $ext = pathinfo($upload->file, PATHINFO_EXTENSION);

        if ($peso>42203) { 
            $rst=2;
            $msj.="El Archivo excede el limite: ". $peso. "Bytes. (Max: 92876)";
        }
        if ($ext!=="csv") { 
            $rst=2; 
            $msj.= "Solo se permite Archivos CSV";
        }

        if ($rst==2) {
            return json_encode(
                array("rst" => $rst,
                                "msj" => $msj,
                                "csv" => "")
            );
        }

        $handle = fopen($upload->file, 'r');

        $csv=array();
        
        while ( ($data = fgetcsv($handle) ) !== FALSE ) {
               
                $material=Material::where('codmat', $data[0])->first();
                $tecnico=Tecnico::where('carnet_tmp', $data[3])->first();

                if ($material!==null && $tecnico!==null && floatval($data[2])>0) {
                    $newmat=array();
                    $newmat['material_id']=$material->id;
                    $newmat['serie']=$data[1];
                    $newmat['cantidad']=$data[2];
                    $newmat['tecnico_id']=$tecnico->id;
                    $newmat['opcion']=0;
                    $newmat['estado']=1;
                    
                    if ($newmat['serie']!=="") {
                        $buscar=TecnicoMaterial::
                                  where('material_id', $newmat['material_id'])
                                ->where('serie', $newmat['serie'])
                                ->first();
                    } else {
                        $buscar=TecnicoMaterial::
                                  where('material_id', $newmat['material_id'])
                                ->where('tecnico_id', $newmat['tecnico_id'])
                                ->first();
                    }
                    if (is_null($buscar)) {
                        $result= new TecnicoMaterial($newmat);
                        $result->save();
                    } else {
                        $result=TecnicoMaterial::find($buscar->id);
                        $result->update($newmat);
                    }

                    $movimientoObj= new TecnicoMaterialMovimiento($newmat);
                    $result->movimiento()->saveMany([$movimientoObj]);
                    $result->save();

                    array_push($csv, $newmat);
                }
        }

        unlink($upload->file);

        return json_encode(
            array("rst" => 1,
                  "msj" => count($csv)." materiales registrados correctamente")
        );
    }

    public function postGuardarmaterial()
    {
        $rst=1;
        $msj="Material Registrado Correctamente";
        
        //validacion    
        if (Input::get('opcion')==1) {
            $reglas = array(
                'material_id' => 'required',
                'cantidad' => 'required|numeric'
            );
        } else {
           $reglas = array(
                'material_id' => 'required',
                'cantidad' => 'required|numeric',
                'tecnico_id' => 'required'
            ); 
        }
        $mensaje = array(
                'required'  => ':attribute es requerido',
                'unique'   => ':attribute ya está registrado',
            );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages(),
                )
            );
        }   

        $datos=Input::all();
        if (Input::get('opcion')==1) {
            if(!is_null(Auth::user()->tecnico_id)) {
                $datos['tecnico_id']=Auth::user()->tecnico_id;
            } else {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => "Su usuario no tiene Tecnico asignado",
                    )
                );
            }
        }

        if ($datos['serie']!=="") {
            $buscar=TecnicoMaterial::
                        where('material_id', $datos['material_id'])
                    ->where('serie', $datos['serie'])
                    ->first();
        } else {
            $buscar=TecnicoMaterial::
                        where('material_id', $datos['material_id'])
                    ->where('tecnico_id', $datos['tecnico_id'])
                    ->first();
        }
        if (is_null($buscar)) {
            $result= new TecnicoMaterial($datos);
            //$result->save();

            $movimientoObj= new TecnicoMaterialMovimiento($datos);
            $result->movimiento()->saveMany([$movimientoObj]);
            $result->save();
        } else {
            $rst=2;
            $msj="Material ya ha sido registrado";
        }

        return Response::json(
            array(
              'rst' => $rst,
              'msj' => $msj,
              )
        );
    }

    public function postEditarmaterial()
    {
        $rst=1;
        $msj="Material Registrado Correctamente";
        $buscar=null;
        $datos=Input::all();
        unset($datos['id']);
        
        //validacion 
        if (!Input::has('estado')) {
            if (Input::get('opcion')==1) {
                $reglas = array(
                    'material_id' => 'required',
                    'cantidad' => 'required|numeric'
                );
            } else {
                $reglas = array(
                    'material_id' => 'required',
                    'cantidad' => 'required|numeric',
                    'tecnico_id' => 'required'
                ); 
            }
        
            $mensaje = array(
                    'required'  => ':attribute es requerido',
                    'unique'   => ':attribute ya está registrado',
                );
            $validator = Validator::make(Input::all(), $reglas, $mensaje);
            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }  

            $buscar=TecnicoMaterial::
                            where('material_id', $datos['material_id'])
                        ->where('id', '<>', Input::get('id'));
                        
            if ($datos['serie']!=="") {
                $buscar->where('serie', $datos['serie']);
            } else {
                $buscar->where('tecnico_id', $datos['tecnico_id']);
            } 

            $buscar=$buscar->first();
        }

        if (is_null($buscar)) {
            $result=TecnicoMaterial::find(Input::get('id'));
            $result->update($datos);

            $movimientoObj= new TecnicoMaterialMovimiento($datos);
            $result->movimiento()->saveMany([$movimientoObj]);
            $result->save();
        } else {
            $rst=2;
            $msj="Material ya ha sido registrado";
        }
        
        return Response::json(
            array(
              'rst' => $rst,
              'msj' => $msj,
              )
        );
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            

            $materiales =TecnicoMaterial::with('tecnico')
                        ->with('material')
                        ->Join('material as m', 'm.id', '=', 'tecnico_material.material_id');
                
            if (Input::get('opcion')==1) {
                $materiales->where("tecnico_id", Auth::user()->tecnico_id);
            } else {
                $materiales->where("usuario_created_at", Auth::user()->id);
            }

            if (Input::get('tecnico')) {
                $materiales->where("tecnico_id", Input::get('tecnico'));
            }

            if (Input::get('tipmat')) {
                $materiales->where("m.tipmat", Input::get('tipmat'));
            }

            if (Input::get('material')) {
                $materiales->where("material_id", Input::get('material'));
            } 

            if (Input::get('created_at')) {
                $date=explode(" - ", Input::get('created_at'));
                if(isset($date[0]) && isset($date[1])) {
                    $materiales->whereDate("created_at",">=", $date[0]);
                    $materiales->whereDate("created_at","<=", $date[1]);
                }
            }
            
            $materiales=$materiales->select('tecnico_material.*')
                        ->get();
            

            return Response::json(
                array(
                'rst' => 1,
                'datos' => $materiales,
                )
            );
        }
    }
}