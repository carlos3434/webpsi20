<?php 

class ProgramacionTecnicoOfscController extends BaseController
{
	/*
	*	Metodo que retorna las franjas horarias (SLOTs)
	*	destinados para el bucket en mención.
	*/
	public function postCapacidadHorario()
	{

	}

	public function postProgramacion()
	{
		if (Request::ajax())
		{
			$filtros = array(
				"fecha_agenda" => Input::get("fecha_agenda"),
				"empresa" => Input::get("empresa", ""),
				"tecnico" => Input::get("tecnico", ""),
				"quiebre" => Input::get("quiebre", "")
				);
			$explode = explode(" - ", Input::get("fecha_agenda"));

			$objProgramacionTecnicoOfsc = new ProgramacionTecnicoOfsc;
			$response = $objProgramacionTecnicoOfsc->get($filtros);
			$datos = $response;
			$solotecnicos = [];
			
			foreach ($response as $key => $value) {
				if ($value["nombre_tecnico"] === null)
					unset($response[$key]);
			}
			$solotecnicos = $response;
			$ordenes_agrupado = array();
			foreach ($solotecnicos as $key => $value) {
				$ordenes_agrupado[$value["fecha_agenda"]][$value["horario_id"]][] = $value;
			}
			$cabecera = ["Técnico", "Quiebre"];
			$subcabecera = ["AM", "PM"];

			$numDias = (strtotime($explode[1]) - strtotime($explode[0])) / 86400;
			$dias = []; $htmlfiladia = [];
			for($i = 0; $i <= $numDias; $i++ )
				$dias[] = strtotime($explode[1]) - 86400*$i;
			foreach ($dias as $key => $value) {
				$dias[$key] = date("Y-m-d", $value);
				$htmlfiladia[$dias[$key]] = ["AM" => "", "PM" => ""];
			}
			return Response::json(
			                array(
			                    'rst'=>1,
			                    'msj'=>'Registro realizado correctamente',
			                    'datos' => $datos,
			                    'cabecera' => $cabecera,
			                    'subcabecera' => $subcabecera,
			                    'solotecnicos' => $solotecnicos,
			                    'dias' => $dias,
			                    'htmlfiladia' => $htmlfiladia,
			                    'ordenes_agrupado' => $ordenes_agrupado
			                )
			            );

		}
	}
}