<?php

class GestioncasuisticaController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
            
            $usuarioId = 0;
            if ( Input::get('usuario_id')!=null ) {
                $usuarioId = Input::get('usuario_id');
            }
            $datos= GestionCasuistica::Listar($usuarioId);
            return Response::json(
                $datos
            );
        }
    }

    public function postListardetalle()
    {
        if (Request::ajax()) {

            $datos= GestionCasuistica::ListarDetalle();
            return Response::json(
                $datos
            );
        }
    }

    public function postListarestado()
    {
        if (Request::ajax()) {
        
            $datos= GestionCasuistica::ListarEstado();
            return Response::json(
                $datos
            );
        }
    }
 


}//fin class
