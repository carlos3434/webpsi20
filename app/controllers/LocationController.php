<?php

use Ofsc\Location;

class LocationController extends BaseController
{
    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postEnvio()
    {
        try {
            $company = Input::get("company");
            $device = Input::get("device"); //external_id
            $x = Input::get("x");
            $y = Input::get("y");
            $time = Input::get("time");

            $location = new location();
            $response = $location->setPosition($company, $device, $x, $y, $time);

            if ( isset($response->error) && $response->error ) {
                //throw new Exception($response->detail->reason);
            } else {
                echo "OK";
            }
        } catch (Exception $error) {
            $this->error->saveError($error);
        }
    }

    public function postPosition()
    {

        $company = Input::get("company");
        $device = Input::get("device"); //external_id
        input::all();


        $location = new location();
        $response = $location->getPosition($company, $device);

        print_r($response);
    }
}