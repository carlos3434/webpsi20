<?php
//controller
class PermisoEventosController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter('csrf', ['only' => ['postEditar']]);
    }

    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $tipopersona = Input::get('tipo_persona');
            if ($tipopersona==1) {
                $permisoeventos = PermisoEventos::getCargarPersonas();
            } else {
                $permisoeventos = PermisoEventos::getCargarTecnicos();
            }
            return Response::json(array('rst'=>1,'datos'=>$permisoeventos));
        }
    }

    public function postEditar()
    {
        if ( Request::ajax() ) {

            $usurioid= Input::get('id');
            $tipopersona= Input::get('tipo_persona');

            $usuario['usuario_updated_at'] = Auth::id();
            
            $datas=array(
                    Auth::id(),
                    $usurioid,
                    $tipopersona
                    );
            $desactivardata=PermisoEventos::getDesactivarpermisos($datas);

            $metodo = Input::get('metodo');
             for ($i=0; $i<count($metodo); $i++) {

                 $metodoId = $metodo[$i];

                 $data=array(
                    $metodoId,
                    $usurioid,
                    $tipopersona,
                    '2',
                    $usuario['usuario_updated_at'],
                    $usuario['usuario_updated_at']
                    );

                 $datos=PermisoEventos::getAgregarEvento($data);
             }

             $consulta = Input::get('consulta');

             for ($i=0; $i<count($consulta); $i++) {

                 $consultaId = $consulta[$i];

                 $data=array(
                    $consultaId,
                    $usurioid,
                    $tipopersona,
                    '1',
                     $usuario['usuario_updated_at'],
                     $usuario['usuario_updated_at']
                    );

                 $datos=PermisoEventos::getAgregarEvento($data);
             }
                return Response::json(
                    array('rst'=>1,
                        'msj'=>"Se modifico permisos exitosamente"
                        )
                );

        }
    }


}
