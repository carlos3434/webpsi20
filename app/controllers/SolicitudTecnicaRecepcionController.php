<?php

class SolicitudTecnicaRecepcionController extends \BaseController {
	//use  DataViewer;
	/**
	 * Display a listing of the resource.
	 * GET /tipificacion
	 *
	 * @return Response
	 */

	public function postListar(){
		//********
	
				$fecha = Input::get("fecha");
				$formFecha = explode(',', $fecha);
				$solicitudTecnica=Input::get("solicitudTecnica");
		
         		$query=  SolicitudTecnicaRecepcion::from('solicitud_tecnica_log_recepcion as st')   
         			
         			->select('st.id as id',
         				'st.id_solicitud_tecnica as soli_tecnica',
         				'st.created_at as fec_creacion', 
         				  DB::raw('CASE stu.estado_st WHEN 1 THEN "OK" WHEN 2 THEN "TRABAJAR EN LEGADO" ELSE "ERROR EN LA TRAMA" END AS estado_gestion'),
         				'so.tipo_operacion as tipo_opera',
         				'le.message as mesaje',
         				'st.descripcion_error as des_error',
         				'usu.apellido as nombres'
         					)
         			->Leftjoin('lego_errores as le','st.code_error','=','le.code')
         			->join('solicitud_tecnica as so','st.solicitud_tecnica_id','=','so.id')
         			->join('usuarios as usu','st.usuario_created_at','=','usu.id')
         			->Leftjoin('solicitud_tecnica_ultimo as stu','st.solicitud_tecnica_id','=','stu.solicitud_tecnica_id');

         		if($fecha!=''){
					$query->whereRaw(" date(st.created_at) BETWEEN '". $formFecha[0]."' and '".$formFecha[1]."'");
				}else if($solicitudTecnica!=''){
					$query->whereRaw(" st.id_solicitud_tecnica = '".$solicitudTecnica."'");
				}         		
				$datos=$query->searchPaginateAndOrder();								
				return $datos;
	}


}