<?php
use Legados\CableModem;
class CableModemController extends \BaseController
{
    public function __construct()
    {

    }

    /**
     * prueba de cable modem
     * POST /componente/cargar
     * @param  string mac
     * @return Response
     */
    public static function postConsulta()
    {
        if (Request::ajax()) {
            if(Input::has('mac')) {
                $mac = Input::get('mac');

                $punto = strpos($mac, ".");

                $validado = false;
                if ((strlen($mac) == 12) && ($punto === false)) {
                    $validado = true;
                }

                if ($validado) {
                    // agrego los puntos cada 4 caracteres
                    $new_mac = '';
                    for ($i=0; $i < 12; $i+=4) { 
                        $punto = '.';
                        if($i == 8) $punto = '';
                        $new_mac .= substr($mac, $i, 4).$punto;
                    }
                    $api = new CableModem();
                    $response = $api->getPruebas($new_mac);
                    /*
                    0#USPWR||DSPWR||USSNR||DSSNR|#rf-error
                    */
                    $array = explode('#', $response);
                    $datos = explode('|', $array[1]);

                    $pwr = array();
                    $snr = array();
                    $estado = ''; $estilo = '';

                    for ($i=0, $lenth = count($datos); $i < $lenth; $i+=2) {
                        $con = $i;
                        $tipo = $datos[$i];
                        $valor = $datos[$con+=1];
                        switch ($tipo) {
                            case 'USPWR':
                                $pwr['USPWR'] = $valor; break;
                            case 'DSPWR':
                                $pwr['DSPWR'] = $valor; break;
                            case 'USSNR':
                                $snr['USSNR'] = $valor; break;
                            case 'DSSNR':
                                $snr['DSSNR'] = $valor; break;
                        }
                    }

                    $tablas = '<div class="col-sm-12">MAC ingresada: <b>'.
                                $mac.'</b></div>';
                    $tablas .= '<div class="col-xs-12 col-sm-6">
                                    <table class="table table-striped">
                                    <thead>
                                      <tr>
                                        <th colspan="2" class="text-center">
                                            <h4>Potencia de la Señal</h4></th>
                                      </tr>
                                    </thead>';

                    foreach ($pwr as $name => $value) {
                        switch ($name) {
                            case 'USPWR':
                                $nombre = 'Upstream'; $min = 37.0; $max = 55.0;
                                break;
                            case 'DSPWR':
                                $nombre = 'Downstream'; $min = -5.0; $max = 10.0;
                                break;
                        }

                        $value = str_replace(" ", "", $value);
                    
                        if ( $name == 'USPWR' || $name == 'DSPWR') {
                            if( $value <= $max && $value >= $min) {
                                $estado = 'Correcto ' .$value.' dBmV';
                                $estilo = 'ok';
                            }
                            else { 
                                $estado = 'Inaceptable ' .$value.' dBmV';
                                $estilo = 'fail';
                            }
                            $msj = '<br>(Ideal: '.$min.',0 a '.$max.',0)';
                        }

                        $tablas .= '<tr>';
                        $tablas .= '<td>'.$nombre.'</td>';
                        $tablas .= '<td><span class="'.$estilo.'">'.$estado.
                                    '</span> '.$msj.'</td>';
                        $tablas .= '</tr>';

                    }
                    $tablas .= '</tbody></table></div>';
                    $tablas .= '<div class="col-xs-12 col-sm-6">
                                    <table class="table table-striped">
                                    <thead>
                                      <tr>
                                        <th colspan="2" class="text-center">
                                            <h4>Relación Señal a Ruido</h4></th>
                                      </tr>
                                    </thead>';
                                    
                    foreach ($snr as $name => $value) {
                        switch ($name) {
                            case 'USSNR':
                                $nombre = 'Upstream'; $min = 27.0; $max = 99;
                                break;
                            case 'DSSNR':
                                $nombre = 'Downstream'; $min = 30.0; $max = 99;
                                break;
                        }

                        $value = str_replace(" ", "", $value);
                    
                        if ( $name == 'USSNR' || $name == 'DSSNR') {
                            if( $value <= $max && $value >= $min) {
                                $estado = 'Correcto ' .$value.' dB';
                                $estilo = 'ok';
                            }
                            else { 
                                $estado = 'Inaceptable ' .$value.' dB';
                                $estilo = 'fail';
                            }
                            $msj = '<br>(Ideal: Mayor a '.$min.',0)';
                        }

                        $tablas .= '<tr>';
                        $tablas .= '<td>'.$nombre.'</td>';
                        $tablas .= '<td><span class="'.$estilo.'">'.$estado.
                                        '</span> '.$msj.'</td>';
                        $tablas .= '</tr>';

                    }
                    $tablas .= '</tbody></table></div>';

                    return Response::json(
                        array(
                            'rst' => 1, 
                            'datos' => $tablas,
                            'resultado' => $response
                        )
                    );
                } else {
                    return Response::json(
                        array(
                            'rst' => 0, 
                            'datos' => 'Verifique que la MAC ingresada sea 
                                            de 12 caracteres y sin puntos.'
                        )
                    );
                }
            } else {
                return Response::json(
                    array(
                        'rst' => 0, 
                        'datos' => 'No se recibió MAC.'
                    )
                );
            }
        }
    }
    
}