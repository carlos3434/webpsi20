<?php

class SubmoduloUsuarioController extends \BaseController
{

    /**
     * consulta la lista de modulos y el estado de asignacion segun usuario
     * POST /submodulousuario/listar
     *
     * @param usuario_id
     * @return Response
     */
    public function postListar()
    {
        //si la peticion es ajax
        if ( Request::ajax() ) {

            $usuarioId = Input::get('usuario_id');
            $visible_menu = 1;
            

            $submoduloUsuario = DB::table('submodulo_usuario as su')
                        ->rightJoin(
                            'submodulos as s', function($join) use ($usuarioId)
                            {
                            $join->on('su.submodulo_id', '=', 's.id')
                                ->on('su.usuario_id', '=', DB::raw($usuarioId));
                            }
                        )
                        ->rightJoin('modulos as m', 's.modulo_id', '=', 'm.id')
                        ->where('m.estado', '=', 1)
                        ->where ('s.parent', '=', 0)
                        ->groupBy('m.nombre', 'm.id')
                        ->select(
                            'm.nombre', 'm.id',
                            DB::raw('MAX(su.estado) as estado')
                        )
                        ->get();
            return Response::json(array('rst'=>1,'datos'=>$submoduloUsuario));
        }
    }
    
    /*
     * muestra los accesos a los submodulos ingresados de cada usuario
     * POST / submodulousuario/auditoriaaccesos
     */
    public function postAuditoriaaccesos()
    {
        if ( Request::ajax() ) {
            $query = "SELECT u.`usuario`, su.`fecha_acceso`,
                    m.`nombre` AS modulo, s.`nombre` AS submodulo 
                    FROM `submodulo_usuario` su
                    LEFT JOIN `submodulos` s
                    ON s.id = su.submodulo_id
                    LEFT JOIN `modulos` m
                    ON m.id = s.`modulo_id`
                    LEFT JOIN `usuarios` u
                    ON u.id = su.usuario_id
                    WHERE `fecha_acceso` IS NOT NULL";
            $audiAccesos = DB::select($query);
            return Response::json(array('rst'=>1,'datos'=>$audiAccesos));
        }
    }
}
