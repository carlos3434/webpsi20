<?php
class RefreshDecoCmsController extends \BaseController
{
    public function getServer()
    {
        try {
            $options = ['cache_wsdl' => WSDL_CACHE_NONE];
            $url = Config::get('wpsi.webservice.decocms');
            $server = new SoapServer($url, $options);
            $server->setClass( 'DecoCms' );
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}