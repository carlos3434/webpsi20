<?php
class GeoCamposController extends BaseController
{
    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => [
                'postCrear', 'postEditar']]
        );
    }

    public function postListar()
    {

        if(Request::ajax())
        {
            $rst = 1;
            $campos = GeoCampos::listar();
        }else{
            $rst=0;
            $campos = '';
        }
        return Response::json(
            array(
                'rst'=>$rst,
                'datos'=>$campos
            )
        );
    }

    public function postCargarcampos()
    {
        if(Request::ajax())
        {
            $rst = 1;
            $campos = GeoCampos::all();
        }else{
            $rst=0;
            $campos = '';
        }
        return Response::json(
            array(
                'rst'=>$rst,
                'datos'=>$campos
            )
        );
    }

    public function postCrear()
    {
        if (Request::ajax()) {
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required='required';
            $reglas = array(
                'nombre' => $required.'|'.$regex,
            );

            $mensaje= array(
                'required'    => ':attribute Es requerido',
                'regex'        => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ( $validator->fails() ) {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>$validator->messages(),
                    )
                );
            }

            $campo = new GeoCampos();
            $campo->nombre = Input::get('nombre');
            $campo->estado = Input::get('estado');
            $campo->usuario_created_at = Auth::id();
            $campo->save();

            return Response::json(
                array(
                    'rst'=>1,
                    'msj'=>'Registro realizado correctamente',
                )
            );
        }
    }

    public function postEditar()
    {
        if ( Request::ajax() ) {
            $campoId = Input::get('id');
            $regex='regex:/^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required='required';
            $reglas = array(
                'nombre' =>
                    $required.'|'.$regex.'|unique:geo_campos,nombre,'.$campoId,
            );

            $mensaje= array(
                'required'    => ':attribute Es requerido',
                'regex'        => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ( $validator->fails() ) {
                return Response::json(
                    array(
                        'rst'=>0,
                        'msj'=>$validator->messages(),
                    )
                );
            }

            $campo = GeoCampos::find($campoId);
            $campo->nombre = Input::get('nombre');
            $campo->estado = Input::get('estado');
            $campo->usuario_updated_at = Auth::id();
            $campo->save();

            return Response::json(
                array(
                    'rst'=>1,
                    'msj'=>'Registro actualizado correctamente',
                )
            );
        }
    }

    public function postCambiarestado()
    {
        if ( Request::ajax() ) {

            $campo = GeoCampos::find(Input::get('id'));
            $campo["estado"]= Input::get('estado');
            $campo["usuario_updated_at"]= Auth::id();
            $campo->save();

            return Response::json(
                array(
                    'rst'=>1,
                    'msj'=>'Registro actualizado correctamente',
                )
            );

        }
    }
}