<?php

class GeocercaProyectoZonasController extends \BaseController
{
    public function postListar()
    {
      $datos = GeocercaProyectoZonas::listar();

      if (Request::ajax()) {
            return Response::json(array('rst'=>1,'datos'=>$datos));
      }
    }

    public function postUpdate()
    {
        $datos = GeocercaProyectoZonas::actualizar();  

        if (Request::ajax()) {
            return Response::json(
                array('rst'=>1,
                      'datos'=>$datos,
                      'msj'=>'Se actualizó la zona correctamente')
            );
        } 
    }

    public function postSave()
    {
        $idpro=Input::get('idproyecto');
        $mapa=Input::get('mapa');
        GeocercaProyectoZonas::guardar($idpro, $mapa);

        if (Request::ajax()) {
            return Response::json(
                array('rst'=>1,
                      'datos'=>$idpro,
                      'msj'=>'Se actualizó la zona correctamente')
            );
        } 
    }
}