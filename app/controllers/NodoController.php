<?php
class NodoController extends \BaseController
{
    public function postListar()
    {
        if(Input::get('parametros')){
            $r = DB::table('geo_nodopunto')
                    ->select(
                        DB::RAW('nodo as id'),
                        DB::RAW('concat(zonal," -> ",nodo, " : ",nombre) as nombre'),
                        DB::raw('CONCAT("Z",zonal) as relation')
                        )
                    ->where('tecnologia','COAXIAL')
                    ->groupBy('nodo')
                    ->orderBY('zonal')
                    ->orderBY('nodo')
                    ->get();
        } else {
            $r =Nodo::getNodoAll();
        }

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }


}

