<?php 
class UbigeoCms extends Eloquent
{

    public $table = 'ubigeo_cms';

    public static function getUbigeo(
        $codigo= "001", $codigodepartamento = "000", $codigoprovincia = "000")
    {
        $sql = DB::table("ubigeo_cms")
                ->where("codigo", "=", $codigo)
                ->where("codigodepartamento", "=", $codigodepartamento)
                ->where("codigoprovincia", "=", $codigoprovincia)
                ->get();
        //print_r($sql);
        return $sql;
    }

}