<?php
class GeoFigurasController extends BaseController
{
    public function postListar()
    {

        if(Request::ajax())
        {
            $rst = 1;
            $figuras = GeoFiguras::listar();
        }else{
            $rst=0;
            $figuras = '';
        }
        return Response::json(
            array(
                'rst'=>$rst,
                'datos'=>$figuras
            )
        );
    }
}