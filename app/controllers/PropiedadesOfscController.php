<?php

class PropiedadesOfscController extends \BaseController
{

    protected $_errorController;
    
    public function __construct(ErrorController $errorController)
    {
        $this->beforeFilter('auth');
        $this->_errorController = $errorController;
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );
    }

    public function postCargar()
    {
        if ( Request::ajax() ) {
            $propiedades_ofsc = DB::table('propiedades_ofsc')->orderBy("id", "DESC")->get();

            return Response::json(array('rst' => 1, 'datos' => $propiedades_ofsc));
        }
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            if (Input::get('empresa_id')) {
                $empresaId = Input::get('empresa_id');
                $quiebreId = Input::get('quiebre_id');
                $celulas = DB::table('celulas AS c')
                        ->join(
                            'celula_quiebre AS cq', 'c.id', '=', 'cq.celula_id'
                        )
                        ->select(
                            'c.id', 'c.nombre', DB::raw(
                                'CONCAT("E",c.empresa_id) as relation'
                            )
                        )
                        ->where('c.estado', '=', '1')
                        ->where('c.empresa_id', '=', $empresaId)
                        ->where('cq.estado', '=', '1')
                        ->where('cq.quiebre_id', '=', $quiebreId)
                        ->where(
                            function($query) {
                                if (Input::get('zonal_id')) {
                                    $query->whereRaw(
                                        'c.zonal_id'
                                        . '="' . Input::get('zonal_id') . '"'
                                    );
                                }
                            }
                        )
                        ->orderBy('c.nombre')
                        ->get();
            } elseif (Input::get('tecnico_id')) {

                $tecnicoId = Input::get('tecnico_id');
                $celulas = DB::table('celula_tecnico as ct')
                        ->rightJoin(
                            'celulas as c', function($join) use ($tecnicoId) {
                                $join->on('ct.celula_id', '=', 'c.id')
                                ->on('ct.tecnico_id', '=', DB::raw($tecnicoId));
                            }
                        )
                        ->select('c.id', 'c.nombre', 'ct.estado')
                        ->where('c.estado', '=', 1)
                        ->get();
            } else {
                $celulas = DB::table('celulas')
                        ->select(
                            'id', 'nombre', DB::raw(
                                'CONCAT("E",empresa_id) as relation'
                            )
                        )
                        ->where('estado', '=', '1')
                        ->orderBy('nombre')
                        ->get();
            }

            return Response::json(array('rst' => 1, 'datos' => $celulas));
        }
    }

    public function postCrear()
    {
        if ( Request::ajax() ) {
            $regex = 'regex:/^([a-zA-Z01-9 .,ñÑÁÉÍÓÚáéíóú_-]{2,60})$/i';
            $required = 'required';
            $reglas = array(
                'etiqueta' => $required . '|' . $regex
            );

            $mensaje = array(
                'required' => ':attribute Es requerido',
                'regex' => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ( $validator->fails() ) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            DB::table("propiedades_ofsc")->insert([

            	'label' => Input::get('etiqueta'),
            	'grupo' => 'codigo_estado_actividad',
            	'tipo_tecnologia' => Input::get('tipo_tecnologia'),
            	'estado' => Input::get('actividad'),
            	'tipo' => Input::get('tipo'),
            	'actividad_id' => Input::get('actividad_id')

            ]);

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente',
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            $propiedad_id = Input::get('id');
            $regex = 'regex:/^([a-zA-Z01-9 .,ñÑÁÉÍÓÚáéíóú_-]{2,60})$/i';
            $required = 'required';
            $reglas = array(
                'etiqueta' => $required . '|' . $regex
            );
            $mensaje = array(
                'required' => ':attribute Es requerido',
                'regex' => ':attribute Solo debe ser Texto',
            );
            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            DB::table("propiedades_ofsc")->where("id", $propiedad_id)->update([

            	'label' => Input::get('etiqueta'),
            	'tipo_tecnologia' => (Input::get('tipo_tecnologia')) == '-1' ? null : Input::get('tipo_tecnologia'),
            	'estado' => Input::get('estado'),
            	'tipo' => Input::get('tipo'),
            	'actividad_id' => (Input::get('actividad_id')) == '-1' ? null : Input::get('actividad_id')

            ]);


            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

    public function postCambiarestado()
    {
        if (Request::ajax() && Input::has('estado') & Input::has('id')) {
            DB::table('propiedades_ofsc')->where("id", Input::get('id'))->update(["estado" => Input::get('estado')]);

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

}
