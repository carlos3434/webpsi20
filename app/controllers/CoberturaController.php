<?php

class CoberturaController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
        
                $segmento = DB::table('coberturas')
                            ->select('id', 'nombre')
                            ->orderBy('nombre')
                            ->get();
        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$segmento
                )
            );
        }
    }
 


}//fin class
