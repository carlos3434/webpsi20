<?php

class UsuarioAreaController extends \BaseController {


	public function postRegistrar(){
		if (Request::ajax()) {
			$data = [];
			if(Input::get('user_id') && Input::get('area_id')){
				$data['usuario_id']=Input::get('user_id');
				$data['area_id']=Input::get('area_id');
				$data['estado']=1;				
			}			
			$rst = UsuarioArea::create($data);
			
			return Response::json(
	            array(
	            	'rst' => ($rst) ? 1 : 0,
	              	'msj' => 'Registrado Correctamente',
	            )
            );
		}
	}

    public function getCargar(){
        $usuarioArea = UsuarioArea::from('usuario_area as ua')
            ->join('usuarios as u',
               'ua.usuario_id',
               '=',
               'u.id'
            )
            ->join('areas as a',
                'a.id',
                '=',
                'ua.area_id'
            )
            ->Leftjoin('grupos as g', 'g.id', '=', 'ua.grupo_id')
            ->Leftjoin('quiebres as q', 'q.id', '=', 'ua.quiebre_id')
            ->select(
                'ua.id as usuario_area_id',
                'u.id as id',
                DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) AS nombre'),
                'u.nombre as nombrep', 
                'u.apellido',
                'a.nombre as area', 
                'ua.estado as ua_estado',
                DB::raw('IFNULL(g.nombre, "") as grupo'),
                DB::raw('IFNULL(q.nombre, "") as quiebre')
            )
            ->where(function($query){
                    if(Input::has('grupo')){
                        $query->whereRaw('(ua.grupo_id = 0 or ua.grupo_id="" or ua.grupo_id IS NULL)');
                    }
                }
            )
            ->where(function($query){
                if(Input::get('filter')){
                    array_filter(Input::get('columns'), function($elem) use (&$query){
                        if ($elem['name']=='nombrep') {
                            $value = "%".Input::get('filter')."%";
                            $query->where('u.nombre','like',$value);
                        }
                    });
                }
                }
            )
            ->searchPaginateAndOrder();
        return Response::json($usuarioArea);
    }
    public function postCargar(){
        if (Request::ajax()) {
            $objClass = new UsuarioArea(); 
            return Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );  
        }
    }


	public function postListar(){
		if (Request::ajax()) {
			$objClass = new UsuarioArea(); 
			$rst = '';

			if(Input::get('libres')){
				$rst = $objClass->getAllAvailable();
			}else{
				$rst = $objClass->getAll();
			}
			return Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $rst
                )
            );	
		}
	}
	 
	public function postCrear()
    {
    	if(Request::ajax()){
    		$usuarios = new Usuario();
    		$usuarios['nombre'] = Input::get('nombre');
            $usuarios['apellido'] = Input::get('apellidos');
            $usuarios['usuario'] = Input::get('usuario');
            $usuarios['password'] = Hash::make(Input::get('password'));
            $usuarios['dni'] = Input::get('dni');            
            $usuarios['perfil_id'] = 3;
            $usuarios['empresa_id'] = Input::get('empresa');
            $usuarios['area_id'] = 51;
            /*$usuarios['tipo_persona_id'] = '';*/
            $usuarios['estado'] = 1;
            $usuarios['usuario_created_at'] = Auth::id();
            $usuarios['usuario_estacion'] = $_SERVER['REMOTE_ADDR'];
            $usuarios->save();

            if($usuarios->id){
            	 // Insertamos dentro de la tabla Usuario_password
            	DB::table('usuario_password')
                ->insert(
                    array(
                       'usuario' => $usuarios->id,
                       'password' => Hash::make(Input::get('password')),
                       'fecha' => date("Y-m-d H:i:s"),
                       'estacion' => $_SERVER['REMOTE_ADDR'],
                       'estado' => '0'
                       )
                );

                DB::table('quiebre_grupo_usuario')
                ->insert(
                    array(
                       'usuario_id' => $usuarios->id,
                       'quiebre_grupo_id' => 10,
                       'created_at' => date("Y-m-d H:i:s"),
                       'estado' => '1'
                    )
                );

                $submodulos =[74,75,76,78,88];
                foreach ($submodulos as $key => $value) {
	                DB::table('submodulo_usuario')
	                ->insert(
	                    array(
	                       'usuario_id' => $usuarios->id,
	                       'submodulo_id' => $value,
	                       'created_at' => date("Y-m-d H:i:s"),
	                       'estado' => '1',
	                       'agregar' => '1',
	                       'editar' => '1',
	                       'eliminar' => '1',
	                    )
	                );
                }

                 DB::table('usuario_zonal')
                ->insert(
                    array(
                       'usuario_id' => $usuarios->id,
                       'zonal_id' => 8,
                       'created_at' => date("Y-m-d H:i:s"),
                       'estado' => '1'
                    )
                );

                $data['usuario_id'] = $usuarios->id;
                $data['area_id'] = 51;
                $data['estado'] = 1;
                UsuarioArea::create($data);


                return Response::json(
	                array(
	                'rst' => 1,
	                'msj' => 'Registro creado correctamente',
             	   )
            	);
            }


    	}

    }



	public function postCambiarestado(){
		if (Request::ajax()) {
			$update = UsuarioArea::find(Input::get('id'));

			if(Input::has('estado')){
				$update->estado = Input::get('estado');				
			}

			if(Input::get('grupo_id')){
				$update->grupo_id = Input::get('grupo_id');
			}

			if(Input::get('quiebre_id')){
				$update->quiebre_id = Input::get('quiebre_id');
			}

			$update->usuario_updated_at = Auth::id();
			$update->updated_at = date('Y-m-d H:i:s');
			$update->save();
			return Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}

	public function postGetasignaciones(){
		if (Request::ajax()) {
			$objClass = new UsuarioArea(); 
			$rst = $objClass->getAsignacionesbyUser();
			return Response::json(
                array(
                    'rst'    => 1,
                    'datos'=>(array)$rst['data'],
                	'cabecera'=>$rst['cabecera']
                )
            );	
		}	
	}


	public function postCreargrupo(){
		if (Request::ajax()) {
            $required='required';
            $numeric='numeric';

            $reglas = array(
                'nombreg'    => $required,
                'quiebre2' => $required.'|'.$numeric,
                'usuarios' => $required,
            );

            $mensaje = array(
                'required'  => ':attribute Es requerido',
                'numeric'   => ':attribute seleccione una opcion',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }else{
            	$data = [
            		'nombre'=>Input::get('nombreg'),
            		'quiebre_id'=>Input::get('quiebre2'),
            		'estado'=>1,
            	];
            	if($result = Grupo::create($data)){
            		$usuarios = (strpos(Input::get('usuarios'), ',') !== false) ? explode(',',Input::get('usuarios')) : array(Input::get('usuarios'));
            		foreach ($usuarios as $key => $value) {
            			DB::table('usuario_area')
			            	->where('usuario_id', $value)
			            	->update(array('grupo_id' => $result->id));
            		}
            	}

				return Response::json(
	                array(
	                    'rst'    => 1,
	                    'msj'=>'Registrado Correctamente',
	                )
	            );	
            }			
		}	
	}
	
	
	/**
	 * Display a listing of the resource.
	 * GET /usuarioarea
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /usuarioarea/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /usuarioarea
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /usuarioarea/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /usuarioarea/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /usuarioarea/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /usuarioarea/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}