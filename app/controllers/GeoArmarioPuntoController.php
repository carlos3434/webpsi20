<?php
class GeoArmarioPuntoController extends \BaseController
{
    public function postListar()
    {

        $r =GeoArmarioPunto::postGeoArmarioPuntoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoArmarioPunto::postMdfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postMdf()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $r =GeoArmarioPunto::postArmarioPuntoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


}

