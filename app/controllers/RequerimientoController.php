<?php

class RequerimientoController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            try {
                $result='';
                if(Input::get('parametros')){
                    $first = DB::table('gtm_averias')
                            ->select(
                                    DB::RAW('distinct(tipo_req) as id'),
                                    DB::RAW('concat("AVE->",tipo_req) as nombre')
                                    );

                    $result = DB::table('webpsi_coc.prov_catv_tiposact')
                                ->select(
                                    DB::RAW('distinct(tipo_req) as id'),
                                    DB::RAW('concat("PROV->",tipo_req) as nombre')
                                    )
                                ->orderBy('tipo_req')
                                ->union($first)
                                ->get();
                }
            } catch (Exception $error) {
               $this->error->handlerError($error);
            }
            return Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
