<?php

class ProyectoEstadoController extends \BaseController
{
    public function postCargar()
    {
        if (Request::ajax()) {

            $datos = DB::table('proyectos_estados')
                    ->select('id', 'nombre')
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postListar()
    {
        if (Request::ajax()) {
            $segmento = DB::table('proyectos_estados')
                    ->select(
                        'nombre as id', 'nombre', 'estado as status', 
                        'id as codigo'
                    )
                    ->orderBy('nombre')
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $segmento
                )
            );
        }
    }

    public function postCrear()
    {
        if (Request::ajax()) {

            DB::table('proyectos_estados')
            ->insert(
                array('nombre' => Input::get('nombre'),
                'estado' => Input::get('estado'),
                'created_at' => date("Y-m-d H:m:s"),
                'usuario_created_at' => Auth::user()->id)
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente'
                )
            );
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            
            $datos = DB::table('proyectos_estados')
                    ->select('id', 'nombre', 'estado')
                    ->where('id', '=', Input::get('id'))
                    ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            
            DB::table('proyectos_estados')
            ->where('id', Input::get('id'))
            ->update(
                array('nombre' => Input::get('nombre'),
                    'estado' => Input::get('estado'),
                    'updated_at' => date("Y-m-d H:m:s"),
                    'usuario_updated_at' => Auth::user()->id)
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Actualizacion realizado correctamente'
                )
            );
        }
    }

    public function postEstado()
    {
        if (Request::ajax()) {
            
            DB::table('proyectos_estados')
            ->where('id', Input::get('id'))
            ->update(
                array('estado' => Input::get('estado'))
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Estado actualizado correctamente'
                )
            );
        }
    }
}

//fin class
