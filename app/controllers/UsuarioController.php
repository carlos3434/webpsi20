<?php

class UsuarioController extends BaseController
{
    /**
     * Constructor de la clase
     *
     */
    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token',
            ['only' => ['postCrear', 'postEditar']]
        );
    }

    public function postValidaacceso()
    {
        $hash = hash(
            'sha256', Config::get('wpsi.permisos.key')
            .Input::get('agregarG')
            .Input::get('editarG')
            .Input::get('eliminarG')
        );
        if ($hash == Input::get('hash')) {
            return json_encode(array('rst'=> 1));
        } else {
            return json_encode(array('rst'=> 2));
        }
    }
    /**
     * Mostrar los datos del contacto actual
     * POST /usuario/cargar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $usuario = Usuario::find(Auth::user()->id);
            $perfilId=$usuario['perfil_id'];
            $usuarios =  DB::table('usuarios AS u')
                        ->join(
                            'empresas as e',
                            'u.empresa_id', '=', 'e.id'
                        )
                        ->join(
                            'perfiles as p',
                            'u.perfil_id', '=', 'p.id'
                        )
                        ->join(
                            'areas as a',
                            'u.area_id', '=', 'a.id'
                        )
                        ->select(
                            'u.id',
                            'u.nombre',
                            'u.apellido',
                            'u.usuario',
                            'u.password',
                            'u.dni',
                            'u.sexo',
                            'u.imagen',
                            'u.email',
                            'u.celular',
                            'u.estado',
                            'u.area_id',
                            'u.perfil_id',
                            'u.empresa_id',
                            'a.nombre as area',
                            'p.nombre as perfil',
                            'e.nombre as empresa'
                        );
            if ($perfilId!=8) {//super user
                $usuarios=$usuarios->where(
                    'usuario_created_at', Auth::user()->id
                );
            }
            $usuarios=$usuarios->get();

            return Response::json(array('rst' => 1, 'datos' => $usuarios));
        }
    }
    /**
     * Store a newly created resource in storage.
     * POST /usuario/cargarsubmodulos
     *
     * @return Response
     */
    public function postCargarsubmodulos()
    {
        $usuarioId = Input::get('usuario_id');
        $parentSubmodulo = 0;
        if(Input::get('parent_submodulo')!=null)
            $parentSubmodulo = Input::get('parent_submodulo');
        $usuario = new Usuario;
        $submodulos = $usuario->getSubmodulos($usuarioId, $parentSubmodulo);
        return Response::json(array('rst'=>1,'datos'=>$submodulos));
    }

    public function postCargarsubmodulosparent()
    {
        $usuarioId = Input::get('usuario_id');
        $usuario = new Usuario;
        $submodulos = $usuario->getSubmodulosParent($usuarioId);
        return Response::json(array('rst'=>1,'datos'=>$submodulos));
    }

    public function postCargarprivilegios()
    {
        $usuarioId = Input::get('usuario_id');
        $moduloId = Input::get('modulo_id');
        $usuario = new Usuario;
        $submodulos = $usuario->getPrivilegios($usuarioId, $moduloId);
        return Response::json(array('rst'=>1,'datos'=>$submodulos));
    }
    /**
     * Store a newly created resource in storage.
     * POST /usuario/crear
     *
     * @return Response
     */
    public function postCrear()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required='required';
            $numeric='numeric';
            $zonalesSelecRequired = "";

            /**
             * Para perfil Proyecto Edificios (id = 9)
             * zonales_selec => No requerido
             */
            switch(Input::get('perfil')){
                case 9:
                    break;
                default:
                    $zonalesSelecRequired = "required";
                    break;
            }
            $usuario =new Usuario();
            $idPerfilUsuario = $usuario->getPerfil();

            if ($idPerfilUsuario == 9) {
                $zonalesSelecRequired = "";
            }
            $reglas = array(
                'nombre'    => $required.'|'.$regex,
                'apellido'    => $required.'|'.$regex,
                'usuario'   => $required.'|'.$regex.'|unique:usuarios',
                'password'  => 'required|between:6,50',
                'dni'       => 'required|min:8|unique:usuarios',
                //'email' => 'required|email',
                //'celular' => $required.'|'.$numeric,
                'perfil' => $required.'|'.$numeric,
                'empresa' => $required.'|'.$numeric,
                'area' => $required.'|'.$numeric,
                'sexo' => $required,
                'zonales_selec' => $zonalesSelecRequired,
            );

            $mensaje = array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
                'numeric'   => ':attribute seleccione una opcion',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }

            $usuarios = new Usuario();
            $usuarios['nombre'] = Input::get('nombre');
            $usuarios['apellido'] = Input::get('apellido');
            $usuarios['usuario'] = Input::get('usuario');
            $usuarios['password'] = Hash::make(Input::get('password'));
            $usuarios['dni'] = Input::get('dni');
            $usuarios['email'] = Input::get('email');
            $usuarios['celular'] = Input::get('celular');
            $usuarios['perfil_id'] = Input::get('perfil');
            $usuarios['sexo'] = Input::get('sexo');
            $usuarios['empresa_id'] = Input::get('empresa');
            $usuarios['area_id'] = Input::get('area');
            $usuarios['estado'] = Input::get('estado');
            $usuarios['usuario_created_at'] = Auth::user()->id;
            $usuarios['usuario_estacion'] = $_SERVER['REMOTE_ADDR'];
            $usuarios['intento'] = '0';
            $usuarios['estacion'] = '';
            $usuarios['fecha_login'] = '';
            $usuarios['fecha_error'] = '';
            $usuarios['contador_error'] = '0';
            $usuarios['estado_log'] = '0';
            $usuarios->save();

            // Insertamos dentro de la tabla Usuario_password
            DB::table('usuario_password')
                ->insert(
                    array(
                       'usuario' => Input::get('usuario'),
                       'password' => Hash::make(Input::get('password')),
                       'fecha' => date("Y-m-d H:i:s"),
                       'estacion' => $_SERVER['REMOTE_ADDR'],
                       'estado' => '0'
                       )
                );

            $empresas=Input::get('empresas');
            $modulos = Input::get('modulos_selec');
            $zonales=explode(',', Input::get('zonales_selec'));
            $pertenece = Input::get('pertenece');
            $estado = 0;
            if (count($zonales) > 0) {
                for ($i=0; $i<count($zonales); $i++) {
                    $zonalId = $zonales[$i];
                    $zonal = Zonal::find($zonalId);
                    if ($pertenece == $zonalId)
                        $estado = 1;
                    else
                        $estado = 0;
                    if ($zonal!=null) {
                        $usuarios->zonales()->save(
                            $zonal,
                            array(
                            'estado'=>1,
                            'pertenece'=> $estado
                            )
                        );
                    }
                }
            }

            $quiebregrupos=Input::get('quiebregrupos');

            $estado = Input::get('estado');
            if ($estado == 0 ) {
                return Response::json(
                    array(
                    'rst'=>1,
                    'msj'=>'Registro actualizado correctamente',
                    )
                );
            }
            if ($modulos) {//si selecciono algun modulo
                $modulos = explode(',', $modulos);

                $modulos = array_unique($modulos);

                $submodulos = array();
                $submoduloId = array();
                $privilegio=array();
                $insert=array();
                //print_r($modulos);
            for ($i=0; $i<count($modulos); $i++) {
                $moduloId = $modulos[$i];
                //almacenar las submodulo seleccionadas
                $submodulos[] = Input::get('submodulos'.$moduloId);
                $submodulosparent[] = Input::get('submodulos_parent'.$moduloId);
            }
                $submodulos = array_merge($submodulos, $submodulosparent);

                for ($i=0; $i<count($submodulos); $i++) {
                    for ($j=0; $j <count($submodulos[$i]); $j++) {
                        //buscar la submodulo en ls BD
                        $submoduloId[] = $submodulos[$i][$j];
                    }
                }
                for ($i=0; $i<count($submoduloId); $i++) {
                    $submodulo = Submodulo::find($submoduloId[$i]);

        if ((Input::has('privilegio'.$submoduloId[$i])) == $submoduloId[$i]) {
                        $privilegio[] = Input::get(
                            'privilegio'.$submoduloId[$i]
                        );
                        $permisos = '';

                        for ($k=0; $k<count($privilegio); $k++) {
                             $permisos = '';
                             for ($l=0; $l<count($privilegio[$k]); $l++) {
                                if ($privilegio[$k][$l] == 1) {
                                    //permisos de agregar
                                    $permisos .= 'a';
                                }
                                if ($privilegio[$k][$l] == 2) {
                                     //tiene permisos de agregar y editar
                                    $permisos .= 'b';
                                }
                                if ($privilegio[$k][$l] == 3) {
                                    //solo tiene permisos de eliminar
                                    $permisos .= 'c';
                                }
                             }
                        }
                        switch ($permisos) {
                        case 'abc':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 1,
                                'editar' => 1, 'eliminar' => 1,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'ab':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 1,
                                'editar' => 1, 'eliminar' => 0,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'ac':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 1,
                                'editar' => 0, 'eliminar' => 1,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'bc':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 0,
                                'editar' => 1, 'eliminar' => 1,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'a':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 1,
                                'editar' => 0, 'eliminar' => 0,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'b':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 0,
                                'editar' => 1, 'eliminar' => 0,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case 'c':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 0,
                                'editar' => 0, 'eliminar' => 1,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        case '':
                            $insert =  array(
                                'estado' => 1, 'agregar' => 0,
                                'editar' => 0, 'eliminar' => 0,
                                'usuario_created_at' => Auth::user()->id,
                                'created_at' => date('Y-m-d h:i:s', time()));
                            break;
                        }

                        $usuarios->submodulos()->save(
                            $submodulo, $insert
                        );
        } else {
                          $usuarios->submodulos()->save(
                              $submodulo, array('estado' => 1)
                          );
        }
                }//enf for
            }
            if (count($empresas) > 0) {
                for ($i=0; $i<count($empresas); $i++) {
                    $empresaId = $empresas[$i];
                    $empresa = Empresa::find($empresaId);
                    $usuarios->empresas()->save($empresa, array('estado' => 1));
                }
            }

            if (count($quiebregrupos) > 0) {
               for ($i=0; $i<count($quiebregrupos); $i++) {
                    $quiebreGrupoId = $quiebregrupos[$i];
                    $quiebreGrupo = QuiebreGrupo::find($quiebreGrupoId);
                    $usuarios->quiebregrupos()->save(
                        $quiebreGrupo,
                        array('estado' => 1)
                    );
               }
            }

            /**
             * Inserción a la tabla proyecto_tipo_usuario
             * Campos: usuarios_id, submodulo_usuario, proyecto_tipo
             */

            if (count($submodulosparent) > 0) {

                $data = array();

                $usuarioId = $usuarios->id;
                $submoduloUsuarioId = array();

                foreach ($submodulosparent as $key => $value) {
                    if ($value == "")
                        unset($submodulosparent[$key]);
                }
                foreach ($submodulosparent as $key => $value) {
                    foreach ($value as $keyD=> $valueD) {
                        $data[$valueD] = array(
                                    "usuarios_id" => $usuarioId,
                                    "submodulo_usuario_id" => $valueD,
                                     );
                        $submoduloUsuarioId[] = $valueD;
                    }

                }

                $row = DB::table('submodulo_usuario as su')
                        ->rightJoin(
                            "submodulos as s", "su.submodulo_id", "=", "s.id"
                            )
                        ->select(
                            "su.id as id", "s.proyecto_tipo as proyecto_tipo",
                            "su.submodulo_id as submodulo_id"
                        )
                        ->whereIn('su.submodulo_id', $submoduloUsuarioId)
                        ->where('su.usuario_id', '=', $usuarioId)
                        ->get();

            foreach ($row as $key => $value) {
                $data[$value->submodulo_id]["submodulo_usuario_id"] = $value->id;
                $data[$value->submodulo_id]["proyecto_tipo"] = $value->proyecto_tipo;
            }
                if (count($data) > 0) {
                    DB::table("proyecto_tipo_usuario")->insert($data);
                }
            }

            return Response::json(
                array(
                'rst' => 1,
                'msj' => 'Registro realizado correctamente',
                )
            );
        }
    }

    /**
     * Update the specified resource in storage.
     * POST /usuario/editar
     *
     * @return Response
     */
    public function postEditar()
    {
        if (Request::ajax()) {

            $usuarioId = Input::get('id');
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required='required';
            $numeric='numeric';
            $zonalesSelecRequired = "";

            /**
             * Para perfil Proyecto Edificios (id = 9)
             * zonales_selec => No requerido
             */
            switch(Input::get('perfil')){
                case 9:
                    break;
                default:
                    $zonalesSelecRequired = "required";
                    break;
            }

            $usuario =new Usuario();
            $idPerfilUsuario = $usuario->getPerfil();

            if ($idPerfilUsuario == 9) {
                $zonalesSelecRequired = "";
            }

            $reglas = array(
                'nombre'    => $required.'|'.$regex,
                'apellido'    => $required.'|'.$regex,
                'usuario'   =>$regex.'|unique:usuarios,usuario,'.$usuarioId,
                //'password'  => 'required|min:6',
                'dni'       => 'required|min:8|unique:usuarios,dni,'.$usuarioId,
                //'email' => 'required|email',
                //'celular' => $required.'|'.$numeric,
                'perfil' => $required.'|'.$numeric,
                'empresa' => $required.'|'.$numeric,
                'area' => $required.'|'.$numeric,
                'sexo' => $required,
                'zonales_selec' => $zonalesSelecRequired,
            );
            $mensaje = array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
                'numeric'   => ':attribute seleccione una opcion',
            );
            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }
            $usuarios = Usuario::find($usuarioId);
            $usuarios['nombre'] = Input::get('nombre');
            $usuarios['apellido'] = Input::get('apellido');
            $usuarios['usuario'] = Input::get('usuario');
            if (Input::get('password')<>'')
                $usuarios['password'] = Hash::make(Input::get('password'));
            $usuarios['dni'] = Input::get('dni');
            $usuarios['email'] = Input::get('email');
            $usuarios['celular'] = Input::get('celular');
            $usuarios['perfil_id'] = Input::get('perfil');
            $usuarios['sexo'] = Input::get('sexo');
            $usuarios['empresa_id'] = Input::get('empresa');
            $usuarios['area_id'] = Input::get('area');
            $usuarios['estado'] = Input::get('estado');
            $usuarios['usuario_updated_at'] = Auth::user()->id;
            $usuarios['usuario_estacion'] = $_SERVER['REMOTE_ADDR'];
            $usuarios->save();

            // Insertamos dentro de la tabla Usuario_password
            if (Input::get('password')<>'') {
                DB::table('usuario_password')
                ->insert(
                    array(
                        'usuario' => Input::get('usuario'),
                        'password' => Hash::make(Input::get('password')),
                        'fecha' => date("Y-m-d H:i:s"),
                        'estacion' => $_SERVER['REMOTE_ADDR'],
                        'estado' => '1'
                    )
                );
            }
            $empresas=Input::get('empresas');
            $submodulos=Input::get('submodulos');
            $quiebregrupos=Input::get('quiebregrupos');

            //empresas
            DB::table('empresa_usuario')
                    ->where('usuario_id', $usuarioId)
                    ->update(array('estado' => 0));

            //si estado de usuario esta activo y no selecciono nin gun quebre
            if (Input::get('estado') == 1
                and $empresas<>'null' and $empresas<>'') {
                for ($i=0; $i<count($empresas); $i++) {
                    $empresaId = $empresas[$i];
                    $empresa = Empresa::find($empresaId);
                    //buscando en la tabla
                    $empresaUsuario = array();
                    $empresaUsuario = DB::table('empresa_usuario')
                        ->where('empresa_id', '=', $empresaId)
                        ->where('usuario_id', '=', $usuarioId)
                        ->first();
                    if (is_null($empresaUsuario)
                        and count($empresaUsuario)==0 ) {
                        $usuarios->empresas()->save(
                            $empresa, array('estado' => 1)
                        );
                    } else {
                        //update a la tabla empresa_usuario
                        DB::table('empresa_usuario')
                            ->where('empresa_id', '=', $empresaId)
                            ->where('usuario_id', '=', $usuarioId)
                            ->update(array('estado' => 1));
                    }
                }
            }
            //zonales
            $pertenece = Input::get('pertenece');
            $zonales=explode(',', Input::get('zonales_selec'));

            //actulizando a estado 0 segun quiebre seleccionado
            DB::table('usuario_zonal')
                    ->where('usuario_id', $usuarioId)
                    ->update(array('estado' => 0, 'pertenece' => 0));
            $estado = 0;
            //si estado de usuario esta activo y selecciono zonales
            if (Input::get('estado') == 1 and !empty($zonales)) {

                for ($i=0; $i<count($zonales); $i++) {
                    $zonalId = $zonales[$i];
                    $zonal = Zonal::find($zonalId);
                    //buscando en la tabla
                    $usuarioZonal = DB::table('usuario_zonal')
                        ->where('usuario_id', '=', $usuarioId)
                        ->where('zonal_id', '=', $zonalId)
                        ->first();
                    //pertenece
                    //$pertenece = Input::get('pertenece'.$zonalId, 0);
                    if ($pertenece == $zonalId)
                        $estado = 1;
                    else
                        $estado = 0;
                    if (is_null($usuarioZonal) and count($usuarioZonal)==0) {
                        if ($zonal!=null) {
                            $usuarios->zonales()->save(
                                $zonal,
                                array(
                                    'estado' => 1,
                                    'pertenece' => $estado
                                )
                            );
                        }

                    } else {
                        DB::table('usuario_zonal')
                            ->where('usuario_id', '=', $usuarioId)
                            ->where('zonal_id', '=', $zonalId)
                            ->update(
                                array(
                                    'estado' => 1,
                                    'pertenece' => $estado
                                    )
                            );
                    }
                }

            }
            //submodulos
            $modulos = Input::get('modulos_selec');
            DB::table('submodulo_usuario')
                    ->where('usuario_id', $usuarioId)
                    ->update(array('estado' => 0));

            if ($modulos) {//si selecciono algun menu

                $modulos = explode(',', $modulos);
                $modulos = array_unique($modulos);
                $submodulos=array();
                $update=array();

                for ($i=0; $i<count($modulos); $i++) {
                   $moduloId = $modulos[$i];
                    //almacenar las opciones seleccionadas
                   $submodulos[] = Input::get('submodulos'.$moduloId);
                   $submodulosparent[] = Input::get('submodulos_parent'.$moduloId);
                }
                $submodulos = array_merge($submodulos, $submodulosparent);
                for ($i=0; $i<count($submodulos); $i++) {
                    for ($j=0; $j <count($submodulos[$i]); $j++) {
                        //buscar la opcion en ls BD
                        $submoduloId = $submodulos[$i][$j];
                        $permisos = '';
                        if (isset($modulos[$i])) {
                            if ((Input::has('privilegio_mod'.$modulos[$i])) == $modulos[$i]) {
                                $update[] = Input::get('privilegio'.$submoduloId);

                                for ($k=0; $k<count($update); $k++) {
                                    $permisos = '';
                                    for ($l=0; $l<count($update[$k]); $l++) {
                                        if ($update[$k][$l] == 1) {
                                            //permisos de agregar
                                            $permisos .= 'a';
                                        }
                                        if ($update[$k][$l] == 2) {
                                             //permisos de agregar y editar
                                            $permisos .= 'b';
                                        }
                                        if ($update[$k][$l] == 3) {
                                            //permisos de eliminar
                                            $permisos .= 'c';
                                        }
                                    }
                                }
                            }
                        }

                        $submodulo = Submodulo::find($submoduloId);
                        $usuarioSubmodulo = array();
                        $usuarioSubmodulo = DB::table('submodulo_usuario')
                            ->where('usuario_id', '=', $usuarioId)
                            ->where('submodulo_id', '=', $submoduloId)
                            ->first();
                        if (is_null($usuarioSubmodulo)
                            and count($usuarioSubmodulo)==0 ) {
                            $usuarios->submodulos()->save(
                                $submodulo, array('estado' => 1)
                            );
                        } else {
                            //update a la tabla cargo_opcion
                            DB::table('submodulo_usuario')
                                ->where('usuario_id', '=', $usuarioId)
                                ->where('submodulo_id', '=', $submoduloId)
                                ->update(array('estado' => 1));
                        }
                        if (isset($modulos[$i])) {
                            if ((Input::has('privilegio_mod'.$modulos[$i])) == $modulos[$i]) {
                                if(Input::get('perfil') == 8) $permisos = 'abc';
                                    Submodulo::updatePrivilegios(
                                        $usuarioId, $submoduloId, $permisos
                                    );
                            }
                        }
                    }
                }
            }

            //quiebregrupos
            DB::table('quiebre_grupo_usuario')
                    ->where('usuario_id', $usuarioId)
                    ->update(array('estado' => 0));
            //restriccion de quiebres
            DB::table('quiebre_usuario_restringido')
                    ->where('usuario_id', $usuarioId)
                    ->update(array('estado' => 0));
            //si estado de usuario esta activo y no selecciono nin gun quebre
            if (Input::get('estado') == 1
                and $quiebregrupos<>'null' and $quiebregrupos<>'') {
                for ($i=0; $i<count($quiebregrupos); $i++) {
                    $quiebreGrupoId = $quiebregrupos[$i];
                    //restriccion de quiebres
                    $quiebres=Input::get('quiebres'.$quiebreGrupoId);
                    //buscar registros
                    for ($k=0; $k<count($quiebres); $k++) {
                        $quiebreId = $quiebres[$k];
                        $row = DB::table('quiebre_usuario_restringido')
                                ->where('usuario_id', $usuarioId)
                                ->where('quiebre_id', $quiebreId)
                                ->first();
                        if (count($row)>0) {
                            DB::table('quiebre_usuario_restringido')
                                ->where('usuario_id', $usuarioId)
                                ->where('quiebre_id', $quiebreId)
                                ->update(array('estado' => 1));
                        } else {
                            DB::table('quiebre_usuario_restringido')
                                ->insert(
                                    array(
                                        'usuario_id' => $usuarioId,
                                        'quiebre_id' => $quiebreId,
                                    )
                                );
                        }
                    }

                    $quiebreGrupo = QuiebreGrupo::find($quiebreGrupoId);
                    //buscando en la tabla
                    $quiebreGrupoUsuario=array();
                    $quiebreGrupoUsuario = DB::table('quiebre_grupo_usuario')
                        ->where('quiebre_grupo_id', '=', $quiebreGrupoId)
                        ->where('usuario_id', '=', $usuarioId)
                        ->first();
                    if (is_null($quiebreGrupoUsuario)
                        and count($quiebreGrupoUsuario)==0 ) {
                        $usuarios->quiebregrupos()->save(
                            $quiebreGrupo, array('estado' => 1)
                        );
                    } else {
                        //update a la tabla quiebre_grupo_usuario
                        DB::table('quiebre_grupo_usuario')
                            ->where('quiebre_grupo_id', '=', $quiebreGrupoId)
                            ->where('usuario_id', '=', $usuarioId)
                            ->update(array('estado' => 1));
                    }
                }
            }
            /**
             * Actualizacion a la tabla proyecto_tipo_usuario
             * Campos: usuarios_id, submodulo_usuario
             */
            foreach ($submodulosparent as $key => $value) {
                if($value == "")
                    unset($submodulosparent[$key]);
            }
            if (count($submodulosparent) > 0) {
                $row = DB::table('proyecto_tipo_usuario')
                        ->where('usuarios_id', '=', $usuarioId)
                        ->delete();
                $data = array();
                $submoduloUsuarioId = array();

                foreach ($submodulosparent as $key => $value) {
                    foreach ($value as $keyD => $valueD) {
                        $data[$valueD] = array(
                                    "usuarios_id" => $usuarioId,
                                    "submodulo_usuario_id" => $valueD);
                        $submoduloUsuarioId[] = $valueD;
                    }

                }
                $row = DB::table('submodulo_usuario as su')
                        ->rightJoin("submodulos as s", "su.submodulo_id", "=", "s.id")
                        ->select(
                            "su.id as id", "s.proyecto_tipo as proyecto_tipo",
                            "su.submodulo_id as submodulo_id"
                        )
                        ->whereIn('su.submodulo_id', $submoduloUsuarioId)
                        ->where('su.usuario_id', '=', $usuarioId)
                        ->get();

            foreach ($row as $key => $value) {
                $data[$value->submodulo_id]["submodulo_usuario_id"] = $value->id;
                $data[$value->submodulo_id]["proyecto_tipo"] = $value->proyecto_tipo;
            }
                if (count($data) > 0) {
                    DB::table("proyecto_tipo_usuario")->insert($data);
                }
            } else {
                $row = DB::table('proyecto_tipo_usuario')
                        ->where('usuarios_id', '=', $usuarioId)
                        ->delete();
            }

            /**
             * Inserta los submodulos parent
             */

            if (count($submodulosparent) > 0) {

            }
            return Response::json(
                array(
                'rst' => 1,
                'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

    /**
     * Cambiar estado del registro de usuario, ello implica cambiar el estado de
     * la tabla empresa_usuario, quiebre_grupo_usuario, submodulo_usuario.
     * POST /usuario/cambiarestado
     *
     * @return Response
     */
    public function postCambiarestado()
    {
        if (Request::ajax()) {
            $estado = Input::get('estado');
            $usuario = Usuario::find(Input::get('id'));
            $usuario['estado'] = Input::get('estado');
            $usuario['usuario_updated_at'] = Auth::user()->id;
            $usuario->save();

            return Response::json(
                array(
                'rst' => 1,
                'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }
    /**
     * Mostrar los datos del contacto actual
     * POST /usuario/misdatos
     *
     * @return Response
     */
    public function postMisdatos()
    {
        if (Request::ajax()) {

        $query = 'SELECT `usuario` FROM `usuarios` WHERE `id` = "'.Auth::id().'"';
            $usuario = DB::select($query);

            if ((strrev(Input::get('newpassword')) == ($usuario[0]->usuario)) ||
             (Input::get('newpassword') == $usuario[0]->usuario)) {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array('newpassword'=>'La contraseña no puede contener su nombre de usuario'),
                    )
                );
            }

            $reglas = array(
                'password'      => 'required|between:6,50',
                'newpassword'   => 'required|regex:"^(?=\w*\d)(?=\w*[a-z])\S{6,50}$"',
            );

            $validator = Validator::make(Input::all(), $reglas);

            if ($validator->fails()) {
                $final='';
                $datosfinal=array();
                $msj=(array) $validator->messages();

                foreach ($msj as $key => $value) {
                    $datos=$msj[$key];
                    foreach ($datos as $keyI => $valueI) {
                        $datosfinal[$keyI]=str_replace(
                            $keyI, trans('greetings.'.$keyI), $valueI
                        );
                    }
                    break;
                }

                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>$datosfinal,
                    )
                );
            }

            $userdata= array(
                'usuario' => Auth::user()->usuario,
                'password' => Input::get('password'),
            );

            if ( Auth::attempt($userdata) ) {

                if ( Input::get('newpassword')!='' ) {

                    $query = "SELECT count(*) as total
                            FROM `usuario_password`
                            WHERE `usuario` = '".Session::get('nomusuario')."'
                            ORDER BY id DESC
                            LIMIT 1";

                    $validarRegistro = DB::select($query);

                    $query = "SELECT `password`,
                            TIMESTAMPDIFF(MINUTE,`fecha`, NOW()) AS hora,
                            estado
                            FROM `usuario_password`
                            WHERE `usuario` = '".Session::get('nomusuario')."'
                            ORDER BY id DESC
                            LIMIT 3";

                    $validarNuevoPass = DB::select($query);

                    if ($validarRegistro[0]->total > 0) {
                        if ($validarNuevoPass[0]->estado == 1) {
                            if ($validarNuevoPass[0]->hora <= 60) {
                                return Response::json(
                                    array(
                                        'rst'=>3,
                                        'msj'=>'Intentar Cambiar Contraseña'
                                        . ' dentro de '
                                        . (60-($validarNuevoPass[0]->hora))
                                        . ' minutos.',
                                    )
                                );
                            }
                        }
                    }

                    foreach ($validarNuevoPass as $pass) {
                        if (Hash::check((Input::get('newpassword')), ($pass->password))) {
                            return Response::json(
                                array(
                                    'rst'=>3,
                                    'msj'=>'Contraseña Usada Anteriormente',
                                )
                            );
                        }
                    }

                    $usuario = Usuario::find(Auth::user()->id);
                    $usuario['password'] = Hash::make(Input::get('newpassword'));
                    $usuario['usuario_updated_at'] = Auth::user()->id;
                    $usuario->save();

                    // Insertamos dentro de la tabla Usuario_password
                    DB::table('usuario_password')
                    ->insert(
                        array(
                            'usuario' => Session::get('nomusuario'),
                            'password' => Hash::make(Input::get('newpassword')),
                            'fecha' => date("Y-m-d H:i:s"),
                            'estacion' => $_SERVER['REMOTE_ADDR'],
                            'estado' => '1'
                        )
                    );
                }

                return Response::json(
                    array(
                        'rst'=>1,
                        'msj'=>'Registro actualizado correctamente',
                    )
                );

            } else {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array('password'=>'Contraseña incorrecta'),
                    )
                );
            }
        }
    }

    /**
     * cargar contactos
     * POST /usuario/cargarcontactos
     *
     * @return Response
     */
    public function postCargarcontactos()
    {
        if (Request::ajax()) {
            $contactos = DB::select(
                'SELECT c.id,c.estado,
                IF(c.usuario_id = ? ,u2.email,u.email) email
                FROM `contactos` c
                INNER JOIN usuarios u ON u.id=c.usuario_id
                INNER JOIN usuarios u2 ON u2.id=c.usuario_id2
                WHERE c.usuario_id = ?
                OR c.usuario_id2 = ? ',
                array(Auth::user()->id,Auth::user()->id,Auth::user()->id)
            );
            return Response::json(array('rst'=>1,'datos'=>$contactos));
        }
    }

    /**
     * Display the specified resource.
     * POST /usuario/eliminarcontacto
     *
     * @param  int  $id
     * @return Response
     */
    public function postEliminarcontacto()
    {
        if (Request::ajax()) {
            $contacto = Contacto::find(Input::get('id'));
            $contacto->delete();

            return Response::json(
                array(
                    'rst'=>1,
                    'msj'=>'Registro eliminado correctamente',
                )
            );
        }
    }

    /**
     * crear nuevo contacto
     * POST /usuario/crearcontacto
     *
     * @return Response
     */
    public function postCrearcontacto()
    {
        $msj ='No se pudo realizar el envio de Email; Favor de verificar su';
        $msj.= ' email e intente nuevamente.';
        if (Request::ajax()) {
            $reglas = array(
                'email'         => 'required|email|exists:usuarios',
            );

            $validator = Validator::make(Input::all(), $reglas);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>$validator->messages(),
                    )
                );
            }

            $usuario = DB::table('usuarios')
                        ->where('email', '=', Input::get('email'))
                        ->first();

            $validaUsuario = DB::select(
                'select * from `contactos`
                where (`usuario_id` = ? and `usuario_id2` = ?)
                or (`usuario_id` = ? and `usuario_id2` = ?)',
                array(
                    Auth::user()->id,$usuario->id,$usuario->id,Auth::user()->id
                )
            );

            $urlValidacion=Hash::make(Input::get('email').date("Y-m-d"));

            if ( count($validaUsuario)==0 ) {
                DB::beginTransaction();
                $contacto = new Contacto;
                $contacto['usuario_id'] = Auth::user()->id;
                $contacto['usuario_id2']= $usuario->id;
                $contacto['url_validacion']=$urlValidacion;
                $contacto['estado'] = 0;
                $contacto->save();

                $parametros=array(
                    'email'    => Input::get('email'),
                    'email2'   => Auth::user()->email,
                    'hash'     => $urlValidacion,
                );

                try{
                    Mail::send(
                        'emailscontacto', $parametros,
                        function($message){
                            $message->to(
                                Input::get('email'),
                                ''
                            )->subject(
                                '.::Ubicame - Confirmación de contacto::.'
                            );
                        }
                    );

                    DB::commit();
                    return Response::json(
                        array(
                            'rst'=>1,
                            'msj'=>'Registro realizado correctamente',
                        )
                    );
                }
                catch(Exception $e){
                    DB::rollback();
                    return Response::json(
                        array(
                            'rst'=>2,
                            'msj'=>array($msj),
                        )
                    );
                    throw $e;
                }
            } else {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array(
                            'email'=>'Usuario ya existe como contacto'
                        ),
                    )
                );
            }
        }
    }

    /**
     * muestra linea de usuarios
     * POST / usuario/usuarioopciones
     * (usuarioopciones_ajax)
     */
    public function postUsuarioopciones()
    {
        if (Request::ajax()) {
            $query = "SELECT e.nombre AS empresa, u.id, u.`nombre`,
                    u.`apellido`, u.`usuario`, p.`nombre` AS perfil, u.`estado`
                    FROM `usuarios` u
                    LEFT JOIN `empresas` e
                    ON e.id = u.empresa_id
                    LEFT JOIN `perfiles` p
                    ON p.id = u.perfil_id
                    ORDER BY e.nombre DESC";

            $usuarioOpciones = DB::select($query);

            return Response::json(array('data' => $usuarioOpciones));
        }
    }


    /**
     * listado del Detalle Usuario (Módulo y SubMódulos)
     * POST / usuario/detaopciones
     * (usuarioopciones_ajax)
     */
    public function postDetaopciones()
    {
        if (Request::ajax()) {
            $queryModulo = "SELECT DISTINCT m.nombre AS modulo, m.id
                    FROM `modulos` m
                    INNER JOIN `submodulos` s
                    ON s.modulo_id = m.id
                    INNER JOIN `submodulo_usuario` su
                    ON su.submodulo_id = s.id
                    WHERE su.usuario_id  = '".Input::get('id')."'
                    ORDER BY m.nombre, s.nombre ASC";


            $querySubModulo = "SELECT m.nombre AS modulo,
                    s.nombre AS submenu, m.id, s.modulo_id
                    FROM `modulos` m
                    INNER JOIN `submodulos` s
                    ON s.modulo_id = m.id
                    INNER JOIN `submodulo_usuario` su
                    ON su.submodulo_id = s.id
                    WHERE su.usuario_id  = '".Input::get('id')."'
                    ORDER BY m.nombre, s.nombre ASC";

            $detaModOpciones = DB::select($queryModulo);
            $detaSubModOpciones = DB::select($querySubModulo);

            return Response::json(
                array(
                    'dataMod' => $detaModOpciones,
                    'dataSubMod' => $detaSubModOpciones
                )
            );
        }
    }

    /**
     * listas al usuario que creó/modificó/eliminó
     * la cuenta, perfil que le asignó, fecha y hora, estación.
     * POST / usuario/auditoriaacciones
     * (usuarioopciones_ajax)
    */
    public function postAuditoriaacciones()
    {
        if (Request::ajax()) {
            $query = "SELECT u.`usuario`, p.`nombre` AS perfil,
                    uc.`usuario` AS ucrea, u.`created_at` as fcreate,
                    uu.`usuario` AS uactualiza,
                    u.`updated_at` as fupdate, u.`usuario_estacion`
                    FROM `usuarios` u
                    LEFT JOIN `perfiles` p
                    ON p.`id` = u.`perfil_id`
                    LEFT JOIN `usuarios` uc
                    ON uc.`id` = u.`usuario_created_at`
                    LEFT JOIN `usuarios` uu
                    ON uu.`id` = u.`usuario_updated_at`";

            $detaAuditoria = DB::select($query);

            return Response::json(array('data' => $detaAuditoria));
        }
    }

    /**
     * relación de la fecha y hora, así como la estacion
     * del ultimo login de usuario.
     * POST / usuario/logeousuarios
     * (logeos_ajax)
     */
    public function postLogeousuarios()
    {
        if (Request::ajax()) {
            $query = "SELECT u.`nombre`, u.`apellido`, u.`usuario`,
                    u.`fecha_login`, u.`estacion`, u.`estado`
                    FROM usuarios u";

            $logeos = DB::select($query);

            return Response::json(array('data' => $logeos));
        }
    }

    /**
     * relación de usuarios que no presentan actividad, desde hace
     * 90 días
     * POST / usuario/detaidentificador
     * (usuarioopciones_ajax)
     */
    public function postDetaidentificador()
    {
        if (Request::ajax()) {
            $query = "SELECT e.`nombre` AS empresa, u.`nombre`,
                    u.`apellido`, u.`usuario`,
                    u.`estacion`, u.`fecha_login`, u.`created_at`,
                    DATEDIFF(NOW(), DATE(u.`created_at`)) AS difCreated,
                    DATEDIFF(NOW(), DATE(u.`fecha_login`)) AS difLogin
                    FROM `usuarios` u
                    INNER JOIN `empresas` e
                    ON e.id = u.empresa_id";

            $detaIdentificador = DB::select($query);

            return Response::json(array('data' => $detaIdentificador));
        }
    }

    /**
     * trar consigo un registro de la última fecha de login, y
     * el contador de errores que se tiene
     * POST / usuario/listarregistrosesion
     * (usuario_ajax)
     */
    public function postListarregistrosesion()
    {
        $query = "SELECT DATE(`fecha_login`) AS flogin,
                DATE(`fecha_error`) AS ferror, `contador_error`, `fecha_login`
                FROM `usuarios`
                WHERE `usuario` = '".Input::get('nomusuario')."'";

            $registroSesion = DB::select($query);

            return Response::json(array('data' => $registroSesion));
    }
}