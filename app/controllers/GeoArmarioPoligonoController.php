<?php
class GeoArmarioPoligonoController extends \BaseController
{
    public function postListar()
    {

        $r =GeoArmarioPoligono::postArmarioPoligonoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }

    //  zonal_mdf_armariopoligon
    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoArmarioPoligono::postMdfFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }

     public function postMdf()
     {
        $array['zonal']=Input::get('zonal');
        $array['mdf']=Input::get('mdf');
        $r =GeoArmarioPoligono::postArmarioFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

     }


}

