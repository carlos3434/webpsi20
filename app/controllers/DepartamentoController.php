<?php

class DepartamentoController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            try {
                if(Input::get('parametros')){
                    $result = DB::table('departamento')
                                ->select(
                                    'departamento_id as id',
                                    'nombre'
                                    )
                                ->orderBy('nombre')
                                ->get();
                }
            } catch (Exception $error) {
               $this->error->handlerError($error);
            }
            return Response::json(array('rst' => 1, 'datos' => $result));
        }
    }


}
