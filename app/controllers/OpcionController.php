<?php

class OpcionController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
            
            $opciones = Opcion::all();
        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=> $opciones
                )
            );
        }
    }
}
