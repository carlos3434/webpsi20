<?php
use Repositories\Lista\ListaRepoInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ListadoController
 *
 * @author Administrador
 */
class ListadoController extends \BaseController
{
    protected $listaRepo;
    public function __construct(ListaRepoInterface $listaRepo) 
    {
        $this->listaRepo = $listaRepo;
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf');
    }
    public function postProcess()
    {
        //$listaRepo = App('Repositories\Lista\ListaRepoInterface');
        $resultado = array();
        $data = array();
        if (Request::ajax() &&
            Input::has("controladores") && Input::has("data")) {

            $controladores = json_decode(Input::get("controladores"), true);
            $data = json_decode(Input::get("data"), true);
            $i = 0;
            //foreach ($controladores as $key => $value) {
                //if (isset($data[$i]) && strlen(trim($value)) > 0) {
                    //$resultado[$i] = Helpers::ruta($value."/listar", "POST", $data[$i], true);
                    $resultado[] = $this->listaRepo->actividad();
                    $resultado[] = $this->listaRepo->estadoofsc();
                    $resultado[] = $this->listaRepo->estado();
                    $resultado[] = $this->listaRepo->quiebre();
                    $resultado[] = $this->listaRepo->empresa();
                    $resultado[] = $this->listaRepo->celula();
                    $resultado[] = $this->listaRepo->tecnico();
                    $resultado[] = $this->listaRepo->zonal();
                    $resultado[] = $this->listaRepo->troba();
                    $resultado[] = $this->listaRepo->nodo();
                    $resultado[] = $this->listaRepo->mdf();
                    $resultado[] = $this->listaRepo->motivo();
                    $resultado[] = $this->listaRepo->submotivo();
                //}
                $i++;
            //}
        }
        return Response::json(
            array(
                'rst' => 1,
                'resultado' => $resultado,
            )
        );
    }

    public function postValid()
    {
        $usuario = new Usuario;
        $result = $usuario->getbandejap(Auth::user()->id);
        return Response::json($result);
    }

}
