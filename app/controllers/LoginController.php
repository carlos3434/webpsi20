<?php

class LoginController extends BaseController
{

    public function postLogin()
    {
        if ( Request::ajax() ) {

            $estadoLog = 0;
            $contadorFallo = 0;
            $user = Input::get('usuario');
            $identificador = DB::select('select count(*) as identificador from usuarios where usuario=?', array($user));

            if ($identificador[0]->identificador == 0) {
                $m='<strong>Usuario incorrecto</strong>';
                        return Response::json(
                            array(
                            'rst'=>'2',
                            'msj'=>''. $m .'',
                            )
                        );
            }

            //obtenemos los Números de Intentos realizados por el Usuario
            $numIntentos = DB::select('select intento from usuarios where usuario=?', array($user));

            if ($numIntentos[0]->intento<5) {
                $userdata= array(
                    'usuario' => Input::get('usuario'),
                    'password' => Input::get('password'),
                );

                if ( Auth::attempt($userdata, Input::get('remember', 0)) ) {
                    $query = "SELECT `fecha`, `estado`,
                    DATEDIFF(NOW(), DATE(`fecha`)) AS difDias
                    FROM `usuario_password`
                    WHERE `usuario` = ?
                    ORDER BY id DESC
                    LIMIT 1";

                    $detaAuditoria = DB::select($query, array($user));

                    // "undefined" => Si no hay movimiento en la tabla UsuarioPassowrd,
                    // quiere decir que no Presenta Password Modificado este Usuario.
                    if ( empty($detaAuditoria[0]->fecha) || $detaAuditoria[0]->fecha == "" || $detaAuditoria[0]->estado == 0 ) {
                    // if (false) {
                        return Response::json(
                            array(
                                'rst'=>'5','datos' => 'y'
                            )
                        );

                    } else {
                        // se encontró movimiento en la tabla UsuarioPassword,
                        // validamos el Tiempo de la última Modificacón del Password
                        // de dicho Usuario. (difDias > 35).
                        if ($detaAuditoria[0]->difDias > 120 && $detaAuditoria[0]->estado == 1) {
                        // if (false) {
                            return Response::json(
                                array(
                                    'rst'=>'5','datos' => 'X'
                                )
                            );

                        } else {
                            //buscar los permisos de este usuario y guardarlos en sesion

                        


                            $query="SELECT m.nombre as modulo, s.nombre as submodulo,
                                     s2.nombre as submodulo2, 
                                     s.menu_visible, s2.menu_visible as menu_visible2,
                                     su.agregar, su.editar, su.eliminar, 
                                     s2.agregar as agregar2, s2.editar as editar2, s2.eliminar as eliminar2, 
                                     CONCAT(m.path,'.', s.path) as path,
                                     CONCAT(m.path,'.', s.path,'.' , s2.path) as path2,
                                     m.icon,
                                     su.submodulo_id,
                                     s2.id as sub2_id
                                     FROM modulos m
                                     JOIN submodulos s ON m.id=s.modulo_id and s.parent=0
                                     JOIN submodulo_usuario su ON s.id=su.submodulo_id
                                     LEFT JOIN (
                                     select s.*,su.estado as estadop,su.agregar,su.editar,su.eliminar 
                                     from submodulos s
                                     JOIN submodulo_usuario  su on su.submodulo_id=s.id and s.parent<>0
                                     where  s.menu_visible=1 and su.usuario_id = ?) s2 on s2.parent=s.id
                                     WHERE su.estado = 1
                                     AND m.estado = 1 AND s.estado = 1
                                     and su.usuario_id = ?
                                     ORDER BY m.nombre, s.nombre, s2.nombre";                  


                            $res = DB::select($query, array(Auth::id(),Auth::id()));

                            $menu = array();
                            $accesos = array();
                            
                            foreach ($res as $data) {
                                $modulo = $data->modulo;
                                $submodulo = $data->submodulo;
                                $submodulo2 = $data->submodulo2;
                                //$accesos[] = $data->path;
                                array_push($accesos, $data->path);

                                if( empty($menu[$modulo]) ){ $i=0;}                            
                                if( $data->submodulo2 <> ''){
                                    $menu[$modulo][$submodulo][$submodulo][] = $data;
                                }                     
                                else{                                    
                                    $menu[$modulo][$i] = $data;
                                    $i++;                                                                      
                                }
                            }

                            $usuario = Usuario::find(Auth::id());

                            Session::set('language', 'Español');
                            Session::set('language_id', 'es');
                            Session::set('menu', $menu);
                            Session::set('accesos', $accesos);
                            Session::set('perfilId', $usuario['perfil_id']);
                            Session::set('nomusuario', Input::get('usuario'));
                            Session::set("s_token", md5(uniqid(mt_rand(), true)));

                            Lang::setLocale(Session::get('language_id'));

                            $estadoLog = 1;
                            $ip = $_SERVER['REMOTE_ADDR'];
                            DB::table('usuarios')
                                    ->where('usuario', '=', $user)
                                    ->update(
                                        array(
                                            'intento' => '0',
                                            'estacion' => $ip,
                                            'fecha_login' => date("Y-m-d H:i:s"),
                                            'estado_log' => $estadoLog
                                        )
                                    );

                            return Response::json(
                                array(
                                'rst'=>'1',
                                'estado'=>Auth::user()->estado
                                )
                            );
                        
                        }
                    }
                } else {
                    // Obtenemos el Contdor de Errores, de la última Fecha
                    // "contador_error" => Solo hace referencia al N° de Errores
                    $numFallos = DB::select(
                        'SELECT COUNT(*) AS total,
                        contador_error
                        FROM usuarios
                        WHERE usuario=?
                        AND DATE(`fecha_error`) = DATE(NOW())', array($user)
                    );

                    // Se reinicia el contador_error, si no se encuentra
                    // registro durante esta última fecha
                    if ( ($numFallos[0]->total) == 0 ) {
                        $contadorFallo = 1;
                    } else {
                        // Como hay datos en contador_error => le sumamos un
                        // intento mas
                        $contadorFallo = $numFallos[0]->contador_error + 1;
                    }

                    $estadoLog = 0;

                    DB::table('usuarios')
                            ->where('usuario', '=', $user)
                            ->update(
                                array(
                                    'intento' => $numIntentos[0]->intento + 1,
                                    'estacion' => $_SERVER['REMOTE_ADDR'],
                                    'fecha_error' => date("Y-m-d H:i:s"),
                                    'contador_error' => $contadorFallo,
                                    'estado_log' => $estadoLog
                                )
                            );

                    $m='<strong>Usuario</strong> y/o la '
                            . '<strong>contraseña</strong>';
                        return Response::json(
                            array(
                            'rst'=>'2',
                            'msj'=>'El '. $m .' son incorrectos.',
                            )
                        );
                }
            } else {

                // Obtenemos los minutos transcurridos, por causa de los 5
                // errores fallados
                $ultFecha = DB::select(
                    'select
                    TIMESTAMPDIFF(MINUTE,`fecha_error`, NOW()) AS minuto
                    from usuarios
                    where usuario=?', array($user)
                );

                // Validamos los 30 min de Conteo,
                // por fallo de todos sus Intentos
                if ( ($ultFecha[0]->minuto) < 30 ) {
                    $valor = 30 - $ultFecha[0]->minuto;


                    $m='<strong>'.$valor.'</strong>';
                        return Response::json(
                            array(
                            'rst'=>'2',
                            'msj'=>'Por favor, ingresar dentro de '
                                   . $valor .' minutos',
                            )
                        );

                } else {
                    $estadoLog = 0;

                    DB::table('usuarios')
                            ->where('usuario', '=', $user)
                            ->update(
                                array(
                                    'intento' => '0',
                                    'fecha_error' => date("Y-m-d H:i:s"),
                                    'estado_log' => $estadoLog
                                )
                            );

                    $m='<strong>Ya Transcurrió el Plazo de 30 min,'
                            . ' Por Favor Logeate</strong>';
                        return Response::json(
                            array(
                            'rst'=>'2',
                            'msj'=>''. $m .'',
                            )
                        );
                }
            }
        }
    }

    public function postImagen()
    {
        if (isset($_FILES['imagen']) and $_FILES['imagen']['size'] > 0) {

            $uploadFolder = 'img/user/' . md5('u' . Auth::user()->id);

            if ( !is_dir($uploadFolder) ) {
                mkdir($uploadFolder);
            }

            $nombreArchivo = $_FILES['imagen']['name'];
            $extArchivo = end((explode(".", $nombreArchivo)));
            $tmpArchivo = $_FILES['imagen']['tmp_name'];
            $archivoNuevo = "u".Auth::user()->id . "." . $extArchivo;
            $file = $uploadFolder . '/' . $archivoNuevo;

            @unlink($file);

            if (!move_uploaded_file($tmpArchivo, $file)) {
                $m='Ocurrio un error al subir el archivo. No pudo guardarse.';
                return Response::json(
                    array(
                    'upload' => false,
                    'rst'     => '2',
                    'msj'      => $m,
                    'error'  => $_FILES['archivo'],
                    )
                );
            }

            $usuario = Usuario::find(Auth::user()->id);
            $usuario->imagen = $archivoNuevo;
            $usuario->save();

            return Response::json(
                array(
                    'rst'        => '1',
                    'msj'        => 'Imagen subida correctamente',
                    'imagen'    => $file,
                    'upload'     => TRUE,
                    'data'         => "OK",
                    )
            );

        }
    }

    public function postMisdatosvalida()
    {
        if (Request::ajax()) {

            $user = Input::get('usuario', '');
            $newpassword = Input::get('newpassword', '');
            if ($newpassword != Input::get('confirm_new_password')) {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array('newpassword'=>'Las contraseñas nuevas no pueden ser diferentes'),
                    )
                );
            }

            $query = 'SELECT `usuario`, `id` FROM `usuarios` WHERE `usuario` = ? ';
            $usuario = DB::select($query, array($user));

            if ((strrev($newpassword) == ($usuario[0]->usuario)) || ($newpassword == $usuario[0]->usuario)) {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array('newpassword'=>'La contraseña no puede contener su nombre de usuario'),
                    )
                );
            }

            $reglas = array(
                'password'      => 'required|between:6,50',
                'newpassword'   => 'required|regex:"^(?=\w*\d)(?=\w*[a-z])\S{6,50}$"',
            );

            $validator = Validator::make(Input::all(), $reglas);

            if ($validator->fails()) {
                $final='';
                $datosfinal=array();
                $msj=(array) $validator->messages();

                foreach ($msj as $key => $value) {
                    $datos=$msj[$key];
                    foreach ($datos as $keyI => $valueI) {
                        $datosfinal[$keyI]=str_replace(
                            $keyI, trans('greetings.'.$keyI), $valueI
                        );
                    }
                    break;
                }

                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>$datosfinal,
                    )
                );
            }

            $userdata= array(
                'usuario' => Input::get('usuario'),
                'password' => Input::get('password'),
            );

            if ( Auth::attempt($userdata) ) {

                if ( $newpassword!='' ) {

                    $query = "SELECT count(*) as total
                            FROM `usuario_password`
                            WHERE `usuario` = ?
                            ORDER BY id DESC
                            LIMIT 1";
                    $validarRegistro = DB::select($query, array($user));

                    $query = "SELECT `password`,
                            TIMESTAMPDIFF(MINUTE,`fecha`, NOW()) AS hora,
                            estado
                            FROM `usuario_password`
                            WHERE `usuario` = ?
                            ORDER BY id DESC
                            LIMIT 3";

                    $validarNuevoPass = DB::select($query, array($user));

                    if ($validarRegistro[0]->total > 0) {
                        if ($validarNuevoPass[0]->estado == 1) {
                            if ($validarNuevoPass[0]->hora <= 60) {
                                return Response::json(
                                    array(
                                        'rst'=>3,
                                        'msj'=>'Intentar Cambiar Contraseña'
                                        . ' dentro de '
                                        . (60-($validarNuevoPass[0]->hora))
                                        . ' minutos.',
                                    )
                                );
                            }
                        }
                    }

                    foreach ($validarNuevoPass as $pass) {
                        if (Hash::check(
                            ($newpassword),
                            ($pass->password)
                        )) {
                            return Response::json(
                                array(
                                    'rst'=>2,
                                    'msj'=>array('newpassword'=>'Contraseña Usada Anteriormente'),
                                )
                            );
                        }
                    }

                    $usuarioModel = Usuario::find($usuario[0]->id);
                    $usuarioModel['password'] = Hash::make($newpassword);
                    $usuarioModel['usuario_updated_at'] = $usuario[0]->id;
                    $usuarioModel->save();

                    // Insertamos dentro de la tabla Usuario_password
                    DB::table('usuario_password')
                    ->insert(
                        array(
                            'usuario' => $user,
                            'password' => Hash::make($newpassword),
                            'fecha' => date("Y-m-d H:i:s"),
                            'estacion' => $_SERVER['REMOTE_ADDR'],
                            'estado' => '1'
                        )
                    );
                }

                return Response::json(
                    array(
                        'rst'=>1,
                        'msj'=>'Registro actualizado correctamente',
                    )
                );

            } else {
                return Response::json(
                    array(
                        'rst'=>2,
                        'msj'=>array('password'=>'Contraseña incorrecta'),
                    )
                );
            }
        }
    }
}