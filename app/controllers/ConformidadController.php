<?php

class ConformidadController extends  \BaseController
{


	public function postUploadexcel() {
        $data = array();
        $cabecera = array();
        $file = Input::file('excel');
        $tmpArchivo = $file->getRealPath();
        $name = $file->getClientOriginalName();
        $column = '';

        $origin_name = 'uploads/' . $name;
        $path = 'uploads/';
        $file->move($path, $name);

        $publicPath = strpos(public_path($path), '\\') === false?
            public_path($path) :
            str_replace('\\', '/', public_path($path));


        $objPHPExcel = PHPExcel_IOFactory::load($publicPath.$name);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        if ($column == '') {
            $highestColumn = $sheet->getHighestColumn();
        } else {
            $highestColumn = $column;
        }

        $val_quiebres =  Quiebre::Listbygroup();
        $val_atencion =  DB::table('pre_temporales_atencion')->select('id','nombre')->where('estado', '=', '1')->get();
        $val_fuente =   DB::table('pre_temporales_rrss')->select('id','nombre')->where('estado', '=', '1')->get();
        $val_averia =  DB::table('actividades_tipos AS a')->select('a.id','a.nombre',DB::raw('CONCAT("A",actividad_id) as relation'),'a.apocope')->where('a.apocope', '<>', '')->orderBy('a.nombre')->limit(4)->get();
        $errores = ['quiebre'=>0,'atencion'=>0,'fuente'=>0,'tipo_averia'=>0];
        for ($row = 1; $row <= $highestRow; $row++) {
            $count_vacio = 0;
            $fila = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            if($row > 1){
                $error = 0;
                foreach ($fila[$row - 1] as $key => $value) {
                    $count_vacio = ($value == '' or $value == NULL) ? $count_vacio + 1 : $count_vacio + 0;   
                    $fila[$row - 1][$cabecera[0][$key]] =  ($value == '' or $value == NULL) ? '' : $value;                   
                    unset($fila[$row - 1][$key]);
                    
                    /*validate if exits required fields*/
                    if($key == 0 && !Helpers::findValueIntoArray($val_quiebres,$value)){ //quiebre
                        $errores['quiebre']+=1;
                        $error+=1;
                    }else if($key == 1 && !Helpers::findValueIntoArray($val_atencion,$value)){ //atencion
                        $errores['atencion']+=1;
                        $error+=1;
                    }else if($key == 2 && !Helpers::findValueIntoArray($val_fuente,$value)){//fuente
                        $errores['fuente']+=1;
                        $error+=1;
                    }else if($key == 5 && !Helpers::findValueIntoArray($val_averia,$value,'apocope')){//tipo_averia
                        $errores['tipo_averia']+=1;
                        $error+=1;
                    }

                    if($error > 0){
                        $fila[$row - 1]['validate'] = $error;
                    }else{
                        $fila[$row - 1]['validate'] = 0; 
                    }

                    /*end validate if exits required fields*/
                }
                /*if its complete all fields so add to array*/
                if($count_vacio < count($fila[$row -1]) - 1){
                    $data[] = $fila[$row - 1];
                }                
            }else{
                $cabecera[] = $fila[$row -1];                    
            }
        }

        return Response::json(
            array(
                'rst' => ($data) ? 1 : 2,
                'data' => ($data) ? $data : '',
                'errors' => $errores
            )
        );
    }

    public function postProcesarexcel(){

    	if(Request::ajax()){
            $idevento = is_array(Input::get('evento_id')) ? Input::get('evento_id')['id'] : Input::get('evento_id');
            if(Input::has('pedidos')){

                $arrayFallo=0;
                $arrayOk = 0;     
                              
                $data = Input::get('pedidos');
                foreach ($data as $key => $value) {
                    $rst = (is_array($value)) ? $value : json_decode($value,true);

                    foreach ($rst as $index => $val) {

                        $obj = (!is_object($val)) ? (object) $val : $val;

                        DB::beginTransaction();
                        set_time_limit(0);
                        try{
                            $PreTemporal = new TemporalConformidad();
                            $PreTemporal->quiebre_id = $this->getIdByTable('quiebres',array('nombre'),array($obj->QUIEBRE));
                            $PreTemporal->tipo_atencion = $this->getIdByTable('pre_temporales_atencion',array('nombre'),array($obj->ATENCION));
                            $PreTemporal->tipo_fuente = $this->getIdByTable('pre_temporales_rrss',array('nombre'),array($obj->FUENTE));
                            $PreTemporal->actividad_tipo_id = $this->getIdByTable('actividades_tipos',array('apocope'),array($obj->TIPOAVERIA));
                            $PreTemporal->codactu = $obj->CODACTU;
                            $PreTemporal->codcli = $obj->CODCLI;
                            $PreTemporal->cliente_nombre = $obj->CLIENTENOMBRE;
                            $PreTemporal->cliente_celular = $obj->CLIENTECELULAR;
                            $PreTemporal->cliente_telefono = $obj->CLIENTETELEFONO;
                            $PreTemporal->cliente_correo =$obj->CLIENTECORREO ;
                            $PreTemporal->cliente_dni = $obj->CLIENTEDNI;
                            $PreTemporal->contacto_nombre = $obj->CONTACTONOMBRE;
                            $PreTemporal->contacto_celular = $obj->CONTACTOCELULAR;
                            $PreTemporal->contacto_telefono = $obj->CONTACTOTELEFONO;
                            $PreTemporal->contacto_correo = $obj->CONTACTOCORREO;
                            $PreTemporal->contacto_dni = $obj->CONTACTODNI;
                            $PreTemporal->embajador_nombre = $obj->EMBAJADORNOMBRE;
                            $PreTemporal->embajador_correo = $obj->EMBAJADORCORREO;
                            $PreTemporal->embajador_celular= $obj->EMBAJADORCELULAR;
                            $PreTemporal->embajador_dni = $obj->EMBAJADORDNI;
                            $PreTemporal->comentario=$obj->COMENTARIO;
                            $PreTemporal->gestion='masiva';

                            if($idevento){
                                $PreTemporal->evento_masiva_id = $idevento;
                            }

                            $PreTemporal->created_at=date('Y-m-d H:i:s');
                            $PreTemporal->save();

                            if(!$PreTemporal->id){                        
                                $arrayFallo+=1;
                            }else{
                                $arrayOk+=1;
                            }                        
                        } catch (Exception $exc) {
                            $errorController = new ErrorController();
                            $errorController->saveError($exc);                   
                        }                  
                        DB::commit();                        
                    }
                }
                return Response::json(
                    array(
                        'rst'       => '1',
                        'msj'       => 'registrados correctamente',
                        'fallo'    => $arrayFallo,
                        'registrados'    => $arrayOk
                    )
                );
            }
        }
    }

    public function getIdByTable($table,$filter = array(),$search = array()){
        if($table && $filter && $search){
            $sql = "";
            $sql.= "SELECT id FROM $table";

            if($filter && $search){
                $sql.= " WHERE ";
                foreach ($filter as $key => $value) {
                    $sql.= ($key == 0) ? "$value='".$search[$key]."'" : " AND $value='".$search[$key]."'";                                                    
                }
            }
            $result = DB::select($sql);        
            return $result[0]->id;            
        }
    }

    public function postReporte()
    {
        $filtros = [];
        $filtro = Input::get("slct_filtro", 1);
        $fecha = explode(" - ", Input::get("fecha_registro"));
        $ticket = Input::get("numticket");
        $dniCliente = Input::get("dni_cliente");
        $telefonoCliente = Input::get("telefono_cliente");
        $fecha[0].=" 00:00:00";
        $fecha[1].=" 23:59:59";
        $flag = Input::get("flag", 1);
        $data = [];

        if ($filtro == 1) {
            $tipo_filtro = "tc.created_at";
        } else {
            $tipo_filtro = "ptm.created_at";
        }
        //case: 1 por fecha de ticket
        switch ($flag) {
            case 1:
                $data = DB::table("temporal_conformidad as tc")
                ->select(
                    [
                    DB::raw('CONCAT("E",tc.id) as ticket'),
                    "tc.tipo_atencion", "a.nombre as actividad",
                    "tc.tipo_fuente", "tc.gestion",
                    "tc.codactu", "tc.codcli",                   
                    "tc.cliente_nombre", "tc.cliente_celular",
                    "tc.cliente_telefono", "tc.cliente_correo",
                    "tc.cliente_dni", "tc.contacto_nombre",
                    "tc.contacto_celular", "tc.contacto_telefono",
                    "tc.contacto_correo", "tc.contacto_dni",
                    "tc.embajador_nombre", "tc.embajador_correo",
                    "tc.embajador_celular", "tc.embajador_dni",
                    "tc.comentario", "q.nombre as quiebre",
                    "ept.nombre as estado",
                     DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                     'em.nombre as evento_masiva'
                    ]
                )
                ->leftJoin("quiebres as q", "q.id", "=", "tc.quiebre_id")
                ->leftJoin(
                    "estados_pre_temporales as ept",
                    "ept.id",
                    "=",
                    "tc.estado_pretemporal_id"
                )
                ->leftJoin("actividades as a", "a.id", "=", "tc.actividad_id")
                ->leftJoin("eventos_masiva as em", "em.id", "=", "tc.evento_masiva_id")
                ->whereBetween("tc.created_at", $fecha);

                if ($ticket!=="") {
                    $array = explode("E", $ticket);
                    if (count($array)>1) {
                        $ticketId = $array[1];
                        $data->where('tc.id', '=', (int)$ticketId);
                    }
                }
                if ($dniCliente!=="") {
                    $data->where("tc.cliente_dni", "LIKE", "%".$dniCliente."%");
                }
                if ($telefonoCliente!="") {
                    $data->where(
                        "tc.cliente_telefono",
                        "LIKE",
                        "%".$telefonoCliente."%"
                    );
                }

                if(Input::get('slct_quiebre')){
                    $data->where('tc.quiebre_id', '=', Input::get('slct_quiebre'));
                }
                $data = $data->get();
                $nameFile = "Tickets";
                break;
            case 2:
                $data = DB::table('temporal_conformidad as tc')
                ->select(
                    DB::raw('CONCAT("E",tc.id) as ticket'),
                    'at.apocope as tipo_averia',
                    'pa.nombre as tipo_atencion',
                    'q.nombre as quiebre',
                    "ept.nombre as EstadoTicket",
                    'tc.codactu as CodAveria',
                    'tc.codcli',
                    'tc.cliente_nombre as ClienteNombre',
                    'tc.cliente_celular as ClienteCelular',
                    'tc.cliente_telefono as ClienteTelefono',
                    'tc.cliente_dni as ClienteDNI',
                    'tc.embajador_nombre as EmbajadorNombre',
                    'tc.embajador_celular as EmbajadorCelular',
                    'tc.embajador_correo as EmbajadorCorreo',
                    'tc.embajador_dni as EmbajadorDNI',
                    'tc.comentario as ComentarioTicket',
                     DB::raw(
                        '
                            DATE_FORMAT(tc.created_at,"%d-%m-%y %H:%i:%s") 
                            as FechaRegistro'
                    ),
                    DB::raw(
                        '
                            DATE_FORMAT(tc.created_at,"%d-%m-%y") 
                            as FechaRegistroTicket'
                    ),
                    DB::raw(
                        '
                            DATE_FORMAT(tc.created_at,"%H:%i:%s") 
                            as HoraRegistroTicket'
                    ),
                    DB::raw('IFNULL(pts.accion," ") as TipoSolucion'),                              
                    DB::raw('IFNULL(m.nombre," ") as Motivos'),
                    DB::raw('IFNULL(s.nombre," ") as Submotivos'),
                    DB::raw('IFNULL(e.nombre,"Recepcionado") as Estados'),
                    'ptm.created_at as FechaMov',
                    'ptm.observacion as ObsMov',
                    DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                    'pts.producto as Producto',
                    'pts.accion as Accion',
                    'pta.estado_legado as EstadoLegado',
                    'pta.fecha_registro_legado as FechaRegistroLegado',
                    'pta.fecha_liquidacion as FechaLiquidacion',
                    'pta.cod_liquidacion as CodigoLiquidacion',
                    'pta.detalle_liquidacion as DetalleLiquidacion',
                    'pta.area as Area',
                    'pta.contrata as Contrata',
                    'pta.zonal as Zonal',                   
                    DB::raw(
                        '(CASE ptm.id WHEN (
                            SELECT MAX(ptm2.id)
                            FROM temp_conformidad_mov as ptm2
                            WHERE ptm2.temporal_conformidad_id = ptm.temporal_conformidad_id
                        )
                        THEN "X" ELSE "" END) as ultimo_movimiento'
                    ),
                    'em.nombre as evento_masiva',
                    DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado')
                )
                ->leftjoin(
                    'temp_conformidad_mov as ptm',
                    'ptm.temporal_conformidad_id',
                    '=',
                    'tc.id'
                )
                ->join(
                    'actividades_tipos as at',
                    'tc.actividad_tipo_id',
                    '=',
                    'at.id'
                )
                ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
                ->Leftjoin('quiebres as q', 'tc.quiebre_id', '=', 'q.id')
                ->leftjoin('motivos as m', 'm.id', '=', 'ptm.motivo_id')
                ->leftjoin('submotivos as s', 's.id', '=', 'ptm.submotivo_id')
                ->leftjoin('estados as e', 'e.id', '=', 'ptm.estado_id')
                ->leftjoin('usuarios as u1', 'u1.id', '=', 'tc.usuario_id')
                ->leftjoin(
                    'estados_pre_temporales as ept', 'ept.id',
                    '=',
                    'ptm.estado_pretemporal_id'
                )
                ->leftjoin(
                    'pre_temporales_solucionado as pts',
                    'pts.id', '=',
                    'ptm.pre_temporal_solucionado_id'
                )
                ->join(
                    'pre_temporales_atencion as pa',
                    'pa.id',
                    '=',
                    'tc.tipo_atencion'
                )
                ->Leftjoin(
                    'temp_conformidad_averia as pta',
                    'pta.temporal_conformidad_id', '=',
                    'tc.id'
                )
                ->leftJoin("eventos_masiva as em", "em.id", "=", "tc.evento_masiva_id")
                ->where(function($query){
                    if(Input::get('slct_quiebre')){
                        $query->whereIn('tc.quiebre_id',Input::get('slct_quiebre'));
                    }
                    if(Input::get('slct_motivo')){
                        $query->whereIn('ptm.motivo_id',Input::get('slct_motivo'));
                    }
                    if(Input::get('slct_submotivo')){
                        $query->whereIn('ptm.submotivo_id',Input::get('slct_submotivo'));
                    }
                })
                ->whereBetween($tipo_filtro, $fecha)
                ->get();
                
                $nameFile = "Movimientos";
                break;
            default:
                break;
        }
        $data = json_decode(json_encode($data), true);

        if (empty($data) > 0) {
            $data = array(
                0 => array('Movimientos' => 'No se encontró ninguno movimiento.')
            );
        }

        /*if (Input::get("tipo") == 'csv') {*/
            return Helpers::exportArrayToCsv($data, $nameFile);
        /*} else {
            return Helpers::exportArrayToExcelHTML($data, $nameFile);
        }*/
    }


    /*public function postVisor(){
        $estadoId = Input::get('estado_id');
        $fechas = Input::get('rango_fechas');
        $fecha_fin =  date('Y-m-d');
        $fechas_diff = 7;       
        

        if($fechas != ''){
            $fechas = explode(' - ', $fechas);
            $fecha_ini = $fechas[0];
            $fecha_fin = $fechas[1];
            $fechas_diff = round( (strtotime($fechas[1]) - strtotime($fechas[0])) /86400);
        }
        $fecha_max = $fechas_diff + 1;
        $liquidado = '';
        $tipo = '';

        $query = DB::table('pre_temporales_ultimos as ptu')
                    ->leftjoin('motivos as m', 'm.id', '=', 'ptu.motivo_id')
                    ->select(
                        DB::raw(
                            'COUNT(DISTINCT(ptu.submotivo_id)) as row_count'
                        ),  
                        'm.nombre as motivo',
                        DB::raw('COUNT((ptu.submotivo_id)) as total_motivo'),
                        'ptu.motivo_id'
                    )->where('ptu.estado_id', $estadoId);

        if ($estadoId == 7) {
            $liquidado = ' AND ptu.motivo_id <> 22 ';
            $tipo = 1;
            $query->whereRaw('ptu.motivo_id <> 22');
        } else {
            $liquidado = ' OR ptu.motivo_id = 22 ';
            $tipo = 2;
            $query->orwhere('ptu.motivo_id', '=', 22);
        }

        $motivo = $query->groupBy('ptu.motivo_id')
                       ->orderBy('m.created_at', 'asc')
                       ->get();

        $query_syntax = '';
        for($j=0; $j<=$fechas_diff; $j++){
           $query_syntax.= 'IF(count(pt'.$j.'.id) = 0,"",count(pt'.$j.'.id)) as "f'.$j.'",';      
        }
             
        $query = 'select m.nombre,ptu.motivo_id, ptu.submotivo_id,s.nombre as submotivo, '.$query_syntax.' count(pt'.$fecha_max.'.id) as "total_submotivo" 
                from pre_temporales_ultimos ptu 
                inner join motivos m on m.id = ptu.motivo_id 
                inner join submotivos s on s.id = ptu.submotivo_id 
                inner join pre_temporales pt on pt.id=ptu.pre_temporal_id';

                for($i=0; $i<=$fechas_diff; $i++){
                    if(Input::get('quiebre_id')){
                        $query.=" left join pre_temporales pt$i on pt$i.id=ptu.pre_temporal_id and date(pt$i.created_at)=date(date_sub('".$fecha_fin."', INTERVAL $i DAY)) and pt$i.quiebre_id=".Input::get('quiebre_id');                        
                    }else{
                        $query.=" left join pre_temporales pt$i on pt$i.id=ptu.pre_temporal_id and date(pt$i.created_at)=date(date_sub('".$fecha_fin."', INTERVAL $i DAY))";                                
                    }
                } 
                if(Input::get('quiebre_id')){
                    $query.=" left join pre_temporales pt".$fecha_max." on pt".$fecha_max.".id=ptu.pre_temporal_id and date(pt".$fecha_max.".created_at) BETWEEN  date(date_sub('".$fecha_fin."', INTERVAL $fechas_diff DAY)) AND date(date_sub('".$fecha_fin."', INTERVAL 0 DAY)) and pt".$fechas_diff.".quiebre_id=".Input::get('quiebre_id');
                }else{
                    $query.=" left join pre_temporales pt".$fecha_max." on pt".$fecha_max.".id=ptu.pre_temporal_id and date(pt".$fecha_max.".created_at) BETWEEN  date(date_sub('".$fecha_fin."', INTERVAL $fechas_diff DAY)) AND date(date_sub('".$fecha_fin."', INTERVAL 0 DAY))";                
                }
                
        $query.=' where ptu.estado_id = ? '.$liquidado.'  
                group by ptu.motivo_id, ptu.submotivo_id
                order by m.created_at asc';



        $submotivo = DB::select($query, array($estadoId));
        dd(DB::getQueryLog());
        return Response::json(
            array(
                "rst" => 1,
                'motivo' => $motivo,
                'submotivo' => $submotivo,
                'estado_id' => $estadoId,
            )
        );
    }*/


    /**
     * consulta de pretemporales ara el visor
     */
    public function postVisorRecepcionados()
    {
        $query = 'SELECT "Recepcionados" as nombre,
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 0 DAY)) 
                        then 1 else null  end), " ") as "f0",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 1 DAY)) 
                        then 1 else null  end), " ") as "f1",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 2 DAY)) 
                        then 1 else null  end), " ") as "f2",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 3 DAY)) 
                        then 1 else null  end), " ") as "f3",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 4 DAY))
                        then 1 else null  end), " ") as "f4",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 5 DAY)) 
                        then 1 else null  end), " ") as "f5",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 6 DAY)) 
                        then 1 else null  end), " ") as "f6",
                 IFNULL(sum(case date(p.created_at) 
                 when date(date_sub(NOW(), INTERVAL 7 DAY)) 
                        then 1 else null  end), " ") as "f7",
                 IFNULL(sum(case when date(p.created_at) 
                        between date(date_sub(NOW(), INTERVAL 7 DAY))  
                        and  date(NOW()) then 1 else null end), " ") as total
                 from pre_temporales p
                 where p.estado_pretemporal_id = 1';

        if(Input::get('quiebre_id')){
            $query.= ' AND p.quiebre_id='.Input::get('quiebre_id');
        }

        $queryDos = "SELECT
                (
                    SELECT IFNULL(sum(case when p.created_at
                    between date_sub(NOW(), INTERVAL 10 MINUTE)
                    and NOW() then 1 else 0 end), 0)
                ) as 'diez',
                (
                    SELECT IFNULL(sum(case when p.created_at
                    between date_sub(NOW(), INTERVAL 30 MINUTE)
                    and date_sub(NOW(), INTERVAL 10 MINUTE) then 1 else 0 end), 0)
                ) as 'treinta',
                (
                    SELECT IFNULL(sum(case when p.created_at
                    between DATE_FORMAT(NOW(), '%Y-%m-%d %00:00:00') 
                    and date_sub(NOW(), INTERVAL 30 MINUTE) then 1 else 0 end), 0)
                ) as 'sesenta'
                from pre_temporales p
                where p.estado_pretemporal_id = 1";

        if(Input::get('quiebre_id')){
            $queryDos.= ' AND p.quiebre_id='.Input::get('quiebre_id');
        }

        $data = DB::select($query);
        $horas = DB::select($queryDos);
        return Response::json(
            array(
                "rst" => 1,
                'recepcionados' => $data,
                'recepcionados_horas' => $horas[0]
            )
        );
    }
    


    public function postRegistrarmov(){
        if(Request::ajax()){
            $rgx =  array("/","_","-","#","$");
            $actividadTipoId = Input::get('tipo_averia');
            $preTemporalAveria['pre_temporal_id'] = Input::get('id');
            $preTemporalAveria['codcli']          = Input::get('codcli');
            $preTemporalAveria['motivo_id']       = Input::get('motivo');
            $preTemporalAveria['submotivo_id']    = Input::get('submotivo');
            $preTemporalAveria['estado_id']       = Input::get('estado');
            $preTemporalAveria['observacion']     = Input::get('observacion');
            //$preTemporalAveria['solucionado_id']  = '';

            //cuando encuentra averia en legados
            if (Input::has('codactu_val')) {
                $preTemporalAveria['codactu'] =Input::get('codactu_val');
                list($preTemporalAveria['codactu']) = explode('|', Input::get('codactu_val'));
                $preTemporalAveria['proceso'] = 2;
            }
            //if (Input::has('solucionado')) {
            if (Input::has('solucionado') && Input::get('solucionado')!='' ) {
                $preTemporalAveria['solucionado_id'] =Input::get('solucionado');
            } else {
                $preTemporalAveria['solucionado_id'] =0;  //dd(Input::get('solucionado'));
            }

            //se registra averia
            if (Input::has('codactu') && Input::get('codactu') != ''
                && Input::get('submotivo')== 60) {
                $preTemporalAveria['codactu'] = str_replace($rgx,'',Input::get('codactu'));
                $preTemporalAveria['proceso'] = 1;
            }
            //tiene reg averia
            if (Input::has('codactu') && Input::get('codactu') != ''
                && Input::get('submotivo')== 75) {
                $preTemporalAveria['codactu'] =Input::get('codactu');
                $preTemporalAveria['proceso'] = 1;
            }

            //estado ticket
            switch (Input::get('motivo')) {
                case '23':
                case '24':
                case '25': 
                case '18': $preTemporalAveria["estado_pretemporal_id"] = 2;
                    break;
                case '19': $preTemporalAveria["estado_pretemporal_id"] = 3;
                    break;
                case '20': $preTemporalAveria["estado_pretemporal_id"] = 4;
                    break;
                case '21': $preTemporalAveria["estado_pretemporal_id"] = 6;
                    break;
                case '22': $preTemporalAveria["estado_pretemporal_id"] = 5;
                    break;
                default: $preTemporalAveria["estado_pretemporal_id"] = 1;
                    break;
            }
            Input::merge(array('estado_pretemporal_id' => $preTemporalAveria["estado_pretemporal_id"]));
            
            //Input::replace($preTemporalAveria);

            //submotivo: ticket con averia
            if (Input::has('codactu_val')) {
                list($codactu,$codcli,$fecha ) = explode('|', Input::get('codactu_val'));
                TemporalConformidad::findOrFail(Input::get('id'))->update([
                    'codactu'                => $codactu,
                    /*'codcli'                 => $codcli,*/
                    'fecha_registro_legado'  => $fecha,
                    'validado'               => 1,
                ]);

                $averia=TempConformidadAveria::where('temporal_conformidad_id',Input::get('id'));                
                if ($averia == null) {                    
                    TempConformidadAveria::create($preTemporalAveria);
                } else {
                    $legados = $this->buscarLegados(
                        $actividadTipoId, Input::get('codactu'), Input::get('codcli')
                    );

                    if (count($legados) > 0) { $legado = $legados[0];
                        $averia->update([
                            'codactu'               => $legado->codactu,
                            'estado_legado'         => $legado->estado,
                            'fecha_registro_legado' => $legado->fecha_registro,
                            'fecha_liquidacion'     => Input::get('fecha_liquidacion'),
                            'cod_liquidacion'       => Input::get("cod_liquidacion", ''),
                            'detalle_liquidacion'   => Input::get("detalle_liquidacion", ''),
                            'codcli'                => $legado->telefono,
                            'nombre_cliente'        => Input::get("nombre_cliente", ''),
                            'area'                  => $legado->area,
                            'contrata'              => Input::get("contrata", ''),
                            'zonal'                 => Input::get("zonal", ''),
                            'proceso'               => 1,
                        ]);
                    }
                }
            }
            //submotivo: 60: Se registra averia | 75: tiene reg avgeria
            if (Input::has('codactu') && Input::get('submotivo')== 60 || Input::has('codactu') && Input::get('submotivo')== 75) {
                TemporalConformidad::findOrFail(Input::get('id'))->update([
                    'codactu' => str_replace($rgx,'',Input::get('codactu'))
                ]);

                $averia = DB::table('temp_conformidad_averia')
                    ->where('temporal_conformidad_id', Input::get('id'))
                    ->get();

                if ($averia == null) {
                    TempConformidadAveria::create($preTemporalAveria);
                }else{
                     TempConformidadAveria::findOrFail($averia[0]->id)->update([
                        'codactu'    => str_replace($rgx,'',Input::get('codactu')),
                        'codcli'     => Input::get('codcli'),
                        'proceso'    => 1,
                    ]);
                }
            }
            $result = TempConformidadMov::crearMovimiento();

            if ($result == 0) {
                return Response::json(
                    array(
                        'rst'    => 0,
                        'ticket' => Input::get('id'),
                        'msj'    => "No se puedo completar el movimiento.",
                    )
                );
            }

            /*if ticket its part of a event, we have to validate if it have already achieve their porcenture */
            /*$successfully = 0;
            if(Input::get('idevento')){
                $successfully = $this->calcCumpleEvento(Input::get('idevento'));
            }*/
            /*end if*/

            return Response::json(
                array(
                    'rst'    => 1,
                    'ticket' => Input::get('id'),
                    'msj'    => "Movimiento creado",
                    //'achieve' => $successfully,
                )
            );
        }
    }


    public function buscarLegados($actividadTipoId, $codactu, $codcli)
    {
        $tipoId = $actividadTipoId;
        $result  = '';
        /*
        *tipo_id = 4 (television|CATV), 5 (telefono|STB), 6 (internet|ADSL)
        *1 -> where es AND
        *0 -> where es OR
        */
        //television | CATV
        if ($tipoId == 4) {
            $result = PreTemporal::getAveriaPenCatvPais(
                $codactu, $codcli, 1
            );
            if (count($result) == 0) {
                $result = PreTemporal::getAveriaPenCatvPais(
                    $codactu, $codcli, 0
                );
            }

            if (count($result) == 0) {
                $result = PreTemporal::getAveriaPendMacro($codactu,$codcli,1);
            }
        } elseif ($tipoId == 5) {
            if ($codcli) {
                $result = PreTemporal::getAveriaPenAdslPais(
                    $codactu, $codcli, 0
                );
                //si tiene 1 adelante, buscar en Lima, sino en provincias
                if (count($result) == 0) {
                    if (substr($codcli, 0, 1) == 1) {
                        $telefono = substr($codcli, 1);
                        $result = PreTemporal::getAveriaPenBasLima(
                            $codactu, $telefono, 1
                        );
                        if (count($result) == 0) {
                            $result = PreTemporal::getAveriaPenBasLima(
                                $codactu, $telefono, 0
                            );
                        }
                    } else {
                        $telefono = substr($codcli, 2);
                        $result = PreTemporal::getAveriaPenBasProv(
                            $codactu, $telefono, 1
                        );
                        if (count($result) == 0) {
                            $result = PreTemporal::getAveriaPenBasProv(
                                $codactu, $telefono, 0
                            );
                        }
                    }
                    if (count($result) == 0) {
                        $telefono = $codcli;
                        $result = PreTemporal::getAveriaPenBasLima(
                            $codactu, $telefono, 1
                        );
                        if (count($result) == 0) {
                            $result = PreTemporal::getAveriaPenBasLima(
                                $codactu, $telefono, 0
                            );
                        }
                        if (count($result) == 0) {
                            $result = PreTemporal::getAveriaPenBasProv(
                                $codactu, $telefono, 1
                            );
                        }
                        if (count($result) == 0) {
                            $result = PreTemporal::getAveriaPenBasProv(
                                $codactu, $telefono, 0
                            );
                        }
                    }
                }
            } else {
                $telefono = $codcli;
                $result = PreTemporal::getAveriaPenBasLima(
                    $codactu, $telefono, 0
                );
                if (count($result) == 0) {
                    $result = PreTemporal::getAveriaPenBasProv(
                        $codactu, $telefono, 0
                    );
                }
            }
        } elseif ($tipoId == 6) { //internet
            $result = PreTemporal::getAveriaPenAdslPais(
                $codactu, $codcli, 1
            );
            
            if (count($result) == 0) {
                $result = PreTemporal::getAveriaPenAdslPais(
                    $codactu, $codcli, 0
                );
            }

            if (count($result) == 0) {
                $result = PreTemporal::getAveriaPendMacro($codactu,$codcli,1);
            }
        }
        return $result;
    }

    public function postValidarlegados()
    {
         $reglas = array(
            'id'  => 'required|numeric'
        );
        $mensaje = array(
            'required'      => 'no se ingreso :attribute'
        );
        $validator = \Validator::make(\Input::all(), $reglas, $mensaje);

        if ($validator->fails()) {
            return \Response::json(
                array(
                'rst' => 2,
                'msj' => $validator->messages()->all()[0]
                )
            );
        }
        $preTemporal=PreTemporal::findOrFail(Input::get('id'));
        if ($preTemporal->validado == 0) {
            $result = $this->buscarLegados(
                $preTemporal->actividad_tipo_id, 
                $preTemporal->codactu,
                $preTemporal->codcli
            );
            if (count($result) > 0) {
                return Response::json(['data' => $result,'rst' => 1]);
            }
        }
        return Response::json(['rst' => 2]);
    }

    public function postValidarpermiso()
    {
        $ticket = Input::get('busqueda');
        $authId = Auth::id();
        if (Redis::get($ticket)) {
            $val = json_decode(Redis::get($ticket));
            if ($val->id != Auth::id()) {
                return Response::json(
                    array(
                        "rst" => 1,
                        "type" => "1|".$val->user
                    )
                );
            }
        } else{
            $value = json_encode(['id'=>$authId,'user'=>Session::get('full_name') ]);
            Redis::set($ticket, $value);
            Redis::expire($ticket, 15*60);
        }

        return Response::json(
            array(
                "rst" => 1,
                "type" => "0"
            )
        );
    }

    public function postListarmovimiento()
    {
        $id = Input::get('busqueda');
        $data = TempConformidadMov::listar_movimientos($id);
        $datos = array(
            'rst' => 1,
            'data' => $data
        );
        return Response::json($datos);
    }

    public function postFinalizarpermiso()
    {
        $id = Input::get('busqueda');
        $type = Input::get('type');
        if (Redis::get($id)) {
            if ($type == 0) {
                Redis::del($id);
                return Response::json(
                    array(
                        "rst" => 1
                    )
                );
            }
        }
        return Response::json(
            array(
                "rst" => 1
            )
        );
    }

        public function getCargar()
    {
        $reglas = array(
            'tipo'        => 'Required|in:ticket,codactu',
            'busqueda'    => 'Required',
        );
        $validator = Validator::make(Input::all(), $reglas);
        if ($validator->fails()) {
            return Response::json(
                array(
                'rst' => 0,
                'msj' => $validator->messages(),
                )
            );
        }
        $tipoBusqueda = Input::get('tipo');
        $busqueda = Input::get('busqueda'); //cod averia o n°ticket
        $ficha = [];

        if ($tipoBusqueda == 'ticket') {
            $ficha = TemporalConformidad::buscarTicket(strtoupper($busqueda));
            if (count($ficha)<=0) {
                return array(
                    'rst' => 0,
                    'msj' => 'No se encontraron datos.'
                );
            } //dd($ficha);
            $fecha=$ficha->fecha_registro_legado;
            $segundos=strtotime($fecha) - strtotime('now');
            $diferencia=abs(intval($segundos/60/60));
            $plazo = 'Fuera del Plazo';
            if ($diferencia <= 24) {
                $plazo = 'Dentro del Plazo';
            }
            $ficha->plazo = $plazo;
            $ficha->origen = '';
            $movimientos = TempConformidadMov::listar_movimientos($busqueda);
            return Response::json([
                'rst' => 1,
                'tipo' =>'i',
                'ficha' => $ficha,
                'movimientos' => $movimientos,
                'msj' => 'Busqueda Individual.'
            ]);
        } elseif ($tipoBusqueda == 'codactu') {
            //cargo la ficha
            $datos = DB::table('pre_temporales as p')
                    ->where('p.codactu', $busqueda)
                    ->select('p.*, at.nombre')
                    ->join(
                        'actividades_tipos as at',
                        'at.id',
                        '=',
                        'p.actividad_tipo_id'
                    )
                    ->get();
            if (count($datos) >0) 
                $ficha = $datos[0];
            return Response::json([
                'rst' => 1,
                'tipo' =>'i',
                'ficha' => $ficha,
                'msj' => 'Busqueda Individual.'
            ]);
        }
    }

    public function  postBuscar(){
        $post = array();
        $post = Input::all();
        foreach ($post as $k => $value) {
            $bk = array("slct_","txt_","chk_","rdb_","_modal");
            $rk = array("","","","","");
            $k = str_replace($bk, $rk, $k);
            $post[$k] = $value;
        }
        Input::replace($post);
        $tipoBusqueda = Input::get('tipo');
        $busqueda = Input::get('busqueda'); //dni de embajador
        if (Input::get('buscar')) {
            $busqueda = Input::get('buscar');
        }
        $PG = Input::get('PG'); //busqueda personalizada o general

        $query = DB::table('temporal_conformidad as p')
        ->Leftjoin('actividades as a', 'a.id', '=', 'p.actividad_id')
        ->join('actividades_tipos as at', 'p.actividad_tipo_id', '=', 'at.id')
        ->Leftjoin('quiebres as q', 'p.quiebre_id', '=', 'q.id')
        ->Leftjoin(
            'temp_conformidad_ultimo as ptu',
            'ptu.temporal_conformidad_id',
            '=',
            'p.id'
        )
        ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
        ->Leftjoin('motivos as m', 'ptu.motivo_id', '=', 'm.id')
        ->Leftjoin('submotivos as s', 'ptu.submotivo_id', '=', 's.id')
        ->Leftjoin(
            'pre_temporales_solucionado as pts',
            'pts.id', '=',
            'ptu.pre_temporal_solucionado_id'
        )
        ->Leftjoin(
            'temp_conformidad_averia as pta',
            'pta.temporal_conformidad_id', '=',
            'p.id'
        )
        ->join(
            'estados_pre_temporales as ep',
            'ep.id',
            '=',
            'p.estado_pretemporal_id'
        )
        ->Leftjoin('eventos_masiva as em', 'em.id', '=', 'p.evento_masiva_id')
        ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'p.tipo_atencion')       
        ->Leftjoin('usuarios as u', 'ptu.usuario_created_at', '=', 'u.id')
        ->Leftjoin('usuarios as u1','p.usuario_id', '=', 'u1.id')
        ->join(
            'quiebre_grupo_usuario as qgu',
            function ($join) {
                $join->on('q.quiebre_grupo_id', '=', 'qgu.quiebre_grupo_id')
                ->where('qgu.usuario_id', '=', Auth::id())
                ->where('qgu.estado', '=', '1');
            }
        );

        /*if(!Input::get('eventos')){
           $query = $query->Leftjoin('eventos_masiva as em',
                function($leftjoin){
                    $leftjoin->on('em.id','=','p.evento_masiva_id')
                    ->where('em.estado','<>','2');            
                    
                }
            );
        }*/


        if (Input::get('excel')) {
            $query->select(
                DB::raw('CONCAT("E",p.id) as Ticket'),
                'at.apocope as TipoAveria',
                'pa.nombre as Atencion',
                'q.nombre as Quiebre',
                'p.codactu as CodAveria',
                'p.codcli',
                'p.cliente_nombre as ClienteNombre',
                'p.cliente_celular as ClienteCelular',
                'p.cliente_telefono as ClienteTelefono',
                'p.cliente_dni as ClienteDNI',
                'p.embajador_nombre as EmbajadorNombre',
                'p.embajador_celular as EmbajadorCelular',
                'p.embajador_correo as EmbajadorCorreo',
                'p.embajador_dni as EmbajadorDNI',
                'p.comentario as ComentarioTicket',
                 DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                DB::raw(
                    'DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                        as FechaRegistro'
                ),
                DB::raw('IFNULL(m.nombre," ") as motivo'),
                DB::raw('IFNULL(s.nombre," ") as submotivo'),
                DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
                'ptu.observacion as ObservacionUltMov',
                DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as FechaUltMov'
                ),
                DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                'pts.producto as Producto',
                'pts.accion as Accion',
                'pta.estado_legado as EstadoLegado',
                'pta.fecha_registro_legado as FechaRegistroLegado',
                'pta.fecha_liquidacion as FechaLiquidacion',
                'pta.cod_liquidacion as CodigoLiquidacion',
                'pta.detalle_liquidacion as DetalleLiquidacion',
                'pta.area as Area',
                'pta.contrata as Contrata',
                'pta.zonal as Zonal',
                'em.nombre as evento_masiva',
                DB::raw('CONCAT(m.codigo_cot,s.codigo_cot) as CodMov')
            );
        } else {
            $query->select(
                DB::raw('CONCAT("E",p.id) as ticket'),
                'p.id',
                'at.apocope as tipo_averia',
                'pa.nombre as tipo_atencion',
                'p.gestion',
                'p.codactu',
                'p.codcli',
                'p.cliente_nombre',
                'p.cliente_celular',
                'p.cliente_telefono',
                'p.cliente_dni',
                'p.embajador_nombre',
                'p.estado_pretemporal_id as estado_id',
                'q.nombre as quiebre',
                DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                DB::raw('IFNULL(CONCAT(pts.producto,"-", pts.accion)," ") as solucion'),
                DB::raw(
                    'IFNULL(
                        CONCAT(pts.producto,"-", pts.accion)," "
                    ) as solucion'
                ),
                DB::raw(
                    'IFNULL(
                        CONCAT(pta.cod_liquidacion)," "
                    ) as codliq'
                ),
                DB::raw('IFNULL(pta.area, " ") as area'),
                DB::raw(
                    'DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                        as fecha_registro'
                ),
                DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as fechaultmov'
                ),
                DB::raw('IFNULL(m.nombre," ") as motivo'),
                DB::raw('IFNULL(s.nombre," ") as submotivo'),
                DB::raw('IFNULL(e.nombre,"Recepcionado") as estado')
            );
        }
        
        $query->where('p.actividad_id', 1)
              ->whereRaw(
                  'q.id NOT IN (
                        SELECT quiebre_id
                        FROM quiebre_usuario_restringido
                        WHERE usuario_id="'.Auth::id().'"
                        AND estado=1
                    )'
              );

        if (Input::get('usuario_id')) {
                $query->where('p.usuario_id', Input::get('usuario_id'));
        }

        if (Input::get('usuarios')) {
                $query->whereIn('p.usuario_id', Input::get('usuarios'));
        }

        if ($PG == 'P') {
            switch ($tipoBusqueda) {
                case 'ticket':
                    $datos= array();
                    $array = explode("E", strtoupper($busqueda));
                    if (count($array)>1) {
                        $ticketId = $array[1];
                        $query->where('p.id', '=', (int)$ticketId);
                    } else {
                        $query->where('p.id', '=', (int)$busqueda);
                    }
                    break;
                case 'codactu':
                    $query->where('p.codactu', '=', $busqueda);
                    break;
                case 'dniemb':
                    $query->where('p.embajador_dni', '=', $busqueda);
                    break;
                case 'dnicli':
                    $query->where('p.cliente_dni', '=', $busqueda);
                    break;
                case 'codcli':
                    $query->where('p.codcli', '=', $busqueda);
                    break;
                break;
            }
        } elseif ($PG == 'G') {
            $tipo_averia = Input::get('averia');
            $quiebre = Input::get('quiebre');
            $estado = Input::get('estado');
            $estadoPre = Input::get('estado_pre');
            $atencion = Input::get('atencion');
            $fechaRegistro = Input::get('fecha_registro');
            $motivo = Input::get('motivo');
            $submotivo = Input::get('submotivo');
            $gestion = Input::get('gestion');

            if(Input::get('eventos')){
                $query->whereIn('p.evento_masiva_id', Input::get('eventos'));                
            }

            if(Input::has('asignacion')){
                if(Input::get('asignacion') == 1){
                    $query->whereRaw('(p.usuario_id != 0 and p.usuario_id!="" and p.usuario_id IS NOT NULL)');                
                }elseif(Input::get('asignacion') == 2){
                    $query->whereRaw('(p.usuario_id = 0 or p.usuario_id="" or p.usuario_id IS NULL)');
                }
            }

            if (count($tipo_averia) != 0) {
                $query->whereIn('at.apocope', $tipo_averia);
            }
            if (count($atencion) != 0) {
                $query->whereIn('p.tipo_atencion', $atencion);
            }
            if (count($estadoPre) != 0) {
                $query->whereIn('p.estado_pretemporal_id', $estadoPre);
            }
            if (count($estado) != 0) {
                $query->whereIn('ptu.estado_id', $estado);
            }
            if (count($motivo) != 0) {
                $query->whereIn('ptu.motivo_id', $motivo);
            }
            if (count($submotivo) != 0) {
                $query->whereIn('ptu.submotivo_id', $submotivo);
            }
            if (count($gestion) != 0) {
                $query->whereIn('p.gestion', $gestion);
            }
            if (count($quiebre) != 0) {
                $query->whereIn('p.quiebre_id', $quiebre);
            } else {
            }
            if (count($fechaRegistro) != 0) {
                $fecha = explode(" - ", $fechaRegistro);
                if (count($fecha) > 0) {
                    $startDate = date('Y-m-d  00:00:00', strtotime($fecha[0]));
                    $endDate = date('Y-m-d  23:59:59', strtotime($fecha[1]));
                    $query->whereBetween(
                        'p.created_at',
                        array($fecha[0]. " 00:00:00",
                                $fecha[1]. " 23:59:59")
                    );
                }
            }
        }
        $resultado = $query->get();


        if(Input::has('accion')){
            $data = json_decode(json_encode($resultado), true);
            return Helpers::exportArrayToCsv($data, 'conformidad');
        }

        if (count($resultado) > 0) {
            $datos = array(
                'rst' => 1,
                'tipo' => 'a',
                'datos' => $resultado,
                'msj' => 'Busqueda Completa'
            );
        } else {
            $datos = array(
                'rst' => 0,
                'msj' => 'No se encontraron datos.'
            );
        }
        return $datos;
    }


}

