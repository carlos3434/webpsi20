<?php
ini_set('memory_limit', '1024M');
class ConsultaController extends BaseController
{

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    private $_acceso;
    private $_clave;
    private $_hashg;
    private $_fecha;

    public function __construct()
    {
        $this->_acceso = "\$PSI20\$";
        $this->_clave = "132756acac57eeec8564aa89cb0cedb7"; //"\$1st3m@\$" ;
        $this->_fecha = Input::get('fecha');
        $this->_hashg = Input::get('hashg');
    }

    public function postUltimomovimento()
    {
        if (!Request::ajax()) {
            $rf = array();
            foreach (Input::all() as $r => $i) {
                $dato[$i] = $r;
            }

            $hash = hash(
                'sha256', $this->_acceso . $this->_clave . $this->_fecha
            );

            if ($hash == $this->_hashg) {
                $r = Consulta::getUltimoMovimiento();
                echo json_encode($r);
            } else {
                echo "Error:0002";
            }
        } else {
            echo "Error:0001";
        }
    }

    public function postMovimientos()
    {
        if (!Request::ajax()) {
            $rf = array();
            foreach (Input::all() as $r => $i) {
                $dato[$i] = $r;
            }

            $hash = hash(
                'sha256', $this->_acceso . $this->_clave . $this->_fecha
            );

            if ($hash == $this->_hashg) {
                $r = Consulta::getMovimientos();
                echo json_encode($r);
            } else {
                echo "Error:0002";
            }
        } else {
            echo "Error:0001";
        }
    }
}
