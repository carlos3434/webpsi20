<?php

use Legados\models\SolicitudTecnica as SolicitudTecnica;
use Ofsc\Inbound;

class AutoCierreSolicitudController extends \BaseController
{
    public function cerrarSolicitud($job, $data){

        $fecha_actualizacion_old = $data['fecha_actualizacion_old'];
        $solicitud_tecnica_id = $data['solicitud_tecnica_id'];
        $codigo_ofsc = $data['codigo_ofsc'];
        $cod_actu = $data['codActu'];

        $solicitud_tecnica = SolicitudTecnica::with('ultimo')->find($solicitud_tecnica_id);
        $ultimo_movimiento_new  = $solicitud_tecnica->ultimo;   
        $id_solicitud_tecnica = $solicitud_tecnica->id_solicitud_tecnica;

        $fecha_actualizacion_new = $ultimo_movimiento_new->updated_at->format('Y-m-d H:i:s');
        if( !Redis::get($id_solicitud_tecnica) ) {
            if( $fecha_actualizacion_old == $fecha_actualizacion_new && 
                $ultimo_movimiento_new->estado_ofsc_id  == 2 &&
                $ultimo_movimiento_new->tipo_legado == 1 &&
                $ultimo_movimiento_new->estado_aseguramiento == 3 ) {

                Auth::loginUsingId(952);
                $inputs = [
                    'codactu' => $cod_actu,
                    'comentario' => 'Cierre automático',
                    'tipo_devolucion' => '',
                    'tematicos' => [],
                    'motivosdevolucion' => '',
                    'solicitud_tecnica_id' => $solicitud_tecnica_id
                ];
                \Helpers::ruta(
                                'bandejalegado/stoporden',
                                'POST',
                                $inputs,
                                false
                            );
                Auth::logout();
            }
        }

        $fecha_actualizacion_new = '';
        $fecha_actualizacion_old = '';

        $job->delete();
    }

}    