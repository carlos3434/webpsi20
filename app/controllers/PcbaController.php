<?php
//use Legados\PruebaElectricaService;

class PcbaController extends \BaseController
{
    public function postPrueba() //solo pcba
    {
        $pcba = new Pcba();
        $response = $pcba->obtenerprueba();
        return \Response::json(
                array(
                    'rst' => 1,
                    'resultado' => $response
                )
            );
    }

    public function postPruebacompletas()
    {
        $rst=$rst_=$rst_a=1;

        // PCBA
        $pcba = new Pcba();
        $responsepcba = $pcba->obtenerprueba();

        //Para analizador y assia
        $telefono=trim(Input::get('codarea')).trim(Input::get('telefono'));
        $array=array('telefono' => $telefono);

        //ANALIZADOR
        $electricobtenida=json_decode(Helpers::ruta('analizador/obteneryprobar',
                                        'POST', 
                                        $array));

         if (!isset($electricobtenida->interprete)) {
            $rst_= 2;
            if(is_null($electricobtenida)) 
                $electricobtenida=array(
                    'rst' => 2,
                    'msj' => 'No hay respuesta'
                );
        } 

        //ASSIA
        $assiaobtenida=json_decode(Helpers::ruta('assia/assia',
                            'POST', 
                            $array));

        return \Response::json(
                array(
                    'responsepcba' => array(
                        'rst' => $rst,
                        'resultado' => $responsepcba
                    ),
                    'responseelec' => array(
                        'rst' => $rst_,
                        'resultado' => $electricobtenida
                    ),
                    'responseassia' => array(
                        'rst' => $rst_a,
                        'resultado' => $assiaobtenida
                    )
                )
            );
    }

}