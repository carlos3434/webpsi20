<?php

use Ofsc\Capacity;
use Ofsc\Activity;
use Ofsc\Inbound;
use Ofsc\Outbound;
use Ofsc\Location;
use Ofsc\Resources;
use Legados\STCmsApi;

class TestOfscController extends \BaseController {
    
    protected $_errorController;
    protected $_bandejaController;
    protected $_gestionMovimientoController;
    
    public function __construct(
            ErrorController $errorController,
            BandejaController $bandejaController,
            GestionMovimientoController $gestionMovimientoController
        ) {
        $this->_errorController = $errorController;
        $this->_bandejaController = $bandejaController;
        $this->_gestionMovimientoController = $gestionMovimientoController;
    }

    public function getValidarrespuestast()
    {
        $objApiST = new STCmsApi;
         $respuesta = [
            'id_solicitud_tecnica' => "C0000072",
            'id_respuesta'=>2, 'observacion'=> "No paso la Parametrizacion LEGO"
         ];
        $response = $objApiST->responderST($respuesta);
        
    }

    public function getCerrarsolicitudtecnica()
    {
        $objApiST = new STCmsApi;
        $response = $objApiST->cierre("C0000072");
        return $response;
    }

    public function getUpdateresources()
    {
        $resource = new Resources();
        
        $actividad['resource_id']='LA1180';
        $actividad['label']='UBICACION_LA1180';
        $actividad['latitude']='-12.05915';
        $actividad['longitude']='-76.97013';

        $propiedades=[];
        foreach ($actividad as $key => $value) {
            $propiedades[]=['name'=>$key,'value'=>$value];
        }

        $locations=['location'=>
            [
                'properties'=>$propiedades
            ]
        ];

        $response = $resource->updateLocations($locations);
        var_dump($response);
    }
    public function getOutbound()
    {
        $Outbound =  new Outbound();
        $request=array('message'=>
                    array(
                        'message_id'=>123456,
                        'status'=>'delivered'
                        )
                    );
        $response=$Outbound->setMessageStatus($request);
        
        print_r($response);dd("Home");
    }
    public function getSearchactivity()
    {
        $activity =  new Activity();
        $datos=array(
            'search_in'=>'appt_number',
            'search_for'=>'6484004',
            'date_from'=>'2016-01-22',
            'date_to'=>'2016-01-29',
            'select_from'=>'1',
            'select_count'=>'1',
            'property_filter'=>'id'
            );
        $datos=array(
                    'search_in'=>'appt_number',
                    'search_for'=>'6484004',
                    'date_from'=>date('Y-m-d', strtotime('-7 days')),
                    'property_filter'=>'id'
                );
        $response = $activity->searchActivity($datos);
        print_r($response);
        //print_r($response->data->activity_list->activities->activity->properties->value);
        
    }
    public function getGetactivity()
    {
        $activity =  new Activity();
        $response = $activity->getActivity(6388524);
        
        print_r($response);dd("Home");
    }
    public function getTesthola()
    {
        $wsdl = file_get_contents('https://telefonica-pe.test.toadirect.com/soap/capacity/?wsdl');
        var_dump($http_response_header);
        //phpinfo();
    }

    public function getTestcancelactivity()
    {
       $code = Input::get('code'); 

       if (!isset($code) || $code == null) {
           return false;
       }

       $activity =  new Activity();
       //$response = $activity->cancelActivity($code);
       //$carga_xml = simplexml_load_string($response);
       //print_r($carga_xml);
    }


    public function getTestgetcapacity() {
        $capacity = new Capacity();
        $response = $capacity->getCapacity();
        $carga_xml = simplexml_load_string($response);
        print_r($carga_xml);
    }

    public function getTestgetquotadata() {
        $capacity = new Capacity();
        $response = $capacity->getQuotaData();
        $carga_xml = simplexml_load_string($response);
        print_r($carga_xml);
    }

    //LOCATION

    public function getGetposition()
    {
        /*
        $company = Input::get("company");
        $device = Input::get("device"); //external_id
*/
        $company = 'HDEC';
        $device = 'Tony';

        $location =  new location();
        $response = $location->getPosition($company, $device);
        
        print_r($response);dd("Home");
    }

    public function getGetpositionattr()
    {
        $location =  new location();
        $response = $location->getPositionAttr('LARI','TEST.TDPTech6');
        
        print_r($response);dd("Home");
    }

    public function getTestcancelar(){
        $activityId = 324;
        
        $activity = new Activity();
        $response = $activity->cancelActivity($activityId);
        print_r($response);
    }
    
    public function getTestcreateactivity() {
        $inbound = new Inbound();
        $response = $inbound->createActivity();
        print_r($response);
    }
    
    public function getPruebaapicapacity(){
        $capacity = new Capacity();
        
        $data["fecha"] = array(
            "2015-12-10"
        );
        //Sin bucket
        //$data["bucket"] = "BK_PRUEBAS_TOA";
        $data["time_slot"] = "";
        $data["work_skill"] = "PROV_INS_M1";
        $data["activity_field"] = array(
                    array(
                        "name" => "worktype_label",
                        "value" => "PROV_INS_M1"
                    ),
                    array(
                        "name" => "XA_WORK_ZONE_KEY",
                        "value" => "MY"
                    ),
                );
        
        $response = $capacity->getCapacity($data);
        print_r($response);
    }
    
    public function getTestupdateactivity()
    {
        $activityId = 374;
        
        $data["direccion"] = "Av. Los Frutos 159";
        
        $activity = new Activity();
        $response = $activity->updateActivity($activityId, $data);
        print_r($response);
    }
    
    public function getTestgetfile(){
        
        $activity = new Activity();
        
        $img = "<img src=\"img/no_image_256x256.png\">";
        
        for ($i=1; $i<=8; $i++) {
            $response = $activity->getFile(
                2466, "XA_DONE_PIC_ADD" . "_" . $i
            );

            if ($response->data->result_code == 0)
            {
                $img = str_replace(
                        "<img src=\"img/no_image_256x256.png\">", 
                        "", 
                        $img
                    );
                foreach ($response->data->file_data as $val) {
                    $img .= "<img src=\"data:image/jpeg;base64,"
                         . base64_encode($val) ."\" />";
                }
            }
        }
        
        echo $img;
    }
    
    public function getTeststartactivity()
    {
        $activityId = 391;
        
        $activity = new Activity();
        $response = $activity->startActivity($activityId);
        print_r($response);
    }

}
