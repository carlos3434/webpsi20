<?php

class PsiInformacionController extends BaseController
{

    public function __construct()
    {

    }

    /**
     * Listar registro de actividades con estado 1
     * POST actividad/listar
     *
     * @return Response
     */
    public function postListar()
    {
        if (Request::ajax()) {         
            $actividades = DB::table('psi_informacion as psi')
                        ->select(
                            'psi.fecha_cms_psi',
                            'psi.fecha_psi_toa',
                            'psi.fecha_toa_psi',
                            'psi.fecha_psi_cms',
                            'psi.fecha_psi_cierre_cms',
                            'psi.fecha_cms_cierre_psi'
                        )
                        ->get();
           
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$actividades
                )
            );
        }
    }

    public function postCargar()
    {
        $rutaFolder = public_path()."/sound/";
        $fileName = "alerta.wav";

        $rutaFile = $rutaFolder.$fileName;
        Input::file('file_json')->move($rutaFolder, $fileName);
        chmod($rutaFile, 0777);
        $solicitudes = json_decode(\File::get($rutaFile), true);
        return;
    }
}
