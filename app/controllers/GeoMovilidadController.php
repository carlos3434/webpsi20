<?php
class GeoMovilidadController extends \BaseController
{
    public function postListar()
    {
        $r =GeoMovilidad::postGeoMovilidadAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

     public function postZonal()
     {
        $array['zonal']=Input::get('zonal');
        $r =GeoMovilidad::postProyectoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
     }

    public function postProyecto()
    {
        $array['zonal']=Input::get('zonal');
        $array['proyecto']=Input::get('proyecto');
        $r =GeoMovilidad::postMovilFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }


}

