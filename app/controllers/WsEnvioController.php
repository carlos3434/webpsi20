<?php

class WsEnvioController extends BaseController
{

    public function postPaginacion()
    {
        if (Request::ajax()) {

            $fechaIni = "";
            $fechaFin = "";
            $data_a_pasar = "";

            if (Input::get('fecha_agenda')) {
                $fechaAgenda = explode(" - ", Input::get('fecha_agenda'));
                $fechaIni = $fechaAgenda[0]; //Fecha inicio
                $fechaFin = $fechaAgenda[1]; //Fecha final
            }
            
            $filtro = array('');
            
            if ( Input::get('tecnico') ) {
                $arrayTecnico = implode(",", Input::get('tecnico'));
                $queryTecnico = 'SELECT REPLACE(GROUP_CONCAT(carnet_tmp),",","|") carnet FROM tecnicos WHERE id IN ('.$arrayTecnico.')';
                $carnetTecnico = DB::select($queryTecnico);
                $filtro[0] = 'AND trama REGEXP "'.$carnetTecnico[0]->carnet.'"';
            }                
               
            $count = 'SELECT COUNT(*) total
                      FROM webpsi_officetrack.wsenvios
                      WHERE (DATE(fecha) 
                      BETWEEN "' . $fechaIni . '" AND "' . $fechaFin . '")
                      '.$filtro[0].'';

            $query = 'SELECT id, fecha, trama, response 
                      FROM webpsi_officetrack.wsenvios
                      WHERE (DATE(fecha) 
                      BETWEEN "' . $fechaIni . '" AND "' . $fechaFin . '")
                      '.$filtro[0].'
                      ORDER BY id asc 
                      LIMIT ' . Input::get('start') . ', ' . 
                      Input::get('length') . '';

            $rcount = DB::select($count);
            $consulta = DB::select($query);

            //Asignado los números de Paginacion al Arreglo $wsenvios
            $wsenvios["draw"] = Input::get('draw');
            $wsenvios["recordsTotal"] = $rcount[0]->total;
            $wsenvios["recordsFiltered"] = $rcount[0]->total;

            //Asignado la data al Arreglo $wsenvios
            $wsenvios["data"] = $consulta;

            return Response::json($wsenvios);
        }
    }
    
    public function postDetalle()
    {
        if (Request::ajax()) {
            $detalle = DB::table('webpsi_officetrack.wsenvios')
                        ->where('id', '=', Input::get('id'))
                        ->select('trama')
                        ->get();
            
            return Response::json(array('rst' => 1, 'datos' => $detalle));
        }
    }
}
