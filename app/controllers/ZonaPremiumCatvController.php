<?php
use Repositories\CargasRepositoryInterface;
use Repositories\CargasRepository;

class ZonaPremiumCatvController extends \BaseController {
    //use  DataViewer;
    /**
     * Display a listing of the resource.
     * GET /tipificacion
     *
     * @return Response
     */

    public function postListar()
    {

        $fecha = Input::get("fecha");
        $formFecha = explode(',', $fecha);
        $solicitudTecnica=Input::get("solicitudTecnica");
        $zonal_premium=Input::get("zonal_premium", null);
        $nodo_premium=Input::get("nodo_premium", null);
        $cod_troba=Input::get("cod_troba", null);
        //$nodosPremium="";

        //dd($nodosPremium);
        $query=  ZonaPremiumCatv::from('zona_premium_catv as st')
                ->select(
                    'st.nodo as id',
                    'st.zona',
                    'st.nodo',
                    'st.troba',
                    'st.contrata',
                    'st.fecha_registro',
                    'st.estado'
                )->where(function ($query) use ($zonal_premium, $nodo_premium, $cod_troba) {

					if ($zonal_premium != "" && $zonal_premium != null) {
						$query->whereIn('zona', $zonal_premium);
					}
					if($nodo_premium != "" && $nodo_premium != null){
						$query->whereIn('nodo', $nodo_premium);
					}
				if ($cod_troba != "" && $cod_troba != null) {
                        $query->whereIn('troba', $cod_troba);
                   }

				})->orderBy('st.fecha_registro', 'desc');

				 //dd($query);			
				$query=$query->searchPaginateAndOrder();
				return $query;
	}

	public function postActualizar(){
			if (Request::ajax()) {
				$filtro= explode(',', Input::get('tipificacion_id'));

			if(Input::has('estado')){
				$estado = Input::get('estado');
			}
							  
				$sql="update zona_premium_catv set estado=".$estado." where nodo='".$filtro[0]."' and troba='".$filtro[1]."' and contrata='".$filtro[2]."'";
        try {
            DB::update($sql);
            Cache::forget('listZonalPremium');
        } catch (Exception $e) {
            
        }
			return Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}


	public function postCreate(){
		if (Request::ajax()) {
			$data= [];
			$idlogeado = Auth::user()->id;
			//dd(Input::get('zonaPremiumModal'));
			$data['zona'] = strtoupper(Input::get('zonaPremiumModal'));
			$data['nodo'] = Input::get('nodo_premium');
			$data['troba'] =Input::get('troba_premium');
			$data['contrata'] =strtoupper(Input::get('contrata_premium'));
			$data['fecha_registro']=date('Y-m-d H:i:s');
			$data['estado'] =Input::get('estado_premium');			
			$data['usuario_created_at']=$idlogeado;
			try {
				$rst = ZonaPremiumCatv::create($data);		
				Cache::forget('listZonalPremium');
				return Response::json(				
	                array(
	                    'rst'    => ($rst) ? 1 : 0,
	                    'msj'    => "Registrado Correctamente",
	                )
	              );
			} catch (Exception $e) {
				$errorController = new ErrorController();
	            $errorController->saveError($e);	            
				return Response::json(
				
	                array(
	                    'rst'    => 3,
	                    'msj'    => "Error al Guardar \n o \n El nodo ".$data['nodo']." y troba ".$data['troba']." ya se encuentran registardos",
	                )
	              );
			}   	
        	
		}
	}

	public function postModificar(){
		if (Request::ajax()) {
			$zona=strtoupper(Input::get('zonaPremiumModal'));
			$nodo=Input::get('nodo_premium');
			$troba=Input::get('troba_premium');
			$contrata=strtoupper(Input::get('contrata_premium'));
			$fecha_registro=date('Y-m-d H:i:s');
			$estado=Input::get('estado_premium');
			$filtro= explode(',', Input::get('tipificacion_id'));
			$idlogeado = Auth::user()->id;
							  
				$sql="update zona_premium_catv set zona='".$zona."',nodo='".$nodo."',troba='".$troba."',contrata='".$contrata."', fecha_registro='".$fecha_registro."', estado=".$estado." ,usuario_updated_at=".$idlogeado." where nodo='".$filtro[0]."' and troba='".$filtro[1]."' ";

        try {
            DB::update($sql);
            Cache::forget('listZonalPremium');
        } catch (Exception $e) {
            
        }
			return Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}

	}

	public function postUploadexcel() 
    {	
    	$file = \Input::file('excel'); 
		$name = $file->getClientOriginalName();

		if(	substr($name,-4)!='.xls' ){
			$data=[];
			$cabeceras=[];
			$rst=0;
			$msj="<b><i style='color:#DE3B19;'>Archivo seleccionado no esta permitido! (.xls)</i></b>";
		}else{

			$columns=4;
	    	$obj= new CargasRepository;
	        $array = $obj->getdataUpload_excel($columns);
	        $cabeceras=$array['cabecera'];
	        $data=$array['data'];
	        $array_p=[];$array_p2=[];
	        $count=0;$cont_2=0;$campos="";$msj="";$rst=1;

	        if (! in_array("zona", $cabeceras) ) {
			    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>Campo 1 / zona, no esta definido en el Excel.</i></b><br>";
				$rst=0;
			}

			if (! in_array("nodo", $cabeceras)) {
			    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>Campo 2 / nodo, no esta definido en el Excel.</i></b><br>";
				$rst=0;
			}

			if (! in_array("troba", $cabeceras)) {
				    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>Campo 3 / troba, no esta definido en el Excel.</i></b><br>";
					$rst=0;
				}

				if (! in_array("contrata", $cabeceras)) {
				    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>Campo 4 / contrata, no esta definido en el Excel.</i></b><br>";
					$rst=0;
				}

				if( $rst == 0 ){
					$msj='Campo(s) : <br>'.$campos;
				}else{
					$concat='';
					$cont=0;
					foreach ($data as $key => $value) {
						if( strlen($value['zona']) == 0 and strlen($value['nodo']) == 0 and 
							strlen($value['troba']) == 0 and strlen($value['contrata']) == 0 ){
							unset($data[$key]);
						}

						if(	strlen($value['nodo']) > 2 ){
							$cont++;
							$concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">(FILA '.($key+1).'- NODO): '.$value['nodo'].' tiene mas de 2 caracteres.</b>';
						}

						if(	strlen($value['troba']) > 4 ){
							$cont++;
							$concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">(FILA '.($key+1).'- TROBA): '.$value['troba'].' tiene mas de 4 caracteres.</b>';
						}

						array_push($array_p,strtoupper($value['nodo']).strtoupper($value['troba']));
					}

					//validar troba y nodo
					$str=implode(",",$array_p);
					foreach ($data as $key => $value) {
						$count = substr_count($str, strtoupper($value['nodo']).strtoupper($value['troba']));
						if($count>1){
							array_push($array_p2,strtoupper($value['nodo']).','.strtoupper($value['troba']));
							$cont_2++;
						}
					}
					
					if($cont>0){
						$rst=2;
						$msj="Validar excel:".$concat;
					}else{
						if($cont_2>0){
							$array_p2=array_unique($array_p2);
							$concat2='';
							foreach ($array_p2 as $key => $value) {
								$campos=explode(',', $value);
								$concat2.="\n".'<b style="font-size:14px;font-style: oblique;">Repetido: </><b style="color:#DE3B19;font-size:14px;font-style: oblique;">NODO: </b> <b style="font-size:14px;font-style: oblique;">'.$campos[0].'</b>  <b style="color:#DE3B19;font-size:14px;font-style: oblique;">TROBA: </b> <b style="font-size:14px;font-style: oblique;">'.$campos[1].'</b>';
							}
							$rst=3;
							$msj="Validar excel:".$concat2;
						}
					}
				} 
		}
		Cache::forget('listZonalPremium');
		
		return \Response::json(
            array(
                'rst' => $rst,
                'msj' => $msj,
                'data' => $data,
                'cabeceras'=>$cabeceras
            )
        );
    }

	public function postCargamasiva()
    {	
    	$flag=false;$estado=0;$msj='';

    	$idlogeado = Auth::user()->id;
    	date_default_timezone_set('America/Lima');
		$fecha = date("Y-m-d G:i:s");
    	$zona=Input::get('zona');
    	$nodo=Input::get('nodo');
    	$troba=Input::get('troba');
    	$contrata=Input::get('contrata');

    	$consulta= DB::select('SELECT nodo,troba FROM zona_premium_catv');
    	$consulta=json_decode(json_encode($consulta),true);
    	$campos="";
    	
    	$cont=0;
		for ($i=0; $i <count($zona); $i++) {
			for ($j=0; $j < count($consulta); $j++) { 
	    		if( strtoupper($consulta[$j]['nodo']) == strtoupper($nodo[$i]) and 
	    			strtoupper($consulta[$j]['troba']) == strtoupper($troba[$i]) ){
	    			$flag=false;
	    			$estado=0;
	    			$cont++;
	    			$campos.="\n".'<b style="color:#DE3B19;font-size:14px;font-style: oblique;">NODO:</b> <b style="font-size:14px;font-style: oblique;">'.$nodo[$i].'</b>  <b style="color:#DE3B19;font-size:14px;font-style: oblique;">TROBA:</b> <b style="font-size:14px;font-style: oblique;">'.$troba[$i].'</b>';
	    		}
    		}

			if(	$cont == 0 ){
    			$data['zona']=strtoupper($zona[$i]);
		    	$data['nodo']=strtoupper($nodo[$i]);
		    	$data['troba']=strtoupper($troba[$i]);
		    	$data['contrata']=strtoupper($contrata[$i]);
		    	$data['fecha_registro']=$fecha;
		    	$data['estado']=1;
		    	$data['usuario_created_at']=$idlogeado;
		    	$data['usuario_updated_at']=$idlogeado;

                $rst=ZonaPremiumCatv::create($data);
            }
        }

    	if(	$cont == 0 ){
    		if($rst){
	    		$estado=1;
	    		$msj="Carga Masiva realizada exitosamente!";
	    	}else{
	    		$estado=0;
	    		$msj="Error en Carga Masiva!";
	    	}
    	}else{
    		$estado=0;
    		$msj="Datos ya existen (Modificar o Eliminar) :".$campos;
    	}
		Cache::forget('listZonalPremium');
		return \Response::json(
            array(
                "rst" => $estado,
                "msj" => $msj
            )
        );
    }

    public function postExcelcargamasiva(){
    	
    	$zona=json_decode(Input::get('zona'),true);
    	$nodo=json_decode(Input::get('nodo'),true);
    	$troba=json_decode(Input::get('troba'),true);
    	$contrata=json_decode(Input::get('contrata'),true);

    	$reporte=[];
    	for ($i=0; $i < count($zona); $i++) { 
    			
    		array_push($reporte,array('ZONA'=>$zona[$i],'NODO'=>$nodo[$i],'TROBA'=>$troba[$i],
    		'CONTRATA'=>$contrata[$i]));

    	}

    	if (Input::get("tipo")=='Excel') {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=ZonaPremiumCatv.xls');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');

	        if(count($reporte)>0){
	            $n = 1;
	            foreach ($reporte as $data) {
	            	//Encabezado
	            	if ($n == 1) {
	                    foreach ($data as $key => $val) {
	                        echo $key . "\t";
	                    }
	                   	echo $val . "\r\n";
	                }
		            //Datos
		            foreach ($data as $val) {
		                $val = str_replace(
		                array("\r\n", "\n", "\n\n", "\t", "\r"),
		                array("", "", "", "", ""),$val);
		                
		                echo $val . "\t";
		            }
		            echo "\r\n";
		            $n++;
	            }
	        }else{$val="Mensaje:No existen registros!";echo $val . "\r\n";}
        } else {return Response::json(array('rst' => 1, 'datos' => $reporte));}
    }
}