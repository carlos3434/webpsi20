<?php
class GeoProyectoController extends \BaseController
{
    public function postSave()
    {
      //inputs
      $mapa=Input::get('mapa');
      $proyecto['nombre']=Input::get('nombre');
      $idpro=0;
      $rst=1;
      $msj='Proyecto Registrado Correctamente';

      //validacion    
      $reglas = array(
              'nombre'    => 'required|unique:geo_proyecto'
      );
      $mensaje = array(
              'required'  => 'Nombre de Proyecto es requerido',
              'unique'   => 'Nombre de Proyecto ya está registrado',
          );
      $validator = Validator::make(Input::all(), $reglas, $mensaje);
      if ($validator->fails()) {
          return Response::json(
              array(
              'rst' => 2,
              'msj' => $validator->messages(),
              )
          );
      }

      if ($mapa) {
        $datos=GeoProyecto::agregar($proyecto);    
        if ($datos->id) {
          $idpro = $datos->id;
          $capas= array();

          foreach ($mapa as $index => $value) {
            if ($value) {
              $idcapa=array_search($value['capa'], $capas);
              if (!$idcapa) {
                $arraycapa['proyecto_id'] = $idpro;
                $arraycapa['nombre']      = $value['capa'];
                $datoscapa=GeoProyectoCapa::getAgregarCapa($arraycapa);
                $idcapa=$datoscapa->id;
                $capas[$idcapa]=$value['capa'];
              }

              if ($idcapa>0) {
                  $value['capa_id']=$idcapa;
                  $value['tabla_id']=$value['elemento'];
                  unset($value['elemento']);
                  unset($value['capa']);
                  $delemento=GeoProyectoCapaElemento::agregar(
                                                  $value);
              }
            }
          }   
        } else {
          $msj='No se pudo guardar el proyecto';
          $delemento=0;
          $rst=0;
        }
      } else {
        $msj='No existen elementos en el mapa';
        $delemento=0;
        $rst=2;
      }

      if ( Request::ajax() ) {
          return Response::json(
              array(
                  'rst' => $rst, 
                  'datos' => $idpro,
                  'msj' => $msj
              )
          );
      } 
    }

    public function postCargar()
    {
       if (Request::ajax()) {
            $datos = GeoProyecto::cargar();

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }

    public function postUpdate()
    {
        $array=Input::all();
        unset($array['id']);
        unset($array['token']);
        $id=Input::get('id',0);

        //validacion    
        $reglas = array(
                'nombre'    => 'unique:geo_proyecto, "nombre", '.$id
        );
        $mensaje = array(
                'unique'   => 'Nombre de Proyecto ya está registrado',
            );
        $validator = Validator::make(Input::all(), $reglas, $mensaje);
        if ($validator->fails()) {
            return Response::json(
                array(
                  'rst' => 2,
                  'msj' => $validator->messages(),
                )
            );
        }

        $datos = GeoProyecto::actualizar($array,$id);  

        if (Request::ajax()) {
            return Response::json(
                array('rst'=>1,
                    'datos'=>$datos)
            );
        } 
    }

    public function postListar()
    {
       $usuario=Auth::id();
       if (Request::ajax()) {
            $datos = GeoProyecto::listar($usuario);

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }
}

