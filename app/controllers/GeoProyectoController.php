<?php
class GeoProyectoController extends \BaseController
{
    public function postSave()
    {
        //$detalles=Input::get('detalles');
        $mapa=Input::get('mapa');
        $proyecto=Input::get('proyecto');
        $usuario=Auth::user()->id;
        $idpro=0;
    
        $idproyecto=GeoProyecto::getidProyecto($proyecto);

        foreach ($idproyecto as $key => $value) {
              $idpro = $value->id;
        }

        if ($idpro==0) {
          if ($mapa) {
              $data=array(
                        $proyecto,
                        $usuario
                        );
    
              $datos=GeoProyecto::getAgregarProyecto($data);
    
              if ($datos) {

                    $idproyecto=GeoProyecto::getidProyecto($proyecto);
                    foreach ($idproyecto as $key => $value) {
                        $idpro = $value->id;
                    }

                   $arraydetail= array();
                   $i=0;
                   $capas= array();
                   $existe=0;
                   $arraymapa= array();
                   $y=0;

                   foreach ($mapa as $index => $value) {
                      if ($value) {
                          if (in_array($value['capa'], $capas)) {
                                $existe++; 
                          } else {
                            array_push($capas, $value['capa']);
                                $arraydetail[$i]="('".$value['capa']."',
                                                   '".$idpro."')";
                                $i++;
                          }

                      $arraymapa[$y]="((select id from geo_proyecto_capa where 
                              nombre='".$value['capa']."' 
                              and proyecto_id='".$idpro."' 
                              order by id desc limit 1 ),
                              '".$value['subelemento']."',
                              '".$value['tabla_detalle_id']."',
                              '".$value['orden']."','".$value['borde']."',
                            '".$value['grosorlinea']."','".$value['fondo']."',
                            '".$value['opacidad']."', 
                            '".$value['elemento']."', 
                            '".$value['tipo']."')";
                           $y++;
                      }

                   } 
                    $valcap= implode(",", $arraydetail);
                    $dcapa=GeoProyectoCapa::getAgregarCapa($valcap);
 
                   if ($dcapa) {

                    $valele= implode(",", $arraymapa);
                    $delemento=
                    GeoProyectoCapaElemento::getAgregarCapaElemento($valele);
                    $msj='Se guardo el Proyecto';
                    $rst=1;
                       
                   } else {
                       $msj='No se pudo guardar la(s) Capa(s)'; 
                       $delemento=0;
                       $rst=0;
                   } 
   
              } else {
                $msj='No se pudo guardar el proyecto';
                $delemento=0;
                $rst=0;
              }
          } else {
            $msj='No existen elementos en el mapa';
             $delemento=0;
             $rst=0;
          }
        } else {
           $msj='Este proyecto ya existe, elija otro nombre';
           $delemento=0;
           $rst=0;
        }
            if ( Request::ajax() ) {
                return Response::json(
                    array(
                        'rst' => $rst, 
                        'datos' => $idpro,
                        'msj' => $msj
                    )
                );
            } 

    }

    public function postCargar()
    {
       if (Request::ajax()) {
            $datos = GeoProyecto::getCargarProyectos();

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }

    public function postUpdate()
    {
        if (Input::get('campo')==1) { // actualiza estado
          $campo='estado';
        } elseif (Input::get('campo')==2) {
          $campo='estado_publico';
        }
          $datas=array(
                      Input::get('idproyecto'),
                      Input::get('valor'),
                      $campo,
                      Auth::user()->id
                      );
          
          if (Request::ajax()) {
              $datos = GeoProyecto::getUpdateProyecto($datas);
  
              return Response::json(
                  array('rst'=>1,
                      'datos'=>$datos)
              );
          } 

    }

    public function postListar()
    {
       $usuario=Auth::user()->id;
       if (Request::ajax()) {
            $datos = GeoProyecto::getListarProyectos($usuario);

            return Response::json(array('rst'=>1,'datos'=>$datos));
       }
    }

}

