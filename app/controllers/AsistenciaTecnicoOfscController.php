<?php
use Legados\models\Tematicos;
use Illuminate\Support\Collection;

class AsistenciaTecnicoOfscController extends \BaseController
{

    public function postListar()
    {
        $buckets = Input::has("bucket")? Input::get("bucket") : [];
        $fecha = explode(" - ", Input::has("fecha")? Input::get("fecha") : []);
        $download = Input::get("flagdownload", false);
        $select = ["asistencia_tecnico_ofsc.*",
            DB::raw("(CONCAT(u.nombre, ' ', u.apellido)) AS usuario")];
        $asistencias = AsistenciaTecnicoOfsc::select($select)
            ->leftJoin("usuarios as u", "u.id", "=", "asistencia_tecnico_ofsc.usuario_created_at");
        
        $search = Input::get("search", "");
        if ($search!="") {
            $asistencias->whereRaw("(asistencia_tecnico_ofsc.bucket LIKE '%$search%')");
        }
        if (count($buckets) > 0) {
            $asistencias->whereIn("asistencia_tecnico_ofsc.bucket", $buckets);
        }
        if (count($fecha) > 0) {
            $asistencias->whereBetween("asistencia_tecnico_ofsc.fecha", $fecha);
        }

        if ($download) {
            $select = [ "ato.fecha",
                "ato.bucket",
                "asistencia_tecnico_ofsc_detalle.carnet",
                "asistencia_tecnico_ofsc_detalle.fecha_ultimo_officetrack",
                "asistencia_tecnico_ofsc_detalle.fecha_ultimo_ofsc",
                DB::raw("(CASE asistencia_tecnico_ofsc_detalle.asistio WHEN 0 THEN 'Ausente' WHEN 1 THEN 'Asistio' WHEN 2 THEN 'Vacaciones' WHEN 3 THEN 'Permiso' END) AS asistencia"),
                "asistencia_tecnico_ofsc_detalle.created_at AS fecregistro",
                "asistencia_tecnico_ofsc_detalle.updated_at AS fecmodificacion",
                DB::raw("(CASE asistencia_tecnico_ofsc_detalle.estado WHEN 1 THEN 'Menor a 20 minutos' WHEN 2 THEN 'Entre 20 minutos a 2 horas' WHEN 3 THEN 'Entre 2 horas a 4 horas' WHEN 4 THEN 'Mas de 4 horas' END) AS referenciaultimoxy")];
            $excel = AsistenciaTecnicoOfscDetalle::select($select)->leftJoin(
                "asistencia_tecnico_ofsc as ato",
                "ato.id",
                "=",
                "asistencia_tecnico_ofsc_detalle.asistencia_tecnico_ofsc_id"
            );
            if ($search!="") {
                $excel->whereRaw("(ato.bucket LIKE '%$search%')");
            }
            if (!is_null($buckets) && $buckets!="null") {
                if (count($buckets) > 0) {
                    $excel->whereIn("ato.bucket", $buckets);
                }
            }
            if (count($fecha) > 0) {
                $excel->whereBetween("ato.fecha", $fecha);
            }
            $excel   = $excel->get();
            $asistencias = json_decode(json_encode($excel), true);
            $txtnombrefile = "";
            if (count($buckets) > 0 && !is_null($buckets) && $buckets!="null") {
                $txtnombrefile = implode(",", $buckets);
            }
            return Helpers::exportArrayToExcel($asistencias, "AsistenciaTecnicos{$txtnombrefile}");
        }

        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];

        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;

        $column = "id";
        $dir = "desc";
        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }
        $asistencias = $asistencias
                    ->orderBy($column, $dir)
                    ->paginate($grilla["perPage"]);
        $data = $asistencias->toArray();
        $col = new Collection([
            'recordsTotal'=>$asistencias->getTotal(),
            'recordsFiltered'=>$asistencias->getTotal(),
        ]);
        return $col->merge($data);
    }

    public function postEditar()
    {
        $id = Input::get("id", 0);
        $fecha = Input::get("fecha", date("Y-m-d"));
        $bucket = Input::get("bucket");
        $response = ["rst" => 1, "obj" => null, "msj" => ""];
        try {
            if ($id > 0) {
                $asistencia = AsistenciaTecnicoOfsc::with("detalle")->find($id);
            } else {
                $asistencia = AsistenciaTecnicoOfsc::whereIn("bucket", $bucket)->where("fecha", $fecha)->with("detalle")->get();
                if (count($asistencia) == 1) {
                    $asistencia = $asistencia[0];
                }
            }
            $response["obj"] = $asistencia;
            $response["actividades"] = OfscHelper::getActividadesByBucket($bucket);
        } catch (Exception $e) {
            $errorController = new ErrorController();
            $errorController->saveError($e);
            $response["rst"] = 2;
            $response["msj"] = "Error en BD";
        }
        
        return  Response::json($response);
    }

    public function postGuardar()
    {
        $marcasasistencias = [];
        $id = Input::get("id", 0);
        $asistencias = Input::has("asistencias")? Input::get("asistencias") : [];
        $bucket = Input::has("bucket") ? Input::get("bucket")[0] : "";

        foreach ($asistencias as $key => $value) {
            foreach ($value as $key2 => $value2) {
                if (isset($value2["asistio"])) {
                    $marcasasistencias[$key2] = $value2["asistio"];
                } else {
                    $marcasasistencias[$key2] = 0;
                }
            }
        }
        if (count($asistencias) == 0 || count($marcasasistencias) == 0) {
            return Response::json(["rst" => 2, "msj" => "Debe por lo menos a un tecnico indicar su asistencia"]);
        }
        if (!isset($asistencias[$bucket])) {
            return Response::json(["rst" => 2, "msj" => "Los tecnicos no coinciden con el bucket enviado"]);
        }
        $asistencias = $asistencias[$bucket];
        try {
            DB::beginTransaction();
            $detalle = [];
            if ($id > 0) {
                $objasistenciatecnico = AsistenciaTecnicoOfsc::with("detalle")->find($id);
                foreach ($objasistenciatecnico->detalle as $key => $value) {
                    $detalle[$value->carnet] = $value;
                }
            } else {
                $objasistenciatecnico = new AsistenciaTecnicoOfsc;
                $objasistenciatecnico->bucket = $bucket;
                $objasistenciatecnico->fecha = date("Y-m-d");
            }
            $objasistenciatecnico->totaltecnicos = count($asistencias);
            $objasistenciatecnico->totalasistio = 0;
            $objasistenciatecnico->totalausente = 0;
            $objasistenciatecnico->totalpermiso = 0;
            $objasistenciatecnico->totalvacaciones = 0;

            foreach ($marcasasistencias as $key => $value) {
                switch ($value) {
                    case 0:
                        $objasistenciatecnico->totalausente++;
                        break;
                    case 1:
                        $objasistenciatecnico->totalasistio++;
                        break;
                    case 2:
                        $objasistenciatecnico->totalvacaciones++;
                        break;
                    case 3:
                        $objasistenciatecnico->totalpermiso++;
                        break;
                    default:
                        # code...
                        break;
                }
            }
            $objasistenciatecnico->save();
            foreach ($asistencias as $key => $value) {
                if (!isset($detalle[$key])) {
                    $objdetalle                             = new AsistenciaTecnicoOfscDetalle;
                    $objdetalle->asistencia_tecnico_ofsc_id = $objasistenciatecnico->id;
                    $objdetalle->tecnico_id                 = $value["tecnico_id"];
                    $objdetalle->carnet                     = $key;
                } else {
                    $objdetalle                             = AsistenciaTecnicoOfscDetalle::find($detalle[$key]->id);
                }
                
                if ($value["fuente"] == "toa") {
                    $objdetalle->coordx_ofsc                = isset($value["coord_x"])? $value["coord_x"] : null;
                    $objdetalle->coordy_ofsc                = isset($value["coord_y"])? $value["coord_y"] : null;
                }
                if ($value["fuente"] == "officetrack") {
                    $objdetalle->coordx_officetrack         = isset($value["coord_x"])? $value["coord_x"] : null;
                    $objdetalle->coordy_officetrack         = isset($value["coord_y"])? $value["coord_y"] : null;
                }
                if (isset($value["time"])) {
                    if ($value["time"]!="" && $value["time"] > 0) {
                        if ($value["fuente"] == "toa") {
                            $objdetalle->fecha_ultimo_ofsc          = $value["time"];
                        }
                        if ($value["fuente"] == "officetrack") {
                            $objdetalle->fecha_ultimo_officetrack   = $value["time"];
                        }
                    }
                }
                $objdetalle->asistio                    = isset($value["asistio"])? $value["asistio"] : 0;
                $objdetalle->estado                     = isset($value["estado"])? $value["estado"] : 4;
                $objdetalle->estado_ot                     = isset($value["estado_ot"])? $value["estado_ot"] : 4;
                $objdetalle->estado_toa                     = isset($value["estado_toa"])? $value["estado_toa"] : 4;
                $objdetalle->save();
            }
            DB::commit();
            if ($id > 0) {
                return Response::json(["rst" => 1, "msj" => "Asistencia Actualizada con Exito", "obj" => $objasistenciatecnico]);
            } else {
                return Response::json(["rst" => 1, "msj" => "Asistencia Creada con Exito", "obj" => $objasistenciatecnico]);
            }
            
        } catch (Exception $e) {
            DB::rollback();
            $errorController = new ErrorController();
            $errorController->saveError($e);
            
            return Response::json(["rst" => 2, "msj" => "Error al Guardar"]);
        }
    }

    public function postReporte()
    {
        $fecha = Input::get("fecha", "");
        $bucket = Input::get("bucket", "");
        $fechas = [];
        if ($fecha == "") {
            return  Response::json(["rst" => 2, "msj" => "Fecha esta vacia"]);
        }
        $fecha = explode(" - ", $fecha);
        if (count($fecha) == 2) {
            $objfechainicio = new DateTime($fecha[0]);
            $objfechafin = new DateTime($fecha[1]);
            $interval = $objfechainicio->diff($objfechafin);
            $dias = isset($interval->d)? $interval->d : 0;
            for ($i = 0; $i <=$dias; $i++) {
                $fechas [] = date("Y-m-d", strtotime("+{$i} day", strtotime($fecha[0])));
            }
            try {
                $asistencias = AsistenciaTecnicoOfscDetalle::from("asistencia_tecnico_ofsc_detalle as atod")
                ->select("ato.bucket", "ato.fecha", "atod.carnet", "atod.asistio")
                ->leftJoin("asistencia_tecnico_ofsc as ato", "ato.id", "=", "atod.asistencia_tecnico_ofsc_id");
                if (count($fecha) == 2) {
                    $asistencias->whereRaw(" (ato.fecha BETWEEN '{$fecha[0]}' AND '{$fecha[1]}') ");
                }
                if ($bucket!="") {
                    $asistencias->where("ato.bucket", $bucket);
                }
                $asistencias = $asistencias->get();
                $resultadoagrupado = [];
                foreach ($asistencias as $key => $value) {
                    $resultadoagrupado[$value->carnet][$value->fecha] = $value;
                }
                return  Response::json(["rst" => 1, "obj" => $resultadoagrupado, "fechas" => $fechas]);
            } catch (Exception $e) {
                $errorController = new ErrorController();
                $errorController->saveError($e);
                return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
            }
        }
        return  Response::json(["rst" => 2, "msj" => "Formato de Fecha Invalido"]);
    }
}
