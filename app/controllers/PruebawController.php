<?php

use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\ComponenteCms;
use Ofsc\Activity;
use Ofsc\Inbound;
use Legados\STCmsApi;
use Legados\models\SolicitudTecnicaLogRecepcion as LogST;
use Legados\models\Error;

class PruebawController extends \BaseController {

	public function getServer()
    {
        function consultar($request)
        {
            Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_consulta.log');
            Log::info([$request]);

            if ($request->tipo_solicitud =='CONSULTA') {
                $averia=array();                

                if (isset($request->cod_averia)) {    
                    try {
                        $averia = PreTemporal::where("codactu", $request->cod_averia)->first();
                        if (!is_null($averia)) {	                    	
                            $respuesta=array(
                                'datos'=>array(
                                    'tipo_operacion'=>'CONSULTA',
                                    'id'=>$averia['id'],
                                    'date'=>date("d/m/Y"),
                                    'time'=>date("H:i:s"),
                                    'codcli'=>$averia['codcli'],
                                )
                            );
                            $respuesta=$respuesta['datos'];
                        } else {
                            $error = Error::find(13);
                            $respuesta = array(
                                'code_error'=>$error->code,
                                'desc_error'=>$error->message
                            );
                        }
                    } catch (Exception $e) {
                        $error = Error::find(10);
                        $respuesta = array(
                            'code_error'=>$error->code, //Error de server
                            'desc_error'=>$e->getMessage()
                        );
                    }
                } else {
                    $error = Error::find(3);
                    $respuesta = array(
                                'code_error'=>$error->code,
                                'desc_error'=>$error->message
                            );
                }            
            return $respuesta;
        	}
    	}

        function generar($request){
            
        }

        try {
            $server = new SoapServer(Config::get('legado.wprueba_web_service.wsdl'));
            $server->addFunction("consultar");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }

}