<?php

class NotificacionTestController extends \BaseController
{
    protected $_errorController;
    protected $_estadoRespuesta;

    public function __construct(ErrorController $errorController,
        EstadoRespuestaTest $estadoRespuestatest)
    {
        $this->_errorController = $errorController;
        $this->_estadoRespuesta = $estadoRespuestatest;
    }
    public function failed()
    {
        // Called when the job is failing...
        Log::error('Job failed!');
    }
    public function fire($job, $data)
    {

        $id = $job->getJobId();
        
        for ( $i=0; $i < count($data); $i++ ) {
            $data[$i]['url_recibido']='2'; //'telefonica-pe.test'
            Mensaje::crear($id, $data[$i]);
            $this->_estadoRespuesta->recibir($data[$i]);

            if ( $this->_estadoRespuesta->getCodactu()!='') {
                $detalle = $this->_estadoRespuesta->responder();
            } elseif ($this->_estadoRespuesta->getSubject()=='INT_SMS'
                || $this->_estadoRespuesta->getSubject()=='TOA_VALIDATE_COORDS') {
                $detalle = $this->_estadoRespuesta->responder();
            }
        }
        $job->delete();
    }
}