<?php
use Ofsc\Resources;
class TecnicoController extends BaseController
{

    public function __construct()
    {
        /*$this->beforeFilter('auth'); // bloqueo de acceso
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );*/
    }
    /**
     * mostrar componente especifico
     */
    public function show($carnet)
    {
        $tecnico = Tecnico::where('carnet', $carnet)->first();
        //$componentes = CatComponente::getComponente($gestionId);
        return $tecnico;
    }
    /**
     * Store a newly created resource in storage.
     * POST /tecnico/cargar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        /*$tecnicos = Cache::get('listTecnicoAll');
        if (!$tecnicos) {
            $tecnicos = Cache::remember('listTecnicoAll', 24*60*60, function () {
                return Tecnico::getAllTecnicos();
            });
        }*/

        $tecnicos = Tecnico::getAllTecnicos();

        if (Request::ajax()) {
            return Response::json(array('rst'=>1,'datos'=>$tecnicos));
        } elseif (Input::has('excel')) {
            return Helpers::exportArrayToExcel($tecnicos, 'tecnicos');
        } elseif (Input::has('from')) {
            return Response::json(array('rst'=>1,'datos'=>$tecnicos));
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /tecnico/listar
     *
     * @return Response
     */
    public function postListar()
    {
      
        if (Request::ajax()) {
            if (Input::get('empresa_id')) {
                $actividad = DB::table('tecnicos as t')
                ->join(
                    'celula_tecnico as ct',
                    'ct.tecnico_id',
                    '=',
                    't.id'
                )
                ->join(
                    'celulas as c',
                    'c.id',
                    '=',
                    'ct.celula_id'
                )
                ->select(
                    't.id',
                    't.nombre_tecnico as nombre',
                    DB::raw(
                        'CONCAT(
                            GROUP_CONCAT( CONCAT("C",ct.celula_id) 
                                SEPARATOR "|,|" 
                            ),"|,|",
                            GROUP_CONCAT( DISTINCT(CONCAT("E",c.empresa_id) )
                                SEPARATOR "|,|"
                            )
                        ) as relation'
                    ),
                    DB::raw(
                        'GROUP_CONCAT(
                            DISTINCT(CONCAT("C",ct.celula_id,
                                            "-",ct.officetrack
                                            )
                                    ) 
                                SEPARATOR "|,|"
                        ) AS evento'
                    )
                )
                ->where('t.estado', '=', '1')
                ->where('ct.estado', '=', '1')
                ->where('c.empresa_id', '=', Input::get('empresa_id'))
                ->where(
                    function ($query) {
                        if (Input::get('zonal_id')) {
                            $query->whereRaw(
                                'c.zonal_id="'.Input::get('zonal_id').'"'
                            );
                        }
                    }
                )
                ->groupBy('t.id')
                ->orderBy('t.nombre_tecnico')
                ->get();
            } elseif (Input::get('parametros')) {
                $actividad = DB::table('tecnicos as t')
                ->select(
                    't.carnet_tmp as id',
                    't.nombre_tecnico as nombre'
                )
                ->where('t.estado', '=', '1')
                ->get();
            } else {
                $actividad = DB::table('tecnicos as t')
                ->join(
                    'celula_tecnico as ct',
                    'ct.tecnico_id',
                    '=',
                    't.id'
                )
                ->join(
                    'celulas as c',
                    'c.id',
                    '=',
                    'ct.celula_id'
                )
                ->select(
                    't.id',
                    't.nombre_tecnico as nombre',
                    DB::raw(
                        'CONCAT(
                            GROUP_CONCAT( CONCAT("C",ct.celula_id) 
                                SEPARATOR "|,|" 
                            ),"|,|",
                            GROUP_CONCAT( DISTINCT(CONCAT("E",c.empresa_id) )
                                SEPARATOR "|,|"
                            )
                        ) as relation'
                    )
                )
                ->where('t.estado', '=', '1')
                ->where('ct.estado', '=', '1')
                ->groupBy('t.id')
                ->orderBy('t.nombre_tecnico')
                ->get();
            }
            return Response::json(array('rst'=>1,'datos'=>$actividad));
        }
    }
    /**
     * obtener celulas por tecnico, para cargar en mantenimiento de tecnicos
     */
    public function postCargarcelulas()
    {
        $tecnicoId = Input::get('tecnico_id');
        $celulas = Tecnico::getCelulas($tecnicoId);
        return Response::json(array('rst'=>1,'datos'=>$celulas));
    }
    /**
     * Listar registro de celula_quiebre con estado 1
     * POST /tecnico/listarcelula
     *
     * @return Response
     */
    public function postListarcelula()
    {
        $tecnicoId = Input::get('tecnico_id');
        //si la peticion es ajax
        if (Request::ajax()) {

            $celulaTecnico = DB::table('celula_tecnico as ct')
                ->rightJoin(
                    'celulas as c',
                    function ($join) use ($tecnicoId) {
                        $join->on('ct.celula_id', '=', 'c.id')
                        ->on('ct.tecnico_id', '=', DB::raw($tecnicoId));
                    }
                )
                ->where('c.estado', '=', 1)
            ->get(array('c.id', 'c.nombre', 'ct.estado'));

            return Response::json(array('rst' => 1, 'datos' => $celulaTecnico));
        }
    }

/**
     * Store a newly created resource in storage.
     * POST /tecnico/crear
     *
     * @return Response
     */
    public function postCrear()
    {
        //si la peticion es ajax
        // if (Request::ajax()) {
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú_-]{2,60})$/i';
            $required='required';
            $numeric='numeric';

            $reglas = array(
                'ape_paterno' => $required.'|'.$regex,
                'ape_materno' => $required.'|'.$regex,
                'nombres' => $required.'|'.$regex,
                'dni' => $required.'|min:8',
                'carnet' => $required."|unique:tecnicos",
                'empresa_id' => $required.'|'.$numeric,
                'celular' => $required
                //'celulas_selec' => $required,
            );

            $mensaje= array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
                'numeric'   => ':attribute seleccione una opcion',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst'=>2,
                    'msj'=>$validator->messages(),
                    )
                );
            }
            $apeP = Input::get('ape_paterno');
            $apeM = Input::get('ape_materno');
            $nombres = Input::get('nombres');
            $carne = Input::get('carnet');

            $tecnicos = new Tecnico;
            $tecnicos['ape_paterno']= $apeP;
            $tecnicos['ape_materno']= $apeM;
            $tecnicos['nombres']= $nombres;
            $tecnicos['celular']= Input::get('celular');
            $tecnicos['nombre_tecnico'] = $apeP.' '.$apeM.' '.$nombres;
            $tecnicos['dni']= Input::get('dni');
            $tecnicos['carnet']= $carne;
            if (Input::has('carnet_tmp') && Input::get('carnet_tmp') <>'') {
                $carneTmp = Input::get('carnet_tmp');
            } else {
                $carneTmp = $carne;
            }

            $tecnicos['carnet_tmp'] = $carneTmp;
            $tecnicos['ninguno']= Input::get('ninguno', 0);
            $tecnicos['estado']= Input::get('estado');
            $tecnicos['empresa_id']= Input::get('empresa_id');
            $tecnicos["bucket"] = Input::get("bucket", "");
            $tecnicos["bucket_id"] = Input::get("bucket_id", "");
            $tecnicos["imei"] = Input::get("imei");
            $tecnicos["imsi"] = Input::get("imsi");
            $tecnicos["marca"] = Input::get("marca");
            $tecnicos["modelo"] = Input::get("modelo");
            $tecnicos["observacion"] = Input::get("observacion");
            $tecnicos["version"] = Input::get("version");
            $tecnicos["fecha_inicio_toa"] = Input::get("fecha_inicio_toa");
            $tecnicos["cargo_tecnico"] = Input::get("cargo_tecnico");
            $tecnicos["supervisor"] = Input::get("supervisor");
            $tecnicos->save();

            if (Input::get("imei") != "" || 
                Input::get("imsi") != "" || 
                Input::get("marca") != "" || 
                Input::get("modelo") != "" || 
                Input::get("version") != "") {
                $tenicoHistorial = new TecnicoHistorial();
                $tenicoHistorial->tecnico_id = $tecnicos->id;
                $tenicoHistorial->imei = Input::get("imei");
                $tenicoHistorial->imsi = Input::get("imsi");
                $tenicoHistorial->marca = Input::get("marca");
                $tenicoHistorial->modelo = Input::get("modelo");
                $tenicoHistorial->version = Input::get("version");
                $tenicoHistorial->save();
            }

            // $celulas=explode(',', Input::get('celulas_selec'));
            $celulas=Input::get('celulas');
            if( count($celulas) and $celulas[0]!='' ){

                for ($i=0; $i<count($celulas); $i++) {
                    $celulaId = $celulas[$i]['id'];
                    $celula = Celula::find($celulaId);
                    // $officetrack = Input::get('officetrack'.$celulaId, 0);
                    $officetrack = $celulas[$i]['officetrack'];
                    if ($officetrack) {
                    // if ($officetrack === 'on') {
                        $officetrack = 1;
                    }
                    
                    $tecnicos->celulas()->save(
                        $celula,
                        array(
                            'estado'=>1,
                            'officetrack'=> $officetrack
                        )
                    );

                    /*$celulaTecnico = new CelulaTecnico();
                    $celulaTecnico->tecnico_id = $tecnicos->id;
                    $celulaTecnico->celula_id = $celulaId;
                    $celulaTecnico->officetrack = $officetrack;
                    $celulaTecnico->estado = 1;
                    $celulaTecnico->save();*/
                }

            }
            
            Cache::forget("listTecnicoAll");
            Cache::forget("listTecnicoLegado");
            Cache::forget("listarTecnicoBandeja");
            return Response::json(
                array(
                'rst'=>1,
                'msj'=>'Registro realizado correctamente',
                )
            );
        // }
    }

    /**
     * actualizar los quiebres y actividades relacionadas
     * POST /tecnico/editar
     *
     * @return Response
     */
    public function postEditar()
    {
        // if (Request::ajax()) {
            $tecnicoId = Input::get('id');
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú_-]{2,60})$/i';
            $required='required';
            $reglas = array(
                'ape_paterno' => $required.'|'.$regex,
                'ape_materno' => $required.'|'.$regex,
                'nombres' => $required.'|'.$regex,
                'dni'       => 'required|min:8',
                'carnet' => $required.'|unique:tecnicos,carnet,'.$tecnicoId,
                'empresa_id' => 'required|numeric'
                //'celulas_selec' => $required,
            );

            $mensaje= array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst'=>2,
                    'msj'=>$validator->messages(),
                    )
                );
            }
            $apeP = Input::get('ape_paterno');
            $apeM = Input::get('ape_materno');
            $nombres = Input::get('nombres');
            $carne = Input::get('carnet');
            //editando quiebre
            $tecnicos = Tecnico::find($tecnicoId);
            $tecnicos['ape_paterno']= $apeP;
            $tecnicos['ape_materno']= $apeM;
            $tecnicos['nombres']= $nombres;
            $tecnicos['celular']= Input::get('celular');
            $tecnicos['nombre_tecnico']= $apeP.' '.$apeM.' '.$nombres;
            $tecnicos['dni']= Input::get('dni');
            $tecnicos['carnet']= $carne;
            if (Input::has('carnet_tmp') && Input::get('carnet_tmp') <>'') {
                $carneTmp = Input::get('carnet_tmp');
            } else {
                $carneTmp = $carne;
            }
            
            $tecnicos['carnet_tmp']= $carneTmp;
            $tecnicos['ninguno']= Input::get('ninguno', 0);
            $tecnicos['estado']= Input::get('estado');
            $tecnicos['empresa_id']= Input::get('empresa_id');
            $tecnicos["bucket"] = Input::get("bucket", "");
            $tecnicos["bucket_id"] = Input::get("bucket_id");
            $tecnicos["imei"] = Input::get("imei");
            $tecnicos["imsi"] = Input::get("imsi");
            $tecnicos["marca"] = Input::get("marca");
            $tecnicos["modelo"] = Input::get("modelo");
            $tecnicos["observacion"] = Input::get("observacion");
            $tecnicos["version"] = Input::get("version");
            $tecnicos["fecha_inicio_toa"] = Input::get("fecha_inicio_toa");
            $tecnicos["cargo_tecnico"] = Input::get("cargo_tecnico");
            $tecnicos["supervisor"] = Input::get("supervisor");
            $tecnicos->save();

            if (Input::get("imei") != Input::get("imeiOld") || 
                Input::get("imsi") != Input::get("imsiOld") || 
                Input::get("marca") != Input::get("marcaOld") || 
                Input::get("modelo") != Input::get("modeloOld") || 
                Input::get("version") != Input::get("versionOld")) {
                // insertamos tabla nueva GG
                $tenicoHistorial = new TecnicoHistorial();
                $tenicoHistorial->tecnico_id = $tecnicoId;
                $tenicoHistorial->imei = Input::get("imei");
                $tenicoHistorial->imsi = Input::get("imsi");
                $tenicoHistorial->marca = Input::get("marca");
                $tenicoHistorial->modelo = Input::get("modelo");
                $tenicoHistorial->version = Input::get("version");
                $tenicoHistorial->save();
            }

            // $celulas=explode(',', Input::get('celulas_selec'));
            $celulas=Input::get('celulas');

            //actulizando a estado 0 segun quiebre seleccionado
            DB::table('celula_tecnico')
                    ->where('tecnico_id', $tecnicoId)
                    ->update(array('estado' => 0, 'officetrack' => 0));

            //si estado de tecnico esta activo y selecciono celulas
            if (Input::get('estado') == 1 and !empty($celulas)) {

                for ($i=0; $i<count($celulas); $i++) {
                    // $celulaId = $celulas[$i];
                    $celulaId = $celulas[$i]['id'];
                    $celula = Celula::find($celulaId);
                    //buscando en la tabla
                    $celulaTecnico = DB::table('celula_tecnico')
                        ->where('tecnico_id', '=', $tecnicoId)
                        ->where('celula_id', '=', $celulaId)
                        ->first();
                    //officetrack
                    $officetrack = $celulas[$i]['officetrack'];
                    // $officetrack = Input::get('officetrack'.$celulaId, 0);
                    // if ($officetrack === 'on') {
                    if ($officetrack) {
                        $officetrack = 1;
                    }
                    if (is_null($celulaTecnico)) {
                        // if( count($celulas) and $celulas[0]!='' ){
                            $tecnicos->celulas()->save(
                                $celula,
                                array(
                                    'estado' => 1,
                                    'officetrack' => $officetrack
                                )
                            );

                            /*$tecnicoMongo = new CelulaTecnicoMongo();
                            $tecnicoMongo->tecnico_id = $tecnicos->id;
                            $tecnicoMongo->celula_id = $celulaId;
                            $tecnicoMongo->officetrack = $officetrack;
                            $tecnicoMongo->estado = 1;
                            $tecnicoMongo->save();*/
                        // }
                    } else {
                        DB::table('celula_tecnico')
                            ->where('tecnico_id', '=', $tecnicoId)
                            ->where('celula_id', '=', $celulaId)
                            ->update(
                                array(
                                    'estado' => 1,
                                    'officetrack' => $officetrack
                                    )
                            );
                    }
                }

            }
            Cache::forget("listTecnicoAll");
            Cache::forget("listTecnicoLegado");
            Cache::forget("listarTecnicoBandeja");
            return Response::json(
                array(
                'rst'=>1,
                'msj'=>'Registro actualizado correctamente',
                )
            );
        // }
    }

    /**
     * Cambiar estado del registro de quiebre, ello implica cambiar el estado de
     * la tabla celula_quiebre, actividad_quiebre.
     * POST /tecnico/cambiarestado
     *
     * @return Response
     */
    public function postCambiarestado()
    {
        // if (Request::ajax()) {
            $tecnico = Tecnico::find(Input::get('id'));
            $tecnico->estado = Input::get('estado');
            $tecnico->save();
            if (Input::get('estado') == 0) {
                DB::table('celula_tecnico')
                        ->where('tecnico_id', Input::get('id'))
                        ->update(array('estado' => 0));
            }
            Cache::forget("listTecnicoAll");
            Cache::forget("listTecnicoLegado");
            Cache::forget("listarTecnicoBandeja");
            return Response::json(
                array(
                'rst'=>1,
                'msj'=>'Registro actualizado correctamente',
                )
            );
        // }
    }

    public function postEstadoofficetrack()
    {
        $estado= Tecnico::getEstadoOfficetrack();
        return $estado;
    }

    public function getArbol()
    {
        $tree = new Tecnico;
        $elementos = $tree->get();
        $padres = $elementos["padres"];
        $hijos = $elementos["hijos"];

        $datos = array();
        // ***************************************************************************
        foreach ($padres as $padre) {
            $arraypadre = array (
                "id"        => $padre->id,
                "parent"    => "#",
                "icon"      => "fa fa-cubes",
                "text"      => $padre->nombre_recurso
            );
            array_push($datos, $arraypadre);
        }

        foreach ($hijos as $hijo) {
            $pos = strpos($hijo->id, "bucket");
            $icon = $pos === false ? "fa fa-user" : "fa fa-cube";
            $arrayhijo = array (
                "id"        => $hijo->id,
                "parent"    => $hijo->parent_id,
                "icon"      => $icon,
                "text"      => $hijo->nombre_recurso
            );
            array_push($datos, $arrayhijo);
        }

        return $datos;
        // ***************************************************************************
    }

    public function postEditarbuket()
    {
        if (Auth::user()->perfil_id != 8) {

            $horarioResource = HorarioResource::where('dia_id', date("N"))->where('estado', 1)->first();
            if (is_null($horarioResource)) {
                return [
                    "rst" => 2,
                    "msj" => "No hay rango de hora configurado para este dia"
                ];
            }

            $fechaIni = date("Y-m-d").$horarioResource->hora_inicio;
            $fechaFin = date("Y-m-d").$horarioResource->hora_fin;
            $fechaIni = new DateTime($fechaIni);
            $fechaFin = new DateTime($fechaFin);

            if (date("Y-m-d H:i:s") < $fechaIni->format("Y-m-d H:i:s") || 
                date("Y-m-d H:i:s") > $fechaFin->format("Y-m-d H:i:s")) {
                return [
                    "rst" => 2,
                    "msj" => "El rango para mover los tecnicos es 
                             {$fechaIni->format('H:i:s')} - {$fechaFin->format('H:i:s')}"
                ];
            }
        }

        $tecnicoArray = explode("_", Input::get("idtecnico"));
        $bucketArray = explode("_", Input::get("idbucket"));
        $empresaArray = explode("_", Input::get("idempresa"));
        
        $idtecnico = $tecnicoArray[1];
        $idbucketDestino = $bucketArray[1];
        $idempresaDestino = $empresaArray[1];

        $tecnico = Tecnico::find($idtecnico);

        if ($idempresaDestino == $tecnico->empresa_id) {
            $bucket = Bucket::find($idbucketDestino);
            if (is_null($bucket)) {
                $rst = 2;
                $msj = "Bucket no existe en PSI";
            } else {
                $resource = new Resources();
                $response = $resource->updateBucket($tecnico->carnet_tmp, $bucket->bucket_ofsc);

                if ($response->error == 1) {
                    $errorMsg = explode("::::", $response->errorMsg);
                    return [
                        "rst" => 2,
                        "msj" => isset($errorMsg[3]) ? 
                                 $errorMsg[3] : "Error desconocido en TOA"
                    ];
                }

                if (isset($response->data['resourceId'])) {
                    $tecnico = Tecnico::find($idtecnico);
                    $tecnico->bucket_id = $idbucketDestino;
                    $tecnico->save();

                    $logTecnicosBucket = new LogTecnicosBucket();
                    $logTecnicosBucket->tecnico_id = $idtecnico;
                    $logTecnicosBucket->bucket_id = $idbucketDestino;
                    $logTecnicosBucket->usuario_created_at = Auth::id();
                    $logTecnicosBucket->save();

                    $rst = 1;
                    $msj = "Dastos actualizados correctamente en TOA y PSI";
                } else {
                    if (isset($response->data['detail'])) {
                        $rst = 2;
                        $msj = $response->data['detail'];
                    } else {
                        $rst = 2;
                        $msj = "Error desconocido en TOA";
                    }
                }
            }
        } else {
            $rst = 2;
            $msj = "El Tecnico solo puede ser movido entre bucket de la misma empresa.";
        }

        return [
            "rst" => $rst,
            "msj" => $msj
        ];
    }

    public function getArbolmedia()
    {
        $tecnico = new Tecnico;
        $elementos = $tecnico->get();
        $padres = $elementos["padres"];
        $hijos = $elementos["hijos"];

        $datos = array();
        $estadistico = $tecnico->getEstadistico();
        $totalEmpresados = [];

        foreach ($padres as $padre) {
            $validacion = 0;
            $totalEmpresa = 0;
            $arraypadre = array(
                "id"        => $padre->id,
                "parent"    => "#",
                "icon"      => "fa fa-cubes",
            );

            foreach ($estadistico["empresas"] as $empresa) {
                if ($empresa->empresa_id == $padre->empresa_id) {
                    $validacion++;
                    $totalEmpresa = $empresa->total;

                    $totalEmpresados[$empresa->empresa_id] = $empresa->total;
                }
            }

            if ($validacion > 0) {
                $arraypadre['text'] = '<span>'.$padre->nombre_recurso.'</span>'.'<span style="position: absolute; right: 24px; margin-right:15px;"><b style="color: #a94442"><i>'.$totalEmpresados[$padre->empresa_id].'</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;<b style="color: #337ab7"><i>100%</b></i></span>';
            } else {
                $arraypadre['text'] = '<span>'.$padre->nombre_recurso.'</span>'.'<span style="position: absolute; right: 24px; margin-right:15px;"><b style="color: #a94442"><i>0</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;<b style="color: #337ab7"><i>0%</b></i></span>';
            }

            array_push($datos, $arraypadre);
        }

        foreach ($hijos as $hijo) {
            $pos = strpos($hijo->id, "bucket");
            $icon = $pos === false ? "fa fa-user" : "fa fa-cube";
            $arrayhijo = array (
                "id"        => $hijo->id,
                "parent"    => $hijo->parent_id,
                "icon"      => $icon,
                "text"      => $hijo->nombre_recurso
            );

            if ($pos === false) {

            } else {
                $validacionHijo = 0;
                $totalBucket = 0;
                $empresaId = 0;
                foreach ($estadistico["bukets"] as $bucket) {
                    if ($bucket->bucket_id == $hijo->bucket_id) {
                        $validacionHijo++;
                        $totalBucket = $bucket->total;
                        $empresaId = $bucket->empresa_id;
                    }
                }

                if ($validacionHijo > 0) {
                    $media = round(($totalBucket / $totalEmpresados[$empresaId]) * 100);
                    $arrayhijo['text'] = '<span>'.$hijo->nombre_recurso.'</span>'.'<span style="position: absolute; right: 24px; margin-right:15px;"><b style="color: #a94442"><i>'.$totalBucket.'</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;<b style="color: #337ab7"><i>'.$media.'%</b></i></span>';
                } else {
                    $arrayhijo['text'] = '<span>'.$hijo->nombre_recurso.'</span>'.'<span style="position: absolute; right: 24px; margin-right:15px;"><b style="color: #a94442"><i>0</i></b>&nbsp;&nbsp;|&nbsp;&nbsp;<b style="color: #337ab7"><i>0%</b></i></span>';
                }
            }

            array_push($datos, $arrayhijo);
        }

        return $datos;
    }
}
