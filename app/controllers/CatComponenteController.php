<?php

class CatComponenteController extends \BaseController
{

    /**
     * Store a newly created resource in storage.
     * POST /cat_componente/cargar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $gestionId = Input::get('gestion_id', '');
            $codactu = Input::get('codactu', '');
            $componentes = CatComponente::getComponente($gestionId, $codactu);
            return Response::json(array('rst' => 1, 'datos' => $componentes));
        }
    }

    /**
     * Listar registro de celulas con estado 1
     * POST /cat_componente/listar
     *
     * @return Response
     */
    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $redis = Redis::connection();
            try {
                $redis->ping();
            } catch (Exception $e) {}
            if ( !isset($e) ) {
                $r = Cache::get('listarCat_componenteBandeja');
                if ( $r ) {
                    $r = Cache::get('listarCat_componenteBandeja');
                } else {
                    $r = Cache::remember('listarCat_componenteBandeja', 20, function(){
                        $datos = DB::table('cat_componentes AS cp')
                            ->select(
                                DB::raw('MAX(cp.cod_componente) AS id'), 
                                'cp.desc_componente AS nombre'
                            )
                            ->groupBy('cp.desc_componente')
                            ->orderBy('cp.desc_componente')
                            ->get();

                        return $datos;
                    });
                }
            } else {
                $r = DB::table('cat_componentes AS cp')
                    ->select(
                        DB::raw('MAX(cp.cod_componente) AS id'), 
                        'cp.desc_componente AS nombre'
                    )
                    ->groupBy('cp.desc_componente')
                    ->orderBy('cp.desc_componente')
                    ->get();
            }

            return Response::json(array('rst' => 1, 'datos' => $r));
        }
    }
    /**
     * mostrar componente especifico
     */
    public function show($gestionId)
    {
        $comp  = array();
        $modem = array();
        $componentes = CatComponente::getComponente($gestionId);
        foreach ($componentes as $c) {
            if($c->modem == 1) {
                array_push ($modem , $c);
            } else {
                array_push ($comp , $c);
            }
        }
        $datos = array (
            'componentes' => $comp, 
            'modem'       => $modem
            );
        return $datos;
    }
    /**
     * activar componente
     */
    public function store()
    {
        //validar si componente: se hizo refresh o se activo
        //buscar en la tabla de webpsi_officetrack.catalogo_decos
        //por tarjeta y serie
        $required='';
        $accion = Input::get('accion', '');
        if ($accion=='activacion') 
            $required = 'Required|';
        
        $reglas = array(
            'gestion_id'    => 'Required|Integer',
            'accion'        => 'Required|AlphaNum|Max:20',
            'serie'         => 'Required|AlphaNum|Max:20',
            'tarjeta'       => $required.'AlphaNum|Max:20',
            'telefono'      => 'Required|Max:20',
        );
        $mensaje = array(
            'required'      => ':attribute Es requerido',
            'Alpha'         => ':attribute Solo debe ser Texto',
            'AlphaNum'      => ':attribute Solo debe ser alfanumerico',
            'Integer'       => ':attribute Solo debe ser entero',
        );

        //validacion repetidos
        $gestion_id = Input::get('gestion_id');
        $serie = Input::get('serie');

        $deco = DB::table('webpsi_officetrack.catalogo_decos')
                ->select('resultado')
                ->where('gestion_id', '=', $gestion_id)
                ->where('serie', '=', $serie)
                ->first();
        if (isset($deco) && $deco->resultado == 'NoProcesado') {
            return Response::json(array(
                'rst' => 2,
                'msj' => 'La serie '.$serie.' se encuentra en proceso',
                'gestion_id' => $gestion_id,
                'serie' =>$serie,
                )
            );
        }

        if ( $accion != 'modem') {
            $validator = Validator::make(Input::all(), $reglas, $mensaje);
            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }
        }
        $response = '';

        $array = [
            'gestion_id' => Input::get('gestion_id',''),
            'asunto' => Input::get('accion',''),
            'serieDeco' => Input::get('serie',''),
            'serieTarjeta' => Input::get('tarjeta',''),
            'macModem' => Input::get('mac',''),
            'resultado' => $response,
            'telefonoOrigen' => Input::get('telefono',''),
            'tipo' => '2'
        ];

        Input::replace($array);

        $r = CatalogoDecos::registrarAR();
        $r['id'] = Input::get('gestion_id');

        if ( isset($r['msj']) && $r['msj'] != '' ) {
            Sms::enviar($array['telefonoOrigen'], $r['msj'], '397');
        }
        return Response::json($r);
    }

}
