<?php
use Legados\models\SolicitudTecnicaGestion;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\ComponenteCms;
use Legados\models\TipoOperacion;
use Ofsc\Activity;
use Ofsc\Inbound;
use Legados\STCmsApi;
use Legados\models\SolicitudTecnicaLogRecepcion as LogST;
use Legados\models\Error;
use Legados\models\RetornoLegado as RetornoLegado;
use Legados\STGestelApi;

use Legados\Repositories\TransaccionesGoRepository as TransaccionesGoRepo;

class SolicitudTecnicaController extends BaseController
{
    public function getServer()
    {
        function generar($request)
        {
            // Desglosamos la solicitud tecnica y los componentes
            Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_generar.log');
            Log::info([$request]);
            //Auth::loginUsingId(\Config::get("legado.user_logs.genera_solicitud"));
            /*$log = new LogST;
            $log->id_solicitud_tecnica = $request->id_solicitud_tecnica;
            $log->trama = json_encode($request);
            $log->usuario_created_at = \Config::get("legado.user_logs.genera_solicitud");
            $log->save();*/

            $log_array = [
                'id_solicitud_tecnica' => $request->id_solicitud_tecnica,
                'trama' => json_encode($request),
                'usuario_created_at' => \Config::get("legado.user_logs.genera_solicitud"),
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ];

            $logId = DB::table('solicitud_tecnica_log_recepcion')
                ->insertGetId($log_array);

            //Auth::logout();
            $response=[
                'id_solicitud_tecnica' => $request->id_solicitud_tecnica,
                'id_respuesta'=>"1", 'observacion'=> "OK"
            ];
            $envio = ["request" => $request, "logId" => $logId];
            Queue::push('GenerarSolicitudTecnica', $envio, 'generar_st');

            //(1) REGISTRO CMS-PSI
            $transaccionesGoRepo = new TransaccionesGoRepo;
            $transaccionesGoRepo->registrarLogsFlujo([
                "id_solicitud_tecnica" => $request->id_solicitud_tecnica,
                "flujo" => 1,
                "tipo" => 1,
                "tipo_operacion" => $request->tipo_operacion
            ]);

            return $response;
        }
        /**
         * consultar
         */
        function consultar($request)
        {
            Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_consulta.log');
            Log::info([$request]);
            if ($request->tipo_operacion=='CONSULTA') {
                $solicitud=array();
                $idsolicitud=$request->id_solicitud_tecnica;

                if ($idsolicitud!='') {
    
                    if (!ctype_alnum($idsolicitud)
                        || strlen($idsolicitud)>11
                        || !is_numeric(substr($idsolicitud, 1)) ) {
    
                        $error = Error::find(2);

                        return array(
                            'code_error'=>$error->code,
                            'desc_error'=>$error->message
                        );
                    }
    
                    try {
                        //['SolicitudTecnica','horario','estadoOfsc','tecnico']
                        //SolicitudTecnica->();
                        $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $idsolicitud)->getUltimo()->first();

                        if (!is_null($solicitud)) {
                            if (isset($solicitud->estado_ofsc_id)) {
                                $objEstadoOfsc = \EstadoOfsc::find($solicitud->estado_ofsc_id);
                                if (!is_null($objEstadoOfsc)) {
                                    $estadotoa = $objEstadoOfsc->name_corto;
                                    $descripciontoa = $objEstadoOfsc->nombre;
                                } else {
                                    $estadotoa='';
                                    $descripciontoa='';
                                }
                            } else {
                                $estadotoa='none';
                                $descripciontoa='No enviado';
                            }

                            $solitudtecnicaid=$solicitud->solicitud_tecnica_id;
                            $detallesolicitud=null;
                            $contrata='';
                            $descontrata='';

                            if($solicitud->tipo_legado==1) { //cms
                                $detallesolicitud = SolicitudTecnicaCms::where("solicitud_tecnica_id", $solitudtecnicaid)->first();
                                $contrata=$detallesolicitud['codigo_contrata'];
                                $descontrata=$detallesolicitud['desc_contrata'];
                            } else { //gestel
                                if($solicitud->actividad_id==2) { //provision
                                    $detallesolicitud = SolicitudTecnicaGestelProvision::where("solicitud_tecnica_id", $solitudtecnicaid)->first();
                                    $contrata=$detallesolicitud['contrata'];
                                    $descontrata='';
                                } elseif ($solicitud->actividad_id==1) { //averia
                                    $detallesolicitud = SolicitudTecnicaGestelAveria::where("solicitud_tecnica_id", $solitudtecnicaid)->first();
                                    $contrata=$detallesolicitud['contrata'];
                                    $descontrata='';
                                }
                            }

                            $fregistro='';
                            if($solicitud['time_of_booking']!==null)
                                $fregistro=date_format(date_create($solicitud['time_of_booking']), 'd/m/Y H:i:s');

                            $fasignacion='';
                            if($solicitud['time_of_assignment']!==null)
                                $fasignacion=date_format(date_create($solicitud['time_of_assignment']), 'd/m/Y H:i:s');
                            
                            $hinicio='';
                            if($solicitud['start_time']!==null)
                                $hinicio=date_format(date_create($solicitud['start_time']), 'His');
                            
                            $hfin='';
                            if($solicitud['end_time']!==null){
                                $hfin=date_format(date_create($solicitud['end_time']), 'His');
                            }

                            $intervalo='';
                            if($solicitud['intervalo']!==null)
                                $intervalo=$solicitud['intervalo'];
                            
                            $fagenda='';
                            if($solicitud['fecha_agenda']!==null)
                                $fagenda=$solicitud['fecha_agenda'];

                            $tecnico='';
                            if($solicitud['resource_id']!==null)
                                $tecnico=$solicitud['resource_id'];

                            $nomtecnico='';
                            if($solicitud['tecnico_id']!=null) {
                                $qtecnico=Tecnico::find($solicitud['tecnico_id']);
                                $nomtecnico=$qtecnico->nombre_tecnico;
                            }

                            $motivo='';
                            if($solicitud['motivo_ofsc_id']!==null) {
                                $qmotivo=MotivoOfsc::find($solicitud['motivo_ofsc_id']);
                                $motivo=$qmotivo->name_corto;
                            }

                            $ventanallegada='';
                            if($solicitud['delivery_window_start']!==null)
                                $ventanallegada=$solicitud['delivery_window_start'];

                            $respuesta=array(
                                'datos'=>array(
                                    'tipo_operacion'=>'RPTA_CONSULTA',
                                    'id_solicitud_tecnica'=>$solicitud['id_solicitud_tecnica'],
                                    'date'=>date("d/m/Y"),
                                    'time'=>date("H:i:s"),
                                    'fecha_cita'=>$fagenda,
                                    'intervalo_tiempo'=>$intervalo,
                                    'hora_inicio'=>$hinicio,
                                    'hora_fin'=>$hfin,
                                    'estado_toa'=>$estadotoa,
                                    'desc_estadotoa'=>substr($descripciontoa,0,30),
                                    'fecha_registrotoa'=>$fregistro,
                                    'fecha_hora_asignacion'=>$fasignacion,
                                    'contrata'=>substr($contrata,0,3),
                                    'desc_contrata'=>substr($descontrata,0,20),
                                    'tecnico'=>substr($tecnico,0,10),
                                    'nombre_tecnico'=>substr($nomtecnico,0,30),
                                    'motivo'=>substr($motivo, 0, 5),
                                    'ventana_llegada'=>substr($ventanallegada, 0, 30),
                                )
                            );
                            $respuesta=$respuesta['datos'];
                        } else {
                            $error = Error::find(13);
                            $respuesta = array(
                                'code_error'=>$error->code,
                                'desc_error'=>$error->message
                            );
                        }
                    } catch (Exception $e) {
                        $error = Error::find(10);
                        $respuesta = array(
                            'code_error'=>$error->code, //Error de server
                            'desc_error'=>$e->getMessage()
                        );
                    }
                } else {
                    $error = Error::find(3);
                    $respuesta = array(
                                'code_error'=>$error->code,
                                'desc_error'=>$error->message
                            );
                }
            } else {
                $error = Error::find(20);
                $respuesta = array(
                                'code_error'=>$error->code,
                                'desc_error'=>$error->message
                            );
            }

            return $respuesta;
        }
        function retornar($request)
        {
            Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_retorno.log');
            Log::info([$request]);
            $codError='';
            $msjError='';

            $validator = Validator::make((array)$request, SolicitudTecnicaCms::$rules_retorno);
            if ($validator->fails()) {
                $codError = "ERROR001";
                $msjError = $validator->messages()->all()[0];

                RetornoLegado::create([
                    'id_solicitud_tecnica'  => $request->solicitud_tecnica,
                    'request'               => json_encode($request),
                    'error_code'            => $codError,
                    'msj_error'             => $msjError
                ]);
                throw new SoapFault('ERROR001', $validator->messages()->all()[0]);
            }

            $tipoOperacion = $request->tipo_operacion;
            $fechaEnvio = $request->fecha_envio;
            $horaEnvio = $request->hora_envio;
            $codEnvio = $request->cod_envio;
            $descripcion = $request->descripcion;
            $idsolicitud = $request->solicitud_tecnica;
            $tipoOperacionCierre = '';

            $solicitudTecnica = SolicitudTecnica::where('id_solicitud_tecnica', $idsolicitud)
                                ->orderBy("updated_at", "DESC")
                                ->first();
            $ultimo = $solicitudTecnica->ultimo;

            if (!is_null($ultimo)) {
                $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $tipoOperacion)
                                    ->where('tipo_legado', $ultimo->tipo_legado)
                                    ->where('tipo', 1)
                                    ->whereRaw('tipo_operacion like "%'.$solicitudTecnica->tipo_operacion.'%"')
                                    ->first();
                if (is_null($tipoOperacionObj)) {
                    $codError = "ERROR002";
                    $msjError =  "Tipo de Operación no valida";
                    
                    RetornoLegado::create([
                        'id_solicitud_tecnica'  => $request->solicitud_tecnica,
                        'request'               => json_encode($request),
                        'error_code'            => $codError,
                        'msj_error'             => $msjError,
                    ]);

                    throw new SoapFault('ERROR002', "Tipo de Operación no valida");
                }

                $tipoOperacionCierre = $tipoOperacionObj->codigo;
                if($ultimo->tipo_legado == 1) {
                    $solicitud = $solicitudTecnica->cms;
                } else {
                    if($ultimo->actividad_id == 2) {
                        $solicitud = $solicitudTecnica->gestelProvision;
                    } elseif ($ultimo->actividad_id == 1) {
                        $solicitud = $solicitudTecnica->gestelAveria;
                    }
                }
            }

            $componentes=[];
            if (isset($ultimo->liquidate)) {
                if ($ultimo->liquidate == 1) {
                    $codError = "ERROR003";
                    $msjError =  'ST ya se retorno';
                    
                    RetornoLegado::create([
                        'id_solicitud_tecnica'  => $request->solicitud_tecnica,
                        'request'               => json_encode($request),
                        'error_code'            => $codError,
                        'msj_error'             => $msjError,
                    ]);
                    throw new SoapFault('ERROR003', 'ST ya se retorno');

                }
            }
            $componentes = $solicitud->componentes()->whereRaw("deleted_at IS NULL")->get();
            $retornar = true;
            if (isset($ultimo->aid)) {
                $aid = $ultimo->aid;
                $codactu = $ultimo->num_requerimiento;

                switch ($ultimo->estado_ofsc_id) {
                    case 1://Pendiente
                        $inbound = new Inbound();
                        $actividad = array();
                        $actividad['A_OBSERVATION'] = 'Retorno por legado';
                        $response = $inbound->cancelActivity($aid, $codactu, $actividad);
                        $rpta = [];
                        if (isset($response->data->data->commands->command->appointment->report->message)) {
                            $resultCode = $response->data->data->commands->command->appointment->report->message;
                            if (isset($resultCode->result) && $resultCode->result=='error') {
                                $rpta["code"] = 2;
                                $rpta['description']=$resultCode->description;
                            } else {
                                for ($y = 0; $y < count($resultCode); $y++) {
                                    if (is_object($resultCode[$y])) {
                                        if ($resultCode[$y]->type=='cancel' && $resultCode[$y]->result=='success') {
                                            $rpta["result"] = $resultCode[$y]->result;
                                            $rpta["code"] = $resultCode[$y]->code;
                                        }
                                    }
                                }
                            }
                        }
                        if ($request->solicitud_tecnica == "C0000295782") {
                            dd($response);
                        }

                        $response = new stdClass();
                        $response->result = isset($rpta['result']) ? $rpta['result'] : '';
                        $response->code = isset($rpta['code']) ? $rpta['code'] : '';
                        $response->description = isset($rpta['description']) ? $rpta['description'] : '';
                        break;
                    case 2://Iniciada
                        //solo si es superusuario de legado, si es usuario se genera un NO RETORNO
                        if ($tipoOperacion == 'RET_PRO_SUP' || $tipoOperacion == 'RET_AVE_SUP' ||
                            $tipoOperacion == 'RET_AVE_MAS') {
                            $inbound = new Inbound;
                            $actividad = [];
                            $actividad['A_OBSERVATION'] = 'Retorno por legado';
                            $response = $inbound->updateActivity($codactu, $actividad, [], "notdone_activity");
                        } else {
                            $tipoOperacionObj = TipoOperacion::where('equivalencia_retorno', $tipoOperacion)
                                                ->where('tipo_legado', $ultimo->tipo_legado)
                                                ->where('tipo', 2)
                                                ->whereRaw('tipo_operacion like "%'.$ultimo->tipo_operacion.'%"')
                                                ->first();
                            $tipoOperacionCierre = $tipoOperacionObj->codigo;
                        }
                        break;
                    case 3://Suspendida
                        $inbound = new Inbound();
                        $actividad = array();
                        $actividad['A_OBSERVATION'] = 'Retorno por legado';
                        $response = $inbound->cancelActivity($aid, $codactu, $actividad);
                        break;
                    case 4://No Realizada
                        $inbound = new Inbound();
                        $actividad = array();
                        $actividad['A_OBSERVATION'] = 'Retorno por legado';
                        $response = $inbound->updateActivity($codactu, $actividad, []);
                        break;
                    case 5://Cancelada
                        //informar a legado que la orden ya fue cancelada en toa
                        $retornar = false;
                        break;
                    case 6://Completada
                        //informar a legado que la orden ya fue completda en toa
                        $retornar = false;
                        break;
                    case 7://Desprogramado
                        $inbound = new Inbound();
                        $actividad = array();
                        $actividad['A_OBSERVATION'] = 'Retorno por legado';
                        $response = $inbound->cancelActivity($aid, $codactu, $actividad);
                        break;
                    case 8://No Enviado
                        //cancelar la orden en web psi
                        $retornar = true;
                        break;
                    default:
                        $retornar = false;
                        break;
                }
            } else {
                $retornar = true;
                $description = 'sin aid (PSI-TOA)';
                $code = '';

                $actuacion = [
                    "estado_aseguramiento" => 7,
                    "xa_observation" => "Retorno por legado | ".$description
                ];

                Auth::loginUsingId(697);
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimo[$key] = $actuacion[$key];
                    }
                }
                $solicitudTecnica->ultimo()->save($ultimo);

                $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                foreach (Movimiento::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimoMovimiento[$key] = $actuacion[$key];
                    }
                }
                $ultimoMovimiento->save();
                Auth::logout();

                if ($ultimo->estado_ofsc_id != 8) {
                    RetornoLegado::create([
                        'id_solicitud_tecnica'  => $request->solicitud_tecnica,
                        'request'               => json_encode($request),
                        'error_code'            => $code,
                        'msj_error'             => $description,
                    ]);
                }
            }
            $responseToa = false;
            if (isset($actividad) && isset($response->result) && $response->result == "success"
                && $response->code == 0) {
                Auth::loginUsingId(697);
                $actuacion = [
                    "estado_aseguramiento" => 7,
                    "A_OBSERVATION" => "Retorno por legado"
                ];
                foreach (Ultimo::$rules as $key => $value) {
                    if (array_key_exists($key, $actuacion)) {
                        $ultimo[$key] = $actuacion[$key];
                    }
                }
                $solicitudTecnica->ultimo()->save($ultimo);
                Auth::logout();
            } else {
                if (isset($response)) {
                    $description = isset($response->description) ? $response->description : 'error desconocido en TOA';
                    $code = isset($response->code) ? $response->code : '';
                    RetornoLegado::create([
                        'id_solicitud_tecnica'  => $request->solicitud_tecnica,
                        'request'               => json_encode($request),
                        'error_code'            => $code,
                        'msj_error'             => $description,
                    ]);
                }
            }
            if ($retornar==true) {
                // ************************** Gestel retorno ****************************
                if ($ultimo->tipo_legado == 2) {
                    if ($ultimo->actividad_id == 2) {
                        $elementos = [
                            'tipo_operacion'            => $tipoOperacionCierre,
                            'solicitud_tecnica_id'      => $idsolicitud,
                            'num_solicitud_tecnica'     => $idsolicitud,
                            'fecha_programacion'        => '',
                            'contrata'                  => '',
                            'tecnico1'                  => '',
                            'tecnico2'                  => '',
                            'fecha_emision_boleta'      => '',
                            'nombre_persona_recibe'     => '',
                            'numero_doc_identidad'      => '',
                            'observacion'               => '',
                            'parentesco'                => '',
                            'indicador_fraude'          => '',
                            'anexos'                    => '',
                            'fecha_ejecucion'           => '',
                            'hora_inicio'               => '',
                            'hora_fin'                  => '',
                            'desplazamiento'            => '',
                            'escenario'                 => '',
                            'cabina'                    => '',
                            'ambiente'                  => '',
                            'cod_mot'                   => '',
                            'des_obs'                   => '',
                            'fecha_hora_devolucion'     => '',
                            'dato1'                     => '',
                            'dato2'                     => '',
                            'dato3'                     => '',
                            'dato4'                     => '',
                            'dato5'                     => '',
                            'DatosMateriales'           => ''
                        ];

                        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
                        $objStapi = new STGestelApi("GESTEL Legado");
                        $response = $objStapi->cierreProvision($elementos);
                    } elseif ($ultimo->actividad_id == 1) {
                        $elementos = [
                            'tipo_operacion'            => $tipoOperacionCierre,
                            'solicitud_tecnica_id'      => $idsolicitud,
                            'numero_solicitud_tecnica'  => $idsolicitud,
                            'fec_hora_programacion'     => '',
                            'contrata'                  => '',
                            'tecnico'                   => '',
                            'tecnico2'                  => '',
                            'tecnico3'                  => '',
                            'ncompro'                   => '',
                            'fecha_emision'             => '',
                            'codigo_franqueo'           => '',
                            't_liquid'                  => '',
                            'hora_inicio'               => '',
                            'hora_fin'                  => '',
                            'codigo_liquidacion'        => '',
                            'codigo_destino'            => '',
                            'codigo_version'            => '',
                            'codigo_detalle'            => '',
                            'fecha_sal'                 => '',
                            'fecha_destino'             => '',
                            'nomcon_liq'                => '',
                            'telecon_liq'               => '',
                            'sintoma'                   => '',
                            'obs_ultima_liq'            => '',
                            'dato1'                     => '',
                            'dato2'                     => '',
                            'dato3'                     => '',
                            'dato4'                     => '',
                            'dato5'                     => '',
                            'datosEquipo'               => '',
                            'reservado'                 => ''
                        ];

                        \Config::set("legado.wsdl.respuestast_gestel", \Config::get("legado.wsdl.respuestast_gestel_desarrollo"));
                        $objStapi = new STGestelApi("GESTEL Legado");
                        $response = $objStapi->cierreAveria($elementos);
                    }
                    return;
                }
                // ************************ Cms retorno *********************************
                $componentesArray = [];
                foreach ($componentes as $key => $value) {
                    $componentesArray[] = [
                        'numero_requerimiento' => $value->numero_requerimiento,
                        'numero_ot'            => $value->numero_ot,
                        'numero_servicio'      => $value->numero_servicio,
                        'componente_cod'       => $value->componente_cod,
                        'marca'                => $value->marca,
                        'modelo'               => $value->modelo,
                        'tipo_equipo'          => $value->componente_tipo,
                        'numero_serie'         => $value->numero_serie,
                        'casid'                => $value->casid,
                        'cadena1'              => $value->valor1,
                        'cadena2'              => $value->valor2,
                        'cadena3'              => $value->valor3,
                        'cadena4'              => $value->valor4,
                        'cadena5'              => $value->valor5
                    ];
                }

                $datetime_ini[0] = $datetime_ini[1] = null;
                if (!is_null($ultimo->start_time)) {
                    $datetime_ini = explode(" ", $ultimo->start_time);
                }
                

                $elementos = [
                    "solicitud_tecnica_id"          => $ultimo->solicitud_tecnica_id,
                    "tipo_operacion"                => $tipoOperacionCierre,
                    "id_solicitud_tecnica"          => $solicitud->id_solicitud_tecnica,
                    "fecha_envio"                   => Helpers::formatoFecha($ultimo->fecha_envio),
                    "hora_envio"                    => $ultimo->hora_envio,
                    "codigo_liquidacion"            => $ultimo->actividad_id == 1 ? 'LT05' : 'LT01',
                    "codigo_tecnico1"               => '',
                    "codigo_tecnico2"               => '',
                    "fecha_inicio"                  => Helpers::formatoFecha($datetime_ini[0]),
                    "hora_inicio"                   => $datetime_ini[1],
                    "fecha_fin"                     => Helpers::formatoFecha($datetime_ini[0]),
                    "hora_fin"                      => $ultimo->end_time,
                    "fecha_liquida"                 => date('d/m/Y'),
                    "hora_liquida"                  => date('H:i:s'),
                    "nombre_contacto"               => $solicitud->nom_cliente,
                    "parentesco_contacto"           => '',
                    "dni_contacto"                  => '',
                    "codigo_contrata"               => $solicitud->codigo_contrata,
                    "observacion"                   => $ultimo->xa_note,
                    "campo1"                        => $solicitud->valor1,
                    "campo2"                        => $solicitud->valor2,
                    "campo3"                        => $solicitud->valor3,
                    "campo4"                        => $solicitud->valor4,
                    "campo5"                        => $solicitud->valor5,
                    "componentes"                   => $componentesArray
                ];

                $solicitusTecnica = new STCmsApi();
                $response = $solicitusTecnica->cierre($elementos);
            }
        }
        try {
            $server = new SoapServer(Config::get('legado.solicitudtecnica.wsdl'));
            $server->addFunction("generar");
            $server->addFunction("consultar");
            $server->addFunction("retornar");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}
