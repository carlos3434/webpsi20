<?php
class ObservacionTipoController extends \BaseController
{
    /**
     * Recepciona datos de Bandeja Controller
     * 
     * @return type
     */
    public function postListar()
    {
        // $m=array();
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $m = Cache::get('listarObservacionTipoBandeja');
            if ( $m ) {
                $m = Cache::get('listarObservacionTipoBandeja');
            } else {
                $m = Cache::remember('listarObservacionTipoBandeja', 20, function(){
                    $datos = DB::table('observaciones_tipos')
                        ->select('id', 'nombre')
                        ->where('estado', '=', '1')
                        ->get();

                    return $datos;
                });
            }
        } else {
            $m=  DB::table('observaciones_tipos')
                ->select('id', 'nombre')
                ->where('estado', '=', '1')
                ->get();
        }

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $m
                )
            );
        }

    }

}

