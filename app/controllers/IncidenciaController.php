<?php

class IncidenciaController extends \BaseController
{

    public function postCreate()
    {
        if (Request::ajax()) {
            $inputAll = Input::all();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                    $inputAll[$key] = $val;
            }
            Input::merge($inputAll);

            $data = [];
            $data['actividad_id']=Input::get('tipotrabajo');
            $data['tipo']=Input::get('tipo');
            $data['sistema_id']=Input::get('tecnologia');
            $data['empresa_id']=Input::get('empresa');
            $data['tipo_legado']=Input::get('tipo_legado');
            $data['tipificacion']=Input::get('tipificacion');
            $data['num_requerimiento']=Input::get('txt_num_requerimiento');
            $data['nivel_impacto']=Input::get('nivel_impacto');
            $data['prioridad']=Input::get('prioridad');
            $data['estado_validacion']=4;
            $data['detalle'] = Input::get('detalle');
            $data['observacion']=Input::get('obervacion');
            $data['estado']=4;
            $rst = Incidencia::create($data);

            if($rst) {
                $incidencia_mov = [];
                $incidencia_mov['incidencia_id'] =$rst->id;
                $incidencia_mov['observacion'] = $rst->observacion;
                $incidencia_mov['estado_validacion'] = $rst->estado_validacion;
                $incidencia_mov['estado'] = $rst->estado;
                $incidencia_mov['nivel_impacto'] = $rst->nivel_impacto;
                $incidencia_mov['asociado_id'] = $rst->id;
                $incidencia_mov['sistema_id'] = $rst->sistema_id;
                IncidenciaMov::create($incidencia_mov);
            }

            if($rst && $_FILES){
                foreach ($_FILES as $key => $value) {
                    if($value['name'] && strpos($key, 'txt_filep') !== false){
                        $file = Input::file($key);
                        $tmpArchivo = $file->getRealPath();
                        $original_name = $rst['id']."&".Helpers::generateRandomString()."&".$file->getClientOriginalName();
                        $path = 'incidencia/';
                        $file->move($path, $original_name);

                        $img = [
                            'incidencia_id'=>$rst['id'],
                            'image'=> $original_name,
                            'estado'=>1
                        ];
                        IncidenciaImg::create($img);
                    }
                }
            }

            if($rst && Input::get('req')){
                $img = $_FILES;
                $reqs = json_decode(Input::get('req'), true);
                $count = count($reqs);
                for($i=1 ; $i <= $count ; $i++){
                    $data = [];

                    if($img["txtfile$i"]['name'] != ''){
                        $file = Input::file("txtfile$i");
                          $tmpArchivo = $file->getRealPath();
                        $original_name = $rst['id']."&".Helpers::generateRandomString()."&".$file->getClientOriginalName();
                        $path = 'incidencia/sub';
                        $file->move($path, $original_name);
                        $data['image'] = $original_name;
                    }

                    $data['tipo_valor']=$reqs[$i-1]['tipo_valor'];
                    $data['incidencia_id']=$rst['id'];
                    $data['valor']=$reqs[$i-1]['valor'];
                    $data['descripcion']=$reqs[$i-1]['descripcion'];
                    $data['estado']=1;    
                    IncidenciaReq::create($data);
                }
            }

            return Response::json(
                array(
                    'rst' => ($rst) ? 1 : 0,
                      'msj' => 'Registrado Correctamente',
                )
            );
        }
    }

    public function postEdit()
    {

        if (Request::ajax()) {
            $inputAll = Input::all();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                    $inputAll[$key] = $val;
            }
            Input::merge($inputAll);
            $data = Incidencia::findOrFail(Input::get('id_modal2'));
            $data->actividad_id=Input::get('tipotrabajo_modal');
            $data->tipo=Input::get('tipo_modal');
            $data->tipificacion=Input::get('tipificacion_modal');
            $data->sistema_id = Input::get('tecnologia_modal');
            $data->nivel_impacto=Input::get('nivel_impacto_modal');
            $data->prioridad=Input::get('prioridad_modal');
            $data->asociado_id=Input::get('ticket_asociado');
            $data->estado_validacion=Input::get('estado_validacion_modal');

            if(Input::get('usuarioresp')){
                $data->usuario_responsable_id=Input::get('usuarioresp');
            }

            if(Input::get('fecha_entrega') !== '0000-00-00 00:00:00'){
                $data->fecha_entrega=Input::get('fecha_entrega');
            }

            $data->observacion=Input::get('obervacion_modal');
            $data->estado=Input::get('eactual_modal');
            $data->cod_remedi=Input::get('remedi');
            $data->num_requerimiento = Input::get('num_requerimiento_modal');
            $data->detalle = Input::get('detalle_modal');
            $data->save();


            if($data && $_FILES){
                foreach ($_FILES as $key => $value) {
                    if($value['name']){
                          $file = Input::file($key);
                          $tmpArchivo = $file->getRealPath();
                        $original_name = $data->id."&".Helpers::generateRandomString()."&".$file->getClientOriginalName();
                        $path = 'incidencia/';
                        $file->move($path, $original_name);

                        $img = [
                            'incidencia_id'=>$data->id,
                            'image'=> $original_name,
                            'estado'=>1
                        ];
                        IncidenciaImg::create($img);
                    }
                }
            }

            if($data){
                $incidencia_mov = [];
                $incidencia_mov['incidencia_id'] =$data->id;
                $incidencia_mov['observacion'] = $data->observacion;
                $incidencia_mov['estado_validacion'] = $data->estado_validacion;
                $incidencia_mov['estado'] = $data->estado;
                $incidencia_mov['asociado_id'] = $data->asociado_id;
                $incidencia_mov['fecha_entrega'] = $data->fecha_entrega;
                $incidencia_mov['usuario_responsable_id'] = $data->usuario_responsable_id;
                $incidencia_mov['nivel_impacto'] = $data->nivel_impacto;
                $incidencia_mov['sistema_id'] = $data->sistema_id;
                IncidenciaMov::create($incidencia_mov);
            }

            return Response::json(
                array(
                    'rst' => ($data) ? 1 : 0,
                      'msj' => 'Actualizado Correctamente',
                )
            );
        }
    }

    public function postDelete()
    {
        if (Request::ajax()) {
            $data = [];
            if(Auth::id() == 1017 or Auth::id() == 7 or Auth::id()==11 or Auth::id()==467){
                $data = Incidencia::findOrFail(Input::get('incidencia_id'));
                $data->flag_eliminado=1;
                $data->save();
                $msj = "Eliminado exitosamente";
            } else {
                $msj = "No cuenta con el permiso";
            }
            
            return Response::json(
                array(
                    'rst' => ($data) ? 1 : 0,
                      'msj' => $msj,
                )
            );
        }
    }

    public function postListar()
    {
        if(Input::get('accion')){
            $inputAll = Input::all();
            foreach ($inputAll as $key => $val) {
                $key = str_replace(array("slct_", "txt_"), array("", ""), $key);
                $inputAll[$key] = $val;
            }
            Input::merge($inputAll);
        }
        $query = Incidencia::from('incidencia as i')
                ->select(
                    'i.id',
                    DB::raw("LPAD(i.id,5,0) as nombre"),
                    'i.num_requerimiento',
                    DB::raw('CASE i.actividad_id WHEN 1 THEN "Averia" WHEN 2 THEN "Provision" END AS actividad'),
                    DB::raw('CASE i.sistema_id WHEN 1 THEN "PSI" WHEN 2 THEN "PSI-TOA" WHEN 3 THEN "CMS" WHEN 4 THEN "GESTEL" WHEN 5 THEN "TOA MOVIL" WHEN 6 THEN "TOA MANAGE" WHEN 7 THEN "CMS-PSI" WHEN 8 THEN "CMS-PSI-TOA" END AS sistema'),
                    'e.nombre as empresa', 
                    DB::raw('CASE i.tipo_legado WHEN 1 THEN "CMS" WHEN 2 THEN "GESTEL" END AS tipo_legado'),
                    DB::raw('CASE i.nivel_impacto WHEN 1 THEN "ALTO" WHEN 2 THEN "MEDIO" WHEN 3 THEN "BAJO" END AS impacto'),
                    DB::raw('CASE i.prioridad WHEN 1 THEN "ALTO" WHEN 2 THEN "MEDIO" WHEN 3 THEN "BAJO" END AS prioridad'),
                    DB::raw('CASE i.tipo WHEN 1 THEN "REQUERIMIENTO" WHEN 2 THEN "INCIDENCIA" WHEN 3 THEN "OM" WHEN 4 THEN "PROYECTO" END AS tipo'),
                    'i.observacion',
                    'i.detalle',
                    'i.estado_validacion',
                    'i.estado',
                    'i.created_at as fecha_registro',
                    'i.asociado_id',
                    'i.fecha_entrega',
                    DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) as usuario_responsable'),
                    DB::raw('CONCAT_WS(" ",u1.nombre,u1.apellido) as usuario_created'),
                    DB::raw('CASE i.estado_validacion WHEN 1 THEN "PENDIENTE" WHEN 2 THEN "POR VALIDAR" WHEN 3 THEN "TERMINADO" WHEN 4 THEN "REGISTRADO" WHEN 7 THEN "SUSPENDIDO" END AS nombre_estado_validacion'),
                    DB::raw('CASE i.estado WHEN 1 THEN "PENDIENTE" WHEN 2 THEN "POR VALIDAR" WHEN 3 THEN "TERMINADO" WHEN 4 THEN "REGISTRADO" WHEN 5 THEN "EN DESARROLLO" END AS nombre_estado'),
                    'i.cod_remedi'
                )
                ->leftjoin('empresas as e', 'i.empresa_id', '=', 'e.id')
                ->leftjoin('usuarios as u', 'i.usuario_responsable_id', '=', 'u.id')
                ->leftjoin('usuarios as u1', 'i.usuario_created_at', '=', 'u1.id')
                ->where(function($query)
            {
                if(Input::get('tipo_busqueda') == 1){
                    if(Input::get('valor')){
                        $query->where('i.id', Input::get('valor'));
                    }
                } else if(Input::get('tipo_busqueda')==2){
                    if(Input::get('tipotrabajo_buscar')){
                        $query->whereIn('i.actividad_id', Input::get('tipotrabajo_buscar'));
                    }
                    if(Input::get('tecnologia_buscar')){
                        $query->whereIn('i.sistema_id', Input::get('tecnologia_buscar'));
                    }
                    if(Input::get('empresa_buscar')){
                        $query->whereIn('i.empresa_id', Input::get('empresa_buscar'));
                    }
                    if(Input::get('tipo_legado_buscar')){
                        $query->whereIn('i.tipo_legado', Input::get('tipo_legado_buscar'));
                    }
                    if(Input::get('nivel_impacto_buscar')){
                        $query->whereIn('i.nivel_impacto', Input::get('nivel_impacto_buscar'));
                    }
                    if(Input::get('estado_validacion_buscar')){
                        $query->whereIn('i.estado_validacion', Input::get('estado_validacion_buscar'));
                    }
                    if(Input::get('prioridad_buscar')){
                        $query->whereIn('i.prioridad', Input::get('prioridad_buscar'));
                    }
                    if(Input::get('eactual_buscar')){
                        $query->whereIn('i.estado', Input::get('eactual_buscar'));
                    }
                    if(Input::get('tipo_buscar')){
                        $query->whereIn('i.tipo', Input::get('tipo_buscar'));
                    }
                    if(Input::get('tipificacion_buscar')){
                        $query->whereIn('i.tipificacion', Input::get('tipificacion_buscar'));
                    }
                    if(Input::get('usuarioresp_buscar')){
                        $query->whereIn('i.usuario_responsable_id', Input::get('usuarioresp_buscar'));
                    }
                    if(Input::get('fecha_registro')){
                        $fechaRegistro = Input::get('fecha_registro');
                        if (count($fechaRegistro) != 0) {
                            $fecha = explode(" - ", $fechaRegistro);
                            if (count($fecha) > 0) {
                                $startDate = date('Y-m-d  00:00:00', strtotime($fecha[0]));
                                $endDate = date('Y-m-d  23:59:59', strtotime($fecha[1]));
                                $query->whereBetween('i.created_at',[ $fecha[0]. " 00:00:00",$fecha[1]. " 23:59:59" ] );
                            }
                        }
                    }
                }
                $query->where('flag_eliminado', 0);
            }
        );

        if(Input::get('accion')){
            $data = json_decode(json_encode($query->get()), true);
            return Helpers::exportArrayToCsv($data, 'Incidencias');
        }
        if (Input::has('column')) {
            return $query->orderBy(Input::get('column'), Input::get('dir'))->searchPaginateAndOrder();
        } else {
            return $query->searchPaginateAndOrder();
        }
    }

    public function postCambiarestado()
    {
        if (Request::ajax()) {
            $update = Incidencia::findOrFail(Input::get('incidencia_id'));

            if(Input::has('estado_actual')){
                $update->estado = Input::get('estado_actual');
            }
            if(Input::has('estado_validacion')){
                $update->estado_validacion = Input::get('estado_validacion');
            }
            $update->usuario_updated_at = Auth::id();
            $update->updated_at = date('Y-m-d H:i:s');
            $update->save();
            return Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );
        }
    }

    public function postGetbyid()
    {
        $objClass = new Incidencia();
        return Response::json(
            array(
                'rst'    => 1,
                'datos'   => $objClass->getIncidenciaById()
            )
        );
    }

    public function postGetmovimientos()
    {
        $query =  Incidencia::from('incidencia_mov as im')
                ->leftjoin('usuarios as u', 'im.usuario_created_at', '=', 'u.id')
                ->leftjoin('usuarios as u1', 'im.usuario_responsable_id', '=', 'u1.id')
                  ->select(
                    'im.id',
                    DB::raw('CASE im.estado_validacion WHEN 1 THEN "PENDIENTE" WHEN 2 THEN "POR VALIDAR" WHEN 3 THEN "TERMINADO" WHEN 4 THEN "REGISTRADO" WHEN 7 THEN "SUSPENDIDO" END AS estado_validacion'),
                    DB::raw('CASE im.estado WHEN 1 THEN "PENDIENTE" WHEN 2 THEN "POR VALIDAR" WHEN 3 THEN "TERMINADO" WHEN 4 THEN "REGISTRADO" WHEN 5 THEN "EN DESARROLLO" END AS estado'),
                    DB::raw('CASE im.nivel_impacto WHEN 1 THEN "ALTO" WHEN 2 THEN "MEDIO" WHEN 3 THEN "BAJO" END AS impacto'),
                    'im.fecha_entrega',
                    DB::raw('CONCAT_WS(" ",u1.nombre,u1.apellido) as usuario_responsable'),
                    'im.observacion',
                    DB::raw('CONCAT_WS(" ",u.nombre,u.apellido) as usuario_created_at'),
                    'im.created_at',
                    'im.asociado_id',
                    DB::raw('CASE im.sistema_id WHEN 1 THEN "PSI" WHEN 2 THEN "PSI-TOA" WHEN 3 THEN "CMS" WHEN 4 THEN "GESTEL" WHEN 5 THEN "TOA MOVIL" WHEN 6 THEN "TOA MANAGE" WHEN 7 THEN "CMS-PSI" WHEN 8 THEN "CMS-PSI-TOA" END AS sistema_id')
                )
                ->where( function($query){
                    if(Input::get('incidencia_id')){
                        $query->where('im.incidencia_id', Input::get('incidencia_id'));
                    }
                }
                )->searchPaginateAndOrder();
        return $query;
    }

    public function postVisor()
    {
        $objClass = new Incidencia();
        $rst = $objClass->visor();
        return Response::json(
            array(
                "rst" => 1,
                'data' => $rst['data'],
                'count' => $rst['count']
            )
        );
    }

    public function postResponsables()
    {
        $objClass = new Incidencia();
        $rst = $objClass->responsables();
        return Response::json(
            array(
                "rst" => 1,
                'sistema' => $rst['sistema'],
                'actividad' => $rst['actividad'],
                'data' => $rst['data']
            )
        );
    }
}