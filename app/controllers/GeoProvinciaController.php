<?php
class GeoProvinciaController extends \BaseController
{
    public function postListar()
    {
       
        $r =GeoProvincia::postDepartamentoFiltro();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postProvincia()
    {
        $array['departamento']=Input::get('departamento');
        $r =GeoProvincia::postProvinciaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postProvinciacoord()
    {
        $array['departamento']=Input::get('departamento');
        $array['provincia']=Input::get('provincia');
        $r =GeoProvincia::postProvinciaCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r,
                    'seleccionado' => $array['provincia']
                )
            );
        }
    }

    public function postListarcoord()
    {
        $array=explode("-", Input::get('subelemento'));

        $r =GeoProvincia::postCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        } 
    }

}

