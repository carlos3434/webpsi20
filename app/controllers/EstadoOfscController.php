<?php

class EstadoOfscController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if ( Request::ajax() ) {
            $r = EstadoOfsc::Listar();
            return Response::json(array('rst'=>1,'datos'=>$r));
        }
    }
}
