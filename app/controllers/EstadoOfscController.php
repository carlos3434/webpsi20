<?php

class EstadoOfscController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $r = EstadoOfsc::Listar();
            return Response::json(array('rst'=>1,'datos'=>$r));
        }
    }

    public function postListarbyname()
    {
        $estados = Cache::get("listEstadosOfscByName");
        if (!$estados) {
            $estados = Cache::remember('listEstadosOfscByName', 24*60*60, function () {
                $data = EstadoOfsc::where("estado", 1)->get();
                $estadostmp = [];
                foreach ($data as $key => $value) {
                    $estadostmp[$value->name] = $value;
                }
                return $estadostmp;
            });
        }
        return Response::json(["rst" => 1, "datos" => $estados]);
    }
}
