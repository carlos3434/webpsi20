<?php
class DiaController extends \BaseController
{
    public function postListar()
    {
        $datos = Dia::all();
        return [
            'rst' => 1,
            'datos' => $datos
        ];
    }
}