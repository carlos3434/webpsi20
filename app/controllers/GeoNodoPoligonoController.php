<?php
class GeoNodoPoligonoController extends \BaseController
{
     public function postListar()
     {

        if (Input::get('modulo') && Input::get('modulo')=='geoplan') {
            $zonal = Input::get('zonal', 'LIM');
            $r =GeoNodoPoligono::getNodozonal($zonal);
        } else {
            $r =GeoNodoPoligono::postGeoNodoPoligonoAll(); 
        }

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
     }

     public function postZonal()
     {
        $array['zonal']=Input::get('zonal');
        $r =GeoNodoPoligono::postNodoPoligonoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
     }

}

