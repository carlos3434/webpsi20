<?php

class JefaturaController extends \BaseController
{

    /**
     * Recepciona datos de Bandeja Controller
     *
     * @return array
     */
    public function postListar()
    {

        if (Request::ajax()) {
            if (Input::get('parametros')) {
                $z = DB::table('coc.zonal')
                                ->select(
                                    DB::RAW('distinct(jefatura) as id'),
                                    'jefatura as nombre'
                                    )
                                ->get();
                }
            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $z
                )
            );
        }
    }

}
