<?php

class CasuisticaProyectoEstadoController extends \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
            $datos = DB::table('casuistica_proyecto_estado as cpe')
                ->join(
                    'proyectos_estados as pe', 'pe.id', '=', 
                    'cpe.proyecto_estado_id'
                )
                ->join('casuisticas as c', 'c.id', '=', 'cpe.casuistica_id')
                ->select(
                    'pe.nombre as proyecto_estado', 'c.nombre as casuistica', 
                    'cpe.estado', 'cpe.id'
                )
                ->orderBy('pe.nombre')
                ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postCrear()
    {
        if (Request::ajax()) {
            DB::table('casuistica_proyecto_estado')
            ->insert(
                array('proyecto_estado_id' => Input::get('proyecto_estado_id'),
                'casuistica_id' => Input::get('casuistica_id'),
                'estado' => Input::get('estado'),
                'created_at' => date("Y-m-d H:m:s"),
                'usuario_created_at' => Auth::user()->id)
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente'
                )
            );
        }
    }

    public function postBuscar()
    {
        if (Request::ajax()) {
            $datos = DB::table('casuistica_proyecto_estado')
                ->select(
                    'id', 'proyecto_estado_id', 'casuistica_id', 'estado'
                )
                ->where('id', '=', Input::get('id'))
                ->get();

            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {
            DB::table('casuistica_proyecto_estado')
            ->where('id', Input::get('id'))
            ->update(
                array(
                    'proyecto_estado_id' => Input::get('proyecto_estado_id'),
                    'casuistica_id' => Input::get('casuistica_id'),
                    'estado' => Input::get('estado'),
                    'updated_at' => date("Y-m-d H:m:s"),
                    'usuario_updated_at' => Auth::user()->id
                )
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Actualizacion realizado correctamente'
                )
            );
        }
    }

    public function postEstado()
    {
        if (Request::ajax()) {
            
            DB::table('casuistica_proyecto_estado')
            ->where('id', Input::get('id'))
            ->update(
                array('estado' => Input::get('estado'))
            );

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Estado actualizado correctamente'
                )
            );
        }
    }
}