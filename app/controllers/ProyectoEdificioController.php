<?php

use Repositories\ProyectoEdificioRepository;
use Repositories\GestionEdificioRepository;
use yajra\Datatables\DatatablesServiceProvider;
use yajra\Datatables\Datatables;
class ProyectoEdificioController extends \BaseController
{

    protected $_proyectoEdificioRepo;
    protected $_gestionEdificioRepo;
    protected $_errorController;

    public function __construct(
        ProyectoEdificioRepository $proyectoEdificioRepo, 
        GestionEdificioRepository $gestionEdificioRepo,
        ErrorController $errorController
    )
    {
        $this->_proyectoEdificioRepo = $proyectoEdificioRepo;
        $this->_gestionEdificioRepo = $gestionEdificioRepo;
        $this->_errorController = $errorController;

        $this->beforeFilter('auth');
    }

    /**
     * 
     * @return type
     * @deprecated
     */
    public function getListar()
    {
        $start = Input::get('start');
        $length = Input::get('length');
        $columns = Input::get('columns');
//        $fechas = (Input::has('fecha_created_at')) 
//            ? Input::get('fecha_created_at') : '';
        $orderColumn = array();
        $orderDir = array();

        foreach (Input::get('order') as $order) {
            $orderColumn = $columns[$order['column']]['data'];
            $orderDir = $order['dir'];
        }

        $datos = Input::all();
        $proyectoEdificios = $this->_proyectoEdificioRepo->getRows(
            $datos, 
            $orderColumn, 
            $orderDir, 
            $start, 
            $length
        );

        $extra['recordsTotal'] = $proyectoEdificios['total'];
        $extra['recordsFiltered'] = $proyectoEdificios['total'];
        $extra['draw'] = Input::get('draw');

        $data = $proyectoEdificios['datos']->toArray();

        $output = array();
        $modal = 'proyectoEdificioModal';
        foreach ($data as $k => $v) {
            $statusClass = ($v['deleted_at']) ? 'danger' : 'success';

            $r = array();
            $r = $v;

            $r['accion'] = Helpers::getAccionesHtml($modal, $v['id']);
            $r['estado'] = Helpers::getEstadoHtml($statusClass);

            array_push($output, $r);
        }

        return Helpers::getResponseJson($output, '', $extra);
    }

    public function postAgregarBitacoraGestion()
    {
       if (Request::ajax()) {
           $proyectoEdificioId = (Input::has('proyecto_edificio_id')) 
            ? Input::get('proyecto_edificio_id') : '';
            $input = Input::all();
            $input['usuario_created_at'] = Auth::id();
            //print_r($input);
          $rMsg = trans('main.success_register');
          $rst = 0;
          $rDatos = '';
        if ($proyectoEdificioId && isset($input['proyecto_tipo_id'])) {
            $observaciones = "";
            $observaciones.="ACTUALIZADO| ";
            $observaciones.="Tipo Proyecto : "
                .ProyectoTipo::find($input['proyecto_tipo_id'])->nombre."| ";
            if (isset($input['fecha_inicio']) 
                && $input['fecha_inicio'] != '') {
                $observaciones.="F. Inicio : ".
                date('d/m/Y', strtotime($input['fecha_inicio']))."| ";   
            }
            if (isset($input['fecha_termino'] ) 
                && $input['fecha_termino'] != '') {
                $observaciones.="F. Termino : ".
                date('d/m/Y', strtotime($input['fecha_termino']))."| ";
            }
            if (isset($input['casuistica_id']) 
                && $input['casuistica_id'] != '') {
                $observaciones.="Causística : ".
                Casuistica::find($input['casuistica_id'])->nombre."| ";    
            }

            try{
                $peb =new ProyectoEdificioBitacora();
                $attr = array();
                $attr['proyecto_edificio_id'] = $proyectoEdificioId;
                $attr['descripcion'] = trim($observaciones, '| ');
                $attr['tipo_registro'] = 3;
                $attr['tab_registro'] = 2;
                $attr['fecha_observacion'] = Carbon\Carbon::now();
                $attr['usuario_created_at'] = Auth::id();
                if ($peb->validate($attr)) {
                    $attr['created_at'] = Carbon\Carbon::now();
                    $attr['updated_at'] = Carbon\Carbon::now();
                    $rDatos =  DB::table('proyecto_edificios_bitacora')
                        ->insert($attr);
                    $rMsg = trans('main.success_register');
                    $rst = 1;
                } else {
                    $rMsg = $peb->errors();
                }
            }catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $rMsg = Lang::get('main.error_select_db');
            }
            return Response::json(
                array(
                    'rst'=>$rst
                    ,'datos'=> $proyectoEdificioId
                    ,'msg' => $rMsg
                )
            );
        } else {
            return array(
                'rst'=>0,
                'datos'=>'',
                'msg' => Lang::get('main.no_parametro')
            );
        }
        
       } 
    }
    
    public function postAgregarBitacoraBusqueda()
    {
        if (Request::ajax()) {
            $input = Input::all();
            $rMsg = trans('main.success_register');
            $rst = 0;
            $rDatos = '';
            $observaciones = "";
            $observaciones.="BUSQUEDA| ";
            foreach ($input as $key => $value) {
                if ($value['valor']!="") 
                    $observaciones.=$value['titulo']." : "
                        . "".$value['valor']."| ";
            }
            try{
                $peb =new ProyectoEdificioBitacora();
                $attr = array();
                $attr['descripcion'] = $observaciones;
                $attr['usuario_created_at'] = Auth::id();
                $attr['fecha_observacion'] = Carbon\Carbon::now();
                $attr['tipo_registro'] = 5;
                $attr['tab_registro'] = 0;
                $attr['proyecto_edificio_id'] = 0;
                
                if ($peb->validate($attr)) {
                    $attr['created_at'] = Carbon\Carbon::now();
                    $attr['updated_at'] = Carbon\Carbon::now();
                    $rDatos =  DB::table('proyecto_edificios_bitacora')
                        ->insert($attr);
                    $rMsg = trans('main.success_register');
                    $rst = 1;
                } else {
                    $rMsg = $peb->errors();
                }
            }catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $rMsg = Lang::get("main.error_select_db");
            }
            return Response::json(
                array(
                    'rst'=>$rst
                    ,'datos'=> $input
                    ,'msg' => $rMsg
                )
            );
        }
    }
   
    /**
     * Proceso: Gestionar
     * @return array
     */
    public function postVerTodo()
    {
        $id = Input::get('proyecto_edificio_id');
        $datos = array();
        $provisional = array();
        $megaproyecto = array();
        $rst = 0;
        $ultimo = [];
        if (Input::has('proyecto_edificio_id')) {
            $select = [
                '*',
                'proyecto_estado AS proyectoEstado'
            ];
            $datos = $this->_proyectoEdificioRepo->find($id, $select);
            $rst = 1;

            $provisional = $this->_proyectoEdificioRepo
                    ->getProvisionales(
                        $id, $datos->proyectoEstado, 0
                    );
            $megaproyecto = $this->_proyectoEdificioRepo
                    ->getMegaproyectoGestiones($id, 0);
            //print_r($provisional);
//            $ultimo = $this->_gestionEdificioRepo->
//                getUltimoGestionesEdificiosRelacion($id);
            
            $ultimos = $this->_gestionEdificioRepo->
                getUltimosGestionesEdificiosRelacion($id);
            
            $result = $this->_proyectoEdificioRepo
            ->getTotalRecordsByProyectoEstado(
                //$datos->proyectoEstado, 
                null, 
                $datos->segmento
            );
        }

        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos,
                'estados' => $result,
                'provisional' => $provisional,
                'megaproyecto' => $megaproyecto,
                //'ultimo' => $ultimo, 
                'ultimos' => $ultimos, 
                //'last' => $this->_proyectoEdificioRepo->getLastId()
            )
        );
    }
    
    /**
     * Actualiza datos de edificios, solo coordenadas X e Y
     * @return type
     */
    public function postUbicar()
    {
        $id = Input::get('proyecto_edificio_id');
        $msg = trans("main.update_error");
        $rst = 0;
        if (Input::has('proyecto_edificio_id') && 
                Input::has('coord_y') &&
                Input::has('coord_x')) {
            $cX = Input::get('coord_x');
            $cY = Input::get('coord_y');
            $model = $this->_proyectoEdificioRepo->find(
                $id, 
                array('id', 'coord_x', 'coord_y')
            );
            $model['coord_x'] = $cX;
            $model['coord_y'] = $cY;
            if ($model->save()) {
                $rst = 1;
                $msg = Lang::get(
                    'main.update_successful', 
                    array('action' => 'Ubicar X e Y')
                );
            }
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msg' => $msg
            )
        );
    }

    public function postUbicarDistancia()
    {
        if (Input::has('coord_x') && Input::has('coord_y') && 
            Input::has('cantidad')) {
            
            $x =Input::get('coord_x');
            $y =Input::get('coord_y');
            $cant =Input::get('cantidad');
            try {
                $edificios = $this->_proyectoEdificioRepo
                ->getEdificioDistance(
                    $x, $y, $cant
                );
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $msj = Lang::get('main.error_select_db');
                return  array(
                    'rst'=>0,
                    'datos'=>$msj
                );
            }

            return array(
                'rst'=>1,
                'datos'=>$edificios
            );
        } else {
            return array(
                'rst'=>0,
                'datos'=>'',
                'msg' => Lang::get('main.no_edificio_alrededor')
            );
        }
    }
    
    public function postListarBitacora()
    {
        if (Input::has('proyecto_edificio_id')) {
            $i =Input::get('proyecto_edificio_id');
            try {
                $datos = (new ProyectoEdificioBitacora())->listWithUser($i);
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                return  array(
                    'rst'=>0,
                    'datos'=>'',
                    'msg' =>Lang::get('main.error_select_db')
                );
            }
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=> $datos
                )
            );/// ['data' => $datos]
        } else {
            return array(
                'rst'=>0,
                'datos'=>'',
                'msg' => Lang::get('main.no_parametro')
            );
        }
    }
    
    public function postAgregarBitacora()
    {
        $rMsg = trans('main.success_register');
        $rst = 0;
        $rDatos = '';
        if (Input::has('proyecto_edificio_id')) {
            $id =Input::get('proyecto_edificio_id');
            try {
                $peb =new ProyectoEdificioBitacora();
                $attr = array();
                $attr['proyecto_edificio_id'] = $id;
                $attr['descripcion'] = Input::get('descripcion');
                $attr['fecha_observacion'] = Input::get('fecha_observacion');
                $attr['usuario_created_at'] = Auth::id();
                if ($peb->validate($attr)) {
                    $attr['created_at'] = Carbon\Carbon::now();
                    $attr['updated_at'] = Carbon\Carbon::now();
                    $rDatos =  DB::table('proyecto_edificios_bitacora')
                        ->insert($attr);
                    $rMsg = trans('main.success_register');
                    $rst = 1;
                } else {
                    $rMsg = $peb->errors();
                }
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $rMsg = Lang::get('main.error_select_db');
            }
            return Response::json(
                array(
                    'rst'=>$rst
                    ,'datos'=> $rDatos
                    ,'msg' => $rMsg
                )
            );
        } else {
            return array(
                'rst'=>0,
                'datos'=>'',
                'msg' => Lang::get('main.no_parametro')
            );
        }
    }
    
    /**
     * plugin: jayra
     * @return array
     */
    public function postListarDos()
    {
        $verde = 0;
        $ambar = -5;
        $posts = ProyectoEdificio::select(
            array(
                'id',
                DB::raw('CAST(id AS UNSIGNED) AS ida')  // item
                , 'segmento', 'tipo_proyecto', 'distrito', 'ticket'
                , DB::raw(
                    'IF(LENGTH(parent)>0, CONCAT(parent,subitem), id) AS parent'
                    . ',TRIM(TRAILING ", " FROM CONCAT_WS'
                    . '(" ",direccion,numero,manzana,lote)) AS direccion'
                )
                , DB::raw(
                    "IF(DATEDIFF(fecha_termino,now()) <= 0, "
                    . "IF(numero_departamentos_habitados > "
                    . "2,'VIVEN','TERMINADO') , "
                    . "CONCAT('{' , YEAR(fecha_termino) , '} - {' "
                    . ", MONTH(fecha_termino) , '}') )  AS mes"
                )
                , DB::raw(
                    "(
                    SELECT 
			IF(
                            UNIX_TIMESTAMP(fecha_termino) > 0
                            ,CONCAT(
                            datediff(fecha_termino,current_date), 
                            '|', 
                            IF( datediff(fecha_termino,current_date) 
                            >= {$verde}, 'VERDE', 
                            IF(datediff(fecha_termino, current_date) 
                            >= {$ambar}, 'AMBAR', 'ROJO') ) 
                            ) 
                            ,CONCAT(
                            datediff(fecha_inicio,current_date), 
                            '|', 
                            IF( datediff(fecha_inicio,current_date) 
                            >= {$verde}, 'VERDE', 
                            IF(datediff(fecha_inicio, current_date) 
                            >= {$ambar}, 'AMBAR', 'ROJO') ) 
                            )
			)
                    FROM 
			gestiones_edificios 
                    WHERE
			proyecto_edificio_id = proyecto_edificios.id
                    AND 
			proyecto_estado_id IN (2,3,4,5,6,7)
                    ORDER BY created_at DESC LIMIT 1
                    ) semaforo"
                )
                , DB::raw("REPLACE(coord_x, ',', '.') as coord_x")
                , DB::raw("REPLACE(coord_y, ',', '.') as coord_y")
                , 'numero_departamentos'
                , 'proyecto_estado'
                , 'nombre_proyecto'
                , 'avance'
                //DB::raw('proyecto_estado AS estado')
                , 'fecha_termino'      // 'seguimiento' --> fecha
            )
        )->whereRaw('deleted_at IS NULL');

        $rfechas = Input::has('txt_rangofecha') ?
                Input::get('txt_rangofecha') :
                null;
        $rtexto = (Input::has('txt_buscar') && Input::has('txt_buscarPor')) ?
                Input::get('txt_buscar') :
                null;
        $rtextoPor = Input::get('txt_buscarPor');
        // array
        $rsegmento = Input::has('slct_segmento') ?
                Input::get('slct_segmento') :
                null;
        // array
        $rtipoproyecto = Input::has('slct_tipoproyecto') ?
                Input::get('slct_tipoproyecto') :
                null;
        // array
        $restadoproyecto = Input::has('slct_estadoproyecto') ?
                Input::get('slct_estadoproyecto') :
                null;
        // string
        $direccionN = Input::has('txt_direccion_n') ?
                Input::get('txt_direccion_n') :
                null;
        
        return Datatables::of($posts)
            ->addColumn(
                'action', function ($into) {
                $btn = '<a data-toggle="modal"'
                        . ' data-id="' . $into->id . '"'
                        . ' data-target="#proyectoEdificioModal"'
                        //.'href="#detalle-' . $into->id . '" '
                        . ' class="btn btn-primary"'
                        . ' ><i class="fa fa-eye"></i></a>';
                $btn .= ' <a data-toggle="modal"'
                        . ' data-target="#proyectoEdificioMantenimientoModal"'
                        . ' class="btn btn-primary editarG"'
                        . ' title="' 
                        . trans('main.editar', ['id' => $into->id]) . '"'
                        . ' data-accion="ACTUALIZAR"'
                        . ' ><i class="fa fa-edit"></i> </a>';
                return $btn . ' <a data-toggle="modal"'
                        . ' data-id="' . $into->id . '"'
                        . ' data-target="#proyectoEdificioEliminarModal"'
                        . ' class="btn btn-primary eliminarG"'
                        . ' title="'
                        . trans('main.eliminar', ['id' => $into->id]) . '"'
                        . ' ><i class="fa fa-remove"></i> </a>';
                }
            )
            ->filter(
                function ($query) use ($rfechas, $rtexto, $direccionN, 
                    $rtextoPor, $rsegmento, $rtipoproyecto, $restadoproyecto) {
                    if ($rfechas) {
                        $fechas = explode(' - ', $rfechas);
                        $query->whereBetween('fecha_termino', $fechas);
                    }
                    if ($rtexto && ($rtextoPor == 'direccion' 
                        || $rtextoPor == 'nombre_proyecto')) {
                        $query->where(
                            $rtextoPor, 'LIKE', "%$rtexto%"
                        );
                    } elseif ($rtexto && $rtextoPor == 'mes') {
                        $query->having('mes', 'LIKE', "%$rtexto%");
                    } elseif ($rtexto && $rtextoPor == 'id') {
                        $query->where(function($q) use ($rtexto)
                        {
                            $q->whereRaw("CONCAT(parent,subitem) = '$rtexto'")
                            ->orWhere('id', $rtexto);
                        });
                    //$query->whereOr($rtextoPor, $rtexto);
                    //$query->whereRaw("CONCAT(parent,subitem) = '$rtexto'");
                    } elseif ($rtexto) {
                        $query->where($rtextoPor, $rtexto);
                    }
                    if ($direccionN && $rtextoPor == 'direccion') {
                        $query->where(
                            DB::raw(
                                'CONCAT_WS(" ",numero,manzana,lote)'
                            ), 'LIKE', "%$direccionN%"
                        );    
                    }
                    if ($rsegmento && is_array($rsegmento)) {
                        $query->where(
                            function ($que) use ($rsegmento) {
                                foreach ($rsegmento as $val) {
                                    $que->orWhere('segmento', $val);
                                }
                            }
                        );
                    }
                    if ($rtipoproyecto && is_array($rtipoproyecto)) {
                        $query->where(
                            function ($que) use ($rtipoproyecto) {
                                foreach ($rtipoproyecto as $val) {
                                    $que->orWhere('tipo_proyecto', $val);
                                }
                            }
                        );
                    }
                    if ($restadoproyecto && is_array($restadoproyecto)) {
                        $query->where(
                            function ($que) use ($restadoproyecto) {
                                foreach ($restadoproyecto as $val) {
                                    $que->orWhere('proyecto_estado', $val);
                                }
                            }
                        );
                    }
                }
            )
            ->make(true);    
    }
    
    public function postProyectosTipo()
    {
        if (Request::ajax()) {
        
            return Response::json(
                $this->_proyectoEdificioRepo->getProyectoTipos()
            );
        }
    }
    
    public function postProvisionalAcceso()
    {
        if (Request::ajax()) {
        
            return Response::json(
                $this->_proyectoEdificioRepo
                ->getProvisionalAcceso(Auth::id())
            );
        }
    }
    
    public function postMegaAcceso()
    {
        if (Request::ajax()) {
        
            return Response::json(
                $this->_proyectoEdificioRepo
                ->getMegaAcceso(Auth::id())
            );
        }
    }
    

    /**
     * Procesar archivo manda error si upload_max_filesize (php.ini) no esta 
     * bien configurado.
     * @return type
     */
    public function postProcesararchivo()
    {
        //validar archivo - print_r(Input::file('archivo')); exit;
        if (Input::hasFile('archivo')) {
            if (Input::file('archivo')->isValid()) {
                $file = Input::file('archivo');
                $tmpArchivo = $file->getRealPath();
                $data = Helpers::fileToJsonAddress($tmpArchivo, 0);
            } else {
                return Response::json(
                    array(
                        'estado' => '0',
                        'msj' => 'Archivo no valido'
                    )
                );
            }
        } else {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'No se encontro archivo valido'
                )
            );
        }

        $name = time() . '-' . $file->getClientOriginalName();
        $path = 'uploads/' . date('Y/m/');
        $file->move($path, $name);
        DB::table('proyecto_edificios_xls')->truncate();
        $publicPath = strpos(public_path($path), '\\') === false?  
            public_path($path) : 
            str_replace('\\', '/', public_path($path));
        $pdo = DB::connection()->getPdo();
        $pdo->exec(
            "LOAD DATA LOCAL INFILE '" . $publicPath . $name .
            "' INTO TABLE proyecto_edificios_xls " .
            "FIELDS TERMINATED BY '\t' " .
            "ENCLOSED BY '\"' " .
            "LINES TERMINATED BY '\n' " .
            "IGNORE 1 LINES "
        );
        //recorrer la tabla proyecto_edificios_xls e insertar registros en
        //proyecto_edificios y luego truncar proyecto_edificios_xls
        $proyectos = DB::table('proyecto_edificios_xls')
            ->select(
                'id', 'foto_uno', 'foto_dos', 'foto_tres', 
                'zonal', 'armario', 'facilidades', 
                'tipo_via', 'tipo_cchh', 'cchh', 'tipo_infraestructura', 
                'departamento', 'provincia', 
                'ruc_constructora', 'gestion_obra', 'zona_competencia'
            )
            ->get();

        set_time_limit(0);
        $registro = array();
        $i = count($proyectos);
        foreach ($proyectos as $proyecto) {
            $registro = (array) $proyecto;
            /*
            //$registro['proyecto']=$proyecto->id;
            //$this->_proyectoEdificioRepo->create($registro);
            //ProyectoEdificio::firstOrCreate($registro);

            //$registro['fecha_termino'] =
            //$this->aFechaSistema($registro['fecha_termino']);
            //ProyectoEdificio::updateOrCreate
            //(['id' => $registro['id']], $registro);
            */
            ProyectoEdificio::where('id', $registro['id'])
            ->update(
                $registro
            );
        }
        //DB::table('proyecto_edificios_xls')->truncate();
        ////////////////que redis
        return Response::json(
            array(
                'rst' => 1,
                'msj' => 'Se cargaron ' . $i . ' registros',
            )
        );
    }
    
    /**
     * Carga masiva de (.txt|excel) a (insert into) proyecto_edificios_uno_xls
     * Carga masiva de (.txt|excel) a (insert / update) proyecto_edificios_col_xls
     * Antes de insertar/actualizar tabla principal
     * @return array
     */
    public function postProcesarArchivoUno()
    {
        if (Input::hasFile('archivouno')) {
            $table = 'proyecto_edificios_uno_xls'; 
            $archivo = 'archivouno';
            $consulta = " IGNORE 1 LINES";
            $charset = 'latin1';   
        } else if (Input::hasFile('archivocolumna')) {
            $table = 'proyecto_edificios_col_xls';
            $archivo = 'archivocolumna';   
            $consulta = "";
            $charset = 'utf8'; 
        } else {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'Parametros no valido.'
                )
            );
        }

        if (Input::file($archivo)->isValid()) {
            $file = Input::file($archivo);
            $tmpArchivo = $file->getRealPath();
            Helpers::fileToJsonAddress($tmpArchivo, 0);
        } else if(!Input::file($archivo)->isValid()) {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'Archivo no valido.'
                )
            );
        }

        $name = time() . '-' . $file->getClientOriginalName();
        $path = 'uploads/' . date('Y/m/');
        $file->move($path, $name);

        DB::table($table)->truncate();
        sleep(3);
        set_time_limit(0);
        $publicPath = strpos(public_path($path), '\\') === false?  
            public_path($path) : 
            str_replace('\\', '/', public_path($path));

        try {
            $pdo = DB::connection()->getPdo();
                //"CHARACTER SET 'utf8';" .
                //"ESCAPED BY '' " .
                //"FIELDS TERMINATED BY ',' " .
            $i = $pdo->exec(
                "LOAD DATA LOCAL INFILE '" . $publicPath . $name ."'" .
                " INTO TABLE $table " .
                " CHARACTER SET " . $charset .
                " FIELDS TERMINATED BY '\t'" .
                " ENCLOSED BY '\"'" .
                " LINES TERMINATED BY '\n'" .
                $consulta
            );
        } catch (Exception $exc) {
            Helpers::saveError($exc);
            return Response::json(
                array(
                    'rst' => '0',
                    'msj' => 'Error al cargar registros.'
                )
            );
        }

        if (!isset($i) || $i == 0 || is_null($i)) {
            $rst = '0';   
            $i = 0;
        } else {
            $rst = '1';
        }

        return Response::json(
            array(
                'rst' => $rst,
                'msj' => 'Se cargaron ' . $i 
                    . ' registros a la tabla temporal'
            )
        );
    }
    
    /**
     * Recorrer la tabla proyecto_edificios_uno_xls e actualizar registros en
     * proyecto_edificios
     * @return array
     */
    public function postProcesarArchivoDos()
    {
        try{
            $table = 'proyecto_edificios_uno_xls';
            $proyectos = DB::table($table)
                ->get();
            
            //////////////// queue redis
            set_time_limit(0);
            $registro = array();
            $i = count($proyectos);
            foreach ($proyectos as $proyecto) {
               // $i++;
                $registro = (array) $proyecto;
                if ($registro['cab_casuistica'] != '') {
                    $casuistica = $registro['cab_casuistica'];
                } elseif ($registro['oc_casuistica'] != '') {
                    $casuistica = $registro['oc_casuistica'];
                } elseif ($registro['lic_casuistica'] != '') {
                    $casuistica = $registro['lic_casuistica'];
                } elseif ($registro['dis_casuistica'] != '') {
                    $casuistica = $registro['dis_casuistica'];
                } else {
                    $casuistica = ''; 
                }
                
                // EL CODIGO DE AQUI ES CAMBIABLE
                $registro['tipo_proyecto'] = 
                    strtoupper($registro['tipo_proyecto']);
                $registro['segmento'] = strtoupper($registro['segmento']);
                $registro['casuistica'] = $casuistica;
                $registro['proyecto_estado'] = $registro['estado'];
                $registro['persona_contacto'] = $registro['contacto'];
                $registro['seguimiento'] = 
                    $this->aFechaSistema($registro['fecha_seguimiento']);
                $registro['fecha_termino'] =
                    $this->aFechaSistema($registro['fecha_termino_cons']);
                //
                $registro['fecha_liquidacion_sis'] =
                    $this->aFechaSistema($registro['fecha_liquidacion_sis']);
                $registro['fecha_termino_pro'] =
                    $this->aFechaSistema($registro['fecha_termino_pro']);
                $registro['fecha_respuesta_eje'] =
                    $this->aFechaSistema($registro['fecha_respuesta_eje']);
                $registro['fecha_entrega_pro'] =
                    $this->aFechaSistemaValor($registro['fecha_entrega_pro']);
                $registro['bitacora_fecha'] =
                    $this->aFechaSistemaValor($registro['bitacora_fecha']);
                
                unset(
                    $registro['fecha_termino_cons'], 
                    $registro['fecha_seguimiento'],     
                    $registro['contacto'],
                    $registro['estado'],
                    $registro['dis_fecha_inicio'],
                    $registro['dis_fecha_compromiso'],     
                    $registro['dis_ultima_fecha_ges'], 
                    $registro['dis_estado_general'], 
                    $registro['dis_gestion_interna'], 
                    $registro['dis_fecha_termino'], 
                    $registro['dis_casuistica'], 
                    $registro['lic_recepcion_expediente'], 
                    $registro['lic_fecha_compromiso'], 
                    $registro['lic_fecha_inicio'], 
                    $registro['lic_fecha_termino'], 
                    $registro['lic_casuistica'], 
                    $registro['oc_fecha_inicio'], 
                    $registro['oc_fecha_compromiso'], 
                    $registro['oc_fecha_termino'], 
                    $registro['oc_casuistica'], 
                    $registro['cab_fecha_inicio'], 
                    $registro['cab_fecha_compromiso'],
                    $registro['cab_fecha_termino'], 
                    $registro['cab_casuistica']
                );
                
                ProyectoEdificio::updateOrCreate(
                    ['id' => $registro['id']], 
                    $registro
                );
                // EL CODIGO DE AQUI ES CAMBIABLE
            }
            $rMsg = 'Se Actualizaron ' . $i . ' registros';
            $rst = 1;
            //////////////// queue redis
        } catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg,
            )
        );    
    }
    
    /**
     * Recorrer la tabla proyecto_edificios_uno_xls e actualizar registros en
     * gestion_proyectos
     * @return array
     */
    public function postProcesarArchivoTres()
    {
        $estado = Input::get('estado');
        $eliminar = Input::get('eliminar');
       
        if (Input::has('estado') && is_numeric($estado)) {
            $estado = intval($estado);
            switch ($estado) {
                case 4:
                    /* iniciativa */
                    $select = [
                        'id', 'fecha_termino_cons', 'fecha_seguimiento'
                    ];
                    break;
                case 3:
                    /* disenio */
                    $select = [
                        'id', 'dis_fecha_inicio', 'dis_fecha_compromiso', 
                        'dis_ultima_fecha_ges', 'dis_estado_general', 
                        'dis_gestion_interna', 'dis_fecha_termino',  
                        'dis_casuistica'
                    ];
                    break;
                case 5:
                    /* licencia */
                    $select = [
                        'id', 'lic_recepcion_expediente', 
                        'lic_fecha_compromiso', 
                        'lic_fecha_inicio', 'lic_fecha_termino',   
                        'lic_casuistica'
                    ];
                    break;
                case 6:
                    /* obra_civil */
                    $select = [
                        'id', 'oc_fecha_inicio', 'oc_fecha_compromiso', 
                        'oc_fecha_termino', 'oc_casuistica'
                    ];
                    break;
                case 7:
                    /* cableado */
                    $select = [
                        'id', 'cab_fecha_inicio', 'cab_fecha_compromiso', 
                        'cab_fecha_termino', 'cab_casuistica'
                        /* ,'cab_fecha_teorica_final' */
                    ];
                    break;
                default:
                    return Response::json(
                        array(
                            'rst' => 0,
                            'msj' => 'No es un ESTADO valido (' . $estado .').'
                        )
                    );
            }
        }
        
        try{
            if (isset($eliminar) && $eliminar == 1) {
                DB::table('gestiones_edificios')
                    ->where('proyecto_estado_id', '=', $estado)
                    ->delete();
            }
            
            $table = 'proyecto_edificios_uno_xls';
            $proyectos = DB::table($table)
                ->select($select)
                ->where(
                    function($qu) use ($estado)
                    {
                        if ( $estado == 4 ) {
                            $qu
                            ->where('estado', '=', 'A1 INICIATIVA');
                        } elseif ( $estado == 3) {
                            $qu
                            ->where('estado', '=', 'B1 DISEÑO');
                        } elseif ( $estado == 5) {
                            $qu
                            ->where('estado', '=', 'B2 LICENCIA');
                        } elseif ( $estado == 6) {
                            $qu
                            ->where('estado', '=', 'B3 OOCC');
                        } elseif ( $estado == 7) {
                            $qu
                            ->where('estado', '=', 'B4 CABLEADO');
                        } 
                    }
                )
                ->get(); 
            
            //////////////// queue redis
            set_time_limit(0);
            $reg = array();
            $i = count($proyectos);
            foreach ($proyectos as $proyecto) {
                $reg = (array) $proyecto;
                //insert en gestion_proyectos
                switch ($estado) {
                    case 4:
                        /* iniciativa */
                        $datos = [
                            'fecha_termino' => $this->aFechaSistemaValor(
                                $reg['fecha_termino_cons']
                            ), 
                            'fecha_seguimiento' => $this->aFechaSistemaValor(
                                $reg['fecha_seguimiento']
                            ),
                            'proyecto_estado_id' => 4, 
                            'casuistica_id' => 80,
                            'proyecto_tipo_id' => 6,
                            'avance' => 4
                        ];
                        break;
                    case 3:
                        /* disenio */
                        $datos = [
                            'fecha_inicio' =>  $this->aFechaSistemaValor(
                                $reg['dis_fecha_inicio']
                            ), 
                            'fecha_compromiso' => $this->aFechaSistemaValor(
                                $reg['dis_fecha_compromiso']
                            ), 
                            'fecha_gestion' => $this->aFechaSistemaValor(
                                $reg['dis_ultima_fecha_ges']
                            ), 
                            'estado_general' => $reg['dis_estado_general'], 
                            'gestion_interna' => $reg['dis_gestion_interna'],  
                            'fecha_termino' => $this->aFechaSistemaValor(
                                $reg['dis_fecha_termino']
                            ),   
                            'proyecto_estado_id' => 3, 
                            'casuistica_id' => 76,
                            'proyecto_tipo_id' => 1,
                            'avance' => 3
                        ];
                        break;
                    case 5:
                        /* licencia */
                        $datos = [
                            'recepcion_expediente' => 
                            $reg['lic_recepcion_expediente'], 
                            'fecha_compromiso' => $this->aFechaSistemaValor(
                                $reg['lic_fecha_compromiso']
                            ),
                            'fecha_inicio' => $this->aFechaSistemaValor(
                                $reg['lic_fecha_inicio']
                            ),
                            'fecha_termino' => $this->aFechaSistemaValor(
                                $reg['lic_fecha_termino']
                            ),  
                            'proyecto_estado_id' => 5, 
                            'casuistica_id' => 77,
                            'proyecto_tipo_id' => 2,
                            'avance' => 5
                        ];
                        break;
                    case 6:
                        /* obra_civil */
                        $datos = [
                            'fecha_inicio' => $this->aFechaSistemaValor(
                                $reg['oc_fecha_inicio']
                            ), 
                            'fecha_compromiso' => $this->aFechaSistemaValor(
                                $reg['oc_fecha_compromiso']
                            ),
                            'fecha_termino' => $this->aFechaSistemaValor(
                                $reg['oc_fecha_termino']
                            ),
                            'proyecto_estado_id' => 6, 
                            'casuistica_id' => 78,
                            'proyecto_tipo_id' => 3,
                            'avance' => 6,
                        ];
                        break;
                    case 7:
                        /* cableado */
                        $datos = [
                            'fecha_inicio' => $this->aFechaSistemaValor(
                                $reg['cab_fecha_inicio']
                            ),
                            'fecha_compromiso' => $this->aFechaSistemaValor(
                                $reg['cab_fecha_compromiso']
                            ),
                            'fecha_termino' => $this->aFechaSistemaValor(
                                $reg['cab_fecha_termino']
                            ),
                            'proyecto_estado_id' => 7, 
                            'casuistica_id' => 79,
                            'proyecto_tipo_id' => 4,
                            'avance' => 7
                        ];
                        break;
                }

                $datos['proyecto_edificio_id'] = $reg['id'];
                $datos['created_at'] = Carbon\Carbon::now();
                $datos['usuario_created_at'] = Auth::id();

                DB::table('gestiones_edificios')->insert($datos);
            }
            $rMsg = 'Se Crearon (' . $estado . ') ' . $i . ' registros';
            $rst = 1;
            //////////////// queue redis
        }catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg
            )
        );
    }

    /**
     * Tabla proyecto_edificios_uno_xls 
     * @return array
     */
    public function postProcesarArchivoBitacora()
    {
        $eliminar = Input::get('eliminar');
        /* bitacora */
        $select = ['id', 'bitacora', 'bitacora_fecha'];
        
        try{
            if (isset($eliminar) && $eliminar == 1) {
                DB::table('proyecto_edificios_bitacora')
                    ->whereRaw('DATE(created_at) = current_date')
                    ->delete();
            }
            
            $table = 'proyecto_edificios_uno_xls';
            $proyectos = DB::table($table)
                ->select($select)
                ->where('bitacora', '!=', '')
                ->get(); 
            
            //////////////// queue redis
            set_time_limit(0);
            $reg = array();
            $i = count($proyectos);
            $fechaActual = Carbon\Carbon::now();
            foreach ($proyectos as $proyecto) {
                $reg = (array) $proyecto;
                // insert en proyecto_edificios_bitacora
                $datos = [];
                $datos['descripcion'] = $reg['bitacora'];
                $datos['fecha_observacion'] = 
                    $this->aFechaSistemaValor($reg['bitacora_fecha']);
                $datos['tipo_registro'] = 1;
                $datos['tab_registro'] = 2;
                $datos['proyecto_edificio_id'] = $reg['id'];
                $datos['created_at'] = $fechaActual;
                $datos['usuario_created_at'] = Auth::id();

                DB::table('proyecto_edificios_bitacora')->insert($datos);
            }
            $rMsg = 'Se Crearon ' . $i . ' registros';
            $rst = 1;
            //////////////// queue redis
        }catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg,
            )
        );
    }

    /**
     * Tabla proyecto_edificios_uno_xls 
     * @return array
     */
    public function postProcesarArchivoMegaproyecto()
    {
        $eliminar = Input::get('eliminar');
        $table = 'proyecto_edificios_uno_xls';
        $aTable = 'gestiones_edificios_megaproyecto';
        /* megaproyecto */
        $select = [
            'id', 'mega_energia_estado', 'mega_fo_estado', 
            'mega_troba_nueva', 'mega_troba_fecha_fin',   
            'mega_troba_estado' 
        ];
        
        try{
            if (isset($eliminar) && $eliminar == 1) {
                DB::table($aTable)
                    ->whereRaw('DATE(created_at) = current_date')
                    ->delete();
            }
            $proyectos = DB::table($table)
                ->select($select)
                ->orWhere('mega_troba_nueva', '!=', '')
                ->orWhere('mega_troba_estado', '!=', '')
                ->orWhere('mega_energia_estado', '!=', '')
                ->orWhere('mega_fo_estado', '!=', '')
                ->orWhere('mega_troba_fecha_fin', '!=', '')
                ->get(); 

            set_time_limit(0);
            $reg = array();
            $i = count($proyectos);
            $fechaActual = Carbon\Carbon::now();
            foreach ($proyectos as $proyecto) {
                $reg = (array) $proyecto;
                $datos = [];
                $datos['energia_estado'] = $reg['mega_energia_estado'];
                $datos['fo_estado'] = $reg['mega_fo_estado'];
                $datos['troba_nueva'] = $reg['mega_troba_nueva'];
                $datos['troba_fecha_fin'] = 
                    $this->aFechaSistemaValor($reg['mega_troba_fecha_fin']);
                $datos['troba_estado'] = $reg['mega_troba_estado'];
                $datos['parent'] = 0;
                $datos['proyecto_edificio_id'] = $reg['id'];
                $datos['created_at'] = $fechaActual;
                $datos['usuario_created_at'] = Auth::id();

                DB::table($aTable)->insert($datos);
            }
            $rMsg = 'Se Crearon ' . $i . ' registros';
            $rst = 1;
        }catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg,
            )
        );
    }

    /**
     * Tabla proyecto_edificios_uno_xls 
     * @return array
     */
    public function postProcesarArchivoProvisional()
    {
        $eliminar = Input::get('eliminar');
        $table = 'proyecto_edificios_uno_xls';
        $aTable = 'provisional_proyecto_edificio';
        /* provisional */
        $select = [
            'id', 'fecha_entrega_pro', 'fecha_respuesta_eje', 
            'fecha_termino_pro', 'fecha_liquidacion_sis',   
            'estado_provisional', 'prov_ptr', 'prov_grafo', 
            'evaluador_ejecutor', 'estado'
        ];
        try{
            if (isset($eliminar) && $eliminar == 1) {
                DB::table($aTable)
                    ->whereRaw('DATE(updated_at) = current_date')
                    ->delete();
            }
            
            $proyectos = DB::table($table)
                ->select($select)
                ->orWhere('fecha_entrega_pro', '!=', '')
                ->orWhere('fecha_respuesta_eje', '!=', '')
                ->orWhere('fecha_termino_pro', '!=', '')
                ->orWhere('fecha_liquidacion_sis', '!=', '')
                ->orWhere('estado_provisional', '!=', '')
                ->orWhere('evaluador_ejecutor', '!=', '')
                ->get(); 

            set_time_limit(0);
            $reg = array();
            $i = count($proyectos);
            $fechaActual = Carbon\Carbon::now();
            foreach ($proyectos as $proyecto) {
                $reg = (array) $proyecto;
                $datos = [];
                $datos['fecentrega'] = 
                $this->aFechaSistemaValor($reg['fecha_entrega_pro']);
                $datos['fecrespuesta'] = 
                $this->aFechaSistemaValor($reg['fecha_respuesta_eje']);
                $datos['fectermino'] = 
                $this->aFechaSistemaValor($reg['fecha_termino_pro']);
                $datos['fecliquidacion'] = 
                $this->aFechaSistemaValor($reg['fecha_liquidacion_sis']);
                $datos['estado_provisional'] = $reg['estado_provisional'];
                $datos['ejecutor_proyecto'] = $reg['evaluador_ejecutor'];
                $datos['parent'] = 0;
                $datos['idproyectoedificio'] = $reg['id'];
                $datos['updated_at'] = $fechaActual;
                $datos['usuario_created_at'] = Auth::id();
                $datos['estado_proyecto'] = $reg['estado'];
                DB::table($aTable)->insert($datos);
            }
            $rMsg = 'Se Crearon ' . $i . ' registros';
            $rst = 1;
        }catch (Exception $exc) {
            $this->_errorController->saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg,
            )
        );
    }
    
    /**
     * Recorrer la tabla proyecto_edificios_uno_xls e actualizar registros en
     * proyecto_edificios
     * @return array
     */
    public function postProcesarArchivoColumna()
    {
        try{
            $tabla = 'proyecto_edificios_col_xls';
            $tablaColumna = 'proyecto_edificios_col_col';
            $seleccion = '';

            /* validar primer campo debe ser: 'ITEM' */
            $primeraFila = (array) DB::table($tabla)->first();
            if (strcmp($primeraFila['campo1'], 'ITEM') !== 0) {
                return Response::json(
                    array(
                        'rst' => '0',
                        'msj' => 'Debe existir la primera COLUMNA "ITEM"',
                    )
                );       
            }

            $listaCampo = (array) DB::table($tablaColumna)->get();
            //Re-Ordenamiento
            $listaCampoOrd = [];
            foreach($listaCampo as $v){
                $v = (array) $v;
                $listaCampoOrd[$v['alias']] = $v;
            }

            foreach($primeraFila as $llave => $valor) {
                if(empty($valor) || !isset($listaCampoOrd[$valor]['nombre'])) {
                    unset($primeraFila[$llave]);
                    continue;
                }
                $seleccion .= "{$llave} AS '{$listaCampoOrd[$valor]['nombre']}',"; 
            }
            $seleccion = trim($seleccion, ',');

            $proyectos = DB::table($tabla)
                ->select(DB::raw($seleccion))
                ->get();
                    
            //////////////// queue redis
            DB::beginTransaction();
            set_time_limit(0);
            $i = count($proyectos) - 1;
            $reg = array();
            foreach ($proyectos as $proLlave => $proyecto) {
                $reg = (array) $proyecto;

                if ($proLlave == 0) {
                    continue;
                }

                if (isset($reg['id']) && $reg['id'] == '') {
                    $reg['id'] = 
                    $this->_proyectoEdificioRepo->getLastId();
                }
                //Validar campos
                foreach ($listaCampoOrd as $base) {
                    if(
                        $base['tipo'] == 'v' 
                        || $base['tipo'] == 't'
                        || $base['tipo'] == 'f'
                    ) {
                        continue;
                    } elseif ($base['tipo'] == 'i') {
                        $reg[$base['nombre']] = 
                            intval($reg[$base['nombre']]);
                    } elseif (!in_array($base['alias'], $primeraFila)) {
                        unset($listaCampoOrd[$base['alias']]);
                    } elseif ($base['tipo'] == 'u') {
                        $reg[$base['nombre']] = 
                            strtoupper($reg[$base['nombre']]);
                    }
                    /*
                    elseif ($base['nombre'] == 'persona_contacto') {
                        $cadena = 
                            explode("|", $reg[$base['nombre']]);

                    }
                    */ 
                    elseif ($base['tipo'] == 'd') {
                        $reg[$base['nombre']] = 
                            $this->aFechaSistema($reg[$base['nombre']]);
                    }
                }
                // $reg['updated_at']
                ProyectoEdificio::updateOrCreate(
                    ['id' => $reg['id']], 
                    $reg
                );
            }
            $rMsg = 'Actualizados / Creados ' . $i . ' registros';
            $rst = 1;
            DB::commit();  
            //////////////// queue redis
        } catch (Exception $exc) {
            Helpers::saveError($exc);
            $rMsg = Lang::get('main.error_select_db');
            $rst = 0;
            DB::rollback();
        }
        
        return Response::json(
            array(
                'rst' => $rst,
                'msj' => $rMsg,
            )
        );    
    }

    /**
     * De '11/08/2016' a '2016-08-11'
     * @param type $fechaSinFormato
     * @return type
     */
    public function aFechaSistema($fechaSinFormato)
    {
        return (strlen($fechaSinFormato) != 10 
                    || $fechaSinFormato == '00/01/1900' 
                    || strpos($fechaSinFormato, '/') 
                    === false
                )? 
                '' : 
                Carbon\Carbon::createFromFormat(
                    'd/m/Y', $fechaSinFormato
                )->toDateString();
    }
    
    public function aFechaSistemaValor($fechaSinFormato)
    {
        $valor = $this->aFechaSistema($fechaSinFormato);
        return empty($valor)? $fechaSinFormato : $valor;
    }

    public function postCrear()
    {
        if (Request::ajax()) {
            $regex='regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú-]{2,90})$/i';
            $regexN='regex:/^[0-9]+$/';
            $required='required';
            $numeric='numeric';
            $input = Input::all();
            $input['id'] = $this->_proyectoEdificioRepo->getLastId();
            $reglas = array(
                'id' => 'required|unique:proyecto_edificios|min:1'
                , 'segmentos' => $required // .'|'.$regex
                , 'tipo_proyectos' => $required // .'|'.$regex
                , 'nombre_proyecto' => $required
                , 'padre' => $regexN
                , 'fecha_termino' => $required
                , 'numero_block' => "$required|$numeric"
                , 'numero_pisos' => "$required|$numeric"
                , 'numero_departamentos' => "$required|$numeric"
                , 'numero_departamentos_habitados' => "$required|$numeric"
                , 'avance' => "$required|$numeric"
                , 'tipo_infraestructura' => $required
                , 'fuente_identificacion' => $required
                , 'seguimiento' => $required
                , 'uno_contacto' => "$required|$regex"
                , 'uno_telefono_a' => $required
                , 'uno_email' => $required
                
            );
            $mensaje = array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
                'numeric'   => ':attribute seleccione una opcion',
            );
            // Sobreescribiendo clave (crear)
            if (strlen($input['padre']) > 0) {
                $input['id'] = $input['padre'] . $input['subitem'];
                $reglas['subitem'] = $required;
            }

            $validator = Validator::make($input, $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                        'insert' => false
                    )
                );
            }   
            
            try{
                $estado = DB::table('proyecto_edificios')
                ->insert(
                    array(
                        'id' => $input['id'],
                        'armario' => $input['armario'],
                        'avance' => $input['avance'],
                        'casuistica' => '', /* bandeja */
                        'troba_bidireccional' => $input['troba_bidireccional'], 
                        'cchh' => $input['cchh'],
                        'competencia_uno' => $input['competencia_uno'], 
                        'coord_x' => $input['coord_x'],
                        'coord_y' => $input['coord_y'],
                        'departamento' => $input['departamento'],
                        'direccion' => $input['direccion'],
                        'distrito' => $input['distrito'],
                        'persona_contactodos' => $input['dos_contacto'],
                        'persona_contactodos_cel' => trim(
                            $input['dos_telefono_a'] . '|' 
                            . $input['dos_telefono_b'], '|'
                        ) , 
                        'pagina_web_dos' => $input['dos_paginaweb'],
                        'proyecto_estado' => $input['proyecto_estado'],
                        'fecha_termino' => $input['fecha_termino'],
                        'lote' => $input['lote'],
                        'manzana' => $input['manzana'],
                        'nombre_constructora' => 
                            $input['nombre_constructora'],
                        'nombre_proyecto' =>  $input['nombre_proyecto'],
                        'numero' => $input['numero'],
                        'numero_block' => $input['numero_block'],
                        'numero_departamentos' => 
                            $input['numero_departamentos'],
                        'numero_departamentos_habitados' => 
                            $input['numero_departamentos_habitados'],
                        'numero_piso' => $input['numero_pisos'],
                        'observacion' => $input['observacion'],
                        'provincia' => $input['provincia'],
                        'tipo_proyecto' => Input::get('tipo_proyectos'),
                        'ruc_constructora' => Input::get('ruc_constructora'),
                        'segmento' => Input::get('segmentos'),
                        'seguimiento' => Input::get('seguimiento'), 
                        'tipo_cchh' => Input::get('tipo_cchh'),
                        'tipo_infraestructura' => 
                            isset($input['tipo_infraestructura'])?
                            $input['tipo_infraestructura'] : '',
                        'tipo_via' => $input['tipo_via'],
                        'troba_gis' => $input['troba_gis'],
                        'persona_contacto' => $input['uno_contacto'],
                        'persona_contacto_cel' => trim(
                            $input['uno_telefono_a'] . '|' 
                            . $input['uno_telefono_b'], '|'
                        ),
                        'pagina_web' => $input['uno_email'], 
                        'created_at' => date("Y-m-d H:m:s"),
                        'usuario_created_at' => Auth::id(),
                        'fuente_identificacion' => $input['fuente_identificacion'],
                        'proyecto_tipo' => '',
                        'parent' => ($input['padre'] != '' ? $input['padre'] : ''),
                        'subitem' => ($input['subitem'] != '' ? $input['subitem'] : '')
                    )
                );

                $rMsg = Lang::get('main.success_register') 
                    . ". Edificio con ID #<b>{$input['id']}</b>";
                $rst = 1;

                // bitacora
                $attr = [];
                $attr['proyecto_edificio_id'] = $input['id'];
                $attr['descripcion'] = 
                    'CREAR|Gestion|ProyectoEdificios|Modal: Crear datos';
                (new ProyectoEdificioBitacora())->add($attr);
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $rMsg = Lang::get('main.error_select_db');
                $rst = 0;
                $estado = false;
            }

            return Response::json(
                array(
                    'rst' => $rst,
                    'msj' => $rMsg,
                    'insert' => $estado
                )
            );
        }
    }

    public function postEditar()
    {
        if (Request::ajax() && (Input::has('id'))) {
            $regex='regex:/^([a-zA-Z01-9 .,ñÑÁÉÍÓÚáéíóú_-]{2,60})$/i';
            //'regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú-]{2,90})$/i';
            $regexN='regex:/^[0-9]+$/';
            $required='required';
            $numeric='numeric';
            $input = Input::all();
            $idWhere = $input['id'];
            $idValue = $input['id'];
            $bitacora = [];

            $reglas = array(
                'segmentos' => $required //.'|'.$regex
                , 'tipo_proyectos' => $required //.'|'.$regex
                , 'nombre_proyecto' => $required
                , 'padre' => "$regexN"
                , 'fecha_termino' => $required
                , 'numero_block' => "$required|$numeric"
                , 'numero_pisos' => "$required|$numeric"
                , 'numero_departamentos' => "$required|$numeric"
                , 'avance' => "$required|$numeric"
                , 'tipo_infraestructura' => $required
                , 'fuente_identificacion' => $required
                , 'seguimiento' => $required
                , 'uno_contacto' => "$required|$regex"
                , 'uno_telefono_a' => $required
                , 'uno_email' => $required
            );
            // Sobreescribiendo clave (editar)
            if (Input::has('padre') && Input::get('subitem')) {
                $idValue = $input['padre'] . $input['subitem'];
                $input['id'] = $input['padre'] . $input['subitem'];
                $reglas['id'] = "$regex|unique:proyecto_edificios,id|min:1";
                $reglas['subitem'] = $required;
            }

            $mensaje = array(
                'required'  => ':attribute Es requerido',
                'regex'     => ':attribute Solo debe ser Texto',
                'numeric'   => ':attribute seleccione una opcion',
            );

            $validator = Validator::make($input, $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                    'rst' => 2,
                    'msj' => $validator->messages(),
                    )
                );
            }

            try{
                $estado = DB::table('proyecto_edificios')
                ->where('id', $idWhere)
                ->update(
                    array(
                        'id' =>  $idValue,
                        'armario' => $input['armario'],
                        'avance' => $input['avance'],
                        'casuistica' => '', /* bandeja */
                        'troba_bidireccional' => $input['troba_bidireccional'], 
                        'cchh' => $input['cchh'],
                        'competencia_uno' => $input['competencia_uno'],
                        'coord_x' => $input['coord_x'],
                        'coord_y' => $input['coord_y'],
                        'departamento' => $input['departamento'],
                        'direccion' => $input['direccion'],
                        'distrito' => $input['distrito'],
                        'persona_contactodos' => $input['dos_contacto'],
                        'persona_contactodos_cel' => trim(
                            $input['dos_telefono_a'] . '|' 
                            . $input['dos_telefono_b'], '|'
                        ), 
                        'pagina_web_dos' => $input['dos_paginaweb'],
                        'proyecto_estado' => $input['proyecto_estado'],
                        'fecha_termino' => $input['fecha_termino'],
                        'lote' => $input['lote'],
                        'manzana' => $input['manzana'],
                        'nombre_constructora' => 
                            $input['nombre_constructora'],
                        'nombre_proyecto' => $input['nombre_proyecto'],
                        'numero' => $input['numero'],
                        'numero_block' => $input['numero_block'],
                        'numero_departamentos' => 
                            $input['numero_departamentos'],
                        'numero_departamentos_habitados' => 
                            $input['numero_departamentos_habitados'],
                        'numero_piso' => $input['numero_pisos'],
                        'observacion' => $input['observacion'],
                        'provincia' => $input['provincia'],
                        'tipo_proyecto' => Input::get('tipo_proyectos'),
                        'ruc_constructora' => Input::get('ruc_constructora'),
                        'segmento' => Input::get('segmentos'),
                        'seguimiento' =>  $input['seguimiento'], 
                        'tipo_cchh' => Input::get('tipo_cchh'),
                        'tipo_infraestructura' => 
                            $input['tipo_infraestructura'],
                        'tipo_via' => $input['tipo_via'],
                        'troba_gis' => $input['troba_gis'],
                        'persona_contacto' => $input['uno_contacto'],
                        'persona_contacto_cel' => trim(
                            $input['uno_telefono_a'] . '|' 
                            . $input['uno_telefono_b'], '|'
                        ),
                        'pagina_web' => $input['uno_email'], 
                        'updated_at' => date("Y-m-d H:m:s"),
                        'usuario_updated_at' => Auth::id(),
                        'fuente_identificacion' => $input['fuente_identificacion'],
                        'proyecto_tipo' => '',
                        'parent' => ($input['padre'] != '' ? $input['padre'] : ''),
                        'subitem' => ($input['subitem'] != '' ? $input['subitem'] : '')
                    )
                );
                $rMsg = Lang::get('main.success_update')
                    . ". Edificio con ID #<b>{$idValue}</b>";
                $rst = 1;

                // bitacora
                $attr = [];
                $attr['proyecto_edificio_id'] = $idValue;
                $attr['descripcion'] = 
                    'EDITAR|Gestion|ProyectoEdificios|Modal: Actualizar datos';
                $bitacora = (new ProyectoEdificioBitacora())->add($attr);
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $rMsg = Lang::get('main.error_select_db');
                $rst = 0;
                $estado = false;
            }

            return Response::json(
                array(
                    'rst' => $rst,
                    'msj' => $rMsg,
                    'update' => $estado,
                    'bitacora' => $bitacora
                )
            );
            
        }
    }
    
    public function postEliminar()
    {
        $datos = 0;
        $rst = 0;
        $msj = trans('main.error_operacion');
        if (Request::ajax() && Input::has('id')) {
            try{
                $id = Input::get('id');
                $datos = $this->_proyectoEdificioRepo
                    ->deleteSoftProyectoEdificio(
                        $id
                    );
                $rst = 1;
                $msj = trans('main.success_delete') .' #' . $id;

                // bitacora
                $attr = [];
                $attr['proyecto_edificio_id'] = $id;
                $attr['descripcion'] = 
                    'ELIMINAR|Gestion|ProyectoEdificios|Eliminar';
                (new ProyectoEdificioBitacora())->add($attr);
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $msj = Lang::get('main.error_select_db');
                $rst = 0;
            }
        }
        return Response::json(
            array(
                'rst' => $rst,
                'datos' => $datos,
                'msj' => $msj
            )
        );
    }
    
    public function postEliminarImagen()
    {
        $datos = [];
        $datos['msj'] = trans('main.error_operacion');
        $datos['rst'] = 2;
        if (Request::ajax() && Input::has('id')) {
            try{
                $id = Input::get('id');
                $label = Input::get('label');
                $datos = $this->_proyectoEdificioRepo
                    ->deleteProyectoEdificioImagen(
                        $id, $label
                    );
            } catch (Exception $exc) {
                $this->_errorController->saveError($exc);
                $datos['msj'] = Lang::get('main.error_select_db');
                $datos['rst'] = 0;
            }
        }
        return Response::json($datos);
    }
    
    /**
     * Proceso: mantenimiento de edificios
     * @return array
     */
    public function postVer()
    {
        if (Request::ajax() && Input::has('id')) {
            $id = Input::get('id');
            
            // Cuando hace click boton btnItemPadre (modal)
            if (Input::has('padre') && Input::get('padre') != ''
                && Input::has('subitem') && Input::get('subitem') != '') {
                // 'id' aqui es txt_padre
                $idPadre = Input::get('padre') . Input::get('subitem');
                $obj = $this->_proyectoEdificioRepo->existsId($idPadre);
                if ($obj > 0) {
                    return Response::json(
                        array(
                            'rst' => 2,
                            'obj' => $obj,
                            'msj' => "Ya existe un registro con ID: $idPadre."  
                        )
                    );
                }
            }
            if (Input::has('padre') && Input::get('padre') != '') {
                $id = Input::get('padre');
            }
            
            $select = [
                // DB::raw("CAST(id AS UNSIGNED) AS id"),
                // DB::raw(
                //   "IF(id REGEXP '[0-9]$','',SUBSTRING(id, -1)) AS subitem"
                // ),
                'id', 
                'ticket', 'segmento', 'tipo_proyecto',
                'departamento', 'provincia', 'distrito', 'direccion', 'numero', 
                'manzana_tdp', 'lote', 'manzana', 'parent', 'subitem', 
                DB::raw(
                    'UPPER(tipo_via) AS tipo_via, '
                    . 'UPPER(tipo_cchh) AS tipo_cchh, '
                    . 'IF(LENGTH(parent)>0, CONCAT(parent,subitem), id) '
                    . 'AS id_form'
                ), 
                'numero_block', 'numero_piso', 
                'numero_departamentos', 'zonal', 
                'avance', 'cchh', 
                'foto_uno', 'foto_dos', 'foto_tres',
                DB::raw("REPLACE(coord_x, ',', '.') as coord_x"), 
                DB::raw("REPLACE(coord_y, ',', '.') as coord_y"),
                //'coord_x', 'coord_y', 
                'persona_contacto', 'email',
                'observacion', 'pagina_web', 
                'armario', 'mdf_gis AS mdf', 'ura_gis AS ura', 'troba_gis', 
                'troba_bidireccional', 'zona_gis', 'eecc_gis', 
                'created_at', 'updated_at',
                //DB::raw("SUBSTRING(updated_at, 1, 10)  as updated_at"), 
                'tipo_infraestructura', 'fuente_identificacion', 
                'nombre_constructora', 'ruc_constructora', 
                'nombre_proyecto', 
                'zona_competencia',
                'numero_departamentos_habitados', 
                'seguimiento', 'fecha_termino', 
                'proyecto_estado', 'casuistica', 'proyecto_tipo', 
                'estado_provisional',
                'competencia_uno', 'persona_contactodos', 
                'persona_contactodos_cel', 'pagina_web_dos', 
                'persona_contacto_cel', DB::raw("proceso_disenio AS diseño"),
                DB::raw(
                    "IFNULL(usuario_created_at,0) as usuario_created_at, 
                    IFNULL(usuario_updated_at,0) as usuario_updated_at"
                )
            ];
            $datos = $this->_proyectoEdificioRepo->find($id, $select);
            $aUsuario = [];
            if (isset($datos->usuario_updated_at) 
                && $datos->usuario_updated_at != '0') {
                $aUsuario['usuario_updated_at'] 
                    = $datos->usuario_updated_at;
            }
            if (isset($datos->usuario_created_at) 
                && $datos->usuario_created_at != '0') {
                $aUsuario['usuario_created_at'] 
                    = $datos->usuario_created_at;
            }
            $anterior = 0;
            foreach ($aUsuario as $key => $val) {
                $u = Usuario::find($val);
                $datos->{$key} = $u->usuario;
                if ($anterior == $val) {
                    $datos->usuario_updated_at = $u->usuario;    
                    break;
                }
                $anterior = $val;
            }

            return Response::json(
                array(
                    'rst' => (count($datos) == 0? 0 : 1),
                    'datos' => $datos,
                    'msj' => (count($datos) == 0? 'No existe Registro' : '')
                )
            );
        }
    }
    /**
     * Si retorna FALSE no existe ningun registro de otro modo ya existe el 
     * ID
     * @return boolean
     */
    public function postValidarId()
    {
        if (Request::ajax() && Input::has('id')) {
            $sub = Input::has('subitem')? Input::get('subitem') : '';
            $datos = $this->_proyectoEdificioRepo->existsId(
                Input::get('id') . $sub
            );
            return Response::json(
                array(
                    'rst' => 1,
                    'datos' => $datos > 0? false: true,
                )
            );
        }
    }
    
    public function postProcesarImagen()
    {   
        if (Input::has('id') && Input::has('input') && 
                (Input::hasFile('txt_captura_1') || 
                Input::hasFile('txt_captura_2')  || 
                Input::hasFile('txt_captura_3'))) {
            
            $input = Input::get('input');
            $nameS = $input;
            $numero = ($input == '1')? 
                    'foto_uno' : 
                    ($input == '2'? 'foto_dos': 'foto_tres');
            if (Input::hasFile("txt_captura_$input") &&
                Input::file("txt_captura_$input")->isValid()) {
                $file = Input::file("txt_captura_$input");
            } else {
                return Response::json(
                    array(
                        'estado' => '0',
                        'msj' => 'Archivo no valido'
                    )
                );
            }
        } else {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'No se encontro archivo valido'
                )
            );
        }
        set_time_limit(0);
        $id = Input::get('id');
        $name = $id . '_' . $nameS . '.' . $file->getClientOriginalExtension(); 
        $path = 'img/edificio/';// . date('Y/m/');
        $file->move($path, $name);
        //date('Ymd') . time() . '-' . $file->getClientOriginalName();
        $estado = DB::table('proyecto_edificios')
                ->where('id', '=', $id)
                ->update(
                    array(
                        $numero => $name,
                        'updated_at' => Carbon\Carbon::now(),
                        'usuario_updated_at' => Auth::id()
                    )
                );

        // bitacora
        $attr = [];
        $attr['proyecto_edificio_id'] = $id;
        $attr['descripcion'] = 
            'IMAGEN|Gestion|ProyectoEdificios|Modal: Crear imagen';
        (new ProyectoEdificioBitacora())->add($attr);

        return Response::json(
            array(
                'rst' => 1
                , 'msj' => "Accion actual: $estado registrado, "
                . "nombre: $numero $name"
                , 'archivo' => $name
                , 'input' => "txt_captura_$input"
                , 'id' => $id
            )
        );
    }
    
    public function postEnlazarPadre()
    {
        
    }
}
//fin class
