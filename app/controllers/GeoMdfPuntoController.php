<?php
class GeoMdfPuntoController extends \BaseController
{
    public function postListar()
    {

        $r =GeoMdfPunto::postGeoMdfPuntoAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postMdfpunto()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoMdfPunto::postMdfPuntoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postMdfpuntocoord()
    {
        $array['zonal']=Input::get('zonal');
        $array['mdfpunto']=Input::get('mdfpunto');
        $r =GeoMdfPunto::postMdfPuntoCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

     public function postListarcoord()
    {
        $array=explode("-", Input::get('subelemento'));

        $r =GeoMdfPunto::postCoord($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        } 
    }

}

