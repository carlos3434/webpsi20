<?php

class GrupoController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
            $result='';
            if(Input::get('parametros')){
                $first = DB::table('gtm_averias')
                            ->select(
                                    DB::RAW('distinct(grupo) as id'),
                                    'grupo as nombre'
                                    );

                $result=DB::table('coc.grupo')
                                ->select(
                                    DB::RAW('distinct(nom_grupo) as id'),
                                    'nom_grupo as nombre'
                                    )
                                ->union($first)
                                ->get();
            }
        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$result
                )
            );
        }
    }
 


}//fin class
