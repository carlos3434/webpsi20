<?php 

use Legados\models\ComponenteOperacion;
class ComponentesController extends \BaseController
{
    /**
     * 
     * Indicador de Operación IWY(mov_opeiwy): 
     * este valor se obtiene de la solicitud que se le hace al legado (activacion, refresh, etc)
     * Indicador de Procesamiento
     * Código de error interno
     * Código de error envío
     * Observación
     */
    public function getServer()
    {
        //validacion de campos
            #responder error de SOAP
            // result_code =1
        //buscar indicador de operacion de IW
            #responder no se encontro operacion
            // result_code =3

        //result_code => 0 sin errores
        function ejecucion($request)
        {
            Log::useDailyFiles(storage_path().'/logs/componente_ejecucion.log');
            Log::info([$request]);
            $result = [
                'result_code'  =>  1,
                'error_msg'    => 'OK'
            ];
            $envio = ["request" => $request];
            Queue::push('EjecucionComponentes', $envio);

            return $result;
        }
       
        try {
            $server = new SoapServer(Config::get('legado.componentes.wsdl'), array('cache_wsdl' => WSDL_CACHE_NONE));
            $server->addFunction("ejecucion");
            $server->handle();
        } catch (Exception $e) {
            $server->fault($e->getCode(), $e->getMessage());
        }
    }
}