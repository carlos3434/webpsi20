<?php

class EmpresaController extends \BaseController
{
    public function __construct() 
    {
        $this->beforeFilter('auth');
        $this->beforeFilter(
            'csrf_token', ['only' => ['postCrear', 'postEditar']]
        );
    }

    /**
     * Store a newly created resource in storage.
     * POST /empresa/cargar
     *
     * @return Response
     */
    public function postCargar()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $empresas = DB::table('empresas as e')
                    ->select(
                        'e.id', 
                        'e.nombre', 
                        'e.estado', 
                        'e.es_ec',
                        'e.cms',
                        'e.gestel',
                        DB::raw('(select GROUP_CONCAT(bucket_id) from empresa_bucket where empresa_id=e.id) as buckets')
                    )
                    ->get();
            return Response::json(array('rst' => 1, 'datos' => $empresas));
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /empresa/listar
     *
     * @return Response
     */
    public function postListar()
    {
        if (Input::get('usuario') == '1') {
            $empresas = DB::table('empresas as e')
                    ->join(
                        'empresa_usuario as eu', function($join) {
                        $join->on('e.id', '=', 'eu.empresa_id')
                        ->where('eu.usuario_id', '=', Auth::id())
                        ->where('eu.estado', '=', '1');
                        }
                    )
                    ->select(
                        'e.id',DB::raw('IF(IFNULL(e.cms,"")="",e.nombre,CONCAT(e.cms," - ",e.nombre)) as nombre'),'e.es_ec', DB::raw('IFNULL(eu.estado,"disabled") as block')
                    )
                    ->where('e.estado', '=', '1')
                    ->orderBy('e.nombre')
                    ->get();
        } elseif (Input::has('usuario_id')) {
            $usuarioId = Input::get('usuario_id');
            $perfilId = Auth::user()->perfil_id;
            $query = "SELECT e.id, e.nombre, 
                        IFNULL((SELECT estado 
                        FROM empresa_usuario 
                        WHERE usuario_id=? AND estado=1
                        AND empresa_id=e.id
                        GROUP BY empresa_id),0) AS estado
                    FROM empresas e ";
            if ($perfilId == '8') {//super user
                $query.=" WHERE e.estado=1 ORDER BY e.nombre";
                $arrParamEmpresas = array($usuarioId);
            } else { // $usuarioId
                $query.=" JOIN empresa_usuario eu 
                        ON e.id=eu.empresa_id
                        WHERE e.estado=1 AND eu.estado=1 AND eu.usuario_id=?
                        ORDER BY e.nombre";
                $arrParamEmpresas = array($usuarioId, Auth::user()->id);
            } // $usuarioId, $usuarioSesion
            $empresas = DB::select($query, $arrParamEmpresas);
        } elseif (Input::has("parametros") ) {
            
            if(Input::get("parametros")==3 || Input::get("parametros")==4) {
                $empresas = DB::table('empresas as e')
                    ->select(
                        'e.gestel as id', 'e.nombre'
                    )
                    ->where('e.estado', '=', '1')
                    ->where('e.gestel', '<>', '')
                    ->orderBy('e.nombre')
                    ->get();
            } else {
                $empresas = DB::table('empresas as e')
                    ->select(
                        'e.cms as id', 'e.nombre'
                    )
                    ->where('e.estado', '=', '1')
                    ->where('e.cms', '<>', '')
                    ->orderBy('e.nombre')
                    ->get();
            }
        } else {
            if (Input::has("estado")) {
                $empresas = DB::table('empresas as e')
                    ->join(
                        'empresa_usuario as eu', function($join) {
                        $join->on('e.id', '=', 'eu.empresa_id')
                            ->where('eu.usuario_id', '=', Auth::id())
                            ->where('eu.estado', '=', '1');
                        }
                    )
                    ->select('e.id', 'e.nombre')
                    ->where('e.estado', '=', '1')
                    ->orderBy('e.nombre')
                    ->get();

            } else {
                $empresas = DB::table('empresas as e')
                    ->join(
                        'empresa_usuario as eu', function($join) {
                        $join->on('e.id', '=', 'eu.empresa_id')
                            ->where('eu.usuario_id', '=', Auth::id())
                            ->where('eu.estado', '=', '1');
                        }
                    )
                    ->select('e.id', 'e.nombre', 'eu.estado')
                    ->where('e.estado', '=', '1')
                    ->orderBy('e.nombre')
                    ->get();
            }
        }
        return Response::json(
            array(
                    'rst' => 1, 
                    'datos' => $empresas,
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     * POST /empresa/crear
     *
     * @return Response
     */
    public function postCrear()
    {
        //si la peticion es ajax
        // if (Request::ajax()) {
            $regex = 'regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required = 'required';
            $reglas = array(
                'nombre' => $required . '|' . $regex
            );

            $mensaje = array(
                'required' => ':attribute Es requerido',
                'regex' => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            $empresas = new Empresa;
            $empresas['nombre'] = Input::get('nombre');
            $empresas['es_ec'] = Input::get('es_ec');
            $empresas['estado'] = Input::get('estado');
            $empresas['cms'] = Input::get('codcms');
            $empresas['gestel'] = Input::get('codgestel');
            $empresas->save();

            if($empresas){
                $bucket = Input::get('bucket');
                if($bucket){
                    foreach ($bucket as $key => $value) {
                        $empresaBucket = new EmpresaBucket();
                        $empresaBucket->bucket_id = $value;
                        $empresaBucket->empresa_id = $empresas->id;
                        $empresaBucket->save();
                    }                    
                }
            }

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente',
                )
            );
    }

    /**
     * Update the specified resource in storage.
     * POST /empresa/editar
     *
     * @return Response
     */
    public function postEditar()
    {
        // if (Request::ajax()) {
            $regex = 'regex:/^([a-zA-Z .,ñÑÁÉÍÓÚáéíóú]{2,60})$/i';
            $required = 'required';
            $reglas = array(
                'nombre' => $required . '|' . $regex,
            );

            $mensaje = array(
                'required' => ':attribute Es requerido',
                'regex' => ':attribute Solo debe ser Texto',
            );

            $validator = Validator::make(Input::all(), $reglas, $mensaje);

            if ($validator->fails()) {
                return Response::json(
                    array(
                        'rst' => 2,
                        'msj' => $validator->messages(),
                    )
                );
            }

            $empresas = Empresa::find(Input::get('id'));
            $empresas['nombre'] = Input::get('nombre');
            $empresas['es_ec'] = Input::get('es_ec');
            $empresas['estado'] = Input::get('estado');
            $empresas['cms'] = Input::get('codcms');
            $empresas['gestel'] = Input::get('codgestel');
            $empresas->save();

             if($empresas){
                EmpresaBucket::where('empresa_id', '=', $empresas->id)->delete();
                $bucket = Input::get('bucket');
                if($bucket){
                    foreach ($bucket as $key => $value) {
                        $empresaBucket = new EmpresaBucket();
                        $empresaBucket->bucket_id = $value;
                        $empresaBucket->empresa_id = $empresas->id;
                        $empresaBucket->save();
                    }                    
                }
            }

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        // }
    }

    /**
     * Changed the specified resource from storage.
     * POST /empresa/cambiarestado
     *
     * @return Response
     */
    public function postCambiarestado()
    {
        // if (Request::ajax() && Input::has('estado') && Input::has('id')) {
        if (Input::has('estado') && Input::has('id')) {

            $empresa = Empresa::find(Input::get('id'));
            $empresa->estado = Input::get('estado');
            $empresa->save();
            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }

    public function postBuscar()
    {
        $data = DB::table('empresas')
                    ->select('id', 'nombre', 'es_ec', 'estado')
                    ->where('id', '=', Input::get('id'))
                    ->first();

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $data
            )
        );
    }

    public function getDemolistar()
    {
        $query = 'SELECT id, nombre, es_ec, estado FROM empresas';
        $count = 'SELECT COUNT(*) total FROM empresas';

        if (Input::get('nombre')) {
            $query .= ' WHERE nombre LIKE "%'.Input::get('nombre').'%"
                       ORDER BY id ASC
                       LIMIT '.Input::get('start').', 10';

            $count .= ' where nombre like "%'.Input::get('nombre').'%"';
        } else {
            $query .= ' ORDER BY id ASC
                       LIMIT '.Input::get('start').', 10';
        }

        $rdata = DB::select($query);
        $rcount = DB::select($count);

        $datos["total"] = $rcount[0]->total;
        $datos["empresas"] = $rdata;

        return Response::json(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }

}
