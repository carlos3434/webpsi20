<?php
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaGestelAveria;
use Legados\models\SolicitudTecnicaGestelProvision;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\STCmsApi;
use Legados\models\Error;
use Legados\helpers\InboundHelper;

class AseguramientoController extends \BaseController
{
    /**
     * descargar solicitudes tecnica con el estado 0
     */
    public function getDescargar()
    {
        // $estadoAseguramiento = json_decode(Input::get('estado_aseguramiento'), true);
        // $quiebre = json_decode(Input::get('quiebre'), true);
        // $nodo = json_decode(Input::get('nodo'), true);
        
        $filename = Helpers::convert_to_file_excel("PEND_TOA");
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Expires: 0');
        header(
            'Cache-Control: must-revalidate, post-check=0, pre-check=0'
        );
        header("Content-Transfer-Encoding: binary");
        header('Pragma: public');

        //0: esta pendiente en web de asegurammiento
        // $query = Ultimo::getCms()
        Ultimo::getCms()
        ->select(
            'oficina',
            'orden_trabajo',
            'cod_cliente',
            'cms.num_requerimiento',
            'peticion',
            'tipo_requerimiento',
            'telefono1',
            'telefono2',
            'cod_nodo',
            'cod_plano',
            'cod_troba',
            'observacion',
            'fecha_registro_requerimiento',
            'fecha_asignacion',
            'codigo_contrata',
            'hora_asignacion'
        )
        ->where('estado_aseguramiento', Input::get('estado_aseguramiento'))
        ->sinErrorTrama()
        ->chunk(
            200,
            function ($reporte)
            {
                echo "oficina" . "\t";
                echo "orden_trabajo" . "\t";
                echo "cod_cliente" . "\t";
                echo "num_requerimiento" . "\t";
                echo "peticion" . "\t";
                echo "tipo_requerimiento" . "\t";
                echo "telefono1" . "\t";
                echo "telefono2" . "\t";
                echo "cod_nodo" . "\t";
                echo "cod_plano" . "\t";
                echo "cod_troba" . "\t";
                echo "observacion" . "\t";
                echo "fecha_registro_requerimiento" . "\t";
                echo "fecha_asignacion" . "\t";
                echo "codigo_contrata" . "\t";
                echo "hora_asignacion" . "\r\n";
            foreach ($reporte as $data)
            {
                echo $data->oficina . "\t";
                echo $data->orden_trabajo . "\t";
                echo $data->cod_cliente . "\t";
                echo $data->num_requerimiento . "\t";
                echo $data->peticion . "\t";
                echo $data->tipo_requerimiento . "\t";
                echo $data->telefono1 . "\t";
                echo $data->telefono2 . "\t";
                echo $data->cod_nodo . "\t";
                echo $data->cod_plano . "\t";
                echo $data->cod_troba . "\t";
                echo $data->observacion . "\t";
                echo $data->fecha_registro_requerimiento . "\t";
                echo $data->fecha_asignacion . "\t";
                echo $data->codigo_contrata . "\t";
                echo $data->hora_asignacion . "\r\n";
            }
            }
        );
    }

    /**
     * cargar solicitudes tecnicas deberan
     * tener la marca de aseguramiento
     */
    public function postCargar()
    {
        if (Input::hasFile('archivocolumna')) {
            $table = 'pre_temporales_masiva';
            $archivo = 'archivocolumna';
            $consulta = "";
        } else {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'Parametros no valido.'
                )
            );
        }

        if (Input::file($archivo)->isValid()) {
            $file = Input::file($archivo);
            $tmpArchivo = $file->getRealPath();
            Helpers::fileToJsonAddress($tmpArchivo, 0);
        } elseif (!Input::file($archivo)->isValid()) {
            return Response::json(
                array(
                    'estado' => '0',
                    'msj' => 'Archivo no valido.'
                )
            );
        }

        $name = time() . '-' . $file->getClientOriginalName();
        $path = 'uploads/' . date('Y/m/');
        $file->move($path, $name);

        DB::table($table)->truncate();
        sleep(3);
        set_time_limit(0);
        $publicPath = strpos(public_path($path), '\\') === false?
            public_path($path) :
            str_replace('\\', '/', public_path($path));

        try {
            $pdo = DB::connection()->getPdo();
                //"CHARACTER SET 'utf8';" .
                //"ESCAPED BY '' " .
                //"FIELDS TERMINATED BY ',' " .
            $i = $pdo->exec(
                "LOAD DATA LOCAL INFILE '" . $publicPath . $name ."'" .
                " INTO TABLE $table " .
                " CHARACTER SET UTF8" .
                " FIELDS TERMINATED BY ';'" .
                " ENCLOSED BY '\"'" .
                " LINES TERMINATED BY '\n'" .
                $consulta
            );
        } catch (Exception $exc) {
            Helpers::saveError($exc);
            return Response::json(
                array(
                    'rst' => '0',
                    'msj' => 'Error al cargar registros.'
                )
            );
        }

        if (!isset($i) || $i == 0 || is_null($i)) {
            $rst = '0';
            $i = 0;
        } else {
            $rst = '1';
        }

        return Response::json(
            array(
                'rst' => $rst,
                'msj' =>
                    'Se cargaron ' . ($i - 1) . ' registros a la tabla temporal'
            )
        );
        return Response::json(["cargar"]);
    }
    /**
     * actualizar las solicitudes tecnicas segun la carga de archivos
     * y continuar con el proceso de envio de solicitud tecnica
     */
    public function postProcesar()
    {
        
        $aid = $resourceId = $tipoEnvioOfsc = null;
        $solicitudTecnicaId = Input::get('solicitud_tecnica_id');
        $solicitud = SolicitudTecnica::find($solicitudTecnicaId);
        if (count($solicitud)<1) {
            return Response::json([$solicitud]);
        }
        $idSolicitudTecnica = $solicitud['id_solicitud_tecnica'];
        $ultimo = $solicitud->ultimo;
        $movimiento = $solicitud->ultimoMovimiento;


        //return Response::json([$movimiento]);
        
        $tipoLegado = $ultimo['tipo_legado'];
        $actividadTipoId = $ultimo['actividad_tipo_id'];
        $actividadId = $ultimo['actividad_id'];
        $quiebreId = $ultimo['quiebre_id'];

        if ($tipoLegado==1) {
            $solicitudLegado = $solicitud->cms;
        } elseif ($tipoLegado == 2 && $actividadId==1) {
            $solicitudLegado = $solicitud->gestelAveria;
        } elseif ($tipoLegado == 2 && $actividadId==2) {
            $solicitudLegado = $solicitud->gestelProvision;
        }

        if (count($solicitudLegado)<1) {
            return Response::json([$solicitudLegado]);
        }

        $estadoOfsc = 8;
        $idRespuesta=1;//todo bien
        $msjRptaLegado=$codError='';
        $estadoAseguramiento = Input::get('estado_aseguramiento', 1);
        //1:ok , 2: devolver a legados
        if ($estadoAseguramiento==2) {
            $idRespuesta=2;
            //$estadoOfsc = 8;
            $error = Error::find(11);
            $codError = $error->code;
            $msjError = $error->message;
            $msjRptaLegado = $error->code.':'.$error->message;
        } else {

            $arrayvalidacion=array();
            $arrayvalidacion['tipo']=1; //envio legado
            $arrayvalidacion['estado']=1;
            $arrayvalidacion['tabla']=1; //tabla legados
            $arrayvalidacion['proveniencia']='CMS';

            //CAPACIDAD
            $workZone = null;
            if ($solicitudLegado->cod_nodo !='') {
                if ($solicitudLegado->cod_troba  != "") {
                    $workZone = $solicitudLegado->cod_nodo."_".$solicitudLegado->cod_troba;
                } elseif ($solicitudLegado->cod_plano  != "") {
                    $workZone = $solicitudLegado->cod_nodo."_".$solicitudLegado->cod_plano;
                }
            }
            $respuesta = $solicitudLegado->capacityOfsc($quiebreId, $actividadTipoId, $workZone);
            //$estadoOfsc = 8;
            //ENVIO
            if (is_array($respuesta)) {
                $parametros = $respuesta;
                $parametros['tipoEnvio'] = 'sla';
                $parametros['nenvio'] = 1;
                $parametros['fechaAgenda'] = '';
                $parametros['timeSlot'] = '';
                $parametros['tipo_legado'] = 1;
                $parametros['actividad'] = $actividadId;
                
                //\Config::set("ofsc.auth.company", "telefonica-pe.test");
                $response = InboundHelper::envioOrdenOfsc($solicitudLegado, $parametros);
                
                if (isset($response->data->data->commands->command->appointment->aid)) {
                    $resourceId = $respuesta['external_id'];
                    $aid = $response->data->data->commands->command->appointment->aid;
                    $estadoOfsc = 1;
                    $tipoEnvioOfsc = 2;
                    $msjError = "Envio a OFSC: OK";
                } else {
                    if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                        $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
                    }
                    //$estadoOfsc=8;
                    $msjError="Envio a OFSC: FAIL|".$msjErrorOfsc;
                    $codError='ERR22';//go debe reenviar
                }
            } else {
                //$estadoOfsc=8;
                $msjError="Consulta CAPACIDAD a OFSC: FAIL|".$respuesta;
                $codError='ERR12';//go debe reenviar
            }
        }

        $movimiento['aid']                  = $aid;
        $movimiento['estado_ofsc_id']       = $estadoOfsc;
        $movimiento['resource_id']          = $resourceId;
        $movimiento['tipo_envio_ofsc']      = $tipoEnvioOfsc;
        $movimiento['estado_st']            = $idRespuesta;
        $movimiento['estado_aseguramiento'] = $estadoAseguramiento;

        unset($movimiento['id']);
        unset($movimiento['created_at']);
        unset($movimiento['updated_at']);
        unset($movimiento['usuario_created_at']);
        unset($movimiento['usuario_updated_at']);
        
        $movimientoObj= new Movimiento($movimiento['attributes']);
        $solicitud->movimientos()->saveMany([$movimientoObj]);

        $ultimo['aid']               = $aid;
        $ultimo['estado_ofsc_id']    = $estadoOfsc;
        $ultimo['resource_id']       = $resourceId;
        $ultimo['tipo_envio_ofsc']   = $tipoEnvioOfsc;
        $ultimo['estado_st']         = $idRespuesta;
        $ultimo['estado_aseguramiento'] = $estadoAseguramiento;

        $solicitud->ultimo()->save($ultimo);
    

        STLogRecepcion::where('solicitud_tecnica_id', $solicitud->id)
        ->update(
            [
            'code_error'            => $codError,
            'descripcion_error'     => $msjError
            ]
        );
        $respuesta = [
            'id_solicitud_tecnica'  => $idSolicitudTecnica,
            'id_respuesta'          => $idRespuesta,
            'observacion'           => $msjRptaLegado,
            'solicitud_tecnica_id'  => $solicitud->id
        ];

        //Config::set("legado.wsdl.respuestast", Config::get("legado.wsdl.respuestast_test"));

        //enviar respuesta a legados
        $objApiST = new STCmsApi;
        $response =$objApiST->responderST($respuesta);
        return Response::json([$response]);
    }

    public function postEstadosaseguramiento()
    {
        $datos = Cache::get('listEstadosAseguramiento');
        if (!$datos) {
            $datos = Cache::remember(
                'listEstadosAseguramiento',
                24*60*60,
                function () {
                    return DB::table('estados_aseguramiento')
                    ->select('id', 'nombre')
                    ->get();
                }
            );
        }
        return Response::json(array('rst' => 1, 'datos' => $datos));
    }
}
