<?php
class GeoTrobaController extends \BaseController
{
    public function postListar()
    {
       
        $r =GeoTroba::postGeoTrobaAll();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postZonal()
    {
        $array['zonal']=Input::get('zonal');
        $r =GeoTroba::postNodoFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

    public function postNodo()
    {
        $array['zonal']=Input::get('zonal');
        $array['nodo']=Input::get('nodo');
        $r =GeoTroba::postTrobaFiltro($array);

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }
    }

}

