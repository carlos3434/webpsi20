<?php

class TipoproyectoController extends  \BaseController
{

    public function postListar()
    {
        if (Request::ajax()) {
                $segmento = DB::table('tipo_proyectos AS tp')
                            ->select(
                                'tp.nombre as id', 'tp.nombre',
                                DB::raw(
                                    'CONCAT("M", s.nombre) AS relation'
                                )
                            )
                            ->leftJoin(
                                'segmentos AS s',
                                's.id', '=', 'tp.id_segmento'
                            )
                            ->where('tp.estado', '=', '1')
                            ->orderBy('tp.nombre')
                            ->get();
        
            return Response::json(
                array(
                    'rst'=>1,
                    'datos'=>$segmento
                )
            );
        }
    }
 


}//fin class
