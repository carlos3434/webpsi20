<?php
use Legados\models\Tematicos;
use Illuminate\Support\Collection;
use Repositories\CargasRepositoryInterface;
use Repositories\CargasRepository;

class TematicoController extends \BaseController
{

    public function postListar()
    {
        $actividadId = Input::get("actividad_id", "");
        $tipoLegado = Input::get("tipo_legado", "");
        $tipoDevolucion = Input::get("tipodevolucion", "");
        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];

        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;

        $tematicos = Tematicos::select(
            "tematicos.*",
            DB::raw("(IF(`tematicos`.parent = 0, 'Sin Padre', 
            CONCAT('|-- ', tp.nombre, ' |-- ',
                IFNULL(
                    (SELECT CONCAT(IFNULL(tp1.nombre, 'Sin Padre'),
                     ' |-- ', IFNULL(tp2.nombre, 'Sin Padre')
                     ) 
                    FROM tematicos AS tp1 
                    LEFT JOIN tematicos AS tp2 ON tp2.id = tp1.parent
                    WHERE tp1.id = tp.parent ), '')
                    ) 
                ) 
            ) AS padre"),
            DB::raw("IF(tematicos.tipo_legado = 1, 'CMS', 'Gestel') AS legado"),
            DB::raw("IF(tematicos.tipo ='C', 'Comercial', 'Tecnico') AS tipodevolucion"),
            DB::raw("IF(tematicos.estado = 1, 'Activo', 'Inactivo') AS estadotematico"),
            "a.nombre AS actividad"
        )
        ->leftJoin("tematicos as tp", "tp.id", "=", "tematicos.parent")
        ->leftJoin("actividades as a", "a.id", "=", "tematicos.actividad_id");

        $isactivo = Input::get("isactivo", "is_pendiente");
        $tematicos->whereRaw("tematicos.".$isactivo."= 1")
            ->whereRaw("tematicos.parent = 0")
            ->whereRaw("tematicos.deleted_at IS NULL");

        $search = Input::get("busqueda", "");
        if ($search!="") {
            /*$tematicos->whereRaw("(tematicos.nombre LIKE '%$search%' OR a.nombre LIKE '%$search%' )");*/
            $tematicos->whereRaw("tematicos.id in ($search)");

        }
        if ($actividadId!="") {
            $tematicos->whereRaw("tematicos.actividad_id = $actividadId");
        }
        if ($tipoLegado!="") {
            $tematicos->whereRaw("tematicos.tipo_legado = $tipoLegado");
        }
        if ($tipoDevolucion!="") {
            $tematicos->whereRaw("tematicos.tipo = '$tipoDevolucion' ");
        }

        $column = "id";
        $dir = "desc";
        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }

        $tematicos = $tematicos
                    ->mergeBindings($tematicos->getQuery())
                    ->orderBy($column, $dir)
                    ->paginate($grilla["perPage"]);
        $data = $tematicos->toArray();
        $col = new Collection([
            'recordsTotal'=>$tematicos->getTotal(),
            'recordsFiltered'=>$tematicos->getTotal(),
        ]);
        return $col->merge($data);
    }

    public function getEditar()
    {
        $id = Input::get("id", 0);
        $tematico = Tematicos::find($id);
        if (!is_null($tematico)) {
            return  Response::json(["rst" => 1, "obj" => $tematico]);
        } else {
            return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
    }

    public function getCambiarestado()
    {
        $id = Input::get("id", 0);
        $estado = Input::get("estado");
        $tematico = Tematicos::find($id);
        if (!is_null($tematico)) {
            if ($estado == 0) {
                $tematico->estado = 1;
            } else {
                $tematico->estado = 0;
            }
            $tematico->save();
            Cache::forget("listTematicosLegadoPadres");
            Cache::forget("listTematicosLegado");
            return  Response::json(["rst" => 1, "obj" => $tematico]);
        } else {
            return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
    }

    public function postGuardar()
    {
        $objvalidar = [
            "nombre" => Input::get("nombre", ""),
            "parent" => (Input::get("parent") == "") ? 0 : Input::get("parent"),
            "tipo_legado" => Input::get("tipo_legado", 0),
            "actividad_id" => Input::get("actividad_id", 0)
        ];
        $validator = Validator::make($objvalidar, Tematicos::$rules);
        if ($validator->fails()) {
            $msjError = $validator->messages()->all()[0];
            return  Response::json(["rst" => 2, "msj" => $msjError]);
        }

        $obj = [
            "id" => Input::get("id"),
            "nombre" => Input::get("nombre"),
            "parent" => (Input::get("parent") == "") ? 0 : Input::get("parent"),
            "tipo_legado" => Input::get("tipo_legado"),
            "actividad_id" => Input::get("actividad_id"),
            "tipo" => Input::get("tipo"),
            "descripcion" => Input::get("descripcion")
        ];
        $obj["tipo"] = ($obj["actividad_id"] == 2)? $obj["tipo"] : null;
        $opciones = Input::get("opciones");
        if (is_null($opciones)) {
            return  Response::json(["rst" => 2, "msj" => "Debe elegir por lo menos un estado a los que aplica el tematico"]);
        }
        $obj["is_pendiente"] = (in_array("is_pendiente", $opciones)) ? 1 : 0;
        $obj["is_no_realizado"] = (in_array("is_no_realizado", $opciones)) ? 1 : 0;
        $obj["is_completado"] = (in_array("is_completado", $opciones)) ? 1 : 0;
        $obj["is_pre_devuelto"] = (in_array("is_pre_devuelto", $opciones)) ? 1 : 0;
        $obj["is_pre_liquidado"] = (in_array("is_pre_liquidado", $opciones)) ? 1 : 0;

        if (($obj["is_no_realizado"] == 1 || $obj["is_pre_devuelto"] == 1) && $obj["actividad_id"] == 2) {
            if (is_null($obj["tipo"])) {
                return  Response::json(["rst" => 2, "msj" => "Es Necesario el Tipo de Devolucion a la que aplica"]);
            }
        }
        try {
            if ($obj["id"] > 0) {
                $tematico = Tematicos::find($obj["id"]);
                $tematico->update($obj);
            } else {
                $tematico = Tematicos::create($obj);
            }
            Cache::forget("listTematicosLegadoPadres");
            Cache::forget("listTematicosLegado");
            return Response::json(["rst" => 1, "msj" => "Tematico Guardado con Exito", "obj" => $tematico]);
        } catch (Exception $e) {
            return Response::json(["rst" => 2, "msj" => "Error al Guardar"]);
        }
    }

    public function postAllparent()
    {
        $padres = Cache::get('listTematicosLegadoPadres');
        if (!$padres) {
            $padres = Cache::remember(
                'listTematicosLegadoPadres',
                24*60*60,
                function () {
                    return Tematicos::select(
                        "tematicos.*",
                        "a.nombre as actividad",
                        DB::raw("IFNULL((SELECT CONCAT(IFNULL(t.nombre, ''), '|', IFNULL(t2.nombre, '')) FROM tematicos AS t 
                            LEFT JOIN tematicos AS t2 ON t2.id = t.parent
                            WHERE t.id = tematicos.parent
                            ), '') AS padre")
                    )
                    ->whereRaw(
                        "((SELECT COUNT(*) FROM tematicos AS t WHERE t.parent = tematicos.id) >= 0
                        OR tematicos.parent = 0) AND tematicos.estado = 1"
                    )->leftJoin("actividades as a", "a.id", "=", "tematicos.actividad_id")
                    ->get();
                }
            );
        }
        return Response::json($padres);
    }

    public function postEliminar()
    {
        $id = Input::get("id", 0);
        $tematico = Tematicos::find($id);
        if (!is_null($tematico)) {
            $tematico->usuario_deleted_at = Auth::id();
            $tematico->save();
            $tematico->delete();
            Cache::forget("listTematicosLegadoPadres");
            Cache::forget("listTematicosLegado");
            return Response::json(["rst" => 1, "msj" => "Tematico Eliminado con Exito"]);
        } else {
            return Response::json(["rst" => 2, "msj" => "Error al Eliminar"]);
        }
    }

    public function postTematicoshijos()
    {
        $id = Input::get("id", 0);
        
        $tematicos = Tematicos::select(
            "tematicos.*",
            DB::raw("(IF(`tematicos`.parent = 0, 'Sin Padre', 
            CONCAT('|-- ', tp.nombre, ' |-- ',
                IFNULL(
                    (SELECT CONCAT(IFNULL(tp1.nombre, 'Sin Padre'),
                     ' |-- ', IFNULL(tp2.nombre, 'Sin Padre')
                     ) 
                    FROM tematicos AS tp1 
                    LEFT JOIN tematicos AS tp2 ON tp2.id = tp1.parent
                    WHERE tp1.id = tp.parent ), '')
                    ) 
                ) 
            ) AS padre"),
            DB::raw("IF(tematicos.tipo_legado = 1, 'CMS', 'Gestel') AS legado"),
            DB::raw("IF(tematicos.tipo ='C', 'Comercial', 'Tecnico') AS tipodevolucion"),
            DB::raw("IF(tematicos.estado = 1, 'Activo', 'Inactivo') AS estadotematico"),
            "a.nombre AS actividad"
        )
        ->leftJoin("tematicos as tp", "tp.id", "=", "tematicos.parent")
        ->leftJoin("actividades as a", "a.id", "=", "tematicos.actividad_id")
        ->where("tematicos.parent",$id)
        ->get();

        return Response::json(array('estado'=>1,'data'=>$tematicos));

    }

   public function postTitleparents()
    {
        $padres = Input::get("padres", 0);
        $id=explode(',', $padres);
        for ($i=0; $i <count($id) ; $i++) { 
            if( $id[$i] == '' ){
                unset($id[$i]);
            }
        }

        $ids=implode(',', $id);
        $tematicos = Tematicos::select(
            DB::raw("nombre")
        )
        ->whereRaw("id in (".$ids.")")
        ->get();

        return Response::json(array('nombres'=>$tematicos));
    }

    public function postUploadexcel() 
    {   
        $file = \Input::file('excel'); 
        $name = $file->getClientOriginalName();

        if( substr($name,-4)!='.csv' ){
            $data=[];
            $cabeceras=[];
            $rst=0;
            $msj="<b><i style='color:#DE3B19;'>Archivo seleccionado no esta permitido! (.csv)</i></b>";
        }else{
            $array=[];
            $columns=13;
            $obj= new CargasRepository;
            $array = $obj->getdataUpload_csv();

            $cabeceras=$array['cabecera'];
            $data=$array['data'];
            $array_p=[];$array_p2=[];
            $count=0;$cont_2=0;$campos="";$msj="";$rst=1;

            if (! in_array("tipo legado", $cabeceras)) {
                $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 1 / Tipo Legado, no esta definido en el Excel.</i></b><br>";
                $rst=0;
            }

            if (! in_array("actividad", $cabeceras)) {
                $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 2 / Actividad, no esta definido en el Excel.</i></b><br>";
                $rst=0;
            }

            if (! in_array("motivo ofsc", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 3 / Motivo ofsc, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("tipo devolucion", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 4 / Tipo devolucion, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("tematico 1", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 5 / Tematico 1, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("tematico 2", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 6 / Tematico 2, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("tematico 3", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 7 / Tematico 3, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("tematico 4", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 8 / tematico 4, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("pendiente", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 9 / Pendiente, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("pre devuelto", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 9 / Pre devuelto, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("no realizado", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 10 / No realizado, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("pre liquidado", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 11 / Pre liquidado, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if (! in_array("completado", $cabeceras)) {
                    $campos.="<b><i style='color:#DE3B19;font-size:14px;font-style: oblique;'>► Campo 12 / Completado, no esta definido en el Excel.</i></b><br>";
                    $rst=0;
            }

            if( $rst == 0 ){
                $msj='Campo(s) : <br>'.$campos;
                $data=[];
            }else{  
                    $concat='';
                    $cont=0;$cont_2=0;
                    $array_p=[];$array_p2=[];

                    foreach ($data as $key => $value) {
                        if(strlen($value['tipo_legado'])==0 and strlen($value['actividad'])==0 and 
                           strlen($value['motivo_ofsc'])==0 and strlen($value['tipo_devolucion'])== 0 and
                           strlen($value['tematico_1'])==0 and strlen($value['tematico_2'])==0 and 
                           strlen($value['tematico_3'])==0 and strlen($value['tematico_4'])==0 and 
                           strlen($value['pendiente'])==0 and strlen($value['pre_devuelto'])==0 and 
                           strlen($value['no_realizado'])==0 and strlen($value['pre_liquidado'])==0 and 
                           strlen($value['completado'])==0){
                            unset($data[$key]);
                        } 
                    }

                    foreach ($data as $key => $value) {
                        $cont_mot=0;
                        $motivo_ofsc=str_replace(' ', '',$value['motivo_ofsc']);
                        $motivos=\DB::table('motivos_ofsc')
                                ->select(
                                \DB::raw('UPPER(descripcion) as motivo_ofsc'),
                                \DB::raw('IF(tipo_legado=1,"CMS",IF(tipo_legado=2,"GESTEL","")) as tipo_legado'),
                                \DB::raw('IF(actividad_id=1,"AVERIA",IF(actividad_id=2,"PROVISION","")) as actividad'))
                                ->where('estado','=','1')
                                ->get();
                        $motivos=json_decode(json_encode($motivos),true);
                        foreach ($motivos as $idx => $val) { 
                            if(  $value['tipo_legado']==$val['tipo_legado'] and $value['actividad']==$val['actividad'] and $value['motivo_ofsc']==trim($val['motivo_ofsc']) ){
                                $cont_mot++;
                            }
                         }

                        //validar tipo legado
                        if( $value['tipo_legado']!='CMS' and $value['tipo_legado']!='GESTEL' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Tipo legado) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "CMS ó GESTEL!"</i></b>';
                        }

                        //validar actividad
                        elseif( $value['actividad']!='AVERIA' and $value['actividad']!='PROVISION' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Actividad) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "AVERIA ó PROVISION!"</i></b>';
                        }

                        //motivo ofsc
                        elseif( $value['motivo_ofsc']!='' and $cont_mot==0 ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Motivo ofsc) :</i><i style="font-size:14px;
                            font-style: oblique;"> Motivo ofsc no existe!"</i></b>';
                        }

                        //validar tipo devolucion(AVERIA)
                        elseif( $value['actividad']=='AVERIA' and $value['tipo_devolucion']!='' and 
                                $value['tipo_devolucion']!='TECNICO' and $value['tipo_devolucion']!='COMERCIAL'){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Tipo devolucion) :</i><i style="font-size:14px;
                            font-style: oblique;"> Si actividad es "AVERIA", solo se permite "TECNICO ó COMERCIAL!"</i></b>';
                        }

                        //validar tipo devolucion(PROVISION)
                        elseif( $value['actividad']=='PROVISION' and $value['tipo_devolucion']!='TECNICO' and 
                                $value['tipo_devolucion']!='COMERCIAL'){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Tipo devolucion) :</i><i style="font-size:14px;
                            font-style: oblique;"> Si actividad es "PROVISION", solo se permite "TECNICO ó COMERCIAL!"</i></b>';
                        }

                        //validar estados
                        elseif( $value['pendiente']!='SI' and $value['pendiente']!='NO' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Pendiente) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "SI ó NO"!</i></b>';
                        }

                        elseif( $value['pre_devuelto']!='SI' and $value['pre_devuelto']!='NO' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Pre devuelto) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "SI ó NO"!</i></b>';
                        }

                        elseif( $value['no_realizado']!='SI' and $value['no_realizado']!='NO' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:No realizado) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "SI ó NO"!</i></b>';
                        }

                        elseif( $value['pre_liquidado']!='SI' and $value['pre_liquidado']!='NO' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Pre liquidado) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "SI ó NO"!</i></b>';
                        }

                        elseif( $value['completado']!='SI' and $value['completado']!='NO' ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).' / Columna:Completado) :</i><i style="font-size:14px;
                            font-style: oblique;"> Solo se permite "SI ó NO"!</i></b>';
                        }

                        //validar pendientes
                        elseif( $value['pendiente']=='SI' and ( $value['pre_devuelto']=='SI' or 
                            $value['no_realizado']=='SI' or $value['pre_liquidado']=='SI' or 
                            $value['completado']=='SI') ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).') :</i><i style="font-size:14px;
                            font-style: oblique;"> Si estado seleccionado es "Pendiente", no puede seleccionar otros estados!</i></b>';
                        }

                        //validar pre_devuelto,no_realizado,pre_liquidado,completado
                        elseif( ( $value['pre_devuelto']=='SI' or $value['no_realizado']=='SI') and 
                            ( $value['pre_liquidado']=='SI' or $value['completado']=='SI') ){
                            $cont++;
                            $concat.="\n".'<b><i style="color:#DE3B19;font-size:14px;font-style: oblique;">► (Fila:'.($key+1).') :</i><i style="font-size:14px;
                            font-style: oblique;"> Si estado seleccionado es "Pre devuelto/No realizado", no puede seleccionar "Pre liquidado/Completado"!</i></b>';
                        }

                        array_push($array_p,($value['tipo_legado'].$value['actividad'].$value['tipo_devolucion'].$value['tematico_1'].$value['tematico_2'].$value['tematico_3'].$value['tematico_4']));
                    }

                    //validar tipo legado, actividad, tematicos
                    $str=implode(",",$array_p);
                    foreach ($data as $key => $value) {
                        $count = substr_count($str,($value['tipo_legado'].$value['actividad'].$value['tipo_devolucion'].$value['tematico_1'].$value['tematico_2'].$value['tematico_3'].$value['tematico_4']));
                        if($count>1){
                            array_push($array_p2,($value['tipo_legado'].','.$value['actividad'].','.
                            $value['tipo_devolucion'].','.$value['tematico_1'].','.$value['tematico_2'].','.
                            $value['tematico_3'].','.$value['tematico_4']));
                            $cont_2++;
                        }
                    }

                    if($cont>0){
                        $rst=2;
                        $msj="Validar excel:".$concat;
                    }else{
                        if($cont_2>0){
                            $array_p2=array_unique($array_p2);
                            $concat2='';
                            foreach ($array_p2 as $key => $value) {
                                $campos=explode(',', $value);
                                $concat2.="\n".'<b style="font-size:14px;font-style: oblique;">► Repetido: <b/>';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">TIP.LEGADO: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[0].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    ACTIVIDAD: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[1].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    TIP.DEVOLUCION: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[2].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    TEMATICO 1: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[3].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    TEMATICO 2: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[4].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    TEMATICO 3: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[5].'</b>;';
                                $concat2.='<b style="color:#DE3B19;font-size:14px;font-style: oblique;">    TEMATICO 4: </b> <b style="font-size:14px;font-style: oblique;">'.
                                    $campos[6].'</b>';
                            }
                            $rst=3;
                            $msj="Validar excel:".$concat2;
                        }
                    }
                } 
        }
        
        return \Response::json(
            array(
                'rst' => $rst,
                'msj' => $msj,
                'data' => $data,
                'cabeceras'=>$cabeceras
            )
        );
    }

    public function postCargamasiva()
    {   
        $cont=0;$estado=0;
        $msj='';

        $idusuario = Auth::user()->id;
        date_default_timezone_set('America/Lima');
        $fecha = date("Y-m-d G:i:s");
        $tipo_legado=Input::get('tipo_legado');
        $actividad=Input::get('actividad');
        $motivos_ofsc=Input::get('motivos_ofsc');
        $tipo_devolucion=Input::get('tipo_devolucion');
        $tematico1=Input::get('tematico1');
        $tematico2=Input::get('tematico2');
        $tematico3=Input::get('tematico3');
        $tematico4=Input::get('tematico4');
        $pendiente=Input::get('pendiente');
        $pre_devuelto=Input::get('pre_devuelto');
        $no_realizado=Input::get('no_realizado');
        $pre_liquidado=Input::get('pre_liquidado');
        $completado=Input::get('completado');

        try {
            DB::beginTransaction();
            for ($i=0; $i < count($tipo_legado); $i++) {
                
                $id_p1=$this->insertaTematico($tipo_legado[$i],$actividad[$i],$motivos_ofsc[$i],
                $tipo_devolucion[$i],$tematico1[$i],0,$pendiente[$i],$pre_devuelto[$i],
                $no_realizado[$i],$pre_liquidado[$i],$completado[$i],$idusuario);

                $id_p2=$this->insertaTematico($tipo_legado[$i],$actividad[$i],$motivos_ofsc[$i],
                $tipo_devolucion[$i],$tematico2[$i],$id_p1,$pendiente[$i],$pre_devuelto[$i],
                $no_realizado[$i],$pre_liquidado[$i],$completado[$i],$idusuario);

                $id_p3=$this->insertaTematico($tipo_legado[$i],$actividad[$i],$motivos_ofsc[$i],
                $tipo_devolucion[$i],$tematico3[$i],$id_p2,$pendiente[$i],$pre_devuelto[$i],
                $no_realizado[$i],$pre_liquidado[$i],$completado[$i],$idusuario);

                $id_p4=$this->insertaTematico($tipo_legado[$i],$actividad[$i],$motivos_ofsc[$i],
                $tipo_devolucion[$i],$tematico4[$i],$id_p3,$pendiente[$i],$pre_devuelto[$i],
                $no_realizado[$i],$pre_liquidado[$i],$completado[$i],$idusuario);
                
                if( $id_p1==0 || $id_p2==0 || $id_p3==0 || $id_p4==0 ){
                    $cont++;
                }
            }

            DB::commit();

            if( $cont==0 ){
                $estado=1;
                $msj="Carga Masiva realizada exitosamente!";
                Cache::forget('listTematicosLegadoPadres');
                Cache::forget("listTematicosLegado");
            }else{
                $estado=0;
                $msj="Error en Carga Masiva!";
                DB::rollback(); 
            }
        }catch(\Illuminate\Database\QueryException $e){

            $errorController = new ErrorController();
            $errorController->saveError($e);
            $estado=0;
            $msj="Error en Carga Masiva!";
            DB::rollback();

        }

        return \Response::json(
            array(
                "rst" => $estado,
                "msj" => $msj
            )
        );
    }

    public function insertaTematico($tipo_legado,$actividad,$motivos_ofsc,$tipo_devolucion,
    $tematico,$padre,$pendiente,$pre_devuelto,$no_realizado,$pre_liquidado,$completado,$idusuario){
        $cont=0;$id=0;
        $ids='';

        $listTem=DB::table('tematicos')
                   ->select('id','parent',
                        DB::raw('UPPER(nombre) as nombre'),
                        DB::raw('IF(tipo_legado=1,"CMS",IF(tipo_legado=2,"GESTEL","")) as tipo_legado'),
                        DB::raw('IF(actividad_id=1,"AVERIA",IF(actividad_id=2,"PROVISION","")) as actividad'),
                        DB::raw('IF(tipo="T","TECNICO",IF(tipo="C","COMERCIAL","")) as tipo_devolucion')
                    )
                   ->where('estado','=','1')
                   ->get();
        $listTem=json_decode(json_encode($listTem),true);

        for ($j=0; $j < count($listTem); $j++) { 
            if( $tipo_legado==$listTem[$j]['tipo_legado'] and $actividad==$listTem[$j]['actividad'] and 
                $tipo_devolucion==$listTem[$j]['tipo_devolucion'] and $tematico==$listTem[$j]['nombre'] and 
                $listTem[$j]['parent']==$padre ){
                    $cont++;
                    $ids.=$listTem[$j]['id'].',';
            }
        }

        if($cont>0){
            $array=explode(',', $ids);
            $id=$array[0];
        }else{
            //tipo legado
            $tipolegado_id=null;
            if( $tipo_legado=='CMS' ){
                $tipolegado_id=1;
            }elseif( $tipo_legado=='GESTEL' ){
                $tipolegado_id=2;
            }

            //actividad
            $actividad_id=null;
            if( $actividad=='AVERIA' ){
                $actividad_id=1;
            }elseif( $actividad=='PROVISION' ){
                $actividad_id=2;
            }

            //tipo devolucion
            $tipo=null;
            if( $tipo_devolucion=='TECNICO' ){
                $tipo='T';
            }elseif( $tipo_devolucion=='COMERCIAL' ){
                $tipo='C';
            }

            //motivos ofsc
            $motivo_ofsc_id=null;
            if($motivos_ofsc!=''){
                $motivo=DB::table('motivos_ofsc')
                    ->select('id')
                    ->where('tipo_legado','=',$tipolegado_id)
                    ->where('actividad_id','=',$actividad_id)
                    ->where('descripcion','=',$motivos_ofsc)
                    ->first();
                $motivo_ofsc_id=$motivo->id;
            }
  
            //pendiente
            $is_pendiente=null;
            if( $pendiente=='SI' ){
                $is_pendiente=1;
            }elseif( $pendiente=='NO' ){
                $is_pendiente=0;
            }

            //pre_devuelto
            $is_predevuelto=null;
            if( $pre_devuelto=='SI' ){
                $is_predevuelto=1;
            }elseif( $pre_devuelto=='NO' ){
                $is_predevuelto=0;
            }

            //no_realizado
            $is_norealizado=null;
            if( $no_realizado=='SI' ){
                $is_norealizado=1;
            }elseif( $no_realizado=='NO' ){
                $is_norealizado=0;
            }

            //pre_liquidado
            $is_preliquidado=null;
            if( $pre_liquidado=='SI' ){
                $is_preliquidado=1;
            }elseif( $pre_liquidado=='NO' ){
                $is_preliquidado=0;
            }

            //pre_liquidado
            $is_completado=null;
            if( $completado=='SI' ){
                $is_completado=1;
            }elseif( $completado=='NO' ){
                $is_completado=0;
            }

            $id=DB::table("tematicos")->insertGetId([
                "tipo_legado" => $tipolegado_id,
                "actividad_id" => $actividad_id,
                "motivo_ofsc_id" => $motivo_ofsc_id,
                "tipo" => $tipo,
                "nombre" => $tematico,
                "parent" => $padre,
                "is_pendiente" => $is_pendiente,
                "is_pre_devuelto" => $is_predevuelto,
                "is_no_realizado" => $is_norealizado,
                "is_pre_liquidado" => $is_preliquidado,
                "is_completado" => $is_completado,
                "descripcion" => null,
                "estado" => 1,
                "usuario_created_at" => $idusuario
            ]);
        }

        return $id;
    }

    public function postBuscartematicos(){

        $id=0;$idparent=0;$idtematicos=[];

        $search=Input::get('search','');
        $isactivo = Input::get("isactivo", "is_pendiente");

        $tematico=DB::table('tematicos as t')
                        ->whereRaw("t.".$isactivo."= 1")
                        ->whereRaw("t.deleted_at IS NULL");
             

        if($search!=''){
        $tematico->whereRaw("REPLACE(UPPER(t.nombre),' ','')=REPLACE(UPPER('$search'),' ','')");
        }

        $data=$tematico->get();
        for ($i=0; $i < count($data) ; $i++) { 
            
            $idparent=$data[$i]->parent;
            $id=$data[$i]->id;
          
            if($idparent!=0){
                    do {    
                        $tematico2=DB::table('tematicos')
                                    ->whereRaw("id=$idparent")
                                    ->get();

                        if(count($tematico2)>0){
                            $idparent=$tematico2[0]->parent;
                            $id=$tematico2[0]->id;
                        }else{
                            $idparent=0;
                        }
                    } while ($idparent != 0);
                array_push($idtematicos, $id);
            }else{
                array_push($idtematicos, $id); 
            }

        }

        $idtematicos = array_unique($idtematicos);
        $id_tematicos=implode(',', $idtematicos);

        return $id_tematicos;
    }
}

