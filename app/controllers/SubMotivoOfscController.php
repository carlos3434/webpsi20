<?php
class SubMotivoOfscController extends \BaseController
{
    public function postListar(){
         $datos = SubmotivoOfsc::select(
                    'submotivos_ofsc.id',
                    DB::raw('submotivos_ofsc.descripcion as nombre') 
                )->get();
        return [
            'rst' => 1,
            'datos' => $datos
        ];
    }

    public function postList()
    {
        $datos = SubmotivoOfsc::select(
                    'submotivos_ofsc.id',
                    'submotivos_ofsc.codigo_ofsc',
                    'submotivos_ofsc.codigo_legado',
                    'submotivos_ofsc.descripcion',
                    'submotivos_ofsc.estado',
                    'mo.descripcion as descripcion_motivo',
                    'motivo_ofsc_id'
                )
                ->leftJoin(
                    'motivos_ofsc as mo', 'mo.id', '=', 'motivo_ofsc_id'
                )
                ->get();
        return [
            'datos' => $datos
        ];
    }

    public function postInsert()
    {
        $motivoOfsc = new SubmotivoOfsc();
        $motivoOfsc->codigo_ofsc = Input::get('codigo_ofsc');
        $motivoOfsc->codigo_legado = Input::get('codigo_legado');
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->motivo_ofsc_id = Input::get('motivo_ofsc_id');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }

    public function postUpdate()
    {
        $motivoOfsc = SubmotivoOfsc::find(Input::get('id'));
        $motivoOfsc->codigo_ofsc = Input::get('codigo_ofsc');
        $motivoOfsc->codigo_legado = Input::get('codigo_legado');
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->motivo_ofsc_id = Input::get('motivo_ofsc_id');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }

    public function postExportar() // Esportar MotivoOfsc a excel
    {
        $SubmotivoOfsc = new SubmotivoOfsc();

        $listSubmotivo = $SubmotivoOfsc->getSubMotivoOfsc();

        $listSubmotivo = json_decode(json_encode($listSubmotivo), true);

        return \Helpers::exportArrayToExcel($listSubmotivo, "submotivoOfsc");
    }
}

