<?php
class MotivosofscController extends \BaseController
{
    public function postListar()
    {

        $r =MotivoOfsc::postMotivoTipo();

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }

    public function postCargar(){
         $datos = MotivoOfsc::select(
                    'motivos_ofsc.id',
                    DB::raw('motivos_ofsc.descripcion as nombre') 
                )->get();
        return [
            'rst' => 1,
            'datos' => $datos
        ];
    }

    public function postList()
    {
        $actividad = Input::get('actividad');
        $estadoOfsc = Input::get('estadoOfsc');
        $tipoLegado = Input::get('tipoLegado');
        $tipoOfsc = Input::get('tipoOfsc');
        $tipoTratamiento = Input::get('tipoTratamiento');
        
        $query = MotivoOfsc::select(
                    'motivos_ofsc.id',
                    'codigo_ofsc',
                    'codigo_legado',
                    'descripcion',
                    'motivos_ofsc.estado',
                    'actividad_id',
                    'a.nombre as actividad',
                    'estado_ofsc',
                    'tipo_legado',
                    'tipo_tecnologia',
                    'estado_ofsc_id',
                    'eo.nombre as nombre_ofsc',
                    DB::raw('if(tipo_tratamiento_legado = 1, "AUTOMATICO", "SIGUE FLUJO") as tipo_tratamiento_legado_descripcion'),
                    'tipo_tratamiento_legado'
                )
                ->leftJoin(
                    'actividades as a', 'a.id', '=', 'actividad_id'
                )
                ->leftJoin(
                    'estados_ofsc as eo', 'eo.id', '=', 'estado_ofsc_id'
                );

        if ($actividad) {
            $query->whereIn('actividad_id', $actividad);
        }

        if ($estadoOfsc) {
            $query->whereIn('estado_ofsc_id', $estadoOfsc);
        }

        if ($tipoLegado) {
            $query->whereIn('tipo_legado', $tipoLegado);
        }

        if ($tipoOfsc) {
            $query->whereIn('estado_ofsc', $tipoOfsc);
        }

        if ($tipoTratamiento) {
            $query->whereIn('tipo_tratamiento_legado', $tipoTratamiento);
        }
                
        $datos = $query->get();
        return [
            'datos' => $datos
        ];
    }

    public function postInsert()
    {
        $motivoOfsc = new MotivoOfsc();
        $motivoOfsc->codigo_ofsc = Input::get('codigo_ofsc');
        $motivoOfsc->codigo_legado = Input::get('codigo_legado');
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->estado_ofsc = Input::get('estado_ofsc');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_tecnologia = Input::get('tipo_tecnologia');
        $motivoOfsc->estado_ofsc_id = Input::get('estado_ofsc_id');
        $motivoOfsc->tipo_tratamiento_legado = Input::get('tipo_tratamiento_legado');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        Cache::forget('listMotivosDevolucionLegado');
        Cache::forget('listMotivosliquidacionLegado');
        return [
            'rst' => 1
        ];
    }

    public function postUpdate()
    {
        $motivoOfsc = MotivoOfsc::find(Input::get('id'));
        $motivoOfsc->codigo_ofsc = Input::get('codigo_ofsc');
        $motivoOfsc->codigo_legado = Input::get('codigo_legado');
        $motivoOfsc->descripcion = Input::get('descripcion');
        $motivoOfsc->estado_ofsc = Input::get('estado_ofsc');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_tecnologia = Input::get('tipo_tecnologia');
        $motivoOfsc->estado_ofsc_id = Input::get('estado_ofsc_id');
        $motivoOfsc->tipo_tratamiento_legado = Input::get('tipo_tratamiento_legado');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        Cache::forget('listMotivosDevolucionLegado');
        Cache::forget('listMotivosliquidacionLegado');
        return [
            'rst' => 1
        ];
    }

    public function postExportar() // Esportar MotivoOfsc a excel
    {
        $motivos = new MotivoOfsc();
        $listMotivos = $motivos->getMotivoOfsc();
        $listMotivos = json_decode(json_encode($listMotivos), true);
        return \Helpers::exportArrayToExcel($listMotivos, "MotivoOfsc");
    }

}

