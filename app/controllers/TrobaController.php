<?php
class TrobaController extends \BaseController
{
    public function postListar()
    {
        $r=array();
        if ( Input::has('usuario') ) {
            $r =Geofftt::getTrobaUsu();
        }elseif ( Input::get('parametros') || Input::get('estado')) {
            
            $r = DB::table('geo_trobapunto')
                       ->select(
                           DB::RAW('troba as id'),
                           //DB::RAW('CONCAT(group_concat(nodo SEPARATOR ","), "->", troba) as nombre'),
                           DB::RAW('troba as nombre'),
                           //DB::raw('CONCAT("N",nodo) as relation')
                           DB::raw('GROUP_CONCAT(DISTINCT CONCAT("N",nodo) SEPARATOR "|,|") as relation')
                           )
                       ->orderBy('nodo');
                  if(Input::get('valor')){
                    $r->WhereIn('nodo',Input::get('valor'));
                  }
                  $r->groupBy('troba');
                  $r=$r->get();
        }  else {
            $r =Geofftt::getTroba(array());
        }

        if ( Request::ajax() ) {
            return Response::json(
                array(
                    'rst' => 1, 
                    'datos' => $r
                )
            );
        }

    }
}

