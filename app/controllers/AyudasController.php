<?php

class AyudasController extends BaseController
{
    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function index(){
        //$model = Ayuda::searchPaginateAndOrder();
        $columns = Ayuda::$columns;
        $create = Ayuda::$create;
        $update = Ayuda::$update;

        return Response::json(Ayuda::searchPaginateAndOrder());

        $emergencias = Ayuda::with('AyudaImagen')->get();
        return Response::json( $emergencias );
    }
    /**
     * muestra datos del recurso
     * api/users/{user}/edit
     * GET
     */
    public function edit($id) {
        $user = Ayuda::find($id);
        return $user;
    }
    /**
     * actualizar usuario en especifico
     * api/users/{user}
     * PUT|PATCH
     */
    public function update(/*UpdateUserRequest $request,*/ $id)
    { 
        $request = app()->make('request');


        $rst= Ayuda::find($id)->update($request->all());
        //return response()->json($rst);
        //$user = Ayuda::find($id);

       
        return Response::json($request->all());
    }

    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function getIndex(){
        $request = app()->make('request');

        $ayudas = Ayuda::select(Ayuda::$columns)
            ->where(function($query) use ($request) {
                if($request->has('fecha')) {
                    $query->where('created_at', '<', $request->get('fecha').' :23:59:59');
                    $query->Where('created_at', '>', $request->get('fecha').' 00:00:01');
                }
            })->get();


        return Response::json( $ayudas );
    }

    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function postUpdate(){
        $id = Input::get('id');
        $data = Input::all();
        unset($data['id']);
        unset($data['ayuda_imagen']);
        //return Response::json([$data]);
        $rst= Emergencia::find( $id )->update( $data );
        if ($rst) {
            return Response::json( 
                [
                'rst'=>$rst,
                'msj'=>'se actualizo con exito'
                ] );
        }
        return Response::json( [
                'rst'=>$rst,
                'msj'=>'ocurrio un error al actualizar'
                ]  );
    }
    /**
     * muestra todos las emergencias
     *
     * @return Response
     */
    public function postNuevo(){
        //$id = Input::get('id');
        $data = Input::all();
        //unset($data['id']);
        //unset($data['ayuda_imagen']);
        //return Response::json([$data]);
        $ayuda = new Ayuda($data);
        $rst = $ayuda->save();
        //$rst= Emergencia::find( $id )->update( $data );
        if ($rst) {
            return Response::json( 
                [
                'rst'=>$rst,
                'msj'=>'se inserto registro con exito'
                ] );
        }
        return Response::json( [
                'rst'=>$rst,
                'msj'=>'ocurrio un error al actualizar'
                ]  );
    }
}