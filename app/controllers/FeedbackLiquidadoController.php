<?php

class FeedbackLiquidadoController extends BaseController
{

    public function __construct()
    {
        $this->beforeFilter('auth'); // bloqueo de acceso
    }

    public function postListar()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $feedback = Cache::get('listarFeedbackBandeja');
            if ( $feedback ) {
                $feedback = Cache::get('listarFeedbackBandeja');
            } else {
                $feedback = Cache::remember('listarFeedbackBandeja', 20, function(){
                    $datos = DB::table('feedback_liquidados')
                        ->select('id', 'nombre')
                        ->where('estado', '=', 1)
                        ->orderBy('nombre')
                        ->get();

                    return $datos;
                });
            }
        } else {
            $feedback = DB::table('feedback_liquidados')
                    ->select('id', 'nombre')
                    ->where('estado', '=', 1)
                    ->orderBy('nombre')
                    ->get();
        }
        

        return     Response::json(
            array(
                'rst'=>1,
                'datos'=>$feedback
            )
        );
    }

}
