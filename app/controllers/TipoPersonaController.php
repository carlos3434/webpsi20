<?php

class TipoPersonaController extends BaseController
{

    public function __construct(ErrorController $errorController)
    {
        $this->error = $errorController;
    }

    public function postListar()
    {
        $result=TipoPersona::get();

        return Response::json(array('rst' => 1, 'datos' => $result));
    }

    public function postCrear()
    {
        if (Request::ajax()) {

            $personas = new TipoPersona;
            $personas->nombre = Input::get('nombre');
            $personas->porcent = str_replace("%", "", Input::get('porcent'));
            $personas->estado = Input::get('estado');
            $personas->save();

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro realizado correctamente',
                )
            );
//            } 
        }
    }

    public function postEditar()
    {
        if (Request::ajax()) {

            $personas = TipoPersona::find(Input::get('id'));
            $personas->nombre = Input::get('nombre');
            $personas->porcent = str_replace("%", "", Input::get('porcent'));
            $personas->estado = Input::get('estado');
            $personas->save();

            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
//            }
        }
    }

    public function postCambiarestado()
    {
        if (Request::ajax() && Input::has('id') && Input::has('estado')) {
            $personas = TipoPersona::find(Input::get('id'));
            $personas->estado = Input::get('estado');
            $personas->usuario_updated_at=Auth::id();
            $personas->save();
            return Response::json(
                array(
                    'rst' => 1,
                    'msj' => 'Registro actualizado correctamente',
                )
            );
        }
    }


}
