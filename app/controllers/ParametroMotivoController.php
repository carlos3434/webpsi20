<?php
class ParametroMotivoController extends \BaseController
{
    public function postList()
    {
        $datos = ParametroMotivo::select(
                    'parametro_motivo.id',
                    'parametro_motivo.nombre',
                    \DB::raw('if(tipo_legado = 1, "CMS", "GESTEL") as tipo_legado'),
                    'parametro_motivo.estado as estado',
                    'b.id as bucket_id',
                    'b.nombre as bucket_nombre',
                    'tipo_legado as tipo_legado_id',
                    'eo.nombre as estado_ofsc_nombre',
                    'eo.id as estado_ofsc_id'
                )
                ->leftJoin('bucket as b', 'b.id', '=', 'parametro_motivo.bucket_id')
                ->leftJoin('estados_ofsc as eo', 'eo.id', '=', 'parametro_motivo.estado_ofsc_id')
                ->get();

        return [
            'datos' => $datos
        ];
    }

    public function postBuscar()
    {
        $datos = ParametroMotivoDetalle::select(
                    'parametro_motivo_detalle.id',
                    'parametro_motivo_detalle.motivo_ofsc_id',
                    'mo.descripcion as nombre_motivo_ofsc',
                    'parametro_motivo_detalle.submotivo_ofsc_id',
                    'so.descripcion as nombre_submotivo_ofsc',
                    'accion_contador',
                    'contador',
                    'toolbox',
                    'parametro_motivo_detalle.estado'
                )
                ->leftJoin('motivos_ofsc as mo', 'mo.id', '=', 'motivo_ofsc_id')
                ->leftJoin('submotivos_ofsc as so', 'so.id', '=', 'submotivo_ofsc_id')
                ->where('parametro_motivo_id', Input::get('id'))
                ->get();

        return [
            'datos' => $datos
        ];
    }

    public function postInsert()
    {
        $motivoOfsc = new ParametroMotivo();
        $motivoOfsc->empresa_id = Input::get('empresa_id');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->area_transferencia = Input::get('area_transferencia');
        $motivoOfsc->contrata_transferencia = Input::get('contrata_transferencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }

    public function postUpdate()
    {
        $motivoOfsc = ContrataTransferencia::find(Input::get('id'));
        $motivoOfsc->empresa_id = Input::get('empresa_id');
        $motivoOfsc->actividad_id = Input::get('actividad_id');
        $motivoOfsc->tipo_legado = Input::get('tipo_legado');
        $motivoOfsc->area_transferencia = Input::get('area_transferencia');
        $motivoOfsc->contrata_transferencia = Input::get('contrata_transferencia');
        $motivoOfsc->estado = Input::get('estado');
        $motivoOfsc->save();

        return [
            'rst' => 1
        ];
    }
}

