<?php
use Legados\CableModem;
use Ofsc\Inbound;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaMovimientoDetalle as Detalle;

class CableModemOfscController extends \BaseController
{
    public function __construct()
    {

    }

    public function postObtener()
    {
        
        $cliente=Input::get('cliente');
        $codactu=Input::get('codactu');
        $rst=1;
        $msj='';
        $toa="";
        
        $result=DB::table('status_movistarx')
            ->select('mac2', 'codserv', 'nameclient',
                'servicepackagecrmid', 'nodo', 'troba')
            ->where('idclientecrm', Input::get('cliente'))
            ->get();

        if($result) {
            $cliente=$result[0]->nameclient;
        } else {
            $rst=2;
            $msj='No se encontraron Modems para este Cliente ['.$cliente.']. ';
            
            $inbound = new Inbound();
            $propiedades["A_IND_REQ_PRUEBA"]=2;
            $properties["A_IND_PRUEBA"]='2';//ok, mostrar boton completar
            $toa=$inbound->updateActivity($codactu, $properties);
            //falta regitrar movimiento
        }
        return Response::json(
                        array(
                            'rst' => $rst, 
                            'datos' => $result,
                            'cliente' => $cliente,
                            'msj' => $msj,
                            'toa' => $toa
                        )
                    );
    }
    public function postPruebamodem()
    {
        
        $rst=2;
        $mac=Input::get('mac');
        $codactu=Input::get('codactu');
        $actualizada=Input::get('actualiza', '');
        $solicitud=Input::get('solicitud', '');
        $estado_prueba=0; // no ok

        /*$ultimo = Ultimo::select('solicitud_tecnica_id', 't.celular')
                  ->leftJoin('solicitud_tecnica as st','st.id','=',
                    'solicitud_tecnica_ultimo.solicitud_tecnica_id')
                  ->leftJoin('tecnicos as t', 't.id', '=', 'tecnico_id')
                  ->where('num_requerimiento', $codactu);

        if($solicitud!==''){
            $ultimo->where('st.id_solicitud_tecnica', $solicitud);
        } else {
            $ultimo->orderBy('solicitud_tecnica_ultimo.id', 'desc');
        }
                 
        $ultimo=$ultimo->first();*/
        
        //if($ultimo) {
            //$celular = $ultimo["celular"];

            $api = new CableModem();
            $response = $api->getPruebas($mac);
            list($error,$mediciones) = explode('#', $response);
    
            $datos = explode('|', $mediciones);
            $pwr = [
                'USPWR' => ['min' => 37.0,'max' => 55.0],
                'DSPWR' => ['min' => -5.0,'max' => 10.0],
                'USSNR' => ['min' => 27.0,'max' => 99],
                'DSSNR' => ['min' => 30.0,'max' => 99]
            ];
            $i=$j=0;
            $ix=0;
            $campo1 = $campo2 = $campo3 = $campo4 = '';
            $resultado=array();
            foreach ($datos as $key => $value) {
                if ($key==1 || $key==3 || $key==5 || $key==7) {
                    continue;
                }
                $resultado[$ix]['valor']= $valor =  isset($datos[$key+1])? $data[$key+1] : 0; //(float)
                $resultado[$ix]['estado']= 0;
                switch ($key) {
                    case 0:
                        $resultado[$ix]['campo']= 'Upstream';
                        $resultado[$ix]['metrica']= 'dBmV';
                        $campo1 = $valor;
                        break;
                    case 2:
                        $resultado[$ix]['campo']= 'Downstream';
                        $resultado[$ix]['metrica']= 'dBmV';
                        $campo2 = $valor;
                        break;
                    case 4:
                        $resultado[$ix]['campo']= 'Upstream';
                        $resultado[$ix]['metrica']= 'dB';
                        $campo3 = $valor;
                        break;
                    case 6:
                        $resultado[$ix]['campo']= 'Downstream';
                        $resultado[$ix]['metrica']= 'dB';
                        $campo4 = $valor;
                        break;
                    default:
                        # code...
                        break;
                }

                $resultado[$ix]['medidas']= $pwr[$value]['min'].' a '.$pwr[$value]['max'];
                
                if ($pwr[$value]['min'] <= $valor && $valor <= $pwr[$value]['max']) {
                    if ($valor==="") { //cuando el valor es vacio para las medidas de DSPWR
                        $pwr[$value]['prueba'] = '';
                        $j++;
                    } else {
                        $pwr[$value]['prueba'] = true;
                        $i++;
                        $resultado[$ix]['estado']= 1;
                    }
                } elseif ($valor==="") {
                    $pwr[$value]['prueba'] = '';
                    $j++;
                } else {
                    $pwr[$value]['prueba'] = false;
                }
                $ix++;
            }
            $toa = '';
            \Config::set("ofsc.auth.company", 'telefonica-pe');
            $inbound = new Inbound();
            if ($i==4) {
                // actualizar propiedad en toa, para que el tenico pueda completar la orden
                $properties["A_IND_PRUEBA"]='2';//ok, mostrar boton completar
                $toa = $inbound->updateActivity($codactu, $properties);
                //enviar mensaje al tecnico que se verifico correctamente
                $rst=1;
                $estado_prueba=1;
                $mensaje="Req:".$codactu." pruebas de cable modem ok.";
            }
            if ($j==4) {
                //enviar mensaje al tecnico que la mac no tiene resultado de pruebas
                $rst=2;
                $properties["A_IND_PRUEBA"]='2';//ok, mostrar boton completar
                $toa = $inbound->updateActivity($codactu, $properties);
                $mensaje="Req:".$codactu." pruebas de cable modem vacias.";
            }
            if ($i!=4 && $j!=4) {
                $properties["A_IND_PRUEBA"]='3';//incorrecto
                $toa = $inbound->updateActivity($codactu, $properties);
                //envir  mensaje que no es valido las pruebas
                $rst=2;
                $mensaje="Req:".$codactu." pruebas de cable modem invalidas.";
                
            }
            //Sms::enviarSincrono($celular, $mensaje, '1', '');
            /*$movimiento= Movimiento::where('solicitud_tecnica_id',$ultimo["solicitud_tecnica_id"])
                        ->orderBy('id','desc')
                        ->first();
            $detalle=[
                'campo1'        => $campo1,
                'campo2'        => $campo2,
                'campo3'        => $campo3,
                'campo4'        => $campo4,
                'mac'           => $mac,
                'tipo'          => 2, //pruebas
                'sms'           => $mensaje.' '.$actualizada,
                'estado_prueba' => $estado_prueba,
            ];    
            $detalleObj = new Detalle($detalle);
            $movimiento->detalle()->save($detalleObj); */ 

            return Response::json(
                            array(
                                'rst' => $rst,
                                'msj' => $mensaje,
                                'resultado' => $resultado,
                                'toa' => $toa
                            )
                        );
        /*} else {
            return Response::json(
                            array(
                                'rst' => 3,
                                'msj' => 'Código de Actuación ['.$codactu.'], no existe',
                                'resultado' => ''
                            )
                        );
        }*/
    } 
    public function postModificarmodem()
    {
        $rst=2;
        $msj='La Mac ingresada no es valida';
        $antiguamac = Input::get('antiguamac');
        $mac = Input::get('nuevamac');
        $cliente = Input::get('cliente');
        $codactu = Input::get('codactu');
        $solicitud = Input::get('solicitud');
        $pruebamodem='';
        $result='';

        $punto = strpos($mac, ".");
        $puntodos = strpos($mac, ":");
        $validado = false;
        if ((strlen($mac) == 12) && ($punto === false) && ($puntodos === false)) {
            $validado = true;
        }
        if ($validado) {
            $url=\Config::get("legado.wsdl.mac")."?idcl=".$cliente."&mac1=".$antiguamac."&mac2=".$mac;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
            curl_setopt($ch, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);

            if (strpos($result, 'Actualizado')!==false) {

                $rst=1;
                $msj='Mac Actualizada OK.';

                /*$ultimo = Ultimo::select('solicitud_tecnica_id', 't.celular')
                  ->leftJoin('solicitud_tecnica as st','st.id','=',
                    'solicitud_tecnica_ultimo.solicitud_tecnica_id')
                  ->Join('tecnicos as t', 't.id', '=', 'tecnico_id')
                  ->where('num_requerimiento', $codactu);
                if($solicitud!=='') {
                    $ultimo->where('st.id_solicitud_tecnica', $solicitud);
                } else {
                    $ultimo->orderBy('solicitud_tecnica_ultimo.id', 'desc');
                }
                $ultimo=$ultimo->first();
                
                if($ultimo) {
                    Sms::enviarSincrono($ultimo["celular"], $msj, '1', '');
                }*/

                $splimac=str_split($mac, 4);
                $nuevamac=implode(".", $splimac);

                $array=array('mac' => $nuevamac,
                             'codactu'=>$codactu,
                             'actualiza' => $msj,
                             'solicitud' => $solicitud);

                $pruebamodem=json_decode(
                    Helpers::ruta(
                        'cablemodemofsc/pruebamodem',
                        'POST',
                        $array
                    )
                );
            } else {
                $msj=$result;
            }
        }

        return Response::json(
            array(
                    'rst' => $rst,
                    'msj' => $msj,
                    'pruebamodem' => $pruebamodem
                )
        );

    }
    
}
