
<?php

use Legados\models\SolicitudTecnica as SolicitudTecnica;
use Legados\Repositories\StGestionRepository as StGestion;
use Ofsc\Inbound;

class AutoLiquidacionSolicitudController extends \BaseController
{
    public function liquidarSolicitud ($job, $data) {
		$fecha_actualizacion_old = $data['fecha_actualizacion_old'];
		$solicitud_tecnica_id = $data['solicitud_tecnica_id'];
		//$cod_actu = $data['codActu'];

		$solicitudTecnica = SolicitudTecnica::with('ultimo')->find($solicitud_tecnica_id);
		$fecha_actualizacion_new = $solicitudTecnica->ultimo->updated_at->format('Y-m-d H:i:s');
		$idSolicitudTecnica = $solicitudTecnica->id_solicitud_tecnica;
		if( !Redis::get($idSolicitudTecnica) ) {
			if( $fecha_actualizacion_old == $fecha_actualizacion_new ) {
				Auth::loginUsingId(952);
				$inputs = [
					"observacion" => "Liquidación Automática",
					"estado_aseguramiento" => 5,
					"tematicos" => []
				];
				$objgestion = new StGestion;
				$objgestion->liquidacionToa($solicitudTecnica, $inputs);

		        Auth::logout();
	    	}
	    } else {
	    	
	    }
		$fecha_actualizacion_new = '';
        $fecha_actualizacion_old = '';
		$job->delete();

	}
}