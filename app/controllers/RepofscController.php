<?php
//controller
class RepofscController extends \BaseController
{
    public function postCargarmessages()
    {   
        //si la peticion es ajax
        if (Request::ajax()) {
            $reporte = Repofsc::getCargarRepoMessage();

            return Response::json(array('rst'=>1,'datos'=>$reporte));
        }
    }

    public function postCargarmessagestext()
    {   
        //si la peticion es ajax
        if (Request::ajax()) {
            $reporte='';
            $message = Repofsc::getCargarRepoMessagestext();
            if(count($message)>0)
            {$message[0]->mcbody =
                  str_replace(["<![CDATA[","]]>"], "", $message[0]->mcbody );
             $reporte=$message[0];
            }

            return Response::json(array('rst'=>1,'datos'=>$reporte));
        }
    }

    public function postActividadesofschistorico(){
        
        $filtros = [];
        if (Request::ajax()) {
            $start = Input::get('start');
            $length = Input::get('length');
            $columns = Input::get('columns');
            $orderColumn = [];
            $orderDir = [];
            foreach (Input::get('order') as $order) {
                $orderColumn = $columns[$order['column']]['data'];
                $orderDir = $order['dir'];
            }
            
                $resultado = [];
                $result = DB::table("actividades_ofsc_historico")->orderBy($orderColumn, $orderDir);
                $resultado["data"] = $result->limit($start, $length)->get();
                $resultado["total"] = count($result->get());
                 $resultado["recordsTotal"] =  $resultado["total"];
                 $resultado['recordsFiltered'] =  $resultado["total"];
             $resultado['draw'] = Input::get('draw');
                //print_r($resultado);
                return json_encode($resultado);
             /*return Response::json_encode(
                [
                    "data" => $objActividadesOfscHistorico->get()
                ]
                );*/
        }
    }
    
}
