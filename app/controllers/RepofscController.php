<?php 
use Ofsc\Activity;

class RepofscController extends \BaseController
{
    public function getCruce(){
        $bucket = Input::get('bucket','BK_LARI_LA_MOLINA_RESTO');
        $fechaReg  = '2017-02-01 00:00:01';

        $query = "
        SELECT codactu, SUM(legados) AS enLegado,
         SUM(psi) AS enPSI, SUM(toa) AS enTOA
         , toa.*
      --   , toa.*
         --   select  *
        FROM
        (
        SELECT legado.codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM
            (
            -- legado (cms)   45702319
            SELECT t.codigo_req AS codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM schedulle_sistemas.prov_pen_catv_pais t
            JOIN `webpsi_coc`.`zona_premium_catv` pr ON t.nodo=pr.nodo AND t.troba=pr.troba
            WHERE pr.estado =1 AND t.contrata='245'
            AND t.codigo_tipo_req  IN (  SELECT tipo_req FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
            AND t.codigo_motivo_req  IN (  SELECT motivo FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
          
            UNION

            SELECT t.codigo_req AS codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM schedulle_sistemas.prov_pen_catv_pais t
            LEFT JOIN `webpsi_coc`.`prov_catv_tiposact` p ON t.codigo_tipo_req=p.tipo_req AND t.codigo_motivo_req=p.motivo
            LEFT JOIN `webpsi_coc`.`zona_premium_catv`  pr ON t.nodo=pr.nodo AND t.troba=pr.troba
            WHERE ( t.nodo IN ('LM' , 'LL' , 'SP' ) AND p.averia_m1='MOVISTAR UNO'  )  AND t.contrata='245'
            AND t.codigo_tipo_req  IN (  SELECT tipo_req FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
            AND t.codigo_motivo_req  IN (  SELECT motivo FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
        ) legado 
        UNION 
            -- psi
            SELECT psi.codactu, '0' AS legados, '1' AS psi, '0' AS toa
            FROM
            (   
                SELECT codactu
                FROM  psi.ultimos_movimientos t2
                JOIN quiebres q ON t2.quiebre_id = q.id
                WHERE (t2.mdf IN ('LM' , 'LL' , 'SP' ) AND t2.quiebre_id='25')
                AND t2.empresa_id = '5'  AND t2.contrata='245' AND t2.area2!='PAI'
                AND (t2.tipo_averia='pen_prov_catv'   )
                AND t2.estado_id NOT  IN  ('4','5','6','14','15','17' )
                AND codactu NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' 
                )
                AND codactu   IN (SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais)


                UNION

                SELECT codactu
                FROM  psi.ultimos_movimientos t2
                JOIN quiebres q ON t2.quiebre_id = q.id
                WHERE  (t2.mdf = 'SB' AND q.quiebre_grupo_id='12')
                AND t2.empresa_id = '5'  AND t2.contrata='245' AND t2.area2!='PAI'
                --  AND (t2.estado_legado ='PENDIENTE'  OR t2.estado_legado IS NULL)
                AND (t2.tipo_averia='pen_prov_catv'   )
                AND t2.estado_id NOT  IN  ('4','5','6','14','15','17' )
                AND codactu NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' 
                )
                AND codactu  IN (SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais)
            ) psi

            UNION 

            --  toa
            SELECT appt_number  AS codactu , '0' AS legados, '0' AS psi, '1' AS toa
            FROM actuaciones_toa_tmp
            WHERE (STATUS='pending' /*OR STATUS='suspended'*/)
            
            -- GROUP BY codactu
            
        )  AS  total 
        LEFT JOIN actuaciones_toa_tmp AS  toa  ON  total.codactu = toa.appt_number  
                AND (SELECT MAX(aid) 
                    FROM actuaciones_toa_tmp  t 
                    WHERE  t.appt_number=total.codactu ) =toa.aid

        WHERE total.codactu!='0'  -- AND (status='pending' OR status='suspended' OR status IS NULL) 
        GROUP BY  total.codactu ";

        $reporte =  DB::select($query);

        $filename = Helpers::convert_to_file_excel("cruce_cms_psi_toa");
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Expires: 0');
        header(
            'Cache-Control: must-revalidate, post-check=0, pre-check=0'
        );
        header("Content-Transfer-Encoding: binary");
        header('Pragma: public');

        $n = 1;
        foreach ($reporte as $data) {
            //Encabezado
            if ($n == 1) {
                foreach ($data as $key=>$val) {
                    echo $key . "\t";
                }
                echo $val . "\r\n";
            }
            //Datos
            foreach ($data as $val) {
                $val = str_replace(
                    array("\r\n", "\n", "\n\n", "\t", "\r"),
                    array("", "", "", "", ""),
                    $val
                );
                echo $val . "\t";
            }
            echo "\r\n";
            $n++;
        }
        //return Response::json($results);
    }
    public function postCargarmessages()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $reporte = Repofsc::getCargarRepoMessage();

            return Response::json(array('rst'=>1,'datos'=>$reporte));
        }
    }

    public function postCargarmessagestext()
    {
        //si la peticion es ajax
        if (Request::ajax()) {
            $reporte='';
            $message = Repofsc::getCargarRepoMessagestext();
            if (count($message) > 0) {
                $message[0]->mcbody =
                  str_replace(["<![CDATA[","]]>"], "", $message[0]->mcbody );
             $reporte=$message[0];
            }

            return Response::json(array('rst'=>1,'datos'=>$reporte));
        }
    }

    public function postActividadesofschistorico(){
        $filtros = [];
        if (Request::ajax()) {
            $start = Input::get('start');
            $length = Input::get('length');
            $columns = Input::get('columns');
            $orderColumn = [];
            $orderDir = [];
            foreach (Input::get('order') as $order) {
                $orderColumn = $columns[$order['column']]['data'];
                $orderDir = $order['dir'];
            }
            $resultado = [];
            $result = ActividadOfscHistorico::select("id", "resource_id", "created_at")
                ->orderBy($orderColumn, $orderDir);
            $resultado["total"] = count($result->get());
            $resultado["data"] = $result->limit($length)->offset($start)->get();

            $resultado["recordsTotal"] =  $resultado["total"];
            $resultado['recordsFiltered'] =  $resultado["total"];
            $resultado['draw'] = Input::get('draw');
            return json_encode($resultado);
        }
    }
    /**
     * retorna listado de dias que se generan actividades en json
     * retorna listado de horas que se generan actividades en json segun fecha
     * retorna listado de hoas que se generan actividades en json segun hora
     */
    public function getActividadesofschistorico()
    {
        $resourceId = Input::get("resource_id");

        if (Input::has("fecha")) {
            $fecha = Input::get("fecha");
            $fecha = [$fecha." 00:00:00", $fecha." 23:59:59"];
            $result["datos"] =
                ActividadOfscHistorico::select("id",
                    DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as fregistro"),
                    "data",
                    DB::raw("DATE_FORMAT(created_at, '%r') as hReporte")
                    )
                ->where(["resource_id" => $resourceId])
                ->whereBetween("created_at", $fecha)
                ->orderBy("fregistro", "DESC")
                ->limit(30)->offset(0)
                ->get();
        } else {
            $result["datos"] =
                ActividadOfscHistorico::select(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as fregistro")
                    )
                ->orderBy("fregistro", "DESC")
                ->groupBy("fregistro")
                ->limit(30)->offset(0)
                ->where(["resource_id" => $resourceId])
                ->get();
        }
        return $result;
    }

    public function postTecnicosproductividad()
    {
        $fecha = Input::get("fecha_agenda", "");
        $bucket = Input::get("bucket", "");
        if ($fecha!="" && count($bucket) > 0) {
            $fecha = explode(" - ", $fecha);
            
            if (count($bucket) > 1) {
                return json_encode(["rst" => 2, "msj" => "Solo debe seleccionar un Bucket"]);
            }
            $bucket = $bucket[0];
            $activity = new Activity;
            $from = $fecha[0];
            $to = $fecha[1];
            //$actividades = $activity->getActivities($bucket, $from, $to);
            $actividades = [];
            $i = 0;
            $tmp = $from;
            $to = date("Y-m-d", strtotime($to));
            $resultado = [];
            $fechas = [];
            $tecnicos = [];
            $estados = [];
            $totalesDias = [];
            $noprogramados = [];
            do {
                $dia = date("Ymd", strtotime($from." +$i day"));
                $tmp = date("Y-m-d", strtotime($dia));
                $fechas[$dia] = $tmp;
                $ruta = public_path("json/actividades/".$bucket."/".$dia.".json");
                if (File::exists($ruta)) {
                    $json = File::get($ruta);
                    $array = json_decode($json, true);
                    foreach ($array as $key => $value) {
                        $actividades[] = $value;
                    }
                }
                $i++;
            }
            while($tmp!==$to);
            $agrupadoEstado = array();
            if (count($actividades) > 0) {
                foreach ($actividades as $key => $value) {
                if (isset($value["resource_id"])) {
                    if ($value["resource_id"]!=$bucket){
                    $dia = date("Ymd", strtotime($value["date"]));
                    if ($value["date"] === "3000-01-01") {
                        //$noprogramados[$value["status"]][$value["resource_id"]][] = 1;
                        $noprogramados[$value["resource_id"]][] = 1;
                    } else {
                        //$fechas[$dia] = $value["date"];
                        if (isset($fechas[$dia])) {
                            $tecnicos[$value["resource_id"]] = $value["resource_id"];
                            $estados[$value["status"]] = $value["status"];
                            $agrupadoEstado[$value["status"]][$value["resource_id"]][$dia][] = 1;
                            $totalesDias[$dia][] = 1;
                        }
                        
                        }
                    
                    }
                    }
                }
            }

            return json_encode(["rst"=> 1, "data" => $agrupadoEstado, "fechas" => $fechas, "tecnicos" => $tecnicos, "estados" => $estados, "totalesdias" => $totalesDias, "noprogramados" => $noprogramados]);
        } else {
            return json_encode(["rst" => 2, "msj" => "Fecha o Bucket no seleccionado"]);
        }
    }

    public function postExceltecnicosproductividad()
    {
        

        $excelData = [];
        $dataReporte = json_decode(Input::get("data_reporte"), true);

        $estados = $dataReporte["estados"];
        $tecnicos = $dataReporte["tecnicos"];
        $agrupado = $dataReporte["data"];
        $noprogramados = $dataReporte["noprogramados"];
        $i = 0;
        $total = 0;

        foreach ($agrupado as $key => $value) {
            foreach ($value as $key2 => $value2) {
                foreach ($value2 as $key3 => $value3) {
                    $excelData[$i] = [
                    "status" => $key,
                    "resource_id" => $key2,
                    "fecha" => date("Y-m-d", strtotime($key3)),
                    "cantidad" => 0
                    ];
                    foreach ($value3 as $key4 => $value4) {
                        $excelData[$i]["cantidad"]++;
                    }
                    $total+=$excelData[$i]["cantidad"];
                    $i++;
                }
                
            }
        }
        
        foreach ($noprogramados as $key => $value) {
            $excelData[$i] = [
                "status" => "noprogramado",
                "resource_id" => $key,
                "fecha" => date("Y-m-d", strtotime("")),
                "cantidad" => 0
            ];
            foreach ($value as $key2 => $value2) {
                $excelData[$i]["cantidad"]++;
            }
            $i++;
        }

        return Helpers::exportArrayToExcelHTML($excelData, "ActivdadesTecnicosEstado");
    }

    /**
     * obtener el json de actividades y tecnicos que se guarda en el mapa de tecnicos
     */
    public function getHistorico()
    {
        $resourceId = Input::get('resource_id');
        $json = Input::get('json');
        $pos = strpos(URL::previous(), 'train');

        if ( $pos === true ) {
            $resourceId.='TRAIN';
        }
        $actividades = public_path()."/json/reportes/".$resourceId."/actividades/".$json;
        $tecnicos = public_path()."/json/reportes/".$resourceId."/tecnicos/".$json;
        $jsonAct = $jsonTec= '[]';
        if (File::exists($actividades)) {
            $jsonAct = File::get($actividades);
        }
        if (File::exists($tecnicos)) {
            $jsonTec = File::get($tecnicos);
        }
        $response=['actividades'=>$jsonAct,'tecnicos'=>$jsonTec];
        return Response::json($response);
    }
    /**
     * exportar un json en xls
     */
    public function postExportarjsonxls()
    {
        $json = Input::get('json');
        $actividades = json_decode(Input::get('json'), true);
        return Helpers::exportArrayToExcel($actividades,'actividades');
    }
    /**
     * descargar archivo kml de las actividades en oracle
     */
    public function postExportarmapakml()
    {
        $json = Input::get('json');
        $actividades = json_decode(Input::get('json'), true);
        
        // Creates the Document.
        $kml = new DOMDocument('1.0', 'UTF-8');

        // Creates the root KML element and appends it to the root document.
        $node = $kml->createElementNS('http://earth.google.com/kml/2.1', 'kml');
        $parNode = $kml->appendChild($node);

        // Creates a KML Document element and append it to the KML element.
        $dnode = $kml->createElement('Document');
        $document = $parNode->appendChild($dnode);

        // Estilo del icono color negro del bucket
        $restStyleNode = $kml->createElement('Style');
        $restStyleNode->setAttribute('id', 'bucket');
        $restIconstyleNode = $kml->createElement('IconStyle');
        $restColor = $kml->createElement('color', 'ff000000');
        $restIconstyleNode->appendChild($restColor);
        $restStyleNode->appendChild($restIconstyleNode);

        $document->appendChild($restStyleNode);

        $resourceUno = '';
        foreach ($actividades as $key => $coordinate) {

            $resourceDos = $coordinate["resource_id"];
            if ( $resourceUno != $resourceDos) {
                $colorkml = HistoricoMapaTecnicoController::colorkml();
                $restStyleNode = $kml->createElement('Style');
                $restStyleNode->setAttribute('id', $resourceDos);
                $restIconstyleNode = $kml->createElement('IconStyle');
                $restColor = $kml->createElement('color', $colorkml);
                $restIconstyleNode->appendChild($restColor);
                $restStyleNode->appendChild($restIconstyleNode);
                $document->appendChild($restStyleNode);

                $resourceUno = $resourceDos;
            }
        }
        foreach ($actividades as $key => $coordinate) {
            $resource = $coordinate["resource_id"];
            // Creates a Placemark and append it to the Document.
            $node = $kml->createElement('Placemark');
            $placeNode = $document->appendChild($node);
            // Creates an id attribute and assign it the value of id column.
            $placeNode->setAttribute('id', 'placemark');
            // Create name, and description elements and assigns 
            //them the values of the name and address columns from the results.
            $nameNode = $kml->createElement('name', 'Direccion');
            $placeNode->appendChild($nameNode);
            $descNode = $kml->createElement('description', $coordinate['address']);
            $placeNode->appendChild($descNode);
            $styleUrl = $kml->createElement('styleUrl', $resource);
            if ($coordinate["resource_id"] == 'LARI_INS_MOVISTAR_UNO') {
                $styleUrl = $kml->createElement('styleUrl', '#bucket');
            }
            $placeNode->appendChild($styleUrl);
            // Creates a Point element.
            $pointNode = $kml->createElement('Point');
            $placeNode->appendChild($pointNode);

            // Creates a coordinates element and gives it the value of 
            //the lng and lat columns from the results.
            $coorStr = $coordinate["coordx"] . ','  . $coordinate["coordy"];
            $coorNode = $kml->createElement('coordinates', $coorStr);
            $pointNode->appendChild($coorNode);
        }
        $kmlOutput = $kml->saveXML();
        header('Content-type: application/vnd.google-earth.kml+xml');
        header("Content-disposition: inline; filename=test.kml");
        echo $kmlOutput;
    }

    /**
     * retornar estadsiticos de un json de actividades
     */
    public function postEstadisticomapa()
    {
        $date = date("Ymd");
        $jsonActividades=Input::get('jsonActividades');
        $jsonTecnicos=Input::get('jsonTecnicos');
        $lonbucket=Input::get('lonbucket');
        $latbucket=Input::get('latbucket');
        $actividadesJson = json_decode($jsonActividades, true);
        $tecnicosJson = json_decode($jsonTecnicos, true);
        $response = $this->estadistico($lonbucket, $latbucket, $actividadesJson, $tecnicosJson);
        return Response::json($response);
    }
    /**
     * calculo de:
     * distancia entre un punto de inicio (bucket) y las actividades del tecnico
     * distancia entre actividades del tecnico
     * sumatoria de las distancias del tecnico
     * promedio de las distancias del tecnico
     */
    public function estadistico($lonbucket, $latbucket, $actividadesJson, $tecnicosJson)
    {
        $cantActividades=[];
        $distanciaEntreActTotal = [];
        $distanciaBucketActTotal = [];
        $promedioDistanciaBucketAct = [];
        $sumaDistancias = [];
        $sumaPromedios = [];
        $pestaniaUno = [];
        $pestaniaDos = [];
        foreach ($tecnicosJson as $carnet => $tecnico) {
            //cantidad de actividad
            $cantActividades[$tecnico['carnet']] = 0;
            $carnet=$tecnico['carnet'];
            foreach ($actividadesJson as $actividad) {

                if ($carnet == $actividad['resource_id']
                   // && $actividad['date']!='3000-01-01'
                    && $actividad["resource_id"]!=""
                    && $actividad['appt_number']!="") {
                    echo $carnet."----".$actividad["resource_id"]."\n";

                    if ($actividad['start_time']=='') {
                        # para las canceladas
                        continue;
                    }
                    if (!isset($actividad['XA_WORK_ZONE_KEY'])) {
                        $actividad['XA_WORK_ZONE_KEY']='';
                    }
                    $actividades[$carnet][$actividad['id']] = [
                        'appt_number' => $actividad['appt_number'],
                        'tipo_envio'  => $actividad['XA_APPOINTMENT_SCHEDULER'],
                        'zona_trabajo' => $actividad['XA_WORK_ZONE_KEY'],
                        'tipo_asignacion' => $actividad['A_ASIGNACION_MANUAL_FLAG'],
                        'coordx'      => $actividad['coordx'],
                        'coordy'      => $actividad['coordy'],
                        'start_time'  => $actividad['start_time'],
                        'id'          => $actividad['id'],
                    ];
                    $cantActividades[$carnet] += 1;
                }
            }
            if (!isset($actividades[$carnet])) {
                continue;
            }
            //ordenar $actividades
            $aux=[];
            foreach ($actividades[$carnet] as $key => $row) {
                $aux[$key] = $row['start_time'];
            }
            array_multisort($aux, SORT_ASC, $actividades[$carnet]);
            $distanciaEntreActTotal[$carnet]=0;
            $distanciaBucketActTotal[$carnet]=0;
            for ($i=0; $i<count($actividades[$carnet]); $i++) {
                $act = $actividades[$carnet][$i];
                if ($i==0) {
                    //prmera actividad
                    $distanciaEntreAct=0;
                    $distanciaBucketAct= $this->getDistancia(
                        $latbucket,
                        $lonbucket,
                        $act['coordy'],
                        $act['coordx']
                    );
                } else {
                    $distanciaBucketAct=0;
                    if ($i+1==count($actividades[$carnet])) {
                        //ultima actividad
                        $distanciaBucketAct= $this->getDistancia(
                            $latbucket,
                            $lonbucket,
                            $act['coordy'],
                            $act['coordx']
                        );
                    }
                    $actAnterior = $actividades[$carnet][$i-1];
                    $distanciaEntreAct =$this->getDistancia(
                        $act['coordy'],
                        $act['coordx'],
                        $actAnterior['coordy'],
                        $actAnterior['coordx']
                    );
                }
                $distanciaEntreActTotal[$carnet] += $distanciaEntreAct;
                $distanciaBucketActTotal[$carnet] += $distanciaBucketAct;
                $pestaniaDos[$act['id']] = [
                    'carnet'          => $carnet,
                    'start_time'      => $act['start_time'],
                    'appt_number'     => $act['appt_number'],
                    'tipo_envio'      => $act['tipo_envio'],
                    'zona_trabajo'    => $act['zona_trabajo'],
                    'tipo_asignacion' => $act['tipo_asignacion'],
                    'actividad_x'     => $act['coordx'],
                    'actividad_y'     => $act['coordy'],
                     // distancia entre actividades
                    'distancia_entre_act'   => $distanciaEntreAct,
                    // distancia entre bucket y actividades del tecnico
                    'distancia_bucket_act'   => $distanciaBucketAct
                ];
            }
            $sumaDistancias[$carnet]=$distanciaEntreActTotal[$carnet] + $distanciaBucketActTotal[$carnet];
            $sumaPromedios[$carnet]=round($distanciaEntreActTotal[$carnet]/$cantActividades[$carnet]) + round($distanciaBucketActTotal[$carnet]/2);

            if ($cantActividades[$carnet]<2) {
                $divisor=$cantActividades[$carnet];
            } else {
                $divisor = $cantActividades[$carnet]-1;
            }
            $promedioDistanciaBucketAct[$carnet] = round($distanciaBucketActTotal[$carnet]/2);
            $promedioDistanciaEntreAct[$carnet] = round($distanciaEntreActTotal[$carnet]/$divisor);

            $pestaniaUno[$carnet] = [
                'carnet'       => $carnet,
                'cantidad_actividades'   => $cantActividades[$carnet],
                'distancia_bucket_act'   => $distanciaBucketActTotal[$carnet],
                'promedio_distancia_bucket_act' =>$promedioDistanciaBucketAct[$carnet],
                'distancia_entre_act'    => $distanciaEntreActTotal[$carnet],
                //prmedio de la distancia entre las actividades
                'promedio_distancia_entre_act' =>$promedioDistanciaEntreAct[$carnet],
                'suma_distancias'=>$sumaDistancias[$carnet],
                'suma_promedios'   => $sumaPromedios[$carnet]
            ];
        }
        $resumen = [
            'carnet'                        => count($distanciaEntreActTotal),
            'cantidad_actividades'          => array_sum($cantActividades),
            'distancia_bucket_act'          =>array_sum($distanciaBucketActTotal),
            'promedio_distancia_bucket_act' =>(count($distanciaEntreActTotal) > 0)? round(array_sum($promedioDistanciaBucketAct)/count($distanciaEntreActTotal)) : 0,
            'distancia_entre_act'           =>array_sum($distanciaEntreActTotal),
            'promedio_distancia_entre_act'  =>(count($distanciaEntreActTotal) > 0)? round(array_sum($promedioDistanciaEntreAct)/count($distanciaEntreActTotal)) : 0,
            'suma_distancias'               => array_sum($sumaDistancias),
            'suma_promedios'                => (count($distanciaEntreActTotal) > 0)? round(array_sum($sumaPromedios)/count($distanciaEntreActTotal)) : 0
        ];
        if (is_array($resumen)) {
            array_push($pestaniaUno, $resumen);
        }
        $sTecnico=[];
        $sInicio=[];
        foreach ($pestaniaDos as $clave => $fila) {
            $sTecnico[$clave] = $fila['carnet'];
            $sInicio[$clave] = $fila['start_time'];
        }
        array_multisort($sTecnico, SORT_ASC, $sInicio, SORT_ASC, $pestaniaDos);
        return [
            'pestaniaUno' => $pestaniaUno,
            'pestaniaDos' => $pestaniaDos,
        ];
    }
    /**
     * distancia en tre dos puntos cardinales
     */
    private function getDistancia($lat1, $long1, $lat2, $long2)
    {
        return (number_format(
            6371 * ACOS(
                COS(deg2rad($lat1)) *
                     COS(deg2rad($lat2)) *
                     COS(deg2rad($long2)  - deg2rad($long1)) +
                SIN(deg2rad($lat1)) * SIN(deg2rad($lat2))
            ), 2, '.', ''
        ))*1000 ;
    }
}
