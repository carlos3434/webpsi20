<?php
use Legados\models\Tematicos;
use Illuminate\Support\Collection;
use UsuarioMotivoSubmotivoOfsc as UsuarioMSO;

class AsignacionMotivoUsuarioController extends \BaseController
{

    public function postListar()
    {
        $idlogeado = Auth::user()->id;
        $order = [
            "column" => Input::get('column'),
            "dir" => Input::get('dir')
        ];

        $grilla['perPage'] = Input::has('per_page') ? (int) Input::get('per_page') : null;
        $grilla["order"] = $order;
        $grilla["start"] = Input::has("start") ? (int) Input::get("start") : 0;

        $usuarios = Usuario::select(
            "usuarios.*",
            "a.nombre as area",
            "p.nombre as perfil",
            "e.nombre as empresa"
        )
        ->where("usuarios.estado", 1)
        ->whereRaw("usuarios.area_id IN (SELECT ua.area_id FROM usuario_area AS ua WHERE ua.usuario_id = $idlogeado AND ua.estado = 1)")
        ->leftJoin("areas as a", "a.id", "=", "usuarios.area_id")
        ->leftJoin("perfiles as p", "p.id", "=", "usuarios.perfil_id")
        ->leftJoin("empresas as e", "e.id", "=", "usuarios.empresa_id");

        $search = Input::get("search", "");
        if ($search!="") {
            $usuarios->whereRaw("(usuarios.nombre LIKE '%$search%' OR usuarios.apellido LIKE '%$search%' OR usuarios.usuario LIKE '%$search%')");
        }

        $column = "id";
        $dir = "desc";

        if (isset($grilla["order"])) {
            $column = $grilla["order"]["column"];
            $dir = $grilla["order"]["dir"];
        }
        $usuarios = $usuarios
                    ->orderBy($column, $dir)
                    ->paginate($grilla["perPage"]);
        $data = $usuarios->toArray();
        $col = new Collection([
            'recordsTotal'=> $usuarios->getTotal(),
            'recordsFiltered'=> $usuarios->getTotal(),
        ]);
        return $col->merge($data);
    }

    public function getEditar()
    {
        $id = Input::get("id", 0);
        $motivos = Cache::get("listAsignacionMotivos{$id}");
        if (!$motivos) {
            $motivos = Cache::remember(
                "listAsignacionMotivos{$id}",
                24*60*60,
                function () use ($id) {
                    return UsuarioMSO::where("usuario_id", $id)->get();
                }
            );
        }
        if (!is_null($motivos)) {
            $obj = [];
            foreach ($motivos as $key => $value) {
                if (!is_null($value->submotivo_ofsc_id)) {
                    $obj[$value->motivo_ofsc_id][$value->submotivo_ofsc_id] = $value;
                } else {
                    $obj[$value->motivo_ofsc_id][0] = $value;
                }
                
            }
            return  Response::json(["rst" => 1, "obj" => $obj]);
        } else {
            return  Response::json(["rst" => 2, "msj" => "Error en BD"]);
        }
    }

    public function postGuardar()
    {
        $listMotivos = Input::has("motivosseleccionados") ? Input::get("motivosseleccionados") : [];
        $listMotivos = json_decode($listMotivos, true);
        $listSubMotivos = Input::has("submotivosseleccionados") ? Input::get("submotivosseleccionados") : [];
        $listSubMotivos = json_decode($listSubMotivos, true);
        $motivos = [];
        $submotivos = [];
        foreach ($listMotivos as $key => $value) {
            if (!is_null($value)) {
                if ($value["seleccionado"]) {
                    $motivos[] = $value["id"];
                }
            }
        }

        foreach ($listSubMotivos as $key => $value) {
            if (!is_null($value)) {
                if ($value["seleccionado"]) {
                    if (!isset($submotivos[$value["motivo_ofsc_id"]])) {
                        $submotivos[$value["motivo_ofsc_id"]] = [];
                    }
                    $submotivos[$value["motivo_ofsc_id"]][] = $value["id"];
                }
            }
        }

        $usuarioId = Input::get("id", 0);
        if (count($motivos) <= 0) {
            return  Response::json(["rst" => 2, "msj" => "No puede Actualizar una Asignacion sin Motivos"]);
        }
        
        try {
            $insert = [];
            foreach ($motivos as $key => $value) {
                $idmotivo = $value;
                if (isset($submotivos[$idmotivo])) {
                    $submotivo = $submotivos[$idmotivo];
                    foreach ($submotivo as $key2 => $value2) {
                        $insert[] = ["motivo_ofsc_id" => $idmotivo, "usuario_id" => $usuarioId, "submotivo_ofsc_id" => $value2];
                    }
                } else {
                    $insert[] = ["motivo_ofsc_id" => $idmotivo, "usuario_id" => $usuarioId, "submotivo_ofsc_id" => null];
                }
            }
            UsuarioMSO::where(["usuario_id" => $usuarioId])->delete();
            UsuarioMSO::insert($insert);
            Cache::forget("listAsignacionMotivos{$usuarioId}");
            return Response::json(["rst" => 1, "msj" => "Asignacion de Motivos Guardado con Exito", "obj" => null]);
        } catch (Exception $e) {
            return Response::json(["rst" => 2, "msj" => "Error al Guardar"]);
        }
    }

    public function postMotivosofsc()
    {
        $motivosofsc = Cache::get("listMotivosOfsc");
        if (!$motivosofsc) {
            $motivosofsc = Cache::remember(
                'listMotivosOfsc',
                24*60*60,
                function () {
                    return MotivoOfsc::select(
                        "motivos_ofsc.*",
                        DB::raw("IFNULL(a.nombre, 'Ambos') as actividad"),
                        DB::raw("IFNULL(eo.nombre, 'No se Define') as estadoofsc"),
                        DB::raw("IFNULL(IF(motivos_ofsc.tipo_legado = 1, 'CMS', 'Gestel'), 'No se Define') as tipolegado"),
                        DB::raw("IF(motivos_ofsc.estado=1, 'Activo', 'Inactivo') AS estadomotivo")
                    )
                    ->where("motivos_ofsc.estado", 1)
                    ->leftJoin("actividades as a", "a.id", "=", "motivos_ofsc.actividad_id")
                    ->leftJoin("estados_ofsc as eo", "eo.id", "=", "motivos_ofsc.estado_ofsc_id")
                    ->get();
                }
            );
        }
        return Response::json($motivosofsc);
    }
    public function postSubmotivosofsc()
    {
        $submotivosofsc = Cache::get("listSubMotivosOfsc");
        if (!$submotivosofsc) {
            $submotivosofsc = Cache::remember(
                'listSubMotivosOfsc',
                24*60*60,
                function () {
                    return SubmotivoOfsc::select(
                        "submotivos_ofsc.*",
                        DB::raw("IFNULL(mo.descripcion, '') as motivoofsc"),
                        DB::raw("IFNULL(eo.nombre, 'No se Define') as estadoofsc")
                    )
                    ->where("submotivos_ofsc.estado", 1)
                    ->leftJoin("motivos_ofsc as mo", "mo.id", "=", "submotivos_ofsc.motivo_ofsc_id")
                    ->leftJoin("estados_ofsc as eo", "eo.id", "=", "mo.estado_ofsc_id")
                    ->get();
                }
            );
        }
        return Response::json($submotivosofsc);
    }

    public function postEliminar()
    {
        $id = Input::get("id", 0);
        try {
            UsuarioMSO::where("usuario_id", $id)->delete();
            Cache::forget("listAsignacionMotivos{$id}");
            return Response::json(["rst" => 1, "msj" => "Asignacion Eliminado con Exito"]);
        } catch (Exception $e) {
            return Response::json(["rst" => 2, "msj" => "Error al Eliminar"]);
        }
    }
}
