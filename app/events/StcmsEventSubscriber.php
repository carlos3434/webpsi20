<?php
use Legados\models\SolicitudTecnicaLog;
class StcmsEventSubscriber
{
 
  /**
   * When a solicitud tecnica cms is created
   */
  public function onCreate($event)
  {
    //
    
  }
 
  /**
   * When a solicitud tecnica cms is updated
   */
  public function onUpdate($event)
  {
      $save = false;
      $antes = [];
      $despues = [];
      foreach ($event->toArray() as $key => $value) {
          if ( $value != $event->getOriginal($key) && 
                $key!='updated_at' && $key !='created_at' && 
                $key!='usuario_updated_at' && $key!='usuario_created_at') {
              $save = true;
              $antes[$key] = $event->getOriginal($key);
              $despues[$key] = $value;
          }
      }
      if ($save===true) {
          $solicitudLog = new SolicitudTecnicaLog();
          $solicitudLog->solicitud_tecnica_id = $event->solicitud_tecnica_id;
          $solicitudLog->antes = json_encode($antes);
          $solicitudLog->despues = json_encode($despues);
          $solicitudLog->save();
      }
  }
 
  /**
   * Register the listeners for the subscriber.
   *
   * @param  Illuminate\Events\Dispatcher  $events
   * @return array
   */
  public function subscribe($events)
  {
    $events->listen('stcms.created', 'StcmsEventSubscriber@onCreate');
 
    $events->listen('stcms.updated', 'StcmsEventSubscriber@onUpdate');
  }
 
}