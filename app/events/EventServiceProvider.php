<?php


//use Cribbb\Events\UserEventHandler;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider {

  /**
   * Register
   */
  public function register()
  {
    $this->app->events->subscribe(new StcmsEventSubscriber);
    $this->app->events->subscribe(new TecnicoEventSubscriber);
  }

}