<?php
class UserEventSubscriber {
 
  /**
   * When a user is created
   */
  public function onCreate($event)
  {
    //
    Log::info(['new user']);
  }
 
  /**
   * When a user is updated
   */
  public function onUpdate($event)
  {
    //

    Log::info([$event]);
  }
 
  /**
   * Register the listeners for the subscriber.
   *
   * @param  Illuminate\Events\Dispatcher  $events
   * @return array
   */
  public function subscribe($events)
  {
    $events->listen('eloquent.created: user', 'UserEventSubscriber@onCreate');
    //$events->listen('user.create', 'UserEventSubscriber@onCreate');
 
    $events->listen('user.updated', 'UserEventSubscriber@onUpdate');
    //$events->listen('user.update', 'UserEventSubscriber@onUpdate');
  }
 
}