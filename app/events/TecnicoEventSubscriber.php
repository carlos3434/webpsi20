<?php

class TecnicoEventSubscriber {
 
  /**
   * When a user is created
   */
  public function onCreate($event)
  {
    //
    Log::info(['new user']);
  }
 
  /**
   * When a user is updated
   */
  public function onUpdate($event)
  { 
    $save = false;
    $antes = [];
    $despues = [];
    Log::useDailyFiles(storage_path().'/logs/tecnico_cambios.log');
    Log::info(['datos'=>$event->toArray()]);
    
    foreach ($event->toArray() as $key => $value) {
        if ( $value != $event->getOriginal($key) && 
              $key!='updated_at' && $key !='created_at' && 
              $key!='usuario_updated_at' && $key!='usuario_created_at') {
            $save = true;
            $antes[$key] = $event->getOriginal($key);
            $despues[$key] = $value;
        }
    }
    if ($save===true) {
        $solicitudLog = new TecnicoLogCambio();
        $solicitudLog->tecnico_id = $event->id;
        $solicitudLog->antes = json_encode($antes);
        $solicitudLog->despues = json_encode($despues);
        $solicitudLog->save();
    }
  }
 
  /**
   * Register the listeners for the subscriber.
   *
   * @param  Illuminate\Events\Dispatcher  $events
   * @return array
   */
  public function subscribe($events)
  {
    $events->listen('tecnico.updated', 'TecnicoEventSubscriber@onUpdate');
    //$events->listen('user.create', 'UserEventSubscriber@onCreate');
 
  }
 
}