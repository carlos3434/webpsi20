<?php
Route::any(
    "toa_outbound", function () {
        $file = public_path(). "/ofsc_xml/outbound.xml";
        if (file_exists($file)) {
            $content = file_get_contents($file);
            return Response::make(
		        $content,
		        200,
		        array('content-type'=>'application/xml')
            );
        }
    }
);

Route::any(
    'toa_outbound_test', function () {
        $file = public_path(). "/ofsc_xml/outboundTest.xml";
        if (file_exists($file)) {
            $content = file_get_contents($file);
            return Response::make(
                $content,
                200,
                array('content-type'=>'application/xml')
            );
        }
    }
);

Route::any('stappmovistar.wsdl', function() {
    $file = public_path(). "/legado_xml/stappmovistar.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

Route::any(
        'deco_cms.wsdl', function () {
        $file = public_path(). "/webpsi_xml/deco_cms.xml";
        if (file_exists($file)) {
            $content = file_get_contents($file);
            return Response::make(
                $content,
                200,
                array('content-type'=>'application/xml')
            );
        }
    }
);

Route::any('componente.wsdl', function() {
    $file = public_path(). "/legado_xml/componentes.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

/*prueba williams*/
Route::any('wprueba_web_service.wsdl', function() {
    $file = public_path(). "/legado_xml/wpruebaWebService.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

Route::any('ussd.wsdl', function() {
    $file = public_path(). "/legado_xml/ussd.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

Route::any('solicitudtecnica.wsdl', function() {
    $file = public_path(). "/legado_xml/solicitudTecnica.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});
Route::any('solicitudtecnica_gestel_averia.wsdl', function() {
    $file = public_path(). "/legado_xml/solicitudTecnicaGestelAveria.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});
Route::any('solicitudtecnica_gestel_provision.wsdl', function() {
    $file = public_path(). "/legado_xml/solicitudTecnicaGestelProvision.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

Route::any('solicitud.wsdl', function() {
    $file = public_path(). "/servicios/solicitud.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            ['content-type'=>'application/xml']
        );
    }
});

Route::any(
    'officetrackws', function () {
    $file = public_path()."/ot_xml/officetrack.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            array('content-type'=>'application/xml')
        );
    }
    }
);
Route::any('stmovistarapp', 'StmovistarappController@getServer');
