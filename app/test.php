<?php
use Ofsc\DailyExtract;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnica as ST;
use Ofsc\Activity;
use Cot\models\PreTemporal;
use Cot\models\PretemporalUltimo;



Route::get('get_activities', function () {
    echo "<pre>";
    $resourceId = "BK_LARI_MICROZONA_MO";
    $dataFrom = "2018-03-03";
    $dateTo = "2018-03-23";

    $activity = new Activity();
    //$responseSoap = $activity->getActivities($resourceId, $dataFrom, $dateTo);

    $responseRest = $activity->getActivities($resourceId, $dataFrom, $dateTo);

    //print_r($responseSoap->data[0]);
    echo sizeof($responseRest->data);
    print_r($responseRest);

    exit;





    $soap = [];
    foreach ($responseSoap->data as $actividad) {
        $soap[$actividad["id"]] = $actividad;
        

        if ($actividad["date"] == "3000-01-01" ) {
            //print_r($actividad);
        }
    }

    $rest = [];
    foreach ($responseRest->data as $actividad) {
        $rest[$actividad["id"]] = $actividad;
            

        if ($actividad["date"] !== "" && $actividad["start_time"] == "3000-01-01 00:00:00") {
            //print_r($actividad);
        }
        
    }

   


    $count = 0;

    foreach ($responseSoap->data as $_soap) {
        foreach ($responseRest->data as $_rest) {

            //echo gettype($_soap["id"]) ."-". gettype($_rest["id"])."<br>";

            if ($_soap["id"] === $_rest["id"]) {
                if ( $_soap == $_rest ) {

                } else {
                    //echo gettype($_soap["XA_IDENTIFICADOR_ST"]) ."-". gettype($_rest["XA_IDENTIFICADOR_ST"])."<br>";
                    print_r($_soap);
                    print_r($_rest);
                    $count++;
                }
            }
        }
    }

    echo "count: ".$count.", size : ".sizeof($soap);

    //print_r($soap[31985]);
    //print_r($rest[31985]);
    exit;

});


Route::get("tiempo_server", function () {
    date_default_timezone_set('America/Lima');
    echo date("Y-m-d G:i:s");
});

Route::get("updateaid", function () {
    $solicitudes = \DB::select("select solicitud_tecnica_id FROM solicitud_tecnica_ultimo WHERE estado_ofsc_id = 1 AND aid IS NULL LIMIT 1000");
    $contador = 0;
    foreach ($solicitudes as $key => $value) {
        $movimiento = \DB::select("select aid FROM solicitud_tecnica_movimiento WHERE solicitud_tecnica_id = {$value->solicitud_tecnica_id} AND aid IS NOT NULL ORDER BY id DESC LIMIT 1");
        
        if (isset($movimiento[0])) {
            print_r($movimiento);
            \DB::table("solicitud_tecnica_ultimo")->where(["solicitud_tecnica_id" => $value->solicitud_tecnica_id])->update(["aid" => $movimiento[0]->aid]);
            $contador++;
        }
        
    }
    echo "hay {$contador} solicitudes\n";
});

Route::get("transaccion_go/{st}", function ($st) {
    date_default_timezone_set('America/Lima');

    $registros = \DB::table("transaccion_go_detalle")->where("id_solicitud_tecnica", $st)->orderBy("flujo")->get();

    if ($registros) {

        $contenido = "Flujo, Send To, Created at, Mensaje Id\n";

        foreach ($registros as $registro) {
            
            $contenido .= $registro->flujo.",".$registro->send_to.",".$registro->created_at.",".$registro->mensaje_id."\n";

        }

        $ruta = "/transaccion_go/transaccion_go_st.csv";
        file_put_contents(public_path() . $ruta, $contenido);

        $headers = array('Content-Type: application/csv',);

        return Response::download(public_path().$ruta, $st.'transaccion_go_' . date("Y-m-d G:i:s") . '.csv', $headers);

    } else {
        return "No se encontraron resgistros.";
    }

});


Route::get("updatecmsvalores", function(){
    $solicitudes = \DB::table("solicitud_tecnica_cms")->select("id_solicitud_tecnica","id_solicitud_tecnica AS stcmsid", "valor2 as valor2real", "valor3 as valor3real")
        ->whereIn("id_solicitud_tecnica", ["C0000273823", "C0000274341", "C0000273667", "C0000274493", "C0000274352"])->get();
    /*$solicitudes = \DB::select("SELECT stc.id AS stcmsid, stc.valor1, stc.valor2, stc.valor3, stc.id_solicitud_tecnica, mo.codigo_legado AS valor2real, sbo.codigo_legado AS valor3real, stu.updated_at
    FROM solicitud_tecnica_cms AS stc 
    LEFT JOIN solicitud_tecnica_ultimo AS stu ON stu.solicitud_tecnica_id = stc.solicitud_tecnica_id
    LEFT JOIN motivos_ofsc AS mo ON mo.id = stu.motivo_ofsc_id 
     LEFT JOIN  submotivos_ofsc AS sbo ON sbo.id = stu.submotivo_ofsc_id
WHERE DATE(stu.updated_at) = '2018-02-13' AND valor1 ='' AND stu.estado_ofsc_id IN (6) AND estado_aseguramiento = 6
ORDER BY stu.updated_at DESC");*/
    foreach ($solicitudes as $key => $value) {
        $update = ["valor1" => "REDCLTE", "valor2" => $value->valor2real, "valor3" => $value->valor3real];
        \DB::table("solicitud_tecnica_cms")->where("id", $value->stcmsid)->update($update);
        echo $value->id_solicitud_tecnica." procesada \n";
    }
});
Route::get("testsms", function() {
    Sms::enviarSincrono("993484365", "Hola Joven", '1', '');
});

Route::get("test_rst/{st}", function ($st) {
    $reporte_db = \DB::select('

        SELECT
            st_custom.*,

            st_m.created_at as mov_fecha_registro,

            st_m.id AS mov_id,

            mov_detalle.id AS mov_detalle_id,

            e_a.nombre AS mov_situacion,

            e_ofsc.nombre AS mov_estado_toa,

            REPLACE(REPLACE(REPLACE(REPLACE(m_ofsc.descripcion,CHAR(9),""),CHAR(10),""),CHAR(13),""),"/","") AS mov_motivo,
            REPLACE(REPLACE(REPLACE(REPLACE(sub_m_ofsc.descripcion,CHAR(9),""),CHAR(10),""),CHAR(13),""),"/","") AS mov_sub_motivo,

            tecnico.nombre_tecnico AS mov_nombre_tecnico,

            tecnico.carnet AS mov_carnet_tecnico,

            mov_detalle.usuario_autorizacion AS mov_usuario_toa,

            mov_detalle.fecha_autorizacion AS mov_fecha_autorizacion,

            tipo_autorizacion.descripcion AS mov_tipo_autorizacion,

            replace( replace( mov_detalle.campo1, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico,
            replace( replace( mov_detalle.campo2, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n1,
            replace( replace( mov_detalle.campo3, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n2,
            replace( replace( mov_detalle.campo4, \'\n\', "" ), ".:: Seleccione ::.                                                  Valor 1+                                                 Valor 2+", "" ) AS mov_tematico_n3,

            bk.nombre AS mov_bucket,

            mov_detalle.coordx_tecnico AS mov_coord_x_tecnico,
            mov_detalle.coordy_tecnico AS mov_coord_y_tecnico,

            st_m.xa_observation AS mov_observacion_tecnico_toa

            FROM (
                SELECT
                    st_cms.id_solicitud_tecnica AS solicitud_tecnica,

                    st_cms.num_requerimiento AS codigo_requerimiento,

                    st_cms.cod_cliente  AS codigo_cliente_cms,

                    st_cms.cod_servicio  AS codigo_servicio_cms,

                    CONCAT( st_cms.fecha_registro_requerimiento,  " ",  st_cms.hora_registro_requerimiento )  AS fecha_registro_cms,

                    st_cms.created_at  AS fecha_llegada_psi,

                    (
                        SELECT created_at FROM solicitud_tecnica_movimiento
                        WHERE  solicitud_tecnica_id = st_cms.solicitud_tecnica_id
                        AND estado_ofsc_id = 2
                        ORDER BY created_at ASC
                        LIMIT 1
                    ) AS fecha_llegada_toa,

                    (
                        SELECT created_at FROM solicitud_tecnica_movimiento
                        WHERE  solicitud_tecnica_id = st_cms.solicitud_tecnica_id
                        AND (estado_ofsc_id = 6 OR estado_ofsc_id = 4)
                        ORDER BY created_at DESC
                        LIMIT 1
                    ) AS fecha_liquidacion_toa,

                    estado_aseguramiento.nombre AS situacion_psi,

                    IF (
                        st_u.estado_st = 1,
                        "OK",
                        IF (
                            st_u.estado_st = 2,
                            "TRABAR LEGADO",
                            "ERROR TRAMA"
                        )
                    )  AS estado_solicitud,

                    IF(
                        st_u.estado_st = 1,
                        "Agenda",
                        IF(
                            st_u.estado_st = 2,
                            "SLA",
                            ""
                        )
                    ) AS tipo_agenda,

                    st_cms.coordx_cliente AS coord_x_cliente,

                    st_cms.coordy_cliente AS coord_y_cliente,

                    st_u.solicitud_tecnica_id,

                    st_u.tipo_legado AS legado,

                    st_u.actividad_id AS actividad

                FROM solicitud_tecnica_cms AS st_cms

                INNER JOIN solicitud_tecnica_ultimo AS st_u ON st_u.solicitud_tecnica_id = st_cms.solicitud_tecnica_id



                LEFT JOIN estados_aseguramiento AS estado_aseguramiento ON st_u.estado_aseguramiento = estado_aseguramiento.id

                where st_cms.id_solicitud_tecnica = "'.$st.'"

            ) AS st_custom

            LEFT JOIN solicitud_tecnica_movimiento AS st_m ON st_m.solicitud_tecnica_id = st_custom.solicitud_tecnica_id

            LEFT JOIN estados_ofsc AS e_ofsc ON e_ofsc.id = st_m.estado_ofsc_id

            LEFT JOIN motivos_ofsc AS m_ofsc ON m_ofsc.id = st_m.motivo_ofsc_id

            LEFT JOIN submotivos_ofsc AS sub_m_ofsc ON sub_m_ofsc.id = st_m.submotivo_ofsc_id

            LEFT JOIN solicitud_tecnica_movimiento_detalle AS mov_detalle ON mov_detalle.solicitud_tecnica_movimiento_id = st_m.id

            LEFT JOIN bucket AS bk ON bk.id = st_m.bucket_id

            LEFT JOIN tipo_autorizacion ON tipo_autorizacion.id = mov_detalle.tipo_autorizacion_id

            LEFT JOIN estados_aseguramiento AS e_a ON e_a.id = st_m.estado_aseguramiento

            LEFT JOIN tecnicos AS tecnico ON st_m.tecnico_id = tecnico.id

            ORDER BY solicitud_tecnica, st_m.created_at DESC,  mov_detalle.created_at DESC
        ');

    echo "<table>";

    foreach ($reporte_db as $key => $row) {
        echo "<tr>";
        foreach ($row as $key2 => $column) {
            echo "<td style='border: solid 1px black;text-align: center;'>".$key2."</td>";
        }
    echo "</tr>";
        break;
    }


    foreach ($reporte_db as $key => $row) {
        echo "<tr>";
        foreach ($row as $key2 => $column) {
            echo "<td style='border: solid 1px black;text-align: center;'>".$column."</td>";
        }
    echo "</tr>";
    }
    echo "</table>";
});


Route::get("testmac", function () {
    $cliente = "3589505";
    $antiguamac = "acd1b8b627e7";
    $mac = "9CAD97C6D0F5";
    $url=\Config::get("legado.wsdl.mac")."?idcl=".$cliente."&mac1=".$antiguamac."&mac2=".$mac;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
            curl_setopt($ch, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
    print_r($result);
            curl_close($ch);
});

Route::get("insertmasivoulti", function () {
    DB::beginTransaction();
    $pretemporales = DB::table('pre_temporales as p')
                    ->select('p.id')
                    ->get();
    foreach ($pretemporales as $key => $value) {
        try {
            $mov = DB::table('pre_temporales_movimientos')->where('pre_temporal_id', $value->id)->orderBy('created_at', 'desc')->limit(1)->get();
            $data =  [   'pre_temporal_id' => $mov[0]->pre_temporal_id,
                        'codcli' => $mov[0]->codcli,
                        'motivo_id' => $mov[0]->motivo_id,
                        'submotivo_id' => $mov[0]->submotivo_id,
                        'estado_id' => $mov[0]->estado_id,
                        'estado_pretemporal_id' => $mov[0]->estado_pretemporal_id,
                        'observacion' => $mov[0]->observacion,
                        'pre_temporal_solucionado_id' => $mov[0]->pre_temporal_solucionado_id,
                        'created_at' => $mov[0]->created_at,
                        'usuario_created_at' => $mov[0]->usuario_created_at,
                        'updated_at' => $mov[0]->updated_at,
                        'usuario_updated_at' => $mov[0]->usuario_updated_at
                    ];
            DB::table('pre_temporales_ultimos_tmp')->insert($data);
            DB::commit();
        } catch (Exception $e) {
            Log::useDailyFiles(storage_path().'/logs/ultimos_cot.log');
            Log::info([$e]);
            DB::rollback();
        }
    }
});
Route::get("downloadlognulo", function ()
{
    $fecha = ["2017-12-09 00:00:00", "2017-12-12 23:59:59"];
    $resultado = [];
    $erradosnull = [];
    $erradosparam = [];
    $ok = [];
    $canterrordata = 1;
    $canterrorparam = 1;
    $log = EnvioLegado::whereBetween("created_at", $fecha)
        ->get()
        ->toArray();
    foreach ($log as $key => $value) {
        if (!isset($resultado[$value["solicitud_tecnica_id"]])) {
            $resultado[$value["solicitud_tecnica_id"]] = [];
        }
        $resultado[$value["solicitud_tecnica_id"]][] = $value;
    }
    foreach ($resultado as $key => $value) {
        foreach ($value as $key2 => $value2) {
            unset($resultado[$key][$key2]["xml_response"]);
            unset($resultado[$key][$key2]["xml_request"]);
            $request = json_decode($value2["request"], true);
            $response = json_decode($value2["response"], true);
            
            if (isset($response["error"])) {
                if ($response["error"] == false) {
                    if (is_null($response["data"])) {
                        $resultado[$key][$key2]["comentario"] = "Devuelve Data Nula";
                        
                        //print_r($request);
                        if (isset($request["id_solicitud_tecnica"])) {
                            if (!isset($erradosnull[$request["id_solicitud_tecnica"]])) {
                                $erradosnull[$request["id_solicitud_tecnica"]] = [];
                            }
                            $erradosnull[$request["id_solicitud_tecnica"]][] = $resultado[$key][$key2];
                            /*if ($value2["accion"] == "CierreSolicitudTecnica") {
                                if (!isset($erradosnull[$request["solicitud_tecnica"]])) {
                                    $erradosnull[$request["solicitud_tecnica"]] = [];
                                }
                                $erradosnull[$request["solicitud_tecnica"]][] = $resultado[$key][$key2];
                            }*/
                            $canterrordata++;
                        }
                        
                    }
                } else {
                    if (isset($request["id_solicitud_tecnica"])) {
                        if (!isset($erradosparam[$request["id_solicitud_tecnica"]])) {
                            $erradosparam[$request["id_solicitud_tecnica"]] = [];
                        }
                            $erradosparam[$request["id_solicitud_tecnica"]][] = $resultado[$key][$key2];
                            /*if ($value2["accion"] == "CierreSolicitudTecnica") {
                                if (!isset($erradosnull[$request["solicitud_tecnica"]])) {
                                    $erradosnull[$request["solicitud_tecnica"]] = [];
                                }
                                $erradosnull[$request["solicitud_tecnica"]][] = $resultado[$key][$key2];
                            }*/
                            $canterrorparam++;
                    }
                    /*$resultado[$key][$key2]["comentario"] = "Error Tipicado";
                    if ($value2["accion"] == "RptaEnvioSolicitudTecnica") {
                        if (isset($request["NumeroSolicitud"])) {
                            if (!isset($erradosparam[$request["NumeroSolicitud"]])) {
                                $erradosparam[$request["NumeroSolicitud"]] = [];
                            }
                            $erradosparam[$request["NumeroSolicitud"]][] = $resultado[$key][$key2];
                        }
                        if (isset($request["SolicitudTecnica"])) {
                            if (!isset($erradosparam[$request["SolicitudTecnica"]])) {
                                $erradosparam[$request["SolicitudTecnica"]] = [];
                            }
                            $erradosparam[$request["SolicitudTecnica"]][] = $resultado[$key][$key2];
                        }
                    }
                    if ($value2["accion"] == "CierreSolicitudTecnica") {
                        if (!isset($erradosparam[$request["solicitud_tecnica"]])) {
                            $erradosparam[$request["solicitud_tecnica"]] = [];
                        }
                        $erradosparam[$request["solicitud_tecnica"]][] = $resultado[$key][$key2];
                    }*/
                    
                }
            }
        }
    }
    echo "<pre>";
    print_r($erradosparam);
    echo "<pre>";
    echo "total == ".$canterrorparam;
    dd();
    //Helpers::exportArrayToExcel($log->toArray(), "logenvio");

});

Route::get("updatebucket", function(){
    $movimientos = Movimiento::select("id", "resource_id", "solicitud_tecnica_id")
        ->where("estado_ofsc_id", "=", 1)
        ->whereRaw("CHAR_LENGTH(resource_id) <> 8")
        ->whereRaw("CHAR_LENGTH(resource_id) <> 6")
        ->whereRaw("resource_id IS NOT NULL")
        ->groupBy("solicitud_tecnica_id")
        ->groupBy(DB::raw("DATE(created_at)"))
        ->get()->toArray();
    $buckets = Bucket::get()->toArray();
    $bucketsarr = [];
    $stupdate = [];
    foreach ($buckets as $key => $value) {
        $bucketsarr[$value["bucket_ofsc"]] = $value;
        $stupdate[$value["bucket_ofsc"]] = [];
    }
    
    foreach ($movimientos as $key => $value) {
       $resourceId = $value["resource_id"];
       if (isset($bucketsarr[$resourceId])) {
          $stupdate[$resourceId][$value["solicitud_tecnica_id"]] = $bucketsarr[$resourceId]["id"];
       }
    }

    foreach ($stupdate as $key => $value) {
        if (count($value) > 0) {
            foreach ($value as $key2 => $value2) {
                DB::table('solicitud_tecnica_ultimo')->where(["solicitud_tecnica_id" => $key2])->update(["bucket_id" => $value2]);
                DB::table('solicitud_tecnica_movimiento')->where(["solicitud_tecnica_id" => $key2])->update(["bucket_id" => $value2]);
            }
        }
    }
    echo "ya esta!!!";
});
Route::get("queryzonapremium", function() {
    $fila = 1;
    $file = "/home/juan/Descargas/microzonas_lamolina.csv";
    $count = 0;
    $tematicos_t = [];
    if (($gestor = fopen($file, "r")) !== FALSE) {
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            $numero = count($datos);
            //echo "<p> $numero de campos en la línea $fila: <br /></p>\n";
            $fila++;
            for ($c=0; $c < $numero; $c++) {
                //echo $datos[$c] . "<br />\n";
                if( $fila > 2) {
                    $column = explode(";", $datos[$c]);
                    $tmp = $column;
                    foreach ($column as $key3 => $value3) {
                        if ($value3=="") {
                            unset($tmp[$key3]);
                        }
                    }
                    $tematicos_t[] = $tmp;

                    $count++;
                }
                
            }
        }
        fclose($gestor);
    }
    //print_r($tematicos_t);
    $query = "";
    if (count($tematicos_t) > 0) {
        $insert = [];
        foreach ($tematicos_t as $key => $value) {
           $insert[] = "('{$value[1]}', '{$value[0]}', '{$value[2]}', 'LARI' , '2017-11-07 18:17:30', '2017-11-07 18:17:30', 1)";
        }
        $implode = implode($insert, ",");
        $query.="INSERT INTO zona_premium_catv (zona, nodo, troba, contrata, created_at, updated_at, estado) VALUES {$implode}";
        echo $query;
    }
});
Route::get("q_test", function(){
    $quiebre_dig = \DB::table("dig_trobas")
        ->leftJoin("geo_trobapunto as gt", "gt.id", "=", "dig_trobas.troba_id")
        ->leftJoin("empresas as e", "e.id", "=", "dig_trobas.empresa_id")
        ->where("e.estado", 1)
        ->where("dig_trobas.est_seguim", "A")
        ->where(function($query)
        {
            $query->where("e.cms", "=", "357")
                ->orWhere("e.cms", "=", "387");
        })
        ->get();
        echo "<pre>";
        print_r($quiebre_dig);
        echo "</pre>";
});

Route::get("validarcoordenadas", function(){
    $solicitud = new Legados\models\SolicitudTecnicaCms;
    $solicitud->cod_nodo = "SP";
    $solicitud->cod_troba = "R014";
    $solicitud->zonal = "LIM";
    $solicitud->coordy_cliente = 0.0000000000000;
    $solicitud->coordx_cliente = 0.0000000000000;
    $solicitud->coordx_direccion_tap = "";
    $solicitud->coordy_direccion_tap = "";
    $solicitud->cod_servicio = "1541566";
    $objstimplements = new Legados\Repositories\SolicitudTecnicaRepository();
    $solicitud = $objstimplements->sanearCoordenadas($solicitud);
    print_r($solicitud);
});
Route::get("solicitudesmovtocsv", function(){
    $fechainicio = "2017-09-01";
    $fechafin = "2017-11-01";
    for($i = 0; $i < 60; $i++) {
        $fecha = date("Y-m-d", strtotime ( '+'.$i.' day' , strtotime ( $fechainicio ) ) );
        $st = DB::query("SELECT solicitud_tecnica_cms.id_solicitud_tecnica, 
            u.num_requerimiento,tec.nombre_tecnico,
            tec.carnet AS carnet_tecnico,
            u.resource_id AS bucket,
            eo.nombre AS estado,
            u.fecha_agenda AS fecha_agendado,
            stm2.created_at AS fecha_iniciado,
            u.updated_at AS fecha_ult_mov,
            solicitud_tecnica_cms.coordx_cliente,
            solicitud_tecnica_cms.coordy_cliente,
            (SELECT stmd.coordx_tecnico FROM solicitud_tecnica_movimiento_detalle AS stmd 
            LEFT JOIN solicitud_tecnica_movimiento AS stm ON stm.id = stmd.solicitud_tecnica_movimiento_id 
            WHERE stm.solicitud_tecnica_id = u.solicitud_tecnica_id  AND stmd.tipo IN (1,8) AND stm.estado_ofsc_id = 2 
            AND stmd.coordx_tecnico IS NOT NULL
            LIMIT 0,1) AS coordx_tecnico_inicio,
            solicitud_tecnica_cms.cod_cliente,
            u.aid AS apointment_toa,
            solicitud_tecnica_cms.tipo_operacion,
            solicitud_tecnica_cms.desc_tipo_requerimiento AS tipo_requerimiento,
            '' AS usuario_valida_coord,
            IF(u.estado_aseguramiento=6 OR u.estado_aseguramiento=9,u.updated_at,'') AS fecha_cierre,
            /*u2.updated_at AS fecha_cierre,*/
            TIMESTAMPDIFF(MINUTE,stm2.created_at,IF(u.estado_aseguramiento=6 OR u.estado_aseguramiento=9,u.updated_at,'')) AS iniciado_to_cierre,
            mo.descripcion AS motivo,
            suo.descripcion AS submotivo 
                    FROM solicitud_tecnica_cms
                    LEFT JOIN solicitud_tecnica_ultimo AS u ON u.solicitud_tecnica_id = solicitud_tecnica_cms.solicitud_tecnica_id
                    LEFT JOIN solicitud_tecnica_movimiento  AS stm2  ON stm2.solicitud_tecnica_id=solicitud_tecnica_cms.solicitud_tecnica_id 
                    AND stm2.id=(SELECT id FROM solicitud_tecnica_movimiento WHERE  solicitud_tecnica_id=solicitud_tecnica_cms.solicitud_tecnica_id AND estado_ofsc_id=2 ORDER BY created_at DESC LIMIT 1) 
                    AND u.num_requerimiento IS NOT NULL  AND u.estado_st = 1
                    AND DATE(u.updated_at) = '".$fecha."'
                    LEFT JOIN motivos_ofsc AS mo ON mo.id = u.motivo_ofsc_id  
                    LEFT JOIN submotivos_ofsc AS suo ON suo.id = u.submotivo_ofsc_id
                    LEFT JOIN estados_ofsc AS eo ON eo.id = u.estado_ofsc_id    
            LEFT JOIN tecnicos AS tec ON u.tecnico_id=tec.id 
                    LEFT JOIN actividades AS act ON act.id = u.actividad_id  
                    ORDER BY u.`updated_at` DESC");

        $file = "/www/psi20/htdocs/public/reportest/solicitudes_".date('Ymd', $fecha).".csv";

        $fp = fopen($file, 'w');

        foreach ($st as $campos) {
            fputcsv($fp, $campos);
        }

        fclose($fp);
    }

});
Route::get("tematicocarga", function() {
    $motivos_ofsc = DB::table("psi.motivos_ofsc")->where(["estado"=> 1, "actividad_id" => 2, "estado_ofsc" => "T", "tipo_legado" => 1])->select("id", "descripcion")->get();

    //echo "<pre>";
    //print_r($motivos_ofsc);
    //echo "</pre>";
    //return;

    DB::table("psi.tematicos")->where(["actividad_id" => 2, "tipo" => "T"])->delete();

    $tematicos_t = [];
    $count = 0;

    $fila = 1;
    $file = public_path("/csv/tematicos.csv");
    if (($gestor = fopen($file, "r")) !== FALSE) {
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            $numero = count($datos);
            //echo "<p> $numero de campos en la línea $fila: <br /></p>\n";
            $fila++;
            for ($c=0; $c < $numero; $c++) {
                //echo $datos[$c] . "<br />\n";
                if( $fila > 2) {
                    $column = explode(";", $datos[$c]);
                    $tematicos_t[] = $column;

                    $count++;
                }
                
            }
        }
        fclose($gestor);
    }

    echo $count;

    $insertados_lvl_1 = [];
    $insertados_lvl_2 = [];
    $insertados_lvl_3 = [];
    $insertados_lvl_4 = [];

    //lvl_1
    foreach ($tematicos_t as $tematico) {
        if ( !isset( $insertados_lvl_1[ $tematico[1] ] ) && $tematico[1] !== "" ) {

            $id = insertaTematicoT( $tematico[1], 0, findMotivo($motivos_ofsc, $tematico[0]) );

            $insertados_lvl_1[ $tematico[1] ] = $id;
        }
    }

    //lvl_2
    foreach ($tematicos_t as $tematico) {
        if ( !isset( $insertados_lvl_2[ $tematico[2] ] ) && $tematico[2] !== "" ) {

            $id = insertaTematicoT($tematico[2], $insertados_lvl_1[ $tematico[1] ], findMotivo($motivos_ofsc, $tematico[0]) );

            $insertados_lvl_2[ $tematico[2] ] = $id;

        }
    }

    //lvl_3
    foreach ($tematicos_t as $tematico) {
        if ( !isset( $insertados_lvl_3[ $tematico[3] ] ) && $tematico[3] !== "" ) {

            $id = insertaTematicoT($tematico[3], $insertados_lvl_2[ $tematico[2] ], findMotivo($motivos_ofsc, $tematico[0]) );

            $insertados_lvl_3[ $tematico[3] ] = $id;

        }
    }

    //lvl_4
    foreach ($tematicos_t as $tematico) {
        if ( !isset( $insertados_lvl_4[ $tematico[4] ] ) && $tematico[4] !== "" ) {

            $id = insertaTematicoT($tematico[4], $insertados_lvl_3[ $tematico[3] ], findMotivo($motivos_ofsc, $tematico[0]) );

            $insertados_lvl_4[ $tematico[4] ] = $id;

        }
    }

    echo "<pre>";
    print_r(["1"=>$insertados_lvl_1,"2"=>$insertados_lvl_2,"3"=>$insertados_lvl_3,"4"=>$insertados_lvl_4]);
    echo "</pre>";

    echo "<pre>";
    print_r($tematicos_t);
    echo "</pre>";


});

function findMotivo($lista, $descripcion){
    foreach ($lista as $motivo) {
        if($descripcion == $motivo->descripcion){
            return $motivo->id;
        }
    }

    return null;
}

function insertaTematicoT($nombre, $padre, $motivo_ofsc_id){
    return DB::table("psi.tematicos")->insertGetId([
        "actividad_id" => 2,
        "nombre" => $nombre,
        "descripcion" => $nombre,
        "estado" => 1,
        "parent" => $padre,
        "tipo" => "T",
        "tipo_legado" => 1,
        "is_no_realizado" => 1,
        "is_completado" => 0,
        "is_pre_devuelto" => 1,
        "is_pre_liquidado" => 0,
        "is_pendiente" => 0,
        "motivo_ofsc_id" => $motivo_ofsc_id
    ]);
}

Route::get("searchfiledownload", function() {
    $body = [];
    $elementos = [];
    $elementos["url"] = "https://api.etadirect.com/rest/ofscCore/v1/folders/dailyExtract/folders";
    $elementos["method"] = "GET";
    $elementos["request"] = json_encode($body);
    $obj = new DailyExtract;
    $response = $obj->getDatesExtract();
    if ($response->error == false) {
        if (isset($response->data)) {
            if (is_array($response->data)) {
                 if (isset($response->data["folders"])) {
                    if (isset($response->data["folders"]->items)) {
                        $items = $response->data["folders"]->items;
                        $i = 0;
                        foreach ($items as $key => $value) {
                           $links = $value->links;
                           $name = $value->name;
                           $responsefiles = $obj->getFilesByDate($name);
                           if ($i == 0) {
                              if (isset($responsefiles->data)) {
                                if (is_array($responsefiles->data)) {
                                    $files = $responsefiles->data["files"];
                                    if (isset($files->items)) {
                                        $itemsarchivos = $files->items;
                                        foreach ($itemsarchivos as $key3 => $value3) {
                                            $responsefile = $obj->getFileDownloadByName($name, $value3->name);
                                        }
                                    }
                                }
                              }
                           }
                        }
                        //print_r($items);
                    }
                 }   
            }
        }
    }

});
Route::get("probandodailyforlder", function () {
    $body = [];
    $elementos = [
        "url" => "https://api.etadirect.com/rest/ofscCore/v1/folders/dailyExtract/folders",
        "method" => "GET",
        'request' => json_encode($body),
    ];

    $url = "https://api.etadirect.com/rest/ofscCore/v1/folders/dailyExtract/folders/2017-10-01/files";
    //$url =sprintf($urltmp, "2017-10-01");

    $body = [
    ];
    $elementos= [
            'body' => $body,
            'url' => $url,
            'method' => 'POST',
            'request' => json_encode($body),
        ];
    $curl = curl_init($elementos['url']);
            if ($curl === false) {
                return 'NOT INIT Curl';
            }
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, \AuthBasic::getAuthString());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            //curl_setopt($curl, CURLOPT_PROXY, \Config::get("wpsi.proxy.host"));
            //curl_setopt($curl, CURLOPT_PROXYPORT, \Config::get("wpsi.proxy.port"));

            if ($elementos['method'] != 'POST') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $elementos['method']);
            }
            if (strlen($elementos['request']) > 2) {
                curl_setopt($curl, CURLOPT_BINARYTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $elementos['request']);
            }
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);

           /* if ($result == false) {
                $response->error=true;
                $response->errorMsg=curl_error($curl);
            } elseif (is_string($result)) {
                $response->data=(array)json_decode(strip_tags($result));
            } else {
                $response->data = (array)json_decode($result);
            }*/
            print_r($result);
            curl_close($curl);
});
Route::get("downloadfile", function(){
    $file = "/var/www/webpsi20/extractortoa/validacioncoordenadas/CoordenadasTecnicas.xls";
    return \Response::download($file, 'output.xls', ['Content-Type: application/vnd.ms-excel; charset=utf-8']);
    //FileHelper::download($file);
});

Route::get("downloadvalidacionxls", function() {
    $data = MensajesResumen::select("fecha", "num_requerimiento", "resource_id", "tiempo", "fec_sending", "fec_delivered")->whereBetween("fecha", ["2017-09-08", "2017-10-09"])->get();
   $data = json_decode(json_encode($data),true);
    Helpers::exportArrayToExcel($data, "prueba");
});

use Legados\STCmsApi;
Route::post('respuestastbyid/', function () {
    $id = Input::get($id);
    echo "aqui";
    $trama = DB::table('solicitud_tecnica_log_recepcion as st')
                    ->where('st.id', '=', $id)
                    ->orderBy('st.id', 'desc')->first();
    
    $demo = [
        'id_solicitud_tecnica' => $trama->id_solicitud_tecnica,
        'id_respuesta' => $trama->code_error,
        'observacion' => $trama->descripcion_error,
        'solicitud_tecnica_id' => $trama->solicitud_tecnica_id
    ];
    print_r($demo);
    $objApiST = new STCmsApi('GESTEL Legado');
    $objApiST->responderST($demo);
});

Route::get("quieb", function() {
        $quiebre_dig = \DB::table("dig_trobas")
                    ->leftJoin("geo_trobapunto as gt", "gt.id", "=", "dig_trobas.troba_id")
                    ->leftJoin("empresas as e", "e.id", "=", "dig_trobas.empresa_id")
                    ->where(["gt.troba" => "R001", "gt.nodo" => "CB"], "e.estado", 1)
                    ->where(function($query)
                    {
                        $query->where("e.cms", "=", "357")
                            ->orWhere("e.cms", "=", "387");
                    })
                    ->get();



        $premium = \DB::table("webpsi_coc.zona_premium_catv")->select(["nodo", "troba"])
            //->where(["nodo" => $st_cms->cod_nodo,
            //    "troba" => $st_cms->cod_troba,
            //    "estado" => 1])
            ->get();

        echo "<pre>";
        print_r($premium);
        echo "</pre>";
        return;


        //datos de simulacion
            $actividadId = 1;
            $st_cms = [
                
            ];

        //quiebres
        $quiebres = Quiebre::select(["id", "nombre"])->with([
            "parametros" => function( $query ) {

                return $query->select(["id", "quiebre_id", "bucket"]);

            },
            "parametros.parametroCriterio" => function( $query ) {

                return $query->select(["parametro_id", "criterios_id", "detalle"]);

            },
            "parametros.parametroCriterio.criterio" => function( $query ) {

                return $query->select(["nombre_trama", "id"]);

            }
        ])->get();
        
        //orden
        $orden = DB::table("quiebres_order")
        ->select("orden")
        ->where(["provinencia" => 2,  "actividad_id" => 1])
        ->first();
        
        //Evaluando orden registrado
        if ( $orden !== null ) {

            $orden = explode(",", $orden->orden);

        } else {

            $orden = [];

        }

        $quiebre_id = null;

        if ( sizeof($orden) > 0 ) {
            //este
            foreach ($orden as $value) {
                foreach ($quiebres as $quiebre) {
                    
                    if ( $value == $quiebre ) {
                        //Procesa
                        $quiebre_id = evaluaQuiebre($quiebre, $st_cms);

                        if ( $quiebre_id != null ) {
                            return $quiebre_id;
                        }

                    }

                }
            }

        } else {

            foreach ($quiebres as $quiebre) {
                //Procesa
                //$quiebre_id = evaluaQuiebre($quiebre, $st_cms);

                if ( $quiebre_id != null ) {
                    return $quiebre_id;
                }

            }

        }

        

        echo "(((".$quiebre_id.")))<br>";
        echo "<pre>";
        print_r($quiebres->toArray());
        //print_r($orden);
        echo "</pre>";
});
Route::get("cancelarmasivoliquidadosaverias", function(){
    Auth::loginUsingId(697);//proceso automatico
    $solicitudes = Ultimo::where("estado_aseguramiento", "=", 9)->where("estado_ofsc_id", "=", 1)->where("actividad_id", "=", 1)->get();
    $api = new Activity();
                   
    foreach ($solicitudes as $key => $value) {
        $movimiento = [];
         $response = $api->cancelActivity($value->aid, []);
            if (isset($response->data->data->commands->command->appointment->aid)) {
                $aid = $response->data->data->commands->command->appointment->aid;
                if (isset($response->data->data->commands->command->external_id)) {
                    $resourceId = $response->data->data->commands->command->external_id;
                }
                $movimiento['estado_ofsc_id']    = 5;//pendiente
                echo "cancele";
            } else {
                if (isset($response->data->data->commands->command->appointment->report->message)) {
                    if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                        $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
                    } else {
                        $mensajes=$response->data->data->commands->command->appointment->report->message;
                        foreach ($mensajes as $key => $value) {
                            if (isset($value->result)) {
                                if ($value->result=="error" || $value->result=="warning") {
                                    $msjErrorOfsc=$value->description;
                                }
                            }
                        }
                    }
                }
                echo "no cancele";
            }
        $solicitudTecnica = ST::find($value->solicitud_tecnica_id);
        $ultimo = $solicitudTecnica->ultimo;
                      
                      foreach (Ultimo::$rules as $key => $value) {
                          if (array_key_exists($key, $movimiento)) {
                              $ultimo[$key] = $movimiento[$key];
                          }
                      }
                      $solicitudTecnica->ultimo()->save($ultimo);


                      $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                      foreach (Movimiento::$rules as $key => $value) {
                          if (array_key_exists($key, $movimiento)) {
                              $ultimoMovimiento[$key] = $movimiento[$key];
                          }
                      }
                      $ultimoMovimiento->save();
    }
    Auth::logout();
});
