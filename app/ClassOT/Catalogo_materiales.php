<?php
namespace ClassOT;
use DB;
class Catalogo_materiales
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "catalogo_materiales";
    
    public $id;
    public $material;
    
    public function buscar($mysql)
    {    
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            $sql = "SELECT 
                        id, material
                    FROM
                        $this->_db.$this->_table
                    WHERE
                        id IS NOT NULL ";

            $valArray = array();
            
            if ( $this->id != "" )
            {
                $sql .= " AND id = ?";
                $valArray[] = $this->id;
            }
            if ( $this->material != "" )
            {
                $sql .= " AND material = ?";
                $valArray[] = $this->material;
            }
            
            $stmt = DB::select($sql,$valArray);
            
            $reporte = array();

            foreach($stmt as $material){
                $reporte[] = (array)$material;
            }
            /*while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            } */

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();            
            return $result;
        }
    }
    
    
}