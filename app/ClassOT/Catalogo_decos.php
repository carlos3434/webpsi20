<?php
namespace ClassOT;
use DB;
class Catalogo_decos
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "catalogo_decos";
    
    public $gestion_id;
    public $carnet;
    public $serie;
    public $tarjeta;
    public $cliente;
    public $fecha_registro;
    public $activo;
    
    public function registrar($mysql)
    {
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            //Registrar decos
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        gestion_id, carnet, serie, tarjeta, 
                        cliente, fecha_registro, accion, activo
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?, ?, ?, ?, ?
                    )";

            $valArray = array(
                $this->gestion_id,
                $this->carnet,
                $this->serie,
                $this->tarjeta,
                $this->cliente,
                $this->fecha_registro,
                $this->accion,
                $this->activo
            );
            $stmt = DB::insert($sql,$valArray);
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Deco registrado correctamente";
            return $result;
        } catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorDecos.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            
            return $result;
        }
    }
    
}