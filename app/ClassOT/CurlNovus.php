<?php
namespace ClassOT;
use Helpers;
class CurlNovus
{
    //private $_base = "10.226.44.222";
    private $_base = "http://localhost";
    private $_acceso="\$PSI20\$";
    private $_clave="\$1st3m@\$";
    
    public function obtener_actu($gid){
        
        try {
            $url     = $this->_base . "/api/obteneractu";
            $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
            $postData=array(
                'gestion_id'    => "$gid",
                'hashg'         => "$hashg"
            );

            /*$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt(
                    $ch, 
                    CURLOPT_POSTFIELDS, 
                    http_build_query($postData)
                    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Retorno  
            $result = curl_exec($ch);
            curl_close($ch);*/
            $result = Helpers::ruta(
                '/api/obteneractu', 'POST', $postData, false
            );
            //$fo = fopen("../03.4_wpsi_api.txt", "w+");
            //fwrite($fo, $result);
            //fclose($fo);
            return $result;
        } catch (Exception $exc) {
            
        }
        
    }
    
    public function calcular_distancia($postData){
        $url= $this->_base . "/api/distanciaactu";
        $gid = $postData["gid"];
        $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
        $postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gid;
        
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);*/
        $result = Helpers::ruta(
            '/api/distanciaactu', 'POST', $postData, false
        );
        return $result;
    }
    
    public function calcular_visitas($gestion_id){
        $url= $this->_base . "/api/estadovisitas";
        
        $hashg   = hash('sha256', $this->_acceso . $this->_clave . $gestion_id);
        $postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gestion_id;
        
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);*/
        $result = Helpers::ruta(
            'api/estadovisitas', 'POST', $postData, false
        );
        return $result;
    }
    
    public function reasignar_trabajo($gestion_id){
        $url= $this->_base . "/api/reenviarot";
        
        //$hashg   = hash('sha256', $this->_acceso . $this->_clave . $gid);
        //$postData["hashg"] = $hashg;
        $postData["gestion_id"] = $gestion_id;
        
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt(
                $ch, 
                CURLOPT_POSTFIELDS, 
                http_build_query($postData)
                );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Retorno  
        $result = curl_exec($ch);
        curl_close($ch);*/
        $result = Helpers::ruta(
            'api/reenviarot', 'POST', $postData, false
        );
        return $result;
    }
    public function notificar_noregistro($phone, $msj){
        //try {
            $url     = $this->_base . "/api/getvalidacion";
            $hashg   = hash('sha256', $this->_acceso . $this->_clave  );
            $postData=array(
                'telefonoOrigen'    => $phone,
                'mensaje'           =>$msj,
                'hashg'         => "$hashg"
            );
            $result = Helpers::ruta(
                'api/getvalidacion', 'POST', $postData, false
            );
            return $result;
            /*
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt(
                    $ch, 
                    CURLOPT_POSTFIELDS, 
                    http_build_query($postData)
                    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Retorno  
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;*/
            //var_dump( $result);
        /*} catch (Exception $exc) {
            return $exc->getMessage();
        }*/
        
    }
}