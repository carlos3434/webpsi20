<?php
namespace ClassOT;

use DB;

class Tarea
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "tareas";

    public function registrar($mysql, $task_id, $cod_tecnico,
                              $paso, $fec_recepcion, $cliente, $fec_agenda)
    {
        $this->_mysql = $mysql;

        try {

            //Iniciar transaccion
            DB::beginTransaction();

            //Registrar error
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, cod_tecnico, 
                        paso, fecha_recepcion, 
                        cliente, fecha_agenda
                    ) 
                    VALUES 
                    (
                       ?, ?, 
                       ?, ?, 
                       ?, ?
                    )";

            $valArray = array(
                $task_id, $cod_tecnico,
                $paso, $fec_recepcion,
                $cliente, $fec_agenda
            );
            DB::insert($sql, $valArray);
            //Ultimo ID registrado
            $queryLastInserted = "SELECT
                                      id
                                  FROM $this->_db.$this->_table
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $id = $lastId[0]->id;
            DB::commit();


            $result["estado"] = true;
            $result["msg"] = "Tarea registrada correctamente";
            $result["id"] = $id;
            return $result;
        } catch (Exception $error) {
            $this->_mysql->rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }

}