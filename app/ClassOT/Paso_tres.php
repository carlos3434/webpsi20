<?php
namespace ClassOT;
use DB;
class Paso_tres
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "paso_tres";
    
    public function registrar($mysql, $TaskNumber, $estado, 
                        $observacion, $tvadicional, 
                        $final_img=array(), $firma_img, $boleta_img)
    {
        $this->_mysql = $mysql;
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        task_id, estado, estado_codigo,
                        observacion, tvadicional, final_img1,
                        final_img2, final_img3,
                        firma_img, boleta_img,
                        fecha_registro
                    ) 
                    VALUES 
                    (
                       ?, ?, ?,
                       ?, ?, ?,
                       ?, ?,
                       ?, ?,
                       ?
                    )";
            
            $estadon=$estado;
            $estadoc="";
            
            $estadoarray=explode("||",$estado);
            if( count($estadoarray)>1 ){
                $estadoc=$estadoarray[0];
                $estadon=$estadoarray[1];
            }
            
            $valArray = array(
                    $TaskNumber, $estadon, $estadoc,
                    $observacion, $tvadicional, $final_img[1],
                    $final_img[2], $final_img[3],
                    $firma_img, $boleta_img,
                    $date
                );
            $stmt = DB::insert($sql,$valArray);

            $queryLastInserted = "SELECT
                                      id
                                  FROM $this->_db.$this->_table
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $reporte["id"] = $lastId;
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso tres registrado correctamente";
            $result["data"] = $reporte;
            return $result;
        } catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorPasoTres.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
}