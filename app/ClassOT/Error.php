<?php
namespace ClassOT;
use DB;
class Error
{    
    private $_table = "errores";
    private $_mysql;
    
    public function registrar( $codigo, $mensaje, $archivo)
    {  
            
        try {
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO webpsi_officetrack.$this->_table 
                    (
                        codigo, mensaje, archivo, fecha
                    ) 
                    VALUES 
                    (
                       ?, ?, ?, ?
                    )";

            $valArray = array($codigo, $mensaje, $archivo, $date);
            DB::insert($sql,$valArray);
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Proyecto registrado correctamente";
        } catch (PDOException $error) {
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
        }
    }
}