<?php
namespace ClassOT;
use DB;
class Asistencia_tecnico
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "asistencia_tecnico";
    
    public $id_entrada;
    public $fecha_asistencia;
    public $direccion;
    public $coor_x;
    public $coor_y;
    public $descripcion;
    public $nombre_tecnico;
    public $numero_tecnico;
    
    public function registrar($mysql)
    {
        $this->_mysql = $mysql;
        
        try {
            DB::beginTransaction();
            
            //Registrar error
            $date = date("Y-m-d H:i:s");
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        id_entrada, fecha_asistencia,
                        direccion, coor_x,
                        coor_y, descripcion,
                        nombre_tecnico, numero_tecnico
                    ) 
                    VALUES 
                    (
                       ?, ?, 
                       ?, ?, 
                       ?, ?, 
                       ?, ?
                    )";

            $valArray = array(
                $this->id_entrada,
                $this->fecha_asistencia,
                $this->direccion,
                $this->coor_x,
                $this->coor_y,
                $this->descripcion,
                $this->nombre_tecnico,
                $this->numero_tecnico
            );
            DB::insert($sql,$valArray);

            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Paso uno registrado correctamente";
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorAsistencia.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);
            //Registrar error
            /*
            require_once './Error.php';
            $Error = new Error();
            
            $Error->registrar(
                $this->_mysql,
                "err_" . $paso_id, 
                $exc->getMessage(), 
                $exc->getFile() . "(" . $exc->getLine() . ")"
            );*/
            
            return $result;
        }
    }
    
}