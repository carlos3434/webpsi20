<?php
namespace ClassOT;
use DB;
class CambioDireccion
{
    private $_mysql;
    private $_db = "psi";
    private $_table = "cambios_direcciones";
    private $_data;
    
    public function registrar($gestion_id, $carnet, 
            $x, $y, $direccion, $referencia)
    {
        
        try {
            $Conexion = new Conexion();
            $this->_mysql = $Conexion->conectar();
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            //ID de tecnico
            $sql = "SELECT id FROM {$this->_db}.tecnicos WHERE carnet_tmp=?";
            $this->_data = array($carnet);
            $data = DB::select($sql,array($carnet));

            $tecnico_id = $data[0]->id;
            
            //INSERT en cambios_direcciones
            $sql = "INSERT INTO {$this->_db}.cambios_direcciones 
                   (gestion_id, tipo_usuario, usuario_id,
                   coord_x, coord_y, direccion, referencia)
                   VALUES (?, ?, ?, ?, ?, ?, ?)";
            $this->_data = array(
                $gestion_id, 'tec', $tecnico_id,
                $x, $y, $direccion, $referencia
            );
            DB::insert($sql,$this->_data);
            
            //UPDATE ultimos_movimientos
            $sql = "UPDATE {$this->_db}.ultimos_movimientos 
                   SET x = ?, y = ? 
                   WHERE gestion_id = ?";
            $this->_data = array(
                $x, $y, $gestion_id
            );
            DB::insert($sql,$this->_data);
            
            //UPDATE gestiones_detalles
            $sql = "UPDATE {$this->_db}.gestiones_detalles 
                   SET x = ?, y = ? 
                   WHERE gestion_id = ?";
            $this->_data = array(
                $x, $y, $gestion_id
            );
            DB::update($sql,$this->_data);
            
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Direccion actualizada";
            //$result["id"] = $id;
            return $result;
        } catch (Exception $error) {
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
}