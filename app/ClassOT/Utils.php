<?php
namespace ClassOT;
use DB;
class Utils 
{
    private $_mysql;
    private $_db = "webpsi_officetrack";
    private $_table = "registro_imagenes";
    
    public function arrayToXml($array, $rootElement = null, $xml = null) {
        $_xml = $xml;

        if ($_xml === null) {
            $_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root><root/>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) { //nested array
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            } elseif(is_object($v)) {
                $this->arrayToXml((array)$v, $k, $_xml->addChild($k));
            } else {
                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

    function is_valid_xml($xml) {
        libxml_use_internal_errors(true);

        $doc = new \DOMDocument('1.0', 'utf-8');

        $doc->loadXML($xml);

        $errors = libxml_get_errors();

        return empty($errors);
    }
    
    /**
     * 
     * @param String $img64 En formato base_64
     * @param Int $gestion_id ID de gestion 
     * @param Int $tarea_id ID de la tarea webpsi_officetrack
     * @param Int $pos Paso 1, 2 o 3
     * @return type Curl response
     */
    public function save_img_curl($img64, $gestion_id, $tarea_id, $pos, $nimg,$mysql) {
        try {
            $this->_mysql = $mysql;
            //Guardar Imagen de inicio
            //$url="http://10.226.44.222:7020/imagen/tareas";
            $url = "http://localhost/webpsi20/public/imagen/tareas";
            $acceso="\$PSI20\$";
            $clave="\$1st3m@\$";
            $hashg=hash('sha256',$acceso.$clave.$gestion_id.$tarea_id.$pos);
            $postData=array(
                'img'           =>"$img64",
                'gestion_id'    =>"$gestion_id",
                'tarea_id'      =>"$tarea_id",
                'pos'           =>"$pos",
                'nimg'          =>"$nimg",
                'hashg'         =>"$hashg"
            );
            //insertar registro de imagen enviada por curl
            $img = "P0".$pos."/g".$gestion_id."/i".$tarea_id."_".$nimg.".jpg";
            //Iniciar transaccion
            //$this->_mysql->beginTransaction();
            $sql = "INSERT INTO $this->_db.$this->_table 
                    (
                        name,  
                        name64, 
                        conteo
                    ) 
                    VALUES 
                    (
                       ?,
                       ?,
                       ?
                    )";

            $valArray = array(
                    $img,
                    $img64,  
                    1
                );
            DB::insert($sql,$valArray);
            
            //Ultimo ID registrado
            $queryLastInserted = "SELECT
                                      id
                                  FROM $this->_db.$this->_table
                                  ORDER BY id DESC LIMIT 1;";
            $lastId = DB::select($queryLastInserted);
            $id = $lastId[0]->id;
            //$this->_mysql->commit();
            
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Retorno  
            $result = curl_exec($ch);
                        
            //fin
            curl_close($ch);
            if ($result=="Finalizado") {
                $sql = "DELETE FROM $this->_db.$this->_table 
                    WHERE id = ? ";

                $valArray = array(
                        $id
                    );
                $reg = DB::delete($sql,$valArray);
            }
            return $result;

        } catch (Exception $exc) {
            $fo = fopen("../00_errorimagen_OT.txt", "w+");
            $fw = fwrite($fo, serialize($exc));
            fclose($fo);
        }
    }

}
