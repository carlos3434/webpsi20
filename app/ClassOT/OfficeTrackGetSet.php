<?php
namespace ClassOT;
class OfficeTrackGetSet
{
    private $_wsdl;
    private $_client;
        
    public function get_wsdl() {
        return $this->_wsdl;
    }

    public function set_wsdl($_wsdl) {
        $this->_wsdl = $_wsdl;
        $this->set_client();
    }

    public function get_client() {
        return $this->_client;
    }

    private function set_client() {
        $wsOptArray = [
            "proxy_host" => \Config::get("wpsi.proxy.host"),
            "proxy_port" => \Config::get("wpsi.proxy.port"),
            "trace" => 1,
            "exception" => 0
        ];
        $opts = array('http' => array('protocol_version' => '1.0'));
        $wsOptArray["stream_context"] = stream_context_create($opts);
        
        try {
            $this->_client = new \SoapClient($this->_wsdl,$wsOptArray);
        } catch (\SoapFault $fault) {
            echo "errorOfficeTrack";
        }
        
    }

}