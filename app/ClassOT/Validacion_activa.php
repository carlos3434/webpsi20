<?php
namespace ClassOT;
use DB;
class Validacion_activa
{
    private $_mysql;
    private $_db = "psi";
    private $_table = "ultimos_movimientos";
    
    public $codactu;
    public $employee;
    
    public function buscar()
    {    
        try {
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            $sql = "SELECT estado_ofsc_id, carnet_tmp, gestion_id from $this->_db.$this->_table um
                            INNER JOIN psi.tecnicos  t
                            on um.tecnico_id=t.id 
                            where t.estado=1  ";

            $valArray = array();
            
            if ( $this->codactu != "" )
            {
                $sql .= " AND codactu = ?";
                $valArray[] = $this->codactu;
            }
            
            $stmt = DB::select($sql,$valArray);

            $reporte = array();
            foreach($stmt as $material){
                $reporte[] = (array)$material;
            }
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (Exception $error) {
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorActivacion.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);

            return $result;
        }
    }
    
     public function consultarphone()
    {    
        try {
            
            //Iniciar transaccion
            DB::beginTransaction();
            
            $sql = "SELECT ape_paterno, RIGHT(celular,9) as celular 
                            from psi.tecnicos 
                            where 1  ";

            $valArray = array();
            
            if ( $this->employee != "" )
            {
                $sql .= " AND carnet_tmp = ?";
                $valArray[] = $this->employee;
            }
            
            $stmt = DB::select($sql,$valArray);

            $reporte = array();
            foreach($stmt as $material){
                $reporte[] = (array)$material;
            }
            
            DB::commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {            
            //Rollback
            DB::rollBack();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            $fo = fopen("errorActivacion.txt", "w+");
            fwrite($fo, serialize($result));
            fclose($fo);

            return $result;
        }
    }
}