<?php
use Legados\models\ComponenteOperacion;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;

class EjecucionComponentes
{
    public function failed()
    {
        Log::error('Job failed!');
    }

    public function fire($job, $data)
    {
        $request = [];
        foreach ($data["request"] as $key => $value) {
            $request[$key] = $value;
        }

        $operacion = ComponenteOperacion::where(
                        'idreg', $request["indicador_operacion"]
                    )->orderBy('id','desc')->first();

        if ($operacion) {
            $operacion['indicador_procesamiento'] = $request["indicador_procesamiento"]; 
            $operacion['codigo_error_interno'] = $request["codigo_error_interno"]; 
            $operacion['codigo_error_envio'] = $request["codigo_error_envio"]; 
            $operacion['observacion_respuesta'] = $request["observacion"];
            $operacion['usuario_updated_at'] = '697'; 
            $operacion['estado_operacion'] = ($request["indicador_procesamiento"] == 1) ? 1:0;
            $operacion->save();

            $celular = $operacion->telefono_origen;
            if (is_null($celular) or $celular == "") {
                $ultimo = Ultimo::where('num_requerimiento', $operacion->codreq)
                        ->leftJoin('tecnicos as t', 't.id', '=', 'tecnico_id')
                        ->orderBy('solicitud_tecnica_ultimo.id', 'desc')->first();
                $celular = $ultimo["celular"];
            }
            if ($request["indicador_procesamiento"] == 1) {
                Sms::enviar($celular, "Operacion exitosa:\n1.Requerimiento {$operacion->codreq}\n2.Operacion {$operacion->operacion}", '1');
            } else {
                Sms::enviar($celular, "Error de operacion IWY:\n1.Requerimiento {$operacion->codreq}\n2.Operacion {$operacion->operacion}", '1');
            }
        }

        $job->delete();
        return;
    }
}
