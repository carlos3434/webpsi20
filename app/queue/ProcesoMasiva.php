<?php

use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnica as Solicitud;
use Legados\Repositories\StGestionRepository as StGestion;
use Ofsc\Inbound;

class ProcesoMasiva
{
    public function failed()
    {
        Log::error('Job failed!');
    }

    public function fire($job, $data)
    {
        $stmasivas=$data['st'];
        foreach ($stmasivas as $key => $value) {
            $solicitud = Solicitud::find($value["solicitud_tecnica_id"]);
            $ultimo = null;
            if (!is_null($solicitud)) {
                $ultimo = $solicitud->ultimo;
            }
            if (!is_null($ultimo)) {
                $ultimomasiva=Masivast::where('solicitud_tecnica_id', $value['solicitud_tecnica_id'])
                            ->first();
                if ($ultimomasiva) {
                    $movimiento = [];
                    $api = new Inbound();
                    $response = $api->cancelActivity(
                        $ultimo->aid,
                        $ultimo->num_requerimiento,
                        []
                    );
                    $movimiento["estado_ofsc_id"] = 5;
                    $movimiento["xa_observation"] = "Cancelacion por Masiva en PSI";
                    if (is_null($ultimo->parametro_id)) {
                        $movimiento["parametro_id"]=$data["parametro"];
                        $movimiento["estado_aseguramiento"]=12;
                    }
                    
                    $objgestion = new StGestion;
                    $objgestion->registrarMovimiento($solicitud, $movimiento, []);
                }
            }
        }
        $result=Masivast::where('parametro_id', $data["parametro"])->delete();
        $job->delete();

        return;
    }
}
