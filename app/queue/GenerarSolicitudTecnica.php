<?php
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\ComponenteCms;
use Legados\helpers\InboundHelper;
use Legados\helpers\SolicitudTecnicaHelper;
use Legados\STCmsApi;
use Legados\models\Error;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;
use Legados\Repositories\SolicitudTecnicaRepository as STRepository;
use Legados\Repositories\StGestionRepository as StGestion;

/**
 * php artisan queue:listen --queue=generar_st
 */
class GenerarSolicitudTecnica
{
    public function failed()
    {
        // Called when the job is failing...
        Log::error('Job failed!');
    }
    /**
     * metodo que ejecuta la cola de solicitud tecnica, su objetivo es responder
     * a legados: 0: error, 1: Ok, y 2: trabajar en legados
     */
    public function fire($job, $data)
    {
        $time_start = microtime(true);
        //$urlTest = \Config::get("legado.wsdl.respuestast_test");
        //\Config::set("legado.wsdl.respuestast", $urlTest);
        //$id = $job->getJobId();
        Auth::loginUsingId(\Config::get("legado.user_logs.genera_solicitud"));
        $objstimplements = new STRepository;
        $aid=$tipoEnvioOfsc=$resourceId=$actividadId=null;
        $msjRptaLegado=$codError=$msjError=$msjErrorOfsc=$registroSt='';
        $idRespuesta = 1;//todo bien
        $logId = $data["logId"];
        $tramaarrays = []; //para inbound helper

        $componentesArray = $data["request"]["componentes"];
        if( !isset($componentesArray["componente"]) ) {
           Log::useDailyFiles(storage_path().'/logs/solicitudtecnica_sincomponentes.log');
           Log::info([$data]);
        }
        unset($data["request"]["componentes"]);
        foreach ($data["request"] as $key => $value) {
            $solicitudArray[$key] = trim($value);
        }


        $idSolicitudTecnica = $solicitudArray['id_solicitud_tecnica'];
        $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $idSolicitudTecnica)
                    ->first();
        
        $stArray['tipo_operacion']=$solicitudArray['tipo_operacion'];
        $stArray['id_solicitud_tecnica']=$solicitudArray['id_solicitud_tecnica'];
        try {
            if (is_null($solicitud)
                || $solicitud->ultimo->estado_st == 0
                || $solicitud->ultimo->estado_st == 2) {
            }
        } catch (Exception $e) {
            $this->finish($time_start);
            Auth::logout();
            $job->delete();
            return;
        }
        
        if (is_null($solicitud) || $solicitud->ultimo->estado_st == 0 || $solicitud->ultimo->estado_st == 2) {
            echo "here  \n";
            $validator = Validator::make($solicitudArray, SolicitudTecnicaCms::$rules);
            if ($validator->fails()) {
                $msjError = $validator->messages()->all()[0];
                $codError = SolicitudTecnicaHelper::getErrorByMessage($msjError);
                $msjRptaLegado = $codError.':'.$msjError;
                $idRespuesta = 0;
            }
            //cuando llega un solo componente
            if( isset($componentesArray["componente"]) ) {
                if (!isset($componentesArray["componente"][0])) {
                    $componentes[0] = $componentesArray['componente'];
                 } else {
                    $componentes = $componentesArray['componente'];
                }
            } else {
               $componentes = [];
            }
            //$tramaarrays['componentes']=$componentes; //para inbound helper
            //validando componentes
            $componentesObj = [];
            foreach ($componentes as $key => $value) {
                if ($value['numero_requerimiento']!='' && $value['numero_ot']!='') {
                    $componentesObj[] = new ComponenteCms($value);
                }
            }
            $tramaarrays['componentes'] = $componentesObj;

            $estadoOfsc = 8;//no se envio a toa
            $estadoAseguramiento = \Config::get("legado.web_aseguramiento");
            
            $ultimo = [
                'estado_ofsc_id'    => $estadoOfsc,
                'estado_st'         => $idRespuesta,
                'estado_aseguramiento'=> $estadoAseguramiento
            ];
            //crear objeto solcititud cms
            if (is_null($solicitud)) {
                $solicitudCms = new SolicitudTecnicaCms($solicitudArray);
                $solicitud = new SolicitudTecnica($stArray);
                $registroSt = 'nuevo';
                $solicitud->save();

                $solicitud->cms()->save($solicitudCms);
                $ultimoObj = new Ultimo($ultimo);
                $solicitud->ultimo()->save($ultimoObj);
            } else {
                $solicitudCms=$solicitud->cms;
                $solicitudCms->update($solicitudArray);

                $ultimoObj=$solicitud->ultimo;
                $ultimoObj['estado_ofsc_id']    = $estadoOfsc;
                $ultimoObj['estado_st']         = $idRespuesta;
                $ultimoObj['estado_aseguramiento'] = $estadoAseguramiento;
                $ultimoObj->save();
            }

            $actividadId = $solicitudCms->getActividadId();

            $movimiento = $ultimo;
            $movimientoObj = new Movimiento($movimiento);
            $solicitud->movimientos()->saveMany([$movimientoObj]);

            //si id respuesta es 0, hubo error en la validacion
            $arrayvalidacion = array();
            $arrayvalidacion['tipo'] = 1; //
            $arrayvalidacion['estado'] = 1;
            $arrayvalidacion['proveniencia'] = 2; //CMS
            if ($solicitudCms->cod_nodo != "") {
                $arrayvalidacion['filtrodetalle'] = $solicitudCms->cod_nodo;
            }

            $arrayvalidacionEnvPsiTest = array();
            $arrayvalidacionEnvPsiTest['tipo'] = 6; //
            $arrayvalidacionEnvPsiTest['estado'] = 1;
            $arrayvalidacionEnvPsiTest['proveniencia'] = 2; //CMS

            $respuesta = [
                'id_solicitud_tecnica'  => $idSolicitudTecnica,
                'id_respuesta'          => $idRespuesta,
                'observacion'           => $msjRptaLegado,
                'solicitud_tecnica_id'  => $solicitud->id
            ];

            if (!$solicitudCms->validaCmsGo($arrayvalidacion)) {
                $idRespuesta = 2;
                $estadoAseguramiento = 2;
                //$estadoOfsc = 8;
                $error = Error::find(11);
                $msjError = $error->message.': '.$solicitudCms->mensaje;
                $msjRptaLegado = $error->code.':'.$error->message;
                $respuesta['id_respuesta'] = 2;
                $respuesta['observacion'] = $msjRptaLegado;
                $this->responderLegado($respuesta);
                ////responder legado
            } else {
                $this->responderLegado($respuesta);

                $solicitudDatos = [
                    'estado_aseguramiento' => $estadoAseguramiento,
                    'solicitud' => $solicitud,
                    'solicitud_cms' => $solicitudCms,
                    'actividad_id' => $actividadId,
                    'id_respuesta' => $idRespuesta,
                    'log_id' => $logId,
                    'tramaarrays' => $tramaarrays
                ];
                //dd($solicitudCms->quiebre_id);
                $valores = $this->realizarValidaciones($solicitudDatos, $msjError);
                
                $error = $valores['error'];   
                $msjError = $valores['msj_error'];
                $estadoAseguramiento = $valores['estado_aseguramiento'];
                //$solicitudCms = $valores['solicitud_cms'];
                //echo(var_dump($solicitudCms));
                //dd();
            }
        //solicitud tecnica ya se encuentra registrada en go
        } elseif ($solicitud->ultimo->estado_st == 1) {
            $error = Error::find(1);
            $respuesta = [
                'id_solicitud_tecnica'  => $idSolicitudTecnica,
                'id_respuesta'          => $idRespuesta,
                'observacion'           => $error->code.":".$error->message,
                'solicitud_tecnica_id'  => $solicitud->id
            ];
            //Queue::push('Legados\EnviarLegado', $respuesta, 'legadorespuesta_st');
            //$this->responderLegado($respuesta);
            $objstimplements->saveLogCache($solicitud->id);
            $this->actualizarLogRecepcion($error, $error->message, $solicitud->id, $logId);
            $this->finish($time_start);
            Auth::logout();
            $job->delete();

            return;
        }
        //generar movimiento y actualizar solicitud tecnica
        
        //comentar
        //$bucket = Bucket::where('bucket_ofsc', $resourceId)->first();
        //actualizar $ultimo
        // $ultimoObj['aid']               = $aid;
        // $ultimoObj['bucket_id']         = "";
        // $ultimoObj['estado_ofsc_id']    = $estadoOfsc;
        // $ultimoObj['resource_id']       = $resourceId;
        // $ultimoObj['tipo_envio_ofsc']   = $tipoEnvioOfsc;
        $ultimoObj['actividad_id']      = $actividadId;
        $ultimoObj['actividad_tipo_id'] = $solicitudCms->getActividadTipoId();
        $ultimoObj['quiebre_id']        = $solicitudCms->quiebre_id;//s
        $ultimoObj['estado_legado']     = $solicitudCms->estado;
        $ultimoObj['estado_st']         = $idRespuesta;
        $ultimoObj['estado_aseguramiento'] = $estadoAseguramiento;
        $ultimoObj['tipo_legado']       = 1;
        $ultimoObj['xa_note']           = $solicitudCms['observacion'];
        $ultimoObj['xa_observation']    = '';
        $ultimoObj['num_requerimiento'] = $solicitudCms['num_requerimiento'];
        $ultimoObj['num_orden_trabajo'] = $solicitudCms['orden_trabajo'];
        $ultimoObj['num_orden_servicio'] = $solicitudCms['cod_servicio'];
        //$ultimoObj['work_zone']         = $solicitudCms['workZone'];

        if ($registroSt == 'nuevo') {
            echo "nueva st  \n";
            //$solicitudCms->parsearFechas();
            //$solicitudCms->save();
            $solicitud->cms->componentes()->saveMany($componentesObj);
            //generar movimiento
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitud, $ultimoObj->toArray(), []);

        } else {
            $solicitud->update($stArray);
            $solicitudArray = SolicitudTecnicaHelper::parsearFechas($solicitudArray);
            $solicitud->cms()->update($solicitudArray);
            $solicitud->cms->componentes()->delete();
            $solicitud->cms->componentes()->saveMany($componentesObj);
            //generar movimiento
            $objgestion = new StGestion;
            $objgestion->registrarMovimiento($solicitud, $ultimoObj->toArray(), []);
        }
        //actualizar log st
        $this->actualizarLogRecepcion($error, $msjError, $solicitud->id, $logId);
        $objstimplements->saveLogCache($solicitud->id);
        echo 'abajo2';
        $this->finish($time_start);
        Auth::logout();
        $job->delete();

        return;
    }


    public function responderLegado($respuesta)
    {
        try {
            $objApiST = new STCmsApi;
            $objApiST->responderST($respuesta);
        } catch (Exception $e) {}

        return;
    }

    public function actualizarLogRecepcion($error, $msjError = null, $solicitudId, $logId) {
        $objlogst = STLogRecepcion::find($logId);
        if (!is_null($objlogst)) {
            $objlogst->update([
                'code_error'            => (is_null($error)) ? '': $error->code,
                'solicitud_tecnica_id'  => $solicitudId,
                'descripcion_error'     => $msjError
            ]);
        }
        return;
    }

    public function realizarValidaciones($solicitudDatos, $msjError) {

        
        $idRespuesta = $solicitudDatos['id_respuesta'];
        $estadoAseguramiento = $solicitudDatos['estado_aseguramiento'];
        $actividadId = $solicitudDatos['actividad_id'];
        $solicitud = $solicitudDatos['solicitud'];
        $solicitudCms = $solicitudDatos['solicitud_cms'];
        $error = $aid = $resourceId = null;
        $actividadTipoId = '';
        $objstimplements = new STRepository;

        $arrayvalidacionMasiva = array( 'tipo' => 4, 'estado' => 1, 'proveniencia' => 2);
        // validacion de masiva
        if ($idRespuesta == 0) {
            $estadoAseguramiento = 10;
        //validar parametrizacion
        } elseif (is_null($solicitudCms->getQuiebreId2($actividadId))) {
            $error = Error::find(24);
            $msjError = json_encode($solicitudCms->getMsjErrorContent());
        } elseif (is_null($solicitudCms->getActividadTipoId())) {
            $error = Error::find(25);
            $msjError = json_encode($solicitudCms->getMsjErrorContent());
        } elseif (is_null($solicitudCms->getWorkZone())) {
            $error = Error::find(26);
            $msjError = $error->message;
        } else if ($solicitudCms->validaMasiva($arrayvalidacionMasiva)) {
            $estadoAseguramiento = 12;
        } else {
            /*if ($objstimplements->validarOtActiva($solicitudCms['num_requerimiento'])) {
                $estadoAseguramiento = 11;}*/
            //web de aseguramiento: 1: envio automatico, 0: envio manual
            if ($estadoAseguramiento == 1) {
                $arrayvalidacion = array();
                $arrayvalidacion['tipo'] = 2; //GOTOA
                $arrayvalidacion['estado'] = 1;
                $arrayvalidacion['proveniencia'] = 2; //CMS

                if ($solicitudCms->cod_nodo != "") {
                    $arrayvalidacion['filtrodetalle'] = $solicitudCms->cod_nodo;
                }

                if ($solicitudCms->validaGoToa($arrayvalidacion)) {

                    $solicitudCms = $objstimplements->sanearCoordenadasCms($solicitudCms);
                    unset($solicitudCms['actividad_tipo_id']);
                    unset($solicitudCms['workZone']);
                    unset($solicitudCms->mensaje);
                    $solicitudCms->parsearFechas();
                    $solicitudCms->save();

                    $solicitudDatos['quiebre_id'] =$solicitudCms->quiebre_id;
                    
                    Queue::push('Legados\queue\ComunicacionToa', $solicitudDatos, 'comunicaciontoa_st');
                    return array('error' => $error, 
                                'msj_error' => $msjError, 
                                'estado_aseguramiento' => $estadoAseguramiento,
                                'actividad_tipo_id' => $actividadTipoId,
                                'solicitud_cms' => $solicitudCms);
                } else {
                    $error = Error::find(23);
                    $msjError = $error->message.': '.$solicitudCms->mensaje;
                }
            } else if($estadoAseguramiento == 11) {
                $msjError = "En requerimiento ya existe con una OT ya activa";
            } else {
               $msjError = "No Validado en Web de aseguramiento";
            }
        }

        $solicitudCms = $objstimplements->sanearCoordenadasCms($solicitudCms);
        unset($solicitudCms['actividad_tipo_id']);
        unset($solicitudCms['workZone']);
        unset($solicitudCms->mensaje);
        $solicitudCms->parsearFechas();
        $solicitudCms->save();

        return array('error' => $error, 
            'msj_error' => $msjError, 
            'estado_aseguramiento' => $estadoAseguramiento,
            'actividad_tipo_id' => $actividadTipoId,
            'solicitud_cms' => $solicitudCms);
    }

    public function finish($time_start)
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        if ($execution_time > 62) {
            DB::table('errores')->insert(
                [
                "date" => date("Y-m-d H:i:s"),
                "file" => realpath(dirname(__FILE__))."/".basename(__FILE__),
                "message" => "Proceso en cola 'generar_st' se está demorando mas de 60 segundos.(Tiempo de proceso: ".$execution_time." segundos)",
                "line" => "function fire()",
                "url" => "",
                "usuario_id" => 1130
                ]
            );
        }
    }
}