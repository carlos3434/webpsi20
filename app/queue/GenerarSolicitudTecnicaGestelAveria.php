<?php
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaGestelAveria as Averia;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\ComponenteGestel;
use Legados\models\Gesteldominio;
use Legados\models\Gestelproductos;
use Legados\helpers\InboundHelper;
use Legados\helpers\SolicitudTecnicaHelper;
use Legados\STCmsApi;
use Legados\models\Error;
use Legados\models\SolicitudTecnicaLogRecepcion as STLogRecepcion;

use Legados\Repositories\SolicitudTecnicaRepository as STRepository;

/**
 * php artisan queue:listen --queue=generar_st_averia
 */
class GenerarSolicitudTecnicaGestelAveria
{
    public function failed()
    {
        // Called when the job is failing...
        Log::error('Job failed!');
    }
    /**
     * metodo que ejecuta la cola de solicitud tecnica, su objetivo es responder
     * a legados: 0: error, 1: Ok, y 2: trabajar en legados
     */
    public function fire($job, $data)
    {
        $time_start = microtime(true);
        //$id = $job->getJobId();
        // $urlTest = \Config::get("legado.wsdl.respuestast_desarrollo");
        // \Config::set("legado.wsdl.respuestast", $urlTest);
        Auth::loginUsingId(\Config::get("legado.user_logs.genera_solicitud_gestel"));
        $aid=$tipoEnvioOfsc=$resourceId=$actividadTipoId=$quiebreId=$actividadId=null;
        $msjRptaLegado=$codError=$msjError=$msjErrorOfsc=$registroSt='';
        $idRespuesta=1;//todo bien

        $logId = $data["logId"];
        $objstimplements = new STRepository;
        try {
             $dominiosArray =
                (isset($data["request"]["datos_dominio"])) ? 
                $data["request"]["datos_dominio"] : array();

            $productosAdquiridosArray = 
                (isset($data["request"]["productos_adquiridos"])) ? 
                $data["request"]["productos_adquiridos"] : array();

            $componentesArray = 
                (isset($data["request"]["componentes"])) ? 
                $data["request"]["componentes"] : array();


            //comunes Averias y Averiaes
            unset($data["request"]["datos_dominio"]);
            unset($data["request"]["productos_adquiridos"]);
            unset($data["request"]["componentes"]);

            foreach ($data["request"] as $key => $value) {
                $solicitudArray[$key] = trim($value);
            }
        } catch (Exception $e) {}
        $idSolicitudTecnica = $solicitudArray['id_solicitud_tecnica'];
        $solicitud = SolicitudTecnica::where("id_solicitud_tecnica", $idSolicitudTecnica)
                    ->first();
        
        $stArray['tipo_operacion']=$solicitudArray['tipo_operacion'];
        $stArray['id_solicitud_tecnica']=$solicitudArray['id_solicitud_tecnica'];
        try {
            if (is_null($solicitud) || 
                $solicitud->ultimo->estado_st==0 || 
                $solicitud->ultimo->estado_st==2) {
            }
        } catch (Exception $e) {
            $this->finish($time_start);
            Auth::logout();
            $job->delete();
            return;
        }
        if (is_null($solicitud) || $solicitud->ultimo->estado_st==0 || $solicitud->ultimo->estado_st==2) {
            //solciitud no existe, no enviada o con error
            $validator = Validator::make($solicitudArray, Averia::$rules);
            if ($validator->fails()) {
                $msjError = $validator->messages()->all()[0];
                $codError = SolicitudTecnicaHelper::getErrorByMessage($msjError);
                $msjRptaLegado = $codError.':'.$msjError;
                $idRespuesta = 0; //si id respuesta es 0, hubo error en la validacion
            }

            $estadoOfsc = 8;//no se envio a toa
            $estadoAseguramiento = \Config::get("legado.web_aseguramiento");

            $ultimo = [
            'estado_ofsc_id'    => $estadoOfsc,
            'estado_st'         => $idRespuesta,
            'estado_aseguramiento'=> $estadoAseguramiento
            ];
            
            //si no existe solicitud
            if (is_null($solicitud)) {
                //crear objeto solcititud Averia
                $solicitudGestel = new Averia($solicitudArray);

                $solicitud = new SolicitudTecnica($stArray);
                $registroSt = 'nuevo';
                $solicitud->save();

                $solicitud->gestelAveria()->save($solicitudGestel);
                $ultimoObj = new Ultimo($ultimo);
                $solicitud->ultimo()->save($ultimoObj);
            } else {
                $solicitudGestel=$solicitud->gestelAveria;
                $solicitudGestel->update($solicitudArray);

                $ultimoObj=$solicitud->ultimo;
                $ultimoObj['estado_ofsc_id']    = $estadoOfsc;
                $ultimoObj['estado_st']         = $idRespuesta;
                $ultimoObj['estado_aseguramiento']= $estadoAseguramiento;
                $ultimoObj->save();
            }

            $movimiento=$ultimo;
            $movimientoObj= new Movimiento($movimiento);
            $solicitud->movimientos()->saveMany([$movimientoObj]);

            $actividadId = $solicitudGestel->getActividadId();

            if (isset($dominiosArray['dato_dominio'])) {
                if (!isset($dominiosArray['dato_dominio'][0])) {
                    $dominios[0] = $dominiosArray['dato_dominio'];
                } else {
                    $dominios= $dominiosArray['dato_dominio'];
                }
                $dominiosObj=[];
                $i=0;
                foreach ($dominios as $key => $value) {
                    if((isset($value['cod_ip']) && $value['cod_ip']!=='') || 
                       (isset($value['mascara_ip']) && $value['mascara_ip']!=='')) {
                    $dominiosObj[$i] = new Gesteldominio($value);
                    $dominiosObj[$i]['actividad_id']=1;
                    $i++;
                    }
                }
                $tramaarrays['dominio']=$dominiosObj;
            }
            
            if (isset($productosAdquiridosArray['producto_adquirido'])) {
                if (!isset($productosAdquiridosArray['producto_adquirido'][0])) {
                    $productosAdquiridos[0] = $productosAdquiridosArray['producto_adquirido'];
                } else {
                    $productosAdquiridos= $productosAdquiridosArray['producto_adquirido'];
                }
                $productosAdquiridosObj=[];
                $i=0;
                foreach ($productosAdquiridos as $key => $value) {
                    if ((isset($value['serv_adquirido']) && $value['serv_adquirido']!='') ||
                        (isset($value['desc_serv_adquirido']) && $value['desc_serv_adquirido']!='')) {
                    $productosAdquiridosObj[$i] = new Gestelproductos($value);
                    $productosAdquiridosObj[$i]['actividad_id']=1;
                    $i++;
                    }
                }
                $tramaarrays['productos_adquiridos']= $productosAdquiridosObj;
            }

            if (isset($componentesArray["componente"])) {
                if (!isset($componentesArray["componente"][0])) {
                    $componentes[0] = $componentesArray['componente'];
                } else {
                    $componentes= $componentesArray['componente'];
                }
                $componentesObj=[];
                $i=0;
                foreach ($componentes as $key => $value) {
                    if ($value['cod_material']!='') {
                        $componentesObj[$i] = new ComponenteGestel($value);
                        $componentesObj[$i]['actividad_id']=1;
                        $i++;
                    }
                }
                $tramaarrays['componentes']= $componentesObj;
            }

            $arrayvalidacion=array();
            $arrayvalidacion['tipo']=1; //LEGO
            $arrayvalidacion['estado']=1;
            $arrayvalidacion['proveniencia']=4;
            
            if ($idRespuesta==0) {
                $responderLegado=true;
            } else if (!$solicitudGestel->validarParam($arrayvalidacion)) {
                $idRespuesta=2;
                //$estadoOfsc = 8;
                $error = Error::find(11);
                $codError = $error->code;
                $msjError = $error->message.': '.$solicitudGestel->mensaje;
                $msjRptaLegado = $error->code.':'.$error->message;
                $responderLegado=true;
            } elseif ( is_null( $solicitudGestel->getQuiebreId() ) ) {
                $error = Error::find(24);
                $codError = $error->code;
                $msjError = $error->message;
                $responderLegado=true;
            } elseif ( is_null( $solicitudGestel->getActividadTipoId() ) ) {
                $error = Error::find(25);
                $codError = $error->code;
                $msjError = $error->message;
                $responderLegado=true;
            } elseif ( is_null( $solicitudGestel->getWorkZone() ) ) {
                $error = Error::find(26);
                $codError = $error->code;
                $msjError = $error->message;
                $responderLegado=true;
            } else {

                $quiebreId = $solicitudGestel->quiebre_id;
                $actividadTipoId = $solicitudGestel->actividad_tipo_id;
                if ( $estadoAseguramiento==1 ) {
                    $arrayvalidacion=array();
                    $arrayvalidacion['tipo']=2; //GOTOA
                    $arrayvalidacion['estado']=1;
                    $arrayvalidacion['proveniencia']=4; //GESTEL AVERIA

                    if ($solicitudGestel->validarParam($arrayvalidacion)) {

                        $respuesta = $solicitudGestel->capacityOfsc();
                        //hay bucket para st
                        if (is_array($respuesta)) {
                            $parametros = $respuesta;
                            $parametros['tipoEnvio'] = 'sla';
                            $parametros['nenvio'] = 1;
                            $parametros['fechaAgenda'] = '';
                            $parametros['timeSlot'] = '';
                            $parametros['tipo_legado'] = 2;
                            $parametros['actividad']=$actividadId; 
                            
                            // \Config::set("ofsc.auth.company", "telefonica-pe.test");

                            $inventarios = [];
                            //$inventarios=$solicitudGestel->getMateriales();

                            $response = InboundHelper::envioOrdenOfscAveria($solicitudGestel, $parametros,$tramaarrays, $inventarios);
                            
                            if (isset($response->data->data->commands->command->appointment->aid)) {
                                $aid = $response->data->data->commands->command->appointment->aid;
                                $estadoOfsc = 1;
                                $tipoEnvioOfsc = 2;
                                $msjError = "Envio a OFSC: OK";
                                $resourceId = $respuesta['external_id'];
                            } else {
                                if (isset($response->data->data->commands->command->appointment->report->message)) {
                                    if (isset($response->data->data->commands->command->appointment->report->message->description)) {
                                        $msjErrorOfsc=$response->data->data->commands->command->appointment->report->message->description;
                                    } else {
                                        $mensajes=$response->data->data->commands->command->appointment->report->message;
                                        foreach ($mensajes as $key => $value) {
                                            if(isset($value->result)) {
                                                if($value->result=="error" || $value->result=="warning") {
                                                    $msjErrorOfsc.=$value->description.' || ';
                                                }
                                            }
                                        }
                                    }
                                }
                                $msjError="Envio a OFSC: FAIL|".$msjErrorOfsc;
                                $codError='ERR22';//go debe reenviar
                            } 
                        } else {
                            $msjError="Consulta CAPACIDAD a OFSC: FAIL|".$respuesta;
                            $codError='ERR12';//go debe reenviar
                        } 
                    } else {
                        $error = Error::find(23);
                        $codError = $error->code;
                        $msjError = $error->message.': '.$solicitudGestel->mensaje;
                    }
                    $responderLegado=true;
                } else {
                    //no se responde a legados
                    $responderLegado=false;
                    $msjError="No Validado en Web de aseguramiento";
                }
            }
            //solicitud tecnica ya se encuentra registrada en go
        } elseif ($solicitud->ultimo->estado_st == 1) { //solicitud OK enviado a toa
            $error = Error::find(1);
            $codError=$error->code;
            $msjError=$error->message;
            STLogRecepcion::find($logId)->update(
                [
                'code_error'            => $codError,
                'solicitud_tecnica_id'  => $solicitud->id,
                'descripcion_error'     => $msjError
                ]
            );
            $respuesta = [
                'id_solicitud_tecnica'  => $idSolicitudTecnica,
                'id_respuesta'          => $idRespuesta,
                'observacion'           => $error->code.":".$error->message,
                'solicitud_tecnica_id'  => $solicitud->id
            ];
            
            $objApiST = new STCmsApi('GESTEL Legado');
            $objApiST->responderST($respuesta);
            $this->finish($time_start);
            Auth::logout();
            $job->delete();
            return;
        }

        $ultimoObj['aid']               = $aid;
        $ultimoObj['estado_ofsc_id']    = $estadoOfsc;
        $ultimoObj['resource_id']       = $resourceId;
        $ultimoObj['tipo_envio_ofsc']   = $tipoEnvioOfsc;
        $ultimoObj['actividad_id']      = $actividadId;
        $ultimoObj['actividad_tipo_id'] = $actividadTipoId;
        $ultimoObj['quiebre_id']        = $quiebreId;
        $ultimoObj['estado_legado']     = $solicitudGestel->estado;
        $ultimoObj['estado_st']         = $idRespuesta;
        $ultimoObj['estado_aseguramiento'] = $estadoAseguramiento;
        $ultimoObj['tipo_legado']       = 2;
        $ultimoObj['nenvio']            = $solicitudGestel->nenvio;
        $ultimoObj['xa_note']           = $solicitudGestel->desobs;
        $ultimoObj['num_requerimiento'] = $solicitudGestel['cod_averia'];

        $movimiento = [
            'aid'               => $aid,
            'estado_ofsc_id'    => $estadoOfsc,
            'resource_id'       => $resourceId,
            'tipo_envio_ofsc'   => $tipoEnvioOfsc,
            'actividad_id'      => $actividadId,
            'actividad_tipo_id' => $actividadTipoId,
            'quiebre_id'        => $quiebreId,
            'estado_legado'     => $solicitudGestel->estado,
            'estado_st'         => $idRespuesta,
            'estado_aseguramiento'=> $estadoAseguramiento,
            'xa_note'           => $solicitudGestel->desobs
        ];

        unset($solicitudGestel->actividad_tipo_id);
        unset($solicitudGestel->workZone);
        unset($solicitudGestel->mensaje);

        if ($registroSt=='nuevo') {
            $solicitudGestel->parsearFechas();
            $solicitud->gestelAveria()->save($solicitudGestel);
            
            $ultimoObj->save();

            $movimientoObj= new Movimiento($movimiento);
            $solicitud->movimientos()->saveMany([$movimientoObj]);

        } else {
            $solicitud->update($stArray);

            $solicitudArray = SolicitudTecnicaHelper::parsearFechas($solicitudArray);
            $solicitud->gestelAveria()->update($solicitudArray);

            $movimientoObj= new Movimiento($movimiento);
            $solicitud->movimientos()->saveMany([$movimientoObj]);

            $ultimoObj->save();

            $solicitud->gestelAveria->componentes()->delete();
            $solicitud->gestelAveria->dominio()->delete();
            $solicitud->gestelAveria->productos()->delete();

        }

        if(isset($componentesObj))
        $solicitud->gestelAveria->componentes()->saveMany($componentesObj);
        if(isset($dominiosObj))
        $solicitud->gestelAveria->dominio()->saveMany($dominiosObj);
        if(isset($productosAdquiridosObj))
        $solicitud->gestelAveria->productos()->saveMany($productosAdquiridosObj);

        STLogRecepcion::find($logId)->update(
            [
            'code_error'            => $codError,
            'solicitud_tecnica_id'  => $solicitud->id,
            'descripcion_error'     => $msjError
            ]
        );

        if ($responderLegado==true) {
            //armar respuesta
            $respuesta = [
                'id_solicitud_tecnica'  => $idSolicitudTecnica,
                'id_respuesta'          => $idRespuesta,
                'observacion'           => $msjRptaLegado,
                'solicitud_tecnica_id'  => $solicitud->id
            ];
            //enviar respuesta a legados
            $objApiST = new STCmsApi('GESTEL Legado');
            $objApiST->responderST($respuesta);
        }
        $objstimplements->saveLogCache($solicitud->id);
        $this->finish($time_start);
        Auth::logout();
        $job->delete();
        return;
    }

    public function finish($time_start){
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        if( $execution_time > 62 ){
            DB::table('errores')->insert([
                "date" => date("Y-m-d H:i:s"),
                "file" => realpath(dirname(__FILE__))."/".basename(__FILE__),
                "message" => "Proceso en cola 'generar_st_averia' se está demorando mas de 60 segundos.(Tiempo de proceso: ".$execution_time." segundos)",
                "line" => "function fire()",
                "url" => "",
                "usuario_id" => 1130
            ]);
        }
    }
}
