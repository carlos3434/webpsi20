<?php
use Legados\models\ComponenteCms;
use Legados\STCmsApi;
use Legados\models\Error;

class GenerarUssd
{
    public function failed()
    {
        Log::error('Job failed!');
    }

    public function fire($job, $data)
    {   
        $request = $data['request'];
        try{            
            $asunto = strtolower(trim($request['asunto']));
            $actividad = strtolower(trim($request['actividad']));
            $requerimiento = strtolower(trim($request['requerimiento']));
            $serieDeco = strtolower(trim($request['serieDeco']));
            $serieTarjeta = strtolower(trim($request['serieTarjeta']));
            $telefonoOrigen = (trim($request['telefonoOrigen']));
            $clave = ($request['clave']) ? $request['clave'] : '';
            $valor = ($request['valor']) ? $request['valor'] : '';
            $fecha = date('Y-m-d H:i:s'); 
            $type = 1;
            $interfaz=Input::get('interfaz',0);

            DB::table('tareas')
                ->insert(
                    array(
                        'asunto' => $asunto,
                        'actividad' => $actividad,
                        'requerimiento' => $requerimiento,
                        'serie_deco' => $serieDeco,
                        'serie_tarjeta' => $serieTarjeta,
                        'telefono_origen' => $telefonoOrigen,
                        'created_at' => date('Y-m-d H:i:s'),
                        'usuario_created_at' => Auth::id(),
                        'clave' => $clave,
                        'valor' => $valor,
                        'procesado' => 0,
                       )
                );

            if ($telefonoOrigen != '' && is_numeric($telefonoOrigen)) {
                $telOrig = substr($telefonoOrigen, strlen($telefonoOrigen) - 9);

                if ( $asunto == 'refresh' || $asunto == 'activacion') {

                    if ($actividad=='averias') { 
                        $actividadId=1;
                    } elseif($actividad=='provision'){
                        $actividadId=2;
                    }
                    $numser = $codmat = '';
                    try {
                        list($numser,$codmat) = explode(',', $serieDeco);
                    } catch (Exception $e) { }
                    if ($numser=='' || $codmat=='') {
                        $msj = "En serie del decodificador debe ingresar dos valores(serie,cod material)";
                        Sms::enviarSincrono($telefonoOrigen, $msj, '1');
                        $job->delete();
                        return;
                    }                

                    
                    $deco = DB::table('componente_cms as cc')
                            ->select(
                                'cc.componente_cod as codcomp',
                                'cc.indorigreq',
                                'cc.numero_requerimiento as numcompxreq',
                                'cc.id as deco_componente_id'
                            )
                            ->where('num_requerimiento', $requerimiento)
                            ->where('codigo_material', $codmat)
                            ->first();

                    //cuando viene del formulario de activacion 7 refresh, ussd, oficetrack
                    if (count($deco) == 0 && $asunto == "activacion") { 
                        $type = 0;
                        //mensaje si no existe requerimiento
                        $req = DB::table('codordtrab')
                                  ->from('schedulle_sistemas.prov_pen_cms_catv_pais')
                                  ->where('edoxot','A')
                                  ->where('codreq',$requerimiento)
                                  ->first();
                        if (count($req)==0) {
                            $msj = "No se encontro requerimiento $requerimiento";
                            Sms::enviarSincrono($telefonoOrigen, $msj, '1');
                            $job->delete();
                            return;
                        }

                        $deco = DB::table('schedulle_sistemas.prov_pen_cms_catv_pais_componentes')
                        ->select(
                            'numcompxreq','codcomp','indorigreq'
                        )
                        ->whereIn('codcomp', function($query) use ($codmat)
                        {
                            $query->select('cod_componente')
                                  ->from('cat_componentes')
                                  ->where('codmat',$codmat);
                        })
                        ->where('codordtrab', function($query) use ($requerimiento)
                        {
                            $query->select('codordtrab')
                                  ->from('schedulle_sistemas.prov_pen_cms_catv_pais')
                                  ->where('edoxot','A')
                                  ->where('codreq',$requerimiento);
                        })
                        //campo de edward para saber si se envio activacion
                        //0: sin envio, 1: envio exitoso, 2: ejecutado
                        ->where('operacion_deco',0)
                        ->first();
                    }

                    if ($asunto == "activacion") {
                        if (count($deco) > 0) {
                            $request = [
                                'codreq'                => $requerimiento,
                                'indorigreq'            => $deco->indorigreq,
                                'numcompxreq'           => $deco->numcompxreq,
                                'codmat'                => $codmat,
                                'numser'                => $numser,
                                'codtar'                => '07870141',
                                'numtar'                => $serieTarjeta,
                                'solicitud_tecnica_id'  => 0,
                                'telefono_origen'       => $telefonoOrigen,
                                'interface'             => $interfaz //0 es USSD, 1: OT
                            ];

                            $response = Helpers::ruta(
                                'componentelegado/activar', 'POST', $request, false
                            );
                            if ($response->rst==1) {
                                //actualizar  prov_pen_cms_catv_pais  set operacion_deco=1
                                $sql = "UPDATE schedulle_sistemas.prov_pen_cms_catv_pais_componentes
                                       SET operacion_deco = 1
                                       WHERE numcompxreq = ?";

                                DB::update($sql, [$deco->numcompxreq]);
                            }
                            $msj = "{$response->msj}:\n1.Requerimiento {$requerimiento}";
                            Sms::enviarSincrono($telefonoOrigen, "{$response->msj}:\n1.Requerimiento {$requerimiento}\n2.Codmat {$codmat}", '1');
                            $rst=1;
                        } else {
                            $msj = "No se encontro componentes:\n1.Requerimiento {$requerimiento}";
                            $rst=2;
                            Sms::enviarSincrono($telefonoOrigen, "No se encontro componentes:\n1.Requerimiento {$requerimiento}\n2.Codmat {$codmat}", '1');
                        }
                    } elseif ($asunto == "refresh") {                       
                        $request = [
                            'codreq'                => $requerimiento,
                            'indorigreq'            => ($actividadId == 1) ? 'A' : 'I',
                            'codmat'                => $codmat,
                            'numser'                => $numser,
                            'codtar'                => '07870141',
                            'solicitud_tecnica_id'  => 0,
                            'telefono_origen'       => $telefonoOrigen,
                            'interface'             => $interfaz, //0 es USSD, 1: OT
                        ];

                        $response = Helpers::ruta(
                            'componentelegado/refresh', 'POST', $request, false
                        );

                        $msj = "{$response->msj}:\n1.Requerimiento {$requerimiento}";
                        Sms::enviarSincrono($telefonoOrigen, "{$response->msj}:\n1.Requerimiento {$requerimiento}\n2.Codmat {$codmat}", '1');
                        $rst=1;
                    }
                }
            }
            $job->delete();
            return;
        } catch (Exception $e) {
            Helpers::saveError($e);
            $job->delete();
            return;
        }
        
    }
}
