<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\EnvioLegado;
use Legados\STCmsApi;

class ReenvioLegadoCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'psi:reenviolegado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reenviar a legado cada vez que haiga un error de data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {      
        Auth::loginUsingId(697);//proceso automatico
        $objApiST = new STCmsApi;
        
        $envioLegado = EnvioLegado::from('envio_legado as el')
                ->select(
                    'el.*'
                )
                ->where('el.response', 'like', '%"data":null%')
                ->get();

        if(!empty($envioLegado)){
            foreach ($envioLegado as $key => $value) {
                $request = json_decode($value['request']);
                switch ($value['accion']) {
                    case 'RptaEnvioSolicitudTecnica':
                        $data = [
                            'id_solicitud_tecnica' => $request->NumeroSolicitud,
                            'id_respuesta' => $request->IndicadorRespuesta,
                            'observacion' => $request->Observacion,
                            'solicitud_tecnica_id' => $value['solicitud_tecnica_id']
                        ];         
                        $objApiST->responderST($data);                
                        break;
                    case 'CierreSolicitudTecnica':
                        $cierre = json_decode(json_encode($request),true);
                        $cierre['componentes'] = $cierre['componente'];
                        unset($cierre['componente']);
                        $cierre['solicitud_tecnica_id'] = $value['solicitud_tecnica_id'];
                        $objApiST->cierre($cierre);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    /*protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }*/

}
