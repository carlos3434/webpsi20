<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Ofsc\Resources;
use Ofsc\Location;

class ProcesoOfscEnvioLocalizacionCommand extends Command
{
    protected $name = 'ofsc:enviolocalizacion';

    protected $description = 'Envio de Localizacion de tecnicos a TOA';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
      
        $resource = new Resources();
        $location = new Location();

        $buckets = OfscHelper::getFileResource();
        $tecnicos = OfscHelper::getFileResource("PR");
        $tecnicosResultado = [];

        foreach ($tecnicos as $key => $value) {
            $locationOfsc = $location->getPosition($value["id"]);
            $yToa = $yOt = $xToa = $xOt = $timeToa = $timeOt = 0;
            $tecnico = [];
                      //validar respuesta de toa
            if (isset($locationOfsc->error) && $locationOfsc->error === false) {
                if (isset($locationOfsc->data->coords->longitude)) {
                    $xToa = $locationOfsc->data->coords->longitude;
                }
                if (isset($locationOfsc->data->coords->latitude)) {
                    $yToa = $locationOfsc->data->coords->latitude;
                }
                if (isset($locationOfsc->data->coords->time)) {
                    $timeToa = $locationOfsc->data->coords->time;
                    $datetime = new DateTime($timeToa);
                  //la fecha y hora de toa llega con diferencia de 5
                    $datetime->sub(new DateInterval('PT5H'));
                    $timeToa = $datetime->format('Y-m-d H:i:s');
                }
                      //consultar ot
                $ubicacion = DB::table('webpsi_officetrack.ultimas_coordenadas')
                  ->select('coord_x', 'coord_y', 'fecha_hora')
                  ->where('carnet', '=', $value['id'])
                  ->first();
                if (!is_null($ubicacion)) {
                    $xOt = str_replace(',', '.', $ubicacion->coord_x);
                    $yOt = str_replace(',', '.', $ubicacion->coord_y);
                    $timeOt = $ubicacion->fecha_hora;
                    $datetime = new DateTime($timeOt);
                    $timeOt = $datetime->format('Y-m-d H:i:s');
                }

                //obteniendo fecha mas reciente
                $coordxOT = "";
                $coordyOT = "";
                $coordxToa = "";
                $coordyToa = "";

                if (trim($timeOt) > trim($timeToa)) {
                    $time   = $timeOt;
                    $x      = $xOt;
                    $y      = $yOt;
                    $fuente = 'officetrak';
                    $coordxOT = $xOt;
                    $coordyOT = $yOt;
                } else {
                    $time   = $timeToa;
                    $x      = $xToa;
                    $y      = $yToa;
                    $fuente = 'toa';
                    $coordxToa = $xToa;
                    $coordyToa = $yToa;
                }
                $objTecnico = [
                    'time'=> $time,
                    'coord_x'=> $x,
                    'coord_y'=> $y,
                    'fuente'=> $fuente
                ];
                $tecnico = $objTecnico;
                $tecnico['name'] = $value['name'];
                $tecnico['phone'] = $value['phone'];
                $tecnico['status'] = $value['status'];
                
                
                try {
                    //actualizar la tabla de tecnicos
                    Tecnico::where('carnet_tmp', $value['id'])
                        ->where('estado', 1)
                        ->update($objTecnico);
                    $tecnico['xOt'] = $coordxOT;
                    $tecnico['yOt'] = $coordyOT;
                    $tecnico['xToa'] = $coordxToa;
                    $tecnico['yToa'] = $coordyToa;
                    $tecnico['timeToa'] = $timeToa;
                    $tecnico['timeOt'] = $timeOt;
                    
                    
                } catch (Exception $e) {
                }
                $historico = CoordenadaTecnicoOfscHistorico::where(["fecha" => date("Y-m-d"), "carnet" => $value["id"]])->get();
                if (count($historico) >= 10) {
                    CoordenadaTecnicoOfscHistorico::where(["fecha" => date("Y-m-d"), "carnet" => $value["id"]])->first()->delete();
                }
                $insert = [];
                $insert["carnet"] = $value["id"];
                $insert["fecha"] = date("Y-m-d");
                $insert["coordx"] = $tecnico["coord_x"];
                $insert["coordy"] = $tecnico["coord_y"];
                $insert["tecnico_id"] = isset($tecnico["tecnico_id"])? $tecnico["tecnico_id"] : null;
                CoordenadaTecnicoOfscHistorico::insert($insert);
                $tecnicosResultado[$value["parent_id"]][$value["id"]] = $tecnico;
            }
        }
        foreach ($tecnicosResultado as $key => $value) {
            $hoy =  date('Ymd');
            if ($key!=="") {
                $url = public_path()."/json/tecnicos/$key/";
                $existe = FileHelper::validaCrearFolder($url);
                if ($existe) {
                    $url.= $hoy.".json";
                    File::put($url, json_encode($value));
                }
            }
            
        }
    }
}
