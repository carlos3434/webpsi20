<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Ofsc\Resources;
use Ofsc\Location;

class ProcesoOfscEnvioLocalizacionCommand extends Command
{
    protected $name = 'ofsc:enviolocalizacion';

    protected $description = 'Envio de Localizacion de tecnicos a TOA';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        function array_to_xml( $data, &$xml_data )
        {
            foreach ( $data as $key => $value ) {
                if ( is_array($value) ) {
                    if ( is_numeric($key) ) {
                        $key = 'item'.$key; //dealing with <0/>..<n/> issues
                    }
                    $subnode = $xml_data->addChild($key);
                    array_to_xml($value, $subnode);
                } else {
                    $xml_data->addChild("$key", htmlspecialchars("$value"));
                }
            }
        }
        $resource = new Resources();
        $location = new Location();

        $response = $resource->getResources('LARI_INS_MOVISTAR_UNO');
        if (isset($response->error) && $response->error === false) {
            $totalRegistro = $response->totalResults;
            $array = $response->data;
            $tecnico = [];
            foreach ($array as $value) {
                if ( $value['status'] =='active' ) {
                    //consultar localizacion
                    $result = $location->getPosition($value['id']);
                    $yToa = $yOt = $xToa = $xOt = $timeToa = $timeOt = 0;
                    //validar respuesta de toa
                    if (isset($result->error) && $result->error === false) {
                        if ( isset($result->data->coords->longitude) )
                            $xToa = $result->data->coords->longitude;
                        if ( isset($result->data->coords->latitude) )
                            $yToa = $result->data->coords->latitude;
                        if ( isset($result->data->coords->time) ) {
                            $timeToa = $result->data->coords->time;
                            $datetime = new DateTime($timeToa);
                            //la fecha y hora de toa llega con diferencia de 5
                            $datetime->sub(new DateInterval('PT5H'));
                            $timeToa = $datetime->format('Y-m-d H:i:s');
                        }
                    }
                    //consultar ot
                    $ubicacion = DB::table('webpsi_officetrack.ultimas_coordenadas')
                            ->select('coord_x', 'coord_y','fecha_hora')
                            ->where('carnet', '=', $value['id'])
                            ->first();
                    if (!is_null($ubicacion)) {
                        $xOt = str_replace(',','.',$ubicacion->coord_x);
                        $yOt = str_replace(',','.',$ubicacion->coord_y);
                        $timeOt = $ubicacion->fecha_hora;
                        $datetime = new DateTime($timeOt);
                        $timeOt = $datetime->format('Y-m-d H:i:s');
                    }

                    //obteniendo fecha mas reciente
                    if(trim($timeOt) > trim($timeToa)){
                        $tecnico[ $value['id'] ] = [
                             'time'=>$timeOt,
                             'x'=>$xOt,
                             'y'=>$yOt,
                             'fuente'=>'officetrak',
                             'name' => $value['name'],
                             'phone' => $value['phone'],
                             'status' => $value['status']
                        ];
                    }else {
                        $tecnico[ $value['id'] ] = [
                             'time'=>$timeToa,
                             'x'=>$xToa,
                             'y'=>$yToa,
                             'fuente'=>'toa',
                             'name' => $value['name'],
                             'phone' => $value['phone'],
                             'status' => $value['status']
                        ];
                    }
                }
            }

        } else {
            return Response::json([]);
        }
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><tecnicos></tecnicos>');
        array_to_xml($tecnico, $xml_data);
        $xml_data->asXML(public_path().'/xml/tecnicos.xml');

        $buckets = ["LARI_INS_MOVISTAR_UNO"];
        $hoy =  date('Ymd');
        $rutaFolderJson = public_path()."/json";
        if (!File::exists($rutaFolderJson)){
          File::makeDirectory($rutaFolderJson, $mode = 0777, true, true);
  	    }
        $rutaFolderActividades = $rutaFolderJson."/tecnicos";

        if (!File::exists($rutaFolderActividades)){
          File::makeDirectory($rutaFolderActividades, $mode = 0777, true, true);
        }

        foreach ($buckets as $key => $value) {

          $folder = preg_replace("/-/", "_", $value);
          $rutaFolder = $rutaFolderActividades."/".$folder;
          if (!File::exists($rutaFolder)){
            File::makeDirectory($rutaFolder, $mode = 0777, true, true);
          }
          $rutaFile = $rutaFolder."/".$hoy.".json";
          File::put($rutaFile,json_encode($tecnico));

        }






    }
}
