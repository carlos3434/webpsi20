<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Ofsc\Capacity;

class OfscObtenerCapacidadCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:obtenercapacidad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'consulta la capacidad en ofsc y la almacena en redis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $fecha= date('Y-m-d');
        for ($i=0; $i <7; $i++) {
            $fechaMultiple[]=$fecha;
            $fecha = date("Y-m-d", strtotime($fecha . " + 1 days"));
        }

        $zonasTrabajoMUno = [
        /*'SP_C001',
        'SP_R001',
        'SP_R002',
        'SP_R003',
        'SP_R004',
        'SP_R005',
        'SP_R006',
        'SP_R007',
        'SP_R008',
        'SP_R009',
        'SP_R010',
        'SP_R011',
        'SP_R012',
        'SP_R013',
        'SP_R014',
        'SP_R015',
        'SP_R016',
        'SP_R017',
        'SP_R018',
        'SP_R019',
        'SP_R020',
        'SP_R021',
        'SP_R022',
        'SP_R023',
        'SP_R024',
        'SP_R025',
        'SP_R026',
        'SP_R027',

        'LL_R001',
        'LL_R002',
        'LL_R003',
        'LL_R004',
        'LL_R005',
        'LL_R006',
        'LL_R007',
        'LL_R008',
        'LL_R009',
        'LL_R010',
        'LL_R011',
        'LL_R012',
        'LL_R013',

        'LM_R001',
        'LM_R002',
        'LM_R003',
        'LM_R004',
        'LM_R005',
        'LM_R006',
        'LM_R007',
        'LM_R008',
        'LM_R009',
        'LM_R010',
        'LM_R011',
        'LM_R012',
        'LM_R013',
        'LM_R014',
        'LM_R015',
        'LM_R016',*/
        ];

        $dataPremium = DB::table("webpsi_coc.zona_premium_catv")->where("estado", "=", 1)->get();
        $zonasTrabajoPremium = [];
        foreach ($dataPremium as $key => $value) {
            $zonasTrabajoPremium[] = $value->nodo."_".$value->troba;
        }
        /*$zonasTrabajoPremium = [
        'MI_R001',
        'MI_R036',
        'MI_R037',
        'MI_R044',
        'MI_R047',
        'MI_R049',
        'MI_R002',
        'MI_R009',
        'MI_R035',
        'MI_R038',
        'MI_R039',
        'MI_R003',
        'MI_R004',
        'MI_R005',
        'MI_R006',
        'MI_R007',
        'MI_R008'
        ]; */
        /*
        $zonasTrabajoPremium = [

        'SB_R001',
        'SB_R007',
        'SB_R008',
        'SB_R009',
        'SB_R010',
        'SB_R011',
        'SB_R012',
        'SB_R013',
        'SB_R014',
        'SB_R016',
        'SB_R017',
        'SB_R018',
        'SB_R019',
        'SB_R020',
        'SB_R021',
        'SB_R022',
        'SB_R023',
        'SB_R024',
        'SB_R025',
        'SB_R026',
        'SB_R027',
        'SB_R028',
        'SB_R029',
        'SB_R030',
        'SB_R031',
        'SB_R032',
        'SB_R033',
        'SB_R034',
        'SB_R035',
        'SB_R036',
        'SB_R037',
        'SB_R038',
        'SB_R039',
        'SB_R043',
        'SB_R044',
        'SB_R045',
        'SB_R047'
        ];*/
        $actividadTipos = ActividadTipo::where('estado', 1)->get();
        foreach ($actividadTipos as $actividadTipo) {
            $worktype = $actividadTipo->label;
            //quiebres
            $quiebres = [
            'PENDMOV1',
            'PREMIUM','PREM_ALT_MONO_COMP_TRIO','PREM_ASIGNADO_A_CRITICOS',
            'PREM_DIGITALIZACION','PREM_INDPEX',
            'PREM_INFLUYENTE','PREM_M1-CRITICOS-PEX','PREM_MASPEX',
            'PREM_OBSERVADAS','PREM_PENDMOV1','PREM_PORTABILIDAD',
            'PREM_POST_DIGIT','PREM_QUIEBRE_TDP','PREM_R9-REIT-CATV',
            'PREM_REFERIDO','PREM_REIT','PREM_RETENCIONES'];
            for ($j=0; $j<count($quiebres); $j++) {

                $pos = strpos($quiebres[$j], "PREM");

                if ('PENDMOV1'==$quiebres[$j]) {
                    $zonas = $zonasTrabajoMUno;// continue;
                } elseif ($pos !== false) {
                    $zonas = $zonasTrabajoPremium;
                }
                $quiebre = $quiebres[$j];
                //zonas
                for ($i=0; $i<count($zonas); $i++) {
                    $data=[
                        'fecha'          => $fechaMultiple,
                        'time_slot'      => '',
                        'worktype_label' => $worktype,
                        'quiebre'        => $quiebre,
                        'zona_trabajo'   => $zonas[$i],
                    ];
                    $capacity = new Capacity();
                    try {
                        $response = $capacity->getCapacity($data);
                        //var_dump($response);
                    } catch (Exception $e) {
                        continue;
                    } catch (\SoapFault $fault) {
                        continue;
                    }
                    //validar si la consulta de la capacidad no tiene errores
                    //var_dump($response);
                    if (isset($response->error) && $response->error===false
                        && isset($response->data->capacity)) {
                        $capacity = $response->data->capacity;

                        $timeSlot = $response->data->time_slot_info;
                        foreach ($capacity as $key => $val) {
                            if (isset($val->work_skill)) {
                                $val->{"time_slot_info"} = $timeSlot;
                                $clave=$val->date.'|'.
                                       $val->time_slot.'|'.
                                       $worktype.'|'.
                                       $quiebre.'|'.
                                       $zonas[$i];
                                Redis::set($clave, json_encode($val));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            
        );
    }

}
