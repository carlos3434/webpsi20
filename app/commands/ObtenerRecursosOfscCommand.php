<?php

use Illuminate\Console\Command;

class ObtenerRecursosOfscCommand extends Command
{
    protected $name = 'ofsc:obtenerrecursos';

    protected $description = 'Obtener recursos de TOA';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        DB::table('arbol_recursos')->truncate();

        $url = 'recursoofsc/registrarrecursos';
        $method = 'GET';


        $resp = Helpers::ruta($url, $method,[]);

        echo $resp;

    }
}