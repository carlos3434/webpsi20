<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Activity;

class ActividadesOfscHistorico extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ofsc:actividadeshistorico';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'generar un json historicos de las actividades en ofsc por día.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

	    $buckets = ["LARI_INS_MOVISTAR_UNO"];
	    //$hoy = "2016-08-11 01:30:00";
	    $hoy = date("Y-m-d H:i:s");
	    $hora = date("G", strtotime($hoy));


	    $activity = new Activity();
	    $rutaFolderJson = public_path()."/json";
	    if (!File::exists($rutaFolderJson)){
	    	File::makeDirectory($rutaFolderJson, $mode = 0777, true, true);
	    }
	    $rutaFolderActividades = $rutaFolderJson."/actividades";

	     if (!File::exists($rutaFolderActividades)){
	    	File::makeDirectory($rutaFolderActividades, $mode = 0777, true, true);
	    }
	    if ($hora === "1") {
	    	foreach ($buckets as $key => $value) {
	    	$desde = '2016-07-13';//date("Y-m-d");
	    	$ayer = date("Y-m-d", strtotime("- 1 days"));
	    	$objDesde = new DateTime($desde);
		$objAyer = new DateTime($ayer);
		$interval = $objDesde->diff($objAyer);
		$diasTotal =  $interval->days;


		for ($i = 0; $i <= $diasTotal; $i++){
			$dia = date("Y-m-d", strtotime("$desde +$i days"));
			$folder = preg_replace("/-/", "_", $value);

			$rutaFolder = $rutaFolderActividades."/".$folder;
			$rutaFile = $rutaFolder."/".preg_replace("/-/", "", $dia).".json";
			$crearArchivo = false;

			// Elimino el archivo del ultimo día y creo uno nuevo
			// que contiene las actividades agendadas solo
			// en el día de ayer, el resto continua la logica
			if($i === $diasTotal){
				if (File::exists($rutaFile)){
					try{
						File::delete($rutaFile);	
					} catch (Illuminate\Filesystem\FileNotFoundException $ex){
						continue;
					}
					
				}
			}

			if (File::exists($rutaFolder)) {
				if(!File::exists($rutaFile)){
					$crearArchivo = true;
				}
			} else {
				File::makeDirectory($rutaFolder, $mode = 0777, true, true);
				if(!File::exists($rutaFile)){
					$crearArchivo = true;
				}
			}

			if ($crearArchivo === true){
				$resultado = $activity->getActivities($value,$dia, $dia);
				$actividades = $resultado->data;
				
				File::put($rutaFile,json_encode($actividades));
			}
		}
	        }
	    } else {
	    	if ($hora === "7" || $hora === "8" || $hora === "9"){
	    	foreach ($buckets as $key => $value) {
		    	$from = date("Y-m-d", strtotime($hoy));
		    	$to = date("Y-m-d", strtotime("$from +7 days"));
		    	$resultado = $activity->getActivities($value,$from, $to);
		    	//print_r($resultado);
			$actividades = $resultado->data;

			DB::table("actividades_ofsc_historico")
			->insert(
				array("data" => json_encode($actividades),
				    	"created_at" => $hoy,
				    	"user_at" => "952")
				);
	    	}
	    		echo "no se crea json";	
	    	} else {
	    	$diaInicio = "2016-07-13"; $diaFin = "2016-08-19";
	    	$dataFuente = [];
	    	$rutaFolderActividades =public_path()."/json/actividades/";
	    	foreach ($buckets as $key => $value) {
	                    $objInicio = new DateTime($diaInicio);
	                    $objFin= new DateTime($diaFin);
	                    $interval = $objInicio->diff($objFin);
	                    $diasTotal =  $interval->days;

	                    for ($i = 0; $i <= $diasTotal; $i++) {
	                        $dia = date("Y-m-d", strtotime("$diaInicio +$i days"));

	                        $dia = preg_replace("/-/", "", $dia);
	                        $rutaFile = $rutaFolderActividades.$value."/".$dia.".json";
	                        try{
	                                $contents = File::get($rutaFile);
	                                foreach (json_decode($contents, true) as $key2 => $value2) {
	                                   $tmp = $value2;
	                                   $tmp = ["codactu" => $tmp["appt_number"], 
	                                   "status" =>$tmp["status"] ];
	                                   if ($tmp["status"] === "pending" || $tmp["status"] === "started"){
	                                   	$where = ["codactu" => $tmp["codactu"], "estado_id" => 15];
	                                   	$ultimo  = UltimoMovimiento::where($where)->get();
	                                   	$ultimo = json_decode(json_encode($ultimo), true);
	                                   	//print_r($ultimo);
	                                   	if (count($ultimo) > 0)
	                                   		$dataFuente[$tmp["codactu"]] = $tmp;
	                                   }
	                                    
	                                }
	                            }
	                            catch (Illuminate\Filesystem\FileNotFoundException $exception)
	                            {
	                                continue;
	                            }
	                    }
	                }
	    	print_r($dataFuente);

	    	}
	    }

	}

}
