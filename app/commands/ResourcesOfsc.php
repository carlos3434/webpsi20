<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Resources;

class ResourcesOfsc extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:generarjsonresource';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un archivo json  de los resources en ofsc.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $from = date("Y-m-d");
        $crearArchivo = false;
        $resource = new Resources();
        $rutaFolderJson = public_path()."/json";
        
        if (!File::exists($rutaFolderJson)){
            File::makeDirectory($rutaFolderJson, $mode = 0777, true, true);
        }
        $rutaFolderResources = $rutaFolderJson."/resources";
        if (!File::exists($rutaFolderResources)){
            File::makeDirectory($rutaFolderResources, $mode = 0777, true, true);
        }

        $rutaFile = $rutaFolderResources."/".preg_replace("/-/", "", $from).".json";

        if (File::exists($rutaFile)) {
            unlink($rutaFile);
        }

        if(!File::exists($rutaFile)){
            $crearArchivo = true;
        }

        if ($crearArchivo === true) {
            $resultado = $resource->getResources();
            $recurso = $resultado->data;
            File::put($rutaFile,json_encode($recurso));
        }
    }

}
