<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use UsuarioSolicitudTecnicaMovimiento as UsuarioSolicitudMovimiento;
use UsuarioSolicitudTecnica as UsuarioSolicitudUltimo;
use Legados\models\SolicitudTecnica as SolicitudTecnica;

class ProcesoAsignarGestores extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:asignarGestores';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Asignar gestores.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Auth::loginUsingId(697);
		
		$solicitudes = SolicitudTecnica::with('Ultimo')->whereHas('Ultimo', function($query){
                    $query->whereRaw('solicitud_tecnica_ultimo.estado_ofsc_id = 4')
                          ->whereRaw('solicitud_tecnica_ultimo.estado_aseguramiento = 5');
                })->get();

	    $usuarios_devolucion = DB::table('usuarios as u')
	        ->select('u.id', 'ucs.cantidad')
	        ->leftJoin('usuario_cant_solicitudes as ucs', 'ucs.usuario_id', '=', 'u.id')
	        ->where('u.perfil_id', '=', 13)
	        ->where('ucs.estado_asignacion', '<>', 0)
	        //->orWhere('ucs.estado_asignacion', '=', null)
	        ->get();


	    $cant_copados = 0;

	    foreach ( $usuarios_devolucion as $key => $user ) {
	        if($user->cantidad == null || $user->cantidad == ''){
	          $user->cantidad = 1000;
	        }
	        $usuarios[] = $user;
	    }
	    $cant_usuarios = count($usuarios);


	    while( list($index, $usuario) = each($usuarios) ){
	        if ( $usuario->cantidad > 0 ) {

	            foreach ($solicitudes as $key => $dato) {
	                //if redis
	                if( !Redis::get($dato->id_solicitud_tecnica) ){

	                    $usuarios[$index]->cantidad = $usuarios[$index]->cantidad - 1;
	                    $solicitud = UsuarioSolicitudUltimo::where('solicitud_tecnica_id', '=', $dato->id )->first();

	                    if ( count($solicitud) == 1 ) {
	                      $solicitud->usuario_id = $usuario->id;
	                      $solicitud->save();
	                    }
	                    else {
	                      $usuario_ultimo = new UsuarioSolicitudUltimo;
	                      $usuario_ultimo->solicitud_tecnica_id = $dato->id;
	                      $usuario_ultimo->usuario_id = $usuario->id;
	                      $usuario_ultimo->save();
	                    }

	                    $usuario_movimiento_solicitud = new UsuarioSolicitudMovimiento();
	                    $usuario_movimiento_solicitud->solicitud_tecnica_id = $dato->id;
	                    $usuario_movimiento_solicitud->usuario_id = $usuario->id;
	                    $usuario_movimiento_solicitud->save();
	                }

	                unset($solicitudes[$key]);
	                break;
	            }
	        }
	        else {
	            $cant_copados +=1;
	        }

	        if ( ($index+1 == $cant_usuarios) && $cant_copados < $cant_usuarios && count($solicitudes) > 0 ) {
	            $cant_copados = 0;
	            reset($usuarios);
	        }
	 
	    }

		Auth::logout();
        return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
