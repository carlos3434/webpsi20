<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoOfscEnvioLiquidadosCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:envioliquidados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de Liquidados a OFSC, los filtros dependen
                            de las opciones escogidas en el mantenimiento
                            de validaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $url='enviosofsc/enviarliquidadoofsc';
        $method='GET';
        Auth::loginUsingId(697);
        echo Helpers::ruta($url, $method, array());
        Auth::logout();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
