<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Configuracion\models\Parametro;

class ProcesoOfscEnvioLiquidadosCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:envioliquidados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de Liquidados a OFSC, los filtros dependen
                            de las opciones escogidas en el mantenimiento
                            de validaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = Config::get("wpsi.ofsc.user_logs.liquidado_masivo");
        Auth::loginUsingId($userId);

        $nombreGrupo = date("Y-m-d H:i:s");
        //$toa = new Toa;

        $array['tipo']=2; //envio de psi a toa
        $array['estado']=1; //activo
        $array['tabla']=3; //1: legados - 2: gestion - 3:temporal

        $parametros = Parametro::getParametroValidacion($array);

        $actuaciones = Toa::enviomasivoLiquidado($parametros);
        if (count($actuaciones)==0) {
            echo "no hay actuaciones \n";
            Auth::logout();
            return;
        }
        $grupoId = DB::table('envio_masivo_ofsc_grupo')
        ->insertGetId(
            [
            'nombre' => $nombreGrupo,
            'created_at' => date("Y-m-d H:i:s"),
            'usuario_created_at' =>  Auth::id()
            ]
        );
        $i=0;
        foreach ($actuaciones as $actuacion) {
            $actuacion->grupoEnvio=$grupoId;
            $actuacion->authId=$userId;
            Queue::push('EnvioMasivoOfsc@liquidarOrden', $actuacion,'envio_liquidados');
            $i++;
        }
        Auth::logout();
        echo "se envio:  ".$i." actuaciones \n";
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
