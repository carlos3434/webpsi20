<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoOfscUpdateTmpCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:updatetmpofsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtener campos de tmp_provision y actualizar  gd, um y la app de TOA - cada 1h 40m despues de envio componentes ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        
          $url='bandeja/updatetmpofsc';
          $method='POST';
          Auth::loginUsingId(697);
          $array=array();
          echo Helpers::ruta($url, $method, $array);
          Auth::logout();

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
