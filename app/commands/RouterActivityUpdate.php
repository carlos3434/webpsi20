<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use Ofsc\Inbound;
// use \controllers\BandejaController;

class RouterActivityUpdate extends Command {


	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ofsc:routeractivityupdate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Consume actividades y tecnicos, 
	actualizar coordenadas. Usa distancia en Km.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		function buscarTecnico($data, $resourceId) {
			foreach($data as $key => $val) {
				if ($val->resourceId == $resourceId) {
					return $key;	
				}
				return '';
			}
		}

    	function getDistanceInKm($coordY, $coordX, $point) {
    		return round(
    			3959000 * 
    			acos(
    				cos(deg2rad($coordY))
    				*
    				cos(deg2rad($point['y']))
    				*
    				cos(deg2rad($point['x']) - deg2rad($coordX))
    				+
    				sin(deg2rad($coordY)) * sin(deg2rad($point['y']))
				)
			);
    	} // -- FUNCTION (getDistanceInKm)

		/**
		 * Formula para sacar distancia entre dos puntos dada la latitud y longitud de dos puntos.
		 * Esta distancia tiene que estar dada en notación DECIMAL y no en SEXADECIMAL (Grados, minutos... etc)
		 * @param type $latitud 1
		 * @param type $longitud 1
		 * @param type $latitud 2
		 * @param type $longitud 2
		 * @return type, Distancia en Kms, con 1 decimal de precisión
		 */
		function harvestine($lat1, $long1, $lat2, $long2){ 
		    //Distancia en kilometros en 1 grado distancia.
		    //Distancia en millas nauticas en 1 grado distancia: $mn = 60.098;
		    //Distancia en millas en 1 grado distancia: 69.174;
		    //Solo aplicable a la tierra, es decir es una constante que cambiaria en la luna, marte... etc.
		    $km = 111.302;
		    
		    //1 Grado = 0.01745329 Radianes    
		    $degtorad = 0.01745329;
		    
		    //1 Radian = 57.29577951 Grados
		    $radtodeg = 57.29577951; 
		    //La formula que calcula la distancia en grados en una esfera, llamada formula de Harvestine. Para mas informacion hay que mirar en Wikipedia
		    //http://es.wikipedia.org/wiki/F%C3%B3rmula_del_Haversine
		    $dlong = ($long1 - $long2); 
		    $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
		    $dd = acos($dvalue) * $radtodeg; 
		    return round(($dd * $km), 2);
		} // -- FUNCTION (harvestine)

		function calcularPuntos(
    		&$tecnicos, $cantidadActividadesPorTecnico, $inicialDistancia, 
    		&$activitiesDinamic, &$cantidad, &$activitiesBody, &$ignorado
		) { 
			foreach($tecnicos as $keyTec =>  $valTec) {
				$distance = [];
				$resourceId = $valTec->resourceId;
				$coordTecnico = ['y' => $valTec->latitud, 'x' => $valTec->longitud];

				// Cuando se acaben la lista de actividades disponibles
				if (count($activitiesDinamic) <= 0) {
					break;
				}

				// Tecnicos indisponibles
				if (in_array($resourceId, $ignorado)) {
					$msj = "\nTecnico, no se le asigna actividades: $resourceId ( ID: $keyTec )\n";
					continue;
				}

				$present = false;
				foreach($activitiesDinamic as $keyact => $act) {
					// Compara si la actividad ya a sido asignado al tecnico previamente.
					/*
					if (strcmp($act['resource_id'], $resourceId) !== 0) {
						continue;
					}
					*/

					// $calculateDistance = getDistanceInKm($act['coordy'], $act['coordx'], $coordTecnico);
					$calculateDistance = harvestine(
						$act['coordy'], $act['coordx'], $coordTecnico['y'], $coordTecnico['x']
					);
					
					if (isset($activitiesBody[$resourceId]) 
						&& count($activitiesBody[$resourceId]) >= $cantidadActividadesPorTecnico ) {
						unset($tecnicos[$keyTec]);
						continue;
					}
					// Obtener solo actividades por minima distancia
					elseif (
						$calculateDistance > $inicialDistancia   
					) {
						continue;
					}

					//echo "\n\n DI:" . $calculateDistance . ' |-| keyACT:' . $keyact . " -- ";

					$activitiesBody[$resourceId][$keyact]['distance'] = $calculateDistance;
					$activitiesBody[$resourceId][$keyact]['activity'] = $act['id'];		
					$activitiesBody[$resourceId][$keyact]['codactu'] = $act['appt_number'];	
					$activitiesBody[$resourceId][$keyact]['code'] = $keyact;
					$distance[$keyact] = $calculateDistance;
					$present = true;
					// Actualiza al ultimo punto de actividad asignada.
					//$coordTecnico = ['y' => $act['coordy'], 'x' => $act['coordx']];
				} // -- -- -- END -- FOREACH (activitiesDinamic)

				//print_r($activitiesBody[$resourceId]);
				//print_r($distance);
				if (isset($activitiesBody[$resourceId]) 
					&& count($activitiesBody[$resourceId]) > 0  && $present) {
					//echo  " - RES:" . $resourceId . ' --- C-aB:' . count($activitiesBody[$resourceId]) . ' - C-d:' . count($distance) . ' --><-- ' . "\n";

					// -- Ordenar por distancia
					/*
					array_multisort($distance, SORT_ASC, $activitiesBody[$resourceId]);
					*/

					// --  Limitar a N actividades por Tecnico
					/*
					$activitiesBody[$resourceId] = 
						array_slice($activitiesBody[$resourceId], 0, $cantidadActividadesPorTecnico);
					*/

					// --  Eliminar una actividad ya asignada
					foreach ($activitiesBody[$resourceId] as $key => $value) {
						$cantidad++;
						unset($activitiesDinamic[$value['code']]);

					} // -- FOREACH (isset)
				} // -- IF (isset)
			} // -- FOREACH (tecnicos)
    	} // -- FUNCTION (calcularPuntos)

		function redistribuirOfsc(&$activitiesBody, &$activitiesDinamic, &$tecnicos, &$ignorado, &$procesado, $activities, $fechaAsignar) {
			$message = "\nDATA \n\n";
			$conActPro = 0;
			$contador = 1; 

			foreach ($activitiesBody as $keyBody => $valBody) {
				//print_r($ignorado);
				// validando que no se considere tecnicos excluidos, ES INNESARIO, se agrego  "calcularPuntos"
				if (in_array($keyBody, $ignorado)) {
					continue;
				}
				foreach($valBody as $keyPoint => $point) {
					//print_r($point['codactu']); exit;
					DB::beginTransaction();
		        	$codactu = $point['codactu'];
		        	// print_r($procesado);
					if (in_array($codactu, $procesado)) {
						continue;
					}
			        try {
				        // ===========================
						$input['data_bandeja']	 = '';
						$input['fono1']	 = '';
						$input['fono2']	 = '';
						$input['fono3']	 = '';
						$input['slaini']	 = '';
						// --  fecha||bucket||slot||horas
						$input['agdsla']		='sla'; // agenda
				        $input['empresa_id']    ='5'; // LARI
				        $input['codactu']       =$codactu;
				        $input['observacion_toa'] = 'proceso auto: RouterActivityUpdate.';
				        // SLA -- AGENDA: 2016-08-16||LARI_INS_MOVISTAR_UNO||PM||13-18
				        $input['hf']            = $fechaAsignar . '||' . $keyBody. '||0||0-0';

				        $url='bandeja/envioofsc';
				        $method='POST';
				        Auth::loginUsingId(697);
				        $response = Helpers::ruta($url, $method, $input);
				        Auth::logout();
				        // ===========================
	   					$message .= $response;
				        $response = (array) json_decode($response);
				        //print_r($response);
						// -- rpta inbound,, 
						$msj = $contador . '.- ' .date('Y-m-d H:i:s') . "\n";
						echo $msj;
			            $message .= $msj;
						$contador++;
						$errores = isset($response['errorCreateActivity'])? 
							((array) $response['errorCreateActivity']) : [];
							//(isset($response['error'])? $response['error'] : null);

						if ((isset($response['rst']) && $response['rst'] == '2')) {
							$msj = '[2.3] - ' . $response['msj'] . ' :: TRAMA-ERROR: ' 
								. json_encode( (array)$errores );
				        	// Volver a agregar actividades
				        	// $activitiesDinamic[$point['code']] = $activities[$point['code']];
						} elseif (isset($errores) && is_array($errores)) {
							$error = '';
							$errorCode = '1.1';
							// warning | error
							foreach ($errores as $k => $v) {
								if (strval($k) == '69179') {
									$errorCode = '2.1';
									$msj = "\nTecnico, retirado: $keyBody";
						        	$message .=  $msj;
						        	echo  $msj;
									//
									$ignorado[] = $keyBody;
									/*
									$keyTec = buscarTecnico($tecnicos, $keyBody);
									echo "\nid-code-tecnico: $keyTec";
									if (!empty($keyTec)) {
										$msj = "\nTecnico, retirado: $keyBody ( ID: $keyTec )\n";
							        	$message .=  $msj;
							        	echo  $msj;
										
										$ignorado[] = $keyBody;
										unset($tecnicos[$keyTec]);
									}
									*/
								} else {
									echo "\nerror-code: {strval($k)}";
								}
								$error .= "*ERROR [[ $k | $v ]] \n";
							}

							if($errorCode == '1.1') {
								$procesado[] = $codactu;
								$conActPro++;
							} else {
					        	// Volver a agregar actividades
				        		$activitiesDinamic[$point['code']] = $activities[$point['code']];
							}
							$msj = '[' . $errorCode . '] - ' . $response['msj'] . ' :: ' . $error;
						} elseif (isset($errores) && ($errores) != '') {
							$msj = '[2.2] - ' . $response['msj'] . ' :: TRAMA-ERROR: ' 
								. json_encode( (array)$errores );
				        	// Volver a agregar actividades
				        	//$activitiesDinamic[$point['code']] = $activities[$point['code']];
						} elseif ((isset($response['rst']) && $response['rst'] == '1')) {
				        	$msj =  '[1.0] - ' . $response['msj'] . ' ::gestionId:: ' 
				        		. $response['gestion_id'] . ' ::aid:: ' . $response['aid'];
				        	$procesado[] = $codactu;
				        	$conActPro++;
						}
				        $message .=  $msj;
				        echo  $msj;

			            DB::commit();  
			        } catch (Exception $ex) {
			            DB::rollback();
			            $msj =  '(3) rollback : ';
			        	$message .=  $msj;
			        	echo  $msj;
			            Helpers::saveError($ex);
			        } //-- END-try
		            $msj =  " CODACTU: " . $codactu .", RESOURCEID: " . $keyBody . " \n\n";
	        		$message .=  $msj;
					echo $msj;
				} //-- END-foreach -- actividad por tecnico

			} //-- END-foreach -- tecnico
			print_r(' ::TOTAL PROCESADAS EN WEBPSI/ TOA: ' . $conActPro . "\n");
			$message .= " \n"; 
			echo " \n";
			//print_r($message);
			return $message;
		} // -- FUNCTION (redistribuirOfsc)


		// =========================================
		// = TODO ESTA EXPRESADO EN KILOMETROS =====
		// =========================================

		$cantidadActividadesPorTecnico = 5;
		$inicialDistancia = 0.01;
    	$limiteDistanciaRadio = 1.2; // Limite de la distancia, del radio;
    	// $limiteDistanciaCorte = 250; // Limite de la distancia iterada; // Se han encontrado bucles infinitos
		$cantidad = 0;
    	$bucket = 'LARI_INS_MOVISTAR_UNO';
    	$today = date('Y-m-d'); // ' '2016-09-07'; strtotime(date('Y-m-d') . " -1 day")
    	$todayClean = preg_replace("/-/", "", $today);
		$activitiesBody = [];
		$ignorado = [];
		$procesado = [];

		// ===============
		// ===============
		// ===============
	    // 1. JSONACTIVIDADES A ARREGLO
    	$jsonPath = public_path()."/json/actividades/{$bucket}/";
    	$jsonFile = $jsonPath . "{$todayClean}.json";
    	// -- $activities is array
		$activities = (json_decode(File::get($jsonFile), true));
		// $activities = array_slice($activities, 0, 10); // limit test
		$activitiesDinamic = $activities;
		$activitiesCount = count($activities);
		print_r("\n\n" . ' ::TOTAL ACTIVIDADES: ' . $activitiesCount . "\n");

		// print_r($activities);
		// exit;
		// ===============
		// ===============
		// ===============
	    // 2. LISTAR TECNICOS | JSONTECNICOS A ARREGLO
    	//$jsonFileTecnico = public_path()."/json/tecnicos/{$bucket}/{$todayClean}.json";
		//$technical = (json_decode(File::get($jsonFileTecnico), true));
		//print_r($technical);exit;
		$tecnicos = DB::table('resources_locations')
			->select('resource_id as resourceId', 'latitud', 'longitud')
		    ->groupBy('resource_id')
		    ->get();
	    // $tecnicos = array_slice($tecnicos, 0, 3);
		$tecnicoCount = count($tecnicos);


		// print_r($tecnicos);
		// exit;
		// ===============
		// ===============
		// ===============
		// 3. ARMAR PUNTOS
		$kilometros = 1;
		while (count($activitiesDinamic) > 0):
			if (count($tecnicos) <= 0) {
				break;
			}
			$calculoDistancia = $inicialDistancia * $kilometros;
		    echo "\n" . ' //// D::' . $calculoDistancia . ' //// IF::' . $cantidad . ' //// ' 
				. ' T::' . count($tecnicos) . ', A::' . count($activitiesDinamic);

			if ( $calculoDistancia >= $limiteDistanciaRadio ) {
				echo "\nCorto en el limite de distancia: $limiteDistanciaRadio es menor a $calculoDistancia  \n";
				break;
			}
			
			calcularPuntos(
				$tecnicos, $cantidadActividadesPorTecnico, $calculoDistancia, 
	    		$activitiesDinamic, $cantidad, $activitiesBody, $ignorado
			);
		    $kilometros++;
		endwhile;
		print_r("\n ::CANTIDAD PROC(1).: $cantidad \n");
		//print_r($activitiesBody['LA4752']);
		//print_r(($activitiesBody));
		//print_r(count($activitiesDinamic) . ' - ');
		//exit;

		// ===============
		// ===============
		// ===============
		// 4. TOA
		$message = redistribuirOfsc($activitiesBody, $activitiesDinamic, 
			$tecnicos, $ignorado, $procesado, $activities, $today);


		// ===============
		// =======(dos) =======
		// ===============
		// 5. ARMAR PUNTOS
		$kilometros = 1;
		while (count($activitiesDinamic) > 0):
			if (count($tecnicos) <= 0) {
				break;
			}
			$calculoDistancia = $inicialDistancia * $kilometros;
		    echo "\n" . ' //// D::' . $calculoDistancia . ' //// IF::' . $cantidad . ' //// ' 
				. ' T::' . count($tecnicos) . ', A::' . count($activitiesDinamic);
			
			if ( $calculoDistancia >= $limiteDistanciaRadio ) {
				echo "\nCorto en el limite de distancia: $limiteDistanciaRadio es menor a $calculoDistancia  \n";
				break;
				// la siguiente condicion se aplica cuando se elimina $limiteDistanciaRadio
			} 
			/*
			else if ($calculoDistancia >= $limiteDistanciaCorte) {
				$message .= "\nDEFINITIVO. Se corto en el limite de distancia: $limiteDistanciaCorte\n";
				break;
			}
			*/
			calcularPuntos(
				$tecnicos, $cantidadActividadesPorTecnico, $calculoDistancia, 
	    		$activitiesDinamic, $cantidad, $activitiesBody, $ignorado
			);
		    $kilometros++;
		endwhile;
		print_r("\n ::CANTIDAD PROC(2).: $cantidad \n");


		// ===============
		// ===============
		// ===============
		// 6. TOA (dos)
		$message .= redistribuirOfsc($activitiesBody, $activitiesDinamic, 
			$tecnicos, $ignorado, $procesado, $activities, $today);


		// ===============
		// ===============
		// ===============
		// 7. Exportar
		File::put($jsonPath . 'router_log.txt', $message);


		print_r("\n\n" . ' ::TOTAL ACTIVIDADES: ' . $activitiesCount . "\n");
		// print_r($activitiesDinamic);
	}

}