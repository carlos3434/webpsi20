<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Resources;

class ResourcesOfscCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:generarjsonresource';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un archivo json  de los resources en ofsc.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $from = date("Y-m-d");
        $crearArchivo = false;
        $resource = new Resources();
        //$fileName = preg_replace("/-/", "", $from).".json";
        $fileName = "resources.json";
        $tipoResource = ["buckets" => "BK", "tecnicos" => "PR"];

        foreach ($tipoResource as $key => $value) {
            $rutaFolderResources = public_path()."/json/resources/".$key;
            $existeFolder = FileHelper::validaCrearFolder($rutaFolderResources);

            if ($existeFolder === true) {
                $resultado = $resource->getResources();
                print_r($resultado);
                echo "\n--------\n";
                $recurso = isset($resultado->data)? $resultado->data : [];
                if (count($recurso) > 0) {
                    $rutaFile = $rutaFolderResources."/".$fileName;
                    $bucketsActivos = [];
                    $recursosActivos = [];
                    foreach ($recurso as $key2 => $value2) {

                        if ($value2["status"] === "active") {
                            if ($value2["type"] === $value) {
                                $bucketsActivos[] = $value2;
                            } else {
                                $recursosActivos[] = $value2;
                            }
                        }
                    }
                    while (count($recursosActivos) > 0) {
                        $response = OfscHelper::getResource(
                            $value,
                            $resources = ["inicial" => $recursosActivos,
                            "final" => $bucketsActivos]
                        );
                        $recursosActivos = $response["activos"];
                        $bucketsActivos = $response["final"];

                    }
                    
                    if (File::exists($rutaFile)) {
                        unlink($rutaFile);
                    }
                    foreach ($bucketsActivos as $key3 => $value3) {
                        if ($value3["type"] == "BK") {
                            $bucket = Bucket::firstOrNew(['bucket_ofsc' => $value3['id']]);
                            $bucket->bucket_ofsc = $value3['id'];
                            $bucket->parent_id = $value3['parent_id'];
                            $bucket->nombre = $value3['name'];
                            $bucket->status = $value3['status'];
                            $bucket->type = $value3['type'];
                            $bucket->estado = 1;
                            $bucket->save();
                        }
                        
                    }
                    File::put($rutaFile, json_encode($bucketsActivos));
                    if ($value == "BK") {
                        Cache::forget("listBuckets");
                        Cache::forget('bucketLegado');
                    }
                }
            }
        }
    }
}
