<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OfscParaLiquidarCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'paraliquidar:ofsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtener las provisiones del quiebre PENMOV1 para cargar 
                          a las macros y que puedan ser liquidadas en legados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //$localDir = public_path().'/toaparalegados/';
        $localDir ='/home/activadeco/';
        $file='paralegados.txt';

          if (!file_exists($localDir)) {
              mkdir($localDir, 0777, true);
          }
          if (file_exists($localDir.$file)){
            unlink($localDir.$file);
          }
          
          $url='ofsclegados/ofsccompletado';
          $method='GET';
          Auth::loginUsingId(697);
          $array=array('files'=>$localDir.$file);
          echo Helpers::ruta($url, $method, $array);
          Auth::logout();

          echo 'OK';

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
