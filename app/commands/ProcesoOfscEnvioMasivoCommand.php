<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Configuracion\models\Parametro;

class ProcesoOfscEnvioMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:enviomasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio masivo de actividades temporales a OFSC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = Config::get("wpsi.ofsc.user_logs.actividades_temporales");
        Auth::loginUsingId($userId);
        
        $nombreGrupo = date("Y-m-d H:i:s");
        $toa = new Toa;

        $array['tipo']=2; //GOTOA
        $array['estado']=1;
        $array['proveniencia']=1; //Gestión
        $array['campo']='nombre_temporal';


        $parametros = Parametro::getParametroValidacion($array);
       /* if ($toa->obtenerConfiguracion(1)==false) { //envio masivo
            echo "no se ha configurado este tipo de envio a Toa \n";
            Auth::logout();
            return;
        }
        $actuaciones = $toa->envioMasivo();*/
        $actuaciones = Toa::enviomasivogestion($parametros);
        if (count($actuaciones)==0) {
            echo "no hay actuaciones \n";
            Auth::logout();
            return;
        }
        $grupoId = DB::table('envio_masivo_ofsc_grupo')
        ->insertGetId(
            [
            'nombre' => $nombreGrupo,
            'created_at' => date("Y-m-d H:i:s"),
            'usuario_created_at' =>  Auth::id()
            ]
        );

        $i=0;
        foreach ($actuaciones as $key => $value) {
            $actuacion = new Actuacion($value);
            if ( $actuacion->validarContrata()==false) continue;
            if ( $actuacion->validarPAI()==false) continue;
            if ( $actuacion->validarProvPendienteCatv()==false) continue;
            if ( $actuacion->validarLiquidada()==false) continue;
            $actuacion->grupoEnvio=$grupoId;
            $actuacion->authId=$userId;
            $i++;
            Queue::push('EnvioMasivoOfsc@envioOrdenOfsc', $actuacion,'enviomasivo');
        }
        Auth::logout();
        echo "se envio:  ".$i." actuaciones \n"; 
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
