<?php
use Ofsc\Activity;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoPsiConsultaActividadesToaCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'go:consulta_data_toa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso que consulta data en toa y la copia a go.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        Auth::loginUsingId('697');
        
        ActuacionesToaTmp::truncate();

        $activity = new Activity;
        $bucket = 'TDP';

        $hoy       = date('Y-m-d');

        //$from      = '2017-02-01';
        $from      = date("Y-m-d", strtotime($hoy . " - 30 days"));
        $fin        = date("Y-m-d", strtotime($hoy . " + 30 days"));


        $to = $from;
        while ($to <= $fin) {
            $to = date("Y-m-d", strtotime($from . " + 15 days"));

            $actividades = $activity->getActivities($bucket, $from, $to);
            //guardar
            foreach ($actividades->data as $actividad ) {
                $actividad['aid'] = $actividad['id'];
                unset($actividad['id']);
                if ('0'==$actividad['appt_number'] || 0==$actividad['appt_number']) {
                    continue;
                }
                if ('3000-01-01'==$actividad['date'] ) {
                    $act = ActuacionesToaTmp::where('aid',$actividad['aid'])->first();

                    if ($act) {
                        //$act->update($actividad);
                    } else {
                        $act = new ActuacionesToaTmp($actividad);
                        $act->save();
                    }
                    continue;
                }
                $act = new ActuacionesToaTmp($actividad);
                $act->save();

            }

            $from = date("Y-m-d", strtotime($from . " + 16 days"));
        }

        Auth::logout();
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
