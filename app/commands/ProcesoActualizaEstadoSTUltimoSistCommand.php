<?php
use Ofsc\Activity;
use Ofsc\Inbound;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaCms as Cms;

class ProcesoActualizaEstadoSTUltimoSistCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'stcms:actualizarEstados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizar Estados en la tabla stUltimo con referencia a la bd schedulle';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
      Auth::loginUsingId(697);//proceso automatico
      $hoy = date('Y-m-d 23:59:59');
      $antes =date('Y-m-j 00:00:00', strtotime ( '-4 day' , strtotime ( $hoy ) )) ;
      $objActivity = new Activity();
 

        //***************************************************************
    

        $catalogo = DB::table('psi.solicitud_tecnica_ultimo as stu')
          ->select('stu.num_requerimiento','st.id_solicitud_tecnica','stu.estado_ofsc_id','stu.aid')
          ->join('solicitud_tecnica as st','stu.solicitud_tecnica_id','=','st.id')
          ->where('estado_aseguramiento', '<>', '9')          
          //->whereBetween('stu.created_at', array($antes, $hoy))
          ->groupBY('stu.num_requerimiento')
          ->get();
        if(count($catalogo)>0){

          for($i=0;$i<count($catalogo);$i++){  


            $response = $objActivity->cancelActivity($catalogo[$i]->aid, "");
            print_r($response);
              if (isset($response->data->result_code) && $response->data->result_code == 0) {
                  if ($catalogo[$i]->estado_ofsc_id==1) {                    
                        $array =['estado_aseguramiento'=>'9',
                            'estado_ofsc_id'=>'5'];
                  } else if($catalogo[$i]->estado_ofsc_id==2){
                        $array =['estado_aseguramiento'=>'9',
                                'estado_ofsc_id'=>'4'];
                  } else {
                        $array =['estado_aseguramiento'=>'9'];
                  }            
                  //$response = ["rst" => 1];
              } else {
                  $array =['estado_aseguramiento'=>'9'];
                  //$error = isset($responseToa->errorMsg)? $responseToa->errorMsg:'error desconocido en TOA';
                  //$response["rst"] = 2;
                  //$response["msj"] = $error;            
              }              
            // buscando en la tabla schedulle_sistemas.aver_liq_catv_pais
            $aver_liq_catv_pais = DB::table('schedulle_sistemas.aver_liq_catv_pais')
              ->select('codigoreq')
              ->where('codigoreq', '=',$catalogo[$i]->num_requerimiento)                       
              ->get();
             

              if(count($aver_liq_catv_pais)>0){ // si encontro algun resultado actualizamos            
                    
                $solicitudTecnica = SolicitudTecnica::where("id_solicitud_tecnica", $catalogo[$i]->id_solicitud_tecnica)
                        ->orderBy("updated_at", "DESC")
                        ->first();

                      $ultimo = $solicitudTecnica->ultimo;
                      
                      foreach (Ultimo::$rules as $key => $value) {
                          if (array_key_exists($key, $array)) {
                              $ultimo[$key] = $array[$key];
                          }
                      }
                      $solicitudTecnica->ultimo()->save($ultimo);


                      $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                      foreach (Movimiento::$rules as $key => $value) {
                          if (array_key_exists($key, $array)) {
                              $ultimoMovimiento[$key] = $array[$key];
                          }
                      }
                      $ultimoMovimiento->save();
                      
              }else{ /* si no encontro un resultados buscamos en la tabla schedulle_sistemas.aver_liq_bas_lima */
                
                $aver_liq_bas_lima = DB::table('schedulle_sistemas.aver_liq_bas_lima')
                  ->select('numero_osiptel')
                  ->where('numero_osiptel', '=', $catalogo[$i]->num_requerimiento)        
                  ->get();

                  if(count($aver_liq_bas_lima)>0){ // si encontro algun resultado actualizamos
                      $solicitudTecnica = SolicitudTecnica::where("id_solicitud_tecnica", $catalogo[$i]->id_solicitud_tecnica)
                        ->orderBy("updated_at", "DESC")
                        ->first();

                      $ultimo = $solicitudTecnica->ultimo;
                      
                      foreach (Ultimo::$rules as $key => $value) {
                          if (array_key_exists($key, $array)) {
                              $ultimo[$key] = $array[$key];
                          }
                      }
                      $solicitudTecnica->ultimo()->save($ultimo);


                      $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                      foreach (Movimiento::$rules as $key => $value) {
                          if (array_key_exists($key, $array)) {
                              $ultimoMovimiento[$key] = $array[$key];
                          }
                      }
                      $ultimoMovimiento->save();
                      
                  }else{ /* si no encontro un resultados buscamos en la tabla schedulle_sistemas.aver_liq_bas_prov_pedidos */
                  
                    $aver_liq_bas_prov_pedidos = DB::table('schedulle_sistemas.aver_liq_bas_prov_pedidos')
                      ->select('correlativo')
                      ->where('correlativo', '=',$catalogo[$i]->num_requerimiento)        
                      ->get();

                      if(count($aver_liq_bas_prov_pedidos)>0){  // si encontro algun resultado actualizamos
                          $solicitudTecnica = SolicitudTecnica::where("id_solicitud_tecnica", $catalogo[$i]->id_solicitud_tecnica)
                          ->orderBy("updated_at", "DESC")
                          ->first();

                        $ultimo = $solicitudTecnica->ultimo;
                        
                        foreach (Ultimo::$rules as $key => $value) {
                            if (array_key_exists($key, $array)) {
                                $ultimo[$key] = $array[$key];
                            }
                        }
                        $solicitudTecnica->ultimo()->save($ultimo);


                        $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                        foreach (Movimiento::$rules as $key => $value) {
                            if (array_key_exists($key, $array)) {
                                $ultimoMovimiento[$key] = $array[$key];
                            }
                        }
                        $ultimoMovimiento->save();
                       
                      }else{
                        $aver_liq_adsl_pais = DB::table('schedulle_sistemas.aver_liq_adsl_pais')
                          ->select('numero_osiptel')
                          ->where('numero_osiptel', '=',$catalogo[$i]->num_requerimiento) 
                          ->get();
                          if(count($aver_liq_adsl_pais)>0){
                            $solicitudTecnica = SolicitudTecnica::where("id_solicitud_tecnica", $catalogo[$i]->id_solicitud_tecnica)
                                ->orderBy("updated_at", "DESC")
                                ->first();

                              $ultimo = $solicitudTecnica->ultimo;
                              
                              foreach (Ultimo::$rules as $key => $value) {
                                  if (array_key_exists($key, $array)) {
                                      $ultimo[$key] = $array[$key];
                                  }
                              }
                              $solicitudTecnica->ultimo()->save($ultimo);


                              $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
                              foreach (Movimiento::$rules as $key => $value) {
                                  if (array_key_exists($key, $array)) {
                                      $ultimoMovimiento[$key] = $array[$key];
                                  }
                              }
                              $ultimoMovimiento->save();
                         
                          }
                      }
                  }
              }


          }
      
        }
        //***************************************************************

     
        Auth::logout();
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
