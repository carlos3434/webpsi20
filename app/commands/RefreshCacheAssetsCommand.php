<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RefreshCacheAssetsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'refresh:assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refresca las variables de js y css.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Cache::forever('js_version_number', time());
        Cache::forever('css_version_number', time());
    }
}
