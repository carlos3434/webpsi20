<?php
use Ofsc\Activity;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoOfscCancelMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:cancelmasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = Config::get("wpsi.ofsc.user_logs.liquidado_masivo");
        Auth::loginUsingId($userId);
        
        $sql="

        SELECT  aid FROM
        (
        SELECT aid, codactu, SUM(legados) AS enLegado,
         SUM(psi) AS enPSI, SUM(toa) AS enTOA
      --   , toa.*
         --   select  *
        FROM
        (
        SELECT '' AS aid,  legado.codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM
            (
            -- legado (cms)   45702319
            SELECT t.codigo_req AS codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM schedulle_sistemas.prov_pen_catv_pais t
            JOIN `webpsi_coc`.`zona_premium_catv` pr ON t.nodo=pr.nodo AND t.troba=pr.troba
            WHERE pr.estado =1 AND t.contrata='245'
            AND t.codigo_tipo_req  IN (  SELECT tipo_req FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
            AND t.codigo_motivo_req  IN (  SELECT motivo FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
          
            UNION

            SELECT t.codigo_req AS codactu, '1' AS legados, '0' AS psi, '0' AS toa
            FROM schedulle_sistemas.prov_pen_catv_pais t
            LEFT JOIN `webpsi_coc`.`prov_catv_tiposact` p ON t.codigo_tipo_req=p.tipo_req AND t.codigo_motivo_req=p.motivo
            LEFT JOIN `webpsi_coc`.`zona_premium_catv`  pr ON t.nodo=pr.nodo AND t.troba=pr.troba
            WHERE ( t.nodo IN ('LM' , 'LL' , 'SP' ) AND p.averia_m1='MOVISTAR UNO'  )  AND t.contrata='245'
            AND t.codigo_tipo_req  IN (  SELECT tipo_req FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
            AND t.codigo_motivo_req  IN (  SELECT motivo FROM  webpsi_coc.prov_catv_tiposact  WHERE actividad_toa IS NOT NULL GROUP BY 1)
        ) legado 
        UNION 
            -- psi
            SELECT '' AS aid, psi.codactu, '0' AS legados, '1' AS psi, '0' AS toa
            FROM
            (
                SELECT codactu
                FROM  psi.ultimos_movimientos t2
                JOIN quiebres q ON t2.quiebre_id = q.id
                WHERE (t2.mdf IN ('LM' , 'LL' , 'SP' ) AND t2.quiebre_id='25')
                AND t2.empresa_id = '5'  AND t2.contrata='245' AND t2.area2!='PAI'
                AND (t2.tipo_averia='pen_prov_catv'   )
                AND t2.estado_id NOT  IN  ('4','5','6','14','15','17' )
                AND codactu NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' 
                )
                AND codactu   IN (SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais)

                UNION

                SELECT codactu
                FROM  psi.ultimos_movimientos t2
                JOIN quiebres q ON t2.quiebre_id = q.id
                WHERE  (t2.mdf = 'SB' AND q.quiebre_grupo_id='12')
                AND t2.empresa_id = '5'  AND t2.contrata='245' AND t2.area2!='PAI'
                --  AND (t2.estado_legado ='PENDIENTE'  OR t2.estado_legado IS NULL)
                AND (t2.tipo_averia='pen_prov_catv'   )
                AND t2.estado_id NOT  IN  ('4','5','6','14','15','17' )
                AND codactu NOT IN (
                    SELECT liq.codigo_req
                    FROM schedulle_sistemas.prov_liq_catv_pais  liq
                    WHERE estado_ot='Q' 
                )
                AND codactu  IN (SELECT codigo_req
                    FROM schedulle_sistemas.prov_pen_catv_pais)
            ) psi

            UNION 

            --  toa
            SELECT aid,appt_number  AS codactu , '0' AS legados, '0' AS psi, '1' AS toa
            FROM actuaciones_toa_tmp
            WHERE (STATUS='pending' OR STATUS='suspended')
            
            -- GROUP BY codactu
            
        )  AS  total 
       -- LEFT JOIN actuaciones_toa_tmp AS  toa  ON  total.codactu = toa.appt_number
       WHERE total.codactu!='0'  -- AND (status='pending' OR status='suspended' OR status IS NULL) 
        GROUP BY  total.codactu 
        )  updated  
        
        WHERE enPSI=0  AND enLegado=0 AND enTOA =1
        ";

        $reporte =  DB::select($sql);
        $i=0;
        foreach ($reporte as $data) {
            $activity = new Activity();
            //$response = $activity->cancelActivity($data->aid, '');
            $response = $activity->completeActivity($data->aid);

            var_dump($response);
            $i++;
        }
        Auth::logout();
        echo "se envio:  ".$i." actuaciones \n";
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
