<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\ComponenteCms;

Class CorregirSolicitudesCommand extends Command {

	protected $name = 'psi:corregirSolicitudes';

	protected $description = 'agregar componentes a solicitudes con errores';

	public function __construct()
    {
            parent::__construct();
    }

    public function fire() {
    	$solicitudes = DB::table('solicitud_tecnica_cms as stc')
    					//->leftJoin('componente_cms as cc', 'stc.solicitud_tecnica_id', '=', 'cc.solicitud_tecnica_cms_id')
    					->leftJoin('solicitud_tecnica_log_recepcion as stl', 'stl.solicitud_tecnica_id', '=', 'stc.solicitud_tecnica_id')
    					->leftJoin('solicitud_tecnica_ultimo as stu', 'stu.solicitud_tecnica_id', '=', 'stc.solicitud_tecnica_id')
    					->whereIn('stu.estado_st', [0,1])
    					->where('stc.tipo_operacion', '=', 'AVE_IND')
    					->whereBetween('stc.created_at', ['2018-03-14 15:25:00', '2018-03-14 16:00:00'])
    					->get(['stl.trama', 'stc.id', 'stc.num_requerimiento']);

    	/*$trama_solicitud = DB::table('solicitud_tecnica_log_recepcion')
    						->limit(1)
    						->get(['trama', 'solicitud_tecnica_id']);*/
    	//$comp_trama = json_decode($trama_solicitud[0]->trama, true)['componentes'][3][];

    	$tramas_solicitud = [];
    	foreach ($solicitudes as $key => $solicitud) {
    		$existe_comp = DB::table('componente_cms')->where('solicitud_tecnica_cms_id', $solicitud->id)->get(['id']);
    		if (count($existe_comp)  == 0) {
    			$tramas_solicitud[] = $solicitud;
    			
    		}
    		echo $solicitud->num_requerimiento. '-------'. count($existe_comp). " \n";
    	}
    	echo count($tramas_solicitud);
    									
 		foreach ($tramas_solicitud as $key => $solicitud) {
 			$trama = json_decode($solicitud->trama, true);
 		
 			$componentes = $trama['componentes']['componente'];
 			foreach ($componentes as $key => $componente) {

 				if ($componente['numero_requerimiento']!='' && $componente['numero_ot']!='') {
                  $componente['solicitud_tecnica_cms_id'] = $solicitud->id;
                  ComponenteCms::create($componente);
                }
                echo "fin  \n";
                
 			}
 			break;
 			//DB::('componente_cms')->insert();
 			//
		}   					

    }	
}