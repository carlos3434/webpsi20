<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\EnvioLegado;

class EnvioLegadoErrorCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'psi:enviolegadoerror';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtener code de error y descripcion del error en envio legados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Auth::loginUsingId(697);//proceso automatico

        $envio =  EnvioLegado::from('envio_legado as el')
                 ->select('el.id','el.response','el.code_error','el.descripcion_error')
                 ->get();
                 
        if($envio){           
            foreach ($envio as $key => $value) {
                try {

                    \DB::beginTransaction();
                    $data = json_decode($value->response);        
                    $envio[$key]['code_error'] = $data->errorMsg->ServerException->exceptionCode;
                    $envio[$key]['descripcion_error'] = $data->errorMsg->ServerException->exceptionDetail;
                    $envio[$key]->save();
                    \DB::commit();   
                } catch (Exception $exc) {
                    \DB::rollback();
                    $errorController = new ErrorController();
                    $errorController->saveError($exc);
                }       
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    /*protected function getArguments() 
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }*/

    /**
     * Get the console command options.
     *
     * @return array
     */
    /*protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }*/

}
