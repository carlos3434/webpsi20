<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportOrdenesOfscToCsvCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	//protected $name = 'command:name';
	protected $name = 'ofsc:exportordenestocsv';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Comando que se ejecuta las 11:30 pm todos los dias y genera un CSV de las ordenes enviadas a OFSC con un rango de los ultimos 35 dias segun Fecha Legado';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$nameFile = date("Ymd");
		$rutaFolder = public_path()."/csv/";
		$rutaFile = $rutaFolder.$nameFile.".csv";
		$fin = date("Y-m-d");
		$inicio = date("Y-m-d", strtotime ( '-35 day' , strtotime ( $fin ) ) );

		$filtro = [
			"nodo" => ["LL", "SP", "LM"],
			"actividad" => [2],
			"bandeja" => 1,
			"estado_ofsc" => [1,2,3,4,5,6,7,8],
			"fecha_registro" => $inicio." - ".$fin,
			"quiebre" => [25],
			"usuario" => 1,
			"withLimit" => 0,
		];
		Input::replace($filtro);
		Auth::loginUsingId(467);

		$gestiones = \Gestion::getCargar();
		$datos = $gestiones["datos"];
		$cabecera = [];
		if (count($datos) > 0){
			foreach ($datos[0] as $key => $value) {
				$cabecera [] = $key;
			}

			File::put($rutaFile, "");
			$file = fopen($rutaFile, 'w');
			fputcsv($file, $cabecera);
		    foreach ($datos as $row) {
		        fputcsv($file, get_object_vars($row));
		    }
		    fclose($file);
		}
		Auth::logout();
	}

}
