<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Ofsc\Inbound;
use Ofsc\Activity;
class ProcesoOfscEnvioComponentesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:enviocomponentes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'actualizacion de componentes en oracle.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $authId = \Config::get("wpsi.ofsc.user_logs.update_componentes");
        Auth::loginUsingId($authId);

        $inbound = new Inbound();
        $compGestion = new ComponenteGestion();
        $dataCodReq = $compGestion->getCodReqComponenteFaltante2();

        $i=0;
        if (count($dataCodReq["data"]) > 0) {
            // table envio_masivo_ofsc_grupo (insert)
            $dateActual = date("Y-m-d H:i:s");
            $grupoId = DB::table('envio_masivo_ofsc_grupo')
            ->insertGetId(
                [
                'nombre' => $dateActual,
                'created_at' => $dateActual,
                'usuario_created_at' =>  Auth::id()
                ]
            );
            foreach ($dataCodReq["data"] as $val) {
                $dataComp = $compGestion->getComponenteFaltanteGrupal($val->codigo_req);
                $resultformat = $compGestion->getArrayformat($dataComp["data"]);
                $data["XA_EQUIPMENT"] = $resultformat;
                $response = $inbound->updateActivity($val->codigo_req, $data, array());
                $insert = [
                    'envio_masivo_ofsc_grupo_id' => $grupoId,
                    'codactu' => $val->codigo_req,
                    'tipo_actividad_id' => $val->actividad_tipo_id,
                    'empresa' => $val->empresa,
                    'accion' => 'inbound_interface',
                    'aid' => $val->aid,
                    'mdf' => $val->mdf,
                    'tipo_masivo'=>'Componentes',
                    'tipo_actividad_id' =>$val->actividad_tipo_id,
                    'bucket' => ''
                ];
                if (isset($response->result)) {
                    if ($response->result=="success") {
                        // Update tabla gestiones_detalles
                        DB::table('gestiones_detalles')
                        ->where('codactu', '=', $val->codigo_req)
                        ->update(['componente' => 1]);
                        $estado = 1;
                        $mensaje = "actualizacion e componentes correcto";
                        $i++;
                        $componentes = $compGestion->getComponenteTmp($val->codigo_req);
                        if ($componentes["error"] === false) {
                            foreach ($componentes["data"] as $value) {
                                ComponenteGestion::create(
                                    [
                                    'gestion_id' => $val->gestion_id,
                                    'componente_id' => $value->idEquipo,
                                    'cantidad' => 1,
                                    'ejecuto' => 0,
                                    'estado' => 1,
                                    ]
                                );
                            }
                        }
                    } else if ($response->result=="error" ) {
                        $estado = 0;
                        $mensaje = 'Envio de Componentes fallido: '.$response->description;
                    } else {
                        $estado = 0;
                        $mensaje = 'error desconocido';
                    }
                } else {
                    $estado = 0;
                    $mensaje = 'No se obtuvo respuesta del API';
                }
                $insert['estado']=$estado;
                $insert['mensaje']=$mensaje;
                EnvioMasivoOfsc::create($insert);
            }
        } else {
            echo "no se encontraron componentes a actualizar";
        }
        echo "se actualizaron: ".$i." componentes";
        Auth::logout();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
