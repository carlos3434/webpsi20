<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Configuracion\models\Parametro;

class TestOfscMasivoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:testmasivo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing de Masivos a OFSC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $userId = Config::get("wpsi.ofsc.user_logs.actividades_temporales");
        Auth::loginUsingId($userId);
        
        $nombreGrupo = date("Y-m-d H:i:s");
        $toa = new Toa;

        /*if ($toa->obtenerConfiguracion(5)==false) { //envio masivo
            echo "no se ha configurado este tipo de envio a Toa \n";
            Auth::logout();
            return;
        }
        $actuaciones = $toa->envioMasivo();*/

        $array['tipo']=2; //envio de psi a toa
        $array['estado']=1; //activo
        $array['tabla']=3; //1: legados - 2: gestion - 3:temporal

        $parametros = Parametro::getParametroValidacion($array);

        $actuaciones = Toa::enviomasivogestion($parametros);

        if (count($actuaciones)==0) {
            echo "no hay actuaciones \n";
            Auth::logout();
            return;
        }
        $grupoId = DB::table('envio_masivo_ofsc_grupo')
        ->insertGetId(
            [
            'nombre' => $nombreGrupo,
            'created_at' => date("Y-m-d H:i:s"),
            'usuario_created_at' =>  Auth::id()
            ]
        );
        $i=0;
        foreach ($actuaciones as $key => $value) {
            $actuacion = new Actuacion($value);
            if ( $actuacion->validarContrata()==false) continue;
            if ( $actuacion->validarPAI()==false) continue;
            if ( $actuacion->validarProvPendienteCatv()==false) continue;
            if ( $actuacion->validarLiquidada()==false) continue;
            $actuacion->grupoEnvio=$grupoId;
            $actuacion->authId=$userId;
            $i++;
            Queue::push('EnvioMasivoOfsc@envioOrdenOfscTest', $actuacion,'enviomasivo_test');
        }
        Auth::logout();
        echo "se envio:  ".$i." actuaciones \n";
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
