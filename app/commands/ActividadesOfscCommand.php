<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Activity;

class ActividadesOfscCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:actualizaractividades';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un json de las actividades en ofsc del dia actual cada 5 minutos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $activity     = new Activity();
        $objAOM     = new ActividadOfscMovimiento();
        $from         = date("Y-m-d");
        $to         = date("Y-m-d", strtotime("+ 7 days"));
        $fileName     = preg_replace("/-/", "", $from).".json";
        $buckets = OfscHelper::getFileResource();
        $bucketsId = OfscHelper::getIdBucket($buckets);        
       
        foreach ($bucketsId as $key => $value) {
            $rutaFolder = public_path()."/json/actividades/".$value;
            $existeFolder = FileHelper::validaCrearFolder($rutaFolder);
            if ($existeFolder === true) {
                $rutaFile = $rutaFolder."/".$fileName;
                $actividades = $activity->getActivities($value, $from, $to);

                if (isset($actividades->data)) {
                    if (count($actividades->data) > 0) {

                        $json = [];
                        if (File::exists($rutaFile)) {
                            $json = json_decode(
                                File::get($rutaFile), true
                            );                            
                           
                            unlink($rutaFile);
                        }
                        File::put($rutaFile, json_encode($actividades->data));
                        $dataNueva = [];
                        foreach ($actividades->data as $key2 => $value2) {
                            if (isset($value2["appt_number"]))
                                $dataNueva[] = $value2;
                        }
                        $objAOM->setCambios($dataNueva, $json);
                    }
                }
            }
        }
    }
}
