<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoResultadoPruebasModemCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'go:enviopruebasmodem';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de modem, para actualizar su 
							resultado_pruebas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $catalogo = DB::table('webpsi_officetrack.catalogo_decos')
            ->select('mac', 'gestion_id')
            ->where('accion', '=', 'modem')
            ->where(
                function($query){
                $query->where('resultado_pruebas', '=', '')
                      ->orWhere('resultado_pruebas', '=', NULL);
                }
            )
            ->where(DB::raw('TIMESTAMPDIFF(MINUTE, fecha_registro, NOW())'), '>', 10)
            ->get();

        foreach ($catalogo as $data) {

            if (isset($data->mac) && isset($data->gestion_id)) {
                //consulta del webservice Modem
                $resultadoWS = CatalogoDecos::consultarModem($data->mac);
                
                DB::table('webpsi_officetrack.catalogo_decos')
                ->where('gestion_id', '=', $data->gestion_id)
                ->where('accion', '=', 'modem')
                ->update(
                    array(
                        'resultado_pruebas' => $resultadoWS
                    )
                );
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            // array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            // array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
