<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Resources;

class ResourceLocationUpdate extends Command {


	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ofsc:resourcelocationupdate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Consume una tabla resources_locations, actualiza longitud y latitud de Location de un recurso en OFSC usando API Resource-Management';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//$hoy = date('Y-m-d');
		//$fecha =   [$hoy . ' 00:00:00', $hoy . ' 23:59:59'];
		$data = DB::table('resources_locations')
			->where('bucket', '1')
		    //->whereBetween('created_at', $fecha)
		    ->get();

		$resource = new Resources();

		    foreach ($data as $key => $val) {
		    	$actividad = [];
		        $actividad['resource_id']=$val->resource_id;
		        $actividad['label']=$val->location;
		        $actividad['latitude']=$val->latitud;
		        $actividad['longitude']=$val->longitud;

		        $propiedades=[];
		        foreach ($actividad as $key2 => $value2) {
		            $propiedades[]=['name'=>$key2,'value'=>$value2];
		        }

		        $locations=['location'=>
		            [
		                'properties'=>$propiedades
		            ]
		        ];
		        $response = $resource->updateLocations($locations);
		        print_r($response);
		    }

	}

}
