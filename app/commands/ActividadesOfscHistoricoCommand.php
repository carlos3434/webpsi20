<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Legados\models\ActividadTecnicoHistorico;
use Ofsc\Activity;

class ActividadesOfscHistoricoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:actividadeshistorico';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generar un json historicos de las actividades en ofsc por día. 1:00am-> del dia anterior, 7, 8, 9, 11, 13 y 15 del dia actual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
            parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //$hoy = "2016-11-29 01:30:00";
        $hoy                     = date("Y-m-d H:i:s");
        $hora                    = date("G", strtotime($hoy));
        $activity                = new Activity();
        $rutaFolderActividades   = public_path()."/json/actividades";

        $buckets = OfscHelper::getFileResource();
        $bucketsId = OfscHelper::getIdBucket($buckets);
        $dataActividad = [];

        foreach ($bucketsId as $key => $value) {
            if ($hora === "1") { // crea el historico de las actividades
                $desde = '2016-07-13';
                $ayer = date("Y-m-d", strtotime("- 1 days"));
                $objDesde = new DateTime($desde);
                $objAyer = new DateTime($ayer);
                $interval = $objDesde->diff($objAyer);
                $diasTotal =  $interval->days;
                $folder = preg_replace("/-/", "_", $value);
                $rutaFolder = $rutaFolderActividades."/".$folder;
                $existeFolder = FileHelper::validaCrearFolder($rutaFolder);

                for ($i = 0; $i <= $diasTotal; $i++) {
                    $dia = date("Y-m-d", strtotime("$desde +$i days"));
                    $fileName = preg_replace("/-/", "", $dia).".json";
                    $rutaFile = $rutaFolder."/".$fileName;
                    if ($existeFolder === true) {
                        // Elimino el archivo del ultimo día y creo uno nuevo
                        // que contiene las actividades agendadas solo
                        // en el día de ayer, el resto continua la logica
                        //##############################################################
                            try {                                    
                                $dataActividad = json_decode(File::get($rutaFile), true);                           
                                    $tecnicosFile = public_path()."/json/tecnicos/".$value."/".$fileName;
                                    if (File::exists($tecnicosFile)) {
                                        $dataTecnico = json_decode(File::get($tecnicosFile), true);                                     
                                        foreach ($dataTecnico as $key => $valueTecnico) {                                                
                                                foreach ($dataActividad as $key3 => $valueActividad) {                        
                                                   if ( $key == $valueActividad['resource_id'] ) {
                                                        $objactividadtecnico = new ActividadTecnicoHistorico;
                                                        $objactividadtecnico->t_time=$valueTecnico['time'];
                                                        $objactividadtecnico->t_coord_x=$valueTecnico['coord_x'];
                                                        $objactividadtecnico->t_coord_y=$valueTecnico['coord_y'];
                                                        $objactividadtecnico->t_fuente=$valueTecnico['fuente'];
                                                        $objactividadtecnico->t_name=$valueTecnico['name'];
                                                        $objactividadtecnico->t_phone=$valueTecnico['phone'];
                                                        $objactividadtecnico->t_status=$valueTecnico['status'];
                                                        $objactividadtecnico->bucket = $value;
                                                        $objactividadtecnico->name = $valueActividad["name"];
                                                        $objactividadtecnico->status = $valueActividad["status"];
                                                        $objactividadtecnico->resource_id = $valueActividad["resource_id"];
                                                        $objactividadtecnico->appt_number = $valueActividad["appt_number"];
                                                        $objactividadtecnico->coordx = $valueActividad["coordx"];
                                                        $objactividadtecnico->coordy = $valueActividad["coordy"];
                                                        $objactividadtecnico->address = $valueActividad["address"];
                                                        $objactividadtecnico->date = $valueActividad["date"];
                                                        $objactividadtecnico->start_time = $valueActividad["start_time"];
                                                        $objactividadtecnico->end_time = $valueActividad["end_time"];
                                                        $objactividadtecnico->time_slot = $valueActividad["time_slot"];
                                                        $objactividadtecnico->time_of_booking = $valueActividad["time_of_booking"];
                                                        $objactividadtecnico->sla_window_start = $valueActividad["sla_window_start"];
                                                        $objactividadtecnico->sla_window_end = $valueActividad["sla_window_end"];
                                                        $objactividadtecnico->XA_APPOINTMENT_SCHEDULER = $valueActividad["XA_APPOINTMENT_SCHEDULER"];
                                                        $objactividadtecnico->A_ASIGNACION_MANUAL_FLAG = $valueActividad["A_ASIGNACION_MANUAL_FLAG"];
                                                        $objactividadtecnico->XA_WORK_ZONE_KEY = $valueActividad["XA_WORK_ZONE_KEY"];
                                                        $objactividadtecnico->save();
                                                   }                                           
                                               }                                    
                                        }
                                    }                      
                           } catch (Illuminate\Filesystem\FileNotFoundException $ex){
                               continue;
                           }
                        //##############################################################    
                        if ($i === $diasTotal && File::exists($rutaFile)) {
                           try {
                               File::delete($rutaFile);
                           } catch (Illuminate\Filesystem\FileNotFoundException $ex){
                               continue;
                           }
                        }
                        $resultado = $activity->getActivities($value, $dia, $dia);
                        $actividades = $resultado->data;
                        File::put($rutaFile, json_encode($actividades));
                    }
                }
            } elseif ($hora === "6" || $hora === "7" || $hora === "8" || $hora === "9" ||
                      $hora === "11" || $hora === "13" || $hora === "15") {
                $from = date("Y-m-d", strtotime($hoy));
                $to = date("Y-m-d", strtotime("$from +7 days"));
                $resultado = $activity->getActivities($value, $from, $to);
                $actividades = $resultado->data;
                ActividadOfscHistorico::insert(
                    [
                    "data" => json_encode($actividades),
                    "resource_id" => $value,
                    "created_at" => $hoy,
                    "user_at" => "952"
                    ]
                );
            }
        }
    }
}
