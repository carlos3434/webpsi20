<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcesoOfscConexionCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:conexionOfsc';

    /**
     * The coknsole command description.
     *
     * @var string
     */
    protected $description = 'Probar conexion firewal a TOA ';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        
          $wait = 10; // wait Timeout In Seconds
          $host = Config::get("wpsi.proxy.host"); //server
          //$host = '190.234.74.6'; 
          //$port = 9561;
          $port = 8590; //server
          $exc= array();
          $impr='';

          $errCode=$errStr='';
          $fp = @fsockopen($host, $port, $errCode, $errStr, $wait);
          echo "# Ping $host:$port ==> ";
          if ($fp) { 
                echo " SUCCESS \r\n";
                      
                      $url = "https://legacy-api.etadirect.com/soap/activity/v2/?wsdl";
                      $ch = curl_init();                
                      curl_setopt($ch, CURLOPT_URL, trim($url));
                      curl_setopt($ch, CURLOPT_PROXY, $host);
                      curl_setopt($ch, CURLOPT_PROXYPORT, $port);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
                      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
                      $result = curl_exec ($ch); 
                      $curl_errno = curl_errno($ch);
                      $curl_error = curl_error($ch);
                      curl_close($ch); 
                      
                      if(strpos($result, "xmlns")=== false){
                        $impr.="# No recibe XML \n";
                        $exc['trace']=$result;
                        $exc['file']='ProcesoOfscConexionCommand.php';
                        $exc['message']=$impr;
                      }

                      if ($curl_errno > 0) {
                          $impr.="# FALLO CONEXION cURL OFSC =>".$url." :  \n".$curl_error." \n";

                          $chgo = curl_init();                
                          curl_setopt($chgo, CURLOPT_URL, trim('74.125.141.94'));
                          curl_setopt($chgo, CURLOPT_PROXY, $host);
                          curl_setopt($chgo, CURLOPT_PROXYPORT, $port);
                          curl_setopt($chgo, CURLOPT_SSL_VERIFYPEER, false);        
                          curl_setopt ($chgo, CURLOPT_RETURNTRANSFER, 1); 
                          $resultgo = curl_exec ($chgo); 
                          $curl_errnogo = curl_errno($chgo);
                          $curl_errorgo = curl_error($chgo);
                          curl_close($chgo); 

                          if ($curl_errnogo > 0) {
                          $impr.="# code: (".$curl_errnogo.") - FALLO CONEXION cURL GOOGLE: ".$curl_errorgo." \n";
                          } else {
                            $impr.="# Conexion cURL GOOGLE: OK \n";
                          }

                          $exc['trace']=$result;
                          $exc['code']=$curl_errno;
                          $exc['message']=$impr;
                          $exc['file']='ProcesoOfscConexionCommand.php';
                          
                      } else {
                          $impr="\n# Data received From TOA  \n";
                      } 
                      
                fclose($fp);
          } else {
                echo " FALLO CONEXION \r\n";

                $impr="# FALLO CONEXION A FIREWALL PING: ".$host.":".$port." => ".$errStr." \n";
                $exc['code']=$errCode;
                $exc['message']=$impr;
                $exc['file']='ProcesoOfscConexionCommand.php';

          }
            echo $impr;

            if(count($exc)>0){
             $Error=New ErrorController();
             $Error->saveError($exc);
            }

            echo PHP_EOL;

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
