<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ObtenerCurlOfscCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'obtenercurl:ofsc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Descarga los reportes OFSC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $host = Config::get("sftp.host");
        $port = Config::get("sftp.port");
        $user = Config::get("sftp.user");
        $password = Config::get("sftp.password");

        $date= date("Y-m-d", strtotime ('- 1 days')); // dia anterior

        $remoteDir = "/dwh/".date("Y-m")."/".$date."/";
        $localDir = public_path().'/files_ofsc/'.date("Y-m").'/';
        
        //echo "Buscando Archivos del dia: ".$date."\n";

        $url =  'sftp://'.$user.':'.$password .'@'.$host.$remoteDir ;  
        //$Proxy =  '10.226.159.191:8590' ;          
        $ch = curl_init (); 
        curl_setopt ( $ch , CURLOPT_URL , $url); 
        curl_setopt ( $ch , CURLOPT_PORT , $port);         
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt ( $ch , CURLOPT_FOLLOWLOCATION ,  0 );   
        curl_setopt ( $ch , CURLOPT_RETURNTRANSFER ,  1 );   
        curl_setopt($ch, CURLOPT_FTPLISTONLY, 1);
        curl_setopt ( $ch , CURLOPT_HEADER ,  0);    
        $curl_scraped_page = curl_exec ( $ch ); 
        $archivo=explode("\n", $curl_scraped_page) ;
        
        if ($curl_scraped_page) {
          if (!file_exists($localDir)) {
              mkdir($localDir, 0777, true);
          }

          $newzips=array();
          
          
          foreach ($archivo as $key => $value) {
              if(stristr($value,$date.'.zip')){
                 
                 if(!file_exists($localDir.$value)){
                   //echo "Descargando Archivos del dia: ".$date."\n";
                   $archivo =  'sftp://'.$user.':'.$password .'@'.$host.$remoteDir.$value;
                   curl_setopt ( $ch , CURLOPT_URL , $archivo); 
               
                   $fp = fopen($localDir.$value, "w"); 
                   curl_setopt($ch, CURLOPT_FILE, $fp); 
                   
                   curl_exec($ch); 
                   fclose($fp); 
                   array_push($newzips, $value);
                 } 
              } 
          }
          //var_dump($curl_scraped_page);
          curl_close($ch); 
          
          $zip = new ZipArchive;
          $newfiles=array();
          foreach ($newzips as $key => $value) {
            if(stristr($value, '.zip')){
               $file=str_replace(".zip", "", $value);
               if ($zip->open($localDir.$value) === TRUE) {
                   if(!file_exists($localDir.$file)){
                      mkdir($localDir.$file, 0777, true);
                   }
                   $zip->extractTo($localDir.$file);
                   $zip->close();
                   //echo 'ok';
                   array_push($newfiles, $localDir.$file);
               } 
            } 
          }

          $url='importarofsc/importarfiles';
          $method='GET';
          Auth::loginUsingId(697);
          $array=array('files' => $newfiles);
          echo Helpers::ruta($url, $method, $array);
          Auth::logout();

        } else {
           
           echo "----- No hay respuesta de Servidor ----- \n";
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
