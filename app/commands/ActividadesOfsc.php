<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ofsc\Activity;

class ActividadesOfsc extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ofsc:actualizaractividades';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'generar un xml  de las actividades en ofsc.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
	    function array_to_xml( $data, &$xml_data ) {
	        foreach( $data as $key => $value ) {
	            if( is_array($value) ) {
	                if( is_numeric($key) ){
	                    $key = 'item'.$key; //dealing with <0/>..<n/> issues
	                }
	                $subnode = $xml_data->addChild($key);
	                array_to_xml($value, $subnode);
	            } else {
	                $xml_data->addChild("$key",htmlspecialchars("$value"));
	            }
	         }
	    }

	    $activity = new Activity();
	    $bucket = 'LARI_INS_MOVISTAR_UNO';
	    $from = date("Y-m-d");
	    $to = date("Y-m-d", strtotime("+ 7 days"));
	    // =================================================
	    $crearArchivo = false;
	    $rutaFolderJson = public_path()."/json";
	    if (!File::exists($rutaFolderJson)){
	    	File::makeDirectory($rutaFolderJson, $mode = 0777, true, true);
	    }
	    $rutaFolderActividades = $rutaFolderJson."/actividades";

	     if (!File::exists($rutaFolderActividades)){
	    	File::makeDirectory($rutaFolderActividades, $mode = 0777, true, true);
	    }
		$folder = preg_replace("/-/", "_", $bucket);

		$rutaFolder = $rutaFolderActividades."/".$folder;
		$rutaFile = $rutaFolder."/".preg_replace("/-/", "", $from).".json";
		// -- actividades_
		if (File::exists($rutaFile)) {
			unlink($rutaFile);
		}
		// --
		if (File::exists($rutaFolder)) {
			if(!File::exists($rutaFile)){
				$crearArchivo = true;
			}
		} else {
			File::makeDirectory($rutaFolder, $mode = 0777, true, true);
			if(!File::exists($rutaFile)){
				$crearArchivo = true;
			}
		}
		// --
	    $resultado = $activity->getActivities($bucket,$from, $from);
	    $actividad = $resultado->data;
		if ($crearArchivo === true) {
			File::put($rutaFile,json_encode($actividad));
		}
	    // =================================================

	    //$from = '2016-07-13';//date("Y-m-d");
	    //$from = date("Y-m-d");
	    //$to = date("Y-m-d");
	    //$to = date("Y-m-d", strtotime("+ 7 days"));
	    $resultado = $activity->getActivities($bucket,$from, $to);
	    $actividad = $resultado->data;
	    foreach ($actividad as $key => $value) {
	        $actividades['actividad'.$value['id'].'-'.$value['resource_id']] = $value;
	    }

	    $xml_data = new SimpleXMLElement('<?xml version="1.0"?><actividades></actividades>');
	    array_to_xml($actividades, $xml_data);
	    $xml_data->asXML(public_path().'/xml/actividades.xml');
	}

}
