﻿<?php
Route::any(
    'toa_outbound', function()
    {
    $file = public_path(). "/ofsc_xml/outbound.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            array('content-type'=>'application/xml')
        );
    }
    }
);
Route::any(
    'officetrackws', function()
    {
    $file = public_path(). "/ot_xml/officetrack.xml";
    if (file_exists($file)) {
        $content = file_get_contents($file);
        return Response::make(
            $content,
            200,
            array('content-type'=>'application/xml')
        );
    }
    }
);
Route::group(
    array('before' => 'auth.toa'), function()
    {
    Route::any('ofsc', 'OutboundController@getServer');
    }
);

Route::group(
    array('prefix' => 'api/v1', 'before' => 'auth.basic'), function()
    {
    Route::resource('asunto', 'AsuntoController');
    }
);/*
Route::group(
    array('before' => 'hash'), function()
    {*/
    Route::controller('api', 'ApiController');
   /* }
);*/
Route::get(
    '/', function () {
        if (Session::has('accesos')) {
            return Redirect::to('/admin.inicio');
        } else {
            return View::make('login');
        }
    }
);

Route::get(
    'salir', function () {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
);

Route::post('ot', 'OfficetrackController@getServer');
Route::get('actualizarpsi', 'GestionMovimientoController@getUpdate');
Route::controller('check', 'LoginController');
Route::controller('imagen', 'ImagenController');
Route::controller('consulta', 'ConsultaController');
//$api = new ApiController();
Route::get(
    '/{ruta}', array('before' => 'auth', function ($ruta) {
        if (Session::has('accesos')) {
            $accesos = Session::get('accesos');
            $menu = Session::get('menu');

            $val = explode("_", $ruta);
            $valores = array(
                'valida_ruta_url' => $ruta,
                'menu' => $menu
            );
            $val2 = explode('.', $ruta);
            if (count($val2) == 3) {
                if ($val2[2] != "misdatos") {
                    $nomSubMod = explode('.', $ruta);
                    $query ='SELECT `id` FROM `submodulos` WHERE `path` = ?';
                    $res = DB::select($query, array($nomSubMod[2]));
                    if (isset($res[0]->id)) {
                        DB::table('submodulo_usuario')
                            ->where('usuario_id', '=', Auth::id())
                            ->where('submodulo_id', '=', $res[0]->id)
                            ->update(
                                array(
                                   'fecha_acceso' => date("Y-m-d H:i:s"),
                                )
                            );
                    }
                }
            }
            if (count($val) == 2) {
                $dv = explode("=", $val[1]);
                $valores[$dv[0]] = $dv[1];
            }
            $rutaBD = substr($ruta, 6);
            //si tiene accesoo si accede al inicio o a misdatos
            if (in_array($rutaBD, $accesos) or
                $rutaBD == 'inicio' or $rutaBD=='mantenimiento.misdatos'
                or substr($ruta, 0, 7)=='angular' or (isset($val2[2]) && in_array($rutaBD.'#/'.$val2[2], $accesos) ) ) {
                return View::make($ruta)->with($valores);
            } else
                return Redirect::to('/');
        } else
            return Redirect::to('/');
    })
);

Route::group(
    array('before' => 'csrf'), function()
    {
        Route::controller('sms', 'SmsController');
    }
);
Route::controller('actividad', 'ActividadController');
Route::controller('agenda', 'AgendaController');
Route::controller('area', 'AreaController');
Route::controller('bandeja', 'BandejaController');
Route::controller('cat_componente', 'CatComponenteController');
Route::controller('celula', 'CelulaController');
Route::controller('configuracion', 'ConfiguracionController');
Route::controller('cupo', 'CupoController');
Route::controller('thorario', 'ThorarioController');
Route::controller('datos', 'DatosController');
Route::controller('dig_troba', 'DigTrobaController');
Route::controller('edificio_cableado', 'EdificioCableadoController');
Route::controller('empresa', 'EmpresaController');
Route::controller('estadomotivosubmotivo', 'EstadoMotivoSubmotivoController');
Route::controller('estado', 'EstadoController');
Route::controller('eventos', 'EventosController');
Route::controller('feedback', 'FeedbackLiquidadoController');
Route::controller('geoplan', 'GeoplanController');
Route::controller('gestion', 'GestionController');
Route::controller('gestion_movimiento', 'GestionMovimientoController');
Route::controller('historico', 'HistoricoController');
Route::controller('horariotipo', 'HorarioTipoController');
Route::controller('horario', 'HorarioController');
Route::controller('language', 'LanguageController');
Route::controller('lista', 'ListaController');
Route::controller('mdf', 'MdfController');
Route::controller('modulo', 'ModuloController');
Route::controller('motivo', 'MotivoController');
Route::controller('nodo', 'NodoController');
Route::controller('obs_tipo', 'ObservacionTipoController');
Route::controller('officetrack', 'OfficetrackController');
Route::controller('perfil', 'PerfilController');
Route::controller('permisoeventos', 'PermisoEventosController');
Route::controller('publicmap', 'PublicMapController');
Route::controller('quiebre', 'QuiebreController');
Route::controller('quiebregrupo', 'QuiebreGrupoController');
Route::controller('registro_manual', 'RegistroManualController');
Route::controller('reporte', 'ReporteController');
Route::controller('solucion', 'SolucionComercialController');
Route::controller('submodulo', 'SubModuloController');
Route::controller('submodulousuario', 'SubmoduloUsuarioController');
Route::controller('listado', 'ListadoController');
Route::controller('submotivo', 'SubMotivoController');
Route::controller('tecnico', 'TecnicoController');
Route::controller('testofsc', 'TestOfscController');
Route::controller('toa', 'ToaController');
Route::controller('tramo', 'TramoController');
Route::controller('troba', 'TrobaController');
Route::controller('ubigeo', 'UbigeoController');
Route::controller('upload', 'UploadController');
Route::controller('usuario', 'UsuarioController');
Route::controller('visorgps', 'VisorgpsController');
Route::controller('zonal', 'ZonalController');
Route::controller('actividadtipo', 'ActividadTipoController');
Route::controller('errores', 'ErrorController');
Route::controller('wsenvios', 'WsEnvioController');
Route::controller('estadoofsc', 'EstadoOfscController');
Route::controller('enviosofsc', 'EnviosOfscController');
Route::controller('enviosofscmasivo', 'EnviosOfscMasivoController');
Route::controller('logofsc', 'LogOfscController');
Route::controller('mensaje', 'MensajeController');
Route::controller("programaciontecnicoofsc", "ProgramacionTecnicoOfscController");

Route::controller('proyecto_edificio', 'ProyectoEdificioController');
Route::controller('segmento', 'SegmentoController');
Route::controller('tipoproyecto', 'TipoproyectoController');
Route::controller('gcasuistica', 'GestioncasuisticaController');
Route::controller('proyecto_gedificio', 'ProyectoGestionEdificioController');
Route::controller('tipoinfraestructura', 'TipoinfraestructuraController');
Route::controller('proyectoedificiodisenio', 'ProyectoEdificioDisenioController');

Route::controller('geoproyecto', 'GeoProyectoController');
Route::controller('geoproyectocapa', 'GeoProyectoCapaController');
Route::controller('geoproyectocapaelemento', 'GeoProyectoCapaElementoController');
Route::controller('tabladetalle', 'TablaDetalleController');
Route::controller('geotabla', 'GeoTablaController');
Route::controller('geomdf', 'GeoMdfController');
Route::controller('geonodopoligono', 'GeoNodoPoligonoController');


Route::controller('georeferencia', 'GeoReferenciaController');

Route::controller('geoalarmatap', 'GeoAlarmaTapController');
Route::controller('geoalarmatapcolor', 'GeoAlarmaTapColorController');
Route::controller('geoalarma', 'GeoAlarmaController');
Route::controller('usuariopassword', 'UsuarioPasswordController');

Route::controller('geocampos', 'GeoCamposController');
Route::controller('figuras', 'GeoFigurasController');

Route::controller('motivosofsc', 'MotivosofscController');
Route::controller('proyectoestado', 'ProyectoEstadoController');
Route::controller('proyectotipo', 'ProyectoTipoController');
Route::controller('casuistica', 'CasuisticaController');
Route::controller('casuisticaproyectoestado', 'CasuisticaProyectoEstadoController');
Route::controller('location', 'LocationController');

Route::controller('officetrack_formulariosinternos', 'FormularioInternoOTController');
Route::controller('componente', 'ComponenteController');
Route::controller('importarofsc','ImportarOfscController');
Route::controller('repofsc','RepofscController');
Route::controller('usuarioofsc', 'UsuarioOfscController');
Route::resource('ofscangular', 'OfscAngularController');
Route::controller('recursoofsc', 'RecursoOfscController');

Route::get(
    'images/{dir}/{dimension}/{file}', 
    'ImagenController@getImage'
);

Route::get(
    'deco/activacionrefresh/{id}/{carnet}', function ($id,$carnet) {
    return View::make('public.activacionrefreshdeco')
    ->with(['id'=>$id,'carnet'=>$carnet]);
    }
);
Route::group(
    array('before' => 'csrf'), function()
    {
        Route::resource('decodificador', 'CatComponenteController');
        Route::resource('tecnicos', 'TecnicoController');
        Route::controller('componentes', 'ComponenteController');
        Route::controller('cablemodem', 'CableModemController');
        Route::resource('tecnicosofsc', 'TecnicosOfscController');
        Route::resource('actividadesofsc', 'ActividadesOfscController');
        Route::resource('locationtecnicoofsc', 'LocationTecnicoOfscController');
    }
);

Route::get(
    'buscar/componente', function () {
        return View::make('public.buscarcomponentes');
    }
);

Route::get(
    'prueba/cablemodem', function () {
        return View::make('public.cablemodem');
    }
);


Route::get(
    'mapa/mapatecnico', function () {
        return View::make('public.mapatecnico');
    }
);
Route::get(
    'mapa/mapatecnicotrain', function () {
        return View::make('public.mapatecnicotrain');
    }
);

Route::controller('ofsclegados','OfscLegadosController');