﻿<?php
Route::get('llenada_tabla', function () {
    $tecnicos = Tecnico::whereRaw('bucket_id is not null and bucket_id != 0')
                ->groupBy('bucket_id')->get();
    foreach ($tecnicos as $tecnico) {
        $empresaBucket = new EmpresaBucket();
        $empresaBucket->empresa_id = $tecnico->empresa_id;
        $empresaBucket->bucket_id = $tecnico->bucket_id;
        $empresaBucket->save();
    }
    print_r("expression");
});

Route::get('mongo', function () {
    $empresa = Tecnico::all();

    foreach ($empresa as $table) {
        $tecnicoMongo = new TecnicoMongo();
        foreach (TecnicoMongo::$rules as $key => $value) {
            if (isset($table[$key])) {
                $tecnicoMongo[$key] = $table[$key];
            }
        }
        $tecnicoMongo->save();
    }
});


use Ofsc\Resources;
Route::get('apiresources', function () {
    $resource = new Resources();
    $response = $resource->updateBucket("CA4424", "BK_LARI_MICROZONA_LL");
    print_r($response);
    /*if (isset($response->data['resourceId'])) {
        print_r($response->data["resourceId"]);
    } else {
        print_r("error");
    }*/
});
use Ofsc\Users;
Route::get('apiusers', function () {
    $users = new Users();
    $response = $users->updateUser("LA1177", "abc123456");
    // if (isset($response->data['mainResourceId'])) {
    //     print_r("ok");
    // } else {
    //     print_r("error");
    // }
    print_r($response);

});


Route::get('pruebatestph', function () {
    //dd("conexion Ok");
    return Redirect::to('http://www.movistar1.com:7020/testcms/go')->with(['sql'=>"selecr * from jojo"]);
    // return View::make("http://www.movistar1.com:7020/prodtotest"); //->with(['id'=>$id,'hashg'=>$hash]);
    
});

use Legados\models\SolicitudTecnicaCms;
use Legados\models\SolicitudTecnicaUltimo;
use Legados\models\SolicitudTecnicaMovimiento;
Route::get('waa', function () {

    $bucket_ofsc = "BK_PRUEBA";
        $datos = SolicitudTecnicaUltimo::from('solicitud_tecnica_ultimo as stu')
                ->select(
                    'stlr.trama'
                )
                ->join(
                    'solicitud_tecnica_log_recepcion as stlr', 
                    'stlr.solicitud_tecnica_id', '=', 'stu.solicitud_tecnica_id'
                )
                ->join(
                    'bucket as b', 
                    'b.id', '=', 'stu.bucket_id'
                )
                // ->whereRaw('date(stlr.created_at) = date(now())')
                ->where('b.bucket_ofsc', '=', $bucket_ofsc)
                ->limit(1)
                ->get();

        $array = array();
        foreach ($datos as $value) {
            $array[] = json_decode($value->trama, true);
        }
        
        $fileName = $bucket_ofsc.".json";
        $rutaFolder = public_path()."/json/solicitudes/".$bucket_ofsc;
        $existeFolder = FileHelper::validaCrearFolder($rutaFolder);
        $rutaFile = $rutaFolder."/".$fileName;
        if ($existeFolder === true) {
            if (File::exists($rutaFile)) {
                unlink($rutaFile);
            }
            File::put($rutaFile, json_encode($array));
        }
});


Route::get('updatebucketultimo', function () {
    print_r("ultimo");
    $ultimo = SolicitudTecnicaUltimo::select("solicitud_tecnica_ultimo.id", "b.id as bucket_id")
                    ->join('bucket as b', 'b.bucket_ofsc', '=', 'solicitud_tecnica_ultimo.resource_id')
                    ->get();

    foreach ($ultimo as $key => $value) {
        DB::statement("UPDATE solicitud_tecnica_ultimo SET bucket_id = '".$value["bucket_id"]."' where id = '".$value["id"]."'");
    }
});

Route::get('updatebucketdetalle', function () {
    print_r("detalle");
    $movimiento = SolicitudTecnicaMovimiento::select("solicitud_tecnica_movimiento.id", "b.id as bucket_id")
                    ->join('bucket as b', 'b.bucket_ofsc', '=', 'solicitud_tecnica_movimiento.resource_id')
                    ->get();

    foreach ($movimiento as $key => $value) {
        DB::statement("UPDATE solicitud_tecnica_movimiento SET bucket_id = '".$value["bucket_id"]."' where id = '".$value["id"]."'");
    }
});

Route::get('updatecms', function () {
    print_r("expression");
    $solicitudCms = SolicitudTecnicaCms::select("solicitud_tecnica_id", "orden_trabajo", "cod_servicio")->get();
    foreach ($solicitudCms as $key => $value) {
        // SolicitudTecnicaUltimo::where('solicitud_tecnica_id', $value["solicitud_tecnica_id"])
        //     ->update([
        //         'num_orden_trabajo' => $value["orden_trabajo"],
        //         'num_orden_servicio' => $value["cod_servicio"]
        //     ]);
        DB::statement("UPDATE solicitud_tecnica_ultimo SET num_orden_trabajo = '".$value["orden_trabajo"]."', num_orden_servicio = '".$value["cod_servicio"]."' where solicitud_tecnica_id = '".$value["solicitud_tecnica_id"]."'");
    }
});

use Legados\models\SolicitudTecnica;
use Legados\models\SolicitudTecnicaMovimiento as Movimiento;
use Legados\models\SolicitudTecnicaUltimo as Ultimo;
use Legados\models\SolicitudTecnicaCms as Cms;
Route::get('st', function () {
    $array =[
        'xa_note'=>'6613',
        'xa_note2'=>''
    ];

    $solicitudTecnica = SolicitudTecnica::where("id_solicitud_tecnica", 'A0000000090')
                ->orderBy("updated_at", "DESC")
                ->first();

    $ultimo = $solicitudTecnica->ultimo;
    
    foreach (Ultimo::$rules as $key => $value) {
        if (array_key_exists($key, $array)) {
            $ultimo[$key] = $array[$key];
        }
    }
    $solicitudTecnica->ultimo()->save($ultimo);


    $ultimoMovimiento = $solicitudTecnica->ultimoMovimiento->replicate();
    foreach (Movimiento::$rules as $key => $value) {
        if (array_key_exists($key, $array)) {
            $ultimoMovimiento[$key] = $array[$key];
        }
    }
    $ultimoMovimiento->save();

    return;

});

Route::get("coordenadaslima", function () {
    //-11.754590, -76.606141
    //-12.027581, -77.055700
    //-8.824126, -78.249223
    //-18.233415, -69.937639
    //-11.901182, -76.943634
    $ok = FuncionesMatematicas::validarRangoXYLima(-76.943634, -11.901182);
    dd($ok);
});
use Legados\helpers\SolicitudTecnicaHelper;
Route::get("validarcoordenadasst", function () {

    $solicitud = new stdClass;
    $solicitud->coordx_cliente = "0.0000000000000";
    $solicitud->coordy_cliente = "0.0000000000000";
    $solicitud->coordx_direccion_tap = "0.0000000000000";
    $solicitud->coordy_direccion_tap = "0.0000000000000";
    $solicitud->cod_nodo = "LO";
    $solicitud->cod_troba = "R026";
    $solicitud->cod_amplificador = "";
    $solicitud->cod_tap = "06";
    $solicitud->zonal = "LIM";
    $solicitud->cod_servicio = "1726844";

    $respuesta = SolicitudTecnicaHelper::setCoordenadas($solicitud, 1);
});
Route::get('imagenes/{src}', function ($src) {
    $pathRelativa = 'imagenes/';
    $path=storage_path($pathRelativa);

    $cacheImage = Image::cache(function ($image) use ($src, $path/*, $size*/) {
        return $image->make($path.$src)->resize(300, 200);//->resize($size[0], $size[1]);
    }, 10, true);

    return Response::make($cacheImage, 200, array('Content-Type' => 'image/jpeg'));
});

use Legados\STCmsApi;

Route::get('holaw', function() {
    $trama = DB::table('solicitud_tecnica_log_recepcion as st')
                    ->where('st.id_solicitud_tecnica', '=', 'C0000000061')
                    ->orderBy('st.id', 'desc')->pluck('trama');
    return $trama;                
});
Route::get('respuesta/{solicitud}', function ($solicitud) {
    $trama = DB::table('solicitud_tecnica_log_recepcion as st')
                    ->where('st.id_solicitud_tecnica', '=', $solicitud)
                    ->orderBy('st.id', 'desc')->first();
    
    /*$demo = [
        'id_solicitud_tecnica' => $trama->id_solicitud_tecnica,
        'id_respuesta' => $trama->code_error,
        'observacion' => $trama->descripcion_error,
        'solicitud_tecnica_id' => $trama->solicitud_tecnica_id
    ];*/
    print_r($array);
    return;
    $demo = (array)$trama;
    print_r($demo);
    $objApiST = new STCmsApi('GESTEL Legado');
    $objApiST->responderST($demo);
});

Route::get(
    'pruebaselectricas',
    function () {
        return View::make('public.pruebaselectricas');
    }
);

use Legados\PruebaElectricaService;

Route::get(
    'wservice',
    function () {
        $api = new PruebaElectricaService();
        $rst= $api->obtenerHistorico('15671606');
        var_dump($rst);

        $rst= $api->obtener('15671606');
        var_dump($rst);

        $rst= $api->ejecutar('15671606', 'ProElectricaText');
        var_dump($rst);
        return;
    }
);

/*end prueba williams*/

Route::group(
    array('before' => 'auth.toa'), function()
    {
    Route::any('ofsc', 'OutboundController@getServer');
    Route::any('ofsc_test', 'OutboundTestController@getServer');
    }
);
Route::group(
    array('before' => 'auth.toa'), function()
    {
    Route::any('deco_cms', 'RefreshDecoCmsController@getServer');
    }
);

Route::group(
    array('prefix' => 'api/v1', 'before' => 'auth.basic'), function()
    {
    Route::resource('asunto', 'AsuntoController');
    }
);
// Route::group(
//     array('before' => 'hash'), function()
//     {
    Route::controller('api', 'ApiController');
//     }
// );
Route::get(
    '/', function () {
        if (Session::has('accesos')) {
            return Redirect::to('/admin.inicio');
        } else {
            return View::make('login');
        }
    }
);

Route::get(
    'salir', function () {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
);

Route::post('ot', 'OfficetrackController@getServer');
Route::get('actualizarpsi', 'GestionMovimientoController@getUpdate');
Route::group(['before' => 'csrf'], function() {
    Route::controller('check', 'LoginController');
});
Route::controller('imagen', 'ImagenController');
Route::controller('consulta', 'ConsultaController');
//$api = new ApiController();

Route::get('/deploy', 'ServerController@deploy');
Route::get(
    '/{ruta}', array('before' => 'auth', function ($ruta) {
        if (Session::has('accesos')) {
            $accesos = Session::get('accesos');
            $menu = Session::get('menu');

            $val = explode("_", $ruta);
            $valores = array(
                'valida_ruta_url' => $ruta,
                'menu' => $menu
            );
            $val2 = explode('.', $ruta);
            if (count($val2) == 3) {
                if ($val2[2] != "misdatos") {
                    $nomSubMod = explode('.', $ruta);
                    $query ='SELECT `id` FROM `submodulos` WHERE `path` = ?';
                    $res = DB::select($query, array($nomSubMod[2]));
                }
            }
            if (count($val) == 2) {
                $dv = explode("=", $val[1]);
                $valores[$dv[0]] = $dv[1];
            }
            $rutaBD = substr($ruta, 6);
            //si tiene accesoo si accede al inicio o a misdatos
            if (in_array($rutaBD, $accesos) or
                $rutaBD == 'inicio' or $rutaBD=='mantenimiento.misdatos'
                or substr($ruta, 0, 7)=='angular' or (isset($val2[2]) && in_array($rutaBD.'#/'.$val2[2], $accesos) ) ) {
                return View::make($ruta)->with($valores);
            } else
                return Redirect::to('/');
        } else
            return Redirect::to('/');
    })
);

Route::group(
    array('before' => 'csrf'), function()
    {
        Route::controller('sms', 'SmsController');
    }
);

Route::controller('propiedades_ofsc', 'PropiedadesOfscController');
Route::controller('agenda', 'AgendaController');
Route::controller('aseguramiento', 'AseguramientoController');
Route::controller('bandeja', 'BandejaController');
Route::controller('cat_componente', 'CatComponenteController');
Route::controller('configuracion', 'ConfiguracionController');
Route::controller('cupo', 'CupoController');
Route::controller('thorario', 'ThorarioController');
Route::controller('datos', 'DatosController');
Route::controller('dig_troba', 'DigTrobaController');
Route::controller('edificio_cableado', 'EdificioCableadoController');
Route::controller('empresa', 'EmpresaController');
Route::controller('emergencia', 'EmergenciasController');
Route::controller('estadomotivosubmotivo', 'EstadoMotivoSubmotivoController');
Route::controller('estado', 'EstadoController');
Route::controller('eventos', 'EventosController');
Route::controller('feedback', 'FeedbackLiquidadoController');
Route::controller('geoplan', 'GeoplanController');
Route::controller('gestion', 'GestionController');
Route::controller('gestion_movimiento', 'GestionMovimientoController');
Route::controller('historico', 'HistoricoController');
Route::controller('horariotipo', 'HorarioTipoController');
Route::controller('horario', 'HorarioController');
Route::controller('language', 'LanguageController');
Route::controller('lista', 'ListaController');
Route::controller('mdf', 'MdfController');
Route::controller('modulo', 'ModuloController');
Route::controller('motivo', 'MotivoController');
Route::controller('nodo', 'NodoController');
Route::controller('obs_tipo', 'ObservacionTipoController');
Route::controller('officetrack', 'OfficetrackController');
Route::controller('perfil', 'PerfilController');
Route::controller('permisoeventos', 'PermisoEventosController');
Route::controller('publicmap', 'PublicMapController');
Route::controller('quiebre', 'QuiebreController');
Route::controller('quiebregrupo', 'QuiebreGrupoController');
Route::controller('registro_manual', 'RegistroManualController');
Route::controller('reporte', 'ReporteController');
Route::controller('solucion', 'SolucionComercialController');
Route::controller('listado', 'ListadoController');
Route::controller('submotivo', 'SubMotivoController');
Route::controller('tecnico', 'TecnicoController');
Route::controller('testofsc', 'TestOfscController');
Route::controller('toa', 'ToaController');
Route::controller('tramo', 'TramoController');
Route::controller('troba', 'TrobaController');
Route::controller('ubigeo', 'UbigeoController');
Route::controller('upload', 'UploadController');
Route::controller('usuario', 'UsuarioController');
Route::controller('visorgps', 'VisorgpsController');
Route::controller('zonal', 'ZonalController');
Route::controller('actividadtipo', 'ActividadTipoController');

Route::controller('estadoofsc', 'EstadoOfscController');
// Route::controller('enviosofsc', 'EnviosOfscController');
Route::controller('envioLegado','EnvioLegadoController');
Route::controller('logofsc', 'LogOfscController');
Route::controller('mensaje', 'MensajeController');
Route::controller("programaciontecnicoofsc", "ProgramacionTecnicoOfscController");
Route::controller("ordenesofscmovimientos", "OrdenOfscMovimientoController");

Route::controller('proyecto_edificio', 'ProyectoEdificioController');
Route::controller('segmento', 'SegmentoController');
Route::controller('tipoproyecto', 'TipoproyectoController');
Route::controller('gcasuistica', 'GestioncasuisticaController');
Route::controller('proyecto_gedificio', 'ProyectoGestionEdificioController');
Route::controller('tipoinfraestructura', 'TipoinfraestructuraController');
Route::controller('proyectoedificiodisenio', 'ProyectoEdificioDisenioController');
Route::controller('asignacionmotivousuario', 'AsignacionMotivoUsuarioController');
Route::controller('incidencia', 'IncidenciaController');
Route::controller('incidenciareq', 'IncidenciaReqController');
Route::controller('tipificacion', 'TipificacionController');
Route::controller('solicitutecnica','SolicitudTecnicaController');
Route::controller('solicitutecnicarecepcion','SolicitudTecnicaRecepcionController');
Route::controller('uploadlog', 'UploadLogController');
Route::controller('informacion', 'PsiInformacionController');


Route::controller('geoproyecto', 'GeoProyectoController');
Route::controller('geoproyectocapa', 'GeoProyectoCapaController');
Route::controller('geoproyectocapaelemento', 'GeoProyectoCapaElementoController');
Route::controller('tabladetalle', 'TablaDetalleController');
Route::controller('geotabla', 'GeoTablaController');
Route::controller('geomdf', 'GeoMdfController');
Route::controller('geonodopoligono', 'GeoNodoPoligonoController');


Route::controller('georeferencia', 'GeoReferenciaController');

Route::controller('geoalarmatap', 'GeoAlarmaTapController');
Route::controller('geoalarmatapcolor', 'GeoAlarmaTapColorController');
Route::controller('geoalarma', 'GeoAlarmaController');
Route::controller('usuariopassword', 'UsuarioPasswordController');

Route::controller('geocampos', 'GeoCamposController');
Route::controller('figuras', 'GeoFigurasController');
Route::controller('informacion', 'PsiInformacionController');


Route::controller('proyectoestado', 'ProyectoEstadoController');
Route::controller('proyectotipo', 'ProyectoTipoController');
Route::controller('casuistica', 'CasuisticaController');
Route::controller('casuisticaproyectoestado', 'CasuisticaProyectoEstadoController');
Route::controller('location', 'LocationController');

Route::controller('officetrack_formulariosinternos', 'FormularioInternoOTController');
Route::controller('componente', 'ComponenteController');
Route::controller('importarofsc', 'ImportarOfscController');
Route::controller('repofsc', 'RepofscController');
Route::controller('coordenadasTecnicas', 'CoordenadasTecnicasController');
Route::resource('ofscangular', 'OfscAngularController');
Route::controller('recursoofsc', 'RecursoOfscController');

Route::controller('asistenciatecnicoofsc', 'AsistenciaTecnicoOfscController');



Route::group(['before' => 'auth'], function() {
    Route::controller('conformidad', 'ConformidadController');
});

Route::post("tecnicosofsc/listadotecnico", "TecnicosOfscController@postListadotecnico");


Route::get(
    'images/{dir}/{dimension}/{file}', 
    'ImagenController@getImage'
);

Route::get(
    'deco/activacionrefresh/{id}/{carnet}', function ($id,$carnet) {
    return View::make('public.activacionrefreshdeco')
    ->with(['id'=>$id,'carnet'=>$carnet]);
    }
);

//http://webpsi20/deco/$2y$10$9g8OImQYw8IurBg1ocAL0E92a9DurXay0kyrkfjGFfNcp8TNEMbS
Route::get('deco/legado/{id}', function ($id) {
    $key = '$2y$10$9g8OImQYw8IurBg1ocAL0E92a9DurXay0kyrkfjGFfNcp8TNEMbS';
    if ($key===$id) {
        $acceso="\$PSI20\$";
        $clave="\$1st3m@\$";
        $hash = hash('sha256', $acceso.$clave);
        return View::make('public.decolegado')->with(['id'=>$id,'hashg'=>$hash]);
    }
    return Redirect::to('/');
});
Route::group(
    array('before' => 'csrf'), function()
    {
        Route::resource('decodificador', 'CatComponenteController');

        Route::resource('tecnicos', 'TecnicoController');
        Route::controller('componentes', 'ComponenteController');
        Route::controller('cablemodem', 'CableModemController');
        Route::resource('tecnicosofsc', 'TecnicosOfscController');
        Route::resource('actividadesofsc', 'ActividadesOfscController');
        Route::resource('locationtecnicoofsc', 'LocationTecnicoOfscController');
        Route::resource('routingoofsc', 'ActividadesRoutingOfscController');
        Route::controller('historico_mapatecnico', 'HistoricoMapaTecnicoController');
    }
);

Route::get(
    'buscar/componente', function () {
        return View::make('public.buscarcomponentes');
    }
);

Route::get(
    'prueba/cablemodem', function () {
        return View::make('public.cablemodem');
    }
);

Route::get(
    '/mapa/mapatecnico', function () {
        $long = Config::get("validacion.lari-ate.longitude");
        $lat = Config::get("validacion.lari-ate.latitude");
        return View::make('public.mapatecnico')
                ->with(['long_bucket'=>$long,'lat_bucket'=>$lat,'entorno'=>'PROD']);
    }
);
Route::get(
    '/mapa/emergencias', function () {
        $long = Config::get("validacion.lari-ate.longitude");
        $lat = Config::get("validacion.lari-ate.latitude");
        return View::make('public.emergencias')
        ->with(['long_bucket'=>$long,'lat_bucket'=>$lat,'entorno'=>'PROD']);
    }
);
Route::get(
    '/mapa/tracking', function () {
        $long = Config::get("validacion.lari-ate.longitude");
        $lat = Config::get("validacion.lari-ate.latitude");
        return View::make('public.tracking')
        ->with(['long_bucket'=>$long,'lat_bucket'=>$lat,'entorno'=>'PROD']);
    }
);
Route::get(
    'mapa/mapatecnicotrain', function () {
        $long = Config::get("validacion.lari-ate.longitude");
        $lat = Config::get("validacion.lari-ate.latitude");
        return View::make('public.mapatecnicotrain')
        ->with(['long_bucket'=>$long,'lat_bucket'=>$lat,'entorno'=>'TRAIN']);
    }
);
Route::get(
    'mapa/mapaactividadesrouting', function () {
        return View::make('public.mapaactividadesrouting');
    }
);
Route::controller('filtrocupos', 'FiltroCuposController');
//******

// Route::controller('criterios', 'CriteriosController');
// Route::controller('parametro', 'ParametroController');
Route::controller('clase', 'ClaseController');
Route::controller('requerimiento', 'RequerimientoController');
Route::controller('jefatura', 'JefaturaController');
Route::controller('grupo', 'GrupoController');
Route::controller('departamento', 'DepartamentoController');
Route::controller('provincia', 'ProvinciaController');
Route::controller('distrito', 'DistritoController');
Route::controller('vip', 'VipController');
Route::controller('indduo', 'IndduoController');
Route::controller('tipotecnologia', 'TipotecnologiaController');



Route::controller('operacion', 'OperacionController');
Route::controller('geoamplificador', 'GeoAmplificadorController');
Route::controller('geotap', 'GeoTapController');
Route::controller('tipopersona', 'TipoPersonaController');
Route::controller('listado_lego', 'ListadoLegoController');
Route::controller('analizador', 'AnalizadorController');

// Route::controller('bandejalegado', 'BandejaLegadoController');


Route::get(
    'deco/activacion/{codactu}', function ($codactu) {
    return View::make('public.activaciondecolegado')
    ->with(['codactu'=>$codactu]);
    }
);

Route::get(
    'cable/cablemodem/{cliente}/{codactu}', function ($cliente, $codactu) {
    return View::make('public.cablemodemofsc')
    ->with(['cliente'=>$cliente,'codactu'=>$codactu]);
    }
);

Route::get(
    'cable/cablemodem/{cliente}/{codactu}/{solicitud}', function ($cliente, $codactu, $solicitud) {
    return View::make('public.cablemodemtoa')
    ->with(['cliente'=>$cliente,'codactu'=>$codactu,'solicitud'=>$solicitud]);
    }
);

Route::get(
    'actualizarxy/{codactu}/{solicitud}', function ($codactu, $solicitud) {
    return View::make('public.actualizarxy')
    ->with(['codactu'=>$codactu,'solicitud'=>$solicitud]);
    }
);


Route::get(
    'componentes.operaciones/{codactu}/{usuario}/{tipost}/{tipolegado}', function ($codactu, $usuario, $tipost, $tipolegado) {
    return View::make('public.componentes')
    ->with(['codactu'=>$codactu, 'usuario'=>$usuario, 'tipost'=>$tipost, 'tipolegado'=>$tipolegado]);
    }
);

Route::get(
    'componentes.operaciones/{codactu}/{usuario}/{tipost}/{tipolegado}/{idsolicitudtecnica}', function ($codactu, $usuario, $tipost, $tipolegado, $idsolicitudtecnica) {
    return View::make('public.componentes')
    ->with(['codactu'=>$codactu, 'usuario'=>$usuario, 'tipost'=>$tipost, 'tipolegado'=>$tipolegado, 'idsolicitudtecnica'=>$idsolicitudtecnica]);
    }
);

Route::get(
    'assia/diagnostico', function () {
        return View::make('public.assia');
    }
);

Route::get(
    'vue/table', function () {
        return View::make('public.table');
    }
);

// Route::group(
//     array('before' => 'csrf'), function()
//     {
        Route::controller('componentelegado', 'Legados\controllers\ComponenteLegadoController');
//     }
// );
Route::controller('cablemodemofsc', 'CableModemOfscController');
Route::controller('tarea', 'TareaController');
Route::controller('assia', 'AssiaController');
Route::controller('cierretest', 'TestLegadoController');
Route::controller('pcba', 'PcbaController');

Route::controller('testcms', 'TestLegadoCmsController');
Route::controller('geodireccion', 'GeoDireccionController');
Route::controller('decodificadorequivalencia', 'DecodificadorEquivalenciaController');
Route::controller('horarioresource', 'HorarioResourceController');
Route::controller('dia', 'DiaController');
