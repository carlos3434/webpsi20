<?php
namespace Dashboard\controllers;
use Legados\models\SolicitudTecnica as ST;
use Legados\models\SolicitudTecnicaLogRecepcion as Log;
use Legados\models\EnvioLegado as EnvioLegado;

class SolicitudTecnicaMonitorController extends \BaseController
{
	public function getUltimosst()
	{
		$ultimos = ST::orderBy("created_at", "DESC")
			->skip(0)
			->take(20)
			->get();

		return \Response::json(
            $ultimos
       	);
	}

	public function getUltimosstintentos()
	{
		$ultimos = Log::orderBy("created_at", "DESC")
			->skip(0)
			->take(20)
			->get();

		return \Response::json(
            $ultimos
       	);
	}

	public function getUltimosmensajeoutbound()
	{
		$ultimos = \Mensaje::select("created_at", "updated_at", "estado", "subject")
            ->orderBy("created_at", "DESC")
            ->whereNotIn("subject", ["INT_SMS", "NOT_LEG"])
			->skip(0)
            ->take(50)
            ->get();

		return \Response::json(
            $ultimos
       	);
	}

	public function getUltimossenviolegado()
	{
		$ultimos = EnvioLegado::select("envio_legado.host", "envio_legado.accion", "envio_legado.created_at", "st.id_solicitud_tecnica")
            ->leftJoin("solicitud_tecnica AS st", "st.id", "=", "envio_legado.solicitud_tecnica_id")
            ->orderBy("envio_legado.created_at", "DESC")
			->skip(0)
			->take(50)
			->get();

		return \Response::json(
            $ultimos
       	);
	}
}