<?php
namespace Dashboard\commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class CommandPrueba extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ofsc:comandoprueba';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'genera un historico en BD y un excel con el reporte de coordenadas del dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        dd("aqui estoy");
        $mensajesvalidacion = Mensaje::select("id", "created_at", "updated_at", "body", "estado")
            ->where(["subject" => "TOA_VALIDATE_COORDS", "estado" => "delivered"])
            ->whereRaw("DATE(created_at) >= DATE_SUB(NOW(), INTERVAL 1 DAY)")
            ->get();
        $datatmpexcel = [];
        if (count($mensajesvalidacion) > 0) {
            $hoy = date("Y-m-d");
            MensajesResumen::whereRaw("DATE(created_at) < DATE_SUB(NOW(), INTERVAL 31 DAY)")->delete();
            MensajesResumen::whereRaw("fecha = '{$hoy}'")->delete();
            foreach ($mensajesvalidacion as $key => $value) {
                $tiempoInicio = strtotime($value->created_at);
                $tiempoFin = strtotime($value->updated_at);
                $tiempo = $tiempoFin - $tiempoInicio;
                $tiempo = $tiempo/60;
                $tiempo = round($tiempo, 4);

                $body = str_replace(["<![CDATA[","]]>"], "", $value->body);
                    if (Helpers::isValidXml($body)) {
                        $body = simplexml_load_string($body);
                    } else {
                        $body = "";
                    }
                    try {
                       if ($body!="") {
                            $value->num_requerimiento = $body->appt_number->__toString();
                            $value->resource_id = $body->external_id->__toString();
                            $value->subject = $body->subject->__toString();
                        } 
                    } catch (Exception $e) {
                        continue;
                    }
                $datatmpexcel[] = [
                    "fecha" => date("Y-m-d", strtotime($value->created_at)),
                    "message_id" => $value->id,
                    "num_requerimiento" => $value->num_requerimiento,
                    "tiempo" => $tiempo,
                    "resource_id" => $value->resource_id,
                    "fec_sending" => date("Y-m-d H:i:s", strtotime($value->created_at)),
                    "fec_delivered" => date("Y-m-d H:i:s", strtotime($value->updated_at)),
                    "subject" => "TOA_VALIDATE_COORDS"
                ];
            }
            MensajesResumen::insert($datatmpexcel);
        }
        
    }
}
