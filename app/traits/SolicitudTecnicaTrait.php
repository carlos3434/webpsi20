<?php

trait SolicitudTecnicaTrait {

    public function scopeGetFechaEnvioToa($query)
    {
        return $query->leftjoin('solicitud_tecnica_movimiento as sm', function($lj) {
            $lj->on('u.solicitud_tecnica_id', '=', 'sm.solicitud_tecnica_id')
                ->where('sm.estado_st', '=', 1);
           
        });
           /* ( 
            'solicitud_tecnica_movimiento as sm',
            'sm.solicitud_tecnica_id',
            '=',
            'u.solicitud_tecnica_id'

        )->whereRaw('sm.estado', '=', 'Pendiente');*/
    }
    public function scopeGetUltimoCms($query)
    {
        return $query->join(
            'solicitud_tecnica_ultimo as u',
            'u.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica_cms.solicitud_tecnica_id'
        );
    }

    public function scopeGetUltimoGestelProvision($query)
    {
        return $query->join(
            'solicitud_tecnica_ultimo as u',
            'u.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica_gestel_provision.solicitud_tecnica_id'
        );
    }

    public function scopeGetUltimoGestelAveria($query)
    {
        return $query->join(
            'solicitud_tecnica_ultimo as u',
            'u.solicitud_tecnica_id',
            '=',
            'solicitud_tecnica_gestel_averia.solicitud_tecnica_id'
        );
    }

    public function scopeGetActividades($query)
    {
        return $query->leftJoin(
            'actividades as act',
            'act.id',
            '=',
            'u.actividad_id'
        );
    }


    public function scopeGetActividadTipo($query)
    {
        return $query->leftJoin(
            'actividades_tipos as at',
            'at.id',
            '=',
            'u.actividad_tipo_id'
        );
    }

    public function scopeGetQuiebre($query)
    {
        return $query->leftJoin(
            'quiebres as q',
            'q.id',
            '=',
            'u.quiebre_id'
        );
    }

    public function scopeGetEstadoofsc($query)
    {
        return $query->leftJoin(
            'estados_ofsc as eo',
            'eo.id',
            '=',
            'u.estado_ofsc_id'
        );
    }

    public function scopeGetTecnico($query)
    {
        return $query->leftJoin(
            'tecnicos as t',
            't.id',
            '=',
            'u.tecnico_id'
        );
    }

    public function scopeGetMotivoofsc($query)
    {
        return $query->leftJoin(
            'motivos_ofsc as mo',
            'mo.id',
            '=',
            'u.motivo_ofsc_id'
        );
    }

    public function scopeGetBucket($query)
    {
        return $query->leftJoin(
            'bucket as bu',
            'bu.id',
            '=',
            'u.bucket_id'
        );
    }

    public function scopeGetSubmotivoofsc($query)
    {
        return $query->leftJoin(
            'submotivos_ofsc as so',
            'so.id',
            '=',
            'u.submotivo_ofsc_id'
        );
    }

    public function scopeGetEstadoaseguramiento($query)
    {
        return $query->leftJoin(
            "estados_aseguramiento as ea",
            "ea.id",
            '=',
            "u.estado_aseguramiento"
        );
    }

    public function scopeGetMovimientos($query)
    {
        return $query->leftJoin(
            "solicitud_tecnica_movimiento as stm",
            "stm.solicitud_tecnica_id",
            '=',
            "u.solicitud_tecnica_id"
        );
    }
    public function scopeGetParametro($query)
    {
        return $query->leftJoin(
            'parametro as p',
            'p.id',
            '=',
            'u.parametro_id'
        );
    }

    public function scopeGetMovimientosDetalle($query)
    {
        return $query->leftJoin(
            "solicitud_tecnica_movimiento_detalle as stmd",
            "stmd.solicitud_tecnica_movimiento_id",
            '=',
            "stm.id"
        )->addSelect(
            \DB::raw("REPLACE((IFNULL(GROUP_CONCAT(stmd.campo1), '')), ',', '|') as tematico1"),
            \DB::raw("REPLACE((IFNULL(GROUP_CONCAT(stmd.campo2), '')), ',', '|') as tematico2"),
            \DB::raw("REPLACE((IFNULL(GROUP_CONCAT(stmd.campo3), '')), ',', '|') as tematico3"),
            \DB::raw("REPLACE((IFNULL(GROUP_CONCAT(stmd.campo4), '')), ',', '|') as tematico4")
        );
    }
}
