<?php

namespace Repositories;

use Abstracts\Repository as AbstractRepository;

class GestionEdificioRepository 
    extends AbstractRepository 
    implements GestionEdificioRepositoryInterface
{

    // This is where the "magic" comes from:
    protected $modelClassName = 'GestionEdificio';

    // This class only implements methods specific to the UserRepository
    public function findGestionEdificioByColumn($column)
    {
        $where = call_user_func_array(
            "{$this->modelClassName}::where", 
            array($column)
        );
        return $where->get();
    }

    public function newGestionEdificio()
    {
        return new \GestionEdificio();
    }

    public function findOrFail($id)
    {
        return \GestionEdificio::findOrFail($id);
    }

    public function getGestionEdificios()
    {
        $gestionEdificios = \GestionEdificio::all();
        $data = [];
        foreach ($gestionEdificios as $key => $value) {
            $data[$value['id']] = $value['nombre'];
        }
        return $data;
    }

    public function getTotalRecords()
    {
        $result = \GestionEdificio::count();
        return $result;
    }

    public function getGestionesEdificiosProyectos($proyectoEdificioId)
    {
        //$result = DB::select("query");
        $result = \DB::table('gestiones_edificios as ge')
                ->join(
                    'proyecto_tipo as pt', 
                    'ge.proyecto_tipo_id', '=', 'pt.id'
                )
                ->join(
                    'casuisticas as c', 
                    'ge.casuistica_id', '=', 'c.id'
                )
                ->join(
                    'proyectos_estados as pe', 
                    'ge.proyecto_estado_id', '=', 'pe.id'
                )
                ->leftJoin(
                    'usuarios as us', 
                    'ge.usuario_created_at', '=', 'us.id'
                )
                ->select(
                    \DB::raw(
                        '( select @rownum := @rownum + 1 
                        from ( select @rownum := 0 ) d2 ) AS fila'
                    ),
                    //'ge.*',
                    'ge.id',     
                    \DB::raw('date(ge.fecha_inicio) AS fecha_inicio'),
                    \DB::raw('date(ge.fecha_seguimiento) AS fecha_seguimiento'),
                    \DB::raw('date(ge.fecha_compromiso) AS fecha_compromiso'),
                    \DB::raw('date(ge.fecha_gestion) AS fecha_gestion'),
                    //\DB::raw('date(ge.fecha_fin) AS fecha_fin'),
//                    \DB::raw(
//                        'date(ge.fecha_teorica_final) AS fecha_teorica_final'
//                    ),
                    \DB::raw('date(ge.fecha_termino) AS fecha_termino'),
                    'ge.estado_general',
                    'ge.gestion_interna',
                    'ge.recepcion_expediente',
                    //'ge.energia_estado',
                    //'ge.fo_estado',
                    //'ge.troba_nueva',
                    //'ge.troba_estado',
                    'ge.created_at',
                    'ge.avance', 
                    \DB::raw('SUBSTRING(ge.observacion,1,30) AS observacion'),
                    'pt.nombre as proyecto_tipo', 
                    \DB::raw(
                        'SUBSTRING(LOWER(pt.nombre),1,2) AS proyecto_tipo_alias'
                    ), 
                    'c.nombre as casuistica', 'pe.nombre AS proyecto_estado',
                    \DB::raw(
                        'CONCAT_WS(", ",us.apellido,us.nombre) AS nusuario'
                    ), 
                    'usuario'
                )
                ->where('ge.proyecto_edificio_id', $proyectoEdificioId)
                ->orderby('ge.created_at', 'desc')
                ->get(); /* 'ge.*',  */
        return $result;
    }

    public function getUltimoGestionesEdificios($proyectoEdificioId)
    {
        $result = \DB::table('gestiones_edificios as ge')
            ->select(
                'id', 'fecha_inicio',
                \DB::raw('proyecto_estado_id AS proyectoestadoid'),
                \DB::raw('casuistica_id AS casuisticaid'),
                'fecha_compromiso', 'fecha_termino', 'fecha_gestion', 
                'created_at', 'proyecto_tipo_id', 'retorna', 'avance' 
            )
            ->where('ge.proyecto_edificio_id', $proyectoEdificioId)
            ->orderby('id', 'desc')
            ->first();
        return $result;
    }
    
    public function getUltimoGestionesEdificiosRelacion($proyectoEdificioId)
    {
        $result = \DB::table('gestiones_edificios as ge')
            ->select(
                \DB::raw('ge.proyecto_estado_id AS proyectoestadoid'),
                \DB::raw('ge.casuistica_id AS casuisticaid'),
                \DB::raw('ge.proyecto_tipo_id AS proyectotipoid'),
                \DB::raw('pe.proyecto_estado AS proyectoestado'),
                \DB::raw('pe.proyecto_tipo AS proyectotipo'),
                \DB::raw('date(ge.fecha_inicio) AS fecha_inicio'),
                \DB::raw('date(ge.fecha_seguimiento) AS fecha_seguimiento'),
                \DB::raw('date(ge.fecha_compromiso) AS fecha_compromiso'),
                \DB::raw('date(ge.fecha_gestion) AS fecha_gestion'),
                //\DB::raw('date(ge.fecha_fin) AS fecha_fin'),
//                \DB::raw(
//                    'date(ge.fecha_teorica_final) AS fecha_teorica_final'
//                ),
                \DB::raw('date(ge.fecha_termino) AS fecha_termino'),
                'ge.id', 'ge.estado_general', 'ge.gestion_interna', 
                'ge.recepcion_expediente', 'ge.proyecto_edificio_id', 
                'ge.proyecto_estado_id', 'ge.casuistica_id', 
                'ge.proyecto_tipo_id', 'ge.created_at', 
                'ge.usuario_created_at', 'ge.retorna', 'ge.avance'
            )
            ->join(
                'proyecto_edificios as pe', 
                'pe.id', '=', 'ge.proyecto_edificio_id'
            )
            ->where('ge.proyecto_edificio_id', $proyectoEdificioId)
            ->orderby('ge.created_at', 'desc')
            ->first();
        return $result;
    }
    
    public function getUltimosGestionesEdificiosRelacion($proyectoEdificioId)
    {
        $result = \DB::table('gestiones_edificios as t1')
            ->select(
                'pres.nombre' , 't1.id', 't1.proyecto_edificio_id',
                't1.proyecto_estado_id', 't1.casuistica_id', 
                't1.proyecto_tipo_id',
                \DB::raw(
                    'DATE(fecha_inicio) AS fecha_inicio, '
                    . 'DATE(fecha_seguimiento) AS fecha_seguimiento, '
                    . 'DATE(fecha_compromiso) AS fecha_compromiso, '
                    . 'DATE(fecha_termino) AS fecha_termino, '
                    . 'DATE(fecha_gestion) AS fecha_gestion'
                ), 
                'estado_general', 
                'gestion_interna', 'recepcion_expediente',
//                \DB::raw(
//                    'DATE(fecha_teorica_final) AS fecha_teorica_final'
//                ),
                'avance', 'retorna'
            )
            ->join(
                'proyectos_estados as pres', 
                'pres.id', '=', 't1.proyecto_estado_id'
            )
            ->whereNotExists(
                function($query) {
                    $query->select(\DB::raw(1))
                        ->from('gestiones_edificios as t2')
                        ->whereRaw(
                            't2.proyecto_edificio_id = t1.proyecto_edificio_id'
                        )
                        ->whereRaw(
                            't2.proyecto_estado_id = t1.proyecto_estado_id '
                        )
                        ->whereRaw(
                            '(
                                t2.created_at > t1.created_at OR
                                t2.created_at = t1.created_at AND 
                                t2.id < t1.id
                            )'
                        );
                }
            )
            ->where('t1.proyecto_edificio_id', '=', $proyectoEdificioId)
            ->orderby('t1.proyecto_edificio_id')
            ->get();
        //return $result;
        $coleccion = \Illuminate\Support\Collection::make($result);
        return $coleccion->keyBy('proyecto_tipo_id');
    }
    
}
