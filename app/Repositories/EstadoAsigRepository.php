<?php namespace Repositories;
use EstadoAsignacion;
use Legados\models\SolicitudTecnicaUltimo;
use Illuminate\Support\Collection;

class EstadoAsigRepository implements EstadoAsigRepositoryInterface
{
    public function listar(){
        $query =  EstadoAsignacion::from('estados_asignacion as ea')
                ->select(
                    'ea.*',
                    'eo.nombre as estado_ofsc',
                    'ease.nombre as estado_st'
                )
                ->leftjoin('estados_ofsc as eo', 'ea.estado_ofsc_id', '=', 'eo.id')
                ->leftjoin('estados_aseguramiento as ease', 'ea.estado_aseguramiento_id', '=', 'ease.id');

         if( \Input::has('estado_asignacion_id')){
            $query->where('ea.id', \Input::get('estado_asignacion_id'));
            return $query->get(); 
         }else{
            $query->orderBy( \Input::get('column'), \Input::get('dir'));         
         }

        $perPage = \Input::has('per_page') ? (int)\Input::get('per_page') : null;
        $result = $query->paginate($perPage);       
        $data = $result->toArray();   
        $col = new Collection([
            'recordsTotal'=>$result->getTotal(),
            'recordsFiltered'=>$result->getTotal()
        ]);
        return $col->merge($data);  
    }

    public function update(){     
        $inputs = \Input::all();             
        if(\Input::has('estado_asignacion_id')){
            $estadoAsignacion = EstadoAsignacion::findOrFail(\Input::get('estado_asignacion_id'));        
            foreach (json_decode(json_encode($estadoAsignacion),true) as $key => $value) {
                if (array_key_exists($key, $inputs)) {
                    $estadoAsignacion->$key = $inputs[$key];
                }
            }
            $estadoAsignacion->save();
        }else{
            $estadoAsignacion = new EstadoAsignacion();
            $estadoAsignacion->fill($inputs);
            $estadoAsignacion->save();
        }

        if($estadoAsignacion->estado == 0){
            SolicitudTecnicaUltimo::where('usuario_gestionando', '<>', 0)
                ->where('estado_ofsc_id', '<>', $estadoAsignacion->estado_ofsc_id)
                ->where('estado_aseguramiento', '<>', $estadoAsignacion->estado_aseguramiento_id)
                ->update(['usuario_gestionando' => 0]);
        }
        return ($estadoAsignacion) ? 1 : 0;
    }

    public function validate(){        
        $validator = Validator::make(Input::all(), EstadoAsignacion::$rules);
        return $validator;
    }
}
