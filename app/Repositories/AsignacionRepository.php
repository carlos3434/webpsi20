<?php namespace Repositories;
use Usuario;
use UsuarioCantSolicitudes;
use UsuarioSolicitudTecnica;
use UsuarioSolicitudTecnicaMovimiento;
use Illuminate\Support\Collection;

class AsignacionRepository implements AsignacionRepositoryInterface
{
    public function usuariosHabilitados(){
        $usuarios = Usuario::from('usuarios as u')
            ->leftjoin('usuario_cant_solicitudes as ucs', 'ucs.usuario_id', '=', 'u.id')
            ->select(
                'u.*',
                'ucs.usuario_id',
                'ucs.estado_asignacion'
            )
            ->where(function($query){
                $query->where(['perfil_id' => 13, 'estado' => 1]);
            });

            if(\Input::get('accion')){
               return $usuarios->get();
            }
            if (\Input::has('column')) {
                $usuarios->orderBy(\Input::get('column'), \Input::get('dir'));
            } else {                
                return $usuarios->get();
            }

        $perPage = \Input::has('per_page') ? (int)\Input::get('per_page') : null;
        $result = $usuarios->paginate($perPage);       
        $data = $result->toArray();   
        $col = new Collection([
            'recordsTotal'=>$result->getTotal(),
            'recordsFiltered'=>$result->getTotal()
        ]);
        return $col->merge($data);  
    }

    public function changeState(){
        $user_id = \Input::get('user_id');
        $estado = \Input::get('estado');

        $user_bolsa = UsuarioCantSolicitudes::where('usuario_id', '=', $user_id)->first();
        if($user_bolsa){        
            $user_bolsa->estado_asignacion = $estado;
            $user_bolsa->save();
        }
        else{   
            $user_bolsa  = new UsuarioCantSolicitudes;
            $user_bolsa->usuario_id = $user_id;
            $user_bolsa->estado_asignacion = $estado;
            $user_bolsa->save();
        }
        return ($user_bolsa) ? 1 : 0;
    }

    public function solicitudestoAsig(){
        $solicitudes = \DB::table('solicitud_tecnica as st')
                    ->join('solicitud_tecnica_ultimo as stu', 'stu.solicitud_tecnica_id', '=', 'st.id')
                    ->join(
                        'estados_asignacion as ea',function ($join) {
                             $join->on('ea.estado_ofsc_id', '=', 'stu.estado_ofsc_id')
                             ->on('ea.estado_aseguramiento_id', '=', 'stu.estado_aseguramiento')
                             ->where('ea.estado','=',1);
                    })
                    //->where('stu.usuario_gestionando',0)
                    ->get();
        return $solicitudes;
    }

    public function usuariostoAsig(){
        $usuarios_devolucion = \DB::table('usuarios as u')
            ->select(
                'u.id',
                    \DB::raw('(SELECT COUNT(*) FROM usuario_solicitud_tecnica where usuario_id=u.id) as cantidad '
                )
            )
            ->where('u.perfil_id', '=', 13)
            ->where('u.estado', '=', 1)
            ->orderBy('cantidad','asc')
            ->get();
        return $usuarios_devolucion;
    }

    public function asignar(){
        $solicitudes = $this->solicitudestoAsig();
        $usuarios_devolucion =$this->usuariostoAsig();
        
        $asig = 0;
        $cant_usuarios = count($usuarios_devolucion);      
        if($usuarios_devolucion){   
            $usuarios = json_decode(json_encode($usuarios_devolucion),true);
            while(list($index, $usuario) = each($usuarios) ){
                foreach ($solicitudes as $key => $solicitud) {                 
                        $solicitud_usuario = UsuarioSolicitudTecnica::where('solicitud_tecnica_id', '=', $solicitud->id )->first();
                        if ( count($solicitud_usuario) == 1 ) {
                            $solicitud_usuario->usuario_id = $usuario['id'];
                            $solicitud_usuario->save();
                        } else {
                            $usuario_ultimo = new UsuarioSolicitudTecnica;
                            $usuario_ultimo->solicitud_tecnica_id = $solicitud->id;
                            $usuario_ultimo->usuario_id = $usuario['id'];
                            $usuario_ultimo->save();
                        }                          
                        $usuario_movimiento_solicitud = new UsuarioSolicitudTecnicaMovimiento();
                        $usuario_movimiento_solicitud->solicitud_tecnica_id = $solicitud->id;
                        $usuario_movimiento_solicitud->usuario_id = $usuario['id'];
                        $usuario_movimiento_solicitud->save();
                        $asig+=1;
                    unset($solicitudes[$key]);
                    break;
                }

                if ( ($index+1 == $cant_usuarios)  && count($solicitudes) > 0 ) {
                    reset($usuarios);
                }
            }
        }
        return array('asignados'=>$asig,'restante'=>count($solicitudes));
    }
}
