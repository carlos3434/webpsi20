<?php

namespace Repositories;

/**
 * The UserRepositoryInterface contains ONLY method signatures for methods 
 * related to the User object.
 *
 * Note that we extend from RepositoryInterface, so any class that implements 
 * this interface must also provide all the standard eloquent methods 
 * (find, all, etc.).
 */
interface GestionEdificioRepositoryInterface extends RepositoryInterface
{

    public function findGestionEdificioByColumn($column);

    public function newGestionEdificio();

    public function findOrFail($id);

    public function getGestionEdificios();

    public function getTotalRecords();
}
