<?php namespace Repositories;

use Repositories\ListadoLegoRepositoryInterface;
use DB;
use Redis;
use Auth;
use Cache;
use Session;
use Legados\models\Tematicos;

class ListadoLegoRepository implements ListadoLegoRepositoryInterface
{
    public function motivosdevolucion($data = array())
    {
        $datos = Cache::get('listMotivosDevolucionLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listMotivosDevolucionLegado',
                24*60*60,
                function () {
                    return \MotivoOfsc::select(
                        "motivos_ofsc.*",
                        "motivos_ofsc.estado_ofsc as tipo"
                    )->where("estado", 1)
                    ->where("estado_ofsc_id", 4)
                    ->whereNotNull("codigo_legado")
                    ->whereNotNull("tipo_legado")
                    ->whereNotNull("estado_ofsc")
                    ->whereNotNull("actividad_id")
                    ->get();
                }
            );
        }
        return $datos;
    }
    public function submotivosdevolucion($data = array())
    {
        $datos = Cache::get('listSubMotivosDevolucionLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listSubMotivosDevolucionLegado',
                24*60*60,
                function () {
                    return \DB::table('submotivos_ofsc as s')
                    ->join("motivos_ofsc as m", 'm.id', '=', 's.motivo_ofsc_id')
                    ->select('s.*')
                    ->where("m.estado", 1)
                    ->where("s.estado", 1)
                    ->whereNotNull("s.motivo_ofsc_id")
                    ->whereNotNull("s.codigo_ofsc")
                    ->get();
                }
            );
        }
        return $datos;
    }
    public function tematicos($data = array())
    {
        $datos = Cache::get('listTematicosLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listTematicosLegado',
                24*60*60,
                function () {
                    $data = Tematicos::where("estado", 1)
                    ->whereRaw("deleted_at IS NULL")
                    ->get();
                    $tematicos = [];
                    foreach ($data as $key => $value) {
                        $tematicos[$value->parent][] = $value;
                    }
                    return $tematicos;
                }
            );
        }
        return $datos;
    }
    public function operacion($data = array())
    {
        // dd($data);
        $datos = Cache::get('listOperacionLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listOperacionLegado',
                24*60*60,
                function () {
                    return DB::table('tipo_operacion')
                            ->select(
                                'codigo as id',
                                'nombre'
                            )->get();
                }
            );
        }
        return $datos;
    }

    public function zonal($data = array())
    {
        $datos = Cache::get('listZonalLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listZonalLegado',
                24*60*60,
                function () {
                    return DB::table('coc.zonal')
                        ->select(
                            DB::RAW('distinct(zon) as id'),
                            'zon as nombre'
                        )->get();
                }
            );
        }
        return $datos;
    }

    public function zonalPremium($data = array())
    {
        $datos = Cache::get('listZonalPremium');
        //print($datos);
        //dd();
        if (!$datos) {
            $datos = Cache::remember('listZonalPremium', 24*60*60, function () {
                return DB::table('zona_premium_catv')
                        ->select(
                            'zona as id',
                            DB::raw(
                                'zona as nombre'
                            )
                        )
                        
                        ->orderBy('zona', 'asc')
                        ->groupBy('zona')
                        ->get();
            });
        }
        //dd($datos);
        return $datos;
    }

    public function nodoPremium($data = array())
    {
        $datos = Cache::get('listNodoPremium');
        if (!$datos) {
            $datos = Cache::remember('listNodoPremium', 24*60*60, function () {
                return DB::table('geo_nodopunto')
                        ->select(
                            'nodo as id',
                            DB::raw(
                                'nodo as nombre'
                            )
                        )
                        
                        ->orderBy('nodo', 'asc')
                        ->get();

            });
        }

        
        return $datos;
    }
    public function requerimiento($data = array())
    {
        $datos = Cache::get('listRequerimientoLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listRequerimientoLegado',
                24*60*60,
                function () {
                    return DB::table('webpsi_coc.prov_catv_tiposact')
                    ->select(
                        DB::RAW('distinct(tipo_req) as id'),
                        'tipo_req as nombre'
                    )
                    ->orderBy('tipo_req')
                    ->get();
                }
            );
        }
        return $datos;
    }

    public function motivo($data = array())
    {
        $datos = Cache::get('listMotivoLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listMotivoLegado',
                24*60*60,
                function () {
                    return DB::table('motivos AS m')
                        ->leftJoin(
                            'motivo_quiebre as mq',
                            'mq.motivo_id',
                            '=',
                            'm.id'
                        )
                        ->select(
                            'm.id',
                            'm.nombre',
                            DB::raw('GROUP_CONCAT(CONCAT("Q",mq.quiebre_id) SEPARATOR "|,|" ) as relation')
                        )
                        ->where('m.estado', '=', '1')
                        ->where('mq.estado', '=', '1')
                        ->groupBy('mq.motivo_id')
                        ->orderBy('m.nombre')
                        ->get();
                }
            );
        }
        return $datos;
    }

    public function segmento($data = array())
    {
        $datos = Cache::get('listSegmentoLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listSegmentoLegado',
                24*60*60,
                function () {
                    return DB::table('coc.segmento')
                        ->select(
                            DB::RAW('cod_segmento as id'),
                            'nom_segmento as nombre'
                        )->where(
                            'nom_segmento',
                            '<>',
                            ''
                        )->get();
                }
            );
        }
        return $datos;
    }

	public function empresa($data = array())
    {
        $datos = Cache::get('listEmpresaLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listEmpresaLegado',
                24*60*60,
                function () {
                    return DB::table('empresas as e')
                            ->join(
                                'empresa_usuario as eu',
                                function ($join) {
                                    $join->on('e.id', '=', 'eu.empresa_id')
                                    ->where('eu.usuario_id', '=', Auth::id())
                                    ->where('eu.estado', '=', '1');
                                }
                            )
                            ->select(
                                'e.id',
                                'e.nombre',
                                'e.es_ec',
                                DB::raw(
                                    'IFNULL(eu.estado,"disabled") as block'
                                )
                            )
                            ->where('e.estado', '=', '1')
                            ->orderBy('e.nombre')
                            ->get();
                }
            );
        }
        return $datos;
    }

    public function tecnico($data = array())
    {
        $datos = Cache::get('listTecnicoLegado');
        if (!$datos) {
            $datos = Cache::remember('listTecnicoLegado', 24*60*60, function(){
                return DB::table('tecnicos as t')
                        ->select(
                            't.carnet_tmp as id', 't.nombre_tecnico as nombre'
                        )
                        ->where('t.estado', '=', '1')
                        ->get();
            });
        }
        return $datos;
	}

	public function departamento($data = array())
	{
        $datos = Cache::get('listDepartamentoLegado');
        if (!$datos) {
            $datos = Cache::remember('listDepartamentoLegado', 24*60*60, function(){
                return DB::table('departamento')
                        ->select(
                            'departamento_id as id',
                            'nombre'
                        )
                        ->orderBy('nombre')
                        ->get();
            });
        }
        return $datos;
	}

	public function provincia($data = array())
	{
        $datos = Cache::get('listProvinciaLegado');
        if (!$datos) {
            $datos = Cache::remember('listProvinciaLegado', 24*60*60, function(){
                return DB::table('provincia')
                        ->select(
                            'provincia_id as id',
                            'nombre',
                            DB::RAW('CONCAT("D",departamento_id) AS relation')
                            )
                        ->orderBy('nombre')
                        ->get();
            });
        }
        return $datos;
	}

	public function distrito($data = array())
	{
        $datos = Cache::get('listDistritoLegado');
        if (!$datos) {
            $datos = Cache::remember('listDistritoLegado', 24*60*60, function(){
                return DB::table('distrito')
                        ->select(
                            'distrito_id as id',
                            'nombre',
                            DB::RAW('CONCAT("P",provincia_id) AS relation')
                            )
                        ->orderBy('nombre')
                        ->get();
            });
        }
        return $datos;
	}

	public function vip($data = array())
	{
        $datos = Cache::get('listVipLegado');
        if (!$datos) {
            $datos = Cache::remember('listVipLegado', 24*60*60, function(){
                return DB::table('vip')
                        ->select('id', 'nombre')
                        ->get();
            });
        }
        return $datos;
	}

	public function jefatura($data = array())
	{
        $datos = Cache::get('listJefaturaLegado');
        if (!$datos) {
            $datos = Cache::remember('listJefaturaLegado', 24*60*60, function(){
                return DB::table('coc.zonal')
                        ->select(
                            DB::RAW('distinct(jefatura) as id'),
                            'jefatura as nombre'
                            )
                        ->get();
            });
        }
        return $datos;
	}

	public function nodo($data = array())
	{
        $datos = Cache::get('listNodoLegado');
        if (!$datos) {
            $datos = Cache::remember('listNodoLegado', 24*60*60, function(){
                return DB::table('geo_nodopunto')
                        ->select(
                            'nodo as id',
                            DB::raw(
                                'CONCAT(nodo,": ",nombre,"->",zonal) as nombre'
                            )
                        )
                        ->where('tecnologia','COAXIAL')
                        ->orderBy('nodo', 'asc')
                        ->get();

            });
        }
        return $datos;
	}

	public function troba($data = array())
	{
        $datos = Cache::get('listTrobaLegado');
        if (!$datos) {
            $datos = Cache::remember('listTrobaLegado', 24*60*60, function(){
                return DB::table('geo_trobapunto')
                       ->select(
                           DB::RAW('troba as id'),
                           'troba as nombre',
                           DB::raw('GROUP_CONCAT(DISTINCT CONCAT("N",nodo) SEPARATOR "|,|") as relation')
                           )
                       ->orderBy('troba')
                       ->groupBy('troba')
                       ->get();
            });
        }
        return $datos;
	}

    public function mdf($data = array())
    {
        $datos = Cache::get('listMdfLegado');
        if (!$datos) {
            $datos = Cache::remember('listMdfLegado', 24*60*60, function(){
                return DB::table('webpsi_fftt.mdfs_eecc_regiones as m')
                          ->leftJoin('geo_mdfpunto as g', 'm.MDF', '=', 'g.mdf')
                          ->select(
                              'm.MDF AS nombre',
                              DB::raw(
                                  'CONCAT(
                                    IFNULL(m.MDF,""),"___",
                                    IFNULL(
                                      replace(
                                        m.EECC_CRITICO,"LARI PLAYAS","LARI"
                                      ), EECC
                                    ) 
                                    ,"   ",
                                    IFNULL(
                                      (
                                        SELECT id
                                        FROM empresas
                                        WHERE nombre IN (
                                          IFNULL(
                                            replace(
                                              m.EECC_CRITICO,"LARI PLAYAS","LARI"
                                            ), EECC
                                          ) 
                                        )
                                      )
                                      , ""
                                    )
                                    ,"___",
                                    IFNULL(m.LEJANO,""),"___",
                                    IFNULL(m.ZONA_CRITICO,"")
                                ) AS id'
                              ),
                              DB::raw("IFNULL(g.coord_x,'') AS coord_x"),
                              DB::raw("IFNULL(g.coord_y,'') AS coord_y")
                          )->orderBy('m.MDF', 'asc')
                          ->get();
            });
        }
        return $datos;
    }

	public function geoamplificador($data = array())
	{
        $datos = Cache::get('listGeoamplificadorLegado');
        if (!$datos) {
            $datos = Cache::remember('listGeoamplificadorLegado', 24*60*60, function(){
                return DB::table('geo_amplificador')
                        ->select('amplificador as id',
                          'amplificador as nombre',
                          DB::raw('GROUP_CONCAT(DISTINCT CONCAT("T",troba) SEPARATOR "|,|") as relation')
                          )
                        ->where('amplificador','<>','')
                        ->groupBy('amplificador')
                        ->orderBy('id', 'asc')
                        ->get();
            });
        }
        return $datos;
	}

	public function geotap($data = array())
	{
        $datos = Cache::get('listGeotapLegado');
        if (!$datos) {
            $datos = Cache::remember('listGeotapLegado', 24*60*60, function(){
                return DB::table('geo_tap')
                        ->select('tap as id',
                          'tap as nombre',
                          DB::raw('GROUP_CONCAT(DISTINCT CONCAT("A",amplificador) SEPARATOR "|,|") as relation')
                          )
                        ->where('tap','<>','')
                        ->groupBy('tap')
                        ->orderBy('id', 'asc')
                        ->get();
            });
        }
        return $datos;
	}

	public function quiebre($data = array())
	{
        $idUser = Auth::id();
        $key = "listQuiebreLegado{$idUser}";
        $datos = Cache::get($key);
        if (!$datos) {
            $datos = Cache::remember($key, 24*60*60, function(){
                return DB::table('quiebres as q')
                        ->join(
                            'quiebre_grupo_usuario as qgu',
                            function($join)
                            {
                                $join->on(
                                    'q.quiebre_grupo_id',
                                    '=',
                                    'qgu.quiebre_grupo_id'
                                )
                                ->where(
                                    'qgu.usuario_id',
                                    '=',
                                    Auth::id()
                                )
                                ->where(
                                    'qgu.estado',
                                    '=',
                                    '1'
                                );
                            }
                        )
                        ->select(
                            'q.id',
                            'q.nombre',
                            DB::raw(
                                'IFNULL(qgu.estado,"disabled") as block'
                            )
                        )
                        ->where('q.estado', '=', '1')
                        ->whereRaw(
                            'q.id NOT IN (
                                SELECT quiebre_id
                                FROM quiebre_usuario_restringido
                                WHERE usuario_id="'.Auth::id().'"
                                AND estado=1
                            )'
                        )
                        ->orderBy('q.nombre')
                        ->get();
            });
        }
        return $datos;
	}

	public function estadoofsc($data = array())
	{
        $datos = Cache::get('listEstadoofscLegado');
        if (!$datos) {
            $datos = Cache::remember('listEstadoofscLegado', 24*60*60, function(){
                return DB::table('estados_ofsc')
                        ->select('id', 'nombre')
                        ->get();
            });
        }
        return $datos;
	}

    public function motivosliquidacion($data = array())
    {
        $datos = Cache::get('listMotivosliquidacionLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listMotivosliquidacionLegado',
                24*60*60,
                function () {
                    return DB::table('motivos_liquidacion')
                        ->select('codigo_liquidacion as id', 'descripcion_liquidacion as nombre')
                        ->get();
                }
            );
        }
        return $datos;
    }

    public function contratas($data = array())
    {
        $datos = Cache::get('listContratasLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listContratasLegado',
                24*60*60,
                function () {
                    return DB::table('contratas_cms')
                        ->select('codctr as id', 'desnomctr as nombre')
                        ->get();
                }
            );
        }
        return $datos;
    }

    public function contratascms($data = array())
    {
        $datos = Cache::get('listContratasLegadoCms');
        if (!$datos) {
            $datos = Cache::remember(
                'listContratasLegadoCms',
                24*60*60,
                function () {
                    return DB::table('empresas')
                        ->select('cms as id', 'nombre as nombre')
                        ->whereRaw("cms IS NOT NULL")
                        ->get();
                }
            );
        }
        return $datos;
    }

    public function contratasgestel($data = array())
    {
        $datos = Cache::get('listContratasLegadoGestel');
        if (!$datos) {
            $datos = Cache::remember(
                'listContratasLegadoGestel',
                24*60*60,
                function () {
                    return DB::table('empresas')
                        ->select('gestel as id', 'nombre as nombre')
                        ->whereRaw("gestel IS NOT NULL")
                        ->get();
                }
            );
        }
        return $datos;
    }
    public function areas($data = array())
    {
        $datos = Cache::get('listAreasLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listAreasLegado',
                24*60*60,
                function () {
                    return DB::table('areas')
                    ->select('id', 'nombre')
                    ->get();
                }
            );
        }
        return $datos;
    }

    public function estadoaseguramiento($data = array())
    {
        $datos = Cache::get('listEstadosAseguramiento');
        if (!$datos) {
            $datos = Cache::remember(
                'listEstadosAseguramiento',
                24*60*60,
                function () {
                    return DB::table('estados_aseguramiento')
                    ->select('id', 'nombre')
                    ->get();
                }
            );
        }
        return $datos;
    }

    public function solicitudes($data = array())
    {
        $solicitudes = Redis::lrange('solicitudes', 0, 900);
        $datos=[];
        foreach ($solicitudes as $key => $value) {
            //if (Redis::get($value)) {
                //$datos[]= ['id'=>$value,'nombre'=>$value];
            $datos[] = $value;
            //}
        }
        return $datos;
    }

    public function actividad($data = array())
    {
        $datos = Cache::get('listActividadLegado');
        if (!$datos) {
            $datos = Cache::remember('listActividadLegado', 24*60*60, function ()
            {
                return DB::table('actividades')
                        ->select(
                            'id',
                            'nombre'
                        )->get();
            });
        }
        return $datos;
    }
    public function actividadtipo($data = array())
    {
        $datos = Cache::get('listActividadTipoLegado');
        if (!$datos) {
            $datos = Cache::remember('listActividadTipoLegado', 24*60*60, function ()
            {
                return DB::table('actividades_tipos')
                        ->select(
                            'id',
                            "nombre"
                        )->where('estado', 1)
                        ->get();
            });
        }
        return $datos;
    }

    //select envio legado
    public function EnvioLegado_Host($data = array())
    {
        $datos = Cache::get('listEnvioLegado');
        //print($datos);
        //dd();
        if (!$datos) {
            $datos = Cache::remember('listEnvioLegado', 24*60*60, function(){
                return DB::table('envio_legado')
                        ->select(
                            'host as id',
                            DB::raw(
                                'host as nombre'
                            )
                        )
                        
                        ->orderBy('host', 'asc')
                        ->groupBy('host')
                        ->get();
            });
        }    
        //dd($datos);
        return $datos;
    }

    public function EnvioLegado_Accion($data = array())
    {
        $datos = Cache::get('listEnvioAccion');
        //print($datos);
        //dd();
        if (!$datos) {
            $datos = Cache::remember('listEnvioAccion', 24*60*60, function(){
                return DB::table('envio_legado')
                        ->select(
                            'accion as id',
                            DB::raw(
                                'accion as nombre'
                            )
                        )
                        
                        ->orderBy('accion', 'asc')
                        ->groupBy('accion')
                        ->get();
            });
        }    
        //dd($datos);
        return $datos;
    }

    public function perfil($data = array())
    {
        $perfilId = Session::get('perfilId');
        $number = $perfilId == 8 ? 8 : 0;
        $key = "listPerfil{$number}";
        $datos = Cache::get($key);
        if (!$datos) {
            $datos = Cache::remember(
                $key,
                24*60*60,
                function () {
                    if (Session::get('perfilId') == 8) {
                        return DB::table('perfiles')
                            ->select('id', 'nombre', 'estado')
                            ->get();
                    } else {
                        return DB::table('perfiles')
                            ->select('id', 'nombre', 'estado')
                            ->where('id', '<>', 8)
                            ->get();
                    }
                }
            );
        }
        return $datos;
    }

    public function modulo($data = array())
    {
        $perfilId = Session::get('perfilId');
        $number = "";
        if ($perfilId == 9) {
            $number = Auth::id();
        }
        $key = "listModulo{$number}";
        $datos = Cache::get($key);
        if (!$datos) {
            $datos = Cache::remember(
                $key,
                24*60*60,
                function () {
                    if (Session::get('perfilId') == 9) {
                        return DB::table('submodulo_usuario as su')
                             ->rightJoin('submodulos as s','su.submodulo_id', '=', 's.id')
                             ->rightJoin('modulos as m','s.modulo_id', '=', 'm.id')
                             ->select('m.id', 'm.nombre', 'm.path')
                             ->where('m.estado', '=' , '1')
                             ->where('su.usuario_id', '=', Auth::id())
                             ->groupBy('m.id')
                             ->get();
                    } else {
                        return DB::table('modulos')
                            ->select('id', 'nombre', 'path')
                            ->where('estado', '=', '1')
                            ->orderBy('nombre')
                            ->get();
                    }
                }
            );
        }
        return $datos;
    }

    public function tipopersona($data = array())
    {
        $datos = Cache::get('listTipoPersona');
        if (!$datos) {
            $datos = Cache::remember(
                'listTipoPersona',
                24*60*60,
                function () {
                    return DB::table('tipo_persona')
                            ->orderBy('nombre')
                            ->get();
                }
            );
        }
        return $datos;
    }

    public function motivogen($data = array())
    {
        $datos = Cache::get('listMotivogenLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listMotivogenLegado',
                24*60*60,
                function () {
                        
                        $first = DB::table('gtm_averias')
                                ->select(
                                    DB::RAW('distinct(motivo) as id'),
                                    DB::RAW('concat("AVE-",motivo) as nombre')
                                )
                                ->where('motivo','<>','""')
                                ->groupBy('motivo');

                        return DB::table('webpsi_coc.prov_catv_tiposact')
                                ->select(
                                    DB::RAW('distinct(motivo) as id'),
                                    DB::RAW('concat("PROV-",motivo) as nombre')
                                    )
                                ->groupBy('motivo')
                                ->orderBy('motivo')
                                ->union($first)
                                ->get();
                }
            );
        }
        return $datos;
    }

    public function tiporeq($data = array())
    {
        $datos = Cache::get('listTiporeqLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'listTiporeqLegado',
                24*60*60,
                function () {
                        
                        $first = DB::table('gtm_averias')
                                ->select(
                                    DB::RAW('distinct(tipo_req) as id'),
                                    DB::RAW('concat("AVE-",tipo_req) as nombre')
                                )
                                ->where('tipo_req','<>','')
                                ->groupBy('tipo_req');

                        return DB::table('webpsi_coc.prov_catv_tiposact')
                                ->select(
                                    DB::RAW('distinct(tipo_req) as id'),
                                    DB::RAW('concat("PROV-",tipo_req) as nombre')
                                    )
                                ->groupBy('tipo_req')
                                ->orderBy('tipo_req')
                                ->union($first)
                                ->get();
                }
            );
        }
        return $datos;
    }

    public function bucket($data = array())
    {
        $datos = Cache::get('bucketLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'bucketLegado',
                24*60*60,
                function () {
                    return DB::table('bucket')
                            ->where('estado', '=', 1)
                            ->get(['id', 'nombre']);
                }
            );
        }
        return $datos;
    }

    public function bucketRecursos($data = array())
    {
        $datos = Cache::get('bucketRecursos');
        if (!$datos) {
            $datos = Cache::remember(
                'listTiporeqLegado',
                24*60*60,
                function () {
                    return DB::table('bucket')
                        ->select(
                            'bucket_ofsc as id',                            
                            'bucket_ofsc as nombre'                            
                        )
                        
                        ->orderBy('bucket_ofsc', 'asc')
                        ->groupBy('bucket_ofsc')
                        ->get();
                }
            );
        }
        return $datos;
    }

    public function masiva($data = array())
    {
        $datos = Cache::get('parametroLegado');
        if (!$datos) {
            $datos = Cache::remember(
                'parametroLegado',
                24*60*60,
                function () {
                    return DB::table('parametro')
                            ->where('estado', '=', 1)
                            ->where("tipo", "=", 4)
                            ->get(['id', 'nombre']);
                }
            );
        }
        return $datos;
    }

    public function geocerca($data = array())
    {
        $datos = Cache::get('geocerca');
        if (!$datos) {
            $datos = Cache::remember(
                'geocerca',
                24*60*60,
                function () {
                    return DB::table('geo_proyecto_capa_elemento')
                            ->select(
                                DB::RAW('geo_proyecto_capa_elemento.detalle as id'), 
                                DB::RAW('geo_proyecto_capa_elemento.detalle as nombre')
                                )
                            ->join('geo_proyecto_capa', 'geo_proyecto_capa_elemento.capa_id', 
                                '=', 'geo_proyecto_capa.id')
                            ->join('geo_proyecto', 'geo_proyecto_capa.proyecto_id', 
                                '=', 'geo_proyecto.id')
                            ->where('figura_id', '=', 4)
                            ->where("geo_proyecto.estado", "=", 1)
                            ->where("geo_proyecto_capa.estado", "=", 1)
                            ->get();
                }
            );
        }
        return $datos;
    }
}