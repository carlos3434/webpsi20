<?php

namespace Repositories;

/**
 * The UserRepositoryInterface contains ONLY method signatures for methods 
 * related to the User object.
 *
 * Note that we extend from RepositoryInterface, so any class that implements 
 * this interface must also provide all the standard eloquent methods 
 * (find, all, etc.)
 */
interface ProyectoEdificioRepositoryInterface extends RepositoryInterface
{

    public function findProyectoEdificioByColumn($column);

    public function getRows($datos, $column, $dir, $start, $length);

    public function getProyectoEdificios($column, $dir, $start, $length);

    public function getTotalRecordsFechas($fechas);

    public function getTotalRecords();
}
