<?php

namespace Repositories;

use Abstracts\Repository as AbstractRepository;
use Illuminate\Support\Facades\DB;

class ProyectoEdificioRepository 
    extends AbstractRepository 
    implements ProyectoEdificioRepositoryInterface
{

    // This is where the "magic" comes from:
    protected $modelClassName = 'ProyectoEdificio';

    // This class only implements methods specific to the UserRepository
    public function findProyectoEdificioByColumn($column)
    {
        $where = call_user_func_array(
            "{$this->modelClassName}::where", 
            array($column)
        );
        return $where->get();
    }

    public function newProyectoEdificio()
    {
        return new \ProyectoEdificio();
    }

    public function findOrFail($id)
    {
        return \ProyectoEdificio::findOrFail($id);
    }
    
    /**
     * 
     * @param type $datos
     * @param type $column
     * @param type $dir
     * @param type $start
     * @param type $length
     * @return type
     * @deprecated since version number
     */
    public function getRows($datos, $column, $dir, $start, $length)
    {
        $result['datos'] = \ProyectoEdificio::orderBy($column, $dir)
                        ->limit($length)->offset($start);
        $result['total'] = \ProyectoEdificio::where(
            function($query) {
                    
            }
        );

        if (isset($datos['fecha_created_at']) 
            && $datos['fecha_created_at'] != '') {
            $fechas = explode(' - ', $datos['fecha_created_at']);
            $result['datos'] = $result['datos']
                ->whereRaw(
                    'DATE(created_at) BETWEEN "' . $fechas[0] . '" 
                        AND "' . $fechas[1] . '"'
                );

            $result['total'] = $result['total']
                ->whereRaw(
                    'DATE(created_at) BETWEEN "' . $fechas[0] . '" 
                        AND "' . $fechas[1] . '"'
                );
        }

        if (isset($datos['segmento'])) {
            $result['datos'] = $result['datos']
                ->whereIn('segmento', $datos['segmento']);
            $result['total'] = $result['total']
                ->whereIn('segmento', $datos['segmento']);
        }

        if (isset($datos['tipoproyecto'])) {
            $result['datos'] = $result['datos']
                ->whereIn('tipo_proyecto', $datos['tipoproyecto']);
            $result['total'] = $result['total']
                ->whereIn('tipo_proyecto', $datos['tipoproyecto']);
        }

        if (isset($datos['tipo'])) {
            if ($datos['tipo'] == 'pe.direccion') {
                $result['datos'] = $result['datos']
                    ->whereRaw(
                        $datos['tipo'] . ' LIKE "' . $datos['buscar'] . '%"'
                    );
                $result['total'] = $result['total']
                    ->whereRaw(
                        $datos['tipo'] . ' LIKE "' . $datos['buscar'] . '%"'
                    );
            } else {
                $result['datos'] = $result['datos']
                    ->where($datos['tipo'], $datos['buscar']);
                $result['total'] = $result['total']
                    ->where($datos['tipo'], $datos['buscar']);
            }
        }

        $result['datos'] = $result['datos']->get();
        $result['total'] = $result['total']->count();

        return $result;
    }

    public function getProyectoEdificios($column, $dir, $start, $length)
    {
        $result = \ProyectoEdificio::orderBy($column, $dir)
            ->limit($length)->offset($start)->get();

        return $result;
    }

    public function getTotalRecordsFechas($fechas)
    {
        $fechas = explode(' - ', $fechas);
        $result = \ProyectoEdificio::whereBetween('created_at', $fechas)
            ->count();
        return $result;
    }

    public function getTotalRecords()
    {
        $result = \ProyectoEdificio::count();
        return $result;
    }

    public function getLastId()
    {
        return \ProyectoEdificio::select(
            DB::raw('IFNULL((CAST(id AS SIGNED) + 1), 1) id')
        )
        ->orderby(DB::raw('CAST(id AS SIGNED)'), 'desc')
        ->take(1)
        ->pluck('id');
    }
    
    public function existsId($id)
    {
        return \ProyectoEdificio::select('id')->where('id', '=', $id)->count();
    }
    
    /**
     * 
     * @param string $raw Where conditional, ex: "CONCAT(parent,subitem) = '$subitem'"
     * @return object
     */
    public function getFieldId($raw)
    {
        return \ProyectoEdificio::whereRaw($raw)->count();
    }
    
    public function deleteProyectoEdificio($id)
    {
        return \ProyectoEdificio::where('id', '=', $id)->delete();
    }
    
    public function deleteProyectoEdificioImagen($id, $field)
    {
        $n = ($field == 'foto_uno') ? '1': ($field == 'foto_dos'? '2' : '3');
        $filename = "{$id}_{$n}.jpg";
        $path = "img/edificio/{$filename}";
        $rst = 2;
        $aPath = strpos(public_path($path), '\\') === false?  
            public_path($path) : str_replace('\\', '/', public_path($path));
        
        $rpta = \ProyectoEdificio::where('id', '=', $id)
            ->update(array("$field" => ''));
        
        if (!\File::exists($aPath)) {
            $msj = ("Imagen no existe. ");
        } elseif ($rpta && unlink($aPath)) {
            $msj = ("Eliminado: $filename. ");
            $rst = 1;
        } else {
            $msj =  ("Imagen existe, Error al eliminar $filename. ");
        }

       return array(
             'rst'=> $rst
           , 'datos'=> $rpta
           , 'msj' => $msj
        );
    }
    
    public function deleteSoftProyectoEdificio($id)
    {
        $date = \Carbon\Carbon::now();
        return \ProyectoEdificio::where('id', '=', $id)->update(
            [
                'deleted_at' => $date, 
                'usuario_deleted_at' => \Auth::id()
            ]
        );
    }
    
    public function getRowsSoftDelete()
    {
        return \ProyectoEdificio::select(
            array(
                'id', DB::raw('CAST(id AS UNSIGNED) AS ida') 
                , 'segmento'        
                , 'tipo_proyecto'   
                , 'distrito'
                , DB::raw(
                    'TRIM(TRAILING ", " FROM CONCAT_WS'
                    . '(" ",direccion,numero,manzana_tdp,lote)) AS direccion'
                )
                , 'coord_x', 'coord_y'
                , 'numero_departamentos'
                , 'proyecto_estado'
                , 'nombre_proyecto'
                , 'avance'
                , 'fecha_termino'
                , 'id AS action'
            )
        )->whereRaw('deleted_at IS NOT NULL')->get();
    }
    /**
     * Hallar el conjunto de edificios que se encuentran alrededor de un punto
     * de coordenadas, 
     * @param double $x
     * @param double $y
     * @param integer $cant distancia desde el pto. De origen
     * @return object
     */
    public function getEdificioDistance( $x, $y, $cant )
    {
        $select = 
            DB::raw(
                "id, coord_y, coord_x, fecha_termino, "
                . "CONCAT("
                . "id,' ', "
                . "IFNULL(IF(nombre_proyecto='0','',nombre_proyecto),'') "
                . ") AS nombre,"
                . "CONCAT_WS(', ',direccion,numero,manzana_tdp,lote) "
                . "AS direccion, "
                . "IF(proyecto_estado is NULL,'building',"
                . "SUBSTRING_INDEX(LOWER(proyecto_estado), ' ', 1)) "
                . "AS estado_alias, "
                . "ROUND( 
                    3959000 
                    * 
                    ACOS( 
                        COS( RADIANS(". (float)$y .") )  
                        *
                        COS( RADIANS(coord_y) ) 
                        *
                        COS( RADIANS(coord_x) - RADIANS(". (float)$x .") ) 
                        +
                        SIN( RADIANS(". (float)$y .") ) * SIN( RADIANS(coord_y))
                    )
                ) AS distance,"
                . "proyecto_estado AS estado_edificio"
            );
        $result  = \ProyectoEdificio::select($select)
            ->having('distance', '<=', $cant)
            ->orderBy('distance')
            ->get(); /* ->limit($cant)->offset(0) */
        
        return $result;
        /* HAVING distance < '%s' */
        // return DB::select($sql, array((float)$y,(float)$x,(float)$y, $cant));
    }

    public function getTotalRecordsByProyectoEstado(
        $estado = null, $segmento = null
    )
    {
            return DB::table('proyecto_edificios')
                ->select(
                    DB::raw(
                        'COUNT(proyecto_estado) AS total, 
                        proyecto_estado AS estado,
                        CONCAT(proyecto_estado," - ",upper(segmento)) 
                        AS proyecto_estado,
                        SUBSTRING(
                        LOWER(SUBSTRING_INDEX(proyecto_estado, " ", -1)),1,2
                        ) 
                        AS alias'
                    )
                )
                ->where(
                    function ($q) use ($estado, $segmento) {
                        if (!is_null($estado)) {
                            $q->where('proyecto_estado', $estado);
                        }
                        if (!is_null($segmento)) {
                            $q->where('segmento', $segmento);
                        }
                    }
                )
                ->whereRaw('proyecto_estado IS NOT NULL')
                ->groupBy('proyecto_estado')
                ->get();      
    }
    
    /** 
        Listar los provisionales que estan asignados al proyecto
    */
    public function getProvisionales($idProyecto, $estadoProyecto, $parent = 0)
    {
        $result['datos'] = DB::table('provisional_proyecto_edificio')
                            ->select(
                                'id', 'ejecutor_proyecto', 
                                'estado_proyecto', 'estado_provisional',
                                'fecentrega', 'fecrespuesta', 'fectermino', 
                                'fecliquidacion'
                            )
                            ->where('idproyectoedificio', '=', $idProyecto)
                            ->where('estado_proyecto', '=', $estadoProyecto)
                            ->where('parent', '=', $parent)
                            ->orderBy('updated_at', 'DESC')
                            ->get();
        return $result;
    }
    /** 
     * Listar las gestiones megaproyectos que estan asignadas al proyecto
     */
    public function getMegaproyectoGestiones ($idProyecto, $parent = 0)
    {
       $result['datos'] = DB::table('gestiones_edificios_megaproyecto')
                            ->where('proyecto_edificio_id', '=', $idProyecto)
                            ->where('parent', '=', $parent)
                            ->orderBy('updated_at', 'DESC')
                            ->get();
        return $result; 
    }
    
    public function getProyectoTipos () 
    {
        $proyectoTipos = DB::table('proyecto_tipo')
                            ->select('id', 'nombre', 'estado')
                            ->orderBy('id')
                            ->get();
        
        return array(
            'rst'=>1,
            'datos'=>$proyectoTipos
        );
    }
    
    public function getProvisionalAcceso ($idUsuario)
    {
       $row = DB::table('proyecto_tipo as pt')
                ->rightJoin(
                    'proyecto_tipo_usuario as ptu', 
                    'pt.id', '=', 'ptu.proyecto_tipo'
                )
                ->select('pt.id', 'pt.nombre')
                ->where('pt.estado', '1')
                ->where('ptu.usuarios_id', '=', $idUsuario)
                ->where('pt.id', '=', 8)
                ->orderBy('orden')
                ->get();
       
       return array(
                   'rst'=>1,
                   'datos'=>$row
            );
    }
    
    public function getMegaAcceso ($idUsuario)
    {
       $row = DB::table('proyecto_tipo as pt')
                ->rightJoin(
                    'proyecto_tipo_usuario as ptu', 
                    'pt.id', '=', 'ptu.proyecto_tipo'
                )
                ->select('pt.id', 'pt.nombre')
                ->where('pt.estado', '1')
                ->where('ptu.usuarios_id', '=', $idUsuario)
                ->where('pt.id', '=', 5)
                ->orderBy('orden')
                ->get();
       
       return array(
                   'rst'=>1,
                   'datos'=>$row
            );
    }
    
}
