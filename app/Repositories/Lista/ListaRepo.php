<?php
namespace Repositories\Lista;
use Repositories\Base\BaseRepo;
use Repositories\Lista\ListaRepoInterface;
use DB;
use Auth;
use Cache;
use Redis;

class ListaRepo extends BaseRepo implements ListaRepoInterface
{
    public function getModel()
    {
        return new Lista;
    }
    public $filters = ['nombre', 'estado'];
    public function filterBySearch($q, $value)
    {
        $q->where('nombre', 'LIKE', "%$value%");
    }

    public function actividad()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $actividades = Cache::get('listarActividadBandeja');
            if ( $actividades ) {
                $actividades = Cache::get('listarActividadBandeja');
            } else {
                $actividades = Cache::remember('listarActividadBandeja', 20, function(){
                    $datos = DB::table('actividades')
                                ->select('id', 'nombre')
                                ->where('estado', '=', '1')
                                ->orderBy('nombre')
                                ->get();

                    return $datos;
                });
            }
        } else {
            $actividades = DB::table('actividades')
                            ->select('id', 'nombre')
                            ->where('estado', '=', '1')
                            ->orderBy('nombre')
                            ->get();
        }

        return json_encode(
            array(
                'rst'=>1,
                'datos'=>$actividades
            )
        );
    }
    public function estadoofsc()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarEstadosOfscBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarEstadosOfscBandeja');
            } else {
                $datos = Cache::remember('listarEstadosOfscBandeja', 20, function(){
                    $data = DB::table('estados_ofsc')
                                ->select('id', 'nombre')
                                ->get();

                    return $data;
                });
            }
        } else {
            $datos = DB::table('estados_ofsc')
                    ->select(
                        'id',
                        'nombre'
                    )
                    ->get();
        }

        return json_encode(
            array(
                'rst'=>1,
                'datos'=>$datos
            )
        );
    }
    public function estado()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarEstadoBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarEstadoBandeja');
            } else {
                $datos = Cache::remember('listarEstadoBandeja', 20, function(){
                    $data =  DB::table('estados AS e')
                        ->join(
                            'estado_motivo_submotivo AS ems',
                            'ems.estado_id', '=', 'e.id'
                        )
                        ->select(
                            'e.id',
                            'e.nombre',
                            DB::raw(
                                'CONCAT(
                                    GROUP_CONCAT(
                                        DISTINCT(CONCAT("M",ems.motivo_id))
                                            SEPARATOR "|,|"
                                    ),"|,|",
                                    GROUP_CONCAT(
                                        DISTINCT(CONCAT("S",ems.submotivo_id))
                                            SEPARATOR "|,|"
                                    )
                                ) AS relation'
                            ),
                            DB::raw(
                                'GROUP_CONCAT(
                                    DISTINCT(CONCAT("M",ems.motivo_id,
                                                    "S",ems.submotivo_id,
                                                    "-",ems.req_tecnico,
                                                    "-",ems.req_horario
                                                    )
                                            )
                                        SEPARATOR "|,|"
                                ) AS evento'
                            )
                        )
                        ->where('e.estado', '=', '1')
                        ->where('ems.estado', '=', '1')
                        ->groupBy('ems.estado_id')
                        ->orderBy('e.nombre')
                        ->get();

                    return $data;
                });
            }
        } else {
            $datos =  DB::table('estados AS e')
                ->join(
                    'estado_motivo_submotivo AS ems',
                    'ems.estado_id', '=', 'e.id'
                )
                ->select(
                    'e.id',
                    'e.nombre',
                    DB::raw(
                        'CONCAT(
                            GROUP_CONCAT(
                                DISTINCT(CONCAT("M",ems.motivo_id))
                                    SEPARATOR "|,|"
                            ),"|,|",
                            GROUP_CONCAT(
                                DISTINCT(CONCAT("S",ems.submotivo_id))
                                    SEPARATOR "|,|"
                            )
                        ) AS relation'
                    ),
                    DB::raw(
                        'GROUP_CONCAT(
                            DISTINCT(CONCAT("M",ems.motivo_id,
                                            "S",ems.submotivo_id,
                                            "-",ems.req_tecnico,
                                            "-",ems.req_horario
                                            )
                                    )
                                SEPARATOR "|,|"
                        ) AS evento'
                    )
                )
                ->where('e.estado', '=', '1')
                ->where('ems.estado', '=', '1')
                ->groupBy('ems.estado_id')
                ->orderBy('e.nombre')
                ->get();
        }

        return json_encode(
            array(
                'rst'=>1,
                'datos'=>$datos
            )
        );
    }
    public function quiebre()
    {
        $quiebres=  DB::table('quiebres as q')
                    ->join(
                        'quiebre_grupo_usuario as qgu',
                        function($join)
                        {
                            $join->on(
                                'q.quiebre_grupo_id',
                                '=',
                                'qgu.quiebre_grupo_id'
                            )
                            ->where(
                                'qgu.usuario_id',
                                '=',
                                Auth::id()
                            )
                            ->where(
                                'qgu.estado',
                                '=',
                                '1'
                            );
                        }
                    )
                    ->select(
                        'q.id',
                        'q.nombre',
                        DB::raw(
                            'IFNULL(qgu.estado,"disabled") as block'
                        )
                    )
                    ->where('q.estado', '=', '1')
                    ->whereRaw(
                        'q.id NOT IN (
                            SELECT quiebre_id
                            FROM quiebre_usuario_restringido
                            WHERE usuario_id="'.Auth::id().'"
                            AND estado=1
                        )'
                    )
                    ->orderBy('q.nombre')
                    ->get();


        return json_encode(array('rst'=>1,'datos'=>$quiebres));
    }
    public function empresa()
    {

        $empresas = DB::table('empresas as e')
                    ->join(
                        'empresa_usuario as eu', function($join) {
                        $join->on('e.id', '=', 'eu.empresa_id')
                        ->where('eu.usuario_id', '=', Auth::id())
                        ->where('eu.estado', '=', '1');
                        }
                    )
                    ->select(
                        'e.id', 'e.nombre', 'e.es_ec', DB::raw(
                            'IFNULL(eu.estado,"disabled") as block'
                        )
                    )
                    ->where('e.estado', '=', '1')
                    ->orderBy('e.nombre')
                    ->get();

        return json_encode(
            array(
                    'rst' => 1,
                    'datos' => $empresas,
                )
        );
    }
    public function celula()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarCelulaBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarCelulaBandeja');
            } else {
                $datos = Cache::remember('listarCelulaBandeja', 20, function(){
                    $data = DB::table('celulas')
                    ->select(
                        'id', 'nombre', DB::raw(
                            'CONCAT("E",empresa_id) as relation'
                        )
                    )
                    ->where('estado', '=', '1')
                    ->orderBy('nombre')
                    ->get();

                    return $data;
                });
            }
        } else {
            $datos = DB::table('celulas')
                ->select(
                    'id', 'nombre', DB::raw(
                        'CONCAT("E",empresa_id) as relation'
                    )
                )
                ->where('estado', '=', '1')
                ->orderBy('nombre')
                ->get();
        }

        return json_encode(array('rst' => 1, 'datos' => $datos));
    }
    public function tecnico()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarTecnicoBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarTecnicoBandeja');
            } else {
                $datos = Cache::remember('listarTecnicoBandeja', 20, function(){
                    $data=
                        DB::table('tecnicos as t')
                        ->join(
                            'celula_tecnico as ct',
                            'ct.tecnico_id', '=', 't.id'
                        )
                        ->join(
                            'celulas as c',
                            'c.id', '=', 'ct.celula_id'
                        )
                        ->select(
                            't.id', 't.nombre_tecnico as nombre',
                            DB::raw(
                                'CONCAT(
                                    GROUP_CONCAT( CONCAT("C",ct.celula_id)
                                        SEPARATOR "|,|"
                                    ),"|,|",
                                    GROUP_CONCAT( DISTINCT(CONCAT("E",c.empresa_id) )
                                        SEPARATOR "|,|"
                                    )
                                ) as relation'
                            )
                        )
                        ->where('t.estado', '=', '1')
                        ->where('ct.estado', '=', '1')
                        ->groupBy('t.id')
                        ->orderBy('t.nombre_tecnico')
                        ->get();

                    return $data;
                });
            }
        } else {
            $datos=
                DB::table('tecnicos as t')
                ->join(
                    'celula_tecnico as ct',
                    'ct.tecnico_id', '=', 't.id'
                )
                ->join(
                    'celulas as c',
                    'c.id', '=', 'ct.celula_id'
                )
                ->select(
                    't.id', 't.nombre_tecnico as nombre',
                    DB::raw(
                        'CONCAT(
                            GROUP_CONCAT( CONCAT("C",ct.celula_id)
                                SEPARATOR "|,|"
                            ),"|,|",
                            GROUP_CONCAT( DISTINCT(CONCAT("E",c.empresa_id) )
                                SEPARATOR "|,|"
                            )
                        ) as relation'
                    )
                )
                ->where('t.estado', '=', '1')
                ->where('ct.estado', '=', '1')
                ->groupBy('t.id')
                ->orderBy('t.nombre_tecnico')
                ->get();
        }
        $datos = Cache::get('listarTecnicoBandeja');
        return json_encode(array('rst'=>1,'datos'=>$datos));
    }
    public function zonal()
    {

        $z = DB::table('zonales')
                ->select(
                    'nombre', DB::raw('CONCAT(abreviatura,"|",id) as id')
                )
                ->where('estado', '=', '1')
                ->where(
                    function($query) {
                    //if (Input::get('usuario')) {
                        $query->whereRaw(
                            ' id IN (SELECT zonal_id
                            FROM usuario_zonal
                            WHERE usuario_id="'
                            . Auth::id() . '"
                            AND estado=1
                            )'
                        );
                    //}
                    }
                )
                ->get();
        return json_encode(
            array(
                'rst' => 1,
                'datos' => $z
            )
        );
    }
    public function troba()
    {
        $r = DB::table('geo_troba as gt')
                ->join(
                    'zonales as z',
                    'z.abreviatura', '=', 'gt.zonal'
                )
                ->join(
                    'usuario_zonal as uz',
                    'uz.zonal_id', '=', 'z.id'
                )
                ->where('uz.usuario_id', '=', Auth::id())
                ->select(
                    'gt.zonal',
                    'gt.nodo',
                    'gt.troba',
                    'gt.coord_x',
                    'gt.coord_y',
                    DB::raw('gt.troba as id, gt.troba as nombre ')
                )
                ->groupBy('gt.troba')
                ->orderBy('gt.troba')
                ->orderBy('gt.id')
                ->get();

        return json_encode(
            array(
                'rst' => 1,
                'datos' => $r
            )
        );

    }
    public function nodo()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarNodoBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarNodoBandeja');
            } else {
                $datos = Cache::remember('listarNodoBandeja', 20, function(){
                    $data = DB::table('geo_nodopunto')
                        ->select(
                            'nodo as id',
                            DB::raw(
                                'CONCAT(nodo,": ",nombre,"->",zonal) as nombre'
                            )
                        )
                        ->where('tecnologia','COAXIAL')
                        ->orderBy('nodo', 'asc')
                        ->get();

                    return $data;
                });
            }
        } else {
            $datos = DB::table('webpsi_fftt.nodos_eecc_regiones')
                ->select(
                    'NODO as id',
                    DB::raw(
                        'CONCAT(NODO,": ",NOMBRE,"->",PROVINCIA,"->",DPTO) as nombre'
                    )
                )
                ->orderBy('NODO', 'asc')
                ->get();
        }

        return json_encode(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }
    public function mdf()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarMdfBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarMdfBandeja');
            } else {
                $datos = Cache::remember('listarMdfBandeja', 20, function(){
                    $data  =   DB::table('webpsi_fftt.mdfs_eecc_regiones as m')
                      ->leftJoin('geo_mdfpunto as g', 'm.MDF', '=', 'g.mdf')
                      ->select(
                          'm.MDF AS nombre',
                          DB::raw(
                              'CONCAT(
                                IFNULL(m.MDF,""),"___",
                                IFNULL(
                                  replace(
                                    m.EECC_CRITICO,"LARI PLAYAS","LARI"
                                  ), EECC
                                )
                                ,"   ",
                                IFNULL(
                                  (
                                    SELECT id
                                    FROM empresas
                                    WHERE nombre IN (
                                      IFNULL(
                                        replace(
                                          m.EECC_CRITICO,"LARI PLAYAS","LARI"
                                        ), EECC
                                      )
                                    )
                                  )
                                  , ""
                                )
                                ,"___",
                                IFNULL(m.LEJANO,""),"___",
                                IFNULL(m.ZONA_CRITICO,"")
                            ) AS id'
                          ),
                          DB::raw("IFNULL(g.coord_x,'') AS coord_x"),
                          DB::raw("IFNULL(g.coord_y,'') AS coord_y")
                      )
                      ->where('m.zonal', '=', 'LIM')
                      ->orderBy('m.MDF', 'asc')
                      ->get();

                    return $data;
                });
            }
        } else {
             $datos  =   DB::table('webpsi_fftt.mdfs_eecc_regiones as m')
              ->leftJoin('geo_mdfpunto as g', 'm.MDF', '=', 'g.mdf')
              ->select(
                  'm.MDF AS nombre',
                  DB::raw(
                      'CONCAT(
                        IFNULL(m.MDF,""),"___",
                        IFNULL(
                          replace(
                            m.EECC_CRITICO,"LARI PLAYAS","LARI"
                          ), EECC
                        )
                        ,"   ",
                        IFNULL(
                          (
                            SELECT id
                            FROM empresas
                            WHERE nombre IN (
                              IFNULL(
                                replace(
                                  m.EECC_CRITICO,"LARI PLAYAS","LARI"
                                ), EECC
                              )
                            )
                          )
                          , ""
                        )
                        ,"___",
                        IFNULL(m.LEJANO,""),"___",
                        IFNULL(m.ZONA_CRITICO,"")
                    ) AS id'
                  ),
                  DB::raw("IFNULL(g.coord_x,'') AS coord_x"),
                  DB::raw("IFNULL(g.coord_y,'') AS coord_y")
              )
              ->where('m.zonal', '=', 'LIM')
              ->orderBy('m.MDF', 'asc')
              ->get();
        }

        return json_encode(
            array(
                'rst' => 1,
                'datos' => $datos
            )
        );
    }
    public function motivo()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarMotivoBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarMotivoBandeja');
            } else {
                $datos = Cache::remember('listarMotivoBandeja', 20, function(){
                    $data = DB::table('motivos AS m')
                        ->leftJoin(
                            'motivo_quiebre as mq',
                            'mq.motivo_id','=','m.id'
                        )
                        ->select(
                            'm.id', 'm.nombre',DB::raw('GROUP_CONCAT(CONCAT("Q",mq.quiebre_id) SEPARATOR "|,|" ) as relation')
                        )
                        ->where('m.estado', '=', '1')
                        ->where('mq.estado', '=', '1')
                        ->groupBy('mq.motivo_id')
                        ->orderBy('m.nombre')
                        ->get();

                    return $data;
                });
            }
        } else {
            $datos = DB::table('motivos AS m')
                ->leftJoin(
                    'motivo_quiebre as mq',
                    'mq.motivo_id','=','m.id'
                )
                ->select(
                    'm.id', 'm.nombre',DB::raw('GROUP_CONCAT(CONCAT("Q",mq.quiebre_id) SEPARATOR "|,|" ) as relation')
                )
                ->where('m.estado', '=', '1')
                ->where('mq.estado', '=', '1')
                ->groupBy('mq.motivo_id')
                ->orderBy('m.nombre')
                ->get();
        }
        
        return json_encode(
            array('rst' => 1,
                    'datos' => $datos
            )
        );
    }
    public function submotivo()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $datos = Cache::get('listarSubmotivoBandeja');
            if ( $datos ) {
                $datos = Cache::get('listarSubmotivoBandeja');
            } else {
                $datos = Cache::remember('listarSubmotivoBandeja', 20, function(){
                    $data = DB::table('submotivos AS m')
                            ->leftJoin(
                                'estado_motivo_submotivo as ms',
                                'ms.submotivo_id','=','m.id'
                            )
                            ->select(
                                'm.id',
                                'm.nombre',
                                DB::raw('CONCAT(
                                            GROUP_CONCAT(DISTINCT(CONCAT("N", ms.motivo_id))
                                                SEPARATOR "|,|"),"|,|",
                                            GROUP_CONCAT(DISTINCT(CONCAT("A",ms.estado_id)) SEPARATOR "|,|")
                                        ) AS relation'
                                )
                            )
                            ->where('m.estado', '=', '1')
                            ->where('ms.estado', '=', '1')
                            ->groupBy('ms.submotivo_id')
                            ->orderBy('m.nombre')
                            ->get();

                    return $data;
                });
            }
        } else {
            $datos = DB::table('submotivos AS m')
                ->leftJoin(
                    'estado_motivo_submotivo as ms',
                    'ms.submotivo_id','=','m.id'
                )
                ->select(
                    'm.id',
                    'm.nombre',
                    DB::raw('CONCAT(
                                GROUP_CONCAT(DISTINCT(CONCAT("N", ms.motivo_id))
                                    SEPARATOR "|,|"),"|,|",
                                GROUP_CONCAT(DISTINCT(CONCAT("A",ms.estado_id)) SEPARATOR "|,|")
                            ) AS relation'
                    )
                )
                ->where('m.estado', '=', '1')
                ->where('ms.estado', '=', '1')
                ->groupBy('ms.submotivo_id')
                ->orderBy('m.nombre')
                ->get();
        }
        
        return json_encode(
            array('rst' => 1,
                    'datos' => $datos
            )
        );
    }
    public function solucion()
    {
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $solucion = Cache::get('listarSolucionBandeja');
            if ( $solucion ) {
                $solucion = Cache::get('listarSolucionBandeja');
            } else {
                $solucion = Cache::remember('listarSolucionBandeja', 20, function(){
                    $datos = DB::table('soluciones_comerciales')
                            ->select('id', 'nombre')
                            ->where('estado', '=', 1)
                            ->orderBy('nombre')
                            ->get();

                    return $datos;
                });
            }
        } else {
            $solucion = DB::table('soluciones_comerciales')
                    ->select('id', 'nombre')
                    ->where('estado', '=', 1)
                    ->orderBy('nombre')
                    ->get();
        }

        return json_encode(
            array(
                'rst'=>1,
                'datos'=>$solucion
            )
        );
    }
    public function feedback(){
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $feedback = Cache::get('listarFeedbackBandeja');
            if ( $feedback ) {
                $feedback = Cache::get('listarFeedbackBandeja');
            } else {
                $feedback = Cache::remember('listarFeedbackBandeja', 20, function(){
                    $datos = DB::table('feedback_liquidados')
                        ->select('id', 'nombre')
                        ->where('estado', '=', 1)
                        ->orderBy('nombre')
                        ->get();

                    return $datos;
                });
            }
        } else {
            $feedback = DB::table('feedback_liquidados')
                    ->select('id', 'nombre')
                    ->where('estado', '=', 1)
                    ->orderBy('nombre')
                    ->get();
        }
        

        return json_encode(
            array(
                'rst'=>1,
                'datos'=>$feedback
            )
        );
    }
    public function cat_componente(){
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $r = Cache::get('listarCat_componenteBandeja');
            if ( $r ) {
                $r = Cache::get('listarCat_componenteBandeja');
            } else {
                $r = Cache::remember('listarCat_componenteBandeja', 20, function(){
                    $datos = DB::table('cat_componentes AS cp')
                        ->select(
                            DB::raw('MAX(cp.cod_componente) AS id'), 
                            'cp.desc_componente AS nombre'
                        )
                        ->groupBy('cp.desc_componente')
                        ->orderBy('cp.desc_componente')
                        ->get();

                    return $datos;
                });
            }
        } else {
            $r = DB::table('cat_componentes AS cp')
                ->select(
                    DB::raw('MAX(cp.cod_componente) AS id'), 
                    'cp.desc_componente AS nombre'
                )
                ->groupBy('cp.desc_componente')
                ->orderBy('cp.desc_componente')
                ->get();
        }

        return json_encode(array('rst' => 1, 'datos' => $r));
    }
    public function obs_tipo(){
        $redis = Redis::connection();
        try {
            $redis->ping();
        } catch (Exception $e) {}
        if ( !isset($e) ) {
            $m = Cache::get('listarObservacionTipoBandeja');
            if ( $m ) {
                $m = Cache::get('listarObservacionTipoBandeja');
            } else {
                $m = Cache::remember('listarObservacionTipoBandeja', 20, function(){
                    $datos = DB::table('observaciones_tipos')
                        ->select('id', 'nombre')
                        ->where('estado', '=', '1')
                        ->get();

                    return $datos;
                });
            }
        } else {
            $m=  DB::table('observaciones_tipos')
                ->select('id', 'nombre')
                ->where('estado', '=', '1')
                ->get();
        }

        return json_encode(
            array(
                'rst' => 1, 
                'datos' => $m
            )
        );
        
    }
}
