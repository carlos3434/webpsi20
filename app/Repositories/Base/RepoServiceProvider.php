<?php
namespace Repositories\Base;

use Repositories\Lista;

use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('Repositories\Lista\ListaRepoInterface',
            'Repositories\Lista\ListaRepo');
    }
}