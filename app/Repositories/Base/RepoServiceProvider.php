<?php
namespace Repositories\Base;

use Repositories\Lista;
use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('Repositories\Lista\ListaRepoInterface',
            'Repositories\Lista\ListaRepo');

        $this->app->bind('Repositories\ListadoLegoRepositoryInterface',
            'Repositories\ListadoLegoRepository');      

        $this->app->bind('Repositories\EstadoAsigRepositoryInterface',
            'Repositories\EstadoAsigRepository');

        $this->app->bind('Repositories\AsignacionRepositoryInterface',
            'Repositories\AsignacionRepository');
    }
}