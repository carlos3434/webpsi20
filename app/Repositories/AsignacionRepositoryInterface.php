<?php namespace Repositories;

interface AsignacionRepositoryInterface
{
    public function usuariosHabilitados();
    public function changeState();
    public function asignar();
}


