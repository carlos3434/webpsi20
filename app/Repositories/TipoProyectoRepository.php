<?php 
namespace Repositories;
use Abstracts\Repository as AbstractRepository;
class TipoProyectoRepository extends AbstractRepository implements TipoProyectoRepositoryInterface
{
    // This is where the "magic" comes from:
    protected $modelClassName = 'TipoProyecto';
    
    // This class only implements methods specific to the UserRepository
    public function findTipoProyectoByColumn($column)
    {
        $where = call_user_func_array("{$this->modelClassName}::where", array($column));
        return $where->get();
    }
    public function newTipoProyecto()
    {
        return new \TipoProyecto();
    }

    public function findOrFail($id)
    {
      return \TipoProyecto::findOrFail($id);
    }
    public function getTipoProyectos()
    {;
        $tipoProyectos = \TipoProyecto::all();
        $data = [];
        foreach ($tipoProyectos as $key => $value) {
            $data[$value['id']] = $value['nombre'];
        }
        return $data;
    }
    public function getTotalRecords()
    {
        $result = \TipoProyecto::count();
        return $result;
    }
}