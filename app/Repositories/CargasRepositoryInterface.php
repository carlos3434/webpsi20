<?php namespace Repositories;

interface CargasRepositoryInterface
{
	public function getdataUpload_excel($columns);
	public function getdataUpload_csv();
}

