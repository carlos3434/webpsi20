<?php namespace Repositories;

interface CotHelpRepositoryInterface
{
    public function setMotiveUpload($fields = array());
    public function headerAtento();
    public function uploadFiles($files,$identificador,$path,$save);
}


