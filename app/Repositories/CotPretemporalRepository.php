<?php namespace Repositories;
use Cot\models\PreTemporal as PreTemporales;
use Cot\models\PretemporalMovimiento as Movimiento;
use Cot\models\PretemporalUltimo as Ultimo;
use Repositories\CotHelpRepository as Helper;
use Illuminate\Support\Collection;

class CotPretemporalRepository implements CotPretemporalRepositoryInterface
{
    protected $_helper;
    public function __construct()
    {
        $this->_helper = new Helper();
    }
  

      /* Users to pass parameters to be in free list */
    public function getLibrebyusers($usuarios = '',$quiebre_id = '',$tipo = '')
    {
        $users_act = \DB::table('usuario_area as ua')
                    ->Leftjoin('usuario_horario as uh', 'ua.usuario_id', '=', 'uh.usuario_id')
                    ->select(
                        'ua.usuario_id',
                        \DB::raw(
                            '((SELECT cantidad FROM pre_temporales_admin where nombre="maxasig") -(SELECT COUNT(id) from pre_temporales  where  estado_pretemporal_id=1 and usuario_id=ua.usuario_id )) as libres'
                        )
                    )
                    ->where(
                        function ($query) use ($usuarios,$quiebre_id,$tipo) {
                        $query->where('ua.estado', 1);
                        $query->where('ua.area_id', 51);
                        $query->whereRaw('uh.dia_id=  (WEEKDAY(CURDATE()) + 1)');         
                        if($usuarios){
                            $query->whereIn('ua.usuario_id', $usuarios);
                        }

                        if($quiebre_id){
                            if($tipo == 2){
                                $query->where('ua.quiebre_id', $quiebre_id);
                            }else{
                                $query->whereRaw("FIND_IN_SET(ua.grupo_id ,(SELECT GROUP_CONCAT(CAST(id as CHAR(8))) FROM grupos WHERE quiebre_id=$quiebre_id and estado=1))");
                            }
                        }
                        $query->whereRaw("(CURDATE() NOT BETWEEN ua.fecha_inicio AND ua.fecha_fin OR fecha_inicio IS NULL)");
                        }
                    )                    
                    ->groupBy('ua.usuario_id')
                    ->having('libres', '>', 0)
                    ->get();
        return $users_act;
    }

    /*Tickets pending*/
    public function ticketPendientes($quiebre_id,$type = 1)
    {
        $inicio = date('Y-m-d', strtotime(date('Y-m-d'). ' - 7 days'));
        $fin = date('Y-m-d');
        $startDate = date('Y-m-d  00:00:00', strtotime($inicio));
        $endDate = date('Y-m-d  23:59:59', strtotime($fin));

        $pendientes = \DB::table('pre_temporales as p')
                ->Leftjoin('actividades as a', 'a.id', '=', 'p.actividad_id')
                ->join('actividades_tipos as at', 'p.actividad_tipo_id', '=', 'at.id')
                ->Leftjoin('quiebres as q', 'p.quiebre_id', '=', 'q.id')
                ->join('estados_pre_temporales as ep', 'ep.id', '=', 'p.estado_pretemporal_id');

        if($type == 1){
            $pendientes = $pendientes->select('p.id', 'p.quiebre_id');   
        }else{
            $pendientes = $pendientes->select('q.nombre as quiebre', \DB::raw('COUNT(p.id) as cantidad'), 'p.quiebre_id');
        }        

        $pendientes = $pendientes->where(function($query) use ($quiebre_id){
            $query->where('p.estado_pretemporal_id', 1);
            $query->whereRaw('(p.usuario_id = 0 or p.usuario_id="" or usuario_id IS NULL)');
            if($quiebre_id){
                $query->whereIn('p.quiebre_id', $quiebre_id);
            }
        })->whereBetween('p.created_at',array($startDate,$endDate));
        
        if($type !== 1){ //count
            $pendientes = $pendientes->groupBy('p.quiebre_id');            
        }
        $pendientes = $pendientes->orderBy('p.quiebre_id', 'ASC')->get();
        return $pendientes;
    }

    public function Rptatento()
    {
        $data = \DB::table('pre_temporales as p')
                ->select(
                    'p.cod_multigestion as CNT_CODIGO',                             
                    \DB::raw('CONCAT("E",p.id) as ID_GESTION'),
                    'ptm.created_at as FECHA_HORA',
                    \DB::raw('CONCAT(u.nombre, " ",u.apellido) as GESTOR'),
                    \DB::raw('IFNULL(m.nombre," ") as TIPO_DE_CONTACTO'),
                    \DB::raw('CONCAT(s.nombre, " ", IFNULL(pts.accion, "")) as RESULTADO'),
                    //'ptm.observacion as OBSERVACIONES'
                    \DB::raw("TRIM(REPLACE(REPLACE(REPLACE(REPLACE(ptm.observacion, CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),'/',' ')) as OBSERVACIONES")
                )
                ->leftjoin('pre_temporales_movimientos as ptm','ptm.pre_temporal_id','=','p.id')
                ->join('actividades_tipos as at','p.actividad_tipo_id','=','at.id')
                ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
                ->leftjoin('motivos as m', 'm.id', '=', 'ptm.motivo_id')
                ->leftjoin('submotivos as s', 's.id', '=', 'ptm.submotivo_id')
                ->leftjoin('pre_temporales_solucionado as pts','pts.id', '=','ptm.pre_temporal_solucionado_id')
                ->where(
                    function($query){
                        $query->where('ptm.usuario_created_at', '<>', 697);
                        $query->whereIn('p.quiebre_id', array(51,52,56));
                        $query->whereNotNull('p.cod_multigestion');                  
                    }
                );
                
                $inicio = new \DateTime;
                $inicio->modify('-48 hours');
                $format_inicio = $inicio->format('Y-m-d H:i:s');

                $fin = new \DateTime;
                $format_fin = $fin->format('Y-m-d H:i:s');
                $data->whereBetween('ptm.created_at', array($format_inicio,$format_fin));
                $data = $data->get();
                return $data;
    }
  public function busquedaTicket(){    
        $post = array();
        $post = \Input::all();
        foreach ($post as $k => $value) {
            $bk = array("slct_","txt_","chk_","rdb_","_modal");
            $rk = array("","","","","");
            $k = str_replace($bk, $rk, $k);
            $post[$k] = $value;
        }
        \Input::replace($post);
        $tipoBusqueda = \Input::get('tipo');        
        $busqueda = \Input::get('busqueda'); //dni de embajador
        if (\Input::get('buscar')) {
            $busqueda = \Input::get('buscar');
        }
        $PG = \Input::get('PG'); //busqueda personalizada o general

        $query =\DB::table('pre_temporales as p')
                ->Leftjoin('actividades as a', 'a.id', '=', 'p.actividad_id')
                ->join('actividades_tipos as at', 'p.actividad_tipo_id', '=', 'at.id')
                ->Leftjoin('quiebres as q', 'p.quiebre_id', '=', 'q.id')
                ->Leftjoin('pre_temporales_ultimos as ptu','ptu.pre_temporal_id','=','p.id')
                ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
                ->Leftjoin('motivos as m', 'ptu.motivo_id', '=', 'm.id')
                ->Leftjoin('submotivos as s', 'ptu.submotivo_id', '=', 's.id')
                ->Leftjoin('pre_temporales_solucionado as pts','pts.id', '=','ptu.pre_temporal_solucionado_id')
                ->Leftjoin('pre_temporales_averias as pta','pta.pre_temporal_id', '=','p.id')
                ->join('estados_pre_temporales as ep','ep.id','=','p.estado_pretemporal_id')
                ->Leftjoin('eventos_masiva as em', 'em.id', '=', 'p.evento_masiva_id')
                ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'p.tipo_atencion')       
                ->Leftjoin('usuarios as u', 'u.id', '=', \DB::raw('CASE WHEN ptu.usuario_updated_at
                                      THEN ptu.usuario_created_at
                                      ELSE ptu.usuario_updated_at
                                  END'))
                //->Leftjoin('usuarios as uc', 'ptu.usuario_created_at', '=', 'uc.id')
                //->Leftjoin('usuarios as u', 'ptu.usuario_updated_at', '=', 'u.id')
                ->Leftjoin('usuarios as u1', 'p.usuario_id', '=', 'u1.id')
                ->leftjoin('usuario_area as ua', 'ptu.usuario_updated_at', '=', 'ua.usuario_id')
                ->leftJoin('grupos as g', 'ua.grupo_id', '=', 'g.id')
        		->join(
                    'quiebre_grupo_usuario as qgu',
                    function ($join) {
                        $join->on('q.quiebre_grupo_id', '=', 'qgu.quiebre_grupo_id')
                        ->where('qgu.usuario_id', '=', \Auth::id())
                        ->where('qgu.estado', '=', '1');
                    }
                );
        /*if(!Input::get('eventos')){
           $query = $query->Leftjoin('eventos_masiva as em',
                function($leftjoin){
                    $leftjoin->on('em.id','=','p.evento_masiva_id')
                    ->where('em.estado','<>','2');            
                    
                }
            );
        }*/

        if (\Input::get('excel')) {
            $query->select(
                \DB::raw('CONCAT("E",p.id) as Ticket'),
                'at.apocope as TipoAveria',
                'pa.nombre as Atencion',
                'q.nombre as Quiebre',
                'p.codactu as CodAveria',
                'p.codcli',
                'p.cliente_nombre as ClienteNombre',
                'p.cliente_celular as ClienteCelular',
                'p.cliente_telefono as ClienteTelefono',
                'p.cliente_dni as ClienteDNI',
                'p.embajador_nombre as EmbajadorNombre',
                'p.embajador_celular as EmbajadorCelular',
                'p.embajador_correo as EmbajadorCorreo',
                'p.embajador_dni as EmbajadorDNI',
                \DB::raw("TRIM(REPLACE(REPLACE(REPLACE(REPLACE(p.comentario, CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),'/',' ')) as ComentarioTicket"),
                \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                \DB::raw(
                    'DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                        as FechaRegistro'
                ),
                \DB::raw('IFNULL(m.nombre," ") as motivo'),
                \DB::raw('IFNULL(s.nombre," ") as submotivo'),
                \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
                \DB::raw("TRIM(REPLACE(REPLACE(REPLACE(REPLACE(ptu.observacion,CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),'/',' ')) as ObservacionUltMov"),
                \DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as FechaUltMov'
                ),
                \DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                'u.usuario as login',
                //\DB::raw('case when ptu.usuario_updated_at IS NU then CONCAT(uc.nombre, " ", uc.apellido) else CONCAT(u.nombre, " ", u.apellido) end as UsuarioMov'),
                //\DB::raw('IFNULL(u.usuario,uc.usuario) as login'),
                'pts.producto as Producto',
                'pts.accion as Accion',
                'pta.estado_legado as EstadoLegado',
                'pta.fecha_registro_legado as FechaRegistroLegado',
                'pta.fecha_liquidacion as FechaLiquidacion',
                'pta.cod_liquidacion as CodigoLiquidacion',
                'pta.detalle_liquidacion as DetalleLiquidacion',
                'pta.area as Area',
                'pta.contrata as Contrata',
                'pta.zonal as Zonal',
                'em.nombre as evento_masiva',
                \DB::raw('CONCAT(m.codigo_cot,s.codigo_cot) as CodMov'),
                \DB::raw('IFNULL(p.fh_reg104," ") as fh_reg104'),
                \DB::raw('IFNULL(p.fh_reg1l," ") as fh_reg1l'),
                \DB::raw('IFNULL(p.fh_reg2l," ") as fh_reg2l'),
                \DB::raw('IFNULL(p.cod_multigestion," ") as cod_multigestion'),
                \DB::raw('IFNULL(p.llamador," ") as llamador'),
                \DB::raw('IFNULL(p.titular," ") as titular'),
                \DB::raw('IFNULL(p.direccion," ") as direccion'),
                \DB::raw('IFNULL(p.distrito," ") as distrito'),
                \DB::raw('IFNULL(p.urbanizacion," ") as urbanizacion'),
                \DB::raw('IFNULL(p.telf_gestion," ") as telf_gestion'),
                \DB::raw('IFNULL(p.telf_entrante," ") as telf_entrante'),
                \DB::raw('IFNULL(p.operador," ") as operador'),
                \DB::raw('IFNULL(p.motivo_call," ") as motivo_call'),
                \DB::raw('IFNULL(g.nombre," ") as grupo')          
            );
        }else if(\Input::get('checks')){
            $query->select(
                'p.id'
            );
        } else {
            $query->select(
                \DB::raw('CONCAT("E",p.id) as ticket'),
                'p.id',
                'at.apocope as tipo_averia',
                'pa.nombre as tipo_atencion',
                'p.gestion',
                'p.codactu',
                'p.codcli',
                'p.cliente_nombre',
                'p.cliente_celular',
                'p.cliente_telefono',
                'p.cliente_dni',
                'p.embajador_nombre',
                'p.estado_pretemporal_id as estado_id',
                'q.nombre as quiebre',
                \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                \DB::raw('IFNULL(CONCAT(pts.producto,"-", pts.accion)," ") as solucion'),
                \DB::raw(
                    'IFNULL(
                        CONCAT(pts.producto,"-", pts.accion)," "
                    ) as solucion'
                ),
                \DB::raw(
                    'IFNULL(
                        CONCAT(pta.cod_liquidacion)," "
                    ) as codliq'
                ),
                \DB::raw('IFNULL(pta.area, " ") as area'),
                \DB::raw(
                    'DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                        as fecha_registro'
                ),
                \DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as fechaultmov'
                ),
                \DB::raw('IFNULL(m.nombre," ") as motivo'),
                \DB::raw('IFNULL(s.nombre," ") as submotivo'),
                \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
		        \DB::raw('IFNULL(p.fh_reg104," ") as fh_reg104'),
                \DB::raw('IFNULL(p.fh_reg1l," ") as fh_reg1l'),
                \DB::raw('IFNULL(p.fh_reg2l," ") as fh_reg2l'),
                \DB::raw('IFNULL(p.cod_multigestion," ") as cod_multigestion'),
                \DB::raw('IFNULL(p.llamador," ") as llamador'),
                \DB::raw('IFNULL(p.titular," ") as titular'),
                \DB::raw('IFNULL(p.direccion," ") as direccion'),
                \DB::raw('IFNULL(p.distrito," ") as distrito'),
                \DB::raw('IFNULL(p.urbanizacion," ") as urbanizacion'),
                \DB::raw('IFNULL(p.telf_gestion," ") as telf_gestion'),
                \DB::raw('IFNULL(p.telf_entrante," ") as telf_entrante'),
                \DB::raw('IFNULL(p.operador," ") as operador'),
                \DB::raw('IFNULL(p.motivo_call," ") as motivo_call'),
                \DB::raw('IFNULL(g.nombre," ") as grupo')          
            );
        }
        
        $query->where('p.actividad_id', 1)
              ->whereRaw(
                  'q.id NOT IN (
                        SELECT quiebre_id
                        FROM quiebre_usuario_restringido
                        WHERE usuario_id="'.\Auth::id().'"
                        AND estado=1
                    )'
              );
            $query->where('p.estado_pretemporal_id', '<>', '10');
	        $query->whereNull('p.deleted_at');

        

        if (\Input::get('usuarios')) {
                $query->whereIn('p.usuario_id', \Input::get('usuarios'));
        }

        if ($PG == 'P') {
            switch ($tipoBusqueda) {
                case 'ticket':
                    $datos= array();
                    $array = explode("E", strtoupper($busqueda));
                    if (count($array)>1) {
                        $ticketId = $array[1];
                        $query->where('p.id', '=', (int)$ticketId);
                    } else {
                        $query->where('p.id', '=', (int)$busqueda);
                    }
                    break;
                case 'codactu':
                    $query->where('p.codactu', '=', $busqueda);
                    break;
                case 'dniemb':
                    $query->where('p.embajador_dni', '=', $busqueda);
                    break;
                case 'dni':
                case 'dnicli':
                    $query->where('p.cliente_dni', '=', $busqueda);
                    break;
                case 'codcli':
                    $query->where('p.codcli', '=', $busqueda);
                    break;
                case 'codmultigestion':
                    $query->where('p.cod_multigestion', '=', $busqueda);
                    break;
                break;
            }
        } elseif ($PG == 'G') {
            $tipo_averia = \Input::get('averia');
            $quiebre = \Input::get('quiebre');
            $estado = \Input::get('estado');
            $estadoPre = \Input::get('estado_pre');
            $atencion = \Input::get('atencion');
            $fechaRegistro = \Input::get('fecha_registro');
            $motivo = \Input::get('motivo');
            $submotivo = \Input::get('submotivo');
            $gestion = \Input::get('gestion');

            if(\Input::get('eventos')){
                $query->whereIn('p.evento_masiva_id', \Input::get('eventos'));                
            }

            if(\Input::has('asignacion')){
                if(\Input::get('asignacion') == 1){
                    $query->whereRaw('(p.usuario_id != 0 and p.usuario_id!="" and p.usuario_id IS NOT NULL)');                
                }elseif(\Input::get('asignacion') == 2){
                    $query->whereRaw('(p.usuario_id = 0 or p.usuario_id="" or p.usuario_id IS NULL)');
                }
            }

            if (count($tipo_averia) != 0) {
                $query->whereIn('at.apocope', $tipo_averia);
            }
            if (count($atencion) != 0) {
                $query->whereIn('p.tipo_atencion', $atencion);
            }
            if (count($estadoPre) != 0) {
                $query->whereIn('p.estado_pretemporal_id', $estadoPre);
            }
            if (count($estado) != 0) {
                $query->whereIn('ptu.estado_id', $estado);
            }
            if (count($motivo) != 0) {
                $query->whereIn('ptu.motivo_id', $motivo);
            }
            if (count($submotivo) != 0) {
                $query->whereIn('ptu.submotivo_id', $submotivo);
            }
            if (count($gestion) != 0) {
                $query->whereIn('p.gestion', $gestion);
            }
            if (count($quiebre) != 0) {
                $query->whereIn('p.quiebre_id', $quiebre);
            } else {
            }
            if (count($fechaRegistro) != 0) {
                $fecha = explode(" - ", $fechaRegistro);         
                if (count($fecha) > 0) {
                    if($fecha[0]){
                        $startDate = date('Y-m-d  00:00:00', strtotime($fecha[0]));
                        $endDate = date('Y-m-d  23:59:59', strtotime($fecha[1]));
                    }else{
                        $startDate = date('Y-m-d', strtotime(date('Y-m-d H:i:s'). ' - 7 days'));
                        $endDate = date('Y-m-d H:i:s');     
                    }                    
                    $query->whereBetween('p.created_at',array( $startDate,$endDate));                        
                }
            }
        }

        if (\Input::get('usuario_id')) {
                $query->where('p.usuario_id', \Input::get('usuario_id'));
                $query->orderBy('p.created_at', 'asc');
        }
        $resultado = $query->get();
       return $resultado;
    }

     public function listarTicketsReasignacion() {
        $post = $this->_helper->convertInput();
        \Input::replace($post);     

        $query = PreTemporales::from('pre_temporales')
                ->join('actividades_tipos as at', 'pre_temporales.actividad_tipo_id', '=', 'at.id')
                ->Leftjoin('quiebres as q', 'pre_temporales.quiebre_id', '=', 'q.id')
                ->Leftjoin('pre_temporales_ultimos as ptu','ptu.pre_temporal_id','=','pre_temporales.id')
                ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
                ->Leftjoin('motivos as m', 'ptu.motivo_id', '=', 'm.id')
                ->Leftjoin('submotivos as s', 'ptu.submotivo_id', '=', 's.id')
                ->Leftjoin('pre_temporales_averias as pta','pta.pre_temporal_id', '=','pre_temporales.id')
                ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'pre_temporales.tipo_atencion')
                ->Leftjoin('pre_temporales_solucionado as pts','pts.id', '=','ptu.pre_temporal_solucionado_id')      
                ->Leftjoin('usuarios as u1', 'pre_temporales.usuario_id', '=', 'u1.id');

        if(\Input::get('checks')){
            $query->select('pre_temporales.id');
        }else{
            $query->select(
            \DB::raw('CONCAT("E",pre_temporales.id) as ticket'),
            'pre_temporales.id',
            'at.apocope as tipo_averia',
            'pa.nombre as tipo_atencion',
            'pre_temporales.codcli',
            'pre_temporales.cliente_nombre',
            'pre_temporales.estado_pretemporal_id as estado_id',
            'q.nombre as quiebre',
            \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
            \DB::raw('IFNULL(CONCAT(pts.producto,"-", pts.accion)," ") as solucion'),
            \DB::raw('IFNULL(CONCAT(pta.cod_liquidacion)," ") as codliq'),
            \DB::raw('IFNULL(pta.area, " ") as area'),
            \DB::raw('DATE_FORMAT(pre_temporales.created_at,"%d-%m-%y %H:%i:%s") as fecha_registro'),
            \DB::raw('IFNULL(DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s")," ") as fechaultmov'),
            \DB::raw('IFNULL(m.nombre," ") as motivo'),
            \DB::raw('IFNULL(s.nombre," ") as submotivo'),
            \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'));
        }

            $query->where(function($q) {
                    $tipo_individual_busqueda = \Input::get('tipo');

                    $busqueda = \Input::get('busqueda'); //dni de embajador
                    if (\Input::get('buscar')) {
                        $busqueda = \Input::get('buscar');
                    }
                    $tipo_busqueda = \Input::get('PG'); //busqueda personalizada o general

                    $q->where('pre_temporales.actividad_id', 1);
                    $q->whereRaw(
                          'q.id NOT IN (
                                SELECT quiebre_id
                                FROM quiebre_usuario_restringido
                                WHERE usuario_id="'.\Auth::id().'"
                                AND estado=1
                            )'
                      );
                    $q->where('pre_temporales.estado_pretemporal_id', '<>', '10');
                    $q->whereNull('pre_temporales.deleted_at');

                    if(\Input::get('search')){
                        $q->where('pre_temporales.codcli','like',"%". \Input::get('search')."%");
                    }

                    if (\Input::get('usuario_id')) {
                         $q->where('pre_temporales.usuario_id', \Input::get('usuario_id'));
                    }

                    if ($tipo_busqueda == 'P') {
                        switch ($tipo_individual_busqueda) {
                            case 'ticket':
                                $datos= array();
                                $array = explode("E", strtoupper($busqueda));
                                if (count($array)>1) {
                                    $ticketId = $array[1];
                                    $q->where('pre_temporales.id', '=', (int)$ticketId);
                                } else {
                                    $q->where('pre_temporales.id', '=', (int)$busqueda);
                                }
                                break;
                            case 'codactu':
                                $q->where('pre_temporales.codactu', '=', $busqueda);
                                break;
                            case 'dniemb':
                                $q->where('pre_temporales.embajador_dni', '=', $busqueda);
                                break;
                            case 'dnicli':
                                $q->where('pre_temporales.cliente_dni', '=', $busqueda);
                                break;
                            case 'codcli':
                                $q->where('pre_temporales.codcli', '=', $busqueda);
                                break;
                            case 'codmultigestion':
                                $q->where('pre_temporales.cod_multigestion', '=', $busqueda);
                                break;
                            break;
                        }
                    } elseif ($tipo_busqueda == 'G') {
                        $tipo_averia = \Input::get('averia');
                        $quiebre = \Input::get('quiebre');
                        $estado = \Input::get('estado');
                        $estadoPre = \Input::get('estado_pre');
                        $atencion = \Input::get('atencion');
                        $fechaRegistro = \Input::get('fecha_registro');
                        $motivo = \Input::get('motivo');
                        $submotivo = \Input::get('submotivo');
                        $gestion = \Input::get('gestion');
                        $eventos = \Input::get('eventos');
                        /*if(\Input::get('eventos')){
                            $q->whereIn('pre_temporales.evento_masiva_id', \Input::get('eventos'));                
                        }*/

                        if(\Input::has('asignacion')){
                            if(\Input::get('asignacion') == 1){
                                $q->whereRaw('(pre_temporales.usuario_id != 0 and pre_temporales.usuario_id!="" and pre_temporales.usuario_id IS NOT NULL)');                
                            }elseif(\Input::get('asignacion') == 2){
                                $q->whereRaw('(pre_temporales.usuario_id = 0 or pre_temporales.usuario_id="" or pre_temporales.usuario_id IS NULL)');
                            }
                        }

                        if (count($eventos) != 0) {
                           $q->whereIn('pre_temporales.evento_masiva_id', \Input::get('eventos'));   
                        }
                        if (count($tipo_averia) != 0) {
                            $q->whereIn('at.apocope', $tipo_averia);
                        }
                        if (count($atencion) != 0) {
                            $q->whereIn('pre_temporales.tipo_atencion', $atencion);
                        }
                        if (count($estadoPre) != 0) {
                            $q->whereIn('pre_temporales.estado_pretemporal_id', $estadoPre);
                        }
                        if (count($estado) != 0) {
                            $q->whereIn('ptu.estado_id', $estado);
                        }
                        if (count($motivo) != 0) {
                            $q->whereIn('ptu.motivo_id', $motivo);
                        }
                        if (count($submotivo) != 0) {
                            $q->whereIn('ptu.submotivo_id', $submotivo);
                        }
                        if (count($gestion) != 0) {
                            $q->whereIn('pre_temporales.gestion', $gestion);
                        }
                        if (count($quiebre) != 0) {
                            $q->whereIn('pre_temporales.quiebre_id', $quiebre);
                        }

                        if (count($fechaRegistro) != 0) {
                            $fecha = explode(" - ", $fechaRegistro);
                            if (count($fecha) > 0) {
                                $q->whereBetween('pre_temporales.created_at', [$fecha[0]. " 00:00:00", $fecha[1]. " 23:59:59"]);
                            }
                        }
                    }                
                });

        if(! \Input::has('checks') ){
            $perPage = \Input::has('per_page') ? (int)\Input::get('per_page') : null;
            $result = $query->paginate($perPage);       
            $data = $result->toArray();   
            $col = new Collection([
                'recordsTotal'=>$result->getTotal(),
                'recordsFiltered'=>$result->getTotal(),
                'searchtype'=>\Input::get('PG')
            ]);
            return $col->merge($data);             
        }else{          
            return $query->get();
        }
    }

   public function listarTickets(){
	 $post = $this->_helper->convertInput();
        \Input::replace($post);      

        $query = PreTemporales::from('pre_temporales')
                ->Leftjoin('actividades as a', 'a.id', '=', 'pre_temporales.actividad_id')
                ->join('actividades_tipos as at', 'pre_temporales.actividad_tipo_id', '=', 'at.id')
                ->Leftjoin('quiebres as q', 'pre_temporales.quiebre_id', '=', 'q.id')
                ->Leftjoin('pre_temporales_ultimos as ptu','ptu.pre_temporal_id','=','pre_temporales.id')
                ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
                ->Leftjoin('motivos as m', 'ptu.motivo_id', '=', 'm.id')
                ->Leftjoin('submotivos as s', 'ptu.submotivo_id', '=', 's.id')
                ->Leftjoin('pre_temporales_solucionado as pts','pts.id', '=','ptu.pre_temporal_solucionado_id')
                ->Leftjoin('pre_temporales_averias as pta','pta.pre_temporal_id', '=','pre_temporales.id')
                ->join('estados_pre_temporales as ep','ep.id','=','pre_temporales.estado_pretemporal_id')
                ->Leftjoin('eventos_masiva as em', 'em.id', '=', 'pre_temporales.evento_masiva_id')
                ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'pre_temporales.tipo_atencion')       
                ->Leftjoin('usuarios as u', 'ptu.usuario_updated_at', '=', 'u.id')
                ->Leftjoin('usuarios as u1', 'pre_temporales.usuario_id', '=', 'u1.id')
                //->leftjoin('usuario_area as ua', 'ua.usuario_id','=','ptu.usuario_updated_at')
                //->leftJoin('grupos as g', 'ua.grupo_id', '=', 'g.id')
                ->join(
                    'quiebre_grupo_usuario as qgu',
                    function ($join) {
                        $join->on('q.quiebre_grupo_id', '=', 'qgu.quiebre_grupo_id')
                        ->where('qgu.usuario_id', '=', \Auth::id())
                        ->where('qgu.estado', '=', '1');
                    }
                );

        if (\Input::get('excel')) {
            $query->select(
                \DB::raw('CONCAT("E",pre_temporales.id) as Ticket'),
                'at.apocope as TipoAveria',
                'pa.nombre as Atencion',
                'q.nombre as Quiebre',
                'pre_temporales.codactu as CodAveria',
                'pre_temporales.codcli',
                'pre_temporales.cliente_nombre as ClienteNombre',
                'pre_temporales.cliente_celular as ClienteCelular',
                'pre_temporales.cliente_telefono as ClienteTelefono',
                'pre_temporales.cliente_dni as ClienteDNI',
                'pre_temporales.embajador_nombre as EmbajadorNombre',
                'pre_temporales.embajador_celular as EmbajadorCelular',
                'pre_temporales.embajador_correo as EmbajadorCorreo',
                'pre_temporales.embajador_dni as EmbajadorDNI',
                'pre_temporales.comentario as ComentarioTicket',
                \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                \DB::raw(
                    'DATE_FORMAT(pre_temporales.created_at,"%d-%m-%y %H:%i:%s") 
                        as FechaRegistro'
                ),
                \DB::raw('IFNULL(m.nombre," ") as motivo'),
                \DB::raw('IFNULL(s.nombre," ") as submotivo'),
                \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
                'ptu.observacion as ObservacionUltMov',
                \DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as FechaUltMov'
                ),
                \DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                'u.usuario as login',
                'pts.producto as Producto',
                'pts.accion as Accion',
                'pta.estado_legado as EstadoLegado',
                'pta.fecha_registro_legado as FechaRegistroLegado',
                'pta.fecha_liquidacion as FechaLiquidacion',
                'pta.cod_liquidacion as CodigoLiquidacion',
                'pta.detalle_liquidacion as DetalleLiquidacion',
                'pta.area as Area',
                'pta.contrata as Contrata',
                'pta.zonal as Zonal',
                'em.nombre as evento_masiva',
                \DB::raw('CONCAT(m.codigo_cot,s.codigo_cot) as CodMov'),
                \DB::raw('IFNULL(pre_temporales.fh_reg104," ") as fh_reg104'),
                \DB::raw('IFNULL(pre_temporales.fh_reg1l," ") as fh_reg1l'),
                \DB::raw('IFNULL(pre_temporales.fh_reg2l," ") as fh_reg2l'),
                \DB::raw('IFNULL(pre_temporales.cod_multigestion," ") as cod_multigestion'),
                \DB::raw('IFNULL(pre_temporales.llamador," ") as llamador'),
                \DB::raw('IFNULL(pre_temporales.titular," ") as titular'),
                \DB::raw('IFNULL(pre_temporales.direccion," ") as direccion'),
                \DB::raw('IFNULL(pre_temporales.distrito," ") as distrito'),
                \DB::raw('IFNULL(pre_temporales.urbanizacion," ") as urbanizacion'),
                \DB::raw('IFNULL(pre_temporales.telf_gestion," ") as telf_gestion'),
                \DB::raw('IFNULL(pre_temporales.telf_entrante," ") as telf_entrante'),
                \DB::raw('IFNULL(pre_temporales.operador," ") as operador'),
                \DB::raw('IFNULL(pre_temporales.motivo_call," ") as motivo_call')
                //\DB::raw('IFNULL(g.nombre," ") as grupo')          
            );
        } else {
            $query->select(
                \DB::raw('CONCAT("E",pre_temporales.id) as ticket'),
                'pre_temporales.id',
                'at.apocope as tipo_averia',
                'pa.nombre as tipo_atencion',
                'pre_temporales.gestion',
                'pre_temporales.codactu',
                'pre_temporales.codcli',
                'pre_temporales.cliente_nombre',
                'pre_temporales.cliente_celular',
                'pre_temporales.cliente_telefono',
                'pre_temporales.cliente_dni',
                'pre_temporales.embajador_nombre',
                'pre_temporales.estado_pretemporal_id as estado_id',
                'q.nombre as quiebre',
                \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                \DB::raw(
                    'IFNULL(
                        CONCAT(pts.producto,"-", pts.accion)," "
                    ) as solucion'
                ),
                \DB::raw(
                    'IFNULL(
                        CONCAT(pta.cod_liquidacion)," "
                    ) as codliq'
                ),
                \DB::raw('IFNULL(pta.area, " ") as area'),
                \DB::raw(
                    'DATE_FORMAT(pre_temporales.created_at,"%d-%m-%y %H:%i:%s") 
                        as fecha_registro'
                ),
                \DB::raw(
                    'IFNULL(
                            DATE_FORMAT(ptu.updated_at,"%d-%m-%y %H:%i:%s"),
                            " "
                        ) as fechaultmov'
                ),
                \DB::raw('IFNULL(m.nombre," ") as motivo'),
                \DB::raw('IFNULL(s.nombre," ") as submotivo'),
                \DB::raw('IFNULL(e.nombre,"Recepcionado") as estado'),
                \DB::raw('IFNULL(pre_temporales.fh_reg104," ") as fh_reg104'),
                \DB::raw('IFNULL(pre_temporales.fh_reg1l," ") as fh_reg1l'),
                \DB::raw('IFNULL(pre_temporales.fh_reg2l," ") as fh_reg2l'),
                \DB::raw('IFNULL(pre_temporales.cod_multigestion," ") as cod_multigestion'),
                \DB::raw('IFNULL(pre_temporales.llamador," ") as llamador'),
                \DB::raw('IFNULL(pre_temporales.titular," ") as titular'),
                \DB::raw('IFNULL(pre_temporales.direccion," ") as direccion'),
                \DB::raw('IFNULL(pre_temporales.distrito," ") as distrito'),
                \DB::raw('IFNULL(pre_temporales.urbanizacion," ") as urbanizacion'),
                \DB::raw('IFNULL(pre_temporales.telf_gestion," ") as telf_gestion'),
                \DB::raw('IFNULL(pre_temporales.telf_entrante," ") as telf_entrante'),
                \DB::raw('IFNULL(pre_temporales.operador," ") as operador'),
                \DB::raw('IFNULL(pre_temporales.motivo_call," ") as motivo_call')
                //\DB::raw('IFNULL(g.nombre," ") as grupo')                    
            );
        }
        
        $query->where(function($q){
            $tipoBusqueda = \Input::get('tipo');
            $busqueda = \Input::get('busqueda'); //dni de embajador
            if (\Input::get('buscar')) {
                $busqueda = \Input::get('buscar');
            }
            $PG = \Input::get('PG'); //busqueda personalizada o general

            $q->where('pre_temporales.actividad_id', 1);
            $q->whereRaw(
                  'q.id NOT IN (
                        SELECT quiebre_id
                        FROM quiebre_usuario_restringido
                        WHERE usuario_id="'.\Auth::id().'"
                        AND estado=1
                    )'
              );
            $q->where('pre_temporales.estado_pretemporal_id', '<>', '10');
            $q->whereNull('pre_temporales.deleted_at');

            if (\Input::get('usuario_id')) {
                 $q->where('pre_temporales.usuario_id', \Input::get('usuario_id'));
            }

            if(\Input::get('search')){
                $q->where('pre_temporales.codcli','like',"%". \Input::get('search')."%");
            }

            if ($PG == 'P') {
                switch ($tipoBusqueda) {
                    case 'ticket':
                        $datos= array();
                        $array = explode("E", strtoupper($busqueda));
                        if (count($array)>1) {
                            $ticketId = $array[1];
                            $q->where('pre_temporales.id', '=', (int)$ticketId);
                        } else {
                            $q->where('pre_temporales.id', '=', (int)$busqueda);
                        }
                        break;
                    case 'codactu':
                        $q->where('pre_temporales.codactu', '=', $busqueda);
                        break;
                    case 'dniemb':
                        $q->where('pre_temporales.embajador_dni', '=', $busqueda);
                        break;
                    case 'dnicli':
                        $q->where('pre_temporales.cliente_dni', '=', $busqueda);
                        break;
                    case 'codcli':
                        $q->where('pre_temporales.codcli', '=', $busqueda);
                        break;
                    case 'codmultigestion':
                        $q->where('pre_temporales.cod_multigestion', '=', $busqueda);
                        break;

                    break;
                }
            } elseif ($PG == 'G') {
                $tipo_averia = \Input::get('averia');
                $quiebre = \Input::get('quiebre');
                $estado = \Input::get('estado');
                $estadoPre = \Input::get('estado_pre');
                $atencion = \Input::get('atencion');
                $fechaRegistro = \Input::get('fecha_registro');
                $motivo = \Input::get('motivo');
                $submotivo = \Input::get('submotivo');
                $gestion = \Input::get('gestion');

                if(\Input::get('eventos')){
                    $q->whereIn('pre_temporales.evento_masiva_id', \Input::get('eventos'));                
                }

                if(\Input::has('asignacion')){
                    if(\Input::get('asignacion') == 1){
                        $q->whereRaw('(pre_temporales.usuario_id != 0 and pre_temporales.usuario_id!="" and pre_temporales.usuario_id IS NOT NULL)');                
                    }elseif(\Input::get('asignacion') == 2){
                        $q->whereRaw('(pre_temporales.usuario_id = 0 or pre_temporales.usuario_id="" or pre_temporales.usuario_id IS NULL)');
                    }
                }

                if (count($tipo_averia) != 0) {
                    $q->whereIn('at.apocope', $tipo_averia);
                }
                if (count($atencion) != 0) {
                    $q->whereIn('pre_temporales.tipo_atencion', $atencion);
                }
                if (count($estadoPre) != 0) {
                    $q->whereIn('pre_temporales.estado_pretemporal_id', $estadoPre);
                }
                if (count($estado) != 0) {
                    $q->whereIn('ptu.estado_id', $estado);
                }
                if (count($motivo) != 0) {
                    $q->whereIn('ptu.motivo_id', $motivo);
                }
                if (count($submotivo) != 0) {
                    $q->whereIn('ptu.submotivo_id', $submotivo);
                }
                if (count($gestion) != 0) {
                    $q->whereIn('pre_temporales.gestion', $gestion);
                }
                if (count($quiebre) != 0) {
                    $q->whereIn('pre_temporales.quiebre_id', $quiebre);
                } else {

                }

                if (count($fechaRegistro) != 0) {
                    $fecha = explode(" - ", $fechaRegistro);
                    if (count($fecha) > 0) {
                        $startDate = date('Y-m-d  00:00:00', strtotime($fecha[0]));
                        $endDate = date('Y-m-d  23:59:59', strtotime($fecha[1]));
                        $q->whereBetween(
                            'pre_temporales.created_at',
                            array($fecha[0]. " 00:00:00",
                                    $fecha[1]. " 23:59:59")
                        );
                    }
                }
            }                
        });     

        if(\Input::has('excel')){
            return $query->get();
        }


        $perPage = \Input::has('per_page') ? (int)\Input::get('per_page') : null;
        $result = $query->paginate($perPage);       
        $data = $result->toArray();   
        $col = new Collection([
            'recordsTotal'=>$result->getTotal(),
            'recordsFiltered'=>$result->getTotal()
        ]);
        return $col->merge($data);
    }
	

    public function Visorcot()
    {
        $estadoId = \Input::get('estado_id');
        $liquidado = '';
        $tipo = '';

        $query = \DB::table('pre_temporales_ultimos as ptu')
                    ->leftjoin('motivos as m', 'm.id', '=', 'ptu.motivo_id')//est
                    ->select( \DB::raw("COUNT(DISTINCT(ptu.submotivo_id)) as 'row_count',
                        m.nombre as 'motivo',
                        COUNT((ptu.submotivo_id)) as 'total_motivo',
                        ptu.motivo_id ") )
                    ->where( function($q) use ($estadoId) {
                        $q->where('ptu.estado_id', '=', $estadoId);
                        if($estadoId == 17){
                            $q->orwhere('ptu.motivo_id', '=', '22');
                        }
                        else {
                            $q->Where('ptu.motivo_id', '<>', '22');
                        }
                    });

        $motivo = $query->groupBy('ptu.motivo_id')
                       ->orderBy('m.created_at', 'asc')
                       ->get();

        $last_day = 8;
        $pre_last_day = $last_day -1;

        $q_pendientes = \DB::table('pre_temporales_ultimos as ptu ')
                            ->select( \DB::raw(" m.nombre,
                                ptu.motivo_id, 
                                ptu.submotivo_id,
                                s.nombre as submotivo,
                                count(pt0.id) as 'f0', 
                                count(pt1.id) as 'f1', 
                                count(pt2.id) as 'f2', 
                                count(pt3.id) as 'f3', 
                                count(pt4.id) as 'f4', 
                                count(pt5.id) as 'f5', 
                                count(pt6.id) as 'f6', 
                                count(pt7.id) as 'f7',
                                count(pt8.id) as 'total_submotivo'
                                "))
                            ->join('motivos as m', 'm.id', '=', 'ptu.motivo_id')
                            ->join('submotivos as s', 's.id', '=', 'ptu.submotivo_id')
                            ->join('pre_temporales as pt', 'pt.id', '=', 'ptu.pre_temporal_id');

        $ahora = date('Y-m-d H:m:i');
        $last_day = 8;
        $pre_last_day = $last_day - 1;

        for ($i = 0; $i <= $last_day; $i++) {

            $fecha = new \DateTime($ahora);
            if ($i == $last_day) {

                $fecha->modify("-{$pre_last_day} days");
                $fechas[0] = $fecha->format('Y-m-d').' 00:00:00';
                $fechas[1] = $ahora.' 23:59:59';
                
            } else {
                $fecha->modify("-{$i} days");
                $fechas[0] = $fecha->format('Y-m-d').' 00:00:00';
                $fechas[1] = $fecha->format('Y-m-d').' 23:59:59';
            }
            $q_pendientes->leftJoin("pre_temporales as pt{$i}", function($l_join) use ($fechas, $i){
                $l_join->on("pt{$i}.id", '=', "ptu.pre_temporal_id")
                    ->where("pt.created_at", '>=', $fechas[0])
                    ->where("pt.created_at", '<=', $fechas[1]);
                if (\Input::get('quiebre_id')) {
                    $l_join->where("pt$i.quiebre_id", '=', \Input::get('quiebre_id'));
                }
            });
        }

        $q_pendientes->where( function($q) use ($estadoId) {
            $q->where('ptu.estado_id', $estadoId);
            if ($estadoId == 17) {
                $q->orWhere('ptu.motivo_id', '=', '22');
            }
            else {
                $q->where('ptu.motivo_id', '<>', '22');
            }
        })
        ->groupBy('ptu.motivo_id', 'ptu.submotivo_id')
        ->orderBy('m.created_at', 'asc');

        $submotivo = $q_pendientes->get();

        return array(
                'motivo' => $motivo,
                'submotivo' => $submotivo,
                'estado_id' => $estadoId,
            );       
    }

    public function visorRecepcionados(){
         $q_recepcionados = \DB::table('pre_temporales as pt ')
                             ->select( \DB::raw(" 'Recepcionados' as 'nombre',
                                count(pt0.id) as 'f0', 
                                count(pt1.id) as 'f1', 
                                count(pt2.id) as 'f2', 
                                count(pt3.id) as 'f3', 
                                count(pt4.id) as 'f4', 
                                count(pt5.id) as 'f5', 
                                count(pt6.id) as 'f6', 
                                count(pt7.id) as 'f7',
                                count(pt8.id) as 'total',
                                count(pt10m.id) as 'diez',
                                count(pt30m.id) as 'treinta',
                                count(pt60m.id) as 'sesenta'
                                ") );

        //$dia = date('Y-m-d');
        $ahora = date('Y-m-d H:m:i');
        $last_day = 8;
        $pre_last_day = $last_day -1;             
        for ($i = 0; $i <= $last_day; $i++) {

            $fecha = new \DateTime($ahora);
            if ($i == $last_day) {
                $fecha->modify("-{$pre_last_day} days");
                $fechas[0] = $fecha->format('Y-m-d').' 00:00:00';
                $fechas[1] = $ahora.' 23:59:59';
            } else {
                $fecha->modify("-{$i} days");
                $fechas[0] = $fecha->format('Y-m-d').' 00:00:00';
                $fechas[1] = $fecha->format('Y-m-d').' 23:59:59';
            }

            $q_recepcionados->leftJoin("pre_temporales as pt{$i}", function($l_join) use ($fechas, $i){
                $l_join->on("pt.id", '=', "pt{$i}.id")
                     ->where("pt$i.created_at", '>=', $fechas[0])
                     ->where("pt$i.created_at", '<=', $fechas[1]);
            });
        }
        $minutos = ['10', '30', '60'];
        foreach ($minutos as $minuto) {
            $f_minuto_inicio = new \DateTime($ahora);
            $f_minuto_final = new \DateTime($ahora);

            switch ($minuto) {
                case '10':
                    $f_minutos[0] = $f_minuto_inicio->format('Y-m-d H:m:i');
                    $f_minuto_final->modify("-10 minutes");
                    $f_minutos[1] = $f_minuto_final->format('Y-m-d H:m:i');
                    break;
                case '30':
                    $f_minuto_inicio->modify("-10 minutes");
                    $f_minutos[0] = $f_minuto_inicio->format('Y-m-d H:m:i');
                    $f_minuto_final->modify("-30 minutes");
                    $f_minutos[1] = $f_minuto_final->format('Y-m-d H:m:i');
                    break;
                default:
                    $f_minuto_inicio->modify("-60 minutes");
                    $f_minutos[0] = $f_minuto_inicio->format('Y-m-d H:m:i');
                    $f_minutos[1] = $f_minuto_final->format('Y-m-d').' 00:00:00';
                    break;
            }

            $q_recepcionados->leftJoin("pre_temporales as pt{$minuto}m", function($l_join) use ($f_minutos, $minuto){
                $l_join->on("pt.id", '=', "pt{$minuto}m.id")
                       ->where("pt{$minuto}m.created_at", '<=', $f_minutos[0]) 
                       ->where("pt{$minuto}m.created_at", '>=', $f_minutos[1]);
            });
        }
        

        $q_recepcionados->where('pt.estado_pretemporal_id', 1);
        if(\Input::get('quiebre_id')){
            $q_recepcionados->where('pt.quiebre_id', \Input::get('quiebre_id'));
        }

        $data = $q_recepcionados->get();
        $horas['diez'] = $data[0]->diez;
        $horas['treinta'] = $data[0]->treinta;
        $horas['sesenta'] = $data[0]->sesenta;
        
        return array(
            'recepcionados' => $data,
            'recepcionados_horas' => $horas
        ); 
    }


    /*modify queries*/
     /**
     * aver_pen_bas_lima
     */
    public function getAveriaPenBasLima( $codactu='', $codcli='', $type=1 )
    {
        $result= array();
        //7. Aver. Pen. TBA Lima (schedulle_sistemas.aver_pen_bas_lima)
        $where = "numero_osiptel=?";
        if($codcli){
            $where.=" or telefono=?";
        }
        if ($type == 1 && $codcli) {
            $where = str_replace("or", "and", $where);
        }
        $sql = "SELECT 
                'Pendiente' as estado,
                numero_osiptel as codactu, 
                telefono,
                telefono as codcli, 
                codigo_distrito as distrito,
                CONCAT(
                    ltrim(rtrim(ape_paterno)) ,
                    ' ',
                    ltrim(rtrim(ape_materno)) ,
                    ' ',ltrim(rtrim(nombre))
                    ) as nombre_cliente, 
                observacion_102 as observacion,
                contrata, 
                area_sig as area, 
                zonal_lima as zonal,
                mdf,
                fecha_reporte as fecha_registro
                FROM schedulle_sistemas.aver_pen_bas_lima
                WHERE $where ";

        $data = ($codcli) ? array( trim($codactu), trim($codcli) ) : array( trim($codactu) );
        $result = \DB::select($sql, $data);

        return $result;
    }
    /**
     * aver_liq_bas_lima
     */
    public function getAveriaLiqBasLima( $codactu='' )
    {
        $sql = "SELECT  
                numero_osiptel as codactu,
                telefono as codcli, 
                liquidacion_ as codigo_liquidacion,
                detalle as detalle_liquidacion, 
                otra_observacion as observacion,
                fecha_de_liquidacion as fecha_liquidacion,
                CONCAT(
                    ltrim(rtrim(ape_paterno)),
                    ' ',
                    ltrim(rtrim(ape_materno)),
                    ' ',
                    ltrim(rtrim(nombre))
                ) AS nombre_cliente,
                contrata,
                area_sig as area,
                zonal_lima as zonal,
                mdf,
                fecha_registro
                FROM schedulle_sistemas.aver_liq_bas_lima
                WHERE  numero_osiptel=? ";

        $result = \DB::select($sql, array( trim($codactu) ));
        return $result;
    }

    /**
     * aver_pen_bas_prov
     */
    public function getAveriaPenBasProv( $codactu='', $codcli='', $type=1 )
    {
        $result= array();
        //8. Aver. Pen. TBA Provincia (schedulle_sistemas.aver_pen_bas_prov)
        $where = "correlativo=? or telefono=?";
        if ($type == 1) {
            $where = "correlativo=? and telefono=?";
        }
        if ($codactu =='') {
            $where = "telefono=?";
        }
        $sql = "SELECT  
                'Pendiente' AS estado,
                correlativo AS codactu,
                telefono AS codcli,
                telefono,
                codigodistrito AS distrito,
                CONCAT(
                    ltrim(rtrim(ape_paterno)),
                    ' ',
                    ltrim(rtrim(ape_materno)),
                    ' ',
                    ltrim(rtrim(nombre))
                ) AS nombre_cliente,
                comentariodeboletin as observacion,
                contrata,
                '' as area,
                '' AS zonal,
                mdf,
                fechahoraboleta AS fecha_registro
                FROM schedulle_sistemas.aver_pen_bas_prov
                WHERE $where ";
        if ( trim($codactu) != '') 
            $result = \DB::select($sql, array( trim($codactu), trim($codcli) ));
        else 
            $result = \DB::select($sql, array( trim($codcli) ));
        

        return $result;
    }
    /**
     * aver_liq_bas_prov_pedidos
     */
    public function getAveriaLiqBasProv( $codactu= '' )
    {
        $sql = "SELECT 
                correlativo as codactu,
                telefono AS codcli,
                codigo_de_liquidacion as codigo_liquidacion, 
                codigo_detalle_liq as detalle_liquidacion,
                desc_detalle_liq as observacion,
                fecha_de_sistema as fecha_liquidacion,
                CONCAT(
                ltrim(rtrim(ape_paterno)),
                ' ',
                ltrim(rtrim(ape_materno)),
                ' ',
                ltrim(rtrim(nombre))
                ) AS nombre_cliente,
                contrata,
                '' as area,
                '' as zonal,
                mdf,
                fecha_hora_boleta as fecha_registro
                FROM schedulle_sistemas.aver_liq_bas_prov_pedidos 
                WHERE correlativo=? ";

        $result = \DB::select($sql, array( trim($codactu) ));

        return $result;

    }
    /**
     * aver_pen_adsl_pais
     */
    public function getAveriaPenAdslPais( $codactu='', $codcli='', $type=1 )
    {
        $result= array();
        //9. Aver. Pen. ADSL (schedulle_sistemas.aver_pen_adsl_pais)
        $where = "numero_osiptel=?";
        if($codcli){
            $where.=" or CAST((concat(zona_telefonica, telefono)) as SIGNED) =?";
        }
        if ($type == 1 && $codcli) {
            $where = str_replace("or", "and", $where);
        }

        $sql = "SELECT 
                'Pendiente' AS estado,
                numero_osiptel AS codactu,
                telefono,
                CAST(
                    (
                        concat(zona_telefonica, telefono)
                    ) AS SIGNED
                ) AS codcli,
                CONCAT(
                ltrim(rtrim(ape_paterno)),
                ' ',
                ltrim(rtrim(ape_materno)),
                ' ',
                ltrim(rtrim(nombre))
                ) AS nombre_cliente,
                '' as observacion,
                contrata,
                estado_ult as area,
                zonal,
                mdf,
                fecha_registro
                FROM schedulle_sistemas.aver_pen_adsl_pais
                WHERE $where ";

        $data = ($codcli) ? array( trim($codactu), trim($codcli) ) : array( trim($codactu) );
        $result = \DB::select($sql, $data);

        return $result;
    }
    /**
     * aver_liq_adsl_pais
     */
    public function getAveriaLiqAdslPais( $codactu='' )
    {
        $sql = "SELECT 
                numero_osiptel as codactu,
                CAST(
                    (
                        concat(zona_telefonica, telefono)
                    ) AS SIGNED
                ) AS codcli,
                codigo_liquidacion,
                detalle as detalle_liquidacion,
                observacion_liquidacion as observacion,
                fecha_liquidacion_ as fecha_liquidacion,
                CONCAT(
                    ltrim(rtrim(ape_paterno)),
                    ' ',
                    ltrim(rtrim(ape_materno)),
                    ' ',
                    ltrim(rtrim(nombre))
                ) AS nombre_cliente,
                contrata,
                estado_liq as area,
                zonal,
                mdf,
                fecha_registro
                FROM schedulle_sistemas.aver_liq_adsl_pais
                WHERE numero_osiptel=? ";

        $result = \DB::select($sql, array( trim($codactu) ));

        return $result;
    }
    /**
     * aver_pen_catv_pais
     */
    public function getAveriaPenCatvPais( $codactu='', $codcli='', $type=1 )
    {
        $result= array();
        //10. Aver. Pen. CATV (schedulle_sistemas.aver_pen_catv_pais)
        $where = "codigo_req=? or codigodelcliente=?";
        if ($type == 1) {
            $where = "codigo_req=? and codigodelcliente=?";
        }
        $sql = "SELECT 
                    'Pendiente' AS estado,
                    codigo_req AS codactu,
                    codigodelcliente AS codcli,
                    codigodelcliente AS telefono,
                    CONCAT(
                        ltrim(rtrim(apellidopaterno)),
                        ' ',
                        ltrim(rtrim(apellidomaterno)),
                        ' ',
                        ltrim(rtrim(nombres))
                    ) AS nombre_cliente,
                    desc_motivo as observacion,
                    contrata,
                    estacion AS area,
                    '' AS zonal,
                    CONCAT(nodo,'/',troba,'/',tap,'/',borne) as mdf,
                    fecharegistro AS fecha_registro
                FROM schedulle_sistemas.aver_pen_catv_pais
                WHERE $where ";

        $result = \DB::select($sql, array( trim($codactu), trim($codcli) ));

        return $result;
    }
    /**
    * aver_liq_catv_pais
    */
    public function getAveriaLiqCatvPais( $codactu='', $codcli='', $type=1 )
    {
        $where = "codigoreq=?";
        if($codcli){
            $where.=" or codigodelcliente=?";
        }
        if($type == 1 && $codcli){
             $where = str_replace("or", "and", $where);
        }
        $sql = "SELECT 
                    codigoreq as codactu, 
                    codigodelcliente as codcli,
                    codigodeliquidacion as codigo_liquidacion, 
                    detalle_liquidacion,
                    '' as observacion,
                    fecha_liquidacion,
                    CONCAT(
                            ltrim(rtrim(apellidopaterno)),
                            ' ',
                            ltrim(rtrim(apellidomaterno)),
                            ' ',
                            ltrim(rtrim(nombres))
                    ) AS nombre_cliente,
                    contrata, 
                    estacion as area, 
                    '' as zonal,
                    CONCAT(nodo,'/',troba,'/',tap,'/',borne) as mdf,
                    fecharegistro as fecha_registro
                FROM schedulle_sistemas.aver_liq_catv_pais
                WHERE $where ";

        $data = ($codcli) ? array( trim($codactu), trim($codcli) ) : array( trim($codactu) );
        $result = \DB::select($sql, $data);

        return $result;
    }

     /**
    * req_pend_macro (rutinas para internet y television)
    */
    public function getAveriaPendMacro( $codactu='', $codcli='', $type=1 )
    {   
        $where = "codreq=?";
        if($codcli){
            $where.=" or codcli=?";
        }
        if($type == 1 && $codcli){
             $where = str_replace("or", "and", $where);
        }
        $sql = "SELECT 
                    'Pendiente' AS estado,
                    codreq as codactu, 
                    codcli,
                    nomcli AS nombre_cliente,
                    desmotv as observacion,
                    desnomctr as contrata,
                    area,
                    '' AS zonal,
                    CONCAT(codnod,'/',nroplano,'/',codtap,'/',codbor) as mdf,
                    fec_registro AS fecha_registro
                FROM webpsi_coc.req_pend_macro
                WHERE $where ";

        $data = ($codcli) ? array( trim($codactu), trim($codcli) ) : array( trim($codactu) );
        $result = \DB::select($sql, $data);

        return $result;
    }



    /* */
    public function buscarTicket($ticket) 
    {              
        if($ticket){
            $datos = \DB::table('pre_temporales as p')
                    ->select(
                        "p.id",
                        "p.codactu",
                        "p.codcli",
                        "p.cliente_nombre",
                        "p.cliente_celular",
                        "p.cliente_telefono",
                        "p.cliente_correo",
                        "p.cliente_dni",
                        "p.contacto_nombre",
                        "p.contacto_celular",
                        "p.contacto_telefono",
                        "p.contacto_correo",
                        "p.contacto_dni",
                        "p.embajador_nombre",
                        "p.embajador_celular",
                        "p.embajador_correo",
                        "p.embajador_dni",
                        "p.comentario",
                        "p.created_at",
                        "p.validado",
                        "p.estado_pretemporal_id",
                        "p.actividad_tipo_id",
                        "p.evento_masiva_id",
                        "p.gestion",
                        'p.quiebre_id',
                        \DB::raw('IFNULL(em.nombre,"") as evento'),
                        \DB::raw("CONCAT('E',p.id) as ticket"),
                        'at.apocope as tipo_averia',
                        'ep.nombre as estado',
                        'pa.nombre as atencion',
                        'q.nombre as quiebre',
                        \DB::raw('IFNULL(e.id,"") as estado_id'),
                        \DB::raw('IFNULL(pf.nombre,"") as fuente'),
                        'pta.estado_legado','pta.fecha_registro_legado',
                        'pta.nombre_cliente','pta.zonal','pta.contrata','pta.area',
                        'pta.mdf','pta.observacion','pta.fecha_liquidacion',
                        'pta.cod_liquidacion','pta.detalle_liquidacion',
                        \DB::raw('IFNULL(p.fh_reg104," ") as fh_reg104'),
                        \DB::raw('IFNULL(p.fh_reg1l," ") as fh_reg1l'),
                        \DB::raw('IFNULL(p.fh_reg2l," ") as fh_reg2l'),
                        \DB::raw('IFNULL(p.cod_multigestion," ") as cod_multigestion'),
                        \DB::raw('IFNULL(p.llamador," ") as llamador'),
                        \DB::raw('IFNULL(p.titular," ") as titular'),
                        \DB::raw('IFNULL(p.direccion," ") as direccion'),
                        \DB::raw('IFNULL(p.distrito," ") as distrito'),
                        \DB::raw('IFNULL(p.urbanizacion," ") as urbanizacion'),
                        \DB::raw('IFNULL(p.telf_gestion," ") as telf_gestion'),
                        \DB::raw('IFNULL(p.telf_entrante," ") as telf_entrante'),
                        \DB::raw('IFNULL(p.operador," ") as operador'),
                        \DB::raw('IFNULL(p.motivo_call," ") as motivo_call')             
                    )
                    ->join('actividades_tipos as at', 'at.id', '=', 'p.actividad_tipo_id')
                    ->join('quiebres as q', 'q.id', '=', 'p.quiebre_id')
                    ->join('estados_pre_temporales as ep', 'ep.id', '=', 'p.estado_pretemporal_id')
                    ->Leftjoin('pre_temporales_ultimos as ptu', 'ptu.pre_temporal_id', '=', 'p.id')
                    ->Leftjoin('estados as e', 'ptu.estado_id', '=', 'e.id')
                    ->join('pre_temporales_atencion as pa', 'pa.id', '=', 'p.tipo_atencion')
                    ->Leftjoin('pre_temporales_averias as pta', 'p.id', '=', 'pta.pre_temporal_id')
                    ->Leftjoin('pre_temporales_rrss as pf', 'pf.id', '=', 'p.tipo_fuente')
                    ->Leftjoin('eventos_masiva as em','em.id','=','p.evento_masiva_id')
                    ->where('p.id', $ticket)
                    ->get();

            return (isset($datos[0])) ? $datos[0] : false;
        }
        return false;   
    }

    public function getbyCodact($codact){
        $rst = DB::table('pre_temporales as p')
                    ->where('p.codactu', $busqueda)
                    ->select('p.*, at.nombre')
                    ->join('actividades_tipos as at','at.id','=','p.actividad_tipo_id')
                    ->get();
        return ($rst) ? $rst : false;
    }

    public function getReporteMov()
    {
        $filtros = [];
        $filtro = \Input::get("slct_filtro", 1);
        $fecha = explode(" - ", \Input::get("fecha_registro"));
        $ticket = \Input::get("numticket");
        $dniCliente = \Input::get("dni_cliente");
        $telefonoCliente = \Input::get("telefono_cliente");
        $fecha[0].=" 00:00:00";
        $fecha[1].=" 23:59:59";
        $flag = \Input::get("flag", 1);
        $data = [];

        if ($filtro == 1) {
            $tipo_filtro = "p.created_at";
        } else {
            $tipo_filtro = "ptm.created_at";
        }
        //case: 1 por fecha de ticket
        switch ($flag) {
            case 1:
                $data = \DB::table("pre_temporales as pt")
                ->select(
                    [
                    \DB::raw('CONCAT("E",p.id) as ticket'),
                    "pt.tipo_atencion", "a.nombre as actividad",
                    "pt.tipo_fuente", "pt.gestion",
                    "pt.codactu", "pt.codcli",                   
                    "pt.cliente_nombre", "pt.cliente_celular",
                    "pt.cliente_telefono", "pt.cliente_correo",
                    "pt.cliente_dni", "pt.contacto_nombre",
                    "pt.contacto_celular", "pt.contacto_telefono",
                    "pt.contacto_correo", "pt.contacto_dni",
                    "pt.embajador_nombre", "pt.embajador_correo",
                    "pt.embajador_celular", "pt.embajador_dni",
                    "pt.comentario", "q.nombre as quiebre",
                    "ept.nombre as estado",
                     \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                     'em.nombre as evento_masiva'
                    ]
                )
                ->leftJoin("quiebres as q", "q.id", "=", "pt.quiebre_id")
                ->leftJoin(
                    "estados_pre_temporales as ept",
                    "ept.id",
                    "=",
                    "pt.estado_pretemporal_id"
                )
                ->leftJoin("actividades as a", "a.id", "=", "pt.actividad_id")
                ->leftJoin("eventos_masiva as em", "em.id", "=", "pt.evento_masiva_id")
                ->whereBetween("pt.created_at", $fecha);

                if ($ticket!=="") {
                    $array = explode("E", $ticket);
                    if (count($array)>1) {
                        $ticketId = $array[1];
                        $data->where('pt.id', '=', (int)$ticketId);
                    }
                }
                if ($dniCliente!=="") {
                    $data->where("pt.cliente_dni", "LIKE", "%".$dniCliente."%");
                }
                if ($telefonoCliente!="") {
                    $data->where(
                        "pt.cliente_telefono",
                        "LIKE",
                        "%".$telefonoCliente."%"
                    );
                }

                if(\Input::get('slct_quiebre')){
                    $data->where('pt.quiebre_id', '=', \Input::get('slct_quiebre'));
                }
                $data = $data->get();
                $nameFile = "Tickets";
                break;
            case 2:
                $data = \DB::table('pre_temporales as p')
                ->select(
                    \DB::raw('CONCAT("E",p.id) as ticket'),
                    'at.apocope as tipo_averia',
                    'pa.nombre as tipo_atencion',
                    'q.nombre as quiebre',
                    "ept.nombre as EstadoTicket",
                    'p.codactu as CodAveria',
                    'p.codcli',
                    'p.cliente_nombre as ClienteNombre',
                    'p.cliente_celular as ClienteCelular',
                    'p.cliente_telefono as ClienteTelefono',
                    'p.cliente_dni as ClienteDNI',
                    'p.embajador_nombre as EmbajadorNombre',
                    'p.embajador_celular as EmbajadorCelular',
                    'p.embajador_correo as EmbajadorCorreo',
                    'p.embajador_dni as EmbajadorDNI',
                    //'p.comentario as ComentarioTicket',
                    \DB::raw("TRIM(REPLACE(REPLACE(REPLACE(REPLACE(p.comentario,CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),'/',' ')) as ComentarioTicket"),
                    \DB::raw(
                        '
                            DATE_FORMAT(p.created_at,"%d-%m-%y %H:%i:%s") 
                            as FechaRegistro'
                    ),
                    \DB::raw(
                        '
                            DATE_FORMAT(p.created_at,"%d-%m-%y") 
                            as FechaRegistroTicket'
                    ),
                    \DB::raw(
                        '
                            DATE_FORMAT(p.created_at,"%H:%i:%s") 
                            as HoraRegistroTicket'
                    ),
                    \DB::raw('IFNULL(pts.accion," ") as TipoSolucion'),                              
                    \DB::raw('IFNULL(m.nombre," ") as Motivos'),
                    \DB::raw('IFNULL(s.nombre," ") as Submotivos'),
                    \DB::raw('IFNULL(e.nombre,"Recepcionado") as Estados'),
                    'ptm.created_at as FechaMov',
                    'ptm.observacion as ObsMov',
                    \DB::raw('CONCAT(u.nombre, " ",u.apellido) as UsuarioMov'),
                    'u.usuario as login',
                    'pts.producto as Producto',
                    'pts.accion as Accion',
                    'pta.estado_legado as EstadoLegado',
                    'pta.fecha_registro_legado as FechaRegistroLegado',
                    'pta.fecha_liquidacion as FechaLiquidacion',
                    'pta.cod_liquidacion as CodigoLiquidacion',
                    'pta.detalle_liquidacion as DetalleLiquidacion',
                    'pta.area as Area',
                    'pta.contrata as Contrata',
                    'pta.zonal as Zonal',
                    'p.fh_reg104',
                    'p.fh_reg1l',
                    'p.fh_reg2l',
                    'p.cod_multigestion',
                    'p.llamador',
                    'p.titular',
                    'p.direccion',
                    'p.distrito',
                    'p.urbanizacion',
                    'p.telf_gestion',
                    'p.telf_entrante',
                    'p.operador',
                    'p.motivo_call',                   
                    \DB::raw(
                        '(CASE ptm.id WHEN (
                            SELECT MAX(ptm2.id)
                            FROM pre_temporales_movimientos as ptm2
                            WHERE ptm2.pre_temporal_id = ptm.pre_temporal_id
                        )
                        THEN "X" ELSE "" END) as ultimo_movimiento'
                    ),
                    'em.nombre as evento_masiva',
                    \DB::raw('IFNULL(CONCAT_WS(" ",u1.nombre,u1.apellido)," ") as asignado'),
                    'g.nombre as grupo'
                )
                ->leftjoin(
                    'pre_temporales_movimientos as ptm',
                    'ptm.pre_temporal_id',
                    '=',
                    'p.id'
                )
                ->join(
                    'actividades_tipos as at',
                    'p.actividad_tipo_id',
                    '=',
                    'at.id'
                )
                ->join('usuarios as u', 'ptm.usuario_created_at', '=', 'u.id')
                ->leftjoin('usuario_area as ua', 'u.id', '=', 'ua.usuario_id')
                ->leftJoin('grupos as g', 'ua.grupo_id', '=', 'g.id')

                ->Leftjoin('quiebres as q', 'p.quiebre_id', '=', 'q.id')
                ->leftjoin('motivos as m', 'm.id', '=', 'ptm.motivo_id')
                ->leftjoin('submotivos as s', 's.id', '=', 'ptm.submotivo_id')
                ->leftjoin('estados as e', 'e.id', '=', 'ptm.estado_id')
                ->leftjoin('usuarios as u1', 'u1.id', '=', 'p.usuario_id')
                ->leftjoin(
                    'estados_pre_temporales as ept', 'ept.id',
                    '=',
                    'ptm.estado_pretemporal_id'
                )
                ->leftjoin(
                    'pre_temporales_solucionado as pts',
                    'pts.id', '=',
                    'ptm.pre_temporal_solucionado_id'
                )
                ->join(
                    'pre_temporales_atencion as pa',
                    'pa.id',
                    '=',
                    'p.tipo_atencion'
                )
                ->Leftjoin(
                    'pre_temporales_averias as pta',
                    'pta.pre_temporal_id', '=',
                    'p.id'
                )
                ->leftJoin("eventos_masiva as em", "em.id", "=", "p.evento_masiva_id")
                ->where(
                    function($query){
                    if(\Input::get('slct_quiebre')){
                        $query->whereIn('p.quiebre_id', \Input::get('slct_quiebre'));
                    }
                    if(\Input::get('slct_motivo')){
                        $query->whereIn('ptm.motivo_id', \Input::get('slct_motivo'));
                    }
                    if(\Input::get('slct_submotivo')){
                        $query->whereIn('ptm.submotivo_id', \Input::get('slct_submotivo'));
                    }
                    }
                )
                ->whereBetween($tipo_filtro, $fecha)
                ->get();
                
                $nameFile = "Movimientos";
                break;
            default:
                break;
        }
        return ($data) ? $data : false;
    }

    public function getfieldsAdmin(){
        $data = \DB::table('pre_temporales_admin as pta')
                ->select('pta.id', 'pta.nombre', 'pta.cantidad', 'pta.obligatorio')
                ->get();
        return ($data) ? $data : false;
    }

    public function calcCumpleEvento($evento_id)
    {
        if($evento_id){
            $query = "SELECT  COUNT(*) as numTickets,COUNT(ptu.id) as numListos,COUNT(ptu1.id) as numFaltantes FROM pre_temporales pt 
                    LEFT JOIN pre_temporales_ultimos ptu ON ptu.pre_temporal_id=pt.id AND ptu.estado_id=17
                    LEFT JOIN pre_temporales_ultimos ptu1 ON ptu1.pre_temporal_id=pt.id AND ptu1.estado_id <> 17
                    WHERE pt.evento_masiva_id =".$evento_id;
            $result  = \DB::select($query)[0];

            if($result){
                $num_tickets = (int)$result->numTickets;
                $listos = (float)$result->numListos;
                $cant_req = $num_tickets * 0.2;

                if($listos > 0){
                    $evento = EventoMasiva::find($evento_id);
                    $evento->estado=11;
                    $evento->save();
                    return 1;   
                }

                if($cant_req <= $listos){
                    $evento = EventoMasiva::find($evento_id);
                    $evento->estado=2;
                    $evento->save();
                    return 1;
                }
            }
        }
        return 0;
    }

    public function getMasivas()
    {
        $nodo = \Input::get('nodo');
        $troba = \Input::get('troba');

        $query = "SELECT '55' AS quiebre_id,'21' AS actividad_tipo_id,'18' AS tipo_atencion,'1' AS tipo_fuente,m.codreq AS codactu,m.codcli as codcli,m.nomcli AS cliente_nombre,c.`movil_1` AS cliente_celular,c.`telecl_1` AS cliente_telefono,'' AS cliente_correo,''AS cliente_dni,c.movil_2 AS contacto_celular, 
            c.`telecl_2` AS contacto_telefono, '' AS contacto_correo, '' AS contacto_dni,'' AS contacto_nombre,'Usuario que registra evento' AS embajador_nombre,'' AS embajador_correo,
            'celular de usuario' AS embajador_celular, 'dni usuario' AS embajador_dni, c.`telecl_3` AS comentario  
            FROM webpsi_coc.req_pend_macro  m 
            LEFT JOIN psi.contacto_cms  c ON   m.`codcli` = c.`cliente`  AND   m.`codsrv`  =  c.`servicio` 
            WHERE m.codnod=? AND m.nroplano=?";
            $result = \DB::select($query, [$nodo,$troba]);
        return ($result) ? $result : false;
    }
}
