<?php 
namespace Repositories;
use Abstracts\Repository as AbstractRepository;
use DB;
class EnvioMasivoOfscRepository extends AbstractRepository implements EnvioMasivoOfscRepositoryInterface
{
    // This is where the "magic" comes from:
    protected $modelClassName = 'EnvioMasivoOfsc';
    
    // This class only implements methods specific to the UserRepository
    public function findEnvioMasivoOfscByColumn($column)
    {
        $where = call_user_func_array(
            "{$this->modelClassName}::where", 
            array($column)
        );
        return $where->get();
    }
    public function newEnvioMasivoOfsc()
    {
        return new \EnvioMasivoOfsc();
    }

    public function findOrFail($id)
    {
      return \EnvioMasivoOfsc::findOrFail($id);
    }
    
    public function getRows( $datos,$column, $dir, $start, $length)
    {
        
        $result['datos'] = \EnvioMasivoOfsc::orderBy($column, $dir)
        ->leftjoin(
            'actividades_tipos as a', 
            'tipo_actividad_id', '=', 'a.id'
        )->leftjoin(
            'actividades as act', 
            'act.id', '=', 'a.actividad_id'
        )->leftjoin(
            'envio_masivo_ofsc_grupo as e', 
            'envio_masivo_ofsc_grupo_id', '=', 'e.id'
        );
       
        if (isset($datos["buscar"])) {
             //personalizado
            if ($datos["buscar"]!=="") {
                 if (isset($datos['tipo'])) {
                    if ($datos['tipo'] == 'codactu') {
                        $result['datos'] = $result['datos']
                            ->where($datos['tipo'], $datos['buscar']);
                    } else if ($datos['tipo'] == 'aid') {
                        $result['datos'] = $result['datos']
                            ->where($datos['tipo'], $datos['buscar']);
                    } 
                } else {
                    $result["datos"] = $result["datos"]->where("codactu", "=", $datos["buscar"]);
                }
            }  
        } else if (isset($datos["codactuacion"]) && $datos["codactuacion"]!=="") {
            if ($datos["codactuacion"] !== "") {
                $result["datos"] = $result["datos"]->where("codactu", "=", $datos["codactuacion"]);
            }
        }
        else {
            //general
            
             if (isset($datos['fecha_created_at']) 
                    && $datos['fecha_created_at'] != '') {
                    $fechas = explode(' - ', $datos['fecha_created_at']);
                    $fechas[1].=" 23:59:59";
                    $result['datos'] = $result['datos']->whereBetween(
                        'envio_masivo_ofsc.created_at', $fechas
                    );
                }
            if (isset($datos['estado']) && $datos['estado'] != '') {
                $result['datos'] = $result['datos']
                        ->where('envio_masivo_ofsc.estado', $datos['estado']);
            }
            if (isset($datos['estado_masivo']) && $datos['estado_masivo'] != '') {
                $result['datos'] = $result['datos']
                        ->where('envio_masivo_ofsc.estado', $datos['estado_masivo']);
            }

            if (isset($datos['mdf'])) {
                $nodos = array ();
                foreach ($datos["mdf"] as $key => $value) {
                   $nodos[] = substr($value, 0, 2);
                }
                $result['datos'] = $result['datos']->whereIn('mdf', $nodos);
            }

            if (isset($datos['nodo_masivo'])) {
                $nodo = $datos['nodo_masivo'];
                if (isset($datos["download_excel"])) {
                    if ($nodo !=="null")
                        $result["datos"] = $result["datos"]->whereRaw(" mdf IN ('".$nodo."') ");
                } else {
                    $result['datos'] = $result['datos']->whereIn('mdf', $nodo);
                }
            }

            if (isset($datos['actividad'])) {
                $actividad = $datos['actividad'];
                if (isset($datos["download_excel"])) {
                    if ($actividad!=="null")
                        $result['datos'] = $result['datos']
                            ->whereRaw("act.id IN (".$actividad.") ");
                } else {
                    $result['datos'] = $result['datos']
                        ->whereIn('act.id', $actividad);
                }
                if (isset($datos['actividad_tipo'])) {
                    if (isset($datos["download_excel"])) {
                        $actividad_tipo = $datos["actividad_tipo"];
                        if ($actividad_tipo!=="null") {
                            $result["datos"] = $result["datos"]->whereRaw("a.id IN ( ".$actividad_tipo.") ");
                        }
                    } else {
                        $result['datos'] = $result['datos']
                        ->whereIn('tipo_actividad_id', $datos['actividad_tipo']);
                    }
                }
            }
            
            if (isset($datos['grupo'])) {
               
                $result['datos'] = $result['datos']
                        ->whereIn('envio_masivo_ofsc_grupo_id', $datos['grupo']);
            }

            if (isset($datos['envio_masivo_ofsc_grupo_id'])) {
               $envio_masivo_ofsc_grupo_id = $datos['envio_masivo_ofsc_grupo_id'];
               if (isset($datos["download_excel"])) {
                    if ($envio_masivo_ofsc_grupo_id !=="null")
                        $result['datos'] = $result['datos']
                                ->whereRaw('envio_masivo_ofsc_grupo_id  IN( '.$envio_masivo_ofsc_grupo_id.') ');
               } else {
                    $result['datos'] = $result['datos']
                        ->whereIn('envio_masivo_ofsc_grupo_id', $envio_masivo_ofsc_grupo_id);
               }
            }

            if (isset($datos['tipo_masivo'])) {
                if (strlen(trim($datos['tipo_masivo'])) > 0) {
                    $result['datos'] = $result['datos']
                        ->where('tipo_masivo', $datos['tipo_masivo']);
                }
                
            }
            if (isset($datos['empresa_masivo'])) {
                $result['datos'] = $result['datos']
                                            ->leftjoin("empresas as em", "envio_masivo_ofsc.empresa", "=", "em.nombre");
               $empresa = $datos['empresa_masivo'];

                if (isset($datos["download_excel"]) ) {
                    if ($empresa!=="null")
                        $result['datos'] = $result['datos']->
                                                    whereRaw(' em.id IN ('.$empresa.') ');
                } else {
                    $result['datos'] = $result['datos']
                        ->whereIn('em.id', $datos['empresa_masivo']);
                    }
            }

            if (isset($datos['actividad_masivo'])) {
               $actividad = $datos['actividad_masivo'];
                    $result['datos'] = $result['datos']->whereIn('act.id', $actividad);
                    if (isset($datos["actividad_tipo_masivo"])) {
                        $result["datos"] = $result["datos"]->whereIn('a.id', $datos["actividad_tipo_masivo"]);
                    }
            }
        }

        if (isset($datos["download_excel"]) && $length === 0) {
            $result['datos'] = $result['datos']
                ->select(
                    'codactu as Codigo_Actuación', 'mdf as Nodo', 'aid as AID', 
                    'e.nombre as Grupo_Envio', 
                    'a.nombre as Actividad Tipo','mensaje', 
                    DB::raw('( CASE envio_masivo_ofsc.estado WHEN 0 THEN "No Enviado" WHEN 1 THEN "Enviado" END ) as Estado '),  'act.nombre as actividad_id',
                    DB::raw('(CASE tipo_masivo WHEN "MasivoArtisan" THEN "Ordenes Temporales" 
                        WHEN "MasivoBandeja" THEN "Filtradas de Bandeja" 
                        WHEN "Componentes" THEN "Componentes de Ordenes" 
                        WHEN "MasivoLiquidado" THEN "Actividades Liquidadas" END) AS tipo_masivo'),
                    'envio_masivo_ofsc.empresa as empresa', 'envio_masivo_ofsc.created_at'
                );
        } else {
            $result['datos'] = $result['datos']
                ->select(
                    'envio_masivo_ofsc.id','codactu', 'mdf', 'aid', 
                    'e.nombre as grupo', 
                    'a.nombre as tipo_actividad_id','mensaje', 
                    'envio_masivo_ofsc.estado', 'envio_masivo_ofsc.created_at',
                    'act.nombre as actividad_id',
                    DB::raw('(CASE tipo_masivo WHEN "MasivoArtisan" THEN "Ordenes Temporales" 
                        WHEN "MasivoBandeja" THEN "Filtradas de Bandeja" 
                        WHEN "Componentes" THEN "Componentes de Ordenes" 
                        WHEN "MasivoLiquidado" THEN "Actividades Liquidadas" END) AS tipo_masivo'),
                    'envio_masivo_ofsc.empresa as empresa', 'envio_masivo_ofsc.numintentos'
                );
        }
        
        $result['total'] = count($result['datos']->get());
        if ($length === 0) {
            $result['datos'] = $result['datos']->get();
        } else {
            $result['datos'] = $result['datos']
                    ->limit($length)->offset($start)
                    ->get();
        }
        return $result;
    }
    public function getEnvioMasivoOfscs( $column, $dir, $start, $length)
    {
        $result = \EnvioMasivoOfsc::orderBy($column, $dir)
                  ->limit($length)->offset($start)->get();

        return $result;
    }

    public function getTotalRecordsFechas($fechas)
    {
        $fechas = explode(' - ', $fechas);
        $result = \EnvioMasivoOfsc::whereBetween('created_at', $fechas)
        ->count();
        return $result;
    }
    public function getTotalRecords()
    {
        $result = \EnvioMasivoOfsc::count();
        return $result;
    }
}
