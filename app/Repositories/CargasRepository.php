<?php namespace Repositories;

//use Cot\models\PreTemporal as PreTemporales;
//use Cot\models\PretemporalMovimiento as Movimiento;
//use Cot\models\PretemporalUltimo as Ultimo;
//use Repositories\CotHelpRepository as Helper;

class CargasRepository implements CargasRepositoryInterface
{
    public function getdataUpload_excel($columns) 
    {   
        $abc= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI',
            'BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
            'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS',
            'CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL',
            'DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ','EA','EB','EC','ED','EE',
            'EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX',
            'EY','EZ','FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ',
            'FR','FS','FT','FU','FV','FW','FX','FY','FZ','GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ',
            'GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ','HA','HB','HC',
            'HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV',
            'HW','HX','HY','HZ','IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO',
            'IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ','JA','JB','JC','JD','JE','JF','JG','JH',
            'JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ','KA',
            'KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT',
            'KU','KV','KW','KX','KY','KZ');

        $data = array();
        $cabecera = array();
        $file = \Input::file('excel');
        $tmpArchivo = $file->getRealPath();
        $name = $file->getClientOriginalName();
        $column = '';

        $origin_name = 'uploads/' . $name;
        $path = 'uploads/';
        $file->move($path, $name);

        $publicPath = strpos(public_path($path), '\\') === false?
            public_path($path) :
            str_replace('\\', '/', public_path($path));

        $objPHPExcel = \PHPExcel_IOFactory::load($publicPath.$name);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        //$highestColumn = $sheet->getHighestColumn();
        $highestColumn = $abc[$columns-1];

        $fila = $sheet->rangeToArray('A1'.':' . $highestColumn . $highestRow, '', TRUE, TRUE);
        $cabecera=strtolower(implode(',',$fila[0]));
        $cabecera=explode(',', $cabecera);
        $datos=[];
        for ($i=1; $i < count($fila); $i++) {
            $fila2=$fila[$i];
            for ($j=0; $j < count($fila2); $j++) { 
                $datos[$i][str_replace(' ','_',strtolower($fila[0][$j]))]=trim(strtoupper($this->
                sanear_string($fila[$i][$j])));
            }
        }
        
        $return=array('cabecera'=>$cabecera,'data'=>$datos);
        return $return;
    }

    public function getdataUpload_csv() 
    {    
        $data = array();
        $cabecera = array();
        $file = \Input::file('excel');
        $tmpArchivo = $file->getRealPath();
        $name = $file->getClientOriginalName();

        $path = 'uploads/';
        $file->move($path, $name);

        $rutaFile = public_path()."/uploads/".$name;
        $datos=array();
        if (file_exists($rutaFile)) {
            $fila = array_map('str_getcsv', file($rutaFile));

            $cabecera=strtolower(implode(',',$fila[0]));
            $cabecera=explode(',', $cabecera);
            $datos=[];
            for ($i=1; $i < count($fila); $i++) {
                $fila2=$fila[$i];
                for ($j=0; $j < count($fila2); $j++) { 
                    $datos[$i][str_replace(' ','_',strtolower($fila[0][$j]))]=trim(strtoupper($this->
                    sanear_string($fila[$i][$j])));
                }
            }                           
        }
              
        $return=array('cabecera'=>$cabecera,'data'=>$datos);
        return $return;
    }

    public function sanear_string($string)
    {
     
        $string = trim($string);
     
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );
     
        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );
     
        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );
     
        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );
     
        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );
     
        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );
     
        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "-", "~",
                 "#", "@", "|", "!", "'",
                 "·", "$", "%", "&", "/",
                 "(", ")", "?", "'", "¡",
                 "¿", "[", "^", "<code>", "]",
                 "+", "}", "{", "¨", "´",
                 ">", "< ", ";", ",", ":",
                 ".",'"',"*"),
            '',
            $string
        );
     
        return $string;
    }
}
