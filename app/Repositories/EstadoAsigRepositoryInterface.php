<?php namespace Repositories;

interface EstadoAsigRepositoryInterface
{
    public function listar();
    public function update();
    public function validate();
}


