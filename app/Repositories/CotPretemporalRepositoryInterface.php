<?php namespace Repositories;

interface CotPretemporalRepositoryInterface
{
	public function getLibrebyusers($usuarios = '',$quiebre_id = '',$tipo = '');
	public function ticketPendientes($quiebre_id,$type);
	public function buscarTicket($ticket);
	public function getbyCodact($codact);
	public function getReporteMov();
	public function Visorcot();
    public function visorRecepcionados();
    public function busquedaTicket();
}


