<?php

namespace Repositories;

/**
 * The UserRepositoryInterface contains ONLY method signatures for methods 
 * related to the User object.
 *
 * Note that we extend from RepositoryInterface, so any class that implements 
 * this interface must also provide all the standard eloquent methods 
 * (find, all, etc.).
 */
interface EnvioMasivoOfscRepositoryInterface extends RepositoryInterface
{
    public function findEnvioMasivoOfscByColumn($column);
    public function getRows($fechas,$column, $dir, $start, $length);
    public function getEnvioMasivoOfscs($column, $dir, $start, $length);
    public function getTotalRecordsFechas($fechas);
    public function getTotalRecords();
}
