<?php
namespace Repositories;

interface ListadoLegoRepositoryInterface
{

    public function motivosdevolucion($data);
    public function submotivosdevolucion($data);
    public function tematicos($data);
    public function operacion($data);
    public function zonal($data);
    public function requerimiento($data);
    public function motivo($data);
    public function empresa($data);
    public function tecnico($data);
    public function departamento($data);
    public function provincia($data);
    public function distrito($data);
    public function vip($data);
    public function jefatura($data);
    public function nodo($data);
    public function troba($data);
    public function geoamplificador($data);
    public function geotap($data);
    public function quiebre($data);
    public function estadoofsc($data);
    public function segmento($data);
    public function motivosliquidacion($data);
    public function contratas($data);
    public function contratascms($data);
    public function contratasgestel($data);
    public function areas($data);
    public function mdf($data);
    public function estadoaseguramiento($data);
    public function solicitudes($data);
    public function actividad($data);
    public function zonalPremium($data);
    public function nodoPremium($data);
    public function EnvioLegado_Host($data);
    public function EnvioLegado_Accion($data);
    public function perfil($data);
    public function modulo($data);
    public function tipopersona($data);
    public function motivogen($data);
    public function tiporeq($data);
    public function bucket($data);
    public function masiva($data);
    public function bucketRecursos($data);
    
}
