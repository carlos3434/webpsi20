<?php
namespace Incidencia\controllers;
class TipificacionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tipificacion
	 *
	 * @return Response
	 */

	public function postListar(){
		if (\Request::ajax()) {
			$objClass = new \Tipificacion(); 
			return \Response::json(
                array(
                    'rst'    => 1,
                    'datos'   => $objClass->getAll()
                )
            );	
		}
	}


	public function postCargar(){
		$query =  \Incidencia::from('tipificacion as t')
		  		->select(
		  			't.id as tip_id',
                    't.nombre',
                    't.estado'
		  		)
				->where(function($query){
			

				 });
				$query=$query->orderBy(\Input::get('column'),\Input::get('dir'))
				->searchPaginateAndOrder();
        return $query; 	
	}


	public function postActualizar(){
		if (\Request::ajax()) {
			$update = \Tipificacion::findOrFail(Input::get('tipificacion_id'));

			if(\Input::has('estado')){
				$update->estado = \Input::get('estado');				
			}

			if(\Input::has('nombre')){
				$update->nombre = \Input::get('nombre');				
			}

			$update->usuario_updated_at = \Auth::id();
			$update->updated_at = date('Y-m-d H:i:s');
			$update->save();
			return \Response::json(
                array(
                    'rst'    => 1,
                    'msj'    => "Actualizado Correctamente",
                )
            );	
		}
	}


	public function postCreate(){
		if (\Request::ajax()) {
			$data= [];
			$data['nombre'] = \Input::get('nombre');
			$data['estado'] = 1;
			$rst = \Tipificacion::create($data);
			return \Response::json(
                array(
                    'rst'    => ($rst) ? 1 : 0,
                    'msj'    => "Registrado Correctamente",
                )
            );	

		}
	}
}