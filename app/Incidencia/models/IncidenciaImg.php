<?php
namespace Incidencia\models;

class IncidenciaImg extends \Eloquent {
	protected $fillable = ['incidencia_id','image','estado'];
	public $table = 'incidencia_img';

	 public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }
}