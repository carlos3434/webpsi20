<?php
namespace Incidencia\models;

class IncidenciaReq extends \Eloquent {
	public $table = 'incidencia_req';
	protected $fillable = ['descripcion','incidencia_id','valor','image','tipo_valor','estado'];

  public static function boot()
  {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
  }

		public function getAll(){
			$query=  \DB::table('incidencia_req as ue')
                        ->select(
                        	'ue.id',
                        	'ue.descripcion',
                            'ue.incidencia_id',
                          	'ue.valor',
                          	'ue.tipo_valor',
                          	'ue.estado',
                            'ue.image'
                        )
                        ->where(function($query){
                        		$query->where('ue.incidencia_id',\Input::get('incidencia_id'));
                            }
                        )                        
                        ->get();
        	return (count($query)>0) ? $query : false;
		}
}