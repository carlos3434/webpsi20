<?php
namespace Incidencia\models;

class Tipificacion extends \Eloquent {
	public $table = 'tipificacion';
	protected $fillable = ['nombre','estado'];

    public static function boot()
    {
       parent::boot();

       static::updating(function ($table) {
           $table->usuario_updated_at = \Auth::id();
       });
       static::creating(function ($table) {
           $table->usuario_created_at = \Auth::id();
       });
    }

    public function getAll(){
			$query=  \DB::table('tipificacion as t')
                        ->select(
                        	't.id',
                        	't.nombre'
                        )
                        ->where(function($query){
                              	$query->where('t.estado',1);
                            }
                        )                        
                        ->get();
        return (count($query)>0) ? $query : false;
	}


}