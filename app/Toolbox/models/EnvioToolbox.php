<?php

namespace Toolbox\models;

class EnvioToolbox extends \Eloquent
{
    public $table = "envios_toolbox";

    protected $fillable = [
        'solicitud_tecnica_id', 'accion',
        'request', 'response',
        'usuario_created_at', 'usuario_updated_at'
    ];
    protected $appends = [
        'xml_request',
        'xml_response'
    ];
    public $userAuth = 697;

    public static function boot() {
        parent::boot();

        static::updating(function($table) {
            $table->usuario_updated_at = \Auth::id();
        });

        static::saving(function($table) {
            $table->usuario_created_at = \Auth::id();
        });
    }
    public function getXmlRequestAttribute()
    {
        $xmlrequest = \Array2XML::createXML('psi', json_decode($this->request, true));
        return $xmlrequest->saveXML();
    }
    public function getXmlResponseAttribute()
    {
        $xmlresponse = \Array2XML::createXML('psi', json_decode($this->response, true));
        return $xmlresponse->saveXML();
    }
    public function usuarioCreated()
    {
        return $this->belongsTo('Usuario','usuario_created_at');
    }
}