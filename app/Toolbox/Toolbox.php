<?php
namespace Toolbox;

use Toolbox\models\EnvioToolbox;

Abstract class Toolbox
{
    protected $_wsdl;
    protected $_client;
    protected $_tipo;

    public function __construct() 
    {
        try {
            $wsOptArray = [
                "trace" => 1,
                "exception" => 0,
                "connection_timeout" => 500000,
            ];
            $opts = array('http' => array('protocol_version' => '1.0'));
            $wsOptArray["stream_context"] = stream_context_create($opts);
            $soapClient = new \SoapClient(
                $this->_wsdl,
                $wsOptArray
            );
            $this->_client = $soapClient;
        } catch (Exception $e) {
            
        }
    }

    protected function doAction($elementos=array())
    {
        $response = new \stdClass();
        $response->error = false;
        $response->errorMsg ='';

        if ($this->_client===false || $this->_client===null) {
            return false;
        }
        try {
            $request = $elementos['body'];
            $this->header($elementos);
            $result = $this->_client->__soapCall(
                $elementos['action'],
                ["request"=>$request]
            );
            $response->data = $result;
            return $response;
        } catch (\SoapFault $fault) {
            $response->error = true;
            $response->obj = $fault;
            $response->errorCode = $fault->faultcode;
            $response->errorString = $fault->faultstring;
            $objError = $fault->getMessage();
            $obj = null;
            $response->errorMsg = $response->errorString;
            if (isset($objError->ServerException)) {
                $obj = $objError->ServerException;
            } else if (isset($objError->ClientException)) {
                $obj = $objError->ClientException;
            }
            if (!is_null($obj)) {
                $response->exceptionCategory = $obj->exceptionCategory;
                $response->exceptionCode = $obj->exceptionCode;
                $response->exceptionMsg = $obj->exceptionMsg;
                $response->exceptionDetail = $obj->exceptionDetail;
                $response->exceptionSeverity = $obj->exceptionDetail;
                $response->errorMsg = $obj->exceptionMsg;
            }
            
            if (isset($fault->detail)) {
                //$response->errorMsg = $fault->detail;
            }
            return $response;
        } catch (\Exception $error) {
            $response->error = true;
            $objError = $error->getMessage();
            $response->errorMsg = "";
            $obj = null;
            if (isset($objError->ServerException)) {
                $obj = $objError->ServerException;
            } else if (isset($objError->ClientException)) {
                $obj = $objError->ClientException;
            }
            if (!is_null($obj)) {
                $response->exceptionCategory = $obj->exceptionCategory;
                $response->exceptionCode = $obj->exceptionCode;
                $response->exceptionMsg = $obj->exceptionMsg;
                $response->exceptionDetail = $obj->exceptionDetail;
                $response->exceptionSeverity = $obj->exceptionDetail;
            }
            return $response;
        }
    }
    
    protected function tracer( $elementos = [], $response = [] )
    {
        $envio = new EnvioToolbox;
        // $reqheader=explode(" ", str_replace("\r\n", " ", $this->_client->__getLastRequestHeaders()));
        $host = null;
        // $urls=\Config::get("legado.url_lego");

        // foreach ($urls as $key => $value) {
        //     if (array_search($value, $reqheader)) {
        //         $host=$key;
        //         break;
        //     }
        // }

        $envio->host = $host;
        $envio->request = json_encode($elementos["body"]);
        $envio->response = json_encode($response);
        $envio->accion = $elementos["action"];
        $envio->solicitud_tecnica_id = $elementos["solicitud_tecnica_id"];
        $envio->save();
        return;
    }
}
