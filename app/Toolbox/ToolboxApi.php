<?php
namespace Toolbox;

use Toolbox\Toolbox;

class ToolboxApi extends Toolbox
{
    public function __construct()
    {

        // $this->_wsdl = \Config::get("toolbox.wsdl.solicitud");
        $this->_wsdl = \Config::get("legado.wsdl.respuestast");
        parent::__construct();
    }

    public function envio($peticion)
    {
        $body = [
            'tipo_gestion'              => $peticion['tipo_gestion'],
            'solicitud_tecnica'         => $peticion['solicitud_tecnica'],
            'num_requerimiento'         => $peticion['num_requerimiento'],
            'usuario'                   => $peticion['usuario'],
            'tipo_motivo'               => $peticion['tipo_motivo'],
            'motivo_ofsc'               => $peticion['motivo_ofsc'],
            'submotivo_ofsc'            => $peticion['submotivo_ofsc'],
            'codigo_tecnico'            => $peticion['codigo_tecnico'],
            'desc_tecnico'              => $peticion['desc_tecnico'],
            'bucket'                    => $peticion['bucket'],
            'fecha_envio_go'            => $peticion['fecha_envio_go'],
            'hora_envio_go'             => $peticion['hora_envio_go'],
            'fecha_inicio_actividad'    => $peticion['fecha_inicio_actividad'],
            'hora_inicio_actividad'     => $peticion['hora_inicio_actividad'],
            'hora_fin_actividad'        => $peticion['hora_fin_actividad'],
            'hora_fin_actividad'        => $peticion['hora_fin_actividad'],
            'observacion_toa'           => $peticion['observacion_toa'],
            'imagenes'                  => $peticion['imagenes'],
            'data_solicitud_tecnica'    => $peticion['data_solicitud_tecnica']
        ];

        $elementos = [
            'action'      => 'EnvioSolicitud',
            'body'      => $body,
            'operation' => 'EnvioSolicitud',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local')) {
            $response = new \stdClass;
            $response->rst = true;
            $response->msj = "Prueba: Enviado correctamente!";
        } else {
            $response = $this->doAction($elementos);
        }
        $this->tracer($elementos, $response);
        print_r($response);
        return $response;
    }

    public function cierre($peticion)
    {
        $body = [
            'tipo_accion_respuesta' => $peticion['tipo_accion_respuesta'],
            'solicitud_tecnica'     => $peticion['solicitud_tecnica'],
            'error'                 => $peticion['error'],
            'observacion'           => $peticion['observacion']
        ];

        $elementos = [
            'action'      => 'CierreSolicitud',
            'body'      => $body,
            'operation' => 'CierreSolicitud',
            'solicitud_tecnica_id' => $peticion["solicitud_tecnica_id"]
        ];
        if (\App::environment('local')) {
            $response = new \stdClass;
            $response->rst = true;
            $response->msj = "Prueba: Enviado correctamente!";
        } else {
            $response = $this->doAction($elementos);
        }
        $this->tracer($elementos, $response);
        print_r($response);
        return $response;
    }
}
