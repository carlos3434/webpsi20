<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Buscar Componentes</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
        <style type="text/css">
            .msjAlert {
                position: fixed;
                z-index: 1050;
                top: 87%;
                width: 100%;
            }
            #topcontrol:after {
                top: -2px;
                left: 8.5px;
                content: "\f106";
                position: absolute;
                text-align: center;
                font-family: FontAwesome;
            }
            #topcontrol {
                color: #fff;
                z-index: 99;
                width: 30px;
                height: 30px;
                font-size: 20px;
                background: #222;
                position: relative;
                right: 14px !important;
                bottom: 11px !important;
                border-radius: 3px !important;
                    opacity: 1;
                cursor: pointer;
            }
            #back-to-top {
                position: fixed;
                bottom: 40px;
                right: 40px;
                z-index: 9999;
                width: 32px;
                height: 32px;
                text-align: center;
                line-height: 30px;
                background: #f5f5f5;
                color: #444;
                cursor: pointer;
                border: 0;
                border-radius: 2px;
                text-decoration: none;
                transition: opacity 0.2s ease-out;
                opacity: 0;
            }
            #back-to-top:hover {
                background: #e9ebec;
            }
            #back-to-top.show {
                opacity: 1;
            }
            .content-header{
                position: relative;
                padding: 0px 15px 0px 20px;
            }
        </style>
    </head>
    <body id="componenteController" class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
        <div class="wrapper">

        <section class="content-header">
            <h1>
                Buscar Componentes
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                  <div class="box-title">Búsqueda direcciones</div>
              </div>
              <div class="box-body">
                <form class="form-inline" style="margin-bottom: 10px;" id="form_busquedaUnica">
                <div class="form-group">
                      <div class="col-xs-6 col-sm-4 col-md-4">
                          <label>Distrito</label>
                          <select class="form-control" name="calle_distrito" id="calle_distrito">
                              <option value="150100"> - Todo Lima - </option>
                              <option value="150101">Lima Cercado</option>
                              <option value="150102">Ancon</option>
                              <option value="150103">Ate</option>
                              <option value="150104">Barranco</option>
                              <option value="150105">Breña</option>
                              <option value="150106">Carabayllo</option>
                              <option value="150107">Chaclacayo</option>
                              <option value="150108">Chorrillos</option>
                              <option value="150109">Cieneguilla</option>
                              <option value="150110">Comas</option>
                              <option value="150111">El Agustino</option>
                              <option value="150112">Independencia</option>
                              <option value="150113">Jesus María</option>
                              <option value="150114">La Molina</option>
                              <option value="150115">La Victoria</option>
                              <option value="150116">Lince</option>
                              <option value="150117">Los Olivos</option>
                              <option value="150118">Lurigancho</option>
                              <option value="150119">Lurin</option>
                              <option value="150120">Magdalena del Mar</option>
                              <option value="150121">Pueblo Libre</option>
                              <option value="150122">Miraflores</option>
                              <option value="150123">Pachacamac</option>
                              <option value="150124">Pucusana</option>
                              <option value="150125">Puente Piedra</option>
                              <option value="150126">Punta Hermosa</option>
                              <option value="150127">Punta Negra</option>
                              <option value="150128">Rimac</option>
                              <option value="150129">San Bartolo</option>
                              <option value="150130">San Borja</option>
                              <option value="150131">San Isidro</option>
                              <option value="150132">San Juan De Lurigancho</option>
                              <option value="150133">San Juan de Miraflores</option>
                              <option value="150134">San Luis</option>
                              <option value="150135">San Martin de Porres</option>
                              <option value="150136">San Miguel</option>
                              <option value="150137">Santa Anita</option>
                              <option value="150138">Santa Maria del Mar</option>
                              <option value="150139">Santa Rosa</option>
                              <option value="150140">Santiago de Surco</option>
                              <option value="150141">Surquillo</option>
                              <option value="150142">Villa el Salvador</option>
                              <option value="150143">Villa Maria del Triunfo</option>
                              <option value="070100"> - Todo Callao - </option>
                              <option value="070101">Callao</option>
                              <option value="070102">Bellavista</option>
                              <option value="070103">Carmen de la Legua Reynoso</option>
                              <option value="070104">La Perla</option>
                              <option value="070105">La Punta</option>
                              <option value="070106">Ventanilla</option>
                          </select>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-3">
                          <label>Calle / Av. / Jr. </label>
                          <input class="form-control" type="text" id="txt_calle_nombre" name="txt_calle_nombre">
                      </div>
                      <div class="col-xs-6 col-sm-4 col-md-3">
                          <label>Número</label>
                          <input class="form-control" type="text" id="txt_calle_numero" name="txt_calle_numero">
                      </div>
                      <div class="col-xs-6 col-sm-3 col-md-2">
                          <label>&nbsp;</label>
                          <input class="form-control btn-info" type="button" id="btn_calle" name="btn_calle" value="Buscar" onclick="getBuscarCalle()">
                      </div>
                    </div>
                </form>
                <hr>
                <div class="form-group">
                  <label>Buscar dirección en Google:</label>
                  <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Ingresa una dirección para buscar">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" id="search">Buscar</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="box-title">Elementos</div>
                            <div class="box-tools pull-right">
                            <button type="button" class="btn btn-primary btn-sm" onclick="Buscar();"><i class="fa fa-search fa-lg"></i> Buscar Elementos</button>
                            
                            <button type="button" class="btn btn-success btn-sm" onclick="ubicacion();"><i class="fa fa-map-marker fa-lg"></i> Obtener Ubicación</button>
                            </div>
                        </div>
                        <div class="box-body">
                            <form id="form_componentes">
                                <div class="form-group">
                                    <label>X,Y encontradas:</label>
                                    <input type="text" class="form-control" id="ubicacion" name="ubicacion">
                                </div>
                            
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                          <th style="width: 10px"><input type="checkbox" id="allcheckbox"></th>
                                          <th>Elemento</th>
                                          <th>Distancia<br>(km)</th>
                                          <th>Nro. Elementos</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_componentes">

                                    </tbody>
                                </table>                  
                            </form>
                            <br>
                            <div class="text-center">
                                <button type="button" class="btn btn-primary btn-sm" onclick="Buscar();"><i class="fa fa-search fa-lg"></i> Buscar Elementos</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12">
           
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="mymap" style="height: 500px"></div>
                            <div id="resultado"></div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://code.jquery.com/ui/1.11.2/jquery-ui.min.js')}}
        {{ HTML::script('http://maps.google.com/maps/api/js?sensor=false&libraries=places') }}
        {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js')}}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script('js/geo/buscarcomponentes.js') }}
        {{ HTML::script('js/geo/buscarcomponentes_ajax.js') }}
    @show
    <script>
    $(function() {
    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    });
    
    </script>

    <a href="#" id="back-to-top" title="Back to top" style="position: fixed; bottom: 5px; right: 5px; opacity: 1; cursor: pointer;">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
    </a>

    </body>
</html>