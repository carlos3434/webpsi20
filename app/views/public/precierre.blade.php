<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pre-Cierre</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
        <style type="text/css">
            .msjAlert {
                position: fixed;
                z-index: 1050;
                top: 87%;
                width: 100%;
            }
            #topcontrol:after {
                top: -2px;
                left: 8.5px;
                content: "\f106";
                position: absolute;
                text-align: center;
                font-family: FontAwesome;
            }
            #topcontrol {
                color: #fff;
                z-index: 99;
                width: 30px;
                height: 30px;
                font-size: 20px;
                background: #222;
                position: relative;
                right: 14px !important;
                bottom: 11px !important;
                border-radius: 3px !important;
                    opacity: 1;
                cursor: pointer;
            }
            #back-to-top {
                position: fixed;
                bottom: 40px;
                right: 40px;
                z-index: 9999;
                width: 32px;
                height: 32px;
                text-align: center;
                line-height: 30px;
                background: #f5f5f5;
                color: #444;
                cursor: pointer;
                border: 0;
                border-radius: 2px;
                text-decoration: none;
                transition: opacity 0.2s ease-out;
                opacity: 0;
            }
            #back-to-top:hover {
                background: #e9ebec;
            }
            #back-to-top.show {
                opacity: 1;
            }
            .content-header{
                position: relative;
                padding: 0px 15px 0px 20px;
            }
        </style>
    </head>
    <body id="componenteController" class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
        <div class="wrapper">

        <section class="content-header">
            <h1>
                Pre Cierre
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                  <div class="box-title">Búsqueda direcciones</div>
              </div>
              <div class="box-body">
                <form class="form-inline" style="margin-bottom: 10px;" id="form_busquedaUnica">
                <div class="form-group">
                      <div class="col-xs-6 col-sm-4 col-md-4">
                          <label>Distrito</label>
                          <select class="form-control" name="calle_distrito" id="calle_distrito">
                          </select>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-3">
                          <label>Calle / Av. / Jr. </label>
                          <input class="form-control" type="text" id="txt_calle_nombre" name="txt_calle_nombre">
                      </div>
                      <div class="col-xs-6 col-sm-4 col-md-3">
                          <label>Número</label>
                          <input class="form-control" type="text" id="txt_calle_numero" name="txt_calle_numero">
                      </div>
                      <div class="col-xs-6 col-sm-3 col-md-2">
                          <label>&nbsp;</label>
                          <input class="form-control btn-info" type="button" id="btn_calle" name="btn_calle" value="Buscar" onclick="getBuscarCalle()">
                      </div>
                    </div>
                </form>
                <hr>
                <div class="form-group">
                  <label>Buscar dirección en Google:</label>
                  <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Ej: Av Javier Prado Este 123 Lince">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" id="search">Buscar</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
                    
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://code.jquery.com/ui/1.11.2/jquery-ui.min.js')}}
        {{ HTML::script('http://maps.google.com/maps/api/js?sensor=false&libraries=places') }}
        {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js')}}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script('js/precierre/precierre.js') }}
        <!--{{ HTML::script('js/geo/buscarcomponentestecnico_ajax.js') }}-->
    @show
    <script>
    $(function() {
    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    });

    </script>

    <a href="#" id="back-to-top" title="Back to top" style="position: fixed; bottom: 5px; right: 5px; opacity: 1; cursor: pointer;">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
    </a>

    </body>
</html>