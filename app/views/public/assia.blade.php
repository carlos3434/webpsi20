<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>ASSIA</title>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}

        {{ HTML::style('//unpkg.com/bootstrap@next/dist/css/bootstrap.min.css') }}
        {{ HTML::style('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css') }}

        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}


        <style type="text/css">
            .loading-img{
                z-index:1070;
                background:url(../../../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
            .overlay{
                z-index:1060;
                background:rgba(255,255,255,.7);
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
            }
        </style>
    </head>
    <body>
        <div id="app">
            <b-navbar toggleable type="inverse" variant="success">
                <b-link class="navbar-brand" to="#">
                    <span>Diagnostico ASSIA</span>
                </b-link>
            </b-navbar>
        
            <div class="container">
                <div  style="padding-top: 2em">
                    Teléfono:
                    <b-form-input v-model="telefono" type="text" placeholder="Ingrese teléfono"></b-form-input>
                    <br>
                    <p><b-btn v-b-toggle.collapse1 variant="primary" @click="diagnostico">Diagnostico</b-btn></p>
                    <b-card :hidden="(respuesta == '') ? true : false"
                            header="Resultado diagnostico ASSIA"
                            variant="outline-danger"
                            class="col-md-12"
                            title="Resultado:"
                            show-footer
                    >
                        <div class="col-md-12">@{{respuesta}}</div>
                    </b-card>
                </div>
            </div>
        </div>
        {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
        {{ HTML::script('https://unpkg.com/vue@2.3.3') }}

        {{ HTML::script('//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
        {{ HTML::script('//unpkg.com/tether@latest/dist/js/tether.min.js') }}
        {{ HTML::script('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js') }}

        {{ HTML::script('js/public/assia.js') }}
    </body>
</html>