<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>TABLE</title>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}

        {{ HTML::style('//unpkg.com/bootstrap@next/dist/css/bootstrap.min.css') }}
        {{ HTML::style('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css') }}

        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}

        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --}}


        <style type="text/css">
            .loading-img{
                z-index:1070;
                background:url(../../../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
            .overlay{
                z-index:1060;
                background:rgba(255,255,255,.7);
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
            }
            @media screen and (max-width: 600px) {
                table {
                    width:100%;
                }
                thead {
                    display: none;
                }
                tr:nth-of-type(2n) {
                    background-color: inherit;
                }
                tr td:first-child {
                    font-weight:bold;font-size:1.3em;
                    background-color: #31708f!important;
                    color: white!important;
                }
                tbody td {
                    display: block;  text-align:center;
                }
                tbody td:before { 
                    content: attr(data-th); 
                    display: block;
                    text-align:center;  
                }
            }
        </style>
    </head>
    <body>
        <div id="app">
            <div class="container">
                <br><br>
                <div class="row text-center">
                    <div class="col-md-2">
                        <label>Mostrar:</label>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" v-on:change="listar" v-model="perPage">
                            <option :value="5">5</option>
                            <option :value="10">10</option>
                            <option :value="15">15</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label>Filtro:</label>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" v-model="filterSelected">
                            <option v-for="head in heads" :value="head.filterHead">
                                @{{ head.nameHead }}
                            </option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <b-tooltip content="Presionar ENTER para buscar">
                            <input class="form-control" v-model="filter" placeholder="Ingresar filtro" v-on:keyup.enter="listar">
                        </b-tooltip>
                    </div>
                </div>
                <br>
                <table class="table table-striped table-bordered">
                    <thead style="background-color: #31708f!important; color: white!important;">
                        <th class="text-center" v-for="head in heads">@{{ head.nameHead }}</th>
                        <th class="text-center">[ . ]</th>
                    </thead>
                    <tbody class="text-center">
                        <tr v-for="dato in datos">
                            <td>@{{ dato.nombre }}</td>
                            <td>@{{ dato.ape_pat }}</td>
                            <td>@{{ dato.ape_mat }}</td>
                            <td>@{{ dato.telecl_1 }}</td>
                            <td><b-button  v-on:click="detalle(dato), $root.$emit('show::modal','modal1')">EDITAR</b-button></td>
                        </tr>
                    </tbody>
                </table>

                <div class="justify-content-center row my-1">
                    <b-pagination size="md" :total-rows="this.totalRows" :per-page="perPage" v-model="currentPage" v-on:click.native="listar"/>
                </div>

                <b-button  v-on:click="$root.$emit('show::modal','modal1')">CREAR</b-button>
                <pre>@{{ action }}</pre>
                <pre>@{{ object }}</pre>

                <b-modal id="modal1" header-style="background-color: #31708f!important; color: white!important;" ok-title="Grabar" hide-header hide-footer size="lg" title="Formulario">
                    <b-card no-block>
                        <b-tabs card ref="tabs" v-model="tabIndex">
                            <b-tab title="General">
                                <form>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Nombre</label>
                                            <div class="input-group">
                                                <input  class="form-control pull-right" type="text" v-model="object.nombre" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>A. Paterno</label>
                                            <div class="input-group">
                                                <input  class="form-control pull-right" type="text" v-model="object.ape_pat" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>A. Materno</label>
                                            <div class="input-group">
                                                <input  class="form-control pull-right" type="text" v-model="object.ape_mat" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Telefono</label>
                                            <div class="input-group">
                                                <input  class="form-control pull-right" type="text" v-model="object.telecl_1" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Estado</label>
                                            <select class="form-control" v-model="object.estado">
                                                <option value="1" selected="true">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-12">
                                            <b-card class="mb-2" variant="success">Simple Card</b-card>2726
                                            <small slot="footer" class="text-muted">
                                                Last updated 3 mins ago
                                            </small>
                                        </div>
                                    </div>
                                </form>
                            </b-tab>
                            <b-tab title="Edit profile">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <b-carousel controls indicators :interval="3000" background="none" height="300px">

                                            <!-- Text slides -->
                                            <b-carousel-slide height="300px" caption="WEBPSI20" text="init" img="http://www.astrolabio.com.mx/wp-content/uploads/2016/10/animal_118_20120518_1511321022.jpg">
                                            </b-carousel-slide>

                                            <!-- Slides with custom text -->
                                            <b-carousel-slide height="300px" caption="WEBPSI20" img="http://res.cloudinary.com/db79cecgq/image/upload/c_crop,h_720,w_1440,y_91/c_fill,h_800,w_1600/v1434685904/como-animales-orientan-campos-magneticos-tierra-1440-2.jpg">
                                              Hello world
                                            </b-carousel-slide>

                                            <!-- Slides with image -->
                                            <b-carousel-slide height="300px" caption="WEBPSI20" img="http://6www.ecestaticos.com/imagestatic/clipping/a69/315/a6931512e05a3be083418b38edbc432e/como-evitar-el-ataque-de-los-animales-mas-peligrosos-del-mundo.jpg?mtime=1474023878">
                                            </b-carousel-slide>

                                        </b-carousel>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col">
                                                <b-button-group vertical>
                                                    <div class="col-sm-12">
                                                        <label>A. Paterno</label>
                                                        <div class="input-group">
                                                            <input  class="form-control pull-right" type="text" v-model="object.ape_pat" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label>A. Materno</label>
                                                        <div class="input-group">
                                                            <input  class="form-control pull-right" type="text" v-model="object.ape_mat" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label>Telefono</label>
                                                        <div class="input-group">
                                                            <input  class="form-control pull-right" type="text" v-model="object.telecl_1" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label>Telefono</label>
                                                        <div class="input-group">
                                                            <input  class="form-control pull-right" type="text" v-model="object.telecl_1" required>
                                                        </div>
                                                    </div>
                                                </b-button-group>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </b-tab>
                        </b-tabs>
                    </b-card>
                </b-modal>

                {{-- <b-modal id="modal1" ok-title="Grabar" size="lg" title="Formulario" @ok="operation">
                    <form>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Nombre</label>
                                <div class="input-group">
                                    <input  class="form-control pull-right" type="text" v-model="object.nombre" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>A. Paterno</label>
                                <div class="input-group">
                                    <input  class="form-control pull-right" type="text" v-model="object.ape_pat" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>A. Materno</label>
                                <div class="input-group">
                                    <input  class="form-control pull-right" type="text" v-model="object.ape_mat" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Telefono</label>
                                <div class="input-group">
                                    <input  class="form-control pull-right" type="text" v-model="object.telecl_1" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Estado</label>
                                <select class="form-control" v-model="object.estado">
                                    <option value="1" selected="true">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </b-modal> --}}
            </div>
        </div>
{{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
{{ HTML::script('https://unpkg.com/vue@2.3.3') }}

{{ HTML::script('//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
{{ HTML::script('//unpkg.com/tether@latest/dist/js/tether.min.js') }}
{{ HTML::script('//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js') }}

{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> --}}

{{ HTML::script('js/public/table.js') }}
</body>
</html>