<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FFTT</title>
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('http://maps.google.com/maps/api/js?sensor=false&libraries=drawing') }}
        {{ HTML::script('js/geo/geo.functions.js') }}
        {{ HTML::script('js/utils.js') }}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script>
            /**
     * Muestra infowindow para un marcador
     * 
     * @param {type} thisMap Mapa donde se mostrar marcador
     * @param {type} element Marcador (marker)
     * @param {type} content Contenido (html) del infowindow
     */
    function doInfoWindow(thisMap, element, content) {
        google.maps.event.addListener(element, 'click', (
                function(marker, infocontent, infowindow) {
                    return function() {
                        infowindow.setContent(infocontent);
                        infowindow.open(thisMap, marker);
                    };
                })(element, content, infowindow));
    }
            $(document).ready(function() {
                var mapTools = {};
                var bounds = new google.maps.LatLngBounds();
                                
                //Iniciar mapa
                objMap = doObjMap("ordentecnico", objMapProps, mapTools);
                
                <?php
                //AGENDA
                if(isset($data)){
                    foreach ($data as $key=>$val) {
                        $temp1 = str_replace("-", "/", $val['tipoactu']);
                        $temp2 = str_replace("_", "/", $temp1);
                        $tipoactu = explode("/",$temp2);
                        $tipo = '';
                     
                        if(array_search("catv", $tipoactu)) {
                            echo 'var agendaIcon2 = "http://psiweb.ddns.net:7020/img/icons/tap2.png";';
                            $tipo = 'catv';
                        } else {
                            echo 'var agendaIcon2 = "http://psiweb.ddns.net:7020/img/icons/mdf.png";';
                            $tipo = 'adsl';
                        }
                        ?>
                            var infocontent = "";
                            bounds.extend(new google.maps.LatLng(<?php echo $val['y'];?>, <?php echo $val['x'];?>));
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(<?php echo $val['y'];?>, <?php echo $val['x'];?>),
                                map: objMap,
                                //icon: agendaIcon,
                                zIndex: zIndex,
                                codactu: '<?php echo $val['codactu'];?>',
                                tipoactu: '<?php echo $val['tipoactu'];?>',
                                actividad: '<?php echo $val['actividad'];?>',
                                idgestion: <?php echo $val['id'];?>,
                                estado: '<?php echo $val['estado'];?>',
                                coordinado: '<?php echo $val['coordinado'];?>',
                                fecha_agenda: '<?php echo $val['fecha_agenda'];?>'
                            });
                            
                            infocontent += "<div class=\"infow\" style=\"overflow: auto; text-align: left\"\">" +
                                "<div class=\"detalle_actu\">" +
                                '<?php echo $val['tipoactu'];?>' + "<br>" +
                                '<?php echo $val['nombre_cliente_critico'];?>' + "<br>" +
                                '<?php echo $val['fh_agenda'];?>' + " / " +
                                '<?php echo $val['direccion_instalacion'];?>' + "<br>" +
                                '<?php echo $val['codactu'];?>' + "<br>" +
                                '<?php echo $val['fftt'];?>' + "<br>" +
                                '<?php echo $val['inscripcion'];?>' + "<br>" +
                                '<?php echo $val['id_atc'];?>' + "<br>" +
                                '<?php echo $val['quiebre'];?>' + "<br>" +
                                "</div>";
                        
                            doInfoWindow(objMap, marker, infocontent);
                        <?php
                    }
                }
                try {
                    //FFTT
                     $tbody = ''; $con = 1;
                    if(isset($fftt)){
                        foreach ($fftt as $key=>$val) {
                            ?>                        
                                var infocontent = "";
                                bounds.extend(new google.maps.LatLng(<?php echo $val['coord_y'];?>, <?php echo $val['coord_x'];?>));
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(<?php echo $val['coord_y'];?>, <?php echo $val['coord_x'];?>),
                                    map: objMap,
                                    icon: agendaIcon2,
                                    zIndex: zIndex--
                                });
                                <?php
                                    $tbody .= '<tr>';
                                    $tbody .= '<td>'.$con.'</td>';
                                    if($tipo == 'catv') {
                                        $info = '"Tap:'.$val['tap'].'<br> Nodo: '.$val['nodo'].'<br> Troba:'.$val['troba'].'<br> Amplificador:'.$val['amplificador'].'<br>"';
                                        
                                        $tbody .= '<td>'.$val['tap'].' - '.$val['nodo'].' - '.$val['troba'].' - '.$val['amplificador'].' - '.$val['zonal'].'</td>';
                                    } else {
                                        $info = '"Armario: '.$val['armario'].'<br> MDF: '.$val['mdf'].'<br>"';
                                        $tbody .= '<td>'.$val['armario'].' - '.$val['mdf'].'- '.$val['zonal'] .'</td>';
                                    }
                                    $tbody .= '</tr>';
                                ?>
                                infocontent += "<div class=\"infow\" style=\"overflow: auto; text-align: left\"\">" +
                                    "<div class=\"detalle_actu\">" +
                                    <?php echo (isset($info))?$info:'';?>+
                                    'Zonal: <?php echo $val['zonal'];?>' + "<br>" +
                                    'Orden: <?php echo $val['orden'];?>' + "<br>" +
                                    "</div>";
                            
                                doInfoWindow(objMap, marker, infocontent);
                            <?php
                            $con++;
                        }
                    }
                } catch (Exception $e) {
                    echo "console.log('Error');";
                }
                ?>
                var tbody = "<?php echo $tbody;?>";
                $('#fftt').html(tbody);
                objMap.fitBounds(bounds);
            });
        </script>

        <style>
            #aordentecnico {
                width: 100%;
                height: 600px;
            }

            .map{
                width: 100%;
                height: 550px;
                border-top: solid 1px #eee;
                border-bottom: solid 1px #eee;
            }
            .margin-bottom-50 {
                margin-bottom: 50px;
            }
            .content {
                padding-top: 0px;
                padding-bottom: 20px;
            }
            .estilo {
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }
            @media (min-width: 320px){
                .estilo {
                    *width: 400px;
                }
                .map{
                    height: 400px;
                }
            }
            @media (min-width: 768px){
                .estilo {
                    width: 900px;
                }
                .map{
                    height: 1200px;
                }
            }
            @media (min-width: 992px){
                .estilo {
                width: 970px;
                }
            }
            @media (min-width: 1200px){
                .estilo {
                    width: 1450px;
                }
                .map{
                    height: 600px;
                }
            }
        </style>
    </head>
    <body>
        <div class="estilo content">
            <div class="wrapper">
                <div class="row">
                    <div class="col-xs-12 col-lg-8">
                        <h1>Orden / FFTT</h1>
                        <div id="ordentecnico" class="map margin-bottom-50"></div>
                    </div>
                    <div class="col-xs-12 col-lg-4">
                        <h1>Listado </h1>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                  <th style="width: 10px">#</th>
                                  <th style="width: 40px">Descripcion</th>
                                </tr>
                            </thead>
                            <tbody id="fftt">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
