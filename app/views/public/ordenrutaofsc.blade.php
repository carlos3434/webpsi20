<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FFTT</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://maps.google.com/maps/api/js?libraries=places,drawing&key='.Config::get('wpsi.map.key').'') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        <!--{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }} -->
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}
        {{ HTML::script('js/geo/geo.functions.js') }}

    </head>   
    <style>
    #update_map, #update_sv {
        width: 100%;
        height: 550px;
    }
    #target {
        width: 345px;
    }
    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 250px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    #update_maps{
        text-align: center;
        height: 600px;
    }
    #update_maps {
        background-color: #EFE;
        border-color: #CDC;
        width: 70%;
    }

    #update_maps {width: 100%;}

    @media screen and (max-width: 700px) {
      #update_maps {width: 100%;}
    }
  </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <section class="content-header">
                    <h1>
                        Orden Coordenadas / FFTT
                    </h1>
                </section>
                <section class="content">
                    <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                        <form style="" id="">
                            <div class="row">
                                <div class="col-md-8 col-xs-12">
                                    <h3 style="margin-top: 0;">Actualizar Coordenadas</h3>
                                    <div class="col-md-2">
                                        <label>Solicitud</label>
                                        <input class="form-control" type="text" solicitud='solicitud' v-model="solicitud" value="{{ $solicitud }}" readonly="">
                                    </div>
                                    <div class="col-md-2">
                                        <label>CodActu</label>
                                        <input class="form-control" type="text" codactu='codactu' v-model="codactu" value="{{ $codactu }}" readonly="">
                                    </div>
                                    <div class="col-md-8">
                                        <label>Direccion</label>
                                        <input class="form-control" type="text" readonly="" value="@{{st.direccion_instalacion}}" style="">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Coord X</label>
                                        <input class="form-control" type="text" coordx='coordx' v-model="coordx" value="@{{coordx}}" readonly="" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Coord Y</label>
                                        <input class="form-control" type="text" coordy='coordy' v-model="coordy" value="@{{coordy}}" readonly="">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Referencia</label>
                                        <input class="form-control" type="text" id="txt_refencia" readonly="" value="" style="">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6" style="margin-top:25px;">
                                        <a class="btn btn-warning" @click="ValidarCoord()" id="btn_validar" style="width:100%">
                                            <i class="fa fa-location-arrow fa-lg"></i> Validar Coordenadas
                                        </a>
                                    </div> 
                                    <div class="col-md-2 col-sm-6 col-xs-6" style="margin-top:25px;">
                                        <a class="btn btn-danger" @click="LimpiarMapa()" id="btn_limpiar" style="width:100%">
                                            <i class="fa fa-undo fa-lg"></i> Limpiar
                                        </a>
                                    </div> 
                                    <div class="col-md-2 col-sm-6 col-xs-6" style="margin-top:25px;">
                                        <a class="btn btn-primary" @click="ObtenerLocalizacion()" style="width:100%">
                                            <i class="fa fa-globe fa-lg"></i> Mi Ubicación  
                                        </a>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6" style="margin-top:25px;">
                                        <a class="btn btn-success" @click="SaveData()" id="btn_actualizar" style="width:100%">
                                            <i class="fa fa-save fa-lg"></i> Actualizar
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <h3 style="margin-top: 5px;">Listado FFTT</h3>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th style="width: 10px">#</th>
                                            <th style="width: 40px">Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody id="fftt">
            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                            <!--<div id="update_maps"style="position: relative; overflow: block; min-height:600px"></div>-->
                            <div id="update_maps">Content</div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div id="update_sv"></div>
                        </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-xs-12 col-lg-8">
                            <div id="ordentecnico" class="map margin-bottom-50"></div>
                        </div>
                        <div class="col-xs-12 col-lg-4">
                            
                        </div>
                    </div>-->
                </section>
            </div>
         </div>
         @section('javascript')
        {{ HTML::script("lib/jquery-ui-1.11.2/jquery-ui.js") }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::script('js/geo/coordenadasfftt.js') }}
    @show
    </body>
</html>
