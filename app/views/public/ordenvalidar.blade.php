<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FFTT</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://maps.google.com/maps/api/js?libraries=places,drawing&key='.Config::get('wpsi.map.key').'') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}
        {{ HTML::script('js/geo/geo.functions.js') }}

    </head>   
    <style>
    #update_map, #update_sv {
        width: 100%;
        height: 550px;
    }

    #update_maps{
        text-align: center;
        height: 400px;
    }
    #update_maps {
        background-color: #EFE;
        border-color: #CDC;
        width: 70%;
    }

    #update_maps {width: 100%;}

    @media screen and (max-width: 700px) {
      #update_maps {width: 100%;}
    }
  </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <section class="content-header">
                    <h1>
                        Validación XY
                    </h1>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom:25px;">
                                        <a class="btn btn-success" @click="ValidarCoord()" id="btn_validar" style="width:100%">
                                            <i class="fa fa-location-arrow fa-lg"></i> Validar Coordenadas
                                        </a>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div id="update_maps">Content</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <!--<div class="col-md-3 col-sm-12 col-xs-12" style="">
                                        <a class="btn btn-primary" @click="ObtenerLocalizacion()" style="width:100%">
                                            <i class="fa fa-globe fa-lg"></i> Mi Ubicación  
                                        </a>
                                    </div>-->
                                    <div class="col-md-2">
                                        <label>Solicitud</label>
                                        <input class="form-control" type="text" solicitud='solicitud' v-model="solicitud" value="{{ $solicitud }}" readonly="">
                                    </div>
                                    <div class="col-md-2">
                                        <label>CodActu</label>
                                        <input class="form-control" type="text" codactu='codactu' v-model="codactu" value="{{ $codactu }}" readonly="">
                                    </div>
                                    <div class="col-md-8">
                                        <label>Direccion</label>
                                        <input class="form-control" type="text" readonly="" value="@{{st.direccion_instalacion}}" style="">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Coord X</label>
                                        <input class="form-control" type="text" coordx='coordx' v-model="coordx" value="@{{coordx}}" readonly="" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Coord Y</label>
                                        <input class="form-control" type="text" coordy='coordy' v-model="coordy" value="@{{coordy}}" readonly="">
                                    </div>
                                </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <!--<div class="row">
                        <div class="col-xs-12 col-lg-8">
                            <div id="ordentecnico" class="map margin-bottom-50"></div>
                        </div>
                        <div class="col-xs-12 col-lg-4">
                            
                        </div>
                    </div>-->
                </section>
            </div>
         </div>
         @section('javascript')
        {{ HTML::script("lib/jquery-ui-1.11.2/jquery-ui.js") }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::script('js/geo/ordenvalidar.js') }}
    @show
    </body>
</html>



        