<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        <style type="text/css">
            .overlay,
            .loading-img {
              position: fixed;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
            }
            .overlay {
              z-index: 1060;
              background: rgba(255, 255, 255, 0.7);
            }
            .overlay.dark {
              background: rgba(0, 0, 0, 0.5);
            }
            .loading-img {
              z-index: 1070;
              background: transparent url('../img/admin/ajax-loader1.gif') 50% 50% no-repeat;
            }
            /* 
                Component: alert
            ------------------------
            */
            .msjAlert {
              position: fixed;
              z-index: 1050;
              top:87%;
              width: 100%;
            }
            .msjAlert > .alert{
              border-color: #000;
              text-align: center;
            }
            .close{
              font-size: 29px;
              font-weight: 900;
              opacity: 0.4;
            }

            .alert {
              padding-left: 30px;
              margin-left: 15px;
              position: relative;
            }
            .alert > .fa,
            .alert > .glyphicon {
              position: absolute;
              left: -15px;
              top: -15px;
              width: 35px;
              height: 35px;
              -webkit-border-radius: 50%;
              -moz-border-radius: 50%;
              border-radius: 50%;
              line-height: 35px;
              text-align: center;
              background: inherit;
              border: inherit;
            }
            .ok {
                color: green;
                font-weight: bold;
            }
            .fail {
                color: red;
                font-weight: bold;
            }
        </style>
    </head>
    <body id="componenteController">
        <div id="msj" class="msjAlert"> </div>
        <div class="container">
            <h3> Cable Modem </h3>
            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                <div class="box-body">
                                    <div class="row">
                                        <form class="" style="margin-bottom: 10px;" id="form_cablemodem">
                                            <div class="form-group">
                                                <label class="col-sm-2">Mac Deco:</label>
                                                <div class="col-sm-6" >
                                                    <input type="text" class="form-control" id="mac" name="mac" placeholder="Ej: 001dd1b84522" minlength="12" maxlength="12">
                                                </div>
                                                <div class="col-sm-2 text-center">
                                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search fa-lg"></i> Buscar</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div id="result"></div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </div>
        </div>
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://code.jquery.com/ui/1.11.2/jquery-ui.min.js')}}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script('js/componentes/cablemodem.js') }}
    </body>
</html>
