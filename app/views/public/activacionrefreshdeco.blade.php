<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>Activacion</title>
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}
        <style type="text/css">
        @media screen and (max-width: 600px) {
            table {width:100%;}
            thead {display: none;}
            tr:nth-of-type(2n) {background-color: inherit;}
            tr td:first-child {background: #f0f0f0; font-weight:bold;font-size:1.3em;}
            tbody td {display: block;  text-align:center;}
            tbody td:before { 
                content: attr(data-th); 
                display: block;
                text-align:center;  
              }
        }
        </style>
    </head>
    <body id="componenteController">
        <div class="container">
            <input type="hidden" id='id' v-model="id" value="{{ $id }}">
            <input type="hidden" id='carnet' v-model="carnet" value="{{ $carnet }}">
            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                <div class="panel panel-primary">
                                <!-- Default panel contents -->
                                <div class="panel-heading">Decodificadores</div>
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered table-stripe active">
                                            <thead>
                                                <th>Nombre</th>
                                                <th>Serie</th>
                                                <th>Tarjeta</th>
                                                <th>Acción</th>
                                                <th> [ - ] </th>
                                            </thead>
                                            <tbody>
                                                <tr v-for="componente in componentes">
                                                    <td><lavel>@{{ componente.nombre }}</lavel></td>
                                                    <td>
                                                        <input type="text" style="min-width:150px" class="form-control" v-model="componente.serie" placeholder="ingrese serie decodificador">
                                                    </td>
                                                    <td>
                                                        <input type="text" style="min-width:150px" class="form-control" v-model="componente.tarjeta" placeholder="ingrese serie tarjeta">
                                                    </td>
                                                    <td>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="accion@{{componente.id}}" value="activacion" v-model="componente.accion"> Activación
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="accion@{{componente.id}}" value="refresh" v-model="componente.accion"> Refresh
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div v-if="componente.accion == 'activacion'">
                                                          <button :disabled="componente.serie.trim()==='' || componente.tarjeta.trim()===''" type="button" class="btn btn-primary btn-sm" @click="EjecutarDeco(componente)">Activación</button>
                                                        </div>
                                                        <div v-if="componente.accion == 'refresh'">
                                                          <button :disabled="componente.serie.trim()===''" type="button" class="btn btn-success btn-sm" @click="EjecutarDeco(componente)">Refresh</button>
                                                        </div>
                                                        <div v-if="componente.rst == 1">
                                                            <span class="glyphicon glyphicon-ok" style="color:#00FF00;"></span>
                                                        <div v-if="componente.rst == 0">
                                                            <span class="glyphicon glyphicon-remove" style="color:#FF0000;"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div>

                                <div class="panel panel-danger" style="border-color: #C83333!important;">
                                <!-- Default panel contents -->
                                <div class="panel-heading" style="background-color: #C83333!important; color: white!important;">Modem</div>
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered table-stripe active">
                                            <thead>
                                                <th>Nombre</th>
                                                <th> CM/RF MAC</th>
                                            </thead>
                                            <tbody>
                                                <tr v-for="modem in modems">
                                                    <td><lavel>@{{ modem.nombre }}</lavel></td>
                                                    <td>
                                                        <input type="text" style="min-width:150px" class="form-control" v-model="modem.mac" placeholder="ingrese mac modem">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div>

                                <div class="form-group">
                                    <button type="button" class="btn btn-primary pull-right" @click="EjecutarDecos(componentes, modems)"><?php echo trans('main.Save') ?></button>
                                </div>
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>
                </section><!-- /.content -->
                <div class="alert alert-success" transition="success" v-if="success">@{{ msj }} </div>
                <div class="alert alert-danger" transition="danger" v-if="danger">@{{ msj }}.</div>
            </div>
        </div>
        {{ HTML::script('js/componentes/activacionrefresh.js') }}
    </body>
</html>
