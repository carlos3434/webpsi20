<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>Cable Modem</title>
        {{-- {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }} --}}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}
        {{-- {{ HTML::script('js/utils.js') }} --}}
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}
        {{ HTML::style("lib/font-awesome-4.2.0/css/font-awesome.min.css") }}

        <style type="text/css">
            .nombre-detalle{display: none;}
            .loading-img{
                z-index:1070;
                background:url(../../../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
            .overlay{
                z-index:1060;
                background:rgba(255,255,255,.7);
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
            }
            @media screen and (max-width: 600px) {
                table {
                    width:100%;
                }
                thead {
                    display: none;
                }
                tr:nth-of-type(2n) {
                    background-color: inherit;
                }
                tr td:first-child {
                    background: #f0f0f0; font-weight:bold;font-size:1.3em;
                }
                tbody td {
                    display: block;  text-align:center;
                }
                tbody td:before { 
                    content: attr(data-th); 
                    display: block;
                    text-align:center;  
                }
                .nombre-detalle{display: inline-block; text-align: left;}
                .tdleft{
                   text-align:left;  
                }
            }
        </style>

    </head>
    <body>

        <div class="container">
            <input type="hidden" cliente='cliente' v-model="cliente" value="{{ $cliente }}">
            <input type="hidden" codactu='codactu' v-model="codactu" value="{{ $codactu }}">
            <input type="hidden" solicitud='solicitud' v-model="solicitud" value="{{ $solicitud }}">
            
            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-12 btn btn-success" style="width: 100%"><b>Cliente: @{{ nomcliente }}</b></label>
                            <br><br>
                            <div class="panel-group" type="info">
                                <div class="panel panel-default panel-info">
                                    <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a class="accordion-toggle">Prueba Cable Modem</a>
                                    </h4>
                                    </div>
                                    <div class="panel-collapse collapse-transition" style="">
                                      <div class="panel-body">
                                            <div class="col-xs-12">
                                                <div class="box-body table-responsive">
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                            <th class="text-center">Cod. Serv.</th>
                                                            <th class="text-center">Mac</th>
                                                            <th class="text-center">Paquete</th>
                                                            <th class="text-center">Nodo</th>
                                                            <th class="text-center">Troba</th>
                                                            <th class="text-center"> [ - ] </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tb_mac" class="text-center">
                                                            <tr v-for="dato in datos">
                                                                    <td><span class="nombre-detalle">Cod. Serv.: </span> @{{ dato.codserv }}</td>
                                                                    <td><span class="nombre-detalle">Mac: </span> @{{ dato.mac2 }}</td>
                                                                    <td><span class="nombre-detalle">Paquete: </span> @{{ dato.servicepackagecrmid }}</td>
                                                                    <td><span class="nombre-detalle">Nodo: </span> @{{ dato.nodo }}</td>
                                                                    <td><span class="nombre-detalle">Troba: </span> @{{ dato.troba }}</td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-primary btn-sm" @click="PruebaModem(dato.mac2)">Probar</button>
                                                                        <button type="button" class="btn btn-primary btn-sm" @click="modalModem(dato.mac2)"><i class="fa fa-edit fa-lg"></i></button>
                                                                    </td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12" class="tab-pane fade" id="div_detalle" style="display:none">
                                                 <fieldset class="scheduler-border">
                                                    <h4><b>Detalle de Mac: @{{detallemac}}</b></h4>
                                                    <div class="box-body table-responsive">
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" class="text-center">Potencia de la Señal</th>
                                                                <th colspan="2" class="text-center">Relación Señal a Ruido</th>
                                                                <th colspan="3"></th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center">Upstream</th>
                                                                <th class="text-center">Downstream</th>
                                                                <th class="text-center">Upstream</th>
                                                                <th class="text-center">Downstream</th>
                                                                <th colspan="3"></th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center">37,0 a 55,0 dBmV</th>
                                                                <th class="text-center">-5,0 a 10,0 dBmV</th>
                                                                <th class="text-center">27,0 a 99 dB</th>
                                                                <th class="text-center">30,0 a 99 dB</th>
                                                                <th class="text-center">Resultado</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="text-center">
                                                            <tr>
                                                                <td v-for="resultado in resultados" class="tdleft" style="background: inherit; font-size:1em; font-weight: 100;">
                                                                    <span class="nombre-detalle">@{{resultado.campo}} [@{{resultado.medidas}}
                                                                    @{{resultado.metrica}}]: </span>
                                                                    <template v-if="(resultado.estado==1)">
                                                                        <span class="label label-success">@{{resultado.valor}}  @{{resultado.metrica}}</span>
                                                                    </template>
                                                                    <template v-else>
                                                                         <span class="label label-danger">@{{resultado.valor}}  @{{resultado.metrica}}</span>
                                                                    </template>
                                                                </td>
                                                                <td class="tdleft"><span class="nombre-detalle">Resultado: </span>
                                                                     @{{estadoresultado}}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                </fieldset>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @include( 'public.form.cablemodem' )
            </div>
        </div>
        {{ HTML::script('js/cablemodem/cablemodem.js') }}
    </body>
</html>