<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>Embajador</title>
        <script language="JavaScript" type="text/javascript">  
        //--> 
        </script> 


        {{ HTML::script('lib/vue/vue.min.js') }}
        {{ HTML::script('lib/vue/vue-resource.min.js') }}
        {{ HTML::script('js/utils.js') }}

        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('lib/bootstrap-3.3.1/js/bootstrap.min.js') }}
        
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}


        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}
        <style type="text/css">
          .form-control::-webkit-input-placeholder { color: #337ab7; opacity: 0.7 !important; }
          .rojo {
            border-color:#FF0000; background:#F6CECE!important;
          }
          .verde {
            border-color:#E3F6CE; background:#BCF5A9!important;
          }
          .azul {
            color: #337ab7;
          }
          .loading-img{
            z-index:1070;
            background:url(../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
          }
          .overlay{
            z-index:1060;
            background:rgba(255,255,255,.7);
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
          }
          textarea {
            resize: none;
            font-size: 20px!important;
          }
        </style>
        <!--<script type="text/javascript">
          window.location="https://10.252.64.132/gestion101/embajador";
        </script>-->
    </head>
    <body id="embajador">
        <div class="container">

            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                <!--<h1 class="text-center">Embajador STC</h1>-->
                <ul class="nav nav-pills  nav-justified">
                  <li class="active"><a data-toggle="pill" href="#registro">Ficha de Registro</a></li>
                  <li><a data-toggle="pill" href="#consulta">Ficha de Consulta</a></li>
                </ul>

                <div class="tab-content">
                  <div id="registro" class="tab-pane fade in active">
                    <br>
                    <form @submit.prevent="Enviar()" id="ficha_form">
                    <div class="col-sm-12">
                        <div class="panel panel-primary">
                            <div class="panel-body form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="nombre">Producto:</label>
                                    <div class="col-sm-2">
                                        <select class="form-control" @change="changeTipoActividad" v-model="ficha.actividad_tipo_id" required>
                                            <option value="" disabled>Seleccione</option>
                                            <option value="5">Telefono</option>
                                            <option value="6">Internet</option>
                                            <option value="4">Televisión</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2"></div>

                                    <label class="control-label col-sm-2" for="codactu" style="color: #8a6d3b;">Cod Avería:</label>
                                    <div class="col-sm-2 has-warning has-feedback">
                                        <input type="text" id='codactu' class="form-control control-label" placeholder="Código de Avería" v-model="ficha.codactu"                                         
                                          onchange="try{setCustomValidity('')}catch(e){}" onblur="javascript:{this.value = this.value.toUpperCase(); }" required>
                                          <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                    </div>
                                    <div class="col-sm-2"></div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Datos de Cliente</div>
                            <div class="panel-body form-horizontal">
                                
                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="nombre">Apellidos y Nombre:</label>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control" v-model="ficha.cliente_nombre" placeholder="Apellidos y Nombres del cliente" pattern="^\S+\s+.*\S$"
                                        data-toggle="tooltip" data-placement="top" title="Formato: Apellidos Nombres" oninvalid="setCustomValidity('El formato debe tener al menos un apellido y un nombre')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>

                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="celular">Celular:</label>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control" v-model="ficha.cliente_celular" placeholder="Ingrese el celular del cliente" pattern="[8-9]{1,1}[0-9]{8,8}" oninvalid="setCustomValidity('El formato debe empezar con 8 ó 9, y tiene que presentar 9 dígitos')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>
                                    <div v-if="ficha.actividad_tipo_id==4" class="form-group has-warning has-feedback" >
                                        <label class="control-label col-sm-3"">Codigo Cliente:</label>
                                        <div class="col-sm-9">
                                            <input type="tel" class="form-control" v-model="ficha.codcli" placeholder="Ingrese el Codigo Cliente">
                                            <!-- oninvalid="setCustomValidity('Telefono/Internet : Debe tener 8 números')" 
                                          onchange="try{setCustomValidity('')}catch(e){}" required-->
                                            <!--<span class="glyphicon glyphicon-asterisk form-control-feedback"></span>-->
                                        </div>
                                    </div>
                                    <div v-if="ficha.actividad_tipo_id==5 || ficha.actividad_tipo_id==6" class="form-group has-warning has-feedback" >
                                        <label class="control-label col-sm-3">N°Servicio Afectado:</label>
                                        <div class="col-sm-9">
                                            <input type="tel" class="form-control" v-model="ficha.codcli" placeholder="Ingrese el N°Servicio Afectado">
                                            <!-- pattern="[0-9]{8}" oninvalid="setCustomValidity('Telefono/Internet : Debe tener 8 números')" 
                                          onchange="try{setCustomValidity('')}catch(e){}" required -->
                                            <!--<span class="glyphicon glyphicon-asterisk form-control-feedback"></span>-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="correo">Correo:</label>
                                      <div class="col-sm-9">
                                        <input type="email" class="form-control" v-model="ficha.cliente_correo" placeholder="Ingrese el correo del cliente">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="dni">DNI:</label>
                                      <div class="col-sm-9">
                                        <input type="text" pattern="[0-9]{8,8}" class="form-control" v-model="ficha.cliente_dni" placeholder="Ingrese el DNI del cliente" oninvalid="setCustomValidity('El formato debe presentar 8 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                                      </div>
                                    </div>

                            </div><!-- /body -->
                        </div><!-- /panel -->
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-info ">
                            <div class="panel-heading">Datos de Contacto (Opcional)</div>
                            <div class="panel-body form-horizontal">
                                
                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="nombre">Apellidos y Nombre:</label>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control" v-model="ficha.contacto_nombre" placeholder="Apellidos y Nombres de contacto" pattern="^\S+\s+.*\S$"
                                        data-toggle="tooltip" data-placement="top" title="Formato: Apellidos Nombres" oninvalid="setCustomValidity('El formato debe tener al menos un apellido y un nombre')" onchange="try{setCustomValidity('')}catch(e){}">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="celular">Celular:</label>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control" v-model="ficha.contacto_celular" placeholder="Ingrese el celular de contacto" pattern="[8-9]{1,1}[0-9]{8,8}" oninvalid="setCustomValidity('El formato debe empezar con 8 ó 9, y tiene que presentar 9 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="nombre">Telefono:</label>
                                      <div class="col-sm-9">
                                        <input type="tel" class="form-control" v-model="ficha.contacto_telefono" placeholder="Ingrese el telefono de contacto">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="correo">Correo:</label>
                                      <div class="col-sm-9">
                                        <input type="email" class="form-control" v-model="ficha.contacto_correo" placeholder="Ingrese el correo de contacto">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-sm-3" for="celular">DNI:</label>
                                      <div class="col-sm-9">
                                        <input type="text" pattern="[0-9]{8,8}" class="form-control" v-model="ficha.contacto_dni" placeholder="Ingrese el DNI de contacto" oninvalid="setCustomValidity('El formato debe presentar 8 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                                      </div>
                                    </div>

                            </div><!-- /body -->
                        </div><!-- /panel -->
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Datos de Embajador</div>
                            <div class="panel-body form-horizontal">
                                    
                                <div class="col-sm-6">
                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="nombre">Apellidos y Nombre:</label>
                                      <div class="col-sm-9">
                                        <input type="text" class="form-control" v-model="ficha.embajador_nombre" placeholder="Apellidos y Nombres de embajador" pattern="^\S+\s+.*\S$"
                                        data-toggle="tooltip" data-placement="top" title="Formato: Apellidos Nombres" oninvalid="setCustomValidity('El formato debe tener al menos un apellido y un nombre')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>

                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="correo">Correo Corporativo:</label>
                                      <div class="col-sm-9">
                                        <input type="email" class="form-control" v-model="ficha.embajador_correo" placeholder="Ingrese el correo de embajador" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="celular">Celular:</label>
                                      <div class="col-sm-9">
                                        <input type="tel" class="form-control" v-model="ficha.embajador_celular" placeholder="Ingrese el celular de embajador" pattern="[8-9]{1,1}[0-9]{8,8}" oninvalid="setCustomValidity('El formato debe empezar con 8 ó 9, y tiene que presentar 9 dígitos')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>
                                    <div class="form-group has-warning has-feedback">
                                      <label class="control-label col-sm-3" for="celular">DNI:</label>
                                      <div class="col-sm-9">
                                        <input type="text" pattern="[0-9]{8,8}" class="form-control" v-model="ficha.embajador_dni" placeholder="Ingrese el dni de embajador" oninvalid="setCustomValidity('El formato debe presentar 8 dígitos')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                                      </div>
                                    </div>

       
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                      <label for="comment">Comentario:</label>
                                      <textarea class="form-control" rows="5" id="comment" v-model="ficha.comentario"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-12 form-group">
                                    <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                                </div>

                            </div><!-- /body -->
                        </div><!-- /panel -->
                    </div>
                   </form> 
                  </div>
                  <div id="consulta" class="tab-pane fade">
                    <br>
                    <div class="col-sm-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading box box-primary">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                                       href="#collapseOne">Búsqueda</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in ">
                                <div class="panel-body">
                                    <form @submit.prevent="Busqueda" >
                                        <div class="col-xs-8">
                                            <div class="form-group ">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-4">
                                                        <label class="titulo">Buscar por:</label>
                                                        <input type="hidden" name="bandeja" id="bandeja" value="1">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" v-model="tipo_busqueda" @change="changeBusqueda">
                                                            <option value="" selected disabled>Seleccione</option>
                                                            <option value="ticket">Ticket de Pedido</option>
                                                            <option value="codactu">Reg Avería</option>
                                                            <option value="dni">DNI Embajador</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" name="busqueda" id="busqueda" v-model="busqueda" class="form-control" required
                                                        @keyup.enter="submit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <p class="text-info">* Reg Avería realiza la consulta a las averías asociadas a un caso Embajador</p>
                    </div>


                    <div class="box-body">
                      <table class="table table-bordered table-stripe active">
                          <thead class="btn-primary">
                              <th>N° Ticket</th>
                              <th>Tipo Servicio</th>
                              <th>Codactu</th>
                              <th>Cliente</th>
                              <th>Tel/Cod Cliente</th>
                              <th>DNI</th>
                              <th>Embajador</th>
                              <th>Estado Pedido</th>
                              <th>Submotivo</th>
                              <th>Estado Registro</th>
                              <th>Fecha Registro</th>
                              <th>Más Detalle</th>
                          </thead>
                          <tfoot class="btn-primary">
                            <tr>
                              <th>N° Ticket</th>
                              <th>Tipo Servicio</th>
                              <th>Codactu</th>
                              <th>Cliente</th>
                              <th>Tel/Cod Cliente</th>
                              <th>DNI</th>
                              <th>Embajador</th>
                              <th>Estado Pedido</th>
                              <th>Submotivo</th>
                              <th>Estado Registro</th>
                              <th>Fecha Registro</th>
                              <th>Más Detalle</th>
                            </tr>
                        </tfoot>
                          <tbody>
                              <tr v-for="dato in datos">
                                  <td><lavel>@{{ dato.ticket }}</lavel></td>
                                  <td><lavel>@{{ dato.tipo_averia }}</lavel></td>
                                  <td><lavel>@{{ dato.codactu }}</lavel></td>
                                  <td><lavel>@{{ dato.cliente_nombre }}</lavel></td>
                                  <td><lavel>@{{ dato.cliente_telefono }}</lavel> </td>
                                  <td><lavel>@{{ dato.cliente_dni }}</lavel> </td>
                                  <td><lavel>@{{ dato.embajador_nombre }}</lavel> </td>
                                  <td><lavel>@{{ dato.estado_ticket }}</lavel> </td>
                                  <td><lavel>@{{ dato.submotivo }}</lavel> </td>
                                  <td><lavel>@{{ dato.estado }}</lavel> </td>
                                  <td><lavel>@{{ dato.fecha_registro }}</lavel> </td>
                                  <td><button @click.prevent="Cargar(dato.id)" class="btn btn-primary btn-sm"><i class="fa fa-search fa-lg"></i></button></td>
                              </tr>
                              <tr v-show="datos.length == 0">
                                <td colspan="12"><span class="text-center">No se encontraron resultados</span></td>
                              <tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                </section><!-- /.content -->
                @include( 'admin.cat.form.embajador_modal' )
            </div>
        </div>
        {{ HTML::script('js/gestion101/embajador.js') }}
    </body>
</html>
