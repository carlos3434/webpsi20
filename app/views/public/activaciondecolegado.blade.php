<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>Activacion</title>
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}

        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}


        <style type="text/css">
          .loading-img{
            z-index:1070;
            background:url(../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
          }
          .overlay{
            z-index:1060;
            background:rgba(255,255,255,.7);
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
          }
            @media screen and (max-width: 600px) {
                table {width:100%;}
                thead {display: none;}
                tr:nth-of-type(2n) {background-color: inherit;}
                tr td:first-child {background: #f0f0f0; font-weight:bold;font-size:1.3em;}
                tbody td {display: block;  text-align:center;}
                tbody td:before { 
                    content: attr(data-th); 
                    display: block;
                    text-align:center;  
                  }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <input type="hidden" codactu='codactu' v-model="codactu" value="{{ $codactu }}">
            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                <div class="panel panel-primary">
                                <!-- Default panel contents -->
                                <div class="panel-heading">Componentes</div>
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered table-stripe active">
                                            <thead>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Cod. Componente</th>
                                                <th>Material</th>
                                                <th>Serie</th>
                                                <th>Serie Deco</th>
                                                <th>Serie Tarjeta</th>
                                                <th> [ - ] </th>
                                            </thead>
                                            <tbody>
                                                <tr v-for="componente in componentes">
                                                    <td>@{{ componente.marca }}</td>
                                                    <td>@{{ componente.modelo }}</td>
                                                    <td>@{{ componente.componente_cod }}</td>
                                                    <td>@{{ componente.codigo_material }}</td>
                                                    <td>@{{ componente.numero_serie }}</td>
                                                    <td>
                                                        <input type="text" style="min-width:150px" class="form-control" v-model="componente.seriedeco" placeholder="ingrese serie decodificador">
                                                    </td>
                                                    <td>
                                                        <input type="text" style="min-width:150px" class="form-control" v-model="componente.serietarjeta" placeholder="ingrese serie tarjeta">
                                                    </td>
                                                    <td>
                                                        <button :disabled="this.rst == 1" type="button" class="btn btn-primary btn-sm" @click="ActivarDeco(componente)">Activación</button>
                                                        <div v-if="this.rst == 1">
                                                            <span class="glyphicon glyphicon-ok" style="color:#00FF00;"></span>
                                                        </div>
                                                        <div v-if="this.rst == 2">
                                                            <span class="glyphicon glyphicon-remove" style="color:#FF0000;"></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </section>
            </div>
        </div>
        {{ HTML::script('js/legados/componentes/activacion.js') }}
    </body>
</html>
