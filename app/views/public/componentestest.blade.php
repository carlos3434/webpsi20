<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="author" content="Telefonica">
        <meta name="theme-color" content="#000000">
        <meta name="description" content="">
        <title>Activacion</title>
        {{-- {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }} --}}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}
        {{-- {{ HTML::script('js/utils.js') }} --}}
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}


        <style type="text/css">
            .loading-img{
                z-index:1070;
                background:url(../../../../img/admin/ajax-loader1.gif) 50% 50% no-repeat;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
            .overlay{
                z-index:1060;
                background:rgba(255,255,255,.7);
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;}.overlay.dark{background:rgba(0,0,0,.5)
            }
            @media screen and (max-width: 600px) {
                table {
                    width:100%;
                }
                thead {
                    display: none;
                }
                tr:nth-of-type(2n) {
                    background-color: inherit;
                }
                tr td:first-child {
                    background: #f0f0f0; font-weight:bold;font-size:1.3em;
                }
                tbody td {
                    display: block;  text-align:center;
                }
                tbody td:before { 
                    content: attr(data-th); 
                    display: block;
                    text-align:center;  
                }
            }
        </style>
    </head>
    <body>



        <div class="container">
            <input type="hidden" codactu='codactu' v-model="codactu" value="{{ $codactu }}">
            <input type="hidden" usuario='usuario' v-model="usuario" value="{{ $usuario }}">
            <input type="hidden" tipost='tipost' v-model="tipost" value="{{ $tipost }}">
            <input type="hidden" tipolegado='tipolegado' v-model="tipolegado" value="{{ $tipolegado }}">
            <div  style="padding-top: 2em">
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="col-sm-12 btn btn-success" style="width: 100%"><b>Número de Requerimiento: {{ $codactu }}</b></label>
                            <br><br>
                            {{-- <pre>@{{$data|json}}</pre> --}}
                            <accordion :one-at-atime="false" type="info">
                                <panel :hidden="tipost != 2 && tipo != 4" type="primary" class="panel panel-info" header="ASIGNACION DECO">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="panel panel-primary">
                                                    
                                                    <div class="panel-heading">Componentes</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th class="text-center">Descripcion deco</th>
                                                                <th class="text-center">Serie Deco</th>
                                                                <th class="text-center">Descripcion tarjeta</th>
                                                                <th class="text-center">Serie Tarjeta</th>
                                                                <th class="text-center"> [ - ] </th>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <tr v-if="tarjetas.length > 0" v-for="(index, deco) in decos">
                                                                    <td>@{{ deco.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" :disabled="deco.flag == 1" style="min-width:150px" class="form-control" v-model="deco.numero_serie" placeholder="ingrese serie decodificador">
                                                                    </td>
                                                                    <td>@{{ tarjetas[index].descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" :readonly="deco.flag == 1" style="min-width:150px" class="form-control" v-model="tarjetas[index].numero_serie" placeholder="ingrese serie tarjeta">
                                                                    </td>
                                                                    <td>
                                                                        <button :disabled="deco.flag == 1" type="button" class="btn btn-primary btn-sm" @click="OperacionDeco(deco, tarjetas[index], 0)">Activar</button>
                                                                        <div v-if="deco.flag == 1">
                                                                            <span class="glyphicon glyphicon-ok" style="color:#00FF00;"></span>
                                                                        </div>
                                                                        <div v-if="deco.flag == 2">
                                                                            <span class="glyphicon glyphicon-remove" style="color:#FF0000;"></span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div :hidden="modems.length == 0" class="panel panel-danger" style="border-color: #C83333!important;">
                                                    <div class="panel-heading" style="background-color: #C83333!important; color: white!important;">Modem</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th class="text-center">Nombre</th>
                                                                <th class="text-center">Serie Modem</th>
                                                                <th class="text-center"> [ - ] </th>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <tr v-for="modem in modems">
                                                                    <td>@{{ modem.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" :readonly="this.flag == 1 || modem.flag == 1" class="form-control" v-model="modem.numero_serie" placeholder="ingrese serie modem">
                                                                    </td>
                                                                    <td>
                                                                        <button :disabled="this.flag == 1 || modem.flag == 1" type="button" class="btn btn-primary btn-sm" @click="GuardarModem(modem)">Guardar</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div :hidden="lineavoips.length == 0" class="panel panel-danger" style="border-color: #c86b33!important;">
                                                    <div class="panel-heading" style="background-color: #c86b33!important; color: white!important;">Linea VOIP</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th class="text-center">Nombre</th>
                                                                <th class="text-center">Serie Linea</th>
                                                                <th class="text-center"> [ - ] </th>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <tr v-for="lineavoip in lineavoips">
                                                                    <td>@{{ lineavoip.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" :readonly="this.flag == 1 || lineavoip.flag == 1" class="form-control" v-model="lineavoip.numero_serie" placeholder="ingrese serie lineavoip">
                                                                    </td>
                                                                    <td>
                                                                        <button :disabled="this.flag == 1 || lineavoip.flag == 1" type="button" class="btn btn-primary btn-sm" @click="GuardarModem(lineavoip)">Guardar</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div :hidden="equipotelefonicos.length == 0" class="panel panel-danger" style="border-color: #226523!important;">
                                                    <div class="panel-heading" style="background-color: #226523!important; color: white!important;">Equipo Telefonico</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th class="text-center">Nombre</th>
                                                                <th class="text-center">Serie Equipo</th>
                                                                <th class="text-center"> [ - ] </th>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <tr v-for="equipotelefonico in equipotelefonicos">
                                                                    <td>@{{ equipotelefonico.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" :readonly="this.flag == 1 || equipotelefonico.flag == 1" class="form-control" v-model="equipotelefonico.numero_serie" placeholder="ingrese serie equipo telefonico">
                                                                    </td>
                                                                    <td>
                                                                        <button :disabled="this.flag == 1 || equipotelefonico.flag == 1" type="button" class="btn btn-primary btn-sm" @click="GuardarModem(equipotelefonico)">Guardar</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel :hidden="tipost != 2 && tipost != 4" class="panel panel-info" header="DESASIGNACION DECO">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            
                                            <div class="box">
                                                <div class="panel panel-primary">
                                                    
                                                    <div class="panel-heading">Componentes</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th>Descripcion deco</th>
                                                                <th>Serie Deco</th>
                                                                <th>Descripcion tarjeta</th>
                                                                <th>Serie Tarjeta</th>
                                                                <th> [ - ] </th>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="tarjetas.length > 0" v-for="(index, deco) in decos">
                                                                    <td>@{{ deco.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" style="min-width:150px" class="form-control" v-model="deco.numero_serie" placeholder="ingrese serie decodificador">
                                                                    </td>
                                                                    <td>@{{ tarjetas[index].descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" style="min-width:150px" class="form-control" v-model="tarjetas[index].numero_serie" placeholder="ingrese serie tarjeta">
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-primary btn-sm" @click="OperacionDeco(deco, tarjetas[index], 1)">Desasignar</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel :hidden="tipost != 1 && tipost != 3" class="panel panel-info" header="CAMBIAR AVERIA">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <pre>@{{$data|json}}</pre>
                                            <div class="box">
                                                    <list-componentes :componentes.sync="componentes" :tipocomponente.sync="tipocomponente" :datachecked.sync="datachecked" :tipo.sync="tipo"></list-componentes>

                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-4">
                                                                <label>Serie actual @{{ tipocomponente }}</label>
                                                                <input type="text" class="form-control" v-model="datachecked.numero_serie" placeholder="Seleccione componente" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div v-if="tipocomponente == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="trama2.cambiaraveria.codigo_material_new_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.numero_serie_new_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipocomponente == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo materia nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.codigo_material_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.numero_serie_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div v-if="tipocomponente == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="trama2.cambiaraveria.codmatpar_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.numserpar_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipocomponente == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo materia nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.codmatpar_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.cambiaraveria.numserpar_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-12">
                                                            <button type="submit" class="btn btn-primary" @click="OperacionCambioAveria(trama2.cambiaraveria, tipocomponente)">Cambiar</button>
                                                        </div>
                                                    </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel :hidden="tipost != 1 && tipost != 3" class="panel panel-info" header="REVERTIR AVERIA">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-body table-responsive">
                                                    <table class="table table-bordered table-stripe active">
                                                        <thead>
                                                            <th class="text-center">Nombre</th>
                                                            <th class="text-center">Serie</th>
                                                            <th class="text-center"> [ - ] </th>
                                                        </thead>
                                                        <tbody class="text-center">
                                                            <tr v-for="rad in decos">
                                                                <td>@{{ rad.descripcion }}</td>
                                                                <td>
                                                                    <input type="text" class="form-control" v-model="rad.numero_serie" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="rdb_revertir_averia_tipo" value="deco" @click="CapturarTrama(rad, 3, 'deco')">
                                                                </td>
                                                            </tr>
                                                            <tr v-for="rat in tarjetas">
                                                                <td>@{{ rat.descripcion }}</td>
                                                                <td>
                                                                    <input type="text" class="form-control" v-model="rat.numero_serie" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="rdb_revertir_averia_tipo" value="tarjeta" @click="CapturarTrama(rat, 3, 'tarjeta')">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <label>Serie antigua @{{ tipo.revertiraveria }}</label>
                                                            <input type="text" class="form-control" v-model="trama2.revertiraveria.numero_serie_antigua">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12">

                                                        {{-- ****************************************************** --}}

                                                            <div v-if="tipo.revertiraveria == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="trama2.revertiraveria.codigo_material_new_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertiraveria.numero_serie_new_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipo.revertiraveria == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo materia nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertiraveria.codigo_material_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertiraveria.numero_serie_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>

                                                        {{-- ****************************************************** --}}
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        
                                                        <div v-if="tipo.revertiraveria == 'deco'">
                                                            <div class="col-sm-6">
                                                                <label>Codigo material nuevo tarjeta</label>
                                                                <select class="form-control" v-model="trama2.revertiraveria.codmatpar_tarjeta">
                                                                    <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                        @{{ tarjetas.descripcion }}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label>Serie nuevo tarjeta</label>
                                                                <input type="text" class="form-control" v-model="trama2.revertiraveria.numserpar_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                            </div>
                                                        </div>

                                                        <div v-if="tipo.revertiraveria == 'tarjeta'">
                                                            <div class="col-sm-6">
                                                                <label>Serie nuevo deco</label>
                                                                <input type="text" class="form-control" v-model="trama2.revertiraveria.codmatpar_deco" placeholder="Ingrese serie nuevo deco">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label>Material equipo nuevo deco</label>
                                                                <input type="text" class="form-control" v-model="trama2.revertiraveria.numserpar_deco" placeholder="Ingrese serie nuevo deco">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary" @click="OperacionRevertirAveria(trama2.revertiraveria, tipo.revertiraveria)">Revertir</button>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel :hidden="tipost != 1 && tipost != 3" class="panel panel-info" header="REPOSICION">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                    <list-componentes :componentes.sync="componentes" :tipocomponente.sync="tipocomponente" :datachecked.sync="datachecked" :tipo.sync="tipo"></list-componentes>

                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-4">
                                                                <label>Serie antigua @{{ tipocomponente }}</label>
                                                                <input type="text" class="form-control" v-model="datachecked.numero_serie" placeholder="Seleccione componente" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">

                                                            {{-- ****************************************************** --}}

                                                            <div v-if="tipocomponente == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="datachecked.codigo_material_new_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.numero_serie_new_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipocomponente == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo materia nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.codigo_material_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.numero_serie_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>

                                                            {{-- ****************************************************** --}}

                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div v-if="tipocomponente == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="datachecked.codmatpar_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.numserpar_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipocomponente == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.codmatpar_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="datachecked.numserpar_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-12">
                                                                <button type="submit" class="btn btn-primary" @click="OperacionReposicion()">Reponer</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel :hidden="tipost != 1 && tipost != 3" class="panel panel-info" header="REVERTIR REPOSICION">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-body table-responsive">
                                                    <table class="table table-bordered table-stripe active">
                                                        <thead>
                                                            <th class="text-center">Nombre</th>
                                                            <th class="text-center">Serie</th>
                                                            <th class="text-center"> [ - ] </th>
                                                        </thead>
                                                        <tbody class="text-center">
                                                            <tr v-for="rrd in decos">
                                                                <td>@{{ rrd.descripcion }}</td>
                                                                <td>
                                                                    <input type="text" class="form-control" v-model="rrd.numero_serie" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="rdb_revertir_reposicion_tipo" value="deco" @click="CapturarTrama(rrd, 5, 'deco')">
                                                                </td>
                                                            </tr>
                                                            <tr v-for="rrt in tarjetas">
                                                                <td>@{{ rrt.descripcion }}</td>
                                                                <td>
                                                                    <input type="text" class="form-control" v-model="rrt.numero_serie" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="radio" name="rdb_revertir_reposicion_tipo" value="tarjeta" @click="CapturarTrama(rrt, 5, 'tarjeta')">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <label>Serie antigua @{{ tipo.revertirreposicion }}</label>
                                                            <input type="text" class="form-control" v-model="trama2.revertirreposicion.numero_serie_antigua">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-sm-12">

                                                        {{-- ****************************************************** --}}

                                                            <div v-if="tipo.revertirreposicion == 'tarjeta'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo material nuevo tarjeta</label>
                                                                    <select class="form-control" v-model="trama2.revertirreposicion.codigo_material_new_tarjeta">
                                                                        <option v-for="tarjetas in tarjetasAlmacen" v-bind:value="tarjetas.codtar" selected>
                                                                            @{{ tarjetas.descripcion }}
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo tarjeta</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertirreposicion.numero_serie_new_tarjeta" placeholder="Ingrese serie nuevo tarjeta">
                                                                </div>
                                                            </div>

                                                            <div v-if="tipo.revertirreposicion == 'deco'">
                                                                <div class="col-sm-6">
                                                                    <label>Codigo materia nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertirreposicion.codigo_material_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <label>Serie nuevo deco</label>
                                                                    <input type="text" class="form-control" v-model="trama2.revertirreposicion.numero_serie_new_deco" placeholder="Ingrese serie nuevo deco">
                                                                </div>
                                                            </div>

                                                        {{-- ****************************************************** --}}

                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-primary" @click="OperacionRevertirReposicion(trama2.revertirreposicion, tipo.revertirreposicion)">Revertir</button>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                                <panel class="panel panel-info" header="REFRESH">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            
                                            <div class="box">
                                                <div class="panel panel-primary">
                                                    
                                                    <div class="panel-heading">Componentes</div>
                                                    <div class="box-body table-responsive">
                                                        <table class="table table-bordered table-stripe active">
                                                            <thead>
                                                                <th>Descripcion deco</th>
                                                                <th>Serie Deco</th>
                                                                <th>Descripcion tarjeta</th>
                                                                <th>Serie Tarjeta</th>
                                                                <th> [ - ] </th>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="tarjetas.length > 0" v-for="(index, deco) in decos">
                                                                    <td>@{{ deco.descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" style="min-width:150px" class="form-control" v-model="deco.numero_serie" placeholder="ingrese serie decodificador">
                                                                    </td>
                                                                    <td>@{{ tarjetas[index].descripcion }}</td>
                                                                    <td>
                                                                        <input type="text" style="min-width:150px" class="form-control" v-model="tarjetas[index].numero_serie" placeholder="ingrese serie tarjeta">
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-primary btn-sm" @click="OperacionDeco(deco, tarjetas[index], 6)">Refresh</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </panel>
                            </accordion>
                            
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <template id="deta_componentes">
            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label>DECOFIDICADOR</label>
                        <input type="radio" name="rdb_cambio_averia_type" value="deco" v-model="tipocomponente" @click="limpiarCheck('rdb_cambio_averia')">
                    </div>
                    <div class="col-sm-3">
                        <label>TARJETA</label>
                        <input type="radio" name="rdb_cambio_averia_type" value="tarjeta" v-model="tipocomponente" @click="limpiarCheck('rdb_cambio_averia')">
                    </div>
                </div>
            </div>

            <div class="box-body table-responsive">
                <table class="table table-bordered table-stripe active">
                    <thead>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Serie</th>
                        <th class="text-center"> [ - ] </th>
                    </thead>
                    <tbody class="text-center">
                        <tr v-for="componente in componentes">
                            <td>@{{ componente.descripcion }}</td>
                            <td>
                                <input type="text" class="form-control" v-model="componente.numero_serie">
                            </td>
                            <td>
                                <input type="radio" name="rdb_cambio_averia" @click="CapturarTrama(componente, tipocomponente)">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </template>

        {{ HTML::script('js/legados/componentes/operacionestest.js') }}
    </body>
</html>
