<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Analizador</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
    </head>
    <body id="componenteController" class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
        <div class="wrapper">

        <section class="content-header">
            <h1>
                Analizador
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
             <div class="box-header with-border">
                  <div class="box-title">Obtener Pruebas</div>
              </div>
              <div class="box-body">
                <form class="form-inline" style="margin-bottom: 10px;" id="form_analizador">
                    <div class="form-group">
                      <div class="col-sm-8">
                          <label>Número</label>
                          <input class="form-control" type="text" id="txt_telefono" name="txt_telefono">
                      </div>
                      <div class="col-sm-3">
                          <label>&nbsp;</label>
                          <input class="form-control btn-success" type="button" id="btn_obtener" name="btn_obtener" value="Buscar" onclick="ObtenerPruebas()">
                      </div>
                    </div>
                    <div class="form-group" id="divpruebas" style="display:none">
                      <div class="col-sm-6">
                          <label>Pruebas</label>
                          <div id="divoption">
                          <!--<select class="form-control" name="slct_pruebas"><option value='' selected="">.::Seleccione::.</option></select>-->  
                          </div>
                      </div>
                    </div>
                    <div class="form-group" id="divresultado" style="display:none">
                        <div class="col-sm-10 table-responsive">
                            <label>Resultado</label>
                            <textarea rows="15" cols="70" id="txt_resultado" style="background: black; color: white; resize: none;">
                            </textarea>
                        </div>
                    </div>
                </form>
                <hr>
              </div>
            </div>
                    
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script('js/analizador/analizador.js') }}
        {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
        {{ HTML::script("lib/jquery-ui-1.11.2/jquery-ui.js") }}
        <!--{{ HTML::script('js/geo/buscarcomponentestecnico_ajax.js') }}-->
    @show


    </body>
</html>