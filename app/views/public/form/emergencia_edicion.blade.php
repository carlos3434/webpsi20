<modal :show.sync="showModalEdicion" large>

  <div slot="modal-header" class="modal-header">
    <button type="button" class="close" @click="showModalEdicion = false"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Estado Planta:</h4>
  </div>

  <div slot="modal-body" class="modal-body">

    <div class="alert alert-danger" transition="danger" v-if="danger">
      <ul>
        <li v-for="error in errores">
          @{{ error }}
        </li>
      </ul>
    </div>
    <div>
        <form action="#" @submit.prevent="actualizacionEmergencia">
            <div class="box-body">
                <div class="row form-group">

                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <label class="control-label">Observacion
                            </label>
                            <input type="text" class="form-control" v-model='imagen.obs'>
                        </div>
                    </div>
                </div>
                    
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <label class="control-label">X (Longitud)
                            </label>
                            <input type="text" class="form-control" v-model='imagen.x'>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Y (Latitud)
                            </label>
                            <input type="text" class="form-control" v-model='imagen.y'>
                        </div>
                        
                        <!-- <div class="col-sm-3">
                            <label class="control-label">DNI
                                <a id="error_dni" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese DNI">
                                    <i class="fa fa-exclamation"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" placeholder="Ingrese DNI" name="txt_dni" id="txt_dni" maxlength="8">
                        </div>
                        <div class="col-sm-3">
                            <label class="control-label">Estado:
                            </label>
                            <select class="form-control" name="slct_estado" id="slct_estado">
                                <option value='0'>Inactivo</option>
                                <option value='1' selected>Activo</option>
                            </select>
                        </div> -->
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="row form-group">
                <div  class="modal-footer">
                    <button type="button" class="btn btn-default" @click="showModal = false">Close</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
        </form>
    </div>

  </div>
  <div v-show="false" slot="modal-footer" class="modal-footer"></div>
</modal>