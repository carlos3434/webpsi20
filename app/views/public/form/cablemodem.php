<!-- /.modal -->
<div class="modal fade" id="CablemodemModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-xs">
      <div class="modal-content">
         <div class="modal-header logo">
            <button type="button" class="btn btn-sm btn-default pull-right" onclick="vm.funcionClose()">
               <i class="fa fa-close"></i>
            </button>
            <h4 class="modal-title">Modificar MAC</h4>
         </div>
         <div class="modal-body">
            <form id="form_mac" name="form_mac" action="" method="post">
               <div class="row form-group">
                  <div class="col-sm-12">
                        <label>Antigua Mac</label>
                        <input class="form-control" antiguamac='antiguamac' v-model="antiguamac" value="{{ antiguamac }}" readonly="">
                        <label>Nueva Mac</label>
                        <input class="form-control" nuevamac='nuevamac' v-model="nuevamac" value="{{ nuevamac }}" placeholder="Ej: 001dd1b84522" minlength="12" maxlength="12">
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="vm.funcionClose()">Close</button>
                <button type="button" class="btn btn-primary" onclick="vm.modificarMac()">Guardar</button>
         </div>
      </div>
   </div>
</div>