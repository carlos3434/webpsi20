<modal :show.sync="showModalAsistencia" large>

  <div slot="modal-header" class="modal-header">
    <button type="button" class="close" @click="showModalAsistencia = false"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Asistencia de Tecnicos - Bucket :  @{{buckettext}}</h4>
  </div>

  <div slot="modal-body" class="modal-body">

    <div class="alert alert-danger" transition="danger" v-if="danger">
      <ul>
        <li v-for="error in errores">
          @{{ error }}
        </li>
      </ul>
    </div>
    <form action="#" @submit.prevent="guardarConformidadAsistencia(pestaniaDos)">
        <div class="box-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th rowspan="2" style="text-align: center; vertical-align: middle; ">Recurso</th>
                        <th colspan="2" style="text-align: center;">Activacion Ruta</th>
                        <th colspan="2" style="text-align: center;">Ubicacion Recurso</th>
                        <th rowspan="2" style="text-align: center; vertical-align: middle;">Marca Manual</th>
                    </tr>
                        <tr>
                            <th style="text-align: center;">OT</th>
                            <th style="text-align: center;">TOA</th>
                            <th style="text-align: center;">OT</th>
                            <th style="text-align: center;">TOA</th>
                        </tr>
                </thead>
                    <tbody>
                        <template v-for="(item, index) in tecnicos">
                            <tr>
                                <td>@{{item}} <i class="fa fa-info btn-xs btn-primary"></i></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button v-if = "@{{index.xOt == '' || index.yOt == ''}}" alt="" type="button" disabled="disabled" class="btn-xs btn-success"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;"></i>Ubicacion OT</button>
                                    <button v-else class="btn-xs btn-success" @click.prevent="verTecnicoMapa(index.xOt,index.yOt)"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;"></i>Ubicacion OT</button>
                                </td>
                                <td>
                                    <button v-if = "@{{index.xToa == '' || index.yToa == ''}}" alt="" type="button" disabled="disabled" class="btn-xs btn-success"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;"></i>Ubicacion Toa</button>
                                    <button v-else class="btn-xs btn-success" @click.prevent="verTecnicoMapa(index.xToa,index.yToa)"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;"></i>Ubicacion Toa</button>
                                </td>
                                <td>
                                    <select class="form-control" name="asistencia[@{{item}}]">
                                        <option value="">¿Asistio?</option>
                                        <option value="1">SI</option>
                                        <option value="0">NO</option>
                                    </select>
                                </td>
                            </tr>
                        </template>
                    </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="row form-group">
                    <div  class="modal-footer">
                        <button type="button" class="btn btn-default" @click="showModalAsistencia = false">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
        </div>
    </form>
  </div>
  <div v-show="false" slot="modal-footer" class="modal-footer"></div>
</modal>