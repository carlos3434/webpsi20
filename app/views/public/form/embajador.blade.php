<!-- Modal -->
<div id="modal_embajador" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Resultado de la Búsqueda</h4>
      </div>
      <div class="modal-body" id="resultado">

        <!-- RESUMEN -->
        <div class="panel panel-primary">
          <div class="panel-body form-horizontal">
              <div class="row">
                  <div class="col-sm-3">
                      <label>N° Ticket</label>
                      <p>@{{ modal.ticket }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Estado Ticket</label>
                      <p>@{{ modal.estado }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Fecha Registro Ticket</label>
                      <p>@{{ modal.created_at }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Producto</label>
                      <p>@{{ modal.tipo_averia }}</p>
                  </div>
              </div>
              <div class="row">
                  <div class="col-sm-3">
                      <label>Cod Avería</label>
                      <p>@{{ modal.codactu }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Estado Avería</label>
                      <p>@{{ modal.estado_legado }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Fecha Registro Avería</label>
                      <p>@{{ modal.fecha_registro_legado }}</p>
                  </div>
                  <div class="col-sm-3">
                      <label>Plazo</label>
                      <p>@{{ modal.plazo }}</p>
                  </div>
              </div>
          </div>
        </div>
          
        <div class="form-group" id="ficha_results" v-if="datos_ficha==true">
          <h4><p class="bg-primary text-center">Datos de Ficha de Pedido</p></h4>

          <div class="row form-group">
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <label>Estado</label>
                    <input type="text" class="form-control input-sm" v-model="modal.estado" readonly>
                </div>
                <div class="col-sm-3">
                    <label>Producto</label>
                    <input type="text" class="form-control input-sm" v-model="modal.tipo_averia" readonly>
                </div>
                <div class="col-sm-3">
                    <label>Cod Averia</label>
                    <input type="text" class="form-control input-sm" v-model="modal.codactu" readonly>
                </div>
                <div class="col-sm-3">
                    <label class="text-primary">N° Ticket</label>
                    <input type="text" class="form-control input-sm azul" v-model="modal.ticket" readonly>
                </div>
            </div>
          </div>

          <!-- datos de cliente -->
          <div class="form-group panel-group">
            <div class="panel panel-success">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#p_cliente"> Datos del Cliente</a>
                </h4>
              </div>
              <div id="p_cliente" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-sm-12">
                    <div class="col-sm-4">
                        <label>Nombre de cliente</label>
                        <input type="text" class="form-control input-sm" v-model="modal.cliente_nombre" readonly>
                    </div>
                    <div class="col-sm-4">
                        <label>Celular de Cliente</label>
                        <input type="text" class="form-control input-sm" v-model="modal.cliente_celular" readonly>
                    </div>
                    <div class="col-sm-4">
                        <label>Telefono</label>
                        <input type="text" class="form-control input-sm" v-model="modal.cliente_telefono" readonly>
                    </div>
                    <div class="col-sm-4">
                        <label>Correo de cliente</label>
                        <input type="text" class="form-control input-sm" v-model="modal.cliente_correo" readonly>
                    </div>
                    <div class="col-sm-4">
                        <label>DNI</label>
                        <input type="text" class="form-control input-sm" v-model="modal.cliente_dni" readonly>
                    </div>
                    <div class="col-sm-4">
                        <label>Cod Cliente</label>
                        <input type="text" class="form-control input-sm" v-model="modal.codcli" readonly>
                     </div>
                </div>

                </div>
              </div>
            </div>
          </div><!--/datos de cliente -->

          <!-- movimientos -->
          <div class="form-group panel-group">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#p_movimientos"><i class="fa fa-caret-down"></i> Movimientos</a>
                </h4>
              </div>
              <div id="p_movimientos" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-sm-12">
                      <table id="t_movimiento" class="table table-bordered table-striped text-center">
                          <thead class="btn-primary">
                              <tr>
                                  <th class="text-center">Motivo</th>
                                  <th class="text-center">Submotivo</th>
                                  <th class="text-center">Estado Registro</th>
                                  <th class="text-center">Observación</th>
                                  <th class="text-center">Fecha</th>
                                  <th class="text-center">Usuario</th>
                                  <th class="text-center">Solución</th>
                              </tr>
                          </thead>
                          <tbody id="tb_movimiento">
                              <tr v-for="dato in movimientos">
                                  <td><lavel>@{{ dato.motivo }}</lavel></td>
                                  <td><lavel>@{{ dato.submotivo }}</lavel></td>
                                  <td><lavel>@{{ dato.estado }}</lavel></td>
                                  <td><lavel>@{{ dato.observacion }}</lavel></td>
                                  <td><lavel>@{{ dato.fecha }}</lavel> </td>
                                  <td><lavel>@{{ dato.usuario }}</lavel> </td>
                                  <td><lavel>@{{ dato.solucionado }}</lavel> </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- movimientos -->

          <!-- datos de contacto -->
          <div class="form-group panel-group">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#p_contacto"><i class="fa fa-caret-down"></i> Datos de Contacto</a>
                </h4>
              </div>
              <div id="p_contacto" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-sm-12">
                      <div class="col-sm-4">
                          <label>Nombre de Contacto</label>
                          <input type="text" class="form-control input-sm" v-model="modal.contacto_nombre" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>Celular</label>
                          <input type="text" class="form-control input-sm" v-model="modal.contacto_celular" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>Telefono</label>
                          <input type="text" class="form-control input-sm" v-model="modal.contacto_telefono" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>Correo</label>
                          <input type="text" class="form-control input-sm" v-model="modal.contacto_correo" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>DNI</label>
                          <input type="text" class="form-control input-sm" v-model="modal.contacto_dni" readonly>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- datos de contacto -->

          <!-- datos de embajador -->
          <div class="form-group panel-group">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#p_embajador"><i class="fa fa-caret-down"></i> Datos de Embajador</a>
                </h4>
              </div>
              <div id="p_embajador" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-sm-12">
                      <div class="col-sm-4">
                          <label>Nombre de embajador</label>
                          <input type="text" class="form-control input-sm" v-model="modal.embajador_nombre" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>Celular embajador</label>
                          <input type="text" class="form-control input-sm" v-model="modal.embajador_celular" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>Correo de embajador</label>
                          <input type="text" class="form-control input-sm" v-model="modal.embajador_correo" readonly>
                      </div>
                      <div class="col-sm-4">
                          <label>DNI Embajador</label>
                          <input type="text" class="form-control input-sm" v-model="modal.embajador_dni" readonly>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!--/datos de embajador -->

          <div class="row form-group">
            <div class="col-sm-12">
                <label>Comentario</label>
                <textarea rows="4" cols="50" class="form-control" v-model="modal.comentario" readonly></textarea>
            </div>
          </div>
        </div><!-- ficha_results -->

        <div class="text-center" v-else>No cuenta con datos Ficha de Pedido</div>

        
        <!-- Averia -->
        <div class="form-group" id="averia_results" v-if="datos_averia==true">
            <h4><p class="bg-primary text-center">Datos de Avería</p></h4>
            <div class="row form-group">
              <div class="col-sm-12">
                  <div class="col-sm-4">
                      <label>Plazo</label>
                      <input type="text" class="form-control input-sm" v-bind:class="classPlazo" v-model="modal.plazo" readonly>
                  </div>
                  <div class="col-sm-4">
                      <label>Estado Legado</label>
                      <input type="text" class="form-control input-sm" v-model="modal.estado_legado" readonly>
                  </div>
                  <div class="col-sm-4">
                      <label>Fecha de Registro Legado</label>
                      <input type="text" class="form-control input-sm" v-model="modal.fecha_registro_legado" readonly>
                  </div>
              </div>

              <div class="col-sm-12">
                  <div class="col-sm-4">
                      <label>Cod de Avería</label>
                      <input type="text" class="form-control input-sm" v-model="modal.codactu" readonly>
                  </div>
                  
                  <div class="col-sm-4">
                      <label>Cliente</label>
                      <input type="text" class="form-control input-sm" v-model="modal.nombre_cliente" readonly>
                  </div>
              </div>

              <div class="col-sm-12">
                  <div class="col-sm-4">
                      <label class="text-primary">Area</label>
                      <input type="text" class="form-control input-sm azul" v-model="modal.area" readonly>
                  </div>
                  <div class="col-sm-4">
                      <label class="text-primary">Estacion</label>
                      <input type="text" class="form-control input-sm" style="color: #337ab7;" v-model="modal.area" readonly>
                  </div>
                  
              </div>

              <div class="col-sm-12">
                  <div class="col-sm-4">
                      <label>Contrata</label>
                      <input type="text" class="form-control input-sm" v-model="modal.contrata" readonly>
                  </div>
                  <div class="col-sm-4">
                      <label>Quiebre</label>
                      <input type="text" class="form-control input-sm" v-model="modal.quiebre" readonly>
                  </div>

                  <div class="col-sm-4">
                      <label>Zonal</label>
                      <input type="text" class="form-control input-sm" v-model="modal.zonal" readonly>
                  </div>
              </div>
            </div>
        </div><!--/Averia -->
        <div class="text-center" v-else>No cuenta con datos de Avería</div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        </div>         
      </div><!--/modal-body-->

    </div><!--/modal-content -->

  </div>
</div><!--/modal-->