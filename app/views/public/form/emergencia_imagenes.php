<modal :show.sync="showModal" large>

  <div slot="modal-header" class="modal-header">
    <button type="button" class="close" @click="showModal = false"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Imagenes:</h4>
  </div>

  <div slot="modal-body" class="modal-body">

    <div class="alert alert-danger" transition="danger" v-if="danger">
      <ul>
        <li v-for="error in errores">
          {{ error }}
        </li>
      </ul>
    </div>
    <div>
        <carousel v-for="imagen of imagenes">
            <slider>
                <img :src="imagen.nombre">
                <div class="carousel-caption">
                  ...
                </div>
            </slider>
        </carousel>
    </div>

  </div>
  <div v-show="false" slot="modal-footer" class="modal-footer"></div>
</modal>