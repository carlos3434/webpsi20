<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Actualizar XY</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }} 
        {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('css/admin/admin.min.css') }}
        {{ HTML::style('css/sweetalert/sweetalert.css') }}
        {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&libraries=places,geometry,drawing') }}
        {{ HTML::script('js/geo/geo.functions.js') }}
        {{ HTML::script('js/geo/markerwithlabel.js') }}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js') }}
        {{ HTML::script('js/psigeo.js') }}

  </head>
  <style>
    #update_map, #update_sv {
        width: 100%;
        height: 550px;
    }
    #target {
        width: 345px;
    }
    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 250px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
  </style>
  <body id="componenteController" class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
    <div class="wrapper">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="content-header">
            <h1>
                Actualizar Coordenadas
            </h1>
        </section>
        <section class="content">
            <div class="row">
            <div class="box box-primary">
             <div class="box-header with-border">
                  <div class="box-title">  </div> <!-- nombre -->
              </div>
              <div class="box-body">
                <form style="" id="">
                    <div class="row">
                      <div class="col-md-2">
                        <label>Solicitud</label>
                        <input class="form-control" type="text" solicitud='solicitud' v-model="solicitud" value="{{ $solicitud }}" readonly="">
                      </div>
                      <div class="col-md-2">
                        <label>CodActu</label>
                        <input class="form-control" type="text" codactu='codactu' v-model="codactu" value="{{ $codactu }}" readonly="">
                      </div>
                      <div class="col-md-6">
                        <label>Direccion</label>
                        <input class="form-control" type="text" readonly="" value="@{{datos.tipo_via}} @{{datos.nombre_via}} @{{datos.numero_via}} PISO @{{datos.piso}} INT @{{datos.interior}} MNZ @{{datos.manzana}} LT @{{datos.lote}} @{{datos.tipo_urbanizacion}}: @{{datos.desc_urbanizacion}}" style="">
                      </div> 
                      <div class="col-md-2 col-sm-12 col-xs-12" style="margin-top:25px;">
                        <a class="btn btn-danger" @click="LimpiarMapa()" id="btn_limpiar" style="width:100%">
                            <i class="fa fa-undo fa-lg"></i> Limpiar
                        </a>
                      </div> 
                      <div class="col-md-4">
                        <label>Referencia</label>
                        <input class="form-control" type="text" id="txt_refencia" readonly="" value="" style="">
                      </div>
                      <div class="col-md-2">
                        <label>Coord X</label>
                        <input class="form-control" type="text" coordx='coordx' v-model="coordx" value="@{{coordx}}" readonly="">
                      </div>
                      <div class="col-md-2">
                        <label>Coord Y</label>
                        <input class="form-control" type="text" coordy='coordy' v-model="coordy" value="@{{coordy}}" readonly="">
                      </div>
                      <div class="col-md-2 col-sm-12 col-xs-12" style="margin-top:25px;">
                        <a class="btn btn-primary" @click="ObtenerLocalizacion()" style="width:100%">
                            <i class="fa fa-location-arrow fa-lg"></i> Mi Ubicación
                        </a>
                      </div>
                      <div class="col-md-2 col-sm-12 col-xs-12" style="margin-top:25px;">
                        <a class="btn btn-success" @click="SaveData()" id="btn_actualizar" style="width:100%">
                            <i class="fa fa-save fa-lg"></i> Actualizar
                        </a>
                      </div>
                    </div>
                </form>
                <hr>
              </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                    <div id="update_maps"style="position: relative; overflow: block; min-height:600px"></div>
                </div>
                <div class="col-sm-6">
                    <div id="update_sv"></div>
                </div>
            </div>
        </section>
      </div>
    </div>
    @section('javascript')
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('js/utils.js') }}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script("lib/jquery-ui-1.11.2/jquery-ui.js") }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        <!--{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}-->
        {{ HTML::script('js/geo/actualizaxy.js') }}
    @show
  </body>
</html>