<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Buscar Tecnicos</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array('async' => 'async')) }}
        {{ HTML::style('css/public/mapatecnico.css') }}
    </head>
    <body id='mapatecnicosController'>
        <spinner id="spinner-box" :size="size" :fixed="fixed" v-show="loaded" text="Espere un momento por favor">
        </spinner>
        <script type='text/javascript'>publicurl = '<?php echo url();?>';</script>
        <alert id="mensaje" type="danger" v-if="mensaje!=''">
            <strong>@{{mensaje}}</strong>
        </alert>
        <div id="map" ></div>
        <script type="text/javascript">
            var hoy = '{{ date("Y-m-d") }}';
            var ahora = '{{ date("Y-m-d H:i:s") }}';
        </script>

            <div id='flotante' style="display:none; overflow: hidden;"> 
                <div class="aside slideright-transition right">
                    <div class="aside-dialog">
                        <div class="aside-content">
                            <div class="aside-header">
                            <a  @click.prevent="cerrarright"  class="close">×</a>
                            <h4 class="aside-title">Tecnicos</h4>
                            </div>
                            <div class="aside-body">
                            <div class="box box-solid">
                                <div class="box-body">
                                    <span class="ppart" style="background-color: #A5D6A7; padding: 5px 5px 5px 5px;">20 minutos</span>
                                    <span class="ppart" style="background-color: #FFCC80; padding: 5px 5px 5px 5px;">2 horas</span>
                                    <span class="ppart" style="background-color: #EF9A9A; padding: 5px 5px 5px 5px;">4 horas</span>
                                    <span class="ppart" style="background-color: #9FA8DA; padding: 5px 5px 5px 5px;">+ 4 horas</span>
                                </div>
                            </div>   
                            <div class="scroll">
                                <div class="aside-body row form-group" id="tecnicos">
                                </div>
                            </div>
                            
                            <div class="aside-footer">
                            </div></div>
                        </div>
                    </div>
                </div>
            </div>

              <div id='f_actividades' style="display:none; overflow: hidden;"> 
                <div class="aside slideright-transition right">
                    <div class="aside-dialog">
                        <div class="aside-content">
                            <div class="aside-header">
                            <a  @click.prevent="cerrarright_actividades"  class="close">×</a>
                            <h4 class="aside-title">Actividades</h4>
                            </div>
                            <div class="aside-body">
                                <div class="box box-solid">
                                    <div class="box-body">
                                        <span class="ppart" style="background-color: #A5D6A7; padding: 5px 5px 5px 5px;">20 minutos</span>
                                        <span class="ppart" style="background-color: #FFCC80; padding: 5px 5px 5px 5px;">2 horas</span>
                                        <span class="ppart" style="background-color: #EF9A9A; padding: 5px 5px 5px 5px;">4 horas</span>
                                        <span class="ppart" style="background-color: #9FA8DA; padding: 5px 5px 5px 5px;">+ 4 horas</span>
                                    </div>
                                </div>   
                                <div class="scroll">
                                    <div class="aside-body row form-group" id="tecnicos">
                                    </div>
                                </div>
                                
                                <div class="aside-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}
        {{ HTML::script('js/fontawesome-markers.min.js') }}
        {{ HTML::script('js/mapas/routing.js') }}
        {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&callback=initMap') }}
        {{ HTML::script('js/geo/markerwithlabel.js' ) }}

    </body>
</html>