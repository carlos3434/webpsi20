<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Buscar Tecnicos</title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        <meta name="theme-color" content="#3c8dbc">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array('async' => 'async')) }}
        {{ HTML::style('css/public/mapatecnico.css') }}
        {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css')}}
        {{ HTML::style('lib/geo_css/colpick.css') }}
        {{ HTML::script('lib/jquery-2.1.3.min.js') }} 
        {{ HTML::script('lib/geo_js/colpick.js') }}
        {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }} 
        <style type="text/css">
            #modalestadistico .tab-content>.tab-pane {visibility: visible !important;}
            #modalestadistico .modal-content {overflow-y: scroll; max-height: 600px;}
            fieldset{
              max-width: 100% !important;
              border: 1px solid #A3A1A1;
              padding:6px 20px 6px 20px;
              border-radius: 10px; 
            }

            legend{
              font-size:14px;
              font-weight: 700;
              width: 20%;
              border-bottom: 0px;
              margin-bottom: 5px;
              color: #A3A1A1
            }
            .btn-gray{
              color: #508d76;
              background-color: #f0f0f0;
              border-color: #ccc;
              font-weight: bold;
            }
            .btn-gray:hover{
              color: #259f71;
              background-color: #E0E8E4;
              border-color: #4DAD71;
            }
        </style>
    </head>

    <body id='mapatecnicosController'>
        <spinner :size="size" :fixed="fixed" v-show="loaded" text="Espere un momento por favor">
        </spinner>
        <alert id='mensaje' type="danger" v-if="mensaje!=''">
            <strong>@{{mensaje}}</strong>
        </alert>
        <div v-show="!showLeft" class="botones form-group">
            <div class="row">
                <div class="col-md-12">
                    <button v-show="!showLeft" class="btn btn-success btn-sm" @click="showLeft = true">
                    <i class="fa fa-search" style="margin-right: 5px;"></i> Filtros
                    </button>
                </div>
                <div class="col-md-12">
                    <button  class="btn btn-success btn-sm" @click="showRight = !showRight">
                    Tecnicos
                    </button>
                </div>
                <div class="col-md-12" id="pintaMapa" style="display: none;">
                    <button type="button" @click="showLeft = false" @click.prevent="clickOpciones(pintarMapa,true)" class="btn btn-success btn-sm">Pintar Rutas</button>

                </div>
                <div class="col-md-12" id="refrescarActividades">
                    <button class="btn btn-danger btn-sm" type="button" @click="refrescarActividades"><i class="fa fa-refresh"></i> Actualizar Actividades</button>
                </div>               
            </div>
        </div>
        <script type="text/javascript">
            var hoy = '{{ date("Y-m-d") }}';
            var ahora = '{{ date("Y-m-d H:i:s") }}';
            var long_bucket = '{{ $long_bucket }}';
            var lat_bucket = '{{ $lat_bucket }}';
            var entorno = '{{$entorno}}';
            var urlMapsbusquedaXY = "http://maps.google.com/maps?q=";
            var buckettext = "";
        </script>
        <aside :show.sync="showLeft" placement="left" header="Filtros" :width="550">
            <div class="aside-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fila">
                            <div class="col-md-4">
                                <label for="actividadesTipo">Tipo de Recursos:</label>
                            </div>
                            <div class="col-md-8">
                                <v-select v-bind:value="actividadesTipoSeleccionados" multiple :disabled="checked ? true : false">
                                <v-option v-for="(key, val) in actividadesTipo" v-bind:value="key">@{{ val }}</v-option>
                                </v-select>
                            </div>
                        </div>
                        <div class="fila">
                            <div class="col-md-4">
                                <label for="actividadesTipo">Bucket:</label>
                            </div>
                            <div class="col-md-8">
                                <v-select :value.sync="resource_id" multiple placeholder='ningun bucket'>
                                    <v-option  @click="selectBucket(resource)" v-for="resource in resources_id" v-bind:value="resource.id">@{{ resource.name }}</v-option>
                                </v-select>
                            </div>
                        </div>
                        <div class="fila">
                            <div class="col-md-4">
                                <label for="tecnicos">Tecnicos</label>
                            </div>
                            <div class="col-md-8">
                                <v-select v-bind:value="tecnicosSeleccionados" multiple placeholder='ningun tecnico'>
                                    <v-option @click="selectAllTecnicos()" value="todos">todos</v-option>
                                    <v-option @click="selectTecnico(key)" v-for="(key, val) in tecnicos" v-bind:value="key">@{{ val.name }}</v-option>
                                </v-select>
                            </div>
                        </div>
                        <div class="fila">
                            <div class="col-md-4">
                                <label for="tecnicos">Trazos:</label>
                            </div>
                            <div class="col-md-4">
                                <checkbox-group :value.sync="trazos" :disabled="checked ? true : false">
                                    <checkbox value="lineas">Lineas</checkbox>
                                    <checkbox value="rutas">Rutas</checkbox>
                                </checkbox-group>
                            </div>
                            <!--<div class="col-md-4">
                                <input type="checkbox" name="GeoCerca" id="GeoCerca" value="1" v-model="GeoCerca" checked="">
                                <label for="GeoCerca">GeoCerca</label> 
                            </div>-->
                        </div>
                        <div class="fila">
                            <div class="col-md-12">
                                <fieldset class="col-md-12">
                                    <legend><b>Programado</b></legend>
                                    <div class="row">
                                        <div class="col-md-6">
                                          <input type="checkbox" id="programadoOrd" value="programadoOrd" v-model="programadoOrd" checked="">
                                          <label for="programadoOrd">Programado Ordenado</label>    
                                        </div>
                                        <div class="col-md-6">
                                          <input type="checkbox" id="programadoNoOrd" value="programadoNoOrd" v-model="programadoNoOrd" checked="">
                                          <label for="programadoNoOrd">Programado No Ordenado</label>   
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="fila">
                            <div class="col-md-12">
                                <fieldset class="col-md-12">
                                    <legend><b>No Programado</b></legend>
                                    <div class="row">
                                        <div class="col-md-6">
                                          <input type="checkbox" id="noProgramadoConTecnico" value="noProgramadoConTecnico" v-model="noProgramadoConTecnico" checked="">
                                          <label for="noProgramadoConTecnico">No Programdo con Tecnico</label> 
                                        </div>
                                        <div class="col-md-6">
                                          <input type="checkbox" id="noProgramadoSinTecnico" value="noProgramadoSinTecnico" v-model="noProgramadoSinTecnico" checked="">
                                          <label for="noProgramadoSinTecnico">No Programdo sin Tecnico</label> 
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="fila">
                            <div class="col-md-12">
                                <fieldset class="col-md-12">
                                    <legend><b>GeoCerca</b></legend>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="tecnicos">Proyecto</label>
                                        </div>
                                        <!--<div class="col-md-8">
                                            <v-select v-bind:value="tecnicosSeleccionados" placeholder='ningun tecnico'>
                                                <v-option @click="selectAllTecnicos()" value="todos">todos</v-option>
                                                <v-option @click="selectTecnico(key)" v-for="(key, val) in tecnicos" v-bind:value="key">@{{ val.name }}</v-option>
                                            </v-select>
                                        </div>-->
                                        <div class="col-md-8">
                                            <select class="form-control" name="slct_proyecto" id="slct_proyecto" v-on:change="slct_capas" v-model="proyecto_id">
                                                <option value="">ningun proyecto</option>
                                                <option v-for="proyecto in proyectos"   v-bind:value="proyecto.id">@{{proyecto.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 5px;">
                                        <div class="col-md-4">
                                            <label for="tecnicos">Elemento</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="slct_capas" v-ref:capas_id name="slct_capas[]" multiple=""></select>
                                            <input type="hidden" id="hdcapasid">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pull right">
                                            <div class="col-md-6">
                                                <button type="button" @click="showLeft = false" @click.prevent="geoCerca()" class="btn btn-gray">Pintar GeoCerca</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" @click="showLeft = false" @click.prevent="limpiargeoCerca()" class="btn btn-gray">Limpiar GeoCerca</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <div class="aside-footer">
                            <button type="button" @click="showLeft = false" class="btn btn-default">Cerrar</button>
                            <button type="button" @click="showLeft = false" @click.prevent="clickOpciones(pintarMapa,true)" class="btn btn-success">Ver Mapa</button>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <accordion :one-at-atime="true" type="info">
                        <panel header="Buscar actividades segun:" type="primary">
                            <div class="col-md-12">
                                <input type="radio" id="historico" value="historico" v-model="lista">
                                <label for="historico">Histórico</label>
                                <br>
                                <input :disabled="entorno==='TRAIN'" type="radio" id="proceso" value="proceso" v-model="lista" @click="listarHistoricoProceso('')">
                                <label for="proceso">Proceso Automatico</label>
                                <br>
                                <input  type="radio" id="carga_csv" value="carga_csv" v-model="lista">
                                <label for="carga_csv">Cargar csv</label>
                            </div>
                            {{-- Histórico --}}
                            <div v-show="lista=='historico'" class="col-md-12">
                                <div class="col-md-8">
                                    <select class="form-control" v-model="json_historico">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="json in json_historicos" v-bind:value="json.json_actividad"> @{{ json.nombre }} | @{{ json.fecha }} </option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" @click.prevent="verMapaHistorico(json_historico)" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                            {{-- Proceso --}}
                            <div v-show="lista=='proceso'" class="col-md-12">
                                <div class="col-md-4">
                                    <select class="form-control" v-model="historicoProceso" @change="listarHistoricoProceso(historicoProceso)">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="(key, val) in historico_proceso" v-bind:value="val.fregistro">@{{ val.fregistro }}</option>
                                    </select>
                                </div>
                                <div v-show="historico_proceso_detalle" class="col-md-4">
                                    <select class="form-control" v-model="historicoProcesoDetalle" @change="listarHistoricoProceso(historicoProceso)">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="(key, val) in historico_proceso_detalle" v-bind:value="val.data">@{{ val.hReporte }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" @click.prevent="verMapaProcAutomatico" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                            {{-- cargar csv --}}
                            <div v-show="lista=='carga_csv'" class="col-md-12">
                                <div class="col-md-5">
                                    <input type="file" @change="onFileChange" id="actividades_csv" accept=".csv">
                                </div>
                                <div class="col-md-5">
                                    <input type="file" @change="onFileChange" id="tecnicos_csv" accept=".csv">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" @click.prevent="verMapaCsv" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                       </panel>
                       <panel header="Opciones">
                            <button type="button" @click.prevent="clickOpciones(guardarMapaToJson)" class="btn btn-danger">Guardar Mapa</button>
                            <button type="button" @click.prevent="clickOpciones(descargarXls)" class="btn btn-info">Descargar XLS</button>
                            <button type="button" @click.prevent="clickOpciones(descargarKml)" class="btn btn-primary">Descargar KML</button>
                            <button type="button" @click.prevent="clickOpciones(analizarMapa)" class="btn btn-danger">Analizar Mapa</button>
                            <button type="button" @click.prevent="asistenciaTecnicos(verFormularioAsistencia)" class="btn btn-danger"><i class="fa fa-calendar" style="margin-right: 5px;"></i>Asistencia Tecnico</button>
                       </panel>
                    </accordion>
                </div>
                
            </div>
        </aside>
        @include( 'public.form.estadistico' )
        @include( 'public.form.asistenciatecnico' )
        <div id="map" ></div>
        <div v-show='showRight' style="overflow: hidden;">
            <div class="aside slideright-transition right">
                <div class="aside-dialog">
                    <div class="aside-content">
                        <div class="aside-header">
                        <a @click="showRight = false" class="close">×</a>
                        <h4 class="aside-title">Tecnicos</h4>
                        </div>
                        <div class="aside-body">
                            <span class="ppart" style="background-color: #A5D6A7; padding: 5px 5px 5px 5px;"> &#60;= 20 minutos</span>
                            <span class="ppart" style="background-color: #FFCC80; padding: 5px 5px 5px 5px;"> &#60;= 2 horas</span>
                            <span class="ppart" style="background-color: #EF9A9A; padding: 5px 5px 5px 5px;"> &#60;= 4 horas</span>
                            <span class="ppart" style="background-color: #9FA8DA; padding: 5px 5px 5px 5px;"> &#62; 4 horas</span>
                        </div>
                        <div class="scroll">
                            <div class="aside-body row form-group" id="tecnicos">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}

        {{ HTML::script('js/fontawesome-markers.min.js') }}
        {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
       
        @include( "admin.js.slct_global_ajax")
        @include( "admin.js.slct_global")
         {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
        {{ HTML::script('js/mapas/tecnicos.js') }}
        {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&callback=initMap') }}
        {{ HTML::script('js/geo/markerwithlabel.js' , array('async' => 'async')) }}
        
    </body>
</html>
