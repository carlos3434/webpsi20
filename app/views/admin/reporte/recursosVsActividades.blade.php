<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
      {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    @include( 'admin.js.slct_global_ajax' )
      @include( 'admin.js.slct_global' )
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
 .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 8px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }
</style>
<section class="content-header">
  <h1>Recursos vs Actividades</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
    <li><a href="#">Logs</a></li>
    <li class="active">Componente Operación</li>
  </ol>
</section>
<section class="content">
  <div class="row form-group">
    <div class="col-sm-12">
      <div>
        <ul id="myTab" class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active">
              <a href="#t_recursos">Recursos disponibles por bucket</a>
          </li>
          <li role="presentation">
              <a href="#t_tecnico">Técnicos que no han activado ruta vs sus pendientes</a>
          </li>
          <li role="presentation">
              <a href="#t_cantidad">Cantidad de actividades disponibles por técnico</a>
          </li>
        </ul>
      </div>
      <div class="tab-content">
        <div class="tab-pane fade active in" id="t_recursos">
          <div class="box">
            <div class="box-body table-responsive">
              <!-- Envíos OFSC -->
              <div class="row">
                <div class="col-xs-11 filtros">
                  <form name="form_buscarRecursos" id="form_buscarRecursos" method="POST" action="" enctype="multipart/form-data">
                    <!--<input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
                      <input type='hidden' name='tipo_busqueda' value='0' id='tipo_busqueda' />-->
                      <div class="panel-group" id="acordion_filtros">            
                        <div class="panel panel-default">
                           <div class="panel-heading box box-primary">
                              <h4 class="panel-title">
                                 <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Filtro Individual</a>
                             </h4>
                           </div>
                           <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                             <div class="personalizado">
                              <div class="box-body">
                               <div class="row">
                                 <div class="col-sm-4">
                                  <label>Bucket</label>
                                  <select class="form-control" name="slct_bucket[]" id="slct_bucket" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div>
                  </form>
                </div>
                <div class="col-xs-1  botones">
                  <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-filtrarRecursos" onclick="filtrarRecursos()"></i>
                  <i class="fa fa-download fa-xs btn btn-success" id="btn-descargarRecursos" onclick="descargarRecursos()" title="Descargar""></i>
                </div>
              </div>
              <div class="panel-heading box box-primary">
                <div class="row">
                  <div class="col-xs-12 filtros"> 
                    <div class="box-body table-responsive">
                      <table id="tb_recursos" class="table table-bordered table-striped dataTable
                        table-hover" style="background: white">
                        <thead>
                          <tr>
                            <th>Bucket</th>
                            <th>Tecnico</th>              
                            <th>Estado</th>
                            <th>Tipo</th>
                            <th>Celular</th>                          
                          </tr>
                        </thead>
                        <tbody id="tbody_recursos">
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.tab -->

        <div class="tab-pane fade" id="t_tecnico">
          <div class="box">
            <div class="box-body table-responsive">                    
              <div class="row">
                <div class="col-xs-11 filtros">
                  <form name="form_buscarTecnico" id="form_buscarTecnico" method="POST" action="" enctype="multipart/form-data">
                    <!--<input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
                      <input type='hidden' name='tipo_busqueda' value='0' id='tipo_busqueda' />-->
                      <div class="panel-group" id="acordion_filtros2">            
                        <div class="panel panel-default">
                           <div class="panel-heading box box-primary">
                              <h4 class="panel-title">
                                 <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros2" href="#collapse2">Filtro Individual</a>
                             </h4>
                           </div>
                           <div id="collapse2" class="panel-collapse collapse in">
                            <div class="panel-body">
                             <div class="personalizado">
                              <div class="box-body">
                               <div class="row">
                                 <div class="col-sm-4">
                                  <label>Bucket</label>
                                  <select class="form-control" name="slct_buckett[]" id="slct_buckett" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>                                 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div>
                  </form>
                </div>
                <div class="col-xs-1  botones">
                  <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-filtrarTecnico" onclick="filtrarTecnico()"></i>
                  <i class="fa fa-download fa-xs btn btn-success" id="btn-descargarTecnico" onclick="descargarTecnico()" title="Descargar""></i>
                </div>
              </div>
              <div class="panel-heading box box-primary">
                <div class="row">
                  <div class="col-xs-12 filtros"> 
                    <div class="box-body table-responsive">
                      <table id="tb_tecnico" class="table table-bordered table-striped dataTable
                        table-hover" style="background: white">
                        <thead>
                          <tr>
                            <th>Bucket</th>
                            <th>Nombre Tecnico</th>              
                            <th>Carnet</th>
                            <th>Fecha Activacion</th>
                            <th>Programado</th>              
                            <th>Estado</th>
                            <th>Cliente</th>              
                            <th>Dirección</th>
                          </tr>
                        </thead>
                        <tbody id="tbody_tecnico">
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.box-body -->
          </div>          
        </div><!-- /.tab -->
        <div class="tab-pane fade" id="t_cantidad">
          <div class="box">
            <div class="box-body table-responsive">                    
              <div class="row">
                <div class="col-xs-11 filtros">
                  <form name="form_buscarCantidad" id="form_buscarCantidad" method="POST" action="" enctype="multipart/form-data">
                    <!--<input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
                      <input type='hidden' name='tipo_busqueda' value='0' id='tipo_busqueda' />-->
                      <div class="panel-group" id="acordion_filtros3">            
                        <div class="panel panel-default">
                           <div class="panel-heading box box-primary">
                              <h4 class="panel-title">
                                 <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros3" href="#collapse3">Filtro Individual</a>
                             </h4>
                           </div>
                           <div id="collapse3" class="panel-collapse collapse in">
                            <div class="panel-body">
                             <div class="personalizado">
                              <div class="box-body">
                               <div class="row">
                                 <div class="col-sm-4">
                                  <label>Bucket</label>
                                  <select class="form-control" name="slct_bucketc[]" id="slct_bucketc" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div>
                  </form>
                </div>
                <div class="col-xs-1  botones">
                  <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-filtrarCantidad" onclick="filtrarCantidad()"></i>
                  <i class="fa fa-download fa-xs btn btn-success" id="btn-descargarCantidad" onclick="descargarCantidad()" title="Descargar""></i>
                </div>
              </div>

              <div class="panel-heading box box-primary">
                <div class="row">
                  <div class="col-xs-12 filtros"> 
                    <div class="box-body table-responsive">
                      <table id="tb_cantidad" class="table table-bordered table-striped dataTable
                        table-hover" style="background: white">
                        <thead>
                          <tr>
                            <th>Bucket</th>
                            <th>Carnet</th>
                            <th>Nombre Tecnico</th>
                            <th>Cantidad Actividades</th>
                            <th>Actividades Programada</th>
                            <th>Actividades no programada</th>
                          </tr>
                        </thead>
                        <tbody id="tbody_cantidad">
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.box-body -->
          </div>
        </div><!-- /.tab -->
      </div>
    </div>
  </div>
</section>

{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("js/fontawesome-markers.min.js") }}   
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/vue.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/vue/vue-resource.min.js") }}
{{ HTML::script("lib/validaCampos/validaCampos.js") }}

@include( "admin.js.slct_global_ajax")
@include( "admin.js.slct_global")

<script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
</script>
<script type="text/javascript" src="js/admin/reporte/recursosVsActividades_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/reporte/recursosVsActividades.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')
