<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@include( 'admin.reporte.js.mensaje_ajax' )
@include( 'admin.reporte.js.mensaje' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('reporte.mantenimiento_mensaje')}}
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>{{trans('reporte.admin')}}</a></li>
        <li><a href="#">{{trans('reporte.reportes')}}</a></li>
        <li class="active">{{trans('reporte.mantenimiento_mensaje')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('reporte.filtro')}}</h3>
                </div>
                <div class="panel-body">
                    <form id="form_mensaje" name="form_mensaje"  role="form" class="form-inline" method="POST">

                        <div class="form-group">
                            <label for="txt_fecha">{{trans('reporte.rango_fecha')}} creación</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                <input class="form-control" id="txt_rangofecha" name="txt_rangofecha" type="text"  aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <button type="submit" id="btn_filter" class="btn btn-primary">{{trans('reporte.mostrar')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <table id="datatable_mensaje" class="table table-bordered table-striped table-condensed display responsive no-wrap" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">{{trans('reporte.id')}}</th>
                        <th class="text-center">{{trans('reporte.id_mensaje')}}</th>
                        <th class="text-center">{{trans('reporte.id_empresa')}}</th>
                        <th class="text-center">{{trans('reporte.envio')}}</th>
                        <th class="text-center">{{trans('reporte.asunto')}}</th>
                        <th class="text-center">{{trans('reporte.cuerpo')}}</th>
                        <th class="text-center">{{trans('reporte.created_at')}}</th>
                        <th class="text-center">{{trans('reporte.updated_at')}}</th>
                        <th class="text-center">{{trans('reporte.estado')}}</th>
                        <th class="text-center">{{trans('reporte.table_accion')}}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">{{trans('reporte.id')}}</th>
                        <th class="text-center">{{trans('reporte.id_mensaje')}}</th>
                        <th class="text-center">{{trans('reporte.id_empresa')}}</th>
                        <th class="text-center">{{trans('reporte.envio')}}</th>
                        <th class="text-center">{{trans('reporte.asunto')}}</th>
                        <th class="text-center">{{trans('reporte.cuerpo')}}</th>
                        <th class="text-center">{{trans('reporte.created_at')}}</th>
                        <th class="text-center">{{trans('reporte.updated_at')}}</th>
                        <th class="text-center">{{trans('reporte.estado')}}</th>
                        <th class="text-center">{{trans('reporte.table_accion')}}</th>
                    </tr>
                </tfoot>
            </table>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('formulario')
@include( 'admin.reporte.form.mensajeDetalle' )
@stop
