<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}

@include( 'admin.reporte.js.mensaje_ajax' )
@include( 'admin.reporte.js.mensaje' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('reporte.mantenimiento_mensaje')}}
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>{{trans('reporte.admin')}}</a></li>
        <li><a href="#">{{trans('reporte.reportes')}}</a></li>
        <li class="active">{{trans('reporte.mantenimiento_mensaje')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('reporte.filtro')}}</h3>
                </div>
                <div class="panel-body">
                    <form id="form_mensaje" name="form_mensaje"  role="form" class="form-inline" method="POST">

                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <label for="txt_fecha">{{trans('reporte.rango_fecha')}} creación: &nbsp;&nbsp;</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                    <input class="form-control" id="txt_rangofecha" name="txt_rangofecha" type="text"  aria-describedby="basic-addon1" readonly>
                                </div>
                            </div>
                            &nbsp;&nbsp;
                            <div class="form-group">
                                <label>{{trans('reporte.id_mensaje')}}: </label>&nbsp;&nbsp;
                                <input name="id_msg" type="text" class="form-control" placeholder="#######">
                            </div>
                        </div>

                        <br> <br>

                        <div class="col-md-12 text-center">
                            <select name="asunto" class="form-control" class="selectpicker" multiple>
                              <option value="WO_SUSPEND">WO_SUSPEND</option>
                              <option value="WO_NOTDONET">WO_NOTDONET</option>
                              <option value="TOA_OUTBOUND_PRE_NOTDONE">TOA_OUTBOUND_PRE_NOTDONE</option>
                              <option value="WO_PRE_COMPLETED">WO_PRE_COMPLETED</option>
                              <option value="TOA_OUTBOUND_PRE_COMPLETED">TOA_OUTBOUND_PRE_COMPLETED</option>
                              <option value="WO_PRE_NOTDONE">WO_PRE_NOTDONE</option>
                              <option value="NOT_LEG">NOT_LEG</option>
                              <option value="TOA_OUTBOUND_SUSPEND">TOA_OUTBOUND_SUSPEND</option>
                              <option value="UNSCHEDULE">UNSCHEDULE</option>
                              <option value="WO_INIT">WO_INIT</option>
                              <option value="TOA_VALIDATE_COORDS">TOA_VALIDATE_COORDS</option>
                              <option value="TOA_OUTBOUND_COMPLETED">TOA_OUTBOUND_COMPLETED</option>
                              <option value="TOA_OUTBOUND_NOTDONE">TOA_OUTBOUND_NOTDONE</option>
                              <option value="INT_SMS">INT_SMS</option>
                              <option value="WO_CANCEL">WO_CANCEL</option>

                              <option value="XA_DONE_PIC">XA_DONE_PIC</option>
                              <option value="TEST_PDM">TEST_PDM</option>
                              <option value="NOT_LEG">NOT_LEG</option>
                              <option value="INT_SMS">INT_SMS</option>
                              <option value="ACT_HFC">ACT_HFC</option>
                              <option value="ACTIV_DTH">ACTIV_DTH</option>
                              <option value="ACTIV_CATV">ACTIV_CATV</option>

                            </select>
                            &nbsp;&nbsp;
                            <select name="estado" class="form-control" multiple>
                              <option value="delivered">delivered</option>
                              <option value="sending">sending</option>
                            </select>
                            &nbsp;&nbsp;
                            <button type="submit" id="btn_filter" class="btn btn-primary">{{trans('reporte.mostrar')}}</button>
                        </div>
                        
                    </form>
                    <!--
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="exampleInputName2">Mensaje ID: </label>&nbsp;&nbsp;
                            <input type="text" class="form-control" id="id_mensaje_input_search" placeholder="#######">
                        </div>
                        <button type="submit" class="btn btn-default">Send invitation</button>
                    </form>
                    -->
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <table id="datatable_mensaje" class="table table-bordered table-striped table-condensed display responsive no-wrap text-center" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">{{trans('reporte.id')}}</th>
                        <th class="text-center">{{trans('reporte.id_mensaje')}}</th>
                        <th class="text-center">{{trans('reporte.id_empresa')}}</th>
                        <th class="text-center">{{trans('reporte.asunto')}}</th>
                        <th class="text-center">{{trans('reporte.cuerpo')}}</th>
                        <th class="text-center">{{trans('reporte.created_at')}}</th>
                        <th class="text-center">{{trans('reporte.updated_at')}}</th>
                        <th class="text-center">{{trans('reporte.estado')}}</th>
                        <th class="text-center">{{trans('reporte.table_accion')}}</th>
                    </tr>
                </thead>
                <tfoot class="hide">
                    <tr>
                        <th class="text-center">{{trans('reporte.id')}}</th>
                        <th class="text-center">{{trans('reporte.id_mensaje')}}</th>
                        <th class="text-center">{{trans('reporte.id_empresa')}}</th>
                        <th class="text-center">{{trans('reporte.asunto')}}</th>
                        <th class="text-center">{{trans('reporte.cuerpo')}}</th>
                        <th class="text-center">{{trans('reporte.created_at')}}</th>
                        <th class="text-center">{{trans('reporte.updated_at')}}</th>
                        <th class="text-center">{{trans('reporte.estado')}}</th>
                        <th class="text-center">{{trans('reporte.table_accion')}}</th>
                    </tr>
                </tfoot>
            </table>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('formulario')
@include( 'admin.reporte.form.mensajeDetalle' )
@stop
