<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')


<style type="text/css">
  .yellow-fieldset{
        max-width: 100% !important;
        border: 3px solid #999;
        padding:10px 50px 0px 9px;
        border-radius: 10px; 
        margin-bottom: 15px;
    }
</style>
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       Solicitudes Tecnicas CMS
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Reporte</a></li>
                        <li class="active">Reporte de Movimiento</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <form name="form_movimiento" id="form_movimiento" method="post" action="reporte/movimiento" enctype="multipart/form-data">
                        <fieldset class="yellow-fieldset">           
                            <div class="row form-group" id="div_fecha">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <label>Seleccione Rango de Fechas por:</label>
                                    </div>
                                     <div class="col-sm-3">
                                        <select class="form-control" name="slct_tipo_fecha" id="slct_tipo_fecha">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="1">Registro CMS</option>
                                            <option value="4">Envio PSI</option>
                                            <option value="2">Registro PSI</option>
                                            <option value="3">Registro TOA</option>                                           
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="AAAA-MM-DD - AAAA-MM-DD" id="fecha" name="fecha" onfocus="blur()"/>
                                    </div>                                   
                                    <div class="col-sm-3"> 
                                        <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>&nbsp;&nbsp;
                                        <a onclick="DescargarExcel();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                    </div>                                    
                                </div>
                            </div>

                        </fieldset>                                              
                    </form>


                     <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                                <table id="tb_solicitudes" class="table table-bordered table-hover">
                                  <thead>
                                        <tr>
                                          <th>Solicitud Tec</th>
                                          <th>Orden Trab</th>
                                          <th>Tipo Reque</th>
                                          <th>Tipo Operacion</th>
                                          <th>Contrata</th>
                                          <th>Quiebre</th>
                                          <th>Cod Cliente</th>
                                          <th>Peticion</th>
                                          <th>FFTT</th>
                                          <th>F_Registro_cms</th>
                                          <th>Fec_Regist_cms</th>
                                          <th>Hora_Regist_cms</th>
                                          <th>Envio_psi</th>
                                          <th>Fec_envio_psi</th>
                                          <th>Hora_envio_psi</th>
                                          <th>Estado_cms</th>
                                          <th>F_registro_psi</th>
                                          <th>Fec_regist_psi</th>
                                          <th>Hora_regist_psi</th>
                                          <th>Aseguramiento</th>
                                          <th>Descripcion_error</th>
                                          <th>F_registro_toa</th>
                                          <th>Fec_regist_toa</th>
                                          <th>Hora_regist_toa</th>
                                          <th>Estado_toa</th>
                                          <th>Estado_st</th>
                                          <th>Requerimiento</th>
                                          <th>cms_to_envio</th>
                                          <th>cms_to_psi</th>
                                          <th>cms_to_toa</th>
                                          <th>psi_to_toa</th>
                                          <th>Tipo_requerimiento</th>
                                          <th>Motivo_generacion</th>
                                          <th>Segmento</th>
                                          <th>Quiebre_id</th>
                                          <th>Nombre</th>
                                          <th>Apellido</th>
                                          <th>F.Cierre TOA</th>
                                          <th>F.Cierre Legado</th>
                            </tr>
                                  </thead>
                                  <tbody id="t_solicitudes">

                                  </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </section><!-- /.content -->
@stop

@section('formulario')
     <!-- @include( 'admin.mantenimiento.form.usuario' )  -->
@stop

@push('scripts')
   {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
   {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
   {{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }} 
   {{ HTML::script('js/sweetalert/sweetalert.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/reporte/rptmovsolicitudes_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/reporte/rptmovsolicitudes.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')