<script type="text/javascript">
    var WsEnvios = {
        CargarWsEnvios: (function () {
            var datos="";
            var columnDefs = [
                {
                    "targets": 0,
                    "data": "fecha",
                    "name": "fecha"
                },
                {
                    "searchable": true,
                    "targets": 1,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return trama.Operation;
                            }
                },
                {
                    "searchable": true,
                    "targets": 2,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return trama.TaskNumber;
                            }
                },
                {
                    "searchable": true,
                    "targets": 3,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return trama.EmployeeNumber;
                            }
                },
                {
                    "targets": 4,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return trama.Data19;
                            }
                },
                {
                    "targets": 5,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return (trama.Data28 ? trama.Data28:"");
                            }
                },
                {
                    "targets": 6,
                    "data": "trama",
                    "name": "trama",
                    "render": function(data, type, full) {
                                var trama = eval('(' + data + ')');
                                return (trama.Data29 ? trama.Data29:"");
                            }
                },
                {
                    "searchable": true,
                    "targets": 7,
                    "data": "response",
                    "name": "response"
                },
                {
                    "targets": 8,
                    "data": "id",
                    "name": "id",
                    "render": function(data, type, full) {
                                return '<button class="btn btn-primary" data-toggle="modal" data-target="#wsenviosModal" data-whatever="Detalle" onclick="WsEnvios.MostrarDetalleWsEnvios(' + data + ');"><i class="fa fa-eye"></i></button>';
                            }
                }
            ];
            $('#t_wsenvios').dataTable().fnDestroy();
            $('#t_wsenvios')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .DataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,
                        "searchable": true,
                       
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        "ajax": {
                            url: "wsenvios/paginacion",
                            type: "POST",
                            cache: false,
                            "data": function(d){
                                var contador=0;
                                datos=$("#form_wsenvios").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");

                                for (var i = datos.length - 1; i >= 0; i--) {
                                    if( datos[i].split("[]").length>1 ){
                                        d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                        contador++;
                                    }
                                    else{
                                        d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                                    }
                                };
                            },
                        },
                        columnDefs
                    });   
        }),
        
        MostrarDetalleWsEnvios: (function (id) {
                    $('#wsenviosModal').on('show.bs.modal', function () {
                        $("#wsenviosModal").find('.modal-body textarea').val("");
                    });
                    $.ajax({
                        type: 'POST',
                        url: "wsenvios/detalle",
                        data: "id=" + id,
                        dataType: 'json',
                        cache: false,
                        success: function (obj) {
                            if(obj.rst === 1){
                                $.each(obj.datos, function (index, wsenvio) {
                                   $("#wsenviosModal").find('.modal-title').text("Detalle Mensaje");
                                   $("#wsenviosModal").find('.modal-body textarea').val(wsenvio.trama);
                                });
                            }
                        }
                    });
                })
    };
</script>