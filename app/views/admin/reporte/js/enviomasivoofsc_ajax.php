<script type="text/javascript">
    var id_table_proy = '#t_envio_masivo';
    var route = 'enviosofsc/';

    var EnvioMasivo = {
        cargar: function () {
            return $(id_table_proy).DataTable({
                "ajax": {
                    url: route+"listarmasivos",
                    type: "POST",
                    data: function (d) {
                        var datos = '';
                        var contador = 0;
                        if (PG == "P") {// Filtro Personalizado
                            datos = $("#form_Personalizado").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("&");
                        } else if (PG == "G") { // Filtro General
                            datos = $("#form_General").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
                        }

                        for (var i = datos.length - 1; i >= 0; i--) {
                            if (datos[i].split("[]").length > 1) {
                                d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
                                contador++;
                            } else {
                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                            }
                        }
                    }
                },
                "columns":[
                    { data: 'codactu', "class":"colunm-index"},
                    { data: 'aid'},
                    { data: 'grupo'},
                    { data: 'mdf'},
                    { data: function ( row, type, val, meta) {
                        if (row.actividad_id == null)
                            row.actividad_id = "";
                        if (row.tipo_actividad_id == null)
                            row.tipo_actividad_id = "";
                        return "<b>"+row.actividad_id+"</b><br>"+row.tipo_actividad_id;
                        }
                    },
                    { data: 'mensaje'},
                    { data: 'estado'}
                    /*{ data: function ( row, type, val, meta) {
                        return moment(row.created_at).format("YYYY-MM-DD");
                        }
                    }*/
                ],
                "columnDefs": [
                    {
                        "targets": 6,
                        "orderable": false,
                        "searchable": false
                    },
                ],
                processing: true,
                serverSide: true,
                paging: true,
                lengthChange: false,
                searching: false,
                ordering: true,
                order: [[ 2, "desc" ]],
                info: true,
                autoWidth: true,
                language: config
            });
        },
        EnvioManual: function () {
            
        },
    };

</script>
