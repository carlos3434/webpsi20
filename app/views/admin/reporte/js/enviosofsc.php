<script type="text/javascript">
$(document).ready(function() {

    $('#myTab a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
      //cuadno hace click en lista actualizar los datos , si hace click en tab TABLA reiniar filtros
    });


    $('#fecha_calen').daterangepicker({
        format: 'YYYY-MM-DD'
    });

     
    $('#fecha_recepcion_ini').daterangepicker({
        format: 'YYYY-MM-DD',
        singleDatePicker: true
    });
    $('#fecha_recepcion_fin').daterangepicker({
        format: 'YYYY-MM-DD',
        singleDatePicker: true
    });

     $('#t_enviosofs').DataTable( {
        "columnDefs": [
            {
                "targets": [ 5 ],
                "visible": false               
            },
            {
                "targets": [ 6 ],
                "visible": false
            } 
        ]
    } );

    $("#mostrar").click(validarInicio);

    $('#t_enviosofs tbody ').on('click', 'button', function (event) {

       var table = $('#t_enviosofs').DataTable();

        var row = $(this).closest("tr").get(0);
        var aData = table.row(row).data();
     
         $.ajax({
            url         : 'enviosofsc/listardetalle',
            type        : 'POST',
            cache       : false,
            contentType : "application/x-www-form-urlencoded",
            data    : {
                e          : aData[5],
                r          : aData[6]
            },           
            success : function(obj) {
                    $('#ItemPopup').modal('show');
                    $("#cmodal").html(obj);
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });  


        event.stopImmediatePropagation();  //prvents the other on click from firing that fires up the inline editor
    });  
});
/*
$.extend( true, $.fn.dataTable.defaults, {
    "language": {
        "lengthMenu": "Mostrar _MENU_ Registros por página",
        "zeroRecords": "Registros No encontrados",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay Registros disponibles",
        "search":         "Buscar:",
        "infoFiltered": "(Filtrado desde _MAX_ total de registros)",
        "paginate": {
        "first":      "Primero",
        "last":       "Ultimo",
        "next":       "Siguiente",
        "previous":   "Anterior"
    },
    }
    
} );*/

HTMLCargarEnviosOfsc=function(datos){
    var html="";
    $('#t_enviosofs').dataTable().fnDestroy();

    $.each(datos,function(index,data){
   
        if(data.enviado.length > 50) enviadoFinal = data.enviado.substring(0, 50) + '...'; else enviadoFinal = data.enviado;
        if(data.respuesta.length > 50) respuestaFinal = data.respuesta.substring(0, 50) + '...'; else respuestaFinal = data.respuesta;
 
        var valor = data.respuesta;

         html+="<tr>"+
            "<td>"+data.accion+"</td>"+
            "<td>"+data.created_at+"</td>"+
            "<td>"+data.usuario+"</td>"+
            "<td>"+enviadoFinal+"</td>"+              
            "<td>"+respuestaFinal+"</td>"+              
            "<td>"+data.enviado+"</td>"+  
            "<td>"+data.respuesta+"</td>"+       
            "<td><button type='button' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-search'></span></button></td>"; 
        html+="</tr>";
    });

    $("#tb_enviosofs").html(html);   

    $('#t_enviosofs').DataTable( {
        "order": [[ 1, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 6 ],
                "visible": false
            },
            {
                "targets": [ 7 ],
                "orderable": false
            } 
        ]
    } );

};

validarInicio = function () {
    if ( $.trim($("#fecha_calen").val()) === '') {
        alert('Seleccione la Fecha');
        
        // if($.trim($("#fecha_recepcion_ini").val()) == '' && $.trim($("#fecha_recepcion_fin").val()) == '') $("#fecha_recepcion_ini").focus();
        // if($.trim($("#fecha_recepcion_ini").val()) == '' && $.trim($("#fecha_recepcion_fin").val()) != '') $("#fecha_recepcion_ini").focus();
        // if($.trim($("#fecha_recepcion_ini").val()) != '' && $.trim($("#fecha_recepcion_fin").val()) == '') $("#fecha_recepcion_fin").focus();

    } else {
        EnviosOfsc.CargarEnviosOfsc();            
    }
};
</script>