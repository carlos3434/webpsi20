<script type="text/javascript">
    $(document).ready(function () {
        $("#mostrar").click(MostrarMovimientosObservaciones);
        $("#t_movimientoObservaciones").dataTable();


        var ids = [];
        var data = {usuario: 1};
        slctGlobal.listarSlct('quiebre', 'slct_quiebre', 'multiple', ids, data);
        slctGlobal.listarSlct('empresa', 'slct_empresa', 'multiple', ids, data, 0, '#slct_celula,#slct_tecnico', 'E');
    });

    MostrarMovimientosObservaciones = function () {
        var fecha_agenda = $("#fecha_agenda").val();
        if (fecha_agenda.trim() === "") {
            alert("Seleccione Fecha");
        } else {
            MovimientoObservaciones.listarMovimientos();
        }
    }

    descargarReporte = function () {
        //$("#form_General").append("<input type='hidden' name='totalPSI' id='totalPSI' value='1'>");
        $("#form_movimientoObservaciones").submit();
        //$("#form_General #totalPSI").remove();
    }
</script>