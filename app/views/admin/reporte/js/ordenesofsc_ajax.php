<script type="text/javascript">
	//$(".right-side").addClass("strech");
	GLOBAL = {};
	$("[name=fecha_registro], [name=fecha_movimiento]").daterangepicker({ format: 'YYYY-MM-DD' });
	$("[name=fecha_registro], [name=fecha_movimiento]").val(
		moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
		 moment(new Date()).format("YYYY-MM-DD")
   	);
	envioMasivoOFSC=function(){
	        r = confirm("<?php echo Lang::get('¿Seguro que desea enviar un Masivo?', array('accion' => 'Continuar')) ?>");
	        if (r === true)
	            enviarMasivoOfsc();
	};
	activarEnvioMasivo = function(button) {
	    
	        flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");

	        if (flagActivoMasivo === null) {
	                     localStorage.setItem("bandejaMasivoActivo", 1);
	                    button.removeClass("btn-success");
	                    button.addClass("btn-danger");
	                    button.html("Desactivar Envio de Masivos");
	                    $(".btn-ejecutar-masivo").css("display", "inline-block");
	                    $("span.cont_checkbox").css("display", "inline-block");
	                    $("span.checkbox_fila").css("display", "inline-block");
	                    $(".contenedor-izquierdo").css("display", "block");
	                    $("span.txt_id").css("margin-left", 10);
	                    $("table#t_bandeja thead th:eq(0)").css("display", "block");
	                    $("table#t_bandeja tfoot th:eq(0)").css("display", "block");
	                    $("table#t_bandeja tbody tr").each(function(e){
	                        $(this).find("td:eq(0)").css("padding", 16);
	                        $(this).find("td:eq(0)").css("display", "block");
	                    });
	        } else {
	            if (flagActivoMasivo === '0') {
	                localStorage.setItem("bandejaMasivoActivo", 1);
	                button.removeClass("btn-success");
	                button.addClass("btn-danger");
	                button.html("Desactivar Envio de Masivos");
	                $(".btn-ejecutar-masivo").css("display", "inline-block");
	                $("span.cont_checkbox").css("display", "inline-block");
	                $("span.checkbox_fila").css("display", "inline-block");
	                $(".contenedor-izquierdo").css("display", "block");
	                $("span.txt_id").css("margin-left", 10);
	                $("table#t_bandeja thead th:eq(0)").css("display", "block");
	                $("table#t_bandeja tfoot th:eq(0)").css("display", "block");
	                $("table#t_bandeja tbody tr").each(function(e){
	                    $(this).find("td:eq(0)").css("padding", 16);
	                    $(this).find("td:eq(0)").css("display", "block");
	                });
	                 
	            } else {
	                 localStorage.setItem("bandejaMasivoActivo", 0);
	                button.removeClass("btn-danger");
	                button.addClass("btn-success");
	                button.html("Activar Envio de Masivos");
	                $(".btn-ejecutar-masivo").css("display", "none");
	                $("span.cont_checkbox").css("display", "none");
	                $("span.checkbox_fila").css("display", "none");
	                $(".contenedor-izquierdo").css("display", "none");
	                $("span.txt_id").css("margin-left", 10);
	                $("table#t_bandeja thead th:eq(0)").css("display", "none");
	                $("table#t_bandeja tfoot th:eq(0)").css("display", "none");
	                 $("table#t_bandeja tbody tr").each(function(e){
	                    $(this).find("td:eq(0)").css("padding", 0);
	                    $(this).find("td:eq(0)").css("display", "none");
	                });
	            }
	        }
	};

	htmlCheckboxFilasMasivo = function(codactu){
	    flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
	    bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
	    bandejaBusqueda = JSON.parse(bandejaBusqueda);
	    //console.log(bandejaBusqueda);
	    display = "";
	    enable = "";
	    checked = "";
	    hidden = 0;
	    if(flagActivoMasivo === null || flagActivoMasivo === '0') {
	        display = "style='display: none' ";
	        if(bandejaBusqueda === null){
	                enable = "disable";
	            }else{
	                   if(typeof bandejaBusqueda[codactu]!=="undefined"){
	                        enable = "enable";
	                           if (bandejaBusqueda[codactu].seleccionado === true){
	                                checked = "checked='checked' ";
	                                hidden = 1;
	                           } else {
	                                 checked = "";
	                                 hidden = 0;
	                            }
	                    } else {
	                           enable = "disable";
	                           checked ="";
	                           hidden = 0;
	                     }
	              }
	    } else {
	          if(bandejaBusqueda === null){
	              display = "style='display:inline-block;'";
	                enable = "disable";
	            }else{
	                   if(typeof bandejaBusqueda[codactu]!=="undefined"){
	                         display = "style='display:inline-block;'";
	                        enable = "enable";
	                           if (bandejaBusqueda[codactu].seleccionado === true){
	                                checked = "checked='checked' ";
	                                hidden = 1;
	                           } else {
	                                 checked = "";
	                                 hidden = 0;
	                            }
	                    } else {
	                          display = "style='display:inline-block;'";
	                           enable = "disable";
	                           checked ="";
	                           hidden = 0;
	                     }
	              }
	   }
	   resultado = new Array(3);
	   resultado[0] = display;
	   resultado[1] = enable;
	   resultado[2] = checked;
	   resultado[3] = hidden;
	   return resultado;
	};
	updateEstadoCheckboxMasivo  = function(button) {
	    codactu = button.attr("codactu");
	    bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
	    bandejaBusqueda = JSON.parse(bandejaBusqueda);
	   
	    if (button.is(":checked")) {
	        bandejaBusqueda[codactu].seleccionado = true;
	    } else {
	        bandejaBusqueda[codactu].seleccionado = false;
	    }
	    bandejaBusqueda  = JSON.stringify(bandejaBusqueda);
	    localStorage.setItem("bandejaBusqueda", bandejaBusqueda);
	    pintarResultadosSeleccionados();
	};
	formBusquedatoArray = function ( isSoloActuaciones) {
	    contador = 0;
	               datos=$("#filtro_envios_ofsc").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("&");

	            datos.push("isSoloActuaciones="+isSoloActuaciones);
	            d =new Array(datos.length);
	             for (var i = datos.length - 1; i >= 0; i--) {
	                    if( datos[i].split("[]").length>1 ){
	                              d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
	                                contador++;
	                      }
	                       else{
	                             d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
	                        }
	                    }
	            return d;
	};
	pintarResultadosSeleccionados = function(){
	    bandejaBusqueda =  localStorage.getItem("bandejaBusqueda");
	    bandejaBusqueda = JSON.parse(bandejaBusqueda);
	    dataset = [];
	    for (var i in bandejaBusqueda) {
	        if (bandejaBusqueda[i].seleccionado=== true)
	            bandejaBusqueda[i].seleccionado = "Seleccionado";
	        else
	            bandejaBusqueda[i].seleccionado = "No Seleccionado";
	        dataset.push([bandejaBusqueda[i].codactu, bandejaBusqueda[i].seleccionado]);
	    }
	    $('#resultados-seleccionados').dataTable().fnDestroy();
	    if(dataset.length >0) {
	        $("#collapseThree").css("margin-top", -390);
	         $('#resultados-seleccionados').DataTable( {
	            data: dataset,
	            columns: [
	                { title: "CodActuación" },
	                { title: "Seleccionado" }
	            ],
	            fixedHeader: true,
	            "scrollY":        "120px",
	            "scrollCollapse": true,
	            "fnAdjustColumnSizing": true
	        } );
	    } else {
	        $("#collapseThree").css("margin-top", -300);
	    }
	};

   	CargarOrdenesTotalBusqueda  = function (isSoloActuaciones) {
        		d = formBusquedatoArray( isSoloActuaciones);
		data = "";
		 for(var i in d){
		       data+="&"+i+"="+d[i];
		}
		data+="&bandeja=1";
		 ordenes = functionajax("gestion/cargar",  "POST", false, "json", false, data);
		ordenes.success(function(data){
			console.log(data);
		 if (data.rst === 1){
		            savedata = functionajax("bandeja/savelocalstorage",  "POST", false, "json", false, {"localStorageBusqueda": JSON.stringify(data.datos)});
		              savedata.success(function(data){
		              	console.log(data);
		              if(data.rst === 1){
		                      localStorage.setItem("bandejaBusqueda", JSON.stringify(data.datos));
		                        pintarResultadosSeleccionados();
		                   }
		                });
		              }
		       });
		};
	enviarMasivoOfsc =function(){
	        bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
	        if (typeof bandejaBusqueda === "undefined" || bandejaBusqueda === "" || bandejaBusqueda === "{}") {
	            Psi.mensaje("danger", "No ha realizado una busqueda primero");
	        } else {
	             masivo = functionajax('enviosofsc/masivoofsc', 'POST', false, 'json', false, {"codactuSeleccionados":bandejaBusqueda});
	            masivo.success(function(data){
	                if  (data.rst == 1) {
	                    Psi.mensaje("success", data.msj, 5000);
	                    setTimeout(function() {
	                        Bandeja.CargarBandeja(GLOBAL.pg,HTMLCargarBandeja);
	                    }, 5000);
	                } else {
	                    Psi.mensaje("danger", data.msj, 5000);
	                }
	            });
	        }
	    };
   	
	OrdenesOfsc = {
		listar: function(){
			CargarOrdenesTotalBusqueda(1);
			GLOBAL.table_enviosofsc  = $("#t_enviosofs").DataTable({
			"processing": true,
		                "serverSide": true,
		                "stateSave": true,
		                "stateLoadCallback": function (settings) {
		                     $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
		                },
		                "stateSaveCallback": function (settings) { 
		                    $(".overlay,.loading-img").remove();
		                    $('#t_enviosofs').removeAttr("style");
		                },
		                "ajax": {
		                    url: "gestion/cargar",
		                    type: "POST",
		                    data: function (d) {
		                        var contador = 0;
		                        datos =  FormSerialize($("#filtro_envios_ofsc"));
		                        for (var i = datos.length - 1; i >= 0; i--) {
		                            if (datos[i].split("[]").length > 1) {
		                                d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
		                                contador++;
		                            } else {
		                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
		                            }
		                        }
		                        if ($("#collapse2").css("display") == "none") {
		                        	d.codigo_actuacion = "";
		                        }
		                        d.bandeja = 1;
		                        d.usuario = 1;
		                        d.withLimit = 1;
		                        d.fecha_registro_psi ="";
		                    }
		                },
		                "columns":[
		                    { data: function (row, type, val, meta){
		                   	estilos = htmlCheckboxFilasMasivo(row.codactu);
		                   	//console.log(estilos);
                                		texto= "<span class='checkbox_fila' "+estilos[0]+" "+estilos[1]+" ><input type='checkbox' name='checkbox_actu[]'   "+estilos[2]+"codactu='"+row.codactu+"'/></span><span class='txt_id'>"+row.codactu+"</span>";
                                		return texto;
		                   }, "class":"colunm-index", name:'codactu'},
		                    { data: 'fecha_registro', name:'fecha_registro'},
		                    { data: 'fecha_registro_psi', name:'fecha_registro_psi'},
		                    { data: 'actividad', name:'actividad'},
		                    { data: 'quiebre', name:'quiebre'},
		                    { data: 'empresa', name:'empresa'},
		                    { data: 'mdf', name:'mdf'},
		                   // { data: 'gestion_ofsc', name: 'gestion_ofsc' },
		                   {data: 'fh_agenda', name:'fh_agenda'},
		                   {data : function( row, type, val, neta) {
		                   	if (row.estado_ofsc ==="")
		                   		return "No Enviado";
		                   	else 
		                   		return row.estado_ofsc;
		                   	}, name: "estado_ofsc"
		                  },
		                  //{data: 'estado_ofsc', name: 'estado_ofsc'},
		                  {data: 'estado', name: 'estado'}
		                  /*{data: 'numintentos', name: 'numintentos'},
		                  { data: function ( row, type, val, meta) {
		                  	btnhistorial = '<a class="pplo btn-sm btn btn-info btn-historial-envios-ofsc" data-codactu="'+row.codactu+'"><i class="fa fa-history" aria-hidden="true"></i></a>';
		                  	if (row.aid!==0 || row.estado_ofsc!==null)
		                  		return '<a class="pplo btn bg-orange btn-sm btn-detalle-ofsc"  data-aid ="'+row.aid+'" data-codactu="'+row.codactu+'"><i class="fa fa-automobile fa-lg"></i></a>';
		                  	else 
		                  		return "";
		                  },
		              	}*/
		                ],
		                "columnDefs": [
		                    {
		                        "targets": 9,
		                        "orderable": false,
		                        "searchable": false
		                    },
		                ],
		                paging: true,
		                lengthChange: false,
		                searching: false,
		                ordering: true,
		                order: [[ 0, "desc" ]],
		                info: true,
		                autoWidth: true,
		                language: config
		            });
			return GLOBAL.table_enviosofsc;
		},
		exportar: function(){
			$("#filtro_envios_ofsc").prop("method", "POST");
			$("#filtro_envios_ofsc").append("<input type='hidden' id='empresa' name='empresa' value='"+$("#slct_empresa").val()+"' /> ");
			$("#filtro_envios_ofsc").append("<input type='hidden' id='actividad' name='actividad' value='"+$("#slct_actividad").val()+"' />");
			$("#filtro_envios_ofsc").append("<input type='hidden' id='actividad_tipo' name='actividad_tipo' value='"+$("#slct_actividad_tipo").val()+"' />");
			$("#filtro_envios_ofsc").append("<input type='hidden' id='nodo' name='nodo' value='"+$("#slct_nodo").val()+"' />");
			$("#filtro_envios_ofsc").append("<input type='hidden' id='tipo_envio' name='tipo_envio' value='"+$("#slct_tipo_envio").val()+"' />");

			$("#filtro_envios_ofsc").prop("action", "enviosofsc/reporteofsc");
			$("#filtro_envios_ofsc").submit();

			$("#filtro_envios_ofsc #empresa").remove();
			$("#filtro_envios_ofsc #actividad").remove();
			$("#filtro_envios_ofsc #actividad_tipo").remove();
			$("#filtro_envios_ofsc #nodo").remove();
			$("#filtro_envios_ofsc #tipo_envio").remove();
		},

		getDetalleOfsc: function(button, row, tr) {
			detalleActividad = functionajax("bandeja/detbandeja", "POST", false, "json", false, {aid: button.attr("data-aid"), codactu: button.attr("data-codactu")});
			detalleActividad.success(function(data){
				console.log(data);
				if (data.rst == 1) {
			 		row.child(data.html).show();
	                    			tr.addClass('shown');
		 		}
			});
		},

		getDetalleEnvios: function (codactu) {
			detalle = functionajax("enviosofsc/detalle-envio", "POST", false, "json", false, {codactu: codactu});
			detalle.success(function(data) {
				//console.log(data);
			});
		}
	};

	OrdenesOfscMasivo = {
		listar: function(){
		            flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
		          //
		                
		GLOBAL.tabla_bandeja_ordenes_masivo = $("#t_enviosofs_masivo").DataTable({
			"processing": true,
		                "serverSide": true,
		                "stateSave": true,
		                "stateLoadCallback": function (settings) {
		                     $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
		                },
		                "stateSaveCallback": function (settings) { 
		                    $(".overlay,.loading-img").remove();
		                    $('#t_enviosofs_masivo').removeAttr("style");
		                },
		                "ajax": {
		                    url: "enviosofsc/listarmasivos",
		                    type: "POST",
		                    data: function (d) {
		                        var contador = 0;
		                        datos =  FormSerialize($("#filtro_envios_masivo_ofsc"));
		                        for (var i = datos.length - 1; i >= 0; i--) {
		                            if (datos[i].split("[]").length > 1) {
		                                d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
		                                contador++;
		                            } else {
		                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
		                            }
		                        }
		                        if ($("#collapse3").css("display") == "none") {
		                        	d.codactuacion = "";
		                        }
		                    }
		                },
		                "columns":[
		                   { data: function (row, type, val, meta){
		                   	/*estilos = htmlCheckboxFilasMasivo(row.codactu);
		                   	//console.log(estilos);
                                		texto= "<span class='checkbox_fila' "+estilos[0]+" "+estilos[1]+" ><input type='checkbox' name='checkbox_actu[]'   "+estilos[2]+"codactu='"+row.codactu+"'/></span><span class='txt_id'>"+row.codactu+"</span>";
                                		return texto;*/
                                		return row.codactu;
		                   }, "class":"colunm-index", name:'codactu'},
		                    { data: 'aid', name:'aid'},
		                    { data: 'fec_movimiento', name: 'fec_movimiento'},
		                    {data:'empresa', name: 'empresa'},
		                    { data: 'mdf', name: 'mdf'},
		                    { data: function ( row, type, val, meta) {
		                        if (row.actividad_id === null)
		                            row.actividad_id = "";
		                        if (row.tipo_actividad_id === null)
		                            row.tipo_actividad_id = "";
		                        return "<b>"+row.actividad_id+"</b><br>"+row.tipo_actividad_id;
		                        }, name: 'tipo_actividad_id'
		                    },
		                    { data: 'mensaje'},
		                     { data: 'tipo_masivo'},
		                     { data: 'estado_ofsc', name: 'estado_ofsc'},
		                     { data: 'estado_psi', name: 'estado_psi'},
		                     {data: 'numintentos', name: 'numintentos'},
		                    { data: 'estado'},
		                    //{ data: 'estado'}
		                    { data: function ( row, type, val, meta) {
		                        return "<i class='fa fa-car btn-orange btn-detalle-gestiones btn btn-info' data-codactu='"+row.codactu+"'></i>"; //moment(row.created_at).format("YYYY-MM-DD");
		                        }
		                    }
		                ],
		                "columnDefs": [
		                    {
		                        "targets": 12,
		                        "orderable": false,
		                        "searchable": false
		                    },
		                ],
		                paging: true,
		                lengthChange: false,
		                searching: false,
		                ordering: true,
		                order: [[ 2, "desc" ]],
		                info: true,
		                autoWidth: true,
		                language: config
		            });
		
		return GLOBAL.tabla_bandeja_ordenes_masivo;
		
		},
		exportar: function(){
			$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='empresa_masivo' name='empresa_masivo' value='"+$("#slct_empresa_masivo").val()+"' /> ");
			//$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='codigoactuacion' name='codactuacion' value='' /> ");
			//$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='length' name='length' value='0' /> ");
			$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='actividad_tipo_masivo' name='actividad_tipo' value='"+$('#slct_actividad_tipo_masivo').val()+"' /> ");
			$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='nodo_masivo' name='nodo_masivo' value='"+$('#slct_nodo_masivo').val()+"' /> ");
			//$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='envio_masivo_ofsc_grupo_id' name='envio_masivo_ofsc_grupo_id' value='"+$('#slct_enviomasivofscgrupo').val()+"' /> ");
			$("#filtro_envios_masivo_ofsc").append("<input type='hidden' id='tipo_masivo' name='tipo_masivo' value='"+$('#slct_tipo_masivo').val()+"' /> ");

			$("#filtro_envios_masivo_ofsc").prop("method", "POST");
			$("#filtro_envios_masivo_ofsc").prop("action", "enviosofsc/reporteofsc");
			submit = $("#filtro_envios_masivo_ofsc").submit();

			$("#filtro_envios_masivo_ofsc #empresa_masivo").remove();
			$("#filtro_envios_masivo_ofsc #codigoactuacion").remove();
			$("#filtro_envios_masivo_ofsc #length").remove();
			$("#filtro_envios_masivo_ofsc #actividad_masivo").remove();
			$("#filtro_envios_masivo_ofsc #actividad_tipo_masivo").remove();
			$("#filtro_envios_masivo_ofsc #nodo_masivo").remove();
			$("#filtro_envios_masivo_ofsc #envio_masivo_ofsc_grupo_id").remove();
			$("#filtro_envios_masivo_ofsc #tipo_masivo").remove();
		},
		   
		  getGestionesMovimientos: function(codactu, row, tr){
		  	gestiones = functionajax("gestion_movimiento/cargar", "POST", false, "json", false, {codactu: codactu, detalle: true}); 
		  	gestiones.success(function(data){
		  		//console.log(data);
		  		if(data.rst ===1){
		  			row.child(data.html).show();
	                    			tr.addClass('shown');
		  		}
		  	});
		  }
	};
</script>