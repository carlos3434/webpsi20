<script>
    var MovimientoObservaciones = {
        listarMovimientos: (function () {
            var datos="";
            var targets=0;
            var columnDefs = [
                {
                    "targets": 0,
                    "data": "gestion_id",
                    "name": "gestion_id"
                }
            ];
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "codactu",
                                "name": "codactu"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "empresa",
                                "name": "empresa"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "quiebre",
                                "name": "quiebre"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fftt",
                                "name": "fftt"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "actividad",
                                "name": "actividad"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Ult_Fecha",
                                "name": "Ult_Fecha"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Ult_Motivo",
                                "name": "Ult_Motivo"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Ult_SubMotivo",
                                "name": "Ult_SubMotivo"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Ult_Estado",
                                "name": "Ult_Estado"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Ult_tecnico",
                                "name": "Ult_tecnico"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Mov_T",
                                "name": "Nro_Mov_T"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Mov_MaIg",
                                "name": "Nro_Mov_MaIg"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Mov_Me",
                                "name": "Nro_Mov_Me"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Fecha_Agenda_Ma",
                                "name": "Nro_Fecha_Agenda_Ma"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Fecha_Agenda_Me",
                                "name": "Nro_Fecha_Agenda_Me"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Inicio_MaIg",
                                "name": "Nro_Inicio_MaIg"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Inicio_Me",
                                "name": "Nro_Inicio_Me"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Cierre_MaIg",
                                "name": "Nro_Cierre_MaIg"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Nro_Cierre_Me",
                                "name": "Nro_Cierre_Me"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Total_Obs",
                                "name": "Total_Obs"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Total_Obs_MaIg",
                                "name": "Total_Obs_MaIg"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Total_Obs_Me",
                                "name": "Total_Obs_Me"
                            });

            for (var i = 1; i < 8; i++) {
            
                targets++;
                columnDefs.push({
                                    "targets": targets,
                                    "data": "Tp"+i,
                                    "name": "Tp"+i
                                });
                targets++;
                columnDefs.push({
                                    "targets": targets,
                                    "data": "Tp"+i+"_MaIg",
                                    "name": "Tp"+i+"_MaIg"
                                });
                targets++;
                columnDefs.push({
                                    "targets": targets,
                                    "data": "Tp"+i+"_Me",
                                    "name": "Tp"+i+"_Me"
                                });

            };

            console.log(columnDefs);

            $('#t_movimientoObservaciones').dataTable().fnDestroy();
            $('#t_movimientoObservaciones')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,
                        "searchable": true,                       
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        "ajax": {
                            url: "gestion_movimiento/movimientoobservaciones",
                            type: "POST",
                            cache: false,
                            "data": function(d){
                                var contador=0;
                                datos=$("#form_movimientoObservaciones").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");

                                for (var i = datos.length - 1; i >= 0; i--) {
                                    if( datos[i].split("[]").length>1 ){
                                        d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                        contador++;
                                    }
                                    else{
                                        d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                                    }
                                };
                            },
                        },
                        columnDefs
                    }); 
        }),
    }
</script>
