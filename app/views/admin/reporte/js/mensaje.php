<script type="text/javascript">
    var datoId;
    var iDatatable;
    var idDatatable = 'datatable_mensaje';

    $(document).ready(function () {

        iDatatable = Mensaje.cargar();

        $('#' + idDatatable + ' tbody').on('click', 'a', function () {
            var tr = $(this).closest('tr');
            var table = iDatatable;
            data = table.row(tr).data();
            $('#mensajeDetalleModal').on('show.bs.modal', function () {
                $('#txt_body').val(data.body);
                $('#txt_address').val(data.address);
                $('#txt_job_id').val(data.job_id);
            });
            $('#mensajeDetalleModal').on('hidden.bs.modal', function (e) {
                $('#txt_body').val('');
                $('#txt_address').val('');
                $('#txt_job_id').val('');
            });
        });
        $('#form_mensaje').on('submit', function(e) {
            iDatatable.draw();
            e.preventDefault();
        });

        $('#col11_filter,#txt_rangofecha').daterangepicker({
            format: 'YYYY-MM-DD'
        });
    });

</script>