<script type="text/javascript">
	$(document).ready(function() {
		 localStorage.removeItem("bandejaMasivoActivo");
    		 localStorage.removeItem("bandejaBusqueda");
		ids = [];
		GLOBAL.data_usuario = {usuario: 1};
		 GLOBAL.data_bandeja = {bandeja: 1};
		$('#myTab a').click(function (e) {
		      e.preventDefault();
		      $(this).tab('show');
		      href = $(this).attr("href");
		      if (href == "#t_envio") {
		      	if( typeof $("ul[data-select=slct_empresa]").html()=="undefined")
		      		slctGlobal.listarSlct('empresa','slct_empresa','multiple',ids,GLOBAL.data_usuario,0,'','E');
		      	if( typeof $("ul[data-select=slct_actividad]").html()=="undefined")
		      		slctGlobal.listarSlct('actividad','slct_actividad','multiple', ids, GLOBAL.data_bandeja, 0, '#slct_actividad_tipo','A');
		      	/*if( typeof $("ul[data-select=slct_actividad_tipo]").html()=="undefined")
		      		slctGlobal.listarSlct('actividadtipo','slct_actividad_tipo','multiple',ids,0,1);*/
		      	if( typeof $("ul[data-select=slct_nodo]").html()=="undefined")
		      		slctGlobal.listarSlct('nodo','slct_nodo','multiple',ids,GLOBAL.data_bandeja);
		      	if( typeof $("ul[data-select=slct_estado_ofsc]").html()=="undefined")
		      		slctGlobal.listarSlct('estadoofsc', 'slct_estado_ofsc', 'multiple');
		      	 slctGlobalHtml("slct_tipo_agendamiento", "simple");
		      		//S$("#t_enviosofs").DataTable().destroy();
		      		//BusquedaOrdenesOfsc();
		      		//tablaEnvioOfsc = GLOBAL.table_enviosofsc;
		      	//setTimeout(function(){BusquedaOrdenesOfscMasivo();}, 2000);
		      }
		      //cuadno hace click en lista actualizar los datos , si hace click en tab TABLA reiniar filtros
		    });
		/** cargando combos **/
		 eventoCargaMostrar();
		 slctGlobal.listarSlct('empresa','slct_empresa_masivo','multiple',ids,GLOBAL.data_usuario,0,'','E');
		 //slctGlobal.listarSlct('actividad','slct_actividad_masivo','multiple', ids, GLOBAL.data_bandeja, 0, '#slct_actividad_tipo_masivo','A');
		 slctGlobal.listarSlct('actividadtipo','slct_actividad_tipo_masivo','multiple', ids, 0, 1);
		 slctGlobal.listarSlct('nodo','slct_nodo_masivo','multiple');
		 slctGlobal.listarSlct('estado','slct_estado_psi','multiple');
		 slctGlobal.listarSlct('estadoofsc','slct_estado_ofsc_masivo','multiple');
		// slctGlobal.listarSlctpost('lista','enviomasivoofscgrupo','slct_enviomasivofscgrupo', 'multiple');
		 slctGlobalHtml('slct_tipo_masivo', 'simple');
		  slctGlobalHtml("slct_estado_masivo", "simple");

		 eventoCargaRemover();
		 $("#buscar-ordenes").click( function(e){
		 	$("#t_enviosofs").DataTable().destroy();
			GLOBAL.table_enviosofsc = BusquedaOrdenesOfsc();
			e.preventDefault();
		});
		 $("#filtro_envios_ofsc .fa-download").click(function(e){
		 	ExportarOrdenesOfsc();
		 });

		 $("#buscar-ordenes-masivas").click( function(e){
			BusquedaOrdenesOfscMasivo();
			e.preventDefault();
		});
		 $("#filtro_envios_masivo_ofsc  .fa-download").click(function(e){
		 	ExportarOrdenesMasivoOfsc();
		 });
		  $(".btn-activar-masivo").click(function(e){
		        activarEnvioMasivo($(this));
		    });
		  $(".btn-ejecutar-masivo").click(function(e){
		        envioMasivoOFSC();
		    });
	});

	BusquedaOrdenesOfsc = function(){
		tablaEnvioOfsc = OrdenesOfsc.listar();
		return tablaEnvioOfsc;
	};
	ExportarOrdenesOfsc  = function(){
		$("#filtro_envios_ofsc").append("<input type='hidden' name='download_excel' value='1' />");
		OrdenesOfsc.exportar();
		$("#filtro_envios_ofsc [name=download_excel]").remove();
	};
	ExportarOrdenesMasivoOfsc  = function(){
		$("#filtro_envios_masivo_ofsc").append("<input type='hidden' name='download_excel' value='1' />");
		OrdenesOfscMasivo.exportar();
		$("#filtro_envios_masivo_ofsc [name=download_excel]").remove();
	};

	FormSerialize  = function (form) {
		return  form.serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
	};
	BusquedaOrdenesOfscMasivo = function(){
		$("#t_enviosofs_masivo").DataTable().destroy();
		tablaEnvioMasivoOfsc = OrdenesOfscMasivo.listar();
		return tablaEnvioMasivoOfsc;
	};
	BusquedaOrdenesOfscMasivo();

	
	 $(document).delegate(".btn-detalle-ofsc", "click", function() {
	 	
		tr = $(this).parent().closest('tr');
		row = tablaEnvioOfsc.row(tr);
		 if ( row.child.isShown() ) {
		            row.child.hide();
		            tr.removeClass('shown');
		 } else {
		 	eventoCargaMostrar();
		 	 OrdenesOfsc.getDetalleOfsc($(this), row, tr);
		 	 setTimeout(function(){eventoCargaRemover();}, 1000 );
		 }
		 
	});
	 $(document).delegate(".btn-historial-envios-ofsc", "click", function() {
	 	OrdenesOfsc.getDetalleEnvios($(this).attr("data-codactu"));
	 });

	 $(document).delegate(".btn-detalle-gestiones", "click", function(){
	 	tr = $(this).parent().closest('tr');
		row = tablaEnvioMasivoOfsc.row(tr);
		 if ( row.child.isShown() ) {
		            row.child.hide();
		            tr.removeClass('shown');
		 } else {
		 	eventoCargaMostrar();
		 	 OrdenesOfscMasivo.getGestionesMovimientos($(this).attr("data-codactu"), row, tr);
		 	 setTimeout(function(){eventoCargaRemover();}, 1000 );
		 }
	 });

	 $(document).delegate(".checkbox_fila input[type=checkbox]", "click", function(){
	 	console.log($(this));
	 	updateEstadoCheckboxMasivo($(this));
	 });
	 
</script>