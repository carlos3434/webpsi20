<script type="text/javascript">
    $(document).ready(function () {
        $('#fecha_agenda').daterangepicker({
            format: 'YYYY-MM-DD'
        });

        $("#mostrar").click(MostrarWsEnvios);
        $("#t_wsenvios").dataTable();
    });
    
    // Con BD
    //  controlador,slct,tipo,usuarioV,afectado,afectados,slct_id
    var ids = []; //para seleccionar un id
    var data = {usuario: 1};
    slctGlobal.listarSlct('tecnico','slct_tecnico','multiple',ids,0,1);

    
    MostrarWsEnvios = function (){
        var fecha_agenda = $("#fecha_agenda").val();
        if (fecha_agenda.trim() === "") {
            alert("Seleccione Rango de Fecha");
        } else {
            WsEnvios.CargarWsEnvios();
        }
    }
</script>