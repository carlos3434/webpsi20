<script type="text/javascript">
    
    var Mensaje =
            {
                cargar: (function () {
                    var oTable = $('#datatable_mensaje')
                            .on('order.dt', function () {
                                eventoCargaMostrar();
                            })
                            .on('search.dt', function () {
                                eventoCargaMostrar();
                            })
                            .on('page.dt', function () {
                                eventoCargaMostrar();
                            })
                            .DataTable({
                                dom: "<'box'"+
                                    "<'box-header'<l>r>"+
                                    "<'box-body table-responsive no-padding't>"+
                                    "<'row'<'col-xs-6'i><'col-xs-6'p>>"+
                                    ">",
                                "processing": true,
                                "serverSide": true,
                                //"responsive": true,
                                "ajax": {
                                    url: "mensaje/listar",
                                    type: "POST",
                                    data: function (d) {
                                        d.txt_rangofecha = $('input[name=txt_rangofecha]').val();
                                    }
                                },
                                "columns": [
                                    {data: 'id', name: 'id', visible: false},
                                    {data: 'message_id', name: 'message_id'},
                                    {data: 'company_id', name: 'company_id', visible: false},
                                    {data: 'send_to', name: 'send_to'},
                                    {data: 'subject', name: 'subject'},
                                    {data: 'body', name: 'body', visible: false, orderable: false, searchable: false},
                                    {data: 'created_at', name: 'created_at'},
                                    {data: 'updated_at', name: 'updated_at'},
                                    {data: 'estado', name: 'estado'},
                                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
                                ],
                                "order": [[6, 'desc']],
                                searchDelay: 750,
                                initComplete: function (settings, json) {
                                    this.api().columns().eq(0).each(function (index) {
                                        if (index == 12) {
                                            return false;
                                        }
                                        var column = this.column(index);
                                        var input = document.createElement("input");
                                        var title = $(column.footer()).text();
                                        $(input).css('width', '100%')
                                                .attr('placeholder', title)
                                                .addClass('form-control input-sm')
                                                .appendTo($(column.footer()).empty())
                                                .on('change', function () {
                                                    column.search($(this).val(), false, false, true).draw();
                                                });
                                    });
                                },
                                drawCallback: function (settings) {
                                    $(".overlay,.loading-img").remove();
                                },
                                "createdRow": function (row, data, dataIndex) {
                                    if ((data['deleted_at']) != null) {
                                        $('td', row).css('color','#DF0101'); //.eq(1)
                                    }
                                }
                            });
                    return oTable;
                }),
            };
            
</script>