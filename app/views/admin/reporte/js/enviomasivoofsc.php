<script type="text/javascript">
    var PG = '';
    $(document).ready(function () {
        $("#slct_tipo").change(ValidaTipo);
        $("#btn_personalizado").click(personalizado);
        //$("#btn_general").click(general);

        dt_envio_masivo = EnvioMasivo.cargar();

        $('#btn_filtro').on('click', function (e) {
            PG = 'G';
            dt_envio_masivo.draw();
            e.preventDefault();
        });

        $('#fecha_created_at').daterangepicker({
            format: 'YYYY-MM-DD'
        });
        $('#fecha_created_at').val(
            moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
            moment(new Date()).format("YYYY-MM-DD")
        );
        
        var ids = []; //para seleccionar un id
        var data = {bandeja: 1};
        slctGlobalHtml('slct_tipo', 'simple');
        slctGlobalHtml('slct_estado', 'simple');
        slctGlobalHtml('slct_tipo_masivo', 'simple');
        slctGlobal.listarSlct('mdf','slct_mdf','multiple',ids,data);
        slctGlobal.listarSlctpost('lista','enviomasivoofscgrupo','slct_grupo', 'multiple');
        //slctGlobal.listarSlct('actividad','slct_actividad','multiple');
        slctGlobal.listarSlct('actividad','slct_actividad','multiple', ids, data, 0, '#slct_actividad_tipo','A');
        slctGlobal.listarSlct('actividadtipo','slct_actividad_tipo','multiple',ids,0,1);

        $('#descarga_checkFiltro').on('ifUnchecked', function (event) {
            $('#descarga_textFiltro').val(0);
        });
        $('#descarga_checkFiltro').on('ifChecked', function (event) {
            $('#descarga_textFiltro').val(1);
        });
    });

    personalizado=function(){
        if( $("#slct_tipo").val()=='' ){
            alert("Seleccione Tipo Filtro");
            $("#slct_tipo").focus();
        }
        else if( $("#txt_buscar").val()=='' ){
            alert("Ingrese datos");
            $("#txt_buscar").focus();
        }
        else{
            PG = 'P';
            dt_envio_masivo.draw();
            e.preventDefault();
        }
    }

    ValidaTipo=function(){
        $("#txt_buscar").val("");
        $("#txt_buscar").focus();
    }

    descargarReporte = function () {
       $("form#form_General").attr("action", "reporte/enviomasivoexcel");
        $("#form_General").submit();
    };
</script>
