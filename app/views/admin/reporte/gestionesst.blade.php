<!DOCTYPE html>
@extends('layouts.masterv2')

@push("stylesheets")
                {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
                {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
                {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
                {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
                {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
                @include( 'admin.js.slct_global_ajax' )
        @include( 'admin.js.slct_global' )
                <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
 .tbl-scroll{
                                max-height: 450px;
                                overflow: hidden;
                }
                fieldset{
                                max-width: 100% !important;
                                border: 1px solid #999;
                                padding:5px 15px 8px 15px;
                                border-radius: 10px;
                }
                legend{
                                font-size:14px;
                                font-weight: 700;
                                width: 12%;
                                border-bottom: 0px;
                                margin-bottom: 0px;
                }
                .slct_days{
                        border-radius: 5px !important;
                }

                .bootstrap-tagsinput{
                                background-color:#F5F5F5 !important;
                                border-radius:7px !important;
                                border: 1px solid;
                                padding:5px;
                        }

                        .bootstrap-tagsinput .label-info{
                                background-color: #337ab7 !important;
                        }

                        .bootstrap-tagsinput input{
                                display: none;
                        }

                        .btn-yellow{
                                color: #0070ba;
                                background-color: ghostwhite;
                                border-color: #ccc;
                                font-weight: bold;
                }
</style>
<section class="content-header">
        <h1>Gestiones de Sol. Tecnicas</h1>
</section>
<section class="content">
        <div class="row">
                <div class="col-xs-11 filtros">
                <form name="form_buscar" id="form_buscar" method="POST" action="">
                <div class="panel-group" id="acordion_filtros">
                    <div class="panel panel-default">
                        <div class="panel-heading box box-primary">
                            <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda Personalizada</a>
                           </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                        <div class="personalizado">
                                        <div class="box-body">
                                                <div class="row">
                                                        <div class="col-md-3">
                                                                        <label>Fechas:</label>
                                                                        <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                                </div>
                                                                        <input type="text" class="form-control pull-right" name="txt_rangoFechas" id="txt_rangoFechas" value="<?php echo date("Y-m-d")." - ".date("Y-m-d"); ?>" readonly=""/>
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                        <label class="control-label">Tipo Legado:</label>
                                                                        <select class="form-control" multiple="multiple"
                                                                        name="slct_tipo_legado[]" id="slct_tipo_legado">
                                                             </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                        <label>Actividad</label>
                                                            <select class="form-control" multiple="multiple"
                                                            name="slct_actividad[]" id="slct_actividad">
                                                            </select>
                                                                </div>
                                                                <!--<div class="col-md-3">
                                                                        <label class="control-label">Bucket:</label>
                                                                        <select class="form-control" multiple="multiple"
                                                                name="slct_buckets[]" id="slct_buckets"></select>
                                                                </div>-->
                                                </div>
                                        </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
                </form>
                </div>

        <div class="col-xs-1  botones">
                <a href="#" id="btnBuscar" class="btn btn-danger col-md-12" title="buscar"><i class="fa fa-search fa-lg"></i> Buscar</a>

        </div>
        </div>
        <div class="row">
                <div class="row">
                        <div class="box-body table-responsive">
                                <table id="tb_gestionesSt" class="table table-bordered table-striped dataTable
                                        table-hover" style="background: white">
                                        <thead>
                                                <tr>
                                                        <th style="width: 8%">Fecha</th>
                                                        <th style="width: 8%">Legado</th>
                                                        <th style="width: 8%">Actividad</th>
                                                        <th style="width: 10%">Cantidad</th>
                                                        <th style="width: 3%"> </th>
                                                </tr>
                                        </thead>
                                        <tbody id="tbody_cor_tecnico">

                                        </tbody>
                                </table>
                        </div>
                </div>
        </div>
</section>

{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/vue.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/vue/vue-resource.min.js") }}

@include( "admin.js.slct_global_ajax")
@include( "admin.js.slct_global")

<script type="text/javascript">
                var tipoPersonaId = "{{Session::get('tipoPersona')}}";
                var perfilId = "{{Session::get('perfilId')}}";
</script>
<script type="text/javascript" src="js/admin/reporte/gestionesst_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/reporte/gestionesst.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')