<!DOCTYPE html>
@extends('layouts.masterv2')

@push("stylesheets")
                {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
                {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
                {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
                {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
                {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
                @include( 'admin.js.slct_global_ajax' )
        @include( 'admin.js.slct_global' )
                <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
 .tbl-scroll{
                                max-height: 450px;
                                overflow: hidden;
                }
                fieldset{
                                max-width: 100% !important;
                                border: 1px solid #999;
                                padding:5px 15px 8px 15px;
                                border-radius: 10px;
                }
                legend{
                                font-size:14px;
                                font-weight: 700;
                                width: 12%;
                                border-bottom: 0px;
                                margin-bottom: 0px;
                }
                .slct_days{
                        border-radius: 5px !important;
                }

                .bootstrap-tagsinput{
                                background-color:#F5F5F5 !important;
                                border-radius:7px !important;
                                border: 1px solid;
                                padding:5px;
                        }

                        .bootstrap-tagsinput .label-info{
                                background-color: #337ab7 !important;
                        }

                        .bootstrap-tagsinput input{
                                display: none;
                        }

                        .btn-yellow{
                                color: #0070ba;
                                background-color: ghostwhite;
                                border-color: #ccc;
                                font-weight: bold;
                }
</style>
<section class="content-header">
        <div class="pull-left">
            <h2>Solicitudes en Proceso - TOA</h1>
        </div>
        <div class="alert alert-danger pull-right" style="margin-top:10px;">
            <strong><i class="glyphicon glyphicon-time"></i> Horas: </strong><br> 
            <p>10 h - 13 h - 16 h - 18 h - 19 h - 20 h - 22 h</p>
        </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-10 filtros">
            <form name="form_buscar" id="form_buscar" method="POST" action="">
                <div class="panel-group" id="acordion_filtros">
                    <div class="panel panel-default">
                        <div class="panel-heading box box-primary">
                            <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda Personalizada</a>
                           </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="personalizado">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Tipo:</label>
                                                <select class="form-control" name="slct_tipo[]" id="slct_tipo">
                                                      <option value="">.:: Seleccione ::.</option>
                                                  </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Fechas:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" name="txt_rangoFechas" id="txt_rangoFechas" value="<?php echo date("Y-m-d")." - ".date("Y-m-d"); ?>" readonly=""/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Buckets:</label>
                                                <select class="form-control" name="slct_buckets[]" id="slct_buckets" multiple="">
                                                      <option value="">.::Seleccione::.</option>
                                                  </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-2  botones">
            <div class="row">
                <a href="#" id="btnBuscar" class="btn btn-danger col-md-10" title="Buscar"><i class="fa fa-search fa-lg"></i> Buscar</a>  
            </div>
            <div class="row">
                <a href="#" id="btnExportar" class="btn btn-success col-md-10" title="Exportar .csv en linea (7 dias)" style="margin-top:3px;"><i class="fa fa-file-excel-o"></i> Exportar </a>
            </div>
            <form name="form_reporte" id="form_reporte" method="post" action="solicitudesenproceso/excel" enctype="multipart/form-data" style="display: none;">
            <br>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="box-body table-responsive">
                <table id="tb_gestionesSt" class="table table-bordered table-striped dataTable
                        table-hover" style="background: white">
                    <thead>
                        <tr>
                                <th style="width: 8%">Bucket</th>
                                <th style="width: 8%">Appt Number</th>
                                <th style="width: 8%">Sol. Tecnica</th>
                                <th style="width: 8%">Status</th>
                                <th style="width: 8%">Date</th>
                                <th style="width: 4%">A Control</th>
                                <th style="width: 4%">Estado CMS</th>
                                <th style="width: 4%">Fecha Liquidacion CMS</th>
                                <th style="width: 4%">Rango Fechas</th>
                                <th style="width: 4%">Fecha Creacion</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_cor_tecnico">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/vue.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/vue/vue-resource.min.js") }}

@include( "admin.js.slct_global_ajax")
@include( "admin.js.slct_global")

<script type="text/javascript">
                var tipoPersonaId = "{{Session::get('tipoPersona')}}";
                var perfilId = "{{Session::get('perfilId')}}";
</script>
<script type="text/javascript" src="js/admin/reporte/solicitudesenproceso_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/reporte/solicitudesenproceso.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')