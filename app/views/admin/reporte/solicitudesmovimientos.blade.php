<!DOCTYPE html>
@extends("layouts.masterv2")
@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Reporte de Solicitudes Tecnicas por movimientos
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Reporte</a></li>
                        <li class="active">Reporte de Solicitudes Tecnicas por movimientos</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <form name="form_solicitudes" id="form_solicitudes" method="post" action="reporte/solicitudesmovimientos" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Inicia contenido -->
                                <div class="box">
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th colspan='2'>Rango de fechas de gestion</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="success">Fecha:</td >
                                                    <td>
                                                        <input type="text" class="form-control" placeholder="AAAA-MM-DD - AAAA-MM-DD" id="fecha" name="fecha" onfocus="blur()"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                <!-- Finaliza contenido -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Inicia contenido -->
                                <div class="box">
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th colspan='2'>Reporte de CMS</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="success">Averias:</td>
                                                    <td>
                                                        <a href="#" id="averias" name="Averias" onclick="descargaCmsAveria();">[ Descargar ]</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="success">Provisiones:</td>
                                                    <td>
                                                        <a href="#" id="provision" name="provision" onclick="descargaCmsProvision();">[ Descargar ]</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                <!-- Finaliza contenido -->
                            </div>
                        </div>
                        <div class="row" style='display: none;'>
                            <div class="col-xs-12">
                                <!-- Inicia contenido -->
                                <div class="box">
                                    <div class="box-body table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <th colspan='2'>Reporte de Gestel</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="success">Averias:</td>
                                                    <td>
                                                        <a href="#" id="averias" name="Averias" onclick="descargaAveriasGestel();">[ Descargar ]</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="success">Provision:</td>
                                                    <td>
                                                        <a href="#" id="provision" name="provision" onclick="descargaProvisionGestel();">[ Descargar ]</a>
                                                        </td >
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                                <!-- Finaliza contenido -->
                            </div>
                        </div>
                    </form>
                </section><!-- /.content -->
@endsection
@push('scripts')
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    <script type="text/javascript" src="js/admin/reporte/solicitudesmovimientos.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush