<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('css/admin/ordenesofsc.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
        {{ HTML::script('js/utils.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

    @include( 'admin.js.tareas' )

    
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Ordenes en OFSC
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Reporte</a></li>
                        <li class="active">Ordenes en OFSC</li>
                    </ol>
                </section>

<!-- Main content -->
                <section class="content">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div>
                                
                            </div>
                            <div class="tab-content">
                                <ul id="myTab" class="nav nav-tabs" role="tablist">
                                    <li role="presentation" >
                                        <a href="#t_envio">Bandeja de Gestión</a>
                                    </li>
                                    <li role="presentation" class="active">
                                        <a href="#t_masivo">Masivos Automaticos a OFSC</a>
                                    </li>
                                </ul>
                                <div class="tab-pane" id="t_envio">
                                    <div class="">
                                    <form name="filtro_envios_ofsc" id="filtro_envios_ofsc">
                                        <div class="box-body table-responsive">
                                                <div class="col-xs-11 filtros">
                                                <div class="panel-group" id="acordion_envios_ofsc">
                                            
                                                <div class="panel panel-default">
                                                    <div class="panel-heading box box-primary">
                                                      <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc" href="#collapse2">Búsqueda Individual</a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse2" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                        <div class="box  personalizado">
                                                                    <div class="box-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-3">
                                                                                    <label class="titulo">Código Requerimiento</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="text" name="codigo_actuacion" class="form-control" />
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                  <div class="panel panel-default">
                                                    <div class="panel-heading box box-primary">
                                                      <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc" href="#collapse1">Busqueda Personalizada</a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse1" class="panel-collapse collapse in">
                                                      <div class="panel-body">
                                                          <div class="box  personalizado">
                                                                    <div class="box-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-3">
                                                                                    <label class="titulo">Fecha  Envío</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                                                        <input class="form-control" id="fecha_registro" name="fecha_registro" type="text"  aria-describedby="basic-addon1" readonly>
                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Empresa</label>
                                                                                    <select class="form-control" name="slct_empresa[]" id="slct_empresa" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Actividad</label>
                                                                                    <select class="form-control" name="slct_actividad[]" id="slct_actividad" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <!--<div class="col-sm-3">
                                                                                    <label class="titulo">Tipo Actividad</label>
                                                                                    <select class="form-control" name="slct_actividad_tipo[]" id="slct_actividad_tipo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div> -->
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Nodo</label>
                                                                                    <select class="form-control" name="slct_nodo[]" id="slct_nodo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Tipo Envio</label>
                                                                                    <select class="form-control" name="slct_tipo_agendamiento" id="slct_tipo_agendamiento" simple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                        <option value="1">Agenda</option>
                                                                                        <option value="2">SLA</option>
                                                                                    </select>
                                                                            </div>
                                                                            <!-- <div class="col-sm-3">
                                                                                    <label class="titulo">Gestion en OFSC</label>
                                                                                    <select class="form-control" name="slct_gestion_ofsc" id="slct_gestion_ofsc" simple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                        <option value="get_capacity">Consulta Capacidad</option>
                                                                                        <option value="inbound_interface">Realiza Agenda</option>
                                                                                    </select>
                                                                            </div> -->
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Estado en OFSC</label>
                                                                                    <select class="form-control" name="slct_estado_ofsc[]" id="slct_estado_ofsc" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                    </div>
                                                                </div>                           
                                                            </div>
                                                      </div>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                                    </div>
                                                    <div class="col-xs-1  botones">
                                                        <div class="">
                                                                <i class="fa fa-search fa-lg btn btn-primary" title="Buscar" id="buscar-ordenes"></i>
                                                                <i class="fa fa-download fa-lg btn btn-success" title="Descargar"></i>
                                                        </div>
                                                    </div>
                                            <input type="hidden" name="bandeja" value="ordenesofsc" />
                                        </form> 
                                            <div class="col-xs-12 box box-primary">
                                                    <div class="box-body table-responsive">
                                                        <table id="t_enviosofs" class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Cod. Req</th>
                                                                    <th>F. Registro</th>
                                                                    <th>F. PSI</th>
                                                                    <th>Actividad</th>
                                                                    <th>Quiebre</th>
                                                                    <th>Empresa</th>
                                                                    <th>MDF</th>
                                                                    <th>F. Agenda</th>
                                                                    <th>Est. OFSC</th>
                                                                    <th>Estado</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tb_enviosofs">
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Cod. Req</th>
                                                                    <th>F. Registro</th>
                                                                    <th>F. PSI</th>
                                                                    <th>Actividad</th>
                                                                    <th>Quiebre</th>
                                                                    <th>Empresa</th>
                                                                    <th>MDF</th>
                                                                    <th>F. Agenda</th>
                                                                    <th>Est. OFSC</th>
                                                                    <th>Estado</th>                            
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                            </div><!-- Finaliza Envíos OFSC -->
                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                </div><!-- /.tab -->

                                <div class="tab-pane fade active in" id="t_masivo">
                                    <div class="">
                                        <div class="box-body table-responsive">
                                            <form name="filtro_envios_masivo_ofsc" id="filtro_envios_masivo_ofsc">
                                                <div class="col-xs-11 filtros">
                                                <!-- Envíos OFSC -->
                                                <div class="panel-group" id="acordion_envios_ofsc_masivo">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading box box-primary">
                                                          <h4 class="panel-title">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc_masivo" href="#collapse3">Búsqueda Individual</a>
                                                          </h4>
                                                        </div>
                                                        <div id="collapse3" class="panel-collapse collapse in">
                                                            <div class="panel-body">
                                                            <div class="box  personalizado">
                                                                        <div class="box-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-3">
                                                                                        <label class="titulo">Código Requerimiento</label>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <input type="text" name="codactuacion" class="form-control" />
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading box box-primary">
                                                      <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc_masivo" href="#collapse4">Busqueda Personalizada</a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse4" class="panel-collapse collapse">
                                                            <div class="box box-primary personalizado">
                                                                    <div class="box-body">
                                                                            <div class="row">
                                                                            <!--<div class="col-sm-3">
                                                                                    <label class="titulo">Grupo Hora-Envio</label>
                                                                                    <select class="form-control" name="slct_envio_masivo_ofsc_grupo_id[]" id="slct_enviomasivofscgrupo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>-->
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Empresa</label>
                                                                                    <select class="form-control" name="slct_empresa_masivo[]" id="slct_empresa_masivo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <!--<div class="col-sm-3">
                                                                                    <label class="titulo">Actividad</label>
                                                                                    <select class="form-control" name="slct_actividad_masivo[]" id="slct_actividad_masivo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>-->
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Tipo Actividad</label>
                                                                                    <select class="form-control" name="slct_actividad_tipo_masivo[]" id="slct_actividad_tipo_masivo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Nodo</label>
                                                                                    <select class="form-control" name="slct_nodo_masivo[]" id="slct_nodo_masivo" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Tipo Masivo</label>
                                                                                    <select class="form-control" name="slct_tipo_masivo" id="slct_tipo_masivo" simple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                        <option value="MasivoArtisan">Ordenes Temporales</option>
                                                                                        <option value="MasivoBandeja">Filtradas de Bandeja</option>
                                                                                        <option value="Componentes "> Componentes de Ordenes</option>
                                                                                        <option value="MasivoLiquidado">Actividades Liquidadas</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Estado Envio</label>
                                                                                    <select class="form-control" name="slct_estado_masivo" id="slct_estado_masivo" simple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                        <option value="1">Envío Exitoso</option>
                                                                                        <option value="0">Envío Fallido</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Estado PSI / Legados</label>
                                                                                    <select class="form-control" name="slct_estado_psi[]" id="slct_estado_psi" multiple>
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Estado OFSC</label>
                                                                                    <select class="form-control" name="slct_estado_ofsc_masivo[]" id="slct_estado_ofsc_masivo" multiple >
                                                                                        <option value="">.::Seleccione::.</option>
                                                                                    </select>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                    <label class="titulo">Fecha  Movimiento</label>
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                                                        <input class="form-control" id="fecha_movimiento" name="fecha_movimiento" type="text"  aria-describedby="basic-addon1" readonly>
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                                </div>                             
                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1  botones">
                                                        <div class="">
                                                                <i class="fa fa-search fa-lg btn btn-primary" title="Buscar" id="buscar-ordenes-masivas"></i>
                                                                <i class="fa fa-download fa-lg btn btn-success" title="Descargar"></i>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="bandeja" value="ordenesmasivoofsc" />
                                            </form>
                                            <div class="col-xs-12 box box-primary">
                                                    <div class="box-body table-responsive">
                                                        <table id="t_enviosofs_masivo" class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Cod. Req</th>
                                                                    <th>AID</th>
                                                                    <th>Ult. Movimiento</th>
                                                                    <th>Empresa</th>
                                                                    <th>Nodo</th>
                                                                    <th>Actividad - Tipo</th>
                                                                    <th>Mensaje</th>
                                                                    <th>Tipo Masivo</th>
                                                                    <th>Ofsc</th>
                                                                    <th>PSI</th>
                                                                    <th># Intento</th>
                                                                    <th>Estado</th>
                                                                    <th>[]</th> 
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tb_enviosofs"></tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Cod. Req</th>
                                                                    <th>AID</th>
                                                                    <th>Ult. Movimiento</th>
                                                                    <th>Empresa</th>
                                                                    <th>Nodo</th>
                                                                    <th>Actividad - Tipo</th>
                                                                    <th>Mensaje</th>
                                                                    <th>Tipo Masivo</th>
                                                                    <th>Ofsc</th>
                                                                    <th>PSI</th>
                                                                    <th># Intento</th>
                                                                    <th>Estado</th>
                                                                    <th>[]</th>                                  
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                            </div><!-- Finaliza Envíos OFSC -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->        
@stop

@section('formulario')
    @include('admin.reporte.js.ordenesofsc_ajax')
    @include('admin.reporte.js.ordenesofsc')
    @if (Auth::user()->id === 743)
        <div class="contenedor-botones-flotantes ">
            <div class="contenedor-derecho panel panel-default">
                <a alt="Envio Masivo a OFSC" class="btn-activar-masivo btn btn-success">Activar Envio de Masivos</a>
                <a class="btn btn-primary btn-ejecutar-masivo">Enviar Masivo</a>
            </div>
            <div class="contenedor-izquierdo">
                <div class="panel-group" id="accordion-resultados">
                    <div class="panel-heading">
                        <h4>
                            <a class="accordion-toggle btn btn-primary" data-toggle="collapse"
                               data-parent="#accordion-resultados" href="#collapseThree">
                                Ordenes Seleccionadas <i class="fa fa-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table id="resultados-seleccionados"
                                   class="table table-bordered table-striped col-sm-12 responsive">
                                <thead>
                                <tr>
                                    <th>Cod Actuacion</th>
                                    <th>Estado</th>
                                </tr>
                                <tbody>

                                </tbody>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
