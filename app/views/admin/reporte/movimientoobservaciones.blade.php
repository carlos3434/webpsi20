<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.reporte.js.movimientoobservaciones_ajax' )
@include( 'admin.reporte.js.movimientoobservaciones' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Reporte Movimiento por Observaciones
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Movimiento Observaciones</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row form-group">
        <form id="form_movimientoObservaciones" name="form_movimientoObservaciones" method="POST" action="reporte/seguimiento">
            <div class="col-sm-12">
                <h3>Filtro(s):</h3>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <label>Quiebre:</label>
                    <select class="form-control" name="slct_quiebre[]" id="slct_quiebre" multiple>
                        <option value="">.::Seleccione::.</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <label>Empresa:</label>
                    <select class="form-control" name="slct_empresa[]" id="slct_empresa" multiple>
                        <option value="">.::Seleccione::.</option>
                    </select>
                </div>
                <div class="col-sm-3"> 
                    <label>Fecha:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right" placeholder="YYYY-MM-DD" name="fecha_agenda" id="fecha_agenda" value="<?php echo date("Y-m-d"); ?>" />
                    </div>
                </div>
                <div class="col-sm-3"> 
                    <label>&nbsp;</label>
                    <div class="input-group">
                        <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>&nbsp;&nbsp;
                        <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="box">

                <div class="box-body table-responsive">
                    <table id="t_movimientoObservaciones" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Gestion</th>
                                <th>codActu</th>
                                <th>Empresa</th>
                                <th>Quiebre</th>
                                <th>Fftt</th>
                                <th>Actividad</th>
                                <th>Ult Fecha</th>
                                <th>Ult Motivo</th>
                                <th>Ult SubMotivo</th>
                                <th>Ult Estado</th>
                                <th>Ult Tecnico</th>
                                <th>Nro_Mov_T</th>
                                <th>Nro_Mov_MaIg</th>
                                <th>Nro_Mov_Me</th>
                                <th>Nro_Fecha_Agenda_Ma</th>
                                <th>Nro_Fecha_Agenda_Me</th>
                                <th>Nro_Inicio_MaIg</th>
                                <th>Nro_Inicio_Me</th>
                                <th>Nro_Cierre_MaIg</th>
                                <th>Nro_Cierre_Me</th>
                                <th>Total_Obs</th>
                                <th>Total_Obs_Maig</th>
                                <th>Total_Obs_Me</th>
                                <th>Total_Obs</th>
                                <th>Total_Obs_MaIg</th>
                                <th>Total_Obs_Me</th>
                                <?php
                                    for ($i=1; $i < 8 ; $i++) { 
                                        echo "<th>Tp".$i."</th>";
                                        echo "<th>Tp".$i."_MaIg</th>";
                                        echo "<th>Tp".$i."_Me</th>";
                                    }
                                ?>
                            </tr>
                        </thead>
                        <tbody id="tb_movimientoObservacion">

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Gestion</th>
                                <th>codActu</th>
                                <th>Empresa</th>
                                <th>Quiebre</th>
                                <th>Fftt</th>
                                <th>Actividad</th>
                                <th>Ult Fecha</th>
                                <th>Ult Motivo</th>
                                <th>Ult SubMotivo</th>
                                <th>Ult Estado</th>
                                <th>Ult Tecnico</th>
                                <th>Nro_Mov_T</th>
                                <th>Nro_Mov_MaIg</th>
                                <th>Nro_Mov_Me</th>
                                <th>Nro_Fecha_Agenda_MaIg</th>
                                <th>Nro_Fecha_Agenda_Me</th>
                                <th>Nro_Inicio_MaIg</th>
                                <th>Nro_Inicio_Me</th>
                                <th>Nro_Cierre_MaIg</th>
                                <th>Nro_Cierre_Me</th>
                                <th>Total_Obs</th>
                                <th>Total_Obs_Maig</th>
                                <th>Total_Obs_Me</th>
                                <th>Total_Obs</th>
                                <th>Total_Obs_MaIg</th>
                                <th>Total_Obs_Me</th>
                                <?php
                                    for ($i=1; $i < 8 ; $i++) { 
                                        echo "<th>Tp".$i."</th>";
                                        echo "<th>Tp".$i."_MaIg</th>";
                                        echo "<th>Tp".$i."_Me</th>";
                                    }
                                ?>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>

</section><!-- /.content -->
@stop
