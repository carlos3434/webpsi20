<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.reporte.js.wsenvios_ajax' )
@include( 'admin.reporte.js.wsenvios' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Reporte WS Envios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Reporte de WS Envios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row form-group">
        <form id="form_wsenvios" name="form_wsenvios" method="POST">
            <div class="col-sm-12">
                <h3>Filtro(s):</h3>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <label>Tecnico:</label>
                    <select class="form-control" name="slct_tecnico[]" id="slct_tecnico" multiple>
                        <option value="">.::Seleccione::.</option>
                    </select>
                </div>
                
                <div class="col-sm-3"> 
                    <label>Fecha:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="fecha_agenda" id="fecha_agenda" value="<?php echo date("Y-m-d") . " - " . date("Y-m-d"); ?>" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <label>&nbsp;</label>
                    <div class="input-group">
                        <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="box">
                
                <div class="box-body table-responsive">
                    <table id="t_wsenvios" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Operacion</th>
                                <th>N° Tarea</th>
                                <th>Cod. Empleado</th>
                                <th>Distrito</th>
                                <th>Descripcion</th>
                                <th>Tipo</th>
                                <th>Response</th>
                                <th>[ . ]</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Fecha</th>
                                <th>Operacion</th>
                                <th>N° Tarea</th>
                                <th>Cod. Empleado</th>
                                <th>Distrito</th>
                                <th>Descripcion</th>
                                <th>Tipo</th>
                                <th>Response</th>
                                <th>[ . ]</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.reporte.form.wsenvios' ) 
@stop
