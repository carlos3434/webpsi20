<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

    {{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
    {{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
    {{ HTML::style('css/admin/reporte.css')}}


    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

    @include( 'admin.reporte.js.enviomasivoofsc_ajax' )
    @include( 'admin.reporte.js.enviomasivoofsc' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Reporte de envio masivo a OFSC
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Reporte</a></li>
        <li class="active">Reporte de envio masivo a OFSC</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="box">
                <br>
                <form name="form_Personalizado" id="form_Personalizado" method="POST" action="" class="form-inline"> 
                    <input type="hidden" name="bandeja" id="bandeja" value="1">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label" for="slct_tipo">Buscar por</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" name="slct_tipo" id="slct_tipo">
                                        <option value="">.::Seleccione::.</option>
                                        <option value="codactu">Código Actuación</option>
                                        <option value="aid">Aproipment Id</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                                </div>
                                <div class="col-sm-1">
                                    <a class="btn btn-primary" id="btn_personalizado">
                                        <i class="fa fa-search fa-lg"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <form name="form_General" id="form_General" class="envio-masivo" method="POST" action="">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">Grupo - Hora Envio:</label>
                                    <select class="form-control" name="slct_grupo[]" id="slct_grupo" multiple>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">MDF:</label>
                                    <select class="form-control" name="slct_mdf[]" id="slct_mdf" multiple>
                                        <option value="">.::Seleccione::.</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">Estado:</label>
                                    <select class="form-control" name="slct_estado" id="slct_estado">
                                        <option value="">.::Seleccione::.</option>
                                         <option value="1">Correcto</option>
                                        <option value="0">Fallido</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">Tipo Masivo:</label>
                                    <select class="form-control" name="slct_tipo_masivo" id="slct_tipo_masivo">
                                        <option value="">.::Seleccione::.</option>
                                         <option value="MasivoArtisan">Actividades Temporales</option>
                                         <option value="MasivoBandeja">Actividades Filtradas en Bandeja</option>
                                        <option value="Componentes">Componentes de Ordenes</option>
                                        <option value="MasivoLiquidado">Actividades Liquidadas</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-sm-12">

                         <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo" for="fecha_created_at">{{trans('reporte.rango_fecha')}}:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                        <input class="form-control" id="fecha_created_at" name="fecha_created_at" type="text" readonly aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">Actividad:</label>
                                    <select class="form-control" name="slct_actividad[]" id="slct_actividad" multiple>
                                        <option value="">.::Seleccione::.</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="titulo">Tipo Actividad:</label>
                                    <select class="form-control" name="slct_actividad_tipo[]" id="slct_actividad_tipo" multiple>
                                        <option value="">.::Seleccione::.</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <label class="titulo titulo-botones">Botones</label>
                                <a class="btn btn-primary" id="btn_filtro">
                                    <i class="fa fa-search fa-lg"></i>
                                </a>
                                <input type="hidden" id="descarga_textFiltro" name="descarga_textFiltro" value="0">
                                <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive" style="min-height:250px">
                    <table id="t_envio_masivo" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                        <thead>
                            <tr>
                                <th>Cod Actu</th>
                                <th>AID</th>
                                <th>Grupo - Fecha Envio</th>
                                <th>Nodo</th>
                                <th>Actividad - Tipo</th>
                                <th>Mensaje</th>
                                <th>Estado</th>
                                <!--<th>Fecha Envio</th>-->
                            </tr>
                        </thead>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop
