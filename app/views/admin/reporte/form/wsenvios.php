<div class="modal fade" id="wsenviosModal" tabindex="-1" role="dialog" aria-labelledby="wsenviosModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo trans('mantenimiento.detalle') ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea style="background: papayawhip" class="form-control resize-vertical" id="txt_message" rows="10" disabled></textarea>
                </div>
            </div>
            <div class="modal-footer" id="btnAccion">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
            </div>
        </div>
    </div>
</div>