<!-- /.modal -->
<div class="modal fade" id="envioMasivoOfscModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">


            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label># Proyecto:</label>
                        <input type="text" class="form-control" id="txt_proyecto" name="txt_proyecto" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>Fecha registro:</label>
                        <input type="text" class="form-control" id="txt_fecha_registro" name="txt_fecha_registro" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>Fecha termino:</label>
                        <input type="text" class="form-control" id="txt_fecha_termino" name="txt_fecha_termino" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>Segmento:</label>
                        <input type="text" class="form-control" id="txt_segmento" name="txt_segmento" readonly>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label>Avance (%):</label>
                        <input type="text" class="form-control" id="txt_avance" name="txt_avance" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>Validado (Call):</label>
                        <input type="text" class="form-control" id="txt_validado_call" name="txt_validado_call" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>gestion:</label>
                        <input type="text" class="form-control" id="txt_gestion_obra" name="txt_gestion_obra" readonly>
                    </div>
                    <div class="col-sm-3">
                        <label>Status:</label>
                        <input type="text" class="form-control" id="txt_status" name="txt_status" readonly>
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="col-sm-4">
                        <label>Tipo:</label>
                        <select class="form-control" id="slct_gcasuistica_modal" name="slct_gcasuistica_modal">
                            <option value="">.::Seleccione::.</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label>Casuistica:</label>
                        <select class="form-control" id="slct_gcasuistica_detalle_modal" name="slct_gcasuistica_detalle_modal">
                            <option value="">.::Seleccione::.</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label>Estado:</label>
                        <select class="form-control" id="slct_estado_modal" name="slct_estado_modal">
                            <option value="">.::Seleccione::.</option>
                        </select>
                    </div>
                </div>
            </div>

            <input type="hidden" id="txt_proyecto_edificio_id" name="txt_proyecto_edificio_id" readonly="true">

            <form id="form_proyecto_diseno" name="form_proyecto_diseno" action="" method="post" style="display:none; overflow: auto;height:200px;" autocomplete="off">
                <div class="row form-group">
                    <div class="col-sm-12" >
                        <div class="col-sm-3">
                            <label>Fecha Inicio:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_inicio" id="txt_fecha_inicio"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Compromiso:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Gestion:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_gestion" id="txt_fecha_gestion"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Estado General:</label>
                            <input type="text" class="form-control" name="txt_estado_general" id="txt_estado_general">
                        </div>
                    </div>
                    <div class="col-sm-12" >

                        <div class="col-sm-3">
                            <label>Gestion Interna:</label>
                            <input type="text" class="form-control" name="txt_gestion_interna" id="txt_gestion_interna">
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Termino:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_termino" id="txt_fecha_termino"/>
                        </div>
                    </div>
                </div>
            </form>

            <form id="form_proyecto_licencia" name="form_proyecto_licencia" action="" method="post" style="display:none; overflow: auto;height:200px;" autocomplete="off">
                <div class="row form-group">
                    <div class="col-sm-12" >
                        <div class="col-sm-3">
                            <label>Recep. Exped.:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_recepcion_expediente" id="txt_recepcion_expediente"/>
                        </div>
                        <div class="col-sm-3">
                            <label>Fecha Compromiso:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                        </div>
                        <div class="col-sm-3">
                            <label>Fecha Inicio:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_inicio" id="txt_fecha_inicio"/>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <label>Fecha Termino:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_termino" id="txt_fecha_termino"/>
                        </div>
                    </div>
                </div>
            </form>

            <form id="form_proyecto_oocc" name="form_proyecto_oocc" action="" method="post" style="display:none; overflow: auto;height:200px;" autocomplete="off">
                <div class="row form-group">
                    <div class="col-sm-12" >
                        <div class="col-sm-3">
                            <label>Fecha Inicio:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_inicio" id="txt_fecha_inicio"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Compromiso:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Termino:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_termino" id="txt_fecha_termino"/>
                        </div>
                    </div>
                </div>
            </form>

            <form id="form_proyecto_cableado" name="form_proyecto_cableado" action="" method="post" style="display:none; overflow: auto;height:200px;" autocomplete="off">
                <div class="row form-group">
                    <div class="col-sm-12" >
                        <div class="col-sm-3">
                            <label>Fecha Inicio:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_inicio" id="txt_fecha_inicio"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Compromiso:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                        </div>

                        <div class="col-sm-3">
                            <label>Fecha Inicio:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_inicio" id="txt_fecha_inicio"/>
                        </div>
                    </div>
                    <div class="col-sm-12" >
                        <div class="col-sm-3">
                            <label>Fecha Termino:</label>
                            <input type="text" class="form-control pull-right" placeholder="YYYY-MM-DD" name="txt_fecha_termino" id="txt_fecha_termino"/>
                        </div>
                    </div>
                </div>
            </form>

            <form id="form_proyecto_megas" name="form_proyecto_megas" action="" method="post" style="display:none; overflow: auto;height:200px;" autocomplete="off">
            </form>
            <div class="modal-footer">
                <button type="button" id="btn_close_modal" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                <button type="button" id="btn_gestion_modal" name="btn_gestion_modal" class="btn btn-primary">Guardar</button>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->
