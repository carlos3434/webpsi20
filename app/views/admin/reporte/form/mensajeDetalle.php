<div class="modal fade" id="mensajeDetalleModal" tabindex="-1" role="dialog" aria-labelledby="mensajeDetalleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="mensajeDetalleModalLabel"><?php echo trans('reporte.detalle') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo trans('reporte.email') ?></label>
                            <input type="text" class="form-control" id="txt_address" readonly="readonly" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label"><?php echo trans('reporte.id_tarea') ?></label>
                            <input type="text" class="form-control" id="txt_job_id" readonly="readonly" >
                        </div>
                    </div>       
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label"><?php echo trans('reporte.cuerpo') ?></label>
                            <textarea class="form-control resize-none" id="txt_body" rows="8" readonly="readonly" ></textarea>
                        </div>
                    </div>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo trans('reporte.cerrar') ?></button>
            </div>
        </div>
    </div>
</div>