<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
        {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
        {{ HTML::script('js/utils.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

    @include( 'admin.reporte.js.enviosofsc_ajax' )
    @include( 'admin.reporte.js.enviosofsc' )

    @include( 'admin.reporte.js.enviomasivoofsc_ajax' )
    @include( 'admin.js.tareas' )

    @include( 'admin.mantenimiento.js.logofsc_ajax' )
    @include( 'admin.mantenimiento.js.logofsc' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Reporte de Envíos OFSC
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Reporte</a></li>
                        <li class="active">Reporte de Envíos OFSC</li>
                    </ol>
                </section>

<!-- Main content -->
                <section class="content">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div>
                                <ul id="myTab" class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#t_envio">Envíos OFSC</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#p_log">Log OFSC</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="t_envio">
                                    <div class="box">
                                        <div class="box-body table-responsive">

                                            <!-- Envíos OFSC -->
                                            <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <!-- <label class="col-sm-1 control-label">Inicio</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="fecha_recepcion_ini" name="fecha_recepcion_ini" readonly="readonly"/>
                                                        </div>
                                                        <label class="col-sm-1 control-label">Final</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="fecha_recepcion_fin" name="fecha_recepcion_fin" readonly="readonly"/>
                                                        </div> -->
                                                        <label class="col-sm-1 control-label">Fechas</label>
                                                         <div class="col-sm-3">
                                                             <!-- <label>Fechas:</label> -->
                                                             <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                     <i class="fa fa-calendar"></i>
                                                                 </div>
                                                                 <input type="text" class="form-control pull-left" name="fecha_calen" id="fecha_calen" readonly="" />
                                                                 <div class="input-group-addon" onclick="cleandate()" style="cursor: pointer">
                                                                     <i class="fa fa-rotate-left" ></i>
                                                                 </div>
                                                             </div>
                                                         </div>



                                                        <div class="col-sm-4">
                                                            <button type="button" id="mostrar" class="btn btn-primary">Mostrar</button>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col-xs-12">
                                                    <div class="box-body table-responsive">
                                                        <table id="t_enviosofs" class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Accion</th>
                                                                    <th>FechaCreacion</th>
                                                                    <th>Usuario</th>
                                                                    <th>Enviado</th>
                                                                    <th>Respuesta</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody id="tb_enviosofs">
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <th>Accion</th>
                                                                    <th>FechaCreacion</th>
                                                                    <th>Usuario</th>
                                                                    <th>Enviado</th>
                                                                    <th>Respuesta</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div><!-- /.box-body -->
                                            </div><!-- Finaliza Envíos OFSC -->

                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                </div><!-- /.tab -->

                                <div class="tab-pane fade" id="p_log">
                                    <div class="box">
                                        <div class="box-body table-responsive">

                                            <!-- Inicia contenido -->
                                                <div class="box-header">
                                                    <h3 class="box-title">Filtros</h3>
                                                </div>
                                                <form name="form_logofsc" id="form_logofsc" method="POST" action="reporte/logexcel">

                                                    <div class="col-sm-12">
                                                        <div class="col-sm-2">
                                                        <label>ID:</label>
                                                        <input type="text" class="form-control pull-left" name="txt_idlog" id="txt_idlog"/>
                                                        </div>
                                                        <div class="col-sm-2">
                                                        <label>Tipo:</label>
                                                        <select class="form-control" name="slct_tipo[]" id="slct_tipo" multiple>
                                                            <option value="">.::Seleccione::.</option>
                                                        </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                             <label>Fechas:</label>
                                                             <div class="input-group">
                                                                             <div class="input-group-addon">
                                                                                 <i class="fa fa-calendar"></i>
                                                                             </div>
                                                                             <input type="text" class="form-control pull-left" name="fecha" id="fecha" readonly="" />
                                                                             <div class="input-group-addon" onclick="cleandate()" style="cursor: pointer">
                                                                                 <i class="fa fa-rotate-left" ></i>
                                                                             </div>
                                                             </div>
                                                         </div>
                                                        <div class="col-sm-1">
                                                           <label>&nbsp;</label><br>
                                                           <button type="button" id="btn_buscar" name="btn_buscar" class="btn btn-danger">
                                                               Buscar
                                                           </button>
                                                        </div>
                                                        <!--<div class="col-sm-2">
                                                            <label>&nbsp;</label><br>
                                                            <button id="btn_exportar" class="btn btn-success" name="btn_exportar"
                                                            type="button" onclick="descargarReporte();" > Exportar </button>
                                                        </div>-->
                                                    </div>



                                                    <table id="t_log" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th style='width:55px !important;' >Id</th>
                                                                <!-- <th style='width:70px !important;'>Proceso</th> -->
                                                                <th style='width:90px !important;'>Tipo</th>
                                                                <th style='width:150px !important;'>Body</th>
                                                                <th style="width: 100px !important;">Fecha</th>
                                                                <th style="width: 100px !important;">
                                                                    <!--<a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>-->[]
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tb_log">

                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th>Id</th>
                                                                <!-- <th>Proceso</th> -->
                                                                <th>Tipo</th>
                                                                <th>Body</th>
                                                                <th>Fecha</th>
                                                                <th style="width: 100px !important;"> []
                                                                </th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </form>
                                            <!-- Finaliza contenido -->

                                        </div><!-- /.box-body -->
                                    </div>
                                </div><!-- /.tab -->

                            </div>
                        </div>
                    </div>

                </section><!-- /.content -->

                <!-- Modal -->
                <div class="modal fade" id="ItemPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document" style="width:1200px">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Resumen Envíos OFSC</h4>
                      </div>
                    <div class="modal-body" id="cmodal">
                    </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                      </div>
                    </div>
                  </div>
                </div>



<div class="modal fade" id="logofscModal" class="logofscModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" id="modallog" >
    </div>
</div>


@stop
