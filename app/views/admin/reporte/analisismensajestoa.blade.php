<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/datepicker/css/datepicker.css")}}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style("css/admin/asistencia.css") }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
 <section class="content-header">
          <h1>Analisis de Mensajes TOA<small> </small></h1>
            <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                  <li><a href="#">Reporte</a></li>
                   <li class="active">Analisis de Mensajes TOA</li>
             </ol>
 </section>
 <section class="content" id="contenedor-mapaLocalizacionRecursos">
    <div class="container-fluid">
            <div class="row">
                    <div class="panel-group contenedor-tecnicos" id="contenedor-tecnicos">
                         <div class="panel panel-default">
                                 <div class="panel-heading box box-primary">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#contenedor-tecnicos"
                                       href="#collapseOne">Por Archivo:</a>
                                </h4>
                            </div>
                             <div id="collapseOne" class="panel-collapse collapse ">
                                    <div class="panel-body">
                                        <form name="form_file" id="form_file" method="post" action="analisismensajes/generar" enctype="multipart/form-data">
                                          <div class="col-md-4">
                                            <input type="file" name="archivomensajes" />
                                          </div>
                                          <div class="col-md-4">
                                            <input type="submit" name="" value="Generar Analisis" class="btn btn-primary" />

                                          </div>
                                        </form>
                                        <!--<div class="scroll">
                                            <div class="aside-body row form-group" id="tecnicos">
                                            </div>
                                        </div>-->
                                 </div>
                                 </div>
                             </div>
                  </div>
                 <div class="col-md-12">
                         <div class="box box-primary">
                             <div class="box-header">
                                 <div id="mapaLocalizacionRecursos">
                                 </div>
                             </div>
                         </div>
                   </div>
        </div>

     </div>
 </section>
@endsection

@section('formulario')
     @include( 'admin.ofsc.form.asistenciatecnico' )
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/datepicker/js/bootstrap-datepicker.js")}}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/bootbox.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/reporte/analisismensajestoa.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')