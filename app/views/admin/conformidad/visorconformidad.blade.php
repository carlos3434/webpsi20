<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}
    
    @include( 'admin.js.slct_global' )
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.cat.js.visorconformidad_ajax' )
    @include( 'admin.cat.js.visorconformidad' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
  .yellow-fieldset{
        max-width: 100% !important;
        border: 3px solid #999;
        padding:10px 50px 10px 9px;
        border-radius: 10px; 
    }
  .  
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Visor Conformidad
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Conformidad</a></li>
        <li class="active">Visor Conformidad {{$fecha = date("Y-m-d")}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-sm-12">
        <fieldset class="yellow-fieldset">
          <!--<div class="col-sm-2" style="padding-top: 5px">
              <span>Seleccionar Quiebre: </span>
          </div>-->
          <div class="col-sm-3">
            <label class="titulo">Seleccionar Quiebre: </label>
            <!--<select class="form-control" name="slct_quiebre" id="slct_quiebre"></select> -->
            <select class="form-control" name="slct_quiebre[]" id="slct_quiebre" multiple>
            </select>
          </div>
          <div class="col-sm-4"> 
              <label class="titulo">Fecha:</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-left"  name="fecha_registro" id="fecha_registro" readonly />
                  </div>
          </div>

          <div class="col-sm-3" style="margin-top: 23px">
              <span class="btn btn-primary btn-md btnSearch" onclick="searchbyQuiebre()"><i class="glyphicon glyphicon-zoom-in"></i> Buscar</span>
          </div>                                                 
      </fieldset>
  </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-body">
               {{-- <div class="row form-group"> --}}
                   <div class="col-sm-12">
                        <h3>Tickets Recepcionados</h3>
                        <div class="box-body table-responsive">
                        <table id="t_visor" class="table table-bordered table-striped col-sm-12 responsive text-center" width="100%">
                            <thead id="th_visor_general">
                                  <th style="width: 400px;">Tickets</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-0 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-1 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-2 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-3 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-4 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-5 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-6 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-7 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-8 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-9 days', strtotime($fecha))) }}</th>
                                  <th style="width: 100px;">{{ date('d M',strtotime('-10 days', strtotime($fecha))) }}</th>

                                  <th style="width: 100px;background-color: #F5F6CE;">Total</th>
                              </thead>
                              <tbody id="tb_visorgestion">
                                
                              </tbody>
                          </table>
                          
                   </div>
                   <div class="col-sm-12">
                            <div class="col-sm-2">
                                <label>Hace 10 minutos</label>
                                <input type="text" class="form-control input-sm" id="minuto10" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Hace 30 minutos</label>
                                <input type="text" class="form-control input-sm" id="minuto30" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Más 60 minutos</label>
                                <input type="text" class="form-control input-sm" id="minuto60" readonly>
                            </div>
                          </div>
                        </div>

                    <div class="col-sm-12">
                        <h3>Pendientes</h3>
                        <div class="box-body table-responsive">
                        <table id="t_visor" class="table table-bordered table-striped col-sm-12 responsive text-center" width="100%">
                            <thead class="th_visor_otros">
                                <th style="width: 400px;">Motivo</th>
                                <th style="width: 400px;">Submotivo</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-0 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-1 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-2 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-3 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-4 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-5 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-6 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-7 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-8 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-9 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-10 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-11 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-12 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-13 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-14 days', strtotime($fecha))) }}</th>

                                <th style="width: 100px;background-color: #F5F6CE;">Total</th>
                            </thead>
                            <tbody id="tb_visoroperaciones">
      <!--<TR>
        <TD ROWSPAN="2">
        Celda de dos filas
        </TD>
        <TD>
        Fila2,Columna2
        </TD>
      </TR>
      <TR>
        <TD>
        Fila3,Columna2
        </TD>
      </TR>-->
                              </tbody>
                          </table>
                        </div>
                   </div>
                   

                   <div class="col-sm-12">
                        <h3>Solucionados</h3>
                        <div class="box-body table-responsive">
                        <table id="t_visor2" class="table table-bordered table-striped col-sm-12 responsive text-center" width="100%">
                              <thead class="th_visor_otros">
                                <th style="width: 400px;">Motivo</th>
                                <th style="width: 400px;">Submotivo</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-0 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-1 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-2 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-3 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-4 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-5 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-6 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-7 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-8 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-9 days', strtotime($fecha))) }}</th>
                                <th style="width: 100px;">{{ date('d M',strtotime('-10 days', strtotime($fecha))) }}</th>
                                

                                <th style="width: 100px;background-color: #F5F6CE;">Total</th>
                              </thead>
                              <tbody id="tb_visor2">
                                
                              </tbody>
                          </table>
                          </div>
                   </div>
               {{-- </div> --}}
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.ofsc.form.reporte' )
     
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
     {{ HTML::style('css/admin/reportecat.css') }}


@stop