<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
@endpush

@section('contenido')

<style type="text/css">
  #pretemporalModal fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 15px 15px;
        border-radius: 10px; 
        margin-bottom: 2%;
    }
  #pretemporalModal legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
        color: #337ab7;
    }
  #pretemporalModal label{
      font-weight: 600 !important;
    }
</style>
<!-- Content Header (Page header) -->
<input type="hidden" id="token" value="{{ csrf_token() }}">
<section class="content-header">
    <h1>
        Bandeja de Gestion
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Bandeja de Gestion</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="panel-group" id="accordion-filtros">
            <div class="panel panel-default">
                <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                           href="#collapseOne">Búsqueda Individual</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in ">
                    <div class="panel-body">
                        <form name="form_Personalizado" id="form_Personalizado" method="POST" action="conformidad/buscar">
                            <div class="col-xs-8">
                                <div class="form-group ">
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label class="titulo">Buscar por:</label>
                                            <input type="hidden" name="bandeja" id="bandeja" value="1">
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="slct_tipo" id="slct_tipo">
                                                <option value="">Seleccione</option>
                                                <option value="ticket">Ticket de Pedido</option>
                                                <option value="codactu">Reg Avería</option>
                                                <option value="dniemb">DNI Embajador</option>
                                                <option value="dnicli">DNI Cliente</option>
                                                <option value="codcli">N°Servicio / Cod Cliente</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <button id="btn_personalizado" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                           href="#collapseTwo">
                            Búsqueda Personalizada
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form name="form_General" id="form_General" method="POST" action="conformidad/buscar">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label class="titulo">Tipo Averia:</label>
                                        <select class="form-control" name="slct_averia[]" id="slct_averia" multiple>
                                            <option value="Telefono">Telefono (STB)</option>
                                            <option value="Internet">Internet (ADSL)</option>
                                            <option value="Television">Television (CATV)</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="titulo">Quiebre:</label>{{-- slct_gestion --}}
                                        <select class="form-control" name="slct_quiebre[]" id="slct_quiebre"
                                                multiple>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="titulo">Atención:</label>
                                        <select class="form-control" name="slct_atencion[]" id="slct_atencion"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="titulo">Rango F. Registro:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="txt_fecha_registro"
                                                   id="txt_fecha_registro"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="titulo">Origen de Registro:</label>
                                        <select class="form-control" name="slct_gestion[]" id="slct_gestion"
                                                multiple>
                                            <option value="psi">Web Pedido (PSI)</option>
                                            <option value="embajador">Web Embajador</option>
                                            <option value="masiva">Masiva CAT</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label class="titulo">Motivo:</label>
                                        <select class="form-control" name="slct_motivo[]" id="slct_motivo"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label class="titulo">Submotivo:</label>
                                        <select class="form-control" name="slct_submotivo[]" id="slct_submotivo"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="titulo">Estado Registro</label>
                                        <select class="form-control" name="slct_estado[]" id="slct_estado"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="titulo">Estado Ticket</label>
                                        <select class="form-control" name="slct_estado_pre[]" id="slct_estado_pre"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <label class="titulo">Masiva Evento</label>
                                        <select class="form-control" name="slct_eventos[]" id="slct_eventos"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label class="titulo">Asignacion </label>
                                        <select class="form-control" name="slct_asignacion" id="slct_asignacion">
                                          <option value="">.::Seleccione::.</option>
                                          <option value="1">Asignados</option>
                                          <option value="2">No Asignados</option>
                                        </select>
                                    </div>

                                     <div class="col-sm-3">
                                        <label class="titulo">Usuarios</label>
                                        <select class="form-control" name="slct_usuarios[]" id="slct_usuarios"
                                                multiple>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-sm-2" style="float: right"><br>
                                      <div class="col-sm-6">
                                        <button id="btn_general" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                      </div>
                                      <div class="col-sm-6">
                                        <button id="btn_limpiar" type="button" class="btn btn-default pull-right"><i class="fa fa-refresh" title="Limpiar Filtros"></i></button>
                                      </div>
                                    </div>                                  
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
        <!--<div class="col-xs-1  botones">
      <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar"></i>
      <i class="fa fa-download fa-xs btn btn-success" id="btn-descargar" title="Descargar"></i>
    </div>-->
  </div>
        <div class="box box-primary">
          <div class="box-body" style="min-height:250px">
            <div class="box-body table-responsive">
              <table id="t_embajador" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                  <thead class="btn-primary">
                    <tr>
                      <th style="width: 60px;">N° Ticket</th>
                      <!-- <th style="width: 60px;">Codactu</th> -->
                      <th style="width: 89px;">F.Registro</th>
                      <th style="width: 89px;">F.UltMov</th>
                      <th style="width: 100px;">Tipo Averia</th>
                      <th style="width: 70px;">CodCli/Tel</th>
                      <th>Quiebre</th>
                      <th>Cliente</th>
                      <th style="width: 100px;">Motivo</th>
                      <th>Submotivo</th>
                      <th>Solución</th>
                      <th>Estado Registro</th>
                      <th>CodLiq</th>
                      <th>Area</th>
                      <th style="width: 100px !important;">
                          <a onclick="descargarReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a>
                      </th>
                    </tr>
                  </thead>
                  <tfoot class="btn-primary">
                    <tr>
                      <th>N° Ticket</th>
                      <!-- <th>Codactu</th> -->
                      <th>F.Registro</th>
                      <th>F.UltMov</th>
                      <th>Tipo Averia</th>
                      <th>CodCli/Tel</th>
                      <th>Quiebre</th>
                      <th>Cliente</th>
                      <th>Motivo</th>
                      <th>Submotivo</th>
                      <th>Solución</th>
                      <th>Estado Registro</th>
                      <th>CodLiq</th>
                      <th>Area</th>
                      <th style="width: 100px !important;">
                                    <a onclick="descargarReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a>
                                </th>
                    </tr>
                </tfoot>
                <tbody id="tb_embajador">

                 </tbody>
              </table>
            </div>
          </div>
        </div>

</section><!-- /.content -->
@stop

@section('formulario')
  @include( 'admin.conformidad.form.conformidad_modal')
@stop

@push('scripts')
   {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
   {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
   {{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }} 
   {{ HTML::script('js/sweetalert/sweetalert.js') }}
   {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
   {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
   {{ HTML::script('js/psi.js') }}
   {{ HTML::script('js/utils.js') }}
   {{ HTML::script('js/sweetalert/sweetalert.js') }}


    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/conformidad/conformidad_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/conformidad/conformidad.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/conformidad/conformidad_modal.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')
