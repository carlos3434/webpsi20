<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
<?php /*
  {{ HTML::script('lib/datepicker/js/bootstrap-datepicker.js') }}
 */ ?>
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/datepicker/css/datepicker.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
{{ HTML::style('css/admin/proyecto_edificio.css') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.gestion.js.proyecto_edificio_ajax' )
@include( 'admin.gestion.js.proyecto_edificio' )
@include( 'admin.gestion.js.proyecto_edificio_gestion' )

@include( 'admin.gestion.js.proyecto_edificio_registro' )
<?php
/*
@include( 'admin.gestion.js.proceso_disenio_ajax' )
@include( 'admin.gestion.js.proceso_disenio' 
 */
?>
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Bandeja de Edificios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Gesti&oacute;n</a></li>
        <li class="active">Bandeja de Edificios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php /*
      <div class="row">
      <div class="col-xs-12">
      <div class="box">
      <div class="box-body table-responsive" style="min-height:250px">
      <table id="t_proyecto_gestion" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
      <thead>
      <tr>
      <th>Item</th>
      <th>Cod Proyecto</th>
      <th>Fuente</th>
      <th>Segmento</th>
      <th>Tipo Proyecto</th>
      <th>Tipo Gestion</th>
      <th>Casuistica</th>
      <th>Estado</th>
      <th>Fecha Registro</th>
      <th style="width: 100px !important;">
      <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
      <input type="checkbox" title="filtrar" name="descarga_checkFiltro" id="descarga_checkFiltro">
      </th>
      </tr>
      </thead>
      </table>
      </div>
      </div>
      </div>
      </div>
     */ ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="box-title">{{trans('gestion.filtros')}}</div>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form name="form_gestion_filtro" id="form_gestion_filtro" method="POST" action="" class="form-horizontal">
                        <input type="hidden" name="tipo_accion" value="busqueda" />
                        <input type="hidden" name="bandeja" id="bandeja" value="1">
                        <input type="hidden" name="txt_buscarPor" id="txt_buscarPor">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="slct_tipo">Buscar por</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button" aria-expanded="false">
                                                    <i>.::Seleccione::.</i>
                                                    <span class="fa fa-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu listaABuscar">
                                                    <li><a href="#" itemid="id">ITEM</a></li>
                                                    <li><a href="#" itemid="ticket">TICKET</a></li>
                                                    <li><a href="#" itemid="direccion">DIRECCION</a></li>
                                                    <li><a href="#" itemid="nombre_proyecto">NOMBRE PROYECTO</a></li>
                                                    <li><a href="#" itemid="mes">MES</a></li>
                                                </ul>
                                            </div>
                                            <!-- /btn-group -->
                                            <input type="text" class="form-control" name="txt_buscar" id="txt_buscar">
                                        </div>
                                    </div>
                                </div>
                                <!--                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label" for="slct_tipo">Buscar</label>
                                                                    <div class="col-sm-9">
                                                                        <select class="form-control" name="slct_tipo" id="slct_tipo">
                                                                            <option value="">.::Seleccione::.</option>
                                                                            <option value="id">Item</option>
                                                                            <option value="ot">OT</option>
                                                                            <option value="direccion">Dirección</option>
                                                                        </select>
                                                                    </div>
                                                                </div>-->
                            </div>
                            <div class="col-sm-1" style="right: 6.33333%;">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="txt_direccion_n" id="txt_direccion_n" style="display: none;" placeholder="{{trans('gestion.numero_manzana_lote')}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="fecha_created_at">{{trans('gestion.fecha_termino')}}</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                            <input class="form-control" id="fecha_created_at" name="fecha_created_at" type="text" readonly aria-describedby="basic-addon1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="slct_segmento">Segmento</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="slct_segmento[]" id="slct_segmento" multiple>
                                            <option value="">.::Seleccione::.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="slct_tipoproyecto">Tipo Proyecto</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="slct_tipoproyecto[]" id="slct_tipoproyecto" multiple>
                                            <option value="">.::Seleccione::.</option>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="slct_estadoproyecto">Estado</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="slct_estadoproyecto[]" id="slct_estadoproyecto" multiple>
                                            <option value="">.::Seleccione::.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-search fa-lg"></i></button>
                                <!--                                <a class="btn btn-primary btn-sm" id="btn_filtro">
                                                                    <i class="fa fa-search fa-lg"></i>
                                                                </a>-->
                                <a class='btn btn-primary pull-right agregarG' data-toggle="modal" data-target="#proyectoEdificioMantenimientoModal" data-accion="REGISTRAR">
                                    <i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo
                                </a>
                                <a style="margin-right:10px;" class='btn btn-primary pull-right eliminarG' data-toggle="modal" data-target="#proyectoEdificioPapeleraModal" data-accion="PAPELERA">
                                    <i class="fa fa-eraser fa-lg"></i>&nbsp;Listado Eliminados
                                </a> 
                                <input type="hidden" id="descarga_textFiltro" name="descarga_textFiltro" value="0">
                            </div>
                        </div>


                    </form>

                    <?php /*
                      <form name="form_Personalizado" id="form_Personalizado" method="POST" action="" class="form-inline">
                      <input type="hidden" name="bandeja" id="bandeja" value="1">
                      <div class="row">
                      <div class="col-sm-11 col-sm-offset-1">
                      <div class="form-group">
                      <label class="col-sm-5 control-label" for="slct_tipo">Buscar por</label>
                      <div class="col-sm-7">
                      <select class="form-control" name="slct_tipo" id="slct_tipo">
                      <option value="">.::Seleccione::.</option>
                      <option value="id">Item</option>
                      <option value="direccion">Dirección</option>
                      </select>
                      </div>
                      </div>
                      <div class="form-group">
                      <div class="col-sm-12">
                      <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                      </div>
                      </div>
                      <a class="btn btn-primary btn-sm" id="btn_personalizado">
                      <i class="fa fa-search fa-lg"></i>
                      </a>
                      <!--                            <div class="form-group">
                      <div class="col-sm-6">
                      <div class="col-sm-3" style="text-align:left">
                      <label>Buscar por:</label>
                      <input type="hidden" name="bandeja" id="bandeja" value="1">
                      </div>
                      <div class="col-sm-4">
                      <select class="form-control" name="slct_tipo" id="slct_tipo">
                      <option value="">.::Seleccione::.</option>
                      <option value="id">Item</option>
                      <option value="direccion">Dirección</option>
                      </select>
                      </div>
                      <div class="col-sm-4">
                      <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                      </div>
                      <div class="col-sm-1">
                      <a class="btn btn-primary btn-sm" id="btn_personalizado">
                      <i class="fa fa-search fa-lg"></i>
                      </a>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      </div>
                      </div>-->
                      </div>
                      </div>
                      </form>

                      <form name="form_General" id="form_General" method="POST" action="reporte/proyectoexcel">
                      <div class="row">
                      <div class="col-sm-3 col-sm-offset-1">
                      <div class="form-group">
                      <label>Segmento</label>
                      <select class="form-control" name="slct_segmento[]" id="slct_segmento" multiple>
                      <option value="">.::Seleccione::.</option>
                      </select>
                      </div>
                      </div>
                      <div class="col-sm-3">
                      <div class="form-group">
                      <label>Tipo Proyecto</label>
                      <select class="form-control" name="slct_tipoproyecto[]" id="slct_tipoproyecto" multiple>
                      <option value="">.::Seleccione::.</option>
                      </select>
                      </div>
                      </div>
                      <div class="col-sm-4">
                      <div class="form-group">
                      <label for="fecha_created_at">{{trans('gestion.rango_fecha')}}</label>
                      <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                      <input class="form-control" id="fecha_created_at" name="fecha_created_at" type="text" readonly aria-describedby="basic-addon1">
                      </div>
                      </div>
                      </div>
                      <div class="col-sm-1">
                      <br>
                      <a class="btn btn-primary btn-sm" id="btn_filtro">
                      <i class="fa fa-search fa-lg"></i>
                      </a>
                      <input type="hidden" id="descarga_textFiltro" name="descarga_textFiltro" value="0">
                      </div>
                      </div>
                      </form>
                     */
                    ?>                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="datatable_proyectoedificio" class="table table-bordered table-striped table-condensed display responsive no-wrap" width="100%">
                <thead class="text-uppercase">
                    <tr>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.ticket')}}</th>
                        <th class="text-center">{{trans('gestion.nombre_proyecto')}}</th>
                        <th class="text-center">{{trans('gestion.segmento')}}</th>
                        <th class="text-center">{{trans('gestion.tipo_proyecto')}}</th>
                        <th class="text-center">{{trans('gestion.distrito')}}</th>
                        <th class="text-center">{{trans('gestion.direccion_numero_manzana_lote')}}</th>
                        <th class="text-center">{{trans('gestion.numero_departamentos')}}</th>
                        <th class="text-center">{{trans('gestion.avance')}}</th>
                        <th class="text-center">{{trans('gestion.fecha_termino')}}</th>
                        <th class="text-center">{{trans('gestion.mes')}}</th>
                        <th class="text-center">{{trans('gestion.estado')}}</th>
                        <th class="text-center">{{trans('gestion.semaf')}}</th>
                        <th class="text-center">{{trans('gestion.table_accion')}}</th>
                    </tr>
                </thead>
                <tfoot class="text-uppercase">
                    <tr>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.item')}}</th>
                        <th class="text-center">{{trans('gestion.ticket')}}</th>
                        <th class="text-center">{{trans('gestion.nombre_proyecto')}}</th>
                        <th class="text-center">{{trans('gestion.segmento')}}</th>
                        <th class="text-center">{{trans('gestion.tipo_proyecto')}}</th>
                        <th class="text-center">{{trans('gestion.distrito')}}</th>
                        <th class="text-center">{{trans('gestion.direccion_numero_manzana_lote')}}</th>
                        <th class="text-center">{{trans('gestion.numero_departamentos')}}</th>
                        <th class="text-center">{{trans('gestion.avance')}}</th>
                        <th class="text-center">{{trans('gestion.fecha_termino')}}</th>
                        <th class="text-center">{{trans('gestion.mes')}}</th>
                        <th class="text-center">{{trans('gestion.estado')}}</th>
                        <th class="text-center">{{trans('gestion.semaf')}}</th>
                        <th class="text-center">{{trans('gestion.table_accion')}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')
@include( 'admin.gestion.form.proyectoEdificio' )
@include( 'admin.gestion.form.proyectoEdificioMantenimiento' )
@include( 'admin.gestion.form.proyectoedificioeliminar' )
@include( 'admin.gestion.form.proyectoedificiopapelera' )
<?php
//@include( 'admin.gestion.form.procesodisenio' )
//print_r($menu);
?>
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

{{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&sensor=false&libraries=places') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/psigeo.js') }}
{{ HTML::script('js/geo/geo.functions.js') }}
@stop
