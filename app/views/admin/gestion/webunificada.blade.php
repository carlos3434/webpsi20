<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}

{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}

{{ HTML::script('js/psi.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.gestion.js.web_unificada_ajax' )
@include( 'admin.gestion.js.web_unificada' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Web Unificada
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Gesti&oacute;n Subida</a></li>
        <li class="active">Web Unificada</li>
    </ol>
</section>
<?php /* @include( 'admin.gestion.js.proyecto_edificio_ajax' ) */ ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(# 0) | Actualización desde Web Unificada</h3>
                </div>
                <div class="panel-body">
                    <form id="form_mensaje" name="form_mensaje"  role="form" class="form-inline" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="file" class="form-control" id="archivo" name="archivo" accept="text/plain">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 hidden">
            <div class="box box-solid bg-teal-gradient">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">(# 1) | Subida/ Actualización desde EXCEL de Trabajo</h3>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn bg-teal btn-sm" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <form id="form_parcial_uno" name="form_parcial_uno"  role="form" class="form-inline" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="file" class="form-control" id="archivouno" name="archivouno" accept="text/plain">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer no-border">
                    <input type="button" value="(# 1.2) | Actualizar edificios" class="btn btn-info btn-lg" id="btnSubidaParteDos">
                    <input type="button" value="(# 1.3) | Crear gestiones" class="btn btn-warning btn-lg" id="btnSubidaParteTres">
                    <select class="form-control inline" id="slct_SubidaParteTres">
                    </select>
                    <input type="checkbox" id="chkSubidaParteTres" >
                    <label for="chkSubidaParteTres" style="color: #000;"> 
                        (#3) Eliminar registro anteriores en: Gestiones
                    </label>
                    <br><br>
                    <input type="button" value="(# 1.4) | Bitacora" class="btn btn-success btn-lg" id="btnSubidaParteBitacora">
                    <input type="button" value="(# 1.5) | Megaproyecto" class="btn btn-success btn-lg" id="btnSubidaParteMegaproyecto">
                    <input type="button" value="(# 1.6) | Provisional" class="btn btn-success btn-lg" id="btnSubidaParteProvisional">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid bg-purple-gradient">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">(# 2) | ACTUALIZACION POR COLUMNA</h3>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn bg-purple btn-sm" type="button">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body border-radius-none">

                    <form id="form_parcial_columna" name="form_parcial_columna"  role="form" class="form-inline" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="file" class="form-control" id="archivocolumna" name="archivocolumna" accept="text/plain">
                            </div>
                        </div>
                    </form>
                    <small style="color: #000;">(*) Subir archivos .CSV, espacio TABULACION (\t) separado por COMILLAS ("")</small><br>
                    <br>
                    <div class="box">
                        <div class="box-header with-border">
                          <h3 class="box-title">Puede usar uno o más campos <em>(ITEM es obligatorio y debe ser el primer campo) </em></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="color: #000; overflow-x: scroll;">
                            <table class="table table-bordered table-condensed">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">ITEM</th>
                                        <th>FECHA_REGISTRO</th>
                                        <th>FECHA_ACTUALIZA</th>
                                        <th>ESTADO</th>
                                        <th>BANDEJA</th>
                                        <th>FOTOS_1|2|3</th>
                                        <th>COORD_Y</th>
                                        <th>COORD_X</th>
                                        <th>DEPARTAMENTO</th>
                                        <th>PROVINCIA</th>

                                        <th>DISTRITO</th>
                                        <th>SEGMENTO</th>
                                        <th>TIPO_PROYECTO</th>
                                        <th>TIPO_URB_CCHH</th>
                                        <th>NOMBRE_URB_CCHH</th>
                                        <th>TIPO_VIA</th>
                                        <th>MANZANA</th>
                                        <th>LOTE</th>
                                        <th>DIRECCION</th>
                                        <th>NUMERO</th>

                                        <th>NOMBRE_PROYECTO</th>
                                        <th>BLOCKS</th>
                                        <th>PISOS</th>
                                        <th>DPTOS</th>
                                        <th>DPTOS_HAB</th>
                                        <th>AVANCE</th>
                                        <th>SEGUIMIENTO</th>
                                        <th>FECHA_TERMINO</th>
                                        <th>TIPO_INFRAESTRUCTURA</th>
                                        <th>OBSERVACION</th>

                                        <th>NOMBRE_CONSTRUCTORA</th>
                                        <th>RUC_CONSTRUCTORA</th>
                                        <th>CONTACTOS</th>
                                        <th>ARMARIO_GIS</th>
                                        <th>ZONA_COMPETENCIA</th>
                                        <th>MDF_GIS</th>
                                        <th>URA_GIS</th>
                                        <th>TROBA_GIS</th>
                                        <th>TROBA_BIDIRECCIONAL</th>
                                        <th>ZONA_GIS</th>

                                        <th>EECC_GIS</th>
                                        <th>USUARIO_REGISTRO</th>
                                        <th>USUARIO_ACTUALIZA</th>
                                        <th>FUENTE_IDENTIFICACION</th>
                                        <th>MES</th>
                                        <th>TICKET</th>
                                    </tr> 
                                    <tr class="text-center">
                                        @for($x = 1; $x <= 46; $x++) 
                                            <td>{{ $x }}</td>
                                        @endfor
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="box-footer no-border">
                    <input type="button" value="(# 2.1) | Actualizar Columnas" class="btn btn-info btn-lg" id="btnActualizacionParteDos">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body webunificada_msn">
                    <?php
                    echo '<pre>';
//                    $b = new OfscRest\Activities();
//                    $data = $b->getActivity(48450);
//                    if (is_object($data)) {
//                        echo 'es objeto......';
//                    }
//                    echo gettype(intval($data->status)) . ' ' . intval($data->status) . ', c:' . count(($data)) . '<br><br>';
//                    if (is_object($data) && isset($data->status) && $data->status != '' &&  intval($data->status) > 200  ) {
//                       echo '.....entro condicional....' ;
//                    }
                    //print_r($data);
                    //print_r((new OfscRest\Users())->getUser('adminit'));
                    // print_r((new OfscRest\Rest())->test());
                    // print_r((new OfscRest\Activities())->getActivity(4845));  

                    // print_r((new OfscRest\Activities())->getActivity(4845));

                   // $capacity1 = \CapacityHelper::getCapacityOnly(['2016-06-14','2016-06-15'], 'PM', 'PROV_INS_M1', 'LL_R007');
                    
                    // $capacity2 = \CapacityHelper::getCapacityOnly(['2016-06-14','2016-06-15'], 'AM', 'PROV_INS_M1', 'LL_R007');
//                                    var_dump($capacity->error);
//                    var_dump($capacity1);
//                    var_dump($capacity2);
//                                    var_dump($capacity->data->capacity[0]->location);
//                                    print_r($capacity->data->capacity[0]->location);
//                    $actividad = new OfscRest\Activities();
//                    $actividad->language = 'es';
//                    $actividad->timeZone = "Peru";
//                    $actividad->
                    echo '</pre>';
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop