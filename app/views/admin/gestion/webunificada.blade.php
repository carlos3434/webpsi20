<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}

{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}

{{ HTML::script('js/psi.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.gestion.js.web_unificada_ajax' )
@include( 'admin.gestion.js.web_unificada' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Web Unificada
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Gesti&oacute;n Subida</a></li>
        <li class="active">Web Unificada</li>
    </ol>
</section>
<?php /* @include( 'admin.gestion.js.proyecto_edificio_ajax' ) */ ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(#1) | Actualización desde Web Unificada</h3>
                </div>
                <div class="panel-body">
                    <form id="form_mensaje" name="form_mensaje"  role="form" class="form-inline" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="file" class="form-control" id="archivo" name="archivo" accept="text/plain">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid bg-teal-gradient">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">(#1) | Subida/ Actualización desde EXCEL de Trabajo</h3>
                    <div class="box-tools pull-right">
                        <button data-widget="collapse" class="btn bg-teal btn-sm" type="button"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    <form id="form_parcial_uno" name="form_parcial_uno"  role="form" class="form-inline" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="file" class="form-control" id="archivouno" name="archivouno" accept="text/plain">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-footer no-border">
                    <input type="button" value="(#2) | Actualizar edificios" class="btn btn-info btn-lg" id="btnSubidaParteDos">
                    <input type="button" value="(#3) | Crear gestiones" class="btn btn-warning btn-lg" id="btnSubidaParteTres">
                    <select class="form-control inline" id="slct_SubidaParteTres">
                    </select>
                    <input type="checkbox" id="chkSubidaParteTres" >
                    <label for="chkSubidaParteTres" style="color: #000;"> 
                        (#3) Eliminar registro anteriores en: Gestiones
                    </label>
                    <br><br>
                    <input type="button" value="(#4) | Bitacora" class="btn btn-success btn-lg" id="btnSubidaParteBitacora">
                    <input type="button" value="(#5) | Megaproyecto" class="btn btn-success btn-lg" id="btnSubidaParteMegaproyecto">
                    <input type="button" value="(#6) | Provisional" class="btn btn-success btn-lg" id="btnSubidaParteProvisional">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body webunificada_msn">
                    <?php
                    echo '<pre>';
//                    $b = new OfscRest\Activities();
//                    $data = $b->getActivity(48450);
//                    if (is_object($data)) {
//                        echo 'es objeto......';
//                    }
//                    echo gettype(intval($data->status)) . ' ' . intval($data->status) . ', c:' . count(($data)) . '<br><br>';
//                    if (is_object($data) && isset($data->status) && $data->status != '' &&  intval($data->status) > 200  ) {
//                       echo '.....entro condicional....' ;
//                    }
                    //print_r($data);
                    //print_r((new OfscRest\Users())->getUser('adminit'));
                    print_r((new OfscRest\Rest())->test());
                    // print_r((new OfscRest\Activities())->getActivity(4845));  

                    print_r((new OfscRest\Activities())->getActivity(4845));

                   // $capacity1 = \CapacityHelper::getCapacityOnly(['2016-06-14','2016-06-15'], 'PM', 'PROV_INS_M1', 'LL_R007');
                    
                    // $capacity2 = \CapacityHelper::getCapacityOnly(['2016-06-14','2016-06-15'], 'AM', 'PROV_INS_M1', 'LL_R007');
//                                    var_dump($capacity->error);
//                    var_dump($capacity1);
//                     
//                    var_dump($capacity2);
//                                    var_dump($capacity->data->capacity[0]->location);
//                                    print_r($capacity->data->capacity[0]->location);
//                    $actividad = new OfscRest\Activities();
//                    $actividad->language = 'es';
//                    $actividad->timeZone = "Peru";
//                    $actividad->
                    echo '</pre>';
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop