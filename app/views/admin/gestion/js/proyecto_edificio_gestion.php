<script type="text/javascript">
    //var relacion_tipo_estado = 0; // 0 no permite GUARDAR
    $(window).load(function () {
        $('#chk_retornar').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-red',
            increaseArea: '20%' // optional
        });
        $('#chk_retornar').iCheck('uncheck');
        $('#chk_retornar').on('ifUnchecked', function (event) {
            chk_retornar = 0;
        });
        $('#chk_retornar').on('ifChecked', function (event) {
            chk_retornar = 1;
        });
        $('#txt_estado_general_si').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '25%'
        }).iCheck('uncheck');
        $('#txt_estado_general_si').on('ifChecked', function (event) {
            $('#form_proyecto_validacionreversa #txt_fecha_termino').val(moment().format("YYYY-MM-DD"));
        });

        $("#boton_mapa_mostrar").click(function () {
            $("#map").toggle("slow", function () {
                // Animation complete.
            });
        });

        $("#boton_calle_mostrar").click(function () {
            $("#pano").toggle("slow", function () {
                // Animation complete.
            });
        });
    });
    // formPorEstado esta en: js/proyecto_edificio_ajax.php
    validarGestionPorEstado = function (ultimo) {
        if (ultimo === null || ultimo === undefined) {
            return false;
        }
        // no se uso mas que inicializar un FORM
        var formPorTipo = {
            1: '#form_proyecto_diseno',
            2: '#form_proyecto_licencia',
            3: '#form_proyecto_oocc',
            4: '#form_proyecto_cableado',
            6: '#form_proyecto_iniciativa',
            7: '#form_proyecto_validacionreversa'
        };
//        var formTipo = formPorTipo[1];
//        if ((ultimo.fecha_termino).length == 0 || ultimo.fecha_termino == null) {
//            // si no existe fecha final, restaura los datos
//            formTipo = formPorTipo[ultimo.avance] == undefined ? '' : formPorTipo[ultimo.avance];
//        } else {
//            // si existe fecha_final, fecha_termino es fecha_inicio proximo estado
//            formTipo = formPorEstado[ultimo.avance] == undefined ? '' : formPorEstado[ultimo.avance];
//        }
//        formTipo = $(formTipo);
//        formTipo.find('#txt_fecha_inicio').val(ultimo.fecha_inicio);
//        formTipo.find('#txt_fecha_seguimiento').val(ultimo.fecha_seguimiento);
//        formTipo.find('#txt_fecha_compromiso').val(ultimo.fecha_compromiso);
//        formTipo.find('#txt_fecha_gestion').val(ultimo.fecha_gestion);
//        formTipo.find('#txt_estado_general').val(ultimo.estado_general);
//        formTipo.find('#txt_gestion_interna').val(ultimo.gestion_interna);
//        formTipo.find('#txt_recepcion_expediente').val(ultimo.recepcion_expediente);
//        formTipo.find('#txt_fecha_teorica_final').val(ultimo.fecha_teorica_final);
//        if ((ultimo.fecha_termino).length > 0 && ultimo.fecha_termino != null && ultimo.fecha_termino != '0000-00-00') {
//            formTipo.find('#txt_fecha_inicio').val(ultimo.fecha_termino);
//        }
//        $('#txt_proyecto_estado_id').val(ultimo.avance);/* proyecto_estado_id */
//        if (formPorEstado[ultimo.avance] !== null) {
//            $(formPorEstado[ultimo.avance]).show();
//            $('#slct_gcasuistica_modal').multiselect('select', ultimo.proyecto_tipo_id);
//            // Disenio
//            if (ultimo.proyecto_tipo_id == 1) {
//                $('#btn_disenio_modal').css('display', 'inline-block');
//            }
//        }
        var last = 0;
        var last_index = 0;
        $.each(ultimo, function (index, text) {
            if (last < text.id) {
                last = text.id;
                last_index = index;
            }
            var form = $(formPorTipo[index]);
            if (form.length) {
                $.each(text, function (i, dato) {
                    if (dato != '' || dato != null || dato != "0000-00-00") {
                        form.find('#txt_' + i).val(dato);
                    }
                });
            }
        });

        var u = ultimo[last_index];
        if (u !== undefined && formPorEstado[u.avance] !== null) {
            $('#txt_proyecto_estado_id').val(u.avance);/* proyecto_estado_id */
            //console.log(u.proyecto_tipo_id);
            // Disenio
            mostrarBotonDisenio(u.proyecto_tipo_id);
            // Si existe fecha_final, fecha_termino es fecha_inicio proximo estado
            // Mostrar el FORM correspondiente
            if ((u.fecha_termino).length > 0 && u.fecha_termino !== "0000-00-00") {
                $(formPorEstado[u.avance]).show();
                $(formPorEstado[u.avance]).find('#txt_fecha_inicio').val(u.fecha_termino);
                $('#slct_gcasuistica_modal').multiselect('select', $(formPorEstado[u.avance]).attr('itemid'));
                tipo_actual = $(formPorEstado[u.avance]).attr('itemid');
            } else {
                $('#slct_gcasuistica_modal').multiselect('select', u.proyecto_tipo_id);
                tipo_actual = u.proyecto_tipo_id;
                $(formPorEstado[u.proyecto_estado_id]).show();
            }
        }
    };

    /**
     * Debe coincidir el tipo con el estado para poder GESTIONAR
     * tipo_actual --> $('#slct_gcasuistica_modal').val()
     * @returns {Boolean}
     */
    validarTipoConEstado = function () {
        if (tipo_actual === 0) {
            Psi.mensaje('info', 'Debe seleccionar un TIPO, si ya selecciono, vuelva hacerlo.', 8000);
            return false;
        }
        estado = $('#txt_proyecto_estado_id').val();
        if (tipo_actual > 0 && estado == '') {
            Psi.mensaje('info', 'No existe un ESTADO, para comenzar debe elegir TIPO: Iniciativa.', 8000);
            return false;
        } else if (estado == '') {
            Psi.mensaje('warning', 'No tiene un ESTADO, iniciara con el ESTADO: Iniciativa.', 8000);
            //return true;
        }

        var tipo_con_estado = {
            6: 4,
            1: 3,
            2: 5,
            3: 6,
            4: 7,
            7: 2
        };
        if (tipo_con_estado[tipo_actual] != null && tipo_con_estado[tipo_actual] == estado) {
            //relacion_tipo_estado = 0;
            return true;
        } else {
            Psi.mensaje('info', 'Esta intentando GESTIONAR con un ESTADO no coincide con el estado ACTUAL, intentar con otro TIPO', 8000);
            //relacion_tipo_estado = 1;
            return false;
        }

    };
</script>