<script type="text/javascript">
    $(document).ready(function () {

        $('#chkSubidaParteTres').iCheck('check');

        $('#btnSubidaParteDos').on('click', function (event) {
            r = confirm('¿Desea continuar "Actualizar edificios"? "Aceptar" para continuar.');
            if (r) {
                //WebUnificada.procesarDos();
                WebUnificada.procesarPasos('archivo-dos', '');
            }
        });

        $('#btnSubidaParteTres').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear gestiones"? "Aceptar" para continuar.');
            //console.log(($(".icheckbox_minimal").hasClass("checked")? 1: 0));
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                estado = 'estado=' + $('#slct_SubidaParteTres').val();
                //WebUnificada.procesarTres(estado, estadoEliminar);
                WebUnificada.procesarPasos('archivo-tres', estado + estadoEliminar);
            }
        });

        $('#btnSubidaParteBitacora').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Bitacora"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                //WebUnificada.procesarBitacora(estadoEliminar);
                WebUnificada.procesarPasos('archivo-bitacora', estadoEliminar);
            }
        });

        $('#btnSubidaParteMegaproyecto').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Megaproyecto"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                //WebUnificada.procesarMegaproyecto(estadoEliminar);
                WebUnificada.procesarPasos('archivo-megaproyecto', estadoEliminar);
            }
        });

        $('#btnSubidaParteProvisional').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Provisional"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                //WebUnificada.procesarProvisional(estadoEliminar);
                WebUnificada.procesarPasos('archivo-provisional', estadoEliminar);
            }
        });

        $('#btnActualizacionParteDos').on('click', function (event) {
            r = confirm('¿Desea continuar "Actualizar por Columnas"? "Aceptar" para continuar.');
            if (r) {
                WebUnificada.procesarPasos('archivo-columna', '');
            }
        });

        $("#archivo,#archivouno,#archivocolumna").fileinput({
            showUpload: false,
            allowedFileExtensions: ["txt", "csv"],
            previewClass: "bg-warning",
            showCaption: true,
            showPreview: false,
            showRemove: false,
            browseLabel: 'Examinar'
        });
        $('#archivo').on('change', prepareUpload);
        $('#archivouno').on('change', prepareUploadUno);
        $('#archivocolumna').on('change', prepareUploadColumna);

        WebUnificada.proyectoEstado();

    });

    prepareUpload = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivo', value);
            WebUnificada.subirArchivo(data, '');
        });
    };
    prepareUploadUno = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivouno', value);
            WebUnificada.subirArchivo(data, '1');
        });
    };
    prepareUploadColumna = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivocolumna', value);
            WebUnificada.subirArchivo(data, 'columna');
        });
    };
    archivoSuccess = function (obj) {
        alert = 'success';
        eventoCargaRemover();
        if (obj.rst == 1) {
            alert = 'success';
        } else {
            alert = 'danger';
        }
        $('.webunificada_msn').html('<div class="alert alert-' + alert + '" role="alert">' + obj.msj + '</div>');
        Psi.mensaje(alert, obj.msj, 6000);
    };
</script>
