<script type="text/javascript">
    $(document).ready(function () {

        $('#chkSubidaParteTres').iCheck('check');

        $('#btnSubidaParteDos').on('click', function (event) {
            r = confirm('¿Desea continuar "Actualizar edificios"? "Aceptar" para continuar.');
            if (r) {
                WebUnificada.procesarDos();
            }
        });

        $('#btnSubidaParteTres').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear gestiones"? "Aceptar" para continuar.');
            //console.log(($(".icheckbox_minimal").hasClass("checked")? 1: 0));
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }

            if (r) {
                estado = 'estado=' + $('#slct_SubidaParteTres').val();
                WebUnificada.procesarTres(estado, estadoEliminar);
            }
        });

        $('#btnSubidaParteBitacora').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Bitacora"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                WebUnificada.procesarBitacora(estadoEliminar);
            }
        });

        $('#btnSubidaParteMegaproyecto').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Megaproyecto"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                WebUnificada.procesarMegaproyecto(estadoEliminar);
            }
        });

        $('#btnSubidaParteProvisional').on('click', function (event) {
            r = confirm('¿Desea continuar "Crear Provisional"? "Aceptar" para continuar.');
            estadoEliminar = '';
            if ($(".icheckbox_minimal").hasClass("checked")) {
                estadoEliminar = '&eliminar=1';
            }
            if (r) {
                WebUnificada.procesarProvisional(estadoEliminar);
            }
        });

        $("#archivo,#archivouno").fileinput({
            showUpload: false,
            allowedFileExtensions: ["txt", "csv"],
            previewClass: "bg-warning",
            showCaption: true,
            showPreview: false,
            showRemove: false,
            browseLabel: 'Examinar'
                    //uploadAsync: true, //ajax
                    //uploadUrl: "/reporte/movimiento"
        });

        $('#archivo').on('change', prepareUpload);
        $('#archivouno').on('change', prepareUploadUno);

        WebUnificada.proyectoEstado();
    });

    prepareUpload = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivo', value);
            WebUnificada.subirArchivo(data);
        });
    };
    prepareUploadUno = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivouno', value);
            WebUnificada.subirArchivoUno(data);
        });
    };

    archivoSuccess = function (obj) {
        $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
        eventoCargaRemover();
        if (obj.rst == 1) {
            Psi.mensaje('success', obj.msj, 6000);
        } else {
            Psi.mensaje('danger', obj.msj, 6000);
        }
    };
</script>
