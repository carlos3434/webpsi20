<script type="text/javascript">
    $(document).ready(function () {

        $('#form_proceso_disenio').on('submit', function (e) {
            ProcesoDisenio.crearProceso();
            return false;
        });

        $('#btn_proceso_disenio_modal').click(function (e) {
            dataEdificioId = $('#proyectoEdificioModal #txt_proyecto_edificio_id').val();
            $('#proyectoProcesoDisenioModal').modal('show');
        });

        $('#btn_disenio_volver_modal').click(function (e) {
            dataEdificio['id'] = dataEdificioId;
            $('#proyectoEdificioModal').modal('show');
        });

        $('#proyectoProcesoDisenioModal').on('shown.bs.modal', function (e) {
            ProcesoDisenio.cargarProceso();
        });

        $('#proyectoProcesoDisenioModal').on('show.bs.modal', function (e) {
            $('#disenioTabs a:first').tab('show');
            $('#PD0').addClass('active');
            $('#proyecto_edificio_id').val(dataEdificioId);
        });

    });

//    $(window).load(function () {
//        // El proceso de diseño esta sin funcionalidad.
//        ProcesoDisenio.cargarPreguntas();
//    });

    cargarHtmlPorPestania = function (datos) {
        var fechas = [];
        if (datos !== undefined && datos.length > 0) {
            $.each(datos, function (i, data) {
                //console.log(data.grupo + ' --> ' + i + data.seccion + ' <-- ');
                var html = '';
                var grupo_seccionpadre_anterior = (i - 1 < 0) ? '' : datos[i - 1].igrupo + datos[i - 1].iseccionpadre;
                var grupo_anterior = (i - 1 < 0) ? '' : datos[i - 1].igrupo;
                var grupo_seccionpadre = data.igrupo + data.iseccionpadre;
                var ids = (data.ids).split('||');
                var preguntas = (data.preguntas).split('||');
                var caracteristica = (data.caracteristica).split('||');
                var tab = $('#PD' + ((data.igrupo == 12 || data.igrupo == 15) ? 0 : data.igrupo));
                // TITULO-PADRE
                if (grupo_anterior != data.igrupo) {
                    html += '<h3 class="clearfix">' + data.grupo + '</h3><hr>';
                }
                if (data.iseccionhijo === null || $('#b' + grupo_seccionpadre).length == 0) {
                    html += '<div class="col-xs-6">';
                }
                html += '<div class="box box-default">';
                // TITULO-SECCION-PADRE
                if (grupo_seccionpadre_anterior !== grupo_seccionpadre) {
                    html += '<div class="box-header with-border">';
                    html += '<h5 class="box-title" style="font-size:17px;"><em>' + data.seccion + '</em></h5>';
                    html += '<div class="box-tools pull-right"><button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button></div>';
                    html += '</div>';
                }
                // TITULO-SECCION-HIJO
                if (data.seccion_hijo !== null) {
                    html += '<h5 style="text-indent:2em;color: #aaa;">' + data.seccion_hijo + '</h5>';
                }
                // PREGUNTAS
                html += '<div class="box-body no-padding" id="b' + grupo_seccionpadre + '">';
                html += '<ul class="list-group">';
                $.each(preguntas, function (i, text) {
                    var input;
                    if (caracteristica[i] == 'CHAR0') {
                        input = '<input id="c' + ids[i] + '" name="' + ids[i] + '" class="pull-right" type="checkbox" value="1">';
                    } else if (caracteristica[i] == 'CHAR1') {
                        input = '<input id="cs' + ids[i] + '" name="' + ids[i] + '[]" class="pull-right" type="text" placeholder="+">';
                        input += '<input id="c' + ids[i] + '" name="' + ids[i] + '[]" class="pull-right" type="checkbox" value="1">';
                    } else if (caracteristica[i] == 'INT1') {
                        input = '<input id="cs' + ids[i] + '" name="' + ids[i] + '[]" class="pull-right w-50" type="text" placeholder="+">';
                        input += '<input id="c' + ids[i] + '" name="' + ids[i] + '[]" class="pull-right w-50" type="text" placeholder="0">';
                    } else if (caracteristica[i] == 'DATE0') {
                        fechas.push('#c' + ids[i]);
                        input = '<input id="c' + ids[i] + '" name="' + ids[i] + '" class="pull-right text-right  w-100" type="text" placeholder="YYYY-MM-DD">';
                    } else {
                        input = '<input id="c' + ids[i] + '" name="' + ids[i] + '" class="pull-right" type="checkbox" value="1">';
                    }
                    html += '<li class="list-group-item">' + text + input + '</li>';
                });
                html += '</ul>';
                html += '</div>';
                html += '</div>';
                if (data.iseccionhijo === null || $('#b' + grupo_seccionpadre).length == 0) {
                    html += '</div>';
                    tab.append(html);
                } else {
                    $('#b' + grupo_seccionpadre).append(html);
                }
                //$(html).appendTo('ul');
            });
            if (fechas.length > 1) {
                $(fechas.join()).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    format: 'YYYY-MM-DD'
                });
            }
            var chekNum = '#c103,#c104,#c105,#c106,#c107,#c108,#c109,#c110,#c111,#c112';
            $(chekNum).change(function () {
                var min = 103;
                if ($(this).is(":checked")) {
                    for (i = min; i < $(this).attr("name"); i++) {
                        $('#c' + i).prop("checked", true);
                    }
                } else {
                    $(chekNum).prop("checked", false);
                }
            });
        }
    };
</script>