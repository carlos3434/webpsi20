<script type="text/javascript">
    $(document).ready(function () {
        Mantenimiento.MostrarProyectoEstado();

        $('#proyectoestadoModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var recipient = button.data('accion');
            var modal = $(this);
            if (recipient === "registrar") {
                modal.find('.modal-title').text('Mantenimiento ' + recipient);
                $("#btn_accion_proyectoestado").attr('onclick', 'Mantenimiento.CrearProyectoEstado();');
            } else {
                modal.find('.modal-title').text('Mantenimiento ' + recipient);
                $("#btn_accion_proyectoestado").attr('onclick', 'Mantenimiento.ActualizarProyectoEstado();');
            }
        });
        $('#proyectoestadoModal').on('hidden.bs.modal', function (event) {
            $("#form_proyectoestado #txt_id").val('');
            $("#form_proyectoestado #txt_nombre").val('');
            $("#form_proyectoestado #slct_estado").val('1');
        });

        // realizando la actualizacion por cada cambio de TAB
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');

            if (($(this).attr('href') == "#tab_proyectoEstado")) {
                Mantenimiento.MostrarProyectoEstado();

                $('#proyectoestadoModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);
                    var recipient = button.data('accion');
                    var modal = $(this);
                    if (recipient === "registrar") {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        $("#btn_accion_proyectoestado").attr('onclick', 'Mantenimiento.CrearProyectoEstado();');
                    } else {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        $("#btn_accion_proyectoestado").attr('onclick', 'Mantenimiento.ActualizarProyectoEstado();');
                    }
                });

                $('#proyectoestadoModal').on('hidden.bs.modal', function (event) {
                    //$('#form_proyectoestado :input').val("");
                    $("#form_proyectoestado #txt_id").val('');
                    $("#form_proyectoestado #txt_nombre").val('');
                    $("#form_proyectoestado #slct_estado").val('1');
                });
            } else if (($(this).attr('href') == "#tab_proyectoTipo")) {
                Mantenimiento.MostrarProyectoTipo();

                $('#proyectotipoModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);
                    var recipient = button.data('accion');
                    var modal = $(this);
                    if (recipient === "registrar") {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyecto('nuevo', null);
                        $("#btn_accion_proyectotipo").attr('onclick', 'Mantenimiento.CrearProyectoTipo();');
                    } else {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyecto('nuevo', null);
                        $("#btn_accion_proyectotipo").attr('onclick', 'Mantenimiento.ActualizarProyectoTipo();');
                    }
                });

                $('#proyectotipoModal').on('hidden.bs.modal', function (event) {
                    $("#form_proyectotipo #txt_id").val('');
                    $("#form_proyectotipo #slct_proyecto_id").val('0');
                    $("#form_proyectotipo #txt_nombre").val('');
                    $("#form_proyectotipo #slct_estado").val('1');
                });
            } else if (($(this).attr('href') == "#tab_casuistica")) {
                Mantenimiento.MostrarCasuistica();

                $('#casuisticaModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);
                    var recipient = button.data('accion');
                    var modal = $(this);
                    if (recipient === "registrar") {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyectoTipo('nuevo', null);
                        $("#btn_accion_casuistica").attr('onclick', 'Mantenimiento.CrearCasuistica();');
                    } else {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyectoTipo('nuevo', null);
                        $("#btn_accion_casuistica").attr('onclick', 'Mantenimiento.ActualizarCasuistica();');
                    }
                });

                $('#casuisticaModal').on('hidden.bs.modal', function (event) {
                    $("#form_casuistica #txt_id").val('');
                    $("#form_casuistica #txt_nombre").val('');
                    $("#form_casuistica #slct_proyecto_tipo_id").val('0');
                    $("#form_casuistica #slct_estado").val('1');
                });
            } else {
                Mantenimiento.MostrarCasuisticaProyectoEstado();
                Mantenimiento.cargarProyectoEstado('nuevo', null);
                Mantenimiento.cargarCasuistica('nuevo', null);

                $('#casuisticaProyectoEstadoModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget);
                    var recipient = button.data('accion');
                    var modal = $(this);
                    if (recipient === "registrar") {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyectoEstado('nuevo', null);
                        Mantenimiento.cargarCasuistica('nuevo', null);
                        $("#btn_accion_casuisticaproyectoestado").attr('onclick', 'Mantenimiento.CrearCasuisticaProyectoEstado();');
                    } else {
                        modal.find('.modal-title').text('Mantenimiento ' + recipient);
                        Mantenimiento.cargarProyectoEstado('nuevo', null);
                        Mantenimiento.cargarCasuistica('nuevo', null);
                        $("#btn_accion_casuisticaproyectoestado").attr('onclick', 'Mantenimiento.ActualizarCasuisticaProyectoEstado();');
                    }
                });

                $('#casuisticaProyectoEstadoModal').on('hidden.bs.modal', function (event) {
                    $("#form_casuisticaproyectoestado #txt_id").val('');
                    $("#form_casuisticaproyectoestado #slct_proyecto_estado_id").val('0');
                    $("#form_casuisticaproyectoestado #slct_casuistica_id").val('0');
                    $("#form_casuisticaproyectoestado #slct_estado").val('1');
                });
            }
        });
    });
</script>