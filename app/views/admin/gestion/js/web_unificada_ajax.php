<script type="text/javascript">
    var route = 'proyecto_edificio/';
    var WebUnificada = {
        subirArchivo: function (data, url) {
            if (url == '1' || url == 'columna') {
                url = 'procesar-archivo-uno';
            } else {
                url = 'procesararchivo';
            }
            $.ajax({
                url: route + url,
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', 'ocurrio un error en la carga.', 6000);
                }
            });
        },
        procesarPasos: function (num, data) {
            if(num == 'archivo-dos') {
                url = 'procesar-archivo-dos';
            } else if (num == 'archivo-tres') {
                url = 'procesar-archivo-tres';
            } else if (num == 'archivo-bitacora') {
                url = 'procesar-archivo-bitacora';
            } else if (num == 'archivo-megaproyecto') {
                url = 'procesar-archivo-megaproyecto';
            } else if (num == 'archivo-provisional') {
                url = 'procesar-archivo-provisional';
            } else if (num == 'archivo-columna') {
                url = 'procesar-archivo-columna';
            } else {
                obj = '{"rst":"0","msj":"Parametro: ' + num + ', no existe."}';
                archivoSuccess(obj);
            }

            $.ajax({
                url: route + url,
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        proyectoEstado: function () {
            $.ajax({
                url: "proyectoestado/listar",
                type: "POST",
                cache: false,
                dataType: 'json',
                beforeSend: function (obj) {
                    $("#slct_SubidaParteTres").after("<i id='fa-slct_SubidaParteTres' class='fa fa-spinner fa-spin'></i>");
                },
                success: function (obj) {
                    $('#fa-slct_SubidaParteTres').remove();
                    if (obj.rst == 1) {
                        listSelect($('#slct_SubidaParteTres'), obj.datos, 'codigo', 'nombre');
                        /*
                        $('#slct_SubidaParteTres')
                        .append(new Option(' - MEGAPROYECTO', 20, false, false))
                        */
                    } else {
                        Psi.mensaje('danger', '', 6000);
                    }
                }
            });
        },
        /*
        procesarDos: function (data) {
            $.ajax({
                url: route + "procesar-archivo-dos",
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        procesarTres: function (estado, eliminar) {
            $.ajax({
                url: route + "procesar-archivo-tres",
                type: "POST",
                data: estado + eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        procesarBitacora: function (eliminar) {
            $.ajax({
                url: route + "procesar-archivo-bitacora",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        procesarMegaproyecto: function (eliminar) {
            $.ajax({
                url: route + "procesar-archivo-megaproyecto",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        procesarProvisional: function (eliminar) {
            $.ajax({
                url: route + "procesar-archivo-provisional",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        subirArchivoUno: function (data) {
            $.ajax({
                url: route + "procesar-archivo-uno",
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', 'ocurrio un error en la carga.', 6000);
                }
            });
        },
        */
    };
</script>
