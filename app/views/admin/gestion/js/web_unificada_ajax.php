<script type="text/javascript">
    var route = 'proyecto_edificio/';
    var WebUnificada = {
        subirArchivo: function (data) {
            $.ajax({
                url: route + "procesararchivo",
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', 'ocurrio un error en la carga.', 6000);
                }
            });
        },
        subirArchivoUno: function (data) {
            $.ajax({
                url: route + "procesar-archivo-uno",
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', 'ocurrio un error en la carga.', 6000);
                }
            });
        },
        procesarDos: function (data) {
            $.ajax({
                url: route + "procesar-archivo-dos",
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                }
            });
        },
        procesarTres: function (estado, eliminar) {
            //var datos = 'proyecto_edificio_id=' + proyecto_edificio_id;
            $.ajax({
                url: route + "procesar-archivo-tres",
                type: "POST",
                data: estado + eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                }
            });
        },
        procesarBitacora: function (eliminar) {
            //var datos = 'proyecto_edificio_id=' + proyecto_edificio_id;
            $.ajax({
                url: route + "procesar-archivo-bitacora",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
//                    $('.webunificada_msn').html('<div class="alert alert-info" role="alert">' + obj.msj + '</div>');
//                    eventoCargaRemover();
//                    if (obj.rst == 1) {
//                        Psi.mensaje('success', obj.msj, 6000);
//                    } else {
//                        Psi.mensaje('danger', obj.msj, 6000);
//                    }
                }
            });
        },
        proyectoEstado: function () {
            $.ajax({
                url: "proyectoestado/listar",
                type: "POST",
                cache: false,
                dataType: 'json',
                beforeSend: function (obj) {
                    $("#slct_SubidaParteTres").after("<i id='fa-slct_SubidaParteTres' class='fa fa-spinner fa-spin'></i>");
                },
                success: function (obj) {
                    $('#fa-slct_SubidaParteTres').remove();
                    if (obj.rst == 1) {
                        listSelect($('#slct_SubidaParteTres'), obj.datos, 'codigo', 'nombre');
//                        $('#slct_SubidaParteTres')
//                        .append(new Option(' - MEGAPROYECTO', 20, false, false))
//                        .append(new Option(' - PROVISIONAL', 30, false, false))
//                        .append(new Option(' - BITACORA', 40, false, false));
                    } else {
                        Psi.mensaje('danger', '', 6000);
                    }
                }
            });
        },
        procesarMegaproyecto: function (eliminar) {
            $.ajax({
                url: route + "procesar-archivo-megaproyecto",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
        procesarProvisional: function (eliminar) {
            $.ajax({
                url: route + "procesar-archivo-provisional",
                type: "POST",
                data: eliminar,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $('.webunificada_msn').html('');
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    archivoSuccess(obj);
                }
            });
        },
    };
</script>
