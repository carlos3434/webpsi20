<script>
    var Mantenimiento = {
        MostrarProyectoEstado: function () {
            $.ajax({
                url: "proyectoestado/listar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        var html = "";
                        var htmlEstado = "";
                        $.each(obj.datos, function (index, data) {
                            htmlEstado = '<span class="btn btn-success" onclick="Mantenimiento.EstadoProyectoEstado(0, ' + data.codigo + ')">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                            if (data.status == 0) {
                                htmlEstado = '<span class="btn btn-danger" onclick="Mantenimiento.EstadoProyectoEstado(1, ' + data.codigo + ')"> Inactivo</span>';
                            }
                            html += "<tr>";
                            html += "<td>" + data.nombre + "</td>";
                            html += "<td>" + htmlEstado + "</td>";
                            html += '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectoestadoModal" onclick="Mantenimiento.BuscarProyectoEstado(' + data.codigo + ')" data-accion="actualizar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                            html += "</tr>";
                        })

                        $("#t_proyectoEstados").dataTable().fnDestroy();
                        $("#tb_proyectoEstados").html(html);
                        $("#t_proyectoEstados").dataTable();
                        $(".overlay,.loading-img").remove();
                    } else {

                    }
                }
            });
        },
        CrearProyectoEstado: function () {
            var datos = $("#form_proyectoestado").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "proyectoestado/crear",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoEstado();
                        $('#proyectoestadoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {

                    }
                }
            })
        },
        BuscarProyectoEstado: function (id) {
            $.ajax({
                url: "proyectoestado/buscar",
                type: "POST",
                data: "&id=" + id,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        $.each(obj.datos, function (index, data) {
                            $("#form_proyectoestado #txt_id").val(data['id']);
                            $("#form_proyectoestado #txt_nombre").val(data['nombre']);
                            $("#form_proyectoestado #slct_estado").val(data['estado']);
                        })
                    } else {

                    }
                }
            })
        },
        ActualizarProyectoEstado: function () {
            var datos = $("#form_proyectoestado").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "proyectoestado/editar",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoEstado();
                        $('#proyectoestadoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                }
            });
        },
        EstadoProyectoEstado: function (estado, id) {
            $.ajax({
                url: "proyectoestado/estado",
                type: "POST",
                data: "estado=" + estado + "&id=" + id,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoEstado();
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                },
                error: function () {

                }
            });
        },
        // ***********************************  Mantenimiento Proyecto Tipo  ***********************************

        MostrarProyectoTipo: function () {
            $.ajax({
                url: "proyectotipo/listar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        var html = "";
                        var htmlEstado = "";
                        $.each(obj.datos, function (index, data) {
                            htmlEstado = '<span class="btn btn-success" onclick="Mantenimiento.EstadoProyectoTipo(0, ' + data.id + ')">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                            if (data.estado == 0) {
                                htmlEstado = '<span class="btn btn-danger" onclick="Mantenimiento.EstadoProyectoTipo(1, ' + data.id + ')"> Inactivo</span>';
                            }
                            html += "<tr>";
                            html += "<td>" + data.proyecto + "</td>";
                            html += "<td>" + data.nombre + "</td>";
                            html += "<td>" + htmlEstado + "</td>";
                            html += '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectotipoModal" onclick="Mantenimiento.BuscarProyectoTipo(' + data.id + ')" data-accion="actualizar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                            html += "</tr>";
                        });

                        $("#t_proyectoTipo").dataTable().fnDestroy();
                        $("#tb_proyectoTipo").html(html);
                        $("#t_proyectoTipo").dataTable();
                        $(".overlay,.loading-img").remove();
                    } 
                    else {

                    }
                }
            });
        },
        cargarProyecto: function (accion, proyecto_id) {
            $.ajax({
                url: 'proyectotipo/cargarproyecto',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    if (obj.rst == 1) {
                        $('#slct_proyecto_id').html('');
                        $.each(obj.datos, function (index, data) {
                            $('#slct_proyecto_id').append('<option value=' + data.id + '>' + data.nombre + '</option>');
                        });
                        if (accion === 'nuevo')
                            $('#slct_proyecto_id').append("<option selected style='display:none;'>--- Elige Proyecto ---</option>");
                        else
                            $('#slct_proyecto_id').val(proyecto_id);
                    }
                }
            });
        },
        CrearProyectoTipo: function () {
            var datos = $("#form_proyectotipo").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "proyectotipo/crear",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoTipo();
                        $('#proyectotipoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {

                    }
                }
            });
        },
        BuscarProyectoTipo: function (id) {
            $.ajax({
                url: "proyectotipo/buscar",
                type: "POST",
                data: "&id=" + id,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        $.each(obj.datos, function (index, data) {
                            $("#form_proyectotipo #txt_id").val(data['id']);
                            $("#form_proyectotipo #slct_proyecto_id").val(data['proyecto_id']);
                            $("#form_proyectotipo #txt_nombre").val(data['nombre']);
                            $("#form_proyectotipo #slct_estado").val(data['estado']);
                        });
                    } else {

                    }
                }
            });
        },
        ActualizarProyectoTipo: function () {
            var datos = $("#form_proyectotipo").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "proyectotipo/editar",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoTipo();
                        $('#proyectotipoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                }
            });
        },
        EstadoProyectoTipo: function (estado, id) {
            $.ajax({
                url: "proyectotipo/estado",
                type: "POST",
                data: "estado=" + estado + "&id=" + id,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarProyectoTipo();
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                },
                error: function () {

                }
            });
        },
        // ***********************************  Mantenimiento Casuistica  ***********************************

        MostrarCasuistica: function () {
            $.ajax({
                url: "casuistica/listar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        var html = "";
                        var htmlEstado = "";
                        $.each(obj.datos, function (index, data) {
                            htmlEstado = '<span class="btn btn-success" onclick="Mantenimiento.EstadoCasuistica(0, ' + data.id + ')">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                            if (data.estado == 0) {
                                htmlEstado = '<span class="btn btn-danger" onclick="Mantenimiento.EstadoCasuistica(1, ' + data.id + ')"> Inactivo</span>';
                            }
                            html += "<tr>";
                            html += "<td>" + data.casuistica + "</td>";
                            html += "<td>" + data.tipo + "</td>";
                            html += "<td>" + htmlEstado + "</td>";
                            html += '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#casuisticaModal" onclick="Mantenimiento.BuscarCasuistica(' + data.id + ')" data-accion="actualizar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                            html += "</tr>";
                        })

                        $("#t_casuistica").dataTable().fnDestroy();
                        $("#tb_casuistica").html(html);
                        $("#t_casuistica").dataTable();
                        $(".overlay,.loading-img").remove();
                    } else {

                    }
                }
            });
        },
        cargarProyectoTipo: function (accion, proyecto_tipo_id) {
            $.ajax({
                url: 'proyectotipo/cargarproyectotipo',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    if (obj.rst == 1) {
                        $('#slct_proyecto_tipo_id').html('');
                        $.each(obj.datos, function (index, data) {
                            $('#slct_proyecto_tipo_id').append('<option value=' + data.id + '>' + data.nombre + '</option>');
                        });
                        if (accion === 'nuevo')
                            $('#slct_proyecto_tipo_id').append("<option selected style='display:none;'>--- Elige Proyecto Tipo ---</option>");
                        else
                            $('#slct_proyecto_tipo_id').val(proyecto_tipo_id);
                    }
                }
            });
        },
        CrearCasuistica: function () {
            var datos = $("#form_casuistica").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "casuistica/crear",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuistica();
                        $('#casuisticaModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {

                    }
                }
            });
        },
        BuscarCasuistica: function (id) {
            $.ajax({
                url: "casuistica/buscar",
                type: "POST",
                data: "&id=" + id,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        $.each(obj.datos, function (index, data) {
                            $("#form_casuistica #txt_id").val(data['id']);
                            $("#form_casuistica #txt_nombre").val(data['nombre']);
                            $("#form_casuistica #slct_proyecto_tipo_id").val(data['proyecto_tipo_id']);
                            $("#form_casuistica #slct_estado").val(data['estado']);
                        })
                    } else {

                    }
                }
            });
        },
        ActualizarCasuistica: function () {
            var datos = $("#form_casuistica").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "casuistica/editar",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuistica();
                        $('#casuisticaModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                }
            })
        },
        EstadoCasuistica: function (estado, id) {
            $.ajax({
                url: "casuistica/estado",
                type: "POST",
                data: "estado=" + estado + "&id=" + id,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuistica();
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                },
                error: function () {

                }
            })
        },
        // ***********************************  Mantenimiento Casuistica Proyecto Estado  ***********************************

        MostrarCasuisticaProyectoEstado: function () {
            $.ajax({
                url: "casuisticaproyectoestado/listar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        var html = "";
                        var htmlEstado = "";
                        $.each(obj.datos, function (index, data) {
                            htmlEstado = '<span class="btn btn-success" onclick="Mantenimiento.EstadoCasuisticaProyectoEstado(0, ' + data.id + ')">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                            if (data.estado == 0) {
                                htmlEstado = '<span class="btn btn-danger" onclick="Mantenimiento.EstadoCasuisticaProyectoEstado(1, ' + data.id + ')"> Inactivo</span>';
                            }
                            html += "<tr>";
                            html += "<td>" + data.proyecto_estado + "</td>";
                            html += "<td>" + data.casuistica + "</td>";
                            html += "<td>" + htmlEstado + "</td>";
                            html += '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#casuisticaProyectoEstadoModal" onclick="Mantenimiento.BuscarCasuisticaProyectoEstado(' + data.id + ')" data-accion="actualizar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                            html += "</tr>";
                        })

                        $("#t_casuisticaproyectoestado").dataTable().fnDestroy();
                        $("#tb_casuisticaproyectoestado").html(html);
                        $("#t_casuisticaproyectoestado").dataTable();
                        $(".overlay,.loading-img").remove();
                    } else {

                    }
                }
            })
        },
        cargarProyectoEstado: function (accion, proyecto_estado_id) {
            $.ajax({
                url: 'proyectoestado/cargar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    if (obj.rst == 1) {
                        $('#slct_proyecto_estado_id').html('');
                        $.each(obj.datos, function (index, data) {
                            $('#slct_proyecto_estado_id').append('<option value=' + data.id + '>' + data.nombre + '</option>');
                        });
                        if (accion === 'nuevo')
                            $('#slct_proyecto_estado_id').append("<option selected style='display:none;'>--- Elige Proyecto Estado ---</option>");
                        else
                            $('#slct_proyecto_estado_id').val(proyecto_estado_id);
                    }
                }
            });
        },
        cargarCasuistica: function (accion, casuistica_id) {
            $.ajax({
                url: 'casuistica/cargar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    if (obj.rst == 1) {
                        $('#slct_casuistica_id').html('');
                        $.each(obj.datos, function (index, data) {
                            $('#slct_casuistica_id').append('<option value=' + data.id + '>' + data.nombre + '</option>');
                        });
                        if (accion === 'nuevo')
                            $('#slct_casuistica_id').append("<option selected style='display:none;'>--- Elige Casuistica ---</option>");
                        else
                            $('#slct_casuistica_id').val(casuistica_id);
                    }
                }
            });
        },
        CrearCasuisticaProyectoEstado: function () {
            var datos = $("#form_casuisticaproyectoestado").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "casuisticaproyectoestado/crear",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuisticaProyectoEstado();
                        $('#casuisticaProyectoEstadoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    } else {

                    }
                }
            })
        },
        BuscarCasuisticaProyectoEstado: function (id) {
            $.ajax({
                url: "casuisticaproyectoestado/buscar",
                type: "POST",
                data: "&id=" + id,
                dataType: "json",
                cache: false,
                success: function (obj) {
                    if (obj.rst === 1) {
                        $.each(obj.datos, function (index, data) {
                            $("#form_casuisticaproyectoestado #txt_id").val(data['id']);
                            $("#form_casuisticaproyectoestado #slct_proyecto_estado_id").val(data['proyecto_estado_id']);
                            $("#form_casuisticaproyectoestado #slct_casuistica_id").val(data['casuistica_id']);
                            $("#form_casuisticaproyectoestado #slct_estado").val(data['estado']);
                        })
                    } else {

                    }
                }
            })
        },
        ActualizarCasuisticaProyectoEstado: function () {
            var datos = $("#form_casuisticaproyectoestado").serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: "casuisticaproyectoestado/editar",
                type: "POST",
                data: datos,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuisticaProyectoEstado();
                        $('#casuisticaProyectoEstadoModal').modal('hide');
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                }
            })
        },
        EstadoCasuisticaProyectoEstado: function (estado, id) {
            $.ajax({
                url: "casuisticaproyectoestado/estado",
                type: "POST",
                data: "estado=" + estado + "&id=" + id,
                dataType: "json",
                cache: false,
                beforeSend: function () {

                },
                success: function (obj) {
                    if (obj.rst === 1) {
                        Mantenimiento.MostrarCasuisticaProyectoEstado();
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                },
                error: function () {

                }
            })
        },
        // ************************************ DEMO  *********************************************
        // MostrarDinamico: function (){
        //     alert('Dentro d la funcion');
        //     $.ajax({
        //         url: "proyectotipo/bandeja",
        //         type: 'POST',
        //         cache: false,
        //         dataType: 'json',
        //         beforeSend: function () {
        //             //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        //         },                       
        //         success: function (obj) {
        //             var html = "";
        //             var htmlCab = "";
        //             if(obj.rst == 1){
        //                 // htmlCab += "<tr>";
        //                 // $.each(obj.estados, function(index, est){
        //                 //     htmlCab += "<th>"+ est.nombre +"</th>";
        //                 // })
        //                 // htmlCab += "</tr>";

        //                 // $.each(obj.datoid, function(index, datape){
        //                 //     //var demo = Object.keys(obj.datoid).length;
        //                 //     //alert(demo);
        //                 //     html += "<tr>";
        //                 //     $.each(obj.datodemo, function(index, datage){

        //                 //         if(datape.id == datage.id){

        //                 //             $.each(obj.estados, function(index, est){
        //                 //                 var demo = Object.keys(obj.estados).length;
        //                 //                 //alert(demo);
        //                 //                 var cadena = "";
        //                 //                 //for(i=1; i<=demo; i++){
        //                 //                     if(est.id == datage.proyecto_estado_id){
        //                 //                         var i = datage.proyecto_estado_id;
        //                 //                         cadena = 'estado'+i;
        //                 //                         html += "<td>"+ datage[cadena] +"</td>";
        //                 //                     //}
        //                 //                 }
        //                 //             })

        //                 //         }

        //                 //     })
        //                 //     html += "/<tr>";                                
        //                 // })



        //                 /*var deta = "";
        //                 var name = "";
        //                 html += "<tr>";
        //                 for(i = 1; i<=10; i++){
        //                     deta = "obj.estados"+i;
        //                     name = "est"+i;
        //                     $.each(deta, function(index, name){
        //                         html += "<td>"+ name.fecha_inicio +"</td>";
        //                         html += "<td>"+ name.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ name.fecha_compromiso +"</td>";
        //                         html += "<td>"+ name.fecha_termino +"</td>";
        //                         html += "<td>"+ name.fecha_gestion +"</td>";
        //                         html += "<td>"+ name.recepcion_expediente +"</td>";
        //                         html += "<td>"+ name.fecha_teorica_final +"</td>";
        //                     })
        //                 }
        //                 html += "</tr>";*/

        //                 html += "<tr>";

        //                 $.each(obj.estados1, function(index, est1){
        //                     if(est1.total == 1){
        //                         html += "<td>"+ est1.fecha_inicio +"</td>";
        //                         html += "<td>"+ est1.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est1.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est1.fecha_termino +"</td>";
        //                         html += "<td>"+ est1.fecha_gestion +"</td>";
        //                         html += "<td>"+ est1.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est1.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados2, function(index, est2){
        //                     if(est2.total == 1){
        //                         html += "<td>"+ est2.fecha_inicio +"</td>";
        //                         html += "<td>"+ est2.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est2.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est2.fecha_termino +"</td>";
        //                         html += "<td>"+ est2.fecha_gestion +"</td>";
        //                         html += "<td>"+ est2.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est2.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados3, function(index, est3){
        //                     if(est3.total == 1){
        //                         html += "<td>"+ est3.fecha_inicio +"</td>";
        //                         html += "<td>"+ est3.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est3.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est3.fecha_termino +"</td>";
        //                         html += "<td>"+ est3.fecha_gestion +"</td>";
        //                         html += "<td>"+ est3.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est3.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }

        //                 })

        //                 $.each(obj.estados4, function(index, est4){
        //                     if(est4.total == 1){
        //                         html += "<td>"+ est4.fecha_inicio +"</td>";
        //                         html += "<td>"+ est4.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est4.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est4.fecha_termino +"</td>";
        //                         html += "<td>"+ est4.fecha_gestion +"</td>";
        //                         html += "<td>"+ est4.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est4.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados5, function(index, est5){
        //                     if(est5.total == 1){
        //                         html += "<td>"+ est5.fecha_inicio +"</td>";
        //                         html += "<td>"+ est5.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est5.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est5.fecha_termino +"</td>";
        //                         html += "<td>"+ est5.fecha_gestion +"</td>";
        //                         html += "<td>"+ est5.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est5.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados6, function(index, est6){
        //                     if(est6.total == 1){
        //                         html += "<td>"+ est6.fecha_inicio +"</td>";
        //                         html += "<td>"+ est6.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est6.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est6.fecha_termino +"</td>";
        //                         html += "<td>"+ est6.fecha_gestion +"</td>";
        //                         html += "<td>"+ est6.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est6.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados7, function(index, est7){
        //                     if(est7.total == 1){
        //                         html += "<td>"+ est7.fecha_inicio +"</td>";
        //                         html += "<td>"+ est7.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est7.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est7.fecha_termino +"</td>";
        //                         html += "<td>"+ est7.fecha_gestion +"</td>";
        //                         html += "<td>"+ est7.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est7.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados8, function(index, est8){
        //                     if(est8.total == 1){
        //                         html += "<td>"+ est8.fecha_inicio +"</td>";
        //                         html += "<td>"+ est8.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est8.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est8.fecha_termino +"</td>";
        //                         html += "<td>"+ est8.fecha_gestion +"</td>";
        //                         html += "<td>"+ est8.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est8.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados9, function(index, est9){
        //                     if(est9.total == 1){
        //                         html += "<td>"+ est9.fecha_inicio +"</td>";
        //                         html += "<td>"+ est9.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est9.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est9.fecha_termino +"</td>";
        //                         html += "<td>"+ est9.fecha_gestion +"</td>";
        //                         html += "<td>"+ est9.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est9.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })

        //                 $.each(obj.estados10, function(index, est10){
        //                     if(est10.total == 1){
        //                         html += "<td>"+ est10.fecha_inicio +"</td>";
        //                         html += "<td>"+ est10.fecha_seguimiento +"</td>";
        //                         html += "<td>"+ est10.fecha_compromiso +"</td>";
        //                         html += "<td>"+ est10.fecha_termino +"</td>";
        //                         html += "<td>"+ est10.fecha_gestion +"</td>";
        //                         html += "<td>"+ est10.recepcion_expediente +"</td>";
        //                         html += "<td>"+ est10.fecha_teorica_final +"</td>";
        //                     }else{
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                         html += "<td>0</td>";
        //                     }
        //                 })



        //                 html += "</tr>";

        //                 alert(html);
        //                 $("#t_demo").dataTable().fnDestroy();
        //                 $("#tb_demo").html(html);
        //                 //$("#th_demo").html(htmlCab);
        //                 $("#t_demo").dataTable();
        //             }else{

        //             }
        //             //$(".overlay,.loading-img").remove();
        //         }
        //     })
        // }
    }

</script>