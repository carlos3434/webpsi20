<script type="text/javascript">
    var route_pd = 'proyectoedificiodisenio/';
    var form_proceso_disenio = '#form_proceso_disenio';
    var ProcesoDisenio = {
        cargarPreguntas: function () {
            $.ajax({
                url: route_pd + 'cargar-pregunta',
                type: 'POST',
                cache: false,
                dataType: 'json',
                //data: datos,
//                beforeSend: function () {
//                    //eventoCargaMostrar();
//                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        cargarHtmlPorPestania(obj.datos);
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                    //eventoCargaRemover();
                }
            });
        },
        cargarProceso: function () {
            $.ajax({
                url: route_pd + 'leer-disenio',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: 'proyecto_edificio_id=' + $('#proyecto_edificio_id').val(),
                beforeSend: function () {
                    //eventoCargaMostrar();
                    $(form_proceso_disenio + ' :input[type=text]').val('');
                    $(form_proceso_disenio + ' :checkbox').prop('checked', false);
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        valores = obj.datos;
                        $.each(valores, function (index, text) {
                            if ((text.respuesta).length > 1) {
                                $(form_proceso_disenio + " #c" + text.preguntaRelacionId).val(text.respuesta);
                            } else {
                                $(form_proceso_disenio + " #c" + text.preguntaRelacionId).prop("checked", true);
                            }
                            if ((text.sustento).length && $(form_proceso_disenio + " #cs" + text.preguntaRelacionId).length) {
                                $(form_proceso_disenio + " #cs" + text.preguntaRelacionId).val(text.sustento);
                            }
                        });
                    } else {
                        Psi.mensaje('error', '', 6000);
                    }
                    eventoCargaRemover();
                }
            });
        },
        crearProceso: function () {
            var datos = $('#form_proceso_disenio').serialize();
            $.ajax({
                url: route_pd + 'crear-disenio',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        Psi.mensaje('info', obj.msj, 6000);
                    } else {
                        Psi.mensaje('error', 'No se creo gestion-diseño', 6000);
                    }
                    eventoCargaRemover();
                }
            });
        },
    };
</script>
