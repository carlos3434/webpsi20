<script type="text/javascript">
    var dt_proyecto_edificios;
    var dt_proyectoBitacora;
    var dt_proyectoGestion;
    var dt_proyectoGestionTotal;
    var dt_proyectoProvisional;
    var dt_proyectoMega;
    var dt_proyectoGestionTotal;
    var dt_proyectoPapelera;
    var dt_proyectoGestionFiltros = {};
    var id_modal_proy = '#proyectoEdificioModal';
    var id_table_proy = '#datatable_proyectoedificio';
    var route = 'proyecto_edificio/';
    var route_g = 'proyecto_gedificio/';
    var chk_retornar = 0;
    var formPorEstado = {
        0: '#form_no_existe',
        3: '#form_proyecto_diseno',
        5: '#form_proyecto_licencia',
        6: '#form_proyecto_oocc',
        7: '#form_proyecto_cableado',
        4: '#form_proyecto_iniciativa',
        2: '#form_proyecto_validacionreversa'
    };
    var ProyectoEdificio = {
        gestionar: function () {
            var datos = "";
            var proyecto_tipo = $("#slct_gcasuistica_modal").val();
            if (proyecto_tipo == '1') {
                datos = $("#form_proyecto_diseno").serialize().split("txt_").join("").split("slct_").join("");
            } else if (proyecto_tipo == '2') {
                datos = $("#form_proyecto_licencia").serialize().split("txt_").join("").split("slct_").join("");
            } else if (proyecto_tipo == '3') {
                datos = $("#form_proyecto_oocc").serialize().split("txt_").join("").split("slct_").join("");
            } else if (proyecto_tipo == '4') {
                datos = $("#form_proyecto_cableado").serialize().split("txt_").join("").split("slct_").join("");
            } else if (proyecto_tipo == '5') {
                datos = $("#form_proyecto_megas").serialize().split("txt_").join("").split("slct_").join("");
            } else if (proyecto_tipo == '6') {
                datos = $("#form_proyecto_iniciativa").serialize().replace(/txt_|slct_/g, '');
            } else if (proyecto_tipo == '7') {
                datos = $("#form_proyecto_validacionreversa").serialize().replace(/txt_|slct_/g, '');
            }
            datos = datos + '&proyecto_edificio_id=' + $('#txt_proyecto_edificio_id').val();
            datos = datos + '&proyecto_tipo_id=' + $('#slct_gcasuistica_modal').val();
            datos = datos + '&casuistica_id=' + $('#slct_gcasuistica_detalle_modal').val();
            datos = datos + '&retorna=' + chk_retornar;
            //datos = datos + '&proyecto_estado_id=' + $('#slct_estado_modal').val();
            //datos = datos + '&avance=' + $('#txt_avance').val();
            //datos = datos + '&' + $("#form_proyecto_mega").serialize().split("txt_").join("").split("slct_").join("");
            //datos = datos + '&' + $("#form_proyecto_mega").serialize().replace(/txt_|slct_/g, '');//.split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: route_g + 'gestionar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        $('#txt_proyecto_estado_id').val(obj.datos.avance); /* proyecto_estado_id */
                        Psi.mensaje('success', obj.msj, 6000);
                        if (obj.datos.fecha_termino !== '') {
                            $(formPorEstado[obj.datos.avance]).find('#txt_fecha_inicio').val(obj.datos.fecha_termino);
                        }
                        //HTMLCargarGestiones(obj.datos);
                        guardarBitacoraGestion(datos);
                        ProyectoEdificio.cargarGestionesDos();
                        ProyectoEdificio.cargarGestionesTotal();
                        // Se recarga listado-bitacora cuando agregar-bitacora-gestion y devuelve un resultado
                        //ProyectoEdificio.cargarBitacoraDos();
                        if (obj.datos.avance != obj.datos.proyecto_estado_id) {
                            $('#lbl_proyecto_estado').html(obj.estado);
                            $('#txt_proyecto_estado').val(obj.estado + ' (' + $('#lbl_provisional_estado').html() + ')');
                        }
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                    eventoCargaRemover();
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        gestionarProvisional: function () {
            datos = "";
            if ($("form[name=form_proyecto_provisional] input[name=txt_fecentrega]").val() != "") {
                datos = datos + '&' + $("form[name=form_proyecto_provisional]").serialize().split("txt_").join("").split("slct_").join("");
                datos += '&estado_proyecto=' + ProyectoEdificio.data.proyecto_estado;
                datos += '&proyecto_edificio_id=' + ProyectoEdificio.data.item;
                var provisional = functionajax(route_g + 'gestionar-provisional', 'POST', false, 'json', false, datos);
                provisional.success(function (obj) {
                    if (parseInt(obj.rst) == 1) {
                        //$("form[name=form_proyecto_provisional] input[type=text]").val("");
                        Psi.mensaje('success', obj.msj, 6000);
                        ProyectoEdificio.cargarProvisionales();
                        // AGREGAR TERMINO LADO DEL PROYECTO_ESTADO
                        sprovisional = '';
                        if ((obj.datos.estado_provisional).length > 0) {
                            sprovisional = ' (' + obj.datos.estado_provisional + ')';
                            $('#txt_proyecto_estado').val($('#lbl_proyecto_estado').html() + sprovisional);
                        }
                        $('#lbl_provisional_estado').html(sprovisional);
                    } else {
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                });
            }
        },
        gestionarMega: function () {
            datos = "";
            datos = datos + '&' + $("form[name=form_proyecto_mega]").serialize().split("txt_").join("").split("slct_").join("");
            eventoCargaMostrar();
            if (ProyectoEdificio.data.segmento.toUpperCase() == "MEGA PROYECTO") {
                datos += '&proyecto_edificio_id=' + $('#txt_proyecto_edificio_id').val();
                var mega = functionajax(route_g + 'gestionar-mega-proyecto', 'POST', false, 'json', false, datos);
                mega.success(function (obj) {
                    if (obj.rst == 1) {
                        setTimeout(eventoCargaRemover(), 3000);
                        Psi.mensaje('success', "Se realizo el Registro con Exito", 6000);
                    } else {
                        setTimeout(eventoCargaRemover(), 3000);
                        Psi.mensaje('danger', "Hubo un error al Registrar", 6000);
                    }

                });
            } else {
                eventoCargaRemover();
                Psi.mensaje('danger', "El proyecto no pertenece a un Segmento MEGAPROYECTO", 6000);
            }
        },
        crearBitacora: function (proyecto_edificio_id) {
            var datos = '&proyecto_edificio_id=' + proyecto_edificio_id;
            var htmls = '';
            $.ajax({
                url: route + 'agregar-bitacora',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: ($("#form_proyecto_bitacora").serialize().replace(/txt_|slct_/g, '')) + datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        fecha = $("#txt_fecha_observacion").val();
                        $('#form_proyecto_bitacora').get(0).reset();
                        $('#txt_fecha_observacion').val(fecha);
                        Psi.mensaje('success', obj.msg, 6000);
                        dt_proyectoBitacora.ajax.reload();
                    } else {
                        msn = '';
                        for (var prop in obj.msg) {
                            msn += obj.msg[prop][0] + ' ';
                        }
                        Psi.mensaje('danger', msn, 6000);
                    }
                    eventoCargaRemover();
                }
            });
        },
        crearBitacoraGestion: function (datos) {
            if (datos != "") {
                bitacora = functionajax(route + 'agregar-bitacora-gestion', 'POST', false, 'json', false, datos);
                bitacora.success(function (datos) {
                    if (datos.datos > 0) {
                        ProyectoEdificio.cargarBitacoraDos();
                    }

                });
            }

        },
        //
        ver: function (proyecto_edificio_id) {
            $("#form_proyecto_provisional input[type=text]").val("");
            var datos = 'proyecto_edificio_id=' + proyecto_edificio_id;
            var htmls = '';
            $.ajax({
                url: route + 'ver-todo',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        ProyectoEdificio.data = obj.datos;
                        validarGestionPorEstado(obj.ultimos);
                        (cargarMapa(obj.datos.coord_x, obj.datos.coord_y));
                        rellenarInputGestionEdificios(obj.datos, obj);
                        rellenarLabelTotales(obj.estados, obj.datos.proyectoEstado);
                        htmls += '<div class="box" style="height:460px;overflow:auto;"><div class="box-body table-responsive no-padding"><table class="table table-condensed table-striped"><tbody>';
                        $.each(obj.datos, function (index, data) {
                            htmls += '<tr>';
                            htmls += '<td class="text-uppercase" style="width:35px">' + index.replace(/_/g, ' ') + '</td>';
                            htmls += '<td>' + data + '</td>';
                            htmls += '</tr>';
                        });
                        htmls += '</tbody></table></div></div>';
                        if (obj.provisional.datos.length > 0) {
                            var provisional = obj.provisional.datos[0];
                            $("#form_proyecto_provisional input[name=txt_fecentrega]").val(moment(provisional.fecentrega).isValid() ? moment(provisional.fecentrega).format("YYYY-MM-DD") : '');
                            $("#form_proyecto_provisional input[name=txt_fecrespuesta]").val(moment(provisional.fecrespuesta).isValid() ? moment(provisional.fecrespuesta).format("YYYY-MM-DD") : '');
                            $("#form_proyecto_provisional input[name=txt_fecliquidacion]").val((moment(provisional.fecliquidacion)).isValid() ? moment(provisional.fecliquidacion).format("YYYY-MM-DD") : '');
                            $("#form_proyecto_provisional input[name=txt_fectermino]").val((moment(provisional.fectermino)).isValid() ? moment(provisional.fectermino).format("YYYY-MM-DD") : '');
                            $("#form_proyecto_provisional input[name=proyecto_estado_temp]").val(ProyectoEdificio.data.proyecto_estado);
                            $("#form_proyecto_provisional input[name=proyecto_provisional_parent_temp]").val(provisional.id);
                            $("#form_proyecto_provisional input[name=txt_ejecutor_proyecto]").val(provisional.ejecutor_proyecto);
                            $("#form_proyecto_provisional #txt_estado_provisional option[value='" + provisional.estado_provisional + "']").attr("selected", "selected");
                        } else {
                            $("#form_proyecto_provisional input[name=proyecto_estado_temp]").val(0);
                            $("#form_proyecto_provisional input[name=proyecto_provisional_parent_temp]").val(0);
                        }
                        if (ProyectoEdificio.data.segmento == "MEGA PROYECTO") {
                            $("#fieldset_proyecto_mega").css('display', 'block');
                        } else {
                            $("#fieldset_proyecto_mega").css('display', 'none');
                        }
                        if (obj.megaproyecto.datos.length > 0) {
                            var megaproyecto = obj.megaproyecto.datos[0];
                            $("#form_proyecto_mega input[name=txt_energia_estado]").val(megaproyecto.energia_estado);
                            $("#form_proyecto_mega input[name=txt_fo_estado]").val(megaproyecto.fo_estado);
                            $("#form_proyecto_mega input[name=txt_troba_nueva]").val(megaproyecto.troba_nueva);
                            $("#form_proyecto_mega input[name=txt_fecha_fin]").val(megaproyecto.troba_fecha_fin);
                            $("#form_proyecto_mega input[name=txt_troba_estado]").val(megaproyecto.troba_estado);
                            $("#form_proyecto_mega input[name=mega_proyecto_parent_temp]").val(obj.megaproyecto.datos[0].id);
                        } else {
                            $("#form_proyecto_mega input[name=mega_proyecto_parent_temp]").val(0);
                        }

                    }
                    $('#tab_4_content').html(htmls);
                    ProyectoEdificio.cargarProvisionales();
                    ProyectoEdificio.validaProvisionalAcceso();
                    ProyectoEdificio.cargarMegaProyecto();
                    ProyectoEdificio.validaMegaAcceso();
                    setTimeout(eventoCargaRemover(), 5000);
                    // ES TEMPORAL... BUSCAR MEJOR SOLUCION
                    // CUANDO SE CREA POR PRIMERA VEZ EL EDIFICIO, EL ESTADO DEBE SER INICIATIVA
                    if (obj.datos.proyecto_estado == 'A1 INICIATIVA' && obj.datos.proyecto_tipo == '' && (obj.ultimos).length == 0) {
                        $('#txt_proyecto_estado_id').val(4);
                    }
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        ubicar: function (proyecto_edificio_id, x, y) {
            var datos = 'proyecto_edificio_id=' + proyecto_edificio_id + '&coord_x=' + x + '&coord_y=' + y;
            $.ajax({
                url: route + 'ubicar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    Psi.mensaje('success', obj.msg, 6000);
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso, favor de intentar nuevamente. (UbicarXY)', 6000);
                }
            });
        },
        edificiosUbicacion: function (y, x, cant, instanciaMapa) {
            if (y.length < 1 || x.length < 1) {
                Psi.mensaje('info', 'Coordenadas X y/o Y incompleto. No se ubicaran otros Edificios ni mostrara Ubicación.', 6000);
                return false;
            }
            $.ajax({
                url: route + 'ubicar-distancia',
                type: 'POST',
                cache: false,
                data: {coord_y: y, coord_x: x, cantidad: cant},
                dataType: 'json',
//                beforeSend: function () {
//                    eventoCargaMostrar();
//                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        cargarPuntoOtroEdificioenMapa(obj.datos, instanciaMapa);
                    } else {
                        Psi.mensaje('info', obj.msg, 6000);
                    }
                    //eventoCargaRemover();
                }
            });
        },
        // LISTAR
        cargarDos: (function () {
            var oTable = $('#datatable_proyectoedificio')
                    .on('order.dt', function () {
                        eventoCargaMostrar();
                    })
                    .on('search.dt', function () {
                        eventoCargaMostrar();
                    })
                    .on('page.dt', function () {
                        eventoCargaMostrar();
                    })
                    .DataTable({
                        dom: "<'box'" +
                                //"<'box-header'<l>r>" +
                                "<'box-body table-responsive no-padding-1't>" +
                                "<'row'<'col-xs-6'ir><'col-xs-6'p>>" +
                                ">",
                        "processing": true,
                        "serverSide": true,
                        //"responsive": true,
                        "ajax": {
                            url: route + "listar-dos",
                            type: "POST",
                            data: function (d) {
                                d.txt_rangofecha = $('input[name=fecha_created_at]').val();
                                d.txt_buscar = $('input[name=txt_buscar]').val();
                                d.txt_buscarPor = $('input[name=txt_buscarPor]').val();
                                d.slct_segmento = $('#slct_segmento').val();
                                d.slct_tipoproyecto = $('#slct_tipoproyecto').val();
                                d.slct_estadoproyecto = $('#slct_estadoproyecto').val();
                                d.txt_direccion_n = $('#txt_direccion_n').val();
                            }
                        },
                        "columns": [
                            {data: 'id', name: 'id', 'class': 'colunm-index', visible: false}
                            , {data: 'ida', name: 'ida', searchable: false, visible: false}
                            , {data: 'parent', name: 'parent'}
                            , {data: 'ticket', name: 'ticket'}
                            , {data: 'nombre_proyecto', name: 'nombre_proyecto'}
                            , {data: 'segmento', name: 'segmento'}
                            , {data: 'tipo_proyecto', name: 'tipo_proyecto'}
                            , {data: 'distrito', name: 'distrito'}
                            , {data: 'direccion', name: 'direccion'}
                            , {data: 'numero_departamentos', name: 'numero_departamentos', orderable: false, searchable: false}
                            , {data: 'avance', name: 'avance', "width": "6%"}
                            , {data: 'fecha_termino', name: 'fecha_termino', orderable: false, searchable: false}
                            , {data: 'mes', name: 'mes', orderable: false, searchable: false}
                            , {data: 'proyecto_estado', name: 'proyecto_estado'}
                            , {data: 'semaforo', name: 'semaforo', orderable: false, searchable: false, className: 'text-center', render: function (data, type, row) {
                                    return columnaSemaforo(data, type, row);
                                }}
                            , {data: 'action', name: 'action', "width": "15%", 'class': 'colunm-acciones', orderable: false, searchable: false, className: 'text-center'}
                        ],
                        "order": [[1, 'desc']],
                        "pageLength": 20,
                        searchDelay: 750,
                        initComplete: function (settings, json) {
                            // PERMISOS
                            if (editarG == 0)
                                $('.editarG').remove();
                            if (eliminarG == 0)
                                $('.eliminarG').remove();
                            if (agregarG == 0)
                                $('.agregarG').remove();
                            //
                            this.api().columns().eq(0).each(function (index) {
                                var column = this.column(index);
                                if (index == 12 || index == 14) {

                                } else if (index == 15) {
                                    // button download
                                    var a = document.createElement('a');
                                    $(a).addClass('btn btn-success')
                                            .html('<i class="fa fa-download fa-lg"></i>')
                                            .attr('href', '#')
                                            .attr('url', 'reporte/proyectoexcel')
                                            .attr('id', 'btnProyectoexcel')
                                            .appendTo($(column.header()).empty());
                                } else {
                                    var input = document.createElement("input");
                                    var title = $(column.footer()).text();
                                    $(input).css('width', '100%')
                                            .attr('placeholder', title)
                                            .addClass('form-control input-sm')
                                            .appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false, true).draw();
                                            });
                                }
                            });
                        },
                        drawCallback: function (settings) {
                            eventoCargaRemover();
                        }
                    });
            return oTable;
        }),
        cargarGestionesTotal: function () {
            if (typeof dt_proyectoGestionTotal === 'undefined') {
                dt_proyectoGestionTotal = $('#t_movimiento_total')
                        .on('order.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('search.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('page.dt', function () {
                            eventoCargaMostrar();
                        })
                        .DataTable({
                            "ajax": {
                                url: route_g + "listargestiones",
                                type: "POST",
                                dataSrc: 'datos',
                                data: function (d) {
                                    d.proyecto_edificio_id = $('#txt_proyecto_edificio_id').val();
                                }
                            },
                            "columns": [
                                {data: 'fila', name: 'fila', "width": "3%"}
                                , {data: 'proyecto_tipo', name: 'proyecto_tipo'}
                                , {data: 'fecha_inicio', name: 'fecha_inicio'}
                                , {data: 'fecha_compromiso', name: 'fecha_compromiso'}
                                , {data: 'fecha_gestion', name: 'fecha_gestion'}
                                , {data: 'estado_general', name: 'estado_general'}

                                , {data: 'gestion_interna', name: 'gestion_interna'}
                                , {data: 'recepcion_expediente', name: 'recepcion_expediente'}
                                //, {data: 'fecha_teorica_final', name: 'fecha_teorica_final'}
                                , {data: 'fecha_termino', name: 'fecha_termino'}
                                , {data: 'casuistica', name: 'casuistica'}
                                , {data: 'proyecto_estado', name: 'proyecto_estado'}

                                , {data: 'fecha_seguimiento', name: 'fecha_seguimiento'}
                                , {data: 'created_at', name: 'created_at'}
                                , {data: 'usuario', name: 'usuario'}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[0, 'desc']],
                            "pageLength": 10,
                            "createdRow": function (row, data, dataIndex) {
                                $(row).addClass('selected-' + data['proyecto_tipo_alias']);
                            },
                            drawCallback: function (settings) {
                                eventoCargaRemover();
                            }
                        });
            } else {
                dt_proyectoGestionTotal.ajax.reload();
            }
        },
        cargarBitacoraDos: function () {
            if (typeof dt_proyectoBitacora === 'undefined') {
                dt_proyectoBitacora = $('#datatable_proyecto_bitacora')
                        .DataTable({
                            "ajax": {
                                url: route + "listar-bitacora",
                                type: "POST",
                                dataSrc: 'datos',
                                data: function (d) {
                                    d.proyecto_edificio_id = $('#txt_proyecto_edificio_id').val();
                                }
                            },
                            "columns": [
                                {data: 'fila', name: 'fila', "width": "3%", "searchable": false, "orderable": false}
                                , {data: 'fecha_observacion', name: 'fecha_observacion', "searchable": false}
                                , {data: 'descripcion', name: 'descripcion', "searchable": false}
                                , {data: 'usuario', name: 'usuario', "searchable": false}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[1, 'desc']],
                            "pageLength": 6,
                        });
            } else {
                dt_proyectoBitacora.ajax.reload();
            }
        },
        cargarProvisionales: function () {
            if (typeof dt_proyectoProvisional === 'undefined') {
                dt_proyectoProvisional = $('#t_movimiento_provisional')
                        .DataTable({
                            "ajax": {
                                url: route_g + "listar-provisionales",
                                type: "POST",
                                dataSrc: 'datos',
                                data: function (d) {
                                    d.proyecto_edificio_id = $('#txt_proyecto_edificio_id').val();
                                    d.proyecto_estado = $("input[name=proyecto_estado_temp]").val();
                                    d.parent = $("#form_proyecto_provisional input[name=proyecto_provisional_parent_temp]").val();
                                }
                            },
                            "columns": [
                                {data: 'fila', width: 'auto'}
                                , {data: 'estado_proyecto', width: '180'}
                                , {data: 'fecentrega', width: '180'}
                                , {data: 'fecrespuesta', width: '180'}
                                , {data: 'fectermino', width: '180'}
                                , {data: 'fecliquidacion', width: '180'}
                                , {data: 'ejecutor_proyecto', width: '180'}
                                , {data: 'estado_provisional', width: '180'}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[0, 'desc']],
                            "pageLength": 10,
                            "createdRow": function (row, data, dataIndex) {
                                data['fecentrega'] = moment(data['fecentrega']).format("YYYY-MM-DD");
                                data['fecrespuesta'] = moment(data['fecrespuesta']).format("YYYY-MM-DD");
                                data['fecliquidacion'] = moment(data['fecliquidacion']).format("YYYY-MM-DD");
                                data['fectermino'] = moment(data['fectermino']).format("YYYY-MM-DD");
                                $('td', row).eq(2).html(data['fecentrega']);
                                $('td', row).eq(3).html(data['fecrespuesta']);
                                $('td', row).eq(4).html(data['fectermino']);
                                $('td', row).eq(5).html(data['fecliquidacion']);
                            },
                        });
            } else {
                dt_proyectoProvisional.ajax.reload();
            }
        },
        cargarMegaProyecto: function () {
            if (typeof dt_proyectoMega === 'undefined') {
                dt_proyectoMega = $('#t_movimiento_megaproyecto')
                        .on('order.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('search.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('page.dt', function () {
                            eventoCargaMostrar();
                        })
                        .DataTable({
                            "ajax": {
                                url: route_g + "listar-mega-proyecto",
                                type: "POST",
                                dataSrc: 'datos',
                                data: function (d) {
                                    d.proyecto_edificio_id = $('#txt_proyecto_edificio_id').val();
                                    d.parent = $("#form_proyecto_mega input[name=mega_proyecto_parent_temp]").val();
                                }
                            },
                            "columns": [
                                {data: 'fila', width: 'auto'}
                                , {data: 'energia_estado', width: '180'}
                                , {data: 'fo_estado', width: '180'}
                                , {data: 'troba_nueva', width: '180'}
                                , {data: 'troba_fecha_fin', width: '180'}
                                , {data: 'troba_estado', width: '180'}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[0, 'desc']],
                            "pageLength": 10,
                            "createdRow": function (row, data, dataIndex) {

                                /*data['fecentrega'] = moment(data['fecentrega']).format("YYYY-MM-DD");
                                 data['fecrespuesta'] = moment(data['fecrespuesta']).format("YYYY-MM-DD");
                                 data['fecliquidacion'] = moment(data['fecliquidacion']).format("YYYY-MM-DD");
                                 data['fectermino'] = moment(data['fectermino']).format("YYYY-MM-DD");
                                 $('td', row).eq(2).html(data['fecentrega']);
                                 $('td', row).eq(3).html(data['fecrespuesta']);
                                 $('td', row).eq(4).html(data['fectermino']);
                                 $('td', row).eq(5).html(data['fecliquidacion']);
                                 */
                                //$(row).addClass('selected-' + data['proyecto_tipo_alias']);
//                                if (data['proyecto_tipo_alias'] == "di") {
//                                    $(row).addClass('selected-green');
//                                }
                            },
                            drawCallback: function (settings) {
                                eventoCargaRemover();
                            }
                        });
            } else {
                dt_proyectoMega.ajax.reload();
            }
        },
        cargarPapelera: function () {
            if (typeof dt_proyectoPapelera === 'undefined') {
                dt_proyectoPapelera = $('#datatable_proyectoedificio_papelera')
                        .DataTable({
                            "ajax": {
                                url: route_g + "listar-papelera",
                                type: "POST",
                                dataSrc: 'datos'
                            },
                            "columns": [
                                {data: 'id', name: 'id', 'class': 'colunm-index'}
                                , {data: 'ida', name: 'ida', searchable: false, visible: false}
                                //, {data: 'codigo_proyecto', name: 'codigo_proyecto'}
                                , {data: 'nombre_proyecto', name: 'nombre_proyecto'}
                                , {data: 'segmento', name: 'segmento'}
                                , {data: 'tipo_proyecto', name: 'tipo_proyecto'}
                                , {data: 'distrito', name: 'distrito'}
                                , {data: 'direccion', name: 'direccion'}
                                , {data: 'numero_departamentos', name: 'numero_departamentos', orderable: false, searchable: false}
                                , {data: 'avance', name: 'avance', "width": "6%"}
                                , {data: 'fecha_termino', name: 'fecha_termino', orderable: false, searchable: false}
                                , {data: 'proyecto_estado', name: 'proyecto_estado'}
                                , {data: 'action', name: 'action', "width": "10%", 'class': 'colunm-acciones', orderable: false, searchable: false, className: 'text-center'}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[1, 'desc']],
                            "pageLength": 8,
                            initComplete: function (settings, json) {
                                this.api().columns().eq(0).each(function (index) {
                                    var column = this.column(index);
                                    var ids = this.column(0).data();
                                    if (index == 11) {
                                        // header
                                        var a = document.createElement('a');
                                        $(a).addClass('btn btn-success')
                                            .html('<i class="fa fa-download fa-lg"></i>')
                                            .attr('href', '#')
                                            .attr('url', 'reporte/proyectoexcel')
                                            .attr('id', 'btnProyectoexcelPapelera')
                                            .appendTo($(column.header()).empty());
                                        // button A
                                        var a = document.createElement('a');
                                        $(a)
                                            .addClass('btn') /* btn-info */
                                            .html(' - ') /* <i class="fa fa-thumbs-o-up"></i> */
                                            .attr('href', '#')
                                            .appendTo($(column.nodes()).empty());
                                    }
                                });
                            },
                        });
            } else {
                dt_proyectoPapelera.ajax.reload();
            }
        },
        // VALIDACION_PROYECTO_EDIFICIO
        validaProvisionalAcceso: function () {
            var provisionalacceso = functionajax(route + 'provisional-acceso', 'POST', false, 'json', false, null);
            provisionalacceso.success(function (obj) {
                if (obj.rst == 1) {
                    if (obj.datos.length > 0) {

                    } else {
                        $("fieldset#fieldset_proyecto_provisional").prop("disabled", true);
                    }
                }
            });
        },
        validaMegaAcceso: function () {
            var megaacceso = functionajax(route + 'mega-acceso', 'POST', false, 'json', false, null);
            megaacceso.success(function (obj) {
                if (obj.rst == 1) {
                    if (obj.datos.length > 0) {

                    } else {
                        $("fieldset#fieldset_proyecto_mega").prop("disabled", true);
                    }
                }
            });
        },
        // MANTENIMIENTO_PROYECTO_EDIFICIO
        cargarSegmento: function () {
            $.ajax({
                url: 'segmento/listar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    var htmlCbo = "";
                    if (obj.rst == 1) {
                        htmlCbo += "<option value='0'>.: Seleccione :.</option>";
                        $.each(obj.datos, function (index, data) {
                            htmlCbo += '<option value="' + data.nombre.toUpperCase() + '">' + data.nombre.toUpperCase() + '</option>';
                        });
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                    $(" #slct_segmentos").html(htmlCbo);
                }
            });
        },
        cargarProyectoTipo: function () {
            $.ajax({
                url: 'proyectotipo/cargarproyectotipo',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    var htmlCbo = "";
                    if (obj.rst == 1) {
                        htmlCbo += "<option value='0'>.: Seleccione :.</option>";
                        $.each(obj.datos, function (index, data) {
                            htmlCbo += '<option value="' + data.nombre + '">' + data.nombre + '</option>';
                        })
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                    $(" #slct_tipo_proyecto").html(htmlCbo);
                }
            });
        },
        cargarTipoInfraestructura: function () {
            $.ajax({
                url: 'tipoinfraestructura/listar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    var htmlCbo = "";
                    if (obj.rst == 1) {
                        htmlCbo += "<option value='0'>.: Seleccione :.</option>";
                        $.each(obj.datos, function (index, data) {
                            htmlCbo += '<option value="' + data.nombre + '">' + data.nombre + '</option>';
                        });
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                    $(" #slct_tipo_infraestructura").html(htmlCbo);
                }
            });
        },
        // PROYECTO_EDIFICIO
        verProyectoEdificio: function (id) {
            $.ajax({
                url: route + 'ver',
                type: 'POST',
                data: 'id=' + id,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        form = '';
                        dato = obj.datos;
                        $(form + ' #txt_id').val(dato.id);
                        $(form + ' #txt_id_actual').val(dato.id);
                        //$(form + ' #txt_id').prop('disabled', false);
                        $(form + ' #txt_departamento,' + form + '#txt_provincia,' + form + '#txt_distrito').attr('readonly', false);
                        $(form + ' #txt_padre').val(dato.parent);
                        $(form + ' #slct_segmentos').multiselect('select', dato["segmento"].toUpperCase());
                        //$(form + ' #slct_segmentos').val(dato["segmento"].toUpperCase());
                        //$("select option[value='']").attr("selected","selected");
                        if (dato["segmento"] == null || dato["segmento"] == undefined || dato["segmento"] == "") {
                            $(form + ' #slct_segmentos').multiselect('select', ''); /*$(form + ' #slct_segmentos').val('0'); */
                        }

                        $(form + ' #slct_tipo_proyectos').multiselect('select', dato["tipo_proyecto"].toUpperCase());
                        //$(form + ' #slct_tipo_proyectos').val(dato["tipo_proyecto"].toUpperCase());
                        if (dato["tipo_proyecto"] == null || dato["tipo_proyecto"] == undefined || dato["tipo_proyecto"] == "") {
                            $(form + ' #slct_tipo_proyectos').multiselect('select', ''); /* $(form + ' #slct_tipo_proyectos').multiselect('select', ''); */
                        }
                        $(form + ' #slct_tipo_cchh').val(dato.tipo_cchh);
                        $(form + ' #txt_cchh').val(dato.cchh);
                        $(form + ' #slct_tipo_via').val(dato.tipo_via);
                        $(form + ' #txt_direccion').val(dato.direccion);
                        $(form + ' #txt_manzana').val(dato.manzana);
                        $(form + ' #txt_numero').val(dato.numero);
                        $(form + ' #txt_lote').val(dato.lote);
                        $(form + ' #txt_nombre_proyecto').val(dato.nombre_proyecto);
                        $(form + ' #txt_numero_block').val(dato.numero_block);
                        $(form + ' #txt_numero_pisos').val(dato.numero_piso);
                        $(form + ' #txt_numero_departamentos').val(dato.numero_departamentos);
                        $(form + ' #txt_numero_departamentos_habitados').val(dato.numero_departamentos_habitados);
                        $(form + ' #txt_avance').val(dato.avance);
                        $(form + ' #txt_fecha_termino').val(dato.fecha_termino);
                        if (dato["tipo_infraestructura"] == null || dato["tipo_infraestructura"] == undefined || dato["tipo_infraestructura"] == "") {
                            $(form + ' #slct_tipo_infraestructura').val('0');
                        } else {
                            $(form + ' #slct_tipo_infraestructura').val(dato["tipo_infraestructura"].toUpperCase());    
                        }
                        $(form + ' #txt_seguimiento').val(dato.seguimiento);
                        $(form + ' #txt_observacion').val(dato.observacion);
                        $(form + ' #txt_nombre_constructora').val(dato.nombre_constructora);
                        $(form + ' #txt_ruc_constructora').val(dato.ruc_constructora);
                        $(form + ' #txt_persona_contacto').val(dato.persona_contacto);
                        $(form + ' #txt_pagina_web').val(dato.pagina_web);
                        $(form + ' #txt_mdf').val(dato.mdf_gis);
                        $(form + ' #txt_ura').val(dato.ura_gis);
                        $(form + ' #txt_armario').val(dato.armario);
                        $(form + ' #txt_troba_gis').val(dato.troba_gis);
                        $(form + ' #txt_troba_bidireccional').val(dato.troba_bidireccional);
                        $(form + ' #txt_empresa_colaboradora').val(dato.eecc_gis);
                        $(form + ' #txt_zona_gis').val(dato.zona_gis);
                        $(form + ' #txt_coord_x').val(dato.coord_x);
                        $(form + ' #txt_coord_y').val(dato.coord_y);
                        $(form + ' #txt_departamento').val(dato.departamento);
                        $(form + ' #txt_provincia').val(dato.provincia);
                        $(form + ' #txt_distrito').val(dato.distrito);
                        $(form + ' #txt_proyecto_estado').val(dato.proyecto_estado);
                        $(form + ' #txt_fecha_registro').val(dato.created_at);
                        $(form + ' #txt_fecha_ultimo').val(dato.updated_at);
                        $(form + ' #txt_subitem').val(dato.subitem);
                        $('.listaSubItem li').parents('.input-group-btn').find('i').html(dato.subitem);
                        $(form + ' #txt_competencia_uno').val(dato["zona_competencia"]);
                        $(form + ' #txt_ticket').val(dato.ticket);
                        $(form + ' #txt_fuente_identificacion').val(dato.fuente_identificacion);
                        //
                        $(form + ' #txt_uno_email').val(dato["pagina_web"]);
                        $(form + ' #txt_uno_contacto').val(dato["persona_contacto"]);
                        var telUno;
                        if (dato["persona_contacto_cel"] != null && dato["persona_contacto_cel"].length > 0) {
                            telUno = dato["persona_contacto_cel"].split('|');
                            $(form + ' #txt_uno_telefono_a').val(telUno[0] != undefined ? telUno[0] : '');
                            $(form + ' #txt_uno_telefono_b').val(telUno[1] != undefined ? telUno[1] : '');
                        }
                        //
                        $(form + ' #txt_dos_paginaweb').val(dato["pagina_web_dos"]);
                        $(form + ' #txt_dos_contacto').val(dato["persona_contactodos"]);
                        $(form + ' #txt_usuario_ultimo').val(dato["usuario_updated_at"]);
                        $(form + ' #txt_usuario_inicial').val(dato["usuario_created_at"]);
                        var telDos;
                        if (dato["persona_contactodos_cel"] != null && dato["persona_contactodos_cel"].length > 0) {
                            telDos = dato["persona_contactodos_cel"].split('|');
                            $(form + ' #txt_dos_telefono_a').val(telDos[0] != undefined ? telDos[0] : '');
                            $(form + ' #txt_dos_telefono_b').val(telDos[1] != undefined ? telDos[1] : '');
                        }
                        
                        mostrarImagenCargada(dato["foto_uno"], 'txt_captura_1', dato.id);
                        mostrarImagenCargada(dato["foto_dos"], 'txt_captura_2', dato.id);
                        mostrarImagenCargada(dato["foto_tres"], 'txt_captura_3', dato.id);
                    }
                    eventoCargaRemover();
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        editarProyectoEdificio: function () {
            var datos = $("#form_proyectoEdificio").serialize().replace(/txt_|slct_/g, '');
            //
            $.ajax({
                url: 'proyecto_edificio/editar',
                type: 'POST',
                data: datos,
                cache: false,
                dataType: 'json',
                success: function (obj) {
                    if (obj.rst == 1) {
                        $("#proyectoEdificioMantenimientoModal").modal('hide');
                        Psi.mensaje('success', obj.msj, 10000);
                    } else if (obj.rst == 2) {
                        datosTodos = '';
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                            datosTodos += datos + '. ';
                        });
                        Psi.mensaje('warning', datosTodos, 8000);
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                }
            });
        },
        crearProyectoEdificio: function () {
            var datos = $("#form_proyectoEdificio").serialize().replace(/txt_|slct_/g, ''); // .serialize().split("txt_").join("").split("slct_").join("");
            $.ajax({
                url: 'proyecto_edificio/crear',
                type: 'POST',
                data: datos,
                cache: false,
                dataType: 'json',
//                beforeSend: function () {
//                    eventoCargaMostrar();
//                },
                success: function (obj) {
//                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        $("#proyectoEdificioMantenimientoModal").modal('hide');
                        Psi.mensaje('success', obj.msj, 10000);
                    } else if (obj.rst == 2) {
                        datosTodos = '';
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                            datosTodos += datos + '. ';
                        });
                        Psi.mensaje('info', datosTodos, 8000);
                    } else {
                        Psi.mensaje('error', obj.msj, 6000);
                    }
                }
//                ,error: function () {
//                    eventoCargaRemover();
//                    Psi.mensaje('danger', '', 6000);
//                }
            });
        },
        eliminarProyectoEdificio: function () {
            var datos = $("#form_proyectoEdificioEliminar").serialize().replace(/txt_|slct_/g, '');
            $.ajax({
                url: 'proyecto_edificio/eliminar',
                type: 'POST',
                data: datos,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    Psi.mensaje('success', obj.msj, 6000);
                    eventoCargaRemover();
                }
            });
        },
        eliminarProyectoEdificioImagen: function (id, etiqueta, idTag, evento) {
            $.ajax({
                url: 'proyecto_edificio/eliminar-imagen',
                type: 'POST',
                data: 'id=' + id + '&label=' + etiqueta,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst && obj.rst == 1) {
                        obj.id = idTag;
                        evento(obj);
                    }
                    Psi.mensaje( (obj.rst != 1? 'warning' : 'success') , obj.msj, 6000);
                    eventoCargaRemover();
                }
            });
        },
        validarIdProyectoEdificio: function () {
            if ($('#txt_id').val().length > 0) {
                $.ajax({
                    url: route + 'validar-id',
                    type: 'POST',
                    data: 'id=' + $('#txt_id').val() + '&subitem=' + $('#txt_subitem').val(),
                    cache: false,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#validate-id').removeClass('fa-star').addClass('fa-spinner fa-spin');
                    },
                    success: function (obj) {
                        $('#validate-id').removeClass('fa-spinner fa-spin').addClass('fa-star');
                        if (obj.datos == true) {
                            $('#validate-id-text').removeClass('text-red').addClass('text-success').html('Disponible');
                        } else {
                            $('#validate-id-text').removeClass('text-success').addClass('text-red').html('Ya Existe');
                        }
                    }
                });
            }
        },
        subirImagenProyectoEdificio: function (data) {
            $.ajax({
                url: route + "procesar-imagen",
                type: "POST",
                data: data, // new FormData($("#form_proyectoEdificio")[0]),
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        mostrarImagenCargada(obj.archivo, obj.input, obj.id);
                        eventoCargaRemover();
                        Psi.mensaje('success', obj.msj, 10000);
                    } else {
                        eventoCargaRemover();
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                }
            });
        },
        enlazarPadreEdificio: function (id) {
            var dato = $('#txt_padre').val();
            var subitem = $('#txt_subitem').val();
            if (dato.length == 0) {
                Psi.mensaje('info', 'Debe ingresar un ID PADRE', 6000);
                return false;
            }
            $.ajax({
                url: route + 'ver',
                type: 'POST',
                data: 'id=' + id + '&padre=' + dato + '&subitem=' + subitem,
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("#error_itempadre").after('<i class="beforeSendLabel fa fa-spinner fa-spin fa-1x fa-fw"></i><span class="sr-only">Loading...</span>');
                },
                success: function (obj) {
                    if (obj.rst == 1) {
                        dato = obj.datos;
                        $('#validate-id-text')
                                .removeClass('text-red').addClass('text-blue')
                                .css('display', 'inline-block').html(
                                    '<small>DATOS COPIADO DE: (' + dato.departamento + ') '
                                    + dato.nombre_proyecto + '</small>'
                                );
                        var form = '#form_proyectoEdificio ';
                        $(form + '#txt_departamento').val(dato.departamento);
                        $(form + '#txt_provincia').val(dato.provincia);
                        $(form + '#txt_distrito').val(dato.distrito);
                        $(form + '#txt_coord_x').val(dato.coord_x);
                        $(form + '#txt_coord_y').val(dato.coord_y);
                        marcador = Psigeo.marker(mapa_para_registro, dato.coord_y, dato.coord_x);
                        obtenerDireccionGoogleMap(dato.coord_y, dato.coord_x, mapa_para_registro, false);
                        ProyectoEdificio.edificiosUbicacion(dato.coord_y, dato.coord_x, 200, mapa_para_registro);
                        //
                        if (dato.segmento == null || dato.segmento === undefined || dato.segmento == "") {
                            $(form + ' #slct_segmentos').multiselect('select', '');
                        } else {
                            $(form + ' #slct_segmentos').multiselect('select', (dato.segmento).toUpperCase());
                        }
                        setTimeout(function () {
                            if (dato.tipo_proyecto == null || dato.tipo_proyecto === undefined || dato.tipo_proyecto == "") {
                                $(form + ' #slct_tipo_proyectos').multiselect('select', '');
                            } else {
                                $(form + ' #slct_tipo_proyectos').multiselect('select', (dato.tipo_proyecto).toUpperCase());
                            }
                        }, 2000);
                        //
                        $(form + '#slct_tipo_cchh').val(dato.tipo_cchh.toUpperCase());
                        $(form + '#slct_tipo_via').val(dato.tipo_via.toUpperCase());
                        $(form + '#txt_manzana').val(dato.manzana);
                        $(form + '#txt_lote').val(dato.lote);
                        $(form + '#txt_cchh').val(dato.cchh);
                        $(form + '#txt_direccion').val(dato.direccion);
                        $(form + '#txt_numero').val(dato.numero);
                        $(form + '#txt_nombre_proyecto').val(dato.nombre_proyecto);
                        if(dato.tipo_infraestructura != null) {
                            $(form + '#slct_tipo_infraestructura').val(dato.tipo_infraestructura.toUpperCase());    
                        }
                        $(form + '#txt_nombre_constructora').val(dato.nombre_constructora);
                        $(form + '#txt_ruc_constructora').val(dato.ruc_constructora);
                        //
                        $(form + ' #txt_uno_email').val(dato.pagina_web);
                        $(form + ' #txt_uno_contacto').val(dato.persona_contacto);
                        var telUno;
                        if (dato.persona_contacto_cel != null && (dato.persona_contacto_cel).length > 0) {
                            telUno = (dato.persona_contacto_cel).split('|');
                            $(form + ' #txt_uno_telefono_a').val(telUno[0] != undefined ? telUno[0] : '');
                            $(form + ' #txt_uno_telefono_b').val(telUno[1] != undefined ? telUno[1] : '');
                        }
                        $(form + ' #txt_dos_paginaweb').val(dato.pagina_web_dos);
                        $(form + ' #txt_dos_contacto').val(dato.persona_contactodos);
                        var telDos;
                        if (dato.persona_contactodos_cel != null && (dato.persona_contactodos_cel).length > 0) {
                            telDos = (dato.persona_contactodos_cel).split('|');
                            $(form + ' #txt_dos_telefono_a').val(telDos[0] != undefined ? telDos[0] : '');
                            $(form + ' #txt_dos_telefono_b').val(telDos[1] != undefined ? telDos[1] : '');
                        }

                    } else {
                        Psi.mensaje('info', obj.msj, 6000);
                    }
                    $('.beforeSendLabel').remove();
                }
            });
        },
        // DEPRECATED
        // ========================================================
        cargarGestionesDos: function () {
            /* DEPRECATED */
            if (typeof dt_proyectoGestion === 'undefined') {
                dt_proyectoGestion = $('#t_movimiento')
                        .on('order.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('search.dt', function () {
                            eventoCargaMostrar();
                        })
                        .on('page.dt', function () {
                            eventoCargaMostrar();
                        })
                        .DataTable({
                            "ajax": {
                                url: route + "listargestiones",
                                type: "POST",
                                dataSrc: 'datos',
                                data: function (d) {
                                    d.proyecto_edificio_id = $('#txt_proyecto_edificio_id').val();
                                }
                            },
                            "columns": [
                                {
                                    "className": 'details-control',
                                    "orderable": false,
                                    "data": null,
                                    "defaultContent": ''
                                },
                                {data: 'fila', name: 'fila', "width": "3%"}
                                , {data: 'created_at', name: 'created_at'}
                                , {data: 'avance', name: 'avance'}
                                , {data: 'usuario', name: 'usuario'}
                                , {data: 'observacion', name: 'observacion'}
                                , {data: 'proyecto_tipo', name: 'proyecto_tipo'}
                            ],
                            lengthChange: false,
                            searching: false,
                            "ordering": false,
                            "order": [[1, 'desc']],
                            "pageLength": 10,
                            drawCallback: function (settings) {
                                eventoCargaRemover();
                            }
                        });
            } else {
                dt_proyectoGestion.ajax.reload();
            }
        },
    };
</script>
