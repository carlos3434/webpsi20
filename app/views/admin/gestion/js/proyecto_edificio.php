<script type="text/javascript">
    var coord_x = -77.016269, coord_y = -12.109121;
    var marcador, map, edificios = [], map_address = [];
    var tipo_actual = 0;
    var dataEdificio = [];
    var geocoder, dataEdificioId;

    $(document).ready(function () {
        geocoder = new google.maps.Geocoder;
        dt_proyecto_edificios = ProyectoEdificio.cargarDos(); // dt_proyecto_edificios = ProyectoEdificio.cargar();

        $(id_modal_proy).on('hide.bs.modal', function (event) {
            $("#form_proyecto_iniciativa, #form_proyecto_diseno, #form_proyecto_licencia, #form_proyecto_oocc, #form_proyecto_cableado, #form_proyecto_megas").hide();
            // Reconstruir combos
            // $('#slct_gcasuistica').multiselect('destroy');
            dt_proyecto_edificios.draw();
            $('#txt_proyecto_estado_id').val('');
            $('#form_proyecto_mega :input').val('');
            $('.form-proyecto-tipo :input, #form_proyecto_provisional :input').val('');
            //$("#btnAccionMega").click();
        });

        $(id_modal_proy).on('show.bs.modal', function (event) {
            validaPestanas();
            $('#txt_proyecto_edificio_id').val(dataEdificio.id);
            // $('#lbl_direccion').html(dataEdificio.direccion);

            ProyectoEdificio.ver(dataEdificio.id);
            ProyectoEdificio.cargarGestionesTotal();
            ProyectoEdificio.cargarBitacoraDos();
            $('#slct_gcasuistica_modal').multiselect('deselect', ['1', '2', '3', '6', '4']);
        });

        $(id_modal_proy).on('hidden.bs.modal', function (event) {
            dataEdificio = [];
        });
// -- DESACTIVADO PROVISIONAL --
        $('#form_gestion_filtro').on('submit', function (e) {
            if ($("input[name=tipo_accion]").val() == "reporteexcel") {

            } else if ($("input[name=tipo_accion]").val() == "busqueda") {
                dt_proyecto_edificios.draw();
                $('#form_gestion_filtro input').each(function (index, value) {
                    var name = $(this).attr("name");
                    var valor = $(this).val();

                    switch (name) {
                        case 'txt_buscar':
                            dt_proyectoGestionFiltros.txt_buscar = {};
                            dt_proyectoGestionFiltros.txt_buscar.valor = valor;
                            dt_proyectoGestionFiltros.txt_buscar.titulo = "Valor a Buscar";
                            break;
                        case 'txt_buscarPor':
                            dt_proyectoGestionFiltros.txt_buscarPor = {};
                            switch ($(this).val()) {
                                case 'id':
                                    dt_proyectoGestionFiltros.txt_buscarPor.valor = "Item ";
                                    dt_proyectoGestionFiltros.txt_buscarPor.titulo = "Búsqueda por ";
                                    break;
                                case 'codigo_proyecto':
                                    dt_proyectoGestionFiltros.txt_buscarPor.valor = "OT ";
                                    dt_proyectoGestionFiltros.txt_buscarPor.titulo = "Búsqueda por ";
                                    break;
                                case 'direccion':
                                    dt_proyectoGestionFiltros.txt_buscarPor.valor = "Dirección ";
                                    dt_proyectoGestionFiltros.txt_buscarPor.titulo = "Búsqueda por ";
                                    break;
                            }
                            break;
                        case 'txt_rangofecha':
                            dt_proyectoGestionFiltros.txt_rangofecha = {};
                            dt_proyectoGestionFiltros.txt_rangofecha.valor = valor;
                            dt_proyectoGestionFiltros.txt_rangofecha.titulo = "Rango de Fecha de Búsqueda ";
                            break;
                        case 'fecha_created_at':
                            dt_proyectoGestionFiltros.fecha_created_at = {};
                            dt_proyectoGestionFiltros.fecha_created_at.valor = valor;
                            dt_proyectoGestionFiltros.fecha_created_at.titulo = "Rango de Fecha de Búsqueda ";
                            break;
                    }
                });
                var bitacorabusqueda = functionajax(route + 'agregar-bitacora-busqueda', 'POST', false, 'json', false, dt_proyectoGestionFiltros);
                bitacorabusqueda.success(function (datos) {
                });
                e.preventDefault();
            }
        });

        $('#btn_bitacora').on('click', function (e) {
            e.preventDefault();
            guardarBitacora($('#txt_proyecto_edificio_id').val());
        });

        $(id_table_proy + ' tbody').on('click', 'a', function () {
            var tr = $(this).closest('tr');
            dataEdificio = dt_proyecto_edificios.row(tr).data();
        });

        $('.listaABuscar li').click(function () {
            var labelSelected = asignarLabelPorDropdown(this);
            $('#txt_buscarPor').val(labelSelected);    
            if (texto == 'DIRECCION') {
                $('#txt_buscar').attr('placeholder', '<?php echo trans('gestion.direccion') ?>');
                $('#txt_direccion_n').css('display', 'block');
            } else {
                $('#txt_buscar').removeAttr('placeholder');
                $('#txt_direccion_n').val('').css('display', 'none');
            }
        });

        $("#btn_gestion_modal").click(guardarGestion);

        $('#btn_aplicar_xy').click(guardarUbicacion);

        var fechas =
                // '#form_proyecto_diseno #txt_fecha_inicio,' +
                '#form_proyecto_diseno #txt_fecha_compromiso,' +
                '#form_proyecto_diseno #txt_fecha_gestion,' +
                '#form_proyecto_diseno #txt_fecha_termino,' +
                // '#form_proyecto_licencia #txt_fecha_inicio,' +
                '#form_proyecto_licencia #txt_recepcion_expediente,' +
                '#form_proyecto_licencia #txt_fecha_compromiso,' +
                '#form_proyecto_licencia #txt_fecha_termino,' +
                // '#form_proyecto_oocc #txt_fecha_inicio,' +
                '#form_proyecto_oocc #txt_fecha_compromiso,' +
                '#form_proyecto_oocc #txt_fecha_termino,' +
                // '#form_proyecto_cableado #txt_fecha_inicio,' +
                '#form_proyecto_cableado #txt_fecha_compromiso,' +
                '#form_proyecto_cableado #txt_fecha_termino,' +
                //'#form_proyecto_cableado #txt_fecha_teorica_final,' +
                '#form_proyecto_provisional #txt_fecentrega,' +
                '#form_proyecto_provisional #txt_fecrespuesta,' +
                '#form_proyecto_provisional #txt_fectermino,' +
                '#form_proyecto_provisional #txt_fecliquidacion,' +
                '#form_proyecto_mega #txt_energia_estado,' +
                '#form_proyecto_mega #txt_fo_estado,' +
                '#form_proyecto_mega #txt_troba_nueva,' +
                '#form_proyecto_mega #txt_fecha_fin,' +
                '#form_proyecto_mega #txt_troba_estado,' +
                '#form_proyecto_iniciativa #txt_fecha_termino,' +
                '#form_proyecto_iniciativa #txt_fecha_seguimiento,' +
                '#txt_fecha_observacion';

        $(fechas).daterangepicker({
            singleDatePicker: true,
            format: 'YYYY-MM-DD'
        });
        $('#fecha_created_at').daterangepicker({
            format: 'YYYY-MM-DD',
            locale: {
                cancelLabel: 'Limpiar'
            }
        });
        $('#fecha_created_at').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        slctGlobal.listarSlct('segmento', 'slct_segmento', 'multiple');
        slctGlobal.listarSlct('tipoproyecto', 'slct_tipoproyecto', 'multiple');
        slctGlobal.listarSlct('proyectoestado', 'slct_estadoproyecto', 'multiple', null);
        slctGlobalHtml('slct_tipo', 'simple');
        usuario_id = '<?=Auth::id();?>';
        slctGlobal.listarSlct('gcasuistica', 'slct_gcasuistica_modal', 'simple', null, {usuario_id: usuario_id}, 0, '#slct_gcasuistica_detalle_modal,#slct_estado_modal', 'M', 0, 0, {change: ChangeGrupoQuiebre});
        slctGlobal.listarSlctpost('gcasuistica', 'listardetalle', 'slct_gcasuistica_detalle_modal', 'simple', null, null, 1, '#slct_estado_modal', 'S', 'slct_gcasuistica_modal', 'M');

        $('input[name=es_provisional]').on('ifChecked', function (event) {
            $("form[name=form_proyecto_provisional]").fadeIn("slow");
        });
        $('input[name=es_provisional]').on('ifUnchecked', function (event) {
            $("form[name=form_proyecto_provisional]").fadeOut("slow");
        });
        $('#descarga_checkFiltro').on('ifUnchecked', function (event) {
            $('#descarga_textFiltro').val(0);
        });
        $('#descarga_checkFiltro').on('ifChecked', function (event) {
            $('#descarga_textFiltro').val(1);
        });

        $("#form_proyecto_provisional").on("submit", function (e) {
            guardarProvisional();
            return false;
        });

        $("#form_proyecto_mega").on("submit", function (e) {
            //guardarProvisional();
            guardarMega();
            return false;
        });

        $(document).delegate("#btnProyectoexcel", "click", function (e) {
            $("input[name=tipo_accion   ]").val("reporteexcel");
            $("#form_gestion_filtro").attr("action", $(this).attr('url'));
            $("#form_gestion_filtro").submit();
            $("#form_gestion_filtro").removeAttr("action");
            $("input[name=tipo_accion]").val("busqueda");
        });
        
        $(document).delegate("#btnProyectoexcelPapelera", "click", function (e) {
            $("input[name=tipo_accion]").val("reporteexcelpapelera");
            $("#form_gestion_filtro").attr("action", $(this).attr('url'));
            $("#form_gestion_filtro").submit();
            $("#form_gestion_filtro").removeAttr("action");
            $("input[name=tipo_accion]").val("busqueda");
        });

        /*  // Add event listener for opening and closing details
         // Agrega un icono al inicio de la fila y despliega detalle
         $('#t_movimiento tbody').on('click', 'td.details-control', function () {
         var tr = $(this).closest('tr');
         var row = dt_proyectoGestion.row(tr);
         if (row.child.isShown()) {
         // This row is already open - close it
         row.child.hide();
         tr.removeClass('shown');
         } else {
         // Open this row
         row.child(format(row.data())).show();
         tr.addClass('shown');
         }
         });
         
         slctGlobal.listarSlctpost('gcasuistica', 'listarestado', 'slct_estado_modal', 'simple', null, null, 1);
         
         $('#btn_filtro').on('click', function (e) {
         //PG = 'G';
         dt_proyecto_edificios.draw();
         e.preventDefault();
         });
         
         $('#btn_personalizado').on('click', function (e) {
         PG = 'P';
         dt_proyecto_edificios.draw();
         e.preventDefault();
         });
         
         // se ejecuta antes muestre
         $(fechas).on('show.daterangepicker', function (ev, picker) {
         if ($(this).is('[readonly]')) {
         } else {
         }
         });
         */
    });

    asignarLabelPorDropdown = function (object) {
        texto = $(object).text();
        $(object).parents('.input-group-btn').find('i').html(texto);
        return $(object).find('a').attr('itemid');
        //$('#txt_buscarPor').val($(this).find('a').attr('itemid'));
    };

    obtenerCoordenadasGoogleMap = function (map, completar) {
        var address = document.getElementById('geo_direccion').value;
        geocoder.geocode(
                {
                    'address': address,
                    componentRestrictions: {
                        country: 'PE'
                    }
                }
        , function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                SetXY(results[0].geometry.location.lat, results[0].geometry.location.lng);
                map.setCenter(results[0].geometry.location);
                marcador.setMap(null);
                marcador = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
                map_address = [];
                cargarMapaInfoMarcador(results[0].formatted_address, map);
                //console.log(map_address);
                //console.log(results);
                rellenarInputRegistroUbicacion(map_address, results, completar);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    };
    obtenerDireccionGoogleMap = function (lat, lng, map, completar) {
        var latlng = new google.maps.LatLng(lat, lng);
        // This is making the Geocode request
        // var geocoder = new google.maps.Geocoder();
        SetXY(lat, lng);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status !== google.maps.GeocoderStatus.OK) {
                alert(status);
            } else if (status == google.maps.GeocoderStatus.OK) {
                map_address = [];
                map.setCenter(results[0].geometry.location);
                cargarMapaInfoMarcador(results[0].formatted_address, map);
                //console.log(map_address);
                rellenarInputRegistroUbicacion(map_address, results, completar);
            }
        });
    };
    cargarMapaInfoMarcador = function (content, map) {
        var infowindow = new google.maps.InfoWindow;
        infowindow.setContent(content);
        infowindow.open(map, marcador);
    };
    cargarMapaEventoPosicionMarcador = function (map) {
        google.maps.event.addListener(map, 'click', function (evento) {
            var latitud = evento.latLng.lat();
            var longitud = evento.latLng.lng();
            if (marcador != undefined) {
                marcador.setMap(null);
            }
            SetXY(latitud, longitud);
            var pos = new google.maps.LatLng(latitud, longitud);
            // Agregar marcador draggable
            marcador = new google.maps.Marker({
                draggable: true,
                position: pos,
                map: map
            });
            ProyectoEdificio.edificiosUbicacion(latitud, longitud, 200, map);
            obtenerDireccionGoogleMap(latitud, longitud, map, true);
            //Psigeo.geoStreetView(latitud, longitud, 'pano');
            //marker.setPosition(pos);
            google.maps.event.addListener(marcador, 'dragend', function () {
                pos2 = marcador.getPosition();
                map2 = marcador.getMap();
                SetXY(pos2.lat(), pos2.lng());
                obtenerDireccionGoogleMap(pos2.lat(), pos2.lng(), map2, true);
                //Psigeo.geoStreetView(pos2.lat(), pos2.lng(), 'pano');
            });
        });
    };
    cargarMapa = function (x, y) {
        //mapa
        if (y.length < 1 || x.length < 1) {
            x = -77.016269;
            y = -12.109121;
        }
        var myLatlng = new google.maps.LatLng(y, x);
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        //marcador
//        marker = new google.maps.Marker({
//            draggable: true,
//            position: new google.maps.LatLng(y, x),
//            map: map,
//        });
        //
        SetXY(y, x);
        // eventos
        Psigeo.geoStreetView(y, x, 'pano');
        //
        google.maps.event.addListener(map, 'click', function (evento) {
            var latitud = evento.latLng.lat();
            var longitud = evento.latLng.lng();
            if (marcador != undefined) {
                marcador.setMap(null);
            }
            SetXY(latitud, longitud);
            var pos = new google.maps.LatLng(latitud, longitud);
            marcador = new google.maps.Marker({
                draggable: true,
                position: pos,
                map: map
            });
            Psigeo.geoStreetView(latitud, longitud, 'pano');
        });
        //
        ProyectoEdificio.edificiosUbicacion(y, x, 300, map);
    };
    cargarPuntoOtroEdificioenMapa = function (datos, instanciaMapa) {
        infowindow = new google.maps.InfoWindow();
        //limpiando mapa
        for (var i = 0; i < edificios.length; i++) {
            edificios[i].setMap(null);
        }
        edificios = [];
        //añadir marcadores
        $.each(datos, function (index, data) {
            //dibujar marcador
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(data.coord_y, data.coord_x),
                draggable: false,
                icon: ((index == 0) ? 'img/icons/tap.png' : 'img/icons/office-' + data.estado_alias + '.png'),
                map: instanciaMapa
            });
            edificios.push(marker);
            infocontent = " Nombre: <strong>" + data.nombre + "</strong>" +
                    " <br> Distancia: <strong>" + data.distance + " metros</strong>" +
                    "<br> Dirección: <strong>" + data.direccion + "</strong>" +
                    "<br> Fec. Termino: <strong>" + data.fecha_termino + "</strong>" +
                    "<br> Estado: <strong>" + data.estado_edificio + "</strong>" +
                    "<br><a href=\"javascript:closeInfoWindow()\">Cerrar</a>";
            doInfoWindow(instanciaMapa, marker, infocontent);
        });
    };
    SetXY = function (y, x) {
        $('#txt_longitud').text(x);
        $('#txt_latitud').text(y);
        $('.input_coord_x').val(x);
        $('.input_coord_y').val(y);
        coord_x = x, coord_y = y;
        //console.log('X: ' + coord_x + ', Y: ' + coord_y);
    };
    /* Otros */
    guardarGestion = function () {
        var r = confirm("<?php echo Lang::get('main.esta_seguro?', array('accion' => 'Continuar')) ?>");
        if (r && validarGestion() && validarTipoConEstado()) {
            ProyectoEdificio.gestionar();
        }
    };
    guardarProvisional = function () {
        if ($("form[name=form_proyecto_provisional] input[name=txt_fecentrega]").val() != "") {
            var r = confirm("<?php echo Lang::get('¿Desea crear un Provisional para esta Etapa del Proyecto?', array('accion' => 'Continuar')) ?>");
            if (r && validarProvisionalProyecto()) {
                //ProyectoEdificio.gestionar();
                ProyectoEdificio.gestionarProvisional();
            }
        }
    };
    guardarMega = function () {
        var r = confirm("<?php echo Lang::get('¿Desea registrar un Mega Proyecto?', array('accion' => 'Continuar')) ?>");
        if (r && validarEdificioMegaProyecto()) {
            ProyectoEdificio.gestionarMega();
        }
    };
    guardarUbicacion = function () {
        ProyectoEdificio.ubicar($('#txt_proyecto_edificio_id').val(), coord_x, coord_y);
    };
    validarGestion = function () {
        valida = true;
        if ($("#slct_gcasuistica_modal").val() === '') {
            Psi.mensaje('warning', 'Seleccione TIPO', 6000);
            valida = false;
        }
        $(".form-proyecto-tipo").each(function (index, elemento) {
            if ($(this).css('display') == "block") {
                var fechas = [];
                var mensaje = "";
                if ($(this).attr("id") == 'form_proyecto_validacionreversa') {
                    if (!$("#txt_estado_general_si").parent('.iradio_minimal').hasClass("checked")) {
                        Psi.mensaje('warning', "Debe seleccionar 'SI' para continuar. ", 6000);
                        valida = false;
                        return valida;
                    }
                }
                $("#" + $(this).attr("id") + " input").each(function () {
                    var name = $(this).attr('name').split("txt_");
                    if ($(this).val() == "") {
                        switch (name[1]) {
                            case 'fecha_inicio':
                                mensaje += " Fecha Inicio está¡ vacía | ";
                                break;
//                            case 'fecha_termino':
//                                mensaje += " Fecha Término está¡ vacía | ";
//                                break;
                            case 'fecha_compromiso':
                                mensaje += " Fecha Compromiso está¡ vacía | ";
                                break;
                            default:
                                break;
                        }
                    } else {
                        if (name[1] != "")
                            fechas[name[1]] = $(this).val();
                    }
                });
                if (mensaje != "") {
                    Psi.mensaje('warning', mensaje, 6000);
                    valida = false;
                    return valida;
                } else {
                    /**
                     * validando fechas
                     * Regla: F. Inicio < F. Compromiso < F. Termino
                     */
                    var fecha_inicio = moment(fechas['fecha_inicio']);
                    var fecha_termino = moment(fechas['fecha_termino']);
                    var fecha_compromiso = moment(fechas['fecha_compromiso']);
                    if ((fechas['fecha_inicio'] !== undefined && fechas['fecha_inicio'] !== null) && fecha_inicio > fecha_compromiso) {
                        Psi.mensaje('warning', "Fecha Inicio debe ser menor a Fecha Compromiso", 6000);
                        valida = false;
                        return valida;
                    } else if ((fechas['fecha_inicio'] !== undefined && fechas['fecha_inicio'] !== null && fechas['fecha_termino'] !== undefined) && fecha_inicio > fecha_termino) {
                        Psi.mensaje('warning', "Fecha Inicio debe ser menor a Fecha Término", 6000);
                        valida = false;
                        return valida;
                    } else if ((fechas['fecha_compromiso'] != undefined && fechas['fecha_compromiso'] !== null && fechas['fecha_termino'] !== undefined) && fecha_compromiso > fecha_termino) {
                        Psi.mensaje('warning', "Fecha Compromiso debe ser menor a Fecha Término", 6000);
                        valida = false;
                        return valida;
                    }
                    valida = true;
                }
            }
        });
        return valida;
    };
    validarProvisionalProyecto = function () {
        var valida = true;
        if ($("form[name=form_proyecto_provisional] input[name=txt_fecentrega]").val() != "") {
            var mensaje = "";
            $("form[name=form_proyecto_provisional] input").each(function () {
                var name = $(this).attr('name');

                switch (name) {
                    case 'txt_fecentrega':
                        break;
                    case 'txt_fecrespuesta':
                        break;
                    case 'txt_fectermino':
                        break;
                    case 'txt_liquidacion':
                        break;
                    default:
                        break;
                }
            });
            if (mensaje == "") {
                return valida;
            } else {
                Psi.mensaje('warning', mensaje, 6000);
                valida = false;
                return valida;
            }

        }
    };
    validarEdificioMegaProyecto = function () {
        valida = true;
        mensaje = "";
        fechas = [];
        $("#form_proyecto_mega input[type=text]").each(function (e) {
            var name = $(this).attr("name");
            if ($(this).val() != "") {
                switch (name) {
                    case 'txt_energia_estado':
                        fechas[name] = $(this).val();
                        break;
                    case 'txt_fo_estado':
                        fechas[name] = $(this).val();
                        break;
                    case 'txt_troba_nueva':
                        fechas[name] = $(this).val();
                        break;
                    case 'txt_fecha_fin':
                        fechas[name] = $(this).val();
                        break;
                    case 'txt_troba_estado':
                        fechas[name] = $(this).val();
                        break;
                }
            } else {
                if (name == "txt_energia_estado") {
                    Psi.mensaje('warning', "Energía Estado está vacío", 6000);
                    valida = false;
                    return valida;
                } else {
                    fechas[name] = $(this).val();
                }
            }
        });
        if (fechas["txt_fo_estado"] != "") {
            if (fechas["txt_energia_estado"] == "") {
                Psi.mensaje('warning', "Se necesita Energía Estado", 6000);
                valida = false;
                return valida;
            }/*else if(moment(fechas["txt_energia_estado"]) > moment(fechas["txt_fo_estado"])){
             Psi.mensaje('warning', "Energia Estado no puede ser Mayor a Fo Estado", 6000);
             valida = false;
             return valida;
             }*/
        }
        if (fechas["txt_troba_nueva"] != "") {
            if (fechas["txt_fo_estado"] == "") {
                Psi.mensaje('warning', "Se necesita Fo Estado", 6000);
                valida = false;
                return valida;
            }
        }
        if (fechas["txt_fecha_fin"] != "") {
            if (fechas["txt_troba_nueva"] == "") {
                Psi.mensaje('warning', "Se necesita Troba Nueva", 6000);
                valida = false;
                return valida;
            } else if (fechas["txt_fo_estado"] == "") {
                Psi.mensaje('warning', "Se necesita Fo Estado", 6000);
                valida = false;
                return valida;
            }
        }

        if (fechas["txt_troba_estado"] != "") {
            if (fechas["txt_troba_nueva"] == "") {
                Psi.mensaje('warning', "Se necesita Troba Nueva", 6000);
                valida = false;
                return valida;
            } else if (fechas["txt_fo_estado"] == "") {
                Psi.mensaje('warning', "Se necesita Fo Estado", 6000);
                valida = false;
                return valida;
            } else if (fechas["txt_fecha_fin"] == "") {
                Psi.mensaje('warning', "Se necesita Fecha Fin", 6000);
                valida = false;
                return valida;
            }
        }

        return valida;
    };
    validaPestanas = function () {
        $(".modal-header>li.logo").css("display", "");
        $(".modal-header>li.logo").removeClass("active");
        $(".tab-pane").removeClass("active");
        $(".tab_3,#tab_3").addClass("active");
    };
    ChangeGrupoQuiebre = function (grupo_quiebre_id, checked) {
        tipo_actual = grupo_quiebre_id;
        $("#form_proyecto_iniciativa, #form_proyecto_diseno, #form_proyecto_licencia, #form_proyecto_oocc, #form_proyecto_cableado, #form_proyecto_megas, #form_proyecto_validacionreversa"
                ).hide();
        $('.btn-app').css('display', 'none');
        if (grupo_quiebre_id == 1) {
            $("#form_proyecto_diseno").show();
            $('.selected-di').css('display', 'inline-block');
        } else if (grupo_quiebre_id == 2) {
            $("#form_proyecto_licencia").show();
            $('.selected-li').css('display', 'inline-block');
        } else if (grupo_quiebre_id == 3) {
            $("#form_proyecto_oocc").show();
            $('.selected-oo').css('display', 'inline-block');
        } else if (grupo_quiebre_id == 4) {
            $("#form_proyecto_cableado").show();
            $('.selected-ca').css('display', 'inline-block');
        } else if (grupo_quiebre_id == 5) {
            $("#form_proyecto_megas").show();
        } else if (grupo_quiebre_id == 6) {
            $("#form_proyecto_iniciativa").show();
            $('.selected-in').css('display', 'inline-block');
        } else if (grupo_quiebre_id == 7) {
            $("#form_proyecto_validacionreversa").show();
            $('.selected-re').css('display', 'inline-block');
        }
        mostrarBotonDisenio(grupo_quiebre_id);
    };
    rellenarInputGestionEdificios = function (data, obj) {
        sprovisional = '';
        if (data.estado_provisional != null && (data.estado_provisional).length > 0) {
            sprovisional = ' (' + data.estado_provisional + ')';
        }
        $('#txt_proyecto').val(data.item);
        $('#lbl_direccion').html(data.direccion + ' ' + data.numero + ' ' + data.manzana + ' ' + data.lote); /* ((data.manzana_tdp).length > 0 ? data.manzana_tdp : data.manzana) */
        //$('#txt_fecha_registro').val(data.fecha_registro);
        $('#txt_fecha_termino').val(data.fecha_termino);
        $('#txt_avance').val(data.avance);
        //$('#txt_validado_call').val(data.validado_call);
        $('#txt_seguimiento').val(data.seguimiento);
        $('#txt_viven').val(data.vive);
        $('#txt_numero_departamentos').val(data.numero_departamentos);
        //$('#txt_gestion_obra').val(data.gestion_obra);
        //$('#txt_status').val(data.estado_edificio);
        $('#txt_tipo_proyecto').val(data.tipo_proyecto);
        $('#txt_segmento').val(data.segmento);
        $('#txt_contacto').val(data.persona_contacto);
        $('#lbl_avance').html(data.avance);
        $('#lbl_proyecto_estado').html(data.proyecto_estado);
        $('#txt_proyecto_estado').val(data.proyecto_estado + sprovisional);
        $('#lbl_proyecto_bandeja').html(data.bandeja);
        $('#lbl_numero_departamento').html(data.numero_departamentos);
        $('#lbl_numero_piso').html(data.numero_pisos);
        $('#lbl_direccion_referencia').html(' - ' + data.distrito);
        $('#txt_troba').val(data.troba_gis);
        $('#lbl_provisional_estado').html(sprovisional);
    };
    rellenarLabelTotales = function (obj, actual) {
        resumen = '';
        $.each(obj, function (index, estado) {
            var display = 'none';
            if (actual == '') {
                actual = 'A1 INICIATIVA';
            }
            if (estado.estado == actual) {
                display = 'inline-block';
            }
            resumen += '<a class="btn btn-app selected-' + estado.alias + '" style="display:' + display + '">';
            resumen += '<span style="font-size: 1.4em;" class="badge bg-red" id="txt_count_cableado">' + estado.total + '</span>';
            resumen += estado.proyecto_estado;
            resumen += '</a>';
        });
        $('.resumen-totales').html(resumen);
    };
    guardarBitacora = function (id) {
        ProyectoEdificio.crearBitacora(id);
    };
    guardarBitacoraGestion = function (datos) {
        ProyectoEdificio.crearBitacoraGestion(datos);
    };

    /**
     * input: (+/-)numero|{ROJO|AMBAR|VERDE} 
     * @param {String} data
     * @returns {htmlTable|String}     */
    columnaSemaforo = function (data, type, row) {
        if (data === null) {
            return '';
        } else {
            var dato = data.split('|');
            var color = dato[1] === 'ROJO' ? 'red' : (dato[1] === 'AMBAR' ? 'warning' : 'success');
            return '<i class="fa fa-2x fa-circle text-' + color + '"></i> <br><small>(' + dato[0] + ')</small>';
        }
    };
    mostrarBotonDisenio = function (estado) {
        if (estado == 1) {
            $('#btn_proceso_disenio_modal').css('display', 'inline-block');
        } else {
            $('#btn_proceso_disenio_modal').css('display', 'none');
        }
    };
    /**
     * `d` is the original data object for the row
     * @param {type} d
     * @returns {htmlTable|String}     */
    function format(d) {
        TheadD =
                '<tr>' +
                '<th><?php echo trans('gestion.fecha_inicio') ?></th>' +
                '<th><?php echo trans('gestion.fecha_compromiso') ?></th>' +
                '<th><?php echo trans('gestion.fecha_gestion') ?></th>' +
                '<th><?php echo trans('gestion.estado_general') ?></th>' +
                '<th><?php echo trans('gestion.gestion_interna') ?></th>' +
                '<th><?php echo trans('gestion.fecha_termino') ?></th>' +
                '<th><?php echo trans('gestion.casuistica') ?></th>' +
                '</tr>';
        tbodyD =
                '<tr>' +
                '<td>' + d.fecha_inicio + '</td>' +
                '<td>' + d.fecha_compromiso + '</td>' +
                '<td>' + d.fecha_gestion + '</td>' +
                '<td>' + d.estado_general + '</td>' +
                '<td>' + d.gestion_interna + '</td>' +
                '<td>' + d.fecha_termino + '</td>' +
                '<td>' + d.casuistica + '</td>' +
                '</tr>';
        //
        TheadL =
                '<tr>' +
                '<th><?php echo trans('gestion.recepcion_expediente') ?></th>' +
                '<th><?php echo trans('gestion.fecha_compromiso') ?></th>' +
                '<th><?php echo trans('gestion.fecha_inicio') ?></th>' +
                '<th><?php echo trans('gestion.fecha_termino') ?></th>' +
                '<th><?php echo trans('gestion.casuistica') ?></th>' +
                '</tr>';
        tbodyL =
                '<tr>' +
                '<td>' + d.recepcion_expediente + '</td>' +
                '<td>' + d.fecha_compromiso + '</td>' +
                '<td>' + d.fecha_inicio + '</td>' +
                '<td>' + d.fecha_termino + '</td>' +
                '<td>' + d.casuistica + '</td>' +
                '</tr>';
        //
        TheadO =
                '<tr>' +
                '<th><?php echo trans('gestion.fecha_inicio') ?></th>' +
                '<th><?php echo trans('gestion.fecha_compromiso') ?></th>' +
                '<th><?php echo trans('gestion.fecha_termino') ?></th>' +
                '<th><?php echo trans('gestion.casuistica') ?></th>' +
                '</tr>';
        tbodyO =
                '<tr>' +
                '<td>' + d.fecha_inicio + '</td>' +
                '<td>' + d.fecha_compromiso + '</td>' +
                '<td>' + d.fecha_termino + '</td>' +
                '<td>' + d.casuistica + '</td>' +
                '</tr>';
        //
        TheadC =
                '<tr>' +
                '<th><?php echo trans('gestion.fecha_inicio') ?></th>' +
                '<th><?php echo trans('gestion.fecha_compromiso') ?></th>' +
<?php /* '<th><?php echo trans('gestion.fecha_teorica_final') ?></th>' +  */ ?>
        '<th><?php echo trans('gestion.fecha_termino') ?></th>' +
                '<th><?php echo trans('gestion.casuistica') ?></th>' +
                '</tr>';
        tbodyC =
                '<tr>' +
                '<td>' + d.fecha_inicio + '</td>' +
                '<td>' + d.fecha_compromiso + '</td>' +
                //'<td>' + d.fecha_teorica_final + '</td>' +
                '<td>' + d.fecha_termino + '</td>' +
                '<td>' + d.casuistica + '</td>' +
                '</tr>';
        //
        TheadM =
                '<tr>' +
                '<th><?php echo trans('gestion.energia_estado') ?></th>' +
                '<th><?php echo trans('gestion.fo_estado') ?></th>' +
                '<th><?php echo trans('gestion.troba_nueva') ?></th>' +
                '<th><?php echo trans('gestion.fecha_fin') ?></th>' +
                '<th><?php echo trans('gestion.troba_estado') ?></th>' +
                '</tr>';
        tbodyM =
                '<tr>' +
                '<td>' + d.energia_estado + '</td>' +
                '<td>' + d.fo_estado + '</td>' +
                '<td>' + d.troba_nueva + '</td>' +
                '<td>' + d.fecha_fin + '</td>' +
                '<td>' + d.troba_estado + '</td>' +
                '</tr>';
        //
        var TheadEs, tbodyEs = '';
        switch ((d.proyecto_tipo).toLowerCase()) {
            case 'diseño':
                TheadEs = TheadD;
                tbodyEs = tbodyD;
                break;
            case 'licencia':
                TheadEs = TheadL;
                tbodyEs = tbodyL;
                break;
            case 'oocc':
                TheadEs = TheadO;
                tbodyEs = tbodyO;
                break;
            case 'cableado':
                TheadEs = TheadC;
                tbodyEs = tbodyC;
                break;
            case 'provisional':
                TheadEs = TheadP;
                tbodyEs = tbodyP;
                break;
            case 'megas':
                TheadEs = TheadM;
                tbodyEs = tbodyM;
                break;
        }

        htmlTable = '<table class="table table-condensed table-bordered" cellpadding="0" cellspacing="0" border="0">' +
                '<thead>' +
                TheadEs +
                '</thead>' +
                '<tbody>' +
                tbodyEs +
                '</tbody>' +
                '</table>';
        return htmlTable;
    }
    // SIN USO
    descargarReporte = function (event) {
        segmento = $('#slct_segmento').val();
        estadoproyecto = $('#slct_estadoproyecto').val();
        fecha = $('#fecha_created_at').val();
        tiproyecto = $('#slct_tipoproyecto').val();
        buscar = $('#txt_buscar').val();
        por = $('#txt_buscarPor').val();

        componente = '';
        componente += '?segmento=' + segmento;
        componente += '&estadoproyecto=' + estadoproyecto;
        componente += '&fecha=' + fecha;
        componente += '&tiproyecto=' + tiproyecto;
        componente += '&buscar=' + buscar;
        componente += '&por=' + por;
        window.location.href = $(this).attr('href') + componente;
    };
</script>

