<script type="text/javascript">
    //var relacion_tipo_estado = 0; // 0 no permite GUARDAR
    var mapa_para_registro;
    var varInputFileBase = {
        showUpload: false,
        allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
        maxFileCount: 1,
        previewClass: "bg-warning",
        showCaption: true,
        showPreview: false,
        showRemove: false,
        browseLabel: 'Examinar',
        maxFileSize: 1024 * 1 / 2
    };
    $(document).ready(function () {
        slctGlobal.listarSlct('segmento', 'slct_segmentos', 'simple', null, null, 0, '#slct_tipo_proyectos', 'M', 0, 0);
        slctGlobal.listarSlctpost('tipoproyecto', 'listar', 'slct_tipo_proyectos', 'simple', null, null, 1);
        ProyectoEdificio.cargarTipoInfraestructura();

        $('#proyectoEdificioMantenimientoModal').on('shown.bs.modal', function (e) {
            $('#pemTab a:first').tab('show');
            if (dataEdificio.id !== undefined) {
                ProyectoEdificio.verProyectoEdificio(dataEdificio.id);
            }

            Psigeo.mapProps.zoom = 16;
            mapa_para_registro = Psigeo.map('map_registrar');
            var yy = Psigeo.defLat;
            var xx = Psigeo.defLng;
            var completar = true;
            if (dataEdificio.coord_x !== undefined && dataEdificio.coord_y !== undefined) {
                yy = dataEdificio.coord_y;
                xx = dataEdificio.coord_x;
                completar = false;
            }
            marcador = Psigeo.marker(mapa_para_registro, yy, xx);
            obtenerDireccionGoogleMap(yy, xx, mapa_para_registro, completar);
            cargarMapaEventoPosicionMarcador(mapa_para_registro);
            ProyectoEdificio.edificiosUbicacion(yy, xx, 200, mapa_para_registro);
        });

        $('#proyectoEdificioMantenimientoModal').on('show.bs.modal', function (event) {
            //console.log(mapa_registrar);
            var button = $(event.relatedTarget);
            var recipient = button.data('accion');
            var modal = $(this);
            if (recipient === "REGISTRAR") {
                modal.find('.modal-title').text(recipient);
                $("#btn_accion_proyectoEdificio").attr('onclick', 'ProyectoEdificio.crearProyectoEdificio();');
                $('#validate-id, #validate-id-text').css('display', 'none');
                $('#txt_id_actual').val('');
                $('#txt_fecha_registro').val(moment().format("YYYY-MM-DD"));
                $('.pemtab_li2').hide();
            } else {
                modal.find('.modal-title').text(recipient);
                $("#btn_accion_proyectoEdificio").attr('onclick', 'ProyectoEdificio.editarProyectoEdificio();');
                //$('#form_proyectoEdificio #txt_id').removeAttr("disabled");
                $('#validate-id, #validate-id-text').css('display', 'inline');
                $('#validate-id-text').html('');
                $('.pemtab_li2').show();
            }
            $('#form_proyectoEdificio #txt_id_actual').prop('disabled', true);
            var fechas =
                    '#form_proyectoEdificio #txt_fecha_termino,' +
                    '#form_proyectoEdificio #txt_fecha_inicio,' +
                    '#form_proyectoEdificio #txt_seguimiento';

            $(fechas).daterangepicker({
                singleDatePicker: true,
                format: 'YYYY-MM-DD'
            });
        });

        $('#proyectoEdificioMantenimientoModal').on('hidden.bs.modal', function (event) {
            //$('#form_proyectoEdificio #txt_id').prop('disabled', false);
            $('#form_proyectoEdificio :input').val('');
            $('#form_proyectoEdificio select').removeAttr("selected");
            $('#form_proyectoEdificio select option:first-child').attr("selected", "selected");
            dataEdificio = [];
            $('#slct_tipo_proyectos,#slct_segmentos').multiselect('rebuild');
            dt_proyecto_edificios.draw();

            $("#txt_captura_1,#txt_captura_2,#txt_captura_3").fileinput(varInputFileBase);
            $('#txt_captura_1,#txt_captura_2,#txt_captura_3').fileinput('reset');

            //$('#txt_captura_1,#txt_captura_2,#txt_captura_3').parents('.input-group').find('.file-caption-name').text('');
            $('#txt_captura_1,#txt_captura_2,#txt_captura_3').parents('.btn-file').removeAttr("disabled");
            $('#txt_captura_1,#txt_captura_2,#txt_captura_3').removeAttr("disabled");
            $('.txt_captura_all').html('');
            $('.txt_captura_all_b').attr('href', '#').hide();
            // Esconder flag de input
            $('#form_proyectoEdificio label').find('a').css('display','none');
        });

        $('#proyectoEdificioEliminarModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var modal = $(this);
            modal.find('.modal-body input').val(button.context.dataset.id);
            modal.find('.modal-footer span').html(button.context.dataset.id);
        });

        $('#proyectoEdificioPapeleraModal').on('show.bs.modal', function (event) {
            ProyectoEdificio.cargarPapelera();
        });
        
        $('.eliminarImagenEdificio').on('click', function (e) {
           e.preventDefault();
           var href = ($(this).attr('href')).substring(1);
           var label = $(this).attr('itemid');
           var idTag = $(this).attr('id');
           console.log(href + ' - ' + label + ' - ' + idTag);

            var r = confirm("Confirmar, ¿Eliminar la imagen? " + label);
            if (r == true) {
               ProyectoEdificio.eliminarProyectoEdificioImagen(href, label, idTag, eventoEliminarImagen);   
            }
        });

        $('#btn_accion_proyectoEdificio_eliminar').on('click', function (e) {
            e.preventDefault();
            ProyectoEdificio.eliminarProyectoEdificio();
            dt_proyecto_edificios.draw();
        });

        $('#btnItemPadre').on('click', function (e) {
            e.preventDefault();
            ProyectoEdificio.enlazarPadreEdificio(dataEdificio.id);
        });

        $("#geo_submit").on("click", function (e) {
            e.preventDefault();
            gc = $('#geo_coordenada').val();
            if (gc.length > 1) {
                var cc = gc.split(',');
                cc[0] = (cc[0] == undefined ? '' : cc[0]);
                cc[1] = (cc[1] == undefined ? '' : cc[1]);
                marcador = Psigeo.marker(mapa_para_registro, cc[0], cc[1]);
                obtenerDireccionGoogleMap(cc[0], cc[1], mapa_para_registro, true);
            } else {
                obtenerCoordenadasGoogleMap(mapa_para_registro, true);
            }
            ProyectoEdificio.edificiosUbicacion(coord_y, coord_x, 200, mapa_para_registro);
        });

        $('.listaSubItem li').click(function () {
            var labelSelected = asignarLabelPorDropdown(this);   
            $('#txt_subitem').val(labelSelected);
        });

        $('#txt_mapa_referencia_d').on('ifChecked', function (event) {
            $('#geo_direccion').css('display', 'inline-block');
            $('#geo_coordenada').css('display', 'none').val('');
        });

        $('#txt_mapa_referencia_c').on('ifChecked', function (event) {
            $('#geo_direccion').css('display', 'none').val('');
            $('#geo_coordenada').css('display', 'inline-block');
        });
        // ProyectoEdificio.cargarSegmento();
        // ProyectoEdificio.cargarProyectoTipo();
        // $("#txt_id, #txt_subitem").focusout(function () {
            // ProyectoEdificio.validarIdProyectoEdificio();
        // });

        $("#txt_captura_1,#txt_captura_2,#txt_captura_3").fileinput(varInputFileBase);
        $('#txt_captura_1,#txt_captura_2,#txt_captura_3').on('change', prepareUpload);
    });
    // formPorEstado esta en: js/proyecto_edificio_ajax.php
    rellenarInputRegistroUbicacion = function (data, resultados, completar) {
        if (completar === true) {
            form = '#form_proyectoEdificio';

            $.each(resultados, function (index, text) {
                var component = text.address_components;
                if (text.types[0] == 'locality') {
                    $(form + ' #txt_distrito').val((component[0].long_name).toUpperCase());
                } else if (text.types[0] == 'administrative_area_level_2') {
                    $(form + ' #txt_departamento').val((component[0].long_name).toUpperCase());
                    $(form + ' #txt_provincia').val((component[1].long_name).toUpperCase());
                } else if (text.types[0] == 'street_address') {
                    $(form + ' #txt_direccion').val((component[1].long_name).toUpperCase());
                    $(form + ' #txt_numero').val((component[0].long_name).toUpperCase());
                }
            });

            $('.input_coord_x').val(coord_x);
            $('.input_coord_y').val(coord_y);
            $(form + ' #txt_proyecto_estado').val($('#slct_estadoproyecto option:first').text().toUpperCase());
        }
    };

    mostrarImagenCargada = function (archivo, input, id) {
        if (archivo != null && archivo !== undefined && archivo.length > 3) {
            $('.' + input).html('');
            $('#' + input).parents('.input-group').find('.file-caption-name').text(archivo);
            var img = $('<img class="img" title="' + archivo + '" alt="' + archivo + '">');
            //img.attr('src', 'images/edificio/200x150/' + archivo).appendTo('.' + input);
            $(img)
            .error(function() {
                $(this).attr( "src", "img/no_image_256x256.png" ).attr('alt', 'JS Imagen');
            })
            .attr('src', 'images/edificio/200x150/' + archivo).appendTo('.' + input);
            $('#' + input + '_b').attr('href', 'images/edificio/800x600/' + archivo).show();
            $('#' + input + '_delete').attr('href', '#' + id).show();



        }
    };

    prepareUpload = function (event) {
        var iden = $(this).attr('name');
        var idenA = iden.split('_');
        var files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append(iden, value);
            data.append('input', idenA[2]);
            data.append('id', $('#txt_id').val());

            ProyectoEdificio.subirImagenProyectoEdificio(data);
        });
    };

    eventoEliminarImagen = function (event) {
        if (event.id && event.rst == 1) {
            $('#' + event.id).parent('div').find('.thumbnail').html('');
        }
    };
</script>
