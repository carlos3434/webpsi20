<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- /.modal -->
<div class="modal fade" id="proyectoEdificioPapeleraModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">LISTADO DE EDIFICIOS ELIMINADOS</h4>
            </div>
            <div class="modal-body" style="height:560px;overflow-y: auto;">
                <div class="table-responsive">
                    <table id="datatable_proyectoedificio_papelera" class="table table-bordered table-striped table-condensed display responsive no-wrap" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center"><?php echo trans('gestion.item') ?></th>
                                <th class="text-center"><?php echo trans('gestion.item') ?></th>
                                <th class="text-center"><?php echo trans('gestion.nombre_proyecto') ?></th>
                                <th class="text-center"><?php echo trans('gestion.segmento') ?></th>
                                <th class="text-center"><?php echo trans('gestion.tipo_proyecto') ?></th>
                                <th class="text-center"><?php echo trans('gestion.distrito') ?></th>
                                <th class="text-center"><?php echo trans('gestion.direccion_numero_manzana_lote') ?></th>
                                <th class="text-center"><?php echo trans('gestion.numero_departamentos') ?></th>
                                <th class="text-center"><?php echo trans('gestion.avance') ?></th>
                                <th class="text-center"><?php echo trans('gestion.fecha_termino') ?></th>
                                <th class="text-center"><?php echo trans('gestion.estado') ?></th>
                                <th class="text-center"><?php echo trans('gestion.table_accion') ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->