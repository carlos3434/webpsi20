<?php
$fechaFormato = 'YYYY-MM-DD';

$tipoUrb = ['URB.', 'CCHH.'];
$tipoVia = ['CA.', 'AV.', 'JR.', 'ALAM.'];
$subItem = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
$identificacion = ['CAMPO', 'REFERIDO', 'CAPECO', 'INMARK', 'CLIENTE', 'DIARIO', 'EVENTO', 'EXPOFERIA', 'INMOBILIARIA', 'REVISTAS', 'WEB'];

?>
<form id="form_proyectoEdificio" name="form_proyectoEdificio" action="" method="post" autocomplete="off" enctype="multipart/form-data">
    <?php
    /*
      <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active">
      <a href="#mapE" aria-controls="mapE" role="tab" data-toggle="tab">
      <?php echo trans('gestion.mapa') ?>
      </a>
      </li>
      <li role="presentation">
      <a href="#registerE" aria-controls="registerE" role="tab" data-toggle="tab">
      <?php echo trans('gestion.registro') ?>
      </a>
      </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="mapE">

      </div>
      <div role="tabpanel" class="tab-pane" id="registerE">
      </div>
      </div>
      </div>
     */
    ?>    
    <fieldset>
        <legend>
            <span class="modal-title"></span>  DATOS 
            <a href="#"><i id="validate-id" class="fa fa-star" style="display: none;"></i></a>
            <span id="validate-id-text" class="text-red" style="font-size: 0.8em;"></span>
        </legend>
        <div class="row form-group">
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <label class="control-label">Item 
                        <input type="hidden" id="txt_id_actual" name="txt_id_actual" style="border: medium none; width: 55px;" value="">
                        <?php /* (<span id="txt_id_actual"></span>) */ ?>
                        <a id="error_id" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                            <i class="fa fa-exclamation"></i>
                        </a>
                    </label>
                    <input type="text" class="form-control" name="txt_id" id="txt_id" placeholder="(AUTO)" disabled="">
                </div>


                <div class="col-sm-4">
                    <label class="control-label">Item Padre
                        <a id="error_itempadre" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                            <i class="fa fa-exclamation"></i>
                        </a>
                    </label>
                    <div class="input-group">
                        <input id="txt_padre" name="txt_padre" type="number" class="form-control" placeholder="..." min="1000">
                        <div class="input-group-btn">
                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"> 
                                <i></i>
                                <span class="caret"></span> 
                                <span class="sr-only">Toggle Dropdown</span> 
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right listaSubItem">
                                <?php
                                foreach ($subItem as $v) {
                                    echo '<li><a itemid="' . $v . '" href="#">' . $v . '</a></li>';
                                }
                                ?>
                            </ul>
                            <button id="btnItemPadre" class="btn btn-info" type="button">ENLAZAR</button>
                            <input type="hidden" class="form-control" name="txt_subitem" id="txt_subitem">
                        </div>
                    </div>
                </div>
                <?php
                /*
                  <div class="col-sm-4">
                  <label class="control-label">Item Padre
                  <a id="error_itempadre" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                  <i class="fa fa-exclamation"></i>
                  </a>
                  </label>
                  <div class="input-group">
                  <input id="txt_padre" name="txt_padre" type="text" class="form-control" placeholder="...">
                  <span class="input-group-btn">
                  <button id="btnItemPadre" class="btn btn-info" type="button">ENLAZAR</button>
                  </span>
                  </div>
                  <?php
                  //                    echo '<select class="form-control" name="txt_subitem" id="txt_subitem" >';
                  //                    echo ' <option value=""></option>';
                  //                    foreach ($subItem as $v) {
                  //                        echo "<option value={$v}>{$v}</option>";
                  //                    }
                  //                    echo '</select>';
                  ?>
                  <input type="text" class="form-control" name="txt_subitem" id="txt_subitem" maxlength="1">
                  </div>
                 */
                ?>
                <div class="col-sm-2">
                    <label class="control-label">Fecha Registro  
                        <a id="error_fecha_registro" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                            <i class="fa fa-exclamation"></i>
                        </a>
                    </label>
                    <input type="text" class="form-control" name="txt_fecha_registro" id="txt_fecha_registro" value="" readonly="">
                </div>
                <div class="col-sm-2">
                    <label class="control-label">Estado
                        <a id="error_proyecto_estado" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                            <i class="fa fa-exclamation"></i>
                        </a>
                    </label>
                    <input type="text" class="form-control" name="txt_proyecto_estado" id="txt_proyecto_estado" readonly="">
                </div>
                <div class="col-sm-2">
                    <label class="control-label">Bandeja
                        <a id="error_bandeja" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                            <i class="fa fa-exclamation"></i>
                        </a>
                    </label>
                    <input type="text" class="form-control" name="txt_bandeja" id="txt_bandeja" readonly="">
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Registros de Ubicación</legend>
        <div class="row form-group">
            <div class="col-sm-4">
                <input type="radio" name="txt_mapa_referencia" id="txt_mapa_referencia_d"> <label for="txt_mapa_referencia_d">Dirección</label> 
                <input type="radio" name="txt_mapa_referencia" id="txt_mapa_referencia_c" checked=""> <label for="txt_mapa_referencia_c">Coordenadas</label>
                <br>
                <label class="control-label">Departamento
                    <a id="error_departamento" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_departamento" id="txt_departamento" readonly>
                <br>
                <label class="control-label">Provincia
                    <a id="error_provincia" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_provincia" id="txt_provincia" readonly>
                <br>
                <label class="control-label">Distrito
                    <a id="error_distrito" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_distrito" id="txt_distrito" readonly>
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">Coordenada X
                            <a id="error_coord_x" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control input_coord_x" name="txt_coord_x" id="txt_coord_x" readonly>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">Coordenada Y
                            <a id="error_coord_y" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control input_coord_y" name="txt_coord_y" id="txt_coord_y" readonly>
                    </div>
                </div>
            </div>
            <div class="col-sm-8" style="position: relative;">
                <div id="floating-panel" style=" position: absolute;top: -20px;left: 25%;z-index: 5;background-color: #fff;padding: 5px;text-align: center;line-height: 25px;padding-left: 10px;">
                    <input id="geo_direccion" type="text" value="" placeholder="DIRECCION, DISTRITO, DPTO" style="display: none;width: 180px;">
                    <input id="geo_coordenada" type="text" value="" placeholder="COORD_Y,COORD_X" style="display: inline-block;width: 180px;">
                    <input id="geo_submit" type="button" value="<?php echo trans('gestion.buscar'); ?>">
                </div>
                <div id="map_registrar" style="height: 320px;"></div>
            </div> 
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Datos del Edificio</legend>
        <div class="row form-group">
            <div class="col-sm-6">
                <label class="control-label">Segmento
                    <a id="error_segmentos" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <select class="form-control" name="slct_segmentos" id="slct_segmentos"></select>
            </div>
            <div class="col-sm-6">
                <label class="control-label">Tipo Proyecto
                    <a id="error_tipo_proyectos" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <select class="form-control" name="slct_tipo_proyectos" id="slct_tipo_proyectos"></select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">Tipo URB / CCHH
                    <a id="error_tipo_cchh" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <select class="form-control" name="slct_tipo_cchh" id="slct_tipo_cchh">
                    <option value=""><?php echo trans('main.seleccionar') ?></option>
                    <?php
                    foreach ($tipoUrb as $v)
                        echo "<option value={$v}>{$v}</option>";
                    ?>
                </select>
            </div>
            <div class="col-sm-6 col-xs-12">
                <label class="control-label">Nombre URB / CCHH
                    <a id="error_cchh" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Nombre URB / CCHH" name="txt_cchh" id="txt_cchh">
            </div>
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">Tipo Via
                    <a id="error_tipo_via" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <select class="form-control" name="slct_tipo_via" id="slct_tipo_via">
                    <option value=""><?php echo trans('main.seleccionar') ?></option>
                    <?php
                    foreach ($tipoVia as $v)
                        echo "<option value={$v}>{$v}</option>";
                    ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-6 col-xs-12">
                <label class="control-label">Direccion
                    <a id="error_direccion" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Direccion" name="txt_direccion" id="txt_direccion">
            </div>
            <div class="col-sm-2 col-xs-4">
                <label class="control-label">Número
                    <a id="error_numero" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Numero" name="txt_numero" id="txt_numero">
            </div>
            <div class="col-sm-2 col-xs-4">
                <label class="control-label">Manzana
                    <a id="error_manzana" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Manzana" name="txt_manzana" id="txt_manzana">
            </div>
            <div class="col-sm-2 col-xs-4">
                <label class="control-label">Lote
                    <a id="error_lote" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Lote" name="txt_lote" id="txt_lote">
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-sm-6 col-xs-12">
                <label class="control-label">Nombre del Proyecto
                    <a id="error_nombre_proyecto" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Nombre del Proyecto" name="txt_nombre_proyecto" id="txt_nombre_proyecto">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Blocks
                    <a id="error_numero_block" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Blocks" name="txt_numero_block" id="txt_numero_block">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Pisos
                    <a id="error_numero_pisos" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Pisos" name="txt_numero_pisos" id="txt_numero_pisos">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Departamentos
                    <a id="error_numero_departamentos" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Departamentos" name="txt_numero_departamentos" id="txt_numero_departamentos">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Departamentos Habitados
                    <a id="error_numero_departamentos_habitados" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Departamentos Habitados" name="txt_numero_departamentos_habitados" id="txt_numero_departamentos_habitados">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">% de Avance
                    <a id="error_avance" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese % de Avance" name="txt_avance" id="txt_avance">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Fecha Termino Construccion
                    <a id="error_fecha_termino" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="<?php echo $fechaFormato ?>" name="txt_fecha_termino" id="txt_fecha_termino" readonly="readonly">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4 col-xs-6">
                <label class="control-label">Tipo Infraestructura
                    <a id="error_tipo_infraestructura" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <select class="form-control" name="slct_tipo_infraestructura" id="slct_tipo_infraestructura"></select>
            </div>
            <div class="col-sm-4 col-xs-12">
                <label class="control-label">Fuente de Identificación
                    <a id="error_fuente_identificacion" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                 <select class="form-control" name="txt_fuente_identificacion" id="txt_fuente_identificacion">
                    <option value="">.::SELECCIONAR::.</option>
                    <?php
                        foreach ($identificacion as $v) {
                            echo  "<option value='$v'>$v</option>";
                        }
                    ?>
                 </select>
                <!--
                <input type="text" class="form-control" name="txt_fuente_identificacion" id="txt_fuente_identificacion">
                 -->
            </div>
            <div class="col-sm-4 col-xs-6">
                <label class="control-label">Fecha Visita de Campo
                    <a id="error_seguimiento" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="<?php echo $fechaFormato ?>" name="txt_seguimiento" id="txt_seguimiento" readonly="readonly">
            </div>
        </div>    
        <div class="row form-group">
            <div class="col-sm-12">
                <label class="control-label">Observaciones
                    <a id="error_observacion" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <textarea rows="2" class="form-control" placeholder="Ingrese Obs. + Bitacora" name="txt_observacion" id="txt_observacion"></textarea>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Datos de la Contructora</legend>
        <div class="row form-group">
            <div class="col-sm-4">
                <label class="control-label">RUC Constructora
                    <a id="error_ruc_constructora" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese RUC Constructora" name="txt_ruc_constructora" id="txt_ruc_constructora" maxlength="11">
            </div>
            <div class="col-sm-8">
                <label class="control-label">Nombre Constructora
                    <a id="error_nombre_constructora" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Nombre Constructora" name="txt_nombre_constructora" id="txt_nombre_constructora">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">Contacto 1
                    <a id="error_uno_contacto" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Contacto 1" name="txt_uno_contacto" id="txt_uno_contacto">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Telefono 1/2
                    <a id="error_uno_telefono_a" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Telefono 1" name="txt_uno_telefono_a" id="txt_uno_telefono_a">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Telefono 2/2
                    <a id="error_uno_telefono_b" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Telefono 2" name="txt_uno_telefono_b" id="txt_uno_telefono_b">
            </div>
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">E-mail 1
                    <a id="error_uno_email" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control"  name="txt_uno_email" id="txt_uno_email">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">Contacto 2
                    <a id="error_dos_contacto" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Contacto 2" name="txt_dos_contacto" id="txt_dos_contacto">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Teléfono 1/2
                    <a id="error_dos_telefono_a" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Telefono 1" name="txt_dos_telefono_a" id="txt_dos_telefono_a">
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label">Teléfono 2/2
                    <a id="error_dos_telefono_b" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" placeholder="Ingrese Telefono 2" name="txt_dos_telefono_b" id="txt_dos_telefono_b">
            </div>
            <div class="col-sm-3 col-xs-12">
                <label class="control-label">E-mail 2
                    <a id="error_dos_paginaweb" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control"  name="txt_dos_paginaweb" id="txt_dos_paginaweb">
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Registro de Planta Tradicional</legend>
        <div class="row form-group">
            <div class="col-xs-4">
                <label class="control-label">MDF
                    <a id="error_mdf" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_mdf" id="txt_mdf" readonly>
            </div>
            <div class="col-xs-4">
                <label class="control-label">URA
                    <a id="error_ura" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_ura" id="txt_ura" readonly>
            </div>
            <div class="col-xs-4">
                <label class="control-label">Armario o Red Directa
                    <a id="error_armario" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_armario" id="txt_armario" readonly>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Registro de Planta Coaxial</legend>
        <div class="row form-group">
            <div class="col-xs-6">
                <label class="control-label">Troba
                    <a id="error_troba_gis" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_troba_gis" id="txt_troba_gis" readonly>
            </div>
            <div class="col-xs-6">
                <label class="control-label">Bidireccionado
                    <a id="error_troba_bidireccional" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_troba_bidireccional" id="txt_troba_bidireccional" readonly>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Registros de Otras Operadoras</legend>
        <div class="row form-group">
            <div class="col-sm-12">
                <label class="control-label">Competencias
                    <a id="error_competencia_uno" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_competencia_uno" id="txt_competencia_uno" readonly>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend class="text-uppercase">Gestión</legend>
        <div class="row form-group">
            <div class="col-sm-2 col-xs-6">
                <label class="control-label">Mes
                    <a id="error_mes" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_mes" id="txt_mes" disabled>
            </div>
            <div class="col-sm-2 col-xs-6">
                <label class="control-label">Ticket
                    <a id="error_ticket" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_ticket" id="txt_ticket" disabled>
            </div>
            <div class="col-sm-3 col-xs-6">
                <label class="control-label zona-gis">NSE
                    <a id="error_zona_gis" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_zona_gis" id="txt_zona_gis" disabled>
            </div>
            <div class="col-sm-5 col-xs-6">
                <label class="control-label">Empresa Colaboradora
                    <a id="error_empresa_colaboradora" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_empresa_colaboradora" id="txt_empresa_colaboradora" disabled>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-4 col-xs-6">
                <label class="control-label">Usuario Registro Inicial
                    <a id="error_usuario_inicial" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_usuario_inicial" id="txt_usuario_inicial" disabled>
            </div>
            <div class="col-sm-4 col-xs-6">
                <label class="control-label">Usuario Ultimo Registro
                    <a id="error_usuario_ultimo" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_usuario_ultimo" id="txt_usuario_ultimo" disabled>
            </div>
            <div class="col-sm-4 col-xs-12">
                <label class="control-label">Fecha de Último Movimiento
                    <a id="error_fecha_ultimo" style="display:none;" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom">
                        <i class="fa fa-exclamation"></i>
                    </a>
                </label>
                <input type="text" class="form-control" name="txt_fecha_ultimo" id="txt_fecha_ultimo" disabled>
            </div>
        </div>
    </fieldset>
</form>