<!-- /.modal -->
<?php
$format_db = 'YYYY-MM-DD';
?>
<div class="modal fade" id="proyectoProcesoDisenioModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_proceso_disenio" name="form_proceso_disenio" action="" method="post" class="" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php echo Lang::get('gestion.disenio'); ?></h4>
                </div>
                <div class="modal-body" style="height: 500px;overflow-y: auto;">

                    <input type="hidden" id="proyecto_edificio_id" name="proyecto_edificio_id">
                    <!-- Nav tabs -->
                    <ul id="disenioTabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#PD0" aria-controls="PD0" role="tab" data-toggle="tab">Inicio</a>
                        </li>
                        <li role="presentation">
                            <a href="#PD1" aria-controls="PD1" role="tab" data-toggle="tab">MML</a>
                        </li>
                        <li role="presentation">
                            <a href="#PD2" aria-controls="PD2" role="tab" data-toggle="tab">MUNICIPAL DISTRITAL</a>
                        </li>
                        <li role="presentation">
                            <a href="#PD3" aria-controls="PD3" role="tab" data-toggle="tab">GTU</a>
                        </li>
                        <li role="presentation">
                            <a href="#PD4" aria-controls="PD4" role="tab" data-toggle="tab">ORDENANZA 341</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="PD0">

                        </div>
                        <!-- //PRIMER::TAB -->
                        <div role="tabpanel" class="tab-pane" id="PD1">

                        </div>
                        <!-- //SEGUNDO::TAB -->
                        <div role="tabpanel" class="tab-pane" id="PD2">

                        </div>
                        <!-- //TERCER::TAB -->
                        <div role="tabpanel" class="tab-pane" id="PD3">

                        </div>
                        <!-- //CUARTO::TAB -->
                        <div role="tabpanel" class="tab-pane" id="PD4">

                        </div>
                        <!-- //QUINTO::TAB -->
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-right">

                            <button id="btn_disenio_volver_modal" type="button" class="btn btn-info" data-dismiss="modal"><?php echo Lang::get('main.volver'); ?></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('main.Close'); ?></button>
                            <button id="btn_disenio_guardar" type="submit" class="btn btn-primary btn-flat"><?php echo Lang::get('main.Save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->
