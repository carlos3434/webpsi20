<!-- /.modal -->
<?php
$format_db = 'YYYY-MM-DD';
?>
<div class="modal fade" id="proyectoEdificioModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Custom Tabs -->
            <input type="hidden" id="txt_proyecto_edificio_id" name="txt_proyecto_edificio_id">
            <input type="hidden" id="txt_proyecto_estado_id" name="txt_proyecto_estado_id">

            <div class="nav-tabs-custom nav-tabs-style-1">
                <ul class="nav nav-tabs logo modal-header" id="proyectoEdificioModalNav">
                    <li class="logo tab_3">
                        <a href="#tab_3" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-building fa-lg"></i> </button>
                            <span>Ubicación</span>
                        </a>
                    </li>
                    <?php /*
                      <li class="logo tab_1">
                      <a href="#tab_1" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-cloud fa-lg"></i> </button>
                      <span>Gesti&oacute;n Edificios</span>
                      </a>
                      </li>
                      <li class="logo tab_2">
                      <a href="#tab_2" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-search-plus fa-lg"></i> </button>
                      <span>Movimientos</span>
                      </a>
                      </li>
                      <li class="logo tab_5">
                      <a href="#tab_5" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-files-o fa-lg"></i> </button>
                      <span><?php echo trans('main.bitacora'); ?></span>
                      </a>
                      </li>
                     */ ?>
                    <li class="logo tab_4">
                        <a href="#tab_4" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-list-ul fa-lg"></i> </button>
                            <span>Detalle</span>
                        </a>
                    </li>
                    <li class="pull-right">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab_3">
                        <div class="row">
                            <div class="col-sm-6">
                                <label><?php echo trans('gestion.direccion_numero_manzana_lote') ?>: </label><br> 
                                <span id="lbl_direccion" style="font-size: 1.2em;"></span>
                                <span id="lbl_direccion_referencia" style="font-size: 1.2em;"></span>
                            </div>

                            <div class="col-sm-2">
                                <label><?php echo trans('gestion.avance') ?> (%): </label><br> 
                                <span id="lbl_avance" style="font-size: 1.2em;"></span>
                            </div>
                            <div class="col-sm-2">
                                <label><?php echo trans('gestion.numero_departamentos') ?>: </label><br> 
                                <span id="lbl_numero_departamento" style="font-size: 1.2em;"></span>
                            </div>  
                            <div class="col-sm-2">
                                <label><?php echo trans('gestion.numero_pisos') ?>: </label><br> 
                                <span id="lbl_numero_piso" style="font-size: 1.2em;"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label><?php echo trans('gestion.estado') ?>: </label><br> 
                                <span id="lbl_proyecto_estado" style="font-size: 1.2em;"></span>
                            </div>
                            <div class="col-sm-3">
                                <label><?php echo trans('gestion.bandeja') ?>: </label><br> 
                                <span id="lbl_proyecto_bandeja" style="font-size: 1.2em;"></span>
                            </div>
                            <div class="col-sm-5">
                                <label><?php echo trans('gestion.provisional') ?>: </label><br> 
                                <span id="lbl_provisional_estado" style="font-size: 1.2em;"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <button class="btn btn-success" id="boton_mapa_mostrar">MAPA</button>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <button class="btn btn-success" id="boton_calle_mostrar">CALLE</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="map"></div>
                            </div>
                            <div class="col-sm-6">
                                <div id="pano"></div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-sm-offset-6 col-sm-6 text-right">
                                <label>Coordenadas: </label>
                                <span id="txt_longitud" style="font-size: 1em;">0</span>, 
                                <span id="txt_latitud" style="font-size: 1em;">0</span>
                            </div>     
                        </div>

                        <div class="modal-footer">
                            <?php echo \View::make('admin.gestion.form.proyectoedificiototales'); ?>
                            <button type="button"  class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                            <button type="button" id="btn_aplicar_xy" name="btn_aplicar_xy" class="btn btn-primary"><?php echo trans('main.Save') ?> XY</button>
                        </div>
                    </div><!-- /.tab-pane -->
                    <?php /*
                      <div class="tab-pane" id="tab_1">
                      <?php echo \View::make('admin.gestion.form.proyectoedificiogestionar', ['format_db' => $format_db]); ?>
                      </div><!-- /.tab-pane -->

                      <div class="tab-pane" id="tab_2">
                      <div class="row" style="height:500px;overflow-y: auto;">
                      <div class="col-sm-12">
                      <h3>GESTIONES DEL EDIFICIO</h3>
                      <div class="table-responsive">
                      <table id="t_movimiento_total" class="table table-bordered table-condensed">
                      <thead>
                      <tr>
                      <th><?php echo trans('gestion.nro') ?></th>
                      <th><?php echo trans('gestion.proyecto_tipo') ?></th>
                      <th><?php echo trans('gestion.fecha_inicio') ?></th>
                      <th><?php echo trans('gestion.fecha_compromiso') ?></th>
                      <th><?php echo trans('gestion.fecha_gestion') ?></th>
                      <th><?php echo trans('gestion.estado_general') ?></th>

                      <th><?php echo trans('gestion.gestion_interna') ?></th>
                      <th><?php echo trans('gestion.recepcion_expediente') ?></th>
                      <th><?php echo trans('gestion.fecha_termino') ?></th>
                      <th><?php echo trans('gestion.casuistica') ?></th>
                      <th><?php echo trans('gestion.proyecto_estado') ?></th>

                      <th><?php echo trans('gestion.fecha_seguimiento') ?></th>
                      <th><?php echo trans('gestion.created_at') ?></th>
                      <th><?php echo trans('gestion.usuario') ?></th>
                      </tr>
                      </thead>
                      </table>
                      </div>

                      <div class="table-responsive">
                      <h3>PROVISIONALES</h3>
                      <table id="t_movimiento_provisional" class="table table-bordered table-condensed">
                      <thead>
                      <tr>
                      <th><?php echo trans('gestion.nro') ?></th>
                      <th><?php echo trans('gestion.proyecto_tipo') ?></th>
                      <th><?php echo trans('gestion.fecentrega') ?></th>
                      <th><?php echo trans('gestion.fecrespuesta') ?></th>
                      <th><?php echo trans('gestion.fectermino') ?></th>
                      <th><?php echo trans('gestion.fecliquidacion') ?></th>

                      <th><?php echo trans('gestion.estadoprovisional') ?></th>
                      <th><?php echo trans('gestion.evaluadorejecutor') ?></th>
                      </tr>
                      </thead>
                      </table>
                      </div>

                      <div class="table-responsive">
                      <h3>MEGAPROYECTOS</h3>
                      <table id="t_movimiento_megaproyecto" class="table table-bordered table-condensed">
                      <thead>
                      <tr>
                      <th><?php echo trans('gestion.nro') ?></th>
                      <th><?php echo trans('gestion.energia_estado') ?></th>
                      <th><?php echo trans('gestion.fo_estado') ?></th>
                      <th><?php echo trans('gestion.troba_nueva') ?></th>
                      <th><?php echo trans('gestion.troba_fecha_final') ?></th>
                      <th><?php echo trans('gestion.troba_estado') ?></th>
                      </tr>
                      </thead>
                      </table>
                      </div>
                      </div>
                      </div>
                      <div class="modal-footer">
                      <?php echo \View::make('admin.gestion.form.proyectoedificiototales'); ?>
                      <button type="button"  class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                      </div>
                      </div><!-- /.tab-pane -->

                      <div class="tab-pane" id="tab_5">
                      <div class="row">
                      <div class="col-md-12">
                      <form id="form_proyecto_bitacora" name="form_proyecto_bitacora" action="" method="post"  autocomplete="off">
                      <div class="row">
                      <div class="col-sm-3">
                      <div class="form-group">
                      <label for="txt_fecha_observacion"><?php echo trans('gestion.fecha') ?></label>
                      <input style="font-size:1.3em;" placeholder="<?php echo $format_db; ?>" type="text" class="form-control input-lg" name="txt_fecha_observacion" id="txt_fecha_observacion" value="<?php echo date('Y-m-d') ?>">
                      </div>
                      </div>
                      <div class="col-sm-9">
                      <div class="form-group">
                      <label for="txt_descripcion"><?php echo trans('gestion.descripcion') ?></label>
                      <textarea style="font-size:1.3em;" placeholder="<?php echo trans('gestion.agregar_descripcion_bitacora') ?>" class="resize-none form-control " rows="2" name="txt_descripcion" id="txt_descripcion"></textarea>
                      </div>
                      </div>
                      </div>
                      </form>
                      <!-- Inicia contenido -->
                      <table id="datatable_proyecto_bitacora" class="table table-bordered table-striped table-condensed display responsive no-wrap" cellspacing="0"  width="100%">
                      <thead>
                      <tr>
                      <th class="text-center" ><?php echo trans('gestion.nro') ?></th>
                      <th class="text-center"><?php echo trans('gestion.fecha') ?></th>
                      <th class="text-center"><?php echo trans('gestion.descripcion') ?></th>
                      <th class="text-center"><?php echo trans('gestion.usuario') ?></th>
                      </tr>
                      </thead>
                      </table>
                      <!-- Finaliza contenido -->
                      </div>
                      </div>
                      <div class="modal-footer">
                      <button type="button"  class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                      <button type="button" id="btn_bitacora" name="btn_bitacora" class="btn btn-primary"><?php echo trans('main.Save') ?></button>
                      </div>
                      </div><!-- /.tab-pane -->
                     */ ?>
                    <div class="tab-pane" id="tab_4">
                        <div class="row">
                            <div class="col-md-12" id="tab_4_content">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"  class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                        </div>
                    </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div>
    </div>
</div>
<!-- /.modal -->
<?php /*
  <div class="col-sm-12">
  <div class="table-responsive">
  <table id="t_movimiento" class="table table-bordered table-striped">
  <thead>
  <tr>
  <th></th>
  <th><?php echo trans('gestion.nro') ?></th>
  <th><?php echo trans('gestion.fecha_movimiento') ?></th>
  <th><?php echo trans('gestion.avance') ?></th>
  <th><?php echo trans('gestion.usuario') ?></th>
  <th><?php echo trans('gestion.observacion') ?></th>
  <th><?php echo trans('gestion.proyecto_tipo') ?></th>
  </tr>
  </thead>
  <tbody id="tb_movimiento">
  </tbody>
  <tfoot>
  <tr>
  <th></th>
  <th><?php echo trans('gestion.nro') ?></th>
  <th><?php echo trans('gestion.fecha_movimiento') ?></th>
  <th><?php echo trans('gestion.avance') ?></th>
  <th><?php echo trans('gestion.usuario') ?></th>
  <th><?php echo trans('gestion.observacion') ?></th>
  <th><?php echo trans('gestion.proyecto_tipo') ?></th>
  </tr>
  </tfoot>
  </table>
  </div>
  </div>
 */ ?>  