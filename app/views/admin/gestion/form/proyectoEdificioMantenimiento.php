<div class="modal fade" id="proyectoEdificioMantenimientoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <ul id="pemTab" class="nav nav-tabs logo modal-header" role="tablist">
                <li role="presentation" class="logo pemtab_li1 active" >
                    <a href="#pemtab_1" data-toggle="tab" aria-controls="pemtab_1" role="tab"  >
                        <button class="btn btn-primary btn-sm"><i class="fa fa-building fa-lg"></i> </button>
                        <span>Edificio</span>
                    </a>
                </li>
                <li role="presentation" class="logo pemtab_li2"  aria-controls="profile">
                    <a href="#pemtab_2" data-toggle="tab" aria-controls="pemtab_2" role="tab"  >
                        <button class="btn btn-primary btn-sm"><i class="fa fa-picture-o fa-lg"></i> </button>
                        <span>Imagenes</span>
                    </a>
                </li>
                <li class="pull-right">
                    <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                    </button>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="pemtab_1">
                    <div class="modal-body" style="height:510px;overflow-y: auto;">
                        <?php echo \View::make('admin.gestion.form.formRegistrarEdificio'); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                        <button type="button" class="btn btn-primary" id="btn_accion_proyectoEdificio"><?php echo trans('main.Save') ?></button>
                    </div>
                </div><!-- /.tab-pane -->

                <div role="tabpanel" class="tab-pane" id="pemtab_2">
                    <div class="modal-body" style="height:510px;overflow-y: auto;">
                        <div class="row">
                            <div class="col-md-12">

                                <form id="archivo_proyectoEdificio" name="form_proyectoEdificio" action="" method="post" autocomplete="off" enctype="multipart/form-data">
                                    <fieldset>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label">Imagen #1</label>
                                                <input type="file" id="txt_captura_1" name="txt_captura_1" class="form control file file-loading" title="Cargar imagen #1" accept="image/*" >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label">Imagen #2</label>
                                                <input type="file" id="txt_captura_2" name="txt_captura_2" class="form control file file-loading" title="Cargar imagen #2" accept="image/*" >
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label">Imagen #3</label>
                                                <input type="file" id="txt_captura_3" name="txt_captura_3" class="form control file file-loading" title="Cargar imagen #3" accept="image/*" >
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>

                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-sm-4">
                                <div class="thumbnail txt_captura_1 txt_captura_all">

                                </div>
                                <a id="txt_captura_1_b" title="Tamaño completo" href="#" class="btn btn-primary btn-sm txt_captura_all_b" target="_blank" style="display: none;">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a id="txt_captura_1_delete" title="Eliminar" href="#" class="btn btn-danger btn-sm txt_captura_all_b eliminarImagenEdificio" itemid="foto_uno" style="display: none;">
                                    <i class="fa fa-eraser"></i>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <div class="thumbnail txt_captura_2 txt_captura_all">

                                </div>
                                <a id="txt_captura_2_b" title="Tamaño completo" href="#" class="btn btn-primary btn-sm txt_captura_all_b" target="_blank" style="display: none;">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a id="txt_captura_2_delete" title="Eliminar" href="#" class="btn btn-danger btn-sm txt_captura_all_b eliminarImagenEdificio" itemid="foto_dos" style="display: none;">
                                    <i class="fa fa-eraser"></i>
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <div class="thumbnail txt_captura_3 txt_captura_all">

                                </div>
                                <a id="txt_captura_3_b" title="Tamaño completo" href="#" class="btn btn-primary btn-sm txt_captura_all_b" target="_blank" style="display: none;">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a id="txt_captura_3_delete" title="Eliminar" href="#" class="btn btn-danger btn-sm txt_captura_all_b eliminarImagenEdificio" itemid="foto_tres" style="display: none;">
                                    <i class="fa fa-eraser"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                    </div>
                </div><!-- /.tab-pane -->
            </div>
        </div>
    </div>
</div>