<?php
$estadoprovisional = [
    'ATENDIDO PROVISIONAL', 'CLIENTE NO ACEPTA', 'CLIENTE SOLICITA CARTA',
    'EN EJECUCION', 'EN EVALUACION', 'FLUJO NORMAL',
    'NO SE UBICA A NADIE', 'NO SE UBICA DIRECCION', 'REPORTADO PROVISIONAL',
    'SE REQUIERE CONTACTO', 'SIN HFC'
        //, 'Solicita carta'
];
?>
<hr>
<fieldset id="fieldset_proyecto_provisional">
    <form id="form_proyecto_provisional" name="form_proyecto_provisional" action="" method="post" class="" autocomplete="off">
        <div class="row">
            <div class="col-md-12">
                <!--<input type="checkbox" name="es_provisional" id="es_provisional"/>--><span> Generar Provisional </span>
                <input type="submit" class="btn btn-success pull-right"  value="Crear Provisional" />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-12" >
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.evaluadorejecutor') ?></label>
                    <input type="text" class="form-control pull-right"  name="txt_ejecutor_proyecto" id="txt_ejecutor_proyecto"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecrespuesta') ?></label>
                    <input type="text" class="form-control pull-right"  name="txt_fecrespuesta" id="txt_fecrespuesta"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.estadoprovisional') ?></label>
                    <select class="form-control pull-right" name="txt_estado_provisional" id="txt_estado_provisional">
                        <option value=""><?php echo trans('main.seleccionar') ?></option>
                        <?php
                        foreach ($estadoprovisional as $v)
                            echo "<option value={$v}>{$v}</option>";
                        ?>
<!--                        <option value="ATENDIDO PROVISIONAL">Atentido provisional</option>
                        <option value="CLIENTE NO ACEPTA">Cliente no acepta</option>
                        <option value="CLIENTE SOLICITA CARTA">Cliente solicita carta</option>
                        <option value="EN EJECUCION">En ejecución</option>
                        <option value="EN EVALUACION">En evaluación</option>
                        <option value="FLUJO NORMAL">Flujo normal</option>
                        <option value="NO SE UBICA A NADIE">No se ubica a nadie</option>
                        <option value="NO SE UBICA DIRECCION">No se ubica dirección</option>
                        <option value="REPORTADO PROVISIONAL">Reportado provisional</option>
                        <option value="SE REQUIERE CONTACTO">Se requiere contacto</option>
                        <option value="Sin HFC">Sin HFC</option>
                        <option value="Solicita carta">Solicita carta</option>-->
                    </select>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fectermino') ?></label>
                    <input type="text" class="form-control pull-right" name="txt_fectermino" id="txt_fectermino"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecliquidacion') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php //echo $format_db        ?>" name="txt_fecliquidacion" id="txt_fecliquidacion"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecentrega') ?></label>
                    <input type="text" class="form-control pull-right" name="txt_fecentrega" id="txt_fecentrega"/>
                </div>
                <input type="hidden" name="is_provisional" value="1" />
                <input type="hidden" name="proyecto_estado_temp" value="" />
                <input type="hidden" name="proyecto_provisional_parent_temp" value="0" />
            </div>
        </div>
    </form>
</fieldset>


