<!-- /.modal -->
<div class="modal fade" id="proyectoEdificioEliminarModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">ELIMINAR EDIFICIO</h4>
            </div>
            <div class="modal-body">
                <form id="form_proyectoEdificioEliminar" name="form_proyectoEdificioEliminar" action="" method="post">
                    <input type="hidden" class="form-control" name="txt_id" id="txt_id">
                    <div class="form-group">
                        <label class="control-label">
                            Confirmar que desea eliminar el EDIFICIO CON ID
                        </label>
                        <input type="text" name="txt_id" readonly style="border: medium none;display: inline;width: 120px;">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                <button type="button" class="btn btn-primary" id="btn_accion_proyectoEdificio_eliminar" data-dismiss="modal"><?php echo trans('main.eliminar') ?> <span></span></button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->