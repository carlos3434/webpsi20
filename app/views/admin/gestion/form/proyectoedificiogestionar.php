<?php
/**
 * partial: admin.gestion.form
 * 
 * Se coloca manualmente los ID de los siguientes TIPOS en cada FORM, etiqueta: "itemid"
 */
?>
<div class="row">
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.codigo_proyecto') ?></label>
        <input type="text" class="form-control" id="txt_proyecto" name="txt_proyecto" readonly>
    </div>
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.numero_departamentos') ?></label>
        <input type="text" class="form-control" id="txt_numero_departamentos" name="txt_numero_departamentos" readonly>
    </div>
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.fecha_termino_cons.') ?></label>
        <input type="text" class="form-control" id="txt_fecha_termino" name="txt_fecha_termino" readonly>
    </div>
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.segmento') ?></label>
        <input type="text" class="form-control" id="txt_segmento" name="txt_segmento" readonly>
    </div>
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.tipo') ?></label>
        <input type="text" class="form-control" id="txt_tipo_proyecto" name="txt_tipo_proyecto" readonly>
    </div>
    <div class="col-sm-2 col-xs-6">
        <label><?php echo trans('gestion.avance') ?> (%)</label>
        <input type="text" class="form-control" id="txt_avance" name="txt_avance" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <label><?php echo trans('gestion.contacto') ?></label>
        <input type="text" class="form-control" id="txt_contacto" name="txt_contacto" readonly>
    </div>
    <div class="col-sm-4 col-xs-12">
        <label><?php echo trans('gestion.proyecto_estado') ?></label>
        <input type="text" class="form-control" id="txt_proyecto_estado" name="txt_proyecto_estado" readonly>
    </div>
    <div class="col-sm-2  col-xs-6">
        <label><?php echo trans('gestion.viven') ?></label>
        <input type="text" class="form-control" id="txt_viven" name="txt_viven" readonly>
    </div>
    <div class="col-sm-2  col-xs-6">
        <label><?php echo trans('gestion.troba') ?></label>
        <input type="text" class="form-control" id="txt_troba" name="txt_troba" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <label>Gestión:</label>
        <select class="form-control" id="slct_gcasuistica_modal" name="slct_gcasuistica_modal">
            <option value="">.::Seleccione::.</option>
        </select>
    </div>
    <div class="col-sm-6">
        <label>Casuistica:</label>
        <select class="form-control" id="slct_gcasuistica_detalle_modal" name="slct_gcasuistica_detalle_modal">
            <option value="">.::Seleccione::.</option>
        </select>
    </div>
</div>
<br>
<section id="tab_1_section">
    <form id="form_proyecto_iniciativa" name="form_proyecto_iniciativa" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="6">
        <div class="row form-group">
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecha_seguimiento') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_seguimiento" id="txt_fecha_seguimiento"/>
                </div>
                <div class="col-sm-3">
                    <label><?php echo trans('gestion.fecha_termino') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_termino" id="txt_fecha_termino"/>
                </div>
            </div>
        </div>
    </form>
    <form id="form_proyecto_diseno" name="form_proyecto_diseno" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="1">
        <div class="row form-group">
            <div class="col-sm-12" >
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecha_inicio') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_inicio" id="txt_fecha_inicio" readonly=""/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecha_compromiso') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecha_gestion') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_gestion" id="txt_fecha_gestion"/>
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.estado_general') ?></label>
                    <input type="text" class="form-control" name="txt_estado_general" id="txt_estado_general">
                </div>


                <div class="col-sm-2">
                    <label><?php echo trans('gestion.gestion_interna') ?></label>
                    <input type="text" class="form-control" name="txt_gestion_interna" id="txt_gestion_interna">
                </div>
                <div class="col-sm-2">
                    <label><?php echo trans('gestion.fecha_termino') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_termino" id="txt_fecha_termino"/>
                </div>
            </div>
        </div>
    </form>
    <form id="form_proyecto_licencia" name="form_proyecto_licencia" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="2">
        <div class="row form-group">
            <div class="col-sm-12" >
                <div class="col-sm-3">
                    <label>Fecha Inicio:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_inicio" id="txt_fecha_inicio" readonly=""/>
                </div>
                <div class="col-sm-3">
                    <label>Recep. Exped.:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_recepcion_expediente" id="txt_recepcion_expediente"/>
                </div>
                <div class="col-sm-3">
                    <label>Fecha Compromiso:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                </div>
                <div class="col-sm-3">
                    <label><?php echo trans('gestion.fecha_termino') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_termino" id="txt_fecha_termino"/>
                </div>
            </div>
        </div>
    </form>                       
    <form id="form_proyecto_oocc" name="form_proyecto_oocc" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="3">
        <div class="row form-group">
            <div class="col-sm-12" >
                <div class="col-sm-3">
                    <label>Fecha Inicio:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_inicio" id="txt_fecha_inicio" readonly=""/>
                </div>
                <div class="col-sm-3">
                    <label>Fecha Compromiso:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                </div>
                <div class="col-sm-3">
                    <label><?php echo trans('gestion.fecha_termino') ?></label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_termino" id="txt_fecha_termino"/>
                </div>
            </div>
        </div>
    </form>
    <form id="form_proyecto_cableado" name="form_proyecto_cableado" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="4">
        <div class="row form-group">
            <div class="col-sm-12" >
                <div class="col-sm-3">
                    <label>Fecha Inicio:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_inicio" id="txt_fecha_inicio" readonly="">
                </div>
                <div class="col-sm-3">
                    <label>Fecha Compromiso:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_compromiso" id="txt_fecha_compromiso"/>
                </div>
                <?php /*
                  <div class="col-sm-3">
                  <label>Fecha Teo.Final:</label>
                  <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_teorica_final" id="txt_fecha_teorica_final"/>
                  </div>
                 */ ?>
                <div class="col-sm-3">
                    <label>Fecha Termino:</label>
                    <input type="text" class="form-control pull-right" placeholder="<?php echo $format_db ?>" name="txt_fecha_termino" id="txt_fecha_termino"/>
                </div>
            </div>
        </div>
    </form>
    <form id="form_proyecto_validacionreversa" name="form_proyecto_validacionreversa" action="" method="post" class="form-proyecto-tipo" autocomplete="off" itemid="7">
        <div class="row form-group"> 
            <div class="col-sm-12">
                <div class="col-sm-4">
                    <label><?php echo trans('gestion.termino') ?></label><br>
                    <!--                    <div class="radio">
                                            <label>
                                                <input type="radio" name="txt_estado_general" id="txt_estado_general_no" value="NO" checked>
                                                NO
                                            </label>
                                        </div>-->
                    <div class="radio">
                        <label>
                            <input type="radio" name="txt_estado_general" id="txt_estado_general_si" value="SI">
                            SI
                        </label>
                    </div>
                </div>
                <div class="col-sm-3" style="display: none;">
                    <label><?php echo trans('gestion.fecha_termino') ?></label>
                    <input type="text" class="form-control pull-right" name="txt_fecha_termino" id="txt_fecha_termino" readonly=""/>
                </div>
            </div>
        </div>
    </form>
</section>
<?php echo \View::make('admin.gestion.form.proyectoedificioprovisional'); ?>

<hr>
<?php echo \View::make('admin.gestion.form.proyectoedificiomegaproyecto'); ?>
<!--<div class="row">
    <div class="col-sm-3">
<!-- <label >
    <input type="checkbox" id="chk_1" name="chk_1" readonly=""> 
</label> 
<button class="btn btn-me btn-danger" type="button" id="btnAccionMega" data-toggle="collapse" data-target="#collapsePEM" aria-expanded="false" aria-controls="collapsePEM">
<?php //echo trans('gestion.megaproyecto') ?>
</button>
</div>
</div>
<div class="collapse" id="collapsePEM">
<div class="well" id="divProyectoMega">
</div>
</div>-->

<div class="modal-footer">
    <!-- <div class="col-sm-9"> -->
    <?php echo \View::make('admin.gestion.form.proyectoedificiototales'); ?>
    <!-- </div> -->
    <button type="button" id="btn_proceso_disenio_modal"  class="btn btn-success" data-dismiss="modal"><?php echo trans('gestion.disenio') ?></button>
    <div class="checkbox" style="display: inline-block;margin-right: 20px;">
        <label>
            <input type="checkbox" id="chk_retornar" name="chk_retornar"> <span class="label label-danger" style="font-size:1em;"><?php echo trans('gestion.retornar') ?></span>
        </label>
    </div>
    <button type="button"  class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
    <button type="button" id="btn_gestion_modal" name="btn_gestion_modal" class="btn btn-primary"><?php echo trans('main.Save') ?></button>
</div>