<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
@include( 'admin.gestion.js.edificiomantenimiento_ajax' )
@include( 'admin.gestion.js.edificiomantenimiento' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Auditoria Usuarios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Seguridad</a></li>
        <li class="active">Auditoria Usuarios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            
            <div>
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active" role="tab" data-toggle="tab">
                        <a href="#tab_proyectoEstado">Proyecto Estado</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tab_proyectoTipo">Proyecto Tipo</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tab_casuistica">Casuistica</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tab_casuisticaproyectoestado">Casuistica Proyecto Estado</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tab_proyectoEstado">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_proyectoEstados" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_proyectoEstados">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <a class='btn btn-primary btn-sm' class="btn btn-primary" 
                               data-toggle="modal" data-target="#proyectoestadoModal" data-accion="registrar"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_proyectoTipo">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_proyectoTipo" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Proyecto</th>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_proyectoTipo">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Proyecto</th>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <a class='btn btn-primary btn-sm' class="btn btn-primary" 
                               data-toggle="modal" data-target="#proyectotipoModal" data-accion="registrar"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tab_casuistica">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_casuistica" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_casuistica">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <a class='btn btn-primary btn-sm' class="btn btn-primary" 
                               data-toggle="modal" data-target="#casuisticaModal" data-accion="registrar"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tab_casuisticaproyectoestado">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_casuisticaproyectoestado" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Proyecto Estado</th>
                                        <th>Casuistica</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_casuisticaproyectoestado">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Proyecto Estado</th>
                                        <th>Casuistica</th>
                                        <th>Estado</th>
                                        <th>[ . ]</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <a class='btn btn-primary btn-sm' class="btn btn-primary" 
                               data-toggle="modal" data-target="#casuisticaProyectoEstadoModal" data-accion="registrar"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')
    @include( 'admin.gestion.form.proyectoEstado' )
    @include( 'admin.gestion.form.proyectoTipo' )
    @include( 'admin.gestion.form.casuistica' )
    @include( 'admin.gestion.form.casuisticaProyectoEstado' )
@stop
