<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}

{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}

{{ HTML::style('css/admin/proyecto_edificio.css') }}

{{ HTML::script('js/psi.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.gestion.js.proceso_disenio_ajax' )
@include( 'admin.gestion.js.proceso_disenio' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Diseño
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Procesos</a></li>
        <li class="active">Diseño</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <form id="form_proceso_disenio" name="form_proceso_disenio" action="" method="post" class="" autocomplete="off">

        <div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-default">
                        <div class="box-body">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="proyecto_edificio_id">ITEM</label>
                                    <input type="text" class="form-control" id="proyecto_edificio_id" placeholder="0">
                                </div>
                                <a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" id="btnEdificioId">{{ trans('gestion.comprobar') }}</a>
                                <span style="margin-left:10px;" id="lbl_edificio_nombre"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#PD0" aria-controls="PD0" role="tab" data-toggle="tab">Inicio</a>
                </li>
                <li role="presentation">
                    <a href="#PD1" aria-controls="PD1" role="tab" data-toggle="tab">MML</a>
                </li>
                <li role="presentation">
                    <a href="#PD2" aria-controls="PD2" role="tab" data-toggle="tab">MUNICIPAL DISTRITAL</a>
                </li>
                <li role="presentation">
                    <a href="#PD3" aria-controls="PD3" role="tab" data-toggle="tab">GTU</a>
                </li>
                <li role="presentation">
                    <a href="#PD4" aria-controls="PD4" role="tab" data-toggle="tab">ORDENANZA 341</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="PD0">

                </div>
                <!-- //PRIMER::TAB -->
                <div role="tabpanel" class="tab-pane" id="PD1">

                </div>
                <!-- //SEGUNDO::TAB -->
                <div role="tabpanel" class="tab-pane" id="PD2">

                </div>
                <!-- //TERCER::TAB -->
                <div role="tabpanel" class="tab-pane" id="PD3">

                </div>
                <!-- //CUARTO::TAB -->
                <div role="tabpanel" class="tab-pane" id="PD4">

                </div>
                <!-- //QUINTO::TAB -->
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 text-center">
                <input type="submit" class="btn btn-lg btn-info btn-flat" value="{{ trans('main.Save') }}" >
                <button class="btn btn-lg btn-info btn-flat">{{ trans('main.Save') }}</button>
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body gestion_msn">
                </div>
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->

{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
@stop
