<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

        @section('autor')
        <meta name="author" content="Jorge Salcedo (Shevchenko)">
        @show

        <link rel="shortcut icon" href="favicon.ico">

        @section('descripcion')
        <meta name="description" content="">
        @show
        <title>
            @section('titulo')
            PSI 2.0
            @show
        </title>

        @section('includes')
        <?php echo HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css'); ?>
        <?php echo HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css'); ?>

        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('lib/jquery-ui-1.11.2/jquery-ui.min.js') }}
        {{ HTML::script('lib/bootstrap-3.3.1/js/bootstrap.min.js') }}
        <!-- //code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css -->
        <?php echo HTML::style('css/master/ionicons.min.css'); ?>
        <?php echo HTML::style('lib/datatables-1.10.4/media/css/dataTables.bootstrap.css'); ?>
        <?php echo HTML::style('css/admin/admin.css'); ?>
        <?php echo HTML::style('css/admin/horarios.css'); ?>

        {{ HTML::script('lib/datatables-1.10.4/media/js/jquery.dataTables.js') }}
        {{ HTML::script('lib/datatables-1.10.4/media/js/dataTables.bootstrap.js') }}
        {{ HTML::script('js/psi.js') }}
        @include( 'admin.js.app' )
        @show


        <script type="text/javascript">
            // Función para determinar cuando cambia la url de la página.
            $(document).ready(function (){
//                if (typeof window.history.pushState == 'function') {
//                    pushstate();
//                    window.location = "admin.seguridad.datos";
//                } else {
//                    window.location = "admin.seguridad.datos";
//                }
                
                
                var URLactual = window.location.href;
                if(URLactual != "http://localhost/webpsi20/public/admin.seguridad.datos"){
                    window.location = "admin.seguridad.datos";
                }
                alert(URLactual);
            });
            
            
            function pushstate(){
                var links = $("a");
                // Evento al hacer click.
                links.live('click', function(event) {
                    var url = $(this).attr('href');
                    // Cambio el historial del navegador.
                    history.pushState({ path: url }, url, url);
                    // Muestro la nueva url
                    alert(url);
                    return false;
                });

                // Función para determinar cuando cambia la url de la página.
                $(window).bind('popstate', function(event) {
                    var state = event.originalEvent.state;
                    if (state) {
                        // Mostrar url.
                        alert(state.path);
                    }
                });
            }
            
        </script>
    </head>

    <body class="skin-blue">
        <div id="msj" class="msjAlert"> </div>


        <header class="header">
            <a href="#" class="logo">
                PSI 2.0
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">             
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">                            
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Auth::user()->usuario }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue" data-toggle="modal" data-target="#imagenModal">
                                    @if (Auth::user()->imagen!=null)
                                    <img src="img/user/<?= md5('u' . Auth::user()->id) . '/' . Auth::user()->imagen; ?>" class="img-circle" alt="User Image" />
                                    @else
                                    <img src="" class="img-circle" alt="User Image" />
                                    @endif
                                    <p>
                                        {{ Auth::user()->usuario }}
                                        <small>{{ trans('greetings.desde_usuario') }} <?php echo date("Y-m-d", strtotime(Auth::user()->created_at)); ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="salir" class="btn btn-default btn-flat">{{ trans('greetings.cerrar_session') }}</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">                        
                        <div class="pull-left image" data-toggle="modal" data-target="#imagenModal">
                            @if (Auth::user()->imagen!=null)
                            <img src="img/user/<?= md5('u' . Auth::user()->id) . '/' . Auth::user()->imagen; ?>" class="img-circle" alt="User Image" />
                            @else
                            <img src="" class="img-circle" alt="User Image" />
                            @endif
                        </div>

                        <div class="pull-left info">
                            <p>Hola, {{ Auth::user()->usuario }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('greetings.inicio_sesion') }}</a>
                        </div>
                    </div>

                </section>
                <!-- /.sidebar -->
            </aside>

            @include( 'layouts.form.imagen' )

            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        <!--{{ trans('greetings.menu_info_actualizar') }}-->
                        Actualizar Contraseña, para acceder al Sistema
                        <small> </small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-6">    
                            <h2>
                                {{ trans('greetings.menu_info_actualizar') }}
                                <small> </small>
                            </h2>            
                            <!-- Inicia contenido -->
                            <div class="row">

                                <!-- form start -->
                                <form role="form" method="post" action="" name="form_misdatos" id="form_misdatos">
                                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                                    <div class="col-md-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <!-- <h3 class="box-title">Detalle</h3> -->
                                            </div><!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label><i class="fa fa-asterisk"></i>&nbsp;{{ trans('greetings.ingrese_contraseña') }}:
                                                        <a id="error_password" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="">
                                                            <i class="fa fa-exclamation"></i>
                                                        </a>
                                                    </label>
                                                    <input type="password" class="form-control" id="txt_password" name="txt_password" placeholder="{{ trans('greetings.ingrese_contraseña') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>{{ trans('greetings.contraseña_nueva') }}:
                                                        <a id="error_newpassword" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="">
                                                            <i class="fa fa-exclamation"></i>
                                                        </a>
                                                    </label>
                                                    <input type="password" class="form-control" id="txt_newpassword" name="txt_newpassword" placeholder="{{ trans('greetings.contraseña_nueva') }}">
                                                    <i>OBS. Contraseña debe contener: Texto y Número</i>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{ trans('greetings.confirm_contraseña_nueva') }}:</label>
                                                    <input type="password" onkeyup="return validaEnter(event, 'btn_guardar');" class="form-control" id="txt_confirm_new_password" name="txt_confirm_new_password" placeholder="{{ trans('greetings.confirm_contraseña_nueva') }}">
                                                    <i>OBS. Contraseña debe contener: Texto y Número</i>
                                                </div>                       
                                            </div><!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="button" class="btn btn-primary" id="btn_guardar"><i class='fa fa-save fa-lg'></i>&nbsp;&nbsp;{{ trans('greetings.save') }}</button>
                                            </div>

                                        </div>
                                    </div><!--/.col (right) -->

                                </form>
                            </div>
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->

        @yield('formulario')
    </body>

    <?php
    echo '<script type="text/javascript">';
    echo "var envioValida=0;
            var agregarG='';
            var editarG='';
            var eliminarG='';";
    echo "$('ul.sidebar-menu li').each(function(indice, elemento) {
                htm=$(elemento).html();
                if(htm.split('<a href=" . '"' . $valida_ruta_url . '"' . "').length>1){
                    $(elemento).addClass('active');
                    poshtm=$(elemento).find('a[href=" . '"' . $valida_ruta_url . '"' . "]');
                    var accesosG=poshtm.attr('data-accesos');
                    var hash=poshtm.attr('data-clave');
                    agregarG=accesosG.substr(0,1);
                    editarG=accesosG.substr(1,1);
                    eliminarG=accesosG.substr(2,1);

                    var datos={agregarG:agregarG,editarG:editarG,eliminarG:eliminarG,hash:hash};
                    
                    if(envioValida==0){
                        $.ajax({
                            url         : 'usuario/validaacceso',
                            type        : 'POST',
                            cache       : false,
                            dataType    : 'json',
                            async        : false,
                            data        : datos,
                            beforeSend : function() {                 
                            },
                            success : function(obj) {
                                envioValida++;
                                if(obj.rst!='1'){
                                    alert('Ud no cuenta con permisos para acceder; Ingrese nuevamente');
                                    window.location='salir';
                                }
                            },
                            error: function(){
                            }
                        });

                    }
                }
            });

            ";
    echo '</script>';


    //$hash= hash('256', Config::get('wpsi.permisos.key').);
    ?>
</html>