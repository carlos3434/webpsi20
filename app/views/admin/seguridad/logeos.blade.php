<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.seguridad.js.logeos_ajax' )
@include( 'admin.seguridad.js.logeos' )
@include( 'admin.seguridad.js.usuarioopciones_ajax' )
@include( 'admin.seguridad.js.usuarioopciones' )
@include( 'admin.seguridad.js.accesocriticos_ajax' )
@include( 'admin.seguridad.js.accesocriticos' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Auditoria Usuarios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Seguridad</a></li>
        <li class="active">Auditoria Usuarios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            
            <div>
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active" role="tab" data-toggle="tab">
                        <a href="#tb_logeoUsuarios">Logeo Usuarios</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tb_accsesoUsuarios">Acceso Usuarios</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tb_auditoriaUsuarios">Auditoria Usuarios</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tb_auditoriaAccesoCriticos">Auditoria Acceso Criticos</a>
                    </li>
                    <li role="presentation" role="tab" data-toggle="tab">
                        <a href="#tb_auditoriaInactivos">Auditoria Inactivos (90 días)</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tb_logeoUsuarios">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_logeos" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Usuario</th>
                                        <th>Fecha de Login</th>
                                        <th>Estacion</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_logeo">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Usuario</th>
                                        <th>Fecha de Login</th>
                                        <th>Estacion</th>
                                        <th>Estado</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tb_accsesoUsuarios">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_usuarioOpciones" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Apellidos y Nombre</th>
                                        <th>Usuario</th>
                                        <th>Perfil</th>
                                        <th>Estado</th>
                                        <th class="editarG"> [ ] </th>
                                    </tr>
                                </thead>
                                <tbody id="tb_usuarioOpcion">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Apellidos y Nombre</th>
                                        <th>Usuario</th>
                                        <th>Perfil</th>
                                        <th>Estado</th>
                                        <th class="editarG"> [ ] </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tb_auditoriaUsuarios">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_usuarioAuditorias" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Pefil</th>
                                        <th>Creador del Usuario</th>
                                        <th>F. Creación</th>
                                        <th>Actualizador del Usuario</th>
                                        <th>F. Actualización</th>
                                        <th>Estacion</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_usuarioAuditoria">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Pefil</th>
                                        <th>Creador del Usuario</th>
                                        <th>F. Creación</th>
                                        <th>Actualizador del Usuario</th>
                                        <th>F. Actualización</th>
                                        <th>Estacion</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tb_auditoriaAccesoCriticos">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_auditoriaAccesoCriticos" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>F. Acceso</th>
                                        <th>Modulo</th>
                                        <th>Sub Modulo</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_auditoriaAccesoCritico">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>F. Acceso</th>
                                        <th>Modulo</th>
                                        <th>Sub Modulo</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                
                <div role="tabpanel" class="tab-pane fade" id="tb_auditoriaInactivos">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtros</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="t_usuarioIdentificadores" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Apellidos y Nombre</th>
                                        <th>Usuario</th>
                                        <th>Estacion</th>
                                        <th>Fecha</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_usuarioIdentificador">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Apellidos y Nombre</th>
                                        <th>Usuario</th>
                                        <th>Estacion</th>
                                        <th>Fecha</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')
@include( 'admin.seguridad.form.usuarioopciones' )
@stop
