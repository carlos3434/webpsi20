<div class="modal fade" id="usuarioopcionesDetalleModal" tabindex="-1" role="dialog" aria-labelledby="usuarioopcionesDetalleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header logo">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="errorComentarioModalLabel"><?php echo trans('mantenimiento.escribir_solucion') ?></h4>
            </div>
            <div class="modal-body">
                <div id="resultadoOfsc" style="word-wrap: break-word;padding: 5px;background-color: #D0FED9;border:1px solid #000;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                   <?php echo trans('main.Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>