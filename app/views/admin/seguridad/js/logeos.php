<script type="text/javascript">
    $(document).ready(function () {
        Logeos.CargarLogeoUsuario();
        $('#t_logeos').dataTable();
        // Tabla contenida en el Modal dentro de form usuarioidentificador.php
        $("#t_usuarioIdentificadores").dataTable();
        
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            
            if(($(this).attr('href') == "#tb_logeoUsuarios")) {
                $("#t_logeos").dataTable().fnDestroy();
                Logeos.CargarLogeoUsuario();
            }
            else if(($(this).attr('href') == "#tb_accsesoUsuarios")) {
                UsuarioOpciones.CargarUsuarioOpciones();
            }
            else if(($(this).attr('href') == "#tb_auditoriaUsuarios")) {
                UsuarioOpciones.MostrarAuditoriaDetalle();
            }
            else if(($(this).attr('href') == "#tb_auditoriaAccesoCriticos")) {
                AccesoCriticos.ListarAuditoriaAccesoCriticos();
            }
            else {
                UsuarioOpciones.MostrarIdentificadorDetalle();
            }
        });
    })
</script>