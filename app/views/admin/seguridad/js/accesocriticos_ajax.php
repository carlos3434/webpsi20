<script>
    var AccesoCriticos = {
        // pinta la fecha y hora de los accesos a los submodulos
        // por parte de los Usuarios.
        ListarAuditoriaAccesoCriticos: function (){
            $.ajax({
                url: "submodulousuario/auditoriaaccesos",
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },  
                success: function (obj) {
                    var html = "";
                    $.each(obj.datos, function(index, dato){
                        html += "<tr>";
                        html += "<td>"+dato.usuario+"</td>";
                        html += "<td>"+dato.fecha_acceso+"</td>";
                        html += "<td>"+dato.modulo+"</td>";
                        html += "<td>"+dato.submodulo+"</td>";
                        html += "</tr>";
                    })
                    
                    $("#t_auditoriaAccesoCriticos").dataTable().fnDestroy();
                    $("#tb_auditoriaAccesoCritico").html(html);
                    $("#t_auditoriaAccesoCriticos").dataTable();

                    $(".overlay,.loading-img").remove();
                }
            })
        }
    }
</script>