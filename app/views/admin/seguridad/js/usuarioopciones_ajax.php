<script>
var UsuarioOpciones = {
    CargarUsuarioOpciones: function (){
        $.ajax({
            url: "usuario/usuarioopciones",
            type: 'POST',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },                       
            success: function (obj) {
                var html = "";
                var estadoUsuario = "";
                $.each(obj.data, function(index, usuarioOpt){
                    
                    estadoUsuario = '<span class="btn btn-success">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                    if(usuarioOpt.estado == 0){
                        estadoUsuario = '<span class="btn btn-danger"> Inactivo</span>';
                    }
                    html += "<tr>";
                    html += "<td>"+usuarioOpt.empresa+"</td>";
                    html += "<td>"+usuarioOpt.apellido+", "+usuarioOpt.nombre+"</td>";
                    html += "<td>"+usuarioOpt.usuario+"</td>";
                    html += "<td>"+usuarioOpt.perfil+"</td>";
                    html += "<td>"+estadoUsuario+"</td>";
                    html += '<td><button class="btn btn-primary" data-toggle="modal" data-target="#usuarioopcionesDetalleModal" data-whatever="Detalle" onclick="UsuarioOpciones.MostrarOpcionesDetalle(' + usuarioOpt.id + ');"><i class="fa fa-eye"></i></button></td>';
                    html += "</tr>";
                })
                
                $("#t_usuarioOpciones").dataTable().fnDestroy();
                $("#tb_usuarioOpcion").html(html);
                $("#t_usuarioOpciones").dataTable();
                
                $(".overlay,.loading-img").remove();
            }
        })
    },
    
    MostrarOpcionesDetalle: function (id){
        $.ajax({
            url: "usuario/detaopciones",
            type: 'POST',
            cache: false,
            data: "id=" + id,
            dataType: 'json',
            beforeSend: function () {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },  
            success: function (obj) {
                var html = "";
                var htmlo2 = "";
                $("#usuarioopcionesDetalleModal").find('.modal-title').text("Detalle Opciones");
                $.each(obj.dataMod, function (index, detaOpt){
                    
                    html += '<strong>' + detaOpt.modulo + '</strong><br/>';
                    
                    $.each(obj.dataSubMod, function (index, detaOpt2){
                        if (detaOpt.id === detaOpt2.modulo_id) {
                            html += '<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- ' + detaOpt2.submenu + '</i><br/>';
                        }
                    })
                });
                
                $("#resultadoOfsc").html(html);
                
                $(".overlay,.loading-img").remove();
            }
        })
    },
    
    MostrarIdentificadorDetalle: function (){
        $.ajax({
            url: "usuario/detaidentificador",
            type: 'POST',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },  
            success: function (obj) {
                var html = "";
                var estadoUsuario = "";
                $("#usuarioidentificadorModal").find('.modal-title').text("90 días sin Actividad, los Usuarios son:");
                $.each(obj.data, function(index, usuarioIdent){
                    if(usuarioIdent.fecha_login == "0000-00-00 00:00:00"){
                        if (usuarioIdent.difCreated > 90){
                            html += "<tr>";
                            html += "<td>"+usuarioIdent.empresa+"</td>";
                            html += "<td>"+usuarioIdent.apellido+", "+usuarioIdent.nombre+"</td>";
                            html += "<td>"+usuarioIdent.usuario+"</td>";
                            html += "<td>"+usuarioIdent.estacion+"</td>";
                            html += "<td>"+usuarioIdent.created_at+"</td>";
                            html += "</tr>";
                        }
                    } else {
                        if (usuarioIdent.difLogin > 90){
                            html += "<tr>";
                            html += "<td>"+usuarioIdent.empresa+"</td>";
                            html += "<td>"+usuarioIdent.apellido+", "+usuarioIdent.nombre+"</td>";
                            html += "<td>"+usuarioIdent.usuario+"</td>";
                            html += "<td>"+usuarioIdent.estacion+"</td>";
                            html += "<td>"+usuarioIdent.fecha_login+"</td>";
                            html += "</tr>";
                        }
                    }
                })
                $("#t_usuarioIdentificadores").dataTable().fnDestroy();
                $("#tb_usuarioIdentificador").html(html);
                $("#t_usuarioIdentificadores").dataTable();
                
                $(".overlay,.loading-img").remove();
            }
        })
    },
        
    MostrarAuditoriaDetalle: function (){
        $.ajax({
            url: "usuario/auditoriaacciones",
            type: 'POST',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },  
            success: function (obj) {
                var html = "";
                var usCreador = "";
                var fechaCreacion = "";
                var usActualizador = "";
                var fechaActualizador = "";
                var estacion = "";
                $.each(obj.data, function(index, usuarioAudi) {
                    usCreador = usuarioAudi.ucrea;
                    if(usuarioAudi.ucrea == null){
                        usCreador = "-";
                    }
                    
                    fechaCreacion = usuarioAudi.fcreate;
                    if(usuarioAudi.fcreate == null){
                        fechaCreacion = "-";
                    }
                    
                    usActualizador = usuarioAudi.uactualiza;
                    if(usuarioAudi.uactualiza == null){
                        usActualizador = "-";
                    }
                    
                    fechaActualizador = usuarioAudi.fupdate;
                    if(usuarioAudi.fupdate == null){
                        fechaActualizador = "-";
                    }
                    
                    estacion = usuarioAudi.usuario_estacion;
                    if(usuarioAudi.usuario_estacion == null){
                        estacion = "-";
                    }
                    
                    html += "<tr>";
                    html += "<td>"+usuarioAudi.usuario+"</td>";
                    html += "<td>"+usuarioAudi.perfil+"</td>";
                    html += "<td style='color: darkgreen'>"+usCreador+"</td>";
                    html += "<td style='color: darkgreen'>"+fechaCreacion+"</td>";
                    html += "<td style='color: brown'>"+usActualizador+"</td>";
                    html += "<td style='color: brown'>"+fechaActualizador+"</td>";
                    html += "<td><b>"+estacion+"</b></td>";
                    html += "</tr>";
                })
                
                $("#t_usuarioAuditorias").dataTable().fnDestroy();
                $("#tb_usuarioAuditoria").html(html);
                $("#t_usuarioAuditorias").dataTable();
                
                $(".overlay,.loading-img").remove();
            }
        })
    },
}

</script>