<script>
var Logeos = {
    CargarLogeoUsuario: function (){
        $.ajax({
            url: "usuario/logeousuarios",
            type: 'POST',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },                       
            success: function (obj) {
                var html = "";
                var estadoLogeo = "";
                var fechaLogin = "";
                var estacion = "";
                $.each(obj.data, function(index, logeo) {
                    estadoLogeo = '<span class="btn btn-danger"> Inactivo</span>';
                    if(logeo.estado == 1){
                        estadoLogeo = '<span class="btn btn-success">&nbsp;&nbsp;Activo&nbsp;&nbsp;</span>';
                    }
                    fechaLogin = logeo.fecha_login;
                    if(logeo.fecha_login === null || logeo.fecha_login === "0000-00-00 00:00:00"){
                        fechaLogin = "Usuario sin Login";
                    }
                    estacion = logeo.estacion
                    if(logeo.estacion === null){
                        estacion = "Estación no Definida";
                    }
                    html += "<tr>";
                    html += "<td>"+logeo.nombre+"</td>";
                    html += "<td>"+logeo.apellido+"</td>";
                    html += "<td>"+logeo.usuario+"</td>";
                    html += "<td>"+fechaLogin+"</td>";
                    html += "<td>"+estacion+"</td>";
                    html += "<td>"+estadoLogeo+"</td>";
                    html += "</tr>";
                })
                $("#t_logeos").dataTable().fnDestroy();
                $("#tb_logeo").html(html);
                $("#t_logeos").dataTable();
                
                $(".overlay,.loading-img").remove();
            }
        })
    }
}

</script>