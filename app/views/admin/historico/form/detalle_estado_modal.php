<!-- /.modal -->
<div class="modal fade" id="detalleestadoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Detalle de estado</h4>
            </div>
            <div class="modal-body">
                <form id="form_detalleestado" name="form_detalleestado" action="" method="post">
                    <div class="row">
                            <div class="form-group">
                             <div class="col-sm-6">
                                <label class="control-label">Estado Ofsc
                                    <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                        <i class="fa fa-exclamation"></i>
                                    </a>
                                </label>
                                <input type="text" class="form-control" name="txt_estado_ofsc" id="txt_estado_ofsc" disabled="disabled">
                             </div>
                             <div class="col-sm-6">
                                <label class="control-label">Tipo
                                    <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                        <i class="fa fa-exclamation"></i>
                                    </a>
                                </label>
                                <input type="text" class="form-control" name="txt_tipo_motivo" id="txt_tipo_motivo" disabled="disabled">
                             </div>
                            </div>
                    </div>
                    <div class="row">
                            <div class="form-group">
                             <div class="col-sm-6">
                                <label class="control-label">Codigo Motivo
                                    <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                        <i class="fa fa-exclamation"></i>
                                    </a>
                                </label>
                                <input type="text" class="form-control" name="txt_codigo" id="txt_codigo" disabled="disabled">
                             </div>
                             <div class="col-sm-6">
                                <label class="control-label">Motivo
                                    <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                        <i class="fa fa-exclamation"></i>
                                    </a>
                                </label>
                                <input type="text" class="form-control" name="txt_descripcion" id="txt_descripcion" disabled="disabled">
                             </div>
                            </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->