<!-- /.modal -->
<div class="modal fade" id="ofscModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <button class="btn btn-m btn-default pull-right" data-dismiss="modal">
                <i class="fa fa-close"></i>
            </button>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs logo modal-header">
                    <li class="logo tab_0 active">
                        <a href="#tab_ofsc_0" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-lg"></i></button>
                            Detalle
                        </a>
                    </li>
                    <li class="logo tab_1">
                        <a href="#tab_ofsc_1" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-clock-o fa-lg"></i></button>
                            Movimientos
                        </a>
                    </li>
                </ul>
                <div class="tab-content" style="overflow: scroll;height:500px;">
                    <div class="tab-pane active" id="tab_ofsc_0">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody id="detalleOfsc"></tbody>
                            </table>
                        </div>
                        <div class="row" style="padding: 0 15px;">
                            <div class="col-md-6" id="maps_ofsc" style="min-height: 369px"></div>
                            <div class="col-md-6" id="street_ofsc" style="min-height: 369px"></div>
                            <div class="col-md-6" id="street_ofsc2" style="min-height: 369px; display: none;"></div>
                            <div class="col-md-6" id="street_ofsc3" style="min-height: 369px; display: none;"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_ofsc_1">
                        <table id="t_movimiento_ofsc" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>F. Movimiento</th>
                                <th>Empresa</th>
                                <th>Cliente</th>
                                <th>Fecha Agenda</th>
                                <th>Celula</th>
                                <th>Tecnico</th>
                                <th>Estado</th>
                                <th>Usuario</th>
                                <th> Observación</th>
                            </tr>
                            </thead>
                            <tbody id="tb_movimiento_ofsc">
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>F. Movimiento</th>
                                <th>Empresa</th>
                                <th>Cliente</th>
                                <th>Fecha Agenda</th>
                                <th>Celula</th>
                                <th>Tecnico</th>
                                <th>Estado</th>
                                <th>Usuario</th>
                                <th> Observación</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo trans('main.Close') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
