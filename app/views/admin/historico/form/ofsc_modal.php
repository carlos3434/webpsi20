<!-- /.modal -->
<div class="modal fade" id="ofscModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs logo modal-header nav-ofsc-modal">
                    <li role="presentation" class="logo tab_0 active">
                        <a href="#tab_ofsc_0" aria-controls="tab_ofsc_0" role="tab" data-toggle="tab" title="DETALLE">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-lg"></i></button>
                            <small>DETALLE</small>
                        </a>
                    </li>
                    <li role="presentation" class="logo tab_1">
                        <a id="ofscModalMovimiento" href="#tab_ofsc_1" aria-controls="tab_ofsc_1" role="tab" data-toggle="tab" title="MOVIMIENTOS">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-clock-o fa-lg"></i></button>
                            <small>MOVIMIENTOS</small>
                        </a>
                    </li>
                    <li role="presentation" class="logo tab_2">
                        <a href="#tab_ofsc_2" aria-controls="tab_ofsc_2" role="tab" data-toggle="tab" title="COMPONENTES" title="COMPONENTES">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-th-large fa-lg"></i></button>
                            <small>COMPONENTES</small>
                        </a>
                    </li>
                    <li role="presentation" class="pull-right">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </li>
                </ul>
                <div class="tab-content tab-content-ofsc-modal" style="overflow: scroll;height:500px;">
                    <div role="tabpanel" class="tab-pane active" id="tab_ofsc_0">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody id="detalleOfsc"></tbody>
                            </table>
                        </div>
                        <div class="row" style="padding: 0 15px;">
                            <div class="col-md-6" id="maps_ofsc" style="min-height: 369px"></div>
                            <div class="col-md-6" id="street_ofsc" style="min-height: 369px"></div>
                            <div class="col-md-6" id="street_ofsc2" style="min-height: 369px; display: none;"></div>
                            <div class="col-md-6" id="street_ofsc3" style="min-height: 369px; display: none;"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab_ofsc_1">
                        <table id="t_movimiento_ofsc" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>F. Movimiento</th>
                                <th>Empresa</th>
                                <th>Cliente</th>
                                <th>Fecha Agenda</th>
                                <th>Celula</th>
                                <th>Tecnico</th>
                                <th>Estado</th>
                                <th>Usuario</th>
                                <th> Observación</th>
                            </tr>
                            </thead>
                            <tbody id="tb_movimiento_ofsc">
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>F. Movimiento</th>
                                <th>Empresa</th>
                                <th>Cliente</th>
                                <th>Fecha Agenda</th>
                                <th>Celula</th>
                                <th>Tecnico</th>
                                <th>Estado</th>
                                <th>Usuario</th>
                                <th> Observación</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab_ofsc_2">
                        <fieldset>
                            <ul class="list-group" id="componente-ofsc-modal">
                                <li class="list-group-item">Cras justo odio</li>
                            </ul>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo trans('main.Close') ?></button>
            </div>

        </div>
    </div>
</div>
<!-- /.modal -->
