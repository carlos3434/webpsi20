<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/admin/bandeja_historico.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('https://maps.googleapis.com/maps/api/js?libraries=places,geometry,drawing') }}
{{ HTML::script('js/geo/geo.functions.js', array('async' => 'async')) }}
{{ HTML::script('js/geo/markerwithlabel.js', array('async' => 'async')) }}
{{ HTML::script('js/utils.js', array('async' => 'async')) }}
{{ HTML::script('js/psigeo.js') }}

{{ HTML::script('js/psi.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.js.horarios_ajax' )
@include( 'admin.historico.js.usegestion_ajax' )

@include( 'admin.historico.js.officetrack_modal' )

@include( 'admin.historico.js.ofsc_ajax' )

@include( 'admin.historico.js.bandeja_ajax' )
@include( 'admin.historico.js.bandeja' )
@include( 'admin.js.tareas' )
@include( 'admin.historico.js.bandeja_modal' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Bandeja de Gestión
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Historico</a></li>
        <li class="active">Bandeja de Gestión</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel-group" id="accordion-filtros">
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseOne">Búsqueda Individual</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in ">
                        <div class="panel-body">
                            <form name="form_Personalizado" id="form_Personalizado" method="POST" action="">
                                <div class="col-xs-5">
                                    <div class="form-group ">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4">
                                                <label class="titulo">Buscar por:</label>
                                                <input type="hidden" name="bandeja" id="bandeja" value="1">
                                            </div>
                                            <div class="col-sm-4">
                                                <select class="" name="slct_tipo" id="slct_tipo">
                                                    <option value="">.::Seleccione::.</option>
                                                    <option value="gd.averia">Código Actuación</option>
                                                    <option value="g.id">ID</option>
                                                    <option value="gd.telefono">Teléfono</option>
                                                    <option value="OT">OT</option>

                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" name="txt_buscar" id="txt_buscar"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a class="btn btn-primary btn-sm" id="btn_personalizado">
                                        <i class="fa fa-search fa-lg"></i>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">
                                Búsqueda Personalizada
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="form_General" id="form_General" method="POST" action="reporte/bandejaexcel">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2">
                                            <label class="titulo">Actividad:</label>
                                            <input type="hidden" name="bandeja" id="bandeja" value="1">
                                            <input type="hidden" name="usuario" id="usuario" value="1">
                                            <input type="hidden" name="idUsuario" id="idUsuario" value="{{Auth::user()->id}}">
                                            <select class="form-control" name="slct_actividad[]" id="slct_actividad"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Estado:</label>
                                            <select class="form-control" name="slct_estado[]" id="slct_estado" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Quiebre:</label>
                                            <select class="form-control" name="slct_quiebre[]" id="slct_quiebre"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Empresa:</label>
                                            <select class="form-control" name="slct_empresa[]" id="slct_empresa"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Celula:</label>
                                            <select class="form-control" name="slct_celula[]" id="slct_celula" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Tecnico:</label>
                                            <select class="form-control" name="slct_tecnico[]" id="slct_tecnico"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="col-sm-2">
                                            <label class="titulo">Est. Legado:</label>
                                            <select class="form-control" name="slct_legado" id="slct_legado">
                                                <option value="">.::Todo::.</option>
                                                <option value="1">Pendiente</option>
                                                <option value="0">No Pendiente</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">OT Est. Transmisión:</label>
                                            <select class="form-control" name="slct_transmision[]" id="slct_transmision"
                                                    multiple onchange="validarImagenExp();">
                                                <option value="1">Envio</option>
                                                <option value="0001-Inicio">Inicio</option>
                                                <option value="0002-Supervicion">Supervición</option>
                                                <option value="0003-Cierre">Cierre</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">OT Cierre Estados:</label>
                                            <select class="form-control" name="slct_cierre_estado[]"
                                                    id="slct_cierre_estado" multiple>
                                                <option value="Atendido">Atendido</option>
                                                <option value="Ausente">Ausente</option>
                                                <option value="En procesos">En procesos</option>
                                                <option value="Inefectiva">Inefectiva</option>
                                                <option value="No deja">No deja</option>
                                                <option value="No desea">No desea</option>
                                                <option value="Otros">Otros</option>
                                                <option value="Transferido">Transferido</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Coordinado:</label>
                                            <select class="form-control" name="slct_coordinado" id="slct_coordinado">
                                                <option value="">.::Todo::.</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="titulo">Agenda:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="fecha_agenda"
                                                       id="fecha_agenda"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <br><br>
                                            <a class="btn btn-primary btn-sm" id="btn_general">
                                                <i class="fa fa-search fa-lg"></i>
                                            </a>
                                            <a class="btn btn-primary btn-sm" id="btn_vista_detalle"
                                               onclick="vistadetalleG('detalle',this.id);">
                                                <i class="fa fa-expand fa-lg"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!--<div id="valid_filtro">
                                    </div>-->
                                    <div class="col-sm-12 detalle" style="display:none">
                                        <div class="col-sm-1">
                                            <label class="titulo">Zonal:</label>
                                            <select class="form-control" name="slct_zonal[]" id="slct_zonal" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="titulo">Nodo:</label>
                                            <select class="form-control" name="slct_nodo[]" id="slct_nodo" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <label class="titulo">Troba:</label>
                                            <select class="form-control" name="slct_troba[]" id="slct_troba" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                            </div>
                                        <div class="col-sm-1">
                                            <label class="titulo">MDF:</label>
                                            <select class="form-control" name="slct_mdf[]" id="slct_mdf" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Motivo:</label>
                                            <select class="form-control" name="slct_motivo[]"
                                                    id="slct_motivo" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Submotivo:</label>
                                            <select class="form-control" name="slct_submotivo[]"
                                                    id="slct_submotivo" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">C. Componentes:</label>
                                            <select class="form-control" name="slct_c_componentes[]"
                                                    id="slct_c_componentes" multiple>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 detalle" style="display:none">
                                        <div class="col-sm-3" style="display:none">
                                            <label class="titulo">Tipo Actividad Ofsc:</label>
                                            <select class="form-control" name="slct_actividad_tipo[]"
                                                    id="slct_actividad_tipo" multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Estado Ofsc:</label>
                                            <select class="form-control" name="slct_estado_ofsc[]" id="slct_estado_ofsc"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">Desprogramado por:</label>
                                            <select class="form-control" name="slct_envio_ofsc" id="slct_envio_ofsc">
                                                <option value="">.::Seleccione::.</option>
                                                <option value="1">Agenda</option>
                                                <option value="3">Cancelación</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">T. Agendamiento:</label>
                                            <select class="form-control" name="slct_tipo_agendamiento"
                                                    id="slct_tipo_agendamiento">
                                                <option value="">.::Seleccione::.</option>
                                                <option value="1">Agenda</option>
                                                <option value="2">SLA</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">F. Ultimo Movimiento:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="fecha_ultimo"
                                                       id="fecha_ultimo"/>
                                            </div>
                                          <!--  <label class="titulo">Estado de Masivo OFSC - TOA:</label>
                                            <select class="form-control" name="slct_estado_envio_masivo_ofsc"
                                                    id="slct_estado_envio_masivo_ofsc">
                                                <option value="">.::Seleccione::.</option>
                                                <option value="1">Enviado</option>
                                                <option value="0">No Enviado</option>
                                            </select> -->
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">F. Registro Legados:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="fecha_registro"
                                                       id="fecha_registro"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="titulo">F. Registro PSI:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right"
                                                       name="fecha_registro_psi" id="fecha_registro_psi"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body" style="min-height:250px">
                    <div class="box-body table-responsive">
                        <table id="t_bandeja" class="table table-bordered table-striped col-sm-12 responsive"
                               width="100%">
                            <thead>
                            <tr>
                                <th style='width:55px !important;'><span class="cont_checkbox"><input type="checkbox"
                                                                                                      name="check_masivo"
                                                                                                      checked
                                                                                                      id="check_all_masivo_sup"/></span>Id
                                </th>
                                <th style='width:70px !important;'>Cod Actu</th>
                                <th style='width:90px !important;'>F. Registro</th>
                                <th style='width:90px !important;'>F. PSI</th>
                                <th style='width:100px !important;'>Tipo</th>
                                <th>Quiebre</th>
                                <th>Empresa</th>
                                <th style='width:40px !important;'>MDF</th>
                                <th style='width:90px !important;'>Fecha Agenda</th>
                                <th style='width:200px !important;'>Tecnico</th>
                                <th>Estado Ofsc</th>
                                <th>Estado</th>
                                <th style="width: 100px !important;">
                                    <a onclick="descargarReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a>
                                    <a onclick="descargarReporteImagen();" class="expImage btn btn-success"><i
                                                class="fa fa-picture-o fa-lg"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="tb_bandeja">

                            </tbody>
                            <tfoot>
                            <tr>
                                <th><span class="cont_checkbox"><input type="checkbox" name="check_masivo" checked
                                                                       id="check_all_masivo_inf"/></span>Id
                                </th>
                                <th>Cod Actu</th>
                                <th>F. Registro</th>
                                <th>F. PSI</th>
                                <th>Tipo</th>
                                <th>Quiebre</th>
                                <th>Empresa</th>
                                <th>MDF</th>
                                <th>Fecha Agenda</th>
                                <th>Tecnico</th>
                                <th>Estado Ofsc</th>
                                <th>Estado</th>
                                <th>
                                    <a onclick="descargarReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a>
                                    <a onclick="descargarReporteImagen();" class="expImage btn btn-success"><i
                                                class="fa fa-picture-o fa-lg"></i></a>
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('formulario')
    @include( 'admin.historico.form.ofsc_modal' )
    @include( 'admin.historico.form.bandeja_modal' )
    @include( 'admin.historico.form.officetrack_modal' )
@stop
