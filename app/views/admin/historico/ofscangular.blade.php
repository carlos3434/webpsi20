<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::script('lib/angular/angular.min.js') }}
{{ HTML::script('lib/angular/angular-ui-router.min.js') }}
{{ HTML::script('lib/angular/ocLazyLoad.min.js') }}
{{ HTML::script('../laravel/app/views/app/app.js') }}
@stop
        <!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
    <div ng-app="psi">
        <h1>Angular PSI</h1>
        <ui-view></ui-view>
    </div>
@stop