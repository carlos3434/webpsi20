<script type="text/javascript">

var googleMarktxt = "http://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld="; 
            
var markTextColor = textByColor("#" 
                    + $("#pickerMarkBack")
                    .val())
                    .substring(1);

var Bandeja={

    guardarMovimiento:function(){
        var datos=$("#form_bandeja").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        var accion="bandeja/recepccion";
        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos + "&cliente_xy_insert=" + cliente_xy_insert,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $("#txt_buscar").val(obj.codactu);
                    $("#slct_tipo").val('gd.averia');
                    $("#slct_tipo").multiselect("refresh");

                    Bandeja.CargarBandeja('P',HTMLCargarBandeja,variables);
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#bandejaModal .modal-footer [data-dismiss="modal"]').click();
                }
                else{
                    alert(obj.msj);
                }
            },
            error: function(xhr, status, error){
                var respta1 = xhr.responseText;
                var respta2 = respta1.substr(0,16); 
                $(".overlay,.loading-img").remove();

                if(respta2=="errorOfficeTrack"){
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error_ofsc"); ?>', 8000); 
                }else{
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
                }

            }
        });
    },
    guardarObservacion:function(){
        var datos=$("#form_observacion").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        var accion="gestion_movimiento/crearobs";

        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){

                    $("#txt_buscar").val(obj.codactu);
                    $("#slct_tipo").val('gd.averia');
                    $("#slct_tipo").multiselect("refresh");

                    Bandeja.CargarBandeja('P',HTMLCargarBandeja,variables);

                    $("#slct_obs_tipo").val(0);
                    $("#txt_observacion_o_modal").val("");
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#observacionModal .modal-footer [data-dismiss="modal"]').click();
                } else if (obj.rst == 2) {
                    Psi.mensaje('warning', obj.msj, 6000);
                } else{
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    CargarBandeja:function(PG,evento,variables){
        var datos="";
        var fondo=[];
        GLOBAL.pg = PG;
        if(PG=="P" || PG=="G"){
            $('#t_bandeja').dataTable().fnDestroy();
            GLOBAL.table_bandeja =$('#t_bandeja')
            .on( 'page.dt',   function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .on( 'search.dt', function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .on( 'order.dt',  function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .DataTable( {
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "stateLoadCallback": function (settings) {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
                  var trsimple;
                    for(i=0;i<fondo.length;i++){
                        trsimple=$("#tb_bandeja tr td").filter(function() {
                            return $(this).text() == fondo[i];
                        }).parent('tr');
                        $(trsimple).find("td:eq(0)").css("background-color", "#DA8F66");
                    }
                    $(".overlay,.loading-img").remove();
                    $('#t_bandeja').removeAttr("style");
                },
                "ajax": {
                    "url": "gestion/cargar",
                    "type": "POST",

                    "data": function(d){
                        //d.datos = datos;
                        var contador=0;
                        if(PG=="P"){// Filtro Personalizado
                            datos=$("#form_Personalizado").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("&");
                        }
                        else if(PG=="G"){ // Filtro General
                            datos=$("#form_General").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
                        }
                        datos.push("withLimit=1"); 
                        datos.push("isSoloActuaciones=0");
                        for (var i = datos.length - 1; i >= 0; i--) {
                            if( datos[i].split("[]").length>1 ){
                                d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                contador++;
                            } else{
                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                            }
                        }
                    },
                    "error": function(xhr, status, error) {
                    }
                },
                "columnDefs": [
                    {
                        "targets": 0,
                        "data": function ( row, type, val, meta ) {
                            var vid=row.id;
                                if(row.id===''){
                                    temporalBandeja++;
                                    vid=  "T_"+temporalBandeja;
                                }
                                if(row.existe*1!=1){
                                    fondo.push(vid);
                                }
                                texto = vid;
                                // ******************************************************************************
                                // Mostramos el semaforo con aquellos codactu,
                                // que presentan estado_ofsc
                                var semaforo = '';
                                if( row.estado_ofsc!=null && row.estado_ofsc && row.estado_ofsc != "undefined" ){
                                    semaforo = '<br><i style="color:'+row.estado_ofsc_color+';" class="fa fa-circle fa-lg"></i>';
                                }
                                return texto+semaforo;
                                // ******************************************************************************
                                // return texto;
                        },
                        "defaultContent": '',
                        "name": "id"
                    },
                    {
                        "targets": 1,
                        "data": "codactu",
                        "name": "codactu"
                    },
                    {
                        "targets": 2,
                        "data": "fecha_registro",
                        "name": "fecha_registro"
                    },
                    {
                        "targets": 3,
                        "data": "fecha_registro_psi",
                        "name": "fecha_registro_psi"
                    },
                    {
                        "targets": 4,
                        "data": "actividad",
                        "name": "actividad"
                    },
                    {
                        "targets": 5,
                        "data": "quiebre",
                        "name": "quiebre"
                    },
                    {
                        "targets": 6,
                        "data": "empresa",
                        "name": "empresa"
                    },
                    {
                        "targets": 7,
                        "data": "mdf",
                        "name": "mdf"
                    },
                    {
                        "targets": 8,
                        "data": "fh_agenda",
                        "name": "fh_agenda"
                    },
                    {
                        "targets": 9,
                        "data": "tecnico",
                        "name": "tecnico"
                    },
                    {
                        "targets": 10,
                        "data": function ( row, type, val, meta) {
                            if (row.estado_ofsc === "")
                                return "No Enviado";
                            else
                                return row.estado_ofsc;
                        },
                        "name": "estado_ofsc"
                    },
                    {
                        "targets": 11,
                        "data": function ( row, type, val, meta) {
                            return row.estado+"<br><font color='#327CA7'>"+row.cierre_estado+"</font>";
                        },
                        "defaultContent": '',
                        "name": "estado"
                    },
                    {
                        "targets": 12,
                        "orderable":false,
                        "searchable": false,
                        "data": function ( row, type, val, meta ) {
                            var officetrackColor='';var officetrackImage='';var officetrackImage2='';var officetrackbotton=''; var toaBotton = '';var ve ="'";var officetrackImage3='';
                            toggleForzarEnvioOfsc = "";
                            
                            if( row.estado_ofsc!=null && row.estado_ofsc && row.estado_ofsc != "undefined" ){

                                officetrackImage2=' &nbsp; <a class="pplo btn bg-orange btn-sm" name="det["'+row.id_atc+'"]"  data-id_atc="'+row.id_atc+'"><i class="fa fa-automobile fa-lg"></i> </a>';

                                officetrackImage3=' &nbsp; <a class="pplu btn bg-blue btn-sm"  data-id_atc="'+row.id_atc+'"><i class="fa fa-eye fa-lg"></i> </a>';

                                // Si estado de la orden es cancelada
                                if (row.estado_ofsc ==="Cancelada" && row.estado_id !==15){
                                    toggleForzarEnvioOfsc+="<a data-codactu='"+row.codactu+"' class='btn-activar-envio-ofsc' ><i class='fa fa-toggle-on' aria-hidden='true'></i></a>";
                                    toggleForzarEnvioOfsc+="<a data-codactu='"+row.codactu+"' class='btn-desactivar-envio-ofsc' ><i class='fa fa-toggle-off' aria-hidden='true'></i></a>";
                                }
                            }
                            if(row.transmision!=null && row.transmision!=0){
                                officetrackColor='btn-default';
                                if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,6).toLowerCase()=='inicio' ){
                                    officetrackColor='btn-success';
                                } else if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,11).toLowerCase()=='supervision' ){
                                    officetrackColor='btn-warning';
                                } else if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,6).toLowerCase()=='cierre' ){
                                    officetrackColor='btn-primary';
                                }

                                if(officetrackColor!='btn-default'){
                                    officetrackbotton='data-toggle="modal" data-target="#officetrackModal" data-id="'+row.id+'"';
                                }
                                officetrackImage=' &nbsp; <a class="btn '+officetrackColor+' btn-sm" '+officetrackbotton+'><i class="fa fa-mobile fa-3x"></i> </a>';
                            }
                            return  '<a class="btn bg-navy btn-sm" data-toggle="modal" data-target="#bandejaModal" data-codactu="'+row.codactu+'">'+
                                    '   <i class="fa fa-desktop fa-lg"></i>'+
                                    '</a>'+ officetrackImage+officetrackImage2+officetrackImage3+toggleForzarEnvioOfsc;
                        },
                        "defaultContent": ''
                    },
                ],
                "createdRow": function (row, data, dataIndex) {
                    flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
                    if(flagActivoMasivo === null || flagActivoMasivo === '0') {
                        $(row).removeClass("fila-activa-masivo");
                    } else {
                         $(row).addClass("fila-activa-masivo");
                    }
                }
            });
        } else {
            if(PG=="M"){ //Modal
                if (typeof variables!=="undefined")
                    variables.isSoloActuaciones = 0;
                datos=variables;
            }
            $.ajax({
                url         : 'gestion/cargar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        evento(obj.datos);
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
                }
            });
        }
    },
    CargarGestionMovimiento:function(datos,evento,objant){
        $.ajax({
            url         : 'gestion_movimiento/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,objant);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    validaEstado:function(datos,evento,datosanteriores){
        $.ajax({
            url         : 'estado/validar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,datosanteriores);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    extraerXY:function(datos){
        $.ajax({
            url         : 'bandeja/extraerxy',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    cliente_x = obj.coord.x;
                    cliente_y = obj.coord.y;
                    cliente_xy_insert = obj.ins;
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    CargarComponentes:function(evento,datos){
        $.ajax({
            url         : 'cat_componente/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    UpdateLatLng:function (actu, lat, lng){
        $.ajax({
            url         : 'gestion/actualizaxy',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : 'actu=' + actu + '&lat=' + lat + '&lng=' + lng,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    Psi.mensaje('success', obj.msj, 8000); 
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    ValidaPermisoToa:function(datos,permiso){
        $.ajax({
            url         : 'toa/validagestion',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    permiso=obj.permiso;
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
        return permiso;
    },

    Campos:function(datos,evento,table,lng,lat,fftt){
        $.ajax({
            url         : 'tabladetalle/campos',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,table,lng,lat,fftt);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },

    Obtenerxyfft:function(fftt,table,obj){
        var valores=[];
        var y=0;
        var punto='';
        var tipo='';
        //fftt='LL|R002|06|04';
        for (var i=2; i<= obj.cant; i++) {
            tipo=obj.c_detalle.split('|')[(i-1)];
            punto=fftt.split('|')[y];
            if(parseInt(punto)%1==0 && tipo!='terminald' && tipo!='terminalf'){
                punto=parseInt(punto);
            }
            valores[y]={tipo:tipo,
                        valor:punto};
            y++;
        }
        var datos={valor:valores,c:i,tabla_id:table,idtipo:0};
        $.ajax({
            url         : 'tabladetalle/camposdetalle',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1 && obj.coords.length>0){
                    //evento(obj.datos,table,lng,lat);
                    $("#txt_x_fftt").val(obj.coords[0].coord_x);
                    $("#txt_y_fftt").val(obj.coords[0].coord_y);
                    $("#tipo_fftt").val(obj.coords[0].tipo);
                    initializeMapModal($('#txt_x_modal2').val(), $('#txt_y_modal2').val(), 2);
                } else {
                    $("#txt_x_fftt").val('');
                    $("#txt_y_fftt").val('');
                    $("#tipo_fftt").val('');
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },

    QuitarValidacionModal:function(datos){
        $.ajax({
            url         : 'toa/quitarvalidacion',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                // $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    PintarMapaUpdate:function(lng,lat,data,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden){
        
        $.ajax({
            url         : 'tabladetalle/camposdetalle',
            type        : 'POST',
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();            
                if(obj.rst==1){
                    htmlListarSlct(obj,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden);
                    if (funciones!=='' && funciones!==undefined) {
                        if (funciones.success!=='' && funciones.success!==undefined) {
                            funciones.success(obj.datos);
                        }
                    }
                    if(obj.coords ){
                        var map = new google.maps.Map(document.getElementById('map_canvas2'), {
                            zoom: 16,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var iconSelected = googleMarktxt
                                  + 'NF' 
                                  + "|3248c7|" 
                                  + markTextColor;

                        if(obj.coords.length == 1){
                            yactu = obj.coords[0].coord_y;
                            xactu = obj.coords[0].coord_x;
                            var icon=iconSelected;
                            var locate=window.location.origin;
                            if(obj.coords[0].tipo=='amplificador') icon=locate+"/img/icons/amplificador.png";
                            if(obj.coords[0].tipo=='tap') icon=locate+"/img/icons/tap.png";
                            if(obj.coords[0].tipo=='terminald') icon=locate+"/img/icons/terminal.png";
                            if(obj.coords[0].tipo=='terminalf') icon=locate+"/img/icons/terminal.png";

                            var markerdos = new google.maps.Marker({
                                position: {lat: parseFloat(yactu), lng: parseFloat(xactu) },
                                map: map,
                                icon: icon,
                                draggable: false,
                            });

                            map.setCenter(new google.maps.LatLng(yactu, xactu));
                            map.setZoom(16);
                            var updstreet = geoStreetView(yactu, xactu, 'street_canvas2');
                        }else {
                            var polygons = [];                        
                            var poli_array = Psigeo.objToLatLngArray(obj.coords);
                            var color = Psi.color_aleatorio();
                            var poligono = Psigeo.poligono(poli_array, color, 3, color, 0.25);
                            poligono.setMap(map);
                            polygons.push(poligono);
                            var updbounds = new google.maps.LatLngBounds();
                            $.each(poli_array, function () {
                                updbounds.extend(this);
                            });
                            map.fitBounds(updbounds);

                            poligono.addListener('click', function(evento) {

                                polat= evento.latLng.lat();
                                polng= evento.latLng.lng();
                        
                                SetXY(polng, polat, cond);
                                marker.setPosition(
                                    new google.maps.LatLng(
                                        polat,
                                        polng
                                    )
                                );
                                initStreetView(polat, polng, 'street_canvas2', map, marker, cond);
                        
                            });

                        }

                        //FFTT ACTUAL
                        var marker = new google.maps.Marker({
                            position: {lat: parseFloat($("#txt_y_fftt").val()),
                                       lng: parseFloat($("#txt_x_fftt").val()) },
                            map: map,
                            draggable: false,
                            icon: googleMarktxt 
                                  + 'FT' 
                                  + "|3248c7|" 
                                  + markTextColor
                        });

                        //ACTIVIDAD
                        var marker = new google.maps.Marker({
                            position: {lat: parseFloat(lat), lng: parseFloat(lng) },
                            map: map,
                            draggable: true,
                            id: 1
                        });
                        var cond=2;
                        google.maps.event.addListener(marker, 'click', function() {
                            var markerLatLng = marker.getPosition();
                            SetXY(markerLatLng.lng(), markerLatLng.lat(), cond);
                        });
                        google.maps.event.addListener(marker, 'dragend', function() {
                            var markerLatLng = marker.getPosition();
                            SetXY(markerLatLng.lng(), markerLatLng.lat(), cond);
                            initStreetView(markerLatLng.lat(), markerLatLng.lng(), 'street_canvas2', map, marker, cond);
                        });

                        google.maps.event.addListener(map, 'click', function(evento) {

                            lat = evento.latLng.lat();
                            lng= evento.latLng.lng();
                    
                            SetXY(lng, lat, cond);
                            marker.setPosition(
                                new google.maps.LatLng(
                                    lat,
                                    lng
                                )
                            );
                            initStreetView(lat, lng, 'street_canvas2', map, marker, cond);
                        });

                    }
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    obtenerActividadOfsc:function(aid, evento){
        $.ajax({
            type: "POST",
            url: "bandeja/activity-ofsc",
            dataType: 'json',
            data: {aid: aid},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                evento(obj);
                //ToaActivityImage = obj.imagenofsc;
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Pestaña "Movimientos": <?php echo trans("greetings.mensaje_error"); ?>', 8000); 
            }
        });
    },
    CargarComponenteOperacion:function(datos) {
        $.ajax({
            url         : 'componentelegado/componentoperacion',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {codactu:datos},
            success : function(obj) {
                HTMLCargarComponenteOperacion(obj);
            }
        });
    },
};
</script>
