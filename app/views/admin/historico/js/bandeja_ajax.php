<script type="text/javascript">


var Bandeja={

    guardarMovimiento:function(){
        var datos=$("#form_bandeja").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        //var accion="gestion_movimiento/crear";
        var accion="bandeja/recepccion";
        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos + "&cliente_xy_insert=" + cliente_xy_insert,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){

                    $("#txt_buscar").val(obj.codactu);
                    $("#slct_tipo").val('gd.averia');
                    $("#slct_tipo").multiselect("refresh");

                    Bandeja.CargarBandeja('P',HTMLCargarBandeja,variables);
//                    $("#msj").html('<div class="alert alert-dismissable alert-success">'+
//                                        '<i class="fa fa-check"></i>'+
//                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
//                                        '<b>'+obj.msj+'</b>'+
//                                    '</div>');
                    Psi.mensaje('success', obj.msj, 6000);    
                    $('#bandejaModal .modal-footer [data-dismiss="modal"]').click();
                }
                else{
                    alert(obj.msj);
                }
            },
            error: function(xhr, status, error){
                var respta1 = xhr.responseText;
                var respta2 = respta1.substr(0,16); 
                $(".overlay,.loading-img").remove();

                if(respta2=="errorOfficeTrack"){
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error_ofsc"); ?>', 8000); 
//                    $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
//                                            '<i class="fa fa-ban"></i>'+
//                                            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
//                                            '<b><?php //echo trans("greetings.mensaje_error_ofsc"); ?></b>'+
//                                        '</div>');
                }else{
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000); 
//                    $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
//                                            '<i class="fa fa-ban"></i>'+
//                                            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
//                                            '<b><?php //echo trans("greetings.mensaje_error"); ?></b>'+
//                                        '</div>');                 
                }

            }
        });
    },
    guardarObservacion:function(){
        var datos=$("#form_observacion").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        var accion="gestion_movimiento/crearobs";

        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){

                    $("#txt_buscar").val(obj.codactu);
                    $("#slct_tipo").val('gd.averia');
                    $("#slct_tipo").multiselect("refresh");

                    Bandeja.CargarBandeja('P',HTMLCargarBandeja,variables);

                    $("#slct_obs_tipo").val(0);
                    $("#txt_observacion_o_modal").val("");
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#observacionModal .modal-footer [data-dismiss="modal"]').click();
                } else if (obj.rst == 2) {
                    Psi.mensaje('warning', obj.msj, 6000);
                } else{
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    CargarBandeja:function(PG,evento,variables){
        var datos="";
        var fondo=[];
        GLOBAL.pg = PG;
        if(PG=="P" || PG=="G"){
            flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
            localStorage.setItem("forzarAgendaOFSC", "[]");
            $('#t_bandeja').dataTable().fnDestroy();
            GLOBAL.table_bandeja =$('#t_bandeja')
            .on( 'page.dt',   function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .on( 'search.dt', function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .on( 'order.dt',  function () { $("body").append('<div class="overlay"></div><div class="loading-img"></div>'); } )
            .DataTable( {
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "stateLoadCallback": function (settings) {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
                  var trsimple;
                    for(i=0;i<fondo.length;i++){
                        trsimple=$("#tb_bandeja tr td").filter(function() {
                            return $(this).text() == fondo[i];
                        }).parent('tr');
                        $(trsimple).find("td:eq(0)").css("background-color", "#DA8F66");
                    }
                    $(".overlay,.loading-img").remove();
                    $('#t_bandeja').removeAttr("style");
                },
                "ajax": {
                    "url": "gestion/cargar",
                    "type": "POST",
                    //"async": false,

                    "data": function(d){
                        //d.datos = datos;
                        var contador=0;
                        if(PG=="P"){// Filtro Personalizado
                            datos=$("#form_Personalizado").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("&");
                        }
                        else if(PG=="G"){ // Filtro General
                            datos=$("#form_General").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
                        }
                        datos.push("withLimit=1"); 
                        datos.push("isSoloActuaciones=0");
                        for (var i = datos.length - 1; i >= 0; i--) {
                            if( datos[i].split("[]").length>1 ){
                                d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                contador++;
                            }
                            else{
                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                            }
                        }
                    },
                    "error": function(xhr, status, error) {
                    }
                },
                "columnDefs": [
                    {
                        "targets": 0,
                        "data": function ( row, type, val, meta ) {
                            
                            var vid=row.id;
                                if(row.id===''){
                                    temporalBandeja++;
                                    vid=  "T_"+temporalBandeja;
                                }
                                if(row.existe*1!=1){
                                    fondo.push(vid);
                                }

                                texto = vid;

                                // ******************************************************************************
                                // Mostramos el semaforo con aquellos codactu,
                                // que presentan estado_ofsc
                                var semaforo = '';
                                if( row.estado_ofsc!=null && row.estado_ofsc && row.estado_ofsc != "undefined" ){
                                    semaforo = '<br><i style="color:'+row.estado_ofsc_color+';" class="fa fa-circle fa-lg"></i>';
                                }
                                return texto+semaforo;
                                // ******************************************************************************
                                // return texto;
                        },
                        "defaultContent": '',
                        "name": "id"                        
                    },
                    {
                        "targets": 1,
                        "data": "codactu",
                        "name": "codactu"
                    },
                    {
                        "targets": 2,
                        "data": "fecha_registro",
                        "name": "fecha_registro"
                    },
                    {
                        "targets": 3,
                        "data": "fecha_registro_psi",
                        "name": "fecha_registro_psi"
                    },
                    {
                        "targets": 4,
                        "data": "actividad",
                        "name": "actividad"
                    },
                    {
                        "targets": 5,
                        "data": "quiebre",
                        "name": "quiebre"
                    },
                    {
                        "targets": 6,
                        "data": "empresa",
                        "name": "empresa"
                    },
                    {
                        "targets": 7,
                        "data": "mdf",
                        "name": "mdf"
                    },
                    {
                        "targets": 8,
                        "data": "fh_agenda",
                        "name": "fh_agenda"
                    },
                    {
                        "targets": 9,
                        "data": "tecnico",
                        "name": "tecnico"
                    },
                    {
                        "targets": 10,
                        //"data": "estado_ofsc",
                        "data": function ( row, type, val, meta) {
                            if (row.estado_ofsc === "")
                                    return "No Enviado";
                            else
                                return row.estado_ofsc;
                        },
                        "name": "estado_ofsc"
                    },
                    {
                        "targets": 11,
                        "data": function ( row, type, val, meta) {
                            return row.estado+"<br><font color='#327CA7'>"+row.cierre_estado+"</font>";
                        },
                        "defaultContent": '',
                        "name": "estado"
                    },
                    {
                        "targets": 12,
                        "orderable":false,
                        "searchable": false,
                        "data": function ( row, type, val, meta ) {
                            var officetrackColor='';var officetrackImage='';var officetrackImage2='';var officetrackbotton=''; var toaBotton = '';var ve ="'";var officetrackImage3='';
                            toggleForzarEnvioOfsc = "";
                            
                             if( row.estado_ofsc!=null && row.estado_ofsc && row.estado_ofsc != "undefined" ){

                                // officetrackImage2=' &nbsp; <a class="pplo btn bg-orange btn-sm" onclick="Bandeja.myButton_onclick(event,'+ve+row.id_atc+ve+')" name="det["'+row.id_atc+'"]"  data-id_atc="'+row.id_atc+'"><i class="fa fa-automobile fa-lg"></i> </a>';

                                officetrackImage2=' &nbsp; <a class="pplo btn bg-orange btn-sm" name="det["'+row.id_atc+'"]"  data-id_atc="'+row.id_atc+'"><i class="fa fa-automobile fa-lg"></i> </a>';

                                 officetrackImage3=' &nbsp; <a class="pplu btn bg-blue btn-sm"  data-id_atc="'+row.id_atc+'"><i class="fa fa-eye fa-lg"></i> </a>';

                                 // Si estado de la orden es cancelada
                                 if (row.estado_ofsc ==="Cancelada" && row.estado_id !==15){
                                     toggleForzarEnvioOfsc+="<a data-codactu='"+row.codactu+"' class='btn-activar-envio-ofsc' ><i class='fa fa-toggle-on' aria-hidden='true'></i></a>";
                                     toggleForzarEnvioOfsc+="<a data-codactu='"+row.codactu+"' class='btn-desactivar-envio-ofsc' ><i class='fa fa-toggle-off' aria-hidden='true'></i></a>";
                                 }
                            }


                            if(row.transmision!=null && row.transmision!=0){
                                officetrackColor='btn-default';
                                if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,6).toLowerCase()=='inicio' ){
                                    officetrackColor='btn-success';
                                }
                                else if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,11).toLowerCase()=='supervision' ){
                                    officetrackColor='btn-warning';
                                }
                                else if(row.transmision!=1 && row.transmision.split("-").length>1 && row.transmision.split("-")[1].substr(0,6).toLowerCase()=='cierre' ){
                                    officetrackColor='btn-primary';
                                }

                                if(officetrackColor!='btn-default'){
                                    officetrackbotton='data-toggle="modal" data-target="#officetrackModal" data-id="'+row.id+'"';
                                }
                                officetrackImage=' &nbsp; <a class="btn '+officetrackColor+' btn-sm" '+officetrackbotton+'><i class="fa fa-mobile fa-3x"></i> </a>';
                            }
                            
                            return  '<a class="btn bg-navy btn-sm" data-toggle="modal" data-target="#bandejaModal" data-codactu="'+row.codactu+'">'+
                                    '   <i class="fa fa-desktop fa-lg"></i>'+
                                    '</a>'+
                                    officetrackImage+officetrackImage2+officetrackImage3+toggleForzarEnvioOfsc;
                        },
                        "defaultContent": ''
                    },
                ],
                    "createdRow": function (row, data, dataIndex) {
                        flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
                        if(flagActivoMasivo === null || flagActivoMasivo === '0') {
                            $(row).removeClass("fila-activa-masivo");
                        } else {
                             $(row).addClass("fila-activa-masivo");
                        }
                        
                    }
            } );

        }
        else {

            if(PG=="M"){ //Modal
                if (typeof variables!=="undefined")
                    variables.isSoloActuaciones = 0;
                datos=variables;
            }

            $.ajax({
                url         : 'gestion/cargar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        evento(obj.datos);
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                            '<i class="fa fa-ban"></i>'+
                                            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                            '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                        '</div>');
                }
            });

        }
    },
    CargarGestionMovimiento:function(datos,evento,objant){
        $.ajax({
            url         : 'gestion_movimiento/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,objant);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    validaEstado:function(datos,evento,datosanteriores){
        $.ajax({
            url         : 'estado/validar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,datosanteriores);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    extraerXY:function(datos){

        $.ajax({
            url         : 'bandeja/extraerxy',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    cliente_x = obj.coord.x;
                    cliente_y = obj.coord.y;
                    cliente_xy_insert = obj.ins;
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    CargarComponentes:function(evento,datos){        
        $.ajax({
            url         : 'cat_componente/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {                
                if(obj.rst==1){
                    evento(obj.datos);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    UpdateLatLng:function (actu, lat, lng){
        $.ajax({
            url         : 'gestion/actualizaxy',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : 'actu=' + actu + '&lat=' + lat + '&lng=' + lng,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $("#msj").html('<div class="alert alert-dismissable alert-success">'+
                                        '<i class="fa fa-check"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b>'+obj.msj+'</b>'+
                                    '</div>');
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
    ValidaPermisoToa:function(datos,permiso){
        //var permiso=9;
        $.ajax({
            url         : 'toa/validagestion',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    permiso=obj.permiso;
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
        
        return permiso;
    },

    Campos:function(datos,evento,table){
        $.ajax({
            url         : 'tabladetalle/campos',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos,table);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },

    QuitarValidacionModal:function(datos){
        //var permiso=9;
        $.ajax({
            url         : 'toa/quitarvalidacion',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                // $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                // $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },
};
</script>
