<script type="text/javascript">
var OFSC={
    Completar:function(evento,datos){
        //var permiso=9;
        $.ajax({
            url         : 'bandeja/completarofsc',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                
                if(obj.rst==1){
                    evento(obj);
                    Psi.mensaje('success',obj.msj,3000);
                }
                else{
                    if( typeof obj.error_msg !='undefined' ){
                        Psi.mensaje('danger', obj.error_msg, 6000);
                    }
                }
                
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
    },
    Iniciar:function(evento,datos){
        $.ajax({
            url         : 'bandeja/iniciarofsc',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                
                if(obj.rst==1){
                    evento(obj);
                    Psi.mensaje('success',obj.msj,3000);
                }
                else{
                    Psi.mensaje('danger',obj.msj,3000);
                    if( typeof obj.error_msg !='undefined' ){
                    }
                }
                
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
    },
    Cancelar:function(evento,datos){
        //var permiso=9;
        $.ajax({
            url         : 'bandeja/cancelarofsc',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                
                if(obj.rst==1){
                    evento(obj);
                    Psi.mensaje('success',obj.msj,3000);
                }
                else{
                    if( typeof obj.error_msg !='undefined' ){
                        Psi.mensaje('danger',obj.error_msg,3000);
                    }
                }
                
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
    },
    Capacity:function(evento,datos){
        //var permiso=9;
        $.ajax({
            url         : 'bandeja/capacity',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    //evento(obj.datos.get_capacity_response);
                    if(obj.datos.error===false && !$.isEmptyObject( obj.datos.data) ){
                        var duracion=parseInt(obj.duracion);
                        evento(obj.datos.data, duracion);
                    } else if (obj.datos.error===false && $.isEmptyObject( obj.datos.data)) {
                        hf=''; /* variable_global: js/bandeja_modal.php */
                        Psi.mensaje('danger', 'No hay capacidad en la fecha y hora seleccionado.', 5000);
                        $(".overlay,.loading-img").remove();
                    } else {
                        Psi.mensaje('danger', obj.datos.errorString, 6000);
                        $(".overlay,.loading-img").remove();
                    }
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
    },
    EnvioOfsc:function(evento,datos){
        //var permiso=9;
        $.ajax({
            url         : 'bandeja/envioofsc',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    evento(obj);
                    Psi.mensaje('success',obj.msj,3000);
                }
                else{
                    Psi.mensaje('danger', 'Error de envio Ofsc;'+ obj.msj);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
        
        //return permiso;
    },
    UpdateOfsc:function(evento,datos){
        $.ajax({
            url         : 'bandeja/updateofsc',
            type        : 'POST',
            cache       : false,
            async       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    //evento(obj);
                    $("#btn_close_modal").click();
                    Psi.mensaje('success',obj.msj,3000);
                }
                else{
                    Psi.mensaje('danger',obj.error_msg,3000);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
            }
        });
    },
    GetActivityOfsc: function(codActuacion, boton, abierto){
        var tr = boton.closest('tr');
        var row = GLOBAL.table_bandeja.row(tr);
        
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            eventoCargaRemover();
        }
        else {
            // Open this row
            activity = functionajax('bandeja/activity', 'POST', false, 'json', false, {codactuacion: codActuacion});
            activity.success(function(data){
                if(data.rst == 1) {
                   row.child(data.html).show();
                    tr.addClass('shown');
                    setTimeout(function(){eventoCargaRemover();}, 3000);
                }else{
                    setTimeout(function(){eventoCargaRemover();}, 3000);
                    Psi.mensaje("danger", data.msj, 5000);
                    
                }
                
            });
        }
    },
    EnvioMasivoOfsc: function(){
        bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
        if (typeof bandejaBusqueda === "undefined" || bandejaBusqueda === "" || bandejaBusqueda === "{}") {
            Psi.mensaje("danger", "No ha realizado una busqueda primero");
        } else {
             masivo = functionajax('enviosofsc/masivoofsc', 'POST', false, 'json', false, {"codactuSeleccionados":bandejaBusqueda});
            masivo.success(function(data){
                if  (data.rst == 1) {
                    Psi.mensaje("success", data.msj, 5000);
                    setTimeout(function() {
                        Bandeja.CargarBandeja(GLOBAL.pg,HTMLCargarBandeja);
                    }, 5000);
                } else {
                    Psi.mensaje("danger", data.msj, 5000);
                }
            });
        }
    },
    CapacityMultiple:function(datos, evento){
           $.ajax({
               url         : 'bandeja/capacity-multiple',
               type        : 'POST',
               cache       : false,
               async       : false,
               dataType    : 'json',
               data        : datos,
               beforeSend: function() {
                   eventoCargaMostrar();
               },
               success: function(obj) {
                    //eventoCargaRemover();
                    if (obj.rst==1 && !$.isEmptyObject(obj.raw)) {
                        evento(obj);    
                    } else if (obj.rst==1 && $.isEmptyObject(obj.raw)) {
                        Psi.mensaje('danger', '(CM) No hay capacidad en la fecha y hora seleccionado. ' . obj.msj, 6000);
                        evento(false);  
                    } else {
                        Psi.mensaje('danger', obj.msj, 10000);
                        evento(false);  
                    }
               },
               error: function() {
                   eventoCargaRemover();
                   Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 5000);
               }
           });
       },
       SetForzarOfsc: function(codactu, activar){
             data = localStorage.getItem("forzarAgendaOFSC");
             data = JSON.parse(data);
             find = OFSC.FindCodactuForzar(codactu, data);
             objeto = {codactu: codactu, activar: activar};
             if (find === -1){
                data.push(objeto);
            } else {
                data[find] = objeto;
            }
             localStorage.setItem("forzarAgendaOFSC", JSON.stringify(data));
       },

       FindCodactuForzar: function (codactu, data){
            index = -1;
            for (var j in data){
                if (data[j].codactu == codactu){
                    return j;
                }
            }
            return index;
       },

       getEstadoForzarOFSC: function (codactu){
            estado = "";
            data = localStorage.getItem("forzarAgendaOFSC");
            data = JSON.parse(data);
            for (var j in data){
                if (data[j].codactu == codactu){
                    return data[j].activar;
                }
            }
            return estado;
       }
};
</script>
