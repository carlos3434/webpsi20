<script type="text/javascript">
GLOBAL = {};
var temporalBandeja=0;
var table;

$(document).ready(function() {
    eventoCargaMostrar();
    // Sidebar separator
        var objSeparator = $('#separator');

        objSeparator.click(function(e) {
            e.preventDefault();
            if (objMain.hasClass('use-sidebar')) {
                hideSidebar();
            }
            else {
                showSidebar();
            }
        }).css('height', objSeparator.parent().outerHeight() + 'px');

    $('#t_bandeja').on('click', 'td a.pplo', function () {
        var tr = $(this).closest('tr');
        var row = GLOBAL.table_bandeja.row(tr);
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
 
            $.ajax({
                url         : 'bandeja/detallebandeja',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {'aid':row.data()['aid']},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.error==undefined || obj.error==false){
                        row.child(format2(obj)).show();
                        tr.addClass('shown');
                    } else {
                        var table='<table class="table table-hover table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"> <tr><td>No existe la actividad en OFSC</td></tr>';
                        row.child(table).show();
                        tr.addClass('shown');
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000);
                }
            });
        }
    });
    var y_inicio;
    var x_inicio;
    var coord_y;
    var coord_x;
    var x_fin;
    var y_fin;
    $('#t_bandeja').on('click', 'td a.pplu', function () {
        var tr = $(this).closest('tr');
        var row = GLOBAL.table_bandeja.row(tr);
            $.ajax({
                url         : 'bandeja/detalleactivity',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {'aid':row.data()['aid']},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst==0){
                        Psi.mensaje('danger', 'No existe la actividad en OFSC', 8000); 
                    } else {
                        var telefonos = obj.ultimo.fonos_contacto;
                        telefonos = telefonos.split('|');
                        if (telefonos.length > 1) {
                            for(var i = 0; i < telefonos.length ; i++){
                                if(!telefonos[i]){
                                    telefonos[i] = '';
                                }
                            }
                        } else{
                            telefonos[0] = '';
                            telefonos[1] = '';
                        }

                        var html = "<tr>" +
                                "<td>Codigo de Actuacion:</td><td>"+obj.ultimo.codactu+"</td>"+ "<td>Nombre Tecnico:</td><td>"+((obj.ultimo.nombre_tecnico == null)?"Sin Tecnico":obj.ultimo.nombre_tecnico)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Quiebre:</td><td>"+obj.ultimo.quiebre_nombre+"</td>"+ "<td>Carnet de Tecnico:</td><td>"+((obj.ultimo.carnet== null)?"Sin Tecnico":obj.ultimo.carnet)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Estado:</td><td>"+obj.ultimo.estado_nombre+"</td>"+ "<td>Celular de Tecnico:</td><td>"+((obj.ultimo.celular== null)?"Sin Tecnico":obj.ultimo.celular)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Motivo:</td><td>"+obj.ultimo.motivo_nombre+"</td>"+ "<td>Nombre de Cliente:</td><td>"+obj.ultimo.nombre_cliente+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Submotivo:</td><td>"+obj.ultimo.submotivo_nombre+"</td>"+ "<td>Telefono de Cliente:</td><td>"+telefonos[0]+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Estado OFSC:</td><td>"+obj.ultimo.estado_ofsc+"</td>"+"<td>Celular de Cliente:</td><td>"+telefonos[1]+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Direccion:</td><td>"+obj.ultimo.direccion_instalacion+"</td>"+"<td>Fecha Agenda:</td><td>"+obj.ultimo.fecha_agenda+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Observacion:</td><td>"+obj.ultimo.observacion+"</td>"+
                            "</tr>";
                        y_inicio=obj.ultimo.y_inicio;
                        x_inicio=obj.ultimo.x_inicio;

                        y_fin=obj.ultimo.y_fin;
                        x_fin=obj.ultimo.x_fin;


                        coord_x=parseFloat(obj.ultimo.x);
                        coord_y=parseFloat(obj.ultimo.y);

                        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
                            x_fin=parseFloat(x_fin);
                            y_fin=parseFloat(y_fin);

                            if(y_inicio !== null && x_inicio !== null && y_inicio != 0 && x_inicio != 0){
                                y_inicio = parseFloat(y_inicio);
                                x_inicio = parseFloat(x_inicio);
                            }else{
                                x_inicio=null;
                                y_inicio=null;
                            }
                        }

                        $('#t_movimiento_ofsc').dataTable().fnDestroy();

                        var variablestable=[];
                        table= $("#t_movimiento_ofsc").DataTable({
                            "data": obj.datos,
                            "iDisplayLength": 5,
                            "order": [[ 1, "desc" ],[0, "desc"]],
                            "columnDefs": [
                                { "targets": 0,
                                    "data": "id_detalle_table",
                                },
                                { "targets": 1,
                                    "data": "fecha_movimiento",
                                },
                                { "targets": 2,
                                    "data": "empresa" },
                                { "targets": 3,
                                    "data": "nombre_cliente_critico" },
                                { "targets": 4,
                                    "data": "fh_agenda"
                                },
                                { "targets": 5,
                                    "data": "celula"
                                },
                                { "targets": 6,
                                    "data": "tecnico" },
                                { "targets": 7,
                                    "data": function ( row, type, val, meta ) {
                                        if (row.estado_ofsc == 'Iniciada') {
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_image('"+row.aid+"', 'Iniciada', '0')\">" + row.estado_ofsc + "</a>";
                                        } else if (row.estado_ofsc == 'Completada') { i++;
                                            arraydetalle[i]={tipo:row.tipo_ofsc,motivo:row.motivo_ofsc,submotivo:row.submotivo_ofsc};
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_image('"+row.aid+"', 'Completada',"+i+")\">" + row.estado_ofsc + "</a>";
                                        } else if (row.estado_ofsc == 'No Realizada') { i++;
                                            arraydetalle[i]={tipo:row.tipo_ofsc,motivo:row.motivo_ofsc,submotivo:row.submotivo_ofsc};
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_image('"+row.aid+"', 'No Realizada',"+i+")\">" + row.estado_ofsc + "</a>";
                                        } else {
                                            return row.estado+'<br><font color="#327CA7">'+$.trim(row.cierre)+'</font>';
                                        }

                                    },
                                    "defaultContent": ''
                                },
                                { "targets": 8,
                                    "data": "usuario" },
                                {   "targets": 9,
                                    "orderable":      false,
                                    "data": function ( row, type, val, meta ) {
                                        if( $.trim(row.observaciones)!=='' ){
                                            variablestable.push(row.id_detalle_table);
                                        }
                                        return row.observacion;
                                    },
                                    "defaultContent": ''
                                },
                            ]
                        });
                        var tr;
                        for(i=0;i<variablestable.length;i++){
                            trsimple=$("#tb_movimiento_ofsc tr td").filter(function() {
                                return $(this).text() == variablestable[i];
                            }).parent('tr');
                            $(trsimple).find("td:eq(9)").css('color','red').addClass('details-control');
                        }
                        $("#detalleOfsc").html(html);
                        // -- Componentes
                        $("#componente-ofsc-modal").html('');
                        $.each(obj.componente, function(val, text) {
                            $("#componente-ofsc-modal")
                                .append('<li class="list-group-item">' + text.nombre + '</li>');
                        });
                        gestionDetalle.aid = obj.aid;
                        gestionDetalle.estado_ofsc = (obj.ultimo.estado_ofsc)? obj.ultimo.estado_ofsc : '';

                        $("#ofscModal").modal("show");
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 8000);
                }
            });
    } );

    function format2 ( obj ) {
        var r="";        r= '<table class="table table-hover table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
        r+=     '<tr>';
        r+=     '<th>Id Actividad</th>';
        r+=     '<th>Id Recurso</th>';
        r+=     '<th>Fecha</th>';
        r+=     '<th>Cliente</th>';
        r+=     '<th>Dirección</th>';
        r+=     '<th>N° de Servicio</th>';
        r+=     '<th>Nota</th>';
        r+=     '</tr>';


        r+=     '<tr>';
        if(obj.id!=undefined) {
        r+=         '<td>'+obj.id+'</td>';}
        if(obj.resource_id!=undefined) {
        r+=         '<td>'+obj.resource_id+'</td>';}
        if(obj.date!=undefined) {
        r+=         '<td>'+obj.date+'</td>';}
        if(obj.name!=undefined) {
        r+=         '<td>'+obj.name+'</td>';}
        if(obj.address!=undefined) {
        r+=         '<td>'+obj.address+'</td>';}
        if(obj.XA_NUMBER_SERVICE_ORDER!=undefined) {
        r+=         '<td>'+obj.XA_NUMBER_SERVICE_ORDER+'</td>';}
        if(obj.XA_NOTE!=undefined) {
        r+=         '<td>'+obj.XA_NOTE+'</td>';}
        r+=     '</tr>';
        r+=  '</table>';

        return r;
    };

    $('#ofscModal').on('shown.bs.modal', function () {
        initMaps(y_inicio,x_inicio,coord_y,coord_x,'maps_ofsc',y_fin,x_fin);
        geoStreetView(coord_y, coord_x, 'street_ofsc');
        if(y_inicio !== null && x_inicio !== null && y_inicio != 0 && x_inicio != 0){
            $("#street_ofsc2").css('display','inline');
            geoStreetView(y_inicio, x_inicio, 'street_ofsc2');
        }
        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
            $("#street_ofsc3").css('display','inline');
            geoStreetView(y_fin, x_fin, 'street_ofsc3');
        }
        $('#ofscModal .nav-ofsc-modal > li').css('display', 'block');
        localStorage.removeItem("bandejaPestaniaMovimiento");
    });

    $('#ofscModal').on('show.bs.modal', function () {
        $("#ofscModal .tab-content-ofsc-modal div:first-child, #ofscModal .nav-ofsc-modal li:first-child").addClass("active");
    });


    $('#ofscModal').on('hide.bs.modal', function (event) {
        $('#ofscModal .nav-ofsc-modal > li, #ofscModal div[id^=tab_ofsc_]').removeClass('active');
    });

    $('.nav-tabs a[href="#tab_ofsc_0"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        initMaps(y_inicio,x_inicio,coord_y,coord_x,'maps_ofsc',y_fin,x_fin);
        geoStreetView(coord_y, coord_x, 'street_ofsc');
        if(y_inicio !== null && x_inicio !== null  && y_inicio != 0 && x_inicio != 0){
            $("#street_ofsc2").css('display','inline');
            geoStreetView(y_inicio, x_inicio, 'street_ofsc2');
        }
        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
            $("#street_ofsc3").css('display','inline');
            geoStreetView(y_fin, x_fin, 'street_ofsc3');
        }
        e.relatedTarget // previous active tab
    });

    $("#form_Personalizado").attr("onkeyup","return enterGlobal(event,'btn_personalizado')");
    $("#form_General").attr("onkeyup","return enterGlobal(event,'btn_general')");
    $("[data-toggle='offcanvas']").click();
    $("#slct_tipo").change(ValidaTipo);
    $("#btn_personalizado").click(personalizado);
    $("#btn_general").click(general);
    $('#fecha_agenda').daterangepicker({
            format: 'YYYY-MM-DD'
    });
    $('#fecha_registro').val(
        moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $('#fecha_ultimo').daterangepicker({
        startDate: moment().subtract(15, 'days'),
        endDate: moment(), 
        format: 'YYYY-MM-DD'
    });
    $('#fecha_ultimo').on('apply.daterangepicker', function(ev, picker) {
        $('#fecha_registro').val('');
    });
    $("#fecha_registro_psi").daterangepicker({endDate: moment(), format: 'YYYY-MM-DD'});
    $('#fecha_registro').daterangepicker({
            minDate: moment().subtract(30, 'days'),
            maxDate:  moment(),
            startDate: moment().subtract(15, 'days'),
            endDate: moment(),
            format: 'YYYY-MM-DD'
    });
    $('#fecha_consolidacion').daterangepicker({
            singleDatePicker: true ,
            format: 'YYYY-MM-DD'
        }
    );

    listado = functionajax('listado/valid', 'POST', false, 'json', false);
    listado.success(function(obj){
        var html="";
        var can=obj.length;
        var ca=can-1;
        var c=0;
        for (var i = 0; i < obj.length; i++) {
            if(c=='12'){
                html+='</div>';
                html+='<div class="col-sm-12 detalle" style="display:none">';
                c=0;
            }
            c+=obj[i].columna;
            if(i===0){
                html+='<div class="col-sm-12 detalle" style="display:none">';
            }
            html+='<div class="col-sm-'+obj[i].columna+'">';
            html+='<label class="titulo">'+obj[i].nombre+':</label>';

            if(obj[i].option2=='multiple'){
                html+='<select class="form-control" name="'+obj[i].descripcion+'[]" id="'+obj[i].descripcion+'" '+obj[i].option2+'>';
            }else{
                html+='<select class="form-control" name="'+obj[i].descripcion+'" id="'+obj[i].descripcion+'" '+obj[i].option2+'>';
            }
            html+='<option value="">.::Seleccione::.</option>';
            if(obj[i].option !== null){
              html+=obj[i].option;
            }
            html+='</select>';
            html+='</div>';
        }
        html+='</div>';
        $("#valid_filtro").html(html);
    });
    // Con BD
    //  controlador,slct,tipo,usuarioV,afectado,afectados,slct_id
    var ids = []; //para seleccionar un id
    var data = {usuario: 1};
    GLOBAL.data_usuario = {usuario: 1};
    GLOBAL.data_bandeja = {bandeja: 1};
    
    
    GLOBAL.controladores = ["actividad", "estadoofsc", "estado", "quiebre", "empresa", "celula", "tecnico", "zonal", "troba", "nodo", "mdf","motivo","submotivo",'solucion','feedback','cat_componente','obs_tipo'];
    GLOBAL.slcts = ["slct_actividad", "slct_estado_ofsc", "slct_estado", "slct_quiebre", "slct_empresa", "slct_celula", "slct_tecnico", "slct_zonal", "slct_troba", "slct_nodo", "slct_mdf","slct_motivo","slct_submotivo",'slct_solucion_modal','slct_feedback_modal','slct_componente_modal','slct_obs_tipo'];
    GLOBAL.tipo = ["multiple", "multiple", "multiple","multiple","multiple","multiple","multiple","multiple","multiple","multiple", "multiple","multiple","multiple",'simple','simple','simple'];
    GLOBAL.valarray = [0, 0, 0, ids, ids, ids, ids, ids, ids, ids, ids,ids,ids,[17],[10],0];
    GLOBAL.data = [{}, {}, {}, GLOBAL.data_usuario, GLOBAL.data_usuario, {},{} ,GLOBAL.data_usuario, GLOBAL.data_usuario, GLOBAL.data_bandeja, GLOBAL.data_bandeja,{},{},1,1,{}];
    GLOBAL.afectado = [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1,1,0,0,0];
    GLOBAL.afectados = ["", "", "#slct_submotivo", "#slct_motivo", "#slct_celula,#slct_tecnico", "#slct_tecnico", "", "", "", "", "","#slct_submotivo","",'','',''];
    GLOBAL.slct_id = ["", "", "A", "Q", "E", "C", "", "", "", "","","N","",'','',''];

    slctGlobal.listarSlctMasivo(GLOBAL.controladores, GLOBAL.slcts, GLOBAL.tipo, GLOBAL.valarray, GLOBAL.data, GLOBAL.afectado, GLOBAL.afectados, GLOBAL.slct_id);

    // Solo ingresar los ids, el primer registro es sin #.
    slctGlobalHtml('slct_tipo,#slct_legado,#slct_coordinado','simple');
    slctGlobalHtml('slct_transmision,#slct_cierre_estado','multiple');
    slctGlobalHtml('slct_envio_ofsc','simple');
    slctGlobalHtml('slct_tipo_agendamiento','simple');
    slctGlobalHtml('slct_estado_envio_masivo_ofsc','simple');
    slctGlobalHtml("slct_tiene_componentes", "simple");
    slctGlobalHtml("slct_c_componentes", "multiple");
    $(".expImage").hide();

    activarTabla();

    $(document).delegate(".btn-detalle-toa", "click", function(e){
        eventoCargaMostrar();
        OFSC.GetActivityOfsc($(this).attr("data-codactu"), $(this), false);
    });

    setTimeout(function(){eventoCargaRemover();}, 1000);

    // -- Obtener filtro de Imagenes OFSC
    $('#ofscModal .tab_1, #ofscModalMovimiento').on('click', function (e) {
        consultarOfscActivity(gestionDetalle.aid);
    });
});
descargarReporteImagen=function(){
    $("#form_General").append("<input type='hidden' name='imagen' id='imagen' value='1'>");
    $("#form_General").submit();
    $("#form_General #imagen").remove();
};
validarImagenExp=function(val){
    var datos=$("#slct_transmision").val();
    $(".expImage").hide();
    if( datos!=='null' && datos!==null && datos.length>0 ){
        if( datos.join("|").split("-").length>1 ){
            //$(".expImage").show();
        }
    }
};
descargarReporte=function(){
   $("#form_General").append("<input type='hidden' name='totalPSI' id='totalPSI' value='1'>");
   $("#form_General").append("<input type='hidden' name='isSoloActuaciones'  id='isSoloActuaciones' value='0'>");
    $("#form_General").submit();
    $("#form_General #totalPSI").remove();
    $("#form_General #isSoloActuaciones").remove();
};
personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        alert("Seleccione Tipo Filtro");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        alert("Ingrese datos");
        $("#txt_buscar").focus();
    } else{
        $("#txt_buscar").val($.trim($("#txt_buscar").val()));
        Bandeja.CargarBandeja("P",HTMLCargarBandeja);
    }
};
general=function(){
    Bandeja.CargarBandeja("G",HTMLCargarBandeja);
};
ValidaTipo=function(){
    $("#txt_buscar").val("");
    $("#txt_buscar").focus();
};
HTMLCargarBandeja=function(datos){
var html="";
var gestionado="";
var officetrackbotton="";
    $('#t_bandeja').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        gestionado="";
        if(data.id===''){
            temporalBandeja++;
            data.id="T_"+temporalBandeja;
        } else{
            data.id="ID_"+data.id;
        }
        fondo="";
        if(data.existe*1!=1){
            fondo="style='background-color: #DA8F66'";
        }

        html+="<tr>"+
        "<td "+fondo+">"+data.id+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.fecha_registro+"</td>"+
        "<td>"+data.actividad+"</td>"+
        "<td>"+data.quiebre+"</td>"+
        "<td>"+data.empresa+"</td>"+
        "<td>"+data.mdf+"</td>"+
        "<td>"+data.fh_agenda+"</td>"+
        "<td>"+data.tecnico+"</td>"+
        "<td>"+data.estado_ofsc+"</td>"+
        "<td>"+data.estado+"<br><font color='#327CA7'>"+data.cierre_estado+"</font></td>";

        officetrackColor='';officetrackImage='';officetrackbotton='';
        if(data.transmision!==0 && data.transmision!=='0'){
            officetrackColor='btn-default';
            if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,6).toLowerCase()=='inicio' ){
                officetrackColor='btn-success';
            }
            else if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,11).toLowerCase()=='supervision'){
                officetrackColor='btn-warning';
            }
            else if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,6).toLowerCase()=='cierre' ){
                officetrackColor='btn-primary';
            }

            if(officetrackColor!='btn-default'){
                officetrackbotton='data-toggle="modal" data-target="#officetrackModal" data-id="'+data.id.split("_")[1]+'"';
            }
            officetrackImage=' &nbsp; <a class="btn '+officetrackColor+' btn-sm" '+officetrackbotton+'><i class="fa fa-mobile fa-3x"></i> </a>';
        }
        html+=
            '<td><a class="btn bg-navy btn-sm" data-toggle="modal" data-target="#bandejaModal" data-codactu="'+data.codactu+'"><i class="fa fa-desktop fa-lg"></i> </a>'+
            officetrackImage+
        '</td>';
        html+="</tr>";

    });
    $("#tb_bandeja").html(html);
    activarTabla();
    $('#t_bandeja').attr("style","width:''");
};

activarTabla=function(){
    $("#t_bandeja").dataTable(); // inicializo el datatable
};
validaBandeja=function(){
    $('#form_roles [data-toggle="tooltip"]').css("display","none");
    var a=[];
    a[0]=valida("txt","descripcion","");
    var rpta=true;
    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};
valida=function(inicial,id,v_default){
    var texto="Seleccione";
    if(inicial=="txt"){
        texto="Ingrese";
    }
    if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
        $('#error_'+id).attr('data-original-title',texto+' '+id);
        $('#error_'+id).css('display','');
        return false;
    }
};
</script>