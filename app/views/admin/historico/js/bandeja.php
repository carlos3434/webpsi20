<script type="text/javascript">
GLOBAL = {};
var temporalBandeja=0;
var table;

$(document).ready(function() {
     localStorage.removeItem("bandejaMasivoActivo");
     localStorage.removeItem("bandejaBusqueda");
     localStorage.removeItem("forzarAgendaOFSC");
     localStorage.setItem("forzarAgendaOFSC", "[]");

     $(document).delegate(".btn-desactivar-envio-ofsc", "click", function(){
        codactu = $(this).attr("data-codactu");
        if (confirm("¿Desea forzar el envio a OFSC para el requerimiento "+codactu+"?")) {
            $(this).css("display", "none");
            $("a[data-codactu="+codactu+"].btn-activar-envio-ofsc").css("display", "inline-block");
            OFSC.SetForzarOfsc(codactu, 1);
        }
     });

     $(document).delegate(".btn-activar-envio-ofsc", "click", function(){
        codactu = $(this).attr("data-codactu");
        if (confirm("¿Desea desactivar forzar el envio a OFSC para el requerimiento "+codactu+" ?")) {
            $(this).css("display", "none");
            $("a[data-codactu="+codactu+"].btn-desactivar-envio-ofsc").css("display", "inline-block");
            OFSC.SetForzarOfsc(codactu, 0);
        }
     });

    // $('#t_bandeja').on('click', 'td.details-control', function () {
    $('#t_bandeja').on('click', 'td a.pplo', function () {
    // $('#t_bandeja').click('td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = GLOBAL.table_bandeja.row(tr);
                
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
 
            $.ajax({
                url         : 'bandeja/detallebandeja',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {'aid':row.data()['aid']},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                     $(".overlay,.loading-img").remove();
                    //if(obj.status=='404'){                        
                        if(obj.error==undefined || obj.error==false){
                          row.child(format2(obj)).show();
                          tr.addClass('shown');
                        } else {
                          var table='<table class="table table-hover table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"> <tr><td>No existe la actividad en OFSC</td></tr>';
                          row.child(table).show();
                          tr.addClass('shown');
                        }
                    

                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                            '<i class="fa fa-ban"></i>'+
                                            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                            '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                        '</div>');
                }
            });


        }
    } );
    var y_inicio;
    var x_inicio;
    var coord_y;
    var coord_x;
    var x_fin;
    var y_fin;
    $('#t_bandeja').on('click', 'td a.pplu', function () {
        // $('#t_bandeja').click('td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = GLOBAL.table_bandeja.row(tr);


            $.ajax({
                url         : 'bandeja/detalleactivity',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {'aid':row.data()['aid']},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    //if(obj.status=='404'){
                    if(obj.rst==0){
                        $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                            '<i class="fa fa-ban"></i>'+
                            '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                            '<b>No existe la actividad en OFSC</b>'+
                            '</div>');
                    } else {
                        var telefonos = obj.ultimo.fonos_contacto;
                        telefonos = telefonos.split('|');
                        if(telefonos.length > 1){
                            for(var i = 0; i < telefonos.length ; i++){
                                if(!telefonos[i]){
                                    telefonos[i] = '';
                                }
                            }
                        }else{
                            telefonos[0] = '';
                            telefonos[1] = '';
                        }

                        var html = "<tr>" +
                                "<td>Codigo de Actuacion:</td><td>"+obj.ultimo.codactu+"</td>"+ "<td>Nombre Tecnico:</td><td>"+((obj.ultimo.nombre_tecnico == null)?"Sin Tecnico":obj.ultimo.nombre_tecnico)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Quiebre:</td><td>"+obj.ultimo.quiebre_nombre+"</td>"+ "<td>Carnet de Tecnico:</td><td>"+((obj.ultimo.carnet== null)?"Sin Tecnico":obj.ultimo.carnet)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Estado:</td><td>"+obj.ultimo.estado_nombre+"</td>"+ "<td>Celular de Tecnico:</td><td>"+((obj.ultimo.celular== null)?"Sin Tecnico":obj.ultimo.celular)+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Motivo:</td><td>"+obj.ultimo.motivo_nombre+"</td>"+ "<td>Nombre de Cliente:</td><td>"+obj.ultimo.nombre_cliente+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Submotivo:</td><td>"+obj.ultimo.submotivo_nombre+"</td>"+ "<td>Telefono de Cliente:</td><td>"+telefonos[0]+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Estado OFSC:</td><td>"+obj.ultimo.estado_ofsc+"</td>"+"<td>Celular de Cliente:</td><td>"+telefonos[1]+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Direccion:</td><td>"+obj.ultimo.direccion_instalacion+"</td>"+"<td>Fecha Agenda:</td><td>"+obj.ultimo.fecha_agenda+"</td>"+
                            "</tr>"+
                            "<tr>" +
                                "<td>Observacion:</td><td>"+obj.ultimo.observacion+"</td>"+
                            "</tr>";
                        y_inicio=obj.ultimo.y_inicio;
                        x_inicio=obj.ultimo.x_inicio;

                        y_fin=obj.ultimo.y_fin;
                        x_fin=obj.ultimo.x_fin;


                        coord_x=parseFloat(obj.ultimo.x);
                        coord_y=parseFloat(obj.ultimo.y);

                        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
                            x_fin=parseFloat(x_fin);
                            y_fin=parseFloat(y_fin);

                            if(y_inicio !== null && x_inicio !== null && y_inicio != 0 && x_inicio != 0){
                                y_inicio = parseFloat(y_inicio);
                                x_inicio = parseFloat(x_inicio);
                            }else{
                                x_inicio=null;
                                y_inicio=null;
                            }
                        }

                        $('#t_movimiento_ofsc').dataTable().fnDestroy();

                        var variablestable=[];
                        table= $("#t_movimiento_ofsc").DataTable({
                            "data": obj.datos,
                            "iDisplayLength": 5,
                            "order": [[ 1, "desc" ],[0, "desc"]],
                            "columnDefs": [
                                { "targets": 0,
                                    "data": "id_detalle_table",
                                },
                                { "targets": 1,
                                    "data": "fecha_movimiento",
                                },
                                { "targets": 2,
                                    "data": "empresa" },
                                { "targets": 3,
                                    "data": "nombre_cliente_critico" },
                                { "targets": 4,
                                    "data": "fh_agenda"
                                },
                                { "targets": 5,
                                    "data": "celula"
                                },
                                { "targets": 6,
                                    "data": "tecnico" },
                                { "targets": 7,
                                    "data": function ( row, type, val, meta ) {
                                        if (row.estado_ofsc == 'Iniciada') {
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_inicio('"+row.aid+"', 'Iniciada')\">" + row.estado_ofsc + "</a>";
                                        } else if (row.estado_ofsc == 'Completada') { i++;
                                            arraydetalle[i]={tipo:row.tipo_ofsc,motivo:row.motivo_ofsc,submotivo:row.submotivo_ofsc};
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_completo('"+row.aid+"', 'Completada',"+i+")\">" + row.estado_ofsc + "</a>";
                                        } else if (row.estado_ofsc == 'No Realizada') { i++;
                                            arraydetalle[i]={tipo:row.tipo_ofsc,motivo:row.motivo_ofsc,submotivo:row.submotivo_ofsc};
                                            return "<a href=\"javascript:void(0)\" onclick=\"detalle_ofsc_cancelada('"+row.aid+"', 'No Realizada',"+i+")\">" + row.estado_ofsc + "</a>";
                                        } else {
                                            return row.estado+'<br><font color="#327CA7">'+$.trim(row.cierre)+'</font>';
                                        }

                                    },
                                    "defaultContent": ''
                                },
                                { "targets": 8,
                                    "data": "usuario" },
                                {   "targets": 9,
                                    "orderable":      false,
                                    "data": function ( row, type, val, meta ) {
                                        if( $.trim(row.observaciones)!=='' ){
                                            variablestable.push(row.id_detalle_table);
                                        }
                                        return row.observacion;
                                    },
                                    "defaultContent": ''
                                },
                            ]
                        });
                        var tr;
                        for(i=0;i<variablestable.length;i++){
                            trsimple=$("#tb_movimiento_ofsc tr td").filter(function() {
                                return $(this).text() == variablestable[i];
                            }).parent('tr');
                            $(trsimple).find("td:eq(9)").css('color','red').addClass('details-control');
                        }


                        $("#detalleOfsc").html(html);
                        $("#ofscModal").modal("show");
                    }


                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                        '<i class="fa fa-ban"></i>'+
                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                        '</div>');
                }
            });

    } );


    function format2 ( obj ) {

        var r="";    
        r= '<table class="table table-hover table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
        r+=     '<tr>';
        r+=     '<th>Id Actividad</th>';
        r+=     '<th>Id Recurso</th>';
        r+=     '<th>Fecha</th>';
        r+=     '<th>Cliente</th>';
        r+=     '<th>Dirección</th>';
        r+=     '<th>N° de Servicio</th>';
        r+=     '<th>Nota</th>';
        r+=     '</tr>';


        r+=     '<tr>';
        if(obj.id!=undefined) {
        r+=         '<td>'+obj.id+'</td>';}
        if(obj.resource_id!=undefined) {
        r+=         '<td>'+obj.resource_id+'</td>';}
        if(obj.date!=undefined) {
        r+=         '<td>'+obj.date+'</td>';}
        if(obj.name!=undefined) {
        r+=         '<td>'+obj.name+'</td>';}
        if(obj.address!=undefined) {
        r+=         '<td>'+obj.address+'</td>';}
        if(obj.XA_NUMBER_SERVICE_ORDER!=undefined) {
        r+=         '<td>'+obj.XA_NUMBER_SERVICE_ORDER+'</td>';}
        if(obj.XA_NOTE!=undefined) {
        r+=         '<td>'+obj.XA_NOTE+'</td>';}
        r+=     '</tr>';
        r+=  '</table>';

        return r;
    };


    $('#ofscModal').on('shown.bs.modal', function () {
        initMaps(y_inicio,x_inicio,coord_y,coord_x,'maps_ofsc',y_fin,x_fin);
        geoStreetView(coord_y, coord_x, 'street_ofsc');
        if(y_inicio !== null && x_inicio !== null && y_inicio != 0 && x_inicio != 0){
            $("#street_ofsc2").css('display','inline');
            geoStreetView(y_inicio, x_inicio, 'street_ofsc2');
        }
        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
            $("#street_ofsc3").css('display','inline');
            geoStreetView(y_fin, x_fin, 'street_ofsc3');
        }

//        $('#tab_ofsc_0').css('display','inline');
//        $('#tab_ofsc_0').css('visibility','visible');
        $('#tab_ofsc_0').addClass('active');
        if($('#tab_ofsc_0').hasClass('active')){
            initMaps(y_inicio,x_inicio,coord_y,coord_x,'maps_ofsc',y_fin,x_fin);
            geoStreetView(coord_y, coord_x, 'street_ofsc');
            if(y_inicio !== null && x_inicio !== null && y_inicio != 0 && x_inicio != 0){
                $("#street_ofsc2").css('display','inline');
                geoStreetView(y_inicio, x_inicio, 'street_ofsc2');
            }
            if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
                $("#street_ofsc3").css('display','inline');
                geoStreetView(y_fin, x_fin, 'street_ofsc3');
            }
        }

    });

    $('.nav-tabs a[href="#tab_ofsc_0"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        initMaps(y_inicio,x_inicio,coord_y,coord_x,'maps_ofsc',y_fin,x_fin);
        geoStreetView(coord_y, coord_x, 'street_ofsc');
        if(y_inicio !== null && x_inicio !== null  && y_inicio != 0 && x_inicio != 0){
            $("#street_ofsc2").css('display','inline');
            geoStreetView(y_inicio, x_inicio, 'street_ofsc2');
        }
        if(y_fin !== null && x_fin !== null && y_fin != 0 && x_fin != 0){
            $("#street_ofsc3").css('display','inline');
            geoStreetView(y_fin, x_fin, 'street_ofsc3');
        }
        e.relatedTarget // previous active tab
    });

    //  $(".btn-activar-masivo").click(function(e){
    //     activarEnvioMasivo($(this));
    // });


    $("#form_Personalizado").attr("onkeyup","return enterGlobal(event,'btn_personalizado')");
    $("#form_General").attr("onkeyup","return enterGlobal(event,'btn_general')");
	$("[data-toggle='offcanvas']").click();
    $("#slct_tipo").change(ValidaTipo);
    $("#btn_personalizado").click(personalizado);
    $("#btn_general").click(general);
    $('#fecha_agenda').daterangepicker({
            format: 'YYYY-MM-DD'
    });
    $('#fecha_registro').val(
        moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $('#fecha_ultimo').daterangepicker({
        startDate: moment().subtract(15, 'days'),
        endDate: moment(), 
        format: 'YYYY-MM-DD'
    });
    $('#fecha_ultimo').on('apply.daterangepicker', function(ev, picker) {
        $('#fecha_registro').val('');
    });
    $("#fecha_registro_psi").daterangepicker({endDate: moment(), format: 'YYYY-MM-DD'});
    $('#fecha_registro').daterangepicker({
            minDate: moment().subtract(30, 'days'),
            maxDate:  moment(),
            startDate: moment().subtract(15, 'days'),
            endDate: moment(),
            format: 'YYYY-MM-DD'
    });
    $('#fecha_consolidacion').daterangepicker({
            singleDatePicker: true ,
            format: 'YYYY-MM-DD'
        }
    );
    /*$('#movimientoModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // captura al boton
        var modal = $(this); //captura el modal
        variables={ codactu:button.data('codactu') };
        Bandeja.CargarGestionMovimiento(variables,HTMLCargarGestionMovimiento);
    });

    $('#movimientoModal').on('hide.bs.modal', function (event) {
      var modal = $(this); //captura el modal
    });

    $('#observacionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // captura al boton
        var modal = $(this); //captura el modal
        variables={ codactu:button.data('codactu'),first:'1' };
        Bandeja.CargarGestionMovimiento(variables,HTMLCargarGestionMovimientoFirst);
    });

    $('#observacionModal').on('hide.bs.modal', function (event) {
      var modal = $(this); //captura el modal
      $("#txt_observacion_o_modal").val("");
      $('#form_observacion input[type="hidden"]').remove();
    });*/
    listado = functionajax('listado/valid', 'POST', false, 'json', false, {controladores:'varible1'});
    listado.success(function(obj){
        var html="";
        var can=obj.length;
        var ca=can-1;
        var c=0;
        for (var i = 0; i < obj.length; i++) {
            if(c=='12'){
                html+='</div>';
                html+='<div class="col-sm-12 detalle" style="display:none">';
                c=0;
            }
            c+=obj[i].columna;
            if(i===0){
                html+='<div class="col-sm-12 detalle" style="display:none">';
            }
            html+='<div class="col-sm-'+obj[i].columna+'">';
            html+='<label class="titulo">'+obj[i].nombre+':</label>';

            if(obj[i].option2=='multiple'){
                html+='<select class="form-control" name="'+obj[i].descripcion+'[]" id="'+obj[i].descripcion+'" '+obj[i].option2+'>';
            }else{
                html+='<select class="form-control" name="'+obj[i].descripcion+'" id="'+obj[i].descripcion+'" '+obj[i].option2+'>';
            }
            html+='<option value="">.::Seleccione::.</option>';
            if(obj[i].option !== null){
              html+=obj[i].option;
            }
            html+='</select>';
            html+='</div>';
        }
        html+='</div>';
        $("#valid_filtro").html(html);
    });
    // Con BD
    //  controlador,slct,tipo,usuarioV,afectado,afectados,slct_id
    var ids = []; //para seleccionar un id
    var data = {usuario: 1};
    GLOBAL.data_usuario = {usuario: 1};
    GLOBAL.data_bandeja = {bandeja: 1};

    GLOBAL.controladores = ["actividad", "estadoofsc", "estado", "quiebre", "empresa", "celula", "tecnico", "zonal", "troba", "nodo", "mdf","motivo","submotivo"];
    GLOBAL.slcts = ["slct_actividad", "slct_estado_ofsc", "slct_estado", "slct_quiebre", "slct_empresa", "slct_celula", "slct_tecnico", "slct_zonal", "slct_troba", "slct_nodo", "slct_mdf","slct_motivo","slct_submotivo"];
    GLOBAL.tipo = ["multiple", "multiple", "multiple","multiple","multiple","multiple","multiple","multiple","multiple","multiple", "multiple","multiple","multiple"];
    GLOBAL.valarray = [0, 0, 0, ids, ids, ids, ids, ids, ids, ids, ids,ids,ids];
    GLOBAL.data = [{}, {}, {}, GLOBAL.data_usuario, GLOBAL.data_usuario, {},{} ,GLOBAL.data_usuario, GLOBAL.data_usuario, GLOBAL.data_bandeja, GLOBAL.data_bandeja,{},{}];
    GLOBAL.afectado = [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1,1];
    GLOBAL.afectados = ["", "", "#slct_submotivo", "#slct_motivo", "#slct_celula,#slct_tecnico", "#slct_tecnico", "", "", "", "", "","#slct_submotivo",""];
    GLOBAL.slct_id = ["", "", "A", "Q", "E", "C", "", "", "", "","","N",""];
    eventoCargaMostrar();
    slctGlobal.listarSlctMasivo(GLOBAL.controladores, GLOBAL.slcts, GLOBAL.tipo, GLOBAL.valarray, GLOBAL.data, GLOBAL.afectado, GLOBAL.afectados, GLOBAL.slct_id);

   /* slctGlobal.listarSlct('actividad','slct_actividad','multiple');
    //slctGlobal.listarSlct('actividadtipo','slct_actividad_tipo','multiple');
    slctGlobal.listarSlct('estadoofsc','slct_estado_ofsc','multiple');
    slctGlobal.listarSlct('estado','slct_estado','multiple');
    slctGlobal.listarSlct('quiebre','slct_quiebre','multiple',ids,data);
    slctGlobal.listarSlct('empresa','slct_empresa','multiple',ids,data,0,'#slct_celula,#slct_tecnico','E');
    slctGlobal.listarSlct('celula','slct_celula','multiple',ids,0,1,'#slct_tecnico','C');
    slctGlobal.listarSlct('tecnico','slct_tecnico','multiple',ids,0,1);
    slctGlobal.listarSlct('zonal','slct_zonal','multiple',ids,data);
    slctGlobal.listarSlct('troba','slct_troba','multiple',ids,data);

    data = {bandeja: 1};
    slctGlobal.listarSlct('nodo','slct_nodo','multiple',ids,data);
    slctGlobal.listarSlct('mdf','slct_mdf','multiple',ids,data);*/

    // Solo ingresar los ids, el primer registro es sin #.
    slctGlobalHtml('slct_tipo,#slct_legado,#slct_coordinado','simple');
    slctGlobalHtml('slct_transmision,#slct_cierre_estado','multiple');
    slctGlobalHtml('slct_envio_ofsc','simple');
    slctGlobalHtml('slct_tipo_agendamiento','simple');
    slctGlobalHtml('slct_estado_envio_masivo_ofsc','simple');
    slctGlobalHtml("slct_tiene_componentes", "simple");
    slctGlobalHtml("slct_c_componentes", "multiple");
    $(".expImage").hide();

    activarTabla();
    setTimeout(function(){eventoCargaRemover();}, 3000);

    $(document).delegate(".btn-detalle-toa", "click", function(e){
        eventoCargaMostrar();
        OFSC.GetActivityOfsc($(this).attr("data-codactu"), $(this), false);
    });

    GLOBAL.numintentosbandejamasivo = 0;

    $(".btn-activar-masivo").click(function(e){
        activarEnvioMasivo($(this));
    });
    $(".btn-ejecutar-masivo").click(function(e){
        envioMasivoOFSC();
    });
     $(document).delegate("#t_bandeja input[type=checkbox]", "change", function(e){
       updateEstadoCheckboxMasivo($(this));
    });

     $('#check_all_masivo_inf, #check_all_masivo_sup').on('ifUnchecked', function (event) {
            $('#check_all_masivo_inf, #check_all_masivo_sup').iCheck('uncheck');
            $("#t_bandeja tbody input[type=checkbox]").each(function(e){
                $(this).prop("checked", false);
                updateEstadoCheckboxMasivo($(this));
            });
        });
        $('#check_all_masivo_inf, #check_all_masivo_sup').on('ifChecked', function (event) {
            $('#check_all_masivo_inf, #check_all_masivo_sup').iCheck('check');
            $("#t_bandeja tbody input[type=checkbox]").each(function(e){
                $(this).prop("checked", true);
                updateEstadoCheckboxMasivo($(this));
            });
        });
});
descargarReporteImagen=function(){
    $("#form_General").append("<input type='hidden' name='imagen' id='imagen' value='1'>");
    $("#form_General").submit();
    $("#form_General #imagen").remove();
};
validarImagenExp=function(val){
    var datos=$("#slct_transmision").val();
    $(".expImage").hide();
    if( datos!=='null' && datos!==null && datos.length>0 ){
        if( datos.join("|").split("-").length>1 ){
            //$(".expImage").show();
        }
    }
};
descargarReporte=function(){
   $("#form_General").append("<input type='hidden' name='totalPSI' id='totalPSI' value='1'>");
   $("#form_General").append("<input type='hidden' name='isSoloActuaciones'  id='isSoloActuaciones' value='0'>");
    $("#form_General").submit();
    $("#form_General #totalPSI").remove();
    $("#form_General #isSoloActuaciones").remove();
};
personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        alert("Seleccione Tipo Filtro");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        alert("Ingrese datos");
        $("#txt_buscar").focus();
    } else{
        $("#txt_buscar").val($.trim($("#txt_buscar").val()));
        Bandeja.CargarBandeja("P",HTMLCargarBandeja);
	}
};
general=function(){
    Bandeja.CargarBandeja("G",HTMLCargarBandeja);
};
ValidaTipo=function(){
    $("#txt_buscar").val("");
    $("#txt_buscar").focus();
};
HTMLCargarBandeja=function(datos){
var html="";
var gestionado="";
var officetrackbotton="";
    $('#t_bandeja').dataTable().fnDestroy();

	$.each(datos,function(index,data){
        gestionado="";
        if(data.id===''){
            temporalBandeja++;
            data.id="T_"+temporalBandeja;
        } else{
            data.id="ID_"+data.id;
            /*gestionado='<a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#movimientoModal" data-codactu="'+data.codactu+'"><i class="fa fa-search-plus fa-lg"></i> </a>'+
                        '<a class="btn bg-navy btn-sm" data-toggle="modal" data-target="#observacionModal" data-codactu="'+data.codactu+'"><i class="fa fa-comments fa-lg"></i> </a>';*/
        }
        fondo="";
        if(data.existe*1!=1){
            fondo="style='background-color: #DA8F66'";
        }

        html+="<tr>"+
        "<td "+fondo+">"+data.id+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.fecha_registro+"</td>"+
        "<td>"+data.actividad+"</td>"+
        "<td>"+data.quiebre+"</td>"+
        "<td>"+data.empresa+"</td>"+
        "<td>"+data.mdf+"</td>"+
        "<td>"+data.fh_agenda+"</td>"+
        "<td>"+data.tecnico+"</td>"+
        "<td>"+data.estado_ofsc+"</td>"+
        "<td>"+data.estado+"<br><font color='#327CA7'>"+data.cierre_estado+"</font></td>";

        officetrackColor='';officetrackImage='';officetrackbotton='';
        if(data.transmision!==0 && data.transmision!=='0'){
            officetrackColor='btn-default';
            if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,6).toLowerCase()=='inicio' ){
                officetrackColor='btn-success';
            }
            else if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,11).toLowerCase()=='supervision'){
                officetrackColor='btn-warning';
            }
            else if(data.transmision!=1 && data.transmision.split("-").length>1 && data.transmision.split("-")[1].substr(0,6).toLowerCase()=='cierre' ){
                officetrackColor='btn-primary';
            }

            if(officetrackColor!='btn-default'){
                officetrackbotton='data-toggle="modal" data-target="#officetrackModal" data-id="'+data.id.split("_")[1]+'"';
            }
            officetrackImage=' &nbsp; <a class="btn '+officetrackColor+' btn-sm" '+officetrackbotton+'><i class="fa fa-mobile fa-3x"></i> </a>';
        }
        html+=
            '<td><a class="btn bg-navy btn-sm" data-toggle="modal" data-target="#bandejaModal" data-codactu="'+data.codactu+'"><i class="fa fa-desktop fa-lg"></i> </a>'+
            officetrackImage+
        '</td>';
        html+="</tr>";

	});
	$("#tb_bandeja").html(html);
    activarTabla();
    $('#t_bandeja').attr("style","width:''");
};

activarTabla=function(){
    $("#t_bandeja").dataTable(); // inicializo el datatable
};
validaBandeja=function(){
	$('#form_roles [data-toggle="tooltip"]').css("display","none");
	var a=[];
	a[0]=valida("txt","descripcion","");
	var rpta=true;
	for(i=0;i<a.length;i++){
		if(a[i]===false){
			rpta=false;
			break;
		}
	}
	return rpta;
};
valida=function(inicial,id,v_default){
	var texto="Seleccione";
	if(inicial=="txt"){
		texto="Ingrese";
	}
	if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
		$('#error_'+id).attr('data-original-title',texto+' '+id);
		$('#error_'+id).css('display','');
		return false;
	}
};
envioMasivoOFSC=function(){
        r = confirm("<?php echo Lang::get('¿Seguro que desea enviar un Masivo?', array('accion' => 'Continuar')) ?>");
        if (r == true)
            OFSC.EnvioMasivoOfsc();
};
activarEnvioMasivo = function(button) {
    
        flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");

        if (flagActivoMasivo === null) {
                     localStorage.setItem("bandejaMasivoActivo", 1);
                    button.removeClass("btn-success");
                    button.addClass("btn-danger");
                    button.html("Desactivar Envio de Masivos");
                    $(".btn-ejecutar-masivo").css("display", "inline-block");
                    $("span.cont_checkbox").css("display", "inline-block");
                    $("span.checkbox_fila").css("display", "inline-block");
                    $(".contenedor-izquierdo").css("display", "block");
                    $("span.txt_id").css("margin-left", 10);
                    $("table#t_bandeja thead th:eq(0)").css("display", "block");
                    $("table#t_bandeja tfoot th:eq(0)").css("display", "block");
                    $("table#t_bandeja tbody tr").each(function(e){
                        $(this).find("td:eq(0)").css("padding", 16);
                        $(this).find("td:eq(0)").css("display", "block");
                    });
        } else {
            if (flagActivoMasivo === '0') {
                localStorage.setItem("bandejaMasivoActivo", 1);
                button.removeClass("btn-success");
                button.addClass("btn-danger");
                button.html("Desactivar Envio de Masivos");
                $(".btn-ejecutar-masivo").css("display", "inline-block");
                $("span.cont_checkbox").css("display", "inline-block");
                $("span.checkbox_fila").css("display", "inline-block");
                $(".contenedor-izquierdo").css("display", "block");
                $("span.txt_id").css("margin-left", 10);
                $("table#t_bandeja thead th:eq(0)").css("display", "block");
                $("table#t_bandeja tfoot th:eq(0)").css("display", "block");
                $("table#t_bandeja tbody tr").each(function(e){
                    $(this).find("td:eq(0)").css("padding", 16);
                    $(this).find("td:eq(0)").css("display", "block");
                });
                 
            } else {
                 localStorage.setItem("bandejaMasivoActivo", 0);
                button.removeClass("btn-danger");
                button.addClass("btn-success");
                button.html("Activar Envio de Masivos");
                $(".btn-ejecutar-masivo").css("display", "none");
                $("span.cont_checkbox").css("display", "none");
                $("span.checkbox_fila").css("display", "none");
                $(".contenedor-izquierdo").css("display", "none");
                $("span.txt_id").css("margin-left", 10);
                $("table#t_bandeja thead th:eq(0)").css("display", "none");
                $("table#t_bandeja tfoot th:eq(0)").css("display", "none");
                 $("table#t_bandeja tbody tr").each(function(e){
                    $(this).find("td:eq(0)").css("padding", 0);
                    $(this).find("td:eq(0)").css("display", "none");
                });
            }
        }
};

htmlCheckboxFilasMasivo = function(codactu){
    flagActivoMasivo = localStorage.getItem("bandejaMasivoActivo");
    bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
    bandejaBusqueda = JSON.parse(bandejaBusqueda);
    display = "";
    enable = "";
    checked = "";
    hidden = 0;
    if(flagActivoMasivo === null || flagActivoMasivo === '0') {
        display = "style='display: none' ";
        if(bandejaBusqueda === null){
                enable = "disable";
            }else{
                   if(typeof bandejaBusqueda[codactu]!=="undefined"){
                        enable = "enable";
                           if (bandejaBusqueda[codactu].seleccionado === true){
                                checked = "checked='checked' ";
                                hidden = 1;
                           } else {
                                 checked = "";
                                 hidden = 0;
                            }
                    } else {
                           enable = "disable";
                           checked ="";
                           hidden = 0;
                     }
              }
    } else {
          if(bandejaBusqueda === null){
              display = "style='display:inline-block;'";
                enable = "disable";
            }else{
                   if(typeof bandejaBusqueda[codactu]!=="undefined"){
                         display = "style='display:inline-block;'";
                        enable = "enable";
                           if (bandejaBusqueda[codactu].seleccionado === true){
                                checked = "checked='checked' ";
                                hidden = 1;
                           } else {
                                 checked = "";
                                 hidden = 0;
                            }
                    } else {
                          display = "style='display:inline-block;'";
                           enable = "disable";
                           checked ="";
                           hidden = 0;
                     }
              }
   }
   resultado = new Array(3);
   resultado[0] = display;
   resultado[1] = enable;
   resultado[2] = checked;
   resultado[3] = hidden;
   return resultado;
};
updateEstadoCheckboxMasivo  = function(button) {
    codactu = button.attr("codactu");
    bandejaBusqueda = localStorage.getItem("bandejaBusqueda");
    bandejaBusqueda = JSON.parse(bandejaBusqueda);
   
    if (button.is(":checked")) {
        bandejaBusqueda[codactu].seleccionado = true;
    } else {
        bandejaBusqueda[codactu].seleccionado = false;
    }
    bandejaBusqueda  = JSON.stringify(bandejaBusqueda);
    localStorage.setItem("bandejaBusqueda", bandejaBusqueda);
    pintarResultadosSeleccionados();
};
formBusquedatoArray = function (PG, isSoloActuaciones) {
    contador = 0;
    // if(PG=="P"){// Filtro Personalizado
               datos=$("#ffiltro_envios_masivo_ofsc").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("&");
           /* }
            else if(PG=="G"){ // Filtro General
                  datos=$("#form_General").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
            }*/
            datos.push("isSoloActuaciones="+isSoloActuaciones);
            d =new Array(datos.length);
             for (var i = datos.length - 1; i >= 0; i--) {
                    if( datos[i].split("[]").length>1 ){
                              d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                contador++;
                      }
                       else{
                             d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                        }
                    }
            return d;
};
pintarResultadosSeleccionados = function(){
    bandejaBusqueda =  localStorage.getItem("bandejaBusqueda");
    bandejaBusqueda = JSON.parse(bandejaBusqueda);
    dataset = [];
    for (var i in bandejaBusqueda) {
        if (bandejaBusqueda[i].seleccionado=== true)
            bandejaBusqueda[i].seleccionado = "Seleccionado";
        else
            bandejaBusqueda[i].seleccionado = "No Seleccionado";
        dataset.push([bandejaBusqueda[i].codactu, bandejaBusqueda[i].seleccionado]);
    }
    $('#resultados-seleccionados').dataTable().fnDestroy();
    if(dataset.length >0) {
        $("#collapseThree").css("margin-top", -390);
         $('#resultados-seleccionados').DataTable( {
            data: dataset,
            columns: [
                { title: "CodActuación" },
                { title: "Seleccionado" }
            ],
            fixedHeader: true,
            "scrollY":        "120px",
            "scrollCollapse": true,
            "fnAdjustColumnSizing": true
        } );
    } else {
        $("#collapseThree").css("margin-top", -300);
    }
};

verDetalle = function(){

}
</script>