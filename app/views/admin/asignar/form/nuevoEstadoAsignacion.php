<div id="nuevaTipificacion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
        <form class="form_estadoAsignacion" name="form_estadoAsignacion" id="form_estadoAsignacion">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Nuevo Estado Asignación</h4>
      </div>
      <div class="modal-body">
         <div class="row">
             <div class="col-md-12">
                 <label>Nombre:</label>
                 <input type="text" name="txt_nombre" id="txt_nombre" class="form-control">
            </div>
            <div class="col-md-12">
                 <label>Estado Ofsc:</label>
                 <select class="form-control" id="slct_estadoofsc" name="slct_estado_ofsc_id">              
                 </select>
            </div>
            <div class="col-md-12">
                 <label>Estado Aseguramiento:</label>
                 <select class="form-control" id="slct_estadost" name="slct_estado_aseguramiento_id">              
                 </select>
            </div>
            <div class="col-md-12">
                 <label>Estado:</label>
                 <select class="form-control" id="slct_estado" name="slct_estado">
                  <option value="">.::Seleccione::.</option>
                  <option value="0">Inactivo</option>
                  <option value="1" selected>Activo</option>              
                 </select>
            </div>
        </div>                                                                             
      </div><!--/modal-body-->
      <div class="modal-footer" style="border-top:0px;margin-top: 10px">
        <span class="btn btn-primary btn-md btn_guardar" id="btn_guardar" onclick="guardar()">Guardar <i class="glyphicon glyphicon-upload"></i></span>
      </div>
    </div><!--/modal-content -->
  </form>
  </div>
</div><!--/modal-->
<!-- Modal -->


