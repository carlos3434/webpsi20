<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")

    { HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
@endpush
   
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
	<section class="content-header">
        <h1>
            Reasignar Solicitudes
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Asignar Gestores</a></li>
            <li class="active">Reasignar Solicitudes</li>
        </ol>
    </section>
    
    <section class="content">
		  <div class="row">		
			  <div class="col-xs-12">
			  	<div class="panel-group" id="accordion-filtros">
			  		<div class="panel panel-default">
		                <div class="panel-heading box box-primary">
		                    <h4 class="panel-title">
		                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
		                           href="#collapse3">Reasignacion</a>
		                    </h4>
		                </div>
		                <div id="collapse3" class="panel-collapse collapse in ">
		                    <div class="panel-body">
		                        <form name="form_reasignacion" id="form_reasignacion" method="POST" action="">
		                            <div class="col-xs-8">
		                                <div class="form-group ">
		                                    <div class="col-sm-12">
		                                        <div class="col-sm-2">
		                                            <label class="titulo">Reasignar a: </label>
		                                        </div>
		                                        <div class="col-sm-4 hidden">
		                                            <select class="form-control" name="slct_tiporea" id="slct_tiporea">
		                                                <option value="">.::Seleccione::.</option>
		                                                <option value="1">Persona(s)</option>
		                                                <option value="2">Bolsa</option>
		                                            </select>
		                                        </div>
		                                        <div class="col-sm-4 rea_usuario hidden">
		                                            <select class="form-control" name="slct_usuario[]" id="slct_usuario"
		                                                multiple>
		                                            </select>
		                                        </div>
		                                        <div class="col-sm-2">
		                                          <span class="btn btn-primary btn-md" onclick="reasignacion()">Reasignar <i class="glyphicon glyphicon-random"></i></span> 
		                                          <!---<button id="btn_personalizado" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>-->
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </form>
		                    </div>
		                </div>
			    	</div>
			    </div>	

			  </div>
	      </div>
	</section>      


@stop
@section('formulario')
     @include( 'admin.mantenimiento.form.usuario' ) 
@stop

@push('scripts')
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    <script type="text/javascript" src="js/admin/gestiondevueltas/gestion.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="js/admin/gestiondevueltas/gestion_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
    
@endpush('script')