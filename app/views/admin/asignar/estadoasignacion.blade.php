<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/box-slider/jquery.bxslider.min.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
    {{ HTML::style('lib/fancybox-master/dist/jquery.fancybox.min.css') }}
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')


<style type="text/css">
 .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 8px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }
</style>
            <!-- Content Header (Page header) -->
                <section class="content-header">
                      <h1>
                         Estados Asignacion 
                          <small> </small> <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#nuevaTipificacion">Nuevo Registro <i class="glyphicon glyphicon-plus"></i></span>  
                      </h1>
                      <ol class="breadcrumb">
                          <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                          <li><a href="#">Reporte</a></li>
                          <li class="active">Reporte de Movimiento</li>
                      </ol>
                    <form id="formEstadoAsignacion">                      
                    </form>
                </section>

                <!-- Main content -->
<section class="content">
    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                            <table id="tb_tipificacion" class="table table-bordered table-hover">
                              <thead>
                                   <tr>
                                      <th style="width: 2%">N°Estado</th>
                                      <th style="width: 30%">nombre</th>
                                      <th style="width: 10%">estado_ofsc</th>
                                      <th style="width: 10%">estado_st</th>
                                      <th style="width: 10%">Estado</th>
                                      <th style="width: 10%">[]</th>
                                  </tr>
                              </thead>
                              <tbody id="tbody_tipificacion">

                              </tbody>
                          </table>
                        </div>                            
                    </div>
                    </div>

</section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.asignar.form.nuevoEstadoAsignacion' )
@stop

@push('scripts')
   {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
   {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
   {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('lib/bootstrap-datepicker.js') }}
      {{ HTML::script('lib/bootstrap-datepicker.js') }}

      {{ HTML::script('lib/fancybox-master/dist/jquery.fancybox.min.js') }}

<!-- bxSlider Javascript file -->
   {{ HTML::script('lib/box-slider/jquery.bxslider.min.js') }}
<!-- bxSlider CSS file -->
        {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/asignar/estadoasignacion_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/asignar/estadoasignacion.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')3p