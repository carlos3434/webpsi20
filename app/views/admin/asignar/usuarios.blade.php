<!DOCTYPE html>
@extends('layouts.masterv2')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
   
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
	<section class="content-header">
        <h1>
            Asignar Gestores Devueltas
            <small> </small>
            <span class="btn btn-success btn-sm btnAsignar">Asignar <i class="glyphicon glyphicon-check"></i></span>  
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Asignar Gestores</a></li>
            <li class="active">Usuarios devueltas</li>
        </ol>
    </section>
    
    <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="t_usuarios" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Apellidos</th>
                                                <th>Nombres</th>
                                                <th>Usuario</th>
                                                <th>Estado asignación</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_usuarios">
                                            
                                        </tbody>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <a class='btn btn-primary btn-sm  col-md-12' id="nuevo" data-toggle="modal" data-target="#usuarioModal" data-titulo="Nuevo"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                                        </div>

                                        <div class="col-md-2">
                                            <form name="form_exportar" id="form_exportar" method="post" action="gestordevueltas/listar" enctype="multipart/form-data" onclick="descargarReporte()">
                                                <a href="#" id="ExportExcel" class="btn btn-success btn-sm col-md-12">
                                                    <i class="fa fa-file-excel-o"></i> Exportar
                                                </a>
                                            </form>
                                            <form id="form_gestion">
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->

@stop
@section('formulario')
     @include( 'admin.mantenimiento.form.usuario' ) 
@stop

@push('scripts')
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    <script type="text/javascript" src="js/admin/gestiondevueltas/usuariosdev_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="js/admin/gestiondevueltas/usuariosdev.js?v={{ Cache::get('js_version_number') }}"></script>
    
@endpush('script')