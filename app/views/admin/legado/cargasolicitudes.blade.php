<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    {{-- {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' ) --}}
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Contrata Transferencia
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Contrata Transferencia</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <form enctype="multipart/form-data" name="formuploadajax" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            {{-- <button class="btn btn-md btn-primary" type="button" id="btnSubirExcel">Subir Archivo Excel</button> --}}
                            <input type="file" id="file_json" name="file_json" />
                            <h3>Contenido del archivo:</h3>
                            <pre id="contenido-archivo"></pre>

                            {{-- <input type="file" name="archivo" id="archivo"> --}}
                            <p>
                            <input type="submit" name="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>

    <script type="text/javascript">
        $(document).ready(function () {
            $("form[name='formuploadajax']").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: 'gestionesSt/cargarjson',
                    data: new FormData($(this)[0]),
                    processData: false,
                    contentType: false,
                    beforeSend : function() {
                        eventoCargaMostrar();
                    },
                    success: function (obj) {
                        eventoCargaRemover();
                        swal('Archivo Procesado');
                    }
                });
            })
        });  
    </script>
@stop