<!DOCTYPE html>
@extends("layouts.masterv3")

@push("stylesheets")
    {{ HTML::style('lib/timepicker/css/bootstrap-timepicker.css') }}
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">
    <!--<link rel="stylesheet" type="text/css" href="css/tabscroll.css?v={{ Cache::get('js_version_number') }}">-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Bandeja de Solicitudes Tecnicas 
        <small> </small>
    </h1>
    <ol class="breadcrumb">
      <li><a ><i class="fa fa-dashboard"></i>Admin</a></li>
      <li><a >Legados</a></li>
      <li class="active">Devoluciones</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-11 filtros">
        <form name="form_buscar" id="form_buscar" method="POST" action="">
          <input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
          <div class="panel-group" id="acordion_filtros">
            @if( Session::get("perfilId") != 13 && Session::get("perfilId") != 14 && Session::get("perfilId") != 15)
            <div class="panel panel-default">
              <div class="panel-heading box box-primary">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse2" id="panel_individual">Búsqueda Individual</a>
                </h4>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="personalizado">
                    <div class="box-body">
                      <div class="row">
                        <div class="form-group">
                          <div class="col-sm-6">
                            <label>Tipo de Busqueda</label>
                            <select class="form-control" name="slct_tipo" id="slct_tipo" simple="">
                              <option value="">.::Seleccione::.</option>
                              <option value="num_requerimiento">Requerimiento</option>
                              <option value="id_solicitud_tecnica">Solicitud Tecnica</option>
                              <option value="orden_trabajo">Orden de Trabajo</option>
                              <option value="peticion">Peticion</option>
                            </select>
                          </div>
                          <div class="col-sm-6">
                            <label>Valor</label>
                            <input type="text" class="form-control pull-right" name="txt_valor" id="txt_valor"/>
                          </div>

                          <!-- <div class="col-sm-1">
                              <div class="form-group">
                              <a class='btn btn-primary btn-sm' style="margin-top:25px" id="btn_buscar"><i class="fa fa-search fa-lg"></i>&nbsp;Buscar</a>
                              </div>
                          </div>-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif

            <div class="panel panel-default" id="div_personalizado">
              <div class="panel-heading box box-primary">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda Personalizada</a>
                </h4>
              </div>
              <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="personalizado">
                    <div class="box-body">
                      <div class="row">
                        @if( Session::get("perfilId") != 13 && Session::get("perfilId") != 14 && Session::get("perfilId") != 15)
                          <div class="col-sm-3">
                            <label>F.Registro</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                              <input  class="form-control pull-right"  type="text"  id="txt_created_at" name="txt_created_at" readonly="" />
                              <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()"><i class="fa fa-rotate-left"></i></div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <label>F. Agenda</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                              <input  class="form-control pull-right"  type="text"  id="txt_fecha_agenda" name="txt_fecha_agenda" readonly="" />
                              <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()"><i class="fa fa-rotate-left"></i></div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <label>Tipo de Operación</label>
                            <select class="form-control" name="slct_tipo_operacion[]" id="slct_tipo_operacion" multiple="">
                              <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Zonal</label>
                            <select class="form-control" name="slct_zonal[]" id="slct_zonal" multiple="">
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Nodo</label>
                            <select class="form-control" name="slct_cod_nodo[]" id="slct_cod_nodo" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Troba</label>
                            <select class="form-control" name="slct_cod_troba[]" id="slct_cod_troba" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>MDF</label>
                            <select class="form-control" name="slct_cabecera_mdf[]" id="slct_cabecera_mdf" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Estado TOA</label>
                            <select class="form-control" name="slct_estado_ofsc[]" id="slct_estado_ofsc" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Estado LEGO</label>
                            <select class="form-control" name="slct_estado_st[]" id="slct_estado_st" multiple="" >
                                <option value="0">Error Trama</option>
                                <option value="1">OK</option>
                                <option value="2">Trat. Legados</option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Situacion</label>
                            <select class="form-control" name="slct_estado_aseguramiento[]" id="slct_estado_aseguramiento" multiple="" >
                                <<option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3" id="div_tipo_devolucion_filtro" style="display: none;">
                            <label>Tipo de Devolucion</label>
                            <select class="form-control" name="slct_tipo_devolucion_filtro[]" id="slct_tipo_devolucion_filtro" multiple="" >
                                <option value="C">Comercial</option>
                                <option value="T">Tecnico</option>
                                <!--<option value="S">Soporte de Campo</option>-->
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>T. Envio TOA</label>
                            <select class="form-control" name="slct_tipo_envio_ofsc[]" id="slct_tipo_envio_ofsc" multiple="" >
                                <option value="1">Agenda</option>
                                <option value="2">SLA</option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Quiebre</label>
                            <select class="form-control" name="slct_quiebre[]" id="slct_quiebre" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Actividad</label>
                            <select class="form-control" name="slct_actividad[]" id="slct_actividad" multiple="" >
                                <option value="1">Averia</option>
                                <option value="2">Provision</option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Tipo Legado</label>
                            <select class="form-control" name="slct_tipo_legado[]" id="slct_tipo_legado" multiple="" >
                                <option value="1">CMS</option>
                                <option value="2">Gestel</option>
                            </select>
                          </div>
                          <div class="col-sm-3">
                            <label>Actividad Tipo</label>
                            <select class="form-control" name="slct_actividad_tipo_id[]" id="slct_actividad_tipo_id" multiple="" >
                                <option value=""></option>
                            </select>
                          </div>
                        @else
                          <div class="col-sm-3">
                                <label>Situacion</label>
                                <select class="form-control" name="slct_estado_aseguramiento[]" id="slct_estado_aseguramiento" multiple="" >
                                    <option value="3">Pre Devuelto</option>
                                    <option value="5">Stop Cierre</option>
                                </select>
                          </div>
                          <div class="col-sm-3" id="div_tipo_devolucion_filtro" style="display: none;">
                                <label>Tipo de Devolucion</label>
                                <select class="form-control" name="slct_tipo_devolucion_filtro[]" id="slct_tipo_devolucion_filtro" multiple="" >
                                    <option value="C">Comercial</option>
                                    <option value="T">Tecnico</option>
                                    <!--<option value="S">Soporte de Campo</option>-->
                                </select>
                          </div>
                          <div class="col-sm-3">
                                <label>Tipo Legado</label>
                                <select class="form-control" name="slct_tipo_legado" id="slct_tipo_legado">
                                    <option value="">.::Seleccionar::.</option>
                                    <option value="1">CMS</option>
                                    <option value="2">Gestel</option>
                                </select>
                          </div>
                          <div class="col-sm-3 hide">
                                <label>Motivo devolución</label>
                                <select class="form-control" name="slct_motivo_devolucion_f[]" id="slct_motivo_devolucion_f" multiple="" >
                                  <option></option>
                                </select>
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-xs-1  botones">
        <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar"></i>
        <i class="fa fa-download fa-xs btn btn-success" id="btn-descargar" title="Descargar" style="display:none"></i>
      </div>
    </div>
    <!--tabla-->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="panel panel-default">
            <div class="clearfix"></div>
            <div class="box-body table-responsive">
              <table id="t_listados" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Solicitud</th>
                    <th>OT</th>
                    <th>Req.</th>
                    <th>F.Registro</th>
                    <th>Ultimo Cambio</th>
                    <th>F.Programacion</th>
                    <th>Actividad Tipo</th>
                    <th>Quiebre</th>
                    <th>FFTT</th>
                    <th>Situacion</th>
                    <th>TOA</th>
                    <th>Motivo OFSC</th>                                              
                    <th>[]</th>
                  </tr>
                </thead>
                <tbody id="tb_listado"></tbody>
                <tfoot>
                  <tr>
                    <th>Solicitud</th>
                    <th>OT</th>
                    <th>Req.</th>
                    <th>F.Registro</th>
                    <th>Ultimo Cambio</th>
                    <th>F.Programacion</th>
                    <th>Actividad Tipo</th>
                    <th>Quiebre</th>
                    <th>FFTT</th>
                    <th>Situacion</th>
                    <th>TOA</th>
                    <th>Motivo OFSC</th>                       
                    <th>[]</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div> 
        </div>
      </div>
    </div>  
  </section>  

@endsection

@section('formulario')
     @include( 'admin.legado.form.bandejalegado_modal' )
@endsection
@push('scripts')
  {{ HTML::script("js/fontawesome-markers.min.js") }}
  {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
  {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
  {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
  {{ HTML::script('lib/timepicker/js/bootstrap-timepicker.js') }}
  {{ HTML::script("js/fontawesome-markers.min.js") }}
  {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&libraries=places,geometry,drawing') }}
  {{ HTML::script("lib/markerclusterer/markerclusterer.js") }}
  {{ HTML::script("lib/markerclusterer/OverlappingMarkerSpiderfier-oms.js") }}
  {{ HTML::script("js/geo/markerwithlabel.js") }}
  {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
  {{ HTML::script("lib/validaCampos/validaCampos.js") }}

  @include( "admin.js.slct_global_ajax")
  <script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
  </script>
  <script type="text/javascript" src="js/app.js"></script>
@endpush('script')