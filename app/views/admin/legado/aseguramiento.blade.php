<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}
{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Web de aseguramiento
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Legado</a></li>
        <li class="active">Web de aseguramiento</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" id='aseguramiento'>

    <div class="row">
        <div class="col-xs-12">

            <div class="box box-solid">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">Carga y Descarga de requerimientos</h3>
                </div>
                <div class="box-body border-radius-none">
                    <div class="row">
                        <div class="col-sm-12 col-sm-5">
                            <form id="form_parcial_columna" name="form_parcial_columna"  role="form" enctype="multipart/form-data" class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                      <label class="control-label">Tipo Aseguramiento</label>
                                      <select class="form-control" v-model="idestadoaseguramiento">
                                        <option v-for="estados in estadoaseguramiento" v-bind:value="estados.id">@{{ estados.nombre }}</option>
                                      </select>
                                    </div>

<!-- 
                                    <div class="input-group">
                                        <input type="file" class="form-control" id="archivocolumna" name="archivocolumna" accept="text/plain">
                                    </div>-->
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <button type="button" @click.prevent="descargarArchivo" class="btn btn-primary">Descargar</button>
                                    </div>
                                </div>
                            </form>
                            <!-- <small style="color: #000;">(*) Subir archivos .CSV, con el formato que se muestra en la tabla inferior.</small><br>-->
                        </div>
                    </div>
                </div>
            </div><!-- /.box -->
        </div>
    </div>
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js') }}
    {{ HTML::script('admin/legado/aseguramiento.js?v='.Cache::get('js_version_number')) }}
   
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
</section><!-- /.content -->
@stop