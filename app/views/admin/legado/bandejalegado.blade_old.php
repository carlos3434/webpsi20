<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
{{--     {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }} --}}
    @include( 'admin.legado.js.bandejalegado_ajax' )
    @include( 'admin.legado.js.bandejalegado' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bandeja de Legados
                <small> </small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                <li><a href="#">Legados</a></li>
                <li class="active">Bandeja Legados</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Inicia contenido -->
                    <div class="box box-primary">

                        <div class="box-body table-responsive">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary" id="btn_cierre">Enviar Cierre</button>
                                    </div>
                                </div>
                            </div>

                            <table id="t_bandejalegado" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>[  *  ]</th>
                                        <th>Codactu</th>
                                        <th>Solicitud Tecnica</th>
                                        <th>Tipo Operacion</th>
                                        <th>Contrata</th>
                                        <th>Fecha Envio</th>
                                        <th>N° Envios</th>
                                        <th class="col editarG"> [ ] </th>
                                    </tr>
                                </thead>
                                <tbody id="tb_bandejalegado">
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>[  *  ]</th>
                                        <th>Codactu</th>
                                        <th>Solicitud Tecnica</th>
                                        <th>Tipo Operacion</th>
                                        <th>Contrata</th>
                                        <th>Fecha Envio</th>
                                        <th>N° Envios</th>
                                        <th class="col editarG"> [ ] </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- Finaliza contenido -->
                </div>
            </div>

        </section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.legado.form.bandejalegado' ) 
@stop