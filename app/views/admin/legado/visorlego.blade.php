<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

<script src="https://unpkg.com/highcharts/highcharts.js"></script>
<script src="https://unpkg.com/vue-highcharts/dist/vue-highcharts.min.js"></script>

@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Visor Legados
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Legados</a></li>
            <li class="active">Visor Legados</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="row">
            <div class="col-sm-8">
                <label>F. Registro</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input class="form-control pull-right" id="rangoFechas" type="text" readonly/>
                    <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()">
                        <i class="fa fa-rotate-left"></i>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <label>&nbsp;</label>
                <button type="button" class="btn btn-primary form-control" data-dismiss="modal" @click="listar">Buscar</button>
            </div>

            <div class="col-sm-12 highcharts" style="display: none">
                <highcharts :options="options"></highcharts>
            </div>
            <div class="col-sm-12">
                <br>
                <div class="box-body table-responsive">
                    <table id="t_visor" class="table table-bordered table-striped col-sm-12 responsive text-center" width="100%">
                        <thead id="visorHeadHTML"></thead>
                        <tbody id="visorHTML"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#rangoFechas').daterangepicker({
                singleDatePicker: false,
                maxViewMode: 2,
                showDropdowns: true,
                minDate: moment().subtract(30, 'days'),
                maxDate:  moment(),
                startDate: moment().subtract(15, 'days'),
                endDate: moment(),
                autoApply: true,
                format: 'YYYY-MM-DD'
            });

            
        })
    </script>

    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}

    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{ HTML::script('https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js') }}
    {{ HTML::script('js/admin/legado/visorlego.js') }}
@stop