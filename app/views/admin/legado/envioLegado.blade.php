<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
 .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 8px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }
</style>
              <section class="content-header">
                  <h1>
                      Envio Legado
                      <small> </small>
                  </h1>
              </section>
              <br>
              <section>
               <div class="row">
                <div class="col-xs-10 filtros">
                  <form name="form_buscar" id="form_buscar" method="POST" action="">
                    <div class="panel-group" id="acordion_filtros">
                      <div class="panel panel-default">
                         <div class="panel-heading box box-primary">
                            <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda Personalizada</a>
                           </h4>
                         </div>
                         <div id="collapse1" class="panel-collapse collapse in">
                          <div class="panel-body">
                           <div class="personalizado">
                            <div class="box-body">
                             <div class="row">  
                                <div class="col-sm-4">
                                       <label>Fecha de Registro</label>
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                          <input class="form-control pull-right" id="rangoFechas" type="text" readonly/>
                                          <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()">
                                          <i class="fa fa-rotate-left"></i>
                                      </div>
                                   </div>
                                  </div>
                                <div class="col-sm-4">
                                      <label>HOST</label>
                                      <select class="form-control" name="slct_EnvioLegado_Host[]" id="slct_EnvioLegado_Host" multiple="" >
                                          <option value="">.::Seleccione::.</option>
                                      </select>
                                </div>
                                <div class="col-sm-4">
                                      <label>Accion</label>
                                      <select class="form-control" name="slct_EnvioLegado_Accion[]" id="slct_EnvioLegado_Accion" multiple="" >
                                          <option value="">.::Seleccione::.</option>
                                      </select>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                
              </div>
              </form>              
              </div> 

              <div class="col-xs-2  botones">
                <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar" onclick="filtrar()"></i>
                
              </div>
  </div>
                </section>
    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                            <table id="tb_legado" class="table table-bordered table-hover">
                              <thead>
                                   <tr>
                                      <th style="width: 2%">Usuario</th>
                                      <th style="width: 8%">Accion</th>
                                      <th style="width: 8%">Host</th>
                                      <th style="width: 8%">Fecha Creacion</th>
                                      <th style="width: 3%">[]</th>
                                  </tr>
                              </thead>
                              <tbody id="tbody_legado">
                                   
                              </tbody>
                          </table>
                        </div>                            
                    </div>
                    </div>

</section><!-- /.content -->
<!--Modal-->
<div class="modal fade" id="envioLegado" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width: 177%;margin-left: -188px;">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">Listado de Envio Legado</h4>
      </div>
      <div class="modal-body row form-group">
        <div class="col-sm-6">
        <div id="resultadoOfsc" style="word-wrap: break-word;padding: 5px;background-color: #D0FED9;border:1px solid #000;" style="width:500px">
        <strong>Request:</strong><br/>
        <pre>
            <div id="request">
              
            </div>
        </pre>
        </div>
    </div>
    <div class="col-sm-6">
        <div id="resultadoOfsc2" style="word-wrap: break-word;padding: 5px;background-color: #D0FED9;border:1px solid #000;" style="width:500px">
        <strong>Response:</strong><br/>
        <pre>
            <div id="response">
              
            </div>
        </pre>
        </div>
    </div>
      </div>
      <div class="modal-footer">
     </div>
    </div>
  </div>
</div>
<!--Modal-->
<!-- bxSlider CSS file -->
        {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    

  <script type="text/javascript" src="js/admin/legado/envioLegado_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/legado/envioLegado.js?v={{ Cache::get('js_version_number') }}"></script>

  <script type="text/javascript" src="js/admin/legado/solicitud.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')