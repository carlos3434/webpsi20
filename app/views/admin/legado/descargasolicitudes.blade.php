<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style('lib/timepicker/css/bootstrap-timepicker.css') }}
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Contrata Transferencia
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Contrata Transferencia</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <form name="form_descarga" id="form_descarga" method="post" action="" target="_blank">
                    <div class="row">
                        <div class="col-md-4">
                            <label>F.Registro</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input  class="form-control pull-right"  type="text"  id="created_at" name="created_at" readonly="" />
                                <div class="input-group-addon" style="cursor: pointer">
                                    <i class="fa fa-rotate-left"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Bucket</label>
                            <select class="form-control slct_bucket" name="buckes[]" multiple=""></select>
                        </div>
                        <div class="col-md-4">
                            <label>Estados OFSC</label>
                            <select class="form-control slct_estados_ofsc" name="estados_ofsc[]" multiple=""></select>
                        </div>
                        <div class="col-md-4">
                            <label>Estado LEGO</label>
                            <select class="form-control" name="slct_estado_st[]" id="slct_estado_st" multiple="">
                                <option value="0">Error Trama</option>
                                <option value="1">OK</option>
                                <option value="2">Trat. Legados</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Actividad</label>
                            <select class="form-control" name="slct_actividad[]" id="slct_actividad" multiple="">
                                <option value="1">Averia</option>
                                <option value="2">Provision</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Actividad Tipo</label>
                            <select class="form-control slct_actividad_tipo_id" name="slct_actividad_tipo_id[]" multiple="">
                                <option value="">.::Seleccione::.</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-4">
                        <label>&nbsp;</label>
                        <button class="form-control fa fa-file fa-xs btn btn-danger" id="btn-descargar">
                            Descargar
                        </button>
                    </div>
                </div>
            </div>

            <div class="box-body" style="display: hidden">
                <form enctype="multipart/form-data" name="formuploadajax" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="file" id="imagen" name="imagen" />
                            <p>
                            <input type="submit" name="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>

    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script('lib/timepicker/js/bootstrap-timepicker.js') }}
    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}
    {{ HTML::script("js/admin/legado/descargasolicitudes_ajax.js") }}
    {{ HTML::script("js/admin/legado/descargasolicitudes.js") }}

    <script type="text/javascript">
        $(document).ready(function () {
            $("form[name='formuploadajax']").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: 'gestionesSt/cargarimagen',
                    data: new FormData($(this)[0]),
                    processData: false,
                    contentType: false,
                    beforeSend : function() {
                        eventoCargaMostrar();
                    },
                    success: function (obj) {
                        eventoCargaRemover();
                        swal('Archivo Procesado');
                    }
                });
            })
        });  
    </script>
@stop