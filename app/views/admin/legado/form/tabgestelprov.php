<form id="form_solicitud" name="form_solicitud" action="" class="form-horizontal" method="post">
    <div class="row">
        <div class="col-sm-12" style="background-color: #357ca5;text-align: center;">
            <label style="color: yellow; margin: 5px;">Cliente: {{solicitud.cliente}}-{{fullNameGestel}}</label>
        </div>
    </div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Requerimiento</legend>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Requerimiento</span>
                      <label class="form-control input-sm">{{solicitud.peticion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">ST</span>
                      <label class="form-control input-sm">{{solicitud.id_solicitud_tecnica}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Orden Servicio</span>
                      <label class="form-control input-sm">{{solicitud.numero_orden_servicio}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Solicitud</span>
                      <label class="form-control input-sm">{{solicitud.solicitud}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">F.Solic</span>
                      <label class="form-control input-sm">{{solicitud.fecha_solicitud}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">F.Prog</span>
                      <label class="form-control input-sm">{{solicitud.fecha_programacion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Contrata</span>
                      <label class="form-control input-sm">{{solicitud.contrata}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Ciudad</span>
                        <label class="form-control input-sm">{{solicitud.ciudad}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_ciudad}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Prom</span>
                        <label class="form-control input-sm">{{solicitud.promocion}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_promocion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tip. Serv.</span>
                      <label class="form-control input-sm">{{solicitud.s_desc_tipser}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Nro. Prog</span>
                      <label class="form-control input-sm">{{solicitud.numero_veces_reprog}}</label>
                    </div>
                </div>
            </div>
        </div>
        <hr style="margin: 10px;border-top-color: #000;width: 100%;">
        <div class="row">
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Cod. Serv.</span>
                    <label class="form-control input-sm">{{solicitud.cod_servicio}}</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Servicio</span>
                    <label class="form-control input-sm">{{solicitud.desc_servicio}}</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Inscripcion</span>
                    <label class="form-control input-sm">{{solicitud.inscripcion}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Estado</span>
                    <label class="form-control input-sm">{{solicitud.desc_estado_solicitud}}</label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon input-sm">OOSS</span>
                    <label class="form-control input-sm" style="color: red;">TEST</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Cte-Cta-Pc</span>
                        <label class="form-control input-sm">{{solicitud.cliente}}</label>
                        <label class="form-control input-sm">{{solicitud.cuenta}}</label>
                        <label class="form-control input-sm">{{solicitud.pc}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Pet-Agr</span>
                        <label class="form-control input-sm">{{solicitud.peticion}}</label>
                        <label class="form-control input-sm">{{solicitud.agrupacion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Paquete</span>
                    <label class="form-control input-sm">{{solicitud.tipo_paquete}}</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Trabajar Via:</span>
                    <label class="form-control input-sm" style="color: red;">Test</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Escenario</span>
                    <label class="form-control input-sm">{{solicitud.escenario_gestel}}</label>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Cliente</span>
                    <label class="form-control input-sm">{{fullNameGestel}}</label>
                </div>
            </div>
            <div class="col-sm-3" style="padding-bottom: 10px;">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Tlf.</span>
                    <label class="form-control input-sm">{{solicitud.telefono_contacto}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Tlf. Refer.</span>
                    <label class="form-control input-sm">{{solicitud.telefono_referencia}}</label>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">ISP</span>
                        <label class="form-control input-sm">{{solicitud.cod_isp}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_isp}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-bottom: 10px;">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Clase</span>
                    <label class="form-control input-sm">{{solicitud.desc_clase}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Direccion</span>
                    <label class="form-control input-sm">{{fullDireccionprov1}}</label>
                </div>
            </div>
            <div class="col-sm-4" style="padding-bottom: 10px;">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Distrito</span>
                    <label class="form-control input-sm">{{distrito}}</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Direccion 2</span>
                    <label class="form-control input-sm">{{fullDireccionprov2}}</label>
                </div>
            </div>
            <div class="col-sm-4" style="padding-bottom: 10px;">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Segmento</span>
                        <label class="form-control input-sm">{{solicitud.segmento_cliente}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_segmento}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Sub Segmento</span>
                        <label class="form-control input-sm">{{solicitud.subsegmento_cliente}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_subsegmento_cliente}}</label>
                    </div>
                </div>
            </div>
             <div class="col-sm-6" style="padding-bottom: 10px;">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Observac.</span>
                    <label class="form-control input-sm">{{solicitud.desobs}}</label>
                </div>
            </div>
        </div>
        
    </fieldset>
    <br>
    <div class="control">
        <ul class="nav nav-pills nav-justified">
          <li class="active"><a data-toggle="tab" href="#datosprov" @click="Setvalores()">Datos</a></li>
          <li><a data-toggle="tab" href="#validacion_coordprov">Validacion Coord</a></li>
          <li><a data-toggle="tab" href="#bitacorasprov">Bitacoras</a></li>
          <li><a data-toggle="tab" href="#decosprov">Oper Decos</a></li>
          <li><a data-toggle="tab" href="#cable_modemprov">Cable Modem</a></li>
          <li><a data-toggle="tab" href="#mapaprov">Mapa</a></li>
        </ul>

        <div class="tab-content">
            <div id="datosprov" class="tab-pane fade in active">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Productos Adquiridos</legend>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Cod. Serv. Adquirido</th>
                                            <th>Serv. Adquirido</th>
                                            <th>Cod. Mov. Serv. Adquirido</th>
                                            <th>Mov. Serv. Adquirido</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr v-for="producto in productos">
                                            <td>{{producto.serv_adquirido}} </td>
                                            <td>{{producto.desc_serv_adquirido}} </td>
                                            <td>{{producto.cod_mov_serv_adquirido}} </td>
                                            <td>{{producto.desc_mov_serv}} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Red de Voz</legend>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Cab</span>
                                <label class="form-control input-sm">{{fftt.cabecera_conmutacion}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Telefono</span>
                                <label class="form-control input-sm">{{fftt.telefono}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Ubilic</span>
                                <label class="form-control input-sm">{{pin.ubilic}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Mdf</span>
                                <label class="form-control input-sm">{{cobre.cabecera_mdf}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Cabl</span>
                                <label class="form-control input-sm">{{cobre.cable}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">ParAli</span>
                                <label class="form-control input-sm">{{cobre.par_alimentador}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Arma.</span>
                                <label class="form-control input-sm">{{cobre.cod_armario}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Bneali</span>
                                <label class="form-control input-sm">{{cobre.borne_alimentador}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Sect</span>
                                <label class="form-control input-sm">{{cobre.sector}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Pardis</span>
                                <label class="form-control input-sm">{{cobre.par_dist_directo}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Caja</span>
                                <label class="form-control input-sm">{{cobre.caja}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Bnedis</span>
                                <label class="form-control input-sm">{{cobre.borne_dist_directo}}</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Dir. Armario</span>
                                <label class="form-control input-sm">{{cobre.direcion_armario}}</label>
                            </div>
                        </div>
                        <div class="col-sm-6" style="padding-bottom: 10px;">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Dir. MDF</span>
                                <label class="form-control input-sm">{{cobre.direccion_mdf}}</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Tipo Linea</span>
                                <label class="form-control input-sm">{{fftt.tipo_linea}}</label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Adsl</legend>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Par</span>
                                <label class="form-control input-sm">{{adsl.par_xdsl}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Armario</span>
                                <label class="form-control input-sm">{{adsl.armario_adsl}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Shelf</span>
                                <label class="form-control input-sm">{{adsl.shelf_cg}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Slot</span>
                                <label class="form-control input-sm">{{adsl.slot_cg}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Cab xdsl</span>
                                <label class="form-control input-sm">{{adsl.cabecera_xdsl}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">VPI/VCI</span>
                                <label class="form-control input-sm">{{solicitud.vpi_modem}}/{{solicitud.vci_modem}}</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">T.Tecno.</span>
                                <label class="form-control input-sm">{{fftt.tipo_tecnologia}}</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">T.DSLAM</span>
                                <label class="form-control input-sm">{{adsl.tipo_dslam}}</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon input-sm">Tecnologia</span>
                                <label class="form-control input-sm">{{solicitud.tecnologia}}</label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <!--<fieldset class="scheduler-border">
                    <legend class="scheduler-border">Datos de Dominio</legend>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <td>cod. ip</td>
                                            <td>mascara ip</td>
                                            <td>password</td>
                                            <td>usuario</td>
                                            <td>prefijo wan</td>
                                            <td>suf wan</td>
                                            <td>prefijo lan</td>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr v-for="dominio in dominios">
                                            <td>{{dominio.cod_ip}}</td>
                                            <td>{{dominio.mascara_ip}}</td>
                                            <td>{{dominio.password}}</td>
                                            <td>{{dominio.usuario}}</td>
                                            <td>{{dominio.prefijo_wan}}</td>
                                            <td>{{dominio.suf_wan}}</td>
                                            <td>{{dominio.prefijo_lan}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>-->
            </div>
            <div id="validacion_coordprov" class="tab-pane fade">
            </div>
            <div id="bitacorasprov" class="tab-pane fade">
            </div>
            <div id="decosprov" class="tab-pane fade">
            </div>
            <div id="cable_modemprov" class="tab-pane fade">
            </div>
            <div id="mapaprov" class="tab-pane fade">
                <fieldset class="scheduler-border">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-control" id="mapafftts">
                                
                            </div>
                        </div>
                    </div>
                </fieldset>

            </div>
        </div>
    </div>
</form>