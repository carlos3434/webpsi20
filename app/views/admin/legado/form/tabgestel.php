<form id="form_solicitud" name="form_solicitud" action="" class="form-horizontal" method="post">
    <div class="row">
        <div class="col-sm-12" style="background-color: #357ca5;text-align: center;">
            <label style="color: yellow; margin: 5px;">Cliente: {{solicitud.cod_cliente}}-{{fullNameGestel}}</label>
        </div>
    </div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Requerimiento</legend>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Requerimiento</span>
                      <label class="form-control input-sm">{{solicitud.cod_averia}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon input-sm">Telf.</span>
                    <label class="form-control input-sm">{{solicitud.telefono}}</label>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Inscripción</span>
                      <label class="form-control input-sm">{{solicitud.inscripcion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Ciudad</span>
                      <label class="form-control input-sm">{{solicitud.ciudad}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Reit</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">ST</span>
                      <label class="form-control input-sm">{{solicitud.id_solicitud_tecnica}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Cliente</span>
                      <label class="form-control input-sm">{{fullNameGestel}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Isp</span>
                        <label class="form-control input-sm">{{solicitud.cod_isp}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_isp}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Eps</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Dirección</span>
                        <label class="form-control input-sm">{{solicitud.direccion_completa}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_distrito}}</label>
                    </div>
                </div>
            </div>            
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Dpto.</span>
                      <label class="form-control input-sm">{{solicitud.desc_departamento}}</label>
                    </div>
                </div>
            </div>
             <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Zonal</span>
                      <label class="form-control input-sm" >{{solicitud.zonal}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Tipo Ser.</span>
                        <label class="form-control input-sm">{{solicitud.tipser}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_tipser}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Modalidad</span>
                        <label class="form-control input-sm">{{solicitud.cod_modalidad}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_modalidad}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Velocidad</span>
                      <label class="form-control input-sm" >{{solicitud.velocidad_speedy}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">F. Reporte</span>
                      <label class="form-control input-sm" >{{solicitud.fecha_registro}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tpo. Total</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">T. P. Rj.</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">TM</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Contrata</span>
                        <label class="form-control input-sm">{{solicitud.contrata}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_contrata}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Contacto</span>
                      <label class="form-control input-sm">{{solicitud.nombre_contacto}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tlf. Contacto</span>
                      <label class="form-control input-sm">{{solicitud.telefono_contacto}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Lfct</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Boleta</span>
                      <label class="form-control input-sm">{{solicitud.correlativo_llamada}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Fecha Liq.</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Lado</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Ind. Serv</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Conf US</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Obs. Liq.</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tlf. Ref.</span>
                      <label class="form-control input-sm">{{solicitud.telefono_referencia}}</label>
                    </div>
                </div>
            </div>        
        </div>
    </fieldset>
    <br>
    <div class="control">
        <ul class="nav nav-pills nav-justified" id='st-detalle'>
          <li class="active"><a data-toggle="tab" href="#datosave" @click="Setvalores()">Datos</a></li>
          <li><a data-toggle="tab" href="#validacion_coordave">Validacion Coord</a></li>
          <li><a data-toggle="tab" href="#bitacorasave">Bitacoras</a></li>
          <li><a data-toggle="tab" href="#decosave">Oper Decos</a></li>
          <li><a data-toggle="tab" href="#cable_modemave">Cable Modem</a></li>
          <li><a data-toggle="tab" href="#mapaave">Mapa</a></li>
        </ul>

        <div class="tab-content">
            <div id="datosave" class="tab-pane fade in active">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Productos Adquiridos</legend>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Cod. Serv. Adquirido</th>
                                            <th>Serv. Adquirido</th>
                                            <th>Cod. Mov. Serv. Adquirido</th>
                                            <th>Mov. Serv. Adquirido</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr v-for="producto in productos">
                                            <td>{{producto.serv_adquirido}} </td>
                                            <td>{{producto.desc_serv_adquirido}} </td>
                                            <td>{{producto.cod_mov_serv_adquirido}} </td>
                                            <td>{{producto.desc_mov_serv}} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Datos de Dominio</legend>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <td>cod. ip</td>
                                            <td>mascara ip</td>
                                            <td>password</td>
                                            <td>usuario</td>
                                            <td>prefijo wan</td>
                                            <td>suf wan</td>
                                            <td>prefijo lan</td>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr v-for="dominio in dominios">
                                            <td>{{dominio.cod_ip}}</td>
                                            <td>{{dominio.mascara_ip}}</td>
                                            <td>{{dominio.password}}</td>
                                            <td>{{dominio.usuario}}</td>
                                            <td>{{dominio.prefijo_wan}}</td>
                                            <td>{{dominio.suf_wan}}</td>
                                            <td>{{dominio.prefijo_lan}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div id="validacion_coordave" class="tab-pane fade">
            </div>
            <div id="bitacorasave" class="tab-pane fade">
            </div>
            <div id="decosave" class="tab-pane fade">
            </div>
            <div id="cable_modemave" class="tab-pane fade">

            </div>
            <div id="mapaave" class="tab-pane fade">
                <fieldset class="scheduler-border">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-control" id="mapafftts">
                                
                            </div>
                        </div>
                    </div>
                </fieldset>

            </div>
        </div>
    </div>
</form>