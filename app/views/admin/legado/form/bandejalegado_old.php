<!-- modal -->
<div class="modal fade" id="bandejalegadoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">

            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs logo modal-header">
                    <li class="logo tab_0 active">
                        <a href="#tab_0" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-cloud fa-sm"></i> </button>
                            <small>TRAMA</small>
                        </a>
                    </li>
                    <li class="logo tab_1">
                        <a href="#tab_1" data-toggle="tab">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-edit fa-sm"></i> </button>
                            <small>COMPONENTES</small>
                        </a>
                    </li>
                    <li class="pull-right">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <form id="form_bandeja_legado" name="form_bandeja_legado" action="#" method="post" class="container-fluid">
                            <div class="modal-body">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label>Solicitud Tecnica:</label>
                                            <input type="text" class="form-control input-sm" id="txt_id_solicitud_tecnica" name="txt_id_solicitud_tecnica" readonly>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Tipo Oeracion:</label>
                                            <input type="text" class="form-control input-sm" id="txt_tipo_operacion" name="txt_tipo_operacion" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Fecha Envio:</label>
                                            <input type="text" class="form-control input-sm" id="txt_fecha_envio" name="txt_fecha_envio" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Hora Envio:</label>
                                            <input type="text" class="form-control input-sm" id="txt_hora_envio" name="txt_hora_envio" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label>Cod Liquidacion:</label>
                                            <input type="text" class="form-control input-sm" id="txt_cod_liquidacion" name="txt_cod_liquidacion" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Cod Tecnico 1:</label>
                                            <input type="text" class="form-control input-sm" id="txt_cod_tecnico_liquida_uno" name="txt_cod_tecnico_liquida_uno" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Cod Tecnico 2:</label>
                                            <input type="text" class="form-control input-sm" id="txt_cod_tecnico_liquida_dos" name="txt_cod_tecnico_liquida_dos">
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Fecha Inicio:</label>
                                            <input type="text" class="form-control input-sm" id="txt_fecha_inicio" name="txt_fecha_inicio" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Hora Inicio:</label>
                                            <input type="text" class="form-control input-sm" id="txt_hora_inicio" name="txt_hora_inicio" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Fecha Fin:</label>
                                            <input type="text" class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Hora Fin:</label>
                                            <input type="text" class="form-control input-sm" name="txt_hora_fin" id="txt_hora_fin" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Nombre Contacto:</label>
                                            <input type="text" class="form-control input-sm" name="txt_nombre_contacto" id="txt_nombre_contacto" pattern="^([a-zA-Z _.,ñÑÁÉÍÓÚáéíóú]{1,100})" oninvalid="setCustomValidity('El formato debe ser texto')" onchange="try{setCustomValidity('')}catch(e){}" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Parentesco Contacto:</label>
                                            <input type="text" class="form-control input-sm" name="txt_parentesco_contacto" id="txt_parentesco_contacto">
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Dni Contacto:</label>
                                            <input type="text" class="form-control input-sm" name="txt_dni_contacto" id="txt_dni_contacto">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label>Cod Contrata:</label>
                                            <input type="text" class="form-control input-sm" name="txt_cod_contrata" id="txt_cod_contrata" required>
                                        </div>
                                        <div class="col-sm-9">
                                            <label>Observacion:</label>
                                            <textarea class="form-control input-sm" name="txt_observacion" id="txt_observacion" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Campo 1:</label>
                                            <input type="text" class="form-control input-sm" name="txt_valor1" id="txt_valor1">
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Campo 2:</label>
                                            <input type="text" class="form-control input-sm" name="txt_valor2" id="txt_valor2">
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Campo 3:</label>
                                            <input type="text" class="form-control input-sm" name="txt_valor3" id="txt_valor3">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Campo 4:</label>
                                            <input type="text" class="form-control input-sm" name="txt_valor4" id="txt_valor4">
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Campo 5:</label>
                                            <input type="text" class="form-control input-sm" name="txt_valor5" id="txt_valor5">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Actualizar">
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tab_1">
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <label>Solicitud Tecnica:</label>
                                    <input type="text" class="form-control input-sm" id="txt_id_solicitud_tecnica_componente" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <label>Tipo Operacion:</label>
                                    <input type="text" class="form-control input-sm" id="txt_tipo_operacion_componente" readonly>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br>
                                <table id="t_componentes" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>N° Req</th>
                                            <th>N° OT</th>
                                            <th>N° Servicio</th>
                                            <th>Cod Componente</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Tipo Componente</th>
                                            <th>Serie</th>
                                            <th>CasID</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_componentes">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn_close_modal" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div>
    </div>
</div>



<!-- <div class="modal fade" id="bandejalegadoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">New message</h4>
            </div>
            <form id="form_bandeja_legado" name="form_bandeja_legado" action="#" method="post" class="container-fluid">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <label>Solicitud Tecnica:</label>
                                <input type="text" class="form-control input-sm" id="txt_id_solicitud_tecnica" name="txt_id_solicitud_tecnica" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label>Tipo Oeracion:</label>
                                <input type="text" class="form-control input-sm" id="txt_tipo_operacion" name="txt_tipo_operacion" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Fecha Envio:</label>
                                <input type="text" class="form-control input-sm" id="txt_fecha_envio" name="txt_fecha_envio" pattern="^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})" oninvalid="setCustomValidity('El formato debe ser dd/mm/yyyy')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Hora Envio:</label>
                                <input type="text" class="form-control input-sm" id="txt_hora_envio" name="txt_hora_envio" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <label>Cod Liquidacion:</label>
                                <input type="text" class="form-control input-sm" id="txt_cod_liquidacion" name="txt_cod_liquidacion" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Cod Tecnico 1:</label>
                                <input type="text" class="form-control input-sm" id="txt_cod_tecnico_liquida_uno" name="txt_cod_tecnico_liquida_uno" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Fecha Inicio:</label>
                                <input type="text" class="form-control input-sm" id="txt_fecha_inicio" name="txt_fecha_inicio" pattern="^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})" oninvalid="setCustomValidity('El formato debe ser dd/mm/yyyy')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Hora Inicio:</label>
                                <input type="text" class="form-control input-sm" id="txt_hora_inicio" name="txt_hora_inicio" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <label>Fecha Fin:</label>
                                <input type="text" class="form-control input-sm" name="txt_fecha_fin" id="txt_fecha_fin" pattern="^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})" oninvalid="setCustomValidity('El formato debe ser dd/mm/yyyy')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Hora Fin:</label>
                                <input type="text" class="form-control input-sm" name="txt_hora_fin" id="txt_hora_fin" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Fecha Liquidada:</label>
                                <input type="text" class="form-control input-sm" name="txt_fecha_liquidada" id="txt_fecha_liquidada" pattern="^([0-9]{2}[\/]{1}[0-9]{2}[\/]{1}[0-9]{4})" oninvalid="setCustomValidity('El formato debe ser dd/mm/yyyy')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Hora Liquidada:</label>
                                <input type="text" class="form-control input-sm" name="txt_hora_liquidada" id="txt_hora_liquidada" pattern="^([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2})" oninvalid="setCustomValidity('El formato debe ser hh:mm:ss')" onchange="try{setCustomValidity('')}catch(e){}" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <label>Nombre Contacto:</label>
                                <input type="text" class="form-control input-sm" name="txt_nombre_contacto" id="txt_nombre_contacto" required>
                            </div>
                            <div class="col-sm-3">
                                <label>Parentesco Contacto:</label>
                                <input type="text" class="form-control input-sm" name="txt_parentesco_contacto" id="txt_parentesco_contacto">
                            </div>
                            <div class="col-sm-3">
                                <label>Dni Contacto:</label>
                                <input type="text" class="form-control input-sm" name="txt_dni_contacto" id="txt_dni_contacto">
                            </div>
                            <div class="col-sm-3">
                                <label>Cod Contrata:</label>
                                <input type="text" class="form-control input-sm" name="txt_cod_contrata" id="txt_cod_contrata" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3">
                                <label>Observacion:</label>
                                <input type="text" class="form-control input-sm" name="txt_observacion" id="txt_observacion" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Actualizar">
                </div>
            </form>
        </div>
    </div>
</div> -->
<!-- /.modal