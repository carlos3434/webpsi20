<form id="form_solicitud" name="form_solicitud" method="post">
    <div class="row">
        <div class="col-sm-12" style="background-color: #357ca5;text-align: center;">
            <label style="color: yellow; margin: 5px;">Cliente: {{solicitud.cod_cliente}}-{{fullName}}</label>

            <a class="btn btn-danger btn-xs btn-detalle" id="btnVerBtn" data-toggle="modal" title="Actualizar campos!"><i class="fa fa-edit fa-xs"></i></a>
        </div>
    </div>
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Requerimiento</legend>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">cliente</span>
                      <label class="form-control input-sm">{{solicitud.cod_cliente}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group" id="div_Nombres">
                    <span class="input-group-addon input-sm">Nombres</span>
                    <label class="form-control input-sm" id="lbl_nombres_cms">
                    {{solicitud.nom_cliente}}</label>
                </div>
                <div class="input-group" style="display: none" id="div_editNombres">
                    <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Nombres</span>
                    <input type="text" class="form-control input-sm"  v-bind:value="solicitud.nom_cliente" 
                    id="txt_nombres_cms"> 
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group" id="div_Paterno">
                    <span class="input-group-addon input-sm">Paterno</span>
                    <label class="form-control input-sm" id="lbl_paterno_cms">
                    {{solicitud.ape_paterno}}</label>
                </div>
                <div class="input-group" style="display: none" id="div_editPaterno">
                    <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Paterno</span>
                    <input type="text" class="form-control input-sm" 
                    v-bind:value="solicitud.ape_paterno" 
                    id="txt_paterno_cms">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group" id="div_Materno">
                    <span class="input-group-addon input-sm">Materno</span>
                    <label class="form-control input-sm" id="lbl_materno_cms">
                    {{solicitud.ape_materno}}</label>
                </div>
                <div class="input-group" style="display: none" id="div_editMaterno">
                    <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Materno</span>
                    <input type="text" class="form-control input-sm" 
                    v-bind:value="solicitud.ape_materno" id="txt_materno_cms">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Estacion</span>
                      <label class="form-control input-sm">TELEDIF</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group">
                    <span class="input-group-addon input-sm">
                        <input type="checkbox" v-model="solicitud.ind_vip" v-bind:true-value="S" v-bind:false-value="N">
                    </span>
                    <span class="form-control input-sm">VIP</span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Requerim</span>
                      <label class="form-control input-sm">{{solicitud.num_requerimiento}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Petic</span>
                      <label class="form-control input-sm">{{solicitud.peticion}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group" id="div_ZonaOT">
                      <span class="input-group-addon input-sm">Zona OT</span>
                      <label class="form-control input-sm" id="lbl_zonal_cms">
                      {{solicitud.zonal}}</label>
                    </div>
                    <div class="input-group" style="display: none;" id="div_editZonaOT">
                      <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Zona OT</span>
                      <select class="form-control input-sm" id="slct_zonal_cms">
                      </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tipo</span>
                      <label class="form-control input-sm">
                        {{solicitud.ind_origen_requerimiento}}
                      </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">ST</span>
                      <label class="form-control input-sm">{{solicitud.id_solicitud_tecnica}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">OT</span>
                      <label class="form-control input-sm">{{solicitud.orden_trabajo}}</label>
                    </div>
                </div>
            </div> </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Telefono VOIP</span>
                      <label class="form-control input-sm">{{solicitud.telefono_voip}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Promocion Linea</span>
                      <label class="form-control input-sm">{{solicitud.promocion_linea}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Desc Promocion Linea</span>
                      <label class="form-control input-sm">{{solicitud.desc_linea}}</label>
                    </div>
                </div>
            </div>
        </div>
        <hr style="margin: 10px;border-top-color: #000;width: 100%;">

        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Ind Paquete</span>
                      <label class="form-control input-sm">{{solicitud.tipo_paquete}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Esc Gestel CMS</span>
                        <label class="form-control input-sm">{{solicitud.escenario_cms}}</label>
                        <label class="form-control input-sm">{{solicitud.escenario_gestel}}</label>
                    </div>
                </div>
            </div>
        </div>

        <hr style="margin: 10px;border-top-color: #000;width: 100%;">
        
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline" id="div_Tiporeq">
                        <span class="form-control input-group-addon input-sm">Tipo Req</span>
                        <label class="form-control input-sm" id="lbl_tiporeq_cms">{{solicitud.tipo_requerimiento}}</label>
                        <label class="form-control input-sm" id="lbl_desctiporeq_cms">{{solicitud.desc_tipo_requerimiento}}</label>
                    </div>
                    <div class="input-group" style="display: none;" id="div_editTiporeq">
                      <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Tipo req</span>
                      <select class="form-control input-sm" id="slct_tiporeq_cms"></select>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Portabilidad</span>
                      <label class="form-control input-sm">{{solicitud.tipo_portado}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tipo Req ST</span>
                      <label class="form-control input-sm">{{solicitud.tipo_operacion}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline" id="div_MotivoGen">
                        <span class="form-control input-group-addon input-sm">Motivo Gen</span>
                        <label class="form-control input-sm" id="lbl_motivogen_cms">{{solicitud.cod_motivo_generacion}}</label>
                        <label class="form-control input-sm" id="lbl_descmotivogen_cms">{{solicitud.desc_motivo_generacion}}</label>
                    </div>
                    <div class="input-group" style="display: none;margin-top: 2px" id="div_editMotivoGen">
                      <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Motivo Gen</span>
                      <select class="form-control input-sm" id="slct_motivogen_cms"></select>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Situacion</span>
                        <label class="form-control input-sm">{{solicitud.situacion}}</label>
                        <label class="form-control input-sm">{{solicitud.desc_situacion}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="control-group">
                    <div class="form-inline">
                        <span class="form-control input-group-addon input-sm">Motivo Act</span>
                        <label class="form-control input-sm" id="lbl_motivoact_cms">{{solicitud.cod_motivo_generacion}}</label>
                        <label class="form-control input-sm" id="lbl_descmotivoact_cms">{{solicitud.desc_motivo_generacion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Estado</span>
                      <label class="form-control input-sm">{{solicitud.estado}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="control-group">
                    <div class="form-inline" id="div_Contrata">
                        <span class="form-control input-group-addon input-sm">Contrata</span>
                        <label class="form-control input-sm" id="lbl_codcontrata_cms">{{solicitud.codigo_contrata}}</label>
                        <label class="form-control input-sm" id="lbl_desccontrata_cms">{{solicitud.desc_contrata}}</label>
                    </div>
                    <div class="input-group" style="display: none;" id="div_editContrata">
                      <span class="input-group-addon input-sm" style="color:white;background:#36AE8E;">Contrata</span>
                      <select class="form-control input-sm" id="slct_contrata_cms"></select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">CMS</span>
                      <label class="form-control input-sm">{{fechaReg}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Envio</span>
                      <label class="form-control input-sm">{{fechaEnvio}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Tipo Tec</span>
                      <label class="form-control input-sm">{{solicitud.tipo_tecnologia}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Velocidad</span>
                      <label class="form-control input-sm">{{solicitud.velocidad_internet_requerimiento}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">N° Ave ind.</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Ofic. Adm</span>
                      <label class="form-control input-sm">{{solicitud.oficina}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">GO</span>
                      <label class="form-control input-sm">{{solicitud.created_at}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">TOA</span>
                      <label class="form-control input-sm">{{fechaEnvioToa}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">N° Ave mas.</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Observac</span>
                      <label class="form-control input-sm">{{solicitud.observacion}}</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">F. Liq</span>
                      <label class="form-control input-sm" style="color: red;">TEST</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="control-group">
                    <div class="input-group">
                      <span class="input-group-addon input-sm">Q</span>
                      <label class="form-control input-sm">{{solicitud.quiebre}}</label>
                    </div>
                </div>
            </div>

            <!-- jahir-->
            <div class="col-sm-4" id="div_FchInstalacion">
                <div class="control-group" >
                    <div class="input-group">
                      <span class="input-group-addon input-sm">F.Instalacion</span>
                      <label class="form-control input-sm" id="lbl_fchinstalacion_cms">{{fullFchInstalacion}}</label>
                    </div>
                </div>
            </div>

            <div class="col-sm-6" id="div_editFchInstalacion" style="display: none;">
                <div class="form-inline" >
                    <span class="form-control input-sm pull-left" 
                    style="color:white;background:#36AE8E;">F.Instalacion</span>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" v-bind:value="solicitud.fecha_instalacion_alta" id="txt_fchinstalacion_cms" readonly="">
                    </div>
                    <div class="col-sm-4 bootstrap-timepicker">
                        <input class="form-control input-sm timepicker" v-bind:value="solicitud.hora_instalacion_alta" id="txt_hrainstalacion_cms" readonly="">
                    </div>
                </div>
            </div>

            <div class="col-sm-12" style="margin-top:15px;">
                <div class="control-group">
                    <div class="input-group">
                      <button type="button" class="btn btn-success" id="btnActualizar" style="display: none;"><i class="fa fa-location-arrow fa-lg"></i> Modificar</button>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-inline ">
                <label>Clase de Servicio</label>
                <label class="form-control input-sm">{{solicitud.clase_servicio}}</label>
                <label class="form-control input-sm">{{solicitud.desc_clase_servicio}}</label>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-inline ">
                <label>Tipo de señal</label>
                <label class="form-control input-sm">{{solicitud.tipo_senal_troba}}</label>
            </div>
        </div>
    </div>
    <br>

    <div class="control">
        <ul class="nav nav-pills head nav-justified" id='st-detalle'>
          <li class="active"><a data-toggle="tab" href="#datos">Datos</a></li>
          <li><a data-toggle="tab" href="#validacion_coord">Validacion Coord</a></li>
          <li><a data-toggle="tab" href="#bitacoras">Bitacoras</a></li>
          <li><a data-toggle="tab" href="#decos">Oper Decos</a></li>
          <li><a data-toggle="tab" href="#cable_modem">Cable Modem</a></li>
          <li><a data-toggle="tab" href="#mapa">Mapa</a></li>
        </ul>

        <div class="tab-content">
            <div id="datos" class="tab-pane fade in active">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"></legend>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="control-group">
                                <div class="input-group">
                                  <span class="input-group-addon input-sm">Clave Srv</span>
                                  <label class="form-control input-sm">{{solicitud.desc_clase_servicio}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="control-group">
                                <div class="input-group">
                                  <span class="input-group-addon input-sm">Condicion Srv</span>
                                  <label class="form-control input-sm">{{solicitud.condicion}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="control-group">
                                <div class="input-group">
                                  <span class="input-group-addon input-sm">Categoria Srv</span>
                                  <label class="form-control input-sm">{{solicitud.desc_categoria_servicio}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Tecnicos</legend>
                    
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Nodo</span>
                                      <label class="form-control input-sm">{{solicitud.cod_nodo}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Plano</span>
                                      <label class="form-control input-sm">{{solicitud.cod_plano}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Trb/Tronc</span>
                                      <label class="form-control input-sm">{{solicitud.cod_troba}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Amplificador</span>
                                      <label class="form-control input-sm">{{solicitud.cod_amplificador}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                  
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Tap</span>
                                      <label class="form-control input-sm">{{solicitud.cod_tap}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Sector</span>
                                      <label class="form-control input-sm">{{solicitud.sector_troba}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Borne</span>
                                      <label class="form-control input-sm">{{solicitud.borne}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Split/Multisrv</span>
                                      <label class="form-control input-sm"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Jefatura</span>
                                      <label class="form-control input-sm">{{solicitud.cod_jefatura}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Cmts</span>
                                      <label class="form-control input-sm">{{solicitud.desc_cmts}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="control-group" id="div_Telefonos">
                                <div class="input-group">
                                  <span class="input-group-addon input-sm">Telefono(s)</span>
                                  <label class="form-control input-sm" id="lbl_telefonos_cms">{{telefonos}}</label>
                                </div>
                            </div>
                            <div class="form-inline" id="div_editTelefonos" style="display: none;">
                                <span class="form-control input-sm pull-left" 
                                style="color:white;background:#36AE8E;">Telefono(s)</span>
                                <input class="form-control input-sm pull-left" v-bind:value="solicitud.telefono1" id="txt_telefono1_cms">
                                <input class="form-control input-sm pull-left" v-bind:value="solicitud.telefono2" id="txt_telefono2_cms">
                            </div>
                        </div>
                    </div>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Direccion de instalacion</legend>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Direccion</span>
                                      <label class="form-control input-sm">{{direccionCliente}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Direccion Tap</span>
                                      <label class="form-control input-sm">{{direccionTab}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Urbanizacion</span>
                                      <label class="form-control input-sm">{{urbanizacion}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Distrito</span>
                                      <label class="form-control input-sm">{{solicitud.desc_distrito}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="control-group">
                                    <div class="input-group">
                                      <span class="input-group-addon input-sm">Referencia</span>
                                      <label class="form-control input-sm">{{solicitud.referencia}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="control-group">
                                <div class="input-group">
                                  <button type="button" class="btn btn-success" id="btnActualizar_dtos" style="display: none;"><i class="fa fa-location-arrow fa-lg"></i> Modificar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div id="validacion_coord" class="tab-pane fade">
                <fieldset class="scheduler-border">
                    <h3>Validacion de Coordenadas</h3>
                    <p>Distancia Maxima 1.5 Km</p>
                    <!--Config::get("validacion.distancia")-->
                    <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>F.Registro</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Distancia del tecnico</th>
                                <th>SMS</th>
                                <th>Estado</th>
                                <th>Carnet</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr v-for='index in detalle'>
                              <template v-if="index.tipo==1 || index.tipo==8">
                                <td>{{index.created_at}}</td>
                                <td>{{index.fecha_inicio}}</td>
                                <td>{{index.fecha_fin}}</td>
                                <td>{{index.distancia}} metros</td>
                                <td>{{index.sms}}</td>
                                <td>{{index.estado_prueba}}</td>
                                <td>{{index.tecnico_id}}</td>
                              </template>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </fieldset>
                <fieldset class="scheduler-border fieldset-forzar-validacion">
                    <h3>Forzar Validacion</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="btn btn-success btn-forzar-coordenadas">Forzar Validacion de Coordenadas</a>
                        </div>
                        <div class="col-sm-12">
                            <textarea style="resize: none; margin-top: 10px;" class="form-control" placeholder="Realiza un Comentario..." id='comentarios-forzar-coordenadas' row="3"></textarea>
                        </div>
                    </div>
                   
                </fieldset>
            </div>
            <div id="bitacoras" class="tab-pane fade">
                <fieldset class="scheduler-border">
                <h3>Bitacoras</h3>
                <p></p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Tipo</th>
                                <th>Tematico 1</th>
                                <th>Tematico 2 </th>
                                <th>Tematico 3</th>
                                <th>Tematico 4</th>
                                <th>Comentarios</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for='index in detalle'>
                            <template v-if="(index.tipo==3 || index.tipo==4 || index.tipo==5 || index.tipo==6)">
                                <td>{{index.created_at}}</td>
                                <td><template v-if="(index.tipo==3)">
                                    Tematicos Devolución 
                                </template>
                                <template v-if="(index.tipo==4)">
                                    Tematicos Devolución Tecnica
                                </template>
                                <template v-if="(index.tipo==5)">
                                    Tematicos de Agenda
                                </template>
                                <template v-if="(index.tipo==6)">
                                    Tematicos de Cierre
                                </template>
                                </td>
                                <td>{{index.campo1}}</td>
                                <td>{{index.campo2}}</td>
                                <td>{{index.campo3}}</td>
                                <td>{{index.campo4}}</td>
                                <td>{{index.comentarios}}</td>
                                <td>{{index.usuariodetalle}}</td>
                              </template>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </fieldset>
            </div>
            <div id="decos" class="tab-pane fade">
                <fieldset class="scheduler-border">
                <h3>Operaciones con Decos</h3>
                <template v-for="(nombreOperacion, operacion) in operaciones">
                    <div class='panel panel-info'>
                        <div class='panel-heading' data-toggle='collapse' data-parent='#accordion-comunicacion-legado-operacion' 
                            href='#collapse-comunicacion-operacion{{nombreOperacion}}' style='cursor: pointer'>
                            <h4 class='panel-title'>
                                <a data-toggle='collapse' data-parent='#accordion-comunicacion-legado-operacion' 
                                    href='#collapse-comunicacion-operacion{{nombreOperacion}}'>
                                    {{nombreOperacion}}
                                </a>
                            </h4>
                        </div>
                        <div id='collapse-comunicacion-operacion{{nombreOperacion}}' class='panel-collapse collapse' aria-expanded='true'>
                            <div class='panel-body'>
                                <ul class='timeline'>
                                    <template v-for="indice in operacion">
                                        <li class='time-label'>
                                            <span class='bg-red'>{{indice.created_at}}</span>
                                        </li>
                                        <li>
                                            <i class='fa fa-envelope bg-blue'></i>
                                            <div class='timeline-item'>
                                                <div class='timeline-body'>
                                                    <div class='row'>
                                                        <div class='col-sm-12 text-center'>
                                                            <label class='btn btn-primary col-sm-12 col-xs-12'>
                                                                {{indice.indicador}} - {{indice.estado_operacion}} - {{indice.observacion_respuesta}}
                                                            </label>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <label>Descripcion</label>
                                                            <input type='text' class='form-control input-sm' v-model='indice.descripcion' readonly>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <label>Serie</label>
                                                            <input type='text' class='form-control input-sm' v-model='indice.numser' readonly>
                                                        </div>
                                                        <div class='col-sm-4'>
                                                            <label>Material</label>
                                                            <input type='text' class='form-control input-sm' v-model='indice.codmat' readonly>
                                                        </div>
                                                        <template v-if="(nombreOperacion=='cambioAveria' || nombreOperacion=='reposicion' )">
                                                            <div class='col-sm-4'>
                                                                <label>SerieOld</label>
                                                                <input type='text' class='form-control input-sm' v-model='indice.numserold' readonly>
                                                            </div>
                                                            <div class='col-sm-4'>
                                                                <label>MaterialOld</label>
                                                                <input type='text' class='form-control input-sm' v-model='indice.codmatold' readonly>
                                                            </div>
                                                        </template>
                                                        <template v-if="(nombreOperacion!='revertirReposicion' &&  nombreOperacion!='desasignacionDecos' )">
                                                            <template v-if="(nombreOperacion!='refresh' )">
                                                                <div class='col-sm-4'>
                                                                    <label>SeriePar</label>
                                                                    <input type='text' class='form-control input-sm' v-model='indice.numserpar' readonly>
                                                                </div>
                                                                <div class='col-sm-4'>
                                                                    <label>MaterialPar</label>
                                                                    <input type='text' class='form-control input-sm' v-model='indice.codmatpar' readonly>
                                                                </div>
                                                            </template>
                                                            <div class='col-sm-12'>
                                                                <label>Observacion</label>
                                                                <textarea rows='1' v-model='indice.observacion' readonly class='form-control'></textarea>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <i class='fa fa-clock-o bg-gray'></i>
                                        </li>

                                    </template>
                                </ul>
                            </div>
                        </div>
                    </div>
                </template>
                </fieldset>
            </div>
            <div id="cable_modem" class="tab-pane fade">
                <fieldset class="scheduler-border">
                <h3>Pruebas de cable modem</h3>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Potencia de la Señal</th>

                                <th colspan="2" class="text-center">Relación Señal a Ruido</th>
                                <th colspan="3"></th>
                            </tr>
                            <tr>
                                <th class="text-center">Upstream</th>
                                <th class="text-center">Downstream</th>
                                <th class="text-center">Upstream</th>
                                <th class="text-center">Downstream</th>
                                <th colspan="3"></th>
                            </tr>
                            <tr>
                                <th class="text-center">37,0 a 55,0 dBmV</th>
                                <th class="text-center">-5,0 a 10,0 dBmV</th>
                                <th class="text-center">27,0 a 99 dB</th>
                                <th class="text-center">30,0 a 99 dB</th>

                                <th class="text-center">Mac</th>
                                <th class="text-center">Fecha</th>
                                <th class="text-center">Resultado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for='index in detalle'>
                                <template v-if="index.tipo==2 ">
                                    <td class="text-center">
                                        <template v-if="(index.campo1>=campo1_min && index.campo1<=campo1_max)">
                                            <h4><span class="label label-success">{{index.campo1}}  dBmV</span></h4>
                                        </template>
                                        <template v-else>
                                            <h4><span class="label label-danger">{{index.campo1}}  dBmV</span></h4>
                                        </template>
                                    </td>
                                    <td class="text-center">
                                        <template v-if="(index.campo2>=campo2_min && index.campo2<=campo2_max)">
                                            <h4><span class="label label-success">{{index.campo2}}  dBmV</span></h4>
                                        </template>
                                        <template v-else>
                                            <h4><span class="label label-danger">{{index.campo2}}  dBmV</span></h4>
                                        </template>
                                    </td>
                                    <td class="text-center">
                                        <template v-if="(index.campo3>=campo3_min && index.campo3<=campo3_max)">
                                            <h4><span class="label label-success">{{index.campo3}}  dB</span></h4>
                                        </template>
                                        <template v-else>
                                            <h4><span class="label label-danger">{{index.campo3}}  dB</span></h4>
                                        </template>
                                    </td>
                                    <td class="text-center">
                                        <template v-if="(index.campo4>=campo4_min && index.campo4<=campo4_max)">
                                            <h4><span class="label label-success">{{index.campo4}}  dB</span></h4>
                                        </template>
                                        <template v-else>
                                            <h4><span class="label label-danger">{{index.campo4}}  dB</span></h4>
                                        </template>
                                    </td>
                                    <td class="text-center"><h4>{{index.mac}}</h4></td>
                                    <td class="text-center"><h4>{{index.created_at}}</h4></td>
                                    <td class="text-center">
                                        <h4>{{EstadoPrueba(index.estado_prueba)}}</h4>
                                    </td>
                                </template>
                            </tr>
                        </tbody>
                    </table>
                </div>

                </fieldset>

            </div>
            <div id="mapa" class="tab-pane fade">
                <fieldset class="scheduler-border">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-control" id="mapafftts">
                                
                            </div>
                        </div>
                    </div>
                </fieldset>

            </div>
        </div>
    </div>
</form>