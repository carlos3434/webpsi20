<div class="row form-group">
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Requerimiento : {{solicitud.num_requerimiento}} | ST : {{solicitud.id_solicitud_tecnica}}</legend>
        <div class="col-sm-4">
            <label>Quiebre</label>
            <select class="form-control" name="slct_quiebre_modal" id="slct_quiebre_modal" simple ="" >
            </select>
        </div>
        <div class="col-sm-4">
            <label>Actividad Tipo</label>
            <select class="form-control" name="slct_actividad_tipo_modal" id="slct_actividad_tipo_modal" simple="" >
            </select>
        </div>
        <div class="col-sm-4">
            <label>Contrata</label>
            <div id="div_contrata_modal_cms">
                <select class="form-control" name="slct_contrata_modal" id="slct_contrata_modal_cms" simple="" >
                </select>
            </div>
            <div id="div_contrata_modal_gestel">
                <select class="form-control" name="slct_contrata_modal" id="slct_contrata_modal_gestel" simple="" >
                </select>
            </div>
        </div>
        <div class="col-sm-4" id="div_nodo_modal_cms">
            <label>Nodo</label>
            <div>
                <select class="form-control" name="slct_nodo_modal" id="slct_nodo_modal_cms" simple="" >
                </select>
            </div>
        </div>
        <div class="col-sm-4" id="div_troba_modal_cms">
            <label>troba</label>
            <div>
                <select class="form-control" name="slct_troba_modal" id="slct_troba_modal_cms" 
                simple="" >
                </select>
            </div>
        </div>
    </fieldset>
    
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">CMS</legend>
        <div class="col-sm-3">
            <div class="control-group">
                <div class="input-group">
                  <span class="input-group-addon input-sm">cliente</span>
                  <label class="form-control input-sm">{{solicitud.cod_cliente}}</label>
                </div>
            </div>
        </div>
    </fieldset>
</div>