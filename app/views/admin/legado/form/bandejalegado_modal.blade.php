
<!-- /.modal -->
<div class="modal fade" id="BandejaModal" tabindex="-1" role="dialog" aria-hidden="true" ref="BandejaModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
          <i class="fa fa-close"></i>
        </button>
        <div class="">
         <!--<div class="scroller scroller-left"><i class="glyphicon glyphicon-chevron-left"></i></div>
          <div class="scroller scroller-right"><i class="glyphicon glyphicon-chevron-right"></i></div>-->
          <div class="wrapper2">
            <ul class="nav nav-pills list">
              <li :class="[solicitud.tipo_legado == 1 ? 'tabs-popup active': 'tabs-popup']" v-if="solicitud.tipo_legado == 1">
                <a href="#tab-ordencms" data-toggle="tab">Orden CMS</a>
              </li>
              <li class="tabs-popup">
                <a href="#tab-toa" data-toggle="tab">TOA</a>
              </li>
              <li class="tabs-popup">
                <a href="#tab-go" data-toggle="tab"  v-on:click="consultarCapacidad()">Datos en GO</a>
              </li>
              <li :class="[solicitud.tipo_legado == 2 ? 'tabs-popup active': 'tabs-popup']" v-if="solicitud.tipo_legado == 2">
                <a :href="[solicitud.actividad_id == 1 ? '#tab-ordengestel-averia': '#tab-ordengestel-provision']" data-toggle="tab">Orden GESTEL</a>
              </li>
              <li v-on:click="obtenerMovimientos()" class="tabs-popup">
                <a data-toggle="tab" href="#tab-movimiento" v-on:click="consultarCapacidad()">Movimientos</a>
              </li>

              <!--<li v-if="btn_cierre_visibilidad" :class="[(solicitud.tipo_legado == 2 && solicitud.actividad_id == 2) ? 'active': '']">
                <a href="#tab-ordengestel-provision" data-toggle="tab">Orden Gestel p</a>
              </li>-->
              <li v-if="btn_cierre_visibilidad">
                <a href="#tab-cierre" data-toggle="tab"  v-if="btn_cierre_visibilidad">Cierre</a>
              </li>
              <li class="tabs-popup">
                <a href="#tab-regcambios" data-toggle="tab"  v-on:click="obtenerLogcambios()">Registro de Cambios</a>
              </li>
              <li class="tabs-popup">
                <a href="#tab-sms" data-toggle="tab">SMS</a>
              </li>
              <!--perfilid = 8 NO MOSTRAR-->
              <li class="tabs-popup">
                <a href="#tab-tramaenvio" data-toggle="tab" v-on:click="obtenerLogrecepcion()">Trama de Envio</a>
              </li>
              <!--perfilid = 8 NO MOSTRAR-->
              <li class="tabs-popup">
                <a href="#tab-comunicacionlegados" data-toggle="tab" v-on:click="obtenerComunicacionlego()">Comunicación Legados</a>
              </li>

            </ul>
          </div>
        </div>

      </div>
      <div class="modal-body">
        <div class="tab-content">
          <div id="tab-ordencms" :class="[solicitud.tipo_legado == 1 ? 'tab-pane fade-in active': 'tab-pane']">
            <cms-component :solicitud_data="solicitud" :lista_slcts="lista"></cms-component>
          </div>
          <div class="tab-pane fade-in" id="tab-toa">
            <toa-component :solicitud_data="solicitud"></toa-component>
          </div>
          <div class="tab-pane fade-in" id="tab-go">
            <go-component :solicitud_data="solicitud" :lista_slcts="lista"></go-component>
          </div>
          <div class="tab-pane fade-in" id="tab-movimiento">
            <movimiento-component :solicitud_data="solicitud" :movimientos="movimientos_st"></movimiento-component>
          </div>
          <div id="tab-ordengestel-averia" :class="[solicitud.tipo_legado == 2 ? 'tab-pane fade-in active': 'tab-pane']">
            gestel
          </div>
          <div class="tab-pane fade-in" id="tab-regcambios">
            <regcambios-component :logcambios="logcambios_st"></regcambios-component> 
          </div>
          <div class="tab-pane fade-in" id="tab-sms">
            <sms-component></sms-component>
          </div>
          <div class="tab-pane fade-in" id="tab-tramaenvio">
            <tramaenvio-component :logrecepcion="logrecepcion_st"></tramaenvio-component>
          </div>
          <div class="tab-pane fade-in" id="tab-comunicacionlegados">
            <comunicacionlegados-component :comunicacionlego="comunicacionlego_st"></comunicacionlegados-component>
          </div>

          <!--<div class="tab-pane" id="tabtoa">
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close_2">Close</button>
              <button type="button" class="btn btn-primary" id="btn_enviar_ofsc">Enviar</button>
            </div>
              
          </div>-->
          
          
    
        </div>
      </div>

    </div>
  </div>
</div>