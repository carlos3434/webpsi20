<style>
    .left {float: left;}
    .right {float: right;}

    .clearer {
        clear: both;
        display: block;
        font-size: 0;
        height: 0;
        line-height: 0;
    }

    /*
            Misc
    ------------------------------------------------------------------- */

    .hidden {display: none;}


    /*
            Example specifics
    ------------------------------------------------------------------- */

    /* Layout */

    #center-wrapper {
        margin: 0 auto;
        width: 920px;
    }


    /* Content & sidebar */

    #mymap,#sidebar {
        text-align: center;
        height: 350px;
    }

    #sidebar {
        text-align: left;
        display: none;
        overflow: auto;
    }
    #mymap {
        background-color: #EFE;
        border-color: #CDC;
        border: 1px solid powderblue;
        width: 100%;
    }
    .content {
        background-color: white;
    }

    .use-sidebar #mymap {width: 100%;}
    .use-sidebar #sidebar {
        display: block;
        width: 100%;
    }

    .sidebar-at-left #sidebar {margin-right: 1%;}
    .sidebar-at-right #sidebar {margin-left: 1%;}

    .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
    .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

    .color-text {
      color:#4C35BE;
      font-family: times, serif;
      font-style:italic;
    }

    #separator {
        background-color: #EEE;
        border: 1px solid #CCC;
        display: block;
        outline: none;
        width: 1%;
    }
    .use-sidebar #separator {
        background: url('img/separator/vertical-separator.gif') repeat-y center top;
        border-color: #FFF;
    }
    #separator:hover {
        border-color: #ABC;
        background: #DEF;
    }
    
</style>
<!-- /.modal -->
<div class="modal fade" id="BandejaModal" tabindex="-1" role="dialog" aria-hidden="true" >

  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
          <i class="fa fa-close"></i>
        </button>
        <div class="">
         <!--<div class="scroller scroller-left"><i class="glyphicon glyphicon-chevron-left"></i></div>
          <div class="scroller scroller-right"><i class="glyphicon glyphicon-chevron-right"></i></div>-->
          <select id="slct_data" name="slct_data[]" v-el:alo></select>
          <div class="wrapper2">
            <ul class="nav nav-pills list" id="listopciones">
            <button @click="hola()"  type="button">qweqw</button>
              <li id="litabtoa" class="active" data-opcionperfil="1"><a href="#tabtoa" data-toggle="tab">TOA</a>
              </li>beta
              <li class="tab-orden" tipo-tab="1" id="liopt-ordencms" v-show="@validarTabOrden"><a id="opt-ordencms" href="#tabsolicitudcms" data-toggle="tab">Orden Cms</a>
              </li>
              <li class="tab-orden" tipo-tab="2" id="liopt-ordengestel" v-show="@validarTabOrden"><a id="opt-ordengestel" href="#tabsolicitudgestel" data-toggle="tab">Orden Gestel</a>
              </li>
              <li class="tab-orden" tipo-tab="2" id="liopt-ordengestelprov" v-show="@validarTabOrden"><a id="opt-ordengestelprov" href="#tabsolicitudgestelprov" data-toggle="tab">Orden Gestel</a>
              </li>
              @if (Session::get("perfilId") == 8)
              <li class="tab-orden" id="liopt-datago"><a id="opt-datago" href="#tabdatago" data-toggle="tab">Datos en GO</a>
              </li>
              @endif
              <li id="liopt-componentes"><a href="#tab5" data-toggle="tab">Componentes</a>
              </li>
              @if (Session::get("perfilId") == 8)
              <li style="display: inline-block;"><a href="#tab7" data-toggle="tab">Comunicacion Legados</a>
              </li>
              @endif
              <li id="liopt-cambios"><a href="#tab6" data-toggle="tab">Registro de Cambios</a>
              </li>
              <li id="litabcierre" style="display: none"><a href="#tab8" data-toggle="tab">Cierre</a>
              </li>
              <li id="liopt-movimientos"><a href="#tab9" data-toggle="tab">Movimientos</a>
              </li>
              @if (Session::get("perfilId") == 8)
              <li style="display: inline-block;"><a href="#tab10" data-toggle="tab">Trama de Envio</a>
              </li>
              @endif
              <li><a href="#tabsms" data-toggle="tab">SMS</a>
              </li>
            </ul>
           </div>
        </div>

      </div>
      <div class="modal-body">
        <div class="tab-content">
          <div class="tab-pane fade in active" id="tabtoa">
            @include( 'admin.legado.form.tabtoa' )
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close_2">Close</button>
              <button type="button" class="btn btn-primary" id="btn_enviar_ofsc">Enviar</button>
            </div>
          </div>
          <div class="tab-pane fade" id="tabsolicitudcms">
            @include( 'admin.legado.form.tabcms' )
            <!-- 
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close">Close</button>
              <button type="button" class="btn btn-primary" :disabled='noGestion' id="btn_editar">Guardar</button>
            </div>-->
          </div>
          <div class="tab-pane fade" id="tabsolicitudgestel">
            @include( 'admin.legado.form.tabgestel' )
            <!-- 
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close">Close</button>
              <button type="button" class="btn btn-primary" :disabled='noGestion' id="btn_editar">Guardar</button>
            </div>-->
          </div>
          <div class="tab-pane fade" id="tabsolicitudgestelprov">
            @include( 'admin.legado.form.tabgestelprov' )
            <!-- 
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close">Close</button>
              <button type="button" class="btn btn-primary" :disabled='noGestion' id="btn_editar">Guardar</button>
            </div>-->
          </div>
          <div class="tab-pane fade" id="tabdatago">
            <form id="form_bandeja_datosgo" name="form_bandeja_toa" action="" method="post" style="height:400px;" class="container-fluid">
            @include( 'admin.legado.form.tabgo' )
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close">Close</button>
                <button type="button" class="btn btn-primary" :disabled='noGestion' id="btn_editar_datago">Guardar</button>
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="tab5">
            <div style="min-height: 400px">
              <div class="row form-group">
                <div class="col-sm-12">
                  <a class="btn btn-block btn-social btn-bitbucket" target="_blank" id="url_operaciones">
                    <i class="fa fa-external-link"></i>Realizar Operacion de Componentes
                  </a>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-sm-12">
                  <table id="t_componentes" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>N° Req</th>
                        <th>N° Servicio</th>
                        <th>Cod Material</th>
                        <th>Descripcion</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Serie</th>
                      </tr>
                    </thead>
                    <tbody id="tb_componentes">
                        <tr v-for="componente in componentes">
                          <td>@{{componente.created_at}}</td>
                          <td>@{{componente.nombre}}</td>
                          <td>@{{componente.numero_requerimiento}}</td>
                          <td>@{{componente.numero_servicio}}</td>
                          <td>@{{componente.componente_cod}}</td>
                          <td>@{{componente.descripcion}}</td>
                          <td>@{{componente.marca}}</td>
                          <td>@{{componente.modelo}}</td>
                          <td>@{{componente.numero_serie}}</td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_close">Close</button>
            </div>

          </div>
          <div class="tab-pane fade" id="tab7">
            <div class="row form-group">
              <div class="col-sm-12 contenedor-comunicacion-legado">
                <div class="box-body table-responsive">
                  <div class="panel-group" id="accordion-comunicacion-legado">
                    <template v-for="(i,lego) in comunicacionlego">
                      <div class='panel panel-info'>
                        <div class='panel-heading' data-toggle='collapse' data-parent='#accordion-comunicacion-legado' 
                        href='#collapse-comunicacion@{{i}}' style='cursor: pointer'>
                          <h4 class='panel-title'>
                          <a data-toggle='collapse' data-parent='#accordion-comunicacion-legado' 
                          href='#collapse-comunicacion@{{i}}' >@{{i}}</a>
                          </h4>
                        </div>
                        <div id='collapse-comunicacion@{{i}}' class='panel-collapse collapse' aria-expanded='true'>
                          <div class='panel-body'>
                            <template v-for="le in lego">
                              <fieldset>
                                <div class='fila-envio-legado box box-body' style='border-top: none; box-shadow: none;'>
                                  <legend>
                                    <div class='fila-cabecera'>
                                      <a v-if="(le.accion=='RptaEnvioSolicitudTecnica' || le.accion=='CierreSolicitudTecnica' || le.accion=='asignacionDecos')" class="btn btn-danger btn-xs btn-detalle pull-left" id="btnEditComLeg" data-toggle="modal" title="Actualizar campos!"
                                      @click='fn_btnEditComLeg(le.id,le.solicitud_tecnica_id)'><i class="fa fa-edit fa-xs"></i></a>
                                      <span style="margin-left: 5px;"><b>F. Envio : </b>@{{le.created_at}}</span>
                                    </div>
                                  </legend>
                                  <div class='row' v-bind:id="'show'+le.id">
                                    <div class='col-sm-6'>
                                      <label class=''>Request</label>
                                      <textarea disabled class='form-control' style='resize:none; height: 300px;'>@{{le.xml_request}}</textarea>
                                    </div>
                                    <div class='col-sm-6'>
                                      <label class=''>Response</label>
                                      <textarea disabled class='form-control' style='resize:none; height: 300px;'>@{{le['xml_response']}}</textarea>
                                    </div>
                                  </div>
                                  <div class="row" v-bind:id="'edit'+le.id" style="display: none">
                                    <div class="col-sm-12">
                                        <div class="use-sidebar sidebar-at-left col-sm-12" id="mymap">
                                          <div  id="sidebar">
                                            <form v-bind:name="'form_'+le.id" v-bind:id="'form_'+le.id" method="POST" action="">
                                            <input type="hidden" v-bind:id="'estado'+le.id" v-bind:name="'estado'+le.id" value="0">
                                              <div v-bind:id="le.id" class="row">
                                                
                                              </div>
                                              <div v-bind:id="'detalle'+le.id" class="row">
                                                
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                    </div>
                                  </div><br>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <template v-if="(le.accion=='RptaEnvioSolicitudTecnica' || le.accion=='CierreSolicitudTecnica' || le.accion=='CierreSTProvision' || le.accion=='CierreSTAverias' || le.accion=='asignacionDecos') ">
                                          <button class='btn btn-success pull-right col-sm-3' @click='reenvioOperacionDeco(le.id,le.solicitud_tecnica_id)'>
                                            Reenviar
                                          </button>
                                        </template>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                            </template>
                          </div>
                        </div>
                      </div>
                    </template>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="tab6">
            <div class="row form-group">
              <div class="col-sm-12">
                <div class="box-body table-responsive">
                  <table id="t_cambios" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Antes</th>
                        <th>Despues</th>
                      </tr>
                    </thead>
                    <tbody id="tb_cambios">
                      <tr v-for="cambio in logcambios">
                        <td>@{{cambio.id}}</td>
                        <td>@{{cambio.created_at}}</td>
                        <td>@{{cambio.usuario}}</td>
                        <td>{@{{cambio.htmlantes}}}</td>
                        <td>{@{{cambio.htmldespues}}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tab8" style="display: none">
            @include( 'admin.legado.form.tabcierre')
          </div>
          <div class="tab-pane fade" id="tab9">
            <div class="row form-group">
              <div class="col-sm-12">
                <div class="box-body table-responsive">
                  <table id="t_movimientos" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Situacion</th>
                        <th>Estado TOA</th>
                        <th>Motivo TOA</th>
                        <th>SubMotivo TOA</th>
                        <th>Tecnico</th>
                        <th>Fecha Programacion</th>
                        <th>Tipo Envio</th>
                        <th>Franja</th>
                        <th>Observacion</th>
                        <th>[.]</th>
                      </tr>
                    </thead>
                    <tbody id="tb_movimientos">
                      <template  v-for="movimiento in movimientos">
                        <tr>
                          <td>@{{movimiento.created_at}}</td>
                          <td>@{{movimiento.usuario}}</td>
                          <td>@{{movimiento.estado_aseguramiento_nombre}}</td>
                          <td>@{{movimiento.estado}}</td>
                          <td>@{{movimiento.motivo}}</td>
                          <td>@{{movimiento.submotivo}}</td>
                          <td>@{{movimiento.tecnico}}</td>
                          <td>@{{movimiento.fecha_agenda}}</td>
                          <td>@{{tipoEnvio(movimiento.tipo_envio_ofsc)}}</td>
                          <td>@{{movimiento.intervalo}}</td>
                          <td>@{{movimiento.observaciontoa}}<b v-if="movimiento.sistema_externo_id == 1" style="color: red"><br>(TOOLBOX)</b></td>
                          <td><button v-if="movimiento.cantdetalle>0" class="btn vd_btn" @click="displayDetalleMovimientos(movimiento.id)"><i class="fa fa-eye fa-fw"></i></button></td>
                        </tr>
                        <tr>
                          <template v-if="movimiento.detalle.length > 0">
                            <td colspan='13' :class="idClass(movimiento.id)" style="display: none">
                              <table class="table table-bordered table-striped">
                                <thead>
                                  <th colspan="9">Bitacora de Tematicos</th>
                                </thead>
                                <thead class="btn-success">
                                  <th>F. Registro</th>
                                  <th>Nombre</th>
                                  <th>Apellido</th>
                                  <th>@{{cabs.camp1}}</th>
                                  <th>@{{cabs.camp2}}</th>
                                  <th>@{{cabs.camp3}}</th>
                                  <th>@{{cabs.camp4}}</th>
                                  <th>Comentario</th>
                                  <th>Observacion</th>
                                </thead>
                                <tbody>
                                  <template v-for="deta in movimiento.detalle">
                                      <tr v-if="deta.tipo == 3 || deta.tipo == 4 || deta.tipo == 5 || deta.tipo == 6">
                                        <td>@{{deta.created_at}}</td>
                                        <td>@{{deta.nombre}}</td>
                                        <td>@{{deta.apellido}}</td>
                                        <td>@{{deta.campo1}}</td>
                                        <td>@{{deta.campo2}}</td>
                                        <td>@{{deta.campo3}}</td>
                                        <td>@{{deta.campo4}}</td>
                                        <td>@{{deta.comentarios}}</td>
                                        <td>@{{deta.observacion_form}}</td>
                                      </tr>
                                  </template>
                                </tbody>
                              </table>
                            </td>
                          </template>
                        </tr>
                      </template>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="tab10">
            <div class="row form-group">
              <div class="col-sm-12 contenedor-solicitud-recepcion">
                <div class="box-body table-responsive">
                  <template v-for="(key, recepcion) in logrecepcion">
                    <div class='panel panel-info'>
                      <template v-if="(recepcion.code_error)">
                        <div style='background: #d9534f;cursor: pointer'  class='panel-heading' data-toggle='collapse' data-parent='#accordion-solicitud-recepcion' href='#collapsem@{{key}}'>
                          <h4 class='panel-title'>
                            <a style='color: #fff;' data-toggle='collapse' data-parent='#accordion-solicitud-recepcion' href='#collapsem@{{key}}'>
                            @{{recepcion.created_at}}
                            </a>
                          </h4>
                        </div>
                      </template>
                      <template v-else>
                        <div class='panel-heading' data-toggle='collapse' data-parent='#accordion-solicitud-recepcion' href='#collapsem@{{key}}' style='cursor: pointer'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion-solicitud-recepcion' href='#collapsem@{{key}}'>
                            @{{recepcion.created_at}}
                            </a>
                          </h4>
                        </div>
                      </template>
                      <div id='collapsem@{{key}}' class='panel-collapse collapse' aria-expanded='true'>
                        <div class='panel-body'>
                          <div class='fila-recepcion-solicitud box box-body' style='border-top: none; box-shadow: none;'>
                            <label><b>Code Error : </b>@{{recepcion.code_error}}</label><br>
                            <label><b>Detalle Error : </b>@{{recepcion.detalle_error}}</label><br>

                            <div v-if="typeof recepcion.descripcion_error == 'string'">
                              <b>Mensaje Error :</b>
                              <div v-html="recepcion.descripcion_error"></div>
                            </div>
                            
                            <label v-if="typeof recepcion.descripcion_error == 'string'"><b>Mensaje Error :</b>@{{recepcion.descripcion_error}}</label>

                            <div v-if="typeof recepcion.descripcion_error == 'object' && recepcion.descripcion_error != null" class='row' style="max-height: 200px; overflow-y: scroll; background-color: rgba(0,0,0,0.04);">
                              <div class='col-sm-12'>
                                <label class=''>Mensaje Error :</label><br>
                                  <div style="margin-left: 5px;">

                                    Problema en: @{{recepcion.descripcion_error.fallo}}<br>
                                    Actividad: @{{recepcion.descripcion_error.actividad}}<br>
                                    Tipos:<br>

                                    <div style="margin-left: 16px;">
                                      <template v-for="tipo of recepcion.descripcion_error.tipos">
                                        @{{tipo.nombre}}:

                                        <ul v-if="tipo.quiebres.length <= 0">
                                           <li >
                                            @{{tipo.msj}}
                                            </li>
                                        </ul>


                                        <ul v-if="tipo.quiebres.length > 0" v-for="quiebre of tipo.quiebres">

                                          <span  style="margin-left: -16px">@{{quiebre.nombre}}</span>

                                          <li v-for="motivo of quiebre.motivos">
                                            @{{motivo}}
                                          </li>


                                        </ul>
                                      </template>
                                    </div>

                                  </div>
                              </div>
                            </div>

                            <div class='row'>
                              <div class='col-sm-12'>
                                <label class=''>Trama</label>
                                <textarea disabled class='form-control' style='resize:none; height: 150px;'>@{{recepcion.xmltrama}}</textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </template>
                </div>
              </div>
            </div>
            <div class="row form-group">
              <input type="text" name="solicitudtecnica" id="solicitudtecnica" class="form-control" />
              <a href="" class="btn btn-primary" id="btn-reenviar-st">Reenviar Respuesta de ST</a>
            </div>
          </div>
          <div class="tab-pane fade" id="tabsms">
          <div class="row form-group">
            <div class="col-sm-12">
              <div class="box-body table-responsive">
                <div class="panel-group" id="panel-sms">
                  <form id="form-sms-legado" action="bandejalegado/enviarsms" class="">
                    <div class="col-sm-12">
                      <label>Numeros Telefonicos</label>
                      <input type="text" name="numeros_telefonicos" id="numeros_telefonicos" placeholder="1 o mas numeros separados por comas" class="form-control numeros_telefonicos" required="" />
                    </div>

                    <div class="col-sm-12">
                      <label>Mensaje</label>
                      <textarea placeholder="Ingrese su mensaje aqui..." maxlength="140" name="contenido_sms" id="contenido_sms" class="form-control" style="height: 60px; resize: none;" required=""></textarea>
                    </div>
                    <div class="col-sm-12">
                      <input type="submit" value="Enviar" :disabled='noGestion' class="btn btn-primary pull-right" style="margin-top: 15px"/>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>