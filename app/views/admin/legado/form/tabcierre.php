<div style="min-height: 400px">
 
  <fieldset v-if="solicitud.actividad_id == 2" class="scheduler-border" id="dto_cliente">
  <legend class="scheduler-border">Datos de Cliente</legend>
    <input class="form-control input-sm" type="hidden" v-model='solicitud.actividad_id' id="hd_actividadId" />
    <div class="col-sm-6">
      <label>Nombre Cliente</label>
      <input class="form-control input-sm" type="text" v-model='fullName' disabled="disabled"/>
    </div>
    <div class="col-sm-3">
      <label>DNI</label>
      <input class="form-control input-sm" type="text" v-model="solicitud.doc_identidad" disabled="disabled"/>
    </div>
    <div class="col-sm-3">
      <label>Celular Cliente</label>
      <input class="form-control input-sm" type="text" v-model="telefonos" disabled="disabled"/>
    </div>
    </fieldset>
  
  <fieldset class="scheduler-border">
  <legend class="scheduler-border">Datos de Cierre en TOA</legend>

  <div class="bloque-datos-tecnico col-sm-7">
    <div class="row">
      <input class="form-control input-sm" type="hidden" v-model='solicitud.actividad_id' id="hd_actividadId" />
      <div class="col-sm-6">
        <label>Tecnico</label>
        <input class="form-control input-sm" type="text" v-model='solicitud.nombretecnico' disabled="disabled"/>
      </div>
      <div class="col-sm-3">
        <label>Carnet</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.carnettecnico" disabled="disabled"/>
      </div>
      <div class="col-sm-3">
        <label>Celular Tecnico</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.celulartecnico" disabled="disabled"/>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <label>Contacto</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.a_receive_person_name" disabled="disabled"/>
      </div>
      <div class="col-sm-6">
        <label>DNI</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.a_receive_person_id" disabled="disabled"/>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <label>Observacion TOA</label>
        <textarea class="form-control" rows='2' v-model='solicitud.xa_observation' disabled="disabled"></textarea>
        <!--<input class="form-control input-sm" type="text" v-model='solicitud.xa_observation' disabled="disabled"/>-->
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4" id="div_tipo_cierre">
        <label>Tipo de Devolucion</label>
        <input class="form-control input-sm" type="text" v-model='tipoDevolucion' disabled="disabled" 
        id="txt_tipoDevolucion"/>
      </div>
      <div class="col-sm-8">
        <label id="lb_motivo_cierre">Motivo Devolucion</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.motivoofsc" disabled="disabled"/>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-8" id="div_submotivo_cierre">
        <label id="lb_submotivo_cierre">SubMotivo Devolucion</label>
        <input class="form-control input-sm" type="text" v-model="solicitud.submotivoofsc" disabled="disabled"/>
      </div>
      <div class="col-sm-4">
        <label>Telefono Cliente</label>
        <input class="form-control input-sm" type="text" v-model="telefonos" disabled="disabled" 
        id="lbltelefonos_tbcr" />
      </div>      
    </div> 
</div>

  <div class="form-group col-sm-5" id="completadoCarousel2">
    <!-- <div id="" class="carousel slide" style="display: inline-block;margin: auto;height: 200px;width: 300px;">
    </div> -->
    <!--Imagenes-->
    <template v-if="imagenes">
      <div id="completadoCarousel" class="carousel slide" style="display: inline-block;margin: auto;height: 200px;width: 300px;">
        
        <ol class="carousel-indicators">
          <template v-for="(index, imagen) in imagenes">
            <li data-target="#completadoCarousel" data-slide-to="{{index}}" :class="index == 0 ? 'active' : ''"></li>
          </template>
        </ol>
       
        <div class="carousel-inner">
          <template v-for="imagen in imagenes">
            <div :class="imagen.class">
              <img :src="imagen.src">
              <div class="carousel-caption">
                <h3>{{imagen.nombre}}</h3>
              </div>
            </div>
          </template>
        </div>

        
        <a class="left carousel-control" href="#completadoCarousel" role="button">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#completadoCarousel" role="button">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </template> 
    <!---->
    <!---->
  </div>

  </fieldset>

    <form id="form_cierre" name="form_cierre">
      
      <fieldset id='enlace-cierre-panel' class="scheduler-border">
      <legend class="scheduler-border">Ingreso de Motivos</legend>
      <div class="row form-group" >
        <input type="hidden" id="txt_codactu" name="txt_codactu">
        <input type="hidden" id="txt_tipo_actividad" name="txt_tipo_actividad">
        <input type="hidden" name="txt_tipo_cierre" id="txt_tipo_cierre">       
        <div class="col-sm-12" id="div_tipo_devolucion">
          <div class="col-sm-4" style="display:none;">
            <label>Tipo de Devolucion</label>
            <select class="form-control" name="slct_tipo_devolucion" id="slct_tipo_devolucion">
              <option value=''>.::Seleccione::.</option>
              <option value="C">Comercial</option>
              <option value="T">Tecnico</option>
            </select>
          </div>
        </div>
        <div class="col-sm-4 opt-motivo-cierre" id="opt-motivo-cierre-devolucion">
          <label>Motivo Devolucion</label>
          <select class="form-control" name="slct_motivosdevolucion" id="slct_motivosdevolucion">
          </select>
        </div>

        <div class="col-sm-4" id="divsubmotivosdevolucion" style="display: none;">
          <label>SubMotivo Devolucion</label>
          <select class="form-control" name="slct_submotivosdevolucion" id="slct_submotivosdevolucion">
          </select>
        </div>
      </div>
      <div class="col-sm-12 col-xs-12">
        <!--<label><span class="badge bg-yellow" id="count_error_cierre"></span> Intentos de Cierre</label>-->
        <div class="col-sm-4 opt-motivo-cierre" id="opt-motivo-cierre-liquidacion">
          <label>Motivo Liquidacion</label>
          <select class="form-control" name="slct_motivosliquidacion" id="slct_motivosliquidacion">
          </select>
        </div>
      </div>
      </fieldset>
      
      <fieldset id='contenedor-tematico' class="scheduler-border">
      <legend class="scheduler-border">Ingreso de Bitacoras</legend>
      <div class="row form-group" id="cont-tematico">
        <div class="col-sm-12 btn-danger" id="div-error-tematico-cierre">
            <p>No hay tematicos disponibles para esta Actividad</p>
        </div>
        <div class="col-sm-4" id="divtematico1">
          <label id="tema1">Contactabilidad</label>
          <select class="form-control slct_tematicos" name="slct_tematico_id[1]" id="slct_tematico_id1">
          </select>
        </div>
        <div class="col-sm-4 subtematico" id="divtematico2">
          <label id="tema2">Conformidad</label>
          <select class="form-control slct_tematicos" name="slct_tematico_id[2]" id="slct_tematico_id2">
          </select>
        </div>
        <div class="col-sm-4 subtematico" id="divtematico3">
          <label id="tema3">Motivo de Conformidad</label>
          <select class="form-control slct_tematicos" name="slct_tematico_id[3]" id="slct_tematico_id3">
          </select>
        </div>
        <div class="col-sm-6 subtematico" id="divtematico4">
          <label id="tema4">Submotivo de Conformidad</label>
          <select class="form-control slct_tematicos" name="slct_tematico_id[4]" id="slct_tematico_id4">
          </select>
        </div>
      </div>
      <!--**************************-->
      <div class="row form-group" style="display: none;" id="grupoOculto">        
        <div class="col-sm-3 col-xs-6">
            <label>Tel1:</label>
            <input type="text" class="form-control" maxlength="9" style="background: #fffbd0" id="txt_tel1" name="txt_tel1">
        </div>
        <div class="col-sm-3 col-xs-6">
            <label>Tel2:</label>
            <input type="text" class="form-control" maxlength="9" style="background: #fffbd0" id="txt_tel2" name="txt_tel2">
        </div>
        <div class="col-sm-6 col-xs-12">
            <label>Contacto:</label>
            <input type="text" class="form-control" style="background: #fffbd0" id="txt_contacto" name="txt_contacto">
        </div>
        <div class="col-sm-12 col-xs-12">
            <label>Dirección:</label>
            <input type="text" class="form-control" style="background: #fffbd0" id="txt_direccion" name="txt_direccion">
        </div>
        <div class="col-sm-12 col-xs-12">
            <fieldset class="text-center">
                <br>
                <label>
                    <input type="checkbox" name="chk_freeview" id="chk_freeview" value=1> Acepta Freeview
                </label>
                <label>
                    <input type="checkbox" name="chk_descuento" id="chk_descuento" value=1 > Acepta Descuento
                </label>
            </fieldset>
        </div> 
        
      </div>

      <!--**************************-->
      <div class="row form-group">
        <div class="col-sm-12">
          <textarea rows='1' name="comentario-tematico" id="comentario-tematico" placeholder="Agregar un Comentario" class="form-control"></textarea>
        </div>
      </div>
      </fieldset>
    </form>
  
</div>
<div class="modal-footer">
  <div class="col-sm-4" id="div-btn-envio-legado">
    <button type="button" class="form-control btn btn-block btn-success btn-xl btn-envio-legado" id="btn-envio-legado"  :disabled="estadoBtnGuardarCierre"><span style="margin-right: 5px; display: none;"><i class="fa fa-paper-plane" aria-hidden="true"></i></span><span id="lb_guardar">Guardar</span></button>
  </div>
  <div class="col-sm-4" id="div-activar-precierre">
    <button type="button" class="form-control btn btn-block btn-success btn-xl" id="btn_activar_precierre"><span style="margin-right: 5px; display: none;"><i class="fa fa-paper-plane" aria-hidden="true"></i></span><span id="">Activar Formulario de Pre Cierre</span></button>
  </div>
  <div class="col-sm-4" id="div-btn-cierre">
    <button type="button" class="form-control btn btn-block btn-success btn-xl" id="btn_cierre">
      <span style="margin-right: 5px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
      <span id="lb_realizar_cierre" v-if="!(solicitud.actividad_id == 2 && solicitud.estado_aseguramiento == 4 && solicitud.estado_ofsc_id == 2)">Confirmar Devuelta</span>
      <span v-if="(solicitud.actividad_id == 2 && solicitud.estado_aseguramiento == 4 && solicitud.estado_ofsc_id == 2)">Confirmar Liquidación</span>
    </button>
  </div>
  <div class="col-sm-4" id="div-btn-limpiar-formulario">
    <button type="button" class="form-control btn btn-block btn-primary btn-xl" id="btn_limpiar_formulario">
      <span style="margin-right: 5px;"><i class="fa fa-refresh" aria-hidden="true"></i></span>
      <template v-if="!(solicitud.actividad_id == 2 && solicitud.estado_aseguramiento == 4 && solicitud.estado_ofsc_id == 2)">Rechazar Devolución</template>
      <template v-if="(solicitud.actividad_id == 2 && solicitud.estado_aseguramiento == 4 && solicitud.estado_ofsc_id == 2)">Rechazar Liquidación</template>
    </button>
  </div>
  <div class="col-sm-4" id="div-btn-stop-devuelta">
    <button disabled="disabled" type="button" class="btn btn-danger btn-xl pull-left" id="btn_stop_devuelta" v-if="!(solicitud.actividad_id == 2 && solicitud.estado_aseguramiento == 4 && solicitud.estado_ofsc_id == 2)"><span style="margin-right: 5px;"><i class="fa fa-ban" aria-hidden="true"></i></span><span v-show="solicitud.actividad_id == 1">Cierre posterior</span><span v-if="solicitud.actividad_id == 2">Volver a llamar</span></button>
  </div>
</div>