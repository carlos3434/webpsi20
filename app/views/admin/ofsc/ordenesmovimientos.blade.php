<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('css/admin/ordenesofsc.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
        {{ HTML::script('js/utils.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

    @include( 'admin.js.tareas' )

    
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ordenes en OFSC
         <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Reporte</a></li>
        <li class="active">Ordenes en OFSC</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row form-group">
        <div class="col-sm-12">
            <div class="tab-content">
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#t_masivo">Masivos Automaticos a OFSC</a>
                    </li>
                </ul>
                <div class="tab-pane active" id="t_envio">
                    <div class="">
                        <form name="filtro_envios_ofsc" id="filtro_envios_ofsc">
                        <div class="box-body table-responsive">
                            <div class="col-xs-11 filtros">
                                <div class="panel-group" id="acordion_envios_ofsc">
                                    <!--<div class="panel panel-default">
                                        <div class="panel-heading box box-primary">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc" href="#collapse2">Búsqueda Individual</a>
                                            </h4>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="box  personalizado">
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <label class="titulo">Código Requerimiento</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" name="codigo_actuacion" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div> -->
                                                  <div class="panel panel-default">
                                                    <div class="panel-heading box box-primary">
                                                      <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_envios_ofsc" href="#collapse1">Busqueda Personalizada</a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse1" class="panel-collapse collapse in">
                                                      <div class="panel-body">
                                                        <div class="box  personalizado">
                                                             <div class="box-body">
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label class="titulo">Fecha  Envío</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                                                        <input class="form-control" id="fecha_registro" name="fecha_registro" type="text"  aria-describedby="basic-addon1" readonly>
                                                                    </div>
                                                                </div>
                                                            <div class="col-sm-3">
                                                                <label class="titulo">Empresa</label>
                                                                <select class="form-control" name="slct_empresa[]" id="slct_empresa" multiple>
                                                                <option value="">.::Seleccione::.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label class="titulo">Actividad</label>
                                                                <select class="form-control" name="slct_actividad[]" id="slct_actividad" multiple>
                                                                <option value="">.::Seleccione::.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label class="titulo">Nodo</label>
                                                                <select class="form-control" name="slct_nodo[]" id="slct_nodo" multiple>
                                                                <option value="">.::Seleccione::.</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <label class="titulo">Quiebre</label>
                                                                <select class="form-control" name="slct_quiebre[]" id="slct_quiebre" multiple>
                                                                <option value="">.::Seleccione::.</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>     
                                               </div>
                                            </div>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                                    </div>
                                                    <div class="col-xs-1  botones">
                                                        <div class="">
                                                                <i class="fa fa-search fa-lg btn btn-primary" title="Buscar" id="buscar-ordenes"></i>
                                                                <!--<i class="fa fa-download fa-lg btn btn-success" title="Descargar"></i>-->
                                                        </div>
                                                    </div>
                                            <input type="hidden" name="bandeja" value="ordenesofsc" />
                                      </form> 
                                        <div class="col-xs-12 box box-primary">
                                            <div class="box-body table-responsive">
                                                <table id="t_enviosofs" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Cod. Req</th>
                                                            <th>F. Registro</th>
                                                            <th>Empresa</th>
                                                            <th>Quiebre</th>
                                                            <th>Nodo</th>
                                                            <th>Resource Ant.</th>
                                                            <th>Resource Nuevo.</th>
                                                            <th>F.Agenda Ant.</th>
                                                            <th>F.Agenda Nuevo.</th>
                                                            <th>Mensaje</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb_enviosofs">
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Cod. Req</th>
                                                        <th>F. Registro</th>
                                                        <th>Empresa</th>
                                                        <th>Quiebre</th>
                                                        <th>Nodo</th>
                                                        <th>Resource Ant.</th>
                                                        <th>Resource Nuevo.</th>
                                                        <th>F.Agenda Ant.</th>
                                                        <th>F.Agenda Nuevo.</th>
                                                        <th>Mensaje</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div><!-- /.box-body -->
                                        </div><!-- Finaliza Envíos OFSC -->
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.tab -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->        
@stop
@section('formulario')
    @include('admin.ofsc.js.ordenes_movimientos_ajax')
    @include('admin.ofsc.js.ordenes_movimientos')
@stop