<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/datepicker/css/datepicker.css")}}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style("css/admin/asistencia.css") }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Registro Asistencia de Tecnicos OFSC
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Gestion OFSC</a></li>
        <li class="active">Registro Asistencia de Tecnicos OFSC</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#pane_opciones" data-toggle="tab">Opciones de Registro</a>
          </li>
          <li>
            <a href="#pane_historial" data-toggle="tab">Historial de Registro</a>
          </li>
        </ul>
        <div class="tab-content ">
                    <div class="tab-pane active" id="pane_opciones">
                        <div class="box">
                          <div class="panel-group" style="margin-bottom: 0px;">
                              <div class="panel panel-default">
                                <div class="panel-heading box box-primary" style="margin-bottom: 0px;">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_opciones" href="#opciones" id="panel_individual">Opciones</a>
                                 </h4>
                            </div>
                            <div id="opciones" class="panel-collapse collapse in">
                              <div class="panel-body" style="padding: 0px;">
                                <div class="personalizado">
                                  <div class="box-body">
                                   <div class="row">
                                    <div class="col-md-11">
                                       <div class="form-group">
                                        <form id="form-opciones">
                                          <div class="col-sm-4">
                                            <label>Buckets</label>
                                            <select class="form-control" name="slct_bucket[]" id="slct_bucket_filtro_opt" multiple="">
                                              <option value="">.::Seleccione::.</option>
                                            </select>
                                          </div>
                                          
                                        </form>
                                        <div class="col-sm-4 pull-right">
                                          <a class='btn btn-warning btn-sm form-control' style="margin:5px;" id="nuevo" data-toggle="modal" data-from = "view" data-target="#asistenciaModal" data-titulo="Nuevo" data-id="0"><i class="fa fa-eye fa-lg"></i>&nbsp;Vista Multiple Asistencia</a>
                                          <a class='btn btn-primary btn-sm form-control' style="margin:5px;" id="nuevo" data-toggle="modal" data-target="#asistenciaModal" data-titulo="Nuevo" data-id="0"><i class="fa fa-plus fa-lg"></i>&nbsp;Nueva Marcacion de Asistencia</a>
                                          
                                          </div>
                                      </div>
                                    </div>
                                    <div class="col-md-0 pull-right">
                                      <form id="form-download" method="POST" action="">
                                        <input type="hidden" name="flagdownload" value="true"/>
                                      </form>
                                    </div>
                                  </div>
                                 </div>
                                </div>
                              </div>
                            </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pane_historial">
                       <div class="box">
                          <div class="panel-group" style="margin-bottom: 0px;">
                              <div class="panel panel-default">
                                <div class="panel-heading box box-primary" style="margin-bottom: 0px;">
                                  <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#filtros" id="panel_individual">Filtros</a>
                                  </h4>
                                </div>
                                <div id="filtros" class="panel-collapse collapse in">
                                  <div class="panel-body" style="padding: 0px;">
                                    <div class="personalizado">
                                      <div class="box-body">
                                       <div class="row">
                                        <div class="col-md-11">
                                           <div class="form-group">
                                           <form id="form-filtro" method="POST">
                                            <div class="col-sm-4">
                                              <label>Fecha</label>
                                              <input type="text" class="form-control" name="txt_fecha" id="txt_fecha_filtro" value="{{date('Y-m-d')}} - {{date('Y-m-d')}}"/>
                                            </div>
                                            <div class="col-sm-4">
                                              <label>Buckets</label>
                                              <select class="form-control" name="slct_bucket[]" id="slct_bucket_filtro" multiple="">
                                                <option value="">.::Seleccione::.</option>
                                              </select>
                                            </div>
                                            </form>
                                          </div>
                                        </div>
                                        <div class="col-md-1 pull-right">
                                          <a id="btn-buscar" href="#" class="btn btn-primary"><i class="fa fa-search"></i></a>
                                          <a class='btn btn-success' style="margin:5px;" id="descargar" data-titulo="Descargar">
                                        <i class="fa fa-download"></i></a>
                                         </a>
                                        </div>
                                      </div>

                                      <div class="table-responsive">
                                          <table id="t_asistencia" class="table table-bordered table-striped">
                                              <thead>
                                                  <tr>
                                                    <th>Id</th>
                                                    <th>F. Registro</th>
                                                    <th>Bucket</th>
                                                    <th>T. Tecnicos</th>
                                                    <th>T. Asistio</th>
                                                    <th>T. Ausente</th>
                                                    <th>T. Permiso</th>
                                                    <th>T. Vacaciones</th>
                                                    <th>Usuario</th>
                                                    <th class="editarG"> [ ] </th>
                                                  </tr>
                                              </thead>
                                              <tbody id="tb_asistencia">

                                              </tbody>
                                              <tfoot>
                                                  <tr>
                                                    <th>Id</th>
                                                    <th>F. Registro</th>
                                                    <th>Bucket</th>
                                                    <th>T. Tecnicos</th>
                                                    <th>T. Asistio</th>
                                                    <th>T. Ausente</th>
                                                    <th>T. Permiso</th>
                                                    <th>T. Vacaciones</th>
                                                    <th>Usuario</th>
                                                    <th class="editarG"> [ ] </th>
                                                  </tr>
                                              </tfoot>
                                          </table>
                                      </div><!-- /.box-body -->
                                     </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
</section><!-- /.content -->
@endsection

@section('formulario')
     @include( 'admin.ofsc.form.asistenciatecnico' )
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/datepicker/js/bootstrap-datepicker.js")}}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/bootbox.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&libraries=places,geometry,drawing') }}
    {{ HTML::script("lib/markerclusterer/markerclusterer.js") }}
    {{ HTML::script("lib/markerclusterer/OverlappingMarkerSpiderfier-oms.js") }}
    {{ HTML::script("js/geo/markerwithlabel.js") }}
    {{ HTML::script('js/fontawesome-markers.min.js') }}
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/ofsc/asistenciamodal.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/ofsc/asistenciamapa.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/ofsc/asistenciatecnico_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/ofsc/asistenciatecnico.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')