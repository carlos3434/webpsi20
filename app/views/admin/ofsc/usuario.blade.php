<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/angular/angular-tree.min.css') }}
{{ HTML::script('lib/angular/angular.min.js') }}
{{ HTML::script('lib/angular/angular-ui-router.min.js') }}
{{ HTML::script('lib/angular/ocLazyLoad.min.js') }}
{{ HTML::script('lib/angular/ui-bootstrap-angular.min.js') }}
{{ HTML::script('lib/angular/angular-sanitize.min.js') }}
{{ HTML::script('lib/angular/angular-resource.js') }}
{{ HTML::script('lib/angular/angular-messages.min.js') }}
{{ HTML::script('lib/angular/angular-tree.min.js') }}
{{ HTML::script('app/app.js') }}

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Mantenimiento de Usuarios OFSC
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Mantenimiento de Usuarios OFSC</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" ng-app="psi" >


    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Filtros</h3>
                </div><!-- /.box-header -->
                <ui-view></ui-view>
            </div><!-- /.box -->
            <!-- Finaliza contenido -->
        </div>
    </div>

</section><!-- /.content -->
@stop
