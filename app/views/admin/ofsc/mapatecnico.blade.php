<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style('css/admin/mapatecnico.css') }}
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
{{Form::token() }}
<script type='text/javascript'>publicurl = '<?php echo url();?>';</script>
<spinner id="spinner-box" :size="size" :fixed="fixed" v-show="loaded" text="Espere un momento por favor">
</spinner>
        
 <section class="content-header">
          <h1>Localización de Recursos en OFSC<small> </small></h1>
            <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                  <li><a href="#">Gestión OFSC</a></li>
                   <li class="active">Localización de Recursos OFSC</li>
             </ol>
 </section>
 <section class="content" id="contenedor-mapaLocalizacionRecursos">
    <div id='mapatecnicosController'>
        <spinner :size="size" :fixed="fixed" v-show="loaded" text="Espere un momento por favor">
        </spinner>
        <alert id='mensaje' type="danger" v-if="mensaje!=''">
            <strong>@{{mensaje}}</strong>
        </alert>
        <div class="row">
            <div v-show="!showLeft" class="botones form-group">
                <div class="row">
                    <div class="col-md-3">
                        <button v-show="!showLeft" class="btn btn-success btn-sm" @click="showLeft = true">
                        <i class="fa fa-search" style="margin-right: 5px;"></i> Filtros
                        </button>
                    </div>
                    <div class="col-md-3">
                        <button  class="btn btn-success btn-sm" @click="showRight = !showRight">
                        Tecnicos
                        </button>
                    </div>
                    <div class="col-md-3" id="pintaMapa" style="display: none;">
                        <button type="button" @click="showLeft = false" @click.prevent="clickOpciones(pintarMapa,true)" class="btn btn-success btn-sm">Pintar Rutas</button>

                    </div>
                    <div class="col-md-3" id="refrescarActividades">
                        <button class="btn btn-danger btn-sm" type="button" @click="refrescarActividades"><i class="fa fa-refresh"></i> Actualizar Actividades</button>
                    </div>               
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var hoy = '{{ date("Y-m-d") }}';
            var ahora = '{{ date("Y-m-d H:i:s") }}';
            var long_bucket = '{{ Config::get("validacion.lari-ate.longitude") }}';
            var lat_bucket = '{{ Config::get("validacion.lari-ate.latitude") }}';
            var entorno = 'PROD';
            var urlMapsbusquedaXY = "http://maps.google.com/maps?q=";
            var buckettext = "";
            alturavista = $("body").height();
            alturaheader = $("header").height();
            alturauserpanel = $(".user-panel").height();
            $("#mapatecnicosController").css("height", (alturavista - alturaheader - alturauserpanel)*0.9);
        </script>
        <aside :show.sync="showLeft" placement="left" header="Filtros" :width="550">
            <div class="aside-body">
                <div class="row">
                    <div class="col-md-12">
                    <div class="fila">
                        <div class="col-md-4">
                            <label for="actividadesTipo">Tipo de Recursos:</label>
                        </div>
                        <div class="col-md-8">
                            <v-select v-bind:value="actividadesTipoSeleccionados" multiple :disabled="checked ? true : false">
                            <v-option v-for="(key, val) in actividadesTipo" v-bind:value="key">@{{ val }}</v-option>
                            </v-select>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="col-md-4">
                            <label for="actividadesTipo">Bucket:</label>
                        </div>
                        <div class="col-md-8">
                            <v-select :value.sync="resource_id" multiple placeholder='ningun bucket'>
                                <v-option  @click="selectBucket(resource)" v-for="resource in resources_id" v-bind:value="resource.id">@{{ resource.name }}</v-option>
                            </v-select>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="col-md-4">
                            <label for="tecnicos">Tecnicos:</label>
                        </div>
                        <div class="col-md-8">
                            <v-select v-bind:value="tecnicosSeleccionados" multiple placeholder='ningun tecnico'>
                                <v-option @click="selectAllTecnicos()" value="todos">todos</v-option>
                                <v-option @click="selectTecnico(key)" v-for="(key, val) in tecnicos" v-bind:value="key">@{{ val.name }}</v-option>
                            </v-select>
                        </div>
                    </div>
                    <div class="fila">
                        <div class="col-md-4">
                            <label for="tecnicos">Trazos:</label>
                        </div>
                        <div class="col-md-8">
                            <checkbox-group :value.sync="trazos" :disabled="checked ? true : false">
                                <checkbox value="lineas">Lineas</checkbox>
                                <checkbox value="rutas">Rutas</checkbox>
                            </checkbox-group>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <div class="aside-footer">
                            <button type="button" @click="showLeft = false" class="btn btn-default">Cerrar</button>
                            <button type="button" @click="showLeft = false" @click.prevent="clickOpciones(pintarMapa,true)" class="btn btn-success">Ver Mapa </button>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <accordion :one-at-atime="true" type="info">
                        <panel header="Buscar actividades segun:" type="primary">
                            <div class="col-md-12">
                                <input type="radio" id="historico" value="historico" v-model="lista">
                                <label for="historico">Histórico</label>
                                <br>
                                <input :disabled="entorno==='TRAIN'" type="radio" id="proceso" value="proceso" v-model="lista" @click="listarHistoricoProceso('')">
                                <label for="proceso">Proceso Automatico</label>
                                <br>
                                <input  type="radio" id="carga_csv" value="carga_csv" v-model="lista">
                                <label for="carga_csv">Cargar csv</label>
                            </div>
                            {{-- Histórico --}}
                            <div v-show="lista=='historico'" class="col-md-12">
                                <div class="col-md-8">
                                    <select class="form-control" v-model="json_historico">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="json in json_historicos" v-bind:value="json.json_actividad"> @{{ json.nombre }} | @{{ json.fecha }} </option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" @click.prevent="verMapaHistorico(json_historico)" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                            {{-- Proceso --}}
                            <div v-show="lista=='proceso'" class="col-md-12">
                                <div class="col-md-4">
                                    <select class="form-control" v-model="historicoProceso" @change="listarHistoricoProceso(historicoProceso)">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="(key, val) in historico_proceso" v-bind:value="val.fregistro">@{{ val.fregistro }}</option>
                                    </select>
                                </div>
                                <div v-show="historico_proceso_detalle" class="col-md-4">
                                    <select class="form-control" v-model="historicoProcesoDetalle" @change="listarHistoricoProceso(historicoProceso)">
                                        <option selected>.: Seleccione :.</option>
                                        <option v-for="(key, val) in historico_proceso_detalle" v-bind:value="val.data">@{{ val.hReporte }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" @click.prevent="verMapaProcAutomatico" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                            {{-- cargar csv --}}
                            <div v-show="lista=='carga_csv'" class="col-md-12">
                                <div class="col-md-5">
                                    <input type="file" @change="onFileChange" id="actividades_csv" accept=".csv">
                                </div>
                                <div class="col-md-5">
                                    <input type="file" @change="onFileChange" id="tecnicos_csv" accept=".csv">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" @click.prevent="verMapaCsv" class="btn btn-success pull-right">Ver Mapa</button>
                                </div>
                            </div>
                       </panel>
                       <panel header="Opciones">
                            <button type="button" @click.prevent="clickOpciones(guardarMapaToJson)" class="btn btn-danger">Guardar Mapa</button>
                            <button type="button" @click.prevent="clickOpciones(descargarXls)" class="btn btn-info">Descargar XLS</button>
                            <button type="button" @click.prevent="clickOpciones(descargarKml)" class="btn btn-primary">Descargar KML</button>
                            <button type="button" @click.prevent="clickOpciones(analizarMapa)" class="btn btn-danger">Analizar Mapa</button>
                            <!--<button type="button" @click.prevent="asistenciaTecnicos(verFormularioAsistencia)" class="btn btn-danger"><i class="fa fa-calendar" style="margin-right: 5px;"></i>Asistencia Tecnico</button>-->
                       </panel>
                    </accordion>
                </div>
                
            </div>
        </aside>
        @include( 'admin.ofsc.form.estadistico' )
        <div id="map" ></div>
        <div v-show='showRight' style="overflow: hidden;">
            <div class="aside slideright-transition right">
                <div class="aside-dialog">
                    <div class="aside-content">
                        <div class="aside-header">
                        <a @click="showRight = false" class="close">×</a>
                        <h4 class="aside-title">Tecnicos</h4>
                        </div>
                        <div class="aside-body">
                            <span class="ppart" style="background-color: #A5D6A7; padding: 5px 5px 5px 5px;"> &#60;= 20 minutos</span>
                            <span class="ppart" style="background-color: #FFCC80; padding: 5px 5px 5px 5px;"> &#60;= 2 horas</span>
                            <span class="ppart" style="background-color: #EF9A9A; padding: 5px 5px 5px 5px;"> &#60;= 4 horas</span>
                            <span class="ppart" style="background-color: #9FA8DA; padding: 5px 5px 5px 5px;"> &#62; 4 horas</span>
                        </div>
                        <div class="scroll">
                            <div class="aside-body row form-group" id="tecnicos">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </div>
 </section>

                <!-- Main content -->
@stop
@push('scripts')
<script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
    $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck("destroy");
</script>
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}
    {{ HTML::script('js/fontawesome-markers.min.js') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('js/admin/ofsc/tecnicos.js') }}
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&callback=initMap') }}
    {{ HTML::script('js/geo/markerwithlabel.js' , array('async' => 'async')) }}
@endpush('script')