<!DOCTYPE html>
@extends('layouts.master_optimice')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
   
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('css/admin/programacion_tecnicos.css') }}
   
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
 <section class="content-header">
          <h1>Programación de Técnicos en OFSC<small> </small></h1>
            <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                  <li><a href="#">Gestión OFSC</a></li>
                   <li class="active">Programación de Técnicos OFSC</li>
                   <li class="active"><input type="button" value="-" class="show_hide_filtros"></li>
             </ol>
 </section>

                <!-- Main content -->
<section class="content" id="filtros">
<div class="panel-group" id="accordion-filtros">
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">
                               Filtros de Reporte
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="form_programacion" id="form_programacion" method="POST" action="reporte/bandejaexcel">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label class="titulo">Quiebre:</label>
                                            <select class="form-control" name="slct_quiebre[]" id="slct_quiebre"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                                 <label class="titulo">Empresa</label>
                                                 <select class="form-control" name="slct_empresa[]" id="slct_empresa" multiple>
                                                   <option value="">.::Seleccione::.</option>
                                                    </select>
                                            </div>
                                        
                                        <div class="col-sm-3">
                                            <label class="titulo">Tecnico:</label>
                                            <select class="form-control" name="slct_tecnico[]" id="slct_tecnico"
                                                    multiple>
                                                <option value="">.::Seleccione::.</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3"> 
                                            <label>Agenda:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" name="fecha_agenda" id="fecha_agenda"  />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        </div>
                        <div class="panel-footer">
                        <div class="row form-group">
                                <div class="col-sm-12">
                                        <div class="col-sm-3 pull-rigth ">
                                              <a class="btn btn-success mostrar-tecnicos">Mostrar Técnicos Programados</a>
                                        </div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>



                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseOne">
                               Actividades por Tecnico y Día.
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body contenedor-programacion">
                              <div class="box-body table-responsive">
                                <table id="t_programacion_tecnico" class="table table-bordered table-striped col-sm-12 responsive"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_bandeja">

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">
                               Mapa de Actividades y Tecnicos por Día.
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div>
                              
                        </div>
                    </div>
                </div>
            </div>
                                      
                </section><!-- /.content -->
  
@stop
@section('formulario')
    @include( 'admin.ofsc.form.ordenDetalleModal' )
@stop
@section('javascript')
    @parent
     {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('js/psi.js') }}
     {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/utils.js') }}

    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?libraries=places,geometry,drawing') }}
    {{ HTML::script('js/geo/geo.functions.js', array('async' => 'async')) }}
    {{ HTML::script('js/geo/markerwithlabel.js', array('async' => 'async')) }}
    {{ HTML::script('js/utils.js', array('async' => 'async')) }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    
    @include( 'admin.ofsc.js.programacion_tecnicos_ajax' )
    @include( 'admin.ofsc.js.programacion_tecnicos' )
@stop