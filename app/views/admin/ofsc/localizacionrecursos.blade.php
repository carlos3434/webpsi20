<!DOCTYPE html>
@extends("layouts.mastermapa")  
@section('includes')
    @parent
    {{ HTML::style('css/admin/localizacionrecursos.css') }}
   
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
{{Form::token() }}
<script type='text/javascript'>publicurl = '<?php echo url();?>';</script>
<spinner id="spinner-box" :size="size" :fixed="fixed" v-show="loaded" text="Espere un momento por favor">
</spinner>
        
 <section class="content-header">
          <h1>Localización de Recursos en OFSC<small> </small></h1>
            <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                  <li><a href="#">Gestión OFSC</a></li>
                   <li class="active">Localización de Recursos OFSC</li>
             </ol>
 </section>
 <section class="content" id="contenedor-mapaLocalizacionRecursos">
    <div class="container-fluid">
            <div class="row">
                    <div class="panel-group contenedor-tecnicos" id="contenedor-tecnicos">
                         <div class="col-md-12">
                         <div class="panel panel-default">
                                 <div class="panel-heading box box-primary">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#contenedor-tecnicos"
                                       href="#collapseOne">Tecnicos:</a>
                                </h4>
                            </div>
                             <div id="collapseOne" class="panel-collapse collapse ">
                                    <div class="panel-body">
                                        <ul class="listado-tecnicos">
                                            <li  class="col-md-3" v-for="(key, val) in tecnicos" v-bind:value="key"  @click="mostrarInfoTecnico(key)">
                                                <a    href="#" data-carnet="@{{ key}}" data-color="@{{val.color}}"><i class="fa fa-male" v-bind:style="val.styleObject" aria-hidden="true"></i>
                                                <span>@{{ val.name }}</span></a>
                                            </li>
                                        </ul>
                                        <!--<div class="scroll">
                                            <div class="aside-body row form-group" id="tecnicos">
                                            </div>
                                        </div>-->
                                 </div>
                                 </div>
                             </div>
                       </div>
                  </div>
                 <div class="col-md-12">
                         <div class="box box-primary">
                             <div class="box-header">
                                 <div id="mapaLocalizacionRecursos">
                                 </div>
                             </div>
                         </div>
                   </div>
        </div>

     </div>
 </section>

                <!-- Main content -->
@stop
@section('javascript')
    @parent
     {{ HTML::script('js/fontawesome-markers.min.js') }}
    {{ HTML::script('js/vue_models/localizacionrecursos.js') }}
        {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&callback=initMap') }}
        {{ HTML::script('js/geo/markerwithlabel.js' , array('async' => 'async')) }}
@stop