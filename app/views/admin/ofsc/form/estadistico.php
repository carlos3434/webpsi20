<modal :show.sync="showModal" large id="modalestadistico">

  <div slot="modal-header" class="modal-header">
    <button type="button" class="close" @click="showModal = false"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Estadistico de distancias Bucket:  {{'X:'+long_bucket +' Y:'+  lat_bucket}}</h4>
  </div>

  <div slot="modal-body" class="modal-body">

    <div class="alert alert-danger" transition="danger" v-if="danger">
      <ul>
        <li v-for="error in errores">
          {{ error }}
        </li>
      </ul>
    </div>
    <tabs>
        <tab header="Resumen">
            <form action="#" @submit.prevent="exportarXls(pestaniaUno)">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Carnet</th>
                                <th># act</th>
                                <th>Bucket - actividad (/2)</th>
                                <th>Entre actividades</th>
                                <th>Promedio</th>
                                <th>Distancia Total</th>
                                <th>Promedio Total</th>
                            </tr>
                        </thead>

                        <tbody>
                            <template v-for="(item, index) in pestaniaUno">
                                <tr>
                                    <td>@{{index.carnet}}</td>
                                    <td>{{index.cantidad_actividades}}</td>
                                    <td>{{index.distancia_bucket_act}} - {{index.promedio_distancia_bucket_act}}</td>
                                    <td>{{index.distancia_entre_act}}</td>
                                    <td>{{index.promedio_distancia_entre_act}}</td>
                                    <td>{{index.suma_distancias}}</td>
                                    <td>{{index.suma_promedios}}</td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                <div class="row form-group">
                    <div  class="modal-footer">
                        <button type="button" class="btn btn-default" @click="showModal = false">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </tab>
        <tab header="Detalle">
            <form action="#" @submit.prevent="exportarXls(pestaniaDos)">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Recurso</th>
                                <th>Hora inicio</th>
                                <th>Orden</th>
                                <th>zona_trabajo</th>
                                <th>envio</th>
                                <th>asig</th>
                                <th>Actividad X</th>
                                <th>Actividad Y</th>
                                <th>Bucket-Actividad</th>
                                <th>Entre Actividades</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="(item, index) in pestaniaDos">
                                <tr>
                                    <td>{{index.carnet}}</td>
                                    <td>{{index.start_time}}</td>
                                    <td>{{index.appt_number}}</td>
                                    <td>{{index.zona_trabajo}}</td>
                                    <td>{{envio(index.tipo_envio)}}</td>
                                    <td>{{asignacion(index.tipo_asignacion)}}</td>
                                    <td>{{index.actividad_x}}</td>
                                    <td>{{index.actividad_y}}</td>
                                    <td>{{index.distancia_bucket_act}}</td>
                                    <td>{{index.distancia_entre_act}}</td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                <div class="row form-group">
                    <div  class="modal-footer">
                        <button type="button" class="btn btn-default" @click="showModal = false">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </tab>
    </tabs>
    

  </div>
  <div v-show="false" slot="modal-footer" class="modal-footer"></div>
</modal>