<!-- /.modal -->
<div class="modal fade" id="reporteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Editar Permisos a Eventos</h4>
            </div>
            <div class="modal-body">
                <form id="form_repor_message" name="form_repor_message" action="" method="post" style="display:none">
                    <div class="row form-group">
                        <div class="col-sm-4">
                                <label class="control-label">Cod Actu
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_codactu" id="txt_codactu" readonly="">
                        </div>
                        <div class="col-sm-4">
                                <label class="control-label">Aid
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_aid" id="txt_aid" readonly="">
                        </div>
                        <div class="col-sm-4">
                                <label class="control-label">Id Mensaje
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_idmensaje" id="txt_idmensaje" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-8">
                                <label class="control-label">Descripción
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_descripcion" id="txt_descripcion" readonly="">
                        </div>
                        <div class="col-sm-4">
                                <label class="control-label">Respuesta
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_respuesta" id="txt_respuesta" readonly="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-8">
                                <label class="control-label">Asunto
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_asunto" id="txt_asunto" readonly="">
                        </div>
                        <div class="col-sm-4">
                                <label class="control-label">Fecha
                                </label>
                                <input type="text" class="form-control" placeholder="" name="txt_fecha" id="txt_fecha" readonly="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Mensaje
                        </label>
                        <textarea class="form-control resize-vertical" id="txt_mensaje" name="txt_mensaje"  rows="8" disabled></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->