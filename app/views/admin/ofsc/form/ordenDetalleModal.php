<!-- /.modal -->
<div class="modal fade" id="codactuModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Código de Actuación</h4>
            </div>
            <div class="modal-body"  style="overflow: auto;height:700px;">
                <table id="t_codactu" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cod Actu</th>
                            <th>F. Registro</th>
                            <th>Quiebre</th>
                            <th>Empresa</th>
                            <th>F. Agenda</th>
                            <th>Tecnico</th>
                            <th>Estado Ofsc</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Cod Actu</th>
                            <th>F. Registro</th>
                            <th>Quiebre</th>
                            <th>Empresa</th>
                            <th>Fecha Agenda</th>
                            <th>Tecnico</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box box-body">
                                <a class="btn btn-primary" id="mapActuTec">
                                    <i class="fa fa-globe"></i> Mapa Actu/Inicio
                                </a>
                                <div id="codactu_modal_mapObjectMulti1"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box box-body">
                                <a class="btn btn-primary" id="mapIniActu">
                                    <i class="fa fa-globe"></i> Street View Actu
                                </a>
                                <div id="codactu_modal_mapObjectMulti3"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box box-body">
                                <a class="btn btn-primary" id="mapIniTec">
                                    <i class="fa fa-globe"></i> Street View Inicio
                                </a>
                                <div id="codactu_modal_mapObjectMulti2"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tecnicoprogramado_tareas"></div>
            </div>            
            <div class="modal-footer">
                <button type="button" id="btn_close_codactu_modal" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->