<!-- /.modal -->
<div class="modal fade" id="reporte-actividades-tecnicos" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header logo">
        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
        </button>
        <h4 class="modal-title">Reporte de Actividades vs Tecnicos</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-9">
                            <ul id="myTab" class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#t_programadas" data-toggle="tab">Programadas</a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#t_noprogramadas" data-toggle="tab">No Programadas</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <a href="" class="exportar-actividades-tecnicos btn btn-success">Exportar a Excel</a>
                        </div>
                    </div>
                    
                    
                <div class="tab-pane active" id="t_programadas">
                    <div class="box">
                        <div class="box-body table-responsive">
                        <table id="t_reporte_actividades_ofsc_historico" class="table table-bordered table-striped">
                           <thead id="th_reporte_actividades_ofsc_historico">
                                <tr>
                                    <th>Estados</th>
                                    <th>Tecnicos</th>
                                    </tr>
                            </thead>
                            <tbody id="tb_reporte_actividades_ofsc_historico">
                                
                            </tbody>
                        </table>
                                    
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="tab-pane" id="t_noprogramadas">
                    <div class="box">
                        <div class="box-body table-responsive">
                        <table id="t_reporte_actividades_ofsc_historico" class="table table-bordered table-striped">
                           <thead id="th_reporte_noprogramados">
                                <tr>
                                    <th>Tecnicos</th>
                                    <th>Total</th>
                                    </tr>
                            </thead>
                            <tbody id="tb_reporte_noprogramados">
                                
                            </tbody>
                        </table>
                                    
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                </div>
            </div>
            <form id="form-reporte-actividades-tecnicos-hidden" action="repofsc/exceltecnicosproductividad" method="POST">
                <input type="hidden" name="data_reporte" id="data_reporte" />
            </form>
            </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
        <button type="button" class="btn btn-primary"><?php echo trans('main.Save') ?></button> -->
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->