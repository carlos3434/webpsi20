<!-- /.modal -->
<div class="modal fade" id="asistenciaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Registro de Asistencia</h4>
            </div>
            <div class="modal-body">
                <div> 
                <ul class="nav nav-tabs nav-modal">
                    <li class="active">
                        <a  href="#asistencia" data-toggle="tab">Tecnicos por Buckets</a>
                    </li>
                    <li>
                        <a href="#mapa" data-toggle="tab">Mapa Grupal, Ubicaciones Actual</a>
                    </li>
                    <li>
                        <a href="#sms" data-toggle="tab">Sms Grupal</a>
                    </li>
                </ul>

                <div class="tab-content ">
                    <div class="tab-pane active" id="asistencia">
                        <div class="box">
                            <div class="row">
                                <form id="form_asistencia" name="form_asistencia" action="" method="post">
                                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                                    <div class="form-group">
                                         <div class="box-body table-responsive" id="contenedor-registro-asistencia">
                                            
                                        </div><!-- /.box-body -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="mapa">
                        <div id="mapamodal" style="height: 400px;">
                            
                        </div>
                    </div>
                    <div class="tab-pane" id="sms">
                        <div class="box">
                            <div class="row">
                                <form id="form_sms" name="form_sms" action="" method="post">
                                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea class="form-control" placeholder="Escriba su Mensaje" rows="5" style="resize: none; margin-bottom: 10px;" id="txt_mensaje_texto"></textarea>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="btn btn-success" type="submit" name="" value="Enviar">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <div class="box-body table-responsive" id="contenedor-sms">
                                            <table id="t_sms_tecnicos" class="table table-bordered table-striped">
                                              <thead>
                                                  <tr>
                                                    <th>Bucket</th>
                                                    <th>Tecnicos</th>
                                                  </tr>
                                              </thead>
                                              <tbody id="tb_sms_tecnicos">

                                              </tbody>
                                              <tfoot>
                                                  <tr>
                                                    <th>Bucket</th>
                                                    <th>Tecnicos</th>
                                                  </tr>
                                              </tfoot>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_asistencia">Registrar Asistencia</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->