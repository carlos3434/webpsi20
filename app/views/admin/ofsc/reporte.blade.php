<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::style('css/admin/reporte.css') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.ofsc.js.reporte_ajax' )
    @include( 'admin.ofsc.js.reporte' )
    @include( 'admin.ofsc.js.reporte_ofsc_ajax' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Reportes OFSC
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Gestión OFSC</a></li>
                        <li class="active">Reportes OFSC</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="panel-group" id="accordion-filtros">
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">Ofsc Exportar Excel
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="form_excel_actividades" id="form_excel_actividades" method="POST" action="reporte/excelactividadesofsc">
                           {{Form::token() }}
                           <div class="row form-group">
                               <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Buckets</label>
                                          <select class="form-control" name="bucket[]" id="bucket" multiple="" required="">
                                              <option value="">.::Seleccione::.</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                           <label class="titulo">Tipo Rango de Búsqueda</label>
                                            <select class="form-control" name="rango_busqueda" id="rango_busqueda" simple>
                                                <option value="">.::Seleccione::.</option>
                                                <option value="1">Desde Hoy en Adelante</option>
                                                <option value="2">Hasta Máximo Ayer</option>
                                           </select>
                                      </div>
                                   <div class="col-sm-3"> 
                                        <label class="titulo">F. Agenda:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input disabled type="text" class="form-control pull-left"  name="fecha_agenda" id="fecha_agenda"/>
                                            </div>
                                    </div>
                               </div>
                           </div>
                           </form>
                        </div>
                        <div class="panel-footer">
                        <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-3 pull-rigth ">
                                        <a class="btn btn-success excel-actividades">Exportar XML Actividades Ofsc</a>
                                    </div>
                                    <div class="col-sm-3 pull-rigth ">
                                        <a class="btn btn-success reporte-actividades-tecnicos">Actividades vs Tecnicos</a>
                                    </div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseOne">Descargas Actividades Historico 
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="box-body table-responsive">
                                    <table id="t_reporte_actividades_ofsc_historico" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>F. Generacion</th>
                                                <th>Bucket</th>
                                                <th>[]</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_reporte">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>F. Generacion</th>
                                                <th>Bucket</th>
                                                <th>[]</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Filtros</h3>
                                </div>
                                <form id="form_reporte" name="form_reporte" method="POST" action="reporte/repofscexcel">
                                <div class="col-md-12">
                                    <div class="col-sm-3">
                                         <label>Tipo de Reporte:</label>
                                         <select class="form-control" name="slct_tipo_reporte" id="slct_tipo_reporte">
                                             <option value="0">.::Seleccione::.</option>
                                             <option value="messages" selected>Messages</option>
                                         </select>
                                     </div>
                                     <div class="col-sm-3"> 
                                        <label class="titulo">Fecha:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-left" readonly="" name="fecha_timefrom" id="fecha_timefrom"/>
                                                <div class="input-group-addon" onclick="cleandate()" style="cursor: pointer">
                                                    <i class="fa fa-rotate-left" ></i>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <br>
                                        <button class="btn btn-success" type="button" id="btnBuscar">Buscar Reporte</button>
                                    </div>
                                </div>

                                <div class="box-body table-responsive">
                                    <table id="t_reporte" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cod Actu</th>
                                                <th>AID</th>
                                                <th>ID Mensaje</th>
                                                <th>Descripción</th>
                                                <th>Respuesta</th>
                                                <th>Resultado</th>
                                                <th>Fecha</th>
                                                <th class="editarG"> <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                                 </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_reporte">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Cod Actu</th>
                                                <th>AID</th>
                                                <th>ID Mensaje</th>
                                                <th>Descripción</th>
                                                <th>Respuesta</th>
                                                <th>Resultado</th>
                                                <th>Fecha</th>
                                                <th class="editarG">
                                                <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                </form>
                            </div>  --><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.ofsc.form.reporte' )
     @include( 'admin.ofsc.form.reporte-actividades-tecnicos-modal' )
@stop