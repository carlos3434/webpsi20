<script type="text/javascript">
	ids = [];
	$(document).ready(function(){
		GLOBAL.data_usuario = {usuario: 1};
		GLOBAL.data_bandeja = {bandeja: 1};
		$('#fecha_agenda').daterangepicker({format: 'YYYY-MM-DD'  });
		$('#fecha_agenda').val(
			moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
			 moment(new Date()).format("YYYY-MM-DD")
	   	);
		$(".mostrar-tecnicos").click(function(){ Programacion.listar(); });
		$("#t_programacion_tecnico").dataTable({paging: false, lengthChange: false, searching: false, ordering: true, sort: true,  
			info: false,  autoWidth: true,});
		 slctGlobal.listarSlct('empresa','slct_empresa','multiple',ids,GLOBAL.data_usuario,0,'','E');
		 slctGlobal.listarSlct('actividad','slct_actividad','multiple', ids, GLOBAL.data_bandeja, 0, '#slct_actividad_tipo','A');
		slctGlobal.listarSlct('quiebre','slct_quiebre','multiple',ids,GLOBAL.data);
		slctGlobal.listarSlct('tecnico','slct_tecnico','multiple',ids,0,1);
		$(document).delegate(".panel-group#accordion-resultados table tbody tr td a.codigoactuaciones", "click", 
			function(){
				console.log($(this).attr("data-codactu"));
				modalActividadMovimientos($(this));
			});
	});
	
	htmlCabeceraDias = function (data, div) {
		htmlcabeceradias = "";
		htmlcabeceradias+="<tr>";
		for (var i in data.cabecera){
			htmlcabeceradias+="<th></th>";
		}
		for (var k in data.dias){
			htmlcabeceradias+="<th colspan='2'>"+moment(data.dias[k]).format('ll')+"</th>";
		}
		htmlcabeceradias+="</tr>";
		div.html(htmlcabeceradias);
	};
	htmlCabecera = function(data, div, append){
		htmlcabecera = "<tr>";
	      	for(var i in data.cabecera)
	      		htmlcabecera+="<th>"+data.cabecera[i]+"</th>";
	      	for(var k in data.dias){
	      		for (var j in data.subcabecera)
	      		htmlcabecera+="<th>"+data.subcabecera[j]+"</th>";
	      	}
	      	htmlcabecera+="</tr>";
	      	if (append === true)
	      		div.append(htmlcabecera);
	      	else
			div.html(htmlcabecera);
	};

	htmlCuerpo = function (data, div, dias, filadias){
		htmlcuerpo = "";
		for (var i in data){
			rowspan= 0;
			for (var j in data[i]){
				rowspan++;
			}
			htmlcuerpo+=htmlOrdenes(i, data[i], rowspan, dias, filadias);
			
		}
		div.html(htmlcuerpo);
	};

	htmlOrdenes = function (key, data, rowspan, dias, filadias) {
		console.log(filadias);
		htmlordenes = "";
		if (rowspan <2) {
			for (var i in data){
			htmlordenes+="<tr>";
			htmlordenes+="<td>"+key+"</td>";
			htmlordenes+="<td>"+data[i][0].nombre_quiebre+"</td>";
			htmlordenes+=htmlDiasHorario(data[i], dias, filadias, i);
			htmlordenes+="</tr>";
			}
		}else {
			cont = 0;
			for (var j in  data){
				if (cont === 0) {
					htmlordenes+="<tr>";
					htmlordenes+="<td rowspan='"+rowspan+"'>"+key+"</td>";
					htmlordenes+="<td>"+data[j][0].nombre_quiebre+"</td>";
					htmlordenes+=htmlDiasHorario(data[j], dias, filadias, j);
					htmlordenes+="</tr>";
				} else {
					htmlordenes+="<tr>";
					htmlordenes+="<td>"+data[j][0].nombre_quiebre+"</td>";
					htmlordenes+=htmlDiasHorario(data[j], dias, filadias, j);
					htmlordenes+="</tr>";
				}
				
				cont++;
			}
		}
		return htmlordenes;
	};

	htmlDiasHorario = function (ordenes, dias, filadias, idhorario) {
		
		ordenesporfecha = [];
		for (var i in dias){
			ordenesporfecha[dias[i]] = [];
		}
		for (var k in ordenes) {
			keyfechaagenda = ordenes[k].fecha_agenda;
			ordenesporfecha[keyfechaagenda].push(ordenes[k]);
		}
		htmldiashorario = "";
		for (var j in filadias){
			
			if (typeof ordenesporfecha[j]!=="undefined"){
				if (ordenesporfecha[j].length > 0) {
					console.log("hay data");
					console.log(idhorario);
					html = "<ul>";
					for (var k in ordenesporfecha[j]) {
						html+="<li><a class='"+ordenesporfecha[j][k].estado_ofsc_name+"' data-codactu='"+ordenesporfecha[j][k].codactu+"'>"+ordenesporfecha[j][k].codactu+"</a></li>";
					}
					html+="</ul>";
					if (idhorario === "51"){
						htmldiashorario+="<td> "+html+"</td><td></td>";
					}

					if (idhorario === "52"){
						htmldiashorario+="<td></td><td> "+html+"</td>";
					}
				}else {
					htmldiashorario+="<td></td><td></td>";
				}
			}else{
				htmldiashorario+="<td></td><td></td>";
			}
		}
		return htmldiashorario;
	};

	htmlGrilla = function (data) {
		$("#t_programacion_tecnico tbody").html("");
		htmlCabeceraDias(data, $("#t_programacion_tecnico thead"));
		htmlCabecera(data, $("#t_programacion_tecnico thead"), true);
	      	htmlCabecera(data, $("#t_programacion_tecnico tfoot"), false);
	      	htmlCuerpo(data.ordenes_agrupado, $("#t_programacion_tecnico tbody"), data.dias, data.htmlfiladia);
	};

	htmlListadoAcordion = function (data) {
		cont = 1;
		html = "<div class='panel-group' id='accordion-resultados'>";
		for (var j in data.ordenes_agrupado) {
			html+=htmlFilaAcordion(data.ordenes_agrupado[j], j, cont);
			
			cont++;
		}
		html+="</div>";
		$(".contenedor-programacion").html(html);
		cont = 1;
		for (var k in data.ordenes_agrupado) {
			$("#t_programacion_tecnico"+cont).dataTable({ paging: true, lengthChange: false, searching: true,
			                ordering: true, sort: true,  info: false, autoWidth: true,  order: [[ 0, "desc" ]]});
			cont++;
		}
		
	};

	htmlFilaAcordion = function (data, j, cont) {
		html = "";
		html+="<div class='panel panel-default'>";
		html+="<div class='panel-heading box box-primary'>";
                        	html+="<h4 class='panel-title'>";
                            	html+="<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion-resultados' href='#collapse"+cont+"'>";
                             html+="Reporte de Actividades del Día : "+moment(j).format('ll')+"</a></h4></div>";
                             html+="<div id='collapse"+cont+"' class='panel-collapse collapse'>";
                             html+="<div class='panel-body'>";
                             html+="<div class='box-body table-responsive'>";
                             html+="<table id='t_programacion_tecnico"+cont+"' class='table table-bordered table-striped col-sm-12 responsive'  width='100%'>";
                             html+="<thead>";
                             html+="<tr>";
                             html+="<th>Técnico</th><th>Quiebre</th><th>Ult. Actualización</th><th>Slot</th><th>CodActu</th>";
                             html+="</tr>";
                             html+="</thead>";
                             html+="<tbody>";
                             html+=htmlListadoCuerpo(data);
                             html+="</tbody>";
                             html+="<tfoot>";
                             html+="<tr>";
                             html+="<th>Técnico</th><th>Quiebre</th><th>Ult. Actualización</th><th>Slot</th><th>CodActu</th>";
                             html+="</tr>";
                             html+="</tfoot>";
                             html+="</table>";
                             html+="</div>";
                             html+="</div>";
                             html+="</div>";
		html+="</div>";
		return html;
	};

	htmlListadoCuerpo = function (data){
		html = "";
		for (var k in data) {
			slot = "";
			if (k === "51")
				slot="AM";
			if (k === "52")
				slot = "PM";
			ordenes = ordenarporCodigoActuacion(data[k]);
			for (var m in ordenes){
				console.log(ordenes[m]);
				for (var x in ordenes[m]){
					html+= "<tr><td>"+ordenes[m][x].nombre_tecnico+"</td><td>"+ordenes[m][x].nombre_quiebre+"</td><td>"+ordenes[m][x].as+"</td><td>"+slot+"</td><td><a class='codigoactuaciones "+ordenes[m][x].estado_ofsc_name+"' data-codactu='"+ordenes[m][x].codactu+"' data-movimientos='"+JSON.stringify(ordenes[m])+"' data-xorden='"+ordenes[m][x].x_orden+"' data-yorden='"+ordenes[m][x].y_orden+"' >"+ordenes[m][x].codactu+" - "+ordenes[m][x].estado_ofsc+"</a></td></tr>";
				}
				
			}
				
		}

		return html;
	};

	ordenarporCodigoActuacion = function (data) {
		ordenes = [];
		for (var p in data){
			if (typeof ordenes[data[p].codactu]!=="undefined")
				ordenes[data[p].codactu].push(data[p]);
			else {
				ordenes[data[p].codactu] = [];
				ordenes[data[p].codactu].push(data[p]);
			}
		}
		return ordenes;
	};

	modalActividadMovimientos = function (orden){
		codactu = orden.attr("data-codactu");
		movimientos = orden.attr("data-movimientos");
		x_orden = orden.attr("data-xorden");
		y_orden = orden.attr("data-yorden");
		if (movimientos!=="")
			movimientos = JSON.parse(movimientos);
		if (movimientos.length > 0 ){
			html = "";
			for (var g in movimientos) {
				html+="<tr>";
				html+="<td>"+movimientos[g].gestion_movimiento_id+"</td>";
				html+="<td>"+codactu+"</td>";
				html+="<td>"+movimientos[g].as+"</td>";
				html+="<td>"+movimientos[g].nombre_quiebre+"</td>";
				html+="<td>"+movimientos[g].empresa+"</td>";
				html+="<td>"+movimientos[g].fecha_agenda+"</td>";
				html+="<td>"+movimientos[g].nombre_tecnico+"</td>";
				html+="<td>"+movimientos[g].estado_ofsc+"</td>";
				html+="</tr>";
			}
			$("#t_codactu tbody").html(html);
		}
		$("#t_codactu").dataTable();
		
		$("#codactuModal").modal("show");
		//initMaps(y_inicio,x_inicio,coord_y,coord_x,'codactu_modal_mapObjectMulti3');
		$("#codactu_modal_mapObjectMulti3").html("");
		eventoCargaMostrar();
		setTimeout(function(){
			geoStreetView(y_orden, x_orden, 'codactu_modal_mapObjectMulti3');
			eventoCargaRemover();
		}, 6000);

		
		
	};
	
</script>