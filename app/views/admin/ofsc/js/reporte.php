<script type="text/javascript">
    //js
$(document).ready(function() {

    $('#fecha_timefrom').daterangepicker(
    {
        format: 'YYYY-MM-DD'
    });

    $("#fecha_agenda").daterangepicker({
         format: 'YYYY-MM-DD',
        endDate: moment().subtract(1, 'days').format("YYYY-MM-DD"),
        startDate: moment().subtract(8, 'days').format("YYYY-MM-DD"),
    });

    $('#fecha_agenda').val(
        moment().subtract(8, 'days').format("YYYY-MM-DD") + ' - ' +
        moment().subtract(1, 'days').format("YYYY-MM-DD")
    );

     $('#fecha_timefrom').val(
        moment().subtract(1, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );

    $("#btnBuscar").click(function() {
        var tipo=$("#slct_tipo_reporte").val();

        if(tipo!=='' || tipo!==null){
           Reporte.CargarReportes(tipo);
        } else {
            alert ("Seleccione un Reporte");
        }
    });
    slctGlobalHtml("rango_busqueda", "simple");
    slctGlobalHtml("bucket", "simple");

    $('#reporteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); 
        var titulo = button.data('titulo'); 
        var reporte_id = button.data('id'); 
        var tipo=button.data('tipo'); 

        var modal = $(this); //captura el modal
        modal.find('.modal-title').text(titulo+' '+tipo);

        if(tipo=='messages'){
           $('#form_repor_message').css("display","");
    
            $('#form_repor_message #txt_codactu').val( reporteObj[reporte_id].appt_ext_id );
            $('#form_repor_message #txt_aid').val( reporteObj[reporte_id].appt_id );
            $('#form_repor_message #txt_idmensaje').val( reporteObj[reporte_id].mqid);
            $('#form_repor_message #txt_descripcion').val( reporteObj[reporte_id].scenario_step );
            $('#form_repor_message #txt_respuesta').val( reporteObj[reporte_id].result );
            $('#form_repor_message #txt_fecha').val( reporteObj[reporte_id].timefrom);
            var form='form_repor_message';
            Reporte.CargarMessagesText();
        }
        
    });
    

    $('#reporteModal').on('hide.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body input').val('');
        $('#form_repor_message #slct_tipoevento').val(2);
         $('#form_repor_message #id,#form_repor_message #tipotabla').remove();
    });

    $(".excel-actividades").click(function(){
        $("#form_excel_actividades").submit();
        return false;
    });

     $(document).delegate("ul[data-select=rango_busqueda] li a input[type=radio]", "change", function(event){
        console.log($(this).val());
        if ($(this).val() == 2){
            $("#fecha_agenda").removeAttr("disabled");
        } else {
            $("#fecha_agenda").prop("disabled", true);
        }
    });
     $("#t_reporte_actividades_ofsc_historico").DataTable().destroy();
   Reporte. listarActividadesHistoricos();
});

cleandate=function(){
        $('#fecha_timefrom').val('');
};

descargarReporte=function(){
    $("#form_reporte").append("<input type='hidden' name='totalPSI' id='totalPSI' value='1'>");
    $("#form_reporte").submit();
    $("#form_reporte #totalPSI").remove();
};

HTMLCargarEventos=function(datos,tipo){
    var html="";

    $('#t_reporte').dataTable().fnDestroy();

    $.each(datos,function(index,data){//UsuarioObj
        if(tipo=='messages') {
        estadohtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#reporteModal" \n\
                    data-id="'+index+'" data-titulo="Ver" data-tipo="'+tipo+'" ><i class="fa fa-eye fa-lg"></i> </a>';

         html+="<tr>"+
            "<td>"+data.appt_ext_id+"</td>"+
            "<td>"+data.appt_id+"</td>"+
            "<td>"+data.mqid+"</td>"+
            "<td>"+data.scenario_step+"</td>"+
            "<td>"+data.result+"</td>"+
            "<td>"+data.result_desc+"</td>"+
            "<td>"+data.timefrom+"</td>"+
            "<td>"+estadohtml+"</td>";

        html+="</tr>";
        }
    });
    $("#tb_reporte").html(html);
  
    activarTabla();
};

activarTabla=function(){
    //$("#t_reporte").dataTable();

     $('#t_reporte thead th').each( function () {
        var title = $('#t_reporte thead th').eq( $(this).index() ).text();
        if (title=='Cod Actu' ) {
            $(this).html( '<input id="filter_codactu" type="text" placeholder="Buscar '+title+'" />' );
        } else if ( title=='AID'){
            $(this).html( '<input id="filter_aid" type="text" placeholder="Buscar '+title+'" />' );
        }
    } );

    $("#t_reporte").dataTable();
    $( '#filter_codactu').on( 'keyup change', function () {
        $('#t_reporte').DataTable().column( 0 ).search(
            $( '#filter_codactu').val(),
            true,
            true
        ).draw();
    } );
    $(  '#filter_aid' ).on( 'keyup change', function () {
        $('#t_reporte').DataTable().column( 1 ).search(
            $( '#filter_aid').val(),
            true,
            true
        ).draw();
    } );


};
</script>
