<script type="text/javascript">
	GLOBAL = {};
	$("[name=fecha_registro], [name=fecha_movimiento]").daterangepicker({ format: 'YYYY-MM-DD' });
	$("[name=fecha_registro], [name=fecha_movimiento]").val(
		moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
		 moment(new Date()).format("YYYY-MM-DD")
   	);

	OrdenesOfsc = {
		listar: function(){
			GLOBAL.table_enviosofsc  = $("#t_enviosofs").DataTable({
			"processing": true,
		    "serverSide": true,
		    "stateSave": true,
		    "stateLoadCallback": function (settings) {
		        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
		    },
		    "stateSaveCallback": function (settings) { 
		        $(".overlay,.loading-img").remove();
		        $('#t_enviosofs').removeAttr("style");
		    },
		    "ajax": {
		        url: "ordenesofscmovimientos/listar",
		        type: "POST",
		        data: function (d) {
		            var contador = 0;
		            datos =  FormSerialize($("#filtro_envios_ofsc"));
		            for (var i = datos.length - 1; i >= 0; i--) {
		                if (datos[i].split("[]").length > 1) {
		                    d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
		                    contador++;
		                 } else {
		                    d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
		                 }
		            }
		            if ($("#collapse2").css("display") == "none") {
		                d.codigo_actuacion = "";
		            }
		            d.bandeja = 1;
		            d.usuario = 1;
		            d.withLimit = 1;
		            d.fecha_registro_psi ="";
		        }
		    },
		    "columns":[
		        { data: 'codactu', name:'codactu'},
		        { data: 'created_at', name:'created_at'},
		        { data: 'empresa', name:'empresa'},
		        { data: 'quiebre', name:'quiebre'},
		        { data: 'nodo', name:'nodo'},
		        { data: 'resource_id_anterior', name:'resource_id_anterior'},
		        { data: 'resource_id_nuevo', name:'resource_id_nuevo'},
		        {data: 'start_time_anterior', name:'start_time_anterior'},
		        {data: 'start_time_nuevo', name:'start_time_nuevo'},
		        {data: 'mensaje', name: 'mensaje'}
		    ],
		    "columnDefs": [
		        {
		            "targets": 9,
		            "orderable": false,
		            "searchable": false
		        },
		    ],
		        paging: true,
		        lengthChange: false,
		        searching: false,
		        ordering: true,
		        order: [[ 0, "desc" ]],
		        info: true,
		        autoWidth: true,
		        language: config
		    });
			return GLOBAL.table_enviosofsc;
		},
	};
</script>