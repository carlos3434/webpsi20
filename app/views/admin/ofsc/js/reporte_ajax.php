<script type="text/javascript">
    var reporte_id, reporteObj;
    var Reporte = {
        CargarReportes: function (tipo) {

            var datos = $("#form_reporte").serialize().split("txt_").join("").split("slct_").join("");

            $.ajax({
                url: 'repofsc/cargar'+tipo,
                type: 'POST',
                cache: false,
                dataType: 'json',
                data:datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    var html = "";
                    var estadohtml = "";
                    if (obj.rst == 1) {
                        HTMLCargarEventos(obj.datos,tipo);
                        reporteObj = obj.datos;
                    }
                },
                error: function () {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente', 5000);
                }
            });
        },

        CargarMessagesText: function () {

            var datos = $("#form_repor_message").serialize().split("txt_").join("").split("slct_").join("");

            $.ajax({
                url: 'repofsc/cargarmessagestext',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data:datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    
                     $.each(obj,function(index,data){
                        $('#form_repor_message #txt_asunto').val( data.mcsubject);

                        $('#form_repor_message #txt_mensaje').val( data.mcbody );

                     });
                },
                error: function () {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente', 5000);
                }
            });
        },

        listarActividadesHistoricos: function(){
             return $("#t_reporte_actividades_ofsc_historico").DataTable({
            "processing": true,
                        "serverSide": true,
                        "stateSave": true,
                        "stateLoadCallback": function (settings) {
                             $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) { 
                            $(".overlay,.loading-img").remove();
                            //$('#t_reporte_actividades_ofsc_historico').removeAttr("style");
                        },
                        "ajax": {
                            url: "repofsc/actividadesofschistorico",
                            type: "POST",
                        },
                        "columns":[
                           { data: "id",  name:'codactu'},
                            { data: 'created_at', name:'created_at'},
                            { data: "resource_id",  name:'resource_id'},
                            { data: function ( row, type, val, meta) {
                                form = "<form method='post' action='reporte/excelactividadesofscindividual'>";
                                form+="<input type='hidden' name='id' value='"+row.id+"' />";
                                form+="<input type='hidden' name='dataJson' value='true' />";
                                form+="<input type='submit' value='Descargar' class='btn btn-success' />";
                                form+="</form>"; 
                                return form;
                                }
                            }
                        ],
                        "columnDefs": [
                            {
                                "targets": 2,
                                "orderable": false,
                                "searchable": false
                            },
                        ],
                        paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0, "desc" ]],
                        info: true,
                        autoWidth: true,
                    });
        },

        generarReporteActividadesTecnicos: function()
        {
            $.ajax({
                url: 'repofsc/tecnicosproductividad',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data:$("#form_excel_actividades").serialize(),
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                       data = obj.data;
                        fechas = obj.fechas;
                        tecnicos = obj.tecnicos;
                        estados = obj.estados;
                        totalesdias = obj.totalesdias;
                        noprogramados = obj.noprogramados;

                        pintarCabecera("th_reporte_actividades_ofsc_historico", fechas);
                        pintarCuerpo("tb_reporte_actividades_ofsc_historico", estados, tecnicos, data, totalesdias);

                        pintarNoProgramado("tb_reporte_noprogramados",noprogramados, tecnicos);
                        $("#data_reporte").val(JSON.stringify(obj));
                        $("#reporte-actividades-tecnicos").modal("show");
                    }
                    if (obj.rst == 2) {
                        Psi.mensaje('danger', obj.msj, 5000);
                    }
                    
                },
                error: function () {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente', 5000);
                }
            });
        }
        
    };
</script>