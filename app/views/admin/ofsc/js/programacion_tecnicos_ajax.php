<script type="text/javascript">
	GLOBAL = {};
	Programacion = {
	   listar: function (){
	   	$('#t_programacion_tecnico').dataTable().fnDestroy();
	   	eventoCargaMostrar();
	   	datos =  FormSerialize($("#form_programacion"));
	   	d = [];
	   	contador = 0;
		 for (var i = datos.length - 1; i >= 0; i--) {
		        if (datos[i].split("[]").length > 1) {
		              d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
		                 contador++;
		 } else {
		           d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
		          }
		}
		datos = "";
		for (i in d) {
			datos+=i+"="+d[i]+"&";
		}
	      programacion = functionajax("programaciontecnicoofsc/programacion",  "POST", false, "json", false, datos );
	      programacion.success(function(data){
	      	if (data.rst === 1) {
	      		if (data.ordenes_agrupado!="undefined") {
	      			
	      			//htmlGrilla(data);
	      			//$("#t_programacion_tecnico").dataTable();
	      			htmlListadoAcordion(data);
	      			eventoCargaRemover();
	      		}
	      		eventoCargaRemover();
	      	}
	      });
	   },
	};
</script>