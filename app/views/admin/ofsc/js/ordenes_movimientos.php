<script type="text/javascript">
	$(document).ready(function() {
		 localStorage.removeItem("bandejaMasivoActivo");
    		 localStorage.removeItem("bandejaBusqueda");
		ids = [];
		GLOBAL.data_usuario = {usuario: 1};
		 GLOBAL.data_bandeja = {bandeja: 1};
		/** cargando combos **/
		 eventoCargaMostrar();
		 slctGlobal.listarSlct('empresa','slct_empresa','multiple',ids,GLOBAL.data_usuario,0,'','E');
		 slctGlobal.listarSlct('actividad','slct_actividad','multiple', ids, GLOBAL.data_bandeja, 0, '#slct_actividad_tipo','A');
		 slctGlobal.listarSlct('actividadtipo','slct_actividad_tipo','multiple', ids, 0, 1);
		 slctGlobal.listarSlct('nodo','slct_nodo','multiple');
		 slctGlobal.listarSlct('estado','slct_estado_psi','multiple');
		 slctGlobal.listarSlct('estadoofsc','slct_estado_ofsc','multiple');

    	slctGlobal.listarSlct('quiebre','slct_quiebre','multiple',ids,GLOBAL.data_usuario);

		 eventoCargaRemover();
		 $("#buscar-ordenes").click( function(e){
		 	$("#t_enviosofs").DataTable().destroy();
			GLOBAL.table_enviosofsc = BusquedaOrdenesOfsc();
			e.preventDefault();
		});
		 $("#filtro_envios_ofsc .fa-download").click(function(e){
		 	ExportarOrdenesOfsc();
		 });

		 $("#buscar-ordenes-masivas").click( function(e){
			BusquedaOrdenesOfscMasivo();
			e.preventDefault();
		});
		 $("#filtro_envios_masivo_ofsc  .fa-download").click(function(e){
		 	ExportarOrdenesMasivoOfsc();
		 });
		  $(".btn-activar-masivo").click(function(e){
		        activarEnvioMasivo($(this));
		    });
		  $(".btn-ejecutar-masivo").click(function(e){
		        envioMasivoOFSC();
		    });
	});

	BusquedaOrdenesOfsc = function(){
		tablaEnvioOfsc = OrdenesOfsc.listar();
		return tablaEnvioOfsc;
	};

	FormSerialize  = function (form) {
		return  form.serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
	};
	BusquedaOrdenesOfscMasivo = function(){
		$("#t_enviosofs_masivo").DataTable().destroy();
		tablaEnvioMasivoOfsc = OrdenesOfscMasivo.listar();
		return tablaEnvioMasivoOfsc;
	};
	BusquedaOrdenesOfsc();
</script>