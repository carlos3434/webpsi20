<script type="text/javascript">
var fecha_agenda="";
var horario_agenda="";
var dia_agenda="";
var hora_agenda="";
var evento;
var Agenda={

    /**
     * Mostrar rorario de tecnico a gestionar
     * @param  array  parametros, compuesto de zona y empresa
     * 
     * @return html
     * 
     * ejemplo: Agenda.show({zona:'8',empresa:'4'});
     * en la vista <div id="html" class="box-body table-responsive"></div>
     */
    show:function(id,parametros, clickHorario){
        $.ajax({
            url         : 'agenda/libre',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : parametros,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    $('#'+id).html(obj.html);
                    evento=clickHorario;
                    $('#'+id).after(Agenda.after);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },

    /**
     * ejecutar al seleccionar horario, se establecen variables globales
     * fecha_agenda "2015-03-09"
     * horario_agenda "3"
     * dia_agenda "09"
     * hora_agenda "14pm - 16pm"
     */
    after:function(){

        $("#horario td").click(function(){
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            var color = $(this).css("background-color");

            //para IE8 ya que toma el color como lo pone
            if(color.indexOf('#')!=-1){
                color = color;
            }else{
                color = hexcolor(color);
            }

            id_celda = $(this).attr("title");

            //horario_celda = document.getElementById("horario").getElementsByTagName("td")[id_celda];
            horario_celda = $(this)
            //totales = horario_celda.getAttribute("data-total");
            totales=$(this).data("total");
            if(color!="#ff0000" && color!="#f0e535" && color!="#49afcd" && totales>0){
                //variables globales
                fecha_agenda=$(this).data("fec");
                horario_agenda=$(this).data("horario");
                dia_agenda=$(this).data("dia");
                hora_agenda=$(this).data("hora");

                if (evento !== null && evento !='null' && evento !== undefined && evento !='undefined') {
                    hf_local=hf;
                    evento(horario_celda);
                    if (hf===undefined || hf==='undefined' || hf==='') {
                        hf=hf_local;
                        $(".overlay,.loading-img").remove();
                        return;
                    }
                }
                
                $("#horario td").each(function(){
                    color = $(this).css("background-color");
                    if(color.indexOf('#')!=-1){
                        color = color;
                    }else{
                        color = hexcolor(color);
                    }
                    if(color!="#ff0000" && color!="#f0e535" && color!="#49afcd"){
                        $(this).css({"background":"","color":""});
                    }
                });

                $(this).css({"background":"#5cb85c","color":"#fff"});

                $(".horario .help-inline").css("display","none");
                $(".fecha_error").html("").css("display","none");
            }
            $(".overlay,.loading-img").remove();
        });

        hexcolor=function(colorval) {
            var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            delete(parts[0]);
            for (var i = 1; i <= 3; ++i) {
                parts[i] = parseInt(parts[i]).toString(16);
                if (parts[i].length == 1) parts[i] = '0' + parts[i];
            }
            color = '#' + parts.join('');
            return color;
        };
        
    }
};
</script>
