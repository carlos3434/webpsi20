<script type="text/javascript">
var slctGlobal={
    /**
     * mostrar un mulselect
     * @param:
     * 1 controlador..nombre del controlador   modulo
     * 2 slct         nombre del multiselect   slct_modulos
     * 3 tipo         simple o multiple
     * 4 valarray     valores que se seleccionen
     * 5 data         valores a enviar por ajax
     * 6 afectado     si es afectado o no (1,0)
     * 7 afectados    a quien afecta (slct_submodulos)
     * 8 slct_id      identificador que se esta afectando ('M')
     * 9 slctant
     * 10 slctant_id
     * 11 funciones   evento a ejecutar al hacer hacer changed
     *
     * @return string
     */

    listarSlct:function(controlador,slct,tipo,valarray,data,afectado,afectados,slct_id,slctant,slctant_id, funciones){
        $.ajax({
            url         : controlador+'/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    htmlListarSlct(obj,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones);
                    if (funciones!=='' && funciones!==undefined) {
                        if (funciones.success!=='' && funciones.success!==undefined) {
                            funciones.success(obj.datos);
                        }
                    }
                }
            },
            error: function(){
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    listarSlctpost:function(controlador,funcion,slct,tipo,valarray,data,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden){
        $.ajax({
            url         : controlador+'/'+funcion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    htmlListarSlct(obj,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden);
                    if (funciones!=='' && funciones!==undefined) {
                        if (funciones.success!=='' && funciones.success!==undefined) {
                            funciones.success(obj.datos);
                        }
                    }
                    if(obj.coords){
                        var map = new google.maps.Map(document.getElementById('map_canvas2'), {
                            zoom: 16,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        if(obj.coords.length == 1){
                            yactu = obj.coords[0].coord_y;
                            xactu = obj.coords[0].coord_x;

                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(yactu, xactu),
                                map: map,
                                title: 'Hello World!'
                            });

                            marker.setMap(null);
                            marker.setPosition(
                                new google.maps.LatLng(
                                    yactu,
                                    xactu
                                )
                            );
                            marker.setMap(map);

                            map.setCenter(new google.maps.LatLng(yactu, xactu));
                            map.setZoom(16);
                            var updstreet = geoStreetView(yactu, xactu, 'street_canvas2');
                        }else {
                            var polygons = [];
                            //Coordenadas
                            var poli_array = Psigeo.objToLatLngArray(obj.coords);

                            var color = Psi.color_aleatorio();

                            //Dibujar poligono
                            var poligono = Psigeo.poligono(poli_array, color, 3, color, 0.25);
                            poligono.setMap(map);

                            polygons.push(poligono);

                            //Limites del mapa
                            var updbounds = new google.maps.LatLngBounds();

                            $.each(poli_array, function () {
                                updbounds.extend(this);
                            });

                            map.fitBounds(updbounds);
                        }
                    }
                }
            },
            error: function(){
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    listarSlctFijo:function(controlador,slct){
        $.ajax({
            url         : controlador+'/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                var html="";
                html += "<option value='0'> .::Seleccione::. </option>";
                if(obj.rst==1){
                    $.each(obj.datos,function(index,data){
                            html += "<option value=\"" + data.id + "\">" + data.nombre + "</option>";
                    });
                }
                $("#"+slct).html(html);
            },
            error: function(){
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },

    listarSlctMasivo: function (controladores, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones){
        if(controladores.length > 0){
            //console.log(data);
            listado = functionajax('listado/process', 'POST', false, 'json', false, {controladores: JSON.stringify(controladores), slcts: JSON.stringify(slcts), data: JSON.stringify(data)});
            listado.success(function(obj){
                //console.log(obj);
                j = 0;
                //console.log(obj.resultado.length);
                for(i = 0; i <obj.resultado.length; i++){
                    //console.log(i);
                    //console.log(slcts[j]);
                    obj.datos = JSON.parse(obj.resultado[i]).datos;
                    //console.log(obj.datos);
                    if(typeof slcts[j] !="undefined") {
                       htmlListarSlct(obj, slcts[j],tipo[i],valarray[j], afectado[j], afectados[j], slct_id[j]);
                        if (funciones!=='' && funciones!==undefined) {
                            if (funciones.success!=='' && funciones.success!==undefined) {
                                funciones.success(obj.datos);
                            }
                        } 
                    }
                    
                    j++;
                }
                //$("#usuarioModal").modal("show");
            });
        }
    }
};
</script>
