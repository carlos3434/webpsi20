<script type="text/javascript">
    var Map={
        generateKML:function(markers){
            console.log(markers);
            var coordinates = [];
            var contents = [];
            for(var i = 0; i < markers.length; i++)
            {
                var lat = markers[i].getPosition().lat();
                var lng = markers[i].getPosition().lng();
                if(addressContent[i]){
                    contents.push(addressContent[i]);
                }
                coordinates.push({lat:lat,lng:lng});
            }
            $.ajax({
                url: 'georeferencia/generarkml',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async:false,
                data: {coordinates:coordinates,contents:contents},
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst == 1) {

                        var element = $('<a class="btn btn-default">Link de Descarga</a>');
                        var url = 'data:text/plain,' + encodeURIComponent(obj.data);

                        var popoverTemplate = ['<div class="popover" role="tooltip">',
                            '<div class="arrow"></div>',
                            '<h3 class="popover-title"></h3>',
                            '<div class="popover-content"></div>',
                            '</div>'].join('');
                        var date = new Date();
                        var stringDate = date.getDate() + "/"
                            + (date.getMonth() + 1) + "/"
                            + date.getFullYear() + "@"
                            + date.getHours() + ":"
                            + date.getMinutes() + ":"
                            + date.getSeconds();
                        var content = ['<a download="kml-' + stringDate + '.kml" href="' + url + '">Link de Descarga</a>'].join('');


                        $('#btn_kml').popover({
                            trigger: 'manual',
                            content: content,
                            title: "Descarga",
                            template: popoverTemplate,
                            placement: "rigth",
                            html: true,
                            delay: { "show": 500, "hide": 100 }
                        });
                        $("#btn_kml").popover("show");
                    }else{
                        Psi.mensaje('danger', obj.data, 6000);
                    }
                },error:function(){
                    $(".overlay,.loading-img").remove();
                    console.log("error");
                }
            });
        },
        uploadKML:function(map,form){
            var content = $("#kml_file").val();
            if(content.length)
            {
                var data = new FormData($('#'+form)[0]);

                $.ajax({
                    url: 'georeferencia/uploadkml',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    type: 'POST',
                    beforeSend: function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    }, success: function (data) {
                        $(".overlay,.loading-img").remove();
                        if(data.type === "point") {
                            Map.kmlToMap(data['coordinates'], data.type, map);
                        }else{
                            Map.kmlToMap(data.polygons, data.type, map);
                        }
                    }
                });
            }else{
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'No ha seleccionado ningun archivo', 6000);
            }
        },
        kmlToMap:function(coordinates, type,map){
            bounds = new google.maps.LatLngBounds();
            if(type === "point") {
                $.each(coordinates, function () {
                    var content;
                    for (var key in this.content) {
                        content = this.content[key];
                    }
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(this.lng, this.lat),
                        map: map,
                        animation: google.maps.Animation.DROP
                    });

                    var pt = new google.maps.LatLng(this.lng, this.lat);
                    bounds.extend(pt);
                    var infowindow = new google.maps.InfoWindow({
                        content: content
                    });

                    google.maps.event.addListener(marker, "click", function (event) {
                        infowindow.open(map, marker);
                    });
                });
            }else if(type === "polygon")
            {
                $.each(coordinates,function(){

                    for(var i = 0; i< this.coordinates.length ; i++){
                        var latlng = this.coordinates[i];
                        var pt = new google.maps.LatLng(latlng.lat, latlng.lng);
                        bounds.extend(pt);
                    }

                    var bermudaTriangle = new google.maps.Polygon({
                        paths: this.coordinates,
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35
                    });
                    bermudaTriangle.setMap(map);

                    var contentString = '<div>'+
                        '<h4>'+this.name+'</h4>'+
                        '</div>';

                    google.maps.event.addListener(bermudaTriangle,'click',function(event){
                        infowindow.setContent(contentString);
                        infowindow.setPosition(event.latLng);
                        infowindow.open(map);
                    });
                });
            }
            map.fitBounds(bounds);
        },
        preparePolygon:function(polygons,evento){
            var coordinates = new Array();
            var titles = new Array();
            $.ajax({
                url: 'georeferencia/generatekmlpolygon',
                type: 'POST',
                cache: false,
                dataType: 'json',
                async:false,
                data: {capas:polygons},
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst == 1)
                    {   
                       window.location=window.location.origin+'/'+obj.name;
                       evento(obj.name);
                    }
                }
            });
        }
    }
</script>