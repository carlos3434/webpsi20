<script type="text/javascript">
$(document).ready(function() {
    
});

htmlListarSlct=function(obj,slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden,placeholder){
    if(!Array.isArray(slct)) {
        slct=new Array(slct);
    }
    $.each(slct,function(ix,slct_){
        
    var html="";var disabled='';
    if(tipo!="multiple"){
        html+= "<option value=''>.::Seleccione::.</option>";
    }

    if(obj.rst==1){                    
        $.each(obj.datos,function(index,data){
        disabled=''; 
        rel=''; rel2='';rel3='';x='';y='';direccion='';
            if(data.block=='disabled'){ // validacion pra visualizacion
                disabled='disabled';
            }

            if( data.relation!='' && data.relation!=null ){
                rel='data-relation="|'+data.relation+'|"';
            }

            if( data.evento!='' && data.evento!=null ){
                rel2=' data-evento="|'+data.evento+'|"';
            }
            else  if ( $("#"+slct_).attr('data-evento')=='1' ) { 
                rel2=' data-evento="|1|"';
            }

            if( data.select!='' && data.select!=null ){
                rel3=' data-select="|'+data.select+'|"';
            }
            if (data.coord_x!='' && data.coord_x!=null) {
                x=' data-coord_x="'+data.coord_x+'" ';
            }
            if (data.coord_y!='' && data.coord_y!=null) {
                y=' data-coord_y="'+data.coord_y+'" ';
            }
            if (data.direccion!='' && data.direccion!=null) {
                direccion=' data-direccion="'+data.direccion+'" ';
            }
                        //si se recibe estado
            if (data.estado==1 && tipo=='multiple')
                html += "<option selected"+rel+rel2+x+y+direccion+" value=\"" + data.id + "\" "+disabled+">" + data.nombre + "</option>";
            else
                html += "<option "+rel+rel2+rel3+x+y+direccion+" value=\"" + data.id + "\" "+disabled+">" + data.nombre + "</option>";
            
        }); 
        
        if(slct_=='slct_estado'){
            html += "<option value=\"-1\">Temporal</option>";
        }
    }      
    $("#"+slct_).html(html);
    
    slctGlobalHtml(slct_,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden, placeholder);
    });
};

slctGlobalHtml=function(slct,tipo,valarray,afectado,afectados,slct_id,slctant,slctant_id, funciones,hidden,placeholder){
    $("#"+slct).multiselect({
        maxHeight: 200,             // max altura...
        filterPlaceholder: 'Buscar', //texto a mostrar
        enableCaseInsensitiveFiltering: true, // Insensitive
        buttonContainer: '<div class="btn-group col-xxs-12" />', // actualiza la clase del grupo
        buttonClass: 'btn btn-primary col-xxs-12', // clase boton
        templates: {
            ul: '<ul data-select="'+slct+'" class="multiselect-container dropdown-menu col-xxs-12"></ul>',
            li: '<li ><a tabindex="0"><label></label></a></li>'
        },
        includeSelectAllOption: true, //opcion para seleccionar todo
        selectAllText: 'Selecci&oacute;n Todo', //mostar texto espanol para seleccionar todo
        enableFiltering: true,    // activa filtro
        onDropdownShow: function() {
            if(afectado==1 && afectado!=null){
                $("[data-select='"+slct+"'] li").css('display','');
                $("[data-select='"+slct+"'] li.disabled").css('display','none');
            }
        },
        onDropdownHidden:function(){
            if($("#"+slct).attr("data-hidden")){
                hidden($("#"+slct).attr("data-hidden"));
            }
            var select=$("#"+slct+">option[value='"+$("#"+slct).val()+"']").attr('data-select');
            if(slct_id!='' && slct_id!=null && afectados!='' && afectados!=null){
                filtroSlct(slct,tipo,slct_id,afectados,slctant,slctant_id,select);
            }

            if( tipo!="multiple" && $("#"+slct+">option[value='"+$("#"+slct).val()+"']").attr('data-evento') ){
                eventoSlctGlobalSimple(slct,$("#"+slct+">option[value='"+$("#"+slct).val()+"']").attr('data-evento'));
            }
        },
        buttonText: function(options, select) { // para multiselect indicar vacio...
            if(tipo=="multiple"){
                if (options.length === 0) {
                    if(placeholder === 'ninguno'){
                        return '.::Ninguno::.';
                    }
                    return '.::Todo::.';
                }
                else if (options.length > 2) {
                    return options.length+' Seleccionados';//More than 3 options selected!
                }
                else {
                     var labels = [];
                     options.each(function() {
                         if ($(this).attr('label') !== undefined) {
                             labels.push($(this).attr('label'));
                         }
                         else {
                             labels.push($(this).html());
                         }
                     });
                     return labels.join(', ') + '';
                }
            }
            else{
                return $(options).html();
            }
        },
        onChange: function(option, checked, select) {
            if (funciones!=='' && funciones!==undefined) {
                if (funciones.change!=='' && funciones.change!==undefined) {
                    funciones.change($(option).val(), checked, $(option));
                }
            }
        },
        onSelectAll: function () {
            if (funciones!=='' && funciones!==undefined) {
                if (funciones.selectedall!=='' && funciones.selectedall!==undefined) {
                    funciones.selectedall($(this));
                }
            }
        },
        onDeselectAll: function () {
        }
    });
    if(valarray!=null && valarray.length>=1){

        if(afectado==1 && afectados!=null && afectados!='' && tipo!='multiple'){  // pre seleccion para simple y limpiar valores
            filtroSlct(afectados.split("|")[0],tipo,afectados.split("|")[2],afectados.split("|")[1],slctant,slctant_id,'',valarray);
        }
        else if (valarray=='all') {
            $('#'+slct).multiselect('selectAll', false);
            $('#'+slct).multiselect('updateButtonText');
            //$('#'*slct).multiselect('refresh');
        }
        else{
            $('#'+slct).multiselect('select', valarray);
            $('#'+slct).multiselect('refresh');
        }
            if( tipo!="multiple" && $("#"+slct+">option[value='"+$("#"+slct).val()+"']").attr('data-evento') ){
                eventoSlctGlobalSimple(slct,$("#"+slct+">option[value='"+$("#"+slct).val()+"']").attr('data-evento'));
            }
    }
    
    $("li.multiselect-all").removeAttr("data-select");
};

filtroSlct=function(slct,tipo,slct_id,afectados,slctant,slctant_id,select,valarray){
    $(afectados).multiselect('deselectAll', false);
    $(afectados).multiselect('updateButtonText');
    if(afectados.indexOf(',',10)>0){
        afectados=afectados.substr(0,afectados.indexOf(',',10));
    }
    detafectados=afectados.split(",");
    
    valores='||';
    valores2='';
    if( $("#"+slct).val()!=null ){
        if(tipo=="multiple"){
            if(slctant!=null && slctant!=''){
                valores2='|'+slctant_id+$("#"+slctant).val().join('|'+slctant_id)+'|';
            }
            valores='|'+slct_id+$("#"+slct).val().join('|'+slct_id)+'|';
        }
        else{
            if(slctant!=null && slctant!=''){
                valores2='|'+slctant_id+$("#"+slctant).val()+'|';
            }
            valores='|'+slct_id+$("#"+slct).val()+'|';
        }
        
        var primerId=0;
        var primerSelect=""; var primerValor="";
        for(i=0;i<detafectados.length;i++){
            $('option', $(detafectados[i])).each(function(element) {
                $(this).removeAttr('disabled');
                val=$(this).attr('data-relation');
                if(val!='' && val!=null){
                detval=val.split(",");
                valida=0;
                    for(j=0;j<detval.length;j++){
                        if( valores.split(detval[j]).length>1 ){
                            valida++;
                            break;
                        }
                    }

                    if(valores2!='' && valida>0){
                        valida=0;
                        for(j=0;j<detval.length;j++){
                            if( valores2.split(detval[j]).length>1 ){
                                valida++;
                                break;
                            }
                        }

                    }

                    if(valida==0){
                        $(this).attr('disabled','true');
                    }
                    else{
                        if(primerId==0 && tipo!="multiple" && $.trim(select)!='' && detafectados.length==1){
                            primerSelect=detafectados[i];
                            primerValor=$(this).val();
                        }
                        primerId++;
                    }
                }
            });
        }

        if(primerId==1 && primerSelect!='' && tipo!="multiple" && $.trim(select)!='' && detafectados.length==1){ // valida solo cuando tiene una sola opcion
            $(primerSelect+">option[value='"+primerValor+"']").attr("selected","true");
            if( tipo!="multiple" && $(primerSelect+">option[value='"+primerValor+"']").attr('data-evento') ){
                eventoSlctGlobalSimple(primerSelect.substr(1),$(primerSelect+">option[value='"+primerValor+"']").attr('data-evento'));
            }
        }

        if(valarray!=null && valarray.length>=1){
            $(afectados).multiselect('select', valarray);
        }

    }
    else{
        $(detafectados.join(" option, ")+" option").removeAttr('disabled');
    }

    $(afectados).multiselect('refresh');
};

enterGlobal=function(e,etiqueta){
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==13){
        $("#"+etiqueta).click(); 
    }
};



// rutas coordenadas
// (mapa, array(coordenaaadas {}), panel detalle)
function initMap(objMap, xyArray,panel) {
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  directionsDisplay.setMap(objMap);

  calculateAndDisplayRoute(directionsService, directionsDisplay, xyArray,objMap,panel);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay,checkboxArray,objMap,panel) {
  
  var waypts = [];
  // solo permite 8 hitos ademas de inicio y fin, para mas puntos se debe contratar
  // el servicio web Direcciones API de Google Maps
  var final=checkboxArray.length -1;

  for (var i = 1; i < final; i++) { // no tocará ni la primera ni la ultima
    if (checkboxArray[i]) {
      waypts.push({
        location: checkboxArray[i],
        stopover: true //true: significa que el x,y es una parada obligatoria
      });
    }
  }

  directionsService.route({
    origin: checkboxArray[0], // primera coordenada
    destination: checkboxArray[final], // ultima coordenada
    waypoints: waypts,
    optimizeWaypoints: false, // true: permite la reordenación de los hitos de forma más eficiente
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      var route = response.routes[0]; 
      if (panel!=null && panel!='') { // mostrara un div con detalles
        var summaryPanel = document.getElementById(panel);
        summaryPanel.innerHTML = '';
        // For each route, display summary information.
        for (var i = 0; i < route.legs.length; i++) {
          var routeSegment = i + 1;
          summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
              '</b><br>';
          summaryPanel.innerHTML += route.legs[i].start_address + ' - a - ';
          summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
          summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
        } 
      }

    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

</script>
