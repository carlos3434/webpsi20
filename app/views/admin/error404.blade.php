
{{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
{{ HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css') }}
{{ HTML::style('css/admin/admin.min.css') }}

<!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Ups! Página no encontrada.</h3>
          <p>
          No hemos podido encontrar la página que estabas buscando. Mientras tanto, es posible <a href="../admin.inicio">volver al panel.</a>
          </p>

          <form class="search-form">
            <div class="input-group">              
            </div>
            <!-- /.input-group -->
          </form>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->