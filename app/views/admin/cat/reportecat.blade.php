<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.cat.js.reportecat_ajax' )
    @include( 'admin.cat.js.reportecat' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Reportes CAT 101
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Reportes 101 CAT </li>
    </ol>
</section>            


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-group" id="accordion-filtros">

                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">101 CAT Exportar Excel
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="form_excel_actividades" id="form_excel_actividades" method="POST" action="pretemporal/reporte">
                           {{Form::token() }}
                               <div class="row form-group">
                                   <div class="col-sm-12">
                                        <div class="col-sm-4"> 
                                            <label class="titulo">Filtro por:</label>
                                                <div class="input-group col-sm-12">
                                                    <select class="form-control" name="slct_filtro" id="slct_filtro">
                                                        <option value="1">Fecha de Registro Ticket</option>
                                                        <option value="2">Fecha de Movimiento</option>
                                                    </select>
                                                </div>
                                        </div>
                                       <div class="col-sm-3"> 
                                            <label class="titulo">F. Movimientos:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-left"  name="fecha_registro" id="fecha_registro"/>
                                                </div>
                                        </div>

                                    <div class="col-sm-3">
                                        <label class="titulo">Quiebre:</label>{{-- slct_gestion --}}
                                        <select class="form-control" name="slct_quiebre[]" id="slct_quiebre"
                                                multiple>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="titulo">Motivo:</label>
                                        <select class="form-control" name="slct_motivo[]" id="slct_motivo"
                                                multiple>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label class="titulo">Submotivo:</label>
                                        <select class="form-control" name="slct_submotivo[]" id="slct_submotivo"
                                                multiple>
                                        </select>
                                    </div>
                                        <!-- <div class="col-sm-2 pull-rigth "><br>
                                              <a class="btn btn-success excel-movimientos-tickets">Exportar Movimientos XLS</a>
                                        </div> -->
                                        <div class="col-sm-2 pull-rigth "><br>
                                              <a class="btn btn-success csv-movimientos-tickets">Exportar Movimientos CSV</a>
                                        </div>
                                   </div>
                               </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                        <div class="row">
                                <div class="col-sm-12">
<!--                                     <div class="col-sm-3 pull-rigth ">
                                          <a class="btn btn-success excel-tickets">Exportar Tickets</a>
                                    </div> -->
                                <!--     <div class="col-sm-3 pull-rigth ">
                                          <a class="btn btn-success excel-movimientos-tickets">Exportar Movimientos</a>
                                    </div> -->
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseOne">Grafica 
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body" id="container-pretemporal">
                            <div class="row">
                                <div class="col-md-12" id="grafica-legados">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Filtros</h3>
                                </div>
                                <form id="form_reporte" name="form_reporte" method="POST" action="reporte/repofscexcel">
                                <div class="col-md-12">
                                    <div class="col-sm-3">
                                         <label>Tipo de Reporte:</label>
                                         <select class="form-control" name="slct_tipo_reporte" id="slct_tipo_reporte">
                                             <option value="0">.::Seleccione::.</option>
                                             <option value="messages" selected>Messages</option>
                                         </select>
                                     </div>
                                     <div class="col-sm-3"> 
                                        <label class="titulo">Fecha:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-left" readonly="" name="fecha_timefrom" id="fecha_timefrom"/>
                                                <div class="input-group-addon" onclick="cleandate()" style="cursor: pointer">
                                                    <i class="fa fa-rotate-left" ></i>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <br>
                                        <button class="btn btn-success" type="button" id="btnBuscar">Buscar Reporte</button>
                                    </div>
                                </div>

                                <div class="box-body table-responsive">
                                    <table id="t_reporte" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cod Actu</th>
                                                <th>AID</th>
                                                <th>ID Mensaje</th>
                                                <th>Descripción</th>
                                                <th>Respuesta</th>
                                                <th>Resultado</th>
                                                <th>Fecha</th>
                                                <th class="editarG"> <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                                 </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_reporte">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Cod Actu</th>
                                                <th>AID</th>
                                                <th>ID Mensaje</th>
                                                <th>Descripción</th>
                                                <th>Respuesta</th>
                                                <th>Resultado</th>
                                                <th>Fecha</th>
                                                <th class="editarG">
                                                <a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                </form>
                            </div>  --><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.ofsc.form.reporte' )
     
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
     {{ HTML::style('css/admin/reportecat.css') }}
@stop
