<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/bootstrap-tagsinput/src/bootstrap-tagsinput.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('lib/perfect-scrollbar/perfect-scrollbar.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}
    {{ HTML::script('lib/perfect-scrollbar/perfect-scrollbar.js') }}
    {{ HTML::script('lib/bootstrap-tagsinput/src/bootstrap-tagsinput.js') }}
    {{ HTML::style('lib/datepicker.css') }}
    {{ HTML::script('lib/bootstrap-datepicker.js') }}


    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.cat.js.listadopersonal_ajax' )
    @include( 'admin.cat.js.listadopersonal' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style type="text/css">
    .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 5px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 32%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }

</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        PERSONAL COT
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Admin 101 CAT </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-group" id="accordion-filtros">               
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">Acciones Listado
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="col-md-5">
                                        <fieldset>
                                        <legend>Asignacion</legend>
                                            <div class="col-md-8">
                                               <select class="form-control" id="slct_quiebre" name="slct_quiebre[]" multiple></select>     
                                            </div>
                                            <div class="col-md-4">
                                                <span class="btn btn-warning btn-sm btnautoasignar">Auto Asignar <i class="glyphicon glyphicon-share"></i></span>
                                            </div>                                                                                
                                        </fieldset>
                                    </div>
                                    <div class="col-md-2" style="margin-top:3%">
                                        <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#validaPersonal">Validar Personal <i class="glyphicon glyphicon-check"></i></span>    
                                    </div>
                                    <div class="col-md-2" style="margin-top:3%">
                                        <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#nuevoUsuario">Nuevo Usuario <i class="glyphicon glyphicon-plus"></i></span>    
                                    </div>
                                    <div class="col-md-2" style="margin-top:3%">
                                        <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#nuevoGrupo">Nuevo Grupo <i class="glyphicon glyphicon-plus"></i></span>    
                                    </div>
                                </div>                                
                            </div>

                            <form name="form_configuracion" id="form_configuracion" method="POST" action="pretemporal/configuracion">
                           {{Form::token() }}
                               <div class="row form-group hidden">
                                   <div class="col-sm-12">
                                        <div class="col-sm-8"> 
                                             <label class="control-label col-sm-3" for="nombre">Cod Avería:</label>
                                             <div class="col-sm-3">
                                                <select class="form-control" name="slct_codactu" id="slct_codactu">
                                                    <option value="1">Obligatorio</option>
                                                    <option value="0">No Obligatorio</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="col-sm-4 pull-rigth "><br>
                                              <a class="btn btn-primary btn-guardar">Guardar Canbios</a>
                                        </div>
                                   </div>
                               </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                            <table id="tb_personal" class="table table-bordered table-hover">
                              <thead>
                                   <tr>
                                      <th style="width: 1%">N°</th>
                                      <th style="width: 10%">Nombres</th>
                                      <th style="width: 10%">Apellidos</th>
                                      <th style="width: 10%">Area</th>
                                      <th style="width: 15%">Grupo</th>
                                      <th style="width: 15%">Quiebre Apoyo</th>
                                      <th style="width: 30%">Horario</th>
                                      <th style="width: 5%"></th>
                                      <th style="width: 5%"></th>
                                  </tr>
                              </thead>
                              <tbody id="tbody_personal">

                              </tbody>
                          </table>
                        </div>                            
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')     
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
    @include( 'admin.cat.form.validapersonal_modal' )
    @include( 'admin.cat.form.nuevousuario_modal' )
     @include( 'admin.cat.form.nuevogrupo_modal' )
    @include( 'admin.cat.form.buscarpersonal_modal' )
    @include( 'admin.cat.form.exoneracion_modal' )
@stop
