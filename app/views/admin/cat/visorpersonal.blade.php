<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.cat.js.visorpersonal_ajax' )
    @include( 'admin.cat.js.visorpersonal' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
  .yellow-fieldset{
        max-width: 100% !important;
        border: 3px solid #999;
        padding:10px 50px 10px 9px;
        border-radius: 10px; 
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       VISOR PERSONAL
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">VISOR PERSONAL {{$fecha = date("Y-m-d")}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-sm-12">
    <form id="formBusqueda">
        <fieldset class="yellow-fieldset">
          <div class="col-sm-2" style="padding-top: 5px">
              <label class="titulo">Rango Fechas:</label>
          </div>
          <div class="col-sm-3">                                             
              <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-left"  name="fecha_registro" id="fecha_registro"/>
              </div>
          </div>
           <div class="col-sm-3">
              <select class="form-control" name="slct_quiebre" id="slct_quiebre" multiple></select>
          </div>
          <div class="col-sm-3">
              <span class="btn btn-primary btn-md btnSearch" onclick="searchbydate()"><i class="glyphicon glyphicon-zoom-in"></i> Buscar</span>
          </div>                                                 
      </fieldset>
    </form>
  </div>
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-body">
               {{-- <div class="row form-group"> --}}
                   <div class="col-sm-12">                       
                        <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover table-striped visorpersonal">
                          <thead>
                            <tr>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            
                          </tbody>
                        </table>
                   </div>

               {{-- </div> --}}
            </div>
            <!-- Finaliza contenido -->
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.ofsc.form.reporte' )
     
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
     {{ HTML::style('css/admin/reportecat.css') }}
@stop
