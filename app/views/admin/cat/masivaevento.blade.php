<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.cat.js.masivaevento_ajax' )
    @include( 'admin.cat.js.masivaevento' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style type="text/css">
  .yellow-fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:0px 15px 15px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 34%;
        border-bottom: 0px;
        margin-bottom: 10px;
    }
    .img-evento{
        width: 100%;
        border: 2px solid gray;
        border-radius: 5px;
        padding: 1px;
    }
</style>
<!-- Content Header (Page header) -->
<section style="margin-top: 55px;" class="content-header">
    <h1>
        EVENTO MASIVA
        <small> </small>
        <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#nuevoEvento">Nuevo <i class="glyphicon glyphicon-plus"></i></span>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Admin 101 CAT </li>
    </ol>

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filtro(s):</h3>
            </div>
        
            <div class="panel-body">
                <form name="form_Personalizado" id="form_Personalizado" method="POST" action="eventomasivo/reporte"> 
                    <div class="col-xs-12">
                        <div class="form-group ">
                            <div class="col-sm-3">
                                <label class="titulo">Buscar por:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="fechas_busqueda" id="fechas_busqueda" readonly="">
                                </div>

                            </div>
                            <div class="col-sm-1">
                                <button id="btn-buscar" type="button" class="btn btn-primary" style="margin-top: 25px"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                            <div class="col-sm-2">
                                <button id="btn-reporte" type="button" class="btn btn-success" style="margin-top: 25px"><i class="fa fa-cloud-download"></i> Descargar</button>
                            </div>
                        </div>
                    </div>              
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table-responsive">
                <table id="table_reporte" class="table table-bordered table-hover">
                    <thead>
                         <tr>
                            <th>Nombre</th>
                            <th>Nodo</th>
                            <th>Troba</th>
                            <th>Empresa</th>
                            <th>Usuario Creador</th>
                            <th>Fecha Creación</th>
                            <th>Usuario Liquida</th>
                            <th>Fecha Liquidación</th>
                            <th>Estado Actual</th>
                            <th>N° Tickets</th>
                            <th>Gestionados</th>
                            <th>Con servicio</th>
                            <th>Sin Servicio</th>
                            <th>% Con Servicio</th>
                            <th>Detalle</th>
                            <th>Averias</th>
                            <th>Cancelar</th>
                        </tr>
                    </thead>
                    <tbody id="tb_reporte">

                    </tbody>
                </table>
            </div>
        </div>


    </div>

</section><!-- /.content -->
@stop

@section('formulario')     
    @include( 'admin.cat.form.nuevoevento_modal' )
    @include( 'admin.cat.form.cancelar_modal' )
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
@stop
