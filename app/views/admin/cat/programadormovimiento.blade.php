<!DOCTYPE html>
@extends('layouts.masterv2')

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.min.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/timepicker/css/bootstrap-timepicker.min.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}

    {{ HTML::style('//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css') }}
@endpush    

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<input type="hidden" id="token" value="{{ csrf_token() }}">
<section class="content-header">
    <h1>
        Programador de Movimientos
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Bandeja de Pedidos</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="panel-group" id="accordion-filtros"> 

            <div class="box box-primary">
              <div class="box-body" style="min-height:250px">
                <div class="box-body table-responsive">
                  <table id="t_comando" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                        <thead>
                            <tr>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Nombre</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Variante de Búsqueda</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Fecha inicio de búsqueda</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Fecha final de búsqueda </th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Tipo de  búsqueda</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Fecha inicio de ejecución del comando</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Fecha final de ejecución del comando</th>
                              <th style="width: 60px; color: #3c8dbc; text-align: center">Estado</th>
                              <th style="width: 60px; color: #3c8dbc;">[]</th>
			      <th style="width: 60px; color: #3c8dbc;">[]</th>

                            </tr>
                        </thead>
                  
                        <tbody id="tb_comando">

                        </tbody>
                  </table>
                </div>
              </div>
            </div>

        </div>
    </div>
    <div class="col-md-2" style="margin-top:3%">
        <span class="btn btn-success btn-sm" data-toggle="modal" data-titulo="Nuevo" data-target="#nuevoEvento">Nuevo Comando <i class="glyphicon glyphicon-plus"></i></span>    
    </div>
  </div>
  
 
        

</section><!-- /.content -->

@stop

@section('formulario')
    @include( 'admin.cat.form.programadormovimiento_modal' )
@stop

@push('scripts')
    {{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js') }}

    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('lib/timepicker/js/bootstrap-timepicker.min.js') }}

    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}

    {{ HTML::script('js/utils.js') }}
    {{ HTML::script('js/psi.js') }}
    {{ HTML::script('js/utils.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
 
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    <script type="text/javascript" src="js/gestion101/programadormovimiento.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="js/gestion101/programadormovimiento_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
    
@endpush('script')




