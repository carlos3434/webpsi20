<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.min.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}


{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}


@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.cat.js.rrss_ajax' )
@include( 'admin.cat.js.rrss' )
@stop
<style type="text/css">
  .form-control::-webkit-input-placeholder { color: #337ab7; opacity: 1 !important; }
  .rojo {
    border-color:#FF0000; background:#F6CECE!important;
  }
  .verde {
    border-color:#E3F6CE; background:#BCF5A9!important;
  }
  .azul {
    color: #337ab7;
  }
  .panel-heading.box {
    margin-bottom: 0px;
  }
</style>

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<input type="hidden" id="token" value="{{ csrf_token() }}">
<section class="content-header">
    <h1>
        Registro de Pedidos
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Registro de Pedidos</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">

      <input type="hidden" id='actividad'>
      <!-- Main content -->

      <div class="tab-content">
        <div id="registro" class="tab-pane fade in active">
          <form id="ficha_form" action="pretemporal/registrar">
          
          <div class="col-sm-12">
              <div class="panel panel-primary">
                  <div class="panel-body form-horizontal">
                      <div class="row">
                        <label class="control-label col-sm-2" for="nombre">Quiebre:</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="slct_quiebre_id" id="slct_quiebre_id">
                            </select>
                        </div>
                        <div id="fuente_opc" style="display:none;">
                          <label class="control-label col-sm-2" for="nombre">Tipo de Fuente:</label>
                          <div class="col-sm-2">
                              <select class="form-control" name="slct_tipo_fuente"  id="slct_fuente">
                              </select>
                          </div>
                        </div>
                      </div>
                      <input type="hidden" name="txt_actividad_id" value="1">
                      <input type="hidden" name="txt_interface" value="go_rrss">
                      <input type="hidden" name="txt_gestion" value="psi">
                      <div class="form-group">
                          <label class="control-label col-sm-2" for="nombre">Producto:</label>
                          <div class="col-sm-2">
                              <select class="form-control" name="slct_actividad_tipo_id" id="tipo_averia">
                                  <option value="" disabled selected>Seleccione</option>
                                  <option value="5">Telefono</option>
                                  <option value="6">Internet</option>
                                  <option value="4">Televisión</option>
                              </select>
                          </div>

                          <label class="control-label col-sm-2" for="nombre">Tipo de Atencion:</label>
                          <div class="col-sm-2">
                              <select class="form-control" name="slct_tipo_atencion" id="slct_atencion">
                              </select>
                          </div>

                          <label class="control-label col-sm-1" for="nombre">Cod Avería:</label>
                          <input type="hidden" id="codactu_admin" value="">
                          <div class="col-sm-2">
                              <input type="text" data-toggle="tooltip" data-placement="top" title="Este dato agiliza el proceso"
                                class="form-control" placeholder="Código de Avería" name="txt_codactu" 
                                id="codactu" oninvalid="setCustomValidity('El formato de codigo de avería es incorrecto.')"
                                onchange="try{setCustomValidity('')}catch(e){}" onblur="upperCase(this)">
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="panel panel-primary">
                  <div class="panel-heading">Formulario de Cliente</div>
                  <div class="panel-body form-horizontal">
                      
                          <div class="form-group has-warning has-feedback">
                            <label class="control-label col-sm-3" for="nombre">Apellidos y Nombre:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" placeholder="Ingrese el nombre del cliente" 
                              name="txt_cliente_nombre" pattern="^\S+\s+.*\S$" data-toggle="tooltip" 
                              data-placement="top" title="Formato: Apellidos Nombres" 
                              oninvalid="setCustomValidity('El formato debe tener al menos un apellido y un nombre')" 
                              onchange="try{setCustomValidity('')}catch(e){}" required>
                              <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                            </div>
                          </div>

                          <div class="form-group has-warning has-feedback">
                            <label class="control-label col-sm-3" for="celular">Celular:</label>
                            <div class="col-sm-9">
                              <input type="tel" class="form-control" placeholder="Ingrese el celular del cliente" name="txt_cliente_celular" pattern="[8-9]{1,1}[0-9]{8,8}" oninvalid="setCustomValidity('El formato debe empezar con 8 ó 9, y tiene que presentar 9 dígitos')" onchange="try{setCustomValidity('')}catch(e){}" required>
                              <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                            </div>
                          </div>

                          <div class="form-group has-warning has-feedback" id="telefono">
                            <label class="control-label col-sm-3" for="telefono" id="codcli">Telefono:</label>
                            <div class="col-sm-9">
                              <input type="tel" class="form-control" id="input_codcli" 
                              name="txt_codcli" placeholder="Ingrese el telefono del cliente" 
                              oninvalid="setCustomValidity('Telefono/Internet : Debe tener 8 números')" 
                              onchange="try{setCustomValidity('')}catch(e){}" required>
                              <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="correo">Correo:</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" name="txt_cliente_correo" placeholder="Ingrese el correo del cliente">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="dni">DNI:</label>
                            <div class="col-sm-9">
                              <input type="text" pattern="[0-9]{8,8}" class="form-control" name="txt_cliente_dni" placeholder="Ingrese el DNI del cliente" oninvalid="setCustomValidity('El formato debe presentar 8 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                            </div>
                          </div>

                  </div><!-- /body -->
              </div><!-- /panel -->
          </div>
          <div class="col-sm-6">
              <div class="panel panel-info ">
                  <div class="panel-heading">Formulario de Contacto (Opcional)</div>
                  <div class="panel-body form-horizontal">
                      
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="nombre">Apellidos y Nombre:</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" pattern="^\S+\s+.*\S$" placeholder="Ingrese el nombre de contacto" name="txt_contacto_nombre" oninvalid="setCustomValidity('El formato debe tener al menos un apellido y un nombre')" onchange="try{setCustomValidity('')}catch(e){}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="celular">Celular:</label>
                            <div class="col-sm-9">
                              <input type="text" pattern="[8-9]{1,1}[0-9]{8,8}" class="form-control" placeholder="Ingrese el celular de contacto" name="txt_contacto_celular" oninvalid="setCustomValidity('El formato debe empezar con 8 ó 9, y tiene que presentar 9 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="nombre">Telefono:</label>
                            <div class="col-sm-9">
                              <input type="tel" class="form-control" placeholder="Ingrese el telefono de contacto" name="txt_contacto_telefono">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="correo">Correo:</label>
                            <div class="col-sm-9">
                              <input type="email" class="form-control" placeholder="Ingrese el correo de contacto" name="txt_contacto_correo">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="celular">DNI:</label>
                            <div class="col-sm-9">
                              <input type="text" pattern="[0-9]{8,8}" class="form-control" placeholder="Ingrese el DNI de contacto" name="txt_contacto_dni" oninvalid="setCustomValidity('El formato debe presentar 8 dígitos')" onchange="try{setCustomValidity('')}catch(e){}">
                            </div>
                          </div>

                  </div><!-- /body -->
              </div><!-- /panel -->
          </div>
          <div class="col-sm-12">
              <div class="panel panel-primary">
                  
                  <div class="panel-body form-horizontal">
                          
                    <input type="hidden" class="form-control" placeholder="Ingrese el nombre" name="txt_embajador_nom" required>
                    <input type="hidden" class="form-control" placeholder="Ingrese el correo" name="txt_embajador_correo">
                    <input type="hidden" class="form-control" placeholder="Ingrese el celular" name="txt_embajador_cel" required>
                    <input type="hidden" class="form-control" placeholder="Ingrese el dni" name="txt_embajador_dni" required>

                      <div class="col-sm-12">
                          <div class="form-group">
                            <label for="comment">Comentario:</label>
                            <span style="color:red;">Para Internet o Voz Ip: Determinar tipo Red: HFC (indicar codigo cliente ) o Cobre( indicar nro servicio)</span>
                            <textarea class="form-control" rows="5" id="comment" value="1" name="txt_comentario"></textarea>
                          </div>
                      </div>

                      <div class="col-sm-12 form-group">
                          <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                      </div>

                  </div><!-- /body -->
              </div><!-- /panel -->
          </div>
         </form> 
        </div>
      </div>
    </div>
  </div>

</section><!-- /.content -->


@stop
