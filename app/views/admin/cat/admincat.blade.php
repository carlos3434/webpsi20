<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
    {{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('https://code.highcharts.com/highcharts.js') }}
    {{ HTML::script('https://code.highcharts.com/modules/exporting.js') }}

    @include( 'admin.cat.js.admincat_ajax' )
    @include( 'admin.cat.js.admincat' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Administrador CAT 101
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Admin 101 CAT </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-group" id="accordion-filtros">

                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">Campos a Validar
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <!--<form name="form_configuracion" id="form_configuracion" method="POST" action="pretemporal/configuracion">-->
                           {{Form::token() }}
                           <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body table-responsive">
                                    <table id="tb_config" class="table table-bordered table-hover">
                                      <thead>
                                           <tr>
                                              <th style="width: 25%">Campo</th>
                                              <th style="width: 20%">Obligatorio</th>
                                              <th style="width: 20%">Cantidad</th>
                                              <th style="width: 5%"></th>
                                          </tr>
                                      </thead>
                                      <tbody id="tbody_config">

                                      </tbody>
                                  </table>
                                </div>                            
                            </div>
                            </div>
                               <!--<div class="row form-group">
                                   <div class="col-sm-12">
                                        <!<div class="col-sm-8"> 
                                             <label class="control-label col-sm-3" for="nombre">Cod Avería:</label>
                                             <div class="col-sm-3">
                                                <select class="form-control" name="slct_codactu" id="slct_codactu">
                                                    <option value="1">Obligatorio</option>
                                                    <option value="0">No Obligatorio</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="col-sm-4 pull-rigth "><br>
                                              <a class="btn btn-primary btn-guardar">Guardar Canbios</a>
                                        </div>
                                   </div>-->
                               </div>
                            <!--</form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section><!-- /.content -->
@stop

@section('formulario')     
     {{ HTML::style('css/sweetalert/sweetalert.css') }}
@stop
