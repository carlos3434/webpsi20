<script type="text/javascript">

var listado={
    Userstovalidate: function(){
        $.ajax({
            url         : 'quiebregrupo/getuserbygrupo',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(data) {
                HTMLUsuariosValidar(data.data);
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    codactu: function(){
        datos=$("#form_configuracion").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'pretemporal/configuracion',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    validateUsers: function(datos,element){
        $.ajax({
            url         : 'usuarioarea/registrar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();                
            },
            success : function(data) {
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                    $(element).parent().parent().remove();
                    listado.cargarPersonal('',HTMLCargarPersonal);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
                eventoCargaRemover();
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    cargarPersonal: function(data,evento){
        $.ajax({
            url         : 'usuarioarea/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {       
            },
            success : function(data) {
                if(data.rst==1){
                    evento(data.datos)
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
        CambiarEstado: function(data){
        $.ajax({
            url         : 'usuarioarea/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    listado.cargarPersonal('',HTMLCargarPersonal);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
      nuevoUsuario: function(datos){
        $("#formNuevoUsuario #txt_ci").remove();
        $("#formNuevoUsuario").append('<input type="hidden" value="CI-" name="txt_ci" id="txt_ci">');
        var datos=$("#formNuevoUsuario").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
        $.ajax({
            url         : 'usuarioarea/crear',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {    
                 eventoCargaMostrar();   
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                    $("#nuevoUsuario").modal('hide');
                    listado.cargarPersonal('',HTMLCargarPersonal);               
                }else{
                    swal('Mensaje','Error','warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    autoasignar(data = ''){       
         $.ajax({
            url         : 'pretemporal/autoasignar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    var html = '';
                    html+='Asignados:'+data.asignados;
                    html+=', Quedan Pendientes:'+data.pendientes;
                    swal('Mensaje','Asignados:'+html,'success');
                    $("#slct_quiebre").multiselect('refresh');
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    Rusuariohorario(data,value,text,usuario_id){
        $.ajax({
            url         : 'usuariohorario/registrar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje','Registrado','success');
                    $(".clsInput[iduser="+ usuario_id +"]").tagsinput('add', { "value": value , "text": text  });
                }else{
                    swal('Mensaje','Error al registrar','danger');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    listarHorarios(){
        $.ajax({
            url         : 'usuariohorario/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1 && data.datos.length > 0){
                    HTMLHorarios(data.datos);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    deleteDay(data){
        $.ajax({
            url         : 'usuariohorario/destroyday',
            type        : 'POST',
            data        : data,
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==0){
                    swal('Mensaje','Error al eliminar','danger');                     
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    crearGrupo(data){
        $.ajax({
            url         : 'usuarioarea/creargrupo',
            type        : 'POST',
            data        : data,
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){                
                    $('#nuevoGrupo').modal('hide');
                    swal('Mensaje','Registrado','success');                     
                }else{
                    swal('Mensaje','Error','danger');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    listarGrupos(url,data,contenedor,usuarioa){
        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1 && data.datos.length > 0){
                    if(url == 'grupocot/listar'){
                        HTMLGrupos(data.datos,contenedor,usuarioa,'grupo');                        
                    }else{
                        HTMLGrupos(data.datos,contenedor,usuarioa,'quiebre');
                    }
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    exonerar(data,contenedor){
         $.ajax({
            url         : 'usuarioexoneracion/crear',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje','Registrado','success');  
                   listado.listarExoneraciones({usuario_id:data.usuario_id});
                }else{
                    swal('Mensaje','Error al registrar','danger');  
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    listarExoneraciones(data){
         $.ajax({
            url         : 'usuarioexoneracion/listar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1 && data.datos.length > 0){
                    HTMLCargarExoneraciones(data.datos);
                }else{
                    $("#tb_exoneracion").html('');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    deleteEXoneracion(data){
         $.ajax({
            url         : 'usuarioexoneracion/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    listado.listarExoneraciones({usuario_id:data.usuario_id});
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }
};
</script>

