<script type="text/javascript">

var Consulta={
    Listar:function(busqueda){

        if (busqueda == 'P') {
            findTypeGLOBAL = 'P';
            var ficha_busqueda = {
                PG: busqueda,
                tipo_busqueda: busqueda,
                tipo: $("#slct_tipo").val(),
                busqueda: $("#txt_buscar").val()
            }
        }

        if ($("#slct_tipo").val() != '' || busqueda =="G") {
            $.ajax({
                url         : 'pretemporal/buscar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : ficha_busqueda,
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();

                    if(data.length > 0){
                        HTMLListarPretemporal(data);
                    }else{
                        swal('Mensaje',data.msj,'warning');
                        $("#tb_embajador").html('<tr><td colspan="12">Ningún dato disponible en esta tabla</td></tr>');
                        
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error")
                }
            });
        } else swal('Mensaje','Seleccione tipo de búsqueda','info');

    },
    Cargar: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        }
            $.ajax({
                url         : 'pretemporal/cargar',
                type        : 'GET',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();
                    if(data.rst==1){
                        ListarDataModal(data);
                    }else{
                        swal('Mensaje',data.msj,'warning');
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error")
                }
            });
    },
    Movimiento: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        }

        $.ajax({
            url         : 'pretemporal/listarmovimiento',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarMovimientos(data.data);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }
};
</script>
