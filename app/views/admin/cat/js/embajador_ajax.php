<script type="text/javascript">

var Embajador={
    CargarEmbajador:function(busqueda){

        if (busqueda == 'P') {
            var ficha_busqueda = {
                PG: busqueda,
                tipo_busqueda: busqueda,
                tipo: $("#slct_tipo").val(),
                busqueda: $("#txt_buscar").val()
            }
        } else if(busqueda=="G"){ // Filtro General
            // var ficha_busqueda = {
            //     PG: busqueda,
            //     tipo_busqueda: busqueda,
            //     tipo_averia: $("#slct_actividad").val(),
            //     estado: $("#slct_estado").val(),
            //     fecha_registro: $("#txt_fecha_registro").val()
            // }
            ficha_busqueda=$("#form_General").serialize().split("txt_").join("").split("slct_").join("")+ '&PG=G';

        }

        if ($("#slct_tipo").val() != '' || busqueda=="G") {
            $.ajax({
                url         : 'embajador/buscarembajador',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : ficha_busqueda,
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();

                    if(data.rst==1){
                        console.log(data.datos[0]);
                        HTMLCargarEmbajador(data.datos);
                    }else{
                        swal('Mensaje',data.msj,'warning');
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error")
                }
            });
        } else swal('Mensaje','Seleccione tipo de búsqueda','info');

    }
};
</script>
