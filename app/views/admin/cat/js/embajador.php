<script type="text/javascript">
GLOBAL = {};
// var temporalBandeja=0;
// var table;

$(document).ready(function() {
    eventoCargaMostrar();
    slctGlobalHtml('slct_averia,#slct_averia','multiple');
    slctGlobalHtml('slct_gestion,#slct_gestion','multiple');
    slctGlobalHtml('slct_atencion,#slct_atencion','multiple');
    slctGlobalHtml('slct_actividad,#slct_actividad','multiple');
    slctGlobalHtml('slct_estado,#slct_estado','multiple');
    slctGlobalHtml('slct_atencion,#slct_atencion','multiple');
    slctGlobalHtml('slct_tipo','simple');

    activarTabla();

    $('#txt_fecha_registro').val(
        moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $('#txt_fecha_registro').daterangepicker({
        //minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });
    
   $("#btn_personalizado").click(personalizado);
    $("#btn_general").click(general);

    setTimeout(function(){eventoCargaRemover();}, 1000);
});

personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        swal("Mensaje","Seleccione Tipo Filtro", "warning");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        swal("Mensaje","Ingrese datos", "warning");
        $("#txt_buscar").focus();
    } else{
        //$("#txt_buscar").val($.trim($("#txt_buscar").val()));
        Embajador.CargarEmbajador("P");
    }

};

general=function(){
    Embajador.CargarEmbajador("G");
};

activarTabla=function(){
    $("#t_embajador").dataTable(); // inicializo el datatable
};

HTMLCargarEmbajador=function(datos){
var html="";
    $('#t_embajador').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td>"+data.ticket+"</td>"+
        "<td>"+data.tipo_averia+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.codcli+"</td>"+
        "<td>"+data.gestion+"</td>"+
        "<td>"+data.tipo_atencion+"</td>"+
        "<td>"+data.cliente_nombre+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.fecha_registro+"</td>"+
        "<td></td>";
        html+="</tr>";
    });
    $("#tb_embajador").html(html);
    activarTabla();
};



    Cargar= function (ticket) {
        var ficha_busqueda = {
            tipo_busqueda: 'ticket',
            busqueda: ticket
        }
        this.$http.get('/webpsi20/public/embajador/i', ficha_busqueda, function (data) {
            if (data.rst==1) {
                this.datos_averia = false;
                this.datos_ficha = false;
                this.tipo_result = data.tipo;

                //completo formulario de ficha
                if (data.ficha.length != 0){
                    ticket = data.ficha;
                    this.datos_ficha = true;

                    this.identificador_label = 'N° Ticket';
                    this.identificador = 'E' +pad(ticket.id, 5);
                    this.estado = ticket.estado;
                    this.fecha_registro = ticket.created_at;
                    
                    this.f_estado=ticket.estado;
                    this.f_tipoaveria = ticket.tipo_averia;
                    this.f_codactu=ticket.codactu;
                    this.f_ticket= 'E' +pad(ticket.id, 5);

                    this.f_nomcli=ticket.cliente_nombre;
                    this.f_celcli =ticket.cliente_celular;
                    this.f_telcli = ticket.cliente_telefono;
                    this.f_correocli = ticket.cliente_correo;
                    this.f_dnicli = ticket.cliente_dni;
                    this.f_codcli =ticket.codcli;

                    this.f_nomcon=ticket.contacto_nombre;
                    this.f_celcon =ticket.contacto_celular;
                    this.f_telcon = ticket.contacto_telefono;
                    this.f_correocon = ticket.contacto_correo;
                    this.f_dnicon = ticket.contacto_dni;
                    
                    this.f_nomemb=ticket.embajador_nombre;
                    this.f_celemb =ticket.embajador_celular;
                    this.f_correoemb = ticket.embajador_correo;

                    this.f_comentario = ticket.comentario;
                } else {
                    $("#ficha_results input[type=text]").val('');
                    $("#ficha_results textarea").val('');
                }

                if (data.averia.length != 0){
                    averia = data.averia;
                    this.datos_averia = true;
                    
                    if (averia.plazo == 'Dentro') {
                        this.plazo= 'Dentro del Plazo';
                        this.btn_reforzar = 0;
                        $("#plazo").addClass('verde').removeClass('rojo');
                    } else if (averia.plazo == 'Fuera') {
                        this.plazo= 'Fuera del Plazo';
                        this.btn_reforzar = 1;
                        $("#plazo").addClass('rojo').removeClass('verde');
                    }

                    this.identificador_label = 'Codigo Avería';
                    this.identificador = averia.codactu;
                    this.estado = averia.estado;
                    this.fecha_registro = averia.fecha_registro;

                    this.a_estado= averia.estado;
                    this.a_fecha= averia.fecha_registro;
                    this.a_codactu= averia.codactu;
                    this.a_telefono= averia.telefono;
                    this.a_cliente= averia.nombre_cliente;
                    this.a_tipo_averia= averia.tipo_averia;
                    this.a_tipo_servicio= averia.tipo_servicio;
                    this.a_zonal= averia.zonal;
                    this.a_contrata= averia.contrata;
                    this.a_distrito= averia.distrito;
                    this.a_area= averia.area;
                    this.a_estacion= averia.area2;
                    this.a_quiebre= averia.quiebre;
                } else {
                    $("#averia_results input[type=text]").val('');
                }
                this.eventoCargaRemover();
                $("#myModal").modal();
                
            } else {
                swal('Mensaje',data.msj,'warning');
            }
        }).error(function(errors) {
            alert('error');
        }); 
    };
</script>