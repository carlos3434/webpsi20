<script type="text/javascript">

var Rrss={
    Enviar:function(url){
        var datos = $("#ficha_form").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('N°Ticket: '+data.ticket, data.msj, "success");
                    $("#ficha_form input[type='text']").val('');
                    $("#ficha_form input[type='email']").val('');
                    $("#ficha_form input[type='tel']").val('');
                    $("#ficha_form textarea").val('');
                    $("#slct_tipo_averia").val('').prop('selected', true);

                    //$("#slct_quiebre_id, #slct_fuente, #slct_atencion, #slct_atencion, #tipo_averia").multiselect("clearSelection");
                    $("#slct_quiebre_id").multiselect("refresh");
                    $("#slct_atencion").multiselect("refresh");
                    $("#slct_fuente").multiselect("refresh");
                    $("#tipo_averia").multiselect("refresh");
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });

    },
    CargarAdmin: function(){
        $.ajax({
            url         : 'pretemporal/cargaradmin',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(data) {
                eventoCargaRemover();
                $("#codactu_admin").val(data.datos);
                if (data.datos == 1) {
                    $("#codactu").css( "background-color", "#F5F6CE" );
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }
};
</script>