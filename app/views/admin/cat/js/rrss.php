<script type="text/javascript">
	$(document).ready(function(){

        Rrss.CargarAdmin();

		$("#ficha_form").submit(function(e){
			e.preventDefault();
            if ($("#codactu_admin").val() == 1 && $("#codactu").val().trim() == '') {
                swal("Mensaje","El codigo de Avería es Obligatorio ","warning");
                return;
            }
            if ($("#tipo_averia").val() == null) {
                swal("Mensaje","Seleccione el tipo de Averia","warning");
                return;
            }
            if ($("#slct_quiebre_id").val() == '') {
                swal("Mensaje","Seleccione el quiebre","warning"); 
                return;
            }
            if ($("#slct_quiebre_id").val() == '47' && $("#slct_fuente").val() == '') {
                swal("Mensaje","Seleccione el tipo de fuente","warning"); 
                return;
            }
            if ($("#slct_atencion").val() == '') {
                swal("Mensaje","Seleccione el tipo de Atencion","warning");
                return;
            }

            if ($("#slct_quiebre_id").val() == '46' || $("#slct_quiebre_id").val() == '47'
                || $("#slct_quiebre_id").val() == '49' || $("#slct_quiebre_id").val() == '51' || $("#slct_quiebre_id").val() == '52') {
                var url = $(this).prop("action");
                Rrss.Enviar(url);
            } else {
                swal("Mensaje","Solo se admite quiebres: Embajador, Redes Sociales y Multicentro Agencia","warning");
                return;
            }

            // if ($("#slct_quiebre_id").val() == 47 && $("#codactu").val() == '') {
            //     swal("Redes Sociales","Es obligatorio el Código de Avería","warning");
            //     $("#codactu").focus();
            //     return;
            // }
		});
        slctGlobalHtml('tipo_averia','simple');
        slctGlobal.listarSlct('pretemporal_atencion', 'slct_atencion', 'simple', null);
        slctGlobal.listarSlct('listado', 'slct_quiebre_id', 'simple', null);
        slctGlobal.listarSlct('pretemporal_rrss', 'slct_fuente', 'simple', null);

        $("#slct_quiebre_id").on('change', function() {
            switch(this.value) {
                case '46':
                $("#fuente_opc").css("display","none");
                break;
                case '47':
                $("#fuente_opc").css("display","");
                break;
            }
        });

        $("#tipo_averia").change(function(){
            $("#codactu").val("");
            $("#codactu").focus();
        });

        $('#tipo_averia').on('change', function() {
          switch(this.value) {
            case '4':
                //Television
                $("#codcli").html('Codigo Cliente');
                $("#codactu").attr('pattern', "[0-9]+");
                $("#input_codcli").removeAttr('pattern');
                break;
            case '6':
                //Internet
                $("#codcli").html('N°Servicio Afectado:');
                $("#input_codcli").attr('pattern', "[0-9]{8}");
                $("#codactu").attr('pattern', "[A-Z]{3}[0-9]{7}");
                break;
            case '5':
                //Telefono
                $("#codcli").html('N°Servicio Afectado:');
                $("#input_codcli").attr('pattern', "[0-9]{8}");
                $("#codactu").attr('pattern', "[A-Z]{3}[0-9]{7}");
                break;
            default:
                $("#codcli").html('Telefono:');
                $("#codactu").attr('pattern', "[0-9]+");
            }
        });
	});

    upperCase =function(input) {
       var x=$(input).val();
       $(input).val(x.toUpperCase());
    }
</script>
