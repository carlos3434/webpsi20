<script type="text/javascript">
GLOBAL = {};
var findTypeGLOBAL = '';
$(document).ready(function() {
    eventoCargaMostrar();
    $(".navbar-btn.sidebar-toggle").click();
    /*slctGlobalHtml('slct_averia,#slct_averia','multiple');*/
    slctGlobalHtml('slct_actividad,#slct_actividad','multiple');
    slctGlobalHtml('slct_gestion,#slct_gestion','multiple');
    slctGlobalHtml('slct_tipo','simple');
    slctGlobalHtml('slct_asignacion','simple');
    slctGlobalHtml('slct_tiporea','simple');
    

    slctGlobal.listarSlct('listado', 'slct_quiebre_transferencia', 'simple', null);
    slctGlobal.listarSlct('actividadtipo', 'slct_averia', 'multiple', null,{apocope:1});
    slctGlobal.listarSlct('listado', 'slct_quiebre', 'multiple', null);
    slctGlobal.listarSlct('estado_pretemporal', 'slct_estado_pre', 'multiple', null);
    slctGlobal.listarSlct('pretemporal_atencion', 'slct_atencion', 'multiple', null);
    slctGlobal.listarSlct('eventomasivo', 'slct_eventos', 'multiple', null);
    slctGlobal.listarSlct('usuarioarea','slct_usuarios','multiple',null);
    slctGlobal.listarSlct('usuarioarea','slct_usuario','multiple',null,{libres:1});
    activarTabla();
    $('#txt_fecha_registro').val(
        moment().subtract(7, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $('#txt_fecha_registro').daterangepicker({      
        startDate: moment().subtract(7, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });
    var data={};var data2={};
    data = { requerimiento: '0-0', mas: '1' };
    data2 = { requerimiento: '0-0', mas: '1', quiebre_id: '46,55', interface: 'pretemporal' };
    $('#slct_motivo,#slct_submotivo,#slct_estado').multiselect('destroy');
    var ids = [];
    slctGlobal.listarSlct('motivo','slct_motivo','multiple',ids,data2,0,'#slct_submotivo,#slct_estado','M');
    slctGlobal.listarSlct('submotivo','slct_submotivo','multiple',ids,data,1,'#slct_estado','S','slct_motivo','M');
    slctGlobal.listarSlct('estado','slct_estado','multiple',ids,data,1);
    //motivos,submotivos,estados del modal
    $('#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal').multiselect('destroy');
    slctGlobal.listarSlct('motivo','slct_motivo_modal','simple',ids,data2,0,'#slct_submotivo_modal,#slct_estado_modal','M');
    slctGlobal.listarSlct('submotivo','slct_submotivo_modal','simple',ids,data,1,'#slct_estado_modal','S','slct_motivo_modal','M');
    slctGlobal.listarSlct('estado','slct_estado_modal','simple',ids,data,1);
    $("#slct_tipo").change(ValidaTipo);
    $("#btn_personalizado").click(personalizado);
    $("#btn_general").click(general);
    $("#btn_limpiar").click(limpiarFormulario);
    setTimeout(function(){eventoCargaRemover();}, 1000);

    /*if select all*/
    $('#chk_todo').on('ifChanged', function(event){
        var table = $('#t_embajador').DataTable();
        $(':checkbox', table.rows().nodes()).prop('checked', event.target.checked);

        if(event.target.checked == true){
            Pretemporales.Listar(searchtype);                
        }else{          
            slct_indivual = [];
        }
    });
    /*end if select all*/

    $(document).on('change', '#slct_tiporea', function(event) {
        if($(this).val() == 1){
            $(".rea_usuario").removeClass('hidden');
            $(".select_quiebres").addClass('hidden');
        }else if($(this).val() ==  3){
            $(".select_quiebres").removeClass('hidden');
            $(".rea_usuario").addClass('hidden');
        }else{
            $(".select_quiebres").addClass('hidden');
            $(".rea_usuario").addClass('hidden');
        }
    });


    $(document).on('change','.chkSelect',function(event){
        var value = $(this).attr('value');                
        var index = slct_indivual.indexOf(string(value));

        if(this.checked) {
            if(index === -1){
                slct_indivual.push(string(value));
            }
        }else{
            if(index !== -1){
                slct_indivual.splice(index, 1);
            }
        }

        if(count_init !== slct_indivual.length){
            $('input[id="chk_todo"]').removeAttr('checked').iCheck('update');
        }
    });

});
personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        swal("Mensaje","Seleccione Tipo Filtro", "warning");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        swal("Mensaje","Ingrese datos", "warning");
        $("#txt_buscar").focus();
    } else{
        $('input[id="chk_todo"]').removeAttr('checked').iCheck('update');
        $('.chkSelect').prop('checked',false);
        slct_indivual = [];
        Pretemporales.cargarPretemporal("P");
    }
};
general=function(){    
    $('input[id="chk_todo"]').removeAttr('checked').iCheck('update');
    $('.chkSelect').prop('checked',false);
    slct_indivual = [];
    Pretemporales.cargarPretemporal("G");
};
ValidaTipo=function(){
    $("#txt_buscar").val("");
    $("#txt_buscar").focus();
};
activarTabla=function(){
    $("#t_embajador").dataTable(); 
};
limpiarFormulario=function(){
    $("#slct_averia, #slct_quiebre, #slct_atencion, #slct_estado_pre, #slct_estado, #slct_motivo, #slct_submotivo, #slct_tipo, #slct_gestion,#slct_eventos").multiselect("clearSelection");
    $("#slct_tipo").multiselect("refresh");
    $('#txt_fecha_registro').val(
        moment().subtract(7, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
     $('#txt_fecha_registro').daterangepicker({
        startDate: moment().subtract(7, 'days'),
        format: 'YYYY-MM-DD'
    });
    $('#txt_buscar').val('');
    $("#tb_embajador").html('<tr><td colspan="12">Ningún dato disponible en esta tabla</td></tr>');
};

HTMLListarPretemporal=function(datos){
var html="";
    $('#t_embajador').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        color= '#F7BE81';
        fecha_usuario = data.fecha_registro + " " +data.asignado;
        if(data.estado_id == 1) color= '#FFFFFF';
        if(data.estado == 'Cerrado') color= '#A9BCF5';
        html+="<tr>"+
        "<td style='background-color:"+color+";'>"+data.ticket+"<br>"+data.tipo_atencion+"</td>"+
        "<td>"+fecha_usuario+"</td>"+
        "<td>"+data.fechaultmov+"</td>"+
        "<td>"+data.tipo_averia+"</td>"+
        "<td>"+data.codcli+"</td>"+
        "<td>"+data.quiebre+"</td>"+
        "<td>"+data.cliente_nombre+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.solucion+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.codliq+"</td>"+
        "<td>"+data.area+"</td>"+
        "<td><input class='form-control chkSelect' type='checkbox' name='chk_select[]' id='chk_select' value='"+data.id+"'/></td>";
        html+="</tr>";
    });
    $("#tb_embajador").html(html);
    activarTabla();
};

ListarDataModal=function(obj){
    //validar permisos del quierbre
    $("#txt_id_modal").val(obj.id);
    $("#txt_codactu_modal, #txt_codactu_modal_mov").val(obj.codactu);
    $("#txt_codcli_modal, #txt_codcli_modal_mov").val(obj.codcli);
    $("#txt_estado_modal, #txt_estado_modal_mov").val(obj.estado);
    $("#txt_ticket_modal, #txt_ticket_modal_mov").val(obj.ticket);
    $("#txt_atencion").val(obj.atencion);
    $("#txt_averia_modal, #txt_averia_modal_mov").val(obj.tipo_averia);

    if(obj.evento !== ''){
        $(".evento").removeClass('hidden');
        $("#txt_evento_modal,#txt_evento_modal_2").val(obj.evento);
        $("#txt_idevento").val(obj.evento_masiva_id);
        
        //$("#txt_atencion").addClass('hidden');
    }else{
        $(".evento").addClass('hidden');
        //$("#txt_atencion").removeClass('hidden');
    }

    $("#txt_tipo_averia_modal").val(obj.actividad_tipo_id);
    $("#busqueda").val(obj.id);
    if (obj.gestion == 'rrss') $("#lbl_registrador").html('Usuario');
    else $("#lbl_registrador").html('Embajador');

    //activar para Pendientes y Recepcionado
    if (obj.estado_id == 7 || obj.estado_pretemporal_id == 1) {
        $("#tab_gestion_modal").css("display","block");
        $('#tab_1').addClass("active");
        $('.tab_1').addClass("active");
    } else {
        $("#tab_gestion_modal").css("display","none");
        $('#tab_2').addClass("active"); $('.tab_2').addClass("active");
    }
    //mensaje de averia registrada
    if (obj.codactu !== '') {
        if (obj.validado == 1) {
            $("#mensaje_modal").css("display","block");
            $("#mensaje_modal").html('<h4>Cuenta con Avería Registrada.</h4>');
            $('#mensaje_modal').addClass("alert-success").removeClass("alert-warning");
        } else {
            $("#mensaje_modal").css("display","block");
            $("#mensaje_modal").html('<h4>La avería no ha sido validada aún.</h4>');
            $('#mensaje_modal').addClass("alert-warning").removeClass("alert-success");
        }
    } else 
        $("#mensaje_modal").css("display","none");
    if ((obj.codactu).trim()) {
        $("#tab_datos_averia_modal").css("display","block");
    }
    $('#slct_solucionado_modal').multiselect('destroy');
    slctGlobal.listarSlct('pretemporal_solucion','slct_solucionado_modal','simple',[], {actividad_tipo: obj.actividad_tipo_id});
    $("#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal").multiselect("clearSelection");
    $("#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal").multiselect("refresh");

    $.each(obj, function(key, value){
        $('#ticketForm [name='+key+']').val(value);
    });
    $('#f_comentario').val(obj.comentario);
    $('#asunto').val('AD101 - '+obj.quiebre+
        ' / Cli: '+ obj.codcli +
        ' / Av: '+ obj.codactu +
        ' / Ticket:'+ obj.ticket);
    $('#quiebre_cat').html(obj.quiebre);
    //averia
    $.each(obj, function(key, value){
        $('#averiaForm [name='+key+']').val(value);
    });
    tipoValidacion();
};
eventoSlctGlobalSimple=function(slct,valores){ 
    var m=""; var s=""; var val=""; var dval="";
    if(slct=="slct_estado_modal"){
        M="M"+$("#slct_motivo_modal").val();
        S="S"+$("#slct_submotivo_modal").val();
        //validacion - solucionado
        if (M == "M23" && S == "S58") {
            $("#form_bandeja .solucionado").css('display', '');
        } else {
            $("#form_bandeja .solucionado").css('display', 'none');
        }
        //validacion - 60: se registra Averia - 75: tiene reg averia
        if (M == "M24" && S == "S60"|| M == "M24" && S == "S75") {
            $("#txt_codactu_modal").prop('readonly', false);
        } else {
            $("#txt_codactu_modal").prop('readonly', true);
        }
    }
};
upperCase =function(input) {
   var x=$(input).val();
   $(input).val(x.toUpperCase());
};
ListarMovimientos = function(datos) {
    var html="", con = 1;
    $('#t_movimiento').dataTable().fnDestroy();
    $("#tb_movimiento").html('');
    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td>"+con+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.observacion+"</td>"+
        "<td>"+data.fecha+"</td>"+
        "<td>"+data.usuario+"</td>"+
        "<td>"+data.solucionado+"</td>";
        html+="</tr>";
        con++;
    });
    $("#tb_movimiento").html(html);
    $('#t_movimiento').dataTable();
};
HTMLListarAveria=function(datos, id){
    var html="";
    $('#t_averia').dataTable().fnDestroy();
    $('#tb_averias').html= '';
    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td><input name='rad_codactu_val' type='radio' value='"+data.codactu+"|"+data.telefono+"|"+data.fecha_registro+"'></input></td>"+
        "<td>"+data.telefono+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.nombre_cliente+"</td>";
        html+="</tr>";
    });
    $("#tb_averias").html(html);
};
descargarReporte = function () {
    if (findTypeGLOBAL == "G") {
        $("#form_General").append("<input type='hidden' name='PG' id='PG' value='G'>");
        $("#form_General").append("<input type='hidden' name='excel' id='excel' value='excel'>");
        $("#form_General").submit();
    } else if (findTypeGLOBAL == "P") {
        $("#form_Personalizado").append("<input type='hidden' name='PG' id='PG' value='P'>");
        $("#form_Personalizado").append("<input type='hidden' name='excel' id='excel' value='excel'>");
        $("#form_Personalizado").submit();
    }
};
descargarMovimientoReporte = function () {
    $("#form_Movimientos").submit();
};

printCheks = function(){
    $.each(slct_indivual,function(key,value){
        $('.chkSelect[value='+value+']').prop('checked',true);
    });
}

reasignacion = function(){
    /*get data to re-assig throught form*/
    var datos=$("#form_reasignacion").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");
    /*end get data to re-assig throught form*/

    /*get orders to re-assig*/
    if(checks != ''){
        pedidos = checks;        
    }else{
        pedidos = slct_indivual;
    }
    /*end get orders to re-assig*/

    /*validations*/
    if($.trim($("#slct_tiporea").val()) == ''){
        swal('Seleccione destino asignacion');
    }else if($.trim($("#slct_tiporea").val()) == 1 && $.trim($("#slct_usuario").val()) == ''){
        swal('Seleccione personas para re asignar');
    }else if($.trim(pedidos) == ''){
        swal('Seleccione pedidos a re asignar');
    }else{
        datos+='&pedidos='+pedidos.join(',');
        Pretemporales.Reasignar(datos);
    }
    /*end validations*/
}

</script>
