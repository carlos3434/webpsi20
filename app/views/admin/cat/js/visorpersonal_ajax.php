<script type="text/javascript">
    var reporte_id, reporteObj;
    var Reporte = {
    	grafica: function (tipo){
    		$.ajax({
                url         : 'pretemporal/grafica',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {tipo:tipo},
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();
                    graficarPie(data);
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error")
                }
            });
    	},
        visor: function (estado_id,quiebre_id = ''){
            $.ajax({
                url         : 'pretemporal/visor',
                type        : 'POST',
                data        : {estado_id: estado_id,quiebre_id: quiebre_id},
                cache       : false,
                dataType    : 'json',
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        ListarVisor(obj, obj.estado_id);
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error")
                }
            });
        },
        visorRecepcionados: function (quiebre = ''){
            $.ajax({
                url         : 'pretemporal/visor-recepcionados',
                type        : 'POST',
                data: {quiebre_id: quiebre},
                cache       : false,
                dataType    : 'json',
                success : function(obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        ListarRecepcionados(obj);
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error");
                }
            });
        },
        visorPersonal: function (data = ''){
            $.ajax({
                url         : 'usuarioarea/getasignaciones',
                type        : 'POST',
                data        : data,
                cache       : false,
                dataType    : 'json',
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                       HTMLVisorPersonal(obj.datos,obj.cabecera);
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error");
                }
            });
        }
    };
</script>
