<script type="text/javascript">

var Administrador={
    cargar: function(){
        $.ajax({
            url         : 'pretemporal/cargaradmin',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(data) {
                eventoCargaRemover();
                HTMLCargar(data.datos);
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    codactu: function(datos){
        /*datos=$("#form_configuracion").serialize().split("txt_").join("").split("slct_").join("");*/
        $.ajax({
            url         : 'pretemporal/configuracion',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                    Administrador.cargar();
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }
};
</script>

