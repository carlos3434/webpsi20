<script type="text/javascript">
    //js
$(document).ready(function() {

    slctGlobal.listarSlct('listado', 'slct_quiebre', 'multiple', null);
     $("#fecha_registro").daterangepicker({
        format: 'YYYY-MM-DD',
        endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
    });

     $('#fecha_registro').val(
        moment().format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );

    Reporte.visorPersonal();
});

ListarVisor = function(obj, estado_id) {
    var html = "";
    $.each(obj.motivo,function(index,data){
        html+="<tr>";
        html+="<td style='vertical-align:middle;' rowspan="+data.row_count+"><p>"+data.motivo+"</p></td>";
        $.each(obj.submotivo,function(index,data2){
            if (data2.motivo_id == data.motivo_id) {
                html+="<td>"+data2.submotivo+"</td>";
                html+="<td>"+data2.f0+"</td>";
                html+="<td>"+data2.f1+"</td>";
                html+="<td>"+data2.f2+"</td>";
                html+="<td>"+data2.f3+"</td>";
                html+="<td>"+data2.f4+"</td>";
                html+="<td>"+data2.f5+"</td>";
                html+="<td>"+data2.f6+"</td>";
                html+="<td>"+data2.f7+"</td>";
                html+="<td style='background-color: #F7F7E2;'>"+data2.total_submotivo+"</td>";
                html+="</tr>";
            }
        });
    });

    if (estado_id == 7) {
        $("#tb_visoroperaciones").html(html);
    } else if (estado_id == 17) {
        $("#tb_visor2").html(html);
    }
};

ListarRecepcionados = function(obj) {
    var html = "";
    $.each(obj.recepcionados,function(index,data){
        html+="<tr>";
        html+="<td>"+data.nombre+"</td>";
        html+="<td>"+data.f0+"</td>";
        html+="<td>"+data.f1+"</td>";
        html+="<td>"+data.f2+"</td>";
        html+="<td>"+data.f3+"</td>";
        html+="<td>"+data.f4+"</td>";
        html+="<td>"+data.f5+"</td>";
        html+="<td>"+data.f6+"</td>";
        html+="<td>"+data.f7+"</td>";
        html+="<td style='background-color: #F7F7E2;'>"+data.total+"</td>";
        html+="</tr>";
    });
    $("#minuto10").val(obj.recepcionados_horas.diez);
    $("#minuto30").val(obj.recepcionados_horas.treinta);
    $("#minuto60").val(obj.recepcionados_horas.sesenta);

    $("#tb_visorgestion").html(html);
};

HTMLVisorPersonal = function(data,cabecera){
        html = '';
        head = '';
        $(".visorpersonal").dataTable().fnDestroy();
        /*header*/
        head+=' <tr>';
        head+=' <th></th>';
        $.each(cabecera,function(index, el) {
            head+=' <th colspan="2">'+el+'</th>';       
        });
        head+=' </tr>';

        head+=' <tr>';
        head+=' <th>Usuario</th>';
        $.each(cabecera,function(index, el) {
            head+=' <th>Asignados</th>';
            head+=' <th>Atendidos</th>';       
        });         
        head+=' </tr>';
        /*end header*/

        /*body*/      
        $.each(data,function(key, value) {
            html+='<tr>';
            html+=' <td>'+value.usuario+'</td>';

            var map = [];
            $.each(value,function(index, el) {
                map[index] = el;
            });

            for(var i=1;i<= cabecera.length - 1;i++){
                if((map['at'+i] == 0 && map['a'+i] > map['at'+i]) || (map['at'+i] == 0 && map['a'+i] == 0)){ //not start or not was assig some orders
                    html+=' <td style="background-color:rgba(245, 105, 84, 0.65)">'+map['a'+i]+'</td>';
                    html+=' <td style="background-color:rgba(245, 105, 84, 0.65)">'+map['at'+i]+'</td>';  
                }else if(map['a'+i] > map['at'+i] && map['at'+i] > 0){ // in process
                    html+=' <td style="background-color:rgba(243, 156, 18, 0.62)">'+map['a'+i]+'</td>';
                    html+=' <td style="background-color:rgba(243, 156, 18, 0.62)">'+map['at'+i]+'</td>';
                }else if (map['a'+i] == map['at'+i]){
                    html+=' <td style="background-color:rgba(0, 166, 90, 0.86)">'+map['a'+i]+'</td>';
                    html+=' <td style="background-color:rgba(0, 166, 90, 0.86)">'+map['at'+i]+'</td>';  
                }                         
            }
            html+=' <td>'+value.total+'</td>';
            html+=' <td>'+value.total+'</td>';
            html+='</tr>';
        });
        /*end body*/

        $(".visorpersonal tbody").html(html);
        $(".visorpersonal thead").html(head);
        $(".visorpersonal").dataTable();
}

searchbydate = function(){
    data = {};
    var date = $("#fecha_registro").val();
    if(date){
        data.fecha = date;
    }
    if($.trim($("#slct_quiebre").val()) != ''){
        data.quiebre_id = $("#slct_quiebre").val();
    }

    if(data){
        Reporte.visorPersonal(data);
    }
}

</script>

