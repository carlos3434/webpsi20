<script type="text/javascript">
    $(document).ready(function () {
        slctGlobal.listarSlct('eventomasivo', 'slct_eventos', 'simple', null,{selector:1});

        cargarInfo();
        $("#archivocolumna").fileinput({
            showUpload: false,
            //allowedFileExtensions: ["txt", "csv"],
            previewClass: "bg-warning",
            showCaption: true,
            showPreview: false,
            showRemove: false,
            browseLabel: 'Examinar'
        });

        $('#fecha').daterangepicker({
            format: 'YYYY-MM-DD',
            minDate: moment().subtract(35, 'days'),
            maxDate:  moment(),
            startDate: moment().subtract(15, 'days'),
            endDate: moment(),
        });

        $('#archivocolumna').on('change', prepareUploadColumna);

         $('.chkTipoCarga').on('ifChanged', function(event){
            var valor = event.target.value;
            var checked = event.target.checked;
            if(valor==1){
                if(checked == true){
                    $(".normal").removeClass('hidden');
                    $(".masivaevento").addClass('hidden');
                    $('#chk_masiva').parent().removeClass('checked').attr('aria-checked',false); 
                }else{
                     $(".normal").addClass('hidden');
                }
           }else if(valor==2){
             if(checked == true){
                    $(".masivaevento").removeClass('hidden');
                    $(".normal").addClass('hidden');
                    $('#chk_normal').parent().removeClass('checked').attr('aria-checked',false);
                }else{
                     $(".masivaevento").addClass('hidden');
                }               
           }
        });

        $(document).on('click', '#mostrar', function(event) {
            if($.trim($("#fecha").val()) != ''){
                PreTemporalMasiva.listarLog();
            }else{
                swal('Seleccione un rango de fechas');       
            }
        });


        $(document).on('click', '.btnSaveALl', function(event) {
            var confirmed = confirm("¿Seguro de cargar todo?");
            if (confirmed) {
                var data =[];
                var cont = 0;
                var registros_group = [];

                var evento = '';
                if($.trim($("#slct_eventos").val()) !== ''){ // && its checked chk_masiva
                    evento = $("#slct_eventos").val();
                }

                $("#tblData tbody tr").each(function(key,el){
                    //if(typeof $(el).attr('class') == 'undefined'){
                        cont+=1;
                        var td = $(el).find('td');
                        var object = {};
                        $.each(td,function(index, el) {
                            if(el.id != 'EXTRA' && el.id != 'EVENTO'){
                                object[el.id] = el.innerHTML;                            
                            }
                        });
                        data[cont - 1] = object;                    
                    //}
                });

                if(data.length > 0){
                    var i,j,temparray,chunk = 48;
                        for (i=0,j=data.length; i<j; i+=chunk) {
                            temparray = data.slice(i,i+chunk);
                            registros_group.push(JSON.stringify(temparray));
                        }
                    PreTemporalMasiva.procesarMasivo(registros_group,evento);                           
                }           
            }   
        });

        $(document).on('click', '.btnEdit', function(event) {
            var tds = $(this).parent().parent().find('td');
            var template = $("#tblData tfoot").find('.repeat').clone().removeClass('repeat').removeClass('hidden');
            $.each(tds,function(index, el) {
                template.find('td[id="'+el.id+'"] input').val(el.innerHTML);                
            });            
            $(this).parent().parent().replaceWith(template);
        });

        $(document).on('click', '#btnAdd', function(event) {
            event.preventDefault();           
            var template = $("#tblData tfoot").find('.repeat').clone().removeClass('repeat').removeClass('hidden');
            $("#tblData tbody").append(template);
        });

        $(document).on('click', '.btnRemove', function(event) { 
            var confirmed = confirm("¿Desea Eliminar el Registro de la Carga?");
            if (confirmed) {
               $(this).parent().parent().remove();
            }     
        });

        $(document).on('click','.btnAgregar',function(event){
            var tr = $(this).parent().parent().find('td');
            if(validateFieldsRequired(tr)){
                var html = '<tr>';            
                $.each(tr,function(index, el) {
                    var value = $(el).find('input');
                    if(el.id == 'EXTRA'){
                        html+='<td id="'+el.id+'" style="display:flex"><span class="btn btn-danger btn-xs btnRemove"><i class="glyphicon glyphicon-remove"></i></span>'
                        html+=' &nbsp&nbsp<span class="btn btn-warning btn-xs btnEdit"><i class="glyphicon glyphicon-pencil"></i></span>';
                        html+='</td>';                        
                    }else{
                        html+='<td id="'+el.id+'">'+value.val()+'</td>';
                    }
                });            
                html+= '</tr>';
                $(this).parent().parent().replaceWith(html);                
            }
        });
        


        /*Upload Files Excel*/
        $('#btnSubirExcel').click(function() {
            $('#btnHiddenExcel input').click();
            return false;
        });

         $("#btnHiddenExcel").ajaxUpload({
            action: 'pretemporal/uploadexcel',
            url: 'pretemporal/uploadexcel',
            name: 'excel',
            onSubmit: function(file, extension) {
                $('#btnSubirExcel').html('Cargando...').prop('disabled', true);
            },
            onComplete: function(result) {
                var data = JSON.parse(result);
                if(data.rst == 1 && data.data != ''){
                    poblateMasivo(data.data);

                    /*errors*/
                    /*html = '';
                    $.each(data.errors,function(index, el) {
                        if(el > 0){
                            html+=index+":"+el+" ";
                        } 
                    });                    
                    if(html){swal('Errores:'+html);}*/
                    /*end errors */
                    
                    $('#btnSubirExcel').html('Listo').prop('disabled', true);
                }else{
                    swal('Error al cargar la data');
                }
            }
        });
    
        $("#btnHiddenExcel").click(function(e){
            $("#btnHiddenExcel").css('background-color', '#2496e2');
            $("#btnHiddenExcel").css('border-color', '#2496e2');
        });
        /*End to Upload Files Excel*/
    });

    validateFieldsRequired = function(elementSearch){
        var search = ['QUIEBRE','ATENCION','FUENTE','TIPOAVERIA'];/*do dinamic searching data */
        var array_rst = [];

        $.each(elementSearch,function(index, el) {
            var rst = $.inArray(el.id, search);
            if(rst !== -1){
                if($(el).find('input').val() == ''){
                    array_rst.push(el.id);
                }                
            }
        });        
        if(array_rst.length > 0){
            swal('Complete '+array_rst.join(', ')+' por favor');
            return false;            
        }
        return true;
    }

    poblateMasivo = function(data){
        var html = '';
        var evento = '';
        if($.trim($("#slct_eventos").val()) !== ''){ // && its checked chk_masiva
            evento = $('#slct_eventos option[value="'+$("#slct_eventos").val()+'"]').text();
        }

        $.each(data,function(index, el) {
            if($.trim(el.QUIEBRE) == '' || $.trim(el.ATENCION) == '' || $.trim(el.FUENTE) == '' || $.trim(el.TIPOAVERIA) == ''|| el.validate > 0){
                html+='<tr class="danger">';
            }else{
                html+='<tr>';
            }

            html+=' <td id="quiebre_id">'+el.QUIEBRE+'</td>';
            html+=' <td id="tipo_atencion">'+el.ATENCION+'</td>';
            html+=' <td id="tipo_fuente">'+el.FUENTE+'</td>';
            html+=' <td id="codactu">'+el.CODACTU+'</td>';
            html+=' <td id="codcli">'+el.CODCLI+'</td>';
            html+=' <td id="actividad_tipo_id">'+el.TIPOAVERIA+'</td>';
            html+=' <td id="cliente_nombre">'+el.CLIENTENOMBRE+'</td>';
            html+=' <td id="cliente_celular">'+el.CLIENTECELULAR+'</td>';
            html+=' <td id="cliente_telefono">'+el.CLIENTETELEFONO+'</td>';
            html+=' <td id="cliente_correo">'+el.CLIENTECORREO+'</td>';
            html+=' <td id="cliente_dni">'+el.CLIENTEDNI+'</td>';
            html+=' <td id="contacto_nombre">'+el.CONTACTONOMBRE+'</td>';
            html+=' <td id="contacto_celular">'+el.CONTACTOCELULAR+'</td>';
            html+=' <td id="contacto_telefono">'+el.CONTACTOTELEFONO+'</td>';
            html+=' <td id="contacto_correo">'+el.CONTACTOCORREO+'</td>';
            html+=' <td id="contacto_dni">'+el.CONTACTODNI+'</td>';
            html+=' <td id="embajador_nombre">'+el.EMBAJADORNOMBRE+'</td>';
            html+=' <td id="embajador_correo">'+el.EMBAJADORCORREO+'</td>';
            html+=' <td id="embajador_celular">'+el.EMBAJADORCELULAR+'</td>';
            html+=' <td id="embajador_dni">'+el.EMBAJADORDNI+'</td>';
            html+=' <td id="comentario">'+el.COMENTARIO+'</td>';
            html+=' <td id="EVENTO">'+evento+'</td>';
            html+=' <td id="fh_reg104">'+el.FH_REG104+'</td>';
             
            html+=' <td id="fh_reg1l">'+el.FH_REG1L+'</td>';

            html+=' <td id="fh_reg2l">'+el.FH_REG2L+'</td>';
            html+=' <td id="cod_multigestion">'+el.CODMULTIGESTION+'</td>';

            html+=' <td id="llamador">'+el.LLAMADOR+'</td>';
            html+=' <td id="titular">'+el.TITULAR+'</td>';
            html+=' <td id="direccion">'+el.DIRECCION+'</td>';
            html+=' <td id="distrito">'+el.DISTRITO+'</td>';
            html+=' <td id="urbanizacion">'+el.URBANIZACION+'</td>';
            
            html+=' <td id="telf_gestion">'+el.TELF_GESTION+'</td>';
            html+=' <td id="telf_entrante">'+el.TELF_ENTRANTE+'</td>';
            html+=' <td id="operador">'+el.OPERADOR+'</td>';
            html+=' <td id="motivo_call">'+el.MOTIVO_CALL+'</td>';
            html+=' <td id="EXTRA" style="display:flex">';
            html+='     <span class="btn btn-danger btn-xs btnRemove"><i class="glyphicon glyphicon-remove"></i></span>';
            html+='     &nbsp&nbsp<span class="btn btn-warning btn-xs btnEdit"><i class="glyphicon glyphicon-pencil"></i></span>';
            html+=' </td>';
            html+='</tr>';
        });
        $("#tblData tbody").html(html);
    }

    prepareUploadColumna = function (event) {
        files = event.target.files;
        event.stopPropagation();
        event.preventDefault();
        $.each(files, function (key, value)
        {
            var data = new FormData();
            data.append('archivocolumna', value);
            PreTemporalMasiva.subirArchivo(data);
        });
    };


    cargarInfo = function(){
        var search = [{'name':'quiebre','url':'quiebre/listbygroup'},{'name':'atencion','url':'pretemporal_atencion/listar'},{'name':'fuente','url':'pretemporal_rrss/listar'},{'name':'tipo_averia','url':'actividadtipo/listar','data':{apocope:1}}];
        $.each(search,function(index, el) {
            PreTemporalMasiva.getInformation(el.url,el.name,el.data);
        });
    }

    cargarHTML = function(data,name){
        var html = '';
            html+='<p><h4><i>'+name.toUpperCase()+'</i></h4></p>';
            $.each(data,function(index, el) {
                    html+= '<b class="text-primary">'+el.nombre+'</b><br>';                                    
            });
            $("."+name).html(html);
    }

    cargaMasivaEvento = function(){
        if($.trim($("#slct_eventos").val()) !== ''){
            PreTemporalMasiva.getMasiva();            
        }else{
            swal('Seleccione Evento');
        }
    }

</script>

