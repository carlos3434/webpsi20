<script type="text/javascript">

$(document).ready(function() {	

	slctGlobal.listarSlct('empresa', 'slct_empresa_id', 'simple', null);
	slctGlobal.listarSlct('troba', 'slct_troba', 'simple', null,{estado: 1});
	slctGlobal.listarSlct('nodo', 'slct_nodo', 'simple', null);
	slctGlobal.listarSlct('zonal', 'slct_zonal', 'simple', null,{estado:1,usuario:1});
	slctGlobalHtml('slct_tipo_trabajo','simple');
	slctGlobalHtml('slct_tecnologia','simple');

    $("#fechas_busqueda").daterangepicker({
        format: 'YYYY-MM-DD',
        endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
        },function (startDate, endDate, period) {
            var diff = daydiff(parseDate(startDate.format('L')), parseDate(endDate.format('L')));
            if(diff > 45){
                var last = parseDate(startDate.format('L'));
                last.setDate(last.getDate() + 45);
                $('#fechas_busqueda').val(
                    moment(startDate).format("YYYY-MM-DD") + ' - ' +
                    moment(last).format("YYYY-MM-DD")
                );          
                swal('El rango es maximo de 45 días');
            }
    });


    var today = moment();
    $('#fechas_busqueda').val(
        moment(today).subtract(3, 'days').format("YYYY-MM-DD")  + ' - ' +  moment().format("YYYY-MM-DD")
    );

	listado.cargar({fechas_busqueda : $("#fechas_busqueda").val()});
	$('#btn-buscar').on('click', function(){
		let fechas = $("#fechas_busqueda").val();
		listado.cargar({fechas_busqueda : fechas});
	});

	$('#btn-reporte').on('click', function(ev){
		ev.preventDefault();
		if($("#fechas_busqueda").val() == '' ){
			swal('Debe seleccionar una fecha');
		} else {
			descargarReporte();
		}
	});

    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }
    /*$('#fechas_busqueda').val(
        moment().format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );*/
	
	$(document).on('click', '.openFile', function(event) {
		var target = $(this).attr('target_f');
		$("#"+target).trigger('click');
	});

	 $(".file").change(function() {
	    readURLI(this, 'img',$(this).attr('numero'));
	});

	$("form[name='frmCrearmasiva']").submit(function(e) {
        e.preventDefault();

        if($.trim($("#slct_nodo").val()) == ""){
        	swal('Seleccione nodo');
        }else if($.trim($("#slct_troba").val()) == ""){
        	swal('Seleccione troba');
        }else if($.trim($("#slct_empresa_id").val()) == ""){
        	swal('Seleccione empresa');
        }else if($("#txt_codliquidacion").val() == "" || $("#txt_subcodliqui").val() == "" ){
        	swal('Complete sub y cod liquidacion');
        }else if($.trim($("#slct_tipo_trabajo").val()) == ""){
        	swal('Seleccione tipo trabajo');
        }else if($.trim($("#slct_tecnologia").val()) == ""){
        	swal('Seleccione tecnologia');
        }else if($("#txt_zonal").val() == ""){
        	swal('Complete zonal');
        }else if($("#txt_carnet").val() == ""){
        	swal('Complete carnet tecnico');
        }else{
	        $.ajax({
	            type: "POST",
	            url: 'eventomasivo/guardar',
	            data: new FormData($(this)[0]),
	            processData: false,
	            contentType: false,
	            success: function (obj) {
	                if(obj.rst==1 || obj.rst==2){                  
	                	swal('Mensaje',obj.msj,'success');
	                	$("#txt_nombre").val(obj.codgenerado);
	                	$("#frmCrearmasiva input[type='hidden'],#frmCrearmasiva input[type='text'],#frmCrearmasiva input[type='file'],#frmCrearmasiva select,#frmCrearmasiva textarea").prop('disabled',true);
	                	$(".buscarAverias").removeClass('hidden');
	                	var data = {id:obj.id,nodo:obj.nodo,troba:obj.troba};
	                	if(obj.rst ==2){
	                		listado.cargar();
	                	}else{
			       			listado.getMasiva(data);	                		
	                	}
	                	$("#btn_guardar").addClass('hidden');	                	
	                }else if(obj.rst == 3){
                        swal('Mensaje','¡Ya existe la combinacion de nodo y troba!','warning');
                    }else{
                        swal('Mensaje','¡Error al realizar acción!','warning');
                    }
	            }
	        });
    	}
    });

    $('#nuevoEvento').on('hide.bs.modal', function (event) {
    	$("#frmCrearmasiva input[type='hidden'],#frmCrearmasiva input[type='text'],#frmCrearmasiva input[type='file'],#frmCrearmasiva select,#frmCrearmasiva textarea").prop('disabled',false);
      	$("#frmCrearmasiva input[type='hidden'],#frmCrearmasiva input[type='text'],#frmCrearmasiva input[type='file'],#frmCrearmasiva select,#frmCrearmasiva textarea").not('.mant').val("");
		$('#frmCrearmasiva select').multiselect('refresh');
		$(".buscarAverias").addClass('hidden');
	    $("#btn_guardar").removeClass('hidden');
	    $("#txt_nombre").attr('disabled',true);
	    $(".liquidacion").addClass('hidden');
    });
    
    $("#cancelMasiva").on('hide.bs.modal', function (event) {       
        $("#formCancelacion input[type='hidden'],#formCancelacion input[type='text'],#formCancelacion input[type='file'],#formCancelacion select,#formCancelacion textarea").not('.mant').val("");    
    });
});


exportCodigoCli = function(){
	var idevento = $("#txt_idevento").val();
	if(idevento){		
	 	window.location='eventomasivo/exportcodcli?idevento='+idevento;
	}else{
		swal('error al exportar');
	}
}

exportTelefono = function(){
	var idevento = $("#txt_idevento").val();
	if(idevento){		
	 	window.location='eventomasivo/exportelefono?idevento='+idevento;
	}else{
		swal('error al exportar');
	}
}


function readURLI(input, tipo,numero) {
    if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                if (tipo == 'img') {
                    $('#txt_image'+numero).val(e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


HTMLUsuariosValidar = function(data){
	if(data.length > 0){
		var html = '';
		$('#tblValidaciones').dataTable().fnDestroy();
		$.each(data,function(index, el) {
			html+='<tr>';
			html+='<td>'+el.usuario+'</td>';
			html+='<td>'+el.dni+'</td>';
			html+='<td>'+el.nombre+'</td>';
			html+='<td>'+el.celular+'</td>';
			html+='<td><input type="checkbox" id="chk_validado" name="chk_validado"/></td>';
			html+='</tr>';
		});
		$("#tblValidaciones tbody").html(html);
		$("#tblValidaciones").dataTable();
	}
}

HTMLEventos = function(data){
	var html = '';
	$('#table_reporte').dataTable().fnDestroy();
	$.each(data,function(index, el) {
		var calc = 0;
		if(el.cantickets != 0 && el.atendidos !=0){
			if(el.cantickets != el.atendidos){
				calc = Math.round((el.atendidos / el.cantickets) * 100);	
			}else{
				calc = 100;	
			}
		}
		estado = '';
		if(el.estado != 2 && el.estado != 7){
			if(el.estado == 1){
				estado = '<span class="btn btn-success btn-sm estado" onClick="cambiarEstado('+el.id+',8)">Recepcionado</span>';
			}else if(el.estado == 8){
				estado = '<span class="btn btn-danger btn-sm estado" onClick="cambiarEstado('+el.id+',1)">Cerrado</span>';
			}
		}else{
			if(el.estado == 2){
				estado = '<span class="btn btn-success btn-sm estado" style="opacity:0.5">Validado</span>';
			}else if(el.estado == 7){
				estado = '<span class="btn btn-success btn-sm estado" style="opacity:0.5">En Proceso</span>';					
			}
		}

		if(el.estado == 2){
			html+='<tr class="success">';
		}else{
			html+='<tr>';
		}       
		
		html+='<td><a href="/admin.cat.pretemporal?c='+el.id+'" target="_blank">'+el.nombre+'</a></td>';
		html+='<td>'+el.nodo+'</td>';
		html+='<td>'+el.troba+'</td>';
		html+='<td>'+el.empresa+'</td>';
		html+='<td>'+el.userCreated+'</td>';
		html+='<td>'+el.fecharegistro+'</td>';

        if(el.registro_franqueo){
            html+='<td>'+el.usuario_liquida+'</td>';            
        }else{
            html+='<td></td>';
        }

        html+='<td>'+el.registro_franqueo+'</td>';
		html+='<td>'+el.estadoActual+'</td>';
		html+='<td>'+el.cantickets+'</td>';
		html+='<td>'+el.atendidos+'</td>';
        html+='<td>'+el.con_servicio+'</td>';
        html+='<td>'+el.sin_servicio+'</td>';
		html+='<td>'+calc+"%"+'</td>';

        if(el.estado !== 12 && el.editar == 1){
            html+='<td><span class="btn btn-primary btn-sm tdetalles" idevento="'+el.id+'" onClick="cargarInfo(this)"><i class="glyphicon glyphicon-th-list"></i></span></td>';
        }else{
             html+='<td><span class="btn btn-primary btn-sm tdetalles" idevento="'+el.id+'" style="opacity:0.5"><i class="glyphicon glyphicon-th-list"></i></span></td>';
        }

        html+='<td><span class="btn btn-primary btn-sm tadetalles" idevento="'+el.id+'" images="'+el.imagenes+'" onClick="cargaDetalles(this)">Averias</span></td>';

        if((el.estado == 11 || el.estado == 1 || el.estado == 2) && el.editar == 1){
            html+='<td><span class="btn btn-danger btn-sm cancel" idevento="'+el.id+'" onClick="cancelar(this)"><i class="glyphicon glyphicon-trash"></i></span></td>';            
        }else{
            html+='<td><span class="btn btn-danger btn-sm cancel" style="opacity:0.5"><i class="glyphicon glyphicon-trash"></i></span></td>';
        }
		html+='</tr>';
	});
	$("#tb_reporte").html(html);
	$('#table_reporte').dataTable({
		 "order": [[ 5, 'desc' ]]
	});
}

cargaDetalles= function(object){
	var id = object.getAttribute('idevento');
	var imagenes =object.getAttribute('images');
	if(id){
		data = {idevento:id};
		listado.getTicketsbyEvent(data,imagenes);
	}else{
		swal('No cuenta con registros');
	}
}

HTMLTickets = function(data,imagenes){
	if(data.length > 0){
		var html = '';
		$('#tb_tickets').removeClass('hidden');
		$('#tb_tickets').dataTable().fnDestroy();
		$("#txt_idevento").val(data[0].masivaid);
		$.each(data,function(index, el) {
			html+='<tr>';
			html+='<td>'+el.quiebre+'</td>';
			html+='<td>'+el.codcli+'</td>';
			html+='<td>'+el.codaveria+'</td>';
			html+='<td>'+el.cliente_nombre+'</td>';	
			html+='<td>'+el.estado+'</td>';
			html+='<td>'+el.cliente_celular+'</td>';
			html+='<td>'+el.cliente_telefono+'</td>';
			html+='<td>'+el.contacto_celular+'</td>';
			html+='</tr>';
		});
		$("#tbody_tickets").html(html);
		$('#tb_tickets').dataTable();
	}else{
		$('#tb_tickets').dataTable().fnDestroy();
		$('#tb_tickets').addClass('hidden');
	}

	if(imagenes != 'null'){
		var src = '';
		var cont=0;
		$.each(imagenes.split(","),function(index, el) {
			cont+=1;
			$('#img'+cont).attr('src','uploads/image_evento_masiva/'+el);
			$('#img'+cont).addClass('img-evento');
		});
	}else{
		$("#img1,#img2,#img3").attr('src','');
		$("#img1,#img2,#img3").removeClass('img-evento');
	}
	$("#detalleEvent").modal('show');
}

HTMLDetalle = function(data){
	if(data){
		UsuarioId='<?php echo Auth::id(); ?>';
		document.querySelector('#txt_nombre').value = data.nombre;
		document.querySelector('#slct_tecnologia').value = data.tecnologia;
		document.querySelector('#slct_empresa_id').value = data.empresa_id;
		document.querySelector('#txt_tecnico_liquidacion').value = data.tecnico_liquidacion;
		document.querySelector('#txt_cod_liquidacion').value = data.cod_liquidacion;
		document.querySelector('#txt_sub_cod_liquidacion').value = data.sub_cod_liquidacion;
		document.querySelector('#slct_tipo_trabajo').value = data.tipo_trabajo;
		document.querySelector('#slct_zonal').value = data.zonal;
		document.querySelector('#slct_troba').value = data.troba;
		document.querySelector('#slct_nodo').value = data.nodo;
		document.querySelector('#txt_amplificador').value = data.amplificador;
		document.querySelector('#txt_observacion').value = data.observacion;
		$('#frmCrearmasiva select').multiselect('refresh');
		$("#frmCrearmasiva input[type='hidden'],#frmCrearmasiva input[type='text'],#frmCrearmasiva input[type='file'],#frmCrearmasiva select,#frmCrearmasiva textarea").prop('disabled',true);
		$("#frmCrearmasiva #txt_idevento").remove();
		$("#frmCrearmasiva").append('<input type="hidden" value="'+data.id+'" name="txt_idevento" id="txt_idevento">');

		if(data.cod_franqueo){
			document.querySelector('#txt_cod_franqueo').value = data.cod_franqueo;
			document.querySelector('#txt_fechaliqui').value = data.registro_franqueo;
            document.querySelector('#txt_obs_liquidacion').value = data.obs_liquidacion;
            $('.liquidacion textarea[name="txt_obs_liquidacion"]').prop('disabled', false);
		}else{
			var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
			document.querySelector('#txt_fechaliqui').value = utc;
            $('.liquidacion input[name="txt_cod_franqueo"],.liquidacion input[name="txt_image4"],.liquidacion input[type="file"],.liquidacion textarea[name="txt_obs_liquidacion"]').prop('disabled', false);
		}		
	
		$("#btn_guardar").prop('disabled', false);			
		$(".liquidacion").removeClass('hidden');
		$("#nuevoEvento").modal('show');
	}else{
		swal('Error, al cargar información');
	}
}

cambiarEstado = function(id,estado){
	listado.CambiarEstado({id:id,estado:estado});
}

cargarAverias = function(obj){
	var data = {id:obj.getAttribute('id-evento'),nodo:obj.getAttribute('nodo'),troba:obj.getAttribute('troba')};
    if(data != ''){
    	var r = confirm("¿Esta seguro de cargar las averias asociadas?");
		if (r == true) {
       		listado.getMasiva(data);
		}
    }else{
    	swal('no existe codigo de evento');
    }
}

cargarInfo = function(object){
	var id_evento = object.getAttribute('idevento');
	if(id_evento){
		listado.cargar({idevento : id_evento});		
	}else{
		swal('no cuenta con id evento');
	}
}

descargarReporte = function () {
	$("#form_Personalizado").append("<input type='hidden' name='PG' id='PG' value='P'>");
    $("#form_Personalizado").append("<input type='hidden' name='excel' id='excel' value='excel'>");

    $("#form_Personalizado").submit();
};

cancelar = function(obj){
   $("#txt_idevento").val(obj.getAttribute('idevento'));
   $("#cancelMasiva").modal('show');
};

cancel = function(){
    var id_evento  =  $("#txt_idevento").val();
    var obs = $("#txt_observacion_can").val();
    if(id_evento && obs){
        listado.CambiarEstado({'idevento':id_evento,'obs_cancelacion':obs,'inactivo':1});
    }else{
        swal('Complete los campos, porfavor');
    }
};

</script>
