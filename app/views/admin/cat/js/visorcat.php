<script type="text/javascript">
    //js
$(document).ready(function() {

    slctGlobal.listarSlct('listado', 'slct_quiebre', 'simple', null);
    Reporte.visor(7);
    Reporte.visorRecepcionados();
    Reporte.visor(17);
});

ListarVisor = function(obj, estado_id) {
    var html = "";
    $.each(obj.motivo,function(index,data){
        html+="<tr>";
        html+="<td style='vertical-align:middle;' rowspan="+data.row_count+"><p>"+data.motivo+"</p></td>";
        $.each(obj.submotivo,function(index,data2){
            if (data2.motivo_id == data.motivo_id) {
                html+="<td>"+data2.submotivo+"</td>";
                html+="<td>"+data2.f0+"</td>";
                html+="<td>"+data2.f1+"</td>";
                html+="<td>"+data2.f2+"</td>";
                html+="<td>"+data2.f3+"</td>";
                html+="<td>"+data2.f4+"</td>";
                html+="<td>"+data2.f5+"</td>";
                html+="<td>"+data2.f6+"</td>";
                html+="<td>"+data2.f7+"</td>";
                html+="<td style='background-color: #F7F7E2;'>"+data2.total_submotivo+"</td>";
                html+="</tr>";
            }
        });
    });

    if (estado_id == 7) {
        $("#tb_visoroperaciones").html(html);
    } else if (estado_id == 17) {
        $("#tb_visor2").html(html);
    }
};

ListarRecepcionados = function(obj) {
    var html = "";
    $.each(obj.recepcionados,function(index,data){
        html+="<tr>";
        html+="<td>"+data.nombre+"</td>";
        html+="<td>"+data.f0+"</td>";
        html+="<td>"+data.f1+"</td>";
        html+="<td>"+data.f2+"</td>";
        html+="<td>"+data.f3+"</td>";
        html+="<td>"+data.f4+"</td>";
        html+="<td>"+data.f5+"</td>";
        html+="<td>"+data.f6+"</td>";
        html+="<td>"+data.f7+"</td>";
        html+="<td style='background-color: #F7F7E2;'>"+data.total+"</td>";
        html+="</tr>";
    });
    $("#minuto10").val(obj.recepcionados_horas.diez);
    $("#minuto30").val(obj.recepcionados_horas.treinta);
    $("#minuto60").val(obj.recepcionados_horas.sesenta);

    $("#tb_visorgestion").html(html);
};

searchbyQuiebre = function(){
    var quiebre = $("#slct_quiebre").val();
    Reporte.visorRecepcionados(quiebre);
    Reporte.visor(7,quiebre);
    Reporte.visor(17,quiebre);
}

</script>

