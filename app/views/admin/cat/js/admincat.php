<script type="text/javascript">

$(document).ready(function() {

    Administrador.cargar();

    $(".btn-guardar").click(function(){
        Administrador.codactu();
    });
});


HTMLCargar = function(data){
	if(data.length > 0){
		var html = '';
		$.each(data,function(index, el) {
			cantidad = (el.cantidad == null || el.cantidad == '') ? '' : el.cantidad;
			obligatorio = '<select class="form-control slct_obligatorio" id="slct_obligatorio" name="slct_obligatorio">';
			if(el.obligatorio == 1){
				obligatorio += '<option value="1" selected>Obligatorio</option>';
				obligatorio += '<option value="0">No Obligatorio</option>';					
			}else{
				obligatorio += '<option value="0" selected>No Obligatorio</option>';
				obligatorio += '<option value="1">Obligatorio</option>';				
			}
			obligatorio += '</select>';

			html+='<tr id-config='+el.id+'>';
			html+='<td>'+el.nombre+'</td>';
			html+='<td>'+obligatorio+'</td>';
			html+='<td><input class="form-control" type="text" name="txt_cantidad" id="txt_cantidad" value="'+cantidad+'"/></td>';
			html+='<td><span class="btn btn-primary btn-sm btnguardar" onclick="cambios(this)">Guardar Cambios <i class="glyphicon glyphicon-check"></i></span></td>';
			html+='</<tr>';
		});
		$("#tbody_config").html(html);
	}else{
		swal('No se encuentra data');
	}
}

cambios = function(obj){
	var tr = obj.parentNode.parentNode;
	var obligatorio = $(tr).find("#slct_obligatorio").val();
	var cantidad = $(tr).find("#txt_cantidad").val();
	var data ={};
	data.id = tr.getAttribute('id-config');
	if(obligatorio != ''){
		data.obligatorio = obligatorio;
	}
	if(cantidad != ''){
		data.cantidad = cantidad;
	}
	Administrador.codactu(data);
}
</script>

