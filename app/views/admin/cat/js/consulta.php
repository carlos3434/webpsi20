<script type="text/javascript">
GLOBAL = {};
var findTypeGLOBAL = '';

$(document).ready(function() {
    eventoCargaMostrar();
    $(".navbar-btn.sidebar-toggle").click();

    slctGlobalHtml('slct_tipo','simple');

    activarTabla();

    $("#slct_tipo").change(ValidaTipo);
    $("#btn_personalizado").click(personalizado);

     $('#consultaModal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        Consulta.Cargar(id);
        Consulta.Movimiento(id);
    });

    $('#consultaModal').on('hide.bs.modal', function (event) {
        $("#tab_datos_averia_modal").css("display","none");
        $("#content_averia").css('display', 'none');
        $("#averiaForm input[type='text']").val('');
        $("#ticketForm input[type='text']").val('');
        $("#txt_observacion_modal").val('');
    });

    setTimeout(function(){eventoCargaRemover();}, 1000);
});

personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        swal("Mensaje","Seleccione Tipo Filtro", "warning");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        swal("Mensaje","Ingrese datos", "warning");
        $("#txt_buscar").focus();
    } else{
        //$("#txt_buscar").val($.trim($("#txt_buscar").val()));
        Consulta.Listar("P");
    }

};

ValidaTipo=function(){
    $("#txt_buscar").val("");
    $("#txt_buscar").focus();
};
activarTabla=function(){
    $("#t_consulta").dataTable(); // inicializo el datatable
};

HTMLListarPretemporal=function(datos){
var html="";
    $('#t_consulta').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        color= '#F7BE81';
        if(data.estado_id == 1) color= '#FFFFFF';
        if(data.estado == 'Cerrado') color= '#A9BCF5';
        html+="<tr>"+
        "<td style='background-color:"+color+";'>"+data.ticket+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.fecha_registro+"</td>"+
        "<td>"+data.tipo_averia+"</td>"+
        "<td>"+data.codcli+"</td>"+
        "<td>"+data.quiebre+"</td>"+
        "<td>"+data.tipo_atencion+"</td>"+
        "<td>"+data.cliente_nombre+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.cod_multigestion+"</td>"+
        "<td>"+data.llamador+"</td>"+
        "<td>"+data.titular+"</td>"+
        "<td>"+data.motivo_call+"</td>"+
        "<td><button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#consultaModal' data-id='"+data.id+"'><i class='fa fa-search fa-lg'></i></button></td>";
        html+="</tr>";
    });
    $("#tb_consulta").html(html);
    activarTabla();
};

ListarDataModal=function(obj){
    //validar permisos del quierbre
    $("#txt_id_modal").val(obj.ficha.id);
    $("#txt_codactu_modal").val(obj.ficha.codactu);
    $("#txt_codcli_modal").val(obj.ficha.codcli);
    $("#txt_estado_modal").val(obj.ficha.estado);
    $("#txt_ticket_modal").val(obj.ficha.ticket);
    $("#txt_averia_modal").val(obj.ficha.tipo_averia);
    $("#txt_tipo_averia_modal").val(obj.ficha.actividad_tipo_id);

    $("#busqueda").val(obj.ficha.id);
    $(".txt_codactu_modal_mov").val(obj.ficha.codactu);
    $(".txt_estado_modal_mov").val(obj.ficha.estado);
    $(".txt_ticket_modal_mov").val(obj.ficha.ticket);
    $(".txt_averia_modal_mov").val(obj.ficha.tipo_averia);

     /*atento's data*/
    $("#txt_fhreg104").val(obj.ficha.fh_reg104);
    $("#txt_fhreg1l").val(obj.ficha.fh_reg1l);
    $("#txt_fhreg2l").val(obj.ficha.fh_reg2l);
    $("#txt_codmultigestion").val(obj.ficha.cod_multigestion);
    $("#txt_llamador").val(obj.ficha.llamador);
    $("#txt_titular").val(obj.ficha.titular);
    $("#txt_direccion").val(obj.ficha.direccion);
    $("#txt_distrito").val(obj.ficha.distrito);
    $("#txt_urbanizacion").val(obj.ficha.urbanizacion);
    $("#txt_telfgestion").val(obj.ficha.telf_gestion);
    $("#txt_telfentrante").val(obj.ficha.telf_entrante);
    $("#txt_operador").val(obj.ficha.operador);
    $("#txt_motivo_llamada").val(obj.ficha.motivo_call);
    /*end atento's data*/

    if (obj.ficha.gestion == 'rrss') $("#lbl_registrador").html('Usuario');
    else $("#lbl_registrador").html('Embajador');

    if ((obj.ficha.codactu).trim()) {
        $("#tab_datos_averia_modal").css("display","block");
    }

    $.each(obj.ficha, function(key, value){
        $('#ticketForm [name='+key+']').val(value);
    });
    $('#f_comentario').val(obj.ficha.comentario);

    //averia
    /*$.each(obj.averia, function(key, value){
        $('#averiaForm [name='+key+']').val(value);
    });*/
};

ListarMovimientos = function(datos) {
    var html="", con = 1;
    $('#t_movimiento').dataTable().fnDestroy();

    $("#tb_movimiento").html('');
    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td>"+con+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.observacion+"</td>"+
        "<td>"+data.fecha+"</td>"+
        "<td>"+data.usuario+"</td>"+
        "<td>"+data.solucionado+"</td>";
        html+="</tr>";
        con++;
    });
    $("#tb_movimiento").html(html);
    $('#t_movimiento').dataTable();
};

</script>
