<script type="text/javascript">
var Pretemporales={
    Listar:function(busqueda){
        var ficha_busqueda={};
        if (busqueda == 'P') {
            findTypeGLOBAL = 'P';
            ficha_busqueda = {
                PG: busqueda,
                tipo_busqueda: busqueda,
                tipo: $("#slct_tipo").val(),
                busqueda: $("#txt_buscar").val()
            };
        } else if(busqueda=="G"){ // Filtro General
            findTypeGLOBAL = 'G';
            ficha_busqueda=$("#form_General").serialize().split("txt_").join("").split("slct_").join("")+ '&PG=G';
        } 

        if ($("#slct_tipo").val() !== '' || busqueda =="G") {
            $.ajax({
                url         : 'pretemporal/buscar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : ficha_busqueda,
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();
                    if(data.length > 0){ 
                        HTMLListarPretemporal(data);
                    }else{
                        swal('Mensaje',data.msj,'warning');
                        $("#tb_embajador").html('<tr><td colspan="12">Ningún dato disponible en esta tabla</td></tr>');
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error");
                }
            });
        } else swal('Mensaje','Seleccione tipo de búsqueda','info');
    },
    Cargar: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/cargar',
            type        : 'GET',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarDataModal(data.ficha);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    Movimiento: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/listarmovimiento',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarMovimientos(data.data);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    Relistar:function(busqueda, ticket){
        var ficha_busqueda = {
            PG: busqueda,
            tipo: 'ticket',
            busqueda: ticket
        };
        $.ajax({
            url         : 'pretemporal/buscar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : ficha_busqueda,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.length > 0){ 
                    HTMLListarPretemporal(data);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    guardarMovimiento: function() {
        var datos=$("#form_bandeja").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");
        $.ajax({
            url         : "pretemporal/recepcion",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
		if(obj.rst==1){
                    Pretemporales.Relistar('P',obj.ticket);
                    swal("Mensaje",obj.msj,"success");
                    $('#pretemporalModal .modal-footer [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    ValidarLegados: function(id) {
        $.ajax({
            url         : "pretemporal/validarlegados",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {id: id},
            beforeSend : function() {
            },
            success : function(obj) {
                if (obj.rst == 1) {
                    HTMLListarAveria(obj.data, id);
                    $("#content_averia").css('display', 'block');
                } else  $("#content_averia").css('display', 'none');
            },
            error: function(obj){
            }
        });
    },
    ValidarPermiso: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/validarpermiso',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst == 1){
                    permisoGlobal = data.type;
                    validaPermiso();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    FinalizarPermiso: function(id, type){
        var datos = {
            busqueda: id,
            type: type
        };
        $.ajax({
            url         : 'pretemporal/finalizarpermiso',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    }
};
</script>
