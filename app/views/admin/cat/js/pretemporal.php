<script type="text/javascript">
GLOBAL = {};
var findTypeGLOBAL = '';
$(document).ready(function() {
    getTicketsByMasivo();
    eventoCargaMostrar();
    $(".navbar-btn.sidebar-toggle").click();
    slctGlobalHtml('slct_averia,#slct_averia','multiple');
    slctGlobalHtml('slct_actividad,#slct_actividad','multiple');
    slctGlobalHtml('slct_gestion,#slct_gestion','multiple');
    slctGlobalHtml('slct_tipo','simple');
    slctGlobalHtml('slct_asignacion','simple');

    slctGlobal.listarSlct('listado', 'slct_quiebre', 'multiple', null);
    slctGlobal.listarSlct('estado_pretemporal', 'slct_estado_pre', 'multiple', null);
    slctGlobal.listarSlct('pretemporal_atencion', 'slct_atencion', 'multiple', null);
    slctGlobal.listarSlct('usuarioarea','slct_usuarios','multiple',null);

    activarTabla();
    /*$('#txt_fecha_registro').val(
        moment().subtract(7, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $('#txt_fecha_registro').daterangepicker({
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(7, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });*/

    $("#txt_fecha_registro").daterangepicker({
        format: 'YYYY-MM-DD',
        endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
        },function (startDate, endDate, period) {
            var diff = daydiff(parseDate(startDate.format('L')), parseDate(endDate.format('L')))
            if(diff > 34){
                var last = parseDate(startDate.format('L'));
                last.setDate(last.getDate() + 31);
                
                $('#txt_fecha_registro').val(
                    moment(startDate).format("YYYY-MM-DD") + ' - ' +
                    moment(last).format("YYYY-MM-DD")
                );               
                swal('El rango es maximo de 31 días');          
            }
    });

    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    var data={};var data2={};
    data = { requerimiento: '0-0', mas: '1' };
    data2 = { requerimiento: '0-0', mas: '1', quiebre_id: '46,55', interface: 'pretemporal' };
    $('#slct_motivo,#slct_submotivo,#slct_estado').multiselect('destroy');
    var ids = [];
    slctGlobal.listarSlct('motivo','slct_motivo','multiple',ids,data2,0,'#slct_submotivo,#slct_estado','M');
    slctGlobal.listarSlct('submotivo','slct_submotivo','multiple',ids,data,1,'#slct_estado','S','slct_motivo','M');
    slctGlobal.listarSlct('estado','slct_estado','multiple',ids,data,1);

    $("#slct_tipo").change(ValidaTipo);
    $("#btn_personalizado").click(personalizado);
    $("#btn_general").click(general);
    $("#btn_limpiar").click(limpiarFormulario);
    setTimeout(function(){eventoCargaRemover();}, 1000);
});
personalizado=function(){
    if( $("#slct_tipo").val()==='' ){
        swal("Mensaje","Seleccione Tipo Filtro", "warning");
        $("#slct_tipo").focus();
    } else if( $("#txt_buscar").val()==='' ){
        swal("Mensaje","Ingrese datos", "warning");
        $("#txt_buscar").focus();
    } else{
        Pretemporales.Listar("P");
    }
};
general=function(){
    Pretemporales.Listar("G");
};
ValidaTipo=function(){
    $("#txt_buscar").val("");
    $("#txt_buscar").focus();
};
activarTabla=function(){
    $("#t_embajador").dataTable(); // inicializo el datatable
};
limpiarFormulario=function(){
    $("#slct_averia, #slct_quiebre, #slct_atencion, #slct_estado_pre, #slct_estado, #slct_motivo, #slct_submotivo, #slct_tipo, #slct_gestion").multiselect("clearSelection");
    $("#slct_tipo").multiselect("refresh");
    $('#txt_fecha_registro').val(
        moment().subtract(7, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
     $('#txt_fecha_registro').daterangepicker({
        startDate: moment().subtract(7, 'days'),
        format: 'YYYY-MM-DD'
    });
    $('#txt_buscar').val('');
    $("#tb_embajador").html('<tr><td colspan="12">Ningún dato disponible en esta tabla</td></tr>');
};

HTMLListarPretemporal=function(datos){
    var html="";
    $('#t_embajador').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        color= '#F7BE81';
        fecha_usuario = data.fecha_registro + " " +data.asignado;
        if(data.estado_id == 1) color= '#FFFFFF';
        if(data.estado == 'Cerrado') color= '#A9BCF5';
        html+="<tr>"+
        "<td style='background-color:"+color+";'>"+data.ticket+"<br>"+data.tipo_atencion+"</td>"+
        "<td>"+fecha_usuario+"</td>"+
        "<td>"+data.fechaultmov+"</td>"+
        "<td>"+data.tipo_averia+"</td>"+
        "<td>"+data.codcli+"</td>"+
        "<td>"+data.quiebre+"</td>"+
        "<td>"+data.cliente_nombre+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.solucion+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.codliq+"</td>"+
        "<td>"+data.area+"</td>"+
        "<td>"+data.cod_multigestion+"</td>"+
        "<td>"+data.llamador+"</td>"+
        "<td>"+data.titular+"</td>"+
        "<td>"+data.motivo_call+"</td>"+
        "<td><button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#pretemporalModal' data-id='"+data.id+"'><i class='fa fa-desktop fa-lg'></i></button></td>";
        html+="</tr>";
    });
    $("#tb_embajador").html(html);
    activarTabla();
};
ListarDataModal=function(obj){
    //validar permisos del quierbre
    $("#txt_id_modal").val(obj.id);
    $("#txt_codactu_modal, #txt_codactu_modal_mov").val(obj.codactu);
    $("#txt_codcli_modal, #txt_codcli_modal_mov").val(obj.codcli);
    $("#txt_estado_modal, #txt_estado_modal_mov").val(obj.estado);
    $("#txt_ticket_modal, #txt_ticket_modal_mov").val(obj.ticket);
    $("#txt_atencion").val(obj.atencion);
    $("#txt_averia_modal, #txt_averia_modal_mov").val(obj.tipo_averia);

    /*atento's data*/
    $("#txt_fhreg104").val(obj.fh_reg104);
    $("#txt_fhreg1l").val(obj.fh_reg1l);
    $("#txt_fhreg2l").val(obj.fh_reg2l);
    $("#txt_codmultigestion").val(obj.cod_multigestion);
    $("#txt_llamador").val(obj.llamador);
    $("#txt_titular").val(obj.titular);
    $("#txt_direccion").val(obj.direccion);
    $("#txt_distrito").val(obj.distrito);
    $("#txt_urbanizacion").val(obj.urbanizacion);
    $("#txt_telfgestion").val(obj.telf_gestion);
    $("#txt_telfentrante").val(obj.telf_entrante);
    $("#txt_operador").val(obj.operador);
    $("#txt_motivo_llamada").val(obj.motivo_call);
    /*end atento's data*/

    if(obj.evento !== ''){
        $(".evento").removeClass('hidden');
        $("#txt_evento_modal,#txt_evento_modal_2").val(obj.evento);
        $("#txt_idevento").val(obj.evento_masiva_id);
        
        //$("#txt_atencion").addClass('hidden');
    }else{
        $(".evento").addClass('hidden');
        //$("#txt_atencion").removeClass('hidden');
    }

    $("#txt_tipo_averia_modal").val(obj.actividad_tipo_id);
    $("#busqueda").val(obj.id);
    if (obj.gestion == 'rrss') $("#lbl_registrador").html('Usuario');
    else $("#lbl_registrador").html('Embajador');

    //activar para Pendientes y Recepcionado
    if (obj.estado_id == 7 || obj.estado_pretemporal_id == 1) {
        $("#tab_gestion_modal").css("display","block");
        $('#tab_1').addClass("active");
        $('.tab_1').addClass("active");
    } else {
        $("#tab_gestion_modal").css("display","none");
        $('#tab_2').addClass("active"); $('.tab_2').addClass("active");
    }
    
    //mensaje de averia registrada
    if (obj.codactu !== '') {
        if (obj.validado == 1) {
            $("#mensaje_modal").css("display","block");
            $("#mensaje_modal").html('<h4>Cuenta con Avería Registrada.</h4>');
            $('#mensaje_modal').addClass("alert-success").removeClass("alert-warning");
        } else {
            $("#mensaje_modal").css("display","block");
            $("#mensaje_modal").html('<h4>La avería no ha sido validada aún.</h4>');
            $('#mensaje_modal').addClass("alert-warning").removeClass("alert-success");
        }
    } else 
        $("#mensaje_modal").css("display","none");
    if ((obj.codactu).trim()) {
        $("#tab_datos_averia_modal").css("display","block");
    }
    $('#slct_solucionado_modal').multiselect('destroy');
    slctGlobal.listarSlct('pretemporal_solucion','slct_solucionado_modal','simple',[], {actividad_tipo: obj.actividad_tipo_id});
    $("#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal").multiselect("clearSelection");
    $("#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal").multiselect("refresh");


     //motivos,submotivos,estados del modal
    var quiebre_id = (obj.quiebre_id !== 55) ? 46 : obj.quiebre_id;
    var data_modal={};var data_modal2={};  var ids = [];
    data_modal = { requerimiento: '0-0', mas: '1' };
    data_modal2 = { requerimiento: '0-0', mas: '1', quiebre_id: quiebre_id, interface: 'pretemporal' };
    $('#slct_motivo_modal,#slct_submotivo_modal,#slct_estado_modal').multiselect('destroy');
    slctGlobal.listarSlct('motivo','slct_motivo_modal','simple',ids,data_modal2,0,'#slct_submotivo_modal,#slct_estado_modal','M');
    slctGlobal.listarSlct('submotivo','slct_submotivo_modal','simple',ids,data_modal,1,'#slct_estado_modal','S','slct_motivo_modal','M');
    slctGlobal.listarSlct('estado','slct_estado_modal','simple',ids,data_modal,1);
    //if (obj.quiebre == 'VAL MASIVA'){

 
        /*let items_list = $(document).find("[data-select='slct_motivo_modal']").find('li');
        $.each(items_list, function(){
            if ($($(this).find('input')[0]).val() == '28') {
                $(this).show();
            } else {
                $(this).hide();
            }
        });*/
    //}

    $.each(obj, function(key, value){
        $('#ticketForm [name='+key+']').val(value);
    });
    $('#f_comentario').val(obj.comentario);
    $('#asunto').val('AD101 - '+obj.quiebre+
        ' / Cli: '+ obj.codcli +
        ' / Av: '+ obj.codactu +
        ' / Ticket:'+ obj.ticket);
    $('#quiebre_cat').html(obj.quiebre);
    //averia
    $.each(obj, function(key, value){
        $('#averiaForm [name='+key+']').val(value);
    });
};
eventoSlctGlobalSimple=function(slct,valores){ 
    var m=""; var s=""; var val=""; var dval="";
    if(slct=="slct_estado_modal"){
        M="M"+$("#slct_motivo_modal").val();
        S="S"+$("#slct_submotivo_modal").val();
        //validacion - solucionado
        if (M == "M23" && S == "S58") {
            $("#form_bandeja .solucionado").css('display', '');
        } else {
            $("#form_bandeja .solucionado").css('display', 'none');
        }
        //validacion - 60: se registra Averia - 75: tiene reg averia
        if ((M == "M24" && S == "S60")|| (M == "M24" && S == "S75") ||  (M == "M24" && S == "S96")||  (M == "M24" && S == "S97") ) {
            $("#txt_codactu_modal").prop('readonly', false);
        } else {
            $("#txt_codactu_modal").prop('readonly', true);
        }
    }
};
upperCase =function(input) {
   var x=$(input).val();
   $(input).val(x.toUpperCase());
};
ListarMovimientos = function(datos) {
    var html="", con = 1;
    $('#t_movimiento').dataTable().fnDestroy();
    $("#tb_movimiento").html('');
    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td>"+con+"</td>"+
        "<td>"+data.motivo+"</td>"+
        "<td>"+data.submotivo+"</td>"+
        "<td>"+data.estado+"</td>"+
        "<td>"+data.observacion+"</td>"+
        "<td>"+data.fecha+"</td>"+
        "<td>"+data.usuario+"</td>"+
        "<td>"+data.solucionado+"</td>";
        html+="</tr>";
        con++;
    });
    $("#tb_movimiento").html(html);
    $('#t_movimiento').dataTable();
};
HTMLListarAveria=function(datos, id){
    var html="";
    $('#t_averia').dataTable().fnDestroy();
    $('#tb_averias').html= '';
    $.each(datos,function(index,data){
        html+="<tr>"+
        "<td><input name='rad_codactu_val' type='radio' value='"+data.codactu+"|"+data.telefono+"|"+data.fecha_registro+"'></input></td>"+
        "<td>"+data.telefono+"</td>"+
        "<td>"+data.codactu+"</td>"+
        "<td>"+data.nombre_cliente+"</td>";
        html+="</tr>";
    });
    $("#tb_averias").html(html);
};
descargarReporte = function () {
    if (findTypeGLOBAL == "G") {
        $("#form_General").append("<input type='hidden' name='PG' id='PG' value='G'>");
        $("#form_General").append("<input type='hidden' name='excel' id='excel' value='excel'>");
        $("#form_General").submit();
    } else if (findTypeGLOBAL == "P") {
        $("#form_Personalizado").append("<input type='hidden' name='PG' id='PG' value='P'>");
        $("#form_Personalizado").append("<input type='hidden' name='excel' id='excel' value='excel'>");
        $("#form_Personalizado").submit();
    }
};
descargarMovimientoReporte = function () {
    $("#form_Movimientos").submit();
};


getTicketsByMasivo = function (){
    var urlParams = new URLSearchParams(window.location.search);
    var parameter = urlParams.getAll('c')[0];
    if(parameter){
        slctGlobal.listarSlct('eventomasivo', 'slct_eventos', 'multiple', [parameter]);
        $("#txt_fecha_registro").val('');
        $("#btn_general").click(general);
    }else{
        slctGlobal.listarSlct('eventomasivo', 'slct_eventos', 'multiple', null);
        $('#txt_fecha_registro').val(moment().format("YYYY-MM-DD") + ' - ' +moment(new Date()).format("YYYY-MM-DD"));
    }
}
</script>
