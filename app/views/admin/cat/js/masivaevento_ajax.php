<script type="text/javascript">

var listado={
    Userstovalidate: function(){
        $.ajax({
            url         : 'quiebregrupo/getuserbygrupo',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(data) {
                HTMLUsuariosValidar(data.data);
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    codactu: function(){
        datos=$("#form_configuracion").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'pretemporal/configuracion',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },

    cargar: function(data){
        $.ajax({
            url         : 'eventomasivo/cargar',
            type        : 'POST',
            data        : data,
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    if(data.tipo == 1){
                        HTMLDetalle(data.datos[0]);
                    }else if(data.tipo == 2){
                        HTMLEventos(data.datos);
                        
                    }
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    CambiarEstado: function(data){
        $.ajax({
            url         : 'eventomasivo/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    listado.cargar();
                    $("#cancelMasiva").modal('hide');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
     getTicketsbyEvent: function(data,img = ''){
        $.ajax({
            url         : 'eventomasivo/getticketsbyevent',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLTickets(data.datos,img);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    getMasiva: function (data) {
        $.ajax({
            url: 'pretemporal/masivas',
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            beforeSend: function () {
               $('.btncarga').html('Cargando...').prop('disabled', true);
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    if(obj.data.length > 0){
                        /*get groups of 35 elements 'cause php is restricted' */
                        var registros_group = [];
                        var i,j,temparray,chunk = 32;
                        for (i=0,j=obj.data.length; i<j; i+=chunk) {
                            temparray = obj.data.slice(i,i+chunk);
                            registros_group.push(JSON.stringify(temparray));
                        }
                        listado.procesarMasivo(registros_group,data.id);                              
                        /*end */                            
                    }
                   $('.btncarga').html('Listo').prop('disabled', true);
                }
            }
        });
    },
    procesarMasivo: function (data,evento_id) {
        $.ajax({
            url: 'pretemporal/procesarmasivo',
            type: "POST",
            data: {
                evento_id : evento_id,
                pedidos : data,
                },
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    alert = 'success';
                    $("#nuevoEvento").modal('hide');
                }
                listado.cargar();
                swal('Mensaje','Registrados:'+obj.registrados,alert);
            }
        });
    }

};
</script>

