<script type="text/javascript">

$(document).ready(function() {	
	$('.tbl-scroll,.tblgrupo').perfectScrollbar();

	slctGlobal.listarSlct('empresa', 'slct_empresa', 'simple', null);
    slctGlobal.listarSlct('listado', 'slct_quiebre', 'multiple', null);
    slctGlobal.listarSlct('listado', 'slct_quiebre2', 'simple', null);    

	listado.Userstovalidate();
	listado.cargarPersonal('',HTMLCargarPersonal);

    $(".btn-guardar").click(function(){
        Administrador.codactu();
    });

    $(document).on('click', '.btnautoasignar', function(event) {
    	var r = confirm("¿Esta seguro de auto asignar los pendientes?");
		if (r == true) {
			data = ($.trim($("#slct_quiebre").val()) != '') ? {quiebre_id:$("#slct_quiebre").val()} : '';
			listado.autoasignar(data);
		}
    });

    function initDatePicker(){
   		$('.txt_fechaini,.txt_fechafin').datepicker({
	        format: 'yyyy-mm-dd',
	        language: 'es',
	        multidate: 1,
	        todayHighlight:true,
	        startDate: new Date(),
	     /*  daysOfWeekDisabled: '0', //bloqueo domingos*/
	        onSelect: function (date, el) {
	        }
    	})
	}

    $(document).on('click', '#btnAdd', function(event) {
        event.preventDefault();
        var template = $("#t_exoneracion").find('.trNuevo').clone().removeClass('trNuevo').removeClass('hidden');
        $("#tb_exoneracion").append(template);
        initDatePicker();
    }); 


     $('#nuevoUsuario').on('hide.bs.modal', function (event) {
     	$("#formNuevoUsuario input[type='hidden'],#formNuevoUsuario input[type='password'],#formNuevoUsuario input[type='text'],#formNuevoUsuario input[type='file'],#formNuevoUsuario select,#formNuevoUsuario textarea").not('.mant').val('');
  		$("#formNuevoUsuario select").multiselect('refresh');
    });

    $('#nuevoGrupo').on('show.bs.modal', function (event) {
     	listado.cargarPersonal({grupo:0},HTMLSelectGrupo);
    });

    /*$('#fechaExonerar').on('show.bs.modal', function (event) {
    	var button = $(event.relatedTarget); // captura al boton
	    var id= $.trim( button.data('id') );
     	$("#txt_idusuario").val(id);
     	listado.listarExoneraciones({usuario_id:id});
    });*/

    $('#nuevoGrupo').on('hide.bs.modal', function (event) {
     	$("#formNuevoGrupo input[type='hidden'],#formNuevoGrupo input[type='password'],#formNuevoGrupo input[type='text'],#formNuevoGrupo input[type='file'],#formNuevoGrupo select,#formNuevoGrupo textarea").not('.mant').val('');
       $("#formNuevoGrupo select").multiselect('refresh');
    });

    $(document).on('change','.slct_days',function(event){
    	var value = $(this).val();
    	var usuario_id = $(this).attr('iduser');
    	var text = $(".slct_days[iduser="+usuario_id+"]>option[value="+value+"]").text();
    	if(value && usuario_id){
    		var r = confirm("¿Esta seguro agregar este dia para el usuario?");
			if (r == true) {
    			listado.Rusuariohorario({dia_id:value,usuario_id:usuario_id},value,text,usuario_id);    			
			}
    	}
    });

    $(document).on('change','.slct_grupo',function(event){
    	var r = confirm("¿Esta seguro de cambiar de grupo?");
    	if(r == true){
	    	var value = $(this).val();
	    	var idusuarioa = $(this).attr('idusuarioa');
	    	listado.CambiarEstado({'id':idusuarioa,'grupo_id':value});    		
    	}else{
    		$('.slct_grupo').remove();
    	}
    });

    $(document).on('change','.slct_quiebre',function(event){
    	var r = confirm("¿Esta seguro de cambiar de quiebre?");
    	if(r == true){
	    	var value = $(this).val();
	    	var idusuarioa = $(this).attr('idusuarioa');
	    	listado.CambiarEstado({'id':idusuarioa,'quiebre_id':value});    		
    	}else{
    		$('.slct_quiebre').remove();
    	}
    });

    //delete from levels to page
    $(document).on('beforeItemRemove','.clsInput',function(e){
    	usuario_id = $(this).attr('iduser');
		dia_id = e.item.value;
		if(usuario_id && dia_id){
			var r = confirm("¿Esta seguro de eliminar el siguiente dia?");
			if (r == true) {
				listado.deleteDay({usuario_id:usuario_id,dia_id:dia_id});
			}else{
				e.cancel = true;
			}
		}else{
			e.cancel = true;
		}
    });

    $(document).on('click', '.paginate_button', function(e) {
		listado.listarHorarios();
	});
});

function onTaginput(){
	    $('.clsInput').tagsinput({
			itemValue: 'value',
	  		itemText: 'text',
	  		allowDuplicates: false,
	  		maxTags: 7,
		});    	
}

HTMLUsuariosValidar = function(data){
	if(data.length > 0){
		var html = '';
		$('#tblValidaciones').dataTable().fnDestroy();
		$.each(data,function(index, el) {
			html+='<tr>';
			html+='<td>'+el.usuario+'</td>';
			html+='<td>'+el.dni+'</td>';
			html+='<td>'+el.nombre+'</td>';
			html+='<td>'+el.celular+'</td>';

			if(el.validado == 1){
				html+='<td style="text-align:center"><input class="form-control chk_validado" type="checkbox" id="chk_validado" name="chk_validado" value="'+el.usuario_id+'" onClick="validateUser(this)" checked/></td>';
			}else{
				html+='<td style="text-align:center"><input class="form-control chk_validado" type="checkbox" id="chk_validado" name="chk_validado" value="'+el.usuario_id+'" onClick="validateUser(this)"/></td>';
			}
			html+='</tr>';
		});
		$("#tblValidaciones tbody").html(html);
		$("#tblValidaciones").dataTable();
	}
}

guardar = function(){
	if($.trim($("#txt_nombre").val()) == ""){
		swal('completa el nombre');
	}else if($.trim($("#txt_apellidos").val()) == ""){
		swal('completa apellidos');
	}else if($.trim($("#txt_dni").val()) == ""){
		swal('completa el dni');
	}else if($.trim($("#txt_usuario").val()) == ""){
			swal('completa el usuario');
	}else if($.trim($("#txt_password").val()) == ""){
		swal('registre su password');	
	}else{
		listado.nuevoUsuario();
	}
}

validateUser = function(obj){
	var UserId = obj.getAttribute('value');
	if(UserId){
		var r = confirm("¿Esta seguro de incorporar a su area al usuario seleccionado?");
		if (r == true) {
			listado.validateUsers({user_id:UserId,area_id:51},obj);
		}
	}else{
		swal('No se pudo realizar la acción');
	}
}

HTMLCargarPersonal = function(data){
	if(data.length > 0){		
		var html  = '';
		var cont = 0;
		$("#tb_personal").dataTable().fnDestroy();
		$.each(data,function(index, el) {
			html2='<div class="row">'
			html2+='	<div class="col-md-5 col-sm-12 col-xs-12">';
			html2+= '	<select class="form-control slct_days" id="slct_days" name="slct_days" iduser="'+el.id+'">';
			html2+= '		<option value="">.::Seleccione::.</option>';
			html2+= '		<option value="1">Lunes</option>';
			html2+= '		<option value="2">Martes</option>';
			html2+= '		<option value="3">Miercoles</option>';
			html2+= '		<option value="4">Jueves</option>';
			html2+= '		<option value="5">Viernes</option>';
			html2+= '		<option value="6">Sabado</option>';
			html2+= '		<option value="7">Domingo</option>';
			html2+= '	<select>';
			html2+='	</div>';
			html2+='	<div class="col-md-7 col-sm-12 col-xs-12">';
			html2+='		<input class="clsInput" id="txtprueba" type="text" iduser="'+el.id+'" idhorario="'+el.usuario_area_id+'"/> ';
			html2+='	</div>';
			html2+='</div>';

			estado = '<span class="btn btn-danger btn-sm estado" onClick="cambiarEstado('+el.usuario_area_id+',1)">Inactiva</span>';
			if(el.ua_estado == 1){
				estado = '<span class="btn btn-success btn-sm estado" onClick="cambiarEstado('+el.usuario_area_id+',0)">Activo</span>';
			}
			cont+=1;
			html+='<tr>';
			html+='<td>'+cont+'</td>';
			html+='<td>'+el.nombrep+'</td>';
			html+='<td>'+el.apellido+'</td>';
			html+='<td>'+el.area+'</td>';

			html+='<td><i class="glyphicon glyphicon-pencil pEdit" style="cursor:pointer" onClick="changeGroup(this,'+el.usuario_area_id+')"></i> '+el.grupo+'</td>';
			html+='<td><i class="glyphicon glyphicon-pencil pEdit" style="cursor:pointer" onClick="changeQuiebre(this,'+el.usuario_area_id+')"></i> '+el.quiebre+'</td>';
			html+='<td>'+html2+'</td>';
			html+='<td><span class="btn btn-danger btn-sm" data-id="'+el.id+'" data-toggle="modal" data-target="#fechaExonerar" onClick="Exoneraciones('+el.id+','+el.usuario_area_id+')">Exonera</span></td>';
			html+='<td>'+estado+'</td>';
			html+='</tr>';
		});
		$("#tbody_personal").html(html);
		$("#tb_personal").dataTable();

		onTaginput();
		listado.listarHorarios();  		
	}else{
		swal('No encuentra datos');
	}
}

Exoneraciones = function(usuario_id,usuario_area_id){
	$("#txt_idusuario").val(usuario_id);
	$("#txt_idusuarioarea").val(usuario_area_id);
    listado.listarExoneraciones({usuario_id:usuario_id});
}

HTMLSelectGrupo = function(data){
	if(data.length > 0){	
		$("#tg_personal").dataTable().fnDestroy();
		cont=0;
		html = '';
		$.each(data,function(index, el) {
			cont+=1;
			html+='<tr>';
			html+='<td>'+cont+'</td>';
			html+='<td>'+el.nombrep+'</td>';
			html+='<td>'+el.apellido+'</td>';
			html+='<td><input type="checkbox" name="chk_grupo id="chk_grupo" class="form-control chkSelect" value="'+el.id+'"/></td>';
			html+='</tr>';
		});
		$("#tbgbody_personal").html(html);
		$("#tg_personal").dataTable();
	}
}

crearGrupo = function(){

	if($.trim($("#txt_nombreg").val()) == ''){
		swal('Ingrese nombre de grupo');
	}else if($.trim($("#slct_quiebre2").val()) == ''){
		swal('Seleccione quiebre');
	}else{
		 /*get data to re-assig throught form*/
	    var datos=$("#formNuevoGrupo").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");
	    /*end get data to re-assig throught form*/

	    /*get orders to re-assig*/
	    var table = $('#tg_personal').DataTable();
	    var checks =  table.$(".chkSelect:checked", {"page": "all"});
	    var usuarios = '';
	    if(checks.length > 0){
	        $.each(checks,function(index, el) {
	            usuarios+=(index == 0) ? $(el).val() : "," + $(el).val();            
	        });
	    }
	    datos+='&usuarios='+usuarios;
	    listado.crearGrupo(datos);
	    /*end get orders to re-assig*/
    }
}

cambiarEstado = function(id,estado){
	listado.CambiarEstado({id:id,estado:estado});
}

HTMLHorarios = function(data){
	if(data){
 		onTaginput();
		var dias = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
		$.each(data,function(index, el) {
			//$(".clsInput[iduser="+ el.usuario_id +"]").tagsinput('add', { "value": el.dia_id , "text": dias[parseInt(el.dia_id) - 1]  });					
            var text = dias[parseInt(el.dia_id) - 1];
            if (el.usuario_id=='1215') {
                console.log(el);
            }
            var usuarioHorario = { 
                "value": el.dia_id ,
                "text": text
            };
			$(".clsInput[iduser="+ el.usuario_id +"]").tagsinput('add', usuarioHorario);
		});
	}
}

changeGroup = function(obj,id){
	var td = obj.parentNode;
	listado.listarGrupos('grupocot/listar','',td,id);
}

changeQuiebre = function(obj,id){
	var td = obj.parentNode;
	listado.listarGrupos('quiebre/listar',{usuario:1},td,id);
}

HTMLGrupos = function(data,contenedor,id_usuario_area,tipo){
	$(".slct_"+tipo).remove();
	html = '';
	html+='<select name="slct_'+tipo+'" id="slct_'+tipo+'" class="form-control slct_'+tipo+'" idusuarioa="'+id_usuario_area+'">';
	html+='<option value="">.::Seleccione::.</option>';
	$.each(data,function(index, el) {
		html+='<option value='+el.id+'>'+el.nombre+'</option>';
	});
	html+='</select>';
	$(contenedor).append(html);
}

Deletetr = function(obj){
	var tr = obj.parentNode.parentNode;
	var id_exoneracion = obj.getAttribute('id-exoneracion');
	var r = confirm("¿Esta seguro de cancelar el registro?");
	if (r == true) {
		if(id_exoneracion){
			listado.deleteEXoneracion({id:id_exoneracion});
		}else{
			tr.remove(tr);
		}
	}
}

saveExoneracion = function(obj){
	var tr = obj.parentNode.parentNode;
	var fecha_inicio = $(tr).find('.txt_fechaini').val();
	var fecha_fin = $(tr).find('.txt_fechafin').val();
	var observacion = $(tr).find('.txt_observacion').val();
	if($.trim($("#txt_idusuario").val()) != '' && fecha_inicio != '' && fecha_fin != ''){
		listado.exonerar(
			{usuario_area_id:$("#txt_idusuarioarea").val(),
			usuario_id:$("#txt_idusuario").val(),
			fecha_inicio:fecha_inicio,
			fecha_fin:fecha_fin,
			obs:observacion},tr);
	}else{
		swal('Faltan datos');
	}
}

HTMLCargarExoneraciones = function(data){
	html = '';
	if(data){
		$.each(data,function(index, el) {
			html+='<tr>';
			html+='<td>'+el.fecha_inicio+'</td>';
			html+='<td>'+el.fecha_fin+'</td>';
			html+='<td>'+el.observacion+'</td>';
			if(el.estado==1){
				html+='<td style="text-align:center"><span id="btnDelete" name="btnDelete" class="btn btn-danger"  id-exoneracion="'+el.id+'" btn-sm btnDelete" onclick="Deletetr(this)"><i class="glyphicon glyphicon-remove"></i></span></td>';				
			}else{
				html+='<td style="text-align:center"><span id="btnDelete" name="btnDelete" class="btn btn-danger  btn-sm btnDelete" style="opacity:0.5"><i class="glyphicon glyphicon-remove"></i></span></td>';	
			}
			html+='</tr>';		
		});
		$("#tb_exoneracion").html(html);
	}	
}

</script>
