<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script('lib/ajaxUpload/ajaxupload.min.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}
{{ HTML::script('js/psi.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.cat.js.masiva_ajax' )
@include( 'admin.cat.js.masiva' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
    .btn-yellow{
                color: #0070ba;
                background-color: ghostwhite;
                border-color: #ccc;
                font-weight: bold;
    }
      fieldset{
        max-width: 75% !important;
        border: 1px solid #999;
        padding:6px 20px 6px 20px;
        border-radius: 10px; 
    }

      .yellow-fieldset{
        max-width: 100% !important;
        border: 3px solid #999;
        padding:10px 50px 0px 9px;
        border-radius: 10px; 
        margin-bottom: 15px;
    }

    legend{
        font-size:14px;
        font-weight: 700;
        width: 34%;
        border-bottom: 0px;
        margin-bottom: 5px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Masiva 101 CAT
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Masiva 101 CAT</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <ul class="nav nav-pills  nav-justified">
        <li class="active"><a data-toggle="pill" href="#registro">Carga Masiva</a></li>
        <li><a data-toggle="pill" href="#log">Log</a></li>
    </ul>

    <div class="tab-content">
        <div id="registro" class="tab-pane fade in active">
            <div class="row">
                            <div class="col-xs-12">

                                <div class="box box-solid">
                                    <div class="box-header ui-sortable-handle">
                                        <h3 class="box-title">SUBIDA MASIVA DE PEDIDOS</h3>
                                    </div>
                                    <div class="box-body border-radius-none">
                                        <div class="row">
                                            <div class="col-sm-12 col-sm-4">
                                                <div class="col-sm-12 form-group">
                                                    <fieldset>
                                                        <legend>Tipo Carga</legend>
                                                        <div class="col-md-6">
                                                            <input class="form-control chkTipoCarga" type="checkbox" name="chk_normal" id="chk_normal" value="1" checked/> Normal                                
                                                        </div>
                                                        <!--<div class="col-md-6">
                                                            <input class="form-control chkTipoCarga" type="checkbox" name="chk_masiva" id="chk_masiva" value="2"/> Masiva
                                                        </div>-->                            
                                                    </fieldset>                                
                                                </div>

                                                <div class="col-sm-12 normal form-group">
                                                    <form id="form_parcial_columna" name="form_parcial_columna"  role="form" class="form-inline" enctype="multipart/form-data">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <!--<input type="file" class="form-control" id="archivocolumna" name="archivocolumna" accept="text/plain">-->
                                                                <button class="btn btn-md btn-primary" type="button" id="btnSubirExcel">Subir Archivo Excel</button>
                                                            </div><!-- 
                                                            <div class="input-group">
                                                                <input type="button" value="Subir Pedidos" class="btn btn-primary" id="btnActualizacion" disabled>
                                                            </div> -->
                                                        </div>
                                                    </form>
                                                    <small style="color: #000;">(*) Subir archivos xlx, con el formato descargado.</small><br>
                                                </div>

                                                <div class="col-sm-12 masivaevento hidden form-group">
                                                    <div class="col-md-6">
                                                        <select class="form-control" id="slct_eventos" name="slct_eventos"></select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="btn btn-primary btn-md btncarga" onclick="cargaMasivaEvento()">Cargar Masiva  <i class="glyphicon glyphicon-list-alt"></i></span>
                                                    </div>
                                                    <small style="color: #000;">(*)Seleccione evento a asociar.</small><br>    
                                                </div>
                                            </div>


                                            <div class="col-sm-12 col-sm-2 quiebre">
                                                <p><h4><i>QUIEBRE:</i></h4></p>
                                                    <b class="text-primary">EMBAJADOR</b><br>
                                                    <b class="text-primary">REDES SOCIALES</b> 
                                            </div>
                                            <div class="col-sm-12 col-sm-3 atencion">
                                                <p><h4><i>ATENCION:</i></h4></p>
                                                    <b class="text-primary">Normal</b> <br>
                                                    <b class="text-primary">Prioritario</b> <br>
                                                    <b class="text-primary">Influyente</b> <br>
                                                    <b class="text-primary">Critico</b> 
                                            </div>
                                            <div class="col-sm-12 col-sm-1 tipo_averia">
                                                <p><h4><i>TIPO AVERIA:</i></h4></p>
                                                    <b class="text-primary">Television</b> <br>
                                                    <b class="text-primary">Telefono</b> <br>
                                                    <b class="text-primary">Internet</b> <br>
                                            </div>
                                            <div class="col-sm-12 col-sm-2 fuente">
                                                <p><h4><i>FUENTE:</i> (Solo rrss)</h4></p>
                                                    <b class="text-primary">RRSS</b> <br>
                                                    <b class="text-primary">Cuantico</b> <br>
                                                    <b class="text-primary">Twitter</b> <br>
                                                    <b class="text-primary">Facebook</b> <br>
                                                    <b class="text-primary">Otros1</b> <br>
                                                    <b class="text-primary">Otros2</b> <br>
                                            </div>
                                        </div>
                                        <div class="row hidden">
                                            <div class="col-xs-12">
                                                <div class="box-header with-border">
                                                  <h3 class="box-title">El orden de los campos no es importante</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body" style="color: #000; overflow-x: scroll;">
                                                    <table class="table table-bordered table-condensed">
                                                        <tbody>
                                                            <tr>
                                                                <th>QUIEBRE</th>
                                                                <th>ATENCION</th>
                                                                <th>FUENTE</th>
                                                                <th>CODACTU</th>
                                                                <th>CODCLI</th>
                                                                <th>TIPOAVERIA</th>
                                                                <th>CLIENTENOMBRE</th>
                                                                <th>CLIENTECELULAR</th>
                                                                <th>CLIENTETELEFONO</th>
                                                                <th>CLIENTECORREO</th>
                                                                <th>CLIENTEDNI</th>
                                                                <th>CONTACTONOMBRE</th>
                                                                <th>CONTACTOCELULAR</th>
                                                                <th>CONTACTOTELEFONO</th>
                                                                <th>CONTACTOCORREO</th>
                                                                <th>CONTACTODNI</th>
                                                                <th>EMBAJADORNOMBRE</th>
                                                                <th>EMBAJADORCORREO</th>
                                                                <th>EMBAJADORCELULAR</th>
                                                                <th>EMBAJADORDNI</th>
                                                                <th>COMENTARIO</th>
                                                                <th>EVENTO</th>
                                                            </tr> 
                                                            <tr class="text-center">
                                                                @for($x = 1; $x <= 21; $x++) 
                                                                    <td>{{ $x }}</td>
                                                                @endfor
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- NUEVA SUBIDA DE EXCEL -->
                                        <!--<b>IMPORTANTE: La solicitud de ingresos se gestiona con mínimo 24 horas de anticipación</b>-->
                                        <div>
                                            <h5 class="informacion hidden"></h5>
                                            <h4>
                                                (Descargar formato xls <a href="uploads/registro-masivo-formato.xls">aqui</a>)
                                                <button id="btnHiddenExcel" class="hidden"></button><span class="btn btn-success btnSaveALl btn-xs">Guardar<i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                            <h5>(*) Las Filas de color rojo no seran registrados por no tener completos los campos</h5>
                                        </div>
                                        <div class="table margin-bottom-5" style="overflow: scroll;max-height: 600px">
                                            <table id="tblData" class=" table-responsive table table-hover table table-bordered margin-0" cellspacing="0" width="100%" border="1">
                                                <thead>
                                                    <tr style="color:#000000">
                                                        <th>QUIEBRE</th>
                                                        <th>ATENCION</th>
                                                        <th>FUENTE</th>
                                                        <th>CODACTU</th>
                                                        <th>CODCLI</th>
                                                        <th>TIPOAVERIA</th>
                                                        <th>CLIENTENOMBRE</th>
                                                        <th>CLIENTECELULAR</th>
                                                        <th>CLIENTETELEFONO</th>
                                                        <th>CLIENTECORREO</th>
                                                        <th>CLIENTEDNI</th>
                                                        <th>CONTACTONOMBRE</th>
                                                        <th>CONTACTOCELULAR</th>
                                                        <th>CONTACTOTELEFONO</th>
                                                        <th>CONTACTOCORREO</th>
                                                        <th>CONTACTODNI</th>
                                                        <th>EMBAJADORNOMBRE</th>
                                                        <th>EMBAJADORCORREO</th>
                                                        <th>EMBAJADORCELULAR</th>
                                                        <th>EMBAJADORDNI</th>
                                                        <th>COMENTARIO</th>
                                                        <th>EVENTO</th>                                   
                                                        <th>FH_REG104</th>
                                                        <th>FH_REG1L</th>
                                                        <th>FH_REG2L</th>                                                   
                                                        <th>CODIGO DE REGISTRO DE MULTIGESTION</th>
                                                        <th>NOMBRE DE LA PERSONA QUE REALIZA LA LLAMADA</th>
                                                        <th>IDENTIFICAR SI EL LLAMADOR ES EL TITULAR</th>
                                                        <th>DIRECCION</th>
                                                        <th>DISTRITO</th>
                                                        <th>URBANIZACION</th>    
                                                        <th>TELF_GESTION</th>
                                                        <th>TELF_ENTRANTE</th>
                                                        <th>NOMBRE DE OPERADOR</th>
                                                        <th>MOTIVO DE LA LLAMADA</th>
                                                        <th>EXTRA</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="color: black;">
                                                </tbody>
                                                <tfoot>
                                                    <tr class="repeat hidden">
                                                        <td style="width: 150px;" class="form-group QUIEBRE" id="QUIEBRE"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group ATENCION" id="ATENCION"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group FUENTE" id="FUENTE"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CODACTU" id="CODACTU"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CODCLI" id="CODCLI"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group TIPOAVERIA" id="TIPOAVERIA"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CLIENTENOMBRE" id="CLIENTENOMBRE"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CLIENTECELULAR" id="CLIENTECELULAR"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CLIENTETELEFONO" id="CLIENTETELEFONO"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CLIENTECORREO" id="CLIENTECORREO"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CLIENTEDNI" id="CLIENTEDNI"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CONTACTONOMBRE" id="CONTACTONOMBRE"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CONTACTOCELULAR" id="CONTACTOCELULAR"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CONTACTOTELEFONO" id="CONTACTOTELEFONO"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CONTACTOCORREO" id="CONTACTOCORREO"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CONTACTODNI" id="CONTACTODNI"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group EMBAJADORNOMBRE" id="EMBAJADORNOMBRE"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group EMBAJADORCORREO" id="EMBAJADORCORREO"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group EMBAJADORCELULAR" id="EMBAJADORCELULAR"><input class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group EMBAJADORDNI" id="EMBAJADORDNI"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group COMENTARIO" id="COMENTARIO"><input  class='form-control  txt_quiebre' type='text'/></td> 
                                                        <td style="width: 150px;" class="form-group EVENTO" id="EVENTO"></td>                         
                                                        <td style="width: 150px;" class="form-group FH_REG104" id="FH_REG104"><input  class='form-control  txt_quiebre' type='text'/></td>

                                                            <td style="width: 150px;" class="form-group FH_REG1L" id="FH_REG1L"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        
                                                        <td style="width: 150px;" class="form-group FH_REG2L" id="FH_REG2L"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group CODMULTIGESTION" id="CODMULTIGESTION"><input  class='form-control  txt_quiebre' type='text'/></td>

                                                        <td style="width: 150px;" class="form-group LLAMADOR" id="LLAMADOR"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group TITULAR" id="TITULAR"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group DIRECCION" id="DIRECCION"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group DISTRITO" id="DISTRITO"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group URBANIZACION" id="URBANIZACION"><input  class='form-control  txt_quiebre' type='text'/></td>

                                                        <td style="width: 150px;" class="form-group TELF_GESTION" id="TELF_GESTION"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group TELF_ENTRANTE" id="TELF_ENTRANTE"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group OPERADOR" id="OPERADOR"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="width: 150px;" class="form-group MOTIVO_CALL" id="MOTIVO_CALL"><input  class='form-control  txt_quiebre' type='text'/></td>
                                                        <td style="display: flex;padding: 13px" id="EXTRA">
                                                            <span class="btn btn-danger btn-xs btnRemove"><i class="glyphicon glyphicon-remove"></i></span>&nbsp&nbsp<span class="btn btn-success btn-xs btnAgregar"><i class="glyphicon glyphicon-check"></i></span>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <div>
                                            <button id="btnAdd" class="btn btn-yellow" style="width: 100%" type="button"><span class="glyphicon glyphicon-plus"></span> AGREGAR PEDIDO</button>
                                        </div>
                                        <input type="hidden" id="file_name" name="file_name" value="" />
                                        <!-- FIN DE NUEVO SUBIDA DE DEXCEL -->

                                    </div>
                                </div><!-- /.box -->
                            </div>
            </div>
        </div>
        <div id="log" class="tab-pane fade">
                <form name="form_movimiento" id="form_movimiento" method="post" action="reporte/movimiento" enctype="multipart/form-data">
                    <fieldset class="yellow-fieldset">           
                        <div class="row form-group" id="div_fecha">
                            <div class="col-sm-12">
                                <div class="col-sm-2">
                                    <label>Seleccione Rango de Fechas:</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" placeholder="AAAA-MM-DD - AAAA-MM-DD" id="fecha" name="fecha" onfocus="blur()"/>
                                </div>                                   
                                <div class="col-sm-3"> 
                                    <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>
                                </div>                                    
                            </div>
                        </div>
                    </fieldset>                                              
                </form>


                     <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                                <table id="tb_solicitudes" class="table table-bordered table-hover">
                                  <thead>
                                        <tr>
                                          <th>Nombre Original del archivo</th>
                                          <!--<th>Arch.Registrados</th>
                                          <th>Arch.Rechazado</th>-->
                                          <th>Num Registrados</th>
                                          <th>Num Rechazados</th>
                                          <th>Num Existentes</th>
                                          <th>Responsable</th>
                                          <th>Fecha Registro</th>
                                          <!--<th>Registrado</th>   
                                          <th>Rechazado</th>-->                                           
                            </tr>
                                  </thead>
                                  <tbody id="t_solicitudes">

                                  </tbody>
                                </table>
                            </div>                            
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
@stop
