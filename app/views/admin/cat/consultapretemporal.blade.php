<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.min.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}


{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}


@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.cat.js.consulta_ajax' )
@include( 'admin.cat.js.consulta' )
@stop
<style type="text/css">
  .form-control::-webkit-input-placeholder { color: #337ab7; opacity: 1 !important; }
  .rojo {
    border-color:#FF0000; background:#F6CECE!important;
  }
  .verde {
    border-color:#E3F6CE; background:#BCF5A9!important;
  }
  .azul {
    color: #337ab7;
  }
  .panel-heading.box {
    margin-bottom: 0px;
  }
</style>

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<input type="hidden" id="token" value="{{ csrf_token() }}">
<section class="content-header">
    <h1>
        Consulta de Pedidos
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Consulta de Pedidos</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Filtro(s):</h3>
              </div>

              <div class="panel-body">
                  <form name="form_Personalizado" id="form_Personalizado" method="POST">
                    <div class="col-xs-8">
                        <div class="form-group ">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label class="titulo pull-right">Buscar por:</label>
                                    <input type="hidden" name="bandeja" id="bandeja" value="1">
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" name="slct_tipo" id="slct_tipo">
                                        <option value="" selected disabled>Seleccione</option>
                                        <option value="ticket">Ticket de Pedido</option>
                                        <option value="codactu">Cod Avería</option>
                                        <option value="dni">DNI Cliente</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                                </div>
                                <div class="col-xs-2">
                                    <button id="btn_personalizado" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-body" style="min-height:250px">
      <div class="box-body table-responsive">
        <table id="t_consulta" class="table table-bordered table-stripe active">
            <thead class="btn-primary">
              <tr>
                <th style="width: 60px;">N° Ticket</th>
                <th style="width: 60px;">Codactu</th>
                <th style="width: 89px;">F.Registro</th>
                <th style="width: 100px;">Tipo Averia</th>
                <th style="width: 70px;">Cod Cli</th>
                <th>Quiebre</th>
                <th style="width: 70px;">Atencion</th>
                <th>Cliente</th>
                <th style="width: 100px;">Motivo</th>
                <th>Submotivo</th>
                <th>Estado Registro</th>
                <th>Cod_multigestion</th>
                <th>llamador</th>
                <th>titular</th>         
                <th>motivo_call</th>
                <th style="width: 100px !important;">Ver más</th>
              </tr>
            </thead>
            <tfoot class="btn-primary">
              <tr>
                <th>N° Ticket</th>
                <th>Codactu</th>
                <th>F.Registro</th>
                <th>Tipo Averia</th>
                <th>Cod Cli</th>
                <th>Quiebre</th>
                <th>Atencion</th>
                <th>Cliente</th>
                <th>Motivo</th>
                <th>Submotivo</th>
                <th>Estado Registro</th>
                <th>Cod_multigestion</th>
                <th>llamador</th>
                <th>titular</th>         
                <th>motivo_call</th>
                <th>Ver más</th>
              </tr>
          </tfoot>
          <tbody id="tb_consulta">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section><!-- /.content -->

@stop

@section('formulario')
    @include( 'admin.cat.form.consulta_modal' )
@stop
