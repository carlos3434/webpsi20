<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.min.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}


{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/utils.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}


@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.cat.js.embajador' )
@include( 'admin.cat.js.embajador_ajax' )
@stop
<style type="text/css">
  .form-control::-webkit-input-placeholder { color: #337ab7; opacity: 1 !important; }
  .rojo {
    border-color:#FF0000; background:#F6CECE!important;
  }
  .verde {
    border-color:#E3F6CE; background:#BCF5A9!important;
  }
  .azul {
    color: #337ab7;
  }
  .panel-heading.box {
    margin-bottom: 0px;
  }
</style>

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<input type="hidden" id="token" value="{{ csrf_token() }}">
<section class="content-header">
    <h1>
        Embajador
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">101 CAT</a></li>
        <li class="active">Embajador</li>
    </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="panel-group" id="accordion-filtros">
            <div class="panel panel-default">
                <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                           href="#collapseOne">Búsqueda Individual</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in ">
                    <div class="panel-body">
                        <form name="form_Personalizado" id="form_Personalizado" method="POST" action="">
                            <div class="col-xs-8">
                                <div class="form-group ">
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label class="titulo">Buscar por:</label>
                                            <input type="hidden" name="bandeja" id="bandeja" value="1">
                                        </div>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="tipo_busqueda" id="slct_tipo">
                                                <option value="">Seleccione</option>
                                                <option value="ticket">Ticket de Pedido</option>
                                                <option value="codactu">Reg Avería</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="txt_buscar" id="txt_buscar" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <button id="btn_personalizado" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                           href="#collapseTwo">
                            Búsqueda Personalizada
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <form name="form_General" id="form_General" method="POST" action="reporte/bandejaexcel">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <label class="titulo">Tipo Averia:</label>
                                        <select class="form-control" name="slct_averia[]" id="slct_averia"
                                                multiple>
                                            <option value="Telefono">Telefono (STB)</option>
                                            <option value="Internet">Internet (ADSL)</option>
                                            <option value="Television">Television (CATV)</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="titulo">Gestión:</label>
                                        <select class="form-control" name="slct_gestion[]" id="slct_gestion"
                                                multiple>
                                            <option value="embajador">Embajador</option>
                                            <option value="rrss">Redes Sociales</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="titulo">Atención:</label>
                                        <select class="form-control" name="slct_atencion[]" id="slct_atencion"
                                                multiple>
                                            <option value="prioritario">Normal</option>
                                            <option value="prioritario">Prioritario</option>
                                            <option value="influyente">Influyente</option>
                                            <option value="critico">Crítico</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="titulo">Estado:</label>
                                        <select class="form-control" name="slct_estado[]" id="slct_estado"
                                                multiple>
                                            <option value="bas">Recepcionado</option>
                                            <option value="adsl">Pendiente</option>
                                            <option value="catv">Cerrado</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="titulo">F. Registro:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="txt_fecha_registro"
                                                   id="txt_fecha_registro"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-2"><br>
                                        <button id="btn_general" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="box box-primary">
          <div class="box-body" style="min-height:250px">
            <div class="box-body table-responsive">
              <table id="t_embajador" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                  <thead>
                      <th>N° Ticket</th>
                      <th>Tipo Servicio</th>
                      <th>Codactu</th>
                      <th>Cliente</th>
                      <th>Gestion</th>
                      <th>Atencion</th>
                      <th>Cliente</th>
                      <th>Estado</th>
                      <th>Fecha Registro</th>
                      <th>[ ]</th>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>N° Ticket</th>
                      <th>Tipo Servicio</th>
                      <th>Codactu</th>
                      <th>Cliente</th>
                      <th>Gestion</th>
                      <th>Atencion</th>
                      <th>Cliente</th>
                      <th>Estado</th>
                      <th>Fecha Registro</th>
                      <th>[ ]</th>
                    </tr>
                </tfoot>
                <tbody id="tb_embajador">

                 </tbody>
              </table>
            </div>
          </div>
        </div>

    </div>
  </div>

</section><!-- /.content -->

@stop

@section('formulario')
    @include( 'admin.cat.form.embajador_modal' )
@stop
