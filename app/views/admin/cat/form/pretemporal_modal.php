<!-- Modal -->
<div id="pretemporalModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs logo modal-header">
              <li class="logo tab_1 active" id="tab_gestion_modal" style="display:none;">
                  <a href="#tab_1" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-gears fa-sm"></i> </button>
                      <small>GESTION</small>
                  </a>
              </li>
              <li class="logo tab_2">
                  <a href="#tab_2" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-file-o fa-sm"></i> </button>
                      <small>DATOS DE TICKET</small>
                  </a>
              </li>
              <li class="logo tab_3" id="tab_datos_averia_modal" style="display:none;">
                  <a href="#tab_3" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-edit fa-sm"></i> </button>
                      <small>DATOS DE AVERIA</small>
                  </a>
              </li>
              <li class="logo tab_6">
                  <a href="#tab_6" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-file-o fa-sm"></i> </button>
                      <small>DATOS DE ATENTO</small>
                  </a>
              </li>
              <li class="logo tab_4">
                  <a href="#tab_4" data-toggle="tab" id="bandejaModalMovimiento">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-sm"></i> </button>
                      <small>MOVIMIENTOS</small>
                  </a>
              </li>
              <li class="logo tab_4">
                  <a href="#tab_5" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-sm"></i> </button>
                      <small>REF: 101-OP</small>
                  </a>
              </li>
              <li class="pull-right">
                    <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                    </button>
              </li>
          </ul>
          <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <form id="form_bandeja" name="form_bandeja" action="" method="post" style="overflow: auto;height:450px;" class="container-fluid">
                    <input type="hidden" class="form-control input-sm" id="txt_id_modal" name="txt_id_modal" readonly>
                    <div class="row form-group">
                        <div class="col-sm-12">
                          <div class="alert alert-dismissible" style="display:none;" id="mensaje_modal">
                          </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <label class="text-primary">N° Ticket</label>
                                <input type="text" class="form-control input-sm azul" id="txt_ticket_modal" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label class="text-primary">Atencion</label>
                                <input type="text" class="form-control input-sm azul" id="txt_atencion" readonly style="background-color: yellow;">
                            </div>
                            <div class="col-sm-2">
                                <label>Estado Registro</label>
                                <input type="text" class="form-control input-sm" id="txt_estado_modal" readonly>
                            </div>
                            <div class="col-sm-2 evento hidden">
                                <label>Evento Masiva</label>
                                <input type="hidden" id="txt_idevento" name="txt_idevento">
                                <input type="text" class="form-control input-sm" id="txt_evento_modal" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Producto</label>
                                <input type="text" class="form-control input-sm" id="txt_averia_modal" readonly>
                                <input type="hidden" class="form-control input-sm tipo_averia_modal" id="txt_tipo_averia_modal" name="txt_tipo_averia_modal">
                            </div>
                            <div class="col-sm-2">
                                <label>Cod Averia</label>
                                <input type="text" class="form-control input-sm" name="txt_codactu_modal" id="txt_codactu_modal" readonly
                                onblur="upperCase(this)" onkeyup="validacionCod()">
                            </div>
                            <div class="col-sm-2">
                                <label>CodCli / Tel</label>
                                <input type="text" class="form-control input-sm" name="txt_codcli_modal" id="txt_codcli_modal" readonly>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <label>Motivo:</label>
                                <select class="form-control input-sm" id="slct_motivo_modal" name="slct_motivo_modal">
                                    <option value="">.::Seleccione::.</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Submotivo:</label>
                                <select class="form-control input-sm" id="slct_submotivo_modal" name="slct_submotivo_modal">
                                    <option value="">.::Seleccione::.</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Estado:</label>
                                <select class="form-control input-sm" id="slct_estado_modal" name="slct_estado_modal">
                                    <option value="">.::Seleccione::.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-8">
                                <label>Observación:</label>
                                <textarea maxlength="500" rows="3" class="form-control input-sm" id="txt_observacion_modal" name="txt_observacion_modal"></textarea>
                            </div>
                             <div class="col-sm-4 solucionado" style="display:none;">
                                <label>Solucionado:</label>
                                <select class="form-control input-sm" id="slct_solucionado_modal" name="slct_solucionado_modal">
                                    <option value="">.::Seleccione::.</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12" id="content_averia" style="display:none;">
                          <br>
                          <p class="text-info">Seleccione la avería que corresponde:</p>
                          <table id="t_averia" class="table table-bordered table-striped col-sm-12 responsive text-center" width="100%">
                              <thead>
                                <th>Marca</th>
                                <th style="width: 200px;">Cod Cli / N° Servicio</th>
                                <th style="width: 100px;">Cod Avería</th>
                                <th style="width: 400px;">Cliente</th>
                              </thead>
                              <tbody id="tb_averias">
                                
                              </tbody>
                          </table>
                      </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                    <button type="button" class="btn btn-primary" id="btn_gestion_modal" name="btn_gestion_modal">Guardar</button>
                </div>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <form id="ticketForm">
                  <div class="form-group" id="ficha_results">
                      <fieldset>
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <label class="text-primary">N° Ticket</label>
                                <input type="text" class="form-control input-sm azul" name="ticket" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label>Estado Registro</label>
                                <input type="text" class="form-control input-sm" name="estado" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Producto</label>
                                <input type="text" class="form-control input-sm" name="tipo_averia" readonly>
                                <input type="hidden" class="form-control input-sm" id="txt_tipo_averia_modal" name="txt_tipo_averia_modal">
                            </div>
                            <div class="col-sm-2">
                                <label>Cod Averia</label>
                                <input type="text" class="form-control input-sm" name="codactu" id="txt_codactu_modal" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>CodCli / Tel</label>
                                <input type="text" class="form-control input-sm" name="codcli" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label>Quiebre</label>
                                <input type="text" class="form-control input-sm" name="quiebre" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Atencion</label>
                                <input type="text" class="form-control input-sm" name="atencion" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Fuente</label>
                                <input type="text" class="form-control input-sm" name="fuente" readonly>
                            </div>
                            <div class="col-sm-2">
                                <label>Origen Registro</label>
                                <input type="text" class="form-control input-sm" name="gestion" readonly>
                            </div>
                            <div class="col-sm-3">
                                <label>Fecha Registro Ticket</label>
                                <input type="text" class="form-control input-sm" name="created_at" readonly>
                            </div>
                             <div class="col-sm-3 evento hidden">
                                <label>Evento Masiva</label>
                                <input type="text" class="form-control input-sm" id="txt_evento_modal_2" readonly>
                            </div>
                        </div>
                      </fieldset>
                      <!-- datos de cliente -->
                      <fieldset>
                      <legend>Datos del Cliente</legend>
                        <div class="col-sm-12">
                            <div class="col-sm-4">
                                <label>Apellidos y Nombres</label>
                                <input type="text" class="form-control input-sm" id="cliente_nom" name="cliente_nombre" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Celular de Cliente</label>
                                <input type="text" class="form-control input-sm" id="f_celcli" name="cliente_celular" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Telefono</label>
                                <input type="text" class="form-control input-sm" id="f_telcli" name="cliente_telefono" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Correo de cliente</label>
                                <input type="text" class="form-control input-sm" id="f_correocli" name="cliente_correo" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>DNI</label>
                                <input type="text" class="form-control input-sm" id="f_dnicli" name="cliente_dni" readonly>
                            </div>
                        </div>
                      
                        <div class="col-sm-12">
                            <label>Comentario</label>
                            <textarea rows="4" cols="50" class="form-control" id="f_comentario" readonly></textarea>
                        </div>
                      </fieldset>
                      <!-- datos de contacto -->
                      <fieldset>
                      <legend>Datos de Contacto</legend>
                          <div class="col-sm-12">
                              <div class="col-sm-4">
                                  <label>Apellidos y Nombres</label>
                                  <input type="text" class="form-control input-sm" id="f_nomcon" name="contacto_nombre" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Celular</label>
                                  <input type="text" class="form-control input-sm" id="f_celcon" name="contacto_celular" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Telefono</label>
                                  <input type="text" class="form-control input-sm" id="f_telefonocon" name="contacto_telefono" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Correo</label>
                                  <input type="text" class="form-control input-sm" id="f_correocon" name="contacto_correo" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>DNI</label>
                                  <input type="text" class="form-control input-sm" id="f_dnicon" name="contacto_dni" readonly>
                              </div>
                          </div>
                      </fieldset>

                      <!-- datos de embajador -->
                      <fieldset>
                      <legend>Datos de <span id="lbl_registrador">Embajador</span></legend>
                          <div class="col-sm-3">
                              <label>Apellidos y Nombres</label>
                              <input type="text" class="form-control input-sm" id="f_nomemb" name="embajador_nombre" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Celular</label>
                              <input type="text" class="form-control input-sm" id="f_celemb" name="embajador_celular" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Correo</label>
                              <input type="text" class="form-control input-sm" id="f_correoemb" name="embajador_correo" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>DNI</label>
                              <input type="text" class="form-control input-sm" id="f_dnioemb" name="embajador_dni" readonly>
                          </div>
                      </fieldset>
                  </div><!-- ficha_results -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <form id="averiaForm">
                  <!-- PSI /Legados -->
                  <div class="row form-group" id="averia_results">

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Plazo</label>
                              <input type="text" class="form-control input-sm" name="plazo"  id="plazo" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Estado Legado</label>
                              <input type="text" class="form-control input-sm" name="estado_legado" id="a_estado" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fecha de Registro Legado</label>
                              <input type="text" class="form-control input-sm" name="fecha_registro_legado" id="a_fecha" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Cod Avería</label>
                              <input type="text" class="form-control input-sm" name="codactu" id="a_codactu" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Cod Cli/N°Servicio</label>
                              <input type="text" class="form-control input-sm" name="codcli" id="a_telefono" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Cliente</label>
                              <input type="text" class="form-control input-sm" name="nombre_cliente" id="a_cliente" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Contrata</label>
                              <input type="text" class="form-control input-sm" name="contrata" id="a_contrata" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Area</label>
                              <input type="text" class="form-control input-sm azul" name="area"  id="a_area" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Zonal</label>
                              <input type="text" class="form-control input-sm" name="zonal" id="a_zonal" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Codigo Liquidacion</label>
                              <input type="text" class="form-control input-sm" name="cod_liquidacion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Detalle Liquidacion</label>
                              <input type="text" class="form-control input-sm" name="detalle_liquidacion" readonly>
                          </div>

                          <div class="col-sm-4">
                              <label>Fecha Liquidación</label>
                              <input type="text" class="form-control input-sm" name="fecha_liquidacion" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>MDF</label>
                              <input type="text" class="form-control input-sm" name="mdf" readonly>
                          </div>
                          <div class="col-sm-8">
                              <label>Observación</label>
                              <input type="text" class="form-control input-sm" name="observacion" readonly>
                          </div>
                      </div>

                  </div><!--/PSI /Legados -->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                  <div class="row form-group" style="overflow: auto;height:500px;">
                      <form name="form_Movimientos" id="form_Movimientos" method="POST" action="reporte/movimientosexcel">
                          <div class="col-sm-12">
                              <input type="hidden" name="busqueda" id="busqueda">
                              <div class="col-sm-3">
                                  <label class="text-primary">N° Ticket</label>
                                  <input type="text" class="form-control input-sm azul" id="txt_ticket_modal_mov" readonly>
                              </div>
                              <div class="col-sm-3">
                                  <label>Estado Registro</label>
                                  <input type="text" class="form-control input-sm" id="txt_estado_modal_mov" readonly>
                              </div>
                              <div class="col-sm-2">
                                  <label>Producto</label>
                                  <input type="text" class="form-control input-sm" id="txt_averia_modal_mov" readonly>
                              </div>
                              <div class="col-sm-2">
                                  <label>Cod Averia</label>
                                  <input type="text" class="form-control input-sm" id="txt_codactu_modal_mov" readonly>
                              </div>
                              <div class="col-sm-2">
                                <label>CodCli / Tel</label>
                                <input type="text" class="form-control input-sm" id="txt_codcli_modal_mov" readonly>
                            </div>
                          </div>
                      </form>
                      <div class="col-sm-12">
                          <br/>
                          <table id="t_movimiento" class="table table-bordered table-striped">
                              <thead class="btn-primary">
                                  <tr>
                                      <th>ID</th>
                                      <th>Motivo</th>
                                      <th>Submotivo</th>
                                      <th>Estado Registro</th>
                                      <th>Observación</th>
                                      <th>Fecha</th>
                                      <th>Usuario</th>
                                      <th>Solución&nbsp;<a onclick="descargarMovimientoReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a></th>
                                  </tr>
                              </thead>
                              <tbody id="tb_movimiento">
                              </tbody>
                              <tfoot class="btn-primary">
                                  <tr>
                                      <th>ID</th>
                                      <th>Motivo</th>
                                      <th>Submotivo</th>
                                      <th>Estado Registro</th>
                                      <th>Observación</th>
                                      <th>Fecha</th>
                                      <th>Usuario</th>
                                      <th>Solución&nbsp;<a onclick="descargarMovimientoReporte();" class="btn btn-success"><i
                                                class="fa fa-download fa-lg"></i></a></th>
                                  </tr>
                              </tfoot>
                          </table>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_5">
                <div class="row form-group" style="overflow: auto;height:500px;">
                  <form id="mensajeForm">
                    <!-- Asunto -->
                         <div class="col-sm-1">
                        Asunto:
                        </div>
                        <div class="col-sm-10">
                          <input type="text" class="form-control input-sm" id="asunto" readonly>
                        </div>
                        <br><br>
                        <div class="col-sm-12">
                          <p style="font-size: 20px;">Estimados,<br><br>
                          Agradeceré confirmar agenda y atención al presente requerimiento, ingresado con motivo de atención diferenciada <label id="quiebre_cat"></label>
                          </p>
                        </div>
                  </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                </div>
              </div><!-- /.tab-pane -->



              <div class="tab-pane" id="tab_6">
                <form id="FormAtento">
                  <!-- PSI /Legados -->
                  <div class="row form-group" id="atento_results">

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Fh_reg104</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg104"  id="txt_fhreg104" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fh_reg1l</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg1l" id="txt_fhreg1l" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fh_reg2l</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg2l" id="txt_fhreg2l" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Cod_multigestion</label>
                              <input type="text" class="form-control input-sm" name="txt_codmultigestion" id="txt_codmultigestion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>llamador</label>
                              <input type="text" class="form-control input-sm" name="txt_llamador" id="txt_llamador" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Titular</label>
                              <input type="text" class="form-control input-sm" name="txt_titular" id="txt_titular" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Dirección</label>
                              <input type="text" class="form-control input-sm" name="txt_direccion" id="txt_direccion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Distrito</label>
                              <input type="text" class="form-control input-sm azul" name="txt_distrito"  id="txt_distrito" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Urbanizacion</label>
                              <input type="text" class="form-control input-sm" name="txt_urbanizacion" id="txt_urbanizacion" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Telf_gestion</label>
                              <input type="text" class="form-control input-sm" name="txt_telfgestion" id="txt_telfgestion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Telf_entrante</label>
                              <input type="text" class="form-control input-sm" name="txt_telfentrante" id="txt_telfentrante" readonly>
                          </div>

                          <div class="col-sm-4">
                              <label>Operador</label>
                              <input type="text" class="form-control input-sm" name="txt_operador" id="txt_operador" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-12">
                              <label>Motivo_call</label>
                              <textarea class="form-control input-sm" name="txt_motivo_llamada" id="txt_motivo_llamada" readonly></textarea>
                          </div>
                      </div>

                  </div><!--/PSI /Legados -->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->



          </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
      </div><!--/modal-content -->
  </div>
</div><!--/modal-->
