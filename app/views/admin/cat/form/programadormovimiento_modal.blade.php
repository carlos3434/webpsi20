<div id="nuevoEvento" class="modal fade in" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <form class="frmCrearmasiva" name="frmCrearcomando" id="frmCrearcomando" method="post">
      <div class="modal-content">
        <div class="modal-header" style="border-bottom: 0px">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title pull-left">Editar Comando</h4>
          <div class="col-sm-3">
            <select class="form-control" name="slct_estado_programacion" id="slct_estado_programacion">
              <option value="0">Normal</option>
              <option value="1">Recursivo</option>
            </select>
          </div>
        </div>
        <div class="modal-body" style="border-top:1px;margin-top: 30px; padding-top: 0px;">
          <div class="row">
              <input type="hidden" name="txt_id" id="txt_id" value="15">
              <div class="col-md-12">
                <div class="col-md-12">
                  <div class="panel-group" id="accordion-filtros-modal"> 
                    <div class="panel panel-default">
                      <div class="panel-heading box box-primary" style="margin-bottom:0px; color: #3c8dbc;">
                          <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros-modal" aria-expanded="true">
                                  Datos a buscar en el ticket
                              </a>
                          </h4>
                      </div>
                      <div id="collapseOneModal" class="panel-collapse collapse in" aria-expanded="true">
                        <div class="panel-body">
                          <div class="row form-group" style="margin-bottom: 0px">
                            <div class="col-md-12">
                              
                              <div class="col-md-4">
                                  <label class="titulo">Quiebres:</label>
                                  <select class="form-control" name="slct_quiebre[]" id="slct_quiebre" multiple=""></select>
                              </div>

                              <div class="col-md-4">
                                  <label class="titulo">Atención:</label>
                                  <select class="form-control" name="slct_atencion[]" id="slct_atencion" multiple=""></select>
                              </div>
                              <div class="col-sm-4">
                                <label class="titulo">Seleccionar el tipo de fecha:</label>
                                <select class="form-control" name="slct_tipo_fecha" id="slct_tipo_fecha">
                                  <option value="1">Fecha de registro PSI</option>
                                  <option value="2">Fecha de llamada al cliente</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                              

                              
                            </div>  
                            <div class="col-md-12" style="margin-top: 10px">                               
                              <div class="col-sm-4">
                                  <label class="titulo">Intervalos de búsqueda:</label>
                                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" name="txt_fechas_busqueda" id="txt_fechas_busqueda" readonly="">
                                  </div>
                              </div>
                              <div class="col-sm-2">
                                <label>Hora de inicio</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input id="txt_dias_hora_inicio" name="txt_dias_hora_inicio" type="text" class="form-control input-small" readonly="">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                              </div>
                              <div class="col-sm-2">  
                                <label>Hora de final</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input id="txt_dias_hora_fin" name="txt_dias_hora_fin" type="text" class="form-control input-small" readonly="">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                              </div>
                              <div class="col-md-4 form-dias">
                                  <label class="titulo">Días:</label>
                                  <select class="form-control" name="slct_dias[]" id="slct_dias" multiple="">
                                  </select>
                              </div>
                              
                            </div>  
            
                    
                              <div class="col-sm-12" style="margin-top: 10px;"> 
                                
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="panel-group" id="accordion-filtros-modal"> 
                    <div class="panel panel-default">
                      <div class="panel-heading box box-primary" style="margin-bottom:0px; color: #3c8dbc;">
                          <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros-modal" aria-expanded="true">
                                  Comando
                              </a>
                          </h4>
                      </div>
                      <div id="collapseFourModal" class="panel-collapse collapse in" aria-expanded="true">
                        <div class="panel-body">
                          <div class="row form-group" style="margin-bottom: 0px">
                            <div class="col-md-12">
                              
                              <div class="col-md-5">
                                  <label>Nombre:</label>
                                  <input class="form-control" type="text" name="txt_nombre" id="txt_nombre" required="">
                              </div>
                              <div class="col-md-3">
                                <label class="titulo">Estado del comando:</label>
                                <select class="form-control" name="slct_estado" id="slct_estado">
                                  <option value="1">Activo</option>
                                  <option value="0">Inactivo</option>
                                </select>
                              </div>
                            
                              <div class="col-md-4">
                                <label class="titulo">Intervalos de ejecución:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="txt_fechas_comando" id="txt_fechas_comando" readonly="">
                                </div>
                              </div>
                            </div>  
                          </div>
                          
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="panel-group" id="accordion-filtros-modal"> 
                      <div class="panel panel-default">
                          <div class="panel-heading box box-primary" style="margin-bottom:0px; color: #3c8dbc;">
                              <h4 class="panel-title">
                                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros-modal" aria-expanded="true">
                                      Datos para generar movimientos
                                  </a>
                              </h4>
                          </div>
                          <div id="collapseTwoModal" class="panel-collapse collapse in" aria-expanded="true">

                              <div class="panel-body">
                                  <div class="row form-group" style="margin-bottom: 0px">
                                      
                                      <div class="col-sm-12" style="margin-top: 10px;">  
                                        <div class="col-md-4">
                                          <label class="titulo">Motivos:</label>
                                          <select class="form-control" name="slct_motivo" id="slct_motivo">
                                          </select>
                                        </div>
                                        <div class="col-md-4">
                                          <label class="titulo">SubMotivos:</label>
                                          <select class="form-control" name="slct_submotivo" id="slct_submotivo">
                                          </select>
                                        </div>
                                        <div class="col-md-4">
                                          <label class="titulo">Estado:</label>
                                          <select class="form-control" name="slct_estado_pre" id="slct_estado_pre">
                                          </select>
                                        </div>
					<div class="col-md-6 solucionado" style="display: none; margin-top: 10px;">
                                          <label class="titulo">Solucionado:</label>
                                          <select class="form-control" name="slct_solucionado" id="slct_solucionado"></select> 
                                        </div>
                                      </div>
                                      <div class="col-sm-12" style="margin-top: 10px;">    
                                        <div class="col-sm-12">
                                          <label>Observación</label>
                                          <div class="form-group">
                                              <input id="txt_observacion" name="txt_observacion" type="text" class="form-control input-small" >
                                          </div>
                                        </div>
 
                                      </div>
                                  </div>
                             
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
                
                
              </div>        
          </div>
                                   
        </div><!--/modal-body-->

        <div class="modal-footer" style="margin-top:-15px;">
          <input type="submit" name="btn_guardar" id="btn_guardar" class="btn btn-primary btn-md" value="Guardar">
          <!---<span class="btn btn-primary btn-md buscarAverias hidden" onclick="cargarAverias(this)">Cargar Averias <i class="glyphicon glyphicon-upload"></i></span>-->
        </div>
      </div><!--/modal-content -->
    </form>

  </div>
</div>
