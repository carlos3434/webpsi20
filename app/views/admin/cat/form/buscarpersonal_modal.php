<div id="buscarPersonal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Buscar Persona</h4>
      </div>
      <div class="modal-body">
        <table class="table table-hover table-bordered table-stripped">
          
        </table>
      </div><!--/modal-body-->

      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
      </div>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
<!-- Modal -->
