<div id="nuevoUsuario" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Registrar Nuevo Usuario</h4>
      </div>
      <div class="modal-body" style="border-top:1px;margin-top: 10px">
        <form id="formNuevoUsuario" name="formNuevoUsuario">
          <div class="row">
            <div class="col-md-6">
              <div class="col-md-12">
                <label>Nombre: </label>
                <input class="form-control" type="text"  id="txt_nombre" name="txt_nombre">
              </div>
              <div class="col-md-12">
                <label>Apellidos: </label>
                <input class="form-control" type="text" id="txt_apellidos"  name="txt_apellidos">
              </div>
              <div class="col-md-12">
                <label>DNI: </label>
                <input class="form-control" type="text"  id="txt_dni" name="txt_dni">
              </div>
            </div>
            <div class="col-md-6">
               <div class="col-md-12">
                <label>Empresa: </label>
                <select class="form-control" id="slct_empresa" name="slct_empresa"></select>
              </div>                                          
              <div class="col-md-12">
                   <label>Usuario: </label>
                <input class="form-control" type="text"  id="txt_usuario" name="txt_usuario">
              </div>
              <div class="col-md-12">
                <label>Password: </label>
                <input class="form-control" type="password"  id="txt_password" name="txt_password">
              </div>
            </div>
          </div>
        </form>
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top:0px;">
        <span class="btn btn-primary btn-md" onclick="guardar()">Guardar <i class="glyphicon glyphicon-plus"></i></span>
      </div>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
<!-- Modal -->
