<div id="validaPersonal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="margin-bottom: -20px;border-bottom:0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Personal por Validar</h4>
        <br><hr>        
      </div>
      <div class="modal-body">
        <table class="table table-hover table-bordered table-stripped" id="tblValidaciones">
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Dni</th>
              <th>Tipo Persona</th>
              <th>Celular</th>
              <th>Valido</th>
            </tr>
          </thead>
          <tbody>
                        
          </tbody>          
        </table>
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top: 0px;">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
      </div>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
<!-- Modal -->
