<div id="nuevoEvento" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
        <form class="frmCrearmasiva" name="frmCrearmasiva" id="frmCrearmasiva" method="post" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Registrar Nuevo Evento</h4>
      </div>
      <div class="modal-body" style="border-top:1px;margin-top: 10px">
                            <div class="row">
                                <div class="col-md-8">
                                     <div class="col-md-6">
                                        <label>Nombre:</label>
                                        <input class="form-control" type="text" name="txt_nombre" id="txt_nombre" disabled>
                                    </div>
                                     <div class="col-md-6">
                                        <label>Tipo Trabajo:</label>
                                        <select class="form-control" name="slct_tipo_trabajo" id="slct_tipo_trabajo">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="1">Masiva</option>
                                            <option value="2">Trabajo Programado</option>
                                        </select>
                                    </div>
                                     <div class="col-md-6">
                                        <label>Tipo de Servicio:</label>
                                        <select class="form-control" name="slct_tecnologia" id="slct_tecnologia">
                                            <option value="">.::Seleccione::.</option>
                                            <option value="1">CATV</option>
                                            <option value="2">M1</option>
                                            <option value="3">CATV + M1</option>
                                        </select>
                                    </div>                                                                         
                                      <div class="col-md-6">
                                        <label>Zonal:</label>
                                        <select class="form-control" name="slct_zonal" id="slct_zonal"></select>      
                                    </div>                                                          
                                    <div class="col-md-6">
                                        <label>Empresa:</label>
                                        <select class="form-control" name="slct_empresa_id" id="slct_empresa_id"></select>                                    
                                    </div>
                                    <div class="col-md-6">
                                        <label>Nodo:</label>
                                        <select class="form-control" name="slct_nodo" id="slct_nodo"></select>
                                    </div>                                                                          
                                </div>
                                <div class="col-md-4">
                                     <fieldset class="yellow-fieldset">
                                        <legend>Imagenes</legend>
                                         <div class="row form-group">
                                             <div class="col-md-2">
                                                <input type="file" class="file" numero="1" name="txt_file" id="txt_file" style="display:none">
                                                <span class="btn btn-primary btn-md openFile" target_f="txt_file"><i class="glyphicon glyphicon-plus"></i></span>
                                             </div>
                                             <div class="col-md-10">
                                                 <input class="form-control" type="text" name="txt_image1" id="txt_image1">
                                             </div>
                                         </div>
                                          <div class="row form-group">
                                             <div class="col-md-2">
                                                <input type="file" class="file" numero="2" name="txt_file2" id="txt_file2" style="display:none">
                                                <span class="btn btn-primary btn-md openFile" target_f="txt_file2"><i class="glyphicon glyphicon-plus"></i></span>
                                             </div>
                                             <div class="col-md-10">
                                                 <input class="form-control" type="text" name="txt_image2" id="txt_image2">
                                             </div>
                                         </div>
                                          <div class="row form-group">
                                             <div class="col-md-2">
                                                <input type="file" class="file" numero="3" name="txt_file3" id="txt_file3" style="display:none">
                                                <span class="btn btn-primary btn-md openFile" target_f="txt_file3"><i class="glyphicon glyphicon-plus"></i></span>
                                             </div>
                                             <div class="col-md-10">
                                                 <input class="form-control" type="text" name="txt_image3" id="txt_image3">
                                             </div>
                                         </div>
                                     </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label>Carnet de Tecnico:</label>
                                        <input class="form-control" type="text" name="txt_tecnico_liquidacion" id="txt_tecnico_liquidacion">
                                    </div>   
                                    <div class="col-md-4">
                                        <label>Troba:</label>
                                       <select class="form-control" name="slct_troba" id="slct_troba"></select>                                   
                                    </div>                             
                                                                  
                                     <div class="col-md-4">
                                        <label>Amplificador:</label>
                                        <input class="form-control" type="text" name="txt_amplificador" id="txt_amplificador">                                    
                                    </div>                                                                                        
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div>
                                                <label>Codigo de liquidación:</label>
                                                <input class="form-control" type="text" name="txt_cod_liquidacion" id="txt_cod_liquidacion">
                                            </div>
                                            <div>
                                                <label>Sub Codigo liquidación:</label>
                                                <input class="form-control" type="text" name="txt_sub_cod_liquidacion" id="txt_sub_cod_liquidacion">                                    
                                            </div>
                                            <!--<div>
                                                <label>Codigo de Masiva:</label>
                                                <input class="form-control" type="text" name="txt_codmasiva" id="txt_codmasiva">                                    
                                            </div> -->                                                        
                                        </div>
                                        <div class="col-md-8">
                                            <label>Observacion:</label>
                                            <textarea class="form-control" name="txt_observacion" id="txt_observacion" rows="4"></textarea>
                                        </div>
                                    </div>        
                            </div>
                            <br>
                            <div class="row liquidacion hidden">
                              <div class="col-md-12">
                                <fieldset class="yellow-fieldset">
                                  <legend>Datos Liquidacion de Masiva</legend>
                                  <div class="col-md-4 form-group">
                                    <label>Codigo de Franqueo:</label>
                                    <input class="form-control" type="text" name="txt_cod_franqueo" id="txt_cod_franqueo">
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <label>Fecha Liquidación:</label>
                                    <input class="form-control" type="text" name="txt_fechaliqui" id="txt_fechaliqui" disabled>
                                  </div>
                                  <div class="col-md-4 form-group">
                                    <label>Seleccione imagen:</label>
                                    <div style="display:flex">
                                     <input type="file" class="file" numero="4" name="txt_adjunto" id="txt_adjunto" style="display:none">
                                     <span class="btn btn-primary btn-md openFile" target_f="txt_adjunto"><i class="glyphicon glyphicon-plus"></i></span>
                                     <input class="form-control" type="text" name="txt_image4" id="txt_image4">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <label>Observacion Liquidacion</label>
                                    <textarea class="form-control" id="txt_obs_liquidacion" name="txt_obs_liquidacion" rows="3" placeholder="Comentario..."></textarea>
                                  </div>
                                  <br>
                                </fieldset>
                              </div>
                            </div>                            
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top:0px;">
        <input type="submit" name="btn_guardar" id="btn_guardar" class="btn btn-primary btn-md" value="Guardar"/>
        <!---<span class="btn btn-primary btn-md buscarAverias hidden" onclick="cargarAverias(this)">Cargar Averias <i class="glyphicon glyphicon-upload"></i></span>-->
      </div>
    </div><!--/modal-content -->
  </form>

  </div>
</div><!--/modal-->
<!-- Modal -->


<div id="detalleEvent" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
        
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Detalle Evento</h4>
      </div>
      <div class="modal-body" style="border-top:1px;margin-top: 10px">
          <input type="hidden" name="txt_idevento" id="txt_idevento">
         <section class="content">
            <ul class="nav nav-pills  nav-justified">
                <li class="active"><a data-toggle="pill" href="#averias">Averias Asociadas</a></li>
                <li><a data-toggle="pill" href="#imagenes">Imagenes</a></li>
            </ul>

            <div class="tab-content">
              <div id="averias" class="tab-pane fade in active">
                  <div class="box-body table-responsive">
                      <table id="tb_tickets" class="table table-bordered table-hover">
                          <thead>
                               <tr>
                                  <th style="width: 10%">Quiebre</th>
                                  <th style="width: 30%">Codcli</th>
                                  <th style="width: 10%">Cod Averia</th>
                                  <th style="width: 10%">Cliente</th>
                                  <th style="width: 10%">Estado</th>
                                  <th style="width: 10%">Telefono 1</th>
                                  <th style="width: 10%">Telefono 2</th>
                                  <th style="width: 10%">Telefono 3</th>
                              </tr>
                          </thead>
                          <tbody id="tbody_tickets">

                          </tbody>
                      </table>
                  </div>
                </div>
                <div id="imagenes" class="tab-pane fade">
                  <div class="box-body imgEventos">
                    <div style="width: 50%">
                      <img src="" id="img1"/>                      
                    </div>
                     <div style="width: 50%">
                      <img src="" id="img2"/>                      
                    </div>
                     <div style="width: 50%">
                      <img src="" id="img3"/>                      
                    </div>
                  </div>
                </div>
            </div>
          </section>
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top:0px;">
        <span class="btn btn-success btn-sm" onclick="exportCodigoCli()">Cod Cliente <i class="glyphicon glyphicon-download"></i></span>
        <span class="btn btn-success btn-sm" onclick="exportTelefono()">Telefonos <i class="glyphicon glyphicon-download"></i></span>
      </div>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
