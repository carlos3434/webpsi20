<div id="nuevoGrupo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Crear Nuevo Grupo</h4>
      </div>
      <div class="modal-body" style="border-top:1px;margin-top: 10px">
        <form id="formNuevoGrupo" name="formNuevoGrupo">
          <div class="row">
            <div class="col-md-12 form-group">
                <label>Nombre Grupo:  </label>
                <input class="form-control" type="text"  id="txt_nombreg" name="txt_nombreg">          
            </div>
             <div class="col-md-12 form-group">
                <label>Quiebre de Trabajo:  </label>
                 <select class="form-control" id="slct_quiebre2" name="slct_quiebre2"></select>          
            </div>
            <div class="col-md-12 form-group">
               <label>Asociar Usuario(s):  </label>
                <div class="box-body table-responsive">
                  <div class="tblgrupo" style="max-height: 450px;height: 450px;overflow: hidden;">
                  <table id="tg_personal" class="table table-bordered table-hover">
                      <thead>
                            <tr>
                              <th style="width: 2%">N°</th>
                              <th style="width: 40%">Nombres</th>
                              <th style="width: 40%">Apellidos</th>
                              <th style="width: 5%"></th>
                          </tr>
                      </thead>
                      <tbody id="tbgbody_personal">

                      </tbody>
                  </table>
                  </div>
                </div>                            
              </div>
          </div>
        </form>
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top:0px;">
        <span class="btn btn-primary btn-md" onclick="crearGrupo()">Guardar <i class="glyphicon glyphicon-plus"></i></span>
      </div>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
<!-- Modal -->
