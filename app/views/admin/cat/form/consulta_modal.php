<!-- Modal -->
<div id="consultaModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs logo modal-header">
              <li class="logo tab_2 active">
                  <a href="#tab_2" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-file-o fa-sm"></i> </button>
                      <small>DATOS DE TICKET</small>
                  </a>
              </li>
              <li class="logo tab_3" id="tab_datos_averia_modal" style="display:none;">
                  <a href="#tab_3" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-edit fa-sm"></i> </button>
                      <small>DATOS DE AVERIA</small>
                  </a>
              </li>
              <li class="logo tab_4">
                  <a href="#tab_4" data-toggle="tab" id="bandejaModalMovimiento">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-sm"></i> </button>
                      <small>MOVIMIENTOS</small>
                  </a>
              </li>
              <li class="logo tab_6">
                  <a href="#tab_6" data-toggle="tab">
                      <button class="btn btn-primary btn-sm"><i class="fa fa-file-o fa-sm"></i> </button>
                      <small>DATOS DE ATENTO</small>
                  </a>
              </li>
              <li class="pull-right">
                    <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                    </button>
              </li>
          </ul>
          <div class="tab-content">
              <div class="tab-pane active" id="tab_2">
                <form id="ticketForm">
                  <div class="form-group" id="ficha_results">
                      <div class="row">
                        <div class="col-sm-12">
                          <input type="hidden" name="busqueda" id="busqueda">
                          <div class="col-sm-3">
                              <label>Estado Registro</label>
                              <input type="text" class="form-control input-sm txt_estado_modal_mov" id="txt_estado_modal_mov" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Producto</label>
                              <input type="text" class="form-control input-sm txt_averia_modal_mov" id="txt_averia_modal_mov" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Cod Averia</label>
                              <input type="text" class="form-control input-sm txt_codactu_modal_mov" id="txt_codactu_modal_mov" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label class="text-primary">N° Ticket</label>
                              <input type="text" class="form-control input-sm azul txt_ticket_modal_mov" id="txt_ticket_modal_mov" readonly>
                          </div>
                        </div>
                      </div>
                      <!-- datos de cliente -->
                      <fieldset>
                      <legend>Datos del Cliente</legend>
                          <div class="col-sm-12">
                            <div class="col-sm-4">
                                <label>Apellidos y Nombres</label>
                                <input type="text" class="form-control input-sm" id="cliente_nom" name="cliente_nombre" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Celular de Cliente</label>
                                <input type="text" class="form-control input-sm" id="f_celcli" name="cliente_celular" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Telefono</label>
                                <input type="text" class="form-control input-sm" id="f_telcli" name="cliente_telefono" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Correo de cliente</label>
                                <input type="text" class="form-control input-sm" id="f_correocli" name="cliente_correo" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>DNI</label>
                                <input type="text" class="form-control input-sm" id="f_dnicli" name="cliente_dni" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Cod Cliente</label>
                                <input type="text" class="form-control input-sm" id="f_codcli" name="codcli" readonly>
                             </div>
                          </div>
                      </fieldset>

                      <!-- datos de contacto -->
                      <fieldset>
                      <legend>Datos de Contacto</legend>
                          <div class="col-sm-12">
                              <div class="col-sm-4">
                                  <label>Apellidos y Nombres</label>
                                  <input type="text" class="form-control input-sm" id="f_nomcon" name="contacto_nombre" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Celular</label>
                                  <input type="text" class="form-control input-sm" id="f_celcon" name="contacto_celular" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Telefono</label>
                                  <input type="text" class="form-control input-sm" id="f_telefonocon" name="contacto_telefono" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>Correo</label>
                                  <input type="text" class="form-control input-sm" id="f_correocon" name="contacto_correo" readonly>
                              </div>
                              <div class="col-sm-4">
                                  <label>DNI</label>
                                  <input type="text" class="form-control input-sm" id="f_dnicon" name="contacto_dni" readonly>
                              </div>
                          </div>
                      </fieldset>

                      <!-- datos de embajador -->
                      <fieldset>
                      <legend>Datos de <span id="lbl_registrador">Embajador</span></legend>
                          <div class="col-sm-3">
                              <label>Apellidos y Nombres</label>
                              <input type="text" class="form-control input-sm" id="f_nomemb" name="embajador_nombre" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Celular</label>
                              <input type="text" class="form-control input-sm" id="f_celemb" name="embajador_celular" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>Correo</label>
                              <input type="text" class="form-control input-sm" id="f_correoemb" name="embajador_correo" readonly>
                          </div>
                          <div class="col-sm-3">
                              <label>DNI</label>
                              <input type="text" class="form-control input-sm" id="f_dnioemb" name="embajador_dni" readonly>
                          </div>
                      </fieldset>

                      <div class="row form-group">
                        <div class="col-sm-12">
                            <label>Comentario</label>
                            <textarea rows="4" cols="50" class="form-control" id="f_comentario" readonly></textarea>
                        </div>
                      </div>
                  </div><!-- ficha_results -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <form id="averiaForm">
                  <!-- PSI /Legados -->
                  <div class="row form-group" id="averia_results">

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Plazo</label>
                              <input type="text" class="form-control input-sm" name="plazo"  id="plazo" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Estado Legado</label>
                              <input type="text" class="form-control input-sm" name="estado_legado" id="a_estado" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fecha de Registro Legado</label>
                              <input type="text" class="form-control input-sm" name="fecha_registro_legado" id="a_fecha" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Cod Avería</label>
                              <input type="text" class="form-control input-sm" name="codactu" id="a_codactu" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Cod Cli/N°Servicio</label>
                              <input type="text" class="form-control input-sm" name="codcli" id="a_telefono" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Cliente</label>
                              <input type="text" class="form-control input-sm" name="nombre_cliente" id="a_cliente" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label class="text-primary">Area</label>
                              <input type="text" class="form-control input-sm azul" name="area"  id="a_area" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label class="text-primary">Estacion</label>
                              <input type="text" class="form-control input-sm" name="estacion" style="color: #337ab7;" id="a_estacion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label class="text-primary">Rubro</label>
                              <input type="text" class="form-control input-sm" name="rubro" style="color: #337ab7;" id="a_rubro" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Contrata</label>
                              <input type="text" class="form-control input-sm" name="contrata" id="a_contrata" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Quiebre</label>
                              <input type="text" class="form-control input-sm" name="quiebre" id="a_quiebre" readonly>
                          </div>

                          <div class="col-sm-4">
                              <label>Zonal</label>
                              <input type="text" class="form-control input-sm" name="zonal" id="a_zonal" readonly>
                          </div>

                      </div>

                  </div><!--/PSI /Legados -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                  <div class="row form-group" style="overflow: auto;height:500px;">
                      <form name="form_Movimientos" id="form_Movimientos" method="POST" action="reporte/movimientosexcel">
                          <div class="col-sm-12">
                              <input type="hidden" name="busqueda" id="busqueda">
                              <div class="col-sm-3">
                                  <label>Estado Registro</label>
                                  <input type="text" class="form-control input-sm txt_estado_modal_mov" id="txt_estado_modal_mov" readonly>
                              </div>
                              <div class="col-sm-3">
                                  <label>Producto</label>
                                  <input type="text" class="form-control input-sm txt_averia_modal_mov" id="txt_averia_modal_mov" readonly>
                              </div>
                              <div class="col-sm-3">
                                  <label>Cod Averia</label>
                                  <input type="text" class="form-control input-sm txt_codactu_modal_mov" id="txt_codactu_modal_mov" readonly>
                              </div>
                              <div class="col-sm-3">
                                  <label class="text-primary">N° Ticket</label>
                                  <input type="text" class="form-control input-sm azul txt_ticket_modal_mov" id="txt_ticket_modal_mov" readonly>
                              </div>
                          </div>
                      </form>
                      <div class="col-sm-12">
                          <br/>
                          <table id="t_movimiento" class="table table-bordered table-striped">
                              <thead class="btn-primary">
                                  <tr>
                                      <th>ID</th>
                                      <th>Motivo</th>
                                      <th>Submotivo</th>
                                      <th>Estado Registro</th>
                                      <th>Observación</th>
                                      <th>Fecha</th>
                                      <th>Usuario</th>
                                      <th>Solución</th>
                                  </tr>
                              </thead>
                              <tbody id="tb_movimiento">
                              </tbody>
                              <tfoot class="btn-primary">
                                  <tr>
                                      <th>ID</th>
                                      <th>Motivo</th>
                                      <th>Submotivo</th>
                                      <th>Estado Registro</th>
                                      <th>Observación</th>
                                      <th>Fecha</th>
                                      <th>Usuario</th>
                                      <th>Solución</th>
                                  </tr>
                              </tfoot>
                          </table>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
              </div><!-- /.tab-pane -->

              
              <div class="tab-pane" id="tab_6">
                <form id="FormAtento">
                  <!-- PSI /Legados -->
                  <div class="row form-group" id="atento_results">

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Fh_reg104</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg104"  id="txt_fhreg104" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fh_reg1l</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg1l" id="txt_fhreg1l" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Fh_reg2l</label>
                              <input type="text" class="form-control input-sm" name="txt_fhreg2l" id="txt_fhreg2l" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Cod_multigestion</label>
                              <input type="text" class="form-control input-sm" name="txt_codmultigestion" id="txt_codmultigestion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>llamador</label>
                              <input type="text" class="form-control input-sm" name="txt_llamador" id="txt_llamador" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Titular</label>
                              <input type="text" class="form-control input-sm" name="txt_titular" id="txt_titular" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Dirección</label>
                              <input type="text" class="form-control input-sm" name="txt_direccion" id="txt_direccion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Distrito</label>
                              <input type="text" class="form-control input-sm azul" name="txt_distrito"  id="txt_distrito" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Urbanizacion</label>
                              <input type="text" class="form-control input-sm" name="txt_urbanizacion" id="txt_urbanizacion" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-4">
                              <label>Telf_gestion</label>
                              <input type="text" class="form-control input-sm" name="txt_telfgestion" id="txt_telfgestion" readonly>
                          </div>
                          <div class="col-sm-4">
                              <label>Telf_entrante</label>
                              <input type="text" class="form-control input-sm" name="txt_telfentrante" id="txt_telfentrante" readonly>
                          </div>

                          <div class="col-sm-4">
                              <label>Operador</label>
                              <input type="text" class="form-control input-sm" name="txt_operador" id="txt_operador" readonly>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <div class="col-sm-12">
                              <label>Motivo_call</label>
                              <textarea class="form-control input-sm" name="txt_motivo_llamada" id="txt_motivo_llamada" readonly></textarea>
                          </div>
                      </div>

                  </div><!--/PSI /Legados -->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                  </div>
                </form>
              </div><!-- /.tab-pane -->

          </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
      </div><!--/modal-content -->
  </div>
</div><!--/modal-->
