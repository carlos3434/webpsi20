
<div id="cancelMasiva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="formCancelacion">
          <div class="modal-header" style="border-bottom: 0px">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title pull-left">Cancelacion</h4>
          </div>
          <div class="modal-body" style="border-top:1px;margin-top: 10px">
            <input type="hidden" name="txt_idevento" id="txt_idevento">
            <label>¿Esta seguro de la cancelar la masiva?</label>
            <textarea class="form-control" name="txt_observacion_can" id="txt_observacion_can" rows="3" placeholder="Ingrese una breve motivo de la cancelación"></textarea>
          </div><!--/modal-body-->
          <div class="modal-footer" style="border-top:0px;">
            <span class="btn btn-success btn-sm" onclick="cancel()"><i class="glyphicon glyphicon-ok"></i></span>
            <span class="btn btn-warning btn-sm" class="close" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></span>       
          </div>          
        </form>
    </div><!--/modal-content -->

  </div>
</div><!--/modal-->
