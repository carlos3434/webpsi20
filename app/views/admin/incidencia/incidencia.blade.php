<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/box-slider/jquery.bxslider.min.css') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}
{{ HTML::style('lib/fancybox-master/dist/jquery.fancybox.min.css') }}
<link rel="stylesheet" type="text/css" href="css/admin/incidencia.css?v={{ Cache::get('js_version_number') }}">
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      INCIDENCIAS 
      <small> </small> <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#incidencia">Nuevo Registro <i class="glyphicon glyphicon-plus"></i></span>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
      <li><a href="#">Incidencias</a></li>
      <li class="active">REgistro</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-11">
        <!-- Inicia contenido -->
        <div class="panel-group" id="accordion-filtros">
          <form name="form_movimiento" id="form_movimiento" method="post" action="" enctype="multipart/form-data"> 
            <div class="panel panel-default">
              <div class="panel-heading box box-primary">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse2" id="panel_individual">Búsqueda Individual</a>
                </h4>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="personalizado">
                    <div class="box-body">
                      <div class="row">
                        <div class="form-group">
                          <div class="col-sm-1">
                            <label>N° Registro: </label>
                          </div>
                          <div class="col-sm-2">
                            <input type="text" class="form-control pull-right" name="txt_valor" id="txt_valor"/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading box box-primary">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros" href="#collapseTwo">Filtros
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                  <input type="hidden" class="form-control pull-right" name="txt_tipo_busqueda" id="txt_tipo_busqueda" value="2"/>
                  <div class="row form-group">
                    <div class="col-sm-12">
                      <div class="col-sm-3">
                        <label class="titulo">Actividad:</label>
                        <select class="form-control" name="slct_tipotrabajo_buscar[]" id="slct_tipotrabajo_buscar" multiple>
                          <option value="1">Averia </option>
                          <option value="2">Provision</option>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <label>Sistema:</label>
                        <select class="form-control" name="slct_tecnologia_buscar[]" id="slct_tecnologia_buscar" multiple>
                          <option value="1">PSI </option>
                          <option value="3">CMS</option>
                          <option value="4">Gestel</option>
                          <option value="5">TOA MOVIL</option>
                          <option value="6">TOA MANAGE</option>
                          <option value="7">CMS-PSI</option>
                          <option value="2">PSI-TOA</option>
                          <option value="8">CMS-PSI-TOA</option>
                        </select>
                      </div>
                      <div class="col-sm-2">
                        <label>Tipo:</label>
                        <select class="form-control" name="slct_tipo_buscar[]" id="slct_tipo_buscar" multiple>
                          <option value="1">REQUERIMIENTO</option>
                          <option value="2">INCIDENCIA</option>
                          <option value="3">OM</option>
                          <option value="4">PROYECTO</option>
                        </select>
                      </div>
                      <div class="col-sm-2">
                        <label>Empresa:</label>
                        <select class="form-control" name="slct_empresa_buscar[]" id="slct_empresa_buscar" multiple></select> 
                      </div>

                      <div class="col-sm-2">
                        <label>Origen:</label>
                        <select class="form-control" name="slct_tipo_legado_buscar[]" id="slct_tipo_legado_buscar" multiple>
                          <option value="1">CMS </option>
                          <option value="2">GESTEL</option>
                        </select>
                      </div>

                      <div class="col-sm-2">
                        <label>Nivel de Impacto: </label>
                        <select class="form-control" name="slct_nivel_impacto_buscar[]" id="slct_nivel_impacto_buscar" multiple>
                          <option value="1">ALTO</option>
                          <option value="2">MEDIO</option>
                          <option value="3">BAJO</option>
                        </select>
                      </div>

                      <div class="col-sm-3">
                        <label>E.Validacion_usuario:</label>
                        <select class="form-control" name="slct_estado_validacion_buscar[]" id="slct_estado_validacion_buscar" multiple>
                          <!--<option value="4">REGISTRADO</option>-->
                          <option value="1">PENDIENTE  </option>
                          <option value="2">POR VALIDAR </option>
                          <!--<option value="6">DOCUMENTACIÓN</option>-->
                          <option value="7">SUSPENDIDO</option>
                          <option value="3">TERMINADO</option>
                        </select>
                      </div>

                      <div class="col-sm-3">
                        <label>Prioridad:</label>
                        <select class="form-control" name="slct_prioridad_buscar[]" id="slct_prioridad_buscar" multiple>
                          <option value="1">ALTA</option>
                          <option value="2">MEDIA</option>
                          <option value="3">BAJA</option>
                        </select>
                      </div>

                      <div class="col-sm-2">
                        <label>E.Seguimiento:</label>
                        <select class="form-control" name="slct_eactual_buscar[]" id="slct_eactual_buscar" multiple>
                          <option value="4">REGISTRADO</option>
                          <option value="1">PENDIENTE  </option>
                          <option value="2">POR VALIDAR </option>
                          <option value="6">DOCUMENTACIÓN</option>
                          <option value="3">TERMINADO</option>
                          <option value="5">EN DESARROLLO</option>
                        </select>
                      </div>

                      <div class="col-sm-3">
                        <label>Tipificacion:</label>
                        <select class="form-control" name="slct_tipificacion_buscar[]" id="slct_tipificacion_buscar" multiple>
                        </select>
                      </div>

                      <div class="col-sm-3">
                        <label>Usuario Responsable</label>
                        <select class="form-control" name="slct_usuarioresp_buscar[]" id="slct_usuarioresp_buscar" multiple>
                        </select>
                      </div>

                      <div class="col-sm-2">
                        <label class="titulo">Rango F. Registro:</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="txt_fecha_registro" id="txt_fecha_registro"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-xs-1  botones">
        <i class="fa fa-search fa-md btn btn-primary btn-md" title="Buscar" id="btn_general"></i>
        <a onclick="DescargarExcel();" class="btn btn-success btn-md"><i class="fa fa-download fa-md"></i></a> 
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box-body table-responsive">
          <table id="tb_incidencia" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 20%">N°Reg</th>
                <th style="width: 20%">Asociado</th>
                <th style="width: 10%">Tipo</th>
                <th style="width: 10%">Actividad</th>
                <th style="width: 10%">Empresa</th>
                <th style="width: 10%">Responsable</th>
                <th style="width: 10%">Impacto</th>
                <th style="width: 10%">Prioridad</th>
                <th style="width: 20%">Detalle</th>
                <th style="width: 20%">Observacion</th>
                <th style="width: 20%">F.Registro</th>
                <th style="width: 10%">E.Validacion_usuario</th>
                <th style="width: 10%">E.Seguimiento</th>
                <th style="width: 5%">[]</th>
              </tr>
            </thead>
            <tbody id="tbody_incidencia">

            </tbody>
          </table>
        </div>
      </div>
    </div>

  </section>
<!-- /.content -->
@endsection

@section('formulario')
@include( 'admin.incidencia.form.newincidencia' )
@include( 'admin.incidencia.form.incidencia_gestion' )
@endsection

@push('scripts')
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}
{{ HTML::script('lib/bootstrap-datepicker.js') }}
{{ HTML::script('lib/fancybox-master/dist/jquery.fancybox.min.js') }}
{{ HTML::script('lib/box-slider/jquery.bxslider.min.js') }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}

<script type="text/javascript">
    var area_id = "<?php echo Auth::user()->area_id; ?>";
    console.log(area_id);
    var perfilId = "{{Session::get('perfilId')}}";
</script>

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

<script type="text/javascript" src="js/admin/incidencia/incidencia_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/incidencia/incidencia.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush