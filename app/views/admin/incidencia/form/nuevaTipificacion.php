<div id="nuevaTipificacion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
        <form class="frm_tipificacion" name="frm_tipificacion" id="frm_tipificacion">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title pull-left">Tipificacion</h4>
      </div>
      <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nombre:</label>
                                    <input type="text" name="txt_nombre" id="txt_nombre" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <label>Estado:</label>
                                    <select class="form-control" id="slct_estado" name="slct_estado"> 
                                    <option value="">.::Seleccionar::.</option>
                                    <option value="1" selected>ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                    </select>
                                </div>
                            </div>                                                                             
      </div><!--/modal-body-->

      <div class="modal-footer" style="border-top:0px;margin-top: 10px">
        <!--<input type="submit" name="btn_guardar" id="btn_guardar" class="btn btn-primary btn-md" value="Guardar"/>-->
        <span class="btn btn-primary btn-md btn_guardar" id="btn_guardar" onclick="guardar()">Guardar <i class="glyphicon glyphicon-upload"></i></span>
      </div>
    </div><!--/modal-content -->
  </form>

  </div>
</div><!--/modal-->
<!-- Modal -->


