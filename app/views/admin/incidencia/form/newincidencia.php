<div id="incidencia" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <form class="frmIncidencia" name="frmIncidencia" id="frmIncidencia" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 0px">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title pull-left">Nuevo Registro</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col-sm-6">
                                <label>Num Requerimiento</label>
                                <input type="text" name="txt_num_requerimiento" id="txt_num_requerimiento" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Actividad:</label>
                                <select class="form-control" name="slct_tipotrabajo" id="slct_tipotrabajo">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">Averia </option>
                                    <option value="2">Provision</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Tipo:</label>
                                <select class="form-control" name="slct_tipo" id="slct_tipo">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">REQUERIMIENTO</option>
                                    <option value="2">INCIDENCIA</option>
                                    <option value="3">OM</option>
                                    <option value="4">PROYECTO</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Sistema:</label>
                                <select class="form-control" name="slct_tecnologia" id="slct_tecnologia">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">PSI </option>
                                    <option value="3">CMS</option>
                                    <option value="4">Gestel</option>
                                    <option value="5">TOA MOVIL</option>
                                    <option value="6">TOA MANAGE</option>
                                    <option value="7">CMS-PSI</option>
                                    <option value="2">PSI-TOA</option>
                                    <option value="8">CMS-PSI-TOA</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Empresa:</label>
                                <select class="form-control" name="slct_empresa" id="slct_empresa"></select>
                            </div>
                            <div class="col-md-6">
                                <label>Origen:</label>
                                <select class="form-control" name="slct_tipo_legado" id="slct_tipo_legado">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">CMS </option>
                                    <option value="2">GESTEL</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nivel Impacto:</label>
                                <select class="form-control" name="slct_nivel_impacto" id="slct_nivel_impacto">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">ALTO</option>
                                    <option value="2">MEDIO</option>
                                    <option value="3">BAJO</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>E.Validacion_usuario:</label>
                                <select class="form-control" name="slct_estado_validacion" id="slct_estado_validacion" disabled>
                                    <option value="">.::Seleccione::.</option>
                                    <option value="4">REGISTRADO</option>
                                    <option value="1">PENDIENTE  </option>
                                    <option value="2">POR VALIDAR </option>
                                    <option value="6">DOCUMENTACIÓN</option>
                                    <option value="3">TERMINADO</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Prioridad:</label>
                                <select class="form-control" name="slct_prioridad" id="slct_prioridad">
                                    <option value="">.::Seleccione::.</option>
                                    <option value="1">ALTA</option>
                                    <option value="2">MEDIA</option>
                                    <option value="3">BAJA</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>E.Seguimiento:</label>
                                <select class="form-control" name="slct_eactual" id="slct_eactual" disabled>
                                    <option value="">.::Seleccione::.</option>
                                    <option value="4">REGISTRADO</option>
                                    <option value="1">PENDIENTE  </option>
                                    <option value="2">POR VALIDAR </option>
                                    <option value="6">DOCUMENTACIÓN</option>
                                    <option value="3">TERMINADO</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Tipificacion: </label>
                                <select class="form-control" name="slct_tipificacion" id="slct_tipificacion"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <fieldset class="yellow-fieldset">
                                <legend>Imagenes</legend>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="1" name="txt_filep" id="txt_filep" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep1" id="txt_imagep1">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="2" name="txt_filep2" id="txt_filep2" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep2"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep2" id="txt_imagep2">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="3" name="txt_filep3" id="txt_filep3" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep3"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep3" id="txt_imagep3">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="4" name="txt_filep4" id="txt_filep4" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep4"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep4" id="txt_imagep4">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="5" name="txt_filep5" id="txt_filep5" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep5"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep5" id="txt_imagep5">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="6" name="txt_filep6" id="txt_filep6" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep6"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep6" id="txt_imagep6">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="7" name="txt_filep7" id="txt_filep7" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep7"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep7" id="txt_imagep7">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <input type="file" class="fileP" numero="8" name="txt_filep8" id="txt_filep8" style="display:none" accept="image/*">
                                        <span class="btn btn-primary btn-md openFile" target_f="txt_filep8"><i class="glyphicon glyphicon-plus"></i></span>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="txt_imagep8" id="txt_imagep8">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-md-6">
                            <label>Observacion:</label>
                            <textarea class="form-control" name="txt_obervacion" id="txt_obervacion" rows="4"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Detalle:</label>
                            <textarea class="form-control" name="txt_detalle" id="txt_detalle" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-top: 10px">
                        <fieldset>
                            <legend>Incidencia</legend>
                            <div class="box-body table-responsive">
                                <table id="t_req" class="table table-bordered table-striped t_req" validate="1">
                                    <thead>
                                        <tr>
                                            <th>Tipo Valor</th>
                                            <th>Valor</th>
                                            <th>Detalle</th>
                                            <th>Image</th>
                                            <th>Estado</th>
                                            <th>[]</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_req">
                                    </tbody>
                                    <tfoot>
                                        <tr class="trNuevo hidden">
                                            <td id="td_tipovalor" style="vertical-align : middle;">
                                                <select class="form-control slct_tipovalor" name="slct_tipovalor" id="slct_tipovalor">
                                                    <option value="">.::Seleccione::.</option>
                                                    <option value="1">REQUERIMIENTO</option>
                                                    <option value="2">PETICION</option>
                                                    <option value="3">SOLICITUD TECNICA</option>
                                                    <option value="4">ORDEN TRABAJO</option>
                                                    <option value="5">COD CLIENTE</option>
                                                </select>
                                            </td>
                                            <td id="td_valoi" style="vertical-align : middle;">
                                                <input type='text' name='txt_valor' id='txt_valor' class='form-control datepicker txt_valor'/>
                                            </td>
                                            <td id="td_detalle" style="vertical-align : middle;">
                                                <textarea class="form-control txt_observacion" rows="2" id="txt_observacion" name="txt_observacion"></textarea>
                                            </td>
                                            <td id="td_image" style="vertical-align : middle;">
                                                <div class="row form-group" style="margin-top: 10px">
                                                    <div class="col-md-2">
                                                        <input type="file" class="file" numero="1" name="txt_file" id="txt_file" style="display:none" accept="image/*">
                                                        <span class="btn btn-primary btn-md openFile" target_f="txt_file1"><i class="glyphicon glyphicon-plus"></i></span>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input class="form-control image" type="text" name="txt_image1" id="txt_image">
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="td_estado" style="vertical-align : middle;">
                                                <select class="form-control slct_estado_req" name="slct_estado_req" id="slct_estado_req" disabled>
                                                    <option value="">.::Seleccione::.</option>
                                                    <option value="1" selected>PENDIENTE</option>
                                                    <option value="2">POR VALIDAR</option>
                                                    <option value="6">DOCUMENTACIÓN</option>
                                                    <option value="3">TERMINADO</option>
                                                </select>     
                                            </td>
                                            <td id="tdDescripcion" style="vertical-align : middle;display: flex;padding-top: 18px;padding-bottom: 18px">
                                                <span id="btnDelete" name="btnDelete" class="btn btn-danger  btn-sm btnDelete" onclick="Deletetr(this)"><i class="glyphicon glyphicon-remove"></i></span>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <button id="btnAdd2" class="btn btn-yellow" style="width: 100%;margin-top:-20px" type="button"><span class="glyphicon glyphicon-plus"></span> AGREGAR </button>
                            </div>
                        </fieldset>
                    </div>
                </div><!--/modal-body-->

                <div class="modal-footer" style="border-top:0px;margin-top: 10px">
                    <input type="submit" name="btn_guardar" id="btn_guardar" class="btn btn-primary btn-md" value="Guardar"/>
                </div>
            </div><!--/modal-content -->
        </form>

    </div>
</div><!--/modal-->
<!-- Modal -->


<div id="detalleEvent" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title pull-left">Detalle Evento</h4>
            </div>
            <div class="modal-body" style="border-top:1px;margin-top: 10px">
                <input type="hidden" name="txt_idevento" id="txt_idevento">
                <section class="content">
                    <ul class="nav nav-pills  nav-justified">
                        <li class="active"><a data-toggle="pill" href="#averias">Averias Asociadas</a></li>
                        <li><a data-toggle="pill" href="#imagenes">Imagenes</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="averias" class="tab-pane fade in active">
                            <div class="box-body table-responsive">
                                <table id="tb_tickets" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Quiebre</th>
                                            <th style="width: 30%">Codcli</th>
                                            <th style="width: 10%">Cod Averia</th>
                                            <th style="width: 10%">Cliente</th>
                                            <th style="width: 10%">Estado</th>
                                            <th style="width: 10%">Telefono 1</th>
                                            <th style="width: 10%">Telefono 2</th>
                                            <th style="width: 10%">Telefono 3</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_tickets">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="imagenes" class="tab-pane fade">
                            <div class="box-body imgEventos">
                                <div style="width: 50%">
                                    <img src="" id="img1"/>
                                </div>
                                <div style="width: 50%">
                                    <img src="" id="img2"/>
                                </div>
                                <div style="width: 50%">
                                    <img src="" id="img3"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div><!--/modal-body-->

            <div class="modal-footer" style="border-top:0px;">
                <span class="btn btn-success btn-sm" onclick="exportCodigoCli()">Cod Cliente <i class="glyphicon glyphicon-download"></i></span>
                <span class="btn btn-success btn-sm" onclick="exportTelefono()">Telefonos <i class="glyphicon glyphicon-download"></i></span>
            </div>
        </div><!--/modal-content -->

    </div>
</div><!--/modal-->