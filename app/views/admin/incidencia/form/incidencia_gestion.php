<!-- Modal -->
<div id="incidenciaGestion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <input type="hidden" name="txt_id_modal" id="txt_id_modal">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs logo modal-header">
          <li class="logo tab_1 active" id="datos_incidencia">
            <a href="#tab_1" data-toggle="tab">
              <button class="btn btn-primary btn-sm"><i class="fa fa-file-o fa-sm"></i> </button>
              <small>DATOS DE INCIDENCIA</small>
            </a>
          </li>
          <li class="logo tab_2" id="tab_gestion_modal">
            <a href="#tab_2" data-toggle="tab">
              <button class="btn btn-primary btn-sm"><i class="fa fa-gears fa-sm"></i> </button>
              <small>REQUERIMIENTOS</small>
            </a>
          </li>
          <li class="logo tab_3" id="tab_datos_averia_modal" style="display:none;">
            <a href="#tab_3" data-toggle="tab">
              <button class="btn btn-primary btn-sm"><i class="fa fa-edit fa-sm"></i> </button>
              <small>DATOS DE AVERIA</small>
            </a>
          </li>
          <li class="logo tab_4">
            <a href="#tab_4" data-toggle="tab" id="bandejaModalMovimiento">
              <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-sm"></i> </button>
              <small>MOVIMIENTOS</small>
            </a>
          </li>
          <li class="logo tab_4" style="display:none;">
            <a href="#tab_5" data-toggle="tab">
              <button class="btn btn-primary btn-sm"><i class="fa fa-bars fa-sm"></i> </button>
              <small>REF: 101-OP</small>
            </a>
          </li>
          <li class="pull-right">
            <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
              <i class="fa fa-close"></i>
            </button>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab_2">
            <form id="form_requerimiento" name="form_bandeja" action="" method="post" style="overflow: auto;height:450px;max-height: :450px;" class="container-fluid">
              <input type="hidden" name="txt_incidencia_id" id="txt_incidencia_id">
              <div class="box-body table-responsive">
                <table id="t_req" class="table table-bordered table-striped t_req" validate="0">
                  <thead>
                    <tr>
                      <th>Tipo Valor</th>
                      <th>Valor</th>
                      <th>Detalle</th>
                      <th>Estado</th>
                      <th>Image</th>
                      <th>[]</th>
                    </tr>
                  </thead>
                  <tbody id="tb_req">
                  </tbody>
                  <tfoot>
                    <tr class="trNuevo hidden">
                      <td id="td_tipovalor" style="vertical-align : middle;">
                        <select class="form-control slct_tipovalor" name="slct_tipovalor" id="slct_tipovalor">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">REQUERIMIENTO</option>
                          <option value="2">PETICION</option>
                          <option value="3">SOLICITUD TECNICA</option>
                          <option value="4">ORDEN TRABAJO</option>
                          <option value="5">COD CLIENTE</option>
                        </select>     
                        <!---<input type='text' name='txt_tipovalor' id='txt_tipovalor' class='form-control datepicker txt_tipovalor'/>-->
                      </td>
                      <td id="td_valoi" style="vertical-align : middle;">
                        <input type='text' name='txt_valor' id='txt_valor' class='form-control datepicker txt_valor'/>
                      </td>
                      <td id="td_detalle" style="vertical-align : middle;">
                        <textarea class="form-control txt_observacion" rows="2" id="txt_observacion" name="txt_observacion"></textarea>
                      </td>
                      <td id="td_estado" style="vertical-align : middle;">
                        <select class="form-control slct_estado_req" name="slct_estado_req" id="slct_estado_req" disabled>
                          <option value="">.::Seleccione::.</option>
                          <option value="1" selected>PENDIENTE</option>
                          <option value="2">POR VALIDAR</option>
                          <option value="6">DOCUMENTACIÓN</option>
                          <option value="3">TERMINADO</option>
                        </select>     
                      </td>
                      <td id="td_img" style="vertical-align : middle;"><span class="btn btn-primary btn-md" style="opacity: 0.5"><i class="glyphicon glyphicon-eye-open"></i></span></td>
                      <td id="tdDescripcion" style="vertical-align : middle;display: flex;padding-top: 18px;padding-bottom: 18px">
                        <span id="btnSave" name="btnSave" class="btn btn-success btn-sm" style="margin-right: 5px;" onclick="saveReq(this)"><i class="glyphicon glyphicon-ok"></i></span>  
                        <span id="btnDelete" name="btnDelete" class="btn btn-danger  btn-sm btnDelete" onclick="Deletetr(this)"><i class="glyphicon glyphicon-remove"></i></span>
                      </td>
                    </tr>
                  </tfoot>
                </table>
                <button id="btnAdd" class="btn btn-yellow" style="width: 100%;margin-top:-20px" type="button"><span class="glyphicon glyphicon-plus"></span> AGREGAR </button>
              </div>
            </form>
            <div class="modal-footer">
              <div class="gallery">

              </div>
              <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
            </div>
          </div><!-- /.tab-pane -->
          <div class="tab-pane" id="tab_1">
            <form class="frmIncidenciaEdit" name="frmIncidenciaEdit" id="frmIncidenciaEdit" method="post" enctype="multipart/form-data">
              <input type="hidden" name="txt_id_modal2" id="txt_id_modal2">
              <div class="form-group" id="ficha_results">
                <fieldset>
                  <legend>Incidencia</legend>
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="col-sm-3">
                        <label class="text-primary">N° Registro</label>
                        <input type="text" class="form-control input-sm azul" id="incidencia_t" name="incidencia_t" readonly disabled>
                      </div>
                      <div class="col-sm-3">
                        <label>Tipo Actividad</label>
                        <select class="form-control" name="slct_tipotrabajo_modal" id="slct_tipotrabajo_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">Averia </option>
                          <option value="2">Provision</option>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <label>Sistema</label>
                        <select class="form-control" name="slct_tecnologia_modal" id="slct_tecnologia_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">PSI </option>
                          <option value="3">CMS</option>
                          <option value="4">Gestel</option>
                          <option value="5">TOA MOVIL</option>
                          <option value="6">TOA MANAGE</option>
                          <option value="7">CMS-PSI</option>
                          <option value="2">PSI-TOA</option>
                          <option value="8">CMS-PSI-TOA</option>
                        </select>
                      </div>   
                      <div class="col-sm-3">
                        <label>Origen</label>
                        <select class="form-control" name="slct_tipo_legado_modal" id="slct_tipo_legado_modal" disabled>
                          <option value="">.::Seleccione::.</option>
                          <option value="1">CMS </option>
                          <option value="2">GESTEL</option>
                        </select>     
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-3">
                        <label>Nivel Impacto</label>
                        <select class="form-control" name="slct_nivel_impacto_modal" id="slct_nivel_impacto_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">ALTO</option>
                          <option value="2">MEDIO</option>
                          <option value="3">BAJO</option>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <label>E.Validacion_usuario</label>
                        <select class="form-control" name="slct_estado_validacion_modal" id="slct_estado_validacion_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="4">REGISTRADO</option>
                          <option value="1">PENDIENTE</option>
                          <option value="6">DOCUMENTACIÓN</option>
                          <option value="2">POR VALIDAR</option>
                          <option value="3">TERMINADO</option>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <label>Prioridad</label>
                        <select class="form-control" name="slct_prioridad_modal" id="slct_prioridad_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">ALTA</option>
                          <option value="2">MEDIA</option>
                          <option value="3">BAJA</option>
                        </select>  
                      </div>
                      <div class="col-sm-3">
                        <label>E.Seguimiento</label>
                        <select class="form-control" name="slct_eactual_modal" id="slct_eactual_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="4">REGISTRADO</option>
                          <option value="1">PENDIENTE  </option>
                          <option value="2">POR VALIDAR </option>
                          <option value="6">DOCUMENTACIÓN</option>
                          <option value="3">TERMINADO</option>
                          <option value="5">EN DESARROLLO</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-3">
                        <label>Empresa</label>
                        <select class="form-control" name="slct_empresa_modal" id="slct_empresa_modal" disabled></select>    
                      </div>          
                      <div class="col-sm-3">
                        <label>Tipo</label>
                        <select class="form-control" name="slct_tipo_modal" id="slct_tipo_modal">
                          <option value="">.::Seleccione::.</option>
                          <option value="1">REQUERIMIENTO</option>
                          <option value="2">INCIDENCIA</option>
                          <option value="3">OM</option>
                          <option value="4">PROYECTO</option>
                        </select>    
                      </div>
                      <div class="col-sm-3">
                        <label>Fecha Entrega</label>
                        <input type="text" name="txt_fecha_entrega" id="txt_fecha_entrega" class="form-control">
                      </div>
                      <div class="col-sm-3">
                        <label>Num Requerimiento</label>
                        <input type="text" name="txt_num_requerimiento_modal" id="txt_num_requerimiento_modal" class="form-control">
                      </div>                                                               
                    </div>
                    <div class="row">
                      <div class="col-sm-2">
                        <label>R.Asociado</label>
                        <input type="text" name="txt_ticket_asociado" id="txt_ticket_asociado" class="form-control">
                      </div>
                      <div class="col-sm-5">
                        <label>Tipificacion</label>
                        <select class="form-control" name="slct_tipificacion_modal" id="slct_tipificacion_modal">
                        </select>     
                      </div>  
                      <div class="col-sm-3">
                        <label>Usuario Responsable</label>
                        <select class="form-control" name="slct_usuarioresp" id="slct_usuarioresp">
                        </select>     
                      </div>
                      <div class="col-sm-2">
                        <label>Remedi</label>
                        <input type="text" name="txt_remedi" id="txt_remedi" class="form-control">
                      </div>
                      <div class="row">
                        <div class="col-sm-6">
                          <label>Observacion</label>
                          <textarea class="form-control" name="txt_obervacion_modal" id="txt_obervacion_modal" rows="4"></textarea>
                        </div>
                        <div class="col-sm-6">
                          <label>Detalle</label>
                          <textarea class="form-control" name="txt_detalle_modal" id="txt_detalle_modal" rows="4"></textarea>
                        </div> 
                      </div>                                  
                    </div>

                    <div class="row" style="margin-top:10px">
                      <div class="col-md-3">
                        <div class="row form-group"> 
                          <div class="col-md-2">
                            <input type="file" class="fileEdit" numero="1" name="txt_file_edit1" id="txt_file_edit1" style="display:none" accept="image/*">
                            <span class="btn btn-primary btn-md openFile" target_f="txt_file_edit1"><i class="glyphicon glyphicon-plus"></i></span>
                          </div>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="txt_image_edit1" id="txt_image_edit1">
                          </div>
                        </div>                                
                      </div>
                      <div class="col-md-3">
                        <div class="row form-group"> 
                          <div class="col-md-2">
                            <input type="file" class="fileEdit" numero="2" name="txt_file_edit2" id="txt_file_edit2" style="display:none" accept="image/*">
                            <span class="btn btn-primary btn-md openFile" target_f="txt_file_edit2"><i class="glyphicon glyphicon-plus"></i></span>
                          </div>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="txt_image_edit2" id="txt_image_edit2">
                          </div>
                        </div>                                
                      </div>
                      <div class="col-md-3">
                        <div class="row form-group"> 
                          <div class="col-md-2">
                            <input type="file" class="fileEdit" numero="3" name="txt_file_edit3" id="txt_file_edit3" style="display:none" accept="image/*">
                            <span class="btn btn-primary btn-md openFile" target_f="txt_file_edit3"><i class="glyphicon glyphicon-plus"></i></span>
                          </div>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="txt_image_edit3" id="txt_image_edit3">
                          </div>
                        </div>                                
                      </div>
                      <div class="col-md-3">
                        <div class="row form-group"> 
                          <div class="col-md-2">
                            <input type="file" class="fileEdit" numero="4" name="txt_file_edit4" id="txt_file_edit4" style="display:none" accept="image/*">
                            <span class="btn btn-primary btn-md openFile" target_f="txt_file_edit4"><i class="glyphicon glyphicon-plus"></i></span>
                          </div>
                          <div class="col-md-10">
                            <input class="form-control" type="text" name="txt_image_edit4" id="txt_image_edit4">
                          </div>
                        </div>                                
                      </div>
                    </div>
                  </fieldset>
                  <!-- datos de cliente -->
                  <br>
                  <fieldset class="Imagenes">
                    <legend>Imagenes</legend>
                    <div class="slider col-sm-12">
                    </div>
                  </fieldset>
                </div><!-- ficha_results -->
                <div class="modal-footer">
                  <input type="submit" name="btn_guardar" id="btn_guardar" class="btn btn-primary btn-md" value="Guardar"/>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                </div>
              </form>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">
              <form id="averiaForm">
                <!-- PSI /Legados -->
                <div class="row form-group" id="averia_results">

                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <label>Plazo</label>
                      <input type="text" class="form-control input-sm" name="plazo"  id="plazo" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Estado Legado</label>
                      <input type="text" class="form-control input-sm" name="estado_legado" id="a_estado" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Fecha de Registro Legado</label>
                      <input type="text" class="form-control input-sm" name="fecha_registro_legado" id="a_fecha" readonly>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <label>Cod Avería</label>
                      <input type="text" class="form-control input-sm" name="codactu" id="a_codactu" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Cod Cli/N°Servicio</label>
                      <input type="text" class="form-control input-sm" name="codcli" id="a_telefono" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Cliente</label>
                      <input type="text" class="form-control input-sm" name="nombre_cliente" id="a_cliente" readonly>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <label>Contrata</label>
                      <input type="text" class="form-control input-sm" name="contrata" id="a_contrata" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Area</label>
                      <input type="text" class="form-control input-sm azul" name="area"  id="a_area" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Zonal</label>
                      <input type="text" class="form-control input-sm" name="zonal" id="a_zonal" readonly>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <label>Codigo Liquidacion</label>
                      <input type="text" class="form-control input-sm" name="cod_liquidacion" readonly>
                    </div>
                    <div class="col-sm-4">
                      <label>Detalle Liquidacion</label>
                      <input type="text" class="form-control input-sm" name="detalle_liquidacion" readonly>
                    </div>

                    <div class="col-sm-4">
                      <label>Fecha Liquidación</label>
                      <input type="text" class="form-control input-sm" name="fecha_liquidacion" readonly>
                    </div>
                  </div>

                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <label>MDF</label>
                      <input type="text" class="form-control input-sm" name="mdf" readonly>
                    </div>
                    <div class="col-sm-8">
                      <label>Observación</label>
                      <input type="text" class="form-control input-sm" name="observacion" readonly>
                    </div>
                  </div>

                </div><!--/PSI /Legados -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                </div>
              </form>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_4">
              <div class="row form-group" style="overflow: auto;height:500px;">
                <form name="form_Movimientos" id="form_Movimientos" method="POST" action="reporte/movimientosexcel">

                </form>
                <div class="col-sm-12">
                  <br/>
                  <table id="t_movimientos_inc" class="table table-bordered table-striped">
                    <thead class="btn-primary">
                      <tr>
                        <th>ID</th>
                        <th>E.Validacion</th>
                        <th>E.Registro</th>
                        <th>Sistema</th>
                        <th>Observación</th>
                        <th>Usuario</th>
                        <th>Fecha Mov</th>
                        <th>Fecha Entrega</th>
                        <th>Usuario Responsable</th>
                        <th>Inpacto</th>
                        <th>Asociado</th>
                      </tr>
                    </thead>
                    <tbody id="tb_movimientos_inc">
                    </tbody>
                    <tfoot class="btn-primary">
                      <tr>
                        <th>ID</th>
                        <th>E.Validacion</th>
                        <th>E.Registro</th>
                        <th>Sistema</th>
                        <th>Observación</th>
                        <th>Usuario</th>
                        <th>Fecha Mov</th>
                        <th>Fecha Entrega</th>
                        <th>Usuario Responsable</th>
                        <th>Inpacto</th>
                        <th>Asociado</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
              </div>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_5">
              <div class="row form-group" style="overflow: auto;height:500px;">
                <form id="mensajeForm">
                  <!-- Asunto -->
                  <div class="col-sm-1">
                    Asunto:
                  </div>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="asunto" readonly>
                  </div>
                  <br><br>
                  <div class="col-sm-12">
                    <p style="font-size: 20px;">Estimados,<br><br>
                      Agradeceré confirmar agenda y atención al presente requerimiento, ingresado con motivo de atención diferenciada <label id="quiebre_cat"></label>
                    </p>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
              </div>
            </div><!-- /.tab-pane -->
          </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
      </div><!--/modal-content -->
    </div>
</div><!--/modal-->