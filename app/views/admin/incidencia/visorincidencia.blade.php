<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/box-slider/jquery.bxslider.min.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
     {{ HTML::style('lib/datepicker.css') }}
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')


<style type="text/css">
 .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 8px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }
</style>
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       VISOR       
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Reporte</a></li>
                        <li class="active">Reporte de Movimiento</li>
                    </ol>
                </section>

                <!-- Main content -->
<section class="content">
                          <form class="form_filtros" id="form_filtros" name="form_filtros" method="post" action="">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <div class="panel-group" id="accordion-filtros">               
                <div class="panel panel-default">
                    <div class="panel-heading box box-primary">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-filtros"
                               href="#collapseTwo">Filtros
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                              <div class="row form-group">
                                <div class="col-sm-12">
                                     <div class="col-sm-2">
                                        <label>Tipo:</label>
                                        <select class="form-control" name="slct_tipo_buscar" id="slct_tipo_buscar" multiple>
                                            <option value="1">REQUERIMIENTO</option>
                                            <option value="2">INCIDENCIA</option>
                                            <option value="3">OM</option>
                                            <option value="4">PROYECTO</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Empresa:</label>
                                        <select class="form-control" name="slct_empresa_buscar" id="slct_empresa_buscar" multiple></select> 
                                    </div>

                                    <div class="col-sm-2">
                                        <label>Tipo Legado:</label>
                                        <select class="form-control" name="slct_tipo_legado_buscar" id="slct_tipo_legado_buscar" multiple>
                                            <option value="1">CMS </option>
                                            <option value="2">GESTEL</option>
                                        </select>   
                                    </div>

                                    <div class="col-sm-2">
                                     <label>Nivel de Impacto: </label>
                                         <select class="form-control" name="slct_nivel_impacto_buscar" id="slct_nivel_impacto_buscar" multiple>
                                            <option value="1">ALTO</option>
                                            <option value="2">MEDIO</option>
                                            <option value="3">BAJO</option>
                                        </select>          
                                    </div>

                                    <div class="col-sm-3">
                                         <label>E. Validacion:</label>
                                       <select class="form-control" name="slct_estado_validacion_buscar" id="slct_estado_validacion_buscar" multiple>
                                            <option value="4">REGISTRADO</option>
                                            <option value="1">PENDIENTE  </option>
                                            <option value="2">POR VALIDAR </option>
                                            <option value="3">TERMINADO</option>                                            
                                       </select>     
                                    </div>

                                    <div class="col-sm-3">
                                      <label>Prioridad:</label>
                                       <select class="form-control" name="slct_prioridad_buscar" id="slct_prioridad_buscar" multiple>
                                            <option value="1">ALTA</option>
                                            <option value="2">MEDIA</option>
                                            <option value="3">BAJA</option>
                                       </select>     
                                    </div>

                                    <div class="col-sm-2">
                                      <label>Estado Actual:</label>
                                       <select class="form-control" name="slct_eactual_buscar" id="slct_eactual_buscar" multiple>
                                            <option value="4">REGISTRADO</option>
                                            <option value="1">PENDIENTE  </option>
                                            <option value="2">POR VALIDAR </option>
                                            <option value="3">TERMINADO</option>
                                       </select>     
                                    </div>    

                                    <div class="col-sm-3">
                                      <label>Tipificacion:</label>
                                       <select class="form-control" name="slct_tipificacion_buscar" id="slct_tipificacion_buscar" multiple>
                                       </select>     
                                    </div>  

                                    <div class="col-sm-3">
                                        <label>Usuario Responsable</label>
                                        <select class="form-control" name="slct_usuarioresp_buscar" id="slct_usuarioresp_buscar" multiple>
                                        </select>     
                                    </div>   

                                    <div class="col-sm-2">
                                        <label class="titulo">Rango F. Registro:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="txt_fecha_registro"
                                                   id="txt_fecha_registro"/>
                                        </div>
                                    </div>                               
                                </div>
                                <div class="col-md-12">
                                    <div class="col-sm-2" style="float: right"><br>
                                      <div class="col-sm-6">
                                        <button id="btn_general" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar</button>
                                      </div>
                                    </div>                                  
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                          </form>


                    <div class="col-sm-12">
                        <div class="box-body table-responsive">
                        <table id="t_visor" class="table table-bordered table-hover col-sm-12 responsive text-center" width="100%">
                            <thead>
                                <tr>
                                  <th colspan="2"></th>
                                  <th colspan="4">INCIDENCIAS</th>
                                  <th colspan="4">O.MEJORA</th>
                                  <th></th>
                                </tr>
                                <tr>
                                  <th style="width: 12%;">Actividad</th>
                                  <th style="width: 15%;">Sistema</th>
                                  <th style="width: 8%;">Registrado</th>
                                  <th style="width: 8%;">Pendiente</th>
                                  <th style="width: 8%;">Por Validar</th>
                                  <th style="width: 8%;">Terminado</th>
                                  <th style="width: 8%;">Registrado</th>
                                  <th style="width: 8%;">Pendiente</th>
                                  <th style="width: 8%;">Por Validar</th>
                                  <th style="width: 8%;">Terminado</th>
                                  <th style="width: 8%;background-color: #F5F6CE;">Total</th>
                                </tr>
                              </thead>
                              <tbody id="tb_visoroperaciones">
                              </tbody>
                          </table>
                        </div>
                   </div>

                   <div class="col-sm-12">
                        <div class="box-body table-responsive">
                        <table id="t_responsable" class="table table-bordered table-hover col-sm-12 responsive text-center" width="100%">
                            <thead>
                              </thead>
                              <tbody id="tb_responsable">
                              </tbody>
                          </table>
                        </div>
                   </div>

</section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.incidencia.form.newincidencia' )
     @include( 'admin.incidencia.form.incidencia_gestion' )
@stop

@push('scripts')
   {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
   {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
   {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('lib/bootstrap-datepicker.js') }}

<!-- bxSlider Javascript file -->
   {{ HTML::script('lib/box-slider/jquery.bxslider.min.js') }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/incidencia/visorincidencia_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/incidencia/visorincidencia.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')