<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('js/sweetalert/sweetalert.js') }}
{{ HTML::style('css/sweetalert/sweetalert.css') }}
{{ HTML::script('https://maps.google.com/maps/api/js?libraries=places,drawing&key='.Config::get('wpsi.map.key').'') }}

{{ HTML::script('js/utils.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.georeferencia.js.proyectos_ajax' )

{{ HTML::script('js/georeferencia/buscardirecciones.js') }}
{{ HTML::script('js/georeferencia/buscardirecciones_ajax.js') }}

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style>
    .left {float: left;}
    .right {float: right;}

    .clearer {
        clear: both;
        display: block;
        font-size: 0;
        height: 0;
        line-height: 0;
    }

    /*
            Misc
    ------------------------------------------------------------------- */

    .hidden {display: none;}


    /*
            Example specifics
    ------------------------------------------------------------------- */

    /* Layout */

    #center-wrapper {
        margin: 0 auto;
        width: 920px;
    }


    /* Content & sidebar */

    #mymap{
        text-align: center;
        height: 600px;
    }
    #sidebar {
        text-align: center;
        min-height: 500px;
        max-height: 700px;
    }

    #sidebar {
        text-align: left;
        display: none;
        overflow: auto;
    }
    #mymap {
        background-color: #EFE;
        border-color: #CDC;
        width: 98%;
    }
    .content {
        background-color: white;
    }

    .use-sidebar #mymap {width: 54%;}
    .use-sidebar #sidebar {
        display: block;
        width: 45%;
    }
    @media screen and (max-width: 700px) {
      .use-sidebar #mymap {width: 100%;}
      .use-sidebar #sidebar {
        width: 100%;
      }
    }

    .sidebar-at-left #sidebar {margin-right: 1%;}
    .sidebar-at-right #sidebar {margin-left: 1%;}

    .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
    .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

    /*#separator {
        background-color: #EEE;
        border: 1px solid #CCC;
        display: block;
        outline: none;
        width: 1%;
    }
    .use-sidebar #separator {
        background: url('img/separator/vertical-separator.gif') repeat-y center top;
        border-color: #FFF;
    }
    #separator:hover {
        border-color: #ABC;
        background: #DEF;
    }*/

    fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    box-shadow: 0 0 0 0 #000;
    margin: 0 0 1.5em !important;
    padding: 0 1.4em 1.4em !important;
  }
    .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 250px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }
    
</style>

<div class="box box-default">
    <div class="box-header with-border">
        <div class="box-title"><h1>Buscar Direcciones</h1></div>
        <div class="box-tools pull-right">
           
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
<!-- </div> /.box -->

<!-- Main content -->
<section class="content box-body">              
    <div class="row">
      <div class="col-md-12" id=''>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
              <a id='divaccion' href="#elementcontent" data-parent="#divelemento" data-toggle="collapse" class="accordion-toggle">Ver en Mapa</a>
            </h3>
          </div>
          <div class="box-body collapse in" id="elementcontent" aria-expanded="true">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">  
                  <div class="col-md-6">
                  <fieldset class="scheduler-border" >
                    <h5></h5>
                    <div class="col-md-8">
                      <label class="control-label">Adjuntar Archivo CSV</label>
                      <input id="txt_file_plan" type="file" name="txt_file_plan">
                    </div>
                    <div class="col-md-2 col-xs-12">
                        <button class="btn btn-primary" style="margin-top:25px" onclick="CargarArchivos()" type="button">Cargar Archivo</button>
                    </div>
                  </fieldset>
                  </div>
                  <div class="col-md-6">
                  <fieldset class="scheduler-border">
                    <h5></h5> 
                    <div class="col-md-4">
                        <label>Proyectos</label>
                        <select id="slct_proyectos" class="form-control" name="slct_proyectos" onchange="combocapas()">
                          <option value="">.::Seleccione::.</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Capas</label>
                        <select id="slct_capas" class="form-control" name="slct_capas[]" multiple="">
                          <option value="">.::Seleccione::.</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>&nbsp;</label>
                        <a style="margin:25px 0 0;" id="btn_trazarproy" class="btn btn-success"><i class="fa fa-object-ungroup fa-lg"></i> Trazar Proyecto</a>
                    </div>
                  </fieldset>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                </div>
              </div>
            </div>
          </div>                         
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="use-sidebar sidebar-at-left" id="main">
          <input id="pac-input" class="controls" type="text" style="width:250px">
          <div id="sidebar">
            <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">
                            <a href="#listadocontent" data-parent="#divlistado" data-toggle="collapse" >Resultados</a>
                          </h3>
                        </div>
                        <div class="box-body collapse in" id="listadocontent" aria-expanded="true">
                          <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4">
                              <a style="margin:0 0 10px;" onclick="LimpiarMapa()" class="btn btn-danger" ><i class="fa fa-refresh fa-lg"></i> Limpiar Mapa</a>
                            </div>
                            <div class="col-md-12">
                                <div>
                                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#t_direccion" id="tabdireccion">Direcciones</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#t_registro">Registros</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="t_direccion">
                                        <div class="box" >
                                        <h5></h5>
                                          <div class="col-md-12">
                                            <div class="col-md-8">
                                                <label>Nombre Grupo</label>
                                                <input id="txt_nombre" class="form-control" type="text" name="txt_nombre" >
                                            </div>
                                            <div class="col-md-4">
                                              <a style="margin:25px 0 10px;" id="btnguardar" onclick="guardargrupo()" class="btn btn-success" >Guardar Grupo</a>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="col-md-8">
                                                <label>Filtrar Resultados</label>
                                                    <select class="form-control" id="slct_filtro" onchange="filtraraddress(this.value)">
                                                    <option value="all" selected="">.:: Todos ::.</option>
                                                    <option value="0">Sin Resultados</option>
                                                    <option value="1">1 Resultado</option>
                                                    <option value="2">2 Resultados</option>
                                                    <option value="3">3 Resultados</option>

                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                              <a style="margin:25px 0 10px;" id="btncancelar" onclick="Cancelar()" class="btn btn-warning" disabled="">Cancelar Cambios</a>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="col-md-8 alert-danger" id="div_resultados">
                                                
                                            </div>
                                          </div>
                                            <div class="box-body table-responsive" >
                                                <table id="t_direcciones" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Direcciones</th>
                                                            <th>Datos</th>
                                                            <!--<th>Ver</th>-->
                                                            <th>Resultados</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb_direcciones">
                                                    </tbody>
                                                </table>
                                            </div><!-- /.box-body -->
                                        </div><!-- /.box -->
                                    </div>
                                    <div class="tab-pane fade in" id="t_registro" >
                                        <div class="box">
                                            <div class="box-body table-responsive">
                                                <table id="t_grupo" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Grupo Dirección</th>
                                                            <th>Eliminar</th>
                                                            <th>Ver</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb_grupo">
                                                    </tbody>
                                                </table>
                                            </div><!-- /.box-body -->
                                        </div>
                                    </div>
                                  </div>
                            </div>
                          </div>
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                  </div>
                </div>
                <div class="row">
                    
                </div>
            </div><!-- /.box-body -->
          </div>
          <div id="mymap">Content</div>
          <a href="#" id="separator"></a>
          <div class="clearer">&nbsp;</div>
        </div>
      </div>
    </div>
</section>
</div>
@stop
@section('formulario')
@stop
