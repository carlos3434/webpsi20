<!-- /.modal -->
<div class="modal fade" id="proyectocapaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">New message</h4>
            </div>
            <div class="modal-body">
                <form id="form_proyectocapa" name="form_proyectocapa" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Nombre</label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre" >
                        </div>
                        <div class="col-sm-12">
                            <label class="control-label">Elementos</label>
                            <div class="col-sm-12 table-responsive" style="height: 200px" id="capaelemento">
                        </div>
                    </div>
                    <div class="row form-group" style=" display: none">
                        <div class="row estiloItem" id="estilo_poligono_edit" > 
                            <div class="col-sm-12">
                            <div class="col-sm-12 callout callout-warning">
                                <h4>Poligono</h4>
                                <div class="row">
                                <div class="col-xs-8 col-sm-6">
                                    <p><label>Color fondo</label> <input type="text" style="cursor: pointer" title="polyDemoCreate_edit"
                                    name="pickerPolyBack_edit"  id="pickerPolyBack_edit" class="color-picker" value="F7584C" /></p>
                                    <p><label>Color linea &nbsp; </label><input type="text" style="cursor: pointer" title="polyDemoCreate_edit" 
                                        name="pickerPolyLine_edit"  id="pickerPolyLine_edit" class="color-picker" value="F7584C" /></p>
                                    <p>
                                        <label for="polyOpacCreate">Opacidad</label>
                                      <div id="sliderPolyOpac_edit" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner- all" 
                                           name="sliderPolyOpac_edit" style="width: 200px" title="polyDemoCreate_edit">
                                        <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                                        </div>
                                    </p>
                                    <p>
                                        <label>Linea </label>
                                        <select name="polyLineCreate_edit" id="polyLineCreate_edit" >
                                            <?php
                                            for ($i = 0; $i <= 10; $i++) {
                                                echo "<option value=\"$i\">$i</option>";
                                            }
                                            ?>
                                        </select>
                                        <div></div>
                                    </p>  
                                </div>
                                <div class="col-xs-4 col-sm-6">
                                    <div id="polyDemoCreateMapx_edit" class="demoPolygon" style="float: right; position: relative; border-color: rgb(16, 107, 56); border-style: solid; border-width: 0px; ">
                                     <img id="polyDemoCreateMap_edit" class="demoPolygon" style="; z-index: 0; position: absolute" src="lib/geo_css/images/geoPolyImageConstruct_80x80.png">
                                     <p id="polyDemoCreate_edit" name="polyDemoCreate_edit" class="demoPolygon" style="background-color: #F7584C; z-index: 1; opacity: 0; position: absolute; "></p>
                                     <p id="polyDemoCreateborder_edit" class="demoPolygon" style="z-index: 2; border-style: solid; border-color: #F7584C; position: relative; border-width: 0px"></p>
                                     </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row estiloItem" id="estilo_punto_edit" style=" display: none"> 
                            <div class="col-sm-12">
                            <div class="col-sm-12 callout callout-warning">
                                <h4>Punto</h4>
                                <div class="row">
                                <div class="col-xs-8 col-sm-6">
                                   <!-- <p><input type="radio" value="default" name="iconSelect" >Por defecto</p> -->
                                    <p><label>Color fondo</label>
                                        <input id="pickerMarkBack_edit" style="cursor: pointer" class="color-picker" type="text" value="F7584C">
                                    </p>  
                                </div>
                                <div class="col-xs-4 col-sm-6">
                                   <div class="newMarkerBackground">
                                       <img src="lib/geo_css/images/transparent_marker.png" style="z-index: 1; position: absolute" />
                                   </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_close_modal2" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->