<!-- /.modal -->
<div class="modal fade" id="geoproyectozonaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">New message</h4>
            </div>
            <div class="modal-body">
                <form id="form_proyectozona" name="form_proyectozona" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label class="control-label">Nombre</label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_detalle_modal" id="txt_detalle_modal" >
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Tipo Zona</label>
                            <select class="form-control"  id="slct_tipo_geozona_id_modal" name="slct_tipo_geozona_id_modal" >
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Actividad</label>
                            <select id="slct_actividad_id_modal" class="form-control" name="slct_actividad_id_modal[]" multiple="">
                                    <option value="1">Avería</option>
                                    <option value="2">Provisión</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Quiebre</label>
                            <select class="form-control"  id="slct_quiebre_id_modal" name="slct_quiebre_id_modal[]" multiple="">
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Actividad Tipo</label>
                            <select class="form-control"  id="slct_actividad_tipo_id_modal" name="slct_actividad_tipo_id_modal[]" multiple="">
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_close_modal2" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->