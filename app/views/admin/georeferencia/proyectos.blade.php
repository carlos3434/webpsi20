<!DOCTYPE html>

@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}

{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
<!--{{ HTML::script('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}-->

<!--{{ HTML::style('lib/geo_css/jquery.multiselect.css') }}
{{ HTML::style('lib/geo_css/jquery.multiselect.filter.css') }} 
{{ HTML::style('lib/geo_css/magnific-popup.css') }}
{{ HTML::style('lib/geo_css/easy-responsive-tabs.css') }}
{{ HTML::script('lib/geo_js/jquery.magnific-popup.min.js') }}
{{ HTML::script('lib/geo_js/easyResponsiveTabs.js') }} -->
{{ HTML::style('lib/geo_css/geo.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.css') }}
{{ HTML::style('lib/geo_css/flexigrid.pack.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.theme.css') }} 
{{ HTML::style('lib/geo_css/colpick.css') }} 
{{ HTML::script('http://maps.google.com/maps/api/js?sensor=false&libraries=drawing') }}

{{ HTML::script('lib/geo_js/colpick.js') }} 
{{ HTML::script('lib/geo_js/jquery.simple-color.min.js') }}
{{ HTML::script('lib/geo_js/json2.js') }}
{{ HTML::script('lib/geo_js/jquery.md5.js') }} 
{{ HTML::script('lib/geo_js/flexigrid.pack.js') }}

{{ HTML::style('lib/geo_css/colpick.css') }}  

{{ HTML::script('js/psigeo.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/geo/geo.functions.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.georeferencia.js.proyectos' )
@include( 'admin.georeferencia.js.proyectos_ajax' )
@include( 'admin.js.kml' )


@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<div class="box box-default">
    <div class="box-header with-border">
        <div class="box-title">Proyectos</div>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <form name="form_proyectos" id="form_proyectos" >
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                              <div class="col-sm-12">
                                  <div>
                                      <ul id="myTab" class="nav nav-tabs" role="tablist">
                                          <li role="presentation" class="active">
                                              <a href="#t_proyectos">Proyectos</a>
                                          </li>
                                          <li role="presentation">
                                              <a href="#t_capas" id='a_capa'>Capas</a>
                                          </li>
                                      </ul>
                                  </div>
                                  <div class="tab-content">
                                      <div class="tab-pane fade active in" id="t_proyectos">
                                          <div class="box" >
                                              <div class="box-body table-responsive" >
                                                  <table id="t_proyecto" class="table table-bordered table-striped">
                                                      <thead>
                                                          <tr>
                                                              <th>Id</th>
                                                              <th>Proyecto</th>
                                                              <th>Fecha</th>
                                                              <th>Estado</th>
                                                              <th>Público</th>
                                                              <th class="editarG"> [] </th>
                                                          </tr>
                                                      </thead>
                                                      <tbody id="tb_proyecto">
                                                          
                                                      </tbody>
                                                  </table>
                                              </div><!-- /.box-body -->
                                          </div><!-- /.box -->
                                      </div>
                                      <div class="tab-pane fade in" id="t_capas" >
                                          <div class="box">
                                              <div class="box-body table-responsive">
                                                  <table id="t_capa" class="table table-bordered table-striped">
                                                      <thead>
                                                          <tr>
                                                              <th>Id</th>
                                                              <th>Proyecto</th>
                                                              <th>Capa</th>
                                                              <th class="editarG" > Accion </th>
                                                              <th class="editarG" > [] </th>
                                                          </tr>
                                                      </thead>
                                                      <tbody id="tb_capa">
                                                          
                                                      </tbody>
                                                  </table>
                                                  
                                              </div><!-- /.box-body -->
                                          </div>
                                          <a id="agregar" class="btn btn-primary btn-sm" disabled>
                                          <i class="fa fa-plus fa-lg"></i>
                                          Agregar Capa
                                          </a>
                                      </div>
                                  </div>
                              </div>
                         </div>
                    </div><!-- /.box-body -->
                </div>
            </div><!-- /.col -->
        </div>
        </form>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<div class="row">
           <div class="col-md-4" id='divelemento'>
                <div class="box box-primary">
                    <div class="box-header with-border">
                       <h3 class="box-title">
                        <div id='divaccion'>Elementos</div>
                       </h3>
                       <!--<div class="box-tools pull-right">
                         <button class="btn btn-box-tool" data-widget="collapse" type="button"> <i class="fa fa-minus"></i></button>
                        </div>-->
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Buscar</label>
                                    <select class="form-control"  id="slct_elemento" name="slct_elemento" onchange="BuscarElemento()">
                                        <option value="">-Seleccione-</option>
                                    </select>
                                    <input id="txt_orden" type="hidden" value="" >
                                    <input id="txt_selfinal" type="hidden" value="" >
                                    <input id="txt_proyectoselect" type="hidden" value="" >
                                    <input id="txt_capaselect" type="hidden" value="" >
                                </div>
                            </div>
                            <div class="col-sm-12" id="selects">
                                        
                             </div>
                             <div id="geo_pattern"></div>
                             <div id="geo_responsd"></div>
                        </div>
                        <div class="row estiloItem" id="estilo_poligono" style=" display: none" > 
                            <div class="col-sm-12">
                            <div class="col-sm-12 callout callout-warning">
                                <h4>Poligono</h4>
                                <div class="row">
                                <div class="col-xs-8 col-sm-6">
                                    <p><label>Color fondo</label> <input type="text" style="cursor: pointer" title="polyDemoCreate"
                                    name="pickerPolyBack"  id="pickerPolyBack" class="color-picker" value="F7584C" /></p>
                                    <p><label>Color linea &nbsp; </label><input type="text" style="cursor: pointer" title="polyDemoCreate" 
                                        name="pickerPolyLine"  id="pickerPolyLine" class="color-picker" value="F7584C" /></p>
                                    <p>
                                        <label for="polyOpacCreate">Opacidad</label>
                                      <div id="sliderPolyOpac" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner- all" 
                                           name="sliderPolyOpac" style="width: 200px" title="polyDemoCreate">
                                        <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
                                        </div>
                                    </p>
                                    <p>
                                        <label>Linea </label>
                                        <select name="polyLineCreate" id="polyLineCreate" name="polyLineCreate">
                                            <?php
                                            for ($i = 0; $i <= 10; $i++) {
                                                echo "<option value=\"$i\">$i</option>";
                                            }
                                            ?>
                                        </select>
                                        <div></div>
                                    </p>  
                                </div>
                                <div class="col-xs-4 col-sm-6">
                                    <div id="polyDemoCreateMapx" class="demoPolygon" style="float: right; position: relative; border-color: rgb(16, 107, 56); border-style: solid; border-width: 0px; ">
                                     <img id="polyDemoCreateMap" class="demoPolygon" style="; z-index: 0; position: absolute" src="lib/geo_css/images/geoPolyImageConstruct_80x80.png">
                                     <p id="polyDemoCreate" name="polyDemoCreate" class="demoPolygon" style="background-color: #F7584C; z-index: 1; opacity: 0; position: absolute; "></p>
                                     <p id="polyDemoCreateborder" class="demoPolygon" style="z-index: 2; border-style: solid; border-color: #F7584C; position: relative; border-width: 0px"></p>
                                     </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row estiloItem" id="estilo_punto" style=" display: none"> 
                            <div class="col-sm-12">
                            <div class="col-sm-12 callout callout-warning">
                                <h4>Punto</h4>
                                <div class="row">
                                <div class="col-xs-8 col-sm-6">
                                   <!-- <p><input type="radio" value="default" name="iconSelect" >Por defecto</p> -->
                                    <p><label>Color fondo</label>
                                        <input id="pickerMarkBack" style="cursor: pointer" class="color-picker" type="text" value="F7584C">
                                    </p>  
                                </div>
                                <div class="col-xs-4 col-sm-6">
                                   <div class="newMarkerBackground">
                                       <img src="lib/geo_css/images/transparent_marker.png" style="z-index: 1; position: absolute" />
                                   </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row" id="div_capa" style=" display: none">
                           <div class="form-group">
                            <div class="col-sm-5">
                                    <label>Capa</label>
                                <input id="txt_capa" class="form-control" type="text" value="" name="txt_capa" placeholder="Ingrese Capa">
                             </div>
                             <div class="col-sm-7">
                                 <label>&nbsp;</label>
                                 <button type="button" id="btn_capa" class="form-control btn btn-primary"  onClick="GenerarCapa()">Generar Capa</button>
                             </div>
                            </div>
                        </div>
                        <div class="row" id="div_pro" style=" display: none">
                            <div class="form-group">
                             <div class="col-sm-5">
                                    <label>Proyecto</label>
                                <input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">
                             </div>
                             <div class="col-sm-7">
                                <label>&nbsp;</label>
                                <button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()"
                                >Guardar Proyecto</button>
                             </div>
                            </div>
                        </div>
                        <div class="row" id="div_cancel" style=" display: none">
                               <div class="col-sm-12">
                                <label>&nbsp;</label>
                                <button id="btn_cancelar" type="button" class="form-control btn-danger" onClick="cancelarActualzc()">Cancelar</button></div>
                        </div>
                    </div><!-- /.box-body -->
                     <div class="box-footer">
                      <div class="row">
                          <form id="form_kml" class="form-inline">
                             <div class="form-group col-sm-12">
                                 <input type="file" name="kml" id="kml_file">
                             </div>
                             <div class="form-group col-sm-6">
                                 <button type="button" id="btn_kml_upload" class="btn btn-success"><i class="fa fa-upload"></i> Importar KML</button>
                             </div>
                          </form>
                      </div>
                    </div> 
                </div><!-- /.box -->
            </div><!-- /.col -->
           <div class="col-md-8">
                <div class="box box-info">
                  <div class="box-header with-border" >
                    <h3> &nbsp;
                        <button type="button" class="btn btn-danger" id="btn_kml" rel=popover><i class="fa fa-download"></i> Exportar KML </button>
                        <button type="button" id="btn_clean" class="btn btn-primary">Limpiar Mapa</button>
                    </h3>
                  </div>
                    <div class="box-body">
                        <div id="mymap" style="height: 500px">Content</div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
</div> 

@stop
@section('formulario')
     @include( 'admin.georeferencia.form.proyecto' )
@stop
