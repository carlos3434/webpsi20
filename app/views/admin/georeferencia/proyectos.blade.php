<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

<!--
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
{{ HTML::style('lib/geo_css/flexigrid.pack.css') }}
{{ HTML::script('lib/geo_js/flexigrid.pack.js') }}
-->
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::style('lib/geo_css/geo.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.theme.css') }} 
{{ HTML::style('lib/geo_css/colpick.css') }} 
{{ HTML::script('https://maps.google.com/maps/api/js?libraries=drawing&key='.Config::get('wpsi.map.key').'') }}
{{ HTML::script('js/geo/geo.functions.js') }}

{{ HTML::script('lib/geo_js/colpick.js') }} 
{{ HTML::script('lib/geo_js/jquery.simple-color.min.js') }}
{{ HTML::script('lib/geo_js/json2.js') }}
{{ HTML::script('lib/geo_js/jquery.md5.js') }} 

{{ HTML::script('js/psigeo.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/utils.js') }}

{{ HTML::script('js/georeferencia/proyectos.js') }}
{{ HTML::script('js/georeferencia/proyectos_ajax.js') }}
{{ HTML::script('js/georeferencia/georeferenciafunction.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

<!--.@include( 'admin.georeferencia.js.proyectos' )
@include( 'admin.georeferencia.js.proyectos_ajax' )-->
@include( 'admin.js.kml' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style>
    .left {float: left;}
    .right {float: right;}

    .clearer {
        clear: both;
        display: block;
        font-size: 0;
        height: 0;
        line-height: 0;
    }

    /*
            Misc
    ------------------------------------------------------------------- */

    .hidden {display: none;}


    /*
            Example specifics
    ------------------------------------------------------------------- */

    /* Layout */

    #center-wrapper {
        margin: 0 auto;
        width: 920px;
    }


    /* Content & sidebar */

    #mymap,#sidebar {
        text-align: center;
        height: 600px;
    }

    #sidebar {
        text-align: left;
        display: none;
        overflow: auto;
    }
    #mymap {
        background-color: #EFE;
        border-color: #CDC;
        width: 98%;
    }
    .content {
        background-color: white;
    }

    .use-sidebar #mymap {width: 53%;}
    .use-sidebar #sidebar {
        display: block;
        width: 45%;
    }

    .sidebar-at-left #sidebar {margin-right: 1%;}
    .sidebar-at-right #sidebar {margin-left: 1%;}

    .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
    .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

    #separator {
        background-color: #EEE;
        border: 1px solid #CCC;
        display: block;
        outline: none;
        width: 1%;
    }
    .use-sidebar #separator {
        background: url('img/separator/vertical-separator.gif') repeat-y center top;
        border-color: #FFF;
    }
    #separator:hover {
        border-color: #ABC;
        background: #DEF;
    }
    
</style>

<div class="box box-default">
    <div class="box-header with-border">
        <div class="box-title">Proyectos</div>
        <div class="box-tools pull-right">
           
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
<!-- </div> /.box -->

<!-- Main content -->
<section class="content box-body">              
    <div class="use-sidebar sidebar-at-left" id="main">

        <div id="mymap">Content</div>
        <div id="sidebar">
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                              <div class="col-md-12" >
                                   <div class="">
                                       <div class="box-body">
                                           <div class="row">
                                               <div class="col-md-8">
                                               </div>
                                               <div class="col-sm-4">
                                                  <button type="button" id="btn_clean" class="btn btn-danger">Limpiar Mapa</button>
                                               </div>
                                           </div>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                        <div class="row">
                              <div class="col-md-12" id='divelemento'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <!--<div id='divaccion'>Elementos</div>-->
                                           <a id='divaccion' href="#elementcontent" data-parent="#divelemento" data-toggle="collapse" class="accordion-toggle">Elementos</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="elementcontent" aria-expanded="true">
                                           <div class="row">
                                               <div class="col-sm-12">
                                                   <div class="form-group">
                                                       <label>Buscar</label>
                                                        <select class="form-control"  id="slct_elemento" name="slct_elemento" onchange="                   BuscarElemento()">
                                                           <option value="">-Seleccione-</option>
                                                       </select>
                                                       <input id="txt_orden" type="hidden" value="" >
                                                       <input id="txt_selfinal" type="hidden" value="" >
                                                       <input id="txt_proyectoselect" type="hidden" value="" >
                                                       <input id="txt_capaselect" type="hidden" value="" >
                                                   </div>
                                               </div>
                                               <div class="col-sm-12" id="selects">
                                                           
                                                </div>
                                                <div id="geo_pattern"></div>
                                                <div id="geo_responsd"></div>
                                           </div>
                                           <div class="row estiloItem" id="estilo_poligono" style=" display: none" > 
                                               <div class="col-sm-12">
                                               <div class="col-sm-12 callout callout-warning">
                                                   <h4>Poligono</h4>
                                                   <div class="row">
                                                   <div class="col-xs-8 col-sm-6">
                                                        <p><label>Color fondo</label> 
                                                        <input type="text" style="cursor: pointer" title="polyDemoCreate" name="pickerPolyBack"  id="pickerPolyBack" class="color-picker" value="F7584C" /></p>
                                                        <p><label>Color linea &nbsp; </label><input type="text" style="cursor: pointer" title="polyDemoCreate" name="pickerPolyLine"  id="pickerPolyLine" class="color-picker" value="F7584C"/></p>
                                                       <p><label for="polyOpacCreate">Opacidad</label>
                                                          <div id="sliderPolyOpac" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner- all" name="sliderPolyOpac" style="width: 200px" title="polyDemoCreate">
                                                          <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left:0%;"></a>
                                                          </div>
                                                       </p>
                                                       <p><label>Linea </label>
                                                           <select name="polyLineCreate" id="polyLineCreate" name="polyLineCreate">
                                                               <?php
                                                               for ($i = 0; $i <= 10; $i++) {
                                                                   echo "<option value=\"$i\">$i</option>";
                                                               }
                                                               ?>
                                                           </select>
                                                           <div></div>
                                                       </p>  
                                                   </div>
                                                   <div class="col-xs-4 col-sm-6">
                                                        <div id="polyDemoCreateMapx" class="demoPolygon" style="float: right; position: relative; border-color: rgb(16, 107, 56); border-style: solid; border-width: 0px; ">
                                                        <img id="polyDemoCreateMap" class="demoPolygon" style="; z-index: 0; position:absolute" src="lib/geo_css/images/geoPolyImageConstruct_80x80.png">
                                                        <p id="polyDemoCreate" name="polyDemoCreate" class="demoPolygon" style="background-color: #F7584C; z-index: 1; opacity: 0; position: absolute; "></p>
                                                        <p id="polyDemoCreateborder" class="demoPolygon" style="z-index: 2; border-style:solid; border-color: #F7584C; position: relative; border-width: 0px"></p>
                                                        </div>
                                                   </div>
                                                   </div>
                                               </div>
                                               </div>
                                           </div>
                                           <div class="row estiloItem" id="estilo_punto" style=" display: none"> 
                                               <div class="col-sm-12">
                                               <div class="col-sm-12 callout callout-warning">
                                                   <h4>Punto</h4>
                                                   <div class="row">
                                                   <div class="col-xs-8 col-sm-6">
                                                      <!-- <p><input type="radio" value="default" name="iconSelect" >Por defecto</p> -->
                                                       <p><label>Color fondo</label>
                                                        <input id="pickerMarkBack" style="cursor: pointer" class="color-picker" type="text" value="F7584C">
                                                       </p>  
                                                   </div>
                                                   <div class="col-xs-4 col-sm-6">
                                                      <div class="newMarkerBackground">
                                                        <img src="lib/geo_css/images/transparent_marker.png" style="z-index: 1; position:absolute" />
                                                      </div>
                                                   </div>
                                                   </div>
                                               </div>
                                               </div>
                                           </div>
                                           <div class="row" id="div_capa" style=" display: none">
                                              <div class="form-group">
                                               <div class="col-sm-5">
                                                       <label>Capa</label>
                                                      <input id="txt_capa" class="form-control" type="text" value="" name="txt_capa"placeholder="Ingrese Capa">
                                                </div>
                                                <div class="col-sm-7">
                                                    <label>&nbsp;</label>
                                                    <button type="button" id="btn_capa" class="form-control btn btn-warning"  onClick="GenerarCapa()">Generar Capa</button>
                                                </div>
                                               </div>
                                           </div>
                                           <div class="row" id="div_pro" style=" display: none">
                                               <div class="form-group">
                                                <div class="col-sm-5">
                                                       <label>Proyecto</label>
                                                        <input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">
                                                </div>
                                                <div class="col-sm-7">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()">Guardar Proyecto</button>
                                                </div>
                                               </div>
                                           </div>
                                           <div class="row" id="div_cancel" style=" display: none">
                                                  <div class="col-sm-12">
                                                   <label>&nbsp;</label>
                                                  <button id="btn_cancelar" type="button" class="form-control btn-danger" onClick="cancelarActualzc()">Cancelar</button></div>
                                           </div>
                                           <br>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                               <div class="col-md-12" id='divzonapersonalizada' style="display:none">
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <!--<div id='divaccion'>Elementos</div>-->
                                           <a id='divzona' href="#zonacontent" data-parent="#divelemento" data-toggle="collapse" class="accordion-toggle">Zona Personalizada</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="zonacontent" aria-expanded="true">
                                           <div class="row">
                                              <div class="form-group">
                                                <fieldset style="border: 1px solid silver;padding: 0.35em 0 0.75em; margin: 20px;">
                                                <div class="col-sm-6">
                                                  <label>Nombre Zona</label>
                                                  <input id="txt_poligono" class="form-control" type="text" value="" name="txt_poligono" >
                                                </div>
                                                <div class="col-sm-6">
                                                  <label>Actividad</label>
                                                  <select id="slct_actividad" class="form-control" name="slct_actividad[]" multiple="">
                                                      <option value="1">Avería</option>
                                                      <option value="2">Provisión</option>
                                                  </select>
                                                </div>
                                                <div class="col-sm-6">
                                                  <label>Tipo Zona</label>
                                                  <select class="form-control"  id="slct_tipo_zona_id" name="slct_tipo_zona_id" onchange="Zonachange()">
                                                    <option value="1">GeoCerca</option>
                                                    <option value="2">Zona Sombra</option>
                                                    <option value="3">Zona Peligrosa</option>
                                                    <option value="4">Zona Tipo 4</option>
                                                    <option value="5">Zona Tipo 5</option>
                                                    <option value="6">Zona Tipo 6</option>
                                                    <option value="7">Zona Tipo 7</option>
                                                    <option value="8">Zona Tipo 8</option>
                                                    <option value="9">Zona Tipo 9</option>
                                                    <option value="10">Zona Tipo 10</option>
                                                  </select>
                                                </div>
                                                </fieldset>
                                                <div id="divguardarzonas">
                                                <div class="col-sm-6">
                                                  <label>Nombre Proyecto</label>
                                                  <input id="txt_proyecto_zona" class="form-control" type="text" value="" name="txt_proyecto_zona" >
                                                </div>
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_proyecto_zona" type="button" class="form-control btn btn-success" onClick="GuardarProyectoZona()" >Guardar Proyecto</button>
                                                </div>
                                                </div>
                                                <div id="divagregarzonas" style="display:none">
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_cancelar" type="button" class="form-control btn-danger" onClick="cancelActualizaZona()">Cancelar</button>
                                                </div>
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_agregar_zona" type="button" class="form-control btn btn-success" onClick="GuardarZonas()" >Agregar Zonas</button>
                                                </div>
                                                </div>
                                              </div>
                                           </div>
                                           <br>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                        <div class="row">
                              <div class="col-md-12" id='divlistado'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <a href="#listadocontent" data-parent="#divlistado" data-toggle="collapse" >Listado</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="listadocontent" aria-expanded="true">
                                           <div class="row">
                                              <div class="col-sm-12">
                                                  <div>
                                                      <ul id="myTab" class="nav nav-tabs" role="tablist">
                                                          <li role="presentation" class="active">
                                                              <a href="#t_proyectos">Proyectos</a>
                                                          </li>
                                                          <li role="presentation">
                                                              <a href="#t_capas" id='a_capa'>Capas</a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                                  <div class="tab-content">
                                                      <div class="tab-pane fade active in" id="t_proyectos">
                                                          <div class="box" >
                                                              <div class="box-body table-responsive" >
                                                                  <table id="t_proyecto" class="table table-bordered table-striped">
                                                                      <thead>
                                                                          <tr>
                                                                              <th>Id</th>
                                                                              <th>Proyecto</th>
                                                                              <th>Fecha</th>
                                                                              <th>Público</th>
                                                                              <th width="130px"> [] </th>
                                                                          </tr>
                                                                      </thead>
                                                                      <tbody id="tb_proyecto">
                                                                          
                                                                      </tbody>
                                                                  </table>
                                                              </div><!-- /.box-body -->
                                                          </div><!-- /.box -->
                                                      </div>
                                                      <div class="tab-pane fade in" id="t_capas" >
                                                          <div class="box">
                                                              <div class="box-body table-responsive">
                                                                  <div class="col-md-8">
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                  <a id="agregar" onclick="Agregarcapa()" class="btn btn-success btn-sm" disabled>
                                                                  <i class="fa fa-plus fa-lg"></i>
                                                                  Agregar Capa
                                                                  </a>
                                                                  <h1></h1>
                                                                  </div>
                                                                  <table id="t_capa" class="table table-bordered table-striped">
                                                                      <thead>
                                                                          <tr>
                                                                              <th>Id</th>
                                                                              <th>Proyecto</th>
                                                                              <th>Capa</th>
                                                                              <th class="editarG" > Accion </th>
                                                                              <th class="editarG" > [] </th>
                                                                          </tr>
                                                                      </thead>
                                                                      <tbody id="tb_capa">
                                                                          
                                                                      </tbody>
                                                                  </table>
                                                                  
                                                              </div><!-- /.box-body -->
                                                          </div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                           </div>
                                       </div><!-- /.box-body -->
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                         <div class="row">
                              <div class="col-md-12" id='divkml'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <a href="#kmlcontent" data-parent="#divkml" data-toggle="collapse" >KML</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="kmlcontent" aria-expanded="true">
                                           <div class="form-group">
                                              <div class="col-sm-12">
                                                  <button type="button" class="btn btn-info" id="btn_kml" rel=popover name="btn_kml"><i class="fa fa-download"></i>    Exportar KML </button>
                                                  <h1></h1>
                                              </div>
                                           </div>
                                           <div class="row">
                                                <form id="form_kml" class="form-inline">
                                                    <div class="form-group col-sm-8">
                                                        <input type="file" name="kml_file" id="kml_file">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                    <button type="button" id="btn_kml_upload" class="btn btn-info">
                                                    <i class="fa fa-upload"></i> Importar KML</button>
                                                    </div>
                                                </form>
                                            </div>
                                       </div><!-- /.box-body -->
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                    </div><!-- /.box-body -->
                </div>
        </div>

        <a href="#" id="separator"></a>

        <div class="clearer">&nbsp;</div>

    </div>

</section><!-- /.content -->
</div>
@stop
@section('formulario')
     @include( 'admin.georeferencia.form.proyecto' )
     @include( 'admin.georeferencia.form.proyectocapa' )
@stop
