<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::style('lib/iCheck/all.css') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.date.extensions.js') }}

{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}


{{ HTML::script('http://maps.google.com/maps/api/js?libraries=geometry&v=3.exp&sensor=true') }}

<?php // HTML::script('js/geo/geo.functions.js') ?>

{{ HTML::script('js/psigeo.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@include( 'admin.georeferencia.js.alarma_ajax' )
@include( 'admin.georeferencia.js.alarma' )

<style type="text/css">
    .alarmatap-leyenda .list-group-item{padding: 1px 15px;}
    .alarmatap-lista,.alarmatap-detalle{height:220px;overflow:auto;}
    .alarmatap-detalle .list-group-item{padding: 2px 15px;}
    .alarmatap-lista .list-group-item{padding: 4px 15px;border: 1px solid #000;}
    .alarmatap-lista .list-group-item a{color: #000;display: block;}
</style>
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<section class="content-header">
    <h1>
        {{trans('main.alarmas')}}
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{trans('main.admin')}}</a></li>
        <li><a href="#">{{trans('main.georeferencia')}}</a></li>
        <li class="active">{{trans('main.alarmas')}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-4 col-sm-3 col-xs-12">

            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title">{{trans('georeferencia.alarma_tap')}}</div>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" id="form_alarmatap">
                        <div class="form-group"  style="margin-bottom: 0;">
                            <label for="chk_zonal" class="col-sm-2 control-label">{{trans('georeferencia.zonal')}}</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="slct_zonal" name="slct_zonal">
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="col-sm-offset-1 col-sm-11">
                                <div class="checkbox">
                                    <label>
                                        <input checked="" type="checkbox" id="chk_mostrar"> {{trans('georeferencia.mostrar_alarma')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="chk_actualizar"> {{trans('georeferencia.actualizacion')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-title">{{trans('georeferencia.trobas')}}</div>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <h5><b>Lista de trobas</b></h5>
                    <ul class="list-group alarmatap-lista">
                        
                    </ul>
                    <hr>
                    <h5><b>Detalle de troba </b><span class="alarmatap-detalle-titulo"></span></h5>
                    <ol class="list-group alarmatap-detalle">

                    </ol>
                </div>
            </div>

            <div class="box box-info collapsed-box">
                <div class="box-header with-border">
                    <div class="box-title">{{trans('georeferencia.leyenda')}}</div>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="display: none;">
                    <ul class="list-group alarmatap-leyenda">
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-danger btn-lg alarmatap-limpiar">{{trans('georeferencia.limpiar_mapa')}}</button>
                </div>
            </div>

        </div>
        <div class="col-md-8 col-sm-9 col-xs-12">
   
            <div class="box box-info">
                <div class="box-body no-padding">
                    <div id="mymap" style="height: 500px">Content</div>
                </div>
            </div>

        </div>
    </div>
</section>
@stop