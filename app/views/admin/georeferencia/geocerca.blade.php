<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
<!--
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/input-mask/js/jquery.inputmask.js') }}
{{ HTML::style('lib/geo_css/flexigrid.pack.css') }}
{{ HTML::script('lib/geo_js/flexigrid.pack.js') }}
-->
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::style('lib/geo_css/geo.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.css') }}
{{ HTML::style('lib/geo_css/jquery-ui.theme.css') }} 
{{ HTML::style('lib/geo_css/colpick.css') }} 
{{ HTML::script('https://maps.google.com/maps/api/js?libraries=drawing&key='.Config::get('wpsi.map.key').'') }}
{{ HTML::script('js/geo/geo.functions.js') }}

{{ HTML::script('lib/geo_js/colpick.js') }} 
{{ HTML::script('lib/geo_js/jquery.simple-color.min.js') }}
{{ HTML::script('lib/geo_js/json2.js') }}
{{ HTML::script('lib/geo_js/jquery.md5.js') }} 

{{ HTML::script('js/psigeo.js') }}
{{ HTML::script('js/psi.js') }}
{{ HTML::script('js/utils.js') }}

{{ HTML::script('js/georeferencia/geocerca.js') }}
{{ HTML::script('js/georeferencia/geocerca_ajax.js') }}
{{ HTML::script('js/georeferencia/georeferenciafunction.js') }}

@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

<!--
@include( 'admin.georeferencia.js.proyectos' )
@include( 'admin.georeferencia.js.proyectos_ajax' )-->

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<div class="box box-default">
    <div class="box-header with-border">
        <div class="box-title">Geo Cercas</div>
        <div class="box-tools pull-right">
           
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
<!-- </div> /.box -->

<!-- Main content -->
<section class="content box-body">              
    <div class="use-sidebar sidebar-at-left" id="main">

        <div id="mymap">Content</div>
        <div id="sidebar">
            <div class="box box-info">
                    <div class="box-body">
                        <div class="row">
                              <div class="col-md-12" >
                                   <div class="">
                                       <div class="box-body">
                                           <div class="row">
                                               <div class="col-md-8">
                                               </div>
                                               <div class="col-sm-4">
                                                  <button type="button" id="btn_clean" class="btn btn-danger">Limpiar Mapa</button>
                                               </div>
                                           </div>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                        <div class="row">
                              <div class="col-md-12" id='divzonapersonalizada'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <!--<div id='divaccion'>Elementos</div>-->
                                           <a id='divzona' href="#zonacontent" data-parent="#divelemento" data-toggle="collapse" class="accordion-toggle">Creación de Zonas</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="zonacontent" aria-expanded="true">
                                           <div class="row">
                                              
                                                <fieldset style="border: 1px solid silver;padding: 0.35em 0 0.75em; margin: 0 20px;">
                                                <legend class="legendblade">Antes de crear una zona:</legend>
                                                <div class="form-group col-sm-12">
                                                <div class="col-sm-6">
                                                  <label>Nombre Zona</label>
                                                  <input id="txt_poligono" class="form-control" type="text" value="" name="txt_poligono" >
                                                </div>
                                                <div class="col-sm-6">
                                                  <label>Tipo Zona</label>
                                                  <select class="form-control"  id="slct_tipo_zona_id" name="slct_tipo_zona_id" onchange="Zonachange()">
                                                  </select>
                                                </div>
                                                </div>
                                                <div class="form-group col-sm-12">
                                                <div class="col-sm-6">
                                                  <label>Actividad</label>
                                                  <select id="slct_actividad" class="form-control" name="slct_actividad[]" multiple="">
                                                      <option value="1">Avería</option>
                                                      <option value="2">Provisión</option>
                                                  </select>
                                                </div>
                                                <div class="col-sm-6">
                                                  <label>Quiebre</label>
                                                  <select class="form-control"  id="slct_quiebre_id" name="slct_quiebre_id[]" multiple="">
                                                  </select>
                                                </div>
                                                </div>
                                                <div class="form-group col-sm-12">
                                                <div class="col-sm-6">
                                                  <label class="control-label">Actividad Tipo</label>
                                                  <select class="form-control"  id="slct_actividad_tipo_id" name="slct_actividad_tipo_id[]" multiple="">
                            </select>
                                                </div>
                                                </div>
                                                </fieldset>
                                                <div id="divguardarzonas">
                                                <div class="col-sm-6">
                                                  <label>Nombre Proyecto</label>
                                                  <input id="txt_proyecto_zona" class="form-control" type="text" value="" name="txt_proyecto_zona" >
                                                </div>
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_proyecto_zona" type="button" class="form-control btn btn-success" onClick="GuardarProyectoZona()" >Guardar Proyecto</button>
                                                </div>
                                                </div>
                                                <div id="divagregarzonas" style="display:none">
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_cancelar" type="button" class="form-control btn-danger" onClick="cancelActualizaZona()">Cancelar</button>
                                                </div>
                                                <div class="col-sm-6">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_agregar_zona" type="button" class="form-control btn btn-success" onClick="GuardarZonas()" >Agregar Zonas</button>
                                                </div>
                                                </div>
                                           </div>
                                           <br>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                              <div class="col-md-12" id='divelemento'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <!--<div id='divaccion'>Elementos</div>-->
                                           <a id='divaccion' href="#elementcontent" data-parent="#divelemento" data-toggle="collapse" class="accordion-toggle">Geo Elementos</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="elementcontent" aria-expanded="true">
                                           <div class="row">
                                               <div class="col-sm-12">
                                                   <div class="form-group">
                                                       <label>Buscar</label>
                                                        <select class="form-control"  id="slct_elemento" name="slct_elemento" onchange="                   BuscarElemento()">
                                                           <option value="">-Seleccione-</option>
                                                       </select>
                                                       <input id="txt_orden" type="hidden" value="" >
                                                       <input id="txt_selfinal" type="hidden" value="" >
                                                       <input id="txt_proyectoselect" type="hidden" value="" >
                                                   </div>
                                               </div>
                                               <div class="col-sm-12" id="selects">
                                                           
                                                </div>
                                                <div id="geo_pattern"></div>
                                                <div id="geo_responsd"></div>
                                           </div>
                                           <div class="row" id="div_capa" style=" display: none">
                                              <div class="form-group">
                                                <!--<div class="col-sm-5">
                                                       <label>Capa</label>
                                                      <input id="txt_capa" class="form-control" type="text" value="" name="txt_capa"placeholder="Ingrese Capa">
                                                </div>-->
                                                <div class="col-sm-12">
                                                    <label>&nbsp;</label>
                                                    <button type="button" id="btn_capa" class="form-control btn btn-warning"  onClick="MapearElementos()">Mapear Elementos</button>
                                                </div>
                                               </div>
                                           </div>
                                           <!--<div class="row" id="div_pro" style=" display: none">
                                               <div class="form-group">
                                                <div class="col-sm-5">
                                                       <label>Proyecto</label>
                                                        <input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">
                                                </div>
                                                <div class="col-sm-7">
                                                   <label>&nbsp;</label>
                                                    <button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()">Guardar Proyecto</button>
                                                </div>
                                               </div>
                                           </div>-->
                                           <div class="row" id="div_cancel" style=" display: none">
                                                  <div class="col-sm-12">
                                                   <label>&nbsp;</label>
                                                  <button id="btn_cancelar" type="button" class="form-control btn-danger" onClick="cancelarActualzc()">Cancelar</button></div>
                                           </div>
                                           <br>
                                       </div><!-- /.box-body -->
                                        
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                        <div class="row">
                              <div class="col-md-12" id='divlistado'>
                                   <div class="box box-primary">
                                       <div class="box-header with-border">
                                          <h3 class="box-title">
                                           <a href="#listadocontent" data-parent="#divlistado" data-toggle="collapse" >Listado</a>
                                          </h3>
                                       </div>
                                       <div class="box-body collapse in" id="listadocontent" aria-expanded="true">
                                           <div class="row">
                                              <div class="col-sm-12">
                                                  <div>
                                                      <ul id="myTab" class="nav nav-tabs" role="tablist">
                                                          <li role="presentation" class="active">
                                                              <a href="#t_proyectos">Proyectos</a>
                                                          </li>
                                                          <li role="presentation">
                                                              <a href="#t_zona" id='a_capa'>Zonas</a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                                  <div class="tab-content">
                                                      <div class="tab-pane fade active in" id="t_proyectos">
                                                          <div class="box" >
                                                              <div class="box-body table-responsive" >
                                                                  <table id="t_proyecto" class="table table-bordered table-striped">
                                                                      <thead>
                                                                          <tr>
                                                                              <th>Id</th>
                                                                              <th>Proyecto</th>
                                                                              <th>Estado</th>
                                                                              <th width="120px"> [] </th>
                                                                          </tr>
                                                                      </thead>
                                                                      <tbody id="tb_proyecto">
                                                                          
                                                                      </tbody>
                                                                  </table>
                                                              </div><!-- /.box-body -->
                                                          </div><!-- /.box -->
                                                      </div>
                                                      <div class="tab-pane fade in" id="t_zona" >
                                                          <div class="box">
                                                              <div class="box-body table-responsive">
                                                                  <div class="col-md-4">
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                    <button id="btn_cleaned" class="btn btn-danger btn-sm" type="button">Limpiar Mapa</button>
                                                                  </div>
                                                                  <div class="col-md-4">
                                                                  <a id="agregar" onclick="Agregarzona()" class="btn btn-success btn-sm" disabled="">
                                                                  <i class="fa fa-plus fa-lg"></i>
                                                                  Agregar Zonas
                                                                  </a>
                                                                  <h1></h1>
                                                                  </div>
                                                                  <table id="t_zonas" class="table table-bordered table-striped">
                                                                      <thead>
                                                                          <tr>
                                                                              <th>Id</th>
                                                                              <th>Detalle</th>
                                                                              <th width="100px">Zona / Actividad</th>
                                                                              <th class="editarG" > Accion </th>
                                                                              <th class="editarG" width="20px"> [] </th>
                                                                          </tr>
                                                                      </thead>
                                                                      <tbody id="tb_zonas">
                                                                          
                                                                      </tbody>
                                                                  </table>
                                                                  
                                                              </div><!-- /.box-body -->
                                                          </div>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                           </div>
                                       </div><!-- /.box-body -->
                                   </div><!-- /.box -->
                               </div><!-- /.col -->
                         </div>
                    </div><!-- /.box-body -->
                </div>
        </div>

        <a href="#" id="separator"></a>

        <div class="clearer">&nbsp;</div>

    </div>

</section><!-- /.content -->
</div>
@stop
@section('formulario')
     @include( 'admin.georeferencia.form.geoproyecto' )
     @include( 'admin.georeferencia.form.geoproyectozona' )
@stop

<style>
    .left {float: left;}
    .right {float: right;}

    .clearer {
        clear: both;
        display: block;
        font-size: 0;
        height: 0;
        line-height: 0;
    }

    /*
            Misc
    ------------------------------------------------------------------- */

    .hidden {display: none;}


    /*
            Example specifics
    ------------------------------------------------------------------- */

    /* Layout */

    #center-wrapper {
        margin: 0 auto;
        width: 920px;
    }


    /* Content & sidebar */

    #mymap,#sidebar {
        text-align: center;
        height: 600px;
    }

    #sidebar {
        text-align: left;
        display: none;
        overflow: auto;
    }
    #mymap {
        background-color: #EFE;
        border-color: #CDC;
        width: 98%;
    }
    .content {
        background-color: white;
    }

    .use-sidebar #mymap {width: 53%;}
    .use-sidebar #sidebar {
        display: block;
        width: 45%;
    }

    .sidebar-at-left #sidebar {margin-right: 1%;}
    .sidebar-at-right #sidebar {margin-left: 1%;}

    .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
    .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

    #separator {
        background-color: #EEE;
        border: 1px solid #CCC;
        display: block;
        outline: none;
        width: 1%;
    }
    .use-sidebar #separator {
        background: url('img/separator/vertical-separator.gif') repeat-y center top;
        border-color: #FFF;
    }
    #separator:hover {
        border-color: #ABC;
        background: #DEF;
    } 
    .legendblade {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
    border-image: none;
    border-style: none none solid;
    border-width: 0 0 1px;
    color: #333;
    display: block;
    border-style: none;
    width: 50%;
    font-size: 14px;
    margin-bottom: 5px;
    }
</style>
