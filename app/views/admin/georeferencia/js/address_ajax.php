<script type="text/javascript">
    var actividadObj;
    var actividadTipoObj;
    var currentPage = 0;
    var search = '';
    var Funciones = {
        BuscarDireccion: function (dat) {
            var datos = dat;
            var accion = "georeferencia/buscar";

            bounds = new google.maps.LatLngBounds();
            $.ajax({
                url: accion,
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    $(".overlay,.loading-img").remove();
                    addressToMap(
                        obj,
                        $("#slct_distrito option:selected").text(),
                        $("#txt_calle").val(),
                        obj.length,
                        false
                    );
                },
                error: function () {
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'No se encontro resultado', 6000);

                }
            });
        }, BuscarMasiva: function () {
            //var inputFileImage = document.getElementById("dirfile");
            //var file = inputFileImage.files[0];
            //var data = new FormData();
            var url = "georeferencia/buscarmasiva";

            //data.append('archivo', file);

            $.ajax({
                url: url,
                type: 'POST',
                contentType: false,
                //data: data,
                processData: false,
                cache: false,
                dataType: "json",
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (datos) {
                    $(".overlay,.loading-img").remove();
                    addressToMap(
                        datos.data,
                        '',
                        '',
                        datos.data.length,
                        true
                    );
                    /*var html = '';
                    for (i = 0; i < datos.notFound.length; i++) {
                        html += "<tr><td>" + (i + 1) + "</td><td>" + datos.notFound[i][2] + "</td><td>" + datos.notFound[i][0] + "</td><td>" + datos.notFound[i][1] + "</td></tr>";
                    }
                    $("#sidebar #notFound").html(html);*/
                    html = '<table><tr><th>N</th><th>ID</th><th>Direccion</th><th>XY</th></tr>';
                    for (i = 0; i < datos.data.length; i++) {
                        html += "<tr><td>" + (i + 1) + "</td><td>" + datos.data[i].id + "</td><td>" + datos.data[i].SEARCH + "</td><td>" + datos.data[i].XY + "</td></tr>";
                    }
                    html += "</table>";

                    console.log(html);

                    window.open('data:application/vnd.ms-excel,' + html);
                }
            });
        },
        DescargarExcel: function () {
            var datos = $("#FormularioExportacion").serialize();
            $.ajax({
                url: "georeferencia/descargarexcel",
                type: 'POST',
                cache: false,
                data: datos,
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (datos) {
                    $(".overlay,.loading-img").remove();
                }
            });
        }
    }
</script>
