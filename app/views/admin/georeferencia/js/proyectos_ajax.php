<script type="text/javascript">
var proyectos_id, proyectosObj;
var capas_id, capasObj;
var mapObjects = [];
var coordinatesObjects = [];
var viewcapaskml = [];
var bounds;
var ixobj=0;
var markTextColor = textByColor("#" 
                    + $("#pickerMarkBack")
                    .val())
                    .substring(1);

  /*  itemChars = "&chld=" 
            + 1
            + "|" 
            + $("#pickerMarkBack").val() 
            + "|"
            + markTextColor;*/
var Proyectos = {
        /*BuscarElemento: function(elemento_id,origen) {
          
           $.ajax({
            url         : 'geodependencia/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {elemento_id:elemento_id,origen:origen},
            beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
            success : function(obj) {
                   $(".overlay,.loading-img").remove();
                var _html="";
                var array=[];
                //$(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $.each(obj.datos,function(index,data){
                             _html+="<label>"+data.etiqueta+"</label>"
                             _html+="<div id='div_"+data.campo+"'><select class='form-control geo_item' name='slct_" + data.campo + "[]' id='slct_" + data.campo + "' "+
                                    " title=\""+data.campo+"\"  onchange=\"opcionSelected(this.title)\" > "+
                                     "<option value='0'>- Seleccione -</option>"+
                                    "</select></div>";
                             if (index==0) {
                                campo=data.campo
                             }
                             //$("#slct_"+data.campo).attr('onchange','Filtroslct()');
                             array[index]=data.campo;

                    });
                     $("#txt_orden").val(array.join("_"));
                     $("#geo_response").html(_html);
                     $("#slct_"+campo).attr('multiple','multiple');
                     slctGlobal.listarSlct('geo'+origen,'slct_'+campo,'multiple',null,'');

                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
        },

        CargarFiltro: function(origen,titulo,campo) {
           var datos=$("#form_proyectos").serialize().split("txt_").join("").split("slct_").join("");
           slctGlobal.listarSlctpost('geo'+origen,campo,'slct_'+campo,'multiple',null,datos);
        },

        TrazarCapaPoligono: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

            var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    var lati="";
                    var longi="";
                    if(obj.rst==1){ 
                      if(grosorlinea==0){grosorlinea=3;}
                      if(opacidad==''){opacidad='0.3';}
                        
                        $.each(obj.datos, function(index, dato) {
                              var triangleCoords=[];
                              var parame=[];
                              parame=dato.coord.split("|");
                              for (var i = 0; i < parame.length; i++) {
                                  var locat=[];
                                  locat=parame[i].split(",");
                                  triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                  var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                  bounds.extend(pt);
                              }

                              var bermudaTriangle = new google.maps.Polygon({
                                                      paths: triangleCoords,
                                                      strokeColor: '#'+borde,
                                                      strokeOpacity: 0.8,
                                                      strokeWeight: grosorlinea,
                                                      fillColor: '#'+fondo,
                                                      fillOpacity: opacidad,
                                                      id:"Capa: "+capa+"<br>"+dato.detalle,
                                                      indice:ixobj,
                                                      tipo:2
                                                  });

                                mapObjects.push({"elemento": origen,"figura_id":'2',"capa": capa,"detalle":dato.detalle,"orden":'',
                                  "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":"",
                                  "fondo":'#'+fondo,"opacidad":opacidad});

                                coordinatesObjects.push({"coord":dato.coord,"figura_id":'2'});

                                ixobj++;
                                bermudaTriangle.setMap(map);
                                infoWindow = new google.maps.InfoWindow; 

                                bermudaTriangle.addListener("click", function(event) {
                                        doPolyAction(event,this);
                                });
                            });
                            map.fitBounds(bounds);
                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

        TrazarCapaPunto: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

            //var data = $("form#form_proyectos").serialize().split("txt_").join("").split("slct_").join("");
            var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                     $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                     bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    if(obj.rst==1){
                          var idpunto=0;
                          $.each(obj.datos, function(index, dato) {
                                idpunto++;

                                iconSelected = googleMarktxt 
                                  + idpunto 
                                  + "|" 
                                  + $("#pickerMarkBack").val() 
                                  + "|" 
                                  + markTextColor;

                                 var locat=[];
                                 locat=dato.coord.split(",");
                                 var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                            
                                 var marker = new google.maps.Marker({
                                     position: myLatLng,
                                     map: map,
                                     draggable: true,
                                     animation: google.maps.Animation.DROP,
                                     icon: iconSelected,
                                     id:"Capa: "+capa+"<br>"+dato.detalle,
                                     indice:ixobj,
                                     tipo:3,
                                     numero:idpunto,
                                     draggable:false
                                  });
                                 ixobj++;

                                  mapObjects.push({"elemento": origen, "figura_id":'3',"capa": capa,"detalle":dato.detalle,"orden":idpunto,
                                                   "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":"",
                                                   "fondo":'#'+$("#pickerMarkBack").val(),"opacidad":opacidad});

                                  coordinatesObjects.push({"coord":dato.coord,"figura_id":'3'});

                                
                                  infoWindow = new google.maps.InfoWindow;
    
                                  marker.addListener("click", function(event) {
                                       doPolyAction(event,this);
                                  });

                        });

                        map.fitBounds(bounds);

                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

         TrazarCapaCircle: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

           var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    if(obj.rst==1){
                        if(grosorlinea==0){grosorlinea=3;}
                        if(opacidad==''){opacidad='0.3';}   
                        $.each(obj.datos, function(index, dato) {
                                 var locat=[];
                                 locat=dato.coord.split(",");
                                 var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                            
                                 var cityCircle = new google.maps.Circle({
                                    strokeColor: '#'+borde,
                                    strokeOpacity: 0.8,
                                    strokeWeight: grosorlinea,
                                    fillColor: '#'+fondo,
                                    fillOpacity: opacidad,
                                    map: map,
                                    center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                    radius: Number(dato.dato),
                                    id:"Capa: "+capa+"<br>"+dato.detalle,
                                    indice:ixobj,
                                    tipo:1
                                  });

                                 ixobj++;

                                 bounds.union(cityCircle.getBounds());

                                 mapObjects.push({"elemento": origen,"figura_id":'1',"capa": capa,"detalle":dato.detalle,"orden":'',
                                          "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":"",
                                          "fondo":'#'+fondo,"opacidad":opacidad});

                                coordinatesObjects.push({"figura_id":'1',"coord":dato.coord,"radius":Number(dato.dato)});
                                
                                  infoWindow = new google.maps.InfoWindow;

                                  cityCircle.addListener("click", function(event) {
                                       doPolyAction(event,this);
                                  }); 
                            });

                        map.fitBounds(bounds);

                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

        GuardarProyecto: function(origen) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyecto/save',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,nombre: $("#txt_proyecto").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            Proyectos.CargarProyectos();
                            $("#txt_proyecto").val('');
                            Proyectos.CargarCapas(obj.datos);
                            $("#txt_proyectoselect").val(obj.datos);
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#btn_clean").click();
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                        }
                        else{
                            var msj;
                            $.each(obj.msj,function(index,datos){
                              msj=datos;
                            });
                            Psi.mensaje('danger', msj.join(), 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        ModificarProyecto: function() {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapa/agregarcapa',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idproyecto: $("#txt_proyectoselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                            //Proyectos.CargarProyectos();
                            Proyectos.CargarCapas(obj.datos);
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#btn_clean").click();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        ModificarProyectoCapa: function() {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapaelemento/agregarelemento',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idcapa: $("#txt_capaselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                            Proyectos.CargarProyectos();
                            Proyectos.CargarCapas($("#txt_proyectoselect").val());
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#txt_proyectoselect").val('')
                            $("#btn_clean").click();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        ModificarProyectoZona: function() {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapaelemento/agregarelementozona',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idcapa: $("#txt_capaselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            Proyectos.CargarProyectos();
                            Proyectos.CargarCapas($("#txt_proyectoselect").val());
                            //BuscarElemento();
                            $("#txt_poligono").val('');
                            $("#btn_clean").click();
                            cancelActualizaZona();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Dibuje Zonas');
            }
        },

        LimpiarMapa: function() {
           $("#txt_proyecto").val('');
           $("#txt_capa").val('');
           if(mapObjects.length>0) {
             mapObjects.length=0;
             mapObjects=[];
             ixobj=0;
             coordinatesObjects.length=0;
             coordinatesObjects=[];
           }
           if(viewcapaskml.length>0) {
             viewcapaskml.length=0;
             viewcapaskml=[];
           }
        },

        ModificarPoligono: function(id,tipo) {
          if (tipo==2 || tipo==1 || tipo==4) {
              mapObjects[id]['borde']='#' + $("#singlePolygonLine").val();
              mapObjects[id]['grosorlinea']=$("#singlePolygonStroke").val();
              mapObjects[id]['fondo']='#'+ $("#singlePolygonFill").val();
              mapObjects[id]['opacidad']= $("#singlePolygonOpacity").val();
          }
          if (tipo==3 ){
            mapObjects[id]['fondo']='#'+ $("#singlePolygonFill").val();
            }
          if (tipo==4) {
            mapObjects[id]['detalle']=$("#txt_detalle").val();
            mapObjects[id]['tipo_zona']=$("#slct_tipo_zona_edit_id").val();
            var actividad=$("#slct_actividad_edit").val();
            var provision=averia=0;
            if(actividad!==null){
              if(actividad.includes("1")){
                averia=1;
              } 
              if(actividad.includes("2")){
                provision=1;
              }
            }
            mapObjects[id]['averia']=averia;
            mapObjects[id]['provision']=provision;
          }
          infoWindow.close(objMap);
          //alert(id);
          //alert(JSON.stringify(mapObjects[id]));
        },

        EliminarPoligono: function(id,tipo) {
            //mapObjects.splice(id,1);
            delete mapObjects[id]; // el id debe quedar solo vacia, para no afectar el array de otro poligono o punto
            coordinatesObjects.splice(id,1);
        },

        CargarProyectos:function(){
           $.ajax({
               url         : 'geoproyecto/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarProyectos(obj.datos);
                       proyectosObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       CargarCapas:function(id){
           $.ajax({
               url         : 'geoproyectocapa/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {id:id},
               beforeSend : function() {
                   $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarCapas(obj.datos);
                       capasObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       MapearCapas:function(id,idproyecto,map){
           $.ajax({
               url         : 'geoproyectocapa/mapear',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {capaid:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                  bounds = new google.maps.LatLngBounds();
               },
               success : function(obj) {
                  $(".overlay,.loading-img").remove();
                  if(obj.rst==1){
                     $.each(obj.datos, function(index, data) { 
                        viewcapaskml.push(data);
                        if (data.figura_id==2 || data.figura_id==4) {
                            var triangleCoords=[];
                            var parame=[];
                            if(data.coord!==null) {
                              parame=data.coord.split("|");
                            } else {
                              parame=data.coordenadas.split("|");
                            }
                            for (var i =0; i < parame.length; i++) {
                                 var locat=[];
                                 locat=parame[i].split(",");
                                 triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                             }
                             var actividad=[];
                             if(data.averia==1){
                              actividad.push("1");
                             }
                             if(data.provision==1){
                              actividad.push("2");
                             }

                             var bermudaTriangle = new google.maps.Polygon({
                                       paths: triangleCoords,
                                       strokeColor: data.borde,
                                       strokeOpacity: 0.8,
                                       strokeWeight: data.grosorlinea,
                                       fillColor: data.fondo,
                                       fillOpacity: data.opacidad,
                                       title:"Proyecto: "+data.proyecto+"<br>"+"Capa: "+data.capa,
                                       proyecto: "Proyecto: "+data.proyecto,
                                       id:data.id,
                                       detalle: data.detalle,
                                       tipo:data.figura_id,
                                       idproyecto:idproyecto,
                                       capa_id:id,
                                       tipo_zona:data.tipo_zona,
                                       actividad:actividad
                                   });

                                   bermudaTriangle.setMap(map);

                                   infoWindow = new google.maps.InfoWindow; 
                                   bermudaTriangle.addListener("click", function(event) {
                                           doPolyActionEdit(event,bermudaTriangle);
                                   });
    
                                   $("input[name='sh"+id+"']").change(function(event) {
                                       if($(this).is(':checked')==true){ 
                                           bermudaTriangle.setMap(map);
                                         }else {
                                         bermudaTriangle.setMap(null);
                                       }
                                   });
                                   var cont=0;
                                   var lim;
                                   $("input[name='flick"+id+"']").change(function(event) {
                                       if($(this).is(':checked')==true){ 
                                             lim=setInterval(function (){
                                                   if(cont%2==0){
                                                     bermudaTriangle.setMap(null);
                                                   }else{
                                                     bermudaTriangle.setMap(map);
                                                   }
                                                   cont++;
                                               }
                                             ,1000);
                                         }else {
                                         clearInterval(lim);
                                           bermudaTriangle.setMap(map);
                                       }
                                   });
                                              
                           map.fitBounds(bounds);
                        }
                        if (data.figura_id==3) {
                                              
                             iconSelected = googleMarktxt 
                             + data.orden
                             + "|" 
                             + data.fondo
                             + "|" 
                             + markTextColor;

                             var locat=[];
                             locat=data.coord.split(",");
                             var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                             var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                             bounds.extend(pt);

                              var marker = new google.maps.Marker({
                                  position: myLatLng,
                                  map: map,
                                  draggable: true,
                                  animation: google.maps.Animation.DROP,
                                  icon: iconSelected,
                                  id:data.detalle,
                                  indice:ixobj,
                                  tipo:data.figura_id,
                                  title:"Proyecto: "+data.proyecto+"<br>"+"Capa: "+data.capa,
                                  id:data.id,
                                  detalle: data.detalle,
                                  idproyecto:idproyecto,
                                  capa_id:id,
                                  numero:data.orden,
                                  draggable:false
                                });
                                

                                infoWindow = new google.maps.InfoWindow; 
                                marker.addListener("click", function(event) {
                                        doPolyActionEdit(event,marker);
                                      });
                                $("input[name='sh"+id+"']").change(function(event) {
                                    if($(this).is(':checked')==true){ 
                                        marker.setMap(map);
                                      }else {
                                      marker.setMap(null);
                                    }
                                });
                                $("input[name='flick"+id+"']").change(function(event) {
                                    if($(this).is(':checked')==true){ 
                                        marker.setAnimation(google.maps.Animation.BOUNCE);
                                      }else {
                                        marker.setAnimation(null);
                                    }
                                });
                                map.fitBounds(bounds);
                        }
                        if (data.figura_id==1) {
                             var locat=[];
                             locat=data.coord.split(",");

                              var cityCircle = new google.maps.Circle({
                                  strokeColor: data.borde,
                                  strokeOpacity: 0.8,
                                  strokeWeight: data.grosorlinea,
                                  fillColor: data.fondo,
                                  fillOpacity: data.opacidad,
                                  map: map,
                                  center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                  radius: Number(data.dato),
                                  tipo:data.figura_id,
                                  title:"Proyecto: "+data.proyecto+"<br>"+"Capa: "+data.capa,
                                  id:data.id,
                                  detalle: data.detalle,
                                  idproyecto:idproyecto,
                                  capa_id:id
                                });

                                 bounds.union(cityCircle.getBounds());
                                 infoWindow = new google.maps.InfoWindow; 
                                 cityCircle.addListener("click", function(event) {
                                         doPolyActionEdit(event,cityCircle);
                                       });
                                 $("input[name='sh"+id+"']").change(function(event) {
                                     if($(this).is(':checked')==true){ 
                                       cityCircle.setMap(map);
                                       }else {
                                       cityCircle.setMap(null);
                                     }
                                 });
                                   var cont=0;
                                   var lim;
                                 $("input[name='flick"+id+"']").change(function(event) {
                                     if($(this).is(':checked')==true){ 
                                       lim=setInterval(function (){
                                             if(cont%2==0){
                                               cityCircle.setMap(null);
                                             }else{
                                               cityCircle.setMap(map);
                                             }
                                             cont++;
                                         }
                                       ,1000);
                                   }else {
                                     clearInterval(lim);
                                     cityCircle.setMap(map);
                                   }
                                 });
                            map.fitBounds(bounds);                  
                        }
                     });

                  }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       UpdateGeoPlan: function(id,idproyecto,tipo) {
          var tipozona='';
          var provision=averia=0;
          if (tipo==4) {
            tipozona=$("#slct_tipo_zona_edit_id").val();
            var actividad=$("#slct_actividad_edit").val();
            if(actividad!==null){
              if(actividad.includes("1")){
                averia=1;
              } 
              if(actividad.includes("2")){
                provision=1;
              }
            }
          }

           $.ajax({
               url         : 'geoproyectocapaelemento/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idelemento:id,
                              borde:'#' + $("#singlePolygonLine").val(),
                              grosorlinea:$("#singlePolygonStroke").val(),
                              fondo:'#'+ $("#singlePolygonFill").val(),
                              opacidad:$("#singlePolygonOpacity").val(),
                              detalle:$("#txt_detalle").val(),
                              tipo: tipo,
                              tipo_zona: tipozona,
                              averia: averia,
                              provision: provision
                            },
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se guardó exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        deleteUpdatePlanPoly: function(id,idproyecto) {
           $.ajax({
               url         : 'geoproyectocapaelemento/delete',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idelemento:id,
                              idproyecto:idproyecto
                            },
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se elimino exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        EliminarCapas: function(id) {
           $.ajax({
               url         : 'geoproyectocapa/delete',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idcapa:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se elimino exitosamente', 6000);
                     Proyectos.CargarCapas($("#txt_proyectoselect").val());
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        Updateproyect: function(idproyecto,valor,campo) {
          var datos={};
          datos['idproyecto']=idproyecto;
          if(campo==2){
            campo='estado_publico';
          } else if(campo==1){
            campo='estado';
          }
          datos[campo]=valor;

          if (idproyecto==null) {
             datos=$("#form_proyecto").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
          }

           $.ajax({
               url         : 'geoproyecto/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : datos,
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modifico exitosamente', 6000);
                     Proyectos.CargarProyectos();
                     $("#btn_close_modal").click();
                   } else {
                     Psi.mensaje('danger', obj.msj, 6000);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        EditarCapa: function(datos) {
           $.ajax({
               url         : 'geoproyectocapa/updatecapa',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : datos,
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modifico exitosamente', 6000);
                     $("#btn_close_modal2").click();
                     CargarCapas($("#txt_proyectoselect").val());
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        }, */

        MapearProyectos:function(id,idproyecto,map){
           $.ajax({
               url         : 'geoproyectocapa/mapear',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {capaid:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                  bounds = new google.maps.LatLngBounds();
               },
               success : function(obj) {
                  $(".overlay,.loading-img").remove();
                  if(obj.rst==1){
                     $.each(obj.datos, function(index, data) {
                      parame=[];
                        if (data.figura_id=='2' || data.figura_id==4) {

                            var triangleCoords=[];
                            var parame=[];

                            if(data.coord!==null) {
                              parame=data.coord.split("|");
                            } else {
                              parame=data.coordenadas.split("|");
                            }
                            for (var i =0; i < parame.length; i++) {
                                 var locat=[];
                                 locat=parame[i].split(",");
                                 triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                             }

                             var bermudaTriangle = new google.maps.Polygon({
                                       paths: triangleCoords,
                                       strokeColor: data.borde,
                                       strokeOpacity: 0.8,
                                       strokeWeight: data.grosorlinea,
                                       fillColor: data.fondo,
                                       fillOpacity: data.opacidad,
                                       id:data.proyecto+'['+data.capa+'] => '+data.detalle,
                                       title:data.id,
                                       tipo:2,
                                       idproyecto:idproyecto,
                                       capa_id:id
                                   });

                                   bermudaTriangle.setMap(map);

                                   bermudaTriangle.addListener("click", function(event) {
                                           doPolyAction(event,bermudaTriangle);
                                   });
                                              
                           map.fitBounds(bounds);
                        }
                        if (data.figura_id=='3') {
                                              
                             iconSelected = googleMarktxt 
                             + ''
                             + "|" 
                             + data.fondo
                             + "|" 
                             + markTextColor;

                             var locat=[];
                             locat=data.coord.split(",");
                             var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                             var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                             bounds.extend(pt);

                              var marker = new google.maps.Marker({
                                  position: myLatLng,
                                  map: map,
                                  draggable: true,
                                  animation: google.maps.Animation.DROP,
                                  icon: iconSelected,
                                  id:data.detalle,
                                  indice:ixobj,
                                  tipo:3,
                                  id:data.proyecto+'['+data.capa+'] => '+data.detalle,
                                  title:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id,
                                  numero:data.orden
                                });

                                marker.addListener("click", function(event) {
                                        doPolyAction(event,marker);
                                      });
                               

                            map.fitBounds(bounds);
                      }
                      if (data.figura_id=='1') {
                             var locat=[];
                             locat=data.coord.split(",");

                              var cityCircle = new google.maps.Circle({
                                  strokeColor: data.borde,
                                  strokeOpacity: 0.8,
                                  strokeWeight: data.grosorlinea,
                                  fillColor: data.fondo,
                                  fillOpacity: data.opacidad,
                                  map: map,
                                  center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                  radius: Number(data.dato),
                                  tipo:1,
                                  id:data.proyecto+'['+data.capa+'] => '+data.detalle,
                                  title:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id
                                });

                                 bounds.union(cityCircle.getBounds());
                                 
                                 cityCircle.addListener("click", function(event) {
                                         doPolyAction(event,cityCircle);
                                       });
                                
                            map.fitBounds(bounds);                   
                      }
                     });

                  }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
        },

       uploadkml: function(data,evento) {
           
           $.ajax({
                url: 'georeferencia/uploadkml',
                type: "POST",
                cache: false,
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   if(obj.upload==1){
                     Psi.mensaje('success', 'Se importó el archivo correctamente', 6000);
                     
                      evento(obj.file);
                     
                   } else {
                    Psi.mensaje('error', obj.msg, 6000);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove(); 
                   
               }
           });

        },

        FinalizarKml: function(file) {
           
           $.ajax({
                url: 'georeferencia/eliminarkml',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {file:file},
                beforeSend: function() {
                    //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
               success : function(obj) {

               },
               error: function(){
                   
               }
           });

        },
/*
        GuardarProyectoZona: function(origen,evento) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyecto/savezonas',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,nombre: $("#txt_proyecto_zona").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $("#txt_proyecto_zona").val('');
                            $("#txt_poligono").val('');
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                            Proyectos.CargarProyectos();
                            Proyectos.CargarCapas(obj.datos);
                            $("#txt_proyectoselect").val(obj.datos);
                            $("#btn_clean").click();
                        }
                        else{
                          var msj;
                          if(typeof obj.msj=='string') {
                            msj=obj.msj;
                          } else {
                            $.each(obj.msj,function(index,datos){
                              msj=datos;
                            });
                            msj=msj.join();
                          }
                          Psi.mensaje('danger', msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Poligonos');
            }
        },*/

    }
/*
var TablaDetalle={
    Campos:function(datos,evento){
        $.ajax({
            url         : 'tabladetalle/campos',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    }
} */
</script>