<script type="text/javascript">
var proyectos_id, proyectosObj;
var capas_id, capasObj;
var mapObjects = [];
var coordinatesObjects = [];
var detailcapa = [];
var subelementos=[];
var bounds;
var ixobj=0;
var markTextColor = textByColor("#" 
                    + $("#pickerMarkBack")
                    .val())
                    .substring(1);

  /*  itemChars = "&chld=" 
            + 1
            + "|" 
            + $("#pickerMarkBack").val() 
            + "|"
            + markTextColor;*/

var Proyectos = {
        BuscarElemento: function(elemento_id,origen) {
          
           $.ajax({
            url         : 'geodependencia/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {elemento_id:elemento_id,origen:origen},
            beforeSend : function() {
            },
            success : function(obj) {
                var _html="";
                var array=[];
                //$(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $.each(obj.datos,function(index,data){
                             _html+="<label>"+data.etiqueta+"</label>"
                             _html+="<div id='div_"+data.campo+"'><select class='form-control geo_item' name='slct_" + data.campo + "[]' id='slct_" + data.campo + "' "+
                                    " title=\""+data.campo+"\"  onchange=\"opcionSelected(this.title)\" > "+
                                     "<option value='0'>- Seleccione -</option>"+
                                    "</select></div>";
                             if (index==0) {
                                campo=data.campo
                             }
                             //$("#slct_"+data.campo).attr('onchange','Filtroslct()');
                             array[index]=data.campo;

                    });
                     $("#txt_orden").val(array.join("_"));
                     $("#geo_response").html(_html);
                     $("#slct_"+campo).attr('multiple','multiple');
                     slctGlobal.listarSlct('geo'+origen,'slct_'+campo,'multiple',null,'');

                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
        },

        CargarFiltro: function(origen,titulo,campo) {
           var datos=$("#form_proyectos").serialize().split("txt_").join("").split("slct_").join("");
           slctGlobal.listarSlctpost('geo'+origen,campo,'slct_'+campo,'multiple',null,datos);
        },

        TrazarCapaPoligono: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

            var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    var lati="";
                    var longi="";
                    if(obj.rst==1){ 
                      if(grosorlinea==0){grosorlinea=3;}
                      if(opacidad==''){opacidad='0.3';}
                        
                        $.each(obj.datos, function(index, dato) {
                              var triangleCoords=[];
                              var parame=[];
                              parame=dato.coord.split("|");
                              for (var i = 0; i < parame.length; i++) {
                                  var locat=[];
                                  locat=parame[i].split(",");
                                  triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                  var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                  bounds.extend(pt);
                              }
                              
                              var bermudaTriangle = new google.maps.Polygon({
                                                      paths: triangleCoords,
                                                      strokeColor: '#'+borde,
                                                      strokeOpacity: 0.8,
                                                      strokeWeight: grosorlinea,
                                                      fillColor: '#'+fondo,
                                                      fillOpacity: opacidad,
                                                      id:dato.detalle,
                                                      indice:ixobj,
                                                      tipo:2
                                                  });

                                mapObjects.push({"elemento": origen,"tipo":'2',"capa": capa,"subelemento":dato.detalle,"orden":'',
                                  "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":dato.tabla_detalle_id,
                                  "fondo":'#'+fondo,"opacidad":opacidad});

                                coordinatesObjects.push({"coordinates":triangleCoords,"tipo":'2'});

                                ixobj++;
                                bermudaTriangle.setMap(map);
                                infoWindow = new google.maps.InfoWindow; 

                                bermudaTriangle.addListener("click", function(event) {
                                        doPolyAction(event,this);
                                });
                            });
                            map.fitBounds(bounds);
                            //detailcapa.push({"elemento": origen, "tipo": 2,"capa": capa}); 
                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

        TrazarCapaPunto: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

            //var data = $("form#form_proyectos").serialize().split("txt_").join("").split("slct_").join("");
            var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                     $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                     bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    if(obj.rst==1){
                          var idpunto=0;
                          $.each(obj.datos, function(index, dato) {

                                idpunto++;

                                iconSelected = googleMarktxt 
                                  + idpunto 
                                  + "|" 
                                  + $("#pickerMarkBack").val() 
                                  + "|" 
                                  + markTextColor;

                                 var locat=[];
                                 locat=dato.coord.split(",");
                                 var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                            
                                 var marker = new google.maps.Marker({
                                     position: myLatLng,
                                     map: map,
                                     draggable: true,
                                     animation: google.maps.Animation.DROP,
                                     icon: iconSelected,
                                     id:dato.detalle,
                                     indice:ixobj,
                                     tipo:3,
                                     numero:idpunto
                                  });
                                 ixobj++;

                                  mapObjects.push({"elemento": origen, "tipo":'3',"capa": capa,"subelemento":dato.detalle,"orden":idpunto,
                                                   "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":dato.tabla_detalle_id,
                                                   "fondo":'#'+$("#pickerMarkBack").val(),"opacidad":opacidad});

                                  coordinatesObjects.push({"coordinates":marker,"tipo":'3'});

                                
                                  infoWindow = new google.maps.InfoWindow;
    
                                  marker.addListener("click", function(event) {
                                       doPolyAction(event,this);
                                  });

                        });

                        map.fitBounds(bounds);
                        //detailcapa.push({"elemento": origen, "tipo": 3,"capa": capa}); 

                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

         TrazarCapaCircle: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa) {

           var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    bounds = new google.maps.LatLngBounds();
                },
                success: function(obj) {
                    if(obj.rst==1){
                        if(grosorlinea==0){grosorlinea=3;}
                        if(opacidad==''){opacidad='0.3';}   
                        $.each(obj.datos, function(index, dato) {

                                 var locat=[];
                                 locat=dato.coord.split(",");
                                 var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                            
                                 var cityCircle = new google.maps.Circle({
                                    strokeColor: '#'+borde,
                                    strokeOpacity: 0.8,
                                    strokeWeight: grosorlinea,
                                    fillColor: '#'+fondo,
                                    fillOpacity: opacidad,
                                    map: map,
                                    center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                    radius: Number(dato.dato),
                                    id:dato.detalle,
                                    indice:ixobj,
                                    tipo:1
                                  });

                                 ixobj++;

                                 bounds.union(cityCircle.getBounds());

                                 mapObjects.push({"elemento": origen,"tipo":'1',"capa": capa,"subelemento":dato.detalle,"orden":'',
                                          "borde":'#'+borde,"grosorlinea":grosorlinea, "tabla_detalle_id":dato.tabla_detalle_id,
                                          "fondo":'#'+fondo,"opacidad":opacidad});

                                coordinatesObjects.push({"tipo":'1',"coordinates":{lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                                        "radius":Number(dato.dato)});
                                
                                  infoWindow = new google.maps.InfoWindow;

                                  cityCircle.addListener("click", function(event) {
                                       doPolyAction(event,this);
                                  }); 
                            });

                        map.fitBounds(bounds);

                       // detailcapa.push({"elemento": origen, "tipo": 1,"capa": capa});

                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        },

         GuardarProyecto: function(origen) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyecto/save',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,proyecto: $("#txt_proyecto").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $('#slct_elemento').val('0');
                            Proyectos.CargarProyectos();
                            $("#txt_proyecto").val('');
                            Proyectos.CargarCapas(obj.datos);
                            BuscarElemento();
                            $("#txt_proyectoselect").val(obj.datos);
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            Proyectos.LimpiarMapa();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        ModificarProyecto: function(origen) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapa/agregarcapa',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idproyecto: $("#txt_proyectoselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $('#slct_elemento').val('0');
                            Proyectos.CargarProyectos();
                            Proyectos.CargarCapas(obj.datos);
                            BuscarElemento();
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            Proyectos.LimpiarMapa();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        ModificarProyectoCapa: function(origen) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapaelemento/agregarelemento',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idcapa: $("#txt_capaselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $('#slct_elemento').val('0');
                            Proyectos.CargarProyectos();
                            Proyectos.CargarCapas($("#txt_proyectoselect").val());
                            BuscarElemento();
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            Proyectos.LimpiarMapa();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
        },

        LimpiarMapa: function() {
           $("#txt_proyecto").val('');
           $("#txt_capa").val('');
           if(mapObjects.length>0) {
             mapObjects.length=0;
             ixobj=0;
           }
        },

        ModificarPoligono: function(id,tipo) {

           if (tipo==2 || tipo==1 ) {
           mapObjects[id]['borde']='#' + $("#singlePolygonLine").val();
           mapObjects[id]['grosorlinea']=$("#singlePolygonStroke").val();
           mapObjects[id]['fondo']='#'+ $("#singlePolygonFill").val();
           mapObjects[id]['opacidad']= $("#singlePolygonOpacity").val();
          }
          if (tipo==3 ){
            mapObjects[id]['fondo']='#'+ $("#singlePolygonFill").val();
          }
          infoWindow.close(objMap);
          //alert(id);
          //alert(JSON.stringify(mapObjects[id]));
        },

        EliminarPoligono: function(id,tipo) {
            //mapObjects.splice(id,1);
            delete mapObjects[id]; // el id debe quedar solo vacia, para no afectar el array de otro poligono o punto
            coordinatesObjects.splice(id,1);
        },

        CargarProyectos:function(){
           $.ajax({
               url         : 'geoproyecto/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               beforeSend : function() {
                  
               },
               success : function(obj) {
                
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarProyectos(obj.datos);
                       proyectosObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       CargarCapas:function(id){
           $.ajax({
               url         : 'geoproyectocapa/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {id:id},
               beforeSend : function() {
                   $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarCapas(obj.datos);
                       capasObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       MapearCapas:function(id,idproyecto,map){
           $.ajax({
               url         : 'geoproyectocapa/mapear',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {capaid:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                  bounds = new google.maps.LatLngBounds();
               },
               success : function(obj) {
                  $(".overlay,.loading-img").remove();
                  if(obj.rst==1){
                     $.each(obj.datos, function(index, data) { 
                        if (data.figura_id=='2') {
                            var triangleCoords=[];
                            var parame=[];
                            parame=data.coord.split("|");
                            for (var i =0; i < parame.length; i++) {
                                 var locat=[];
                                 locat=parame[i].split(",");
                                 triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                             }

                             var bermudaTriangle = new google.maps.Polygon({
                                       paths: triangleCoords,
                                       strokeColor: data.borde,
                                       strokeOpacity: 0.8,
                                       strokeWeight: data.grosorlinea,
                                       fillColor: data.fondo,
                                       fillOpacity: data.opacidad,
                                       title:data.detalle,
                                       id:data.id,
                                       tipo:2,
                                       idproyecto:idproyecto,
                                       capa_id:id
                                   });

                                   bermudaTriangle.setMap(map);

                                   infoWindow = new google.maps.InfoWindow; 
                                   bermudaTriangle.addListener("click", function(event) {
                                           doPolyActionEdit(event,bermudaTriangle);
                                   });
    
                                   $("input[name='sh"+id+"']").change(function(event) {
                                       if($(this).is(':checked')==true){ 
                                           bermudaTriangle.setMap(map);
                                         }else {
                                         bermudaTriangle.setMap(null);
                                       }
                                   });
                                   var cont=0;
                                   var lim;
                                   $("input[name='flick"+id+"']").change(function(event) {
                                       if($(this).is(':checked')==true){ 
                                             lim=setInterval(function (){
                                                   if(cont%2==0){
                                                     bermudaTriangle.setMap(null);
                                                   }else{
                                                     bermudaTriangle.setMap(map);
                                                   }
                                                   cont++;
                                               }
                                             ,1000);
                                         }else {
                                         clearInterval(lim);
                                           bermudaTriangle.setMap(map);
                                       }
                                   });
                                              
                           map.fitBounds(bounds);
                        }
                        if (data.figura_id=='3') {
                                              
                             iconSelected = googleMarktxt 
                             + data.orden
                             + "|" 
                             + data.fondo
                             + "|" 
                             + markTextColor;

                             var locat=[];
                             locat=data.coord.split(",");
                             var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                             var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                             bounds.extend(pt);

                              var marker = new google.maps.Marker({
                                  position: myLatLng,
                                  map: map,
                                  draggable: true,
                                  animation: google.maps.Animation.DROP,
                                  icon: iconSelected,
                                  id:data.detalle,
                                  indice:ixobj,
                                  tipo:3,
                                  title:data.detalle,
                                  id:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id,
                                  numero:data.orden
                                });


                                infoWindow = new google.maps.InfoWindow; 
                                marker.addListener("click", function(event) {
                                        doPolyActionEdit(event,marker);
                                      });
                                $("input[name='sh"+id+"']").change(function(event) {
                                    if($(this).is(':checked')==true){ 
                                        marker.setMap(map);
                                      }else {
                                      marker.setMap(null);
                                    }
                                });
                                $("input[name='flick"+id+"']").change(function(event) {
                                    if($(this).is(':checked')==true){ 
                                        marker.setAnimation(google.maps.Animation.BOUNCE);
                                      }else {
                                        marker.setAnimation(null);
                                    }
                                });

                            map.fitBounds(bounds);
                      }
                      if (data.figura_id=='1') {
                             var locat=[];
                             locat=data.coord.split(",");

                              var cityCircle = new google.maps.Circle({
                                  strokeColor: data.borde,
                                  strokeOpacity: 0.8,
                                  strokeWeight: data.grosorlinea,
                                  fillColor: data.fondo,
                                  fillOpacity: data.opacidad,
                                  map: map,
                                  center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                  radius: Number(data.dato),
                                  tipo:1,
                                  title:data.detalle,
                                  id:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id
                                });

                                 bounds.union(cityCircle.getBounds());
                                 infoWindow = new google.maps.InfoWindow; 
                                 cityCircle.addListener("click", function(event) {
                                         doPolyActionEdit(event,cityCircle);
                                       });
                                 $("input[name='sh"+id+"']").change(function(event) {
                                     if($(this).is(':checked')==true){ 
                                       cityCircle.setMap(map);
                                       }else {
                                       cityCircle.setMap(null);
                                     }
                                 });
                                   var cont=0;
                                   var lim;
                                 $("input[name='flick"+id+"']").change(function(event) {
                                     if($(this).is(':checked')==true){ 
                                       lim=setInterval(function (){
                                             if(cont%2==0){
                                               cityCircle.setMap(null);
                                             }else{
                                               cityCircle.setMap(map);
                                             }
                                             cont++;
                                         }
                                       ,1000);
                                   }else {
                                     clearInterval(lim);
                                     cityCircle.setMap(map);
                                   }
                                 });
                            map.fitBounds(bounds);                   
                      }
                     });

                  }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

       UpdateGeoPlan: function(id,idproyecto) {
           $.ajax({
               url         : 'geoproyectocapaelemento/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idelemento:id,
                              borde:'#' + $("#singlePolygonLine").val(),
                              grosorlinea:$("#singlePolygonStroke").val(),
                              fondo:'#'+ $("#singlePolygonFill").val(),
                              opacidad:$("#singlePolygonOpacity").val(),
                              idproyecto:idproyecto
                            },
               beforeSend : function() {
                   
               },
               success : function(obj) {
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se guardó exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        deleteUpdatePlanPoly: function(id,idproyecto) {
           $.ajax({
               url         : 'geoproyectocapaelemento/delete',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idelemento:id,
                              idproyecto:idproyecto
                            },
               beforeSend : function() {
                   
               },
               success : function(obj) {
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se elimino exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        EliminarCapas: function(id) {
           $.ajax({
               url         : 'geoproyectocapa/delete',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idcapa:id},
               beforeSend : function() {

               },
               success : function(obj) {
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se elimino exitosamente', 6000);
                     Proyectos.CargarCapas($("#txt_proyectoselect").val());
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        Updateproyect: function(idproyecto,valor,campo) {
           $.ajax({
               url         : 'geoproyecto/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {idproyecto:idproyecto,valor:valor,campo:campo},
               beforeSend : function() {
                  
               },
               success : function(obj) {
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modifico exitosamente', 6000);
                     Proyectos.CargarProyectos();
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        EditarCapa: function(borde,fondo,grosorlinea,opacidad,capaid) {
           $.ajax({
               url         : 'geoproyectocapa/updatecapa',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {borde:borde,fondo:fondo,grosorlinea:grosorlinea,opacidad:opacidad,capaid:capaid},
               beforeSend : function() {
                  
               },
               success : function(obj) {
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modifico exitosamente', 6000);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

        },

        MapearProyectos:function(id,idproyecto,map){
           $.ajax({
               url         : 'geoproyectocapa/mapear',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {capaid:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                  bounds = new google.maps.LatLngBounds();
               },
               success : function(obj) {
                  $(".overlay,.loading-img").remove();
                  if(obj.rst==1){
                     $.each(obj.datos, function(index, data) { 
                        if (data.figura_id=='2') {
                            var triangleCoords=[];
                            var parame=[];
                            parame=data.coord.split("|");
                            for (var i =0; i < parame.length; i++) {
                                 var locat=[];
                                 locat=parame[i].split(",");
                                 triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
                                 var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                                 bounds.extend(pt);
                             }

                             var bermudaTriangle = new google.maps.Polygon({
                                       paths: triangleCoords,
                                       strokeColor: data.borde,
                                       strokeOpacity: 0.8,
                                       strokeWeight: data.grosorlinea,
                                       fillColor: data.fondo,
                                       fillOpacity: data.opacidad,
                                       title:data.detalle,
                                       id:data.id,
                                       tipo:2,
                                       idproyecto:idproyecto,
                                       capa_id:id
                                   });

                                   bermudaTriangle.setMap(map);

                                   bermudaTriangle.addListener("click", function(event) {
                                           doPolyAction(event,bermudaTriangle);
                                   });
                                              
                           map.fitBounds(bounds);
                        }
                        if (data.figura_id=='3') {
                                              
                             iconSelected = googleMarktxt 
                             + data.orden
                             + "|" 
                             + data.fondo
                             + "|" 
                             + markTextColor;

                             var locat=[];
                             locat=data.coord.split(",");
                             var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                             var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
                             bounds.extend(pt);

                              var marker = new google.maps.Marker({
                                  position: myLatLng,
                                  map: map,
                                  draggable: true,
                                  animation: google.maps.Animation.DROP,
                                  icon: iconSelected,
                                  id:data.detalle,
                                  indice:ixobj,
                                  tipo:3,
                                  title:data.detalle,
                                  id:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id,
                                  numero:data.orden
                                });

                                marker.addListener("click", function(event) {
                                        doPolyAction(event,marker);
                                      });
                               

                            map.fitBounds(bounds);
                      }
                      if (data.figura_id=='1') {
                             var locat=[];
                             locat=data.coord.split(",");

                              var cityCircle = new google.maps.Circle({
                                  strokeColor: data.borde,
                                  strokeOpacity: 0.8,
                                  strokeWeight: data.grosorlinea,
                                  fillColor: data.fondo,
                                  fillOpacity: data.opacidad,
                                  map: map,
                                  center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
                                  radius: Number(data.dato),
                                  tipo:1,
                                  title:data.detalle,
                                  id:data.id,
                                  idproyecto:idproyecto,
                                  capa_id:id
                                });

                                 bounds.union(cityCircle.getBounds());
                                 
                                 cityCircle.addListener("click", function(event) {
                                         doPolyAction(event,cityCircle);
                                       });
                                
                            map.fitBounds(bounds);                   
                      }
                     });

                  }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
       },

    }

var TablaDetalle={
    Campos:function(datos,evento){
        $.ajax({
            url         : 'tabladetalle/campos',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    }
}
</script>