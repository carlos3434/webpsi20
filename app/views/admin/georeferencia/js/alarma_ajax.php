<script type="text/javascript">
 
    var Alarma =
            {
                listar: (function () {
                    $.ajax({
                        url: 'geoalarmatapcolor/listar',
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
//                        beforeSend: function () {
//                            eventoCargaMostrar();
//                        },
                        success: function (obj) {
                            var contenido = '';
                            if (obj.rst == 1) {
                                $.each(obj.datos, function (val, text) {
                                    contenido += '<li class="list-group-item"><i class="fa fa-circle" '
                                            + 'style="color:' + text.color + '"></i> '
                                            + text.significa + '</li>';
                                });
                            }
                            $('.alarmatap-leyenda').html(contenido);
                            eventoCargaRemover();
                        },
                        error: function () {
                            $('.alarmatap-leyenda').html('<li class="list-group-item"><?php echo trans('main.no_carga') ?></li>');
                            eventoCargaRemover();
                        }
                    });

                }),
                listarCoordenadas: function () {
                    $.ajax({
                        type: "POST",
                        url: "zonal/listar-coordenada",
                        dataType: 'json',
                        error: function (data) {
                            console.log(data);
                            eventoCargaRemover();
                        },
                        success: function (datos) {
                            $.each(datos, function () {
                                defZonalXY[this.abreviatura] = new google.maps.LatLng(this.coordx, this.coordy);
                            });
                        } // -- end.success
                    });
                },
            };

</script>