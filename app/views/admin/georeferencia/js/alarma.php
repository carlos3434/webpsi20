<script type="text/javascript">
    /**
     * Nueva objeto tipo mapa
     * @type google.maps.Map
     */
    var map;
    /**
     * Limites del mapa
     * @type google.maps.LatLngBounds
     */
    var bounds;
    var numAlertaTroba = 0;
    /**
     * Direcciones
     */
    var tapBounds = new google.maps.LatLngBounds();
    var tapMarkers = [];
    var tapIntervId;
    var blinkAlert = [];
    //Marcadores
    var markers = [];
    var markerArray = [];
    //Poligonos
    var polygonObject;
    var polygonCoords = [];
    var polygonArray = [];
    //Address
    var addressArray = [];
    var addressMarkerArray = [];
    /**
     * Default XY
     */
    var defZonalXY = {
        ARE: new google.maps.LatLng(-16.4040523, -71.5390114),
        CHB: new google.maps.LatLng(-9.0851792, -78.5801855),
        CHY: new google.maps.LatLng(-6.7815563, -79.8490405),
        CUZ: new google.maps.LatLng(-13.5300193, -71.939249),
        HYO: new google.maps.LatLng(-12.0485093, -75.2026391),
        ICA: new google.maps.LatLng(-14.0837542, -75.7456576),
        IQU: new google.maps.LatLng(-3.7529196, -73.2833147),
        LIM: new google.maps.LatLng(-12.099119, -77.028895),
        PIU: new google.maps.LatLng(-5.1930857, -80.6668063),
        TAC: new google.maps.LatLng(-18.0228769, -70.247547),
        TRU: new google.maps.LatLng(-8.1167523, -79.0196156)
    };
    /**
     * Objeto geocoder, libreria Google Maps
     * @type type
     */
    var geocoder;
    /**
     * Objeto tipo infowindow
     * @type type
     */
    var infowindow;
    /**
     * z-index
     * @type Number
     */
    var zIndexItem = 0;

    /**
     * Realiza una acción de acuerdo al nivel de zoom
     * @param {type} zoom
     * @returns {Boolean}
     */
    onZoomDo = function (zoom) {
        return true;
    };
    trobaColor = function () {
        polygonCoords = [];
        bounds = new google.maps.LatLngBounds();
        $.ajax({
            type: "POST",
            url: "geoalarmatap/alarma-trobas",
            data: "zonal=" + $("#slct_zonal").val(),
            dataType: 'json',
            error: function (data) {
                console.log(data);
            },
            success: function (obj) {
                $('.alarmatap-lista').html('');
                //Si no hay datos, mostrar centro por defecto
                if ((obj.datos).length == 0) {
                    map.setCenter(defZonalXY[$("#slct_zonal").val()]);
                    map.setZoom(13);
                } else if (obj.rst == 1) {
                    var data = obj.datos;
                    $.each(data, function () {
                        var nc = this.n;
                        var zonal = this.zonal;
                        var nodo = this.nodo;
                        var blink = this.blink;
                        var troba = this.troba;
                        var alarma = this.alarma;
                        var color = this.color;
                        var opacidad = this.opacidad;
                        if (blink == 1)
                        {
                            var myObj = {id: zonal + nodo + troba, poly: null};
                            blinkAlert.push(myObj);
                        }
                        var fillColor = color;
                        var strokeCollor = "#1E7509";

                        $('.alarmatap-lista').append(
                                "<li class='list-group-item' style=\"background-color: "
                                + fillColor
                                + "\"><a href=\"javascript:void(0)\" onclick=\"listaClientes('" + zonal + "', '" + nodo + "', '" + troba + "');\">"
                                + nodo
                                + " | "
                                + troba
                                + "</a></li>"
                                );
                        $.ajax({
                            type: "POST",
                            url: "geoalarmatap/alarma-trobas-area",
                            data: "zonal=" + this.zonal
                                    + "&nodo=" + this.nodo
                                    + "&troba=" + this.troba,
                            dataType: 'json',
                            error: function (data) {
                                console.log(data);
                            },
                            success: function (obj) {
                                if (obj.rst = 1 && obj.datos != null) {
                                    var datos = obj.datos;
//                                  idCount = 1;
                                    var aCoordenadas = (datos.coord).split("|");
                                    $.each(aCoordenadas, function (index, value) { // datos
//                                        if (idCount === 1) { idFirst = value.id; } 
//                                        else { idLast = value.id; }
                                        cc = value.split(",");
                                        var pt = new google.maps.LatLng(cc[1], cc[0]);
//                                      var pt = new google.maps.LatLng(value.coord_y, value.coord_x);
                                        bounds.extend(pt);
                                        polygonCoords.push(pt);
//                                      idCount++;
                                    });
                                    // Construct the polygon.                        
                                    polygon = new google.maps.Polygon({
                                        paths: polygonCoords,
                                        strokeColor: strokeCollor,
                                        strokeOpacity: 0.8,
                                        strokeWeight: 1.2,
                                        fillColor: fillColor,
                                        fillOpacity: opacidad,
                                        zIndex: zIndexItem++,
                                        map: map
                                    });
                                    $.each(blinkAlert, function (idObject, myObject) {
                                        if (myObject.id == zonal + nodo + troba)
                                        {
                                            myObject.poly = polygon;
                                        }
                                    });
                                    google.maps.event.addListener(polygon, "click", function () {
                                        // polygonDo(this, event, value, true);
                                        listaClientes(zonal, nodo, troba);
                                    });
                                    // Muestra poligono en mapa
                                    polygon.setMap(map);
                                    // Si no hay datos, mostrar centro por defecto
                                    if (datos.length == 0) {
                                        map.setCenter(defZonalXY[$("#slct_zonal").val()]);
                                        map.setZoom(13);
                                    } else {
                                        // Recalcula visibilidad de mapa por limites coord.
                                        map.fitBounds(bounds);
                                    }
                                    polygon = null;
                                    polygonCoords = [];
                                }
                            }
                        });
                    });
                }
                //blink alertados
                setInterval(doBlinkAlert(), 1000);
            }
        });
    };
    doBlinkAlert = function () {
        if (blinkAlert.length > 0)
        {
            $.each(blinkAlert, function (id, obj) {
                if (typeof this === 'object')
                {
                    var element = obj.poly;
                    if (typeof element === 'object')
                    {
                        var d = new Date();
                        var n = d.getSeconds();
                        if (n % 2 === 0) {
                            element.setMap(null);
                        } else {
                            element.setMap(map);
                        }
                    }
                }
            });
        }
    };
    listaClientes = function (zonal, nodo, troba) {
        if (tapMarkers.length > 0)
        {
            $.each(tapMarkers, function (id, val) {
                val.setMap(null);
            });
            tapMarkers = [];
            tapBounds = new google.maps.LatLngBounds();
        }

        $.ajax({
            type: "POST",
            url: "geoalarmatap/tap-clientes",
            data: "zonal=" + zonal
                    + "&nodo=" + nodo
                    + "&troba=" + troba,
            dataType: 'json',
            error: function (data) {
                console.log(data);
            },
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (datos) {
                var contenido = '';
                var subtitulo =
                        zonal
                        + " | "
                        + nodo
                        + " | "
                        + troba
                        + "&nbsp;&nbsp;<a class='label label-primary' " 
                        + "href=\"http://www.movistar1.com:9571/cmts/alertas_down.php?nodo=" + nodo + "&troba=" + troba + "\" target=\"_blank\">Descarga</a>"
                        + "";
                $('.alarmatap-detalle-titulo').html(subtitulo);
                //Mostrar Taps
                $.each(datos.tap, function () {
                    contenido += "<li class='list-group-item'>"
                            + this.codcli
                            + " / "
                            + this.nombres
                            + " / "
                            + this.ipaddress
                            + "</li>";
                    if (this.coord_x !== null && this.coord_y !== null)
                    {
                        var amp = this.amplificador;
                        var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
                        var tap = this.tap;
                        tapBounds.extend(pt);
                        var marker = new google.maps.Marker({
                            position: pt,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            icon: "img/georeferencia/Tap.ico"
                        });
                        google.maps.event.addListener(marker, "click", function (event) {
                            infowindow.setPosition(pt);
                            infowindow.setContent(
                                    "Zonal: " + zonal
                                    + "<br>"
                                    + "Nodo: " + nodo
                                    + "<br>"
                                    + "Troba: " + troba
                                    + "<br>"
                                    + "Amp.: " + amp
                                    + "<br>"
                                    + "Tap: " + tap
                                    );
                            infowindow.open(self.map);
                        });
                        tapMarkers.push(marker);
                    }
                });
                //Mostrar Amplificadores
                $.each(datos.amp, function () {
                    if (this.coord_x !== null && this.coord_y !== null) {
//                    if (this.coord !== null) {
//                        cc = (this.coord).split(",");
//                        var pt = new google.maps.LatLng(cc[1], cc[0]);
                        dd = (this.detalle).split("|");
                        var amp = dd[3];
                        var pt = new google.maps.LatLng(this.coord_y, this.coord_x);
//                      var amp = this.amplificador;
                        tapBounds.extend(pt);
                        var marker = new google.maps.Marker({
                            position: pt,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            icon: "img/georeferencia/Amplificador.ico"
                        });
                        google.maps.event.addListener(marker, "click", function (event) {
                            infowindow.setPosition(pt);
                            infowindow.setContent(
                                    "Zonal: " + zonal
                                    + "<br>"
                                    + "Nodo: " + nodo
                                    + "<br>"
                                    + "Troba: " + troba
                                    + "<br>"
                                    + "Amp.: " + amp
                                    );
                            infowindow.open(self.map);
                        });
                        tapMarkers.push(marker);
                    }
                });
                //$(".alarmatap-detalle").html('');
                $('.alarmatap-detalle').html('').append(contenido);
                //infowindow.setPosition(event.latLng);
                //infowindow.setContent(contenido);
                //infowindow.open(self.map);
                map.fitBounds(tapBounds);
                eventoCargaRemover();
            } // -- end.success
        });
    };
    tapIntervalUpdate = function () {
        $('#chk_mostrar').iCheck('uncheck');
        //$("#chk_mostrar").prop("checked", false);
        //$("#chk_mostrar").parent('div.icheckbox_minimal').removeClass('checked');
        tapMarkers = [];
        tapBounds = new google.maps.LatLngBounds();
        tapMap();
        //trobaColor();
        $('#chk_mostrar').iCheck('check');
        //$("#chk_mostrar").click();
    };
    tapMap = function () {
        bounds = new google.maps.LatLngBounds();
        map = Psigeo.map('mymap');
        infowindow = Psigeo.infowindow;
//        var mapOptions = {
//            zoom: 10,
//                    center: new google.maps.LatLng( - 12.033284, - 77.0715493)
//            };
//        map = new google.maps.Map(document.getElementById('geo_map'),
//                    mapOptions);
//        infowindow = new google.maps.InfoWindow({
//            content: "Loading..."
//        });  
        // Zoom 
        google.maps.event.addListener(map, 'zoom_changed', function () {
            onZoomDo(map.getZoom());
        });
        // Geocoder
        geocoder = new google.maps.Geocoder();
        // Estilos del mapa, carreteras, agua, bloques, etc.
//        var styles = [
//            {
//                stylers: [
//                    {hue: "#F08FD9"},
//                    {saturation: -5}
//                ]
//            }
//            , {
//                featureType: "water",
//                elementType: "geometry",
//                stylers: [
//                    {color: "#b1b0f7"}
//                ]
//            }
//        ];
        // map.setOptions({styles: styles});
        return true;
    };

    $(document).ready(function () {
        eventoCargaMostrar();
        Alarma.listarCoordenadas();
        // ocultar menu lateral
        $("[data-toggle='offcanvas']").click();
        // mapa
        tapMap();
        // zonal
        slctGlobal.listarSlct('zonal', 'slct_zonal', 'simple', null, {abreviatura: 1});
        /* cambio de Zonal */
        $('#slct_zonal').change(function () {
            $('#chk_mostrar').iCheck('check');
            //$("#chk_mostrar").prop('checked', true);
            //$("#chk_mostrar").parent('div.icheckbox_minimal').addClass('checked');
            trobaColor();
        });
        /* clic enviar zona */
        $('.alarmatap-limpiar').click(function () {
            var conf = confirm("Se eliminaran todos los elementos creados\n"
                    + "¿Desea continuar?");
            if (conf) {
                //Limpiar limites del mapa
                bounds = new google.maps.LatLngBounds();
                //Limpiar grupos creados
                layerGroup = new Array();
                //Inicializar mapa
                tapMap();
            }
        });
        // interval
        $('#form_alarmatap input').on('ifChecked', function (event) {
            var chk_id = $(this).attr('id');
            console.log(' activar ' + chk_id);
            if ('chk_actualizar' == chk_id) {
                tapIntervId = setInterval(tapIntervalUpdate, 60000);
            } else if ('chk_mostrar' == chk_id) {
                trobaColor();
            }
        });
        $('#form_alarmatap input').on('ifUnchecked', function (event) {
            var chk_id = $(this).attr('id');
            console.log(' desactivar ' + chk_id);
            if ('chk_actualizar' == chk_id) {
                clearInterval(tapIntervId);
            }
        });
        /*        
         //Interval
         $('#chk_actualizar').click(function () {
         if ($(this).prop("checked") === true){ tapIntervId = setInterval(tapIntervalUpdate, 60000);} 
         else { clearInterval(tapIntervId); }
         });
         // clic enviar zona
         $('#chk_mostrar').on('click', function (e) {
         if ($(this).is(":checked")) { trobaColor();}
         });
         */
    });

    $(window).load(function () {
        Alarma.listar();
    });
</script>
