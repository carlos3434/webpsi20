<script type="text/javascript">
    var googleMarktxt = "http://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld=";
    var temporalBandeja=0;
    var table=0;
    var addressContent = Array();
    var mapTools = {};
    $(document).ready(function() {
        /*
        // Show sidebar
        function showSidebar() {
            objMain.addClass('use-sidebar');
            Psigeo.mapResize(proymap);
        }

        // Hide sidebar
        function hideSidebar() {
            objMain.removeClass('use-sidebar');
            Psigeo.mapResize(proymap);
        }

        // Sidebar separator
        var objSeparator = $('#separator');

        objSeparator.click(function(e) {
            e.preventDefault();
            if (objMain.hasClass('use-sidebar')) {
                hideSidebar();
            }
            else {
                showSidebar();
            }
        }).css('height', objSeparator.parent().outerHeight() + 'px');
        */
        activarTabla();

        $("[data-toggle='offcanvas']").click();
        
        var proymap = Psigeo.map('mymap');
        
        // Variables
        var objMain = $('#main');
        
        slctGlobal.listarSlctFijo('geotabla','slct_elemento');

        Proyectos.CargarProyectos();
        Proyectos.CargarCapas('');
        $("#pickerMarkBack").val('F7584C');
        $("#pickerPolyBack").val('F7584C');
        $("#pickerPolyLine").val('F7584C');
        
        $('#myTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
          //cuadno hace click en lista actualizar los datos , si hace click en tab TABLA reiniar filtros
        });

          //opacidad del poligono
        $( "#sliderPolyOpac" ).slider({
           value:0,
           min: 0,
           max: 0.99,
           step: 0.01,
           slide: function( event, ui ) {
               $("#" + $(this).attr("title")).css("opacity", ui.value);
           }
        });

        $( "#sliderPolyOpac_edit" ).slider({
              value:0,
              min: 0,
              max: 0.99,
              step: 0.01,
              slide: function( event, ui ) {
                  $("#" + $(this).attr("title")).css("opacity", ui.value);
              }
          });

        //Linea de poligono
        $("#polyLineCreate").change(function (){
            $("#polyDemoCreateborder")
                .css("border-width", $( this ).val() + "px" );
        });
        $("#polyLineCreate_edit").change(function (){
            $("#polyDemoCreateborder_edit")
                .css("border-width", $( this ).val() + "px" );
        });

             //Marcador
        $("#pickerMarkBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              // Fill the text box just if the color was set using the picker, 
                          // and not the colpickSetColor function.
              if(!bySetColor) $(el).val(hex);
                          
                          $(".newMarkerBackground").css(
                                  "background-color", 
                                  "#" + $("#pickerMarkBack").val()
                              );
            }
            }).keyup(function(){
             $(this).colpickSetColor(this.value);
         });
          
          $("#pickerMarkBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, 
                            // and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
                            
                            $(".newMarkerBackground").css(
                                    "background-color", 
                                    "#" + $("#pickerMarkBack_edit").val()
                                );
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
          });

            //Color picker control
          $("#pickerPolyBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              // Fill the text box just if the color was set using the picker, 
                          // and not the colpickSetColor function.
              if(!bySetColor) $(el).val(hex);
                          $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
            });
    
            $("#pickerPolyBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, 
                            // and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
                            $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
            });

        $("#pickerPolyLine").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              // Fill the text box just if the color was set using the picker, 
                          // and not the colpickSetColor function.
              if(!bySetColor) $(el).val(hex);
                         // $( "#" + $(el).attr("title") + "Mapx" ).css("border-color", '#'+hex);
                          $("#polyDemoCreateborder").css("border-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
        });
    
        $("#pickerPolyLine_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, 
                            // and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
                            //$( "#" + $(el).attr("title") + "Mapx_edit" ).css("border-color", '#'+hex);
                            $("#polyDemoCreateborder_edit").css("border-color", '#'+hex);
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
          });

          //objMap = doObjMap("mymap", objMapProps, mapTools);
          /*  objMap = new google.maps.Map(document.getElementById('mymap'), {
                zoom: 5,
                center: {lat:-12.0595958, lng: -77.0453983}
            });*/

            //Iniciar mapa
           objMap = doObjMap("mymap", objMapProps, mapTools);


           $("#btn_clean, #btn_cleaned").click(function() {
              var rs=confirm("Se eliminaran todos los elementos creados ¿Desea continuar?");
              if(rs){
                  Proyectos.LimpiarMapa();
                  objMap = doObjMap("mymap", objMapProps, mapTools);
              }
          });

           $('#proyectoModal').on('show.bs.modal', function (event) {

              var button = $(event.relatedTarget); // captura al boton
              var titulo = button.data('titulo'); // extrae del atributo data-
              capas_id = button.data('id'); //extrae el id del atributo data
              // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
              // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
              var modal = $(this); //captura el modal
              modal.find('.modal-title').text(titulo+' Capa');
              $('#form_proyecto [data-toggle="tooltip"]').css("display","none");
      //        $("#form_eventos input[type='hidden']").remove();
                 
              modal.find('.modal-footer .btn-primary').text('Actualizar');

              $('#form_proyecto #txt_nombre').val( capasObj[capas_id].nombre);
              if (capasObj[capas_id].tipo=='3') {
                $("#estilo_poligono_edit").hide();
                $("#estilo_punto_edit").show();
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPunto();');
              } else {
                $("#estilo_poligono_edit").show();
                $("#estilo_punto_edit").hide();
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPoligono();');
              }

              $("#form_proyecto").append("<input type='hidden' value='"+
                    capasObj[capas_id].id+"' name='id' id='id'>");


          });
          
      
          $('#proyectoModal').on('hide.bs.modal', function (event) {
              var modal = $(this);
              //modal.find('.modal-body input').val('');
               $('#form_proyecto #id').remove();
          });


        $("#btn_kml").click(function(){
            var polygons = Array();
            var markers = Array();

            if(coordinatesObjects.length > 0) {
                for (var i in mapObjects) {
                    if (mapObjects[i].tipo == "2") {
                        polygons.push(coordinatesObjects[i]);
                    } else if (mapObjects[i].tipo == "3") {
                        addressContent.push(coordinatesObjects[i].subelemento);
                        markers.push(coordinatesObjects[i].coordinates);
                    }
                }
                if (polygons.length > 0) {
                    Map.preparePolygon(polygons);
                }
                if (markers.length > 0) {
                    Map.generateKML(markers);
                }
                //Map.preparePolygon(mapObjects);
            }else{
                Psi.mensaje('danger', 'No hay ningun objeto pintado.', 6000);
            }
        });

        $("#btn_kml_upload").click(function(){
            Map.uploadKML(objMap,"form_kml");
        });

        $('html').on('click', function(e) {
            if (typeof $(e.target).data('original-title') == 'undefined' &&
                !$(e.target).parents().is('.popover.in')) {
                $('[data-original-title]').popover('hide');
            }
        });

         $("#agregar").click(function() {
              var pos=$("#divelemento").position()
              $('html, body').animate({scrollTop: pos.top}, 1000);
              $('#divaccion').html('Agregar Capa(s)');
              Proyectos.LimpiarMapa();
              //objMap = doObjMap("mymap", objMapProps, mapTools);
              $('#div_pro').html(
                  '<div class="form-group">'+
                     '<div class="col-sm-12"><label>&nbsp;</label>'+
                     '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success" onClick="AgregarCapa()">Actualizar Proyecto</button>'+
                     '</div></div>');
              $('#slct_elemento').val('0');
              BuscarElemento();
              $('#div_cancel').show();
          });

     });
    
    function cancelarActualzc() {
          $('#divaccion').html('Elementos');
          Proyectos.LimpiarMapa();
          //objMap = doObjMap("mymap", objMapProps, mapTools);
          $('#div_pro').html(
              '<div class="form-group"><div class="col-sm-5">'+
                '<label>Proyecto</label>'+
                '<input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">'+
                '</div><div class="col-sm-7">'+
                '<label>&nbsp;</label>'+
                '<button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()">Guardar Proyecto</button>'+
                '</div></div>');
          $('#div_capa').html('<div class="form-group">'+
                            '<div class="col-sm-5"><label>Capa</label>'+
                             '<input id="txt_capa" class="form-control" type="text" value="" name="txt_capa" placeholder="Ingrese Capa">'+
                             '</div><div class="col-sm-7"><label>&nbsp;</label>'+
                             '<button type="button" id="btn_capa" class="form-control btn btn-primary"  onClick="GenerarCapa()">Generar Capa</button>'+
                             '</div></div>');
          $('#slct_elemento').val('0');
          BuscarElemento();
          $('#div_cancel').hide();
    }
    function AgregarCapa() {
        var rs=confirm("Se agregaran al Proyecto las capas creadas ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyecto('');
              }
    }
    
    function GuardarProyecto(h) {
        if ($("#txt_proyecto").val()!='') {
                var rs=confirm("Se guardaran las capas creadas ¿Desea Continuar?");
                if(rs){
                  geoVal = $("#slct_elemento").val().split("_");
                  Proyectos.GuardarProyecto(geoVal[2]);
                }
             } else {
               alert('Indique nombre de proyecto');
             }
    }

    function AgregarElemento(capaid) {
        var pos=$("#divelemento").position()
        $('html, body').animate({scrollTop: pos.top}, 1000);
        $('#divaccion').html('Agregar Elemento(s)');
         $('#txt_capaselect').val(capaid);
        Proyectos.LimpiarMapa();
        //objMap = doObjMap("mymap", objMapProps, mapTools);
        $('#div_capa').html('');
        $('#div_pro').html('<div class="col-sm-6">'+
                            ' <label>&nbsp;</label>'+
                            '<button id="btn_addelemento" type="button" class="form-control btn btn-primary" '+
                            'onClick="GenerarCapaElemento()">Generar Capa</button></div>'+
            '<div class="form-group">'+
                '<div class="col-sm-6"><label>&nbsp;</label>'+
                '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success" onClick="AgregarElementoCapa()">Actualizar Capa</button>'+
                '</div></div>');
        $('#slct_elemento').val('0');
        BuscarElemento();
        $('#div_cancel').show();
    }

    function GenerarCapaElemento() {
        if($("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
              geoVal = $("#slct_elemento").val().split("_");

              if(geoVal[1] == 3){
                      Proyectos.TrazarCapaPunto(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          '',
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );
              } 
              if(geoVal[1] == 2) {
                      polyDemoCreate
                      Proyectos.TrazarCapaPoligono(objMap,
                                          geoVal[0],
                                          geoVal[2],
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );

              }
              if(geoVal[1] == 1) {
                      polyDemoCreate
                      Proyectos.TrazarCapaCircle(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );

              }
          }  else {
              alert('Seleccione almenos un elemento');
          }
    }

    function AgregarElementoCapa() {
        var rs=confirm("Se agregara(n) a la Capa el(los) elemento(s) creado(s) ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyectoCapa('');
              }
    }

    function GenerarCapa() {
        if($("#txt_capa").val()!='' && $("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
              //ordenVal = $("#txt_orden").val().split("_");
              geoVal = $("#slct_elemento").val().split("_");

              if(geoVal[1] == 3){
                      Proyectos.TrazarCapaPunto(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          '',
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );
              } 
              if(geoVal[1] == 2) {
                      polyDemoCreate
                      Proyectos.TrazarCapaPoligono(objMap,
                                          geoVal[0],
                                          geoVal[2],
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );

              }
              if(geoVal[1] == 1) {
                      polyDemoCreate
                      Proyectos.TrazarCapaCircle(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );

              }
            } else {
              if($("#"+$("#txt_selfinal").val()).val()==null){
                alert('Seleccione el ultimo Subelemento');
              }
              else {
              alert('Indique nombre de capa');
              }
            }
    }

    /**
    * Convierte un valor hexadecimal al sistema decimal
    * @param {type} h
    * @returns {unresolved}
    */
    function hexdec(h) {
        h = h.toUpperCase();
        return parseInt(h, 16);
    }
    
    /**
    * Retorna color de texto de acuerdo al color de fondo
    * Color de texto: Blanco o negro.
    * Recibe color en hexadecimal con '#'
    * @param {type} color
    * @returns {String}
    */

    function textByColor(color) {
        color = color.substring(1);
    
        var c_r = hexdec(color.substring(0, 2));
        var c_g = hexdec(color.substring(2, 4));
        var c_b = hexdec(color.substring(4, 6));
    
        var bg = ((c_r * 299) + (c_g * 587) + (c_b * 114)) / 1000;
        if (bg > 130) {
            return "#000000";
        } else {
            return "#FFFFFF";
        }
    }
    
    function BuscarElemento(){

        //var dependencia = "";
        geoVal = $("#slct_elemento").val().split("_");
        //var origen = geoVal[2]; // mdf,provincia, troba, .. elementos

        //Ocultar o mostrar bloque de estilos
        $(".estiloItem").hide();
        if (geoVal[1] == 2 || geoVal[1] == 1) {
            $("#estilo_poligono").show();
        } else if (geoVal[1] == 3) {
            $("#estilo_punto").show();
        }

        /*
        //&elemento_id=" + geoVal[0] + "&origen=" + origen;
        $("#geo_response").html('');

        $("#div_capa").hide();
        $("#div_pro").hide();

        if ($("#slct_elemento").val()!=0) {
          Proyectos.BuscarElemento(geoVal[0],origen);
        }*/
        if($("#slct_elemento").val()==0){
          $("#selects").html('');
          $("#div_capa").hide();
          $("#div_pro").hide();
        } else {
          table=geoVal[0];
          var data = {tabla_id:table};
          TablaDetalle.Campos(data,CargarHTML);
        }
    }

    function opcionSelected(titulo){
      /*geoVal = $("#slct_elemento").val().split("_");
      ordenVal = $("#txt_orden").val().split("_");
      var html="";
      for(var i=0;i<ordenVal.length; i++){
        if(ordenVal[i]==titulo && (i+1)<ordenVal.length){
          var campo=ordenVal[i+1];
          $("#div_"+campo).html('');
          html+="<select class='form-control geo_item' name='slct_" + campo + "[]' id='slct_" + campo + "' "+
                                    " title=\""+campo+"\"  onchange=\"opcionSelected(this.title)\" > "+
                                     "<option value='0'>- Seleccione -</option>"+
                                    "</select>";
          $("#div_"+campo).html(html);
          $("#slct_"+campo).attr('multiple','multiple');
          Proyectos.CargarFiltro(geoVal[2],titulo,campo);
          $("#div_pro").hide();
          $("#div_capa").hide();
        }
        if(ordenVal[ordenVal.length-1]==titulo){
            $("#div_capa").show();
            $("#div_pro").show();
        }
      } */
       if($('#slct_c'+titulo).val()==null){
         $("#div_capa").hide();
         $("#div_pro").hide();
       }
       else {
          $("#div_capa").show();
          $("#div_pro").show();
          $("#txt_selfinal").val(titulo);
       } 
    }

     function doPolyAction(event, polygon) {

         value='';
        var nPoliActu = 0;
        var pos;
        var markerColor = '';

        var img_coord = "";
        polyActu = [];
        tmpObject = {};
        globalElement = polygon;
        tmpActu=[];
        infocontent='';

        var op = "<select id=\"singlePolygonOpacity\">";
        for (var i = 0; i <= 1; ) {
            var sel = "";
            if (Number(polygon.fillOpacity).toFixed(1) == i.toFixed(1)) {
                sel = "selected";
            }
            op += "<option value=\"" + i.toFixed(1) + "\" " + sel + ">" + i.toFixed(1) + "</option>";
            i += 0.1;
        }
        op += "</select>";
    
        var gl = "<select id=\"singlePolygonStroke\">";
        for (var i = 0; i <= 10; i++) {
            var sel = "";
            if (Number(polygon.strokeWeight) == i) {
                sel = "selected";
            }
            gl += "<option value=\"" + i + "\" " + sel + ">" + i + "</option>";
        }
        gl += "</select>";


        var footerContent = '<b>'+polygon.id+'</b><br>' +
                            event.latLng.lat() + ',' + event.latLng.lng() +
                            '<br><br>';
        footerContent+='<input id="txt_polygonid" type="hidden" value="'+polygon.id+'" name="txt_polygonid">';
        if(polygon.tipo==2 || polygon.tipo==1) {
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonFill" value="' + polygon.fillColor.substring(1, 7) + '" style="border-color: ' + polygon.fillColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Opacidad:</label></div>'
            + '<div style="display: table-cell">' + op + '</div>'
            + '</div>'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Linea:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonLine" value="' + polygon.strokeColor.substring(1, 7) + '" style="border-color: ' + polygon.strokeColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Espesor:</label></div>'
            + '<div style="display: table-cell">' + gl + '</div>'
            + '</div>'
            + '</div>';
        }
        if(polygon.tipo==3) {
          puntocolor=polygon.icon.split("|");
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo: &nbsp;</label>'
            + '<input type="text" size="6" class="picker" id="singlePolygonFill" value="' + puntocolor[1] + '" style="border-color: #' + puntocolor[1] + '"></div>'
            + '</div>';
        }

        footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\"  onclick=\"saveGeoPlan()\" value=\"Guardar Cambios\" /></div>";
        footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deletePlanPoly()\" /></div> </div>";
        //infocontent += "<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Planificar</a>";

        infoWindow.setContent(footerContent);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(objMap);

         $('.picker').colpick({
              layout: 'hex',
              submit: 0,
              colorScheme: 'dark',
              onChange: function(hsb, hex, rgb, el, bySetColor) {
                  $(el).css('border-color', '#' + hex);
                  // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                  if (!bySetColor)
                  {$(el).val(hex);
                    if (polygon.tipo==1 || polygon.tipo==2) {
                       polygon.setOptions({
                           strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                           strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                           fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                           fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                       });
                     }
                     if (polygon.tipo==3 ){
                       var markTextColor = textByColor("#" 
                                          + $("#singlePolygonFill")
                                          .val())
                                          .substring(1);

                       iconSelected = googleMarktxt 
                                  + polygon.numero 
                                  + "|" 
                                  + $("#singlePolygonFill").val() 
                                  + "|" 
                                  + markTextColor;

                       polygon.setOptions({
                           icon: iconSelected
                       });
                     }
                  }
              }
          }).keyup(function() {
              $(this).colpickSetColor(this.value);
          });

          $('#singlePolygonStroke').on('change', function() {
              polygon.setOptions({
                        strokeWeight: $("#singlePolygonStroke").val() //grosor linea
                    });
          });

          $('#singlePolygonOpacity').on('change', function() {
              polygon.setOptions({
                        fillOpacity: $("#singlePolygonOpacity").val() //opacidad
                    });
          });

    }

    function deletePlanPoly() {
        
        var rs=confirm("Se eliminará el elemento seleccionado ¿Desea continuar?");
              if(rs){
                 globalElement.setMap(null);
                 infoWindow.close(objMap);
                 Proyectos.EliminarPoligono(globalElement.indice,globalElement.tipo);
              }
    }

    function saveGeoPlan() {
       var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
              if(rs){
                 Proyectos.ModificarPoligono(globalElement.indice,globalElement.tipo);
              }
    }

    function HTMLCargarProyectos(datos){
       var html="";

       $('#t_proyecto').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            verhtml="<a class=\"btn btn-primary btn-sm\" onclick=\"CargarCapas('"+data.id+"')\"><i class=\"fa fa-eye fa-lg\"></i> </a>";

            estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',1,1)" class="btn btn-danger btn-xs">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,1)" class="btn btn-success btn-xs">Activo</span>';
            }

            publicohtml='<span id=\"'+data.id+'\" onClick=\"updateproyect('+data.id+',1,2)\" class=\"btn btn-danger btn-xs\">No Público</span>';
            if(data.publico==1){
                publicohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,2)" class="btn btn-success btn-xs">Público</span>';
            }
                    
            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+data.created_at+"</td>"+
               "<td>"+estadohtml+"</td>"+
               "<td>"+publicohtml+"</td>"+
               "<td>"+verhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_proyecto").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_proyecto").dataTable();
   };

    function activarTabla(){
       //$("#t_proyecto").dataTable(); // inicializo el datatable  
      // $("#t_capa").dataTable();
    };

    function CargarCapas(id){
        $( "#a_capa" ).click();
        $("#txt_proyectoselect").val(id);
        $('#agregar').attr('disabled',false);
        Proyectos.CargarCapas(id);
        $('html, body').animate({scrollTop: '0px'}, 1000);
    };

     function HTMLCargarCapas(datos){
       var html="";

       $('#t_capa').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            estadohtml='<a class="btn btn-primary btn-sm"  title="Mapear Capa" \n\
                    data-id="'+data.id+'" onclick="MapearCapas('+data.id+','+data.idproyecto+')"><i class="fa fa-map-marker"></i> </a>';
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectoModal" title="Personalizar Capa" \n\
                    data-id="'+index+'" data-titulo="Editar" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Capa" \n\
                    data-id="'+data.id+'" onclick="EliminarCapas('+data.id+')"><i class="fa fa-trash-o fa-fw"></i> </a>';
           checkhtml='<div class="has-success"> <label class="checkbox-inline"> '+
                      '<input type="checkbox" name="sh'+data.id+'"  checked> Show'+ //value="'+data.id+'"
                      '</label>'+
                      '<label class="checkbox-inline">'+
                      '<input type="checkbox" name="flick'+data.id+'" > Flick'+
                      '</label> </div>';
            agregarhtml='<a class="btn btn-primary btn-sm"  onclick="AgregarElemento('+data.id+')" title="Agregar Elemento">'+
                        '<i class="fa fa-plus fa-lg"></i></a>';

            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.proyecto+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+estadohtml+"&nbsp;"+
               modificarhtml+"&nbsp;"+
               eliminarhtml+"&nbsp;"+agregarhtml+"</td>"+
               "<td>"+checkhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_capa").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_capa").dataTable();

   };

   function MapearCapas(id,idproyecto){
        Proyectos.MapearCapas(id,idproyecto,objMap);
    };

    function doPolyActionEdit(event, polygon) {

         value='';
        var nPoliActu = 0;
        var pos;
        var markerColor = '';

        var img_coord = "";
        polyActu = [];
        tmpObject = {};
        globalElement = polygon;
        tmpActu=[];
        infocontent='';

        var op = "<select id=\"singlePolygonOpacity\">";
        for (var i = 0; i <= 1; ) {
            var sel = "";
            if (Number(polygon.fillOpacity).toFixed(1) == i.toFixed(1)) {
                sel = "selected";
            }
            op += "<option value=\"" + i.toFixed(1) + "\" " + sel + ">" + i.toFixed(1) + "</option>";
            i += 0.1;
        }
        op += "</select>";
    
        var gl = "<select id=\"singlePolygonStroke\">";
        for (var i = 0; i <= 10; i++) {
            var sel = "";
            if (Number(polygon.strokeWeight) == i) {
                sel = "selected";
            }
            gl += "<option value=\"" + i + "\" " + sel + ">" + i + "</option>";
        }
        gl += "</select>";


        var footerContent = '<b>'+polygon.title+'</b><br>' +
                            event.latLng.lat() + ',' + event.latLng.lng() +
                            '<br><br>';
        footerContent+='<input id="txt_polygonid" type="hidden" value="'+polygon.id+'" name="txt_polygonid">';
        if(polygon.tipo=="2" || polygon.tipo=="1") {
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonFill" value="' + polygon.fillColor.substring(1, 7) + '" style="border-color: ' + polygon.fillColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Opacidad:</label></div>'
            + '<div style="display: table-cell">' + op + '</div>'
            + '</div>'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Linea:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonLine" value="' + polygon.strokeColor.substring(1, 7) + '" style="border-color: ' + polygon.strokeColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Espesor:</label></div>'
            + '<div style="display: table-cell">' + gl + '</div>'
            + '</div>'
            + '</div>';
        }
        if(polygon.tipo=="3") {
          puntocolor=polygon.icon.split("|");
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo: &nbsp;</label>'
            + '<input type="text" size="6" class="picker" id="singlePolygonFill" value="' + puntocolor[1] + '" style="border-color: #' + puntocolor[1] + '"></div>'
            + '</div>';
        }

        footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\"  onclick=\"UpdateGeoPlan()\" value=\"Guardar Cambios\" /></div>";
        footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deleteUpdatePlanPoly()\" /></div> </div>";
        //infocontent += "<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Planificar</a>";

        infoWindow.setContent(footerContent);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(objMap);

         $('.picker').colpick({
              layout: 'hex',
              submit: 0,
              colorScheme: 'dark',
              onChange: function(hsb, hex, rgb, el, bySetColor) {
                  $(el).css('border-color', '#' + hex);
                  // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                  if (!bySetColor)
                  {$(el).val(hex);
                    if (polygon.tipo==1 || polygon.tipo==2) {
                       polygon.setOptions({
                           strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                           strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                           fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                           fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                       });
                     }
                     if (polygon.tipo==3 ){
                       var markTextColor = textByColor("#" 
                                          + $("#singlePolygonFill")
                                          .val())
                                          .substring(1);

                       iconSelected = googleMarktxt 
                                  + polygon.numero 
                                  + "|" 
                                  + $("#singlePolygonFill").val() 
                                  + "|" 
                                  + markTextColor;

                       polygon.setOptions({
                           icon: iconSelected
                       });
                     }
                  }
              }
          }).keyup(function() {
              $(this).colpickSetColor(this.value);
          });

          $('#singlePolygonStroke').on('change', function() {
              polygon.setOptions({
                        strokeWeight: $("#singlePolygonStroke").val() //grosor linea
                    });
          });

          $('#singlePolygonOpacity').on('change', function() {
              polygon.setOptions({
                        fillOpacity: $("#singlePolygonOpacity").val() //opacidad
                    });
          });

    }

    function UpdateGeoPlan() {
       var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
              if(rs){
                 Proyectos.UpdateGeoPlan(globalElement.id, globalElement.idproyecto);
              }
    }
    function deleteUpdatePlanPoly() { //eliminar elemento
       var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
              if(rs){
                 globalElement.setMap(null);
                 infoWindow.close(objMap);
                 Proyectos.deleteUpdatePlanPoly(globalElement.id, globalElement.idproyecto);
              }
    }

    function EliminarCapas(id){
       var rs=confirm("Se eliminará la capa seleccionada, ¿Desea Continuar?");
       if(rs){
           Proyectos.EliminarCapas(id);
      }
    };

    function updateproyect(idproyecto,valor,campo){
        Proyectos.Updateproyect(idproyecto,valor,campo);
    };

    function EditarPoligono(){
        Proyectos.EditarCapa( //borde,fondo,grosorlinea,opacidad,capaid
              '#'+$("#form_proyecto #pickerPolyLine_edit").val(),
              '#'+$("#form_proyecto #pickerPolyBack_edit").val(),
              $("#form_proyecto #polyLineCreate_edit").val(),
              $("#form_proyecto #sliderPolyOpac_edit" ).slider( "value" ),
              $("#form_proyecto #id").val()
          );
    };

    function EditarPunto(){
        Proyectos.EditarCapa( //borde,fondo,grosorlinea,opacidad,capaid
              '',
              '#'+$("#form_proyecto #pickerMarkBack_edit").val(),
              '',
              '',
              $("#form_proyecto #id").val()
          );
    };


    function CargarHTML(obj){
          // 1: Multiple | 0: Simple
          idtipo=1;
          tipo='simple';
          if(idtipo==1){
              tipo="multiple";
          }
      
          $(".add").remove();
          var html="";
          $(".selectdinamico").multiselect('destroy');
          for (var i=1; i<= obj.cant; i++) {
              data={c:i,tabla_id:table,t:obj.cant};
              changue='';
              if(i<obj.cant){
                  if(idtipo==0){
                      changue=' onChange="CargarDetalle('+"'"+i+','+obj.cant+','+table+','+idtipo+"'"+')" ';
                  }
                  else{
                      changue=' data-hidden="'+i+','+obj.cant+','+table+','+idtipo+'" ';
                  }
              }
              if(i==obj.cant){
                  if(idtipo==1){
                    changue=' onChange="opcionSelected('+i+')" ';
                  }
              }
      
              tiposelect='';
              if(tipo=='multiple'){
                  tiposelect='multiple'
              }
      
              html='  <div class="add col-sm-12">'+
                          '<label>'+obj.c_detalle.split('|')[(i-1)]+':</label>'+
                          '<select class="selectdinamico form-control" '+changue+' name="slct_c'+i+'" id="slct_c'+i+'" '+tiposelect+'>'+
                          '    <option value="">.::Seleccione::.</option>'+
                          '</select>'+
                      '</div>';
              $("#selects").append(html);
              if(i>1){
                  slctGlobalHtml('slct_c'+i, tipo);
              }
              if(i==1){
                  if(idtipo==0){
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data);
                  }
                  else{
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
                  }
              }
          };
      };

      function CargarDetalle(cod){
          var id=cod.split(',')[0];
          var t=cod.split(',')[1];
          var tabla_id=cod.split(',')[2];
          var idtipo=cod.split(',')[3];
      
          if($("#slct_c"+id).val()!=null && $("#slct_c"+id).length>0 && $("#slct_c"+id).val()!=''){
          id++;
          var valor=[];
      
              for (var i = 1; i < id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
              var data={valor:valor,c:id,t:t,tabla_id:tabla_id,idtipo:idtipo};
              $('#slct_c'+id).multiselect('destroy');
      
              if(idtipo==0){
                  tipo='simple';
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data);
              }
              else{
                  tipo="multiple";
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
              }
          }
      };



</script>