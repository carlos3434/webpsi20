<script type="text/javascript">
    var googleMarktxt = "http://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld=";
    var temporalBandeja=0;
    var table=0;
    var addressContent = Array();
    var mapTools = {};
    var drawingManager;
    $(document).ready(function() {
        slctGlobalHtml('slct_actividad','simple');
        var objMain = $('#main');

        $("[data-toggle='offcanvas']").click();

        //SELECTS
        slctGlobalHtml('slct_tipo_proyecto_id','simple');
        slctGlobalHtml('slct_tipo_zona_id','simple');
        slctGlobal.listarSlct('geotabla','slct_elemento','simple');

        Crearmapa();
        Proyectos.CargarProyectos();
        //activarTabla();

        function showSidebar() {
            objMain.addClass('use-sidebar');
            mapResize();
        }
        function hideSidebar() {
            objMain.removeClass('use-sidebar');
            mapResize();
        }
        var objSeparator = $('#separator');
        objSeparator.click(function(e) {
            e.preventDefault();
            if (objMain.hasClass('use-sidebar')) {
                hideSidebar();
            }
            else {
                showSidebar();
            }
        }).css('height', objSeparator.parent().outerHeight() + 'px');

        //LIMPIAR MAPA
        $("#btn_clean, #btn_cleaned").click(function() {
             Proyectos.LimpiarMapa();
             Crearmapa();
        });

        //TABLA
        $('#myTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });

        $('html').on('click', function(e) {
              if (typeof $(e.target).data('original-title') == 'undefined' &&
                  !$(e.target).parents().is('.popover.in')) {
                  $('[data-original-title]').popover('hide');
              }
        });

        //COLORES POLIGONO Y MARCADORES
        $("#pickerMarkBack").val('F7584C');
        $("#pickerPolyBack").val('F7584C');
        $("#pickerPolyLine").val('F7584C');

        $( "#sliderPolyOpac" ).slider({
           value:0,
           min: 0,
           max: 0.99,
           step: 0.01,
           slide: function( event, ui ) {
               $("#" + $(this).attr("title")).css("opacity", ui.value);
           }
        });

        $( "#sliderPolyOpac_edit" ).slider({
              value:0,
              min: 0,
              max: 0.99,
              step: 0.01,
              slide: function( event, ui ) {
                  $("#" + $(this).attr("title")).css("opacity", ui.value);
              }
        });

        $("#polyLineCreate").change(function (){
            $("#polyDemoCreateborder")
                .css("border-width", $( this ).val() + "px" );
        });
        $("#polyLineCreate_edit").change(function (){
            $("#polyDemoCreateborder_edit")
                .css("border-width", $( this ).val() + "px" );
        });

        $("#pickerMarkBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              if(!bySetColor) $(el).val(hex);
                          
                          $(".newMarkerBackground").css(
                                  "background-color", 
                                  "#" + $("#pickerMarkBack").val()
                              );
            }
            }).keyup(function(){
             $(this).colpickSetColor(this.value);
        });
          
        $("#pickerMarkBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, 
                            // and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
                            
                            $(".newMarkerBackground").css(
                                    "background-color", 
                                    "#" + $("#pickerMarkBack_edit").val()
                                );
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
        });

        $("#pickerPolyBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              // Fill the text box just if the color was set using the picker, 
                          // and not the colpickSetColor function.
              if(!bySetColor) $(el).val(hex);
                          $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
            });
    
            $("#pickerPolyBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                            $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
            });

        $("#pickerPolyLine").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              if(!bySetColor) $(el).val(hex);
                          $("#polyDemoCreateborder").css("border-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
        });
    
        $("#pickerPolyLine_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                            $("#polyDemoCreateborder_edit").css("border-color", '#'+hex);
              }
              }).keyup(function(){
                        $(this).colpickSetColor(this.value);
        });

        //MODALES
        $('#proyectocapaModal').on('show.bs.modal', function (event) {

              var button = $(event.relatedTarget);
              var titulo = button.data('titulo'); 
              capas_id = button.data('id'); 
              var modal = $(this); //captura el modal
              modal.find('.modal-title').text(titulo+' Capa');
              $('#form_proyectocapa [data-toggle="tooltip"]').css("display","none");

                 
              modal.find('.modal-footer .btn-primary').text('Actualizar');

              $('#form_proyectocapa #txt_nombre').val( capasObj[capas_id].nombre);
              
              var elementos=capasObj[capas_id].elementos.split(",");
              var _html='<table class="table table-bordered">';
              var i=1; //class="info"
                   $.each(elementos, function(index, datae) {
                      if(i==1){
                        _html+='</tr>';
                      }
                      if(i<=5){
                        _html+='<td>'+datae+'</td>';
                        i++;
                      } 
                      if(i==6){
                      _html+='<tr>';
                      i=1;
                      }
                   });
                  _html+='</table>';
              $('#form_proyectocapa #capaelemento').html(_html);

              if (capasObj[capas_id].tipo=='3') {
               /* $("#estilo_poligono_edit").hide();
                $("#estilo_punto_edit").show();*/
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPunto();');
              } else {
                /*$("#estilo_poligono_edit").show();
                $("#estilo_punto_edit").hide();*/
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPoligono();');
              }

              $("#form_proyectocapa").append("<input type='hidden' value='"+
                    capasObj[capas_id].id+"' name='id' id='id'>");

              if(capasObj[capas_id].figura_id==4){
                $('#form_proyectocapa #txt_nombre').attr("disabled",true);
              }
        });

        $('#proyectocapaModal').on('hide.bs.modal', function (event) {
              var modal = $(this);
               $('#form_proyectocapa #id').remove();
               $('#form_proyectocapa #capaelemento').html('');
               $('#form_proyectocapa #txt_nombre').attr("disabled",false);
        });

        $('#proyectoModal').on('show.bs.modal', function (event) {

              var button = $(event.relatedTarget); 
              var titulo = button.data('titulo'); 
              proyectos_id = button.data('id');
              var modal = $(this); //captura el modal
              modal.find('.modal-title').text(titulo+' Proyecto');
              $('#form_proyecto [data-toggle="tooltip"]').css("display","none");
                 
              modal.find('.modal-footer .btn-primary').text('Actualizar');
               modal.find('.modal-footer .btn-primary').attr('onClick','updateproyect();');

              $('#form_proyecto #txt_nombre').val( proyectosObj[proyectos_id].nombre);
              $('#form_proyecto #slct_estado').val( proyectosObj[proyectos_id].estado_publico );

              $("#form_proyecto").append("<input type='hidden' value='"+
                    proyectosObj[proyectos_id].id+"' name='id' id='id'>");
        });
          
        $('#proyectoModal').on('hide.bs.modal', function (event) {
              var modal = $(this);
               $('#form_proyecto #id').remove();
        });

        //KML CARGA Y DESCARGA
        $("#btnupfile").trigger("click");

        $("#btn_kml").click(function(){
              var polygons = Array();
              var markers = Array();
              if(viewcapaskml.concat(mapObjects).length > 0) {
                  for (var i in mapObjects) {
                      mapObjects[i]['coord']=coordinatesObjects[i]['coord'];
                  }
                  Map.preparePolygon(viewcapaskml.concat(mapObjects),Proyectos.FinalizarKml); 
              }else{
                  Psi.mensaje('danger', 'No proyectos Mapeados', 6000);
              }
        });

        $("#btn_kml_upload").click(function(){
              var data = new FormData();
              var inputFile = document.getElementById("kml_file");
              var file = inputFile.files[0];
              data.append('archivo', file);
              Proyectos.uploadkml(data,cargarKML);
        });

     });

    function Crearmapa() {
      //objMap = doObjMap("mymap", objMapProps, mapTools);
      objMap  = new google.maps.Map(document.getElementById("mymap"), objMapProps);
      $('#slct_tipo_zona_id').multiselect('clearSelection');
      $('#slct_tipo_zona_id').multiselect('select', 1);
      $('#slct_tipo_zona_id').multiselect('rebuild');
      drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON,
          ]
        },
        polygonOptions: {
          fillColor: '#4dacf0',
          fillOpacity: 0.5,
          strokeWeight: 2,
          strokeColor: '#0e7dcc',
          editable:true
        }
      });
      drawingManager.setMap(objMap);
       
      google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {

        drawingManager.setOptions({
          drawingMode: null
        });

        var polygonArray = [];
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
          polygonArray.push(polygon.getPath().getAt(i).lng()+','+polygon.getPath().getAt(i).lat());
        }
        
        var actividad=$("#slct_actividad").val();
        
        var provision=averia=0;
        if(actividad!==null){
          if(actividad.includes("1")){
            averia=1;
          } 
          if(actividad.includes("2")){
            provision=1;
          }
        }
        mapObjects.push({"elemento": '',"figura_id":'4',"capa": '',"detalle":$("#txt_poligono").val(),"coordenadas": polygonArray.join('|'),"orden":'',"borde":polygon.strokeColor,"grosorlinea":polygon.strokeWeight, "tabla_detalle_id":"","fondo":polygon.fillColor,"opacidad":polygon.fillOpacity, "tipo_zona": $("#slct_tipo_zona_id").val(), "averia":averia, "provision": provision });
          //console.log(mapObjects);
        polygon.setOptions({
          id:$("#txt_poligono").val(),
          indice:ixobj,
          tipo:4,
          tipo_zona: $("#slct_tipo_zona_id").val(),
          actividad: actividad
        });
        ixobj++;

        infoWindow = new google.maps.InfoWindow; 

        google.maps.event.addListener(polygon, 'click', function (event) {
            doPolyAction(event,this);
        });
        google.maps.event.addListener(polygon.getPath(), 'set_at', function(event) {
          doPolyActionInsert(event, polygon)
        });
        google.maps.event.addListener(polygon.getPath(), 'insert_at', function(event) {
          doPolyActionInsert(event, polygon)
        });
        google.maps.event.addListener(polygon, 'rightclick', function(event) {
          if (event.vertex == undefined) {
            return;
          } else {
            //abrir un infocontent
            OpenremoveVertex(event, polygon);
          }
        });
      }); 
    }

    function OpenremoveVertex(event, polygon) {
      if(infoWindow!==undefined)
        infoWindow.close(objMap);

      infoWindow = new google.maps.InfoWindow; 
      var footerContent='<div id="btn_delete">'+
      '<a style="cursor:pointer;text-decoration:underline">Eliminar'+
      '</a></div>';
      infoWindow.setContent(footerContent);
      infoWindow.setPosition(event.latLng);
      infoWindow.open(objMap);
      $('#btn_delete').on('click', function() {
        var path = polygon.getPath();
        path.removeAt(event.vertex);
        doPolyActionInsert(event, polygon)
        infoWindow.close(objMap);
      });
    }

    //ELEMENTOS
    function GenerarCapa() {
        if($("#txt_capa").val()!='' && $("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
              //ordenVal = $("#txt_orden").val().split("_");
              geoVal = $("#slct_elemento").val().split("_");

              if(geoVal[1] == 3){
                      Proyectos.TrazarCapaPunto(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          '',
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );
              } 
              if(geoVal[1] == 2) {
                      //polyDemoCreate
                      Proyectos.TrazarCapaPoligono(objMap,
                                          geoVal[0],
                                          geoVal[2],
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );

              }
              if(geoVal[1] == 1) {
                     // polyDemoCreate
                      Proyectos.TrazarCapaCircle(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val()
                                          );

              }
            } else {
              if($("#"+$("#txt_selfinal").val()).val()==null){
                alert('Seleccione el ultimo Subelemento');
              }
              else {
              alert('Indique nombre de capa');
              }
            }
    }

    function GenerarCapaElemento() {
        if($("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
              geoVal = $("#slct_elemento").val().split("_");

              if(geoVal[1] == 3){
                      Proyectos.TrazarCapaPunto(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          '',
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );
              } 
              if(geoVal[1] == 2) {
                      polyDemoCreate
                      Proyectos.TrazarCapaPoligono(objMap,
                                          geoVal[0],
                                          geoVal[2],
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );

              }
              if(geoVal[1] == 1) {
                      polyDemoCreate
                      Proyectos.TrazarCapaCircle(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capaselect").val()
                                          );

              }
          }  else {
              alert('Seleccione almenos un elemento');
          }
    }
    
    function GuardarProyecto(h) {
        if ($("#txt_proyecto").val()!='') {
                var rs=confirm("Se guardaran las capas creadas ¿Desea Continuar?");
                if(rs){
                  geoVal = $("#slct_elemento").val().split("_");
                  Proyectos.GuardarProyecto(geoVal[2]);
                }
        } else {
               alert('Indique nombre de proyecto');
        }
    }

    function AgregarElemento(capaid) {
        cancelActualizaZona();
        Proyectos.LimpiarMapa();
        var pos=$("#divelemento").position()
        $('#sidebar').animate({scrollTop: pos.top}, 1000);
        $('html, body').animate({scrollTop: 50}, 1000);
        $('#divaccion').html('Agregar Elemento(s)');
        $('#txt_capaselect').val(capaid);
        $('#div_capa').html('');
        $('#div_pro').html('<div class="col-sm-6">'+
                            ' <label>&nbsp;</label>'+
                            '<button id="btn_addelemento" type="button" class="form-control btn btn-primary" '+
                            'onClick="GenerarCapaElemento()">Generar Capa</button></div>'+
            '<div class="form-group">'+
                '<div class="col-sm-6"><label>&nbsp;</label>'+
                '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success" onClick="AgregarElementoCapa()">Actualizar Capa</button>'+
                '</div></div>');
        $('#slct_elemento').val("").trigger('changue');
        $('#slct_elemento').multiselect('rebuild');
        $('#div_cancel').show();
    }

    function Agregarcapa() {
      cancelActualizaZona();
      $('#divaccion').html('Agregar Capa(s)');  
      Proyectos.LimpiarMapa();
      $('#elementcontent').attr('aria-expanded','true');
      $('#elementcontent').addClass('in');
      $('#elementcontent').height( '100%' );
      $('#div_pro').html(
                    '<div class="form-group">'+
                      '<div class="col-sm-12"><label>&nbsp;</label>'+
                     '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success"  onClick="ActualizarCapa()">Actualizar Proyecto</button>'+
                      '</div></div>');
      $('#slct_elemento').val("").trigger('changue');
      $('#slct_elemento').multiselect('rebuild');
      $('#div_cancel').show();  
    } 

    function ActualizarCapa() {
        var rs=confirm("Se agregaran al Proyecto las capas creadas ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyecto();
              }
    }

    function cancelarActualzc() {
          $('#slct_elemento').val("").trigger('change');
          $('#slct_elemento').multiselect('rebuild');
          $('#divaccion').html('Elementos');
          $('#txt_capaselect').val('');
          Proyectos.LimpiarMapa();
          //objMap = doObjMap("mymap", objMapProps, mapTools);
          $('#div_pro').html(
              '<div class="form-group"><div class="col-sm-5">'+
                '<label>Proyecto</label>'+
                '<input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">'+
                '</div><div class="col-sm-7">'+
                '<label>&nbsp;</label>'+
                '<button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()">Guardar Proyecto</button>'+
                '</div></div>');
          $('#div_capa').html('<div class="form-group">'+
                            '<div class="col-sm-5"><label>Capa</label>'+
                             '<input id="txt_capa" class="form-control" type="text" value="" name="txt_capa" placeholder="Ingrese Capa">'+
                             '</div><div class="col-sm-7"><label>&nbsp;</label>'+
                             '<button type="button" id="btn_capa" class="form-control btn btn-primary"  onClick="GenerarCapa()">Generar Capa</button>'+
                             '</div></div>');
          $('#div_cancel').hide();
    }

    function AgregarElementoCapa() {
        var rs=confirm("Se agregara(n) a la Capa el(los) elemento(s) creado(s) ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyectoCapa();
              }
    }
    
    //ZONA PERSONALIZADA
    function Zonachange() {
      op=$("#slct_tipo_zona_id").val();
      if(op!==null) {
        $("#btn_proyecto_zona").removeAttr('disabled',false);
        $("#btn_agregar_zona").removeAttr('disabled',false);
        if(op.indexOf("3")>=0) {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#e66565',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#fc0d19',
              editable: true
            }
          });
        } else if(op.indexOf("2")>=0) {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#535557',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#000000',
              editable: true
            }
          });
        } else {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#4dacf0',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#0e7dcc',
              editable: true
            }
          });
        }
      } else {
        alert('Debe elegir almenos un tipo zona!');
        $("#btn_proyecto_zona").attr('disabled',true);
        $("#btn_agregar_zona").attr('disabled',true);
      }
    }

    function AgregarPersonalizado(capaid) {
        cancelarActualzc();
        var pos=$("#divzonapersonalizada").position()
        $('#sidebar').animate({scrollTop: pos.top}, 1000);
        $('html, body').animate({scrollTop: 50}, 1000);
        $('#divzona').html('Agregar Zona(s) Perzonalizada(s)');
        $('#txt_capaselect').val(capaid);
        $('#txt_poligono').val('');
        $('#divagregarzonas').css('display','');
        $('#divguardarzonas').css('display','none');
        //$("#btn_clean").click();
    }

    function cancelActualizaZona() {
        var pos=$("#txt_poligono").position()
        $('#sidebar').animate({scrollTop: pos.top}, 1000);
        $('html, body').animate({scrollTop: pos.top}, 1000);
        $('#divzona').html('Zona Personalizada');
        $('#txt_capaselect').val('');
        $('#txt_poligono').val('');
        $('#txt_proyecto_zona').val('');
        $('#divagregarzonas').css('display','none');
        $('#divguardarzonas').css('display','');
        Crearmapa();
    }

    function GuardarZonas() {
        var rs=confirm("Se agregara(n) a la Capa el(los) elemento(s) creado(s) ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyectoZona();
              }
    }

    function GuardarProyectoZona(){
        //console.log(mapObjects);
        if ($("#txt_proyecto_zona").val()!='') {
          var rs=confirm("Se guardaran los poligonos creados ¿Desea Continuar?");
          if(rs){
            Proyectos.GuardarProyectoZona();
            //console.log(mapObjects);
          }
        } else {
          alert('Indique Nombre de Proyecto Zona');
        }
      }

    /**
    * Convierte un valor hexadecimal al sistema decimal
    * @param {type} h
    * @returns {unresolved}
    */
    function hexdec(h) {
        h = h.toUpperCase();
        return parseInt(h, 16);
    }
    
    /**
    * Retorna color de texto de acuerdo al color de fondo
    * Color de texto: Blanco o negro.
    * Recibe color en hexadecimal con '#'
    * @param {type} color
    * @returns {String}
    */

    function textByColor(color) {
        color = color.substring(1);
    
        var c_r = hexdec(color.substring(0, 2));
        var c_g = hexdec(color.substring(2, 4));
        var c_b = hexdec(color.substring(4, 6));
    
        var bg = ((c_r * 299) + (c_g * 587) + (c_b * 114)) / 1000;
        if (bg > 130) {
            return "#000000";
        } else {
            return "#FFFFFF";
        }
    }
    
    function BuscarElemento(){
      //Ocultar o mostrar bloque de estilos
      $(".estiloItem").hide();

        if($("#slct_elemento").val()=="" || $("#slct_elemento").val()==null){
          $("#selects").html('');
          $("#div_capa").hide();
          $("#div_pro").hide();
          //$("#btn_capa").attr('disabled',true);
          //$("#btn_proyecto").attr('disabled',true);
        } else {
          //var dependencia = "";
          geoVal = $("#slct_elemento").val().split("_");
          //var origen = geoVal[2]; // mdf,provincia, troba, .. elementos
          if (geoVal[1] == 2 || geoVal[1] == 1) {
              $("#estilo_poligono").show();
          } else if (geoVal[1] == 3) {
              $("#estilo_punto").show();
          }
          table=geoVal[0];
          var data = {tabla_id:table};
          TablaDetalle.Campos(data,CargarHTML);
        }
    }

    function opcionSelected(titulo){
       if($('#slct_c'+titulo).val()==null){
         //$("#btn_capa").attr('disabled',true);
         //$("#btn_proyecto").attr('disabled',true);
         $("#div_capa").hide();
         $("#div_pro").hide()
       }
       else {
          $("#div_capa").show();
          $("#div_pro").show();
          //$("#btn_capa").removeAttr('disabled',false);
          //$("#btn_proyecto").removeAttr('disabled',false);
          $("#txt_selfinal").val(titulo);
       } 
    }

     function doPolyAction(event, polygon) {
        value='';
        var nPoliActu = 0;
        var pos;
        var markerColor = '';

        var img_coord = "";
        polyActu = [];
        tmpObject = {};
        globalElement = polygon;
        tmpActu=[];
        infocontent='';

        var op = "<select id=\"singlePolygonOpacity\">";
        for (var i = 0; i <= 1; ) {
            var sel = "";
            if (Number(polygon.fillOpacity).toFixed(1) == i.toFixed(1)) {
                sel = "selected";
            }
            op += "<option value=\"" + i.toFixed(1) + "\" " + sel + ">" + i.toFixed(1) + "</option>";
            i += 0.1;
        }
        op += "</select>";
    
        var gl = "<select id=\"singlePolygonStroke\">";
        for (var i = 0; i <= 10; i++) {
            var sel = "";
            if (Number(polygon.strokeWeight) == i) {
                sel = "selected";
            }
            gl += "<option value=\"" + i + "\" " + sel + ">" + i + "</option>";
        }
        gl += "</select>";


        var footerContent = '<b>'+polygon.id+'</b><h5>' +
                            event.latLng.lat() + ',' + event.latLng.lng() +
                            '</h5>';
        footerContent+='<input id="txt_detalle" type="hidden" value="'+polygon.id+'" name="txt_detalle">';
        if(polygon.tipo==4) { //poligonos personalizados
           var footerContent = '<label><b>Nombre:</b></label><input id="txt_detalle" value="'+polygon.id+'" name="txt_detalle">' +
              '<div><select id="slct_actividad_edit" class="form-control" name="slct_actividad_edit[]" multiple="">'+
                                                      '<option value="1">Avería</option>'+
                                                      '<option value="2">Provisión</option>'+
                                                  '</select></div>'+
              '<div><select class="form-control"  id="slct_tipo_zona_edit_id" name="slct_tipo_zona_edit_id">'+
                    '<option value="1">GeoCerca</option>'+
                    '<option value="2">Zona Sombra</option>'+
                    '<option value="3">Zona Peligrosa</option>'+
                    '<option value="4">Zona Tipo 4</option>'+
                    '<option value="5">Zona Tipo 5</option>'+
                    '<option value="6">Zona Tipo 6</option>'+
                    '<option value="7">Zona Tipo 7</option>'+
                    '<option value="8">Zona Tipo 8</option>'+
                    '<option value="9">Zona Tipo 9</option>'+
                    '<option value="10">Zona Tipo 10</option>'+
                '</select></div>'+
                '<h5>'+event.latLng.lat() + ',' + event.latLng.lng() +
                '</h5>';
        }
        if(polygon.tipo==2 || polygon.tipo==1 || polygon.tipo==4) {
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonFill" value="' + polygon.fillColor.substring(1, 7) + '" style="border-color: ' + polygon.fillColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Opacidad:</label></div>'
            + '<div style="display: table-cell">' + op + '</div>'
            + '</div>'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Linea:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonLine" value="' + polygon.strokeColor.substring(1, 7) + '" style="border-color: ' + polygon.strokeColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Espesor:</label></div>'
            + '<div style="display: table-cell">' + gl + '</div>'
            + '</div>'
            + '</div>';
        }
        if(polygon.tipo==3) {
          puntocolor=polygon.icon.split("|");
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo: &nbsp;</label>'
            + '<input type="text" size="6" class="picker" id="singlePolygonFill" value="' + puntocolor[1] + '" style="border-color: #' + puntocolor[1] + '"></div>'
            + '</div>';
        }

        footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\" value=\"Aplicar Cambios\" id=\"btncambios\" /></div>";
        footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deletePlanPoly()\" /></div> </div>";
        //infocontent += "<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Planificar</a>";

        infoWindow.setContent(footerContent);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(objMap);

        if(polygon.tipo=="4") {
          slctGlobalHtml('slct_tipo_zona_edit_id','simple');
          slctGlobalHtml('slct_actividad_edit','multiple');
          $('#slct_tipo_zona_edit_id').multiselect('clearSelection');
          $('#slct_actividad_edit').multiselect('clearSelection');
          var tipozona='';
          if(polygon.tipo_zona!=='') tipozona=polygon.tipo_zona.split("|");
          $('#slct_tipo_zona_edit_id').multiselect('select', tipozona);
          $('#slct_tipo_zona_edit_id').multiselect('rebuild');
          
          $('#slct_actividad_edit').multiselect('select', polygon.actividad);
          $('#slct_actividad_edit').multiselect('rebuild');
        }

         $('.picker').colpick({
              layout: 'hex',
              submit: 0,
              colorScheme: 'dark',
              onChange: function(hsb, hex, rgb, el, bySetColor) {
                  $(el).css('border-color', '#' + hex);
                  // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                  if (!bySetColor) { 
                    $(el).val(hex);
                  }
              }
          }).keyup(function() {
              $(this).colpickSetColor(this.value);
          });

          $('#btncambios').on('click', function() {
              polygon.setOptions({
                        strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                        fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                    });
              if (polygon.tipo==4) {
                polygon.setOptions({
                        detalle: $("#txt_detalle").val(), //nombre
                        tipo_zona: $("#slct_tipo_zona_edit_id").val(),
                        actividad: $("#slct_actividad_edit").val(),
                    });
              }
              if (polygon.tipo==1 || polygon.tipo==2 || polygon.tipo==4) {
                       polygon.setOptions({
                           strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                           strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                           fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                           fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                       });
              }
              if (polygon.tipo==3 ){
                var markTextColor = textByColor("#" 
                                  + $("#singlePolygonFill")
                                  .val())
                                  .substring(1);

                iconSelected = googleMarktxt 
                          + polygon.numero 
                          + "|" 
                          + $("#singlePolygonFill").val() 
                          + "|" 
                          + markTextColor;

                polygon.setOptions({
                    icon: iconSelected
                });
              }

              saveGeoPlan();
          });
    }

    function deletePlanPoly() {    
        var rs=confirm("Se eliminará el elemento seleccionado ¿Desea continuar?");
              if(rs){
                 globalElement.setMap(null);
                 infoWindow.close(objMap);
                 Proyectos.EliminarPoligono(globalElement.indice,globalElement.tipo);
              }
    }

    function saveGeoPlan() {
       var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
              if(rs){
                 Proyectos.ModificarPoligono(globalElement.indice,globalElement.tipo);
              }
    }

    function HTMLCargarProyectos(datos){
       var html="";

       $('#t_proyecto').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            verhtml="<a class=\"btn btn-primary btn-sm\" onclick=\"CargarCapas('"+data.id+"')\"><i class=\"fa fa-eye fa-lg\"></i> </a>";

            estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',1,1)" class="btn btn-danger btn-xs">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,1)" class="btn btn-success btn-xs">Activo</span>';
            }

            publicohtml='<span id=\"'+data.id+'\" onClick=\"updateproyect('+data.id+',1,2)\" class=\"btn btn-danger btn-xs\">No Público</span>';
            if(data.publico==1){
                publicohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,2)" class="btn btn-success btn-xs">Público</span>';
            }
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectoModal" title="Personalizar Proyecto" \n\
                    data-id="'+index+'" data-titulo="Editar" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Proyecto" \n\
                    data-id="'+data.id+'" onclick="EliminarProyecto('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
                    
            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+data.created+"</td>"+
               "<td>"+publicohtml+"</td>"+
                "<td>"+verhtml+modificarhtml+
               eliminarhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_proyecto").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_proyecto").dataTable();
   };

    function activarTabla(){
       //$("#t_proyecto").dataTable(); // inicializo el datatable  
      // $("#t_capa").dataTable();
    };

    function CargarCapas(id){
        $( "#a_capa" ).click();
        $("#txt_proyectoselect").val(id);
        $('#agregar').attr('disabled',false);
        Proyectos.CargarCapas(id);
        //$('html, body').animate({scrollTop: '0px'}, 1000);
    };

    function HTMLCargarCapas(datos){
       var html="";

       $('#t_capa').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            estadohtml='<a class="btn btn-primary btn-sm"  title="Mapear Capa" \n\
                    data-id="'+data.id+'" onclick="MapearCapas('+data.id+','+data.idproyecto+')"><i class="fa fa-map-marker"></i> </a>';
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectocapaModal" title="Personalizar Capa" \n\
                    data-id="'+index+'" data-titulo="Editar" id="ckeditar_'+index+'" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Capa" \n\
                    data-id="'+data.id+'" onclick="EliminarCapas('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
           checkhtml='<div class="has-success"> <label class="checkbox-inline"> '+
                      '<input type="checkbox" name="sh'+data.id+'" checked> Show'+
                      '</label>'+
                      '<label class="checkbox-inline">'+
                      '<input type="checkbox" name="flick'+data.id+'" > Flick'+
                      '</label> </div>';
            var nombre=data.nombre;
            if(data.figura_id==4) {
              agregarhtml='<a class="btn btn-primary btn-sm"  onclick="AgregarPersonalizado('+data.id+')" title="Agregar Elemento">'+
                        '<i class="fa fa-plus"></i></a>';
              $("#agregar").attr('disabled',true);

              var averia=data.averia.split(",");
              var provision=data.provision.split(",");
              nombre="";
              if(averia.includes("1") || averia.includes(1)){
                nombre+="Avería, ";
              }
              if(provision.includes("1") || provision.includes(1)){
                nombre+="Provisión";
              }

              if(data.elementos.length>20){
                nombre+=data.elementos.substring(0, 30)+'<h3 style="margin:0"><a style="cursor:pointer" onclick="mostarEdit('+index+')">[...]</a></h3>';
              } else {
                nombre+=": "+data.elementos;
              }
            } else {
              agregarhtml='<a class="btn btn-primary btn-sm"  onclick="AgregarElemento('+data.id+')" title="Agregar Elemento">'+
                        '<i class="fa fa-plus"></i></a>';
              $("#agregar").removeAttr('disabled');
            }            
            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.proyecto+"</td>"+
               "<td>"+nombre+"</td>"+
               "<td>"+estadohtml+"&nbsp;"+
               modificarhtml+"&nbsp;"+
               eliminarhtml+"&nbsp;"+agregarhtml+"</td>"+
               "<td>"+checkhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_capa").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_capa").dataTable();

   };

   function mostarEdit(index){
      $("#ckeditar_"+index).click();
   }

   function MapearCapas(id,idproyecto){
        $('html, body').animate({scrollTop: '100px'}, 1000);
        Proyectos.MapearCapas(id,idproyecto,objMap);
    };

   function doPolyActionInsert(event, polygon) {
      var polygonArray = [];
      for (var i = 0; i < polygon.getPath().getLength(); i++) {
        polygonArray.push(polygon.getPath().getAt(i).lng()+','+polygon.getPath().getAt(i).lat());
      }     
      mapObjects[polygon.indice]["coordenadas"]=polygonArray.join('|');
      //console.log(mapObjects[polygon.indice]["coordenadas"]);
   }

    function doPolyActionEdit(event, polygon) {
        value='';
        var nPoliActu = 0;
        var pos;
        var markerColor = '';

        var img_coord = "";
        polyActu = [];
        tmpObject = {};
        globalElement = polygon;
        tmpActu=[];
        infocontent='';

        var op = "<select id=\"singlePolygonOpacity\">";
        for (var i = 0; i <= 1; ) {
            var sel = "";
            if (Number(polygon.fillOpacity).toFixed(1) == i.toFixed(1)) {
                sel = "selected";
            }
            op += "<option value=\"" + i.toFixed(1) + "\" " + sel + ">" + i.toFixed(1) + "</option>";
            i += 0.1;
        }
        op += "</select>";
    
        var gl = "<select id=\"singlePolygonStroke\">";
        for (var i = 0; i <= 10; i++) {
            var sel = "";
            if (Number(polygon.strokeWeight) == i) {
                sel = "selected";
            }
            gl += "<option value=\"" + i + "\" " + sel + ">" + i + "</option>";
        }
        gl += "</select>";

        var footerContent = '<b>'+polygon.title+'<br>'+polygon.detalle+'</b>'+'<h5>' +
                            event.latLng.lat() + ',' + event.latLng.lng() +
                            '</h5>';
        footerContent+='<input id="txt_detalle" type="hidden" value="'+polygon.detalle+'" name="txt_detalle">';
        if(polygon.tipo==4) {
           var footerContent =  '<b>'+polygon.proyecto+'</b><h5>' +
                                '<div class="col-sm-12"><label><b>Nombre:</b></label><input id="txt_detalle" value="'+polygon.detalle+'" name="txt_detalle"></div>' +
                             '<div><select id="slct_actividad_edit" class="form-control" name="slct_actividad_edit[]" multiple="">'+
                                                      '<option value="1">Avería</option>'+
                                                      '<option value="2">Provisión</option>'+
                                                  '</select></div>'+
                            '<div><select class="form-control"  id="slct_tipo_zona_edit_id" name="slct_tipo_zona_edit_id">'+
                                '<option value="1">GeoCerca</option>'+
                                '<option value="2">Zona Sombra</option>'+
                                '<option value="3">Zona Peligrosa</option>'+
                                '<option value="4">Zona Tipo 4</option>'+
                                '<option value="5">Zona Tipo 5</option>'+
                                '<option value="6">Zona Tipo 6</option>'+
                                '<option value="7">Zona Tipo 7</option>'+
                                '<option value="8">Zona Tipo 8</option>'+
                                '<option value="9">Zona Tipo 9</option>'+
                                '<option value="10">Zona Tipo 10</option>'+
                            '</select></div>'+
                            '<h5>'+event.latLng.lat() + ',' + event.latLng.lng() +
                            '</h5>';
        }

        if(polygon.tipo=="2" || polygon.tipo=="1" || polygon.tipo=="4") {
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonFill" value="' + polygon.fillColor.substring(1, 7) + '" style="border-color: ' + polygon.fillColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Opacidad:</label></div>'
            + '<div style="display: table-cell">' + op + '</div>'
            + '</div>'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Linea:</label></div>'
            + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonLine" value="' + polygon.strokeColor.substring(1, 7) + '" style="border-color: ' + polygon.strokeColor + '"></div>'
            + '<div style="display: table-cell"><label>&nbsp; Espesor:</label></div>'
            + '<div style="display: table-cell">' + gl + '</div>'
            + '</div>'
            + '</div>';
        }
        if(polygon.tipo=="3") {
          puntocolor=polygon.icon.split("|");
          footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo: &nbsp;</label>'
            + '<input type="text" size="6" class="picker" id="singlePolygonFill" value="' + puntocolor[1] + '" style="border-color: #' + puntocolor[1] + '"></div>'
            + '</div>';
        }

        footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\"  id=\"btn_UpdateGeoPlan\" value=\"Aplicar Cambios\" /></div>";
        footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deleteUpdatePlanPoly()\" /></div> </div>";
        //infocontent += "<a href=\"javascript:void(0)\" onclick=\"doPlanPoly()\">Planificar</a>";

        infoWindow.setContent(footerContent);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(objMap);

        if(polygon.tipo=="4") {
          slctGlobalHtml('slct_tipo_zona_edit_id','simple');
          slctGlobalHtml('slct_actividad_edit','multiple');
          $('#slct_tipo_zona_edit_id').multiselect('clearSelection');
          $('#slct_actividad_edit').multiselect('clearSelection');
          var tipozona='';
          if(polygon.tipo_zona!=='') tipozona=polygon.tipo_zona.split("|");
          $('#slct_tipo_zona_edit_id').multiselect('select', tipozona);
          $('#slct_tipo_zona_edit_id').multiselect('rebuild');
          $('#slct_actividad_edit').multiselect('select', polygon.actividad);
          $('#slct_actividad_edit').multiselect('rebuild');
        }

         $('.picker').colpick({
              layout: 'hex',
              submit: 0,
              colorScheme: 'dark',
              onChange: function(hsb, hex, rgb, el, bySetColor) {
                  $(el).css('border-color', '#' + hex);
                  // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                  if (!bySetColor) {
                    $(el).val(hex);
                  }
              }
          }).keyup(function() {
              $(this).colpickSetColor(this.value);
          });

          $('#btn_UpdateGeoPlan').on('click', function() {
              polygon.setOptions({
                        strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                        fillOpacity: $("#singlePolygonOpacity").val() //opacidad
                    });

              if (polygon.tipo==4) {
                polygon.setOptions({
                        detalle: $("#txt_detalle").val(), //nombre
                        tipo_zona: $("#slct_tipo_zona_edit_id").val(),
                        actividad: $("#slct_actividad_edit").val()
                    });
              }
              if (polygon.tipo==1 || polygon.tipo==2 || polygon.tipo==4) {
                       polygon.setOptions({
                           strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                           strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                           fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                           fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                       });
                     }
              if (polygon.tipo==3 ){
                       var markTextColor = textByColor("#" 
                                          + $("#singlePolygonFill")
                                          .val())
                                          .substring(1);

                       iconSelected = googleMarktxt 
                                  + polygon.numero 
                                  + "|" 
                                  + $("#singlePolygonFill").val() 
                                  + "|" 
                                  + markTextColor;

                       polygon.setOptions({
                           icon: iconSelected
                       });
                     }

              UpdateGeoPlan();
          });
    }

    function UpdateGeoPlan() {
       var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
              if(rs){
                 Proyectos.UpdateGeoPlan(globalElement.id, globalElement.idproyecto,globalElement.tipo);
              }
    }

    function deleteUpdatePlanPoly() { //eliminar elemento
       var rs=confirm("Se eliminará la zona ¿Desea continuar?");
              if(rs){
                 globalElement.setMap(null);
                 infoWindow.close(objMap);
                 Proyectos.deleteUpdatePlanPoly(globalElement.id, globalElement.idproyecto);
              }
    }

    function EliminarCapas(id){
       var rs=confirm("Se eliminará la capa seleccionada, ¿Desea Continuar?");
       if(rs){
           Proyectos.EliminarCapas(id);
       }
    }

    function EliminarProyecto(id){
       var rs=confirm("Se eliminará el proyecto seleccionada, ¿Desea Continuar?");
       if(rs){
           updateproyect(id,'0','1')
       }
    }

    function updateproyect(idproyecto,valor,campo){
        Proyectos.Updateproyect(idproyecto,valor,campo);
    }

    function cargarKML(file){
       //var src='http://psiweb.ddns.net:7020/kml/jenn.kml';
       var src=window.location.origin+'/'+file;
       kmlLayer(src, objMap, '') ; 
       Proyectos.FinalizarKml(file);   
       
    }

    function EditarPoligono(){
      var rs=confirm("¿Desea guardar los cambios?");
       if(rs){
        data={/*borde:'#'+$("#form_proyectocapa #pickerPolyLine_edit").val(),
            fondo:'#'+$("#form_proyectocapa #pickerPolyBack_edit").val(),
            grosorlinea:$("#form_proyectocapa #polyLineCreate_edit").val(),
            opacidad:$("#form_proyectocapa #sliderPolyOpac_edit" ).slider( "value" ),*/
            capaid:$("#form_proyectocapa #id").val(),
            nomcapa:$("#form_proyectocapa #txt_nombre").val()};
        Proyectos.EditarCapa(data);
      }
    }

    function EditarPunto(){
        data={/*borde:'',
            fondo:'#'+$("#form_proyectocapa #pickerMarkBack_edit").val(),
            grosorlinea:'',
            opacidad:'',*/
            capaid:$("#form_proyectocapa #id").val(),
            nomcapa:$("#form_proyectocapa #txt_nombre").val()};

        Proyectos.EditarCapa(data);
    }

    function CargarHTML(obj){
          // 1: Multiple | 0: Simple
          idtipo=1;
          tipo='simple';
          if(idtipo==1){
              tipo="multiple";
          }
      
          $(".add").remove();
          var html="";
          $(".selectdinamico").multiselect('destroy');
          for (var i=1; i<= obj.cant; i++) {
              data={c:i,tabla_id:table,t:obj.cant};
              changue='';
              if(i<obj.cant){
                  if(idtipo==0){
                      changue=' onChange="CargarDetalle('+"'"+i+','+obj.cant+','+table+','+idtipo+"'"+')" ';
                  }
                  else{
                      changue=' data-hidden="'+i+','+obj.cant+','+table+','+idtipo+'" ';
                  }
              }
              if(i==obj.cant){
                  if(idtipo==1){
                    changue=' onChange="opcionSelected('+i+')" ';
                  }
              }
      
              tiposelect='';
              if(tipo=='multiple'){
                  tiposelect='multiple'
              }
      
              html='  <div class="add col-sm-12">'+
                          '<label>'+obj.c_detalle.split('|')[(i-1)]+':</label>'+
                          '<select class="selectdinamico form-control" '+changue+' name="slct_c'+i+'" id="slct_c'+i+'" '+tiposelect+'>'+
                          '    <option value="">.::Seleccione::.</option>'+
                          '</select>'+
                      '</div>';
              $("#selects").append(html);
              if(i>1){
                  slctGlobalHtml('slct_c'+i, tipo);
              }
              if(i==1){
                  if(idtipo==0){
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data);
                  }
                  else{
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
                  }
              }
          };
    }

    function CargarDetalle(cod){
          var id=cod.split(',')[0];
          var t=cod.split(',')[1];
          var tabla_id=cod.split(',')[2];
          var idtipo=cod.split(',')[3];
      
          if($("#slct_c"+id).val()!=null && $("#slct_c"+id).length>0 && $("#slct_c"+id).val()!=''){
          id++;
          var valor=[];
      
              for (var i = 1; i < id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
              var data={valor:valor,c:id,t:t,tabla_id:tabla_id,idtipo:idtipo};
              $('#slct_c'+id).multiselect('destroy');
      
              if(idtipo==0){
                  tipo='simple';
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data);
              }
              else{
                  tipo="multiple";
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
              }
          }
    }



</script>