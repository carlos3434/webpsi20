<script type="text/javascript">
    var componentesObj = [];
    var componentes;
    var Componentes = {
        CargarComponentes: function (evento) {
            $.ajax({
                url: 'componente/cargar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        componentesObj = obj.datos;
                        $("#tb_componentes").html(obj.datos);
                    } else
                        $("#tb_componentes").html('');
                },
                error: function () {
                    $("#tb_componentes").html('');
                    eventoCargaRemover();
                }
            });
        },
        buscarComponentes: function () {
            var datos = $("#form_componentes").serialize();
            
            $.ajax({
                url: "componente/buscar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                    $("#resultado").html('');
                },
                success: function (obj) {
                    eventoCargaRemover();
                    limpiarComponentes();

                    if (obj.rst == 1) {
                        marcarComponentes(obj.datos);
                        $("#resultado").html(obj.tabla);
                    } else if (obj.rst == 0){
                        Psi.mensaje('warning', obj.datos, 6000);
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    eventoCargaRemover(); 
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        BuscarDireccion: function (dat) {
            var datos = dat;
            var accion = "georeferencia/buscar";

            bounds = new google.maps.LatLngBounds();
            $.ajax({
                url: accion,
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function (obj) {
                    $(".overlay,.loading-img").remove();
                    
                    if (obj.length > 0) {
                        var yx = obj[0]['XY'];
                        var array_yx = yx.split(",");
                        var x = array_yx[1];
                        var y = array_yx[0];
                        var content = document.getElementById("ubicacion"); 
                        var ubicacion = x +','+ y;
                        initMap(x,y);
                        content.value = (ubicacion);
                    } else Psi.mensaje('warning', 'No se encontro resultado', 6000);
                },
                error: function () {
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'No se encontro resultado', 6000);

                }
            });
        },
        BuscarCalle: function () {
        var calle = $.trim($("#txt_calle_nombre").val());
        var numero = $.trim($("#txt_calle_numero").val());

        if (calle == '')
        {
            Psi.mensaje('danger', 'Ingrese nombre de calle, Av., Jr.', 5000);
            return false;
        }
        
        $.ajax({
            url: 'tramo/buscar',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: 'calle=' + calle
                    + '&numero=' + numero
                    + '&distrito=' + $("#calle_distrito").val(),
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(obj) {
                $(".overlay,.loading-img").remove();
                if (obj.rst == 1) {
                    if (obj.datos.length > 0) {
                        var x = ((parseFloat(obj.datos[0]['XA'])+parseFloat(obj.datos[0]['XB']))/2);
                        var y = ((parseFloat(obj.datos[0]['YA'])+parseFloat(obj.datos[0]['YB']))/2);
                        var content = document.getElementById("ubicacion"); 
                        var ubicacion = x +','+ y;
                        initMap(x,y);
                        content.value = (ubicacion);
                    } else Psi.mensaje('warning', 'No se encontro resultado', 6000);

                }
            },
            error: function() {
                $(".overlay,.loading-img").remove();

                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
        }
    };
</script>