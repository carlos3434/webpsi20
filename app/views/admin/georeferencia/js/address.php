<script type="text/javascript">

    //z-index
    var zIndexItem = 0;
    /**
     * Ubicaciones actuales (finales) de tecnicos como Google Markers
     * @type Array|@exp;marker
     */
    var tecMark = {};

    /**
     * Lista de actuaciones por tecnico
     * @type Array
     */
    var tecActu = [];

    /**
     * Lista de actuaciones temporales, sin gestión
     * @type Array
     */
    var tmpActu = [];

    /**
     * Mostrar trafico en Google Maps
     *
     * @type google.maps.TrafficLayer
     */
    var trafficLayer;

    /**
     * Ruta de un tecnico segun su ubicacion
     * @type Array
     */
    var tecPath = [];

    /**
     * Coleccion de todos los elementos del mapa
     * @type Array
     */
    var mapObjects = {};

    /**
     * Mostrar herramientas de dibujo Google Maps
     *
     * @type Boolean
     */

    var mapTools = {draw: false}

    var markers = [];

    var addressContent = [];

    $(document).ready(function () {
        $('#tabsBusqueda a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $("[data-toggle='offcanvas']").click();

        // Variables
        var objMain = $('#main');

        // Show sidebar
        function showSidebar() {
            objMain.addClass('use-sidebar');
            mapResize();
        }

        // Hide sidebar
        function hideSidebar() {
            objMain.removeClass('use-sidebar');
            mapResize();
        }

        // Sidebar separator
        var objSeparator = $('#separator');

        objSeparator.click(function (e) {
            e.preventDefault();
            if (objMain.hasClass('use-sidebar')) {
                hideSidebar();
            }
            else {
                showSidebar();
            }
        }).css('height', objSeparator.parent().outerHeight() + 'px');

        //Iniciar mapa
        objMap = doObjMap("mymap", objMapProps, mapTools);

        $("#btn_buscar").click(function () {
            var datos = $("#form_visorgps").serialize().split("txt_").join("").split("slct_").join("");
            Funciones.BuscarDireccion(datos);
        });
        $("#btn_buscarUnica").click(function () {
            var datos = $("#form_busquedaUnica #txt_direccion").val();
            var calle = "";
            var numero = 0;
            var distrito = $("#slct_distrito_unica").val();
            datos = datos.split(" ");
            for (var i = 0; i < datos.length; i++) {

                if (isNaN(datos[i])) {
                    calle += datos[i] + " ";
                } else {
                    numero = datos[i];
                    break;
                }
                //Do something
            }
            datos = {calle: calle, numero: numero, distrito: distrito};
            Funciones.BuscarDireccion(datos);
            //Funciones.BuscarDireccion();
        });

        $("#btn_buscarMasiva").click(function () {
            Funciones.BuscarMasiva();
        });

        $(".botonExcel").click(function (event) {
            //$("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
            //Funciones.DescargarExcel();
            window.open('data:application/vnd.ms-excel,' + $('#tablaN').html());
            event.preventDefault();
            //$("#FormularioExportacion").submit();
        });

        $("#btn_kml").click(function () {
            Map.generateKML(markers);
        });

        $("#btn_kml_upload").click(function () {
            Map.uploadKML(objMap, "form_kml");
        });

        $('html').on('click', function (e) {
            if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
                $('[data-original-title]').popover('hide');
            }
        });
    });

    function addressToMap(data, coddis, calle, ndir, masivo) {
        var neach = 1;
        var newCoordIni;
        var newCoordEnd;
        var xDir;
        var yDir;
        var pt;
        var addressData = [];
        var markTextColor;
        var bounds;
        bounds = new google.maps.LatLngBounds();
        if (data.length === 0 && masivo === false) {
            alert("No se encontraron resultados");
            $("body").removeClass("loading");
        } else if (data.length > 0) {


            $.each(data, function () {
                polyLineCoords = [];

                var N = Number(this.numero);
                //Coordenadas inicio y fin
                newCoordIni = new google.maps.LatLng(this.YA, this.XA);
                newCoordEnd = new google.maps.LatLng(this.YB, this.XB);

                //Punto indicador de direccion
                xDir = Number(this.XB) + ((-Number(this.NUM_IZQ_FI) + N) * (-Number(this.XB) + Number(this.XA)) / (-Number(this.NUM_IZQ_FI) + Number(this.NUM_DER_IN)));
                yDir = Number(this.YB) + ((-Number(this.NUM_IZQ_FI) + N) * (-Number(this.YB) + Number(this.YA)) / (-Number(this.NUM_IZQ_FI) + Number(this.NUM_DER_IN)));


                pt = new google.maps.LatLng(this.YA, this.XA);
                bounds.extend(pt);
                pt = new google.maps.LatLng(this.YB, this.XB);
                bounds.extend(pt);

                //Marcador solo si el numero es valido
                if (N > 0) {
                    addressData = [];
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(yDir, xDir),
                        //map: null,
                        animation: google.maps.Animation.DROP
                    });

                    markers.push(marker);

                    var content = '<table>';
                    content += '<tr><td>ID</td><td>' + this.id + '</td></tr>';
                    content += '<tr><td>X</td><td>' + xDir + '</td></tr>';
                    content += '<tr><td>Y</td><td>' + yDir + '</td></tr>';
                    content += '<tr><td>Ubicacion</td><td>' + this.NOM_VIA_TR + '</td></tr>';
                    content += '</table>';

                    addressContent.push(content);
                    var infowindow = new google.maps.InfoWindow({
                        content: content
                    });

                    google.maps.event.addListener(marker, "click", function (event) {
                        infowindow.open(objMap, marker);
                    });

                    if (coddis === '') {
                        coddis = this.COD_DPTO + this.COD_PROV + this.COD_DIST;
                    }
                    if (calle === '') {
                        calle = this.NOM_VIA_TR;
                    }

                    addressData.push(coddis);
                    addressData.push(calle);
                    addressData.push(N);
                    addressData.push(xDir);
                    addressData.push(yDir);
                }

                //Orden visibilidad
                zIndexItem++;

                //Mostrar o no Inicio y Fin del tramo
                if ($("#showPathMarker").prop("checked") === true) {
                    //Marcadores inicio y fin de recta
                    markTextColor = textByColor("#"
                        + $("#pickerAddressLine")
                            .val())
                        .substring(1);

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(this.YA, this.XA),
                        //map: null,
                        icon: googleMarktxt
                        + "I|"
                        + $("#pickerAddressLine").val()
                        + "|"
                        + markTextColor,
                        animation: google.maps.Animation.DROP,
                        zIndex: zIndexItem
                    });

                    markers.push(marker);

                    var content = '<table>';
                    content += '<tr><td>X</td><td>' + this.XA + '</td></tr>';
                    content += '<tr><td>Y</td><td>' + this.YA + '</td></tr>';
                    content += '<tr><td>Ubicacion</td><td>' + this.NOM_VIA_TR + '</td></tr>';
                    content += '</table>';
                    addressContent.push(content);
                    var infowindow = new google.maps.InfoWindow({
                        content: content
                    });

                    google.maps.event.addListener(marker, "click", function (event) {
                        infowindow.open(objMap, marker);
                    });

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(this.YB, this.XB),
                        map: objMap,
                        icon: googleMarktxt
                        + "F|"
                        + $("#pickerAddressLine").val()
                        + "|"
                        + markTextColor,
                        animation: google.maps.Animation.DROP,
                        zIndex: zIndexItem
                    });

                    markers.push(marker);

                    var content = '<table>';
                    content += '<tr><td>X</td><td>' + this.XB + '</td></tr>';
                    content += '<tr><td>Y</td><td>' + this.YB + '</td></tr>';
                    content += '<tr><td>Ubicacion</td><td>' + this.NOM_VIA_TR + '</td></tr>';
                    content += '</table>';
                    addressContent.push(content);
                    var infowindow = new google.maps.InfoWindow({
                        content: content
                    });

                    google.maps.event.addListener(marker, "click", function (event) {
                        infowindow.open(objMap, marker);
                    });
                }

                //Creando path
                polyLineCoords.push(newCoordIni);
                polyLineCoords.push(newCoordEnd);

                var addressPath = new google.maps.Polyline({
                    path: polyLineCoords,
                    geodesic: true,
                    strokeColor: '#' + $("#pickerAddressLine").val(),
                    strokeOpacity: 1.0,
                    strokeWeight: $("#polyLineAddress").val(),
                    zIndex: zIndexItem
                });

                var lineData = this;

                //Infowindow del tramo
                google.maps.event.addListener(addressPath, "click", function (event) {

                    var content = "";

                    content += '<table>';
                    content += '<tr><td>COD DEP</td><td>' + lineData.COD_DPTO + '</td></tr>';
                    content += '<tr><td>COD PRO</td><td>' + lineData.COD_PROV + '</td></tr>';
                    content += '<tr><td>COD DIS</td><td>' + lineData.COD_DIST + '</td></tr>';
                    content += '<tr><td>NOM VIA</td><td>' + lineData.NOM_VIA_TR + '</td></tr>';
                    content += '<tr><td>NUM DER IN</td><td>' + lineData.NUM_DER_IN + '</td></tr>';
                    content += '<tr><td>NUM DER FI</td><td>' + lineData.NUM_DER_FI + '</td></tr>';
                    content += '<tr><td>NUM IZQ IN</td><td>' + lineData.NUM_IZQ_IN + '</td></tr>';
                    content += '<tr><td>NUM IZQ FI</td><td>' + lineData.NUM_IZQ_FI + '</td></tr>';
                    content += '<tr><td>XA</td><td>' + lineData.XA + '</td></tr>';
                    content += '<tr><td>YA</td><td>' + lineData.YA + '</td></tr>';
                    content += '<tr><td>XB</td><td>' + lineData.XB + '</td></tr>';
                    content += '<tr><td>YB</td><td>' + lineData.YB + '</td></tr>';
                    content += '</table>';

                    infowindow.content = content;
                    infowindow.position = newCoordIni;
                    infowindow.open(objMap);
                });

                addressPath.setMap(objMap);
                //map.fitBounds(bounds);

                // Remove loading
                if (neach === data.length && masivo === false) {
                    $("body").removeClass("loading");
                }

                neach++;
            });

            objMap.fitBounds(bounds);
        }
    }
    function markerPathInfoWindow(event) {
        var content = "";
        var href = '<a href="http://maps.google.com?q='
            + event.latLng.lat()
            + ','
            + event.latLng.lng()
            + '" target="_blank">Go</a>';

        content += '<table>';
        content += '<tr><td>X</td><td>' + event.latLng.lng() + '</td></tr>';
        content += '<tr><td>Y</td><td>' + event.latLng.lat() + '</td></tr>';
        content += '<tr><td>Ubicacion</td><td>' + href + '</td></tr>';
        content += '</table>';

        infowindow.setPosition(event.latLng);
        infowindow.setContent(content);
        infowindow.open(self.map);
    }

    function addressInfoWindow(data, event) {
        var content = "";
        var href = '<a href="http://maps.google.com?q='
            + data[4]
            + ','
            + data[3]
            + '" target="_blank">Go</a>';

        content += '<table>';
        content += '<tr><td>Distrito</td><td>' + data[0] + '</td></tr>';
        content += '<tr><td>Calle</td><td>' + data[1] + '</td></tr>';
        content += '<tr><td>Numero</td><td>' + data[2] + '</td></tr>';
        content += '<tr><td>X</td><td>' + data[3] + '</td></tr>';
        content += '<tr><td>Y</td><td>' + data[4] + '</td></tr>';
        content += '<tr><td>Ubicacion</td><td>' + href + '</td></tr>';
        content += '</table>';

        infowindow.setPosition(event.latLng);
        infowindow.setContent(content);
        infowindow.open(self.map);
    }
</script>
