<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('https://maps.google.com/maps/api/js?sensor=false&libraries=drawing&key='.Config::get('wpsi.map.key').'') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.js.kml' )
@include( 'admin.georeferencia.js.address' )
@include( 'admin.georeferencia.js.address_ajax' )
@stop
        <!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
    <style>
        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .clearer {
            clear: both;
            display: block;
            font-size: 0;
            height: 0;
            line-height: 0;
        }

        /*
            Misc
        ------------------------------------------------------------------- */

        .hidden {
            display: none;
        }

        /*
            Example specifics
        ------------------------------------------------------------------- */

        /* Layout */

        #center-wrapper {
            margin: 0 auto;
            width: 920px;
        }

        /* Content & sidebar */

        #mymap, #sidebar {
            text-align: center;
            border: 1px solid;
            height: 600px;
        }

        #sidebar {
            text-align: left;
            background-color: #DEF;
            border-color: #BCD;
            display: none;
            overflow: auto;
        }

        #mymap {
            background-color: #EFE;
            border-color: #CDC;
            width: 98%;
        }

        .use-sidebar #mymap {
            width: 69%;
        }

        .use-sidebar #sidebar {
            display: block;
            width: 27%;
        }

        .sidebar-at-left #sidebar {
            margin-right: 1%;
        }

        .sidebar-at-right #sidebar {
            margin-left: 1%;
        }

        .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {
            float: right;
        }

        .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {
            float: left;
        }

        #separator {
            background-color: #EEE;
            border: 1px solid #CCC;
            display: block;
            outline: none;
            width: 1%;
        }

        .use-sidebar #separator {
            background: url('img/separator/vertical-separator.gif') repeat-y center top;
            border-color: #FFF;
        }

        #separator:hover {
            border-color: #ABC;
            background: #DEF;
        }

        .content .tab-content div {
            margin-top: 20px;
        }
    </style>

    <script src="js/geo/geo.functions.js"></script>
    <script src="js/utils.js"></script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Direcciones
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Historico</a></li>
            <li class="active">Bandeja de Gestión</li>
        </ol>
    </section>

    <section>

    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Nav tabs -->
        <ul id="tabsBusqueda" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#individual" aria-controls="individual" role="tab"
                                                      data-toggle="tab">Busqueda Individual</a></li>
            <li role="presentation"><a href="#masiva" aria-controls="masiva" role="tab" data-toggle="tab">Busqueda
                    Masiva</a></li>
            <li role="presentation"><a href="#tramos" aria-controls="tramos" role="tab" data-toggle="tab">Tramos</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="individual">
                <form class="form-inline" style="margin-bottom: 10px;" id="form_busquedaUnica">
                    <div class="form-group">
                        <label for="txt_direccion">Direccion</label>
                        <input type="text" class="form-control input-lg" id="txt_direccion"
                               placeholder="Ingrese direccion" style="width: 800px;">
                    </div>
                    <div class="form-group">
                        <button id="btn_buscarUnica" type="button" class="btn btn-default">Buscar</button>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="slct_distrito[]" id="slct_distrito_unica">
                            <option value="150100"> - Todo Lima -</option>
                            <option value="150101">Lima Cercado</option>
                            <option value="150102">Ancon</option>
                            <option value="150103">Ate</option>
                            <option value="150104">Barranco</option>
                            <option value="150105">Breña</option>
                            <option value="150106">Carabayllo</option>
                            <option value="150107">Chaclacayo</option>
                            <option value="150108">Chorrillos</option>
                            <option value="150109">Cieneguilla</option>
                            <option value="150110">Comas</option>
                            <option value="150111">El Agustino</option>
                            <option value="150112">Independencia</option>
                            <option value="150113">Jesus María</option>
                            <option value="150114">La Molina</option>
                            <option value="150115">La Victoria</option>
                            <option value="150116">Lince</option>
                            <option value="150117">Los Olivos</option>
                            <option value="150118">Lurigancho</option>
                            <option value="150119">Lurin</option>
                            <option value="150120">Magdalena del Mar</option>
                            <option value="150121">Pueblo Libre</option>
                            <option value="150122">Miraflores</option>
                            <option value="150123">Pachacamac</option>
                            <option value="150124">Pucusana</option>
                            <option value="150125">Puente Piedra</option>
                            <option value="150126">Punta Hermosa</option>
                            <option value="150127">Punta Negra</option>
                            <option value="150128">Rimac</option>
                            <option value="150129">San Bartolo</option>
                            <option value="150130">San Borja</option>
                            <option value="150131">San Isidro</option>
                            <option value="150132">San Juan De Lurigancho</option>
                            <option value="150133">San Juan de Miraflores</option>
                            <option value="150134">San Luis</option>
                            <option value="150135">San Martin de Porres</option>
                            <option value="150136">San Miguel</option>
                            <option value="150137">Santa Anita</option>
                            <option value="150138">Santa Maria del Mar</option>
                            <option value="150139">Santa Rosa</option>
                            <option value="150140">Santiago de Surco</option>
                            <option value="150141">Surquillo</option>
                            <option value="150142">Villa el Salvador</option>
                            <option value="150143">Villa Maria del Triunfo</option>
                            <option value="070100"> - Todo Callao -</option>
                            <option value="070101">Callao</option>
                            <option value="070102">Bellavista</option>
                            <option value="070103">Carmen de la Legua Reynoso</option>
                            <option value="070104">La Perla</option>
                            <option value="070105">La Punta</option>
                            <option value="070106">Ventanilla</option>
                        </select>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="masiva">
                <input type="button" name="updirfile" id="btn_buscarMasiva" value="Buscar" class="btn btn-default"/>
            </div>
            <div role="tabpanel" class="tab-pane" id="tramos">
                <form name="form_visorgps" id="form_visorgps" method="post">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="txt_calle" id="txt_calle"
                                       placeholder="Nombre calle, av., jir&oacute;n">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="txt_numero" id="txt_numero"
                                       placeholder="N&uacute;mero">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="slct_distrito[]" id="slct_distrito">
                                    <option value="150100"> - Todo Lima -</option>
                                    <option value="150101">Lima Cercado</option>
                                    <option value="150102">Ancon</option>
                                    <option value="150103">Ate</option>
                                    <option value="150104">Barranco</option>
                                    <option value="150105">Breña</option>
                                    <option value="150106">Carabayllo</option>
                                    <option value="150107">Chaclacayo</option>
                                    <option value="150108">Chorrillos</option>
                                    <option value="150109">Cieneguilla</option>
                                    <option value="150110">Comas</option>
                                    <option value="150111">El Agustino</option>
                                    <option value="150112">Independencia</option>
                                    <option value="150113">Jesus María</option>
                                    <option value="150114">La Molina</option>
                                    <option value="150115">La Victoria</option>
                                    <option value="150116">Lince</option>
                                    <option value="150117">Los Olivos</option>
                                    <option value="150118">Lurigancho</option>
                                    <option value="150119">Lurin</option>
                                    <option value="150120">Magdalena del Mar</option>
                                    <option value="150121">Pueblo Libre</option>
                                    <option value="150122">Miraflores</option>
                                    <option value="150123">Pachacamac</option>
                                    <option value="150124">Pucusana</option>
                                    <option value="150125">Puente Piedra</option>
                                    <option value="150126">Punta Hermosa</option>
                                    <option value="150127">Punta Negra</option>
                                    <option value="150128">Rimac</option>
                                    <option value="150129">San Bartolo</option>
                                    <option value="150130">San Borja</option>
                                    <option value="150131">San Isidro</option>
                                    <option value="150132">San Juan De Lurigancho</option>
                                    <option value="150133">San Juan de Miraflores</option>
                                    <option value="150134">San Luis</option>
                                    <option value="150135">San Martin de Porres</option>
                                    <option value="150136">San Miguel</option>
                                    <option value="150137">Santa Anita</option>
                                    <option value="150138">Santa Maria del Mar</option>
                                    <option value="150139">Santa Rosa</option>
                                    <option value="150140">Santiago de Surco</option>
                                    <option value="150141">Surquillo</option>
                                    <option value="150142">Villa el Salvador</option>
                                    <option value="150143">Villa Maria del Triunfo</option>
                                    <option value="070100"> - Todo Callao -</option>
                                    <option value="070101">Callao</option>
                                    <option value="070102">Bellavista</option>
                                    <option value="070103">Carmen de la Legua Reynoso</option>
                                    <option value="070104">La Perla</option>
                                    <option value="070105">La Punta</option>
                                    <option value="070106">Ventanilla</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="button" class="form-control" value="Buscar" name="btn_buscar"
                                       id="btn_buscar">
                            </div>
                            <div class="col-sm-2">

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div style="margin:15px">

            <form id="form_kml" class="form-inline">
                <div class="form-group">
                    <button type="button" class="btn btn-danger" id="btn_kml" rel=popover><i class="fa fa-download"></i>
                        Exportar KML
                    </button>
                </div>
                <div class="form-group">
                    <input type="file" name="kml" id="kml_file">
                </div>
                <div class="form-group">
                    <button type="button" id="btn_kml_upload" class="btn btn-success"><i class="fa fa-upload"></i>
                        Importar KML
                    </button>
                </div>
            </form>
        </div>

        <div class="use-sidebar sidebar-at-left" id="main">

            <div id="mymap">Content</div>

            <div id="sidebar">
                <h2>No encontrados</h2>

                <div id="tablaN">
                    <table>
                        <thead>
                        <tr>
                            <th>N</th>
                            <th>ID</th>
                            <th>Direccion</th>
                            <th>Numero</th>
                        </tr>
                        </thead>
                        <tbody id="notFound"></tbody>
                    </table>
                </div>
                <form method="post" target="_blank" id="FormularioExportacion">
                    <p>
                        <button type="button" class="botonExcel btn btn-primary">Exportar a Excel</button>
                    </p>
                    <input type="hidden" id="datos_a_enviar" name="datos_a_enviar"/>
                </form>
            </div>

            <a href="#" id="separator"></a>

            <div class="clearer">&nbsp;</div>

        </div>
    </section><!-- /.content -->
    <div class="modal fade" id="modalDescarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Descargar KML</h4>
                </div>
                <div class="modal-body">
                    Desea descarga el archivo kml
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <div id="modalBody"></div>
                </div>
            </div>
        </div>
    </div>
@stop
