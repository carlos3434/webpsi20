<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent

{{ HTML::script('js/admin/legado/pruebas.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )


<!--{{ HTML::style('lib/bootstrap-fileinput/fileinput.css') }} -->
<!--{{ HTML::script('lib/bootstrap-fileinput/fileinput.min.js') }}-->

@stop
        <!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pruebas
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Legado</a></li>
            <li class="active">Pruebas</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Inicia contenido -->
                <div class="box" id="">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <br>
                    <div class="row">
                      <form class="form-inline" style="margin-bottom: 100px; min-height: 200px" id="form_analizador">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <div class="form-group">
                                <div class="col-sm-10">
                                  <label>Telefono</label>
                                  <input class="form-control" type="text" id="txt_codarea" name="txt_codarea"  style="width:70px"><b>-</b>
                                  <input class="form-control" type="text" id="txt_telefono" name="txt_telefono">
                                </div>
                                <div class="col-sm-2">
                                  <input class="form-control btn-success" type="button" id="btn_obtener" name="btn_obtener" value="Realizar Pruebas" onclick="RealizarPrueba2()">
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-md-12">
                          <div class="form-group col-md-12 col-sm-12 col-xs-12">
                              <div class="panel-group" id="accordion">
                        <div class="panel panel-info">
                           <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel" style="cursor: pointer">
                              <h4 class="panel-title">
                                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel">Resultados PCBA</a>
                              </h4>
                           </div>
                           <div id="collapse_cliente_panel" class="panel-collapse collapse in" aria-expanded="true">
                              <div class="panel-body">
                                 <div class="row" >
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="tablaspcba">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel panel-info">
                           <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_datos_panel" style="cursor: pointer">
                              <h4 class="panel-title">
                                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse_datos_panel">Resultados Analizador</a>
                              </h4>
                           </div>
                           <div id="collapse_datos_panel" class="panel-collapse collapse" aria-expanded="true">
                              <div class="panel-body">
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <textarea class="form-control" rows="10" cols="105" id="txt_resultado" style="resize: none;"></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel panel-info">
                           <div class="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_servicio_panel" style="cursor: pointer">
                              <h4 class="panel-title">
                                 <a data-toggle="collapse" data-parent="#accordion" href="#collapse_servicio_panel">Resultados Assia</a>
                              </h4>
                           </div>
                           <div id="collapse_servicio_panel" class="panel-collapse collapse" aria-expanded="true">
                              <div class="panel-body">
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                      <textarea class="form-control" rows="10" cols="105" id="txt_resultadoassia" style="resize: none;"></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                          </div>
                        </div>
                      </form>
                    </div>
                </div><!-- /.box -->
                <!-- Finaliza contenido -->
            </div>
        </div>
        
  
    </section><!-- /.content -->
@stop
