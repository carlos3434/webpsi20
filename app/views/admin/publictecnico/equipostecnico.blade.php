<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent

{{ HTML::style('css/sweetalert/sweetalert.css') }}
{{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/backbone/js/json2.js') }}
{{ HTML::script('lib/backbone/js/underscore.min.js') }}
{{ HTML::script('lib/backbone/js/backbone.min.js') }}
{{ HTML::script('lib/backbone/js/handlebars.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )

@stop
<style type="text/css">
.legendblade {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
    border-image: none;
    border-style: none none solid;
    border-width: 0 0 1px;
    color: #333;
    display: block;
    border-style: none;
    width: 30%;
    font-size: 18px;
    margin: 5px;
}
.fieldsetblase {
    border: 1px solid silver;
    padding: 0.35em 0 0.75em;
    margin: 15px;
}
</style>
@section('contenido')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Equipos Técnico
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Técnico</a></li>
            <li class="active">Equipos Técnico</li>
        </ol>
    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Inicia contenido -->
                <div class="box" id="">
                    <div class="box-header">
                        <h3 class="box-title">Listado de Equipos Técnico</h3>
                    </div><!-- /.box-header -->
                    <form id="form_busqueda" name="form_busqueda">
                    <div class="row" style="margin:20px 0px">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label>Tipo Material</label>
                                <select class="form-control" name="slct_tipmat[]" id="slct_tipmat" multiple="">
                                        <option value="EQ" >Equipo</option>
                                       <option value="MA" >Material</option>
                                       <option value="MC" >MC</option>
                                       <option value="TR" >TR</option>
                                       <option value="RV" >RV</option>
                                       <option value="UN" >UN</option>
                                </select>
                            </div>
                            <div class="col-md-4" >
                                <label>Material</label>
                                <select class="form-control" name="slct_material[]" id="slct_material" multiple="">
                                </select>
                            </div>
                            <div class="col-md-3" >
                                <label>Fecha Registro</label>
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-calendar"></i>
                                  </div>
                                  <input id="txt_created_at" class="form-control pull-right" type="text" readonly="" name="txt_created_at">
                                  <div class="input-group-addon" onclick="cleardate()" style="cursor: pointer"><i class="fa fa-rotate-left"></i></div>
                                  </div>
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-12" style="margin-top:25px">
                                <button type="button" class="btn btn-success" id="btnbusqueda">Buscar</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                                <table id="t_listado" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Carnet Tecnico</th>
                                        <th>Cód. Mat.</th>
                                        <th>Material</th>
                                        <th>T. Mat</th>
                                        <th>Cantidad</th>
                                        <th>Fecha Reg.</th>
                                        <th>Estado</th>
                                        <th>[]</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_listado">
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Carnet Tecnico</th>
                                        <th>Cód. Mat.</th>
                                        <th>Material</th>
                                        <th>T. Mat</th>
                                        <th>Cantidad</th>
                                        <th>Fecha Reg.</th>
                                        <th>Estado</th>
                                        <th>[]</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!-- /.col -->
                    </div>
                    <div class="row" >
                        <div class="col-md-12">
                            <div class="col-md-12" style="margin-bottom:10px">
                            <a id="nuevo" class="btn btn-primary btn-sm" data-titulo="Nuevo" data-target="#materialtecnicoModal" data-toggle="modal"><i class="fa fa-plus fa-lg"></i>Nuevo</a>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
                <!-- Finaliza contenido -->
            </div>
        </div>
        
    
    </section><!-- /.content -->

    @section('javascript')
        {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
        {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
        {{ HTML::script('js/sweetalert/sweetalert.js') }}
        {{ HTML::script('js/tecnico/materialtecnico.js') }}
        {{ HTML::script('js/tecnico/materiales_ajax.js') }}
    @show
@stop


@section('formulario')
     @include( 'admin.publictecnico.form.materialtecnico' )
@stop