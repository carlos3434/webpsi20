<!-- /.modal -->
<div class="modal fade" id="materialtecnicoModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header logo">
            <button class="btn btn-sm btn-default pull-right" data-dismiss="modal" id="btn_close">
               <i class="fa fa-close"></i>
            </button>
            <h4 class="modal-title">New message</h4>
         </div>
         <div class="modal-body">
            
            <form id="form_materiales">
               <div class="row form-group">
                     
                            <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                  <label>Tipo Material</label>
                                  <select class="form-control" id="slct_tipo_material" multiple="">
                                       <option value="EQ" >Equipo</option>
                                       <option value="MA" >Material</option>
                                       <option value="MC" >MC</option>
                                       <option value="TR" >TR</option>
                                       <option value="RV" >RV</option>
                                       <option value="UN" >UN</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                  <label>Material</label>
                                  <select class="form-control" name="slct_material_id" id="slct_material_id">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                  <label>Serie</label>
                                  <input class="form-control" type="text" id="txt_serie" name="txt_serie">
                                </div>
                                <div class="col-sm-6">
                                  <label>Cantidad</label>
                                  <input class="form-control" type="text" id="txt_cantidad" name="txt_cantidad">
                                </div>
                            </div>
                            </div>
                     
               </div><!-- /.col -->
            </form>
         </div>
         <div class="modal-footer">
                <button type="button" class="btn btn-success">Guardar Material</button>
         </div>
      </div>
   </div>
</div>