<!-- /.modal -->
<div class="modal fade" id="materialModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header logo">
            <button class="btn btn-sm btn-default pull-right" data-dismiss="modal" id="btn_close">
               <i class="fa fa-close"></i>
            </button>
            <h4 class="modal-title">New message</h4>
         </div>
         <div class="modal-body">
            
            <form id="form_materiales">
               <div class="row form-group">
                     <fieldset class="fieldsetblase">
                            <legend class="legendblade">Registro de Materiales</legend>
                            <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                  <label>Tipo Material</label>
                                  <select class="form-control" id="slct_tipo_material" >
                                       <option value="EQ" >Equipo</option>
                                       <option value="MA" >Material</option>
                                       <option value="MC" >MC</option>
                                       <option value="TR" >TR</option>
                                       <option value="RV" >RV</option>
                                       <option value="UN" >UN</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                  <label>Material</label>
                                  <select class="form-control" name="slct_material_id" id="slct_material_id">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                  <label>Serie</label>
                                  <input class="form-control" type="text" id="txt_serie" name="txt_serie">
                                </div>
                                <div class="col-sm-6">
                                  <label>Cantidad</label>
                                  <input class="form-control" type="text" id="txt_cantidad" name="txt_cantidad">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                  <label>Tecnico</label>
                                  <select class="form-control" name="slct_tecnico_id" id="slct_tecnico_id">
                                  </select>
                                </div>
                                <div class="col-sm-6">
                                  <button type="button" class="btn btn-success" style="margin-top:25px">Guardar Material</button>
                                </div>
                            </div>
                            </div>
                     </fieldset>
               </div><!-- /.col -->
            </form>
            <form id="form_cargamasiva" style="display:none">
               <div class="row form-group">
                  <fieldset class="fieldsetblase">
                     <legend class="legendblade">Carga Masiva</legend>
                     <div class="">
                        <div class="col-md-12">
                                <div class="col-sm-8">
                                  <label class="control-label">Adjuntar Archivo CSV (Incluir cabecera)</label>
                                 <input id="txt_file_plan" type="file" name="txt_file_plan">
                                </div>
                                <div class="col-sm-4">
                                  <button class="btn btn-danger" type="button" onclick="CargarArchivos()" style="margin-top:25px">Cargar Archivo</button>
                                </div>
                            </div>
                     </div>
                  </fieldset>
               </div><!-- /.col -->
            </form>


         </div>
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>