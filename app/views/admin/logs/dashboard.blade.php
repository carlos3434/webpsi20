<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">
    <style type="text/css">
        @media screen and (max-width: 600px) {
            table {
                width:100%;
            }
            thead {
                display: none;
            }
            tr:nth-of-type(2n) {
                background-color: inherit;
                /*border: 1px solid black;*/
            }
            tr td:first-child {
                /*background: #f0f0f0; */
                font-size:80%;
            }
            tbody td {
                display: block;  text-align:center;
            }
            tbody td:before { 
                content: attr(data-th); 
                display: block;
                text-align:center;  
            }
        }

        #mymap,#sidebar {
        text-align: center;
        height: 460px;
        }

        #sidebar {
            text-align: left;
            display: none;
            overflow: auto;
        }
        #mymap {
            background-color: #EFE;
            border-color: #CDC;
            border: 1px solid powderblue;
            width: 100%;
        }
        .content {
            background-color: white;
        }

        .use-sidebar #mymap {width: 100%;}
        .use-sidebar #sidebar {
            display: block;
            width: 100%;
        }

        .sidebar-at-left #sidebar {margin-right: 1%;}
        .sidebar-at-right #sidebar {margin-left: 1%;}

        .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
        .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

        .color-text {
          color:#4C35BE;
          font-family: times, serif;
          font-style:italic;
        }

        #separator {
            background-color: #EEE;
            border: 1px solid #CCC;
            display: block;
            outline: none;
            width: 1%;
        }
        .use-sidebar #separator {
            background: url('img/separator/vertical-separator.gif') repeat-y center top;
            border-color: #FFF;
        }
        #separator:hover {
            border-color: #ABC;
            background: #DEF;
        }
    </style>
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<section class="content-header">
   <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Logs</a></li>
        <li class="active">Envío Legados</li>
    </ol>
</section>
<section class="content">
  <div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary" style="height: 580px;font-size:30px;">
            <div class="panel-heading"><i class="glyphicon glyphicon-stats"></i> <b>INFORMACION DEL DASHBOARD <span id="last_update"></span></b></div>
            <div class="panel-body">
                <div class="use-sidebar sidebar-at-left col-sm-12" id="mymap">
                    <div  id="sidebar">
                        <br>
                        <table class="table table-hover" id="tbl_cabecera">
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/validaCampos/validaCampos.js") }}
<script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
</script>
<audio id="audio-alerta" src="sound/alerta.wav"></audio>
<script type="text/javascript" src="js/admin/logs/dashboard_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/logs/dashboard.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')
