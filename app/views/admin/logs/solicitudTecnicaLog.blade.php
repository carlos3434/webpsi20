<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
		{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
   		{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
		{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
		{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
		{{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
		@include( 'admin.js.slct_global_ajax' )
    	@include( 'admin.js.slct_global' )
		<link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">

@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

<style type="text/css">
 .tbl-scroll{
				max-height: 450px;
				overflow: hidden;
		}
		fieldset{
				max-width: 100% !important;
				border: 1px solid #999;
				padding:5px 15px 8px 15px;
				border-radius: 10px; 
		}
		legend{
				font-size:14px;
				font-weight: 700;
				width: 12%;
				border-bottom: 0px;
				margin-bottom: 0px;
		}
		.slct_days{
			border-radius: 5px !important;
		}

		.bootstrap-tagsinput{
				background-color:#F5F5F5 !important;
				border-radius:7px !important;
				border: 1px solid;
				padding:5px;
			}

			.bootstrap-tagsinput .label-info{
				background-color: #337ab7 !important;
			}

			.bootstrap-tagsinput input{
				display: none;
			}

			.btn-yellow{
				color: #0070ba;
				background-color: ghostwhite;
				border-color: #ccc;
				font-weight: bold;
		}
</style>
<section class="content-header">
	<h1>Solicitud Técnica Log Recepción</h1>
  <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Logs</a></li>
        <li class="active">Solicitud Técnica Log Recepción</li>
  </ol>
</section>
<section class="content">
	<div class="row">
    <div class="col-xs-11 filtros">
      <form name="form_buscar" id="form_buscar" method="POST" action="" enctype="multipart/form-data">
          <input type='hidden' name='tipo_busqueda' value='0' id='tipo_busqueda' />
          <input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
           <div class="panel-group" id="acordion_filtros">
             <div class="panel panel-default">
                 <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                       <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Filtro Individual</a>
                   </h4>
                 </div>
                 <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                   <div class="personalizado">
                    <div class="box-body">
                     <div class="row">
                       <div class="col-sm-6">
                              <label>Código de solicitud técnica</label>
                              <input type="text" name="" id="solicitud_tecnica" placeholder="Ingrese Código de solicitud técnica" class="form form-control">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse2" id="panel_individual">Filtro Personalizado</a>
                     </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="personalizado">
                    <div class="box-body">
                     <div class="row">
                      <div class="form-group">
                        <div class="col-sm-12">
                          <label>Fecha de Registro</label>
                          <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                  <input class="form-control pull-right" id="rangoFechas" type="text" readonly/>
                                  <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()">
                                  <i class="fa fa-rotate-left"></i>
                              </div>
                           </div>
                        </div>
                       

                        <!-- <div class="col-sm-1">
                            <div class="form-group">
                            <a class='btn btn-primary btn-sm' style="margin-top:25px" id="btn_buscar"><i class="fa fa-search fa-lg"></i>&nbsp;Buscar</a>
                            </div>
                        </div>-->
                      </div>
                    </div>
                   </div>
                  </div>
                </div>
                </div>
            </div>
        </div>
      </form>
    </div>
    <div class="col-xs-1  botones">
      <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar" onclick="filtrar()"></i>
      <i class="fa fa-download fa-xs btn btn-success" id="btn-descargar" onclick="descargar()" title="Descargar""></i>      
    </div>
  </div>

	<br>
	<div class="row">
		<div class="col-xs-12 filtros">	
			<div class="box-body table-responsive">
				<table id="tb_solicitudTecnicaLogRec" class="table table-bordered table-striped dataTable
					table-hover" style="background: white">
					<thead>
						<tr>
              <th>Fecha de Registro</th>
							<th>Solicitud Tecnica</th>							
							<th>Codigo Error</th>
							<th>Descripcion Error</th>							
							<th>Usuario</th>							
							<th></th>
						</tr>
					</thead>
					<tbody id="tbody_solicitudTecnicaLogRec">
					
					</tbody>
				</table>
			</div>
		</div>
	</div>

	 <!-- Modal -->
    <div class="modal fade" id="ItemPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:1200px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Resumen Trama</h4>
          </div>
        <div class="modal-body" id="cmodal">
          <!--/////////////////////////////////////////////////////////-->
              <div class="row form-group">
                  <div class="col-sm-12">
                    <div id="resultadoOfsc" style="word-wrap: break-word;padding: 5px;background-color: #D0FED9;border:1px solid #000;" style="width:500px">

                    <strong>Trama:</strong><br/>
                    <pre id="trama">                    
                    </pre>
                    </div>
                  </div>
               
              </div>
            <!--////////////////////////////////////////////////////////-->
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="logofscModal" class="logofscModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" id="modallog" >
        </div>
    </div>

</section>
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/validaCampos/validaCampos.js") }}
@include( "admin.js.slct_global_ajax")
@include( "admin.js.slct_global")

<script type="text/javascript">
		var tipoPersonaId = "{{Session::get('tipoPersona')}}";
		var perfilId = "{{Session::get('perfilId')}}";
</script>
<script type="text/javascript" src="js/admin/logs/solicitudTecnicaLogRec_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/logs/solicitudTecnicaLogRec.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')
