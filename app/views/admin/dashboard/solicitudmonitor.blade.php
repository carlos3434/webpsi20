<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/datepicker/css/datepicker.css")}}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style("css/admin/asistencia.css") }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Monitor de Seguimiento de ST
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Monitor de Seguimiento de ST</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    
      <div class="panel-group contenedor-tecnicos" id="contenedor-tecnicos">
        <div class="panel panel-default">
          <div class="panel-heading box box-primary">
            <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#contenedor-tecnicos"
                                       href="#collapseOne">Logs:</a>
            </h4>
                            </div>
                             <div id="collapseOne" class="panel-collapse collapse in ">
                                    <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h4>Ultimas Solicitudes Procesadas</h4>
              <div class="box-body table-responsive">
                <table id="t_ultimasst" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>F. Registro</th>
                      <th>ST</th>
                      <th>Tipo</th>
                    </tr>
                  </thead>
                  <tbody id="tbultimasst">
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>F. Registro</th>
                      <th>ST</th>
                      <th>Tipo</th>
                    </tr>
                  </tfoot>                          
                </table>
            </div><!-- /.box-body -->
          </div>

          <div class="col-md-6">
            <h4>Ultimas Intentos de ST Recepcionadas</h4>
              <div class="box-body table-responsive">
                <table id="t_ultimasstintentos" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>F. Registro</th>
                      <th>ST</th>
                      <th>Error</th>
                    </tr>
                  </thead>
                  <tbody id="tbultimasst">
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>F. Registro</th>
                      <th>ST</th>
                      <th>Error</th>
                    </tr>
                  </tfoot>                          
                </table>
            </div><!-- /.box-body -->
          </div>

          <div class="col-md-6">
            <h4>Ultimos Mensajes de Toa a PSI</h4>
              <div class="box-body table-responsive">
                <table id="t_ultimosmensajestoa" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>F. Registro</th>
                      <th>F. Update</th>
                      <th>Asunto</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody id="tbultimasst">
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>F. Registro</th>
                      <th>F. Update</th>
                      <th>Asunto</th>
                      <th>Estado</th>
                    </tr>
                  </tfoot>                          
                </table>
            </div><!-- /.box-body -->
          </div>

          <div class="col-md-6">
            <h4>Ultimos Envios a OSB/Legados</h4>
              <div class="box-body table-responsive">
                <table id="t_ultimosenvioslegados" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>F. Registro</th>
                      <th>ST</th>
                      <th>Host</th>
                      <th>Accion</th>
                    </tr>
                  </thead>
                  <tbody id="tbultimasst">
                    
                  </tbody>
                  <tfoot>
                    <tr>
                     <th>F. Registro</th>
                      <th>ST</th>
                      <th>Host</th>
                      <th>Accion</th>
                    </tr>
                  </tfoot>                          
                </table>
            </div><!-- /.box-body -->
          </div>
        </div>
                                        <!--<div class="scroll">
                                            <div class="aside-body row form-group" id="tecnicos">
                                            </div>
                                        </div>-->
                                 </div>
                                 </div>
        </div>

                         <div class="col-md-12">

                       </div>
      </div>
                 <div class="col-md-12">
                         <div class="box box-primary">
                             <div class="box-header">
                                 <div id="mapaLocalizacionRecursos">
                                 </div>
                             </div>
                         </div>
                   </div>
  </div>
</section>
@endsection

@section('formulario')
     
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/datepicker/js/bootstrap-datepicker.js")}}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/bootbox.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/dashboard/solicitudmonitor_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/dashboard/solicitudmonitor.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')