<!DOCTYPE html>
@extends('layouts.masterv2')

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
@stop

<script type="text/javascript">
    var perfilId = "{{Session::get('perfilId')}}";
</script>

<style type="text/css">
    .legendblade {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
        border-image: none;
        border-style: none none solid;
        border-width: 0 0 1px;
        color: #333;
        display: block;
        border-style: none;
        width: 50%;
        font-size: 18px;
        margin-bottom: 5px;
    }
    .fieldsetblase {
        border: 1px solid silver;
        padding: 0.35em 0 0.75em;
        margin-bottom: 20px;
    }
    .dropdown-menu>li>a {
        padding: 0px 30px !important;
    }
</style>
        <!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Parametrización
            <small> </small> <a id="nuevo" class="btn btn-success btn-sm" data-titulo="Nuevo" data-target="#parametrosModal" data-toggle="modal"><i class="fa fa-plus fa-lg"></i>Nuevo</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Parametrizacion</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Inicia contenido -->
                <div class="box" id="">
                    <!--/.box-header -->
                    <form id="form_busqueda" name="form_busqueda">
                    <div class="box-header">
                        <h3 class="box-title">Listado de Parametros</h3>
                    </div>
                    <div class="row" style="margin:20px 0px">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label>Tipo Parametro</label>
                                <select class="form-control" name="slct_tipo[]" id="slct_tipo" multiple="">
                                    @if (Session::get("perfilId") != 17)
                                        <option value="1">LEGADO a PSI</option>
                                        <option value="2">PSI a TOA</option>
                                        <option value="4">MASIVA</option>
                                    @else
                                        <option value="4" selected="">MASIVA</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Sistema</label>
                                <select class="form-control" name="slct_proveniencia[]" id="slct_proveniencia" multiple="">
                                    <option value="2" >CMS</option>
                                    <option value="3" >GESTEL PROVISIÓN</option>
                                    <option value="4" >GESTEL AVERÍA</option>
                                </select>
                            </div>
                            <div class="col-md-4" >
                                <label>Bucket</label>
                                <select class="form-control" name="slct_bucket[]" id="slct_bucket" multiple="">
                                </select>
                            </div>
                            <div class="col-md-2 col-xs-12" style="margin-top:25px">
                                <button type="button" class="btn btn-success" id="btnbusqueda">Buscar</button>
                            </div>
                            <div class="col-md-3" >
                                <label>Tipo Masiva</label>
                                <select class="form-control" name="slct_tipo_masiva_id[]" id="slct_tipo_masiva_id" multiple="">
                                </select>
                            </div>
                            
                        </div>
                    </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                                <table id="t_listado" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Sistema</th>
                                        <th>Bucket</th>
                                        <th>Tipo Masiva</th>
                                        <th>Fecha Creación</th>
                                        <th>Estado</th>
                                        <th>Ver</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_listado">
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Sistema</th>
                                        <th>Bucket</th>
                                        <th>Tipo Masiva</th>
                                        <th>Fecha Creación</th>
                                        <th>Estado</th>
                                        <th>Ver</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!-- /.col -->
                    </div>
                </div><!-- /.box -->
                <!-- Finaliza contenido -->
            </div>
        </div>
        
	
    </section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.mantenimiento.form.parametros' )
@stop

@push('scripts')
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/backbone/js/json2.js') }}
    {{ HTML::script('lib/backbone/js/underscore.min.js') }}
    {{ HTML::script('lib/backbone/js/backbone.min.js') }}
    {{ HTML::script('lib/backbone/js/handlebars.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/mantenimiento/parametro_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/mantenimiento/parametro.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')