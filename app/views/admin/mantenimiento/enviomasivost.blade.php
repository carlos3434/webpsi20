<!DOCTYPE html>
@extends('layouts.masterv2')

@push("stylesheets")
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style>
    .left {float: left;}
    .right {float: right;}

    .clearer {
        clear: both;
        display: block;
        font-size: 0;
        height: 0;
        line-height: 0;
    }

    /*
            Misc
    ------------------------------------------------------------------- */

    .hidden {display: none;}


    /*
            Example specifics
    ------------------------------------------------------------------- */

    /* Layout */

    #center-wrapper {
        margin: 0 auto;
        width: 920px;
    }


    /* Content & sidebar */

    #mymap,#sidebar {
        text-align: center;
        height: 180px;
    }

    #sidebar {
        text-align: left;
        display: none;
        overflow: auto;
    }
    #mymap {
        background-color: white;
        border-color: #CDC;
        border: 1px solid #e0e0e0;
        width: 100%;
    }
    .content {
        background-color: white;
    }

    .use-sidebar #mymap {width: 100%;}
    .use-sidebar #sidebar {
        display: block;
        width: 100%;
    }

    .sidebar-at-left #sidebar {margin-right: 1%;}
    .sidebar-at-right #sidebar {margin-left: 1%;}

    .sidebar-at-left #mymap, .use-sidebar.sidebar-at-right #sidebar, .sidebar-at-right #separator {float: right;}
    .sidebar-at-right #mymap, .use-sidebar.sidebar-at-left #sidebar, .sidebar-at-left #separator {float: left;}

    .color-text {
      color:#4C35BE;
      font-family: times, serif;
      font-style:italic;
    }

    #separator {
        background-color: #EEE;
        border: 1px solid #CCC;
        display: block;
        outline: none;
        width: 1%;
    }
    .use-sidebar #separator {
        background: url('img/separator/vertical-separator.gif') repeat-y center top;
        border-color: #FFF;
    }
    #separator:hover {
        border-color: #ABC;
        background: #DEF;
    }
    
</style>

 <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Envio Masivo St
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimientos</a></li>
        </ol>
    </section>

    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-12 jumbotron">
                <div class="row"> 
                    <div class="col-md-5">
                        <div class="panel panel-default" style="margin-top: 26px;">
                            <div class="panel-heading text-center">
                                <div class="row">
                                    <span><i class="glyphicon glyphicon-th-large"></i>
                                        <b style="margin-left:3px;">TOA - PSI</b>
                                    </span>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-sm btn btn-danger">
                                        Cantidad ST <span class="badge badge-light" id="cantidad">
                                        </span>
                                        </button>
                                    </div>
                                    <div class="pull-left">
                                       <h4>ERROR - Coneccion TOA</h4>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:40px;">  
                                    <div class="col-md-12">
                                        <a href="#" class="btn btn-success btn_envio_masivo col-md-12" data-toggle="tooltip" data-placement="top" title="Envio TOA" style="margin-top: 10px;">
                                            <i class="glyphicon glyphicon-send"></i><span style="margin-left: 5px;">Enviar TOA</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
                    <div class="col-md-7">
                        <div class="use-sidebar sidebar-at-left col-sm-12" id="mymap">
                            <div  id="sidebar">
                                <br>
                                <span><b><i class="glyphicon glyphicon-list-alt"></i>  LOG DE ENVIOS MASIVOS</b></span>
                                <table id="tb_log_envio_masivo" class="table table-bordered dataTable table-hover">
                                    <thead>
                                        <tr style="background: #F0FFF0 ">
                                            <th style="width: 8%">Fecha Envio</th>
                                            <th style="width: 8%">Usuario</th>
                                            <th style="width: 8%">Flujo</th>
                                            <th style="width: 8%">Codigo Error</th>
                                            <th style="width: 8%">Error</th>
                                            <th style="width: 8%">Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody_log_envio_masivo">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-body table-responsive">
                            <table id="tb_st_caidas" class="table table-bordered table-striped dataTable table-hover" style="background: white">
                                <thead>
                                    <tr>
                                        <th style="width: 8%">Solicitud Tecnica</th>
                                        <th style="width: 8%">Requerimiento</th>
                                        <th style="width: 8%">Legado</th>
                                        <th style="width: 8%">Actividad</th>
                                        <th style="width: 8%">Estado ofsc</th>
                                        <th style="width: 8%">Flujo</th>
                                        <th style="width: 8%">Codigo Error</th>
                                        <th style="width: 4%">Error</th>
                                        <th style="width: 4%">Intentos</th>
                                        <th style="width: 4%">Observacion</th>
                                        <th style="width: 4%">Fecha Creacion</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_st_caidas">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
    </div>

{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
{{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
{{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
{{ HTML::script("js/fontawesome-markers.min.js") }}
{{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
{{ HTML::script("lib/vue/vue.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/vue/vue-resource.min.js") }}

@include( "admin.js.slct_global_ajax")
@include( "admin.js.slct_global")

<script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
</script>
<script type="text/javascript" src="js/admin/mantenimiento/envioMasivoSt.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/mantenimiento/envioMasivoSt_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')