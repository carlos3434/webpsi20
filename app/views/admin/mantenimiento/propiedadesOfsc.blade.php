<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
  
    {{ HTML::script('js/psi.js') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

@stop


@section('contenido')

	<section class="content-header">
		<h1>
			Mantenimiento de Propiedades OFSC
			<small> </small>
		</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
		<li><a href="#">Mantenimientos</a></li>
		<li class="active">Mantenimiento de Celulas</li>
	</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
			<!-- Inicia contenido -->
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Filtros</h3>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive">
						<table id="t_propiedades_ofsc" class="table table-bordered table-striped text-center">
							<thead>
								<tr>
									<th style="display: none;">ID</th>
									<th>Etiqueta</th>
									<th>Tipo de tecnología</th>
									<th>Actividad</th>
									<th>Tipo</th>
									<th>Estado</th>
									<th class="editarG"> [ ] </th>
								</tr>
							</thead>
							<tbody id="tb_propiedades_ofsc">
							</tbody>
							<tfoot>
								<tr>
									<th style="display: none;">ID</th>
									<th>Etiqueta</th>
									<th>Tipo de tecnología</th>
									<th>Actividad</th>
									<th>Tipo</th>
									<th>Estado</th>
									<th class="editarG"> [ ] </th>
								</tr>
							</tfoot>
						</table>

						<a class='btn btn-primary btn-sm' id="nuevo" data-toggle="modal" data-target="#propiedadesOfscModal" data-titulo="Nueva">
							<i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

<script type="text/javascript" src="js/admin/mantenimiento/propiedades_ofsc_ajax.js?v=z{{ Cache::get('js_version_number') }}">
</script>

<script type="text/javascript" src="js/admin/mantenimiento/propiedades_ofsc.js?v=z{{ Cache::get('js_version_number') }}">
</script>

@stop

@section('formulario')
     @include( 'admin.mantenimiento.form.propiedadesofsc' )
@stop