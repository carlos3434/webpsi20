<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
{{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
    {{ HTML::script('lib/ajaxUpload/ajaxupload.min.js') }}
    <link rel="stylesheet" type="text/css" href="css/admin/legado.css?v={{ Cache::get('js_version_number') }}">
    <!--<link rel="stylesheet" type="text/css" href="css/tabscroll.css?v={{ Cache::get('js_version_number') }}">-->
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style type="text/css">
    fieldset{
      max-width: 90% !important;
      border: 1px solid #999;
      padding:6px 20px 6px 20px;
      border-radius: 10px; 
    }

    legend{
      font-size:14px;
      font-weight: 700;
      width: 20%;
      border-bottom: 0px;
      margin-bottom: 5px;
    }
    .btn-yellow{
      color: #508d76;
      background-color: #f0f0f0;
      border-color: #ccc;
      font-weight: bold;
    }
    .btn-yellow:hover{
      color: #259f71;
      background-color: #ebf4f0;
    }
    .btn-purple{
      color: white;
      background-color: #715481;
    }
    .btn-purple:hover{
      color: white;
      background-color: #6C09A6;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
            ZONA PREMIUM CATV
            <small> </small>
    </h1>

    <ol class="breadcrumb">
        <li><a ><i class="fa fa-dashboard"></i>Admin</a></li>
        <li><a >Legados</a></li>
        <li class="active">Devoluciones</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-xs-11">
    <ul id="myTab" class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active" role="tab" data-toggle="tab">
        <a href="#tb_carga" id="carga">Carga</a>
      </li>
      <li role="presentation" role="tab" data-toggle="tab">
        <a href="#tb_cargamasiva" id="cargamasiva">Carga Masiva</a>
      </li>
    </ul>
  </div>

  <!-- carga -->
  <div class="col-xs-12">
    <div role="tabpanel" class="tab-pane fade in active" id="tb_carga">
      <div class="col-xs-11 filtros">
          <br>
          <form name="form_buscar" id="form_buscar" method="POST" action="">
            <div class="panel-group" id="acordion_filtros">
                <div class="panel panel-default">
                  <div class="panel-heading box box-primary">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda Personalizada</a>
                    </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="personalizado">
                            <div class="box-body">
                              <div class="row">  
                                <div class="col-sm-4">
                                  <label>Zonal Premium</label>
                                  <select class="form-control" name="slct_zonal_premium[]" id="slct_zonal_premium" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>
                                <div class="col-sm-4">
                                  <label>Nodo Permiun</label>
                                  <select class="form-control" name="slct_nodo_premium[]" id="slct_nodo_premium" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>
                                <div class="col-sm-4">
                                  <label>Troba</label>
                                  <select class="form-control" name="slct_cod_troba[]" id="slct_cod_troba" multiple="" >
                                    <option value="">.::Seleccione::.</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
          </form>
      </div>

      <div class="col-xs-1  botones"><br>
        <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar" onclick="filtrar()"></i>
        
        <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#nuevaZonaPremium">Nuevo  <i class="glyphicon glyphicon-plus"></i></span>  
      </div>


      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive">
            <table id="tb_zonaPremium" class="table table-bordered table-hover">
              <thead>
                <tr> 
                  <th style="width: 25%">ZONA</th>
                  <th style="width: 10%">NODO</th>
                  <th style="width: 12%">TROBA</th>
                  <th style="width: 10%">CONTRATA</th>
                  <th style="width: 20%">FECHA REGISTO</th>
                  <th style="width: 10%">ESTADO</th>
                  <th style="width: 10%">[]</th>
                </tr>
              </thead>
              <tbody id="tbody_zonaPremium">
              </tbody>
            </table>
          </div>                            
        </div>
      </div>
    </div>
  </div>
  <!-- carga masiva-->
  <div role="tabpanel" class="tab-pane fade in active" id="tb_cargamasiva" style="display: none;">
    <!--<ul class="nav nav-pills  nav-justified">
        <li class="active"><a data-toggle="pill" href="#registro">Carga Masiva</a></li>
        <li><a data-toggle="pill" href="#log">Log</a></li>
    </ul>-->

    <div class="tab-content">
      <div id="registro" class="tab-pane fade in active">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-solid">
              <div class="box-header ui-sortable-handle">
                  <h3 class="box-title">SUBIDA MASIVA DE ZONA PREMIUM CATV </h3>
              </div>
              <div class="box-body border-radius-none">
                  <div class="row">
                    <div class="col-sm-12 col-sm-4">
                      <div class="col-sm-12 normal form-group">
                        <form id="form_parcial_columna" name="form_parcial_columna"  role="form" class="form-inline" enctype="multipart/form-data">
                          <div class="form-group">
                            <div class="input-group">
                              <button class="btn btn-md btn-primary" type="button" id="btnSubirExcel">Subir Archivo Excel</button>
                            </div> 
                          </div>
                        </form>
                        <!--<small style="color: #000;">(*) Subir archivos xls, con el formato descargado.</small><br>-->
                      </div>
                    </div>
                    <div class="col-sm-12 col-sm-4">
                      <fieldset>
                          <legend>CAMPOS </legend>
                          <div class="col-md-8">
                            <b class="text-primary">1: Zona</b><br>
                            <b class="text-primary">2: Nodo</b><br> 
                            <b class="text-primary">3: Troba</b><br>
                            <b class="text-primary">4: Contrata</b><br>                               
                          </div>                            
                      </fieldset>
                    </div>
                    <div class="col-sm-12 col-sm-5">
                      <div class="alert alert-danger alert_modal" role="alert" 
                      style="display:none;">
                      </div>
                      <div class="alert alert-success alert_modal_success" role="alert" 
                      style="display:none;"> 
                      </div>
                    </div>
                  </div>
                  <div>
                    <h4>
                      <button id="btnHiddenExcel" class="hidden" ></button>
                      <span class="btn btn-success btnSaveALl btn-xs">Guardar<i class="fa fa-save fa-lg" style="margin-left: 5px;"></i></span>
                      <span class="btn btn-danger btnCancelALl btn-xs">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;""></i></span>
                    </h4>
                    <form name="form_reportecargmasiva" id="form_reportecargmasiva" method="post" action="zonapremiumcatv/excelcargamasiva" enctype="multipart/form-data" style="display: none;">
                        (Descargar formato xls <a id="link_descarga" href="#">aqui</a>)
                    </form>
                  </div>
                  
                  <div class="table margin-bottom-5" style="overflow: scroll;max-height: 600px">
                    <table id="tbl_cargamasiva" class=" table-responsive table table-hover table table-bordered margin-0" border="1">
                      <thead>
                        <tr style="color:white;background: #6F9DC2;">
                          <th>ZONA</th>
                          <th>NODO</th>
                          <th>TROBA</th>
                          <th>CONTRATA</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody style="color: black;">
                      </tbody>
                      <tfoot>                     
                        <tr class="repeat hidden">
                          <td>
                            <input class='form-control  hd_index' type='hidden'/>
                            <input class='form-control  hd_zona' type='hidden' style="width: 100%" placeholder="Ingresar Zona" />
                            <input class='form-control  txt_zona' type='text' style="width: 100%" placeholder="Ingresar Zona" maxlength="100"/>
                          </td>
                          <td>
                            <input class='form-control  hd_nodo' type='hidden' style="width: 100%" placeholder="Ingresar Zona" />
                            <input class='form-control  txt_nodo' type='text' style="width: 100%" 
                            placeholder="Ingresar Nodo" maxlength="2"/>
                          </td>
                          <td>
                            <input class='form-control  hd_troba' type='hidden' style="width: 100%" placeholder="Ingresar Zona" />
                            <input class='form-control  txt_troba' type='text' style="width: 100%"
                            placeholder="Ingresar Troba" maxlength="4"/>
                          </td>
                          <td >
                            <input class='form-control  hd_contrata' type='hidden' style="width: 100%" placeholder="Ingresar Zona" />
                            <input class='form-control  txt_contrata' type='text' style="width: 100%"
                            placeholder="Ingresar Contrata" maxlength="40"/>
                          </td>
                          <td>
                            <a class="btn btn-purple btnSaveAdd btn-xs" data-toggle='modal' title='Guardar registro' style="display: none;">Guardar<i class="fa fa-save fa-lg" style="margin-left: 5px;"></i>
                            </a>
                            <a class="btn btn-purple btnCancelAdd btn-xs" data-toggle='modal' title='Cancelar acción' style="margin-top: 2px;display: none;">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;"></i>
                            </a>
                            <a class="btn btn-purple btnUpdateEdit btn-xs" data-toggle='modal' title='Modificar registro' style="display: none;">Modificar<i class="glyphicon glyphicon-pencil" style="margin-left: 5px;"></i>
                            </a>
                            <a class="btn btn-purple btnCancelEdit btn-xs" data-toggle='modal' title='Cancelar acción' style="margin-top: 2px;display: none;">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;"></i>
                            </a>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  
                    <div>
                      <button class="btn btn-yellow btnAdd" style="width: 100%" type="button"><span class="glyphicon glyphicon-plus"></span> AGREGAR</button>
                    </div>
                    <input type="hidden" id="file_name" name="file_name" value="" />
                  <!-- FIN DE NUEVO SUBIDA DE DEXCEL -->
                  </div>
            </div><!-- /.box -->
          </div>
        </div>
        </div>
      </div>
      
      <div id="log" class="tab-pane fade">
          <form name="form_movimiento" id="form_movimiento" method="post" action="reporte/movimiento" enctype="multipart/form-data">
            <fieldset class="yellow-fieldset">           
              <div class="row form-group" id="div_fecha">
                <div class="col-sm-12">
                  <div class="col-sm-2">
                    <label>Seleccione Rango de Fechas:</label>
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" placeholder="AAAA-MM-DD - AAAA-MM-DD" id="fecha" name="fecha" onfocus="blur()"/>
                  </div>                                   
                  <div class="col-sm-3"> 
                    <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>
                  </div>                                    
                </div>
              </div>
            </fieldset>                                              
          </form>
          
          <div class="row">
            <div class="col-md-12">
                <div class="box-body table-responsive">
                  <table id="tb_logcargamasiva" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>NOMBRE DEL ARCHIVO</th>
                        <th>NUM. REGISTROS</th>
                        <th>RESPONSABLE</th>
                        <th>FECHA REGISTRO</th>                                       
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                </div>                            
            </div>
          </div>
      </div>
    </div>
  </div>
</section><!-- /.content -->
<!-- ******* -->

@endsection

@section('formulario')   
    @include( 'admin.mantenimiento.form.zonaPremiumCatv' )
@stop

@push('scripts')
    <!--{{ HTML::script("lib/jquery.ui.autocomplete.js") }}-->
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("js/fontawesome-markers.min.js") }}   
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
    {{ HTML::script("lib/validaCampos/validaCampos.js") }}
   
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")

<script type="text/javascript" src="js/admin/mantenimiento/zonaPremium_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/mantenimiento/zonaPremium.js?v={{ Cache::get('js_version_number') }}"></script>    
<script type="text/javascript">
    var tipoPersonaId = "{{Session::get('tipoPersona')}}";
    var perfilId = "{{Session::get('perfilId')}}";
</script>


<script type="text/javascript" src="js/admin/legado/solicitud.js?v={{ Cache::get('js_version_number') }}"></script>


@endpush('script')