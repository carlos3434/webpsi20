<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    {{ HTML::style('lib/timepicker/css/bootstrap-timepicker.min.css') }}
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Mantenimiento Horario Resource
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Horario Resource</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <!-- ************************************* -->
                <div class="row">
                    <div class="col-sm-4">
                        <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                            <b-form-select class="form-control" 
                                :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                v-model="perPage">
                            </b-form-select>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                            <b-form-input class="form-control" v-model="filter" placeholder="Ingresar texto"></b-form-input>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <div class="table-responsive">
                            <b-table striped hover class="text-center"
                               :items="datos"
                               :fields="fields"
                               :current-page="currentPage"
                               :per-page="perPage"
                               :filter="filter">
                                <template slot="dia" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="rango_hora" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="hora_inicio" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="hora_fin" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="estado" scope="item">
                                    <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                        @click="setEstado(item.item)">&nbsp;Activo&nbsp;
                                    </label>
                                    <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                        @click="setEstado(item.item)">Inactivo
                                    </label>
                                </template>
                                <template slot="actions" scope="item">
                                    <a class='btn btn-primary btn-sm' 
                                        data-toggle="modal" data-target="#horarioModal" 
                                        @click="find(item.item)">
                                        <i class="fa fa-eye fa-sm"></i>
                                    </a>
                                </template>
                            </b-table>
                        </div>
                    </div>
                    <!-- <div class="col-sm-3">
                        <br>
                        <a class='btn btn-primary btn-sm' data-toggle="modal" data-target="#horarioModal"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                    </div> -->
                    <div class="col-sm-12" style="text-align:right;">
                        <b-pagination :total-rows="datos.length" :per-page="perPage" v-model="currentPage" />
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal -->
        <div class="modal fade" id="horarioModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header logo">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                        <h4 class="modal-title">@{{ type }} Horario Resource</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_horarioresource" name="form_horarioresource" action="" method="post">
                            <!-- <pre>@{{ objHorarioResource }}</pre> -->
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Dia</label>
                                    <select class="form-control dia_id" disabled="true"></select>
                                </div>
                                <div class="col-sm-4 bootstrap-timepicker">
                                    <label>Hora Inicio</label>
                                    <input type="text" class="form-control hora_inicio">
                                </div>
                                <div class="col-sm-4 bootstrap-timepicker">
                                    <label>Hora Fin</label>
                                    <input type="text" class="form-control hora_fin">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Estado</label>
                                    <select class="form-control" v-model="objHorarioResource.estado">
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                        <button type="button" class="btn btn-primary" @click="action">@{{ type }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

    </section>

    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script('lib/timepicker/js/bootstrap-timepicker.js') }} 
    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}
    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{ HTML::script('js/bootstrap-vue/bootstrap-vue.js') }}

    {{ HTML::script('js/admin/mantenimiento/horarioresource.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_horarioresource .hora_inicio, .hora_fin').timepicker({
                showInputs: false,
                showMeridian: false,
                minuteStep: 1,
                showSeconds: false,
                secondStep: 1
            });

            slctGlobal.listarSlctpost('dia','listar','form_horarioresource .dia_id','simple',null,null,0,1);

            $("#form_horarioresource .dia_id").change(function(e) {
                vm.objHorarioResource.dia_id = this.value;
            });

            $('#form_horarioresource .hora_inicio').change(function(e) {
                vm.objHorarioResource.hora_inicio = $('#form_horarioresource .hora_inicio').val();
            });

            $('#form_horarioresource .hora_fin').change(function(e) {
                vm.objHorarioResource.hora_fin = $('#form_horarioresource .hora_fin').val();
            });
        });
    </script>
@stop