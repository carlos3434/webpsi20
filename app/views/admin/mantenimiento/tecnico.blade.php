<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    {{ HTML::style('lib/timepicker/css/bootstrap-timepicker.min.css') }}
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Mantenimiento Tecnicos
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Tecnicos</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <form id="form_general" name="form_general" action="" method="post">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Empresa</label>
                            <select class="form-control empresa" name="empresa[]"  multiple=""></select>
                        </div>
                        <div class="col-sm-4">
                            <label>Bucket</label>
                            <select class="form-control bucket" name="bucket[]"  multiple=""></select>
                        </div>
                        <div class="col-sm-4">
                            <label>&nbsp;</label>
                            <button type="button" class="btn btn-primary form-control" @click="listar">Buscar</button>
                        </div>
                    </div><br>
                </form>
                <!-- ************************************* -->
                <div class="row">
                    <div class="col-sm-4">
                        <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                            <b-form-select class="form-control" 
                                :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                v-model="perPage">
                            </b-form-select>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                            <b-form-input class="form-control" v-model="filter" placeholder="Ingresar texto"></b-form-input>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <div class="table-responsive">
                            <b-table striped hover class="text-center"
                               :items="datos"
                               :fields="fields"
                               :current-page="currentPage"
                               :per-page="perPage"
                               :filter="filter">
                                <template slot="ape_paterno" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="ape_materno" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="nombres" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="dni" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="carnet" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="carnet_tmp" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="empresa" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="bucket" scope="item">
                                    @{{item.value}}
                                </template>
                                <!-- <template slot="empresa" scope="item">
                                    @{{item.value.nombre}}
                                </template>
                                <template slot="bucket" scope="item">

                                    <template v-if="item.value">
                                        @{{item.value.bucket_ofsc}}
                                    </template>
                                    <template v-else>
                                        
                                    </template>
                                    
                                </template> -->
                                <template slot="estado" scope="item">
                                    <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                        @click="setEstado(item.item)">&nbsp;Activo&nbsp;
                                    </label>
                                    <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                        @click="setEstado(item.item)">Inactivo
                                    </label>
                                </template>
                                <template slot="actions" scope="item">
                                    <a class='btn btn-primary btn-sm' 
                                        data-toggle="modal" data-target="#tecnicoModal" 
                                        @click="find(item.item)">
                                        <i class="fa fa-eye fa-sm"></i>
                                    </a>
                                </template>
                            </b-table>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <br>
                        <a class='btn btn-primary btn-sm' data-toggle="modal" data-target="#tecnicoModal"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                    </div>
                    <div class="col-sm-3">
                        <br>
                        <form name="form_exportar" id="form_exportar" method="post" action="tecnico/cargar" enctype="multipart/form-data">
                            <a href="#" id="ExportExcel" class="btn btn-success btn-sm">
                                <i class="fa fa-file-excel-o"></i> Exportar
                            </a>
                        </form>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <b-pagination :total-rows="datos.length" :per-page="perPage" v-model="currentPage" />
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal -->
        <div class="modal fade" id="tecnicoModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header logo">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                        <h4 class="modal-title">@{{ type }} Tecnico</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_tecnicos" name="form_tecnicos" action="" method="post">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Apellido Paterno</label>
                                    <input type="text" class="form-control" v-model="objTecnico.ape_paterno">
                                </div>
                                <div class="col-sm-4">
                                    <label>Apellido Materno</label>
                                    <input type="text" class="form-control" v-model="objTecnico.ape_materno">
                                </div>
                                <div class="col-sm-4">
                                    <label>Nombres</label>
                                    <input type="text" class="form-control" v-model="objTecnico.nombres">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>DNI</label>
                                    <input type="text" class="form-control" v-model="objTecnico.dni">
                                </div>
                                <div class="col-sm-4">
                                    <label>Carnet</label>
                                    <input type="text" class="form-control" v-model="objTecnico.carnet">
                                </div>
                                <div class="col-sm-4">
                                    <label>Carnet Criticos</label>
                                    <input type="text" class="form-control" v-model="objTecnico.carnet_tmp">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Estado</label>
                                    <select class="form-control" v-model="objTecnico.estado">
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Empresa</label>
                                    <select class="form-control empresa"></select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Ninguno</label><br>
                                    <button type="button" class="col-sm-12" 
                                            :class="objTecnico.ninguno == 0 ? 'btn btn-default' : 'btn btn-success'"
                                            @click="setNinguno"
                                            title="Si activa: él tecnico podra recibir varias tareas en un horario">
                                            @{{ objTecnico.ningunoNombre }}
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Fecha Inicio TOA</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input class="form-control pull-right fecha_inicio_toa" ype="text">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Cargo Tecnico</label>
                                    <select class="form-control" v-model="objTecnico.cargo_tecnico">
                                        <option value="1">TECNICO</option>
                                        <option value="0">SUPERVISOR</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Supervisor</label>
                                    <input type="text" class="form-control" v-model="objTecnico.supervisor">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Celular</label>
                                    <input type="text" class="form-control" v-model="objTecnico.celular">
                                </div>
                                <div class="col-sm-4">
                                    <label>IMEI</label>
                                    <input type="text" class="form-control" v-model="objTecnico.imei">
                                </div>
                                <div class="col-sm-4">
                                    <label>IMSI</label>
                                    <input type="text" class="form-control" v-model="objTecnico.imsi">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Marca</label>
                                    <input type="text" class="form-control" v-model="objTecnico.marca">
                                </div>
                                <div class="col-sm-4">
                                    <label>Modelo</label>
                                    <input type="text" class="form-control" v-model="objTecnico.modelo">
                                </div>
                                <div class="col-sm-4">
                                    <label>Version</label>
                                    <input type="text" class="form-control" v-model="objTecnico.version">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Observación</label>
                                    <textarea class="form-control" v-model="objTecnico.observacion"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <br>
                                    <fieldset id="f_celulas">
                                        <legend>Celulas:</legend>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Celula</label>
                                                <select class="form-control celula"></select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>&nbsp;</label>
                                                <button type="button" class="btn btn-success form-control" @click="addCelula">
                                                    <i class="fa fa-plus fa-sm"></i> &nbsp;Añadir
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <br>
                                                <ul class="list-group">
                                                    <div is="celulas" v-bind:celula="objCelula" v-on:remove="removeCelula"></div>
                                                </ul>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <br>
                                    <fieldset id="f_celulas">
                                        <legend>Buckets:</legend>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Bucket</label>
                                                <select class="form-control bucket"></select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success pull-left" @click="enviarSms">
                            <i class="fa fa-envelope-o" style="margin-right: 10px;"></i>Enviar Sms
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                        <button type="button" class="btn btn-primary" @click="action">@{{ type }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

    </section>

    <script type="text/x-template" id="celulas-template">
        <div>
            <template v-for="(celul, index) in celula">
                <li class='list-group-item'>
                    <div class='row'>
                        <div class='col-sm-6'><h5>@{{ celul.nombre }}</h5></div>
                        <div class='col-sm-4'>
                            <div class='checkbox'>
                                <label>
                                    <input type='checkbox' v-model="celul.officetrack">OFFICETRACK
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" @click="removeGrilla(index)" class="btn btn-danger btn-sm">
                                <i class="fa fa-minus fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </li>
            </template>
        </div>
    </script>

    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script('lib/timepicker/js/bootstrap-timepicker.js') }}    
    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}
    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{ HTML::script('js/bootstrap-vue/bootstrap-vue.js') }}

    {{ HTML::script('js/admin/mantenimiento/tecnico.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_tecnicos .fecha_inicio_toa').daterangepicker({
                singleDatePicker: true,
                timePicker: false,
                format: 'YYYY-MM-DD'
            });

            $("#ExportExcel").click(function() {
                $("#form_exportar").append("<input type='hidden' value='1' name='excel'>");
                $("#form_exportar").submit();
            });

            slctGlobal.listarSlctpost('empresa','listar','form_tecnicos .empresa','simple',null,null,0,1);
            slctGlobal.listarSlctpost('bucket','listar','form_tecnicos .bucket','simple',null,null,0,1);
            slctGlobal.listarSlctpost('celula','listar','form_tecnicos .celula','simple',null,null,0,1);
            slctGlobal.listarSlctpost('empresa','listar','form_general .empresa','multiple',null,null,0,1);
            slctGlobal.listarSlctpost('bucket','listar','form_general .bucket','multiple',null,null,0,1);

            $("#form_tecnicos .empresa").change(function(e) {
                vm.objTecnico.empresa_id = this.value;
                filtroSlct("form_tecnicos .empresa","simple",'E','.celula');
                vm.objCelula = [];
            });

            $("#form_tecnicos .bucket").change(function(e) {
                vm.objTecnico.bucket_id = this.value;
            });

            $("#form_tecnicos .celula").change(function(e) {
                vm.objTecnico.celula_id = this.value;
                $("#form_tecnicos .celula option:selected").each(function() {
                    vm.objTecnico.celula = $(this).text() + " ";
                });
            });

            $('#form_tecnicos .fecha_inicio_toa').change(function(e) {
                vm.objTecnico.fecha_inicio_toa = $('#form_tecnicos .fecha_inicio_toa').val();
            });
        });
    </script>
@stop