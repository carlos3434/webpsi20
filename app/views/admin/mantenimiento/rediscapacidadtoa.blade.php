<!DOCTYPE html>
@extends('layouts.master')
@section('includes')
@parent
	{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
	{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
	{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
	{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
	{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
	@include( 'admin.mantenimiento.js.rediscapacidadtoa' )
@stop

@section('contenido')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Mantenimiento de capacidad TOA
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Mantenimiento de capacidad TOA</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->

            <div class="panel-body">
       
                
           

            
                    <div class="box">
                        <div class="box-header" style="display: flex;justify-content: baseline;align-items: center;padding-top: 6px;">
                            <h3 class="box-title">Filtros:</h3>
			        		<form id="search_form" style="display: none;" name="form_mensaje"  role="form" class="form-inline" method="POST">

			                    <div class="col-md-12 text-center">
			                        <div class="form-group">
			                            <div class="input-group">
			                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
			                                <input style="min-width: 170px;" class="form-control" id="fecha" name="fecha" type="text"  aria-describedby="basic-addon1" readonly>
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <select id="am_pm" class="form-control" >
			                            	<option value="*">Periodo</option>
			                            	<option value="AM">AM</option>
			                            	<option value="PM">PM</option>
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <select id="tipo_actividad" class="form-control" >
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <select id="quiebre" class="form-control" >
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <select id="nodo" class="form-control" >
			                            </select>
			                        </div>

			                        <div class="form-group">
			                            <select id="troba" class="form-control" >
			                            </select>
			                        </div>

			                        <button id="search" class="btn btn-primary" type="button">
			                        	Buscar
			                        </button>
			                        
			                    </div>
			            		
			                </form>
			            	<div class="filter_message">
			            		Cargando filtros...
			            	</div>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                        	<table id="tb_keys2" class="table table-bordered table-striped table-condensed display responsive no-wrap text-center" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>Fecha</th>
	                                    <th>Periodo</th>
	                                    <th>Tipo actividad</th>
	                                    <th>Quiebre</th>
	                                    <th>Nodo</th>
	                                    <th>Troba</th>
	                                    <th class="editarG"> Borrar cache </th>
						            </tr>
						        </thead>
						        <tfoot>
						            <tr>
						                <th>Fecha</th>
	                                    <th>Periodo</th>
	                                    <th>Tipo actividad</th>
	                                    <th>Quiebre</th>
	                                    <th>Nodo</th>
	                                    <th>Troba</th>
	                                    <th class="editarG"> Borrar cache </th>
						            </tr>
						        </tfoot>
						    </table>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
              </div>  

        </div>
    </div>

</section><!-- /.content -->
@stop