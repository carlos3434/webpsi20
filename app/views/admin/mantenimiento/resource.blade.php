<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
    {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <!-- <link rel="stylesheet" href="lib/jstree-3.2.1/style.min.css" /> -->
    <style type="text/css">
        a.jstree-anchor {
            width: 100%
        }
    </style>
@stop

@section('contenido')
    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Mantenimiento de Empresas
                <small> </small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                <li><a href="#">Mantenimientos</a></li>
                <li class="active">Mantenimiento de Recursos</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Inicia contenido -->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Filtro</h3>
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3" style="display: none">
                                            <button type="button" class="btn btn-success btn-sm form-control" onclick="crear();"><i class="glyphicon glyphicon-asterisk"></i> Crear</button>
                                        </div>
                                        <div class="col-md-3" style="display: none">
                                            <button type="button" class="btn btn-warning btn-sm form-control" onclick="rename();"><i class="glyphicon glyphicon-pencil"></i> Renombrar</button>
                                        </div>
                                        <div class="col-md-3" style="display: none">
                                            <button type="button" class="btn btn-danger btn-sm form-control" onclick="eliminar();"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" value="" class="form-control" id="buscar" placeholder="Buscar" />
                                        </div>
                                        <div class="col-md-9" style="text-align:right;">
                                                <span class="label label-lg" style="background: #a94442">&nbsp;TOTAL&nbsp;&nbsp;</span>
                                                <span class="label label-lg" style="background: #337ab7">MEDIA</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>
                                        <div class="col-md-12" id="dragTree"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- Finaliza contenido -->
                </div>
            </div>

        </section><!-- /.content -->
@stop

@section('formulario')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <!-- <script type="text/javascript" src="lib/jstree-3.2.1/jstree.min.js"></script> -->
    <script type="text/javascript" src="js/admin/mantenimiento/resource_ajax.js"></script>
    <script type="text/javascript" src="js/admin/mantenimiento/resource.js"></script>
@stop