<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css">
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Motivos OFSC
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Motivos OFSC</li>
        </ol>
    </section>

    <section class="content" id="app">


        <div>
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active" role="tab" data-toggle="tab">
                    <a href="#tb_motivos">Motivos</a>
                </li>
                <li role="presentation" role="tab" data-toggle="tab">
                    <a href="#tb_submotivos">Submotivos</a>
                </li>
            </ul>
        </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="tb_motivos">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Filtros</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Tipo Tratamiento:</label>
                                <multiselect 
                                    {{-- v-model="selectsValueObj.tipoTratamiento"  --}}
                                    :options="selectsOptionsObj.tipoTratamiento"
                                    :multiple="true"
                                    track-by="id"
                                    label="nombre"
                                    :limit="1"
                                    :limit-text="limitText"
                                    placeholder="Seleccionar"
                                    :close-on-select="false"
                                    @input="getSelectedTipoTratamiento"
                                    >
                                    <span slot="noResult"></span>
                                </multiselect>
                            </div>
                            <div class="col-sm-4">
                                <label>Estado OFSC:</label>
                                <multiselect 
                                    {{-- v-model="selectsValueObj.estadoOfsc"  --}}
                                    :options="selectsOptionsObj.estadoOfsc"
                                    :multiple="true"
                                    track-by="id"
                                    label="nombre"
                                    :limit="1"
                                    :limit-text="limitText"
                                    placeholder="Seleccionar"
                                    :close-on-select="false"
                                    @input="getSelectedEstadoOfsc"
                                    >
                                    <span slot="noResult"></span>
                                </multiselect>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo OFSC:</label>
                                <multiselect 
                                    {{-- v-model="selectsValueObj.tipoOfsc"  --}}
                                    :options="selectsOptionsObj.tipoOfsc"
                                    :multiple="true"
                                    track-by="id"
                                    label="nombre"
                                    :limit="1"
                                    :limit-text="limitText"
                                    placeholder="Seleccionar"
                                    :close-on-select="false"
                                    @input="getSelectedTipoOfsc"
                                    >
                                    <span slot="noResult"></span>
                                </multiselect>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo Legado:</label>
                                <multiselect 
                                    {{-- v-model="selectsValueObj.tipoLegado"  --}}
                                    :options="selectsOptionsObj.tipoLegado"
                                    :multiple="true"
                                    track-by="id"
                                    label="nombre"
                                    :limit="1"
                                    :limit-text="limitText"
                                    placeholder="Seleccionar"
                                    :close-on-select="false"
                                    @input="getSelectedTipoLegado"
                                    >
                                    <span slot="noResult"></span>
                                </multiselect>
                            </div>
                            <div class="col-sm-4">
                                <label>Actividad:</label>
                                <multiselect 
                                    {{-- v-model="selectsValueObj.actividad"  --}}
                                    :options="selectsOptionsObj.actividad"
                                    :multiple="true"
                                    track-by="id"
                                    label="nombre"
                                    :limit="1"
                                    :limit-text="limitText"
                                    placeholder="Seleccionar"
                                    :close-on-select="false"
                                    @input="getSelectedActividad"
                                    >
                                    <span slot="noResult"></span>
                                </multiselect>
                            </div>
                            <div class="col-sm-1">
                                <br>
                                <button type="button" class="btn btn-primary" @click="buscarMotivo"><i class="fa fa-search fa-xs"></i> Buscar</button>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                                    <b-form-select class="form-control" 
                                        :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                        v-model="perPagemotivo">
                                    </b-form-select>
                                </b-form-fieldset>
                            </div>
                            <div class="col-sm-4 pull-right">
                                <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                                    <b-form-input class="form-control" v-model="filtermotivo" placeholder="Ingresar texto"></b-form-input>
                                </b-form-fieldset>
                            </div>
                            <div class="col-sm-12">
                                <br>
                                <div class="table-responsive">
                                    <b-table striped hover class="text-center"
                                       :items="datosMotivo"
                                       :fields="fieldsMotivo"
                                       :current-page="currentPagemotivo"
                                       :per-page="perPagemotivo"
                                       :filter.sync="filtermotivo">
                                        <template slot="codigo_ofsc" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="codigo_legado" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="descripcion" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="tipo_tratamiento_legado_descripcion" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="nombre_ofsc" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="estado_ofsc" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="tipo_legado" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="actividad" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="tipo_tecnologia" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="estado" scope="item">
                                            <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                                @click="setEstado(item.item)">&nbsp;Activo&nbsp;
                                            </label>
                                            <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                                @click="setEstado(item.item)">Inactivo
                                            </label>
                                        </template>
                                        <template slot="actions" scope="item">
                                            <a class='btn btn-primary btn-sm' 
                                                data-toggle="modal" data-target="#motivosModal" 
                                                @click="find(item.item)">
                                                <i class="fa fa-eye fa-sm"></i>
                                            </a>
                                        </template>
                                    </b-table>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <br>
                                <a class='btn btn-primary btn-sm  col-md-12' id="nuevo" data-toggle="modal" data-target="#motivosModal" data-titulo="Nuevo"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                            </div>
                            <div class="col-sm-2">
                                <br>
                                <form name="form_exportar" id="form_exportar" method="post" action="motivosofsc/exportar" enctype="multipart/form-data">
                                    <a href="#" id="ExportExcel" class="btn btn-success btn-sm col-md-12">
                                        <i class="fa fa-file-excel-o"></i> Exportar
                                    </a>
                                </form>
                            </div>
                            <div class="col-sm-6 pull-right">
                                <b-pagination :total-rows.sync="datosMotivo.length" :per-page.sync="perPagemotivo" v-model.sync="currentPagemotivo" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tb_submotivos">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Filtros</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                        </div>
                    
                        <div class="row">
                            <div class="col-sm-4">
                                <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                                    <b-form-select class="form-control" 
                                        :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                        v-model="perPage">
                                    </b-form-select>
                                </b-form-fieldset>
                            </div>
                            <div class="col-sm-4 pull-right">
                                <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                                    <b-form-input class="form-control" v-model="filter" placeholder="Ingresar texto"></b-form-input>
                                </b-form-fieldset>
                            </div>
                            <div class="col-sm-12">
                                <br>
                                <div class="table-responsive">
                                    <b-table striped hover class="text-center"
                                       :items="datosSubmotivo"
                                       :fields="fields"
                                       :current-page="currentPage"
                                       :per-page="perPage"
                                       :filter.sync="filter">
                                        <template slot="descripcion_motivo" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="codigo_ofsc" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="codigo_legado" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="descripcion" scope="item">
                                            @{{item.value}}
                                        </template>
                                        <template slot="estado" scope="item">
                                            <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                                @click="setEstadoSubmotivo(item.item)">&nbsp;Activo&nbsp;
                                            </label>
                                            <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                                @click="setEstadoSubmotivo(item.item)">Inactivo
                                            </label>
                                        </template>
                                        <template slot="actions" scope="item">
                                            <a class='btn btn-primary btn-sm' 
                                                data-toggle="modal" data-target="#submotivosModal" 
                                                @click="findSubmotivo(item.item)">
                                                <i class="fa fa-eye fa-sm"></i>
                                            </a>
                                        </template>
                                    </b-table>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <br>
                                <a class='btn btn-primary btn-sm col-md-12' data-toggle="modal" data-target="#submotivosModal"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                            </div>

                            <div class="col-sm-2">
                            <br>
                                <form name="form_exportar1" id="form_exportar1" method="post" action="submotivoofsc/exportar" enctype="multipart/form-data">
                                    <a href="#" id="ExportExcel1" class="btn btn-success btn-sm col-md-12">
                                        <i class="fa fa-file-excel-o"></i> Exportar
                                    </a>
                                </form>
                            </div>
                            <div class="col-sm-6 pull-right">
                                <b-pagination :total-rows.sync="datosSubmotivo.length" :per-page.sync="perPage" v-model.sync="currentPage" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- /.modal -->
        <div class="modal fade" id="motivosModal" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header logo">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                        <h4 class="modal-title">@{{ type }} Motivo</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" v-model="objMotivo.id">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Codigo OFSC</label>
                                <input type="text" class="form-control" v-model="objMotivo.codigo_ofsc">
                            </div>
                            <div class="col-sm-4">
                                <label>Codigo Legado</label>
                                <input type="text" class="form-control" v-model="objMotivo.codigo_legado">
                            </div>
                            <div class="col-sm-4">
                                <label>Descripcion</label>
                                <input type="text" class="form-control" v-model="objMotivo.descripcion">
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo Tratamiento</label>
                                <select class="form-control" v-model="objMotivo.tipo_tratamiento_legado">
                                    <option value="1">AUTOMATICO</option>
                                    <option value="0">SIGUE FLUJO</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Estado OFSC</label>
                                <select class="form-control" v-model="objMotivo.estado_ofsc_id">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <option value="4">No Realizada</option>
                                    <option value="6">Completada</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo OFSC</label>
                                <select class="form-control" v-model="objMotivo.estado_ofsc">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <option value="T">TECNICO</option>
                                    <option value="C">COMERCIAL</option>
                                    <option value="S">SOPORTE DE CAMPO</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo Legado</label>
                                <select class="form-control" v-model="objMotivo.tipo_legado">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <option value="1">CMS</option>
                                    <option value="2">GESTEL</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Actividad</label>
                                 <select class="form-control" v-model="objMotivo.actividad_id">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <template v-for="act in this.selectsOptionsObj.actividad">
                                        <option v-bind:value="act.id">@{{ act.nombre }}</option>
                                    </template>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo Tecnologia</label>
                                <select class="form-control" v-model="objMotivo.tipo_tecnologia" >
                                    <option value="undefined">NO DEFINIDO</option>
                                    <option value="1">CATV</option>
                                    <option value="2">Speedy</option>
                                    <option value="3">Basica</option>
                                    <option value="4">DTH</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Estado</label>
                                <select class="form-control" v-model="objMotivo.estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="action()">@{{ type }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

        <!-- /.modal -->
        <div class="modal fade" id="submotivosModal" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header logo">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                        <h4 class="modal-title">@{{ type }} Submotivo</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" v-model="objSubmotivo.id">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Actividad</label>
                                <select class="form-control" v-model="objSubmotivo.motivo_ofsc_id">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <template v-for="dato in datosMotivo">
                                        <option v-if="dato.estado == 1" v-bind:value="dato.id">@{{ dato.codigo_ofsc }} - @{{ dato.descripcion }}</option>
                                    </template>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Codigo OFSC</label>
                                <input type="text" class="form-control" v-model="objSubmotivo.codigo_ofsc">
                            </div>
                            <div class="col-sm-4">
                                <label>Codigo Legado</label>
                                <input type="text" class="form-control" v-model="objSubmotivo.codigo_legado">
                            </div>
                            <div class="col-sm-4">
                                <label>Descripcion</label>
                                <input type="text" class="form-control" v-model="objSubmotivo.descripcion">
                            </div>
                            <div class="col-sm-4">
                                <label>Estado</label>
                                <select class="form-control" v-model="objSubmotivo.estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="actionSubmotivo()">@{{ type }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

    </section>
    <script src="https://unpkg.com/vue-multiselect@2.0.0"></script>
    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}

    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{-- {{ HTML::script('https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js') }} --}}
    {{ HTML::script('js/bootstrap-vue/bootstrap-vue.js') }}

    {{ HTML::script('js/admin/mantenimiento/motivosofsc.js') }}
@stop