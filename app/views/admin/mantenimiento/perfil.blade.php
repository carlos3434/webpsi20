<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Perfil de Usuarios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Perfil de Usuarios</li>
    </ol>
</section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="t_perfil" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Descripcion</th>
                                                <th>Estado</th>
                                                <th style="width: 80px;">[]</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_perfiles">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>Descripcion</th>
                                                <th>Estado</th>
                                                <th style="width: 80px;">[]</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <a class='btn btn-primary btn-sm' class="btn btn-primary" style="margin:5px;" id="nuevo" data-toggle="modal" data-target="#PerfilModal" data-titulo="Nuevo" data-id="0">
                                              <i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                                        </div>
                                        <div class="col-md-2">
                                            <form name="form_exportar" id="form_exportar" method="post" action="perfil/listar" enctype="multipart/form-data">
                                                <a href="#" id="ExportExcel" class="btn btn-success btn-sm col-md-12">
                                                    <i class="fa fa-file-excel-o"></i> Exportar
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@endsection

@section('formulario')
     @include( 'admin.mantenimiento.form.perfil' )
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
   
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/mantenimiento/perfil_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/mantenimiento/perfil.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')