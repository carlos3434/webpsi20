<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'admin.mantenimiento.js.quiebre_ajax' )
    @include( 'admin.mantenimiento.js.quiebre' )
    @include( 'admin.mantenimiento.js.quiebre_configuracion' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->

<style type="text/css">
.legendblade {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
    border-image: none;
    border-style: none none solid;
    border-width: 0 0 1px;
    color: #333;
    display: block;
    border-style: none;
    width: 50%;
    font-size: 18px;
    margin-bottom: 5px;
}
.fieldsetblase {
    border: 1px solid silver;
    padding: 0.35em 0 0.75em;
    margin-bottom: 20px;
}
</style>



@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Mantenimiento de Quiebres
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Mantenimientos</a></li>
                        <li class="active">Mantenimiento de Quiebres</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Filtros</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="t_quiebres" class="table table-bordered table-striped text-center">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apocope</th>
                                                <th>Grupo</th>
                                                <th>Estado</th>
                                                <th class="editarG"> Editar </th>
                                                <th class="editarG"> Configuración </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_quiebres">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Apocope</th>
                                                <th>Grupo</th>
                                                <th>Estado</th>
                                                <th class="editarG"> Editar</th>
                                                <th class="editarG"> Configuración </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <a class='btn btn-primary btn-sm' id="nuevo"
                                    data-toggle="modal" data-target="#quiebreModal" data-titulo="Nuevo"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                                    <button class='btn btn-info btn-sm' data-toggle="modal" data-target="#ordenModal">
                                        Orden
                                    </button>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop

@section('formulario')
    @include( 'admin.mantenimiento.form.quiebre' )
    @include( 'admin.mantenimiento.form.quiebre_configuracion' )
@stop