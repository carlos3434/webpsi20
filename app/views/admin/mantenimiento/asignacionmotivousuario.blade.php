<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asignacion de Motivos a Usuarios
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Asignacion de Motivos a Usuarios</li>
    </ol>
</section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">

                                <div class="box-body table-responsive">
                                    <table id="t_usuarios" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Apellidos</th>
                                                <th>Nombres</th>
                                                <th>Usuario</th>
                                                <th>Area</th>
                                                <th>Perfil</th>
                                                <th>Empresa</th>
                                                <th style="width: 80px;">[]</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_usuarios">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Apellidos</th>
                                                <th>Nombres</th>
                                                <th>Usuario</th>
                                                <th>Area</th>
                                                <th>Perfil</th>
                                                <th>Empresa</th>
                                                <th style="width: 80px;">[]</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <form name="form_exportar" id="form_exportar" method="post" action="usuario/cargar" enctype="multipart/form-data">
                                                <a href="#" id="ExportExcel" class="btn btn-success btn-sm col-md-12">
                                                    <i class="fa fa-file-excel-o"></i> Exportar
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@endsection

@section('formulario')
     @include( 'admin.mantenimiento.form.asignacionmotivousuario' )
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
   
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/mantenimiento/asignacion_motivo_usuario_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/mantenimiento/asignacion_motivo_usuario.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')