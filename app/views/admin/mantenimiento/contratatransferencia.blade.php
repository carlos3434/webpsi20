<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.0.0/dist/vue-multiselect.min.css">
    <script src="https://unpkg.com/vue-multiselect@2.0.0"></script>
@stop
@section('contenido')
    <section class="content-header">
        <h1>
            Contrata Transferencia
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Contrata Transferencia</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4">
                        <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                            <b-form-select class="form-control" 
                                :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                v-model="perPage">
                            </b-form-select>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                            <b-form-input class="form-control" v-model="filter" placeholder="Ingresar texto"></b-form-input>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <div class="table-responsive">
                            <b-table striped hover class="text-center"
                               :items="datos"
                               :fields="fields"
                               :current-page="currentPage"
                               :per-page="perPage"
                               :filter.sync="filter">
                                <template slot="index" scope="item">
                                    @{{ item.index + 1 }}
                                </template>
                                <template slot="empresa" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="tipo_legado_nombre" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="actividad" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="area_transferencia" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="contrata_transferencia" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="estado" scope="item">
                                    <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                        @click="setEstado(item.item)">&nbsp;Activo&nbsp;
                                    </label>
                                    <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                        @click="setEstado(item.item)">Inactivo
                                    </label>
                                </template>
                                <template slot="actions" scope="item">
                                    <a class='btn btn-primary btn-sm' 
                                        data-toggle="modal" data-target="#transferenciaModal" 
                                        @click="find(item.item)">
                                        <i class="fa fa-eye fa-sm"></i>
                                    </a>
                                </template>
                            </b-table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <a class='btn btn-primary btn-sm' data-toggle="modal" data-target="#transferenciaModal"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <b-pagination :total-rows.sync="datos.length" :per-page.sync="perPage" v-model.sync="currentPage" />
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal -->
        <div class="modal fade" id="transferenciaModal" tabindex="-1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header logo">
                        <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                        <h4 class="modal-title">@{{ type }} Contrata Transferencia</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" v-model="objContrataTransferencia.id">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Empresa</label>
                                <select class="form-control" v-model="objContrataTransferencia.empresa_id">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <template v-for="emp in empresa">
                                        <option v-bind:value="emp.id">@{{ emp.nombre }}</option>
                                    </template>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Tipo Legado</label>
                                <select class="form-control" v-model="objContrataTransferencia.tipo_legado">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <option value="1">CMS</option>
                                    <option value="2">GESTEL</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Actividad</label>
                                <select class="form-control" v-model="objContrataTransferencia.actividad_id">
                                    <option value="undefined">NO DEFINIDO</option>
                                    <template v-for="act in actividad">
                                        <option v-bind:value="act.id">@{{ act.nombre }}</option>
                                    </template>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Area Transferencia</label>
                                <input type="text" class="form-control" v-model="objContrataTransferencia.area_transferencia">
                            </div>
                            <div class="col-sm-4">
                                <label>Contrata Transferencia</label>
                                <input type="text" class="form-control" v-model="objContrataTransferencia.contrata_transferencia">
                            </div>
                            <div class="col-sm-4">
                                <label>Estado</label>
                                <select class="form-control" v-model="objContrataTransferencia.estado">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="action()">@{{ type }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

    </section>
    
    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}

    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{ HTML::script('js/bootstrap-vue/bootstrap-vue.js') }}

    {{ HTML::script('js/admin/mantenimiento/contratatransferencia.js') }}
@stop