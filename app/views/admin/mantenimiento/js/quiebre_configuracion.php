<script type="text/javascript">

//Datos de la configuracion del quiebre
var quiebre_conf_data = {};

//Criterios del lado izquierdo del modal
var criterios_data = [];

//var criterio = [];

//casos seleccionados en el combo
var casos = [];

//Parametro(Caso) seleccionado
var selected_parametro = {};

var nombre_quiebre = "";
var quiebre_id = "";

var special_case_1 = 0;
var special_case_2 = 0;

$(document).ready(function(){

    $('#special_case-1').on('ifChanged', function(event) {
        if(event.target.checked) {
            special_case_1 = 1;
        } else {
            special_case_1 = 0
        }

    });

    $('#special_case-2').on('ifChanged', function(event) {
        if(event.target.checked) {
            special_case_2 = 1;
        } else {
            special_case_2 = 0
        }

    });

    //Activar select de proveniencia
    slctGlobalHtml('slct_proveniencia','simple');

    //Al abrir el modal configuracion
    $('#quiebreModalConfiguracion').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        nombre_quiebre = button.data('titulo');
        quiebre_id = button.data('id');
        var modal = $(this)
        modal.find('.modal-title').text('Configuración de quiebre: ' + nombre_quiebre);

        quiebre_conf.getQuiebreConfiguration(quiebre_id);
    });

    $('#slct_proveniencia').change(function() {
        quiebre_conf.carga_select_casos();
        quiebre_conf.CargaCriterio();
        HTMLCargarCriterio();
    });

    $('#slct_caso').change(function() {
        $("#combos").html("");
        HTMLCargarCriterio();
    });

    $("#guardar_caso").on("click", function(){
        quiebre_conf.GuardarCaso();
    });

    $("#btn_eliminar").on("click", function() {
        var rs = confirm("¿Esta seguro de eleminar este Caso?");

        if (rs) {
            quiebre_conf.borrarcaso($(this).attr("caso_id"));
        }

    });

    ////////////////////////////////////////////////////////////////////////////////

    //Cuando abren el modal de ordenamiento
    $('#ordenModal').on('show.bs.modal', function (event) {
        order_conf.getQuiebres();
    });

    $("#guardar_orden").on("click", function() {
        order_conf.setOrder($("#sistema_order_select").val(), $("#tipo_actividad_order_select").val());
    });

    $("#sistema_order_select").on("change", function(){
        order_conf.getOrder($(this).val(), $("#tipo_actividad_order_select").val());
    });

     $("#tipo_actividad_order_select").on("change", function(){
        order_conf.getOrder($("#sistema_order_select").val(), $(this).val());
    });

});

var order_conf = {
    quiebres_list: [],
    actividades: [],
    getQuiebres: function() {
        $.ajax({
            url         : 'quiebre/ordermanagementdata',
            type        : 'GET',
            cache       : false,
            //dataType    : 'json',
            data        : "",
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(data) {
                //console.log(data.quiebres);
                order_conf.quiebres_list = data.quiebres;
                order_conf.actividades = data.actividades;
                order_conf.setTipoActividadSelect();
                order_conf.getOrder($("#sistema_order_select").val(), $("#tipo_actividad_order_select").val());
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    renderQuiebres: function(data) {

        var html = "<ul id='sortable'>";

        if ( data.id != undefined ) {

            var orden = (data.orden).split(",");

            for (var i = 0; i < orden.length; i++) {

                for (var j = 0; j < order_conf.quiebres_list.length; j++) {

                    if( parseInt(orden[i]) == order_conf.quiebres_list[j].id ) {

                        var color = "";
                        var flag = "";

                        if ( parseInt(order_conf.quiebres_list[j].estado) == 0 ) {
                            color = "style='background-color:crimson;'";
                        }


                        if ( (order_conf.quiebres_list[j].parametros).length > 0 ) {
                            flag = "<span class='glyphicon glyphicon-flag' aria-hidden='true'></span>";
                        }


                        html  += "<li " + color + " quiebre_id='" + order_conf.quiebres_list[j].id + "'>" + order_conf.quiebres_list[j].nombre + flag + "</li>";

                    }

                }
                
            }

        } else {

            for (var i = 0; i < order_conf.quiebres_list.length; i++) {

                var color = "";
                var flag = "";

                if ( parseInt(order_conf.quiebres_list[i].estado) == 0 ) {
                    color = "style='background-color:crimson;'";
                }

                if ( (order_conf.quiebres_list[i].parametros).length > 0 ) {
                    flag = "<span class='glyphicon glyphicon-flag' aria-hidden='true'></span>";
                }

                html  += "<li " + color + " quiebre_id='" + order_conf.quiebres_list[i].id + "'>" + order_conf.quiebres_list[i].nombre + flag +"</li>";
            }

            alert("No se encontraron registros");
            
        }

        html += "</ul>";

        $("#order_container_div").html(html);
        $( "#sortable" ).sortable();
    },
    setTipoActividadSelect: function() {
        var html = "";

        for (var i = 0; i < order_conf.actividades.length; i++) {
            var selected = "";
            if( order_conf.actividades[i].id == 1 ){
                selected = "selected='selected'";
            }

            html += "<option " + selected + " value='" + order_conf.actividades[i].id + "'>" + order_conf.actividades[i].nombre + "</option>";
        }

        $("#tipo_actividad_order_select").html(html);

    },
    getOrder: function(sistema, tipo_actividad) {
        $.ajax({
            url         : 'quiebre/getorden',
            type        : 'GET',
            cache       : false,
            //dataType    : 'json',
            data        : {sistema: sistema, tipo_actividad: tipo_actividad},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(data) {
                order_conf.renderQuiebres(data);
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    setOrder: function(sistema, tipo_actividad){
        var orden = $("#sortable").sortable( "toArray", {attribute: "quiebre_id"});

        $.ajax({
            url         : 'quiebre/setorden',
            type        : 'POST',
            cache       : false,
            //dataType    : 'json',
            data        : {sistema: sistema, tipo_actividad: tipo_actividad, orden: orden.join(",")},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(data) {


                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    }
};

var quiebre_conf = {
    CargaCriterio:function(){
        $.ajax({
            url         : 'criterios/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {proveniencia: $("#slct_proveniencia").val()},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                //console.log(obj.datos);
                if ( obj.rst == 1 ) {
                    criterios_data = obj.datos;
                    HTMLCargarCriterio();
                }

                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    getQuiebreConfiguration: function(quiebre_id) {
        $.ajax({
            url         : 'quiebre/configuracion',
            type        : 'GET',
            cache       : false,
            //dataType    : 'json',
            data        : {quiebre_id: quiebre_id},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(data) {
                quiebre_conf.clasifica_quiebre_conf_data(data);
                quiebre_conf.carga_select_casos();
                quiebre_conf.CargaCriterio();

                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    clasifica_quiebre_conf_data: function(data) {
        var cms = [];
        var gestel_p = [];
        var gestel_a = [];

        for (var i = 0; i < data.length; i++) {
            if( data[i].proveniencia == 2){
                cms.push(data[i]);
            }

            if( data[i].proveniencia == 3){
                gestel_p.push(data[i]);
            }

            if( data[i].proveniencia == 4){
                gestel_a.push(data[i]);
            }
            
        }

        quiebre_conf_data = {
            'cms': cms,
            'gestel_p': gestel_p,
            'gestel_a': gestel_a
        }
    },
    carga_select_casos: function() {

        var tipo = $('#slct_proveniencia').val();
        var parametros = [];

        if ( tipo == 2 ) {
            parametros = quiebre_conf_data.cms;
        }

        if ( tipo == 3 ) {
            parametros = quiebre_conf_data.gestel_p;
        }

        if ( tipo == 4 ) {
            parametros = quiebre_conf_data.gestel_a;
        }

        var html = "<option value=''>Nuevo</option>";

        for (var i = 0; i < parametros.length; i++) {
            html += "<option value='" + parametros[i].id + "'>" + parametros[i].nombre + "</option>";
        }

        $("#slct_caso").multiselect('destroy');
        $("#slct_caso").html(html);
        slctGlobalHtml('slct_caso','simple');

        casos = parametros;
    },
    getParametro: function(parametros, id_p_s) {
        for (var i = 0; i < parametros.length; i++) {
            if ( parametros[i].id == id_p_s ) {
                return parametros[i];
            }
            
        }

        return {};
    },
    getCriterio: function(search_id) {

        if ( Object.keys(selected_parametro).length > 0 ) {

            var criterios = selected_parametro.parametro_criterio;

            for (var i = 0; i < criterios.length; i++) {

                if ( criterios[i].criterios_id == search_id ) {
                    return criterios[i];
                }
                
            }

        }

        return {};
    },
    GuardarCaso:function(){

        var caso_selected = $("#slct_caso").val();
        var criterios_added = [];
        var url = "";

        //validacion
        //if ( $("#combos select").length < 1 ) {
        //    Psi.mensaje('info', 'No seleccionó ningun criterio.', 6000);
        //    return;
        //}

        //Criterio seleccionados para trabajar
        $("#combos select").each(function(index) {
            var detalle = $(this).val();
            if ( detalle == null ) {
                var new_detalle = [];
                $(this).children("option").each(function(index) {
                    new_detalle.push($(this).attr("value"));
                });

                detalle = new_detalle;
            }

            detalle = detalle.join(",");
            
            criterios_added.push({
                criterios_id: $(this).attr("criterio_id"),
                detalle: detalle,
                parametro_criterio_id: $(this).attr("parametro_criterio_id")
            });
        });

        //Si no se ha elejido un caso, se crea uno de lo contrario se actualiza.
        if ( caso_selected == "" ) {

            url = "quiebre/crearcaso";
            var parametro = {
                nombre: quiebre_conf.getNombreCaso(),
                proveniencia: $('#slct_proveniencia').val(),
                quiebre_id: quiebre_id,
                bucket: special_case_1+","+special_case_2,
                criterios: criterios_added
            };

            

        } else {

            url = "quiebre/actualizarcaso";
            var parametro = {
                id: caso_selected,
                bucket: special_case_1+","+special_case_2,
                criterios: criterios_added
            };


        }

        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            //dataType    : 'json',
            data        : parametro,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(data) {
                quiebre_conf.getQuiebreConfiguration(quiebre_id);
                if ( data.rst == 1 ) {
                    //$('#parametrosModal .modal-footer [data-dismiss="modal"]').click();
                    //Psi.mensaje('success', obj.msj, 6000);
                    //Parametros.CargaListado();
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    getNombreCaso: function() {

        if ( casos.length > 0 ) {
            var n = 0;

            for (var i = 0; i < casos.length; i++) {

                var str_array = (casos[i].nombre).split("|");
                var last_n = parseInt(str_array[str_array.length-1]);

                if( n <  last_n ) {
                    n = last_n;
                }
            }

            return nombre_quiebre + " |" + (n + 1);

        } else {

            return nombre_quiebre + " |1";

        }
        
    },
    borrarParametroCriterio: function(p_c_id) {
        if ( p_c_id !== '' ) {

            $.ajax({
                url         : "quiebre/borrarquiebreparametrocriterio",
                type        : 'GET',
                cache       : false,
                //dataType    : 'json',
                data        : "p_c_id="+p_c_id,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(data) {
                    quiebre_conf.getQuiebreConfiguration(quiebre_id);
                    if ( data.rst == 1 ) {
                        //Psi.mensaje('success', obj.msj, 6000);
                        //Parametros.CargaListado();
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        }
    },
    borrarcaso: function(caso_id) {
        if ( caso_id !== '' ) {

            $.ajax({
                url         : "quiebre/borrarcaso",
                type        : 'GET',
                cache       : false,
                //dataType    : 'json',
                data        : "c_id="+caso_id,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(data) {
                    quiebre_conf.getQuiebreConfiguration(quiebre_id);
                    if ( data.rst == 1 ) {
                        //Psi.mensaje('success', obj.msj, 6000);
                        //Parametros.CargaListado();
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });

        }
    }
};

HTMLCargarCriterio = function() {

    var html = "";
    var tipo = $('#slct_proveniencia').val();
    var criterios_select = [];

    var caso_selected = $("#slct_caso").val();

    if ( caso_selected != "" ) {
        var data = null;

        if ( tipo == 2 ) {
            data = quiebre_conf_data.cms;
        }

        if ( tipo == 3 ) {
            data = quiebre_conf_data.gestel_p;
        }

        if ( tipo == 4 ) {
            data = quiebre_conf_data.gestel_a;
        }

        selected_parametro = quiebre_conf.getParametro(data, caso_selected);
        criterios_select = selected_parametro.parametro_criterio;

        $("#btn_eliminar").attr("caso_id", caso_selected);
        $("#btn_eliminar").show();

        var bucket = (selected_parametro.bucket == 'null') ? "0,0" : selected_parametro.bucket;

        var checks = bucket.split(",");

        if( parseInt(checks[0]) ) {
            $("#special_case-1").iCheck('check');
        } else {
            $("#special_case-1").iCheck('uncheck');
        }

        if( parseInt(checks[1]) ) {
            $("#special_case-2").iCheck('check');
        } else {
            $("#special_case-2").iCheck('uncheck');
        }

    } else {
        $("#btn_eliminar").attr("");
        $("#btn_eliminar").hide();
        $("#special_case-1").iCheck('uncheck');
        $("#special_case-2").iCheck('uncheck');

    }

    $.each( criterios_data, function(index, data) {

        if ( data.controlador != '' ) {

            //24 -> criterio quiebre
            if ( data.id != 24 ) {

                var checked = '';
                var disabled = 'disabled=""';
                var parametro_criterio_id = '';


                for (var i = 0; i < criterios_select.length; i++) {
                    if ( criterios_select[i].criterios_id == data.id ) {
                        checked = 'checked';
                        disabled = '';
                        parametro_criterio_id = criterios_select[i].id;
                    }
                }

                var estadohtml = '<input parametro_criterio_id="'+parametro_criterio_id+'" type="checkbox" id="check_'+data.id+'" name="check[]" onchange="mostrartipos('+data.id+')" control="'+data.controlador+'" value="'+data.id+'|'+data.controlador+'" '+checked+'>';

                var ver = '<a id="check_eye_'+data.id+'" class="btn btn-danger btn-sm" onClick="visualizar('+data.id+')" control="'+data.controlador+'" '+disabled+'><i class="fa fa-eye fa-lg"></i></a>';

                html += "<tr>"+
                    "<td>" + data.nombre +" ("+data.nombre_trama+")</td>"+
                    "<td>"+estadohtml+"</td>"+
                    "<td>"+ver+"</td>";

                html += "</tr>";
            }
        }
    });

    $("#tb_criterio").html(html);
    $("#combos").html("");
}


mostrartipos = function(id) {
    
    var tipo = $('#slct_proveniencia').val();
    var control = $("#check_"+id).attr("control");

    var valores = null;

    //Cuando le das check
    if ( $("#check_"+id).is(':checked') && control != '' && control != null ) {

        //Si no existe el select
        if ( !document.getElementById('slct_criterio' + id) ) {

            //if ( criterio[id] !== undefined ) {
            //    valores = criterio[id];
            //}

            CargarCombo(id, control, tipo, [], "");
            $("#check_eye_"+id).removeClass( "btn-danger" );
            $("#check_eye_"+id).addClass( "btn-primary" );
       }
    }

    //Cuando quitas el check
    if ( !$("#check_"+id).is(':checked') ) {

        var p_c_id = $("#check_"+id).attr("parametro_criterio_id");

        if ( p_c_id !== '' ) {
            var rs = confirm("¿Quitar Selección?");
        } else {
            var rs = true;
        }

        

        if (rs) {
            $('#div_'+id).remove();
            //$("#form_parametros #check_eye_"+id).prop('checked', false);
            $("#form_parametros #check_eye_"+id).attr('disabled', true);
            $("#check_eye_"+id).removeClass( "btn-primary" );
            $("#check_eye_"+id).addClass( "btn-danger" );

            
            quiebre_conf.borrarParametroCriterio(p_c_id);

        } else {

            $("#check_"+id).prop('checked', true);

        }
    }
}

CargarCombo = function(id, control, tipo, datoid, parametro_criterio_id) {

    var data = { parametros: tipo };

    var html = '<div class="col-sm-12" id="div_'+id+'">'+
             '<label>'+control+'</label>'+
             '<select parametro_criterio_id="'+parametro_criterio_id+'" criterio_id="'+id+'" class="selectdinamico form-control" name="slct_criterio'+id+'[]" id="slct_criterio'+id+'" multiple="">'+
             '<option value="">.::Seleccione::.</option>'+
             '</select></div>';

       $("#combos").append(html);

       slctGlobal.listarSlctpost(control, 'listar', 'slct_criterio' + id, 'multiple', datoid, data, 1, null, null, null, null, '', null);
}

visualizar = function(id) {
    var tipo = $('#slct_proveniencia').val();
    var control = $('#check_eye_'+id).attr("control");


    var afectado = $("#check_eye_"+id).attr("afectado");
    var cod = $("#check_eye_"+id).attr("cod");
    
    /*if( $("#check_eye_"+id).is(':checked')
        && control!='' && control!=null){ */
    if ( !document.getElementById('slct_criterio'+id) ) {
        CargarCombo(id,control,tipo,  (quiebre_conf.getCriterio(id).detalle).split(","), quiebre_conf.getCriterio(id).id );
        $("#check_eye_"+id).removeClass( "btn-danger" );
        $("#check_eye_"+id).addClass( "btn-primary" );
    } else {
        $('#div_'+id).remove();
        $("#check_eye_"+id).removeClass( "btn-primary" );
        $("#check_eye_"+id).addClass( "btn-danger" );
    }
    //} 
    /*if( !$("#check_eye_"+id).is(':checked') ) {
          
    }*/
}
</script>