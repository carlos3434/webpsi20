<script type="text/javascript">

$(document).ready(function() {
        $("#btnbusqueda").click(BusquedaParam);
        BusquedaParam();
        slctGlobal.listarSlctpost('parametro','bucket','slct_bucket_modal','simple',null,null,0,1);
        slctGlobal.listarSlct('tipomasiva','slct_tipo_masiva_id_modal','simple',null,null,0,1);
        slctGlobalHtml('slct_proveniencia_modal','simple');
        //slctGlobalHtml('form_parametros #slct_tipo_modal','simple');
        slctGlobal.listarSlctpost('parametro','bucket','slct_bucket','multiple',null,null,0,1);
        slctGlobal.listarSlct('tipomasiva','slct_tipo_masiva_id','multiple',null,null,0,1);
        slctGlobalHtml('slct_proveniencia','multiple');
        slctGlobalHtml('slct_tipo','multiple');

        $('#form_parametros #slct_tipo_modal').change(function() {
            $("#form_parametros #slct_proveniencia_modal option[value='1']").attr('disabled','disabled');
            if(this.value==1 || this.value==2 || this.value==5){
                $('#form_parametros #slct_proveniencia_modal').val(2).trigger('change');
                $("#form_parametros #slct_proveniencia_modal").multiselect('rebuild');
            }
            if(this.value==1 || this.value==5){
                $("#check_24").attr("disabled", true);
                $("#check_32,#check_33").attr("disabled", true);
                $("#div_proveniencia").css("display","");
                $("#div_bucket, #div_masiva").css("display","none");
                //Parametros.CargaCriterio();
            } else if(this.value==2) {
                $("#div_bucket, #div_proveniencia").css("display","");
                $("#div_masiva").css("display","none");                
                $("#check_32,#check_33").attr("disabled", true);
                //Parametros.CargaCriterio();
            } else if(this.value==4) {
                $("#div_bucket").css("display","none");
                $("#div_masiva,#div_proveniencia").css("display","");
                Parametros.CargaCriterio(1);
            }else if(this.value==6) {
                $("#div_bucket").css("display","none");
                Parametros.CargaCriterio();
            }
        });
        $('#form_parametros #slct_proveniencia_modal').change(function() {
            /*if(($("#combos").html()).indexOf("div")!=-1) {
                var rs=confirm("Se perderán los criterios ya seleccionados ¿Está de acuerdo?");
                if(!rs){
                    $('#form_parametros #slct_proveniencia_modal').val($("#tipo").val());
                    $("#form_parametros #slct_proveniencia_modal").multiselect('rebuild');
                    return;
                }
            }
            $("#tipo").val(this.value);
            $("#combos").html('');*/
            if($('#form_parametros #slct_tipo_modal').val()==4) {
                Parametros.CargaCriterio(1);
            } else {
                Parametros.CargaCriterio();
            }
                      
        });

        var enforceModalFocusFn;
        $('#parametrosModal').on('show.bs.modal', function (event) {
            enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
            var button = $(event.relatedTarget); 
            var titulo = button.data('titulo'); 
            var modal = $(this); 

            modal.find('.modal-title').text(titulo + ' Parametro');

            if (titulo == 'Nuevo') {
                /*
                $('#form_parametros #slct_estado').show();
                $('#form_parametros .n_estado').remove();*/
                modal.find('.modal-footer .btn-primary').text('Guardar');
                modal.find('.modal-footer .btn-primary').attr('onClick', 'Guardar();');

                $('#form_parametros #slct_tipo_modal').removeAttr("disabled");
                $('#form_parametros #slct_tipo_modal').val(1).trigger('change');
                //$('#form_parametros #slct_proveniencia_modal').val(2).trigger('change');
                //$("#form_parametros #slct_proveniencia_modal").multiselect('rebuild');

            } else {
                parametros_id = button.data('id');
                dataid={usuario:1,id:parametrosObj[parametros_id].id};
                Parametros.CargaFiltro(dataid,parametrosObj[parametros_id].proveniencia);
                modal.find('.modal-footer .btn-primary').text('Actualizar');
                modal.find('.modal-footer .btn-primary').attr('onClick', 'Guardar();');

                $('#form_parametros #slct_proveniencia_modal').multiselect('select',parametrosObj[parametros_id].proveniencia);
                $('#form_parametros #slct_proveniencia_modal').multiselect('rebuild');

                $('#form_parametros #slct_tipo_masiva_id_modal').multiselect('select',parametrosObj[parametros_id].tipo_masiva_id);
                $('#form_parametros #slct_tipo_masiva_id_modal').multiselect('rebuild');
               
               if(parametrosObj[parametros_id].tipo==2){
                   $('#form_parametros #slct_bucket_modal').multiselect('select',parametrosObj[parametros_id].bucket);
                   $('#form_parametros #slct_bucket_modal').multiselect('rebuild');
               }
                
                $('#form_parametros #txt_nombre').val(parametrosObj[parametros_id].nombre);

                $("#form_parametros").append("<input type='hidden' id='id' value='" + parametrosObj[parametros_id].id + "' name='id'>");
                $('#form_parametros #slct_tipo_modal').attr("disabled","true"); 

                setTimeout(function (){
                        $('#form_parametros #slct_tipo_modal').val(parametrosObj[parametros_id].tipo).trigger('change');
                        },100);
            }

            $("#form_parametros").append("<input type='hidden' id='tipo' value='" + $('#form_parametros #slct_proveniencia_modal').val() + "' >");
            $("#form_parametros").append("<input type='hidden' id='tipoparam' value='" + $('#form_parametros #slct_tipo_modal').val() + "' >");

        });

        $('#parametrosModal').on('hide.bs.modal', function (event) {
            $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
            //var modal = $(this); //captura el modal
            //modal.find('.modal-body input').val('');
            parametros_id='' ;
            criterio=[];
            $("#form_parametros input[type='hidden']").remove();
            LimpiarCampos();
        });

});

BusquedaParam=function(){
    Parametros.CargaListado();   
}

LimpiarCampos=function(){
    $("#form_parametros input[type=text]").val('');
    $("#form_parametros input[type=file]").val('');
    $("#form_parametros input[type=hidden]").val('');
    $("#form_parametros #combos").html('');
    //$("#form_parametros input").prop('checked', false);
    $('#form_parametros #bucket').multiselect('select','');
    $('#form_parametros #bucket').multiselect('rebuild');
}

activarTabla=function(tabla){
    $("#t_"+tabla).dataTable();
}

Guardar=function(){
    if($("#form_parametros #slct_tipo_modal").val()==2 && $('#form_parametros #slct_bucket_modal').val()=='') 
    {
        alert("Seleccione una opcion de Bucket");
        return;
    }

    var msj="¿Seguro de Guardar Parametro?";
    if ($("#slct_tipo_modal").val()==4) {
        msj="¿Seguro de Guardar? Se ejecutará el Proceso para filtrar Masivas";
    } 
    var rs=confirm(msj);
    if(rs){
    Parametros.GuardarFiltro();
    }
}

CargarArchivos=function () {
    

        if ($("#txt_file_plan").val()=='') {
            alert("Cargue archivo");
            return false;
        }
    
        var inputFile = document.getElementById("txt_file_plan");
        var file = inputFile.files[0];
        var data = new FormData();

        data.append('archivo', file);
        data.append('proveniencia', $("#form_parametros #slct_proveniencia_modal").val());
        data.append('nombre', $("#txt_nombre").val());
        //data.append('idfiltro', $("#txt_idfiltro").val());
        
        var dataToDownload = "data:application/octet-stream,";        

        $.ajax({
            url: "parametro/cargaplana",
            type: "POST",
            cache: false,
            data: data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            error: function(data) {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            },
            success: function(obj) {
                if(obj.rst==1){
                        LimpiarCampos();
                        Psi.mensaje('success', obj.msj, 6000);
                        Parametros.CargaListado();
                }
                $(".overlay,.loading-img").remove();
            }
        });
    }

HTMLCargarCriterio=function(datos){
    var html="";
    $('#t_criterio').dataTable().fnDestroy();
    var s="'";
    $.each(datos,function(index,data){
        if(data.controlador!='') {
        //var pasante=data.id+'|'+data.afectado+'|'+data.cod;
        
        //var estadohtml='<input type="checkbox" id="check_'+data.id+'" name="check[]" onchange="mostrartipos('+s+pasante+s+')" value="'+data.id+'|'+data.controlador+'">';
        var checked='';
        var disabled='disabled=""';
        if(criterio[data.id]!==undefined){
                checked='checked';
                //$('#check_eye_'+data.id).removeAttr("disabled");
                disabled='';
            }

        var bloqueo='';
        if($("#form_parametros #slct_tipo_modal").val()==1) {
            if(data.id==24 || data.id==32 || data.id==33){ //solo para PSI a TOA || masiva
                bloqueo='disabled=""';
            }
        } 
        if($("#form_parametros #slct_tipo_modal").val()==2) {
            if(data.id==32 || data.id==33){ //solo para masiva
                bloqueo='disabled=""';
            }
        }
        if($("#form_parametros #slct_tipo_modal").val()==5) {
            if(data.id==24 || data.id==32 || data.id==33 || data.id==34 || data.id==1 || 
               data.id==7 || data.id==9 || data.id==14 || data.id==5 || data.id==8 || 
               data.id==4 || data.id==12 || data.id==16 || data.id==17 || data.id==18 || 
               data.id==35 || data.id==29){ //solo para PSI a TOA || masiva
                bloqueo='disabled=""';
            }
        }

        var estadohtml='<input type="checkbox" id="check_'+data.id+'" name="check[]" onchange="mostrartipos('+data.id+')" control="'+data.controlador+'" value="'+data.id+'|'+data.controlador+'" afectado="'+data.afectado+'" cod="'+data.cod+'" '+checked+' '+bloqueo+' >';

        var ver='<a id="check_eye_'+data.id+'" class="btn btn-danger btn-sm" onClick="visualizar('+data.id+')" control="'+data.controlador+'" afectado="'+data.afectado+'" cod="'+data.cod+'" '+disabled+' '+bloqueo+' ><i class="fa fa-eye fa-lg"></i></a>';

        //fa-arrow-right 

        /*'<input type="checkbox" id="check_eye_'+data.id+'" onchange="visualizar('+data.id+')" control="'+data.controlador+'" tipo="'+data.tipo+'" afectado="'+data.afectado+'" cod="'+data.cod+'" '+disabled+' >';*/

        html+="<tr>"+
            "<td>"+data.nombre+"</td>"+
            "<td>"+estadohtml+"</td>"+
            "<td>"+ver+"</td>";

        html+="</tr>";
        }
    });
    $("#tb_criterio").html(html);

   //activarTabla('criterio');
}
activar=function(id){
    Parametros.CambiarEstado(id,1);
}
desactivar=function(id){
    Parametros.CambiarEstado(id,0);
}
descargar=function(){
    window.location=window.location.origin+'/tmpfile/Ejemplo_ArchivoPlano_Parametro.csv';
}

HTMLCargarListado=function(datos){
    var html="";
    $('#t_listado').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        var estado='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estado='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }

        var estadohtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#parametrosModal" data-id="' + index + '" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a>';

        var tipo='LEGADO a PSI';
        if(data.tipo==2){
            tipo='PSI a TOA';
        }
        if(data.tipo==4){
            tipo='MASIVA';
        }
        var masiva="";
        if(data.tipo_masiva_id!==null) {
            masiva=data.tipo_masiva.nombre;
        }

        html+="<tr>"+
            "<td>"+data.nombre+"</td>"+
            "<td>"+tipo+"</td>"+
            "<td>";
            if(data.proveniencia==1) html+="BANDEJA GESTIÓN";
            if(data.proveniencia==2) html+="CMS";
            if(data.proveniencia==3) html+="GESTEL PROVISIÓN";
            if(data.proveniencia==4) html+="GESTEL AVERÍA";
        html+="</td>"+
            "<td>";
        if(data.bucket!==null) html+=data.bucket;
        html+="</td>"+
            "<td>"+masiva+"</td>"+
            "<td>"+estado+"</td>"+
            "<td>"+estadohtml+"</td>";

        html+="</tr>";
    });
    $("#tb_listado").html(html);

   activarTabla('listado');
}


mostrartipos=function(id){
    
    var tipo=$('#form_parametros #slct_proveniencia_modal').val();
    var control=$("#check_"+id).attr("control");
    var afectado=$("#check_"+id).attr("afectado");
    var cod=$("#check_"+id).attr("cod");

    var valores=null;
    if( $("#check_"+id).is(':checked')
        && control!='' && control!=null){
       if (!document.getElementById('slct_criterio'+id)) { 
            if(criterio[id]!==undefined){
                valores=criterio[id];
            }
            CargarCombo(id,control,tipo,valores,afectado,cod);
            //$('#check_eye_'+id).removeAttr("disabled");
            //$("#form_parametros #check_eye_"+id).prop('checked', true);
            $("#check_eye_"+id).removeClass( "btn-danger" );
            $("#check_eye_"+id).addClass( "btn-primary" );
       }
    } 
    if( !$("#check_"+id).is(':checked') ) {
       var rs=confirm("¿Quitar Selección?");
       if(rs){
          $('#div_'+id).remove();
          //$("#form_parametros #check_eye_"+id).prop('checked', false);
          $("#form_parametros #check_eye_"+id).attr('disabled', true);
          $("#check_eye_"+id).removeClass( "btn-primary" );
          $("#check_eye_"+id).addClass( "btn-danger" );
       } else {
          $("#check_"+id).prop('checked', true);         
       }
    }
}

CargarCombo=function(id,control,tipo,datoid,afectado,cod){
    
    var letra=null;
    var slctafectado=null;
    var data={parametros:tipo};
    if(tipo==1){ //BANDEJA GESTIÓN
        data={estado:1};
    }

    var html='<select class="form-control" name="slct_criterio'+id+'[]" id="slct_'+id+'" multiple=""><option value="">.::Seleccione::.</option></select>';

       html='<div class="col-sm-12" id="div_'+id+'">'+
             '<label>'+control+'</label>'+
             '<select class="selectdinamico form-control" name="slct_criterio'+id+'[]" id="slct_criterio'+id+'" multiple="" data-hidden="'+id+','+afectado+'">'+
             '<option value="">.::Seleccione::.</option>'+
             '</select></div>';

       $("#combos").append(html);
       if(afectado!=0) {
        letra=cod;
        slctafectado='#slct_criterio'+afectado;
       }
       //slctGlobal.listarSlct('capilla','slct_id_capilla','simple',null,data,0,'#slct_id_recepcionista','C');
       slctGlobal.listarSlctpost(control,'listar','slct_criterio'+id,'multiple',datoid,data,1,null,null, null,null,'',cargar);
}   

visualizar=function(id){
    var tipo= $('#form_parametros #slct_proveniencia_modal').val();
    var control=$('#check_eye_'+id).attr("control");
    var afectado=$("#check_eye_"+id).attr("afectado");
    var cod=$("#check_eye_"+id).attr("cod");
    
    /*if( $("#check_eye_"+id).is(':checked')
        && control!='' && control!=null){ */
       if (!document.getElementById('slct_criterio'+id)) { 
            CargarCombo(id,control,tipo,criterio[id],afectado,cod);
            $("#check_eye_"+id).removeClass( "btn-danger" );
            $("#check_eye_"+id).addClass( "btn-primary" );
       } else {
            $('#div_'+id).remove();
            $("#check_eye_"+id).removeClass( "btn-primary" );
            $("#check_eye_"+id).addClass( "btn-danger" );
       }
    //} 
    /*if( !$("#check_eye_"+id).is(':checked') ) {
          
    }*/
}

cargar=function(cod){
    
    var id=cod.split(',')[0];
    var idfinal=cod.split(',')[1];
    if(idfinal!=0) {
    if($("#slct_criterio"+id).val()!=null && $("#slct_criterio"+id).length>0 && $("#slct_criterio"+id).val()!=''){
        
        if (document.getElementById('slct_criterio'+idfinal)) {       
              var valor=$("#slct_criterio"+id).val();
              var tipo=$('#form_parametros #slct_tipo_modal').val();
              
              var data={valor:valor,parametros:1};
              if(tipo==2){ //PSI -> TOA
                  var data={valor:valor,estado:1};
              }
              var control=$("#check_"+idfinal).attr("control");
              $('#slct_criterio'+idfinal).multiselect('destroy');

              slctGlobal.listarSlctpost(control,'listar','slct_criterio'+idfinal, "multiple",null,data,null,null,null,null,null,'',cargar);
        }
    }
    }
}


</script>