<script type="text/javascript">
    $(document).ready(function () {
        $("#form_personas").submit(function (event) {
            event.preventDefault();
        });
        Personas.Cargarpersonas(activarTabla);

        $('#personasModal').on('show.bs.modal', function (event) {
            $("#personas_id").remove();
            var button = $(event.relatedTarget);
            var titulo = button.data('titulo'); 
            var modal = $(this);
            modal.find('.modal-title').text(titulo + ' Personas');
            $('#form_personas [data-toggle="tooltip"]').css("display", "none");

            if (titulo == 'Nuevo') {
                modal.find('.modal-footer .btn-primary').text('Guardar');
                modal.find('.modal-footer .btn-primary').attr('onClick', 'Agregar();');
                $('#form_personas #slct_estado').val(1);
                $('#form_personas #txt_nombre').focus();
                $('#form_personas #slct_estado').show();
                $('#form_personas .n_estado').remove();
                $('#form_personas #txt_token').val("<?php echo Session::get('s_token'); ?>");
            } else {
                modal.find('.modal-footer .btn-primary').text('Actualizar');
                modal.find('.modal-footer .btn-primary').attr('onClick', 'Editar();');
                
               personas_id = button.data('id')

                $('#form_personas #txt_nombre').val(personasObj[personas_id].nombre);
                $('#form_personas #txt_porcent').val(personasObj[personas_id].porcent);
                $('#form_personas #slct_estado').show();

                $("#form_personas").append("<input type='hidden' id='personas_id' value='" + personasObj[personas_id].id + "' name='id'>");
            }

        });

        $('#personasModal').on('hide.bs.modal', function (event) {
            var modal = $(this); 
            modal.find('.modal-body input').val(''); 
        });
    });

    activarTabla = function () {
        var tabla = $("#t_personas").dataTable(); // inicializo el datatable
    };
    Editar = function () {
        if (validapersonas()) {
            Personas.AgregarEditarpersonas(1);
        }
    };
    activar = function (id) {
        Personas.CambiarEstadopersonas(id, 1);
    };
    desactivar = function (id) {
        Personas.CambiarEstadopersonas(id, 0);
    };
    Agregar = function () {
        if (validapersonas()) {
            Personas.AgregarEditarpersonas(0);
        }
    };
</script>