<script type="text/javascript">
    var search = '';
    var currentPage = 0;
    var personasObj;
    var personas_id;
    var Personas = {
        AgregarEditarpersonas: function (AE) {
            var datos = $("#form_personas").serialize().split("txt_").join("").split("slct_").join("");
            var accion = "tipopersona/crear";
            if (AE == 1) {
                accion = "tipopersona/editar";
            }
            $.ajax({
                url: accion,
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    //$(".overlay,.loading-img").remove();
                    if (obj.rst == 1) {
                        $('#t_personas').dataTable().fnDestroy();
                        search = $('input[type=search]').val();
                        var table = $('#t_personas').DataTable();
                        var info = table.page.info();
                        currentPage = info.page;

                        Personas.Cargarpersonas(activarTabla);
                        $('#t_personas').dataTable().fnPageChange(currentPage, true);
                        Psi.mensaje('success', obj.msj, 6000);
                        $('#personasModal .modal-footer [data-dismiss="modal"]').click();
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    //$(".overlay,.loading-img").remove();
                    eventoCargaRemover();
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        Cargarpersonas: function (evento) {
            $.ajax({
                url: 'tipopersona/listar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    var html = "";
                    var estadohtml = "";
                    if (obj.rst == 1) {
                        if (agregarG == 0) {
                            $('#nuevo').remove();
                        }
                        personasObj=obj.datos;
                        $.each(obj.datos, function (index, data) {
                            estadohtml = '<span id="' + data.id + '" onClick="activar(' + data.id + ')" class="btn btn-danger">Inactivo</span>';
                            if (data.estado == 1) {
                                estadohtml = '<span id="' + data.id + '" onClick="desactivar(' + data.id + ')" class="btn btn-success">Activo</span>';
                            }
                            //PRIVILEGIO DESACTIVAR                        
                            if (eliminarG == 0) {
                                estadohtml = '<span class="">Inactivo</span>';
                                if (data.estado == 1) {
                                    estadohtml = '<span class="">Activo</span>';
                                }
                            }
                            html += "<tr>" +
                                    "<td >" + data.nombre + "</td>" +
                                    "<td >" + data.porcent + "</td>" +
                                    "<td data-estado='" + data.estado + "'>" + estadohtml + "</td>";

                                html += '<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#personasModal" data-id="' + index + '" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a></td>';

                            html += "</tr>";
                        });
                    }
                    $("#t_personas").dataTable().fnDestroy();

                    $("#tb_personas").html(html);
                    if (editarG == 0)
                        $('.editarG').hide();
                    eventoCargaRemover();
                    activarTabla();
                    $('#t_personas').dataTable().fnPageChange(currentPage, true);
                    $('input[type=search]').val(search);
                    $('input[type=search]').trigger('keyup');
                },
                error: function () {
                    eventoCargaRemover();
                    //$(".overlay,.loading-img").remove();
                }
            });
        },
        CambiarEstadopersonas: function (id, AD) {
            var datos = 'id=' + id + '&estado=' + AD;
            $.ajax({
                url: 'tipopersona/cambiarestado',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        var table = $('#t_personas').DataTable();
                        var info = table.page.info();
                        currentPage = info.page;
                        $('#t_personas').dataTable().fnDestroy();
                        Personas.Cargarpersonas(activarTabla);
                        $('#t_personas').dataTable().fnPageChange(currentPage, true);
                        Psi.mensaje('success', obj.msj, 6000);
                        $('#personasModal .modal-footer [data-dismiss="modal"]').click();
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    eventoCargaRemover();
                    Psi.mensaje('danger', '', 6000);
                }
            });
        }
    };
</script>