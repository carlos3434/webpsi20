<script type="text/javascript">
$(document).ready(function() {  
    Tecnicos.CargarTecnicos(activarTabla);
    Tecnicos.getBuckets();
    //slctGlobal.listarSlct('recursoofsc', 'slct_bucket_filtro', 'simple');
    slctGlobal.listarSlct('recursoofsc', 'slct_bucket', 'simple');
    $("#ExportExcel").click(function(){
        $("#form_exportar").append("<input type='hidden' value='1' name='excel'>");
        $("#form_exportar").submit();
    });

    $("#btn_ninguno").tooltip({
        title : 'Si activa: él tecnico podra recibir varias tareas en un horario',
        placement : 'right'
    });
    $(document).delegate("#slct_bucket_filtro", "change", function(e){
        bucketfiltro = $(this).val();
        Tecnicos.CargarTecnicos(activarTabla);
    });

    $("#btn_ninguno").click(function() {
        if ($(this).hasClass('btn btn-default')) {
            $(this).attr('class','btn btn-success');
            $(this).html('ACTIVO');
            ninguno=1;
        } else {
            $(this).attr('class','btn btn-default');
            $(this).html('INACTIVO');
            ninguno=0;
        }
    });
    $("#btn-sms-tecnico").click(function(e){
        enviarSms($(this).attr("data-celular"), $(this).attr("data-nombre"));
    });
    $('#tecnicoModal').on('show.bs.modal', function (event) {
        $("#tecnico_id").remove();
        var button = $(event.relatedTarget); // captura al boton
        var titulo = button.data('titulo'); // extrae del atributo data-
        var tecnico_id = button.data('id');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
        modal.find('.modal-title').text(titulo+' Tecnico');
        $('#form_tecnicos [data-toggle="tooltip"]').css("display","none");
//        $("#form_tecnicos input[type='hidden']").remove();

        if(titulo=='Nuevo'){
            $("#slct_celula").val(0);
            $("#slct_celula").multiselect('refresh');
            $("#slct_bucket").val(0);
            $("#slct_bucket").multiselect('refresh');
            Tecnicos.CargarCelulas('nuevo',null);
            modal.find('.modal-footer .btn-primary').text('Guardar');
            modal.find('.modal-footer .btn-primary').attr('onClick','Agregar();');
            $('#form_tecnicos #slct_estado').val(1); 
            $('#form_tecnicos #txt_ape_paterno').focus();
            $('#form_tecnicos #slct_estado').show();
            $('#form_tecnicos .n_estado').remove();
            $('#form_tecnicos #txt_token').val("<?php echo Session::get('s_token');?>");
        }
        else{
            empresa_id=tecnicosObj[tecnico_id].empresa_id;
            $("#slct_empresa").multiselect('select',[empresa_id]);
            $("#slct_empresa").multiselect('rebuild');
            $("#slct_empresa").multiselect('refresh');
            filtroSlct("slct_empresa","simple",'E','#slct_celula');

            $("#slct_celula").val(0);
            $("#slct_celula").multiselect('refresh');

            bucket = tecnicosObj[tecnico_id].bucket;
            $("#slct_bucket").multiselect('select',[bucket]);
            $("#slct_bucket").multiselect('refresh');

            Tecnicos.CargarCelulas(tecnicosObj[tecnico_id].id);

            modal.find('.modal-footer .btn-primary').text('Actualizar');
            modal.find('.modal-footer .btn-primary').attr('onClick','Editar();');
            $('#form_tecnicos #txt_ape_paterno').val( tecnicosObj[tecnico_id].ape_paterno );
            $('#form_tecnicos #txt_ape_materno').val( tecnicosObj[tecnico_id].ape_materno );
            $('#form_tecnicos #txt_nombres').val( tecnicosObj[tecnico_id].nombres );
            $('#form_tecnicos #txt_dni').val( tecnicosObj[tecnico_id].dni );
            $('#form_tecnicos #txt_carnet').val( tecnicosObj[tecnico_id].carnet );
            $('#form_tecnicos #txt_carnet_tmp').val( tecnicosObj[tecnico_id].carnet_tmp );
            $('#form_tecnicos #txt_celular').val( tecnicosObj[tecnico_id].celular );
            $('#form_tecnicos #txt_token').val("<?php echo Session::get('s_token');?>");
            //PRIVILEGIO DESACTIVAR EN LA OPCION DE EDITAR
            if(eliminarG == 0) {
                $('#form_tecnicos .n_estado').remove();
                $("#form_tecnicos label:contains('Estado:')").after('<div class="n_estado"></div>');
                $('#form_tecnicos #slct_estado').hide();
                $('#form_tecnicos .n_estado').show();
                var est = tecnicosObj[tecnico_id].estado;
                if(est == 1) est ='Activo';
                else est = 'Inactivo';
                $("#form_tecnicos .n_estado").text( est );
            }
            $('#form_tecnicos #slct_estado').val( tecnicosObj[tecnico_id].estado );            
            $('#form_tecnicos #chk_ninguno').val( tecnicosObj[tecnico_id].ninguno );
            if (tecnicosObj[tecnico_id].ninguno==1) {
                $('#btn_ninguno').attr('class','btn btn-success');
                $('#btn_ninguno').html('ACTIVO');
                ninguno=1;
            } else {
                $('#btn_ninguno').attr('class','btn btn-default');
                $('#btn_ninguno').html('INACTIVO');
                ninguno=0;
            }
            $("#btn-sms-tecnico").css("display", "inline-block");
            $("#btn-sms-tecnico").attr("data-celular", tecnicosObj[tecnico_id].celular);
            $("#btn-sms-tecnico").attr("data-nombre", tecnicosObj[tecnico_id].nombres);
            $("#form_tecnicos").append("<input type='hidden' id='tecnico_id' value='"+tecnicosObj[tecnico_id].id+"' name='id'>");
        }
        $('#slct_empresa').change(function() {
            $( "#slct_celula" ).val(0);
            $( "#slct_celula" ).multiselect('refresh');
            celulas_selec=[];
            $("#t_celulasTecnico").html(''); 
        });
        $( "#form_tecnicos #slct_estado" ).trigger('change');
        $( "#form_tecnicos" ).on('change','#slct_estado', function() {
            if ($( "#form_tecnicos #slct_estado" ).val()==1)
                $('#f_celulas').removeAttr('disabled');
            else
                $('#f_celulas').attr('disabled', 'disabled');
        });

    });

    $('#tecnicoModal').on('hide.bs.modal', function (event) {
        var modal = $(this); //captura el modal
        modal.find('.modal-body input').val(''); // busca un input para copiarle texto
        $("#slct_empresa").multiselect('deselectAll', false);
        $("#slct_empresa").multiselect('rebuild');
        $("#slct_empresa").multiselect('refresh');
        $("#slct_celula").multiselect('deselectAll', false);
        $("#slct_celula").multiselect('rebuild');
        $("#slct_celula").multiselect('refresh');
        $("#slct_bucket").multiselect('deselectAll', false);
        $("#slct_bucket").multiselect('rebuild');
        $("#t_celulasTecnico").html('');
        celulas_selec=[];
        $('#btn_ninguno').attr('class','btn btn-default');
        $('#btn_ninguno').html('INACTIVO');
        $("#btn-sms-tecnico").css("display", "none");
        ninguno=0;
    });
});

activarTabla =function(){
    var html="", estadohtml="";
    //$("#t_tecnicos").DataTable().destroy();
    var tabletecnico = $("#t_tecnicos").DataTable();
    tabletecnico.clear().draw();
    //PRIVILEGIO AGREGAR
    if(agregarG == 0) { 
        $('#nuevo').remove();  
    }  
    $.each(tecnicosObj,function(index,data){
        var tecnico = data;
        if (bucketfiltro!=""){
            if (tecnico.bucket!=bucketfiltro) {
                tecnico = null;
            }
        }
        /*if (bucketfiltro!="") {
            if (data.bucket!=bucketfiltro) {
                tecnico = null;
            }
        }*/
        if (tecnico!=null && tecnico!="null") {
            estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
            }
            //PRIVILEGIO DESACTIVAR
            if(eliminarG == 0) {
                estadohtml='<span class="">Inactivo</span>';
                if(data.estado==1){
                    estadohtml='<span class="">Activo</span>';
                }
            }
             //PRIVILEGIO EDITAR
            var btneditar = "";
            if(editarG == 1) {
                btneditar = '<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tecnicoModal" data-id="'+index+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a>';
            }
                
            html+="</tr>";

            tabletecnico.row.add( [
                data.ape_paterno,
                data.ape_materno,
                data.nombres,
                data.dni,
                data.carnet,
                data.carnet_tmp,
                data.empresa,
                data.bucket,
                estadohtml,
                btneditar
            ]).draw(false);
        }
    });
    if(editarG == 0) $('.editarG').hide();
    bucketfiltro = "";
};

Editar=function(){
    if(validaTecnicos()){
        Tecnicos.AgregarEditarTecnico(1);
    }
};

activar=function(id){
    Tecnicos.CambiarEstadoTecnicos(id,1);
};
HTMLListarSlct=function(obj){
    var html="";
    $.each(obj.datos, function(i,celula){
        var check = '', celulaId='';
        if (celula.officetrack==1) {
            check = 'checked';
        }
        celulaId=celula.id;
        html+="<li class='list-group-item'><div class='row'>";
        html+="<div class='col-sm-6' id='celula_"+celulaId+"'><h5>"+celula.nombre+"</h5></div>";
        html+="<div class='col-sm-4'>";
            html+="<div class='checkbox'>";
                html+="<label><input type='checkbox'"+check+" name='officetrack"+celulaId+"' id='officetrack"+celulaId+"'>";
        html+="Officetrack</label></div></div>";
        html+='<div class="col-sm-2">';
        html+='<button type="button" id="'+celulaId+'" Onclick="EliminarCelula(this)" class="btn btn-danger btn-sm" >';
        html+='<i class="fa fa-minus fa-sm"></i> </button></div>';
        html+="</div></li>";
        celulas_selec.push(Number(celulaId));
    });
    $("#t_celulasTecnico").html(html); 
};
desactivar=function(id){
    Tecnicos.CambiarEstadoTecnicos(id,0);
};

Agregar=function(){
    if(validaTecnicos()){
        Tecnicos.AgregarEditarTecnico(0);
    }
};
AgregarCelula=function(){
    //añadir registro "opcion" por usuario
    var empresaId=$('#slct_empresa option:selected').val();
    var celulaId=$('#slct_celula option:selected').val();
    var celula=$('#slct_celula option:selected').text();
    var buscar_celula = $('#celula_'+celulaId).text();
    if (celulaId!=='' && celulaId!==undefined) {
        if (buscar_celula==="") {
            //evaluar si selecciono empresa
            if (empresaId!=='' && empresaId !==undefined) {
                var html='';
                html+="<li class='list-group-item'><div class='row'>";
                html+="<div class='col-sm-6' id='celula_"+celulaId+"'><h5>"+celula+"</h5></div>";
                html+="<div class='col-sm-4'>";
                    html+="<div class='checkbox'>";
                        html+="<label><input type='checkbox' name='officetrack"+celulaId+"' id='officetrack"+celulaId+"'>";
                html+="Officetrack</label></div></div>";
                html+='<div class="col-sm-2">';
                html+='<button type="button" id="'+celulaId+'" Onclick="EliminarCelula(this)" class="btn btn-danger btn-sm" >';
                html+='<i class="fa fa-minus fa-sm"></i> </button></div>';
                html+="</div></li>";
                $("#t_celulasTecnico").append(html);
                celulas_selec.push(Number(celulaId));
            } else {
                alert("Seleccione Empresa");
            }

        } else 
            alert("Ya se agrego este celula");
    } else 
        alert("Seleccione Celula");
};
EliminarCelula=function(obj){
    var valor= obj.id;
    obj.parentNode.parentNode.parentNode.remove();
    var index = celulas_selec.indexOf(Number(valor));
    celulas_selec.splice( index, 1 );
};
validaTecnicos=function(){
    $('#form_tecnicos [data-toggle="tooltip"]').css("display","none");
    var a=[];
    a[0]=validat("txt","nombres","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};
validat=function(inicial,id,v_default){
    var texto="Seleccione";
    if(inicial=="txt"){
        texto="Ingrese";
    }

    if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
        $('#error_'+id).attr('data-original-title',texto+' '+id);
        $('#error_'+id).css('display','');
        return false;
    }
};
renderSelectBuckets = function() {
    var html = "";
    html+="<option value=''>.::Seleccione::.</option>";
    html+="<option value='SinBucket'>SinBucket</option>";
    for (var i in listBuckets) {
        html+="<option value='"+listBuckets[i].id+"'>"+listBuckets[i].nombre+"</option>";
    }
    $("#slct_bucket_filtro").html(html);
    slctGlobalHtml("slct_bucket_filtro", "simple");
};
enviarSms = function(celular, nombretecnico) {
    if (celular==""){
        Psi.sweetAlertError("El tecnico No tiene un celular asignado");
        return;
    }
    var title = "Enviar Sms a un Tecnico :"+ nombretecnico;
    bootbox.prompt({
        title: title, 
        inputType: 'textarea',
        callback : function(result){
            if (result) {
                Tecnicos.enviarSms(celular, result);
                nombretecnico = "";
            }
            $(".bootbox modal h4.modal-title").html(title);
        }
    });
};
</script>