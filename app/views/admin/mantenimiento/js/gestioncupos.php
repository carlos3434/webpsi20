<script type="text/javascript">

$(document).ready(function() {
        //activarTabla();
    slctGlobal.listarSlct('filtrocupos','slct_bucket','simple',null,null,0,1);
    slctGlobal.listarSlctpost('horariotipo','listargo' ,'slct_horario', 'simple', [7],null,1);
    slctGlobal.listarSlct('tipopersona','slct_persona', 'simple', null,null,1);
    $("#buscar").click(Buscar);
    $("#guardar").click(GuardarFiltro);
});

LimpiarCampos=function(){
    $("input[type=text]").val('');
    $("input[type=file]").val('');
    $("input[type=hidden]").val('');
}

descargar=function(){
    window.location=window.location.origin+'/tmpfile/Ejemplo_ArchivoPlano_cupos.csv';
}

CargarArchivos=function () {
        if ($("#txt_file_plan").val()=='') {
            alert("Cargue archivo");
            return false;
        }

        if ($("#slct_persona").val()=='') {
            alert("Seleccione un Tipo de Persona");
            return false;
        }
    
        var inputFile = document.getElementById("txt_file_plan");
        var file = inputFile.files[0];
        var data = new FormData();

        data.append('archivo', file);
        data.append('idfiltro', $("#txt_idfiltro").val());
        data.append('persona', $("#slct_persona").val());
        
        var dataToDownload = "data:application/octet-stream,";        

        $.ajax({
            url: "filtrocupos/cargaplana",
            type: "POST",
            cache: false,
            data: data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            error: function(data) {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            },
            success: function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                        LimpiarCampos();
                        Psi.mensaje('success', obj.msj, 6000);
                } else {
                    Psi.mensaje('danger', obj.msj, 6000);
                }
                
            }
        });
    }

GuardarFiltro=function(){
    var total=0;
    if(horarioObj && horarioObj.length>0) { //saber si se hizo la busqueda
        var valida=true;
        $.each(diaObj,function(index,data){
            total=0;
            $.each(horarioObj,function(index_,data_){
                var val=$("input[name=txt_cupo_"+data_.id+"_"+data+"]").val();
                if(!Number.isInteger(parseFloat(val))){
                   alert('Ingrese solo números');
                   valida=false;
                   return false; 
                } 
               // total=parseFloat(total)+parseFloat(val);
               if(parseFloat(val)>100){
                alert ("Por Rango Horario la capacidad no deben pasar el 100%");
                valida=false;
                return false;
               }
            });
            if(!valida){
                return false;
            }
            /*if(total>100){
                alert ("Por dia los cupos no deben pasar el 100%");
                return false;
            }*/
        });
        if(valida){
            var rs=confirm("¿Seguro de Guardar?");
            if(rs){
                GestionCupos.GuardarFiltro();
            }
        } 
    } else {
        alert('Debe Cargar un Horario');
    }
}

Buscar=function(id){
    if($("#slct_bucket").val()!=''){
        if($("#slct_persona").val()!=''){
            GestionCupos.CargaFiltro(); 
        } else {
        alert('Seleccione un Tipo de Persona');
        }
    } else {
        alert('Seleccione un Bucket');
    }
}

calcular=function(id){
    var porcent=$("input[name="+id+"]").val();
    var capa=$("#txt_capacidad").val();
    if(porcent<=100) {
        var total=parseFloat(porcent)*parseFloat(capa)/100;
        $("#"+id).html('Capacidad: '+Math.round(total));
    } else {
        alert("La capacidad no debe pasar el 100%");
        $("input[name="+id+"]").val('0');
    }


}

completar=function(valor,fila){    
    $("input[name^=txt_cupo_"+fila+"]").val(valor);
    var capa=$("#txt_capacidad").val();
    var total=parseFloat(valor)*parseFloat(capa)/100;
    $("div[id^=txt_cupo_"+fila+"]").html('Capacidad: '+Math.round(total));

}

HTMLCargarCupo=function(datos,horario,capacidad){

    var html="";
    $("#txt_capacidad").val(capacidad);
    var $ix=0;
    $.each(horario,function(index,data){
        
        html+='<tr>';
        html+='<td>'+data.horario+'</td>';
        html+='<td><input class="porcent" type="text" onChange="completar(this.value,'+data.id+')"></td>';
        var pos= (''+data.id).length;
        $.each(datos,function(index_,dato){
            if(data.id==index_.substr(0,pos)){
               var name=data.id+'_'+diaObj[$ix];
               html+='<td><input class="porcent" type="text" name="txt_cupo_'+name+'" value="'+dato+'" onChange="calcular(this.name)" >%<div id="txt_cupo_'+name+'" style ="font-size: 11px">Cupos: '+Math.round(parseFloat(dato)*capacidad/100)+'</div></td>'; 
            }
            $ix++;
            if($ix==diaObj.length){
                $ix=0;
            } 
        });

        html+='</tr>';
    });

    $("#tb_horario").html(html);
}


</script>
