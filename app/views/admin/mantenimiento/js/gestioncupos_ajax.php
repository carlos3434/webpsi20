<script type="text/javascript">
var horarioObj;
var diaObj;
    var GestionCupos={
        CargaFiltro:function(){
             var datos=$("#form_filtro").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
            $.ajax({
                url         : 'filtrocupos/cargarcupo',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                       horarioObj=obj.horario;
                       diaObj=obj.dia;
                       $("#txt_idfiltro").val(obj.idfiltro);
                       HTMLCargarCupo(obj.datos,obj.horario,obj.capacidad);
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        GuardarFiltro:function(){
             var datos=$("#form_filtro").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
            $.ajax({
                url         : 'filtrocupos/guardar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                      Psi.mensaje('success', obj.msj, 6000);
                      $("#txt_capacidad").val('');
                      $("#txt_idfiltro").val('');
                      $("#tb_horario").html('');
                    } else Psi.mensaje('danger', obj.msj, 6000);
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },

    };
</script>
