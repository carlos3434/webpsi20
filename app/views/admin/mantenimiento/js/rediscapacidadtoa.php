<script type="text/javascript">

	var rediscapavidadtoa = {
		setDefultDateOptionRange: function(){
			var today = moment();

    		$('#fecha').daterangepicker({
		        singleDatePicker: true,
		        showDropdowns: false,
		        startDate: today,
		        format: 'YYYY-MM-DD'
		    });

		    $('#fecha').val(today.format('YYYY-MM-DD'));

		},
		getFilterSelects: function(){
			$.ajax({
				data:  [],
				url:   '/rediscapacidadtoa/selects',
				type:  'get',
				beforeSend: function () {
					$(".filter_message").slideDown();
				},
				success:  function (response) {

					//Tipos de actividad
					var tipos_de_actividad_options = "";
					tipos_de_actividad_options+= "<option value='*'>Tipo actividad</option>";
					for (var i = 0; i < response.tipos_actividades.length; i++) {
						tipos_de_actividad_options+= "<option value='"+response.tipos_actividades[i].label+"'>"+response.tipos_actividades[i].nombre+"</option>";
					}
					$("#tipo_actividad").append(tipos_de_actividad_options);
					$('#tipo_actividad').multiselect({
						nonSelectedText: 'Tipo de actividad',
						enableFiltering: true,
						maxHeight: 400
					});

					//Quiebres
					var quiebre_options = "";
					quiebre_options+= "<option value='*'>Quiebre</option>";
					for (var i = 0; i < response.quiebres.length; i++) {
						quiebre_options+= "<option value='"+response.quiebres[i].apocope+"'>"+response.quiebres[i].nombre+"</option>";
					}
					$("#quiebre").append(quiebre_options);
					$('#quiebre').multiselect({
						nonSelectedText: 'Quiebre',
						enableFiltering: true,
						maxHeight: 400
					});

					//Nodos
					var nodo_options = "";
					nodo_options+= "<option value='*'>Nodo</option>";
					for (var i = 0; i < response.nodos.length; i++) {
						nodo_options+= "<option value='"+response.nodos[i].nodo+"'>"+response.nodos[i].nodo+"</option>";
					}
					$("#nodo").append(nodo_options);
					$('#nodo').multiselect({
						nonSelectedText: 'Nodo',
						enableFiltering: true,
						maxHeight: 400
					});

					//Trobas
					var troba_options = "";
					troba_options+= "<option value='*'>Troba</option>";
					for (var i = 0; i < response.trobas.length; i++) {
						troba_options+= "<option value='"+response.trobas[i].troba+"'>"+response.trobas[i].troba+"</option>";
					}
					$("#troba").append(troba_options);
					$('#troba').multiselect({
						nonSelectedText: 'Troba',
						enableFiltering: true,
						maxHeight: 400
					});

					$("#search_form").slideDown();
					$(".filter_message").slideUp();
				}
			});
		},
		filltable: function(search){
			$('#tb_keys2').DataTable().destroy();
			$('#tb_keys2').DataTable( {
		        "ajax": "/rediscapacidadtoa/searchcache?search="+search
		    } );

		},
		deleteCache: function(key){
			$.ajax({
				data:  "key="+key,
				url:   '/rediscapacidadtoa/deletecache',
				type:  'get',
				beforeSend: function () {
					//$(".filter_message").slideDown();
				},
				success:  function (response) {
					rediscapavidadtoa.searchCache();
					console.log(response);
				}
			});
		},
		searchCache: function(){
			var fecha = $("#fecha").val();
			var periodo = $("#am_pm").val();
			var tipo_actividad = $("#tipo_actividad").val();
			var quiebre = $("#quiebre").val();
			var nodo = $("#nodo").val();
			var troba = $("#troba").val();

			var search = fecha+"|"+periodo+"|"+tipo_actividad+"|"+quiebre+"|"+nodo+"_"+troba;

			rediscapavidadtoa.filltable(search);
		}
	}
	
	$(document).ready(function(){
		console.log(2);
		rediscapavidadtoa.setDefultDateOptionRange();
		$('#am_pm').multiselect({
			nonSelectedText: 'Periodo'
		});
		rediscapavidadtoa.getFilterSelects();

		$("#search").on("click", function(){
			rediscapavidadtoa.searchCache();
		});

		$(document).on('click', '.delete-cache', function(event) {
			rediscapavidadtoa.deleteCache($(this).attr("key"));
		});
	});
</script>