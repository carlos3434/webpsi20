<script type="text/javascript">
var Submodulos={
    AgregarEditarSubmodulos:function(AE){
        var datos=$("#form_submodulos").serialize().split("txt_").join("").split("slct_").join("");
        var accion="submodulo/crear";
        if(AE==1){
            accion="submodulo/editar";
        }
        
        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $('#t_submodulos').dataTable().fnDestroy();

                    Submodulos.CargarSubmodulos(activarTabla);
                    $('#t_submodulos').dataTable();
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#submoduloModal .modal-footer [data-dismiss="modal"]').click();
                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    CargarSubmodulos:function(evento){
        $.ajax({
            url         : 'submodulo/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                var html="";
                var estadohtml="";
                //PRIVILEGIO AGREGAR
                if(agregarG == 0) { 
                    $('#nuevo').remove();  
                } 
                if(obj.rst==1){
                    $("#t_submodulos").dataTable().fnDestroy();
                    $.each(obj.datos,function(index,data){
                        estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
                        if(data.estado==1){
                            estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
                        }
                        //PRIVILEGIO DESACTIVAR
                        if(eliminarG == 0) { 
                            $('#form_submodulos #slct_estado').replaceWith('<div class="n_estado"></div>')
                            estadohtml='<span class="">Inactivo</span>';
                            if(data.estado==1){
                                estadohtml='<span class="">Activo</span>';
                            }
                        } 
                        html+="<tr>"+
                            "<td id='nombre_"+data.id+"'>"+data.nombre+"</td>"+
                            "<td id='path_"+data.id+"'>"+data.path+"</td>"+
                            "<td id='modulo_id_"+data.id+"' modulo_id='"+data.modulo_id+"'>"+data.modulo+"</td>"+
                            "<td id='estado_"+data.id+"' data-estado='"+data.estado+"'>"+estadohtml+
                            "<input type='hidden' name='proyecto_tipo' value='"+data.proyecto_tipo+"'/>"+
                            "<input type='hidden' name='submoduloparent' value='"+data.parent+"'/>"+
                            "<input type='hidden' name='menu_visible' value='"+data.menu_visible+"'/></td>";
                        //PRIVILEGIO EDITAR
                        if(editarG == 1) { 
                            html+='<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#submoduloModal" data-id="'+data.id+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                        } else {
                            html+='<td class="editarG"></td>';
                        }
                            

                        html+="</tr>";
                    });
                }
                $("#tb_submodulos").html(html); 
                if(editarG == 0) $('.editarG').hide(); 
                evento();
                $("#t_submodulos").dataTable();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },
    cargarModulos:function(accion,modulo_id){
        $.ajax({
            url         : 'modulo/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(obj) {
                if(obj.rst==1){
                    $('#slct_modulo_id').html('');
                    $.each(obj.datos,function(index,data){
                        $('#slct_modulo_id').append('<option value='+data.id+'>'+data.nombre+'</option>');
                    });
                    if (accion==='nuevo')
                        $('#slct_modulo_id').append("<option selected style='display:none;'>--- Elige Modulo ---</option>");
                    else
                       $('#slct_modulo_id').val( modulo_id );
                } 
            }
        });
    },
    CambiarEstadoSubmodulos:function(id,AD){
        $("#form_submodulos").append("<input type='hidden' id='id_t2' value='"+id+"' name='id'>");
        $("#form_submodulos").append("<input type='hidden' id='estado_t2' value='"+AD+"' name='estado'>");
        var datos=$("#form_submodulos").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'submodulo/cambiarestado',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $('#t_submodulos').dataTable().fnDestroy();
                    Submodulos.CargarSubmodulos(activarTabla);
                    $("#t_submodulos").dataTable();
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#submoduloModal .modal-footer [data-dismiss="modal"]').click();
                    $("#id_t2").remove();
                    $("#estado_t2").remove();
                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    
    CargarProyectoTipos: function(){
        var proyecto_tipo = functionajax('proyecto_edificio/proyectos-tipo', 'POST', false, 'json', false, {});
            proyecto_tipo.success(function(datos){
                console.log(datos);
                var html = "";
                for (var i in datos.datos){
                    html+="<option value='"+datos.datos[i].id+"'>"+datos.datos[i].nombre+"</option>";
                }
                
                $("#proyecto_tipo_submodulo").css('display', 'block');
                $("select#slct_proyecto_tipo_submodulo").html(html);
            });
    },
};
</script>