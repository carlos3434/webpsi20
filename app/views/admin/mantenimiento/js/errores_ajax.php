<script type="text/javascript">

    var Errores =
            {
                cargar: (function () {
                    var oTable = $('#datatable_error')
                            .on('order.dt', function () {
                                eventoCarga('Order');
                            })
                            .on('search.dt', function () {
                                eventoCarga('Search');
                            })
                            .on('page.dt', function () {
                                eventoCarga('Page');
                            })
                            .DataTable({
                                dom: "<'box'"+
                                    "<'box-header'<l>r>"+
                                    "<'box-body table-responsive no-padding't>"+
                                    "<'row'<'col-xs-6'i><'col-xs-6'p>>"+
                                    ">",
                                "processing": true,
                                "serverSide": true,
                                "ajax": {
                                    url: "errores/listar",
                                    type: "POST",
                                    data: function (d) {
                                        d.txt_rangofecha = $('input[name=txt_rangofecha]').val();
                                        d.per_page = 1;
                                        d.page = 2;
                                    }
                                },
                                "columns": [
                                    {data: 'id', name: 'id', visible: false},
                                    {data: 'nombre', name: 'nombre'},
                                    {data: 'url', name: 'url'},
                                    {data: 'file', name: 'file'},
                                    {data: 'code', name: 'code'},
                                    {data: 'date', name: 'date'},
                                    {data: 'comentario', name: 'comentario', visible: false, orderable: false, searchable: false},
                                    {data: 'message', name: 'message', visible: false, orderable: false, searchable: false},
                                    {data: 'trace', name: 'trace', visible: false, orderable: false, searchable: false},
                                    {data: 'actionstatus', name: 'actionstatus', orderable: false, searchable: false, className: 'text-center'},
                                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
                                ],
                                "order": [[0, 'desc']],
                               // searchDelay: 500,
                                initComplete: function (settings, json) {
                                    this.api().columns().eq(0).each(function (index) {
                                        if (index == 7 || index == 8) {
                                            return false;
                                        }
                                        var column = this.column(index);
                                        var input = document.createElement("input");
                                        var title = $(column.footer()).text();
                                        $(input).css('width', '100%')
                                                .attr('placeholder', title)
                                                .addClass('form-control input-sm')
                                                .appendTo($(column.footer()).empty())
                                                .on('change', function () {
                                                    column.search($(this).val(), false, false, true).draw();
                                                });
                                    });
                                },
                                drawCallback: function (settings) {
                                    $(".overlay,.loading-img").remove();
                                }
                                //, "language": datatable_lang
                            });
                    return oTable;
                }),
                BuscarErrores: (function () {
                    var datos = "";
                    var columnDefs = [
                        {
                            "targets": 0,
                            "data": "id",
                            "name": "id",
                            "visible": false
                        },
                        {
                            "targets": 1,
                            "data": "nombre",
                            "name": "nombre"
                        },
                        {
                            "targets": 2,
                            "data": "code",
                            "name": "code"
                        },
                        {
                            "targets": 3,
                            "data": "file",
                            "name": "file"
                        },
                        {
                            "targets": 4,
                            "data": "date",
                            "name": "date",
                            "orderable": false
                        },
                        {
                            "targets": 5,
                            "data": "comentario",
                            "name": "comentario",
                            "visible": false
                        },
                        {
                            "targets": 6,
                            "data": "message",
                            "name": "message",
                            "visible": false
                        },
                        {
                            "targets": 7,
                            "data": "estado",
                            "name": "estado",
                            "orderable": false,
                            "render": function (data, type, full) {
                                if (data == 0) {
                                    return '<span class="btn btn-success" data-toggle="modal" data-target="#errorComentarioModal"> Reparado</span>';
                                } else {
                                    return '<span class="btn btn-danger" data-toggle="modal" data-target="#errorComentarioModal"> Diligencia</span>';
                                }
                            }
                        },
                        {
                            "targets": 8,
                            "data": "id",
                            "name": "id",
                            "orderable": false,
                            "render": function (data, type, full) {
                                return '<button class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-whatever="Detalle" onclick="Errores.MostrarDetalleError(' + data + ');"><i class="fa fa-eye"></i></button>';
                            }
                        }
                    ];

                    $('#t_errores').dataTable().fnDestroy();
                    var dt = $('#t_errores')
                            .on('page.dt', function () {
                                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                            })
                            .on('search.dt', function () {
                                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                            })
                            .on('order.dt', function () {
                                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                            })
                            .DataTable({
                                "processing": true,
                                "serverSide": true,
                                "stateSave": true,
                                "stateLoadCallback": function (settings) {
                                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                                },
                                "stateSaveCallback": function (settings) {
                                    $(".overlay,.loading-img").remove();
                                },
                                "ajax": {
                                    url: "errores/buscar",
                                    type: "POST",
                                    cache: false,
                                    "data": function (d) {
                                        datos = $("#form_errores").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");

                                        for (var i = datos.length - 1; i >= 0; i--) {
                                            if (datos[i].split("[]").length > 1) {
                                                d[ datos[i].split("[]").join("[" + contador + "]").split("=")[0] ] = datos[i].split("=")[1];
                                                contador++;
                                            } else {
                                                d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                                            }
                                        }
                                        ;
                                    },
                                },
                                columnDefs
                            });
                    return dt;
                }),
                CambiarEstadoErrores: (function (id, estado) {
                    $.ajax({
                        type: 'POST',
                        url: "errores/cambiarestado",
                        cache: false,
                        data: "id=" + id + "&estado=" + estado + "&comentario=" + $('#txt_comentario').val(),
                        beforeSend: function () {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        success: function () {
//                            var aColor = 'btn-danger';
//                            var aColor2 = 'btn-success';
//                            var aTexto = 'Diligencia';
//                            if (estado == 0) {
//                                aColor = 'btn-success';
//                                aColor2 = 'btn-danger';
//                                aTexto = 'Reparado';
//                            }
//                            $('a[href=#diligencia-' + id + ']').text(aTexto).addClass(aColor).removeClass(aColor2);
                            $(".overlay,.loading-img").remove();
                            iDatatable.ajax.reload(null, false);
                            //Errores.BuscarErrores();
                        }
                    });
                }),
                MostrarDetalleError: (function (id) {
                    $('#myModal').on('show.bs.modal', function () {
                        $("#myModal").find('.modal-body textarea').val("");
                    });
                    $.ajax({
                        type: 'POST',
                        url: "errores/detalle",
                        data: "id=" + id,
                        dataType: 'json',
                        cache: false,
                        success: function (obj) {
                            $.each(obj.data, function (index, error) {
                                $("#myModal").find('.modal-title').text("Detalle Mensaje");
                                $("#myModal").find('.modal-body textarea').val(error.message);
                            });
                        }
                    });
                })

            };
</script>