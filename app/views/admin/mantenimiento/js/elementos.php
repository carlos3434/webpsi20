<script>
    var positionsFields;
    var positionsFields_edit;
    $(document).ready(function(){

        Funciones.cargarCampos();

        slctGlobal.listarSlct('geocampos','fields','multiple',null,null);
        slctGlobal.listarSlct('geocampos','slct_fields_edit','multiple',null,null);
        slctGlobal.listarSlct('figuras','figure','simple');
        slctGlobal.listarSlct('figuras','slct_figure_edit','simple');
        slctGlobal.listarSlct('geotabla','slct_elemento','simple');

        var orderArray = [];
        $("#fields").change(function(){
            var tr = '';
            var cont = 1;
            var positionsInicial = [];
            $( "#fields option:selected" ).each(function() {
                var id = $(this).val();
                tr += "<li style='display: list-item;' data-id='"+id+"' class='list-group-item ui-sortable-handle'>"+$( this ).text()+"</li>";
                var name = $(this).text();
                var pos = cont;
                var id = id;
                positionsInicial.push({index:pos,name:name,id:id});
                cont++;
            });
            positionsFields = positionsInicial;
            $("#fieldsOrder").html(tr);
            $( "#fieldsOrder" ).sortable({
                stop:function(event, ui){
                    var positions = [];
                    $(this).find("li").each(function(){
                        var name = $(this).text();
                        var pos = $(this).index() + 1;
                        var id = $(this).data("id");
                        positions.push({index:pos,name:name,id:id});
                    });
                    positionsFields = positions;
                }
            }).disableSelection();
        });

        var orderArray_edit = [];
        $("#slct_fields_edit").change(function(){
            var tr = '';
            var cont = 1;
            var positionsInicial = [];
            $( "#slct_fields_edit option:selected" ).each(function() {
                var id = $(this).val();
                tr += "<li style='display: list-item;' data-id='"+id+"' class='list-group-item ui-sortable-handle'>"+$( this ).text()+"</li>";
                var name = $(this).text();
                var pos = cont;
                var id = id;
                positionsInicial.push({index:pos,name:name,id:id});
                cont++;
            });
            positionsFields_edit = positionsInicial;
            $("#fieldsOrder_edit").html(tr);
            $( "#fieldsOrder_edit" ).sortable({
                stop:function(event, ui){
                    var positions = [];
                    $(this).find("li").each(function(){
                        var name = $(this).text();
                        var pos = $(this).index() + 1;
                        var id = $(this).data("id");
                        positions.push({index:pos,name:name,id:id});
                    });
                    positionsFields_edit = positions;
                }
            }).disableSelection();
        });

        $("#btnGuardar").click(function(){
            var rs=confirm("Se migrará la tabla indicada ¿Desea continuar?");
              if(rs){
                Funciones.generarTabla(positionsFields);
              }
        });

        $("#btnGuardar_edit").click(function(){
            var rs=confirm("Se reemplazara todas las coordenadas del elemento seleccionado ¿Desea continuar?");
            if(rs){
                if(validaElemento()){
                    Funciones.EditarElemento(positionsFields_edit);
                }
            }
        });


        $('#camposModal').on('show.bs.modal', function (event) {
            $("#form_campos").trigger('reset');
            var button = $(event.relatedTarget);
            var titulo = button.data('titulo');
            var campo_id = button.data('id');

            var modal = $(this);
            modal.find('.modal-title').text(titulo+' Campo');
            $('#form_campos [data-toggle="tooltip"]').css("display","none");
//            $("#form_actividades input[type='hidden']").remove();

            if(titulo=='Nuevo') {
                $('#form_campos #txt_token').val("<?php echo Session::get('s_token');?>");
                modal.find('.modal-footer .btn-primary').text('Guardar');
                modal.find('.modal-footer .btn-primary').attr('onClick','Agregar();');
                $('#form_campos #slct_estado').val(1);
                $('#form_campos #txt_nombre').focus();
                $('#form_campos #slct_estado').show();
                $('#form_campos .n_estado').remove();
            }
            else {
                var data = {'campo_id':camposObj[campo_id].id};

                modal.find('.modal-footer .btn-primary').text('Actualizar');
                modal.find('.modal-footer .btn-primary').attr('onClick','Editar();');
                $('#form_campos #txt_nombre').val( camposObj[campo_id].nombre );
                $('#form_campos #txt_token').val("<?php echo Session::get('s_token');?>");
                //PRIVILEGIO DESACTIVAR EN LA OPCION DE EDITAR
                if(eliminarG == 0) {
                    $('#form_campos .n_estado').remove();
                    $("#form_campos").append('<div class="n_estado"></div>');
                    $('#form_campos #slct_estado').hide();
                    $('#form_campos .n_estado').show();
                    var est = camposObj[campo_id].estado;
                    if(est == 1) est ='Activo';
                    else est = 'Inactivo';
                    $("#form_campos .n_estado").text( est );
                }
                $('#form_campos #slct_estado').val( camposObj[campo_id].estado );
                //$("#form_actividades").append("<input type='hidden' value='"+actividadObj[actividad_id].id+"' name='id'>");
                $('#form_campos input[name=txt_id]').val(camposObj[campo_id].id);
            }
            $("#form_campos #slct_estado").trigger("change");
        });

        $("#slct_elemento").change(function(){
            var data =  $(this).val().split('_');
            Funciones.CargarDetalleTabla(data[0]);
        });
    });

    HTMLCargarCampos = function(datos){
        var html="";
        $('#t_campos').dataTable().fnDestroy();

        //PRIVILEGIO AGREGAR
        if(agregarG == 0) {
            $('#nuevo').remove();
        }

        $.each(datos,function(index,data){
            estadohtml='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
            }
            //PRIVILEGIO DESACTIVAR
            if(eliminarG == 0) {
                estadohtml='<span class="">Inactivo</span>';
                if(data.estado==1){
                    estadohtml='<span class="">Activo</span>';
                }
            }
            html+="<tr>"+
                "<td>"+data.nombre+"</td>"+
                "<td>"+estadohtml+"</td>";

            //PRIVILEGIO EDITAR
            if(editarG == 1) {
                html+='<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#camposModal" data-id="'+index+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a></td>';
            } else {
                html+='<td class="editarG"></td>';
            }
            html+="</tr>";
        });
        $("#tb_campos").html(html);
        if(editarG == 0) $('.editarG').hide();
        activarTabla();
    }
    activarTabla=function(){
        $("#t_campos").dataTable();
    };
    validaCampo=function(){
        $('#form_campos [data-toggle="tooltip"]').css("display","none");
        var a=[];
        a[0]=valida("txt","nombre","");
        var rpta=true;

        for(i=0;i<a.length;i++){
            if(a[i]===false){
                rpta=false;
                break;
            }
        }
        return rpta;
    };

    validaElemento=function(){
        $('#form_campos [data-toggle="tooltip"]').css("display","none");

        var a=[];
        a[0]=valida("slct","elemento","");
        a[1]=valida("txt","name_edit","");
        a[2]=valida("txt","etiqueta_edit","");
        a[3]=valida("slct","fields_edit","");
        a[4]=valida("slct","figure_edit","");
        var rpta=true;

        for(i=0;i<a.length;i++){
            if(a[i]===false){
                rpta=false;
                break;
            }
        }
        return rpta;
    };

    valida=function(inicial,id,v_default){
        var texto="Seleccione";
        if(inicial=="txt"){
            texto="Ingrese";
        }
        if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
            $('#error_'+id).attr('data-original-title',texto+' '+id);
            $('#error_'+id).css('display','');
            return false;
        }
    };
    Agregar=function(){
        if(validaCampo()){
            Funciones.AgregarEditarCampo(0);
        }
    };
    Editar=function(){
        if(validaCampo()){
            Funciones.AgregarEditarCampo(1);
        }
    };

    desactivar=function(id){
        Funciones.CambiarEstadoCampo(id,0);
    };

    activar=function(id){
        Funciones.CambiarEstadoCampo(id,1);
    };
</script>