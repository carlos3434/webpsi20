<script type="text/javascript">
$(document).ready(function() {
    $( "#form_modulos" ).submit(function( event ) {
        event.preventDefault();
    });
    Modulos.CargarModulos();

    $('#moduloModal').on('show.bs.modal', function (event) {
        $("#modulo_id").remove();
      var button = $(event.relatedTarget); // captura al boton
      var titulo = button.data('titulo'); // extrae del atributo data-
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this); //captura el modal
      modal.find('.modal-title').text(titulo+' Modulo');
      $('#form_modulos [data-toggle="tooltip"]').css("display","none");
        if(titulo=='Nuevo'){
            //$('#form_modulos #accion_estado').css('display', "block");
            $("#btn_modulo").text('Guardar');
            $("#btn_modulo").attr('onClick','Agregar1();');
            $('#form_modulos #slct_estado1').val(1); 
            $('#form_modulos #txt_nombre1').focus();
            $('#form_modulos #slct_estado1').show();
            $('#form_modulos .n_estado').remove();
            $('#form_modulos #txt_token').val("<?php echo Session::get('s_token');?>");
        }
        else{
            //$('#form_modulos #accion_estado').css('display', "none");
            $("#btn_modulo").text('Actualizar');
            $("#btn_modulo").attr('onClick','Editar1();');
            $('#form_modulos #txt_nombre1').val( $('#t_modulos #nombre_'+button.data('id') ).text() );
            $('#form_modulos #txt_path').val( $('#t_modulos #path_'+button.data('id') ).text() );
            $('#form_modulos #txt_token').val("<?php echo Session::get('s_token');?>");
            //PRIVILEGIO DESACTIVAR EN LA OPCION DE EDITAR
            if(eliminarG == 0) {
                $('#form_modulos .n_estado').remove();
                $("#form_modulos").append('<div class="n_estado"></div>');
                //$('#form_modulos #slct_estado').hide();
                $('#form_modulos .n_estado').show();
                var est = $('#t_modulos #estado_'+button.data('id') ).attr("data-estado");
                if(est == 1) est ='Activo';
                else est = 'Inactivo';
                $("#form_modulos .n_estado").text( est );
            }
            $('#form_modulos #slct_estado1').val( $('#t_modulos #estado_'+button.data('id') ).attr("data-estado") );
            $("#form_modulos").append("<input type='hidden' id='modulo_id' value='"+button.data('id')+"' name='id'>");
        }
    });

    $('#moduloModal').on('hide.bs.modal', function (event) {
      var modal = $(this); //captura el modal
      $('#form_modulos #txt_nombre1').val(''); // busca un input para copiarle texto
      $('#form_modulos #txt_path').val('');
      $('#form_modulos #slct_estado1').val(1);
    });
    
    //**************************************************************************************************************
    $("#t_submodulos").dataTable().fnDestroy();
    Submodulos.CargarSubmodulos(activarTabla);
    $("#t_submodulos").dataTable();
    $('#submoduloModal').on('show.bs.modal', function (event) {
        $("#submodulo_id").remove();
        var button = $(event.relatedTarget); // captura al boton
        var titulo = button.data('titulo'); // extrae del atributo data-
        //var submodulo_id = button.data('id'); //extrae el id del atributo data
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
        modal.find('.modal-title').text(titulo+' Submodulo');
        $('#form_submodulos [data-toggle="tooltip"]').css("display","none");
//        $("#form_submodulos input[type='hidden']").remove();
        
        if(titulo=='Nuevo') {
            //$('#form_submodulos #accion_subEstado').css('display', "block");
            Submodulos.cargarModulos('nuevo',null);
            $("#btn_submodulo").text('Guardar');
            $("#btn_submodulo").attr('onClick','Agregar();');
            $('#form_submodulos #slct_estado2').val(1);
            $('#form_submodulos #txt_nombre').val('');
            $('#form_submodulos #txt_path').val('');
            $('#form_submodulos #txt_nombre').focus();
            $('#form_submodulos #txt_token').val("<?php echo Session::get('s_token');?>");
        }
        else {
            //$('#form_submodulos #accion_subEstado').css('display', "none");
            
            
            var proyecto_tipo = $('#t_submodulos #estado_'+button.data('id')+" input[name=proyecto_tipo]").val();
            var submoduloparent = $('#t_submodulos #estado_'+button.data('id')+" input[name=submoduloparent]").val();
            var menuvisible = $('#t_submodulos #estado_'+button.data('id')+" input[name=menu_visible]").val();
            //console.log(menuvisible);
            proyecto_tipo = parseInt(proyecto_tipo);
            submoduloparent = parseInt(submoduloparent);
            menuvisible = parseInt(menuvisible);
            //console.log(menuvisible);
            
            // Si tiene un proyecto_tipo relacionado al submodulo
            $("#proyecto_tipo_submodulo").css('display', 'none');
            if(proyecto_tipo > 0){
                $("select#opt_proyecto_tipo").val(1);
                $("#proyecto_tipo_submodulo").css('display', 'block');
                Submodulos.CargarProyectoTipos();
                $("#slct_proyecto_tipo_submodulo").val(proyecto_tipo);
            }else{
                $("select#opt_proyecto_tipo").val(0);
            }
            
            // si tiene un submodulo padre relacionado el submodulo
            $(".form-group#submodulo_parent").css('display', 'none');
            if(submoduloparent > 0){
                $(".form-group#submodulo_parent").css('display', 'block');
                var modulo_id = $('#t_submodulos #modulo_id_'+button.data('id') ).attr('modulo_id');
                if(parseInt(modulo_id) > 0){
                    $("select#opt_submodulo_parent").val(1);
                   cargarSubmodulosbyModulo(modulo_id); 
                   $("#slct_submodulo_parent").val(submoduloparent);
                }else{
                    $("select#opt_submodulo_parent").val(0);
                }
                
            }else{
                $("select#opt_submodulo_parent").val(0);
            }
            $("#slct_menu_visible").val(menuvisible);
            modulo_id=$('#t_submodulos #modulo_id_'+button.data('id') ).attr('modulo_id');
            Submodulos.cargarModulos('editar',modulo_id);
            $("#btn_submodulo").text('Actualizar');
            $("#btn_submodulo").attr('onClick','Editar();');
            $('#form_submodulos #txt_nombre').val( $('#t_submodulos #nombre_'+button.data('id') ).text() );
            $('#form_submodulos #txt_path').val( $('#t_submodulos #path_'+button.data('id') ).text() );
            $('#form_submodulos #txt_token').val("<?php echo Session::get('s_token');?>");
            //PRIVILEGIO DESACTIVAR EN LA OPCION DE EDITAR
            if(eliminarG == 0) {
                var est = $('#t_submodulos #estado_'+button.data('id') ).attr("data-estado");
                if(est == 1) est ='Activo';
                else est = 'Inactivo';
                $("#form_submodulos .n_estado").text( est );
            } else {
                $('#form_submodulos #slct_estado2').val( $('#t_submodulos #estado_'+button.data('id') ).attr("data-estado") );
            }
            $('#form_submodulos #slct_estado2').val( $('#t_submodulos #estado_'+button.data('id') ).attr("data-estado") );
            $("#form_submodulos").append("<input type='hidden' id='submodulo_id' value='"+button.data('id')+"' name='id'>");
        }

    });
    
    $('#submoduloModal').on('hide.bs.modal', function (event) {
      var modal = $(this); //captura el modal
      //modal.find('.modal-body input').val(''); // busca un input para copiarle texto
      $('#form_modulos #txt_nombre').val(''); // busca un input para copiarle texto
      $('#form_modulos #txt_path').val('');
      $('#form_modulos #slct_estado2').val(1);
    });
    
    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    
    $("[href=#tb_sudmodulo]").on('show.bs.tab',function(e){
        Submodulos.CargarSubmodulos(activarTabla);
    });
    
    
    $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            
            if(($(this).attr('href') == "#tb_modulo")) {
                $("#t_modulos").dataTable().fnDestroy();
                Modulos.CargarModulos();
            }
            if(($(this).attr('href') == "#tb_submodulo")) {
                Submodulos.CargarSubmodulos(activarTabla);
            }
        });
        
        
    $("select[name=opt_proyecto_tipo]").change(function (event) {
        if($(this).val() == 0){
            $("#proyecto_tipo_submodulo").css('display', 'none');
            $("select#slct_proyecto_tipo_submodulo").html("");
        }else{
            cargarProyectoTipos();
        }
    });
    
    $("select[name=opt_submodulo_parent]").change(function (event){
        $(".form-group#submodulo_parent").css('display', 'none');
        if($(this).val() == 0){
            $(".form-group#submodulo_parent").css('display', 'none');
            $("select#slct_submodulo_parent").html("");
        }else{
            if(parseInt($("#slct_modulo_id").val()) > 0){
                $(".form-group#submodulo_parent").css('display', 'block');
                cargarSubmodulosbyModulo($("#slct_modulo_id").val());
            }else{
                $(this).val(0);
            }
        }
    });
});    

activarTabla1=function(){
    $("#t_modulos").dataTable(); // inicializo el datatable    
};

Editar1=function(){
    if(validaModulos1()){
        Modulos.AgregarEditarModulo(1);
    }
};

activar1=function(id){
    Modulos.CambiarEstadoModulos(id,1);
};
desactivar1=function(id){
    Modulos.CambiarEstadoModulos(id,0);
};

Agregar1=function(){
    if(validaModulos1()){
        Modulos.AgregarEditarModulo(0);
    }
};

validaModulos1=function(){
    $('#form_modulos [data-toggle="tooltip"]').css("display","none");
    var a=[];
    a[0]=valida1("txt","nombre1","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};

valida1=function(inicial,id,v_default){
    var texto="Seleccione";
    if(inicial=="txt"){
        texto="Ingrese";
    }

    if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
        $('#error_'+id).attr('data-original-title',texto+' '+id);
        $('#error_'+id).css('display','');
        return false;
    }   
};

activarTabla=function(){
    $("#t_submodulos").dataTable(); // inicializo el datatable    
};

Editar=function(){
    if(validaSubmodulos()){
        Submodulos.AgregarEditarSubmodulos(1);
    }
};

activar=function(id){
    Submodulos.CambiarEstadoSubmodulos(id,1);
};

desactivar=function(id){
    Submodulos.CambiarEstadoSubmodulos(id,0);
};

Agregar=function(){
    if(validaSubmodulos()){
        Submodulos.AgregarEditarSubmodulos(0);
    }
};

validaSubmodulos=function(){
    $('#form_submodulos [data-toggle="tooltip"]').css("display","none");
        var a=[];
        a[0]=valida("txt","nombre","");
        var rpta=true;

        for(i=0;i<a.length;i++){
            if(a[i]===false){
                rpta=false;
                break;
            }
        }
        return rpta;
};

valida=function(inicial,id,v_default){
    var texto="Seleccione";
    if(inicial=="txt"){
        texto="Ingrese";
    }

    if( $.trim($("#"+inicial+"_"+id).val())==v_default ){
        $('#error_'+id).attr('data-original-title',texto+' '+id);
        $('#error_'+id).css('display','');
        return false;
    }   
};

cargarProyectoTipos = function (){
    Submodulos.CargarProyectoTipos();
};

cargarSubmodulosbyModulo = function (modulo_id){
    Modulos.CargarSubmodulosbyModulo(modulo_id);
}
</script>