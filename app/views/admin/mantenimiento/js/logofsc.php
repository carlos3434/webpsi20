<script type="text/javascript">
$(document).ready(function() {

    $("#btn_buscar").click(function (){
        buscar();
    });

    $('#fecha').daterangepicker(
        {
            format: 'YYYY-MM-DD'
        }
    );

    function print_r( obj, max, sep, level ) {
    level = level || 0;
    max = max || 10;
    sep = sep || ' ';

    if( level > max ) return "[WARNING: Too much recursion]\n";

    var i, result = '', tab = '', t = typeof obj;

    if( obj === null ) {
        result += "(null)\n";
    } else if( t == 'object' ) {
        level++;

        for( i = 0; i < level; i++ ) { tab += sep; }
        if( obj && obj.length ) { t = 'array'; }

        result += '(' + t + ") :\n";
        for( i in obj ) {
            try {
                result += tab + '[' + i + '] : ' + print_r( obj[i], max, sep, (level + 1) );
            } catch( error ) {
                return "[ERROR: " + error + "]\n";
            }
        }
    } else {
        if( t == 'string' ) {
            if( obj == '' )    obj = '(empty)';
        }
        result += '(' + t + ') ' + obj + "\n";
    }
    return result;
};

    slctGlobal.listarSlct('logofsc','slct_tipo','multiple',null,null);

    activarTabla();

     // $('#logofscModal').on('show.bs.modal', function (event) {
     // $('[name="logofscModal"]').click(function (){
     $('#logofscModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // captura al boton
        var titulo = button.data('titulo'); // extrae del atributo data-
        LogOfsc_id = button.data('id'); //extrae el id del atributo data
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this); //captura el modal
        modal.find('.modal-title').text(titulo+' Log Ofsc');
        $('#form_logofsc [data-toggle="tooltip"]').css("display","none");
        var obj1 = LogOfscObj[LogOfsc_id].body;
        $.ajax({
            url         : 'logofsc/model',
            type        : 'POST',
            cache       : false,
            contentType : "application/x-www-form-urlencoded",
            data        : {
                     e  : obj1
            },
            success : function(obj) {
                $("#modallog").html(obj);
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });

       });

});
/**
  * buscar
  *
*/

descargarReporte=function(){
     var fecha = $("#fecha").val();
     var tipo = $("#slct_tipo").val();
     var idlog = $("#txt_idlog").val();

    $("#form_logofsc").append("<input type='hidden' name='tipo2' id='tipo2' value='"+tipo+"'>");
    $("#form_logofsc").append("<input type='hidden' name='fecha2' id='fecha2' value='"+fecha+"'>");
    $("#form_logofsc").append("<input type='hidden' name='idlog2' id='idlog2' value='"+idlog+"'>");
    $("#form_logofsc").submit();
    $("#form_logofsc #tipo2 #fecha2 #idlog2").remove();
}

buscar=function(){
    LogOfsc.Cargar();
};
HTMLCargarPersona=function(datos){
    var html="";
    var tipo='';
    $('#t_log').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        var vbody=data.body;
        estadohtml='<a class="btn btn-primary btn-sm" id="#logofscModal" data-toggle="modal" data-target="#logofscModal" \n\
                    data-id="'+index+'" data-titulo="Ver"><i class="glyphicon glyphicon-search"></i> </a>';
         html+="<tr>"+
            "<td>"+data.id+"</td>"+
            "<td>"+data.type+"</td>"+
            "<td>"
            +vbody.substring(0,30)+
            "</td>"+
            "<td>"+data.created_at+"</td>"+
            "<td>"+estadohtml+"</td>";

        html+="</tr>";
    });
    $("#tb_log").html(html);
    activarTabla();
};

activarTabla=function(){
    $("#t_log").DataTable(); // inicializo el datatable
};

function cleandate(){
        $('#fecha').val('');
    }

</script>
