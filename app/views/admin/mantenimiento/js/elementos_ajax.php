<script>
    var camposObj;
    var currentPage = 0;
    var search= '';
    var Funciones = {
        generarTabla:function(datos){
            var tabla = $("#name").val();
            var figure = $("#figure").val();
            var etiqueta = $("#etiqueta").val();
            $.ajax({
                url         : 'geotabla/generartabla',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {tabla:tabla, campos:datos,figura:figure,etiqueta:etiqueta},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    console.log(1);
                }
            });
        },
        cargarCampos:function(){
            $.ajax({
                url         : 'geocampos/cargarcampos',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    var html="";
                    var estadohtml="";
                    if(obj.rst==1){
                        HTMLCargarCampos(obj.datos);
                        $('#t_campos').dataTable().fnPageChange(currentPage,true);
                        $('input[type=search]').val(search);
                        $('input[type=search]').trigger('keyup');
                        camposObj=obj.datos;
                    }
                    $(".overlay,.loading-img").remove();
                }
            });
        },
        AgregarEditarCampo:function(AE){
            var datos=$("#form_campos").serialize().split("txt_").join("").split("slct_").join("");
            var accion="geocampos/crear";
            if(AE==1){
                accion="geocampos/editar";
            }

            $.ajax({
                url         : accion,
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                        search=$('input[type=search]').val();
                        var table = $('#t_campos').DataTable();
                        var info = table.page.info();
                        currentPage = info.page;
                        $('#t_campos').dataTable().fnDestroy();
                        Funciones.cargarCampos();
                        $('input[type=search]').val(search);
                        $('input[type=search]').trigger('keyup');
                        $('#t_campos').dataTable().fnPageChange(currentPage,true);
                        $('#camposModal .modal-footer [data-dismiss="modal"]').click();
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                    else{
                        $.each(obj.msj,function(index,datos){
                            $("#error_"+index).attr("data-original-title",datos);
                            $('#error_'+index).css('display','');
                        });
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
                }
            });
        },
        CambiarEstadoCampo:function(id,AD){

            $.ajax({
                url         : 'geocampos/cambiarestado',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : 'id='+id+'&estado='+AD,//   datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                        var table = $('#t_campos').DataTable();
                        var info = table.page.info();
                        currentPage = info.page;
                        $('#t_campos').dataTable().fnDestroy();
                        Funciones.cargarCampos();
                        $('input[type=search]').val(search);
                        $('input[type=search]').trigger('keyup');
                        $('#t_campos').dataTable().fnPageChange(currentPage,true);
                        Psi.mensaje('success', obj.msj, 6000);
                    }
                    else{
                        $.each(obj.msj,function(index,datos){
                            $("#error_"+index).attr("data-original-title",datos);
                            $('#error_'+index).css('display','');
                        });
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);

                }
            });
        },
        CargarDetalleTabla:function(id){
            $.ajax({
                url         : 'geotabla/buscartabla',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {id:id},//   datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst == 1){
                        var html = '';
                        var figura;
                        for(var i = 0; i<obj.datos.length;i++)
                        {
                            figura = obj.datos[i].nombre_figura;
                            html += '<strong>'+obj.datos[i].orden+'</strong>. '+obj.datos[i].nombre_campo+'<br>';
                        }
                        html += '<label>Figura:</label> '+figura;
                        $("#detalleTabla").html(html);
                        $("#detalleDiv").show();
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        EditarElemento:function(datos)
        {
            var idTabla = $("#slct_elemento").val();
            var tabla = $("#txt_name_edit").val();
            var figure = $("#slct_figure_edit").val();
            var etiqueta = $("#txt_etiqueta_edit").val();
            $.ajax({
                url         : 'geotabla/editartabla',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {tabla:tabla, campos:datos,figura:figure,etiqueta:etiqueta,idTabla:idTabla},//   datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst == 1)
                    {
                        Psi.mensaje('success', obj.msj, 6000);
                    }else{
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        }
    }
</script>