<script type="text/javascript">
    var dataId;
    var dataEstado;
    var iDatatable;
    var idDatatable = 'datatable_error';

    eventoCarga = function (type) {
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
    };

    activar = function (id) {
        Errores.CambiarEstadoErrores(id, 1);
    };

    desactivar = function (id) {
        Errores.CambiarEstadoErrores(id, 0);
    };

    $(document).ready(function () {

        iDatatable = Errores.cargar();

        $('#' + idDatatable + ' tbody').on('click', 'a.btn-diligencia', function () {
            var tr = $(this).closest('tr');
            var table = iDatatable;
            data = table.row(tr).data();
            dataId = data.id;

            $('#errorComentarioModal').on('show.bs.modal', function () {
                $('#txt_comentario').val(data.comentario);
                if (data.estado == 1) {
                    $('#btnErrorDiligencia').css('display', 'none');
                } else {
                    $('#btnErrorDiligencia').css('display', 'inline-block');
                }
            });
            $('#errorComentarioModal').on('shown.bs.modal', function () {
                $('#txt_comentario').focus();
            });
        });

        $('#' + idDatatable + ' tbody').on('click', 'a.btn-detalle', function () {
            var tr = $(this).closest('tr');
            var table = iDatatable;
            data = table.row(tr).data();

            $('#myModal').on('show.bs.modal', function () {
                $('#txt_message').val(data.message);
                $('#txt_rastro').val(data.trace);
            });
            $('#myModal').on('hidden.bs.modal', function (e) {
                $('#txt_message').val('');
            });
        });

        $('#form_error').on('submit', function (e) {
            iDatatable.draw();
            e.preventDefault();
        });

        $('#fecha_agenda,#txt_rangofecha').daterangepicker({
            format: 'YYYY-MM-DD'
        });

        $('#btnErrorComentario').on('click', function () {
            if (dataId != '') {
                Errores.CambiarEstadoErrores(dataId, 0);
            }
        });

        $('#btnErrorDiligencia').on('click', function () {
            if (dataId != '') {
                Errores.CambiarEstadoErrores(dataId, 1);
            }
        });
    });

    mostrarErrores = function () {
        if ($.trim($("#fecha_agenda").val()) === '') {
            alert('Seleccione Rango de Fecha');
        } else {
            Errores.BuscarErrores();
            // Dar accion al clickear Button (Update Estado)
            $('#t_errores tbody').on('click', 'span', function () {
                var table = $('#t_errores').DataTable();
                var data = table.row($(this).parents('tr')).data();
                var comentario = data['comentario'];
                dataId = data['id'];
                dataEstado = (data['estado'] == 0 ? 1 : 0);

                $('#errorComentarioModal').on('show.bs.modal', function () {
                    $('#txt_comentario').val(comentario);
                    if (dataEstado == 0) {
                        $('#btnErrorDiligencia').css('display', 'none');
                    } else {
                        $('#btnErrorDiligencia').css('display', 'inline-block');
                    }
                });
                $('#errorComentarioModal').on('shown.bs.modal', function () {
                    $('#txt_comentario').focus();
                });
            });

            $('#btnErrorComentario').on('click', function () {
                Errores.CambiarEstadoErrores(dataId, 0); // dataEstado
            });

            $('#btnErrorDiligencia').on('click', function () {
                Errores.CambiarEstadoErrores(dataId, 1);
            });
        }
    };
</script>