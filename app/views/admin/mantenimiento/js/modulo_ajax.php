<script type="text/javascript">
var Modulos={
    AgregarEditarModulo:function(AE){
        var datos=$("#form_modulos").serialize().split("txt_").join("").split("slct_").join("");
        var accion="modulo/crear";
        if(AE==1){
            accion="modulo/editar";
        }

        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $('#t_modulos').dataTable().fnDestroy();

                    Modulos.CargarModulos();
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#moduloModal .modal-footer [data-dismiss="modal"]').click();
                    $('#t_modulos').dataTable();
                }
                else{
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    CargarModulos:function(){
        $.ajax({
            url         : 'modulo/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                var html="";
                var estadohtml="";
                //PRIVILEGIO AGREGAR
                if(agregarG == 0) { 
                    $('#nuevo').remove();  
                } 
                if(obj.rst==1){
                    $("#t_modulos").dataTable().fnDestroy();
                    $.each(obj.datos,function(index,data){
                        estadohtml='<span id="'+data.id+'" onClick="activar1('+data.id+')" class="btn btn-danger">Inactivo</span>';
                        if(data.estado==1){
                            estadohtml='<span id="'+data.id+'" onClick="desactivar1('+data.id+')" class="btn btn-success">Activo</span>';
                        }
                        //PRIVILEGIO DESACTIVAR
                        if(eliminarG == 0) {
                            estadohtml='<span class="">Inactivo</span>';
                            if(data.estado==1){
                                estadohtml='<span class="">Activo</span>';
                            }
                        } 
                        html+="<tr>"+
                            "<td id='nombre_"+data.id+"'>"+data.nombre+"</td>"+
                            "<td id='path_"+data.id+"'>"+data.path+"</td>"+
                            "<td id='estado_"+data.id+"' data-estado='"+data.estado+"'>"+estadohtml+"</td>";
                        //PRIVILEGIO EDITAR
                        if(editarG == 1) { 
                            html+='<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#moduloModal" data-id="'+data.id+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a></td>';
                        } else {
                            html+='<td class="editarG"></td>';
                        }
                        html+="</tr>";
                    });
                }
                $("#tb_modulos").html(html); 
                if(editarG == 0) $('.editarG').hide();  
                $(".overlay,.loading-img").remove();
                //activarTabla();//filtro de tabla
                $("#t_modulos").dataTable();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },
    CambiarEstadoModulos:function(id,AD){
        $("#form_modulos").append("<input type='hidden' id='id_t' value='"+id+"' name='id'>");
        $("#form_modulos").append("<input type='hidden' id='estado_t' value='"+AD+"' name='estado'>");
        var datos=$("#form_modulos").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'modulo/cambiarestado',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    //$('#t_modulos').dataTable().fnDestroy();
                    Modulos.CargarModulos();
                    Psi.mensaje('success', obj.msj, 6000);
                    $('#moduloModal .modal-footer [data-dismiss="modal"]').click();
                    //$('#t_modulos').dataTable();
                    $("#id_t").remove();
                    $("#estado_t").remove();
                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    
    CargarSubmodulosbyModulo: function(modulo_id){
        var submodulos = functionajax('submodulo/listar', 'POST', false, 'json', false, {modulo_id: modulo_id });
        submodulos.success(function(datos){
            console.log(datos);
            var html = "";
            for(var i in datos.datos){
                html+="<option value='"+datos.datos[i].id+"'>"+datos.datos[i].nombre+"</option>";
            }
            $("select#slct_submodulo_parent").html(html);
        });
    },

};
</script>