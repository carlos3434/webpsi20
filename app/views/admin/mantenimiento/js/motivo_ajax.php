<script type="text/javascript">
    var cuposObj = [];
    var Motivos = {
        AgregarEditarMotivos: function (AE) {
            var datos = $("#form_motivos").serialize().split("txt_").join("").split("slct_").join("");
            var accion = "motivo/crear";
            if (AE == 1) {
                accion = "motivo/editar";
            }

            $.ajax({
                url: accion,
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();/*$("body").append('<div class="overlay"></div><div class="loading-img"></div>');*/
                },
                success: function (obj) {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    if (obj.rst == 1) {
                        $('#t_motivos').dataTable().fnDestroy();

                        Motivos.CargarMotivos(activarTabla);
                        Psi.mensaje('success', obj.msj, 6000);
                        $('#motivoModal .modal-footer [data-dismiss="modal"]').click();
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    Psi.mensaje('danger', '', 6000);
                }
            });
        },
        CargarMotivos: function (evento) {
            $.ajax({
                url: 'motivo/cargar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();/*$("body").append('<div class="overlay"></div><div class="loading-img"></div>');*/
                },
                success: function (obj) {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    if (obj.rst == 1) {
                        cuposObj = obj.datos;
                        evento();
                    } else
                        $("#tb_motivos").html('');
                },
                error: function () {
                    $("#tb_motivos").html('');
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                }
            });
        },
        CambiarEstadoMotivos: function (id, AD) {
//            $("#form_motivos").append("<input type='hidden' value='" + id + "' name='id'>");
//            $("#form_motivos").append("<input type='hidden' value='" + AD + "' name='estado'>");
//            var datos = $("#form_motivos").serialize().split("txt_").join("").split("slct_").join("");
            var datos = 'id=' + id + '&estado=' + AD;
            $.ajax({
                url: 'motivo/cambiarestado',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                beforeSend: function () {
                    eventoCargaMostrar();/*$("body").append('<div class="overlay"></div><div class="loading-img"></div>');*/
                },
                success: function (obj) {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    if (obj.rst == 1) {
                        $('#t_motivos').dataTable().fnDestroy();
                        $('#t_estadomotivosubmotivos').dataTable().fnDestroy();
                        Motivos.CargarMotivos(activarTabla);
                        EstadoMotivoSubmotivo.CargarEstadoMotivoSubmotivo(activarTabla4);
                        Psi.mensaje('success', obj.msj, 6000);
                        $('#motivoModal .modal-footer [data-dismiss="modal"]').click();
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    eventoCargaRemover();/*$(".overlay,.loading-img").remove();*/
                    Psi.mensaje('danger', '', 6000);
                }
            });
        }
    };
</script>