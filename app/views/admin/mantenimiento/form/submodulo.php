<!-- /.modal -->
<div class="modal fade" id="submoduloModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">New message</h4>
            </div>
            <div class="modal-body">
                <form id="form_submodulos" name="form_submodulos" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="form-group">
                        <label class="control-label">Nombre
                            <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ruta
                            <a id="error_path" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Ruta">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control" placeholder="Ingrese Ruta" name="txt_path" id="txt_path">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Modulo:
                        </label>
                        <select class="form-control" name="slct_modulo_id" id="slct_modulo_id">
                        </select>
                    </div>
                    
                    <div class="form-group" style="display:block">
                        <label class="control-label">¿Tiene Submodulo Padre?:
                        </label>
                        <select class="form-control" name="opt_submodulo_parent" id="opt_submodulo_parent">
                            <option value='0'>NO</option>
                            <option value='1'>SI</option>
                        </select>
                    </div>
                    
                    <div class="form-group" id="submodulo_parent" style="display:none">
                        <label class="control-label">Submódulos Padre:
                        </label>
                        <select class="form-control" name="slct_submodulo_parent" id="slct_submodulo_parent">
                        </select>
                    </div>
                    <div class="form-group" id="accion_subEstado" style="display:block">
                        <label class="control-label">Estado:
                        </label>
                        <select class="form-control" name="slct_estado2" id="slct_estado2">
                            <option value='0'>Inactivo</option>
                            <option value='1' selected>Activo</option>
                        </select>
                    </div>
                    
                    <div class="form-group" style="display:block">
                        <label class="control-label">¿Requiere Tipo de Proyecto?:
                        </label>
                        <select class="form-control" name="opt_proyecto_tipo" id="opt_proyecto_tipo">
                            <option value='0'>NO</option>
                            <option value='1'>SI</option>
                        </select>
                    </div>
                    
                    <div class="form-group" id="proyecto_tipo_submodulo" style="display:none">
                        <label class="control-label">Tipo Proyecto:
                        </label>
                        <select class="form-control" name="slct_proyecto_tipo_submodulo" id="slct_proyecto_tipo_submodulo">
                        </select>
                    </div>
                    
                    <div class="form-group" style="display:block">
                        <label class="control-label">Visible en Menú:
                        </label>
                        <select class="form-control" name="slct_menu_visible" id="slct_menu_visible">
                            <option value='0'>NO</option>
                            <option value='1'>SI</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_submodulo">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->