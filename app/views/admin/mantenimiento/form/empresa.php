<!-- /.modal -->
<div class="modal fade" id="empresaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">New message</h4>
            </div>
            <div class="modal-body">
                <form id="form_empresas" name="form_empresas" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Nombre
                                <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                    <i class="fa fa-exclamation"></i>
                                </a>
                            </label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Empresa Colaboradora:
                            </label>
                            <select class="form-control" name="slct_es_ec" id="slct_es_ec">
                                <option value='0'>No</option>
                                <option value='1' selected>Si</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Buckets
                            </label>
                            <select class="form-control" name="slct_bucket[]" id="slct_bucket" multiple>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label class="control-label">Estado:
                            </label>
                            <select class="form-control" name="slct_estado" id="slct_estado">
                                <option value='0'>Inactivo</option>
                                <option value='1' selected>Activo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="control-label">CMS:
                                </label>
                                <input class="form-control" type="text" name="txt_codcms" 
                                id="txt_codcms" placeholder="Ingrese Codigo CMS"/>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">GESTEL:
                                </label>
                                <input class="form-control" type="text" name="txt_codgestel" 
                                id="txt_codgestel" placeholder="Ingrese Codigo GESTEL"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->