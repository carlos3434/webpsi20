<!-- /.modal -->
<div class="modal fade" id="asignacionMotivoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Asignacion de Motivos / Submotivos para <span id="span_nombre_usuario"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-xs-3">
                                <label class="control-label">Actividad:
                                </label>
                                <select class="form-control" name="slct_actividad_id" id="slct_actividad_id" simple="">
                                    <option value="" selected>.:: Seleccione ::.</option>
                                    <option value='1'>Averia</option>
                                    <option value='2'>Provision</option>
                                    <option value="null">Ambos</option>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <label class="control-label">Legado:
                                </label>
                                <select class="form-control" name="slct_tipo_legado" id="slct_tipo_legado" simple="">
                                    <option value="" selected>.:: Seleccione ::.</option>
                                    <option value='1'>CMS</option>
                                    <option value='2'>Gestel</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-1">
                        <a href="#" id="btn-filtro-motivos" class="btn btn-primary"><i class="fa fa-search fa-md"></i></a>
                    </div>
                </div>
                <form id="form_motivos_usuario" name="form_motivos_usuario" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                <div class="box-body table-responsive">
                                    <table id="t_motivos_usuarios" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="chck_todos"/></th>
                                                <th>Motivo</th>
                                                <th>SubMotivo</th>
                                                <th>Actividad</th>
                                                <th>Estado TOA</th>
                                                <th>T. Legado</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_motivos_usuarios">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Motivo</th>
                                                <th>SubMotivo</th>
                                                <th>Actividad</th>
                                                <th>Estado TOA</th>
                                                <th>T. Legado</th>
                                            </tr>
                                        </thead>
                                        </tfoot>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <a class='btn btn-primary btn-sm  col-md-12' id="nuevo" data-toggle="modal" data-target="#usuarioModal" data-titulo="Nuevo"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                                        </div>

                                        <div class="col-md-2">
                                            <form name="form_exportar" id="form_exportar" method="post" action="usuario/cargar" enctype="multipart/form-data">
                                                <a href="#" id="ExportExcel" class="btn btn-success btn-sm col-md-12">
                                                    <i class="fa fa-file-excel-o"></i> Exportar
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_asignacion">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->