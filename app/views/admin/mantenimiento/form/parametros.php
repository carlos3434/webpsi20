<!-- /.modal -->
<div class="modal fade" id="parametrosModal" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header logo">
            <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
               <i class="fa fa-close"></i>
            </button>
            <h4 class="modal-title">New message</h4>
         </div>
         <div class="modal-body">
            <form id="form_parametros" name="form_parametros" action="" method="post">
               <div class="row form-group">
                  <div class="col-md-6">
                     <div class="col-md-12" style="margin-bottom:20px">
                        <div class="box-body table-responsive">
                           <div style="height:500px">
                              <table id="t_criterio" class="table table-bordered table-striped">
                                 <thead>
                                    <tr>
                                       <th>Nombre</th>
                                       <th>Selección</th>
                                       <th>Ver</th>
                                    </tr>
                                        </thead>
                                        <tbody id="tb_criterio">
                                        </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6" style="height:500px;overflow-y: scroll;">      
                     <fieldset class="fieldsetblase">
                     <!--<legend class="legendblade"></legend>-->
                     <div class="col-sm-12">
                        <label>Nombre Parametro</label>
                        <input class="form-control" type='text' name='txt_nombre' id='txt_nombre'>
                     </div>
                     <div class="col-md-12">
                        <label>Tipo Parametro</label>
                        <select class="form-control" name="slct_tipo_modal" id="slct_tipo_modal" disabled="disabled">
                           <option value="1" selected="">LEGADO a PSI</option>
                           <option value="2">PSI a TOA</option>
                           <option value="4">MASIVA</option>
                           <option value="6">ENVIO PSI TEST</option>
                        </select>
                     </div>
                     <div class="col-md-12" id="div_proveniencia">
                        <label>Sistema</label>
                        <select class="form-control" name="slct_proveniencia_modal" id="slct_proveniencia_modal">
                           <option value="1" >BANDEJA GESTIÓN</option>
                           <option value="2" selected="">CMS</option>
                           <option value="3" >GESTEL PROVISIÓN</option>
                           <option value="4" >GESTEL AVERÍA</option>
                        </select>
                     </div>
                     <div class="col-md-12" id="div_bucket" style="">
                        <label>Bucket</label>
                        <select class="form-control" name="slct_bucket_modal" id="slct_bucket_modal">
                        </select>
                     </div>
                     <div class="col-md-12" id="div_masiva" style="display:none">
                        <label>Tipo Masiva</label>
                        <select class="form-control" name="slct_tipo_masiva_id_modal" id="slct_tipo_masiva_id_modal">
                        </select>
                     </div>
                     </fieldset>
                     <fieldset class="fieldsetblase">
                     <legend class="legendblade">Criterios Seleccionados</legend>
                     <div class="col-sm-12">
                     <!--<h3>Selección</h3>-->
                     </div>
                     <div id="combos">
                     </div> 
                     </fieldset>                   
                     <!--<div class="col-sm-8" >
                        <label class="control-label">Otra Opcion: Adjuntar Archivo</label>
                        <input id="txt_file_plan" name="txt_file_plan" type="file">
                     </div>
                     <div class="col-sm-4">
                        <button class="btn btn-primary" type="button" id="btnGuardar" onClick="CargarArchivos()" style="margin-top:25px">Cargar Archivo</button>
                     </div> -->
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Guardar</button>
         </div>
      </div>
   </div>
</div>