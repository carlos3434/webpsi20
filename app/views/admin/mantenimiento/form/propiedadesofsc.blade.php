<!-- /.modal -->
<div class="modal fade" id="propiedadesOfscModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Titulo</h4>
            </div>

            <div class="modal-body">

                <form id="form_propiedadesOfsc" name="form_propiedadesOfsc" action="" method="post">

                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />

                    <div class="form-group">
                        <label class="control-label">Etiqueta
                            <a id="error_etiqueta" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Etiqueta">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>

                        <input type="text" class="form-control" placeholder="Ingrese Etiqueta" name="txt_etiqueta" id="txt_etiqueta">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Tipo tecnología:
                        </label>
                        <select class="form-control" name="slct_tipo_tecnologia" id="slct_tipo_tecnologia">
                            <option value='-1' selected>Seleccionar</option>
                            <option value='1' >CATV</option>
                            <option value='2'>Speedy</option>
                            <option value='3'>Basica</option>
                            <option value='4'>DTH</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Actividad:
                        </label>
                        <select class="form-control" name="slct_actividad_id" id="slct_actividad_id">
                            <option value='-1'>Seleccionar</option>
                            <option value='1' >Averia</option>
                            <option value='2'>Provisión</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Tipo:
                        </label>
                        <select class="form-control" name="slct_tipo" id="slct_tipo">
                            <option value='M' selected>Motivo</option>
                            <option value='S'>Sub Motivo</option>
                            <option value='T'>Tipo</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Estado:
                        </label>
                        <select class="form-control" name="slct_estado" id="slct_estado">
                            <option value='1' selected>Activo</option>
                            <option value='0'>Inactivo</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
                <button type="button" class="btn btn-primary"><?php echo trans('main.Save') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->