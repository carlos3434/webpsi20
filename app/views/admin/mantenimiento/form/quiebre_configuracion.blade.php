<!-- /.modal -->
<div id="quiebreModalConfiguracion" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header logo">
            <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
            <i class="fa fa-close"></i>
            </button>
            <h4 class="modal-title">****</h4>
         </div>
         <div class="modal-body">
            <form id="form_caso" name="form_caso" action="" method="post">
               <div class="row form-group">
                  <div class="col-md-6">
                     <div class="col-md-12" style="margin-bottom:20px">
                        <div class="box-body table-responsive">
                           <div style="height:500px">
                              <table id="t_criterio" class="table table-bordered table-striped">
                                 <thead>
                                    <tr>
                                       <th>Nombre</th>
                                       <th>Selección</th>
                                       <th>Ver</th>
                                    </tr>
                                 </thead>
                                 <tbody id="tb_criterio">
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6" style="height:500px;overflow-y: scroll;">
                     <fieldset class="fieldsetblase">
                        <!--<legend class="legendblade"></legend>-->
                        <div class="col-md-12" id="div_proveniencia">
                            <label>Sistema</label>
                            <select class="form-control" name="slct_proveniencia" id="slct_proveniencia">
                               <option value="2" selected="">CMS</option>
                               <option value="3" >GESTEL PROVISIÓN</option>
                               <option value="4" >GESTEL AVERÍA</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label>Casos</label>
                            <select class="form-control" name="slct_caso" id="slct_caso">
                                <option value=''>Nuevo</option>
                            </select>
                        </div>

                     </fieldset>
                     <fieldset class="fieldsetblase">

                        <legend class="legendblade">&nbsp;&nbsp;Criterios Seleccionados</legend>

                        <div class="checkbox">
                            <label>
                              <input id="special_case-1" type="checkbox">
                              Nodo y troba de microzonas
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                              <input id="special_case-2" type="checkbox">
                              Nodo y troba Digitalizados
                            </label>
                        </div>
                        
                        <div id="combos">
                        </div>

                     </fieldset>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button caso_id="" id="btn_eliminar" style="display: none;" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
            <button id="guardar_caso" type="button" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->

<style>
    #order_container_div {
        max-height: 400px;
        overflow-y: scroll;
        overflow-x: hidden;
        text-align: center;
    }

    #order_container_div #sortable {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    #order_container_div #sortable li {
        height: 90px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: solid 1px silver;
        border-radius: 5px;
        font-size: 12px;
        background-color: white;
        background-color: #00a65a;
        color: white;
        font-weight: 700;
        padding: 3px;
        width: 100%;
        position: relative;
    }

    #order_container_div #sortable li span{
      position: absolute;
      right: 5px;
      top: 5px;
    }
    
    #order_container_div.grid #sortable {
        width: 550px;
    }

    #order_container_div.grid #sortable li {
        margin: 3px 3px 3px 0;
        float: left;
        width: 106px;
    }

    #sortable li:hover {
        cursor: pointer;
    }
</style>

<!-- /.modal -->
<div id="ordenModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Prioridad quiebres</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 grid" id="order_container_div">
                    </div>
                    <div class="col-md-4">
                        <form>
                            <div class="form-group">
                                <label>Sistema</label>
                                <select id="sistema_order_select" class="form-control">
                                    <option value="2" selected="">CMS</option>
                                    <option value="3" >GESTEL PROVISIÓN</option>
                                    <option value="4" >GESTEL AVERÍA</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tipo de actividad</label>
                                <select id="tipo_actividad_order_select" class="form-control">
                                    <option>Seleccionar</option>
                                </select>
                            </div>
                            <div>
                              <label style="padding-left: 0px; font-weight: normal; display: flex;">
                                  <span style="color: white; width: 22px; height: 22px; background-color: #00a65a; border-radius: 50%; display: flex; align-items: center;  justify-content: center; font-size: 10px;" class='glyphicon glyphicon-flag'></span>&nbsp; Quiebres ya configurados.
                              </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>  
            <div class="modal-footer">
                <button id="guardar_orden" type="button" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->


