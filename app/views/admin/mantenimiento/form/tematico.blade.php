<!-- /.modal -->
<div class="modal fade" id="tematicoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Nuevo Tematico</h4>
            </div>
            <div class="modal-body">
                <form id="form_tematicos" name="form_tematicos" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="form-group">
                        <label class="control-label">Nombre
                            <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Descripcion
                            <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <textarea row="2" class="form-control" style="resize: none;" placeholder="Ingrese Descripcion" name="txt_descripcion" id="txt_descripcion"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Actividad:
                        </label>
                        <select class="form-control" name="slct_actividad_id" id="slct_actividad_id" simple="">
                            <option value="" selected>.:: Seleccione ::.</option>
                            <option value='1'>Averia</option>
                            <option value='2'>Provision</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Legado:
                        </label>
                        <select class="form-control" name="slct_tipo_legado" id="slct_tipo_legado" simple="">
                            <option value="" selected>.:: Seleccione ::.</option>
                            <option value='1'>CMS</option>
                            <option value='2'>Gestel</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Estados a los que Aplica:
                        </label>
                        <select class="form-control" name="slct_opciones[]" id="slct_opciones" multiple="">
                            <option value='is_pendiente'>OFSC : Pendiente</option>
                            <option value='is_no_realizado'>OFSC : No Realizado</option>
                            <option value='is_completado'>OFSC : Completado</option>
                            <option value='is_pre_devuelto'>Situacion : Pre Devuelto</option>
                            <option value='is_pre_liquidado'>Situacion : Pre Liquidado</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Motivos OFSC:
                        </label>
                        <select class="form-control" name="slct_motivos_ofsc[]" id="slct_motivos_ofsc" simple="">
                        </select>
                    </div>
                    <div class="form-group" id="div_tipo_devolucion" style="display: none;">
                        <label class="control-label">Tipo de Devolucion:
                        </label>
                        <select class="form-control" name="slct_tipo" id="slct_tipo" simple="">
                            <option value="" selected>.:: Seleccione ::.</option>
                            <option value='C'>Comercial</option>
                            <option value='T'>Tecnico</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tematico Padre:
                        </label>
                        <select class="form-control" name="slct_parent" id="slct_parent">
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_tematico">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->