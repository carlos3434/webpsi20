<!-- /.modal -->
<div class="modal fade" id="PerfilModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Perfil <span id="span_nombre_perfil"></span></h4>
            </div>
            <div class="modal-body">
                <form id="form_perfil" name="form_tematicos" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="form-group">
                        <label class="control-label">Nombre
                            <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                <i class="fa fa-exclamation"></i>
                            </a>
                        </label>
                        <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombre" id="txt_nombre">
                    </div>
                    <fieldset>
                        <legend><label>Opciones de Perfil</label></legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <select name="slct_opcion" id="slct_opcion" class="form-control"></select>
                                </div>
                                <div class="col-md-4">
                                    <a href="" class="btn btn-success pull-right btn-agregar"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="box">
                                    <div class="table-responsive box-body">
                                        <table id="t_perfil_opciones" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Opcion</th>
                                                    <th style="width: 80px;">[]</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tb_perfiles_opciones">
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Opcion</th>
                                                    <th style="width: 80px;">[]</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_guardar_perfil">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->