<!-- /.modal -->
<!-- <div class="modal fade" id="logofscModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog"> -->
        <div class="modal-content">
            <div class="modal-header logo">
                <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title">Body</h4>
            </div>
            <div class="modal-body">
                <form id="form_logofsc" name="form_logofsc" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token'); ?>" />
                    <div class="form-group">
                        <div>
                            <label class="control-label">Body:
                                <a id="error_nombre" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                    <i class="fa fa-exclamation"></i>
                                </a>
                            </label>
                            <pre id="bodylogOfsc" name="bodylogOfsc" style="word-wrap:break-word; padding:5px; background-color:#f3f4f5; border-radius:5px;">
                                    <?php
                                        print_r($result);
                                    ?>
                            </pre>
                            <!-- <div id="bodylogOfsc" name="bodylogOfsc" style="word-wrap:break-word; padding:5px; background-color:#f3f4f5; border-radius:5px;">

                            </div> -->
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo trans('main.Close') ?></button>
            </div>
        </div>
 <!--    </div>
</div> -->
<!-- /.modal -->