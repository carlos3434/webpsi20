<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent
{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.mantenimiento.js.elementos_ajax' )
@include( 'admin.mantenimiento.js.elementos' )
@stop
        <!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style>
.col-md-12 {
    margin: 5px 0;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Mantenimiento de Elementos
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Mantenimiento de Elementos</li>
    </ol>
</section>
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <!-- Inicia contenido -->
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tb_tabla" role="tab" data-toggle="tab">Elementos</a>
                    </li>
                    <li role="presentation">
                        <a href="#tb_camposTab" role="tab" data-toggle="tab">Campos</a>
                    </li>
                    <li role="presentation">
                        <a href="#tb_editar" role="tab" data-toggle="tab">Editar Elemento</a>
                    </li>
                </ul>
    
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tb_tabla">
                        <form id="form_elements">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Tabla</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Tabla a migrar">
                                </div>
                                <div class="form-group">
                                    <label for="">Etiqueta</label>
                                    <input type="text" class="form-control" name="etiqueta" id="etiqueta" placeholder="Nombre a mostrar en la lista">
                                </div>
                                <div class="form-group">
                                    <label for="">Figura</label>
                                    <select class="form-control" name="figure" id="figure"></select>
                                </div>
                                <div class="form-group">
                                    <label for="">Campos</label>
                                    <select multiple="multiple"  class="form-control" name="fields" id="fields"></select>
                                </div>
                                <button id="btnGuardar" type="button" class="btn btn-success">Guardar</button>
                            </div><!-- /.box -->
                            <div class="col-md-6">
                                <h3>Orden de Campos</h3>
                                <ol id="fieldsOrder" class="list-group"></ol>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tb_camposTab">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Campos</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <table id="t_campos" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th class="editarG"> [ ] </th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_campos">
    
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Estado</th>
                                        <th class="editarG"> [ ] </th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <a class='btn btn-primary btn-sm' class="btn btn-primary nuevo"
                                   data-toggle="modal" data-target="#camposModal" data-titulo="Nuevo"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tb_editar">
                        <form id="form_elements_edit">
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label >Elemento
                                        <a id="error_elemento" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Seleccione Elemento">
                                            <i class="fa fa-exclamation"></i>
                                        </a>
                                    </label>
                                    <select class="form-control"  id="slct_elemento" name="slct_elemento" >
                                            <option value="">-Seleccione-</option>
                                     </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Tabla
                                        <a id="error_name_edit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                            <i class="fa fa-exclamation"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control" name="txt_name_edit" id="txt_name_edit" placeholder="Tabla a migrar">
                                </div>
                                <div class="form-group">
                                    <label >Etiqueta
                                        <a id="error_etiqueta_edit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Etiqueta">
                                            <i class="fa fa-exclamation"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control" name="txt_etiqueta_edit" id="txt_etiqueta_edit" placeholder="Nombre a mostrar en la lista">
                                </div>
                                <div class="form-group">
                                    <label >Campos
                                        <a id="error_fields_edit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Seleccione Campos">
                                            <i class="fa fa-exclamation"></i>
                                        </a>
                                    </label>
                                    <select multiple="multiple"  class="form-control" name="slct_fields_edit" id="slct_fields_edit"></select>
                                </div>
                                <div class="form-group">
                                    <label >Figura
                                        <a id="error_figure_edit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                                            <i class="fa fa-exclamation"></i>
                                        </a>
                                    </label>
                                    <select class="form-control" name="slct_figure_edit" id="slct_figure_edit"></select>
                                    <br>
                                </div>
                                <button id="btnGuardar_edit" type="button" class="btn btn-success">Actualizar</button>
                                <br><br><br><br><br>
                            </div><!-- /.box -->
                            <div class="col-md-6" style="display:none;" id="detalleDiv">
                                <h3>Detalle de Tabla</h3>
                                <h4>Campos:</h4>
                                <div id="detalleTabla"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h3>Orden de Campos</h3>
                                    <ol id="fieldsOrder_edit" class="list-group"></ol>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.col -->
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!--
<section class="content-header">
    <h1>
        Mantenimiento de Elementos
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Mantenimiento de Elementos</li>
    </ol>
</section> -->
@stop

@section('formulario')
    @include( 'admin.mantenimiento.form.campos' )
@stop
