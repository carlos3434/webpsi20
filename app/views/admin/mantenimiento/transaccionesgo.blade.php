<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/backbone/js/json2.js') }}
{{ HTML::script('lib/backbone/js/underscore.min.js') }}
{{ HTML::script('lib/backbone/js/backbone.min.js') }}
{{ HTML::script('lib/backbone/js/handlebars.js') }}

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@stop


@section('contenido')

    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Transacciones GO
            <small> </small>
        </h1>

    <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Transacciones GO</li>
        </ol>

    </section>


    <div class="container">

        <br>

    <form id="transaccion_form" method="POST" action="/transaccionGo/descargarexcel"  class="form-inline">

                <input id="tipo" type="hidden" name="tipo">

                <div class="form-group">
                    <label for="txt_fecha">Fecha: &nbsp;</label>
                    <div class="input-group" style="min-width: 215px;">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                            <input class="form-control" id="txt_rangofecha" name="txt_rangofecha" type="text"  aria-describedby="basic-addon1" readonly>
                    </div>
                </div>

                &nbsp;&nbsp;

                <div class="form-group">
                    <button id="btn-d-tgo" type="submit" class="btn btn-success">Descargar Transacciones &nbsp;<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></button>
                </div>

                <div class="form-group">
                    <button id="btn-d-e" type="submit" class="btn btn-warning">Descargar Estadisticas &nbsp;<span class="glyphicon glyphicon-signal" aria-hidden="true"></span></button>
                </div>

        </form>


        </div>


<script type="text/javascript" src="js/admin/mantenimiento/transaccion_go.js?v={{ Cache::get('js_version_number') }}"></script>
@stop