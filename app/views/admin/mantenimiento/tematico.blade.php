<!DOCTYPE html>
@extends("layouts.masterv2")

@push("stylesheets")
    {{ HTML::style("lib/daterangepicker/css/daterangepicker-bs3.css") }}
    {{ HTML::style("lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css") }}
    {{ HTML::style("lib/iCheck/all.css") }}
    {{ HTML::style("lib/sweetalert/css/sweetalert.min.css") }}
    {{ HTML::style('lib/bootstrap-fileinput/css/fileinput.min.css') }}
    {{ HTML::script('lib/bootstrap-fileinput/js/fileinput.min.js') }}
    {{ HTML::script('lib/ajaxUpload/ajaxupload.min.js') }}
@endpush

<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<style>
    /* Content & sidebar */
    legend{
      font-size:14px;
      font-weight: 700;
      width: 20%;
      border-bottom: 0px;
      margin-bottom: 5px;
      color:  #8d9ba8 ;
    }
    
    .btn-group.col-xxs-12 {padding: 0px!important;}

    .btn-yellow{
      color: #508d76;
      background-color: #f0f0f0;
      border-color: #ccc;
      font-weight: bold;
    }
    .btn-yellow:hover{
      color: #259f71;
      background-color: #ebf4f0;
    }
    .btn-purple{
      color: white;
      background-color: #715481;
    }
    .btn-purple:hover{
      color: white;
      background-color: #6C09A6;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Mantenimiento de Tematicos
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="#">Mantenimientos</a></li>
        <li class="active">Tematicos</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div>
          <ul id="myTab" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active" role="tab" data-toggle="tab">
              <a href="#tb_carga" id="carga">Carga</a>
            </li>
            <li role="presentation" role="tab" data-toggle="tab">
              <a href="#tb_cargamasiva" id="cargamasiva">Carga Masiva</a>
            </li>
          </ul>
      </div>
      <!-- Inicia contenido -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tb_carga">
                  <div class="row">
                      <div class="col-xs-12">
                        <div class="box" >
                          <br>
                          <div class="row">
                            <form class="form-inline" style="margin-bottom: 100px; min-height: 200px" id="form_analizador">
                              <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label>ESTADOS: </label>
                                    <select class="form-control" name="slct_situacion" id="slct_situacion">
                                        <option value="" selected>.:: Seleccione ::.</option>
                                        <option value='is_pendiente'>Pendiente</option>
                                        <option value='is_pre_devuelto'>Pre Devuelto</option>
                                        <option value='is_pre_liquidado'>Pre Liquidado</option>
                                        <option value='is_no_realizado'>No Realizado</option>
                                        <option value='is_completado'>Compleatado</option>
                                    </select>
                                </div>
                                <div class="col-sm-9">
                                  <a class='btn btn-success btn-sm pull-right' style="margin-top:30px;" id="nuevo" data-toggle="modal" data-target="#tematicoModal" data-titulo="Nuevo" data-id="0">
                                    <i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo
                                  </a>
                                </div>
                              </div>

                              <div id="divTematicos" class="col-md-12" style="margin-top: 10px;">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                  <div class="panel-group" id="accordion">
                                    <div class="panel panel-info">
                                      <!-- data-toggle="collapse" -->
                                      <div class="panel-heading" data-parent="#accordion" href="#collapse_cliente_panel" style="cursor: pointer">
                                        <h4 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel">TEMATICOS</a>
                                        </h4>
                                      </div>
                                      
                                      <div id="collapse_cliente_panel" class="panel-collapse collapse in" aria-expanded="true">
                                        <div class="panel-body">
                                          <div class="row" >
                                            <div class="col-sm-12">
                                              <fieldset>
                                                <legend>FILTROS </legend>
                                                <div class="col-md-3">
                                                  <label>Nombre</label><br>
                                                    <input type="text" id="txt_nombtematico" class="form-control" placeholder="Ingresar Nombre" style="width: 100%">
                                                </div>
                                                <div class="col-md-2">
                                                  <label>Actividad</label>
                                                    <select class="form-control" name="slct_actividad_id_filtro" id="slct_actividad_id_filtro">
                                                      <option value="" selected>.:: Seleccione ::.</option>
                                                      <option value='1'>Averia</option>
                                                      <option value='2'>Provision</option>
                                                    </select>                               
                                                </div>
                                                <div class="col-md-2">
                                                  <label>T. Devolucion</label>
                                                  <select class="form-control" name="slct_actividad_id_filtro" id="slct_tipodevolucion_filtro">
                                                    <option value="" selected>.:: Seleccione ::.</option>
                                                    <option value='C'>Comercial</option>
                                                    <option value='T'>Tecnica</option>
                                                  </select>
                                                </div>
                                                <div class="col-md-2">
                                                  <label>Tipo Legado</label>
                                                  <select class="form-control" name="slct_tipo_legado_filtro" id="slct_tipo_legado_filtro">
                                                    <option value="" selected>.:: Seleccione ::.</option>
                                                    <option value='1'>CMS</option>
                                                    <option value='2'>Gestel</option>
                                                  </select>
                                                </div>
                                                <div class="col-md-1">
                                                  <a id="btn-buscar" href="#" class="btn btn-danger btn-sm" style="margin-top:26px;"><i class="fa fa-search"></i>&nbsp;Buscar</a>
                                                </div>                              
                                              </fieldset>
                                            </div>
                                          </div>
                                          <br>
                                          <div class="row">
                                            <div class="box-body table-responsive">
                                              <table id="t_tematicos" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th>      </th>
                                                    <th>Nombre</th>
                                                    <th>Padre</th>
                                                    <th>Legado</th>
                                                    <th>Actividad</th>
                                                    <th>Tipo</th>
                                                    <th>Estado</th>
                                                    <th class="editarG"> [ ] </th>
                                                  </tr>
                                                </thead>
                                                <tbody id="tb_pendientes">
                                                
                                                </tbody>
                                                <tfoot>
                                                  <tr>
                                                    <th>      </th>
                                                    <th>Nombre</th>
                                                    <th>Padre</th>
                                                    <th>Legado</th>
                                                    <th>Actividad</th>
                                                    <th>Tipo</th>
                                                    <th>Estado</th>
                                                    <th class="editarG"> [ ] </th>
                                                  </tr>
                                                </tfoot>
                                              </table>
                                            </div><!-- /.box-body -->
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>

                <div role="tabpanel" class="tab-pane fade in active" id="tb_cargamasiva" style="display: none;">
                  <div class="tab-content">
                    <div id="registro" class="tab-pane fade in active">
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header ui-sortable-handle">
                                <h3 class="box-title">SUBIDA MASIVA DE TEMATICOS </h3>
                            </div>
                            <div class="box-body border-radius-none">
                                <div class="row">
                                  <div class="col-sm-2">
                                    <div class="col-sm-12 normal form-group">
                                      <form id="form_parcial_columna" name="form_parcial_columna"  role="form" class="form-inline" enctype="multipart/form-data">
                                        <div class="form-group">
                                          <div class="input-group">
                                            <button class="btn btn-md btn-primary" type="button" id="btnSubir">Subir Archivo .Csv</button>
                                          </div> 
                                        </div>
                                      </form>
                                      <!--<small style="color: #000;">(*) Subir archivos xls, con el formato descargado.</small><br>-->
                                    </div>
                                  </div>
                                  <div class="col-sm-10">
                                    <fieldset>
                                        <legend>CAMPOS </legend>
                                        <div class="col-md-2">
                                          <b class="text-primary">1: Tipo Legado</b><br>
                                          <b class="text-primary">2: Activid</b><br> 
                                          <b class="text-primary">3: Motivo ofsc</b><br>
                                          <b class="text-primary">4: Tipo Devolucion</b><br>
                                        </div>
                                        <div class="col-md-2">
                                          <b class="text-primary">4: Tematico 1</b><br>
                                          <b class="text-primary">6: Tematico 2</b><br> 
                                          <b class="text-primary">7: Tematico 3</b><br>
                                          <b class="text-primary">8: Tematico 4</b><br>
                                        </div>
                                        <div class="col-md-3">
                                          <b class="text-primary">9: Pendiente</b>
                                          <b class="text-danger">('SI' ó 'NO')</b>
                                          <br>
                                          <b class="text-primary">10: Pre Devuelto</b>
                                          <b class="text-danger">('SI' ó 'NO')</b>
                                          <br>
                                          <b class="text-primary">11: No Realizado</b>
                                          <b class="text-danger">('SI' ó 'NO')</b>
                                          <br>
                                          <b class="text-primary">12: Pre Liquidado</b>
                                          <b class="text-danger">('SI' ó 'NO')</b>
                                          <br>
                                        </div>
                                        <div class="col-md-3">                                        
                                          <b class="text-primary">13: Completado</b>
                                          <b class="text-danger">('SI' ó 'NO')</b>
                                          <br>
                                        </div>                              
                                    </fieldset>
                                  </div>
                                  <div class="col-sm-12 col-sm-5">
                                    <div class="alert alert-danger alert_modal" role="alert" 
                                    style="display:none;">
                                    </div>
                                    <div class="alert alert-success alert_modal_success" role="alert" 
                                    style="display:none;"> 
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <h4>
                                    <button id="btnHiddenExcel" class="hidden" ></button>
                                    <span class="btn btn-success btnSaveALl btn-xs">Guardar<i class="fa fa-save fa-lg" style="margin-left: 5px;"></i></span>
                                    <span class="btn btn-danger btnCancelALl btn-xs">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;""></i></span>
                                  </h4>
                                  <!--<form name="form_reportecargmasiva" id="form_reportecargmasiva" method="post" action="zonapremiumcatv/excelcargamasiva" enctype="multipart/form-data" style="display: none;">
                                      (Descargar formato xls <a id="link_descarga" href="#">aqui</a>)
                                  </form>-->
                                </div>
                                
                                <div class="table margin-bottom-5" style="overflow: scroll;max-height: 1000px">
                                  <table id="tbl_cargamasiva" class=" table-responsive table table-hover table table-bordered margin-0" border="1">
                                    <thead>
                                      <tr style="color:white;background: #6F9DC2;">
                                        <th style="width: 5%">TIPO <br>LEGADO</th>
                                        <th style="width: 8%">ACTIVIDAD</th>
                                        <th style="width: 10%">MOTIVO OFSC</th>
                                        <th style="width: 8%">TIPO DEVOLUCION</th>
                                        <th style="width: 10%">TEMATICO 1</th>
                                        <th style="width: 10%">TEMATICO 2</th>
                                        <th style="width: 10%">TEMATICO 3</th>
                                        <th style="width: 10%">TEMATICO 4</th>
                                        <th style="width: 1%">PEND.</th>
                                        <th style="width: 1%">PRE<br>DEV.</th>
                                        <th style="width: 1%">NO<br>REAL.</th>
                                        <th style="width: 1%">PRE<br>LIQ.</th>
                                        <th style="width: 1%">COMP.</th>
                                        <th style="width: 5%;"></th>
                                      </tr>
                                    </thead>
                                    <tbody style="color: black;">
                                    </tbody>
                                    <tfoot>                     
                                      <tr class="repeat hidden">
                                        <td>
                                            <input class='form-control  hd_index' type='hidden'/>
                                            <input class='form-control  hd_tipolegado' type='hidden'/>
                                            <select class="slct_tipolegado_masiva" 
                                            name="slct_tipolegado_masiva" id="slct_tipolegado_masiva">
                                                <option value="" selected>.:: Seleccione ::.</option>
                                                <option value='CMS'>CMS</option>
                                                <option value='GESTEL'>GESTEL</option>
                                            </select>
                                        </td> 
                                        <td>
                                            <input class='form-control  hd_actividad' type='hidden' style="width: 100%"/>
                                            <select class="slct_actividad_masiva" 
                                            name="slct_actividad_masiva" id="slct_actividad_masiva">
                                                <option value="" selected>.:: Seleccione ::.</option>
                                                <option value='AVERIA'>Averia</option>
                                                <option value='PROVISION'>Provision</option>
                                            </select>
                                        </td>
                                        <td style="width:23%;">
                                            <input class='hd_motivoofsc' type='hidden' style="width: 100%"/>
                                            <select class="slct_motivoofsc_masiva" 
                                            name="slct_motivoofsc_masiva" id="slct_motivoofsc_masiva" 
                                            style="width: 90%">
                                            </select>
                                        </td>
                                        <td>
                                            <input class='form-control  hd_tipodevolucion' type='hidden' style="width: 100%"/>
                                            <select class="slct_tipodevolucion_masiva" 
                                            name="slct_tipodevolucion_masiva" 
                                            id="slct_tipodevolucion_masiva">
                                                <option value="" selected>.:: Seleccione ::.</option>
                                                <option value='COMERCIAL'>Comercial</option>
                                                <option value='TECNICO'>Tecnica</option>
                                            </select>
                                        </td>
                                        <td style="width:8%;">
                                          <input class='form-control  hd_tematico1' type='hidden' style="width: 100%"/>
                                          <input class='form-control  txt_tematico1_masiva' type='text' style="width: 100%" placeholder="Tematico 1"/>
                                        </td>
                                        <td style="width:8%;">
                                          <input class='form-control  hd_tematico2' type='hidden' style="width: 100%"/>
                                          <input class='form-control  txt_tematico2_masiva' type='text' 
                                          style="width: 100%" placeholder="Tematico 2"/>
                                        </td>
                                        <td style="width:8%;">
                                          <input class='form-control  hd_tematico3' type='hidden' style="width: 100%"/>
                                          <input class='form-control  txt_tematico3_masiva' type='text' style="width: 100%" placeholder="Tematico 3"/>
                                        </td>
                                        <td style="width:8%;">
                                          <input class='form-control  hd_tematico4' type='hidden' style="width: 100%"/>
                                          <input class='form-control  txt_tematico4_masiva' type='text' style="width: 100%" placeholder="Tematico 4"/>
                                        </td>
                                        <td >
                                          <input class='form-control  hd_pendiente' type='hidden' style="width: 100%"/>
                                          <input class="chk_pendiente_masiva" type="checkbox" value="1"
                                          id="chk_pendiente_masiva">
                                        </td>
                                        <td >
                                          <input class='form-control  hd_predevuelto' type='hidden' style="width: 100%"/>
                                          <input class="chk_predevuelto_masiva" type="checkbox" value="1"
                                          id="chk_predevuelto_masiva">
                                        </td>
                                        <td >
                                          <input class='form-control  hd_norealizado' type='hidden' style="width: 100%"/>
                                          <input class="chk_norealizado_masiva" type="checkbox" value="1" 
                                          id="chk_norealizado_masiva">
                                        </td>
                                        <td >
                                          <input class='form-control  hd_preliquidado' type='hidden' style="width: 100%"/>
                                          <input class="chk_preliquidado_masiva" type="checkbox" value="1" id="chk_preliquidado_masiva">
                                        </td>
                                        <td >
                                          <input class='form-control  hd_completado' type='hidden' style="width: 100%"/>
                                          <input class="chk_completado_masiva" type="checkbox" value="1" 
                                          id="chk_completado_masiva">
                                        </td>
                                        <td>
                                          <a class="btn btn-purple btnSaveAdd btn-xs" data-toggle='modal' title='Guardar registro' style="display: none;">Guardar<i class="fa fa-save fa-lg" style="margin-left: 5px;"></i>
                                          </a>
                                          <a class="btn btn-purple btnCancelAdd btn-xs" data-toggle='modal' title='Cancelar acción' style="margin-top: 2px;display: none;">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;"></i>
                                          </a>
                                          <a class="btn btn-purple btnUpdateEdit btn-xs" data-toggle='modal' title='Modificar registro' style="display: none;">Modificar<i class="glyphicon glyphicon-pencil" style="margin-left: 5px;"></i>
                                          </a>
                                          <a class="btn btn-purple btnCancelEdit btn-xs" data-toggle='modal' title='Cancelar acción' style="margin-top: 2px;display: none;">Cancelar<i class="fa fa-times fa-lg" style="margin-left: 5px;"></i>
                                          </a>
                                        </td>
                                      </tr>
                                    </tfoot>
                                  </table>
                                
                                  <div>
                                    <button class="btn btn-yellow btnAdd" style="width: 100%" type="button"><span class="glyphicon glyphicon-plus"></span> AGREGAR</button>
                                  </div>
                                  <input type="hidden" id="file_name" name="file_name" value="" />
                                <!-- FIN DE NUEVO SUBIDA DE DEXCEL -->
                                </div>
                          </div><!-- /.box -->
                        </div>
                      </div>
                      </div>
                    </div>
                    
                    <div id="log" class="tab-pane fade">
                        <form name="form_movimiento" id="form_movimiento" method="post" action="reporte/movimiento" enctype="multipart/form-data">
                          <fieldset class="yellow-fieldset">           
                            <div class="row form-group" id="div_fecha">
                              <div class="col-sm-12">
                                <div class="col-sm-2">
                                  <label>Seleccione Rango de Fechas:</label>
                                </div>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control" placeholder="AAAA-MM-DD - AAAA-MM-DD" id="fecha" name="fecha" onfocus="blur()"/>
                                </div>                                   
                                <div class="col-sm-3"> 
                                  <button type="button" onclick="" id="mostrar" class="btn btn-primary">Mostrar</button>
                                </div>                                    
                              </div>
                            </div>
                          </fieldset>                                              
                        </form>
                        
                        <div class="row">
                          <div class="col-md-12">
                              <div class="box-body table-responsive">
                                <table id="tb_logcargamasiva" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>NOMBRE DEL ARCHIVO</th>
                                      <th>NUM. REGISTROS</th>
                                      <th>RESPONSABLE</th>
                                      <th>FECHA REGISTRO</th>                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                              </div>                            
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
      </div>     <!-- Finaliza contenido -->  
</section><!-- /.content -->
@endsection

@section('formulario')
     @include( 'admin.mantenimiento.form.tematico' )
@endsection
@push('scripts')
    {{ HTML::script("js/fontawesome-markers.min.js") }}
    {{ HTML::script("lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js") }}
    {{ HTML::script("lib/momentjs/2.9.0/moment.min.js") }}
    {{ HTML::script("lib/daterangepicker/js/daterangepicker.js") }}
    {{ HTML::script("lib/sweetalert/js/sweetalert.min.js") }}
    {{ HTML::script("lib/vue/vue.min.js") }}
    {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
    {{ HTML::script("lib/vue/vue-resource.min.js") }}
   
    @include( "admin.js.slct_global_ajax")
    @include( "admin.js.slct_global")
<script type="text/javascript" src="js/admin/mantenimiento/tematico_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/mantenimiento/tematico.js?v={{ Cache::get('js_version_number') }}"></script>
<script type="text/javascript" src="js/admin/legado/solicitud.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')