<!DOCTYPE html>
@extends('layouts.masterv2')  

@push("stylesheets")
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/box-slider/jquery.bxslider.min.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
    {{ HTML::style('css/sweetalert/sweetalert.css') }}
    {{ HTML::style('lib/fancybox-master/dist/jquery.fancybox.min.css') }}
@endpush
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')


<style type="text/css">
 .tbl-scroll{
        max-height: 450px;
        overflow: hidden;
    }
    fieldset{
        max-width: 100% !important;
        border: 1px solid #999;
        padding:5px 15px 8px 15px;
        border-radius: 10px; 
    }
    legend{
        font-size:14px;
        font-weight: 700;
        width: 12%;
        border-bottom: 0px;
        margin-bottom: 0px;
    }
    .slct_days{
      border-radius: 5px !important;
    }

    .bootstrap-tagsinput{
        background-color:#F5F5F5 !important;
        border-radius:7px !important;
        border: 1px solid;
        padding:5px;
      }

      .bootstrap-tagsinput .label-info{
        background-color: #337ab7 !important;
      }

      .bootstrap-tagsinput input{
        display: none;
      }

      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
    }
</style>
            <!-- Content Header (Page header) -->
<section class="content-header">
     <h1>
          Solicitud Técnica Recepción
          <small> </small>  
     </h1>
     <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
          <li><a href="#">Reporte</a></li>
          <li class="active">Reporte de Movimiento</li>
     </ol>
</section>

                <!-- Main content -->
<section class="content">

<div class="row">
      <div class="col-xs-11 filtros">
                  <form name="form_buscar" id="form_buscar" method="POST" action="">
                  <input type="hidden" name="txt_tipopersona_id" id="txt_tipopersona_id">
                    <div class="panel-group" id="acordion_filtros">
                    <div class="panel panel-default">
                        <div class="panel-heading box box-primary">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse2" id="panel_individual">Búsqueda por Fechas</a>
                             </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="personalizado">
                            <div class="box-body">
                             <div class="row">
                              <div class="form-group">
                                <div class="col-sm-12">
                                  <label>Fecha de Registro</label>
                                  <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                          <input class="form-control pull-right" id="rangoFechas" type="text" readonly/>
                                          <div class="input-group-addon" style="cursor: pointer" onClick="cleardate()">
                                          <i class="fa fa-rotate-left"></i>
                                      </div>
                                   </div>
                                </div>
                               

                                <!-- <div class="col-sm-1">
                                    <div class="form-group">
                                    <a class='btn btn-primary btn-sm' style="margin-top:25px" id="btn_buscar"><i class="fa fa-search fa-lg"></i>&nbsp;Buscar</a>
                                    </div>
                                </div>-->
                              </div>
                            </div>
                           </div>
                          </div>
                        </div>
                        </div>
                    </div>
                      <div class="panel panel-default">
                         <div class="panel-heading box box-primary">
                            <h4 class="panel-title">
                               <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion_filtros" href="#collapse1">Busqueda por Código Solicitud Técnica</a>
                           </h4>
                         </div>
                         <div id="collapse1" class="panel-collapse collapse in">
                          <div class="panel-body">
                           <div class="personalizado">
                            <div class="box-body">
                             <div class="row">
                               <div class="col-sm-6">
                                      <label>Código de solicitud técnica</label>                                    
                                      <input type="text" name="" id="solicitud_tecnica" placeholder="Ingrese Código de solicitud técnica" class="form form-control">
                                </div>                            
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
              </div>
              </form>
              </div>
    <div class="col-xs-1  botones">
      <i class="fa fa-search fa-xs btn btn-primary" title="Buscar" id="btn-buscar" onclick="filtrar()"></i>
      
    </div>
  </div>

    <br>
    <div class="row">
            <div class="col-md-12">
                  <div class="box-body table-responsive">
                      <table id="tb_solicitudtecnica" class="table table-bordered table-hover">
                              <thead>
                                 <tr>
                                    <th style="width: 10%">SOLICITUD TECNICA</th>
                                    <th style="width: 15%">FECHA CREACION</th>                                   
                                    <th style="width: 12%">ESTADO DE SOLICITUD</th>
                                    <th style="width: 10%">TIPO OPERACION</th>
                                    <th style="width: 20%">MENSAJE</th>
                                    <th style="width: 50%">DESC. MENSAJE</th>
                                    <th style="width: 10%">USUARIO CREACION</th>
                                 </tr>
                              </thead>
                            <tbody id="tbody_solicitudtecnica">
                            </tbody>
                     </table>
                  </div>                            
            </div>
    </div>

</section><!-- /.content -->
<!-- ******* -->

@stop



@push('scripts')
   {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
   {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
   {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
   {{ HTML::script('js/sweetalert/sweetalert.js') }}
    {{ HTML::script('lib/bootstrap-datepicker.js') }}
      {{ HTML::script('lib/bootstrap-datepicker.js') }}

      {{ HTML::script('lib/fancybox-master/dist/jquery.fancybox.min.js') }}

<!-- bxSlider Javascript file -->
   {{ HTML::script('lib/box-slider/jquery.bxslider.min.js') }}
<!-- bxSlider CSS file -->
        {{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
        {{ HTML::script("lib/validaCampos/validaCampos.js") }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )

  <script type="text/javascript" src="js/admin/mantenimiento/solicitudtecnica_ajax.js?v={{ Cache::get('js_version_number') }}"></script>
  <script type="text/javascript" src="js/admin/mantenimiento/solicitudtecnica.js?v={{ Cache::get('js_version_number') }}"></script>
@endpush('script')