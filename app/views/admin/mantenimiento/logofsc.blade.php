<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    
    {{ HTML::script('js/utils.js') }}

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
   
    @include( 'admin.js.tareas' )

    @include( 'admin.mantenimiento.js.logofsc_ajax' )
    @include( 'admin.mantenimiento.js.logofsc' )

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Log OFSC
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Mantenimiento</a></li>
                        <li class="active">Log OFSC</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box" style="min-height: 1300px;">
                                <div class="box-header">
                                    <h3 class="box-title">Filtros</h3>
                                </div>
                                <form name="form_logofsc" id="form_logofsc" method="POST" action="reporte/logexcel">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2">
                                        <label>ID:</label>
                                        <input type="text" class="form-control pull-left" name="txt_idlog" id="txt_idlog"/>
                                        </div>
                                        <div class="col-sm-2">
                                        <label>Tipo:</label>
                                        <select class="form-control" name="slct_tipo[]" id="slct_tipo" multiple>
                                            <option value="">.::Seleccione::.</option>
                                        </select>
                                        </div>
                                        <div class="col-sm-3">
                                             <label>Fechas:</label>
                                             <div class="input-group">
                                                             <div class="input-group-addon">
                                                                 <i class="fa fa-calendar"></i>
                                                             </div>
                                                             <input type="text" class="form-control pull-left" name="fecha" id="fecha" readonly="" />
                                                             <div class="input-group-addon" onclick="cleandate()" style="cursor: pointer">
                                                                 <i class="fa fa-rotate-left" ></i>
                                                             </div>
                                             </div>
                                         </div>
                                        <div class="col-sm-1">
                                           <label>&nbsp;</label><br>
                                           <button type="button" id="btn_buscar" name="btn_buscar" class="btn btn-danger">
                                               Buscar
                                           </button>
                                        </div>
                                        <!--<div class="col-sm-2">
                                            <label>&nbsp;</label><br>
                                            <button id="btn_exportar" class="btn btn-success" name="btn_exportar"
                                            type="button" onclick="descargarReporte();" > Exportar </button>
                                        </div>-->
                                    </div>
                                
                                </div>
                                
                                <div class="box-body" >
                                    
                                    <table id="t_log" class="table table-bordered table-striped col-sm-12 responsive" width="100%">
                                        <thead>
                                            <tr>
                                                <th style='width:55px !important;' >Id</th>
                                                <th style='width:70px !important;'>Proceso</th>
                                                <th style='width:90px !important;'>Tipo</th>
                                                <th style='width:150px !important;'>Body</th>
                                                <th style="width: 100px !important;">Fecha</th>
                                                <th style="width: 100px !important;"> 
                                                    <!--<a onclick="descargarReporte();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>-->[]
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_log">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>Proceso</th>
                                                <th>Tipo</th>
                                                <th>Body</th>
                                                <th>Fecha</th>
                                                <th style="width: 100px !important;"> []
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                                </form>
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop

@section('formulario')
     @include( 'admin.mantenimiento.form.logofsc' )
@stop