<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
{{ HTML::script('lib/backbone/js/json2.js') }}
{{ HTML::script('lib/backbone/js/underscore.min.js') }}
{{ HTML::script('lib/backbone/js/backbone.min.js') }}
{{ HTML::script('lib/backbone/js/handlebars.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@include( 'admin.mantenimiento.js.gestioncupos_ajax' )
@include( 'admin.mantenimiento.js.gestioncupos' )
@stop
        <!-- Right side column. Contains the navbar and content of the page -->
<style type="text/css">
    input:focus { 
        background-color: rgb(239, 255, 133);
    }
    .porcent {
        float: left;
        margin-right: 10px;
        width: 70px;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        color: #555;
        display: block;
        font-size: 14px;
        height: 34px;
        line-height: 1.42857;
        padding: 6px 12px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        border-radius: 0 !important;
        box-shadow: none;
    }
</style>
@section('contenido')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Gestión de Cupos
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Gestión de Cupos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Inicia contenido -->
                <div class="box" id="">
                    <div class="box-header">
                        <h3 class="box-title">Criterios</h3>
                    </div><!-- /.box-header -->
                    <div class="row">
            <div class="col-md-12">
                <!-- Inicia contenido 
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tb_tabla" id="presentation1" role="tab" data-toggle="tab">Nuevo Filtro</a>
                    </li>
                    <li role="presentation" >
                        <a href="#tb_camposTab" role="tab" data-toggle="tab">Listado</a>
                    </li>
                </ul> -->
    
                <div class="tab-content">
                    <form id="form_filtro">
                    <div role="tabpanel" class="tab-pane active" id="tb_camposTab">
                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label">Bucket:</label>
                                <select class="form-control" name="slct_bucket" id="slct_bucket"></select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Horario:</label>
                                <select class="form-control" name="slct_horario" id="slct_horario"></select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Tipo de Persona:</label>
                                <select class="form-control" name="slct_persona" id="slct_persona"></select>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label"></label>
                                <input type="button" class="form-control btn btn-primary" id="buscar" name="buscar" value="Buscar" style="margin-top:5px">
                            </div>
                        </div>
                        <div class="row form-group col-sm-12">
                            <div class="col-sm-4">
                                <label class="control-label">Cupos por Rango Horario (Referencial):</label>
                                 <input type="text" class="form-control" id="txt_capacidad" readonly="">
                                 <input type="hidden" id="txt_idfiltro"  name="txt_idfiltro">
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Otra Opcion: Adjuntar Archivo</label>
                                <input id="txt_file_plan" type="file" name="txt_file_plan">
                            </div>
                            <div class="col-sm-2">
                                <button id="btnGuardar" class="btn btn-primary" style="margin-top:25px" onclick="CargarArchivos()" type="button">Cargar Archivo</button>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label"></label>
                                <input type="button" class="form-control btn btn-danger" id="guardar" name="guardar" value="Guardar" style="margin-top:5px">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-body table-responsive">
                                <table id="t_horario" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="30px">Horario</th>
                                        <th>[]</th>
                                        <th>Lunes</th>
                                        <th>Martes</th>
                                        <th>Miercoles</th>
                                        <th>Jueves</th>
                                        <th>Viernes</th>
                                        <th>Sabado</th>
                                        <th>Domingo</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tb_horario">
                                   
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                        <div class="col-md-6" style="margin:30px 0">
                        (*) Los Cupos son porcentuales.
                        <a style="cursor:pointer" onclick="descargar()">Descargar Archivo de Ejemplo</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div><!-- /.col -->
        </div>
                </div><!-- /.box -->
                <!-- Finaliza contenido -->
            </div>
        </div>
        
    </section><!-- /.content -->
@stop
