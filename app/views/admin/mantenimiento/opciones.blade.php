<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
@parent

{{ HTML::script('js/admin/legado/pruebas.js') }}

{{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
{{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
@include( 'admin.js.slct_global_ajax' )
@include( 'admin.js.slct_global' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Mantenimiento de Opciones y Criterios
            <small> </small>
        </h1>
        <!--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Legado</a></li>
            <li class="active">Pruebas</li>
        </ol>-->
    </section>

    <!-- Main content -->
    <section class="content">
      <div>
        <ul id="myTab" class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active" role="tab" data-toggle="tab">
            <a href="#tb_opcion" id="opcion">Opcion</a>
          </li>
          <li role="presentation" role="tab" data-toggle="tab">
            <a href="#tb_criterio" id="criterio">Criterio</a>
          </li>
        </ul>
      </div>
      
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="tb_opcion">
          <div class="row">
            <div class="col-xs-12">
              <div class="box" >
                <br>
                <div class="row">
                  <form class="form-inline" style="margin-bottom: 100px; min-height: 200px" id="form_analizador">
                    <div class="col-md-12">
                      <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <div class="panel-group" id="accordion">
                          <div class="panel panel-info">
                            <!-- data-toggle="collapse" -->
                            <div class="panel-heading" data-parent="#accordion" href="#collapse_cliente_panel" style="cursor: pointer">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel">Lista de Opciones</a>
                              </h4>
                            </div>
                            
                            <div id="collapse_cliente_panel" class="panel-collapse collapse in" aria-expanded="true">
                              <div class="panel-body">
                                <div class="row" >
                                  <div class="col-md-6">
                                    <a href="#" id="btnNuevoOpc" class="btn btn-primary"><i class="fa fa-plus fa-lg"></i> Nuevo</a>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="box-body table-responsive">
                                      <form name="form_buscar" id="form_buscar" method="POST" action=""></form>
                                      <table id="tb_opciones" class="table table-bordered table-striped dataTable table-hover" style="background: white">
                                        <thead>
                                          <tr>
                                            <th style="width: 6%;text-align: center">Opcion</th>
                                            <th style="width: 4%;text-align: center">Tipo</th>
                                            <th style="width: 6%;text-align: center">Fecha de Creacion</th>
                                            <th style="width: 4%;text-align: center">Estado</th>
                                            <th style="width: 4%;text-align: center"></th>
                                          </tr>
                                        </thead>
                                        
                                        <tbody id="tbody_opciones" style="text-align: center;">
            
                                        </tbody>
                                      </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane fade in active" id="tb_criterio" style="display: none">
          <div class="box">
            <br>
              <div class="row">
                <form class="form-inline" style="margin-bottom: 100px; min-height: 200px" id="form_analizador">
                  <div class="col-md-12">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                      <div class="panel-group" id="accordion">
                        <div class="panel panel-info">
                          <!-- data-toggle="collapse" -->
                          <div class="panel-heading" data-parent="#accordion" href="#collapse_cliente_panel" style="cursor: pointer">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel">Lista de Criterios</a>
                            </h4>
                          </div>
                            
                          <div id="collapse_cliente_panel" class="panel-collapse collapse in" aria-expanded="true">
                            <div class="panel-body">
                              <div class="row" >
                                <div class="col-sm-2">
                                  <a href="#" id="btnNuevoCrit" class="btn btn-primary"><i class="fa fa-plus fa-lg"></i> Nuevo</a>
                                </div>
                              </div>
                              <div class="row">
                                <div class="box-body table-responsive"> 
                                  <form name="form_buscar2" id="form_buscar2" method="POST" action=""></form>
                                    <table id="tb_ctr" class="table table-bordered table-striped dataTable table-hover" style="background: white">
                                      <thead>
                                        <tr>
                                            <th style="width: 6%;text-align: center">Criterio</th>
                                            <th style="width: 4%;text-align: center">Campo</th>
                                            <th style="width: 6%;text-align: center">Fecha de Creacion</th>
                                            <th style="width: 2%;text-align: center">Estado</th>
                                            <th style="width: 2%;text-align: center"></th>
                                         </tr>
                                      </thead>
                                        
                                      <tbody id="tbody_opciones" style="text-align: center;">
            
                                      </tbody>
                                    </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
          </div>
        </div>

      </div>
    </section>

<!--Modal Opciones-->
<div class="modal fade" id="ModalOpcion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header logo">
              <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                <i class="fa fa-close"></i>
              </button>
              <h4 class="modal-title">MANTENIMIENTO DE OPCION</h4>
          </div>
          <div class="modal-body">
            <form id="form_opciones" name="form_opciones" action="" method="post">
              <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token');?>" />
              <input type="hidden" name="opcionId" id="opcionId">  
                <div class="form-group">
                  <label class="control-label">Nombre :
                    <a id="error_nombreOpc" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                      <i class="fa fa-exclamation"></i>
                    </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombreOpc" id="txt_nombreOpc">
                </div>
                <div class="form-group">
                  <label class="control-label">Tipo:
                    <a id="error_tipoOpc" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Seleccione Tipo">
                      <i class="fa fa-exclamation"></i>
                    </a>
                  </label>
                    <select class="form-control" name="slct_tipoOpc" id="slct_tipoOpc">
                    </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btnGuardarOpc"><i class="fa fa-save fa-lg"></i> Guardar</button>
            <button type="button" class="btn btn-success" id="btnModificarOpc" style="display: none;"><i class="fa fa-location-arrow fa-lg"></i> Modificar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelar"><i class="fa fa-times fa-lg"></i> Cancelar</button>
          </div>
      </div>
    </div>
</div>

<!--Modal Criterios para opcion-->
<div class="modal fade" id="ModalCritetio" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header logo">
              <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                <i class="fa fa-close"></i>
              </button>
              <h4 class="modal-title">MANTENIMIENTO DE CRITERIO</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-info col-md-12">
                  <form id="form_criterios" name="form_criterios" action="" method="post">
                    <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token');?>" />
                    <input type="hidden" name="opcionId_Crt" id="opcionId_Crt">
                    <input type="hidden" name="criterioId_Crt" id="criterioId_Crt"> 
                    <input type="hidden" name="Id_Crt_Opc" id="Id_Crt_Opc">
                      <div class="col-md-12"><b><span class="display-4 text-primary" id="opcionCrt"></span></b></div>
                      <div class="col-md-12" style="margin-top: 10px;">
                        <div class="form-group col-md-6">
                          <label class="control-label">Criterio:
                            <a id="error_criterioCrt" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Seleccione Criterio">
                              <i class="fa fa-exclamation"></i>
                            </a>
                          </label>
                          <select class="form-control" name="slct_criterioCrt" id="slct_criterioCrt">
                          </select>
                        </div>
                        <div class="form-group col-md-6">
                          <label class="control-label">Campo:
                            <a id="error_campoCrt" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Campo">
                              <i class="fa fa-exclamation"></i>
                            </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese Campo" name="txt_campoCrt" id="txt_campoCrt" readonly>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group col-md-6">
                          <label class="control-label">Valor:
                            <a id="error_valorCrt" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Valor">
                              <i class="fa fa-exclamation"></i>
                            </a>
                          </label>
                          <input type="text" class="form-control" placeholder="Ingrese Valor" name="txt_valorCrt" id="txt_valorCrt" onread>
                        </div>
                        <!--<div class="form-group col-md-12">
                          <label class="control-label">Estado:
                            <a id="error_estadoCrt" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Seleccione Estado">
                              <i class="fa fa-exclamation"></i>
                            </a>
                          </label>
                          <select class="form-control" name="slct_estadoCrt" id="slct_estadoCrt">
                            <option value="-1">Seleccione...</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>                 
                          </select>
                        </div>-->
                      </div>
                      <div class="col-md-12">
                        <div class="form-group col-md-12">
                          <button type="button" class="btn btn-success" id="btnGuardarCrt"><i class="fa fa-save fa-lg"></i> Guardar</button>
                          <button type="button" class="btn btn-success" id="btnModificarCrt" style="display: none;"><i class="fa fa-location-arrow fa-lg"></i> Modificar</button>
                          <button type="button" class="btn btn-danger" id="btnCancelarCrt"><i class="fa fa-times fa-lg"></i> Cancelar</button>
                        </div>
                      </div>
                  </form> 
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-info">
                  <div class="panel-heading" data-parent="#accordion" href="#collapse_cliente_panel" style="cursor: pointer">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse_cliente_panel">Lista de Criterios</a>
                    </h4>
                  </div>
                  <div class="panel-body">
                    <div class="box-body table-responsive">
                      <form name="form_buscar2" id="form_buscar2" method="POST" action=""></form>
                      <table id="tb_criterios" class="table table-bordered table-striped dataTable table-hover" style="background: white">
                        <thead>
                          <tr>
                            <th style="width: 6%;text-align: center">Criterio</th>
                            <th style="width: 6%;text-align: center">Campo</th>
                            <th style="width: 6%;text-align: center">Valor</th>
                            <th style="width: 2%;text-align: center">   </th>
                          </tr>
                        </thead>
                        <tbody style="text-align: center;">
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div> 
              </div> 
            </div>           
          </div>
      </div>
    </div>
</div>

<!--Modal Criterios-->
<div class="modal fade" id="ModalCrit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header logo">
              <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                <i class="fa fa-close"></i>
              </button>
              <h4 class="modal-title">MANTENIMIENTO DE CRITERIO</h4>
          </div>
          <div class="modal-body">
            <form id="form_crit" name="form_crit" action="" method="post">
              <input type="hidden" name="txt_token" id="txt_token" value="<?php echo Session::get('s_token');?>" />
              <input type="hidden" name="criterioId" id="criterioId">  
                <div class="form-group">
                  <label class="control-label">Nombre :
                    <a id="error_nombreCrit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Nombre">
                      <i class="fa fa-exclamation"></i>
                    </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese Nombre" name="txt_nombreCrit" id="txt_nombreCrit">
                </div>
                <div class="form-group">
                  <label class="control-label">Campo:
                    <a id="error_campoCrit" style="display:none" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ingrese Campo">
                      <i class="fa fa-exclamation"></i>
                    </a>
                  </label>
                  <input type="text" class="form-control" placeholder="Ingrese Campo" name="txt_campoCrit" id="txt_campoCrit">
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btnGuardarCrit"><i class="fa fa-save fa-lg"></i> Guardar</button>
            <button type="button" class="btn btn-success" id="btnModificarCrit" style="display: none;"><i class="fa fa-location-arrow fa-lg"></i> Modificar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-lg"></i> Cancelar</button>
          </div>
      </div>
    </div>
</div>

{{ HTML::script("lib/vue/vue.min.js") }}
{{ HTML::script("lib/vue/axios-0.16.2.min.js") }}
{{ HTML::script("lib/vue/vue-resource.min.js") }}

{{ HTML::script('js/admin/mantenimiento/opciones.js') }}
{{ HTML::script('js/admin/mantenimiento/opciones_ajax.js') }}
@stop


