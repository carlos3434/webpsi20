<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}

    <style type="text/css">
      .btn-yellow{
        color: #0070ba;
        background-color: ghostwhite;
        border-color: #ccc;
        font-weight: bold;
      }

      td{
        padding: 1%;
      }
    </style>

    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
@stop
@section('contenido')
    <section class="content-header">
        <h1>
           Parametro Motivo 
           <a class='btn btn-success btn-sm' data-toggle="modal" data-target="#parametroModal"><i class="fa fa-plus fa-lg"></i>&nbsp;Nuevo</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimiento</a></li>
            <li class="active">Contrata Transferencia</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4">
                        <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                            <b-form-select class="form-control" 
                                :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                v-model="perPage">
                            </b-form-select>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-4 pull-right">
                        <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                            <b-form-input class="form-control" v-model="filter" placeholder="Ingresar texto"></b-form-input>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <div class="table-responsive">
                            <b-table striped hover class="text-center"
                               :items="datos"
                               :fields="fields"
                               :current-page="currentPage"
                               :per-page="perPage"
                               :filter.sync="filter">


                                <template slot="index" scope="item">
                                    @{{ item.index + 1 }}
                                </template>
                                <template slot="nombre" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="tipo_legado" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="bucket_nombre" scope="item">
                                    @{{item.value}}
                                </template>
                                <template slot="estado" scope="item">
                                    <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                        @click="setEstado(item.item)">&nbsp;Activo&nbsp;
                                    </label>
                                    <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                        @click="setEstado(item.item)">Inactivo
                                    </label>
                                </template>
                                <template slot="actions" scope="item">
                                    <a class='btn btn-primary btn-sm' 
                                        data-toggle="modal" data-target="#parametroModal" 
                                        @click="find(item.item)">
                                        <i class="fa fa-eye fa-sm"></i>
                                    </a>
                                </template>
                            </b-table>
                        </div>
                    </div>                   
                    <div class="col-sm-6 pull-right">
                        <b-pagination :total-rows.sync="datos.length" :per-page.sync="perPage" v-model.sync="currentPage" />
                    </div>
                </div>
            </div>
        </div>

        <!-- /.modal -->
        <div class="modal fade" id="parametroModal" tabindex="-1" role="dialog" aria-hidden="true">
           <div class="modal-dialog modal-lg">
              <div class="modal-content">
                 <div class="modal-header logo">
                    <button class="btn btn-sm btn-default pull-right" data-dismiss="modal">
                       <i class="fa fa-close"></i>
                    </button>
                    <h4 class="modal-title">&nbsp;</h4>
                 </div>
                 <div class="modal-body">
                    <form id="form_parametros" name="form_parametros">
                       <div class="row form-group">
                          <div class="col-md-12">      
                             <fieldset class="fieldsetblase">
                                <legend class="legendblade"><i>Parametro Motivo</i></legend>
                                 <div class="col-sm-6">
                                    <label>Nombre Parametro</label>
                                    <input class="form-control" type='text' v-model="objParametroMotivo.nombre">
                                 </div>
                                 <div class="col-md-6">
                                    <label>Sistema</label>
                                    <select class="form-control" v-model="objParametroMotivo.tipo_legado">
                                      <option value="">.::Seleccione::.</option>
                                      <option value="1">CMS</option>
                                      <option value="2">GESTEL</option>
                                    </select>
                                 </div>
                                 <div class="col-md-6">
                                    <label>Bucket</label>
                                    <select class="form-control slct_bucket"></select>
                                 </div>
                                 <!-- <div class="col-md-6">
                                    <label>Estados OFSC</label>
                                    <select class="form-control slct_estado_ofsc" v-model="objParametroMotivo.tipo_legado_id"></select>
                                 </div>-->
                             </fieldset>
                             <br>
                          </div>
                          <div class="col-md-12">
                              <table class="tbDetalle table-responsive table-bordered" id="tbDetalle">
                                  <b-table bordered hover class="text-center"
                                     :items="datosDetalle"
                                     :fields="fieldsDetalle"
                                     :current-page="currentPageDetalle"
                                     :per-page="perPageDetalle"
                                     :filter.sync="filterDetalle">
                                      <template slot="nombre_motivo_ofsc" scope="item">
                                          @{{item.value}}
                                      </template>
                                      <template slot="nombre_submotivo_ofsc" scope="item">
                                          @{{item.value}}
                                      </template>
                                      <template slot="contador" scope="item">
                                          @{{item.value}}
                                      </template>
                                      <template slot="accion_contador" scope="item">
                                          @{{item.value}}
                                      </template>
                                      <template slot="toolbox" scope="item">
                                          @{{item.value}}
                                      </template>
                                      <template slot="estado" scope="item">
                                          <label v-if="item.value == 1" class="btn btn-success btn-sm" 
                                              @click="setEstadoDetalle(item.item)">&nbsp;Activo&nbsp;
                                          </label>
                                          <label v-if="item.value == 0" class="btn btn-danger btn-sm" 
                                              @click="setEstadoDetalle(item.item)">Inactivo
                                          </label>
                                      </template>
                                      <!--<template slot="actions" scope="item">
                                          <a class='btn btn-primary btn-sm' 
                                              data-toggle="modal" data-target="#parametroModal" 
                                              @click="find(item.item)">
                                              <i class="fa fa-eye fa-sm"></i>
                                          </a>
                                      </template>-->
                                  </b-table>
                                  <div class="nuevosDetalles">
                                    <tr class="trAdd hidden">
                                      <td style="width: 24%" id="tdmotivo">
                                        <select class="slct_motivo_ofsc form-control" name="slct_motivo_ofsc" id="slct_motivo_ofsc">
                                          <option value="">.::Seleccione::.</option>
                                        </select>
                                      </td>
                                      <td style="width: 24%" id="tdsubmotivo">
                                        <select class="slct_submotivo_ofsc form-control" name="slct_submotivo_ofsc" id="slct_submotivo_ofsc">
                                        </select>
                                      </td>
                                      <td style="width: 14%" id="tdcontador">
                                        <input class="form-control" type="text" id="txt_contador" name="txt_contador" placeholder="Contador">
                                      </td>
                                      <td style="width: 14%" id="tdaccioncont">
                                        <input class="form-control" type="text" id="txt_accionContador" name="txt_accionContador" placeholder="Accion Contador">
                                      </td>
                                      <td style="width: 14%" id="tdtoolbox">
                                        <input class="form-control" type="text" id="txt_toolbox" name="txt_toolbox" placeholder="Toolbox">
                                      </td>
                                      <td style="width: 6%">
                                        <button type="button" class="removeTr btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                                      </td>
                                    </tr>                                    
                                  </div>
                              </table>
                               <button id="btnAdd" class="btn btn-yellow" style="width: 100%" type="button" @click="clone()"><span class="glyphicon glyphicon-plus"></span> AGREGAR </button>
                          </div>
                       </div>
                    </form>
                 </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="update()">Guardar</button>
                 </div>
              </div>
           </div>
        </div>

    </section>

    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}
    
    <script src="https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
    <script src="https://unpkg.com/tether@latest/dist/js/tether.min.js"></script>
    {{ HTML::script('js/bootstrap-vue/bootstrap-vue.js') }}
    {{ HTML::script('js/admin/mantenimiento/parametromotivo.js') }}

    <script type="text/javascript">
        $(document).ready(function() {
            
            slctGlobal.listarSlctpost('bucket','listar','form_parametros .slct_bucket','simple',null,null,0,1);
            slctGlobal.listarSlctpost('estadoofsc','listar','form_parametros .slct_estado_ofsc','simple',null,null,0,1);
          

            $(".slct_bucket").change(function(e) {
                vm.objParametroMotivo.bucket_id = this.value;
            });

            $(".slct_estado_ofsc").change(function(e) {
                vm.objParametroMotivo.estado_ofsc_id = this.value;
            });

            $(document).on('click', '.removeTr', function(event) {
              let tr = $(this).parent().parent();              
              var r = confirm("¿Esta seguro de eliminar?");
              if (r == true) {
                $(tr).remove();
              }
            });

        });
    </script>
@stop