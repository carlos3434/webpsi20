<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@include( 'admin.mantenimiento.js.errores_ajax' )
@include( 'admin.mantenimiento.js.errores' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('mantenimiento.mantenimiento_error')}}
        <small> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{trans('mantenimiento.admin')}}</a></li>
        <li><a href="#">{{trans('mantenimiento.mantenimientos')}}</a></li>
        <li class="active">{{trans('mantenimiento.mantenimiento_error')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('mantenimiento.filtro')}}</h3>
                </div>
                <div class="panel-body">
                    <form id="form_error" name="form_error" role="form" class="form-inline" method="POST">
                        <div class="form-group">
                            <label for="txt_fecha">{{trans('mantenimiento.rango_fecha')}}</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                <input class="form-control" id="txt_rangofecha" name="txt_rangofecha" type="text"  aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <button type="submit" id="btn_filter" class="btn btn-primary">{{trans('mantenimiento.mostrar')}}</button>
                    </form>
                </div>
            </div>     
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <!-- Inicia contenido -->
            <table id="datatable_error" class="table table-bordered table-striped table-condensed display responsive no-wrap" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">{{trans('mantenimiento.id')}}</th>
                        <th class="text-center">{{trans('mantenimiento.usuario')}}</th>
                        <th class="text-center">{{trans('mantenimiento.url')}}</th>
                        <th class="text-center">{{trans('mantenimiento.archivo')}}</th>
                        <th class="text-center">{{trans('mantenimiento.codigo')}}</th>
                        <th class="text-center">{{trans('mantenimiento.fecha')}}</th>
                        <th class="text-center">{{trans('mantenimiento.comentario')}}</th>
                        <th class="text-center">{{trans('mantenimiento.mensaje')}}</th>
                        <th class="text-center">{{trans('mantenimiento.rastro')}}</th>
                        <th class="text-center editarG">{{trans('mantenimiento.estado')}}</th>
                        <th class="text-center">{{trans('mantenimiento.table_accion')}}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">{{trans('mantenimiento.id')}}</th>
                        <th class="text-center">{{trans('mantenimiento.usuario')}}</th>
                        <th class="text-center">{{trans('mantenimiento.url')}}</th>
                        <th class="text-center">{{trans('mantenimiento.archivo')}}</th>
                        <th class="text-center">{{trans('mantenimiento.codigo')}}</th>
                        <th class="text-center">{{trans('mantenimiento.fecha')}}</th>
                        <th class="text-center">{{trans('mantenimiento.comentario')}}</th>
                        <th class="text-center">{{trans('mantenimiento.mensaje')}}</th>
                        <th class="text-center">{{trans('mantenimiento.rastro')}}</th>
                        <th class="text-center editarG">{{trans('mantenimiento.estado')}}</th>
                        <th class="text-center">{{trans('mantenimiento.table_accion')}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('formulario')
@include( 'admin.mantenimiento.form.error' )
@include( 'admin.mantenimiento.form.errorComentario' )
@stop
