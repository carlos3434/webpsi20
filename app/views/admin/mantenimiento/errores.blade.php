<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
@parent

{{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
{{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
{{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}

@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
    <section class="content-header">
        <h1>
            Mantenimiento de Errores
            <small> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li><a href="#">Mantenimientos</a></li>
            <li class="active">Mantenimiento de Errores</li>
        </ol>
    </section>

    <section class="content" id="app">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4">
                        <b-form-fieldset horizontal label="Mostrar" :label-cols="4">
                            <b-form-select class="form-control" 
                                @change.native="listar"
                                :options="[{text:5,value:5},{text:10,value:10},{text:15,value:15}]" 
                                v-model="perPage">
                            </b-form-select>
                        </b-form-fieldset>
                    </div>

                    <div class="col-sm-4 pull-right">
                        <b-form-fieldset horizontal label="Filtro" :label-cols="4">
                             <select class="form-control" v-model="filterSelected">
                                <option v-for="head in heads" :value="head.filterHead">
                                    @{{ head.nameHead }}
                                </option>
                            </select>

                            <b-tooltip content="Presionar ENTER para buscar">
                                <b-form-input class="form-control"  placeholder="Ingresar texto" 
                                              v-model="filter"
                                              @keyup.enter.native="listar">
                                </b-form-input>
                            </b-tooltip>
                        </b-form-fieldset>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead style="background-color: #3c8dbc!important; color: white!important;">
                                    <th class="text-center" v-for="head in heads">@{{ head.nameHead }}</th>
                                    <th class="text-center">[ . ]</th>
                                </thead>
                                <tbody>
                                    <tr v-for="dato in datos">
                                        <td>@{{ dato.usuario }}</td>
                                        <td>@{{ dato.url }}</td>
                                        <td>@{{ dato.file }}</td>
                                        <td>@{{ dato.code }}</td>
                                        <td>@{{ dato.date }}</td>
                                        <td>
                                            <a class="btn menu-icon  btn-primary btn-sm mgr-10" data-toggle="modal" data-target="#modal" @click="detalle(dato)"><i class="fa fa-eye fa-fw"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right">
                        <b-pagination :total-rows.sync="totalRows" :per-page.sync="perPage" v-model="currentPage" @click.native="listar"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Detalle</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Mensaje</label>
                            <textarea class="form-control resize-vertical" v-model.sync="object.message" rows="5" disabled></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Rastro</label>
                            <textarea class="form-control resize-vertical" v-model.sync="object.trace" rows="15" disabled></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{ HTML::script('https://unpkg.com/axios@0.16.1/dist/axios.min.js') }}
    {{ HTML::script('https://unpkg.com/vue@2.3.3') }}

    {{ HTML::script('https://unpkg.com/babel-polyfill@latest/dist/polyfill.min.js') }}
    {{ HTML::script('https://unpkg.com/tether@latest/dist/js/tether.min.js') }}
    {{ HTML::script('https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js') }}
    {{ HTML::script('js/admin/mantenimiento/errores.js') }}
@stop