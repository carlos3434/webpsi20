<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

        @section('autor')
        <meta name="author" content="Telefonica">
        @show

        <link rel="shortcut icon" href="favicon.ico">

        @section('descripcion')
        <meta name="description" content="">
        @show
        <title>
            @section('titulo')
                PSI 2.0
            @show
        </title>
        <meta name="token" id="token" value="{{ csrf_token() }}">
        @section('includes')
            {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
            {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}

            {{ HTML::script('lib/jquery-2.1.3.min.js') }}
            {{ HTML::script('lib/jquery-ui-1.11.2/jquery-ui.min.js') }}
            {{ HTML::script('lib/bootstrap-3.3.1/js/bootstrap.min.js') }}
            <!-- //code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css -->
            {{ HTML::style('css/master/ionicons.min.css') }}
            {{ HTML::style('lib/datatables-1.10.4/media/css/dataTables.bootstrap.css') }}
            {{ HTML::style('css/admin/admin.min.css') }}
            {{ HTML::style('css/admin/horarios.css') }}

            {{ HTML::script('lib/datatables-1.10.4/media/js/jquery.dataTables.js') }}
            {{ HTML::script('lib/datatables-1.10.4/media/js/dataTables.bootstrap.js') }}
            {{ HTML::script('js/psi.js', array('async' => 'async')) }}
            @include( 'admin.js.app' )
        @show
    </head>

    <body class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
        @include( 'layouts.admin_head' )

        <div class="wrapper row-offcanvas row-offcanvas-left">
            @include( 'layouts.admin_left' )

            <aside class="right-side">
            @yield('contenido')
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->
       @yield('formulario')
    </body>
    <script>
    // disable datatables error prompt
    $.fn.dataTable.ext.errMode = 'none';
    $(document).ajaxError(function(event, jqxhr, settings, exception) {
        if (exception == 'Unauthorized') {
            alert("La sesión ha expirado. Clic en Aceptar para ir a la pagina de Login.");
            window.location = '<?php echo url();?>';
        }
    });
        GLOBAL = {};
        var envioValida=0;
        var agregarG='';
        var editarG='';
        var eliminarG='';

        $('ul.sidebar-menu li').each(function(indice, elemento) {
             htm=$(elemento).html();
             //console.log(htm);
            if(htm.split('<a href="{{ $valida_ruta_url }}"').length>1){
                if( "{{ $valida_ruta_url }}" == 'admin.mantenimiento.misdatos') return;
                $(elemento).addClass('active');
                poshtm=$(elemento).find('a[href="{{ $valida_ruta_url }}"]');
                //console.log(accesosG);
                var accesosG=poshtm.attr('data-accesos');
                var hash=poshtm.attr('data-clave');
                agregarG=accesosG.substr(0,1);
                editarG=accesosG.substr(1,1);
                eliminarG=accesosG.substr(2,1);

                var datos={agregarG:agregarG,editarG:editarG,eliminarG:eliminarG,hash:hash};
                if(envioValida==0){
                    var ajax = functionajax('usuario/validaacceso', 'POST', false, 'json', false, datos);
                    ajax.success(function(obj){
                        envioValida++;
                        if(obj.rst!='1'){
                            Psi.mensaje('danger', 'Ud no cuenta con permisos para acceder. Ingrese nuevamente', 6000);
                            setTimeout(function(){window.location='salir';}, 5000);
                        }
                    });
                }
            }
        });
    </script>

</html>