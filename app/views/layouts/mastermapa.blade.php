<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta name="token" id="token" value="{{ csrf_token() }}">
        @section('autor')
        <meta name="author" content="Telefonica">
        @show

        <link rel="shortcut icon" href="favicon.ico">

        @section('descripcion')
        <meta name="description" content="">
        @show
        <title>
            @section('titulo')
                PSI 2.0
            @show
        </title>

        @section('includes')
            {{ HTML::style('lib/bootstrap-3.3.1/css/bootstrap.min.css') }}
            {{ HTML::style('lib/font-awesome-4.2.0/css/font-awesome.min.css') }}
            {{ HTML::style('css/admin/admin.min.css') }}
        @show
    </head>

    <body class="skin-blue">
    <div id="msj" class="msjAlert"> </div>
        @include( 'layouts.admin_head' )

        <div class="wrapper row-offcanvas row-offcanvas-left">
            @include( 'layouts.admin_left' )

            <aside class="right-side">
            @yield('contenido')
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->
       @yield('formulario')
    </body>
    @section('javascript')
        {{ HTML::script('lib/jquery-2.1.3.min.js') }}
        {{ HTML::script('https://code.jquery.com/ui/1.11.2/jquery-ui.min.js')}}
        {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js')}}
        {{ HTML::script('js/psi.js', array('async' => 'async')) }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.2/vue-resource.min.js') }}
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js') }}
        @include( 'admin.js.app' )
    @show
    <script>
        GLOBAL = {};
        var envioValida=0;
        var agregarG='';
        var editarG='';
        var eliminarG='';

        $('ul.sidebar-menu li').each(function(indice, elemento) {
             htm=$(elemento).html();
             //console.log(htm);
            if(htm.split('<a href="{{ $valida_ruta_url }}"').length>1){
                if( "{{ $valida_ruta_url }}" == 'admin.mantenimiento.misdatos') return;
                $(elemento).addClass('active');
                poshtm=$(elemento).find('a[href="{{ $valida_ruta_url }}"]');
                //console.log(accesosG);
                var accesosG=poshtm.attr('data-accesos');
                var hash=poshtm.attr('data-clave');
                agregarG=accesosG.substr(0,1);
                editarG=accesosG.substr(1,1);
                eliminarG=accesosG.substr(2,1);

                var datos={agregarG:agregarG,editarG:editarG,eliminarG:eliminarG,hash:hash};
                if(envioValida==0){
                    var ajax = functionajax('usuario/validaacceso', 'POST', false, 'json', false, datos);
                    ajax.success(function(obj){
                        envioValida++;
                        if(obj.rst!='1'){
                            Psi.mensaje('danger', 'Ud no cuenta con permisos para acceder. Ingrese nuevamente', 6000);
                            setTimeout(function(){window.location='salir';}, 5000);
                        }
                    });
                }
            }
        });
    </script>
</html>