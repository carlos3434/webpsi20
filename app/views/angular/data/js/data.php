<script>
var app= angular.module('angularSmall',['ui.router']);

app.config( function( $stateProvider, $urlRouterProvider ){
    //For any unmatched url, redirect to "/" 
    $urlRouterProvider.otherwise("/");

    //Now set up the states
    $stateProvider
        .state( 'user-index', {
            url: "/",
            templateUrl: "../laravel/app/views/angular/data/form/data.html",
            controller: 'UserIndexCtrl'
        } )
        .state( 'user-show', {
            url: "/users/:user_id",
            templateUrl: "../laravel/app/views/angular/data/form/data-ajax.html",
            controller: 'UserShowCtrl'
        } );
});

app.controller('UserIndexCtrl', function($scope, User){
    $scope.users =  User.all();
    //$scope.users=[];
    $scope.addUser = function(){
        $scope.newUser.id= $scope.users.length+1;
        $scope.users.push( $scope.newUser );
        console.log($scope.newUser);
        $scope.newUser={};
    };
});

app.controller('UserShowCtrl', function($stateParams, $scope, User, $http){
    //$scope.user = { id:1, name:'Betsy', age:73 };
    $scope.user = User.get( $stateParams.user_id );

    var query='http://api.giphy.com/v1/gifs/search?q='+ $scope.user.name +'&api_key=dc6zaTOxFJmzC';
    $http.get(query).then(
        function(data){
            $scope.gifs= data.data.data;
            console.log(data);
        },
        function(error){
            console.log(error);
        }
    );
});

app.controller('MainCtrl', function($scope){
    $scope.message= "World!";
});

app.factory('User', function() {
    var users = [
        { id:1, name:'Betsy', age:73 },
        { id:2, name:'Norman', age:40 },
        { id:3, name:'Sonja', age:6 },
    ];

    return {
        all: function(){
            return users;
        },
        remove: function(chat){
            users.splice( users.indexOf(chat), 1 );
        },
        get: function(userId){
            for( var i=0; i<users.length; i++ ){
                if( users[i].id=== parseInt(userId) ){
                    return users[i];
                }
            }
            return null;
        }
    };

})
</script>
