<script>
var app= angular.module('angularSmall',['ui.router','ngResource']);

app.config( function( $stateProvider, $urlRouterProvider ){
    //For any unmatched url, redirect to "/" 
    $urlRouterProvider.otherwise("/");

    //Now set up the states
    $stateProvider
        .state( 'user-index', {
            url: "/",
            templateUrl: "../laravel/app/views/angular/data/form/resource.html",
            controller: 'UserIndexCtrl'
        } )
        .state( 'user-show', {
            url: "/users/:user_id",
            templateUrl: "../laravel/app/views/angular/data/form/data-ajax.html",
            controller: 'UserShowCtrl'
        } );
});

app.controller('UserIndexCtrl', function($scope, User){
    $scope.users =  User.getUser().query();
    console.log($scope.users);
    
    $scope.addUser = function(){
        User.getUser().save( $scope.newUser, function(data) {
            $scope.users.push( data );
            console.log(data);
            $scope.newUser={};
        });
    };

    $scope.removeUser = function(user){
        User.getUser().remove( {id:user.id}, function(data) {
            var index = $scope.users.indexOf(user);
            $scope.users.splice(index, 1);
        });
    };
});

app.controller('UserShowCtrl', function($stateParams, $scope, User){
    $scope.user=[];

    User.getUser().get( {id:$stateParams.user_id}, function(data) {
        $scope.user= data;
        console.log($scope.user);
        ViewGifs($scope.user);
    });

    function ViewGifs(user){
        User.getImagen().get( {name:user.name.split(" ")[0]}, function(data) {
            $scope.gifs= data.data;
            console.log($scope.gifs);
        });
    }
});

app.controller('MainCtrl', function($scope){
    $scope.message= "World!";
});

app.factory('User', function( $resource ) {

    var HOST='';
    return {
            getUser : function() {
                HOST='http://jsonplaceholder.typicode.com/';
                return $resource(HOST +'users/:id',{ id: '@id' });
            },
            getImagen: function () {
                HOST='http://api.giphy.com/v1/gifs/search?q=:name&api_key=dc6zaTOxFJmzC';
                return $resource(HOST,{name: '@name'});
            }
    };
})
</script>
