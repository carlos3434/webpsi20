<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::script('lib/angular/angular.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js') }}
    {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.0-rc.1/angular-resource.min.js') }}
    

    @include( 'angular.controller.css.app' )
    @include( 'angular.data.js.resource' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Controllers Two
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li class="active">Controllers</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content" ng-app="angularSmall">
                    <div class="row">
                        <div class="col-xs-12" ng-controller="MainCtrl">
                            <!-- Inicia contenido -->
                            <ng-include src="'../laravel/app/views/angular/data/form/navbar.html'"></ng-include>
                            <ui-view></ui-view>
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop
