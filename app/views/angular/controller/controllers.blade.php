<!DOCTYPE html>
@extends('layouts.master')  

@section('includes')
    @parent
    {{ HTML::script('lib/angular/angular.min.js') }}

    @include( 'angular.controller.css.app' )
    @include( 'angular.controller.js.app' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Controllers
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li class="active">Controllers</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content" ng-app="angularSmall">
                    <div class="row">
                        <div class="col-xs-12" ng-controller="MainCtrl">
                            <!-- Inicia contenido -->
                            <h1>Hello @{{message}}</h1>
                            <input type="text" ng-model="message">
                            
                            <br><br>
                            
                            <ul>
                                <li ng-repeat="name in names" 
                                    ng-hide="name.length > 5">
                                @{{name}}
                                </li>
                            </ul>

                            <form ng-submit="addPerson()">
                                <div class="row form-group">
                                <h1>People</h1>
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label>Name:</label>
                                            <input type="text" class="form-control" ng-model="newPerson.name">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-3">
                                            <label>Age:</label>
                                            <input type="text" class="form-control" ng-model="newPerson.age">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            <i class="fa fa-plus fa-lg"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @{{newPerson}}

                            <h2>Adults</h2>
                            <ul>
                                <li ng-repeat="person in people"
                                    ng-show="person.age > 17">
                                @{{person.name}} is @{{person.age}} years old.
                                </li>
                            </ul>

                            <h2>Kids</h2>
                            <ul>
                                <li ng-repeat="person in people"
                                    ng-hide="person.age > 17">
                                @{{person.name}} is @{{person.age}} years old.
                                </li>
                            </ul>
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop
