<script>
var app= angular.module('angularSmall',[]);

app.controller('MainCtrl', function($scope){
    $scope.message= "World!";

    $scope.names = [ 'Betsy' , 'Norman' , 'Sonja', 'Jhosua'];

    $scope.people = [
        { name:'Betsy', age:73 },
        { name:'Norman', age:40 },
        { name:'Sonja', age:6 },
    ];

    $scope.addPerson = function(){
        $scope.people.push( $scope.newPerson );
        $scope.newPerson={};
    };
});
</script>
