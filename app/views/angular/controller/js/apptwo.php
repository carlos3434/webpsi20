<script>
var app= angular.module('angularSmall',['ui.router']);

app.config( function( $stateProvider, $urlRouterProvider ){
    //For any unmatched url, redirect to "/" 
    $urlRouterProvider.otherwise("/");

    //Now set up the states
    $stateProvider
        .state( 'user-index', {
            url: "/",
            templateUrl: "../laravel/app/views/angular/controller/form/apptwo.html",
            controller: 'UserIndexCtrl'
        } )
        .state( 'user-show', {
            url: "/users/:user_id",
            templateUrl: "../laravel/app/views/angular/controller/form/apptwoshow.html",
            controller: 'UserShowCtrl'
        } );
});

app.controller('UserIndexCtrl', function($scope){
    $scope.users = [
        { id:1, name:'Betsy', age:73 },
        { id:2, name:'Norman', age:40 },
        { id:3, name:'Sonja', age:6 },
    ];

    $scope.addUser = function(){
        $scope.users.push( $scope.newUser );
        $scope.newUser={};
    };
});

app.controller('UserShowCtrl', function($stateParams, $scope){
    $scope.user_id = $stateParams.user_id;

    $scope.user = { id:1, name:'Betsy', age:73 };
});

app.controller('MainCtrl', function($scope){
    $scope.message= "World!";
});
</script>
