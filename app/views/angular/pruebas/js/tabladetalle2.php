<script type="text/javascript">
var temporalBandeja=0;
var table=1;
$(document).ready(function() {
    //$("[data-toggle='offcanvas']").click();
    var data = {tabla_id:table};
    TablaDetalle.Campos(data,CargarHTML);
    // Con BD
});

CargarHTML=function(obj){
    // 1: Multiple | 0: Simple
    idtipo=1;
    tipo='simple';
    if(idtipo==1){
        tipo="multiple";
    }

    $(".add").remove();
    var html="";
    $(".selectdinamico").multiselect('destroy');
    for (var i=1; i<= obj.cant; i++) {
        data={c:i,tabla_id:table,t:obj.cant};
        changue='';
        if(i<obj.cant){
            if(idtipo==0){
                changue=' onChange="CargarDetalle('+"'"+i+','+obj.cant+','+table+','+idtipo+"'"+')" ';
            }
            else{
                changue=' data-hidden="'+i+','+obj.cant+','+table+','+idtipo+'" ';
            }
        }

        tiposelect='';
        if(tipo=='multiple'){
            tiposelect='multiple'
        }

        html='  <div class="add col-sm-3">'+
                    '<label>'+obj.campo.split('|')[(i-1)]+':</label>'+
                    '<select class="selectdinamico form-control" '+changue+' name="slct_c'+i+'" id="slct_c'+i+'" '+tiposelect+'>'+
                    '    <option value="">.::Seleccione::.</option>'+
                    '</select>'+
                '</div>';
        $("#selects").append(html);
        if(i>1){
            slctGlobalHtml('slct_c'+i, tipo);
        }
        if(i==1){
            if(idtipo==0){
                slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data);
            }
            else{
                slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
            }
        }
    };
}

CargarDetalle=function(cod){
    var id=cod.split(',')[0];
    var t=cod.split(',')[1];
    var tabla_id=cod.split(',')[2];
    var idtipo=cod.split(',')[3];

    if($("#slct_c"+id).val()!=null && $("#slct_c"+id).length>0 && $("#slct_c"+id).val()!=''){
    id++;
    var valor=[];

        for (var i = 1; i < id; i++) {
            valor.push($("#slct_c"+i).val());
        };
        var data={valor:valor,c:id,t:t,tabla_id:tabla_id,idtipo:idtipo};
        $('#slct_c'+id).multiselect('destroy');

        if(idtipo==0){
            tipo='simple';
            slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data);
        }
        else{
            tipo="multiple";
            slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
        }
    }
}
</script>
