<!DOCTYPE html>
@extends('layouts.master')

@section('includes')
    @parent
    {{ HTML::style('lib/daterangepicker/css/daterangepicker-bs3.css') }}
    {{ HTML::style('lib/bootstrap-multiselect/dist/css/bootstrap-multiselect.css') }}
    {{ HTML::style('lib/iCheck/all.css') }}
    {{ HTML::script('lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}
    {{ HTML::script('lib/momentjs/2.9.0/moment.min.js') }}
    {{ HTML::script('lib/daterangepicker/js/daterangepicker.js') }}
    
    
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?key='.Config::get('wpsi.map.key').'&libraries=places,geometry,drawing') }}
    {{ HTML::script('js/geo/geo.functions.js') }}
    {{ HTML::script('js/geo/markerwithlabel.js') }}
    {{ HTML::script('js/utils.js') }}
    
    @include( 'admin.js.slct_global_ajax' )
    @include( 'admin.js.slct_global' )
    @include( 'angular.pruebas.js.tabladetalle2' )
    @include( 'angular.pruebas.js.tabladetalle_ajax' )
@stop
<!-- Right side column. Contains the navbar and content of the page -->
@section('contenido')
            <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Bandeja de Gestión
                        <small> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
                        <li><a href="#">Historico</a></li>
                        <li class="active">Bandeja de Gestión</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Inicia contenido -->
                            <div class="box">
                                
                                <form name="form_General" id="form_General" method="POST" action="reporte/bandejaexcel">
                                <div class="row form-group">
                                    <div class="col-sm-12" id="selects">
                                        
                                    </div>
                                </div>
                                </form>
                                
                            </div><!-- /.box -->
                            <!-- Finaliza contenido -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop
