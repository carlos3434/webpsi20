 
 ALTER TABLE `psi`.`ultimos_movimientos`   
  CHANGE `id_solicitud_tecnica` `solicitud_tecnica_id` INT(11) NULL;
  
  ALTER TABLE `psi`.`componentes`   
  CHANGE `solicitudtecnica_id` `solicitud_tecnica_id` BIGINT(11) NOT NULL  COMMENT 'id solucion tecnica';
  
DELETE FROM   solicitud_tecnica
  
   TRUNCATE solicitud_tecnica;
 
 TRUNCATE componentes;
 
    ALTER TABLE `psi`.`componentes`   
  ADD CONSTRAINT `fk_solicitud_tecnica` FOREIGN KEY (`solicitudtecnica_id`) REFERENCES `psi`.`solicitud_tecnica`(`id`);
  
  
  ALTER TABLE `psi`.`ultimos_movimientos`   
  CHANGE `solicitud_tecnica_id` `solicitud_tecnica_id` INT(11) NULL  COMMENT 'FK de la tabla solicitud tecnica',
  ADD COLUMN `id_solicitud_tecnica` VARCHAR(11) NULL  COMMENT 'codigo de solicitud tecnica en legados' AFTER `solicitud_tecnica_id`;
