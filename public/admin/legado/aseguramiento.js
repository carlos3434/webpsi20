var app=new Vue({
    el: '#aseguramiento',
    components: {
        // Multiselect: window.VueMultiselect.default
    },
    data: {
        // idestadoaseguramiento : [],
        // idquiebre : [],
        // idnodo : [],
        estadoaseguramiento : {},
        idestadoaseguramiento : 0,
        // quiebre : {},
        // nodo : {},
        // fecha_registro_requerimiento : '',
        // fecha_asignacion : '',
    },
    methods: {
        // getSelectedEstadoAseguramiento: function (obj) {
        //     this.idestadoaseguramiento = [];
        //     for (var key in obj) {
        //         this.idestadoaseguramiento.push(obj[key].id);
        //     }
        // },

        // getSelectedQuiebre: function (obj) {
        //     this.idquiebre = [];
        //     for (var key in obj) {
        //         this.idquiebre.push(obj[key].id);
        //     }
        // },

        // getSelectedNodo: function (obj) {
        //     this.idnodo = [];
        //     for (var key in obj) {
        //         this.idnodo.push(obj[key].id);
        //     }
        // },

        prepareUploadColumna: function (event) {
            files = event.target.files;
            event.stopPropagation();
            event.preventDefault();
            $.each(files, function (key, value)
            {
                var data = new FormData();
                data.append('archivocolumna', value);
                app.subirArchivo(data);
            });
        },

        descargarArchivo: function (){
            var form = document.createElement("form");
                form.setAttribute("method", "get");
                form.setAttribute("action", 'aseguramiento/descargar');
                //form.setAttribute("target", "_blank");
            var input = document.createElement("input");
                input.setAttribute("name", "estado_aseguramiento");
                input.setAttribute("value", this.idestadoaseguramiento);
                // input.setAttribute("value", JSON.stringify(this.idestadoaseguramiento));
                form.appendChild(input);

            // var input2 = document.createElement("input");
            //     input2.setAttribute("name", "quiebre");
            //     input2.setAttribute("value", JSON.stringify(this.idquiebre));
            //     form.appendChild(input2);

            // var input3 = document.createElement("input");
            //     input3.setAttribute("name", "nodo");
            //     input3.setAttribute("value", JSON.stringify(this.idnodo));
            //     form.appendChild(input3);

            // var input4 = document.createElement("input");
            //     input4.setAttribute("name", "fecha_registro_requerimiento");
            //     input4.setAttribute("value", this.fecha_registro_requerimiento);
            //     form.appendChild(input4);

            // var input5 = document.createElement("input");
            //     input5.setAttribute("name", "fecha_asignacion");
            //     input5.setAttribute("value", this.fecha_asignacion);
            //     form.appendChild(input5);

            document.body.appendChild(form);
            form.submit();
        },

        subirArchivo: function (data) {
            /*this.$http.post('pretemporal/procesouno', function (data) {
                this.$set('empresas', data);
            });*/
            eventoCargaMostrar();
            //axios.get('/user?ID=12345')
            axios
            //.post(`${this.source}`,input)
            .post('aseguramiento/cargar',data)
            .then(function (response) {
                obj = response.data;
                eventoCargaRemover();
                if (obj.rst == 1) {
                        
                    swal({
                        title: obj.msj,
                        text: 'Clic en "Continuar" para subir los pedidos',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Continuar',
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            app.procesarArchivo();
                        } else {
                            swal("Cancelado", "No se completo la subida masiva.", "warning");
                        }
                        $("#archivocolumna").fileinput('reset');
                    });

                } else {
                    swal('Mensaje',obj.msj,'error');
                }

            })
            .catch(function (error) {
                console.log(error);

                eventoCargaRemover();
                swal('Error','Ocurrió un error en la carga.','error');
                
            });


        },
        procesarArchivo: function () {
            eventoCargaMostrar();
            axios
            .post('aseguramiento/procesar')
            .then(function (response) {
                obj = response.data;
                console.log(response);
                eventoCargaRemover();
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    alert = 'success';
                }
                swal('Mensaje',obj.msj,alert);

            })
            .catch(function (error) {
                console.log(error);

                eventoCargaRemover();
                swal('Error','Ocurrió un error en la carga.','error');
                
            });
        },

        // listarSelects: function (selects, data) {
        //     axios
        //     .post('listado_lego/selects', {selects: JSON.stringify(selects), datos: JSON.stringify(data)})
        //     .then((response) => {
        //         var obj = response.data;
        //         for(i = 0; i <obj.data.length; i++){
        //             obj.datos = obj.data[i];
        //             eval("this."+selects[i]+" = obj.datos");
        //         }
        //     })
        // },

        listarEstadosAseguramiento: function () {
            axios
            .post('aseguramiento/estadosaseguramiento')
            .then((response) => {
                this.estadoaseguramiento = response.data.datos;
            })
        },
    },
    ready: function () {
        $("#archivocolumna").fileinput({
            showUpload: false,
            allowedFileExtensions: ["csv"],
            previewClass: "bg-warning",
            showCaption: true,
            showPreview: false,
            showRemove: false,
            browseLabel: 'Examinar'
        });
        $('#archivocolumna').on('change', this.prepareUploadColumna);
        this.listarEstadosAseguramiento();

        // var selects = ["estadoaseguramiento", "quiebre", "nodo"];
        // var data = [{}, {}, {}];

        // this.listarSelects(selects, data);
    },
});