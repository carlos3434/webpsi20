var proyectos_id, proyectosObj;
var capas_id, capasObj;
var mapObjects = [];
var coordinatesObjects = [];
var viewcapaskml = [];
var bounds;
var ixobj=0;
var ixproyectoselecc=0;
var markTextColor = textByColor("#" 
                    + $("#pickerMarkBack")
                    .val())
                    .substring(1);
                    
var Proyectos = {
    CargarProyectos:function(){
           $.ajax({
               url         : 'geoproyecto/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarProyectos(obj.datos);
                       proyectosObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
    },

    GuardarProyecto: function(origen) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyecto/save',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,nombre: $("#txt_proyecto").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            Proyectos.CargarProyectos();
                            //Proyectos.CargarCapas(obj.datos);
                            $("#txt_proyecto").val('');
                            //$("#txt_proyectoselect").val(obj.datos);
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#btn_clean").click();
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                        }
                        else{
                            var msj;
                            $.each(obj.msj,function(index,datos){
                              msj=datos;
                            });
                            Psi.mensaje('danger', msj.join(), 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
    },

    CargarCapas:function(id){
           $.ajax({
               url         : 'geoproyectocapa/cargar',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {id:id},
               beforeSend : function() {
                   $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   var html="";
                   var estadohtml="";
                   if(obj.rst==1){
                       HTMLCargarCapas(obj.datos);
                       capasObj=obj.datos;
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
    },

    MapearCapas:function(id,idproyecto,map){
           $.ajax({
               url         : 'geoproyectocapa/mapear',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {capaid:id},
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                  bounds = new google.maps.LatLngBounds();
               },
               success : function(obj) {
                  $(".overlay,.loading-img").remove();
                  if(obj.rst==1){
                    bounds = new google.maps.LatLngBounds();
                     $.each(obj.datos, function(index, data) { 
                        viewcapaskml.push(data);
                        if (data.figura_id==2) {
                            MapearPoligono (data, id, idproyecto,map, bounds, 1);
                        }
                        if (data.figura_id==1) {
                            MapearCircle(data, id, idproyecto, map, bounds, 1)
                        }
                        if (data.figura_id==3) {
                            MapearPunto(data, id, idproyecto, map, bounds, 1)
                        }
                     });

                  }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
    },

    UpdateGeoPlan: function(globalElement) {
           $.ajax({
               url         : 'geoproyectocapaelemento/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {id:globalElement.id,
                              borde: globalElement.strokeColor,
                              grosorlinea: globalElement.strokeWeight,
                              fondo: globalElement.fillColor,
                              opacidad: globalElement.fillOpacity
                            },
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se guardó exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

    },

    deleteUpdatePlanPoly: function(id) {
           $.ajax({
               url         : 'geoproyectocapaelemento/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : {id:id,
                              estado:0
                            },
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                   if(obj.rst==1){
                     Psi.mensaje('success', 'Se eliminó exitosamente', 6000);
                     infoWindow.close(objMap);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });
    },

    Updateproyect: function(idproyecto, campo, valor) {
          var datos={};
          datos['id']=idproyecto;
          if(campo==2){
            campo='estado_publico';
          } else if(campo==1){
            campo='estado';
          }
          datos[campo]=valor;

          if (idproyecto==null) {
             datos=$("#form_proyecto").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
          }

           $.ajax({
               url         : 'geoproyecto/update',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : datos,
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modifico exitosamente', 6000);
                     Proyectos.CargarProyectos();
                     $("#btn_close_modal").click();
                   } else {
                     Psi.mensaje('danger', obj.msj, 6000);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

    },

    EditarCapa: function(datos) {
           $.ajax({
               url         : 'geoproyectocapa/updatecapa',
               type        : 'POST',
               cache       : false,
               dataType    : 'json',
               data        : datos,
               beforeSend : function() {
                  $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
               },
               success : function(obj) {
                   $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                     Psi.mensaje('success', 'Se modificó exitosamente', 6000);
                     $("#btn_close_modal2").click();
                     Proyectos.CargarCapas(proyectosObj[$("#txt_proyectoselect").val()].id);
                   }
               },
               error: function(){
                   $(".overlay,.loading-img").remove();
               }
           });

    },

    ModificarProyecto: function() {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapa/agregarcapa',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,idproyecto: proyectosObj[ixproyectoselecc].id},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            Proyectos.CargarCapas(obj.datos);
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#btn_clean").click();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
    },

    ModificarProyectoCapa: function() {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geoproyectocapaelemento/agregarelemento',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,id: $("#txt_capaselect").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            Proyectos.CargarCapas(proyectosObj[ixproyectoselecc].id);
                            $("#txt_proyecto").val('');
                            $("#txt_capa").val('');
                            $("#div_capa").hide();
                            $("#div_pro").hide();
                            $("#txt_capaselect").val('')
                            $("#btn_clean").click();
                            cancelarActualzc();
                        }
                        else{
                            Psi.mensaje('danger', obj.msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Genere Capa');
            }
    },

}