var BuscarDirecciones = {
    Guardargrupo: function() {
           if (direccionesobj!==undefined) { 
                $.ajax({ 
                    url         : 'geodireccion/save',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {nombre: $("#txt_nombre").val() , dato:direccionesobj},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            //Psi.mensaje('success', obj.msj, 6000);
                            swal("Exito", obj.msj, "success");
                            LimpiarMapa();
                            LimpiarCampos();
                            listarGrupos();
                        }
                        else{
                            var msj;
                            if(typeof obj.msj=='string') {
                                msj=obj.msj;
                            } else {
                                $.each(obj.msj,function(index,datos){
                                msj=datos;
                                });
                                msj=msj.join();
                            }
                            //Psi.mensaje('danger', msj, 6000);
                            swal("Precación", msj, "warning");
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Cargue Archivo CSV');
            }
    },
    ActualizarDirecciones: function() {
           if (direccionesobj!==undefined) { 
                $.ajax({ 
                    url         : 'geodireccion/update',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {grupo:grupodireccionobj[grupodireccion_id].id, dato:direccionesobj},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            //Psi.mensaje('success', obj.msj, 6000);
                            swal("Exito", obj.msj, "success");
                            LimpiarMapa();
                            LimpiarCampos();
                            grupodireccion_id="";
                        }
                        else{
                            var msj;
                            if(typeof obj.msj=='string') {
                                msj=obj.msj;
                            } else {
                                $.each(obj.msj,function(index,datos){
                                msj=datos;
                                });
                                msj=msj.join();
                            }
                            //Psi.mensaje('danger', msj, 6000);
                            swal("Precación", msj, "warning");
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Cargue Archivo CSV');
            }
    },
    Actualizargrupo: function() {
        $.ajax({ 
            url         : 'geodireccion/updategrupo',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {dato:grupodireccionobj[grupodireccion_id]},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    //Psi.mensaje('success', obj.msj, 6000);
                    swal("Exito", obj.msj, "success");
                    listarGrupos();
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    ListarGrupo: function() {
        $.ajax({ 
            url         : 'geodireccion/listargrupo',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                grupodireccionobj=obj;
                HTMLCargarGrupos(obj);
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        }); 
    },
    ListarDirecciones: function() {
        $.ajax({ 
            url         : 'geodireccion/listardirecciones',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {grupo:grupodireccionobj[grupodireccion_id].id},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                LimpiarMapa();
                LimpiarCampos();
                $(".overlay,.loading-img").remove();
                direccionesobj= obj;
                HTMLCargarDirecciones(obj);
                $("#txt_nombre").val(grupodireccionobj[grupodireccion_id].nombre);
                $("#txt_nombre").attr("disabled",true);
                $("#btncancelar").attr("disabled",false);
                $('#btnguardar').text('Actualizar Grupo');
                $('#btnguardar').attr('onClick','ActualizarDirecciones()');
                $("#tabdireccion").click();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        }); 
    },
}