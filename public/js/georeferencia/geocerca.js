var googleMarktxt = "http://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld=";
var temporalBandeja=0;
var table=0;
var addressContent = Array();
var mapTools = {};
var drawingManager;

$(document).ready(function() {
  //SELECTS
  slctGlobalHtml('slct_actividad','multiple');
  //slctGlobal.listarSlct('tipogeozona','slct_tipo_zona_id','simple',[1],null,1);
  
  //modal
  slctGlobalHtml('slct_actividad_id_modal','multiple');
  slctGlobal.listarSlct('tipogeozona',['slct_tipo_zona_id', 'slct_tipo_geozona_id_modal'],'simple',null,null,1);
  slctGlobal.listarSlct('quiebre',['slct_quiebre_id','slct_quiebre_id_modal'],'multiple', null, {},{},"","");
  slctGlobal.listarSlct('actividadtipo',['slct_actividad_tipo_id','slct_actividad_tipo_id_modal'],'multiple', null, {},{},"","");

  slctGlobal.listarSlct('geotabla','slct_elemento','simple');

  Crearmapa();
  Geocerca.CargarProyectos();

  $("[data-toggle='offcanvas']").click();
  var objMain = $('#main');
  function showSidebar() {
      objMain.addClass('use-sidebar');
      mapResize();
  }
  function hideSidebar() {
      objMain.removeClass('use-sidebar');
      mapResize();
  }
  var objSeparator = $('#separator');
  objSeparator.click(function(e) {
      e.preventDefault();
      if (objMain.hasClass('use-sidebar')) {
          hideSidebar();
      }
      else {
          showSidebar();
      }
  }).css('height', objSeparator.parent().outerHeight() + 'px');

  $('html').on('click', function(e) {
        if (typeof $(e.target).data('original-title') == 'undefined' &&
            !$(e.target).parents().is('.popover.in')) {
            $('[data-original-title]').popover('hide');
        }
  });

  //LIMPIAR MAPA
  $("#btn_clean, #btn_cleaned").click(function() {
        LimpiarMapa();
        //Crearmapa();
        for (var i = 0; i < allpolygon.length; i++) {
          allpolygon[i].setMap(null);
        }
  });

  //TABLA
  $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  /*$('#slct_tipo_zona_id').multiselect('clearSelection');
  $('#slct_tipo_zona_id').multiselect('select', 1);
  $('#slct_tipo_zona_id').multiselect('rebuild');*/

  //MODAL
  $('#geoproyectoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); 
    var titulo = button.data('titulo'); 
    proyectos_id = button.data('id');
    var modal = $(this); //captura el modal
    modal.find('.modal-title').text(titulo+' Proyecto');
    $('#form_geoproyecto [data-toggle="tooltip"]').css("display","none");                 
    modal.find('.modal-footer .btn-primary').text('Actualizar');
    modal.find('.modal-footer .btn-primary').attr('onClick','updateproyect();');
    $('#txt_nombre_modal').val( proyectosObj[proyectos_id].nombre);
    $('#slct_estado_modal').val( proyectosObj[proyectos_id].estado);
    $("#form_geoproyecto").append("<input type='hidden' value='"+
                    proyectosObj[proyectos_id].id+"' name='id' id='id'>");
  });
          
  $('#geoproyectoModal').on('hide.bs.modal', function (event) {
    var modal = $(this);
    $('#form_geoproyecto #id').remove();
  });

  $('#geoproyectozonaModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var titulo = button.data('titulo'); 
    zonas_id = button.data('id'); 
    var modal = $(this); //captura el modal
    modal.find('.modal-title').text(titulo+' Zona');
    $('#form_proyectozona [data-toggle="tooltip"]').css("display","none");

    modal.find('.modal-footer .btn-primary').text('Actualizar');
    modal.find('.modal-footer .btn-primary').attr('onClick','updatezona();');

    $('#txt_detalle_modal').val( zonasObj[zonas_id].detalle);
    var quiebre;
    var actividad_tipo;
    if(zonasObj[zonas_id].quiebre_id!==null){
      quiebre=zonasObj[zonas_id].quiebre_id.split(",");
    }
    if(zonasObj[zonas_id].actividad_tipo_id!==null){
      actividad_tipo=zonasObj[zonas_id].actividad_tipo_id.split(",");
    }
    $('#slct_quiebre_id_modal').multiselect('select', quiebre);
    $('#slct_actividad_tipo_id_modal').multiselect('select', actividad_tipo);
    $('#slct_tipo_geozona_id_modal').multiselect('select', zonasObj[zonas_id].tipo_geozona_id);
    var actividad=new Array();
    if(zonasObj[zonas_id].averia==1) actividad.push(1);
    if(zonasObj[zonas_id].provision==1) actividad.push(2);
    $('#slct_actividad_id_modal').multiselect('select', actividad);
    $('#slct_quiebre_id_modal, #slct_actividad_tipo_id_modal, #slct_tipo_geozona_id_modal, #slct_actividad_id_modal').multiselect('rebuild')
    $("#form_proyectozona").append("<input type='hidden' value='"+
                    zonasObj[zonas_id].id+"' name='id' id='id'>");
  });

  $('#geoproyectozonaModal').on('hide.bs.modal', function (event) {
    var modal = $(this);
      $('#form_proyectozona #id').remove();
      $('#slct_quiebre_id_modal, #slct_actividad_tipo_id_modal, #slct_tipo_geozona_id_modal, #slct_actividad_id_modal').multiselect('clearSelection');
  });
});

function Crearmapa() {

  objMap  = new google.maps.Map(document.getElementById("mymap"), objMapProps);
  
  drawingManager = new google.maps.drawing.DrawingManager({
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.POLYGON,
      ]
    },
    polygonOptions: {
      fillColor: '#4dacf0',
      fillOpacity: 0.5,
      strokeWeight: 2,
      strokeColor: '#0e7dcc',
      editable:true
    }
  });
  drawingManager.setMap(objMap);
       
  google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
    drawingManager.setOptions({
       drawingMode: null
    });

    var polygonArray = [];
    for (var i = 0; i < polygon.getPath().getLength(); i++) {
      polygonArray.push(polygon.getPath().getAt(i).lng()+','+polygon.getPath().getAt(i).lat());
    }        
    var actividad=$("#slct_actividad").val();        
    var provision=averia=0;
    if(actividad!==null){
      if(actividad.includes("1")){
        averia=1;
      } 
      if(actividad.includes("2")){
        provision=1;
      }
    }
    mapObjects.push({"elemento": '',"figura_id":'4',"capa": '',"detalle":$("#txt_poligono").val(),
      "coordenadas": polygonArray.join('|'),"orden":'',"borde":polygon.strokeColor,
      "grosorlinea":polygon.strokeWeight, "tabla_detalle_id":"","fondo":polygon.fillColor,
      "opacidad":polygon.fillOpacity, "tipo_zona": $("#slct_tipo_zona_id").val(), 
      "averia":averia, "provision": provision, "actividad_tipo_id": $("#slct_actividad_tipo_id").val(),
      "quiebre_id":  $("#slct_quiebre_id").val()});
      
    polygon.setOptions({
      //      id:$("#txt_poligono").val(),
      detalle: $("#txt_poligono").val(),
      indice:ixobj,
      tipo:4,
      tipo_zona: $("#slct_tipo_zona_id").val(),
      actividad: actividad
    });
    
    ixobj++;
    infoWindow = new google.maps.InfoWindow;

    google.maps.event.addListener(polygon, 'click', function (event) {
      doPolyAction(event,this);
    });
    google.maps.event.addListener(polygon.getPath(), 'set_at', function(event) {
      doPolyActionInsert(event, polygon)
    });
    google.maps.event.addListener(polygon.getPath(), 'insert_at', function(event) {
      doPolyActionInsert(event, polygon)
    });
    google.maps.event.addListener(polygon, 'rightclick', function(event) {
      if (event.vertex == undefined) {
        return;
      } else {
        //abrir un infocontent
        OpenremoveVertex(event, polygon);
      }
    });
    allpolygon.push(polygon);
  }); 

   objMap.addListener('click', function() {
      if(infoWindow!==undefined)
      infoWindow.close(objMap);
  });
}

function Zonachange() {
      op=$("#slct_tipo_zona_id").val();
      if(op!==null) {
        $("#btn_proyecto_zona").removeAttr('disabled',false);
        $("#btn_agregar_zona").removeAttr('disabled',false);
        if(op.indexOf("3")>=0) {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#e66565',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#fc0d19',
              editable: true
            }
          });
        } else if(op.indexOf("2")>=0) {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#535557',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#000000',
              editable: true
            }
          });
        } else {
          drawingManager.setOptions({
            polygonOptions: {
              fillColor: '#4dacf0',
              fillOpacity: 0.5,
              strokeWeight: 2,
              strokeColor: '#0e7dcc',
              editable: true
            }
          });
        }
      } else {
        alert('Debe elegir almenos un tipo zona!');
        $("#btn_proyecto_zona").attr('disabled',true);
        $("#btn_agregar_zona").attr('disabled',true);
      }
}

function OpenremoveVertex(event, polygon, bd=0) {
  if(infoWindow!==undefined)
    infoWindow.close(objMap);
  infoWindow = new google.maps.InfoWindow; 
  var footerContent='<div id="btn_delete">'+
  '<a style="cursor:pointer;text-decoration:underline">Eliminar'+
  '</a></div>';
  infoWindow.setContent(footerContent);
  infoWindow.setPosition(event.latLng);
  infoWindow.open(objMap);
  $('#btn_delete').on('click', function() {
    var path = polygon.getPath();
    path.removeAt(event.vertex);
    if(bd==0) {
      doPolyActionInsert(event, polygon)
    }
    infoWindow.close(objMap);
  });
}

function saveGeoPlan() {
    var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
    if(rs){
        ModificarPoligono(globalElement);
    }
}

function UpdateGeoPlan() {
  var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
  if(rs){
      Geocerca.UpdateGeoPlan(globalElement);
  }
}

function GuardarProyectoZona(){
    if ($("#txt_proyecto_zona").val()!='') {
      var rs=confirm("Se guardaran los poligonos creados ¿Desea Continuar?");
      if(rs){
        Geocerca.GuardarProyectoZona();
      }
    } else {
      alert('Indique Nombre de Proyecto Zona');
    }
}

function HTMLCargarProyectos(datos){
       var html="";

       $('#t_proyecto').dataTable().fnDestroy();

       $.each(datos,function(index,data){
            verhtml="<a class=\"btn btn-primary btn-sm\" onclick=\"CargarZonas("+index+","+data.id+")\"><i class=\"fa fa-eye fa-lg\"></i> </a>";

            estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',1,1)" class="btn btn-danger">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,1)" class="btn btn-success">Activo</span>';
            }

            publicohtml='<span id=\"'+data.id+'\" onClick=\"updateproyect('+data.id+',1,2)\" class=\"btn btn-danger btn-xs\">No Público</span>';
            if(data.publico==1){
                publicohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,2)" class="btn btn-success btn-xs">Público</span>';
            }
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#geoproyectoModal" title="Personalizar Proyecto" \n\
                    data-id="'+index+'" data-titulo="Editar" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Proyecto" \n\
                    data-id="'+data.id+'" onclick="EliminarProyecto('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
            
            mapearhtml   ='<a class="btn btn-primary btn-sm"  title="Mapear Proyecto" \n\
                    data-id="'+data.id+'" onclick="MapearProyecto('+index+','+data.id+')"><i class="fa fa-map-marker"></i> </a>';

            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+estadohtml+"</td>"+
                "<td>"+verhtml+" "+modificarhtml+" "+mapearhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_proyecto").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_proyecto").dataTable();
}

function updateproyect(idproyecto=null,valor=0,campo=0){
  Geocerca.Updateproyect(idproyecto,valor,campo);
}

function updatezona(){
  Geocerca.Updatezona();
}

function CargarZonas(index, id){
  $("#a_capa" ).click();
  $("#txt_proyectoselect").val(index);
  $('#agregar').attr('disabled',false);
  //Geocerca.CargarZonas(id);
  zonasObj=proyectosObj[index].geocercazonas;
  HTMLCargarZonas(zonasObj);
  //$("#btn_cleaned").click();
}

function HTMLCargarZonas(datos){
  var html="";

  $('#t_zonas').dataTable().fnDestroy();

  $.each(datos,function(index,data){//UsuarioObj
    estadohtml   ='<a class="btn btn-primary btn-sm"  title="Mapear Zona" \n\
                    data-id="'+data.id+'" onclick="MapearCapas('+index+','+data.id+','+data.geocerca_proyecto_id+')"><i class="fa fa-map-marker"></i> </a>';
    modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#geoproyectozonaModal" title="Modificar Zona" \n\
                    data-id="'+index+'" data-titulo="Editar" id="ckeditar_'+index+'" ><i class="fa fa-pencil-square-o"></i> </a>';
    eliminarhtml ='<a class="btn btn-primary btn-sm" title="Eliminar Zona" \n\
                    data-id="'+data.id+'" onclick="EliminarZona('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
    checkhtml    ='<div class="has-success"> <label class="checkbox-inline"> '+
                      '<input type="checkbox" name="sh'+data.id+'" checked> Mostar'+
                      '</label>'+
                      '<label class="checkbox-inline" style="margin-left: 0px">'+
                      '<input type="checkbox" name="flick'+data.id+'" > Gif'+
                      '</label>'+ 
                      '<label class="checkbox-inline" style="margin-left: 0px">'+
                      '<input type="checkbox" name="shedit'+data.id+'" > Editar'+
                      '</label> </div>';
    var actividad="";
    if (data.averia==1) {
      actividad+="<br>- Averia ";
    }
    if (data.provision==1) {
      actividad+="<br>- Provisión";
    }
    var zona="";
    if(data.tipo_geozona==undefined){
      zona="Sin Zona";
    } else {
      zona=data.tipo_geozona.nombre;
    }
    html+="<tr>"+
          "<td>"+data.id+"</td>"+
          "<td>"+data.detalle+"</td>"+
          "<td>"+zona+":"+actividad+"</td>"+
          "<td>"+estadohtml+" "+
          modificarhtml+" "+
          eliminarhtml+" "+"</td>"+
          "<td>"+checkhtml+"</td>";
   
      html+="</tr>";
  });

  $("#tb_zonas").html(html);
  //if(editarG == 0) $('.editarG').hide();  
  $("#t_zonas").dataTable();
}

function MapearProyecto(index,idproyecto){
  $('html, body').animate({scrollTop: '100px'}, 1000);
  bounds = new google.maps.LatLngBounds();
  $("#txt_proyectoselect").val(index);
  var zonas=proyectosObj[index].geocercazonas;
  $.each(zonas,function(ix,data){
    MapearPoligono(data,data.id,idproyecto,objMap, bounds, 1);
  });
  //MapearPoligono(zonasObj[index],id,idproyecto,objMap, bounds, 1);
}

function MapearCapas(index,id,idproyecto){
  $('html, body').animate({scrollTop: '100px'}, 1000);
  bounds = new google.maps.LatLngBounds();
  MapearPoligono(zonasObj[index],id,idproyecto,objMap, bounds, 1);
}

function Agregarzona() {
  $("#btn_clean").click();
  var pos=$("#divzonapersonalizada").position()
  $('#sidebar').animate({scrollTop: pos.top}, 1000);
  $('html, body').animate({scrollTop: 50}, 1000);
  $('#divzona').html('Agregar Zona(s) al Proyecto');
  $('#txt_poligono').val('');
  $('#divagregarzonas').css('display','');
  $('#divguardarzonas').css('display','none');
  indexselect=$("#txt_proyectoselect").val();
}

function GuardarZonas() {
    var rs=confirm("Se agregará(n) la(s) zona(s) creada(s) ¿Desea Continuar?");
      if(rs){
        Geocerca.ModificarProyectoZona();
      }
}

function cancelActualizaZona() {
    $('#sidebar').animate({scrollTop: '0px'}, 1000);
    $('html, body').animate({scrollTop: '100px'}, 1000);
    $('#divzona').html('Creación de Zonas');
    $('#txt_poligono').val('');
    $('#txt_proyecto_zona').val('');
    $('#divagregarzonas').css('display','none');
    $('#divguardarzonas').css('display','');
}

function EliminarZona(id){
    var rs=confirm("Se eliminará la zona seleccionada, ¿Desea Continuar?");
    if(rs){
        Geocerca.EliminarZona(id);
    }
}

function MapearElementos(){
  Geocerca.MapearElementos();
}

function deletePlanPoly() {    
    var rs=confirm("Se borrará la figura del mapa ¿Desea continuar?");
        if(rs){
            globalElement.setMap(null);
            infoWindow.close(objMap);
            delete mapObjects[globalElement.indice]; 
            // el id debe quedar solo vacia, para no afectar el array de otro poligono o punto
            coordinatesObjects.splice(globalElement.indice,1);
        }
}

function deleteUpdatePlanPoly() { //eliminar elemento
    var rs=confirm("Se eliminará la figura ¿Desea continuar?");
          if(rs){
              globalElement.setMap(null);
              infoWindow.close(objMap);
              Geocerca.EliminarZona(globalElement.id);
          }
}

function MapearElementos(){
        if($("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
          geoVal = $("#slct_elemento").val().split("_");
          if(geoVal[1] == 3){
                TablaDetalle.TrazarCapa(objMap,
                                      geoVal[0],
                                      '',
                                      '',
                                      'F7584C',
                                      3,
                                      '0.3',
                                      '',
                                      3
                                      );
          } 
          if(geoVal[1] == 2) {
                  //polyDemoCreate
                TablaDetalle.TrazarCapa(objMap,
                                      geoVal[0],
                                      geoVal[2],
                                      'F7584C',
                                      'F7584C',
                                      3,
                                      '0.3',
                                      '',
                                      2
                                      );
          }
          if(geoVal[1] == 1) {
                  // polyDemoCreate
                  TablaDetalle.TrazarCapa(objMap,
                                      geoVal[0],
                                      '',
                                      'F7584C',
                                      'F7584C',
                                      3,
                                      '0.3',
                                      '',
                                      1
                                      );
          }
        } else {
          if($("#"+$("#txt_selfinal").val()).val()==null){
            alert('Seleccione el ultimo Subelemento');
          }
        }
}

function LimpiarMapa() {
  $("#txt_proyecto_zona").val('');
  if(mapObjects.length>0) {
    mapObjects.length=0;
    mapObjects=[];
    ixobj=0;
    coordinatesObjects.length=0;
    coordinatesObjects=[];
  }
}
