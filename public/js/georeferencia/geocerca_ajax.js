var proyectos_id, proyectosObj;
var zonas_id, zonasObj;
var mapObjects = [];
var coordinatesObjects = [];
var bounds;
var ixobj=0;
var indexselect=0;
/*var markTextColor = textByColor("#" 
                    + $("#pickerMarkBack")
                    .val())
                    .substring(1);*/
var Geocerca = {
    
    GuardarProyectoZona: function(origen,evento) {
           if (mapObjects.length>0) { 
                $.ajax({
                    url         : 'geocerca/save',
                    type        : 'POST',
                    cache       : false,
                    dataType    : 'json',
                    data        : {mapa:mapObjects,nombre: $("#txt_proyecto_zona").val()},
                    beforeSend : function() {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    },
                    success : function(obj) {
                        $(".overlay,.loading-img").remove();
                        if(obj.rst==1){
                            Psi.mensaje('success', obj.msj, 6000);
                            $("#txt_proyecto_zona").val('');
                            $("#txt_poligono").val('');
                            Geocerca.CargarProyectos();
                            //Proyectos.CargarCapas(obj.datos);
                            $("#btn_clean").click();
                            $("#slct_elemento").val("").trigger('change');
                            $("#slct_elemento").multiselect('rebuild');
                            $('#slct_quiebre_id, #slct_actividad_tipo_id, #slct_tipo_zona_id, #slct_actividad').multiselect('clearSelection');
                        }
                        else{
                          var msj;
                          if(typeof obj.msj=='string') {
                            msj=obj.msj;
                          } else {
                            $.each(obj.msj,function(index,datos){
                              msj=datos;
                            });
                            msj=msj.join();
                          }
                          Psi.mensaje('danger', msj, 6000);
                        }
                    },
                    error: function(){
                        $(".overlay,.loading-img").remove();
                        Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                    }
                }); 
            }
            else {
                alert('Dibuje Zonas');
            }
    },

    CargarProyectos:function(){
        $.ajax({
            url         : 'geocerca/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                var html="";
                var estadohtml="";
                if(obj.rst==1){
                    HTMLCargarProyectos(obj.datos);
                    proyectosObj=obj.datos;
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },

    Updateproyect: function(idproyecto,valor,campo) {
        var datos={};
        datos['idproyecto']=idproyecto;
        if(campo==2){
        campo='estado_publico';
        } else if(campo==1){
        campo='estado';
        }
        datos[campo]=valor;

        if (idproyecto==null) {
            datos=$("#form_geoproyecto").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        }

        $.ajax({
            url         : 'geocerca/update',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    Psi.mensaje('success', 'Se modifico Proyecto exitosamente', 6000);
                    Geocerca.CargarProyectos();
                    $("#btn_close_modal").click();
                } else {
                    Psi.mensaje('danger', obj.msj, 6000);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },

    UpdateGeoPlan: function(globalElement) {
       /* var actividad= globalElement.actividad;
        var provision=averia=0;
        if(actividad!==null){
            if(actividad.includes("1")){
            averia=1;
            } 
            if(actividad.includes("2")){
            provision=1;
            }
        }*/
        //ACTUALIZAR PUNTOS DEL DIBUJO
        var polygonArray = [];
        for (var i = 0; i < globalElement.getPath().getLength(); i++) {
            polygonArray.push(globalElement.getPath().getAt(i).lng()+','+globalElement.getPath().getAt(i).lat());
        }

        $.ajax({
            url         : 'geocercazonas/update',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {id:globalElement.id,
                            borde: globalElement.strokeColor,
                            grosorlinea: globalElement.strokeWeight,
                            fondo: globalElement.fillColor,
                            opacidad: globalElement.fillOpacity,
                            detalle: globalElement.detalle,
                            tipo_geozona_id: globalElement.tipo_zona,
                            //averia: averia,
                            //provision: provision,
                            coordenadas: polygonArray.join('|')
                        },
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('success', obj.msj, 6000);
                if(obj.rst==1){
                    if(infoWindow!==undefined) {
                        infoWindow.close(objMap);
                    }
                    Geocerca.CargarProyectos();
                    Geocerca.CargarZonas(globalElement.idproyecto);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },

    Updatezona: function() {
        datos=$("#form_proyectozona").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");

        $.ajax({
            url         : 'geocercazonas/update',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    Psi.mensaje('success', 'Se modificó Zona exitosamente', 6000);
                    Geocerca.CargarProyectos();
                    Geocerca.CargarZonas(proyectosObj[$("#txt_proyectoselect").val()].id);
                    $("#btn_close_modal2").click();
                } else {
                    Psi.mensaje('danger', obj.msj, 6000);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },

    CargarZonas:function(id){
        $.ajax({
            url         : 'geocercazonas/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {id:id},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                var html="";
                var estadohtml="";
                if(obj.rst==1){
                    HTMLCargarZonas(obj.datos);
                    zonasObj=obj.datos;
                    //$("#btn_cleaned").click();
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },

    ModificarProyectoZona: function() {
        if (mapObjects.length>0) { 
            $.ajax({
                url         : 'geocercazonas/save',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {mapa:mapObjects,
                               idproyecto: proyectosObj[indexselect].id
                              },
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    $(".overlay,.loading-img").remove();
                    if(obj.rst==1){
                        Psi.mensaje('success', obj.msj, 6000);
                        Geocerca.CargarProyectos();
                        Geocerca.CargarZonas(proyectosObj[$("#txt_proyectoselect").val()].id);
                        indexselect=0;
                        $("#btn_clean").click();
                        cancelActualizaZona();
                        $('#slct_quiebre_id, #slct_actividad_tipo_id, #slct_tipo_zona_id, #slct_actividad').multiselect('clearSelection');
                    }
                    else{
                        Psi.mensaje('danger', obj.msj, 6000);
                    }
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            }); 
        }
        else {
            alert('Es necesario dibujar zonas');
        }
    },

    EliminarZona: function(id) {
        $.ajax({
            url         : 'geocercazonas/update',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {id:id,estado: 0},
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    Psi.mensaje('success', 'Se eliminó la zona seleccionada', 6000);
                    Geocerca.CargarProyectos();
                    Geocerca.CargarZonas(proyectosObj[$("#txt_proyectoselect").val()].id);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
            }
        });
    },
}