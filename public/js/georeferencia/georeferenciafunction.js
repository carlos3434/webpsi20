//  value='';
  var nPoliActu = 0;
  var pos;
  var markerColor = '';
  var img_coord = "";
  var allpolygon= [];
  polyActu = [];
  tmpObject = {};
  tmpActu=[];
  infocontent='';
  var markTextColor = textByColor("#" 
                          + $("#singlePolygonFill")
                          .val())
                          .substring(1);
function getOpacity(polygon){
  var op = "<select id=\"singlePolygonOpacity\">";
  for (var i = 0; i <= 1; ) {
      var sel = "";
      if (Number(polygon.fillOpacity).toFixed(1) == i.toFixed(1)) {
          sel = "selected";
      }
      op += "<option value=\"" + i.toFixed(1) + "\" " + sel + ">" + i.toFixed(1) + "</option>";
      i += 0.1;
  }
  op += "</select>";    
  return op;
}

function getStroke(polygon){
  var gl = "<select id=\"singlePolygonStroke\">";
  for (var i = 0; i <= 10; i++) {
      var sel = "";
      if (Number(polygon.strokeWeight) == i) {
          sel = "selected";
      }
      gl += "<option value=\"" + i + "\" " + sel + ">" + i + "</option>";
  }
  gl += "</select>";
  return gl;
}

function completeContent(event, polygon, action) {
  op = getOpacity(polygon);
  gl = getStroke(polygon);
  var footerContent='';

  if(action=='Action') {
    footerContent = '<b>'+polygon.title+'</b><h5>' +
                    event.latLng.lat() + ',' + event.latLng.lng() +'</h5>';
    footerContent+= '<input id="txt_detalle" type="hidden" value="'+polygon.detalle+'" name="txt_detalle">'; //MODIFICAR PARA ELEMENTOS PROYECTOS
  } else {
    footerContent = '<b>'+polygon.title+polygon.detalle+'</b>'+'<h5>' +
                    event.latLng.lat() + ',' + event.latLng.lng() + '</h5>';
    footerContent+= '<input id="txt_detalle" type="hidden" value="'+polygon.detalle+'" name="txt_detalle">';
  }

  if(polygon.tipo==4) { //poligonos personalizados
      if(action=='Action') {
        footerContent = '<label><b>Nombre:</b></label><input id="txt_detalle" value="'+polygon.detalle+'" name="txt_detalle">';
      } else {
        footerContent =  '<b>'+polygon.proyecto+'</b><h5>' +
                         '<div class="col-sm-12"><label><b>Nombre:</b></label><input id="txt_detalle" value="'+polygon.detalle+'" name="txt_detalle"></div>';
      }
      footerContent+=
      /*'<div><select id="slct_actividad_edit" class="form-control" name="slct_actividad_edit[]" multiple="">'+
                                              '<option value="1">Avería</option>'+
                                              '<option value="2">Provisión</option>'+
            '</select></div>'+
      '<div><select class="form-control"  id="slct_tipo_zona_edit_id" name="slct_tipo_zona_edit_id">'+
            '<option value="1">GeoCerca</option>'+
            '<option value="2">Zona Sombra</option>'+
            '<option value="3">Zona Peligrosa</option>'+
            '<option value="4">Bucket</option>'+
        '</select></div>'+ */
        '<h5>'+event.latLng.lat() + ',' + event.latLng.lng() +'</h5>';
  }
  if(polygon.tipo==2 || polygon.tipo==1 || polygon.tipo==4) {
    footerContent+='<div style="display: table;overflow-y: scroll;" >'
      + '<div style="display: table-row" >'
      + '<div style="display: table-cell"><label>Fondo:</label></div>'
      + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonFill" value="' + polygon.fillColor.substring(1, 7) + '" style="border-color: ' + polygon.fillColor + '"></div>'
      + '<div style="display: table-cell"><label>&nbsp; Opacidad:</label></div>'
      + '<div style="display: table-cell">' + op + '</div>'
      + '</div>'
      + '<div style="display: table-row" >'
      + '<div style="display: table-cell"><label>Linea:</label></div>'
      + '<div style="display: table-cell"><input type="text" size="6" class="picker" id="singlePolygonLine" value="' + polygon.strokeColor.substring(1, 7) + '" style="border-color: ' + polygon.strokeColor + '"></div>'
      + '<div style="display: table-cell"><label>&nbsp; Grosor:</label></div>'
      + '<div style="display: table-cell">' + gl + '</div>'
      + '</div>'
      + '</div>';
  }
  if(polygon.tipo==3) {
    puntocolor=polygon.icon.split("|");
    footerContent+='<div style="display: table;overflow-y: scroll;" >'
            + '<div style="display: table-row" >'
            + '<div style="display: table-cell"><label>Fondo: &nbsp;</label>'
            + '<input type="text" size="6" class="picker" id="singlePolygonFill" value="' + puntocolor[1] + '" style="border-color: #' + puntocolor[1] + '"></div>'
            + '</div>';
  }

  if(action=='Action') {
    footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\" value=\"Aplicar Cambios\" id=\"btncambios\" /></div>";
    footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deletePlanPoly()\" /></div> </div>";
  } else {
    footerContent += "<br><div style='display: table-row' ><div style='display: table-cell'><input type=\"button\" class=\"btn btn-primary\"  id=\"btn_UpdateGeoPlan\" value=\"Aplicar Cambios\" /></div>";
    footerContent += "<div style='display: table-cell'><input type=\"button\"  value=\"Borrar Elemento\" class=\"btn btn-danger\" onclick=\"deleteUpdatePlanPoly()\" /></div> </div>";
  }

  infoWindow.setContent(footerContent);
  infoWindow.setPosition(event.latLng);
  infoWindow.open(objMap);

  if(polygon.tipo=="4") {
    /*$('#slct_quiebre_edit_id').multiselect('destroy');
    $('#slct_tipo_actividad_edit_id').multiselect('destroy');
    slctGlobalHtml('slct_tipo_zona_edit_id','simple');
    slctGlobalHtml('slct_actividad_edit','multiple');
    $('#slct_tipo_zona_edit_id').multiselect('clearSelection');
    $('#slct_actividad_edit').multiselect('clearSelection');
    slctGlobal.listarSlct('quiebre','slct_quiebre_edit_id','multiple', null, {},{},"","");
    slctGlobal.listarSlct('actividadtipo','slct_tipo_actividad_edit_id','multiple', null, {},{},"","");
    var tipozona='';
    if(polygon.tipo_zona!=='') tipozona=polygon.tipo_zona;
    $('#slct_tipo_zona_edit_id').multiselect('select', tipozona);
    $('#slct_tipo_zona_edit_id').multiselect('rebuild');
    $('#slct_actividad_edit').multiselect('select', polygon.actividad);
    $('#slct_actividad_edit').multiselect('rebuild');*/
  }

  $('.picker').colpick({
      layout: 'hex',
      submit: 0,
      colorScheme: 'dark',
      onChange: function(hsb, hex, rgb, el, bySetColor) {
          $(el).css('border-color', '#' + hex);
          // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
          if (!bySetColor) { 
            $(el).val(hex);
          }
      }
  }).keyup(function() {
      $(this).colpickSetColor(this.value);
  });
}

function doPolyAction(event, polygon) {
  globalElement = polygon;
  completeContent(event, polygon, 'Action');

  $('#btncambios').on('click', function() {
      polygon.setOptions({
                strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
            });
      if (polygon.tipo==4) {
        polygon.setOptions({
                detalle: $("#txt_detalle").val(), //nombre
            });
      }
      if (polygon.tipo==1 || polygon.tipo==2 || polygon.tipo==4) {
                polygon.setOptions({
                    strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                    strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                    fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                    fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                });
      }
      if (polygon.tipo==3 ){
        iconSelected = googleMarktxt 
                  + polygon.numero 
                  + "|" 
                  + $("#singlePolygonFill").val() 
                  + "|" 
                  + markTextColor;
        polygon.setOptions({
            icon: iconSelected
        });
      }
      saveGeoPlan();
  });
}

function doPolyActionEdit(event, polygon) {
  globalElement = polygon;
  completeContent(event, polygon, 'Edit');

  $('#btn_UpdateGeoPlan').on('click', function() {
      polygon.setOptions({
                strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                fillOpacity: $("#singlePolygonOpacity").val() //opacidad
            });

      if (polygon.tipo==4) {
        polygon.setOptions({
                detalle: $("#txt_detalle").val(), //nombre
            });
      }
      if (polygon.tipo==1 || polygon.tipo==2 || polygon.tipo==4) {
                polygon.setOptions({
                    strokeColor: '#' + $("#singlePolygonLine").val(), //borde
                    strokeWeight: $("#singlePolygonStroke").val(), //grosor linea
                    fillColor: '#'+ $("#singlePolygonFill").val(), //fondo
                    fillOpacity: $("#singlePolygonOpacity").val(), //opacidad
                });
              }
      if (polygon.tipo==3 ){
                    iconSelected = googleMarktxt 
                              + polygon.numero 
                              + "|" 
                              + $("#singlePolygonFill").val() 
                              + "|" 
                              + markTextColor;

        polygon.setOptions({
            icon: iconSelected
        });
      }

      UpdateGeoPlan();
  });
}

function doPolyActionInsert(event, polygon) {
  var polygonArray = [];
  for (var i = 0; i < polygon.getPath().getLength(); i++) {
    polygonArray.push(polygon.getPath().getAt(i).lng()+','+polygon.getPath().getAt(i).lat());
  }     
  mapObjects[polygon.indice]["coordenadas"]=polygonArray.join('|');
}

function MapearPoligono(data, id, idproyecto, map, bounds, bd=0) {
  
  var triangleCoords=[];
  
  if(data.coord!==undefined) {
    var parame=data.coord.split("|");
  } else {
    var parame=data.coordenadas.split("|");
  }

  for (var i =0; i < parame.length; i++) {
    var locat=[];
    locat=parame[i].split(",");
    triangleCoords.push({"lat": parseFloat(locat[1]), "lng": parseFloat(locat[0])});
    var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
    bounds.extend(pt);
  }
  
  var actividad=[];
  if(data.figura_id==4){
    if(data.averia==1){
      actividad.push("1");
    }
    if(data.provision==1){
      actividad.push("2");
    }
  }

  var bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: data.borde,
            strokeOpacity: 0.8,
            strokeWeight: data.grosorlinea,
            fillColor: data.fondo,
            fillOpacity: data.opacidad,
            detalle: data.detalle,
            tipo:data.figura_id,
            actividad:actividad
        });
  
  if(bd==1) { //si es de Base de datos
    if(data.tipo_geozona_id==undefined) {
      data.tipo_geozona_id="";
    }
    if(data.capa==undefined) {
      data.capa="";
    } else if(data.capa!=='') {
      data.capa="Capa: "+data.capa+"<br>";
    }
    if(data.proyecto==undefined){
      data.proyecto=proyectosObj[$("#txt_proyectoselect").val()].nombre;
    }

    bermudaTriangle.setOptions({
      tipo_zona:data.tipo_geozona_id,
      idproyecto:idproyecto,
      title:"Proyecto: "+data.proyecto+"<br>"+data.capa,
      proyecto: "Proyecto: "+data.proyecto,
      id:data.id
    });
  } else {
    if(data.capa!=='' && data.capa!==undefined) {
      data.capa="Capa: "+data.capa+"<br>";
    }
    if(data.capa==undefined) {
      data.capa="";
    }
    bermudaTriangle.setOptions({
      indice:data.indice,
      title:data.capa+data.detalle,
    });
  }

  bermudaTriangle.setMap(map);

  infoWindow = new google.maps.InfoWindow; 

  if(bd==0) {
    bermudaTriangle.addListener("click", function(event) {
          doPolyAction(event,bermudaTriangle);
    });
  } else {
    bermudaTriangle.addListener("click", function(event) {
          doPolyActionEdit(event,bermudaTriangle);
    });
    showflick(id, map, bermudaTriangle);
  }

  if(data.figura_id==4){
    showEditable(id, map, bermudaTriangle);
  }

  allpolygon.push(bermudaTriangle);
  map.fitBounds(bounds);
}

function MapearCircle(dato, id, idproyecto, map,  bounds, bd=0){  
  var locat=[];
  locat=dato.coord.split(",");
  var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
                            
  var cityCircle = new google.maps.Circle({
    strokeColor: dato.borde,
    strokeOpacity: 0.8,
    strokeWeight: dato.grosorlinea,
    fillColor: dato.fondo,
    fillOpacity: dato.opacidad,
    map: map,
    center: {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) },
    radius: Number(dato.dato),
    detalle: dato.detalle,
    tipo:dato.figura_id
  });

  if(bd==1) { //si es de Base de datos
    cityCircle.setOptions({
      idproyecto:idproyecto,
      capa_id:id,
      title:"Proyecto: "+dato.proyecto+"<br>"+"Capa: "+dato.capa,
      id:dato.id
    });
  } else {
    if(dato.capa!=='' && dato.capa!==undefined) {
      dato.capa="Capa: "+dato.capa+"<br>";
    }
    if(dato.capa==undefined) {
      dato.capa="";
    }
    cityCircle.setOptions({
      indice:data.indice,
      title:dato.capa+dato.detalle,
    });
  }

  bounds.union(cityCircle.getBounds());

  infoWindow = new google.maps.InfoWindow;
  
  if(bd==0) {
    cityCircle.addListener("click", function(event) {
        doPolyAction(event,this);
    });
  } else {
    cityCircle.addListener("click", function(event) {
          doPolyActionEdit(event,this);
    });
    showflick(id, map, cityCircle);
  }

  allpolygon.push(cityCircle);
}

function MapearPunto(data, id, idproyecto, map,  bounds, bd=0){
  markTextColor = textByColor(data.fondo).substring(1);  
  iconSelected = googleMarktxt 
                  + data.orden
                  + "|" 
                  + data.fondo.substring(1)
                  + "|" 
                  + markTextColor;
 
  var locat=[];
  locat=data.coord.split(",");
  var myLatLng = {lat: parseFloat(locat[1]), lng: parseFloat(locat[0]) };
  var pt = new google.maps.LatLng(parseFloat(locat[1]), parseFloat(locat[0]));
  bounds.extend(pt);

  var marker = new google.maps.Marker({
                 position: myLatLng,
                 map: map,
                 animation: google.maps.Animation.DROP,
                 icon: iconSelected,
                 detalle:data.detalle,
                 tipo:data.figura_id,
                 numero:data.orden,
                 draggable:false
               });

  if(bd==1) { //si es de Base de datos
    marker.setOptions({
      idproyecto:idproyecto,
      capa_id:id,
      id:data.id,
      title:"Proyecto: "+data.proyecto+"<br>"+"Capa: "+data.capa,
    });
  } else {
    if(data.capa!=='' && data.capa!==undefined) {
      data.capa="Capa: "+data.capa+"<br>";
    }
    if(data.capa==undefined) {
      data.capa="";
    }
    marker.setOptions({
      title:data.capa+data.detalle,
      indice:data.indice
    });
  }

  map.fitBounds(bounds);

  infoWindow = new google.maps.InfoWindow;
  
  if(bd==0) {
    marker.addListener("click", function(event) {
        doPolyAction(event,this);
    });
  } else {
    marker.addListener("click", function(event) {
          doPolyActionEdit(event,this);
    });
    showflick(id, map, marker);
  }

  allpolygon.push(marker);
}

function ModificarPoligono(globalElement) {
  var id=globalElement.indice;
  var tipo=globalElement.tipo;
  if (tipo==2 || tipo==1 || tipo==4) {
      mapObjects[id]['borde']= globalElement.strokeColor;
      mapObjects[id]['grosorlinea']= globalElement.strokeWeight;
      mapObjects[id]['fondo']= globalElement.fillColor;
      mapObjects[id]['opacidad']= globalElement.fillOpacity;
  }
  if (tipo==3 ){
      mapObjects[id]['fondo']=  "#"+$("#singlePolygonFill").val();
  }
  if (tipo==4) {
      mapObjects[id]['detalle']= globalElement.detalle;
  }
  infoWindow.close(objMap);
}

function showflick(id, map, polygon) {
  $("input[name='sh"+id+"']").change(function(event) {
      if($(this).is(':checked')==true){ 
          polygon.setMap(map);
        }else {
        polygon.setMap(null);
      }
  });
  var cont=0;
  var lim;
  $("input[name='flick"+id+"']").change(function(event) {
      if($(this).is(':checked')==true){ 
            lim=setInterval(function (){
                  if(cont%2==0){
                    polygon.setMap(null);
                  }else{
                    polygon.setMap(map);
                  }
                  cont++;
              }
            ,1000);
      } else {
        clearInterval(lim);
          polygon.setMap(map);
      }
  });
}

function showEditable(id, map, polygon) {
  $("input[name='shedit"+id+"']").change(function(event) {
      if($(this).is(':checked')==true){ 
         polygon.setOptions({
                          editable:true
         });
         google.maps.event.addListener(polygon, 'rightclick', function(event) {
            if (event.vertex == undefined) {
              return;
            } else {
              //abrir un infocontent
              OpenremoveVertex(event, polygon, 1);
            }
          });
      } else {
         polygon.setOptions({
                          editable:false
         });
      }
  });
}

function BuscarElemento(){
  $(".estiloItem").hide();
    if($("#slct_elemento").val()=="" || $("#slct_elemento").val()==null){
      $("#selects").html('');
      $("#div_capa").hide();
      $("#div_pro").hide();
      //$("#btn_capa").attr('disabled',true);
      //$("#btn_proyecto").attr('disabled',true);
    } else {
      //var dependencia = "";
      geoVal = $("#slct_elemento").val().split("_");
      //var origen = geoVal[2]; // mdf,provincia, troba, .. elementos
      if (geoVal[1] == 2 || geoVal[1] == 1) {
          $("#estilo_poligono").show();
      } else if (geoVal[1] == 3) {
          $("#estilo_punto").show();
      }
      table=geoVal[0];
      var data = {tabla_id:table};
      TablaDetalle.Campos(data,CargarHTML);
    }
}

function CargarHTML(obj){
          // 1: Multiple | 0: Simple
          idtipo=1;
          tipo='simple';
          if(idtipo==1){
              tipo="multiple";
          }
      
          $(".add").remove();
          var html="";
          $(".selectdinamico").multiselect('destroy');
          for (var i=1; i<= obj.cant; i++) {
              data={c:i,tabla_id:table,t:obj.cant};
              changue='';
              if(i<obj.cant){
                  if(idtipo==0){
                      changue=' onChange="CargarDetalle('+"'"+i+','+obj.cant+','+table+','+idtipo+"'"+')" ';
                  }
                  else{
                      changue=' data-hidden="'+i+','+obj.cant+','+table+','+idtipo+'" ';
                  }
              }
              if(i==obj.cant){
                  if(idtipo==1){
                    changue=' onChange="opcionSelected('+i+')" ';
                  }
              }
      
              tiposelect='';
              if(tipo=='multiple'){
                  tiposelect='multiple'
              }
      
              html='  <div class="add col-sm-12">'+
                          '<label>'+obj.c_detalle.split('|')[(i-1)]+':</label>'+
                          '<select class="selectdinamico form-control" '+changue+' name="slct_c'+i+'" id="slct_c'+i+'" '+tiposelect+'>'+
                          '    <option value="">.::Seleccione::.</option>'+
                          '</select>'+
                      '</div>';
              $("#selects").append(html);
              if(i>1){
                  slctGlobalHtml('slct_c'+i, tipo);
              }
              if(i==1){
                  if(idtipo==0){
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data);
                  }
                  else{
                      slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+i, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
                  }
              }
          };
}

function CargarDetalle(cod){
          var id=cod.split(',')[0];
          var t=cod.split(',')[1];
          var tabla_id=cod.split(',')[2];
          var idtipo=cod.split(',')[3];
      
          if($("#slct_c"+id).val()!=null && $("#slct_c"+id).length>0 && $("#slct_c"+id).val()!=''){
          id++;
          var valor=[];
      
              for (var i = 1; i < id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
              var data={valor:valor,c:id,t:t,tabla_id:tabla_id,idtipo:idtipo};
              $('#slct_c'+id).multiselect('destroy');
      
              if(idtipo==0){
                  tipo='simple';
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data);
              }
              else{
                  tipo="multiple";
                  slctGlobal.listarSlctpost('tabladetalle','camposdetalle','slct_c'+id, tipo,null,data,null,null,null,null,null,'',CargarDetalle);
              }
          }
}

function opcionSelected(titulo){
  if($('#slct_c'+titulo).val()==null){
    $("#div_capa").hide();
    $("#div_pro").hide()
  }
  else {
    $("#div_capa").show();
    $("#div_pro").show();
    $("#txt_selfinal").val(titulo);
  } 
}

/* --------------------------------------------------------------------------------------------------- */
/* ---------------------- FUNCION COMPARTIDA --------------------------------------------------------- */
/* --------------------------------------------------------------------------------------------------- */

var TablaDetalle={
    Campos:function(datos,evento){
        $.ajax({
            url         : 'tabladetalle/campos',
            type        : 'POST',
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    evento(obj.datos);
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                $("#msj").html('<div class="alert alert-dismissable alert-danger">'+
                                        '<i class="fa fa-ban"></i>'+
                                        '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                        '<b><?php echo trans("greetings.mensaje_error"); ?></b>'+
                                    '</div>');
            }
        });
    },

    TrazarCapa: function(map,origen,titulo,borde,fondo,grosorlinea,opacidad,capa, figura_id) {
            var valor=[];
            id=$("#txt_selfinal").val();
             for (var i = 1; i <= id; i++) {
                  valor.push($("#slct_c"+i).val());
              };
            t=id;
            var data={valor:valor,tabla_id:origen,c:id,t:t++,idtipo:1,idfinal:1};

            $.ajax({
                url: "tabladetalle/camposdetalle",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success: function(obj) {
                    var lati="";
                    var longi="";
                    if(obj.rst==1){ 
                      if(grosorlinea==0){grosorlinea=3;}
                      if(opacidad==''){opacidad='0.3';}
                        bounds = new google.maps.LatLngBounds();
                        var idpunto=0;
                        $.each(obj.datos, function(index, dato) {
                          dato.borde='#'+borde;
                          dato.fondo='#'+fondo;
                          dato.grosorlinea=grosorlinea;
                          dato.opacidad=opacidad;
                          dato.figura_id=figura_id;
                          dato.capa=capa;
                          dato.indice=ixobj;
                          dato.orden=0;
                          ixobj++;
                          if(figura_id==2) { //poligono
                            MapearPoligono(dato, '','', map, bounds);
                          }
                          if(figura_id==1) { //circle
                            MapearCircle(dato, '','', map, bounds);
                          }
                          if(figura_id==3) {
                            idpunto++;
                            dato.orden=idpunto;
                            MapearPunto(dato, '','', map, bounds);
                          }
                          //FALTA MAPEAR PUNTO Y CIRCLE PARA ELEMENTOS
                          mapObjects.push({"elemento": origen,"figura_id":figura_id,"capa": capa,"detalle":dato.detalle,"orden":'',
                                  "borde":'#'+borde,"grosorlinea":grosorlinea,
                                  "fondo":'#'+fondo,"opacidad":opacidad, "orden": dato.orden});

                          coordinatesObjects.push({"coord":dato.coord,"figura_id":figura_id});      
                        });
                    }

                    $(".overlay,.loading-img").remove();
                    
                },
                error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
    },
}