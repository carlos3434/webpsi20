var updMapProps = null;
var updMapTools = {};
var updatemap = null;
var updstreet = null;    
var updbounds = null;    
var boundsArray = [];
var marker = null;
var markeref = null;
var markers = [];
var markersref = [];
var searchBox;
var infoWindow = null;
var geocoder=null;
var markTextColor=null;
var markText = textByColor("#" 
                    + "F7584C")
                    .substring(1);
var googleMarktxt = "https://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld=";
var bounds = null;
var direcciones_id, direccionesobj;
var grupodireccion_id, grupodireccionobj;
var drawingManager=null;
var dtMovimientos;
var objMapProps = {
    center: new google.maps.LatLng(-12.109121, -77.016269),
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP

};

var mapaclean=false;
var orden=['A','B','C','D', 'E'];
var total=0;
$(document).ready(function() {

    $("[data-toggle='offcanvas']").click();

    $('#myTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });

    Crearmapa();
    doSearchBox();
    slctGlobal.listarSlct('geoproyecto', 'slct_proyectos', 'simple', [], {lista:1});
    slctGlobalHtml('slct_capas','multiple');
    slctGlobalHtml('slct_filtro','simple');
    infoWindow = new google.maps.InfoWindow; 
    listarGrupos();

    $("#btn_trazarproy").click(function() {

            if ( $.trim( $("#slct_proyectos").val() ) == '' ) {
                swal("warning", "Seleccione un proyecto ", "warning");
                return false;
            }
            else {
                //console.log($("#slct_capas").val());
              capaid=$("#slct_capas").val();
              mapaclean=true;
              for (var i=0; i<capaid.length; i++)
              {
                Proyectos.MapearProyectos(capaid[i],'',updatemap);
              }
              
            }
    });

    $("#tb_modulo").click(function() {
        $("#tb_modulo").addClass("in active");
        $("#tb_submodulo").removeClass("in active");
    });
    $("#tb_submodulo").click(function() {
        $("#tb_submodulo").addClass("in active");
        $("#tb_modulo").removeClass("in active");
    });

});

function Crearmapa() {
    updatemap  = new google.maps.Map(document.getElementById("mymap"), objMapProps);
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_CENTER,
          drawingModes: [
            google.maps.drawing.OverlayType.MARKER,
          ]
        },
        polygonOptions: {
          fillColor: '#4dacf0',
          fillOpacity: 0.5,
          strokeWeight: 2,
          strokeColor: '#0e7dcc',
          editable:true
        }
      });
}

function CargarArchivos() {
    if ($("#txt_file_plan").val()=='') {
        swal("Error", "Cargue archivo", "error");
        return false;
    }
    LimpiarMapa();
    var inputFile = document.getElementById("txt_file_plan");
    var file = inputFile.files[0];
    var data = new FormData();
    data.append('archivo', file);    
    var dataToDownload = "data:application/octet-stream,"; 

    $.ajax({
        url: "geodireccion/cargaplana",
        type: "POST",
        cache: false,
        data: data,
        dataType: 'json',
        contentType: false,
        processData: false,
        beforeSend: function() {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        },
        error: function(data) {
            $(".overlay,.loading-img").remove();
            Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
        },
        success: function(obj) {
            $(".overlay,.loading-img").remove();  
            LimpiarCampos();
            if(obj.rst==1) {
                direccionesobj= obj.csv;
                HTMLCargarDirecciones();
            } else {
                swal("warning", obj.msj, "warning");
            }
        }
    });
}

function HTMLCargarDirecciones(){
    $("#tb_direcciones").html("");
    var info="";
    var address="";
    bounds = new google.maps.LatLngBounds();
    
    $.each(direccionesobj, function (index, datos) {
        listardirecciones(datos, index);
    });
}

function listardirecciones(datos, ix, contador=0){
    var info="";
    var address="";

    address=datos.direccion_completa;

    info=datos.nombre_dato+": "+datos.dato;

    if(datos.id!==undefined){ //registrados 
        listardireccioneshtml(ix, address, info);
    } else { //desde carga csv.
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 
                address: datos.direccion+' '+datos.numero,
                region:'PE'
            }, function(results, status) {
                if (status == 'OK') {
                    
                    var pos={lat: results[0].geometry.location.lat(),
                            lng: results[0].geometry.location.lng()};
    
                    var coordenadas={
                        coordx:results[0].geometry.location.lng(),
                        coordy:results[0].geometry.location.lat(),
                        validacion:0
                    }
                    direccionesobj[ix]['coordenadas'].push(coordenadas);
                } else if(direccionesobj[ix]['coordenadas'].length==0) {
                        direccionesobj[ix]['coordenadas'][0]={};
                        direccionesobj[ix]['coordenadas'][0]['coordx']="";
                        direccionesobj[ix]['coordenadas'][0]['coordy']="";
                        direccionesobj[ix]['coordenadas'][0]['validacion']=0;
                }
                listardireccioneshtml(ix, address, info);
        });
    }
}

function filtraraddress(num) {
    LimpiarMapa();
    total=0;
    if(direccionesobj!==undefined) {
        $("#tb_direcciones").html("");

        $.each(direccionesobj,function(ix,datos){
            var pasa=crear=false;
            if(num==0 && datos['coordenadas'][0]['coordx']=="") {
                pasa=true;
            }
            if(datos['coordenadas'][0]['coordx']==""){
                crear=true;
            }
            if( (datos.coordenadas.length==num && crear==false) || num=='all' || pasa==true) {
                var info="";
                var address="";
            
                address=datos.direccion_completa;

                /*if (address.split(",").length>1 && datos.numero!=0) {
                    address=address.split(",")[0]+" "+datos.numero+", "+address.split(",")[1];
                } else if (datos[1]!=0){
                    address=datos.direccion+" "+datos.numero;
                }*/
            
                info=datos.nombre_dato+": "+datos.dato;
    
                listardireccioneshtml(ix, address, info);
            }
        });
    }
}

function listardireccioneshtml(ix, address, info) {
    var detalle = direccionesobj[ix]['coordenadas'];
    var i=0;
    var html="";
    $.each(detalle,function(index_,data){
            var check='<div class="has-success">'+
                '<input type="checkbox" id="chksee_'+ix+'_'+index_+'" onclick="mapearMarker('+ix+','+index_+')"> '+
                '</div>';
            
            var cheked="";
            if(data.validacion==1){
                cheked="checked ";
            }
            
            var validar='<div class="has-success">'+
                '<input type="checkbox" id="chkval_'+ix+'_'+index_+'" name="chk_validacoord_'+ix+'" onClick="checkone('+ix+','+index_+')" '+cheked+' disabled="">  '+
                '</div>';
            var clase="";
            if(direccionesobj[ix]['coordenadas'][0]['coordx']=="") {
                clase='style="background: #f56954"';
            }
                html+="<tr><td "+clase+">"+orden[i]+"</td>";
                html+="<td>"+check+"</td>";
                html+="<td>"+validar+"</td></tr>";
                i++;
    });
    if(html==""){
         var check='<div class="has-success">'+
                '<input type="checkbox" id="chksee_'+ix+'_0" onclick="mapearMarker('+ix+',0)"> '+
                '</div>';

            
         var validar='<div class="has-success">'+
                '<input type="checkbox" id="chkval_'+ix+'_0" name="chk_validacoord_'+ix+'" disabled="">  '+
                '</div>';
            
                html+="<tr><td>"+orden[i]+"</td>";
                html+="<td>"+check+"</td>";
                html+="<td>"+validar+"</td></tr>";
                i++;   
    }

    var subtabla='<div style="max-height:200px; overflow-y: auto;"><table class="table table-bordered table-striped">'+
                '<thead>'+
                    '<tr>'+
                        '<th>Opción</th>'+
                        '<th>Ver</th>'+
                        '<th>Validar</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>'+
                html+
                '</tbody></div>';

    contador=ix+1;
    total++;

    $("#tb_direcciones").append("<tr>"+
    "<td>"+contador+"</td>"+
    "<td>"+address+"</td>"+
    "<td>"+info+"</td>"+
    "<td>"+subtabla+"</td>"+
    "</tr>");
    $("#div_resultados").html("<label><b>Total: "+total+" Resultados</b></label>");
}

function mapearMarker(ix, indice){

    if($("input[id='chksee_"+ix+"_"+indice+"']").is(':checked')==true) {

        var icon = googleMarktxt 
                            + parseInt(ix+1)
                            + "|F7584C|"+markText;

        if(direccionesobj[ix]['coordenadas'][indice]['coordx']!=="") {
            var pos={lat: parseFloat(direccionesobj[ix]['coordenadas'][indice]['coordy']),
                lng: parseFloat(direccionesobj[ix]['coordenadas'][indice]['coordx'])};

            var draggable=true;
            if($("input[id='chkval_"+ix+"_"+indice+"']").is(':checked')==true) {
                var draggable=false;
            }
            var marker = new google.maps.Marker({
                map: updatemap,
                position: pos,
                icon:icon,
                draggable:draggable
            });

            bounds.extend(pos);
            updatemap.fitBounds(bounds);
    
            markerevent(marker, ix, indice);
        } else {
            dibujarmarker(ix, icon);
        }
    } else {
        if(markers[ix]!==undefined) {
            markers[ix][indice].setMap(null);
            delete markers[ix][indice];
        }
    }

}

function markerevent(marker, ix, indice){
    marker.addListener('dragend',  function() {
            direccionesobj[ix]['coordenadas'][indice]['coordx']=marker.getPosition().lng();
            direccionesobj[ix]['coordenadas'][indice]['coordy']=marker.getPosition().lat();
        });        
    if(markers[ix]==undefined) markers[ix]=[];
    markers[ix][indice]=marker;    
    $("input[id='chkval_"+ix+"_"+indice+"']").attr("disabled",false);
    $("input[id='chkval_"+ix+"_"+indice+"']").change(function(event) {
        if($(this).is(':checked')==true){ 
            if(marker) {
                marker.setOptions({
                    draggable:false
                });
            }
            direccionesobj[ix]['coordenadas'][indice]['validacion']=1;
        }else {
            if(marker) {
                marker.setOptions({
                    draggable:true
                });
            }                                                
            direccionesobj[ix]['coordenadas'][indice]['validacion']=0;
        }
    });
}

function dibujarmarker(ix, icon){
    var rs=confirm("No se encontro ubicación, ¿Desea Crearla en el mapa?");
    if(rs){
        drawingManager.setMap(updatemap);
        google.maps.event.addListener(drawingManager, 'markercomplete', function(marker) {
            drawingManager.setOptions({
                drawingMode: null
            });
            marker.setOptions({
                icon:icon,
                draggable:true
            });

            drawingManager.setMap(null);

            direccionesobj[ix]['coordenadas'][0]['coordx']=marker.getPosition().lng();
            direccionesobj[ix]['coordenadas'][0]['coordy']=marker.getPosition().lat();
            direccionesobj[ix]['coordenadas'][0]['validacion']=0;

            markerevent(marker, ix, 0);
        });
    } else {
        drawingManager.setMap(null);
        $("input[id='chksee_"+ix+"_"+0+"']").attr("checked", false);
    }
}

function cargardirecciones(indice){
    grupodireccion_id=indice;
    BuscarDirecciones.ListarDirecciones();
}
function checkone(ix, indice){
        if($("input[id='chkval_"+ix+"_"+indice+"']").is(':checked')==true) {
            $("input[name='chk_validacoord_"+ix+"']").prop("checked", false);
            
            $.each(direccionesobj[ix]['coordenadas'],function(index,datos){
                direccionesobj[ix]['coordenadas'][index]['validacion']=0;
            });
            $("input[id='chkval_"+ix+"_"+indice+"']").prop("checked", true);
            direccionesobj[ix]['coordenadas'][indice]['validacion']=1;
        }
}
// SIUD
function listarGrupos(){
    BuscarDirecciones.ListarGrupo();
}

function HTMLCargarGrupos(datos){
    var html="";

    $.each(datos,function(index,data){
        eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Grupo" \n\
                    data-id="'+index+'" onclick="eliminargrupo('+index+')"><i class="fa fa-trash-o"></i> </a>';
        verhtml="<a class=\"btn btn-primary btn-sm\" onclick=\"cargardirecciones('"+index+"')\"><i class=\"fa fa-eye fa-lg\"></i> </a>";

        html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+eliminarhtml+"</td>"+
               "<td>"+verhtml+"</td></tr>";
    });

    $("#tb_grupo").html(html);
}

function guardargrupo(){
      var rs=confirm("¿Desea guardar el Grupo de Direcciones?");
       if(rs){
        BuscarDirecciones.Guardargrupo();
      }
}

function eliminargrupo(indice){
    var rs=confirm("¿Seguro de eliminar el Grupo de Direcciones?");
        if(rs){
            grupodireccion_id=indice;
            grupodireccionobj[grupodireccion_id].estado=0;
            BuscarDirecciones.Actualizargrupo();
        }
}

function ActualizarDirecciones(){
    var rs=confirm("¿Seguro de Actualizar las Direcciones?");
        if(rs){
            BuscarDirecciones.ActualizarDirecciones();
        }
}

//FUNCIONES EXTRAS
function doPolyAction(event, polygon) {

        var footerContent = '<b>'+polygon.id+'</b><h5>' +
                            event.latLng.lat() + ',' + event.latLng.lng() +
                            '</h5>';
        footerContent+='<input id="txt_detalle" type="hidden" value="'+polygon.id+'" name="txt_detalle">';
        if(polygon.tipo==4) { //poligonos personalizados
           var footerContent = '<label><b>Nombre:</b></label><input id="txt_detalle" value="'+polygon.id+'" name="txt_detalle">' +
                '<h5>'+event.latLng.lat() + ',' + event.latLng.lng() +
                '</h5>';
        }
      
        infoWindow.setContent(footerContent);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(updatemap);
}

function doSearchBox(input) {
    var input =(document.getElementById('pac-input'));
    updatemap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);    
    searchBox = new google.maps.places.SearchBox((input));            
    google.maps.event.addListener(searchBox, 'places_changed', function() {
        var places = searchBox.getPlaces();                
        if (places.length == 0) {
            return;
        }
        for (var i = 0; i < markersref.length; i++) {
            markersref[i].setMap(null);
        }    
        // For each place, get the icon, place name, and location.
        markersref = [];
        var bounds = new google.maps.LatLngBounds();
        var icon='https://maps.google.com/mapfiles/ms/icons/green-dot.png';
            for (var i = 0, place; place = places[i]; i++) {
                // Create a markeref for each place.
                var markeref = new google.maps.Marker({
                    map: updatemap,
                    title: place.name,
                    position: place.geometry.location,
                    icon: icon
                });        
                markersref.push(markeref);        
                bounds.extend(place.geometry.location);
            }    
        updatemap.fitBounds(bounds);
    });    
    google.maps.event.addListener(updatemap, 'bounds_changed', function() {
        var bounds = updatemap.getBounds();
        searchBox.setBounds(bounds);
    });
}

function combocapas(){
    $("#slct_capas").multiselect('destroy');
    var proyecto_id=$("#slct_proyectos").val();
    slctGlobal.listarSlctpost('geoproyectocapa', 'cargar', 'slct_capas', 'multiple', [],{id: proyecto_id});
}

function LimpiarMapa(){
    for (var i = 0; i < markersref.length; i++) {
            markersref[i].setMap(null);
    }
    markersref=[];

    for (var i = 0; i < markers.length; i++) {
        if(markers[i]!==undefined) {
            for (var y = 0; y < markers[i].length; y++) {
                if(markers[i][y])
                markers[i][y].setMap(null);
            }
        }
    }
    markers=[];

    if(mapaclean==true) {
        Crearmapa();
    }

    mapaclean=false;
}   

function LimpiarCampos(){
    $("#div_resultados").html("");
    $("#txt_nombre").val("");
    $("#tb_direcciones").html("");
    $("#txt_nombre").attr("disabled",false);
    $("#btncancelar").attr("disabled",true);
    $('#btnguardar').text('Guardar Grupo');
    $('#btnguardar').attr('onClick','guardargrupo()');
    direcciones_id="";
    direccionesobj={};
    $("#slct_filtro").val('all');
    $("#slct_filtro").multiselect('rebuild');
    total=0;
}

function Cancelar() {
    var rs=confirm("Se perderán todos los cambios realizados, ¿Desea Continuar?");
        if(rs){
            LimpiarCampos();
            LimpiarMapa();
            grupodireccion_id="";
        }
}