var googleMarktxt = "http://chart.apis.google.com/chart" 
                    + "?chst=d_map_pin_letter&chld=";
var temporalBandeja=0;
var table=0;
var addressContent = Array();
var mapTools = {};
var drawingManager;

$(document).ready(function() {
        slctGlobalHtml('slct_actividad','simple');
        var objMain = $('#main');

        $("[data-toggle='offcanvas']").click();

        //SELECTS
        slctGlobalHtml('slct_tipo_proyecto_id','simple');
        slctGlobalHtml('slct_tipo_zona_id','simple');
        slctGlobal.listarSlct('geotabla','slct_elemento','simple');

        Crearmapa();
        Proyectos.CargarProyectos();

        function showSidebar() {
            objMain.addClass('use-sidebar');
            mapResize();
        }
        function hideSidebar() {
            objMain.removeClass('use-sidebar');
            mapResize();
        }
        var objSeparator = $('#separator');
        objSeparator.click(function(e) {
            e.preventDefault();
            if (objMain.hasClass('use-sidebar')) {
                hideSidebar();
            }
            else {
                showSidebar();
            }
        }).css('height', objSeparator.parent().outerHeight() + 'px');

        //LIMPIAR MAPA
        $("#btn_clean, #btn_cleaned").click(function() {
             LimpiarMapa();
             Crearmapa();
        });

        //TABLA
        $('#myTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });

        $('html').on('click', function(e) {
              if (typeof $(e.target).data('original-title') == 'undefined' &&
                  !$(e.target).parents().is('.popover.in')) {
                  $('[data-original-title]').popover('hide');
              }
        });

        //COLORES POLIGONO Y MARCADORES
        $("#pickerMarkBack").val('F7584C');
        $("#pickerPolyBack").val('F7584C');
        $("#pickerPolyLine").val('F7584C');

        $( "#sliderPolyOpac" ).slider({
           value:0,
           min: 0,
           max: 0.99,
           step: 0.01,
           slide: function( event, ui ) {
               $("#" + $(this).attr("title")).css("opacity", ui.value);
           }
        });

        $( "#sliderPolyOpac_edit" ).slider({
              value:0,
              min: 0,
              max: 0.99,
              step: 0.01,
              slide: function( event, ui ) {
                  $("#" + $(this).attr("title")).css("opacity", ui.value);
              }
        });

        $("#polyLineCreate").change(function (){
            $("#polyDemoCreateborder")
                .css("border-width", $( this ).val() + "px" );
        });
        $("#polyLineCreate_edit").change(function (){
            $("#polyDemoCreateborder_edit")
                .css("border-width", $( this ).val() + "px" );
        });

        $("#pickerMarkBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              if(!bySetColor) $(el).val(hex);
                          
                          $(".newMarkerBackground").css(
                                  "background-color", 
                                  "#" + $("#pickerMarkBack").val()
                              );
            }
            }).keyup(function(){
             $(this).colpickSetColor(this.value);
        });
          
        $("#pickerMarkBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                // Fill the text box just if the color was set using the picker, 
                            // and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
                            
                            $(".newMarkerBackground").css(
                                    "background-color", 
                                    "#" + $("#pickerMarkBack_edit").val()
                                );
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
        });

        $("#pickerPolyBack").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              // Fill the text box just if the color was set using the picker, 
                          // and not the colpickSetColor function.
              if(!bySetColor) $(el).val(hex);
                          $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
            });
    
            $("#pickerPolyBack_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                            $( "#" + $(el).attr("title") ).css("background-color", '#'+hex);
              }
                }).keyup(function(){
                        $(this).colpickSetColor(this.value);
            });

        $("#pickerPolyLine").colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
              $(el).css('border-color','#'+hex);
              if(!bySetColor) $(el).val(hex);
                          $("#polyDemoCreateborder").css("border-color", '#'+hex);
            }
              }).keyup(function(){
                      $(this).colpickSetColor(this.value);
        });
    
        $("#pickerPolyLine_edit").colpick({
              layout:'hex',
              submit:0,
              colorScheme:'dark',
              onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);
                if(!bySetColor) $(el).val(hex);
                            $("#polyDemoCreateborder_edit").css("border-color", '#'+hex);
              }
              }).keyup(function(){
                        $(this).colpickSetColor(this.value);
        });

        //MODALES
        $('#proyectocapaModal').on('show.bs.modal', function (event) {

              var button = $(event.relatedTarget);
              var titulo = button.data('titulo'); 
              capas_id = button.data('id'); 
              var modal = $(this); //captura el modal
              modal.find('.modal-title').text(titulo+' Capa');
              $('#form_proyectocapa [data-toggle="tooltip"]').css("display","none");

                 
              modal.find('.modal-footer .btn-primary').text('Actualizar');

              $('#form_proyectocapa #txt_nombre').val( capasObj[capas_id].nombre);
              
              var elementos=capasObj[capas_id].elementos.split(",");
              var _html='<table class="table table-bordered">';
              var i=1; //class="info"
                   $.each(elementos, function(index, datae) {
                      if(i==1){
                        _html+='</tr>';
                      }
                      if(i<=5){
                        _html+='<td>'+datae+'</td>';
                        i++;
                      } 
                      if(i==6){
                      _html+='<tr>';
                      i=1;
                      }
                   });
                  _html+='</table>';
              $('#form_proyectocapa #capaelemento').html(_html);

              if (capasObj[capas_id].tipo=='3') {
               /* $("#estilo_poligono_edit").hide();
                $("#estilo_punto_edit").show();*/
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPunto();');
              } else {
                /*$("#estilo_poligono_edit").show();
                $("#estilo_punto_edit").hide();*/
                modal.find('.modal-footer .btn-primary').attr('onClick','EditarPoligono();');
              }

              $("#form_proyectocapa").append("<input type='hidden' value='"+
                    capasObj[capas_id].id+"' name='id' id='id'>");

              if(capasObj[capas_id].figura_id==4){
                $('#form_proyectocapa #txt_nombre').attr("disabled",true);
              }
        });

        $('#proyectocapaModal').on('hide.bs.modal', function (event) {
              var modal = $(this);
               $('#form_proyectocapa #id').remove();
               $('#form_proyectocapa #capaelemento').html('');
               $('#form_proyectocapa #txt_nombre').attr("disabled",false);
        });

        $('#proyectoModal').on('show.bs.modal', function (event) {

              var button = $(event.relatedTarget); 
              var titulo = button.data('titulo'); 
              proyectos_id = button.data('id');
              var modal = $(this); //captura el modal
              modal.find('.modal-title').text(titulo+' Proyecto');
              $('#form_proyecto [data-toggle="tooltip"]').css("display","none");
                 
              modal.find('.modal-footer .btn-primary').text('Actualizar');
               modal.find('.modal-footer .btn-primary').attr('onClick','updateproyect();');

              $('#form_proyecto #txt_nombre').val( proyectosObj[proyectos_id].nombre);
              $('#form_proyecto #slct_estado').val( proyectosObj[proyectos_id].estado_publico );

              $("#form_proyecto").append("<input type='hidden' value='"+
                    proyectosObj[proyectos_id].id+"' name='id' id='id'>");
        });
          
        $('#proyectoModal').on('hide.bs.modal', function (event) {
              var modal = $(this);
               $('#form_proyecto #id').remove();
        });

        //KML CARGA Y DESCARGA
        $("#btnupfile").trigger("click");

        $("#btn_kml").click(function(){
              var polygons = Array();
              var markers = Array();
              if(viewcapaskml.concat(mapObjects).length > 0) {
                  for (var i in mapObjects) {
                      mapObjects[i]['coord']=coordinatesObjects[i]['coord'];
                  }
                  Map.preparePolygon(viewcapaskml.concat(mapObjects),Proyectos.FinalizarKml); 
              }else{
                  Psi.mensaje('danger', 'No proyectos Mapeados', 6000);
              }
        });

        $("#btn_kml_upload").click(function(){
              var data = new FormData();
              var inputFile = document.getElementById("kml_file");
              var file = inputFile.files[0];
              data.append('archivo', file);
              Proyectos.uploadkml(data,cargarKML);
        });
     });


function Crearmapa() {
  objMap = doObjMap("mymap", objMapProps, mapTools);
  objMap.addListener('click', function() {
      if(infoWindow!==undefined)
      infoWindow.close(objMap);
  });
}

function HTMLCargarProyectos(datos){
       var html="";

       $('#t_proyecto').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            verhtml="<a class=\"btn btn-primary btn-sm\" onclick=\"CargarCapas("+index+","+data.id+")\"><i class=\"fa fa-eye fa-lg\"></i> </a>";

            estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',1,1)" class="btn btn-danger btn-xs">Inactivo</span>';
            if(data.estado==1){
                estadohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,1)" class="btn btn-success btn-xs">Activo</span>';
            }

            publicohtml='<span id=\"'+data.id+'\" onClick=\"updateproyect('+data.id+',1,2)\" class=\"btn btn-danger btn-xs\">No Público</span>';
            if(data.publico==1){
                publicohtml='<span id="'+data.id+'" onClick="updateproyect('+data.id+',0,2)" class="btn btn-success btn-xs">Público</span>';
            }
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectoModal" title="Personalizar Proyecto" \n\
                    data-id="'+index+'" data-titulo="Editar" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Proyecto" \n\
                    data-id="'+data.id+'" onclick="EliminarProyecto('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
                    
            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+data.created+"</td>"+
               "<td>"+publicohtml+"</td>"+
                "<td>"+verhtml+modificarhtml+
               eliminarhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_proyecto").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_proyecto").dataTable();
}

function GenerarCapa() {
        if(($("#txt_capa").val()!='' || $('#txt_capaselect').val()!='') && 
              $("#slct_c"+$("#txt_selfinal").val()).val()!=null ) {
              geoVal = $("#slct_elemento").val().split("_");

              if(geoVal[1] == 3){
                      TablaDetalle.TrazarCapa(objMap,
                                          geoVal[0],
                                          '',
                                          '',
                                          $("#pickerMarkBack").val(),
                                          '',
                                          '',
                                          $("#txt_capa").val(),
                                          3
                                          );
              } 
              if(geoVal[1] == 2) {
                      TablaDetalle.TrazarCapa(objMap,
                                          geoVal[0],
                                          geoVal[2],
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val(),
                                          2
                                          );

              }
              if(geoVal[1] == 1) {
                      TablaDetalle.TrazarCapa(objMap,
                                          geoVal[0],
                                          '',
                                          $("#pickerPolyLine").val(),
                                          $("#pickerPolyBack").val(),
                                          $("#polyLineCreate").val(),
                                          $("#sliderPolyOpac" ).slider( "value" ),
                                          $("#txt_capa").val(),
                                          1
                                          );

              }
            } else {
              if($("#"+$("#txt_selfinal").val()).val()==null){
                alert('Seleccione el ultimo Subelemento');
              }
              else {
              alert('Indique nombre de capa');
              }
            }
}

function LimpiarMapa() {
  $("#txt_proyecto").val('');
  $("#txt_capa").val('');
  if(mapObjects.length>0) {
    mapObjects.length=0;
    mapObjects=[];
    ixobj=0;
    coordinatesObjects.length=0;
    coordinatesObjects=[];
  }
  if(viewcapaskml.length>0) {
    viewcapaskml.length=0;
    viewcapaskml=[];
  }
}

function saveGeoPlan() {
    var rs=confirm("Se guardaran los cambios ¿Desea continuar?");
    if(rs){
        ModificarPoligono(globalElement);
    }
}

function deletePlanPoly() {    
    var rs=confirm("Se borrará la figura del mapa ¿Desea continuar?");
    if(rs){
        globalElement.setMap(null);
        infoWindow.close(objMap);
        // el id debe quedar solo vacia, para no afectar el array de otro poligono o punto
        delete mapObjects[globalElement.indice]; 
        coordinatesObjects.splice(globalElement.indice,1);
    }
}

function GuardarProyecto(h) {
    if ($("#txt_proyecto").val()!='') {
            var rs=confirm("Se guardaran las capas creadas ¿Desea Continuar?");
            if(rs){
              geoVal = $("#slct_elemento").val().split("_");
              Proyectos.GuardarProyecto(geoVal[2]);
            }
    } else {
            alert('Indique nombre de proyecto');
    }
}

function CargarCapas(index, id){
    $( "#a_capa" ).click();
    $("#txt_proyectoselect").val(index);
    $('#agregar').attr('disabled',false);
    Proyectos.CargarCapas(id);
}

function HTMLCargarCapas(datos){
       var html="";

       $('#t_capa').dataTable().fnDestroy();

       $.each(datos,function(index,data){//UsuarioObj
            estadohtml='<a class="btn btn-primary btn-sm"  title="Mapear Capa" \n\
                    data-id="'+data.id+'" onclick="MapearCapas('+data.id+','+data.idproyecto+')"><i class="fa fa-map-marker"></i> </a>';
            modificarhtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#proyectocapaModal" title="Personalizar Capa" \n\
                    data-id="'+index+'" data-titulo="Editar" id="ckeditar_'+index+'" ><i class="fa fa-pencil-square-o"></i> </a>';
            eliminarhtml='<a class="btn btn-primary btn-sm" title="Eliminar Capa" \n\
                    data-id="'+data.id+'" onclick="EliminarCapas('+data.id+')"><i class="fa fa-trash-o"></i> </a>';
            checkhtml='<div class="has-success"> <label class="checkbox-inline"> '+
                      '<input type="checkbox" name="sh'+data.id+'" checked> Show'+
                      '</label>'+
                      '<label class="checkbox-inline">'+
                      '<input type="checkbox" name="flick'+data.id+'" > Flick'+
                      '</label> </div>';

           
            agregarhtml='<a class="btn btn-primary btn-sm"  onclick="AgregarElemento('+data.id+','+index+')" title="Agregar Elemento">'+
                        '<i class="fa fa-plus"></i></a>';
            $("#agregar").removeAttr('disabled');
                        
            html+="<tr>"+
               "<td>"+data.id+"</td>"+
               "<td>"+data.proyecto+"</td>"+
               "<td>"+data.nombre+"</td>"+
               "<td>"+estadohtml+"&nbsp;"+
               modificarhtml+"&nbsp;"+
               eliminarhtml+"&nbsp;"+agregarhtml+"</td>"+
               "<td>"+checkhtml+"</td>";
   
           html+="</tr>";
       });
       $("#tb_capa").html(html);
       if(editarG == 0) $('.editarG').hide();  
       $("#t_capa").dataTable();

}

function MapearCapas(id,idproyecto){
    $('html, body').animate({scrollTop: '100px'}, 1000);
    Proyectos.MapearCapas(id,idproyecto,objMap);
}

function UpdateGeoPlan() {
  var rs=confirm("Se guardarán los cambios ¿Desea continuar?");
  if(rs){
      Proyectos.UpdateGeoPlan(globalElement);
  }
}

function deleteUpdatePlanPoly() { //eliminar elemento
    var rs=confirm("Se eliminará el elmento seleccionado ¿Desea continuar?");
          if(rs){
              globalElement.setMap(null);
              infoWindow.close(objMap);
              Proyectos.deleteUpdatePlanPoly(globalElement.id);
          }
}

function updateproyect(idproyecto, valor, campo){
        Proyectos.Updateproyect(idproyecto, campo, valor);
}

function EliminarProyecto(id){
       var rs=confirm("Se eliminará el proyecto seleccionada, ¿Desea Continuar?");
       if(rs){
           updateproyect(id,'0','1')
       }
}

function EditarPoligono(){
      var rs=confirm("¿Desea guardar los cambios?");
       if(rs){
        data={/*borde:'#'+$("#form_proyectocapa #pickerPolyLine_edit").val(),
            fondo:'#'+$("#form_proyectocapa #pickerPolyBack_edit").val(),
            grosorlinea:$("#form_proyectocapa #polyLineCreate_edit").val(),
            opacidad:$("#form_proyectocapa #sliderPolyOpac_edit" ).slider( "value" ),*/
            id:$("#form_proyectocapa #id").val(),
            nombre:$("#form_proyectocapa #txt_nombre").val()};
        Proyectos.EditarCapa(data);
      }
}

function EliminarCapas(id){
       var rs=confirm("Se eliminará la capa seleccionada, ¿Desea Continuar?");
       if(rs){
           data={
            id:id,
            estado:0};
            Proyectos.EditarCapa(data);
       }
}

function Agregarcapa() {
      $('#sidebar').animate({scrollTop: 0}, 1000);
      $('html, body').animate({scrollTop: 0}, 1000);
      $('#divaccion').html('Agregar Capa(s)');  
      LimpiarMapa();
      $('#elementcontent').attr('aria-expanded','true');
      $('#elementcontent').addClass('in');
      $('#elementcontent').height( '100%' );
      $('#div_pro').html(
                    '<div class="form-group">'+
                      '<div class="col-sm-12"><label>&nbsp;</label>'+
                     '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success"  onClick="ActualizarCapa()">Actualizar Proyecto</button>'+
                      '</div></div>');
      $('#slct_elemento').val("").trigger('changue');
      $('#slct_elemento').multiselect('rebuild');
      $('#div_cancel').show();  
      ixproyectoselecc=$("#txt_proyectoselect").val();
} 

function ActualizarCapa() {
    var rs=confirm("Se agregaran al Proyecto las capas creadas ¿Desea Continuar?");
              if(rs){
                Proyectos.ModificarProyecto();
              }
}

function cancelarActualzc() {
          $('#slct_elemento').val("").trigger('change');
          $('#slct_elemento').multiselect('rebuild');
          $('#divaccion').html('Elementos');          
          LimpiarMapa();
          $('#div_pro').html(
              '<div class="form-group"><div class="col-sm-5">'+
                '<label>Proyecto</label>'+
                '<input id="txt_proyecto" class="form-control" type="text" value="" name="txt_proyecto" placeholder="Ingrese Proyecto">'+
                '</div><div class="col-sm-7">'+
                '<label>&nbsp;</label>'+
                '<button id="btn_proyecto" type="button" class="form-control btn btn-success" onClick="GuardarProyecto()">Guardar Proyecto</button>'+
                '</div></div>');
          $('#div_capa').html('<div class="form-group">'+
                            '<div class="col-sm-5"><label>Capa</label>'+
                             '<input id="txt_capa" class="form-control" type="text" value="" name="txt_capa" placeholder="Ingrese Capa">'+
                             '</div><div class="col-sm-7"><label>&nbsp;</label>'+
                             '<button type="button" id="btn_capa" class="form-control btn btn-primary"  onClick="GenerarCapa()">Generar Capa</button>'+
                             '</div></div>');
          $('#div_cancel').hide();
}

function AgregarElemento(capaid) {
        LimpiarMapa();
        var pos=$("#divelemento").position()
        $('#sidebar').animate({scrollTop: pos.top}, 1000);
        $('html, body').animate({scrollTop: 50}, 1000);
        $('#divaccion').html('Agregar Elemento(s)');
        $('#txt_capaselect').val(capaid);
        ixproyectoselecc=$("#txt_proyectoselect").val();
        $('#div_capa').html('');
        $('#div_pro').html('<div class="col-sm-6">'+
                            ' <label>&nbsp;</label>'+
                            '<button id="btn_addelemento" type="button" class="form-control btn btn-primary" '+
                            'onClick="GenerarCapa()">Generar Capa</button></div>'+
            '<div class="form-group">'+
                '<div class="col-sm-6"><label>&nbsp;</label>'+
                '<button id="btn_proyecto_add" type="button" class="form-control btn btn-success" onClick="AgregarElementoCapa()">Actualizar Capa</button>'+
                '</div></div>');
        $('#slct_elemento').val("").trigger('changue');
        $('#slct_elemento').multiselect('rebuild');
        $('#div_cancel').show();
}

function AgregarElementoCapa() {
    var rs=confirm("Se agregara(n) a la Capa el(los) elemento(s) creado(s) ¿Desea Continuar?");
          if(rs){
            Proyectos.ModificarProyectoCapa();
          }
}