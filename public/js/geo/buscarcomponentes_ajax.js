
    var componentesObj = [];
    var componentes;
    var Componentes = {
        CargarComponentes: function (evento) {
            $.ajax({
                url: '../componente/cargar',
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        componentesObj = obj.datos;
                        $("#tb_componentes").html(obj.datos);
                    } else
                        $("#tb_componentes").html('');
                },
                error: function () {
                    $("#tb_componentes").html('');
                    eventoCargaRemover();
                }
            });
        },
        buscarComponentes: function () {
            var datos = $("#form_componentes").serialize();
            var token = document.querySelector('#token').getAttribute('value');
            $.ajax({
                url: "../componentes/buscar",
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: datos,
                headers: {
                    'csrftoken':token
                },
                beforeSend: function () {
                    eventoCargaMostrar();
                    $("#resultado").html('');
                },
                success: function (obj) {
                    eventoCargaRemover();
                    limpiarComponentes();

                    if (obj.rst == 1) {
                        marcarComponentes(obj.datos);
                        $("#resultado").html(obj.tabla);
                    } else if (obj.rst == 0){
                        Psi.mensaje('warning', obj.datos, 6000);
                        console.log(obj.datos);
                    } else {
                        $.each(obj.msj, function (index, datos) {
                            $("#error_" + index).attr("data-original-title", datos);
                            $('#error_' + index).css('display', '');
                        });
                    }
                },
                error: function () {
                    eventoCargaRemover(); 
                    console.log('error');
                }
            });
        },
        BuscarCalle: function () {
        var calle = $.trim($("#txt_calle_nombre").val());
        var numero = $.trim($("#txt_calle_numero").val());
        var token = document.querySelector('#token').getAttribute('value');
        if (calle == '')
        {
            Psi.mensaje('danger', 'Ingrese nombre de calle, Av., Jr.', 5000);
            return false;
        }
        
        $.ajax({
            url: '../componentes/buscarcalle',
            type: 'POST',
            cache: false,
            dataType: 'json',
            headers: {
                    'csrftoken':token
                },
            data: 'calle=' + calle
                    + '&numero=' + numero
                    + '&distrito=' + $("#calle_distrito").val(),
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(obj) {
                $(".overlay,.loading-img").remove();
                if (obj.rst == 1) {
                    if (obj.datos.length > 0) {
                        var x = ((parseFloat(obj.datos[0]['XA'])+parseFloat(obj.datos[0]['XB']))/2);
                        var y = ((parseFloat(obj.datos[0]['YA'])+parseFloat(obj.datos[0]['YB']))/2);
                        var content = document.getElementById("ubicacion"); 
                        var ubicacion = x +','+ y;
                        initMap(x,y);
                        content.value = (ubicacion);
                    } else Psi.mensaje('warning', 'No se encontro resultado', 6000);

                }
            },
            error: function() {
                $(".overlay,.loading-img").remove();

                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
        }
    };