
Vue.config.debug = true;
Vue.config.devtools = true;
var updMapProps = null;
var updMapTools = {};
var updatemap = null;
var updstreet = null;    
var updbounds = null;    
var boundsArray = [];
var marker = null;
var markertecnico=null;
var cityCircle_=null;
var cityCircle = null;
var markeref = null;
var markeft = null;
var markeftall = [];
var markers = [];
var bounds=null;
var infoWindow = null;
var geocoder=null;

var vm = new Vue({
    http: {
        root: '/publicmap',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },

    el: 'body',
    data: {
        datos : { 
        },
        codactu:'',
        solicitud:'',
        rst : 0,
        coordx:-77.030855,
        coordy:-12.046292,
        st: {},
        fftt:{}
    },

    methods: {
        ObtenerSt:function(){
            this.eventoCargaMostrar();
            var request = {
                codactu:this.codactu,
                solicitud:this.solicitud
            };
            this.$http.post('ordenstfftt', request, function(datos){
                if(datos.rst == 2) {
                    swal("Error", datos.msj, "error");
                } else {
                    this.$set('st', datos.datos.data);
                    this.$set('coordx', datos.datos.data.x);
                    this.$set('coordy', datos.datos.data.y);
                }
                this.eventoCargaRemover();
                this.createMap();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },
        createMap: function(){
            updMapProps = {
                center: new google.maps.LatLng(this.coordy, this.coordx),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            updatemap=doObjMap('update_maps', updMapProps, updMapTools);
            
            geocoder = new google.maps.Geocoder(); 
            infoWindow=new google.maps.InfoWindow();

            bounds = new google.maps.LatLngBounds();
            bounds.extend(new google.maps.LatLng(this.coordy, this.coordx));

            marker=new google.maps.Marker({
                    position: new google.maps.LatLng(this.coordy, this.coordx),
                    map: updatemap,
                    draggable:false
            });

            var infocontent = "<div class=\"infow\" style=\"overflow: auto; text-align: left\"\">" +
                                "<div class=\"detalle_actu\">" +
                                this.st.fh_agenda + " <br> " +
                                '<b>'+this.st.nombre_cliente_critico+"</b><br>" +
                                this.st.direccion_instalacion + "<br>" +
                                this.st.fftt + "<br>" +
                                this.st.tipoactu + "<br>" +
                                this.st.quiebre + "<br>" +
                                "</div>";
                        
            this.doInfoWindow(marker, infocontent);
            this.ObtenerLocalizacion();
        },
        ObtenerLocalizacion:function(){
            if(markertecnico!==null) markertecnico.setMap(null);
            if(cityCircle_!==null) cityCircle_.setMap(null);
            if(cityCircle!==null) cityCircle.setMap(null);

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    cityCircle_= new google.maps.Marker({
                        position: new google.maps.LatLng(pos.lat, pos.lng),
                        map: updatemap,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 9,
                            strokeColor: '#4285f4',
                            strokeOpacity: 0.2
                        },
                    });

                    cityCircle= new google.maps.Marker({
                        position: new google.maps.LatLng(pos.lat, pos.lng),
                        map: updatemap,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 5,
                            strokeColor: 'white',
                        },
                    });

                     markertecnico= new google.maps.Marker({
                        position: new google.maps.LatLng(pos.lat, pos.lng),
                        map: updatemap,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 4,
                            strokeColor: '#4285f4',
                            fillColor: '#4285f4', //color de relleno
                            fillOpacity:1// opacidad del relleno
                        },
                    });

                    markertecnico.addListener('click', function(event) {
                            vm.Opendistancia(pos);
                    });

                    bounds.extend(new google.maps.LatLng(pos.lat, pos.lng));
                    updatemap.fitBounds(bounds);

                    var flightPlanCoordinates = [
                        {lat: pos.lat, lng: pos.lng},
                        {lat: parseFloat(vm.coordy), lng: parseFloat(vm.coordx)}
                    ];
                    var flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    });

                    flightPath.setMap(updatemap);

                    vm.Opendistancia(pos);

                    $("#btn_validar").attr('disabled',false);

                }, function() {
                    vm.handleLocationError(true);
                    $("#btn_validar").attr('disabled',true);
                });
            } else {
                this.handleLocationError(false);
                $("#btn_validar").attr('disabled',true);
            }
        },
        Opendistancia:function(pos){
            if(infoWindow!==undefined)
                infoWindow.close(updatemap);                    
            var distancia=0;
            var distancia=vm.getmetros(pos.lat,pos.lng,parseFloat(vm.coordy),parseFloat(vm.coordx));
            infoWindow = new google.maps.InfoWindow; 
            var footerContent='<div id="btn_delete">'+
            '<a style="text-decoration:underline">Distancia: </a> <br>'+ distancia+' m. Aprox.'
            '</div>';
            infoWindow.setContent(footerContent);
            infoWindow.setPosition(pos);
            infoWindow.open(updatemap);
        },
        getmetros:function(lat1,lon1,lat2,lon2) {
         rad = function(x) {return x*Math.PI/180;}
            var R = 6378.137; //Radio de la tierra en km
            var dLat = rad( lat2 - lat1 );
            var dLong = rad( lon2 - lon1 );
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            return d.toFixed(3)*1000; //Retorna tres decimales
         },
        ValidarCoord:function(){
            var coordxtec=coordytec=null;
            if(markertecnico!==null) {
                coordytec=markertecnico.getPosition().lat();
                coordxtec=markertecnico.getPosition().lng();
            }
            if(coordxtec==null || coordytec==null) {
                swal("Error", "Use la opción", "error");
                swal({
                    title: "Es necesario saber su posición",
                    text: 'Utilice la opción "Mi Ubicación" ',
                    type: 'warning'
                });
                return;
            }

            this.eventoCargaMostrar();
            var request = {
                id: this.st.id,
                codactu: this.codactu,
                solicitud_id: this.st.solicitud_tecnica_id,
                coordx: this.st.x,
                coordy: this.st.y,
                coordxtec:coordxtec,
                coordytec:coordytec,
            };

            this.$http.post('validarcoordenadas', request, function(datos){
                if(datos.rst !== 1) {
                    swal("Alerta", datos.msj, "warning");
                } else {
                    swal("Exito", datos.msj, "success");
                }
                this.eventoCargaRemover();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },
        handleLocationError:function(browserHasGeolocation) {
            infoWindow.setPosition(updatemap.getCenter());
            infoWindow.setContent(browserHasGeolocation ?
                    'Error: No se puede obtener sus coordenadas, busque red estable o reinicie su equipo.' :
                    'Error: Tu Buscador no soporta Geolocalización.');
            infoWindow.open(updatemap);
        },
        doInfoWindow: function(element, content) {
            google.maps.event.addListener(element, 'click', (
                function(marker, infocontent, infowindow) {
                    return function() {
                        infowindow.setContent(infocontent);
                        infowindow.open(updatemap, marker);
                    };
                })(element, content, infowindow));
        },
        eventoCargaMostrar: function () {
            var loading = document.getElementsByClassName("loading-img");
            if ( loading.length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }
        },
        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        }
    },

    ready: function() {
        this.ObtenerSt();
        this.createMap();
    }
})