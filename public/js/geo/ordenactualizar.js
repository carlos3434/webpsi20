
Vue.config.debug = true;
Vue.config.devtools = true;
var updMapProps = null;
var updMapTools = {};
var updatemap = null;
var updstreet = null;    
var updbounds = null;    
var boundsArray = [];
var marker = null;
var markertecnico=null;
var cityCircle_=null;
var cityCircle = null;
var markeref = null;
var markeft = null;
var markeftall = [];
var markers = [];
var bounds=null;
var infoWindow = null;
var geocoder=null;

var vm = new Vue({
    http: {
        root: '/publicmap',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },

    el: 'body',
    /*components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },*/
    data: {
        datos : { 
        },
        codactu:'',
        solicitud:'',
        rst : 0,
        coordx:-77.030855,
        coordy:-12.046292,
        st:{},
        fftt:{}
    },

    methods: {
        ObtenerSt:function(){
            this.eventoCargaMostrar();
            var request = {
                codactu:this.codactu,
                solicitud:this.solicitud
            };
            this.$http.post('ordenstfftt', request, function(datos){
                if(datos.rst == 2) {
                    swal("Error", datos.msj, "error");
                    $("#btn_actualizar").attr('disabled',true);
                    $("#btn_validar").attr('disabled',true);
                    $("#btn_limpiar").attr('disabled',true);
                } else {
                    this.$set('st', datos.datos.data);
                    this.$set('fftt', datos.datos.fftt);
                    this.$set('coordx', datos.datos.data.x);
                    this.$set('coordy', datos.datos.data.y);
                    $("#btn_actualizar").attr('disabled',false);
                    $("#btn_validar").attr('disabled',false);
                    $("#btn_limpiar").attr('disabled',false);
                }
                this.eventoCargaRemover();
                this.createMap();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },
        createMap: function(){
            updMapProps = {
                center: new google.maps.LatLng(this.coordy, this.coordx),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            updatemap=doObjMap('update_maps', updMapProps, updMapTools);
            
            geocoder = new google.maps.Geocoder(); 
            infoWindow=new google.maps.InfoWindow();

            bounds = new google.maps.LatLngBounds();
            bounds.extend(new google.maps.LatLng(this.coordy, this.coordx));

            marker=new google.maps.Marker({
                    position: new google.maps.LatLng(this.coordy, this.coordx),
                    map: updatemap,
                    draggable:false
            });

            var infocontent = "<div class=\"infow\" style=\"overflow: auto; text-align: left\"\">" +
                                "<div class=\"detalle_actu\">" +
                                this.st.fh_agenda + " <br> " +
                                '<b>'+this.st.nombre_cliente_critico+"</b><br>" +
                                this.st.direccion_instalacion + "<br>" +
                                this.st.fftt + "<br>" +
                                this.st.tipoactu + "<br>" +
                                this.st.quiebre + "<br>" +
                                "</div>";

            this.doMapEvent();
            this.doSearchBox();
            this.doInfoWindow(marker, infocontent);
            updstreet = geoStreetView(this.coordy, this.coordx, 'update_sv');
            this.doStreetEvent();
        },
        SaveData:function(){
            var rs=confirm("Se guardará las nuevas coordenadas ¿Desea Continuar?");
            if(rs){
                this.eventoCargaMostrar();
                var request = {
                    id: this.st.id,
                    stid:this.st.solicitud_tecnica_id,
                    codactu: this.codactu,
                    solicitud: this.solicitud,
                    coordx: this.coordx,
                    coordy: this.coordy,
                };
                this.$http.post('guardarcoordenadas', request, function(datos){
                    if(datos.rst !== 1) {
                        swal("Error", datos.msj, "error");
                    } else {
                        this.$set('st', datos.datos.data);
                        swal("Exito", datos.msj, "success");
                    }
                    this.eventoCargaRemover();
                }).catch(function(errors) {
                    swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
                });
            }
        },
        doPositionChange:function(pos){
            updatemap.setCenter(pos);
            marker.setPosition(pos);
            marker.setMap(updatemap);
            vm.$set('coordx', pos.lng);
            vm.$set('coordy', pos.lat);
            updstreet = geoStreetView(pos.lat, pos.lng, 'update_sv');
            vm.doStreetEvent();
        },
        doSearchBox:function () {
            var input =(document.getElementById('pac-input'));
            
            updatemap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
            var searchBox = new google.maps.places.SearchBox((input));
    
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                var places = searchBox.getPlaces();
    
                if (places.length == 0) {
                    return;
                }
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
    
                // For each place, get the icon, place name, and location.
                markers = [];
                var bounds = new google.maps.LatLngBounds();
                var icon='https://maps.google.com/mapfiles/ms/icons/green-dot.png';

                for (var i = 0, place; place = places[i]; i++) {
                    // Create a markeref for each place.
                    markeref = new google.maps.Marker({
                        map: updatemap,
                        title: place.name,
                        position: place.geometry.location,
                        icon: icon
                    });        
                    markers.push(markeref);        
                    bounds.extend(place.geometry.location);
                }
    
                updatemap.fitBounds(bounds);
            });
    
            google.maps.event.addListener(updatemap, 'bounds_changed', function() {
                var bounds = updatemap.getBounds();
                searchBox.setBounds(bounds);
            });
        }, 
        doMapEvent: function() {
            google.maps.event.addListener(updatemap, 'dblclick', function(evento) {
                var pos = {
                        lat: evento.latLng.lat(),
                        lng: evento.latLng.lng()
                    };

                marker.setMap(null);
                vm.doPositionChange(pos);
            });
        },
        doStreetEvent: function() {
            google.maps.event.addListener(updstreet, 'position_changed', function() {
                vm.$set('coordx', updstreet.getPosition().lng());
                vm.$set('coordy', updstreet.getPosition().lat());
    
                marker.setMap(null);
                marker.setPosition(
                        new google.maps.LatLng(
                                updstreet.getPosition().lat(),
                                updstreet.getPosition().lng()
                                )
                        );
                marker.setMap(updatemap);
            });
        },
        LimpiarMapa: function(){
            $.each(markers, function(){
                this.setMap(null);
            });

            markers= [];
            
            var pos = {
                        lat: parseFloat(this.st.y),
                        lng: parseFloat(this.st.x)
                    };
            this.doPositionChange(pos);
            bounds = new google.maps.LatLngBounds();
        },
        handleLocationError:function(browserHasGeolocation) {
            infoWindow.setPosition(updatemap.getCenter());
            infoWindow.setContent(browserHasGeolocation ?
                    'Error: Falló el Servicio de Geolocalización.' :
                    'Error: Tu Buscador no soporta Geolocalización.');
            infoWindow.open(updatemap);
        },
        doInfoWindow: function(element, content) {
            google.maps.event.addListener(element, 'click', (
                function(marker, infocontent, infowindow) {
                    return function() {
                        infowindow.setContent(infocontent);
                        infowindow.open(updatemap, marker);
                    };
                })(element, content, infowindow));
        },
        eventoCargaMostrar: function () {
            var loading = document.getElementsByClassName("loading-img");
            if ( loading.length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }
        },
        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        }
    },

    ready: function() {
        this.ObtenerSt();
    }
});