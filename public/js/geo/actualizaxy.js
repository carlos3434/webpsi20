
Vue.config.debug = true;
Vue.config.devtools = true;
var updMapProps = null;
var updMapTools = {};
var updatemap = null;
var updstreet = null;    
var updbounds = null;    
var boundsArray = [];
var marker = null;
var markeref = null;
var markers = [];
  
var infoWindow = null;
var geocoder=null;

var vm = new Vue({
    http: {
        root: '/publicmap',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },

    el: 'body',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },

    data: {
        datos : { 
            tipo_via:'',
            nombre_via:'',
            numero_via:'',
            piso:'',
            interior:'',
            manzana:'',
            lote:'',
            tipo_urbanizacion:'',
            desc_urbanizacion:''
        },
        codactu:'',
        solicitud:'',
        rst : 0,
        coordx:-77.030855,
        coordy:-12.046292
    },

    methods: {
        createMap: function(){
            updMapProps = {
                center: new google.maps.LatLng(this.coordy, this.coordx),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            updatemap=doObjMap('update_maps', updMapProps, updMapTools);
        },
        ObtenerLocalizacion:function(){   
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    vm.doPositionChange(pos);
                    geoStreetView(
                        position.coords.latitude, 
                        position.coords.longitude, 
                        'update_sv'
                    );
                }, function() {
                    vm.handleLocationError(true);
                });
            } else {
                this.handleLocationError(false);
            }
        },
        ObtenerData:function(){
            this.eventoCargaMostrar();
            var request = {
                codactu:this.codactu,
                solicitud:this.solicitud
            };
            this.$http.post('obtenercoordenadas', request, function(datos){
                if(datos.rst == 2) {
                    swal("Error", datos.msj, "error");
                    $("#btn_actualizar").attr('disabled',true);
                    $("#btn_limpiar").attr('disabled',true);
                } else {
                    this.$set('datos', datos.datos);
                    this.$set('coordx', datos.datos.coordx_cliente);
                    this.$set('coordy', datos.datos.coordy_cliente);
                    $("#txt_refencia").val(datos.datos.referencia);
                    $("#btn_actualizar").attr('disabled',false);
                    $("#btn_limpiar").attr('disabled',false);
                }
                this.doSearchBox();
                this.doMapEvent();
                infoWindow=new google.maps.InfoWindow();
                geocoder = new google.maps.Geocoder(); 
                marker=new google.maps.Marker({
                        position: new google.maps.LatLng(this.coordy, this.coordx),
                        map: updatemap,
                        draggable:true
                        });
        
                marker.addListener('dragend',  function() {
                    var pos = {
                                lat: marker.getPosition().lat(),
                                lng: marker.getPosition().lng()
                            };
                    vm.doPositionChange(pos);
                });
                updstreet = geoStreetView(this.coordy, this.coordx, 'update_sv');
                this.doStreetEvent();
                this.eventoCargaRemover();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },
        SaveData:function(){
            this.eventoCargaMostrar();
            var request = {
                id: this.datos.id,
                codactu: this.codactu,
                coordx: this.coordx,
                coordy: this.coordy,
            };
            this.$http.post('guardarcoordenadas', request, function(datos){
                if(datos.rst == 2) {
                    swal("Error", datos.msj, "error");
                } else {
                    this.$set('datos', datos.datos);
                    swal("Exito", datos.msj, "success");
                }
                this.eventoCargaRemover();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },
        doPositionChange:function(pos){
            $("#txt_refencia").val('');
            updatemap.setCenter(pos);
            marker.setPosition(pos);
            marker.setMap(updatemap);
            vm.$set('coordx', pos.lng);
            vm.$set('coordy', pos.lat);
            updstreet = geoStreetView(pos.lat, pos.lng, 'update_sv');
            vm.doStreetEvent();
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var address=results[0]['formatted_address'];
                    $("#txt_refencia").val(address);
                }
            });
        },
        doSearchBox:function () {
            var input =(document.getElementById('pac-input'));
            
            updatemap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
            var searchBox = new google.maps.places.SearchBox((input));
    
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                var places = searchBox.getPlaces();
    
                if (places.length == 0) {
                    return;
                }
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
    
                // For each place, get the icon, place name, and location.
                markers = [];
                var bounds = new google.maps.LatLngBounds();
                var icon='https://maps.google.com/mapfiles/ms/icons/green-dot.png';

                if(places.length==1){
                    for (var i = 0, place; place = places[i]; i++) {
                        bounds.extend(place.geometry.location);

                        var pos = {
                            lat: place.geometry.location.lat(),
                            lng: place.geometry.location.lng()
                        };
    
                        vm.doPositionChange(pos);
                    }
                } else {
                    for (var i = 0, place; place = places[i]; i++) {
                        // Create a markeref for each place.
                        markeref = new google.maps.Marker({
                            map: updatemap,
                            title: place.name,
                            position: place.geometry.location,
                            icon: icon
                        });
        
                        markers.push(markeref);
        
                        bounds.extend(place.geometry.location);
                    }
                }
    
                updatemap.fitBounds(bounds);
            });
    
            google.maps.event.addListener(updatemap, 'bounds_changed', function() {
                var bounds = updatemap.getBounds();
                searchBox.setBounds(bounds);
            });
        }, 
        doMapEvent: function() {
            google.maps.event.addListener(updatemap, 'click', function(evento) {
                var pos = {
                        lat: evento.latLng.lat(),
                        lng: evento.latLng.lng()
                    };

                marker.setMap(null);
                vm.doPositionChange(pos);
            });
        },
        doStreetEvent: function() {
            google.maps.event.addListener(updstreet, 'position_changed', function() {
                vm.$set('coordx', updstreet.getPosition().lng());
                vm.$set('coordy', updstreet.getPosition().lat());
                updatemap.setCenter(
                        new google.maps.LatLng(
                                updstreet.getPosition().lat(),
                                updstreet.getPosition().lng()
                                )
                        );
    
                marker.setMap(null);
                marker.setPosition(
                        new google.maps.LatLng(
                                updstreet.getPosition().lat(),
                                updstreet.getPosition().lng()
                                )
                        );
                marker.setMap(updatemap);
            });
        },
        LimpiarMapa: function(){
            $.each(markers, function(){
                this.setMap(null);
            });
            
            markers = [];
            
            var pos = {
                        lat: parseFloat(this.datos.coordy_cliente),
                        lng: parseFloat(this.datos.coordx_cliente)
                    };
            this.doPositionChange(pos);
            //$("#txt_refencia").val(this.datos.referencia);
        },
        handleLocationError:function(browserHasGeolocation) {
            infoWindow.setPosition(updatemap.getCenter());
            infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(updatemap);
        },
        eventoCargaMostrar: function () {
            var loading = document.getElementsByClassName("loading-img");
            if ( loading.length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }
        },
        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        }
    },

    ready: function() {
        $("#txt_refencia").val('');
        this.ObtenerData();
        this.createMap();
        this.ObtenerLocalizacion();
    }
})