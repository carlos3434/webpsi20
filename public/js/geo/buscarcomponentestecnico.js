    $(document).ready(function () {

        initAutocomplete();

        $("input[type=checkbox]").each(function() {
            activarCombo($(this));
        });

        $('#allcheckbox').on('click', function (event) {
            if ($('#allcheckbox').prop('checked')) {
                $("input[type=checkbox]").prop("checked", true);
                $("input[type=checkbox]").each(function() {
                    activarCombo($(this));
                });
            } else {
                $("input[type=checkbox]").removeAttr("checked");
                $("input[type=checkbox]").each(function() {
                    activarCombo($(this));
                });
            }
        });

        $('#form_componentes').submit(function(e) {
            e.preventDefault();
            var datos = $("#form_componentes").serialize();
        });

        $(document).delegate("#tb_componente input[type=checkbox]","change", function(){
            activarCombo($(this));
        });

        $('#search').on('click', function() {
            // Obtenemos la dirección y la asignamos a una variable
            var address = $('#address').val();
            if (address.length == 0) {
                Psi.mensaje('warning', 'Ingresa dirección', 3000);
            }
            var request = {
                address: address,
                componentRestrictions: {
                    country: 'PE'
                }
            }
            // Creamos el Objeto Geocoder
            var geocoder = new google.maps.Geocoder();
            // Hacemos la petición indicando la dirección e invocamos la función
            // geocodeResult enviando todo el resultado obtenido
            geocoder.geocode(request, geocodeResult);
        });


    });

    //Busca la direccion ingresa en Google Maps
    function geocodeResult(results, status) {
        // Verificamos el estatus
        if (status == 'OK') {
            var x = results[0].geometry.location.lng();
            var y = results[0].geometry.location.lat();

            initMap(x,y);

            var content = document.getElementById("ubicacion"); 
            var ubicacion = x +','+ y;
            content.value = (ubicacion);
        } else {
            alert("Geocoding no tuvo éxito debido a: " + status);
        }
    }

    var autocomplete;
    function initAutocomplete() {
        var options = {
          types: ['geocode'],
          componentRestrictions: {country: "pe"}
         };
        autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('address')),
          options);
    }


    var map;
    var markers = new Array();
    var infowindow;
    var infocontent;

    function initMap(x,y) {
        var myLatlng = new google.maps.LatLng(y,x);
        map = new google.maps.Map(document.getElementById('mymap'), {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        infowindow = new google.maps.InfoWindow({
                content: ""
        });

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
       //marker.setMap(map);

        google.maps.event.addListener(map, 'click', function(evento) {
            var content = document.getElementById("ubicacion"); 
            var latitud = evento.latLng.lat();
            var longitud = evento.latLng.lng();

            var ubicacion = longitud +','+ latitud;
            content.value = (ubicacion);

            if (marker.visible) {
                marker.setMap(null);
                marker=[];
            }
            var markerOptions = {
                map: map
            };
            var new_marker_position = new google.maps.LatLng(latitud, longitud);
            var marker1 = new google.maps.Marker(markerOptions);
            marker1.setPosition(new_marker_position);
            marker1.setPosition(new_marker_position);
            marker = marker1;
            var marker2 = '';
        });

    }


    function activarCombo(check) {
        td = check.parent();
        tr = td.parent();
        checkbox = check;
        selects = tr.find("select");
        selects.each(function() {
            if (checkbox.is(":checked")) {
                $(this).removeAttr("disabled");
            } else {
                $(this).prop("disabled", true);
            }
        })
    }

    limpiarComponentes = function () {
        setMapOnAll(null);
    };

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    Buscar = function () {
        Componentes.buscarComponentes();
        $('html,body').animate({
            scrollTop: $("#mymap").offset().top
        }, 1000);
    };

    marcarComponentes = function (obj) {

        var array_x = new Array();
        var array_y = new Array();
        var bounds = new google.maps.LatLngBounds();
        for (var componente in obj) {

            datos = obj[componente];

            var componenteIcon = "http://psiweb.ddns.net:7020/img/icons/cable.png";
            var info = 'Sin descripcion';
            
            for (i = 0; i < datos.length; i++) { 
             
                switch(componente) {
                    case 'amplificador':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/amplificador.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].nodo+'<br>'+datos[i].troba+'<br>'+datos[i].amplificador+"</div>";
                        break;
                    case 'antenacircle':
                        info ="<div>"+datos[i].departamento+'<br>'+datos[i].provincia+'<br>'+datos[i].distrito+'<br>'+datos[i].ubigeo+"</div>";
                        break;
                    case 'armariopoligono':
                    case 'armariopunto':
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].mdf+'<br>'+datos[i].armario+"</div>";
                        break;
                    case 'mdf':
                    case 'mdfpunto':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/mdf.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].mdf+'<br>'+datos[i].direccion+"</div>";
                        break;
                    case 'nodopoligono':
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].nodo+"</div>";
                        break;
                    case 'tap':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/tap.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].nodo+'<br>'+datos[i].troba+'<br>'+datos[i].amplificador+'<br>'+datos[i].tap+'<br>'+datos[i].direccion+"</div>";
                        break;
                    case 'terminalf':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/terminal.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].mdf+'<br>'+datos[i].armario+'<br>'+datos[i].terminalf+"</div>";
                        break;
                    case 'terminald':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/terminal.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].mdf+'<br>'+datos[i].cable+'<br>'+datos[i].terminald+"</div>";
                        break;
                    case 'troba':
                        componenteIcon = "http://psiweb.ddns.net:7020/img/icons/troba.png";
                        info ="<div>"+datos[i].zonal+'<br>'+datos[i].nodo+'<br>'+datos[i].troba+"</div>";
                        break;
                }
                
                var x = datos[i].coord_x;
                var y = datos[i].coord_y;

                var isMobile = {
                    Android: function() {
                        return navigator.userAgent.match(/Android/i);
                    },
                    BlackBerry: function() {
                        return navigator.userAgent.match(/BlackBerry/i);
                    },
                    iOS: function() {
                        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                    },
                    Opera: function() {
                        return navigator.userAgent.match(/Opera Mini/i);
                    },
                    Windows: function() {
                        return navigator.userAgent.match(/IEMobile/i);
                    },
                    any: function() {
                        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                    }
                };
                if( isMobile.any() ) {
                    info += '<br><a href="waze://?ll='+y+','+x+'">Waze</a>';
                }
                array_x.push(x);
                array_y.push(y);

               // console.log('componente:'+componente + ', x:'+x+', y:'+y);
                bounds.extend(new google.maps.LatLng(y, x));

                marker2 = new google.maps.Marker({
                    position: new google.maps.LatLng(y, x),
                    map: map,
                    icon: componenteIcon
                });

                markers.push(marker2);

                var titulo = "<div class=\"infow\" style=\"overflow: auto; text-align: left\"\"><b>"
                                    + componente.toUpperCase() + "</b></div>";
                var infocont = titulo + info;
                google.maps.event.addListener(marker2, 'click', (
                    function(marker, infocontent, infowindow) {
                        return function() {
                            infowindow.setContent(infocontent);
                            infowindow.open(map, marker);
                        };
                })(marker2, infocont, infowindow));
            }//foreach

        }// fon in

        var content = document.getElementById("ubicacion"); 
        //console.log(content);
        var ubicacion = content.value.split(",");

        //agrego la ubicacion actual para considerarlo en fitBounds
        array_x.push(ubicacion[0]);
        array_y.push(ubicacion[1]);

        var min_x = Math.min.apply(null, array_x);
        var max_x = Math.max.apply(null, array_x);
        var min_y = Math.min.apply(null, array_y);
        var max_y = Math.max.apply(null, array_y);

        //console.log('mayor x: '+max_x+', menor: '+min_x);
        //console.log('mayor y : '+max_y+', menor: '+min_y);
        var mitad_x= ((parseFloat(max_x)) + (parseFloat(min_x)))/2;
        var mitad_y= ((parseFloat(max_y)) + (parseFloat(min_y)))/2;

        //console.log('max x:'+max_x+', max y:'+max_y);
        bounds.extend(new google.maps.LatLng(ubicacion[1], ubicacion[0]));
        map.fitBounds(bounds);
    };
   function getBuscarCalle() {
        Componentes.BuscarCalle();
    }
eventoCargaMostrar = function () {
    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
}
eventoCargaRemover = function () {
    $(".overlay,.loading-img").remove();
}