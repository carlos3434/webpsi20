
Vue.config.debug = true;
Vue.config.devtools = true;

var vm = new Vue({
    http: {
        root: '/cablemodemofsc',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },

    el: 'body',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },

    data: {
        datos : { 
        },
        resultados : {
        },
        detallemac:'',
        antiguamac:'',
        nuevamac:'',
        estadoresultado:'No OK',
        cliente:'',
        nomcliente:'',
        codactu:'',
        solicitud:'',
        rst : 0,
        showModalEdicion: false,
    },

    methods: {
        ListarMac: function() {
            this.eventoCargaMostrar();
            var request = {
                cliente: this.cliente,
                codactu: this.codactu
            };
            this.$http.post('obtener', request, function(datos){
                this.$set('datos', datos.datos);
                this.$set('rst', datos.rst);
                this.$set('nomcliente', datos.cliente);
                this.eventoCargaRemover();
                if(datos.rst == 2) {
                    swal("Error", datos.msj, "error");
                }
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "danger");
            });
        },

        PruebaModem: function(mac){
            this.eventoCargaMostrar();
            this.$set('detallemac', '');
            this.$set('resultados', '');
            this.$set('estadoresultado','No OK');
            $("#div_detalle").css('display','none');
            var request = {
                mac: mac,
                codactu: this.codactu,
                solicitud: this.solicitud
            };
            this.$http.post('pruebamodem', request, function(datos){
                this.$set('rst', datos.rst);
                this.$set('detallemac', mac);
                if (datos.rst == 1) {
                    //swal("Exito", datos.msj, "success");
                    this.$set('estadoresultado','OK');
                } else {
                    swal("Error", datos.msj, "error");
                }
                if(datos.rst != 3) {
                    this.$set('resultados', datos.resultado);
                    $("#div_detalle").css('display','');
                }
                this.eventoCargaRemover();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "error");
                this.eventoCargaRemover();
            });
        },
        modalModem: function(mac){
            this.$set('antiguamac', mac);
            $("#CablemodemModal").css('display','block');
            $("#CablemodemModal").attr('aria-hidden',false);
            $("#CablemodemModal").addClass('in');
        }, 
        funcionClose: function(){
            $("#CablemodemModal").css('display','none');
            $("#CablemodemModal").attr('aria-hidden',true);
            $("#CablemodemModal").removeClass('in');
            this.$set('antiguamac', '');
            this.$set('nuevamac', '');
        },
        modificarMac: function(){
            this.eventoCargaMostrar();
            this.$set('detallemac', '');
            this.$set('resultados', '');
            this.$set('estadoresultado','No OK');
            $("#div_detalle").css('display','none');
            var request = {
                antiguamac: this.antiguamac,
                nuevamac: this.nuevamac,
                cliente: this.cliente,
                codactu: this.codactu,
                solicitud: this.solicitud
            };
            this.$http.post('modificarmodem', request, function(datos){
                this.$set('rst', datos.rst);
                this.$set('detallemac', this.nuevamac);
                if (datos.rst == 1) {
                    if (datos.pruebamodem.rst == 1) {
                        this.$set('estadoresultado','OK');
                        this.$set('resultados', datos.pruebamodem.resultado);
                    } 
                    if (datos.pruebamodem.rst == 3) {
                        swal("Error", datos.pruebamodem.msj, "error");
                    }
                    if(datos.pruebamodem.rst != 3) {
                        $("#div_detalle").css('display','');
                    }
                    this.funcionClose();
                } else {
                    swal("Error", datos.msj, "error");
                }
                this.eventoCargaRemover();
            }).catch(function(errors) {
                swal("Error", 'Ocurrió una interrupción en el proceso.', "error");
                this.eventoCargaRemover();
            }); 
        },  
        eventoCargaMostrar: function () {
            var loading = document.getElementsByClassName("loading-img");
            if ( loading.length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }
        },

        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        }
    },

    ready: function() {
        this.ListarMac();
    }
})