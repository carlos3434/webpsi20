var instance = axios.create({
	baseURL: '/errores'
});

var vm = new Vue ({
	el: "#app",
	data: {
		datos: [],
		heads: [
			{
				filterHead: 'u.usuario',
				nameHead: 'Usuario',
			}, {
				filterHead: 'url',
				nameHead: 'Url',
			}, {
				filterHead: 'file',
				nameHead: 'Archivo',
			}, {
				filterHead: 'code',
				nameHead: 'Codigo',
			}, {
				filterHead: 'date',
				nameHead: 'Fecha',
			},
		],
		items: [],
		fields: {
			usuario: {
				label: 'Usuario',
				sortable: true
			},
			url: {
				label: 'URL',
				sortable: true
			},
			file: {
				label: 'Archivo',
				sortable: true
			},
			code: {
				label: 'Código',
				sortable: true
			},
			date: {
				label: 'Fecha',
				sortable: true
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPage: 1,
		perPage: 10,
		filter: null,
		totalRows: 0,
		filterSelected: 'u.usuario',
		object: {},
	},
	mounted: function () {
		this.listar();
		$('#modal').on('hide.bs.modal', function (event) {
            vm.cleanForm();
        });
	},
	methods: {
		listar: function () {
			eventoCargaMostrar();
			var startRegistro = 0;
	        if (this.currentPage != undefined && this.currentPage != 1) {
	          startRegistro = (this.currentPage - 1) * this.perPage;
	        }
			var request = {
	        	startRegistro : startRegistro,
	        	perPage : this.perPage,
	            filtro : this.filter,
	            filterSelected : this.filterSelected
	        };
			instance.post('/list', request)
				.then((response) => {
					this.datos = response.data.trama.datos;
					this.totalRows = response.data.trama.total;
					eventoCargaRemover();
				})
				.catch(function (error) {
					eventoCargaRemover();
				})
		},

		detalle: function (obj) {
	    	this.object = obj;
	    },

	    cleanForm: function () {
    		this.object = {};
	    },
	}
});