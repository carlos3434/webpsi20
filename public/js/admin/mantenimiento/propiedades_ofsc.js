$(document).ready( function() {

        PropiedadesOfsc.CargarPropiedadesOfsc();

        $('#propiedadesOfscModal').on('show.bs.modal', function (event) {
            var modal = $(this);
            var button = $(event.relatedTarget); 
            var titulo = button.data('titulo'); 
            modal.find('.modal-title').text(titulo+' Propiedad');

            if ( titulo == 'Nueva' ) {
                modal.find('.modal-footer .btn-primary').text('Guardar');
                modal.find('.modal-footer .btn-primary').attr('onClick','Agregar();');

                $('#form_propiedadesOfsc #txt_etiqueta').val( "" );

                $('#slct_tipo_tecnologia').multiselect('select', -1);
                $('#slct_tipo_tecnologia').multiselect('rebuild');

                $('#slct_actividad_id').multiselect('select', -1);
                $('#slct_actividad_id').multiselect('rebuild');

                $('#slct_tipo').multiselect('select', "M");
                $('#slct_tipo').multiselect('rebuild');

                $('#slct_estado').multiselect('select', 1);
                $('#slct_estado').multiselect('rebuild');
            } else {
                var propiedad_index = button.data('index');

                modal.find('.modal-footer .btn-primary').text('Actualizar');
                modal.find('.modal-footer .btn-primary').attr('onClick','Editar();');

                $('#form_propiedadesOfsc #txt_etiqueta').val( propiedades_ofsc_obj[propiedad_index].label );

                $('#slct_tipo_tecnologia').multiselect('select', propiedades_ofsc_obj[propiedad_index].tipo_tecnologia);
                $('#slct_tipo_tecnologia').multiselect('rebuild');

                $('#slct_actividad_id').multiselect('select', propiedades_ofsc_obj[propiedad_index].actividad_id);
                $('#slct_actividad_id').multiselect('rebuild');

                $('#slct_tipo').multiselect('select', propiedades_ofsc_obj[propiedad_index].tipo);
                $('#slct_tipo').multiselect('rebuild');

                $('#slct_estado').multiselect('select', propiedades_ofsc_obj[propiedad_index].estado);
                $('#slct_estado').multiselect('rebuild');

                $("#form_propiedadesOfsc").append("<input type='hidden' id='propiedad_id' value='"+propiedades_ofsc_obj[propiedad_index].id+"' name='id'>");
            }

            $('#form_propiedadesOfsc #txt_etiqueta').focus();
            
        });

});

function HTMLCargarPropiedadesOfsc( datos ){

    var tipos = {
        M: "Motivo",
        S: "Sub Motivo",
        T: "Tipo"
    };

    var tipos_tecnologia = {
        null: "-",
        1: "CATV",
        2: "Speedy",
        3: "Basica",
        4: "DTH"
    };

    var actividades = {
        null: "-",
        1: "Averia",
        2: "Provisión"
    };

    var html = "";
    $('#t_propiedades_ofsc').dataTable().fnDestroy();

    //PRIVILEGIO AGREGAR
    if ( agregarG == 0 ) { 
        $('#nuevo').remove();  
    }  

    $.each( datos, function( index, data ){

        //PRIVILEGIO DESACTIVAR
        if ( eliminarG == 0 ) {

            if ( data.estado == 1 ) {
                estadohtml = '<span class="">Activo</span>';
            } else {
                estadohtml = '<span class="">Inactivo</span>';
            }

        } else {

            if ( data.estado == 1 ) {
                estadohtml = '<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
            } else {
                estadohtml = '<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
            }

        }   

        html += "<tr>" +
                "<td style='display: none;'>" + data.id + "</td>" +
                "<td>" + data.label + "</td>" +
                "<td>" + tipos_tecnologia[(data.tipo_tecnologia)] + "</td>" +
                "<td>" + actividades[(data.actividad_id)] + "</td>" +
                "<td>" + tipos[(data.tipo)] + "</td>" +
                "<td>" + estadohtml + "</td>";

        //PRIVILEGIO EDITAR
        if(editarG == 1) { 
            html+='<td><a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#propiedadesOfscModal" data-index="'+index+'" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a></td>'; 
        } else {
            html+='<td class="editarG"></td>';
        }

        html += "</tr>";
    });

    $("#tb_propiedades_ofsc").html(html);
    if ( editarG == 0 ) $('.editarG').hide();
    $("#t_propiedades_ofsc").dataTable({
        "order": [[ 0, "desc" ]]
    }); 
};

function activar(id){
    PropiedadesOfsc.CambiarEstadoPropiedadesOfsc(id, 1);
};

function desactivar(id){
    PropiedadesOfsc.CambiarEstadoPropiedadesOfsc(id, 0);
};

function Agregar(){
    if ( validaPropiedadesOfsc() ) {
        PropiedadesOfsc.AgregarEditarPropiedad(0);
    }
};

function Editar(){
    if( validaPropiedadesOfsc() ) {
        PropiedadesOfsc.AgregarEditarPropiedad(1);
    }
};

function validaPropiedadesOfsc(){
    $('#form_propiedadesOfsc [data-toggle="tooltip"]').css("display","none");
    var v = true;

    if ( $("#txt_etiqueta").val() == "" ) {
        v = false;
    }

    if (!v) {
        $('#form_propiedadesOfsc [data-toggle="tooltip"]').css("display","inline-flex");
    }

    return v;
};
