$(document).ready(function(){
    opciones.listar();
    opciones.listarCtr();
});

$(document).on('click', '#btnNuevoOpc', function(e){
   $('#ModalOpcion').modal();
   limpiar_formOpc();
   $('#btnGuardarOpc').show();
   $('#btnModificarOpc').hide();
   slctGlobal.listarSlct('opciones','slct_tipoOpc','simple',null,{'input':'tipoOpc','idusuario':'0'});
});

$(document).on('click', '#btnNuevoCrit', function(e){
    $('#ModalCrit').modal();
    limpiar_formCrit();
    $('#btnGuardarCrit').show();
    $('#btnModificarCrit').hide();
});

$(document).on('click', '#btnGuardarOpc', function(e){
    if(validaInput_Opc()){
        opciones.guardar_opciones();
    }
});

$(document).on('click', '#btnModificarOpc', function(e){
    if(validaInput_Opc()){
        opciones.modificar_opciones();
    }
});

$(document).on('click', '#btnGuardarCrt', function(e){
    if(validaInput_Crt()){
        opciones.guardar_valor();
    }
});

$(document).on('click', '#btnModificarCrt', function(e){
  if(validaInput_Crt()){
    opciones.modificar_valor();
  }
});

$(document).on('click', '#btnCancelarCrt', function(e){
  limpiar_formCrt();
  $('#btnModificarCrt').hide();
  $('#btnGuardarCrt').fadeIn('slow');
});

$(document).on('change','#slct_criterioCrt',function(e) {
    e.preventDefault();
    opciones.cargar_campos_criterios();
    console.log($('#txt_campoCrt').val());
});

$(document).on('click', '#btnGuardarCrit', function(e){
    if(validaInput_Crit()){
        opciones.guardar_criterios();
    }
});

$(document).on('click', '#btnModificarCrit', function(e){
    if(validaInput_Crit()){
        opciones.modificar_criterios();
    }
});

validaInput_Opc=function(){
    var a=[];
    a[0]=validar("txt","nombreOpc","");
    a[1]=validar("slct","tipoOpc","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};

validaInput_Crit=function(){
    var a=[];
    a[0]=validar("txt","nombreCrit","");
    a[1]=validar("txt","campoCrit","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};

validaInput_Crt=function(){
    var a=[];
    a[0]=validar("slct","criterioCrt","");
    a[1]=validar("txt","valorCrt","");
    var rpta=true;

    for(i=0;i<a.length;i++){
        if(a[i]===false){
            rpta=false;
            break;
        }
    }
    return rpta;
};

validar=function(inicial,id,v_default){
  if( $.trim($("#"+inicial+"_"+id).val())==v_default || $.trim($("#"+inicial+"_"+id).val())==0 || 
      $.trim($("#"+inicial+"_"+id).val())==(-1)){
    $('#error_'+id).fadeIn('slow');
    return false;
  }else{$('#error_'+id).fadeOut('slow');return true;}
};

limpiar_formOpc=function(){
  $('#slct_tipoOpc').multiselect('destroy');
  $('#opcionId').val('');
  $('#txt_nombreOpc').val('');
  $('#error_nombreOpc').hide();
  $('#error_tipoOpc').hide();
}

limpiar_formCrt=function(){
  slctGlobal.listarSlct('opciones','slct_criterioCrt','simple','0',{'input':'criterio'});
  $('#Id_Crt_Opc').val('');
  $('#criterioId_Crt').val('');
  $('#txt_nombreCrt').val('');
  $('#txt_campoCrt').val('');
  $('#txt_valorCrt').val('');
  $('#slct_estadoCrt').val('-1');
  $('#error_criterioCrt').hide();
  $('#error_nombreCrt').hide();
  $('#error_campoCrt').hide();
  $('#error_valorCrt').hide();
  $('#error_estadoCrt').hide();
}

limpiar_formCrit=function(){
  $('#txt_nombreCrit').val('');
  $('#txt_campoCrit').val('');
  $('#error_nombreCrit').hide();
  $('#error_campoCrit').hide();
}

$('#myTab a').click(function (e) {
  e.preventDefault();
  if(($(this).attr('href') == "#tb_opcion")) {
    //$("#t_modulos").dataTable().fnDestroy();
    $('#tb_criterio').hide();
    $('#tb_opcion').fadeIn('slow');
  }
  if(($(this).attr('href') == "#tb_criterio")) {
    $('#tb_opcion').hide();
    $('#tb_criterio').fadeIn('slow');
  }
});