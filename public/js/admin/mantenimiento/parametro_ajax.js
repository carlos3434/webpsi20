
var parametros_id, parametrosObj;
var criterio=[];
    var Parametros={
        CargaCriterio:function(masiva=0){
            if(($("#combos").html()).indexOf("div")!=-1) {
                var rs=confirm("Se perderán los criterios ya seleccionados ¿Está de acuerdo?");
                if(!rs){
                    $('#form_parametros #slct_proveniencia_modal').val($("#tipo").val());
                    $("#form_parametros #slct_proveniencia_modal").multiselect('rebuild');
                    $('#form_parametros #slct_tipo_modal').val($("#tipoparam").val());
                    return;
                }
            }
            $("#tipo").val($('#form_parametros #slct_proveniencia_modal').val());
            $("#tipoparam").val($('#form_parametros #slct_tipo_modal').val());
            $("#combos").html('');
            $.ajax({
                url         : 'criterios/cargar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {estado:1, tipo:$("#form_parametros #slct_tipo_modal").val(), proveniencia: $("#form_parametros #slct_proveniencia_modal").val(), masiva:masiva},
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        HTMLCargarCriterio(obj.datos);
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        CambiarEstado:function(id,estado){
            $.ajax({
                url         : 'parametro/save',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {estado:estado, id:id}, 
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if (obj.rst==1) {
                        Psi.mensaje('success', obj.msj, 6000);
                        Parametros.CargaListado();
                    } 
                    
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        GuardarFiltro:function(){
            var datos=$("#form_parametros").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
            datos+='&Parametro_Criterio=1';
            $.ajax({
                url         : 'parametro/save',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        $('#parametrosModal .modal-footer [data-dismiss="modal"]').click();
                        Psi.mensaje('success', obj.msj, 6000);
                        Parametros.CargaListado();
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        CargaListado:function(){
            var datos=$("#form_busqueda").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
            datos+="&perfilId="+perfilId;
            $.ajax({
                url         : 'parametro/listar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : datos,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        HTMLCargarListado(obj.datos);
                        parametrosObj = obj.datos;
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
        CargaFiltro:function(data,proveniencia){
            $.ajax({
                url         : 'parametro/listar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : data,
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        var fallo=0;
                        $.each(obj.datos,function(index,data){
                            //$("#txt_idfiltro").val(data.id);
                            //$("#txt_nombre").val(data.nombre);
                            if(data.criterios_id!=null && data.criterio_activo==1) {
                                var datoid=data.detalle.split(",");
                                criterio[data.criterios_id]=datoid;
                            } 
                        });
                        //$('#slct_proveniencia_modal').val(proveniencia).trigger('change');
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
    };
