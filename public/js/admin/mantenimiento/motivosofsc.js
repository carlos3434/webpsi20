
$(document).ready(function(){
	//Exportar a excel Motivos OFSC
	$("#ExportExcel").click(function(){
	    $("#form_exportar").append("<input type='hidden' value='1' name='excel'>");
	    $("#form_exportar").submit();
	});
});

$(document).ready(function(){
	//Exportar a excel SubMotivos OFSC
	$("#ExportExcel1").click(function(){
	    $("#form_exportar1").append("<input type='hidden' value='1' name='excel'>");
	    $("#form_exportar1").submit();
	});
});

var instance = axios.create({
	baseURL: '/motivosofsc'
});

var instanceSubmotivo = axios.create({
	baseURL: '/submotivoofsc'
});

var instanceSelect = axios.create({
	baseURL: '/listado_lego'
});

var vm = new Vue({
	el: '#app',
	components: {
        Multiselect: window.VueMultiselect.default
    },
	data: {
		// **********************************************
		selectsOptionsObj: {
			tipoTratamiento: [
				{ id: '1', nombre: 'AUTOMATICO' },
				{ id: '0', nombre: 'SIGUE FLUJO' }
			],
			estadoOfsc: [
				{ id: '4', nombre: 'No Realizada' },
				{ id: '6', nombre: 'Completada' }
			],
			tipoOfsc: [
				{ id: 'T', nombre: 'T - tecnico' },
				{ id: 'C', nombre: 'C - comercial' },
				{ id: 'S', nombre: 'S - soporte de campo' }
			],
			tipoLegado: [
				{ id: '1', nombre: 'CMS' },
				{ id: '2', nombre: 'GESTEL' }
			],
			actividad: []
		},
		selectsValueObj: {
			tipoTratamiento: [],
			estadoOfsc: [],
			tipoOfsc: [],
			tipoLegado: [],
			actividad: []
		},
	    // **********************************************

		datosMotivo: [],
		objMotivo: {
			estado: 1
		},
		actividad: {},
		type: 'Registrar',
		datosSubmotivo: [],
		objSubmotivo: {
			estado: 1
		},
		typeSubmotivo: 'Registrar',
		
		// ***********************************************
		fieldsMotivo: {
			codigo_ofsc: {
				label: 'Codigo OFSC',
				sortable: true
			},
			codigo_legado: {
				label: 'Codigo LEGADO',
				sortable: true
			},
			descripcion: {
				label: 'Descripcion',
				sortable: true
			},
			tipo_tratamiento_legado_descripcion: {
				label: "Tipo Tratamiento",
				sortable: true
			},
			nombre_ofsc: {
				label: 'Estado OFSC',
				sortable: true
			},
			estado_ofsc: {
				label: 'Tipo OFSC',
				sortable: true
			},
			tipo_legado: {
				label: 'Tipo Legado',
				sortable: true
			},
			actividad: {
				label: 'Actividad',
				sortable: true
			},
			tipo_tecnologia: {
				label: 'Tipo Tecnologia',
				sortable: true
			},
			estado: {
				label: 'Estado'
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPagemotivo: 1,
		perPagemotivo: 10,
		filtermotivo: null,

		// ***********************************************
		items: [],
		fields: {
			descripcion_motivo: {
				label: 'Descripcion Motivo',
				sortable: true
			},
			codigo_ofsc: {
				label: 'Codigo OFSC',
				sortable: true
			},
			codigo_legado: {
				label: 'Codigo LEGADO',
				sortable: true
			},
			descripcion: {
				label: 'Descripcion',
				sortable: true
			},
			estado: {
				label: 'Estado'
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPage: 1,
		perPage: 10,
		filter: null
	},

	mounted: function () {
		this.list();
		var selects = ["actividad"];
        var data = [{}, {}, {}];
        this.listarSelects(selects, data);
        $('#motivosModal, #submotivosModal').on('hide.bs.modal', function (event) {
            vm.cleanForm();
        });

        $('#myTab a').click(function (e) {
	        e.preventDefault();
	        $(this).tab('show');

            if(($(this).attr('href') == "#tb_submotivos")) {
                vm.listSubmotivos();
            }
	    });
	},

	methods: {
		// **************************************************
		limitText: function (count) {
			return `y ${count} mas`
		},

		getSelectedTipoTratamiento: function (obj) {
            this.selectsValueObj.tipoTratamiento = [];
            for (var key in obj) {
                this.selectsValueObj.tipoTratamiento.push(obj[key].id);
            }
        },

        getSelectedEstadoOfsc: function (obj) {
            this.selectsValueObj.estadoOfsc = [];
            for (var key in obj) {
                this.selectsValueObj.estadoOfsc.push(obj[key].id);
            }
        },

        getSelectedTipoOfsc: function (obj) {
            this.selectsValueObj.tipoOfsc = [];
            for (var key in obj) {
                this.selectsValueObj.tipoOfsc.push(obj[key].id);
            }
        },

        getSelectedTipoLegado: function (obj) {
            this.selectsValueObj.tipoLegado = [];
            for (var key in obj) {
                this.selectsValueObj.tipoLegado.push(obj[key].id);
            }
        },

        getSelectedActividad: function (obj) {
            this.selectsValueObj.actividad = [];
            for (var key in obj) {
                this.selectsValueObj.actividad.push(obj[key].id);
            }
        },
		// **************************************************

		list: function () {
			instance.post('/list')
				.then((response) => {
					this.datosMotivo = response.data.datos;
				})
		},

		find: function (obj) {
			this.type = "Actualizar";
			this.objMotivo = obj;
		},

		action: function () {
			switch (this.type) {
				case "Actualizar": 
					this.update();
					break;
				case "Registrar": 
					this.insert();
					break;
			}
			$("#motivosModal").modal('hide');
		},

		insert: function () {
			instance.post('/insert', this.objMotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		update: function () {
			instance.post('/update', this.objMotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		setEstado: function (obj) {
			this.objMotivo = obj;
			this.objMotivo.estado = (this.objMotivo.estado == 1) ? 0 : 1;
			instance.post('/update', this.objMotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
						this.cleanForm();
					}
				})
		},

		listarSelects: function (selects, data) {
			var request = {
				selects: JSON.stringify(selects),
				datos: JSON.stringify(data),
			};

            instanceSelect.post('/selects', request)
	            .then((response) => {
	                var obj = response.data;
	                for(i = 0; i <obj.data.length; i++){
	                    obj.datos = obj.data[i];
	                    eval("this.selectsOptionsObj."+selects[i]+" = obj.datos");
	                }
	            })
        },

        cleanForm: function () {
			this.type = "Registrar";
			this.objMotivo = {estado: 1};
			this.objSubmotivo = {estado: 1};
		},

		listSubmotivos: function () {
			instanceSubmotivo.post('/list')
				.then((response) => {
					this.datosSubmotivo = response.data.datos;
				})
		},

		findSubmotivo: function (obj) {
			this.typeSubmotivo = "Actualizar";
			this.objSubmotivo = obj;
		},

		actionSubmotivo: function () {
			switch (this.typeSubmotivo) {
				case "Actualizar": 
					this.updateSubmotivo();
					break;
				case "Registrar": 
					this.insertSubmotivo();
					break;
			}
			$("#submotivosModal").modal('hide');
		},

		insertSubmotivo: function () {
			instanceSubmotivo.post('/insert', this.objSubmotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.listSubmotivos();
					}
				})
		},

		updateSubmotivo: function () {
			instanceSubmotivo.post('/update', this.objSubmotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.listSubmotivos();
					}
				})
		},

		setEstadoSubmotivo: function (obj) {
			this.objSubmotivo = obj;
			this.objSubmotivo.estado = (this.objSubmotivo.estado == 1) ? 0 : 1;
			instanceSubmotivo.post('/update', this.objSubmotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.listSubmotivos();
						this.cleanForm();
					}
				})
		},

		buscarMotivo: function () {
			instance.post('/list', this.selectsValueObj)
				.then((response) => {
					this.datosMotivo = response.data.datos;
				})
		}
	}
})