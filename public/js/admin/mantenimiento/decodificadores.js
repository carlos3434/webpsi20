var instance = axios.create({
	baseURL: '/decodificadorequivalencia'
});

var vm = new Vue({
	el: '#app',
	components: {
        Multiselect: window.VueMultiselect.default
    },
	data: {
		datos: [],
		objDecodificadorEquivalencia: {
			estado: 1
		},
		type: 'Registrar',
		items: [],
		fields: {
			index: {
				label: 'Indice',
				sortable: true
			},
			descripcion: {
				label: 'Descripcion',
				sortable: true
			},
			codmat: {
				label: 'Código Material',
				sortable: true
			},
			codmat_equivalencia: {
				label: 'Código Material Equivalencia <br><i>(8 dígitos)</i>',
				sortable: true
			},
			estado: {
				label: 'Estado'
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPage: 1,
		perPage: 10,
		filter: null,
	},

	mounted: function () {
		this.list();
        $('#decodificadorModal').on('hide.bs.modal', function (event) {
            vm.cleanForm();
        });
	},

	methods: {
		list: function () {
			instance.post('/list')
				.then((response) => {
					this.datos = response.data.datos;
				})
		},

		find: function (obj) {
			this.type = "Actualizar";
			this.objDecodificadorEquivalencia = obj;
		},

		action: function () {
			switch (this.type) {
				case "Actualizar": 
					this.update();
					break;
				case "Registrar": 
					this.insert();
					break;
			}
			$("#decodificadorModal").modal('hide');
		},

		insert: function () {
			instance.post('/insert', this.objDecodificadorEquivalencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		update: function () {
			instance.post('/update', this.objDecodificadorEquivalencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		setEstado: function (obj) {
			this.objDecodificadorEquivalencia = obj;
			this.objDecodificadorEquivalencia.estado = (this.objDecodificadorEquivalencia.estado == 1) ? 0 : 1;
			instance.post('/update', this.objDecodificadorEquivalencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
						this.cleanForm();
					}
				})
		},

        cleanForm: function () {
			this.type = "Registrar";
			this.objDecodificadorEquivalencia = {estado: 1};
		},

	}
})