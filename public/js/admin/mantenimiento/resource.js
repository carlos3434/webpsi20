$( document ).ready(function() {
    $('#dragTree').jstree({
        'core' : {
            'animation' : 0,
            check_callback : function (op, node, par, pos, more) {
                // par -> datos del padre
                // node -> datos de la rama
                var validacionEmpresa = par.id.search("empresa");
                var validacionTecnico = par.id.search("tecnico");

                if (op === "move_node" && more && more.core) {
                    // validacion padre old
                    var empresaOrigen = node.id.search("empresa");
                    var tecnicoOrigen = node.id.search("tecnico");
                    var bucketOrigen = node.id.search("bucket");

                    // validacion padre new
                    var empresaDestino = par.id.search("empresa");
                    var tecnicoDestino = par.id.search("tecnico");
                    var bucketDestino = par.id.search("bucket");
                    // 57054088
                    switch (true) {
                        case (node.parent == "#"): // Movimientos de Empresa
                            swal("Advertencia", "El movimiento solo es de Tecnico a Bucket", "warning");
                            return false;
                            break;
                        case (bucketOrigen == 0) && (empresaDestino == 0): // Mover: Bucket -> Empresa
                        case (bucketOrigen == 0) && (bucketDestino == 0): // Mover: Bucket -> Bucket
                        case (bucketOrigen == 0) && (tecnicoDestino == 0): // Mover: Bucket -> Tecnico
                            swal("Advertencia", "El movimiento solo es de Tecnico a Bucket", "warning");
                            return false;
                            break;
                        case (tecnicoOrigen == 0) && (empresaDestino == 0): // Mover: Tecnico -> Empresa
                        case (tecnicoOrigen == 0) && (tecnicoDestino == 0): // Mover: Tecnico -> Empresa
                            swal("Advertencia", "El movimiento solo es de Tecnico a Bucket", "warning");
                            return false;
                            break;
                        default:
                            console.log("PARRRR DESTINO");
                            console.log("padre del bucket: "+par.parent);
                            console.log(par.id);
                            console.log("NODEEE ORIGEN");
                            console.log(node.id);
                            if (par.id == node.parent) {
                                swal("Advertencia", "El Tecnico se movio al mismo Bucket", "warning");
                                return false;
                            } else {
                                var idtecnico = node.id;
                                var idbucketDestino = par.id;
                                var idempresaDestino = par.parent;
                                Resource.update(idtecnico, idbucketDestino, idempresaDestino);
                                return false;
                                break;
                            }
                    }

                }
                return true;
            },
            'themes' : {
                'responsive': false,
                'stripes': true
            },
            'data' : {
                'url': 'tecnico/arbol',
                'data': function (node) {
                    return node;
                }
            }
        },
        "plugins" : [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ]
    });

    $('#dragTree').on("create_node.jstree", function () {
        console.log("create");
    });

    $('#dragTree').on("rename_node.jstree", function () {
        console.log("editar");
    });

    $('#dragTree').on("delete_node.jstree", function () {
        console.log("borrando");
    });

    $("#dragTree").jstree({
        "plugins" : [ "search" ]
    });

    var to = false;
    $('#buscar').keyup(function () {
        if(to) { clearTimeout(to); }
        to = setTimeout(function () {
          var v = $('#buscar').val();
          $('#dragTree').jstree(true).search(v);
        }, 250);
    });
});

crear = function () {
    var ref = $('#dragTree').jstree(true),
        sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    sel = ref.create_node(sel, {"type":"file"});
    if(sel) {
        ref.edit(sel);
    }
};
rename = function () {
    var ref = $('#dragTree').jstree(true),
        sel = ref.get_selected();
    if(!sel.length) { return false; }
    sel = sel[0];
    ref.edit(sel);
};
eliminar = function () {
    var ref = $('#dragTree').jstree(true),
        sel = ref.get_selected();
    if(!sel.length) { return false; }
    ref.delete_node(sel);
};