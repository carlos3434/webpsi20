this.onmessage = function(e) {  
  try {
    var xhr = new XMLHttpRequest();
    var url = e.data.url;
    
    xhr.open("POST", url ,true);
    //xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
              postMessage(xhr.responseText);
            }else{
              postMessage('fallo');
            }
        }
    };
    
    xhr.send();
  } catch (e) {
    function ManipulatorException(message) {
      this.name = "ManipulationException";
      this.message = message;
    };
    throw new InverterException('Image manipulation error');
      postMessage(undefined);
  }
}