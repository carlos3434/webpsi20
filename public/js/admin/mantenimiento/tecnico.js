var instance = axios.create({
    baseURL: '/tecnico'
});

var instanceSms = axios.create({
    baseURL: '/bandejalegado'
});

Vue.component('celulas', {
    template: '#celulas-template',
    props: ['celula'],
    methods: {
        removeGrilla: function(index) {
            this.$emit('remove', index);
        }
    }
});

var vm = new Vue({
    el: '#app',
    data: {
        datos: [],
        objTecnico: {
            estado: 1,
            cargo_tecnico: 1,
            ninguno: 0,
            ningunoNombre: "INACTIVO",
            imeiOld: '',
            imsiOld: ''
        },
        objCelula: [],
        type: 'Registrar',
        mensaje : '',
        items: [],
        fields: {
            ape_paterno: {
                label: 'Apellido Paterno',
                sortable: true
            },
            ape_materno: {
                label: 'Apellido Materno',
                sortable: true
            },
            nombres: {
                label: 'Nombres',
                sortable: true
            },
            dni: {
                label: 'DNI',
                sortable: true
            },
            carnet: {
                label: 'Carnet',
                sortable: true
            },
            carnet_tmp: {
                label: 'Carnet Criticos',
                sortable: true
            },
            empresa: {
                label: 'Empresa',
                sortable: true
            },
            bucket: {
                label: 'Bucket',
                sortable: true
            },
            estado: {
                label: 'Estado'
            },
            actions: {
                label: '¿Editar?'
            }
        },
        currentPage: 1,
        perPage: 10,
        filter: null,
    },
    /*components: {
        'celulas': {
            template: '#celulas-template',
            props: ['celula'],
            methods: {
                removeCelula: function (index) {
                    vm.objCelula.splice(index, 1);
                }
            }
        }
    },*/
    mounted: function () {
        this.list();
        $('#tecnicoModal').on('show.bs.modal', function (event) {
            $('#form_tecnicos .empresa').multiselect('select', vm.objTecnico.empresa_id);
            $('#form_tecnicos .bucket').multiselect('select', vm.objTecnico.bucket_id);
            $('#form_tecnicos .empresa, .bucket, .celula').multiselect('rebuild');
            filtroSlct("form_tecnicos .empresa","simple",'E','.celula');
            $('#form_tecnicos .fecha_inicio_toa').val(vm.objTecnico.fecha_inicio_toa);
        });

        $('#tecnicoModal').on('hide.bs.modal', function (event) {
            $('#form_tecnicos .empresa, .bucket, .celula').multiselect('select', "");
            $('#form_tecnicos .empresa, .bucket, .celula').multiselect('rebuild');
            $('#form_tecnicos .fecha_inicio_toa').val("");
            vm.cleanForm();
        });
    },

    methods: {
        listar: function () {
            var data = {
                from: 'vue',
                empresa: $("#form_general .empresa").val(),
                bucket: $("#form_general .bucket").val(),
            }
            instance.post('/cargar', data)
                .then((response) => {
                    this.datos = response.data.datos;
                })
        },

        list: function () {
            instance.post('/cargar', {from: 'vue'})
                .then((response) => {
                    this.datos = response.data.datos;
                })
        },

        find: function (obj) {
            this.type = "Actualizar";
            var copyObj = Object.assign({}, obj);
            this.objTecnico = copyObj;
            this.objTecnico.fecha_inicio_toa = copyObj.fecha_inicio_toa;
            this.objTecnico.imeiOld = copyObj.imei;
            this.objTecnico.imsiOld = copyObj.imsi;
            this.objTecnico.marcaOld = copyObj.marca;
            this.objTecnico.modeloOld = copyObj.modelo;
            this.objTecnico.versionOld = copyObj.version;
            this.objTecnico.ningunoNombre = copyObj.ninguno == 0 ? "INACTIVO" : "ACTIVO";
            this.getCelulas();
        },

        action: function () {
            switch (this.type) {
                case "Actualizar": 
                    this.update();
                    break;
                case "Registrar": 
                    this.insert();
                    break;
            }
        },

        insert: function () {
            this.objTecnico.celulas = this.objCelula;
            instance.post('/crear', this.objTecnico)
                .then((response) => {
                    if (response.data.rst == 1) {
                        swal("Exito", response.data.msj, "success");
                        this.list();
                        this.cleanForm();
                        $("#tecnicoModal").modal('hide');
                    } else {
                        var msj = "";
                        $.each(response.data.msj, function (index, datos) {
                            msj += datos + "\n";
                        });
                        swal("Error", "Error al registrar Tecnico: "+msj, "error");
                    }
                })
        },

        update: function () {
            this.objTecnico.celulas = this.objCelula;
            instance.post('/editar', this.objTecnico)
                .then((response) => {
                    if (response.data.rst == 1) {
                        swal("Exito", response.data.msj, "success");
                        this.list();
                        this.cleanForm();
                        $("#tecnicoModal").modal('hide');
                    } else {
                        var msj = "";
                        $.each(response.data.msj, function (index, datos) {
                            msj += datos + "\n";
                        });
                        swal("Error", "Error al registrar Tecnico: "+msj, "error");
                    }
                })
        },

        setEstado: function (obj) {
            this.objTecnico = obj;
            this.objTecnico.estado = (this.objTecnico.estado == 1) ? 0 : 1;
            instance.post('/cambiarestado', this.objTecnico)
                .then((response) => {
                    if (response.data.rst == 1) {
                        this.cleanForm();
                    } else {
                        this.objTecnico.estado = (this.objTecnico.estado == 1) ? 0 : 1;
                        swal("Error", "Error al actualizar Tecnico", "error");
                    }
                })
        },

        getCelulas: function () {
            instance.post('/cargarcelulas', {tecnico_id: this.objTecnico.id})
                .then((response) => {
                    this.objCelula = response.data.datos;
                })
        },

        addCelula: function () {
            var validacion = 0;
            this.objCelula.forEach(function (value, key) {
                if (value.id == vm.objTecnico.celula_id) {
                    validacion++
                }
            });

            if (validacion > 0) {
                swal("Advertencia", "Ya existe Registro en la Grilla", "warning");
            } else {
                this.objCelula.push({
                    id: this.objTecnico.celula_id,
                    nombre: this.objTecnico.celula,
                    officetrack: 0
                });
            }
        },

        setNinguno: function () {
            this.objTecnico.ninguno = this.objTecnico.ninguno == 0 ? 1 : 0;
            this.objTecnico.ningunoNombre = this.objTecnico.ninguno == 0 ? "INACTIVO" : "ACTIVO";
        },

        removeCelula: function (index) {
            this.objCelula.splice(index, 1);
        },

        cleanForm: function () {
            this.type = "Registrar";
            this.objTecnico = {estado: 1, ninguno: 0, ningunoNombre: "INACTIVO", cargo_tecnico: 1};
            this.objCelula = [];
        },

        enviarSms: function () {
            swal({
                title: vm.objTecnico.nombres + ' ' + vm.objTecnico.ape_paterno + ' ' + vm.objTecnico.ape_materno,
                text: "Ingresar mensaje de exto",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Ingresar mensaje"
            }, function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("Necesitas ingresar el mensaje de texto");
                    return false
                }

                var datos = {
                    _token                  : document.querySelector('#token').getAttribute('value'),
                    numeros_telefonicos     : vm.objTecnico.celular,
                    contenido_sms           : inputValue + ","
                }

                instanceSms.post('/enviarsms', datos).then((response) => {})
                swal("Exito", "Mensaje enviado: " + inputValue, "success");
            });
        }
    }
})