var idperfil = 0;
table = $("table#t_perfil_opciones").DataTable();
contadorfilas = 0;
$(document).ready(function(){
	slctGlobalHtml('slct_actividad_id', 'simple');
	slctGlobalHtml('slct_tipo_legado', 'simple');
	$("#btn_guardar_perfil").click(Perfil.guardar);
    $(".btn-agregar").click(agregaropcion);
    $(document).delegate(".btn-cambiar-estado", "click", function(e){
        var id = $(this).data("id");
        var estado = $(this).data("estado");
        cambiarestado(id, estado);
    });
    $(document).delegate(".btn-eliminar-perfil", "click", function(e){
        var id = $(this).data("id");
        eliminar(id);
    });
    $(document).delegate(".btn-eliminar-opcion", "click", function(e){
        var id = $(this).data("id");
        var perfilid = $(this).data("perfilid");
        $('#slct_opcion option[value='+id+']').removeAttr("disabled");
        $("#slct_opcion").multiselect("refresh");
        table.row($(this).parents('tr')).remove().draw();
    });
    $("#t_perfil_opciones").DataTable().destroy();
});

listar  = function () {
	$("#t_perfil").DataTable().destroy();
	Perfil.listar();
};

eliminar = function(id) {
	swal({
        title: '¿Desea eliminar el usuario?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Eliminar'
    }).then(function () {
        Perfil.eliminar(id);
    });
};
agregaropcion = function() {
    var opcionseleccionado = $("#slct_opcion").val();

    var htmlappend = "";
    if (opcionseleccionado!="" && opcionseleccionado!=null) {
        table.row.add( [
            opcionseleccionado,
            listOpciones[opcionseleccionado].nombre+"<input type='hidden' name='txt_opcion_perfil[]' value='"+opcionseleccionado+"' />",
            "<a class='btn-eliminar-opcion btn btn-danger' data-id='"+opcionseleccionado+"' data-perfilid='"+objperfil.id+"'><i class='fa fa-trash'></i></a>",
        ] ).draw( false );
        contadorfilas++;
        $('#slct_opcion option[value='+opcionseleccionado+']').prop('disabled', true);
    }
    $("#slct_opcion").multiselect("refresh");
    return false;
}
detalle = function() {
    opciones = objperfil.opciones;
    for (var i in opciones) {
        var opcion = opciones[i];
        table.row.add( [
            opcion.opcion.id,
            opcion.opcion.nombre+"<input type='hidden' name='txt_opcion_perfil[]' value='"+opcion.opcion.id+"' />",
            "<a class='btn-eliminar-opcion btn btn-danger' data-id='"+opcion.opcion.id+"' data-perfilid='"+objperfil.id+"'><i class='fa fa-trash'></i></a>",
        ] ).draw( false );
        contadorfilas++;
        $('#slct_opcion option[value='+opcion.opcion.id+']').prop('disabled', true);
    }
    $("#slct_opcion").multiselect("refresh");
    table = $("table#t_perfil_opciones").DataTable({order: [[ 0, "desc" ]], paging: true,
            lengthChange: true});
    $("#txt_nombre").val(objperfil.nombre);
};
cambiarestado = function(idperfil, estado) {
    swal({
        title: '¿Desea cambiar el estado del perfil?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Cambiar'
    }).then(function () {
        Perfil.cambiarestado(idperfil, estado);
    });
};

pintarSelectOpcion = function() {
    var html ="";
    html+="<option value=''>.::Seleccione::.</option>";
    $.each(listOpciones, function(index, data) {
        html+="<option value='"+data.id+"'>"+data.nombre+"</option>";
    });
    $("#slct_opcion").html(html);
    slctGlobalHtml("slct_opcion", "simple");
}; 

listar();
Perfil.listOpciones();

var enforceModalFocusFn;
$('#PerfilModal').on('show.bs.modal', function (event) {
    enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var nombreperfil = button.data("nombre");
    if (id > 0) {
    	idperfil = id;
        $("#span_nombre_perfil").text(nombreperfil);
    	Perfil.get(id);
    } else {
        //
        table = $("table#t_perfil_opciones").DataTable();
        table.clear().draw();
    }
});
$('#PerfilModal').on('hide.bs.modal', function (event) {
    $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    var modal = $(this);
    modal.find('.modal-body input[type=text]').val('');
    modal.find('.modal-body input[type=checkbox]').removeAttr("checked");
    modal.find('.modal-body textarea').val('');
    $("#span_nombre_perfil").text("");
    idperfil = 0;
    objperfil = {id :0};
    contadorfilas = 0;
    $("table#t_perfil_opciones").DataTable().clear().draw();
    $("table#t_perfil_opciones").DataTable().destroy();
    $('#slct_opcion option').removeAttr("disabled");
    $("#slct_opcion option:selected").removeAttr('selected').prop('selected', false);
    $("#slct_opcion").multiselect('refresh');
});