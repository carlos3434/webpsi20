axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
listMotivosOfsc = [];
listSubMotivosOfsc = [];
listMotivosOfscChecked = [];
listSubMotivosOfscChecked = [];
var asignacionseleccionado = [];
var AsignacionMotivo = {
	listar : function () {
	    return $("#t_usuarios").DataTable({
	        "processing": true,
	        "serverSide": true,
	        "stateSave": true,
	        "stateLoadCallback": function (settings) {
	        },
	        "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
	        },
	        ajax: function(data, callback, settings) {
	            AsignacionMotivo.ajaxlistado(data,callback);
	        },
	        "columns":[
	            { data: 'apellido', name:'apellido'},
	            { data: 'nombre', name:'nombre'},
	            { data: 'usuario', name:'usuario'},
	            { data: 'area', name:'area_id'},
	            { data: 'perfil', name:'perfil_id'},
	            { data: 'empresa', name:'empresa_id'},
	            {data : function( row, type, val, meta) {
	                var htmlButtons = "";
	                htmlButtons = "<a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' title='Detalle Motivos Usuario' ";
	                htmlButtons+=" data-target='#asignacionMotivoModal' data-id='"+row.id+"' style='margin-right:5px;'";
	                htmlButtons+=" data-nombreusuario='"+row.nombre+" "+row.apellido+"'>";
	                htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
	                htmlButtons+="<a class='btn btn-xs btn-danger btn-limpiar-asignacion' data-id='"+row.id+"'><i class='fa fa-eraser'></i></a>";
	                return htmlButtons;
	            }, name: "botones"},
	        ],
	        paging: true,
	        lengthChange: true,
	        searching: true,
	        ordering: true,
	        order: [[ 1, "asc" ]],
	        info: true,
	        autoWidth: true,
	        language: config
	    });
	},
	getMotivosOfsc : function () {
		form="&_token="+document.querySelector('#token').getAttribute('value');
		axios.post('asignacionmotivousuario/motivosofsc', form).then(response => {
            var data = response.data;
            for (var i in data) {
            	if (data !=null && data!="null") {
            		listMotivosOfsc[data[i].id] = data[i];
	            	listMotivosOfscChecked[data[i].id] = {id : data[i].id , seleccionado: false};
            	}
            	
            }
        }).catch(e => {
            console.log(e);
        }).then(() => {
            eventoCargaRemover();
        });
	},
	getSubMotivosOfsc : function () {
		form="&_token="+document.querySelector('#token').getAttribute('value');
		axios.post('asignacionmotivousuario/submotivosofsc', form).then(response => {
            var data = response.data;
            for (var i in data) {
            	if (data!="null" && data!=null) {
            		if (typeof listSubMotivosOfsc[data[i].motivo_ofsc_id] == "undefined" || 
	            		typeof listSubMotivosOfsc[data[i].motivo_ofsc_id] == undefined) {
	            		listSubMotivosOfsc[data[i].motivo_ofsc_id] = [];
	            	}
	            	listSubMotivosOfsc[data[i].motivo_ofsc_id][data[i].id] = data[i];
	            	listSubMotivosOfscChecked[data[i].id] = {id : data[i].id , seleccionado: false, motivo_ofsc_id : data[i].motivo_ofsc_id};
            	}
            }
        }).catch(e => {
            console.log(e);
        }).then(() => {
            eventoCargaRemover();
        });
	},
	ajaxlistado : function(request,callback){
        form="&_token="+document.querySelector('#token').getAttribute('value');
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+='&search='+request.search.value;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        eventoCargaMostrar();
        axios.post('asignacionmotivousuario/listar', form).then(response => {
            callback(response.data);
        }).catch(e => {
            console.log(e);
        }).then(() => {
            eventoCargaRemover();
        });
    },
    get : function(idusuario) {
    	eventoCargaMostrar();
    	form="&id="+idusuario;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
	    axios.get('asignacionmotivousuario/editar?'+form).then(response => {
	        var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		    }
		    if (data.rst == "1") {
		    	asignacionseleccionado = data.obj;
		    	detalle(data.obj);
		    }
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    guardar : function() {
    	var form ="&_token="+document.querySelector('#token').getAttribute('value');
    	var motivosofscdata = [];
    	var submotivosofscdata = [];
    	for (var i in listMotivosOfscChecked) {
    		if (listMotivosOfscChecked[i]!=null) {
    			if (listMotivosOfscChecked[i].seleccionado) {
    				motivosofscdata.push(listMotivosOfscChecked[i]);
    			}
    		}
    	}

    	for (var i in listSubMotivosOfscChecked) {
    		if (listSubMotivosOfscChecked[i]!=null) {
    			if (listSubMotivosOfscChecked[i].seleccionado) {
    				submotivosofscdata.push(listSubMotivosOfscChecked[i]);
    			}
    		}
    	}
    	//var form = $("#form_motivos_usuario").serialize().split("txt_").join("").split("slct_").join("").split("chck_").join("");
    	form+="&id="+idusuario;
    	form+="&motivosseleccionados="+JSON.stringify(motivosofscdata);
    	form+="&submotivosseleccionados="+JSON.stringify(submotivosofscdata);
    	axios.post('asignacionmotivousuario/guardar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		    	Psi.sweetAlertConfirm(data.msj);
		    	$("#asignacionMotivoModal").modal("hide");
		    	cambiarestadolistChecked(false);
		    	//listMotivosOfscChecked = [];
		    	listar();
		   	}
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    limpiarasignacion : function(idtematico) {
    	var form="&id="+idtematico;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.post('asignacionmotivousuario/eliminar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	}
		   	if (data.rst == "1") {
		    	Psi.sweetAlertConfirm(data.msj);
		    	listar();
		   	}
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    }
}