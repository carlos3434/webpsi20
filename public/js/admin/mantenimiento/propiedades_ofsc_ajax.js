var propiedades_ofsc_obj;

var PropiedadesOfsc = {

	CargarPropiedadesOfsc: function() {
        $.ajax({
            url         : 'propiedades_ofsc/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                var html = "";
                var estadohtml = "";

                if ( obj.rst == 1 ) {
                    HTMLCargarPropiedadesOfsc( obj.datos );
                    propiedades_ofsc_obj = obj.datos;
                }

                eventoCargaRemover();
            },
            error: function(){
                eventoCargaRemover();
                Psi.mensaje('danger', '', 6000);
            }
        });
    },
    CambiarEstadoPropiedadesOfsc: function( id, AD ) {
        var datos = 'id=' + id + '&estado=' + AD;
        $.ajax({
            url         : 'propiedades_ofsc/cambiarestado',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {

                eventoCargaRemover();

                if ( obj.rst == 1 ) {

                    $('#t_propiedades_ofsc').dataTable().fnDestroy();
                    PropiedadesOfsc.CargarPropiedadesOfsc();
                    Psi.mensaje('success', obj.msj, 6000);

                } else {
                    $.each(obj.msj,function( index, datos ) {
                        $("#error_"+index).attr( "data-original-title", datos );
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                eventoCargaRemover();
                Psi.mensaje('danger', '', 6000);
            }
        });
    },
    AgregarEditarPropiedad:function(AE){
        var datos=$("#form_propiedadesOfsc").serialize().split("txt_").join("").split("slct_").join("");
        var accion="propiedades_ofsc/crear";

        if( AE == 1 ){
            accion = "propiedades_ofsc/editar";
        }

        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if ( obj.rst == 1 ) {

                    $('#t_propiedades_ofsc').dataTable().fnDestroy();

                    PropiedadesOfsc.CargarPropiedadesOfsc();

                    $('#propiedadesOfscModal .modal-footer [data-dismiss="modal"]').click();
                    Psi.mensaje('success', obj.msj, 6000);

                } else {
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                eventoCargaRemover();
                Psi.mensaje('danger', '', 6000);
            }
        });
    },
}