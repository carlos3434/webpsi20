var opciones = {
    listar: function () {
        var datos="";
        var targets=0;
        //$('#tb_cor_tecnicos').dataTable().fnDestroy();
        $('#tb_opciones').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,                
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                opciones.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.nombre;}},
                {data : function( row, type, val, meta) {return row.tipo;}},
                {data : function( row, type, val, meta) {return row.fecha_creacion;}},
                {data : function( row, type, val, meta) {
                    htmlButtons='';
                    if(row.estado=='Activo'){
                        htmlButtons ="<a class='btn btn-success btn-sm btn-detalle' data-toggle='modal' id='"+row.id+"' estado=0 tipo='opcion' onclick='opciones.cambiar_estados(this)'>Activo</a>";
                    }else{htmlButtons ="<a class='btn btn-danger btn-sm btn-detalle' data-toggle='modal' id='"+row.id+"' estado=1 tipo='opcion' onclick='opciones.cambiar_estados(this)'>Inactivo</a>";}
                    return htmlButtons;}},
                {data : function( row, type, val, meta) {
                    htmlButtons ="<div><a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' id='btnEditarOpc' title='Editar Opcion' idOpc='"+row.id+"' nombreOpc='"+row.nombre+"' tipoOpc='"+row.idtipo+"' tipo onclick='opciones.abrir_modalOpcion(this)'>";
                    htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>  ";
                    htmlButtons+="<a class='btn btn-danger btn-xs btn-detalle' data-toggle='modal' title='Asignar Criterios' idOpc='"+row.id+"' nombreOpc='"+row.nombre+"' onclick='opciones.abrir_modalCriterios(this)'>";
                    htmlButtons+="<i class='fa fa-location-arrow fa-lg'></i></a></div>";
                    return htmlButtons;
                }}
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        }); 
    },
    listarCtr: function () {
        var datos="";
        var targets=0;
        //$('#tb_cor_tecnicos').dataTable().fnDestroy();
        $('#tb_ctr').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,                
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                opciones.http_ajax_ctr(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.nombre;}},
                {data : function( row, type, val, meta) {return row.campo;}},
                {data : function( row, type, val, meta) {return row.fecha_creacion;}},
                {data : function( row, type, val, meta) {
                    htmlButtons='';
                    if(row.estado=='Activo'){
                        htmlButtons ="<a class='btn btn-success btn-sm btn-detalle' data-toggle='modal' id='"+row.id+"' estado=0 tipo='criterio' onclick='opciones.cambiar_estados(this)'>Activo</a>";
                    }else{htmlButtons ="<a class='btn btn-danger btn-sm btn-detalle' data-toggle='modal' id='"+row.id+"' estado=1 tipo='criterio' onclick='opciones.cambiar_estados(this)'>Inactivo</a>";}
                    return htmlButtons;}},
                {data : function( row, type, val, meta) {
                    htmlButtons ="<div><a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' id='btnEditarOpc' title='Editar Criterio' idCrit='"+row.id+"' nombreCrit='"+row.nombre+"' campoCrit='"+row.campo+"' onclick='opciones.abrir_modalCrit(this)'>";
                    htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>  ";
                    return htmlButtons;
                }}
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        }); 
    },
    listarCriterios : function(){
        var datos=$("#form_criterios").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/cargarcriterios',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
               $(".overlay,.loading-img").remove();
                var table =$('#tb_criterios').DataTable({
                    lengthChange: false,
                    lengthMenu: [ [5, 10], [5, 10] ],
                    pageLength: 10,
                    paging: true,
                    ordering: true,
                    info: true,
                    data: (data.criterios),
                    searching: false,
                    deferRender:true,
                    scrollCollapse: true,
                    scroller:false,
                    destroy:true,
                    columns: [
                        { "data": "criterio"},
                        { "data": "campo"},
                        { "data": "valor"},
                        {"data": null,"sortable": false,
                            render: function ( row, type, full, meta ) {
                                htmlButtons='';
                                htmlButtons ="<div><a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' id='btnEditarOpc' title='Editar Opcion' idCrtOpc='"+row.idCrtOpc+"' idCrt='"+row.id+"' nombreCrt='"+row.criterio+"' campoCrt='"+row.campo+"' valorCrt='"+row.valor+"' estadoCrt='"+row.idestado+"' tipo onclick='opciones.mostrar_criterios(this)'>";
                                htmlButtons+="<i class='fa fa-edit fa-xs'></i></a></div>";
                            return htmlButtons;
                            }
                        }      
                    ],
                    "language": {
                        "info": "Ver Pagina _PAGE_ de _PAGES_",
                        "sEmptyTable":"Ningún criterio disponible para esta opcion",
                        "sLoadingRecords": "Cargando...",
                        "paginate": {"previous": "Anterior","next":"Siguiente"},
                        "lengthMenu": "Ver _MENU_ "
                    }
                });

                console.log(data.criterios);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    http_ajax: function(request,callback){
        var contador = 0;
        //var rangoFechas =$("#rangoFechas").val();
        //var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        //form+="&fecha="+rangoFechas;

        eventoCargaMostrar();
        axios.post('opciones/cargar',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    http_ajax_ctr: function(request,callback){
        var contador = 0;
        //var rangoFechas =$("#rangoFechas").val();
        //var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar2').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        //form+="&fecha="+rangoFechas;

        eventoCargaMostrar();
        axios.post('opciones/cargarcriterio',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    cambiar_estados : function (element){
        var id=$(element).attr("id");
        var estado=$(element).attr("estado");
        var tipo=$(element).attr("tipo");
        var parameter={'id':id,'estado':estado,'tipo':tipo};
        $.ajax({
            url: 'opciones/estado',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: parameter,
            beforeSend: function() {
            },
            success: function(data) {
                
                if(data.status==true){
                    Psi.mensaje('success', data.msj, 6000);
                    if(data.tipo=='opcion'){opciones.listar();}
                    if(data.tipo=='criterio'){opciones.listarCtr();} 
                }else{Psi.mensaje('danger',data.msj, 6000);}

                
            },
            error: function() {
                //$(".overlay,.loading-img").remove();
                Psi.mensaje('success', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    guardar_opciones : function (){
        var datos=$("#form_opciones").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/guardaropciones',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){
                    
                    $('#ModalOpcion').modal('toggle');
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listar();

                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    modificar_opciones : function (){
        var datos=$("#form_opciones").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/modificaropciones',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){

                    $('#ModalOpcion').modal('toggle');
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listar();

                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    abrir_modalOpcion : function (element){
        var id=$(element).attr("idOpc");
        var nombre=$(element).attr("nombreOpc");
        var tipo=$(element).attr("tipoOpc");

        $('#opcionId').val(id);
        $('#txt_nombreOpc').val(nombre);
        slctGlobal.listarSlct('opciones','slct_tipoOpc','simple',tipo,{'input':'tipoOpc'});
        $('#btnGuardarOpc').hide();
        $('#btnModificarOpc').show();
        $('#error_nombreOpc').hide();
        $('#error_tipoOpc').hide();
        //abrir modal
        $('#ModalOpcion').modal();
    },
    abrir_modalCriterios : function (element){
        var id=$(element).attr("idOpc");
        var nombre=($(element).attr("nombreOpc")).toUpperCase();
        $('#opcionId_Crt').val(id);
        $('#opcionCrt').html('OPCION: "'+nombre+'"');
        limpiar_formCrt();
        $('#btnModificarCrt').hide();
        $('#btnGuardarCrt').show();
        opciones.listarCriterios();
        slctGlobal.listarSlct('opciones','slct_criterioCrt','simple',null,{'input':'criterio','idusuario':'0'});
        //abrir modal
        $('#ModalCritetio').modal();
    },
    abrir_modalCrit : function (element){
        var id=$(element).attr("idCrit");
        var nombre=$(element).attr("nombreCrit");
        var campo=$(element).attr("campoCrit");
        $('#criterioId').val(id);
        $('#txt_nombreCrit').val(nombre);
        $('#txt_campoCrit').val(campo);
        $('#btnModificarCrit').show();
        $('#btnGuardarCrit').hide();
        $('#error_nombreCrit').hide();
        $('#error_campoCrit').hide();
        //abrir modal
        $('#ModalCrit').modal();
    },
    mostrar_criterios : function (element){
        var id=$(element).attr("idCrt");
        var idCrtOpc=$(element).attr("idCrtOpc");
        var nombre=$(element).attr("nombreCrt");
        var campo=$(element).attr("campoCrt");
        var valor=$(element).attr("valorCrt");
        var estado=$(element).attr("estadoCrt");

        $('#Id_Crt_Opc').val(idCrtOpc);
        $('#criterioId_Crt').val(id);
        $('#txt_campoCrt').val(campo);
        $('#txt_valorCrt').val(valor);
        $('#slct_estadoCrt').val(estado);
        slctGlobal.listarSlct('opciones','slct_criterioCrt','simple',id,{'input':'criterio'});

        $('#btnGuardarCrt').hide();
        $('#btnModificarCrt').fadeIn('slow');
    },
    cargar_campos_criterios : function (){
        var datos=$("#form_criterios").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/cargarcamposcriterio',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
            },
            success: function(data) {
                $.each(data.criterios, function(key, val){
                    console.log(val.nombre_gestion);
                    $('#txt_campoCrt').val(val.nombre_gestion);
                 });
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    modificar_valor : function (){
        var datos=$("#form_criterios").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/modificarvalor',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){

                    limpiar_formCrt();
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listarCriterios();

                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    guardar_valor : function (){
        var datos=$("#form_criterios").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/guardarvalor',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){
                    
                    limpiar_formCrt();
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listarCriterios();
                
                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    guardar_criterios : function (){
        var datos=$("#form_crit").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/guardarcriterio',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){
                    
                    $('#ModalCrit').modal('toggle');
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listarCtr();

                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    modificar_criterios : function (){
        var datos=$("#form_crit").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url: 'opciones/modificarcriterio',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){
                    
                    $('#ModalCrit').modal('toggle');
                    Psi.mensaje('success', data.msj, 6000);
                    opciones.listarCtr();

                }else{Psi.mensaje('danger',data.msj, 6000);}
                console.log(data);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    }
}