var envioMasivoSt = {
    listar_solicitudes: function () {
        var datos="";
        var targets=0;
        $('#tb_st_caidas').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                //$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                //$(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                envioMasivoSt.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.solicitud_tecnica;}},
                {data : function( row, type, val, meta) {return row.requerimiento;}},
                {data : function( row, type, val, meta) {return row.legado;}},
                {data : function( row, type, val, meta) {return row.actividad;}},
                {data : function( row, type, val, meta) {return row.estado_ofsc;}},
                {data : function( row, type, val, meta) {return row.nombre_flujo;}},
                {data : function( row, type, val, meta) {return row.codigo_error;}},
                {data : function( row, type, val, meta) {return row.error;}},
                {data : function( row, type, val, meta) {return row.intentos;}},
                {data : function( row, type, val, meta) {return row.observacion;}},
                {data : function( row, type, val, meta) {return row.fecha_creacion;}},
                //{data : function( row, type, val, meta) {return row.estado;}}
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 1 , "asc" ]],
            info: true,
            autoWidth: false,
            destroy:true
        });
    },
    http_ajax: function(request,callback){
        var order = request.order[0];
        form='column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;

        eventoCargaMostrar();
        axios.post('envioMasivoSt/listarsolicitudes',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    listar_log_envio_masivo: function(){
        axios.post('envioMasivoSt/logenviomasivo',form).then(response => {
            var html='<tr>';
            var data_log_envio_masivo=response.data.log_envio_masivo;
            var cantidad=response.data.cantidad;

            $.each(data_log_envio_masivo, function( key, val ) {
                html+='<tr>';
                html+='<td>'+val.fecha_creacion+'</td>';
                html+='<td>'+val.usuario+'</td>';
                html+='<td>'+val.nombre_flujo+'</td>';
                html+='<td>'+val.codigo_error+'</td>';
                html+='<td>'+val.error+'</td>';
                html+='<td>'+val.cantidad+'</td>';
                html+='</tr>';
            });

            $('#tbody_log_envio_masivo').html(html);
            $('#cantidad').html(cantidad);
            
            if(cantidad == 0){
                $('.btn_envio_masivo').attr('disabled', 'disabled');
            }else{
                $('.btn_envio_masivo').removeAttr('disabled');
            }

        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    envio_masivo: function(){
        eventoCargaMostrar();
        axios.post('envioMasivoSt/enviomasivoofsc',{}).then(response => {
            console.log(response);
            var error = response.data.error;
            envioMasivoSt.listar_log_envio_masivo();
            envioMasivoSt.listar_solicitudes();
            if (error === undefined) {
                var estado = response.data.estado;
                var msg = response.data.msg;
                if (estado == true){
                    eventoCargaRemover(); 
                    swal("Success",msg,"success");  
                }
            } else {
                eventoCargaRemover();
                swal("Error",'<b>Error en Envio Masivo:</b>\n'+error.message,"error");
            }
        }).catch(e => {
            vm.errors=e;
            eventoCargaRemover();
            swal("Error",e,"error");  
        }).then(() => {
            eventoCargaRemover();
        });
    }
}