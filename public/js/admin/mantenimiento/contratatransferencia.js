var instance = axios.create({
	baseURL: '/contratatransferencia'
});

var instanceSelect = axios.create({
	baseURL: '/listado_lego'
});

var vm = new Vue({
	el: '#app',
	components: {
        Multiselect: window.VueMultiselect.default
    },
	data: {
		datos: [],
		objContrataTransferencia: {
			estado: 1
		},
		actividad: {},
		empresa: {},
		type: 'Registrar',

		items: [],
		fields: {
			index: {
				label: 'Indice',
				sortable: true
			},
			empresa: {
				label: 'Empresa',
				sortable: true
			},
			tipo_legado_nombre: {
				label: 'Tipo Legado',
				sortable: true
			},
			actividad: {
				label: 'Actividad',
				sortable: true
			},
			area_transferencia: {
				label: 'Area Transferencia',
				sortable: true
			},
			contrata_transferencia: {
				label: 'Contrata Transferencia',
				sortable: true
			},
			estado: {
				label: 'Estado'
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPage: 1,
		perPage: 10,
		filter: null,
	},

	mounted: function () {
		this.list();
		var selects = ["actividad", "empresa"];
        var data = [{}, {}];
        this.listarSelects(selects, data);
        $('#transferenciaModal').on('hide.bs.modal', function (event) {
            vm.cleanForm();
        });
	},

	methods: {
		list: function () {
			instance.post('/list')
				.then((response) => {
					this.datos = response.data.datos;
				})
		},

		find: function (obj) {
			this.type = "Actualizar";
			this.objContrataTransferencia = obj;
		},

		action: function () {
			switch (this.type) {
				case "Actualizar": 
					this.update();
					break;
				case "Registrar": 
					this.insert();
					break;
			}
			$("#transferenciaModal").modal('hide');
		},

		insert: function () {
			instance.post('/insert', this.objContrataTransferencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		update: function () {
			instance.post('/update', this.objContrataTransferencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
					}
				})
		},

		setEstado: function (obj) {
			this.objContrataTransferencia = obj;
			this.objContrataTransferencia.estado = (this.objContrataTransferencia.estado == 1) ? 0 : 1;
			instance.post('/update', this.objContrataTransferencia)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
						this.cleanForm();
					}
				})
		},

		listarSelects: function (selects, data) {
			var request = {
				selects: JSON.stringify(selects),
				datos: JSON.stringify(data),
			};

            instanceSelect.post('/selects', request)
	            .then((response) => {
	                var obj = response.data;
	                for(i = 0; i <obj.data.length; i++){
	                    obj.datos = obj.data[i];
	                    eval("this."+selects[i]+" = obj.datos");
	                }
	            })
        },

        cleanForm: function () {
			this.type = "Registrar";
			this.objContrataTransferencia = {estado: 1};
		},

	}
})