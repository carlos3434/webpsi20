var idusuario = 0;
var todos = false;
var funciones = {
    change : function(value, checked, option) {
        changeComboSubmotivo(value, checked, option);
    },
    selectedall : function(select) {
        if (typeof select[0]!="undefined" && typeof select[0]!=undefined) {
            var motivoid = select[0].motivoid;
            $.each(listSubMotivosOfscChecked, function(index, data){
                if (typeof data!="undefined" && typeof data!=undefined) {
                    if (data.motivo_ofsc_id == motivoid) {
                        listSubMotivosOfscChecked[data.id].seleccionado = true;
                    }
                }
                
            });
        }
    }
};

$(document).ready(function(){
	$(document).delegate(".btn-limpiar-asignacion", "click", function(e){
		var idusuario = $(this).data("id");
		limpiarasignacion(idusuario);
	});
	slctGlobalHtml('slct_actividad_id', 'simple');
	slctGlobalHtml('slct_tipo_legado', 'simple');
	$("#btn_asignacion").click(AsignacionMotivo.guardar);
    $("#btn-filtro-motivos").click(pintarGrillaMotivos);
    $('#chck_todos').on('ifChecked', function (event) {
        $(".chck_motivos").each(function(index, data) {
            var id = $(this).val();
            var idsubmotivo = "combo-submotivo-"+id;
            $("#"+idsubmotivo+" option").attr("selected", "selected").prop('selected', true);
            $("#"+idsubmotivo).multiselect('refresh');
            $(this).prop("checked", true);
        });
        cambiarestadolistChecked(true);
        todos = true;
    });
    $('#chck_todos').on('ifUnchecked', function (event) {
        $(".chck_motivos").each(function(index, data) {
            var id = $(this).val();
            var idsubmotivo = "combo-submotivo-"+id;
            $("#"+idsubmotivo+" option:selected").removeAttr('selected').prop('selected', false);
            $("#"+idsubmotivo).multiselect('refresh');
            $(this).removeAttr("checked");
        });
        cambiarestadolistChecked(false);
        todos = false;
    });
    $(document).delegate(".chck_motivos", "click", function(){
        var id = $(this).val();
        var idsubmotivo = "combo-submotivo-"+id;
        if (!$(this).is(":checked")) {
            listMotivosOfscChecked[id].seleccionado = false;
            cambiarestadolistSubmotivoChecked(id, false);
            if (todos) {
                $('#chck_todos').removeAttr('checked').iCheck('update');
                
            }
            $("#"+idsubmotivo+" option:selected").removeAttr('selected').prop('selected', false);
            $("#"+idsubmotivo).multiselect('refresh');
        } else {
            listMotivosOfscChecked[id].seleccionado = true;
            cambiarestadolistSubmotivoChecked(id, true);
            $("#"+idsubmotivo+" option").attr("selected", "selected").prop('selected', true);
            $("#"+idsubmotivo).multiselect('refresh');
        }
        todos = false;
    });
    $(document).delegate(".combo-submotivo", "change", function(e){
        var value = e.target.value;
        if (value!="") {
            listSubMotivosOfscChecked[value].seleccionado = true;
        }
    });
    AsignacionMotivo.getMotivosOfsc();
    AsignacionMotivo.getSubMotivosOfsc();
});

listar  = function () {
	$("#t_usuarios").DataTable().destroy();
	AsignacionMotivo.listar();
};
pintarGrillaMotivos = function(asignacion) {
    eventoCargaMostrar();
    var html="";
    var selects = [];
    var listMotivosOfscFiltro = [];
    var actividad_id = $("#slct_actividad_id").val();
    var tipo_legado = $("#slct_tipo_legado").val();
    var filtro = {actividad_id : actividad_id, tipo_legado : tipo_legado};
    $('#t_motivos_usuarios').dataTable().fnDestroy();
    var i = 0;
    var asignacion = asignacionseleccionado;
    // filtramos
    $.each(listMotivosOfsc,function(index,data){
        if (typeof data!="undefined" && typeof data!=undefined) {
            listMotivosOfscFiltro[data.id] = data;
            var coincide = true;
            for (var j in filtro) {
                if (filtro[j] !="") {
                    if (data[j] != filtro[j]) {
                        if (coincide == true) {
                            coincide = false;
                        }
                    }
                }
            }
            if (coincide == false) {
                listMotivosOfscFiltro[data.id] = null;
            }
        }
    });
    $.each(listMotivosOfscFiltro,function(index,data){
        if (data !=null && data!="null" && typeof data!="undefined" && typeof data!=undefined) {
            var submotivos = listSubMotivosOfsc[data.id];
            var htmlsubmotivo ="";
            var motivoasignado = null;
            if (typeof asignacion[data.id] != "undefined" && typeof asignacion[data.id] !=undefined) {
                motivoasignado = asignacion[data.id];
            }
            if (typeof submotivos !="undefined" && typeof submotivos !=undefined) {
                var submotivosseleccionados = [];
                htmlsubmotivo+="<select data-motivoid ='"+data.id+"' name='slct_submotivo["+data.id+"][]' class='form-control combo-submotivo' id='combo-submotivo-"+data.id+"' multiple='multiple'>";
                for (var i in submotivos) {
                    submotivo = submotivos[i];
                    htmlsubmotivo+="<option data-motivoid = '"+data.id+"' value='"+submotivo.id+"'>"+submotivo.descripcion+"</option>";
                    if (motivoasignado!=null && motivoasignado!="null") {
                        var submotivoasignado = motivoasignado[submotivo.id];
                        if (typeof submotivoasignado!="undefined" && typeof submotivoasignado !=undefined) {
                            submotivosseleccionados.push(submotivo.id);
                            listSubMotivosOfscChecked[submotivo.id].seleccionado = true;
                        }
                    }
                }
                htmlsubmotivo+="</select>";
                selects[i] = {id : 'combo-submotivo-'+data.id, seleccionados : submotivosseleccionados};
                i++;
            }
            var checked = "";
            if (motivoasignado!=null && motivoasignado!="null") {
                listMotivosOfscChecked[data.id].seleccionado = true;
                checked = "checked = 'checked'";
            }
            html += "<tr>"+
                        "<th><input type='checkbox' name='chck_motivo[]' "+checked+" value='"+data.id+"' data-index='"+index+"' class='chck_motivos'/></th>"+
                        "<td>"+data.descripcion+"</td>"+
                        "<td>"+htmlsubmotivo+"</td>"+
                        "<td>"+data.actividad+"</td>"+
                        "<td>"+data.estadoofsc+"</td>"+
                        "<td>"+data.tipolegado+"</td>";
            html += "</tr>";
        }
    });
    $("#tb_motivos_usuarios").html(html);
    $.each(selects, function(index, data){
        if (typeof data!="undefined" && typeof data!=undefined) {
            slctGlobalHtml(data.id, 'multiple', data.seleccionados, null, null, null, null, null, funciones);
        }
    });
    $("#t_motivos_usuarios").dataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        "order": [[ 0, "asc" ]],
        info: true,
        autoWidth: true,
        language: config,
        "drawCallback": function( settings ) {
            eventoCargaRemover();
        },
        "fnDrawCallback": function( oSettings ) {
            if (todos) {
                $(".chck_motivos").each(function(index, data) {
                    var id = $(this).val();
                    var idsubmotivo = "combo-submotivo-"+id;
                    $("#"+idsubmotivo+" option").attr("selected", "selected").prop('selected', true);
                    $("#"+idsubmotivo).multiselect('refresh');
                    $(this).prop("checked", true);
                });
            } else {
                $(".chck_motivos").each(function(index, data){
                    var id = $(this).val();
                    var idsubmotivo = "combo-submotivo-"+id;
                    
                    if (listMotivosOfscChecked[id].seleccionado) {
                        $(this).prop("checked", true);
                        var seleccionados = [];
                        $("#"+idsubmotivo+" option:selected").each(function(e){
                            listSubMotivosOfscChecked[$(this).val()].seleccionado = true;
                        });
                        slctGlobalHtml(idsubmotivo, 'multiple', seleccionados);
                    } else {
                        $(this).removeAttr("checked"); 
                        $("#"+idsubmotivo+" option:selected").removeAttr('selected').prop('selected', false);
                        $("#"+idsubmotivo).multiselect('refresh');
                    }
                });
            }
            eventoCargaRemover();
        }
    });
    listMotivosOfscFiltro = [];
}
limpiarasignacion = function(idtematico) {
	swal({
        title: '¿Desea eliminar las asignaciones de motivos para el usuario?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Eliminar'
    }).then(function () {
        AsignacionMotivo.limpiarasignacion(idtematico);
    });
};
detalle = function(asignacion) {
    pintarGrillaMotivos(asignacion);
};
cambiarestadolistChecked = function(estado) {
    $.each(listMotivosOfscChecked, function(index, data){
        if (typeof data!="undefined" && typeof data!=undefined) {
            listMotivosOfscChecked[data.id].seleccionado = estado;
        }
    });
    $.each(listSubMotivosOfscChecked, function(index, data){
        if (typeof data!="undefined" && typeof data!=undefined) {
            listSubMotivosOfscChecked[data.id].seleccionado = estado;
        }
    });
};
cambiarestadolistSubmotivoChecked = function(motivoofscid, estado) {
    $.each(listSubMotivosOfscChecked, function(index, data){
        if (typeof data!="undefined" && typeof data!=undefined) {
            if (data.motivo_ofsc_id == motivoofscid) {
                listSubMotivosOfscChecked[data.id].seleccionado = estado;
            }
        }
    });
};
changeComboSubmotivo = function(value, checked, option) {
    console.log(option);
    if (typeof value == "undefined" || typeof value == undefined) {

    } else {
        if (typeof listSubMotivosOfscChecked[value]!="undefined" && typeof listSubMotivosOfscChecked[value]!=undefined) {
            console.log(listSubMotivosOfscChecked[value]);
            listSubMotivosOfscChecked[value].seleccionado = checked;
        }
    }
};
listar();

var enforceModalFocusFn;
$('#asignacionMotivoModal').on('show.bs.modal', function (event) {
    enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var nombreusuario = button.data("nombreusuario");
    if (id > 0) {
    	idusuario = id;
        $("#span_nombre_usuario").text(nombreusuario);
    	AsignacionMotivo.get(id);
    }
});
$('#asignacionMotivoModal').on('hide.bs.modal', function (event) {
    $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    var modal = $(this);
    modal.find('.modal-body input[type=text]').val('');
    modal.find('.modal-body input[type=checkbox]').removeAttr("checked");
    modal.find('.modal-body textarea').val('');
    $('#slct_parent, #slct_tipo_legado, #slct_actividad_id').multiselect('select', [""]);
    $('#slct_parent, #slct_tipo_legado, #slct_actividad_id').multiselect('rebuild');
    $("#slct_opciones option:selected").removeAttr('selected').prop('selected', false);
    $("#slct_opciones").multiselect('refresh');
    $("#div_tipo_devolucion").css("display", "none");
    //modal.find(".modal-title").text("Nuevo Tematico");
    $("#span_nombre_usuario").text("");
    idusuario = 0;
    asignacionseleccionado = [];
    todos = false;
    cambiarestadolistChecked(false);
});