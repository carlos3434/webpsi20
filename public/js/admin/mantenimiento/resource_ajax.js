var Resource = {
	update: function (idtecnico, idbucketDestino, idempresaDestino) {
		var datos = {
			idtecnico: idtecnico,
			idbucket: idbucketDestino,
			idempresa: idempresaDestino
		}
        $.ajax({
            url: 'tecnico/editarbuket',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
				$(".overlay,.loading-img").remove();
				if (data.rst == 1) {
                    $('#dragTree').jstree().refresh();
					swal("Exito", data.msj, "success");
				} else {
					swal("Error", data.msj, "error");
				}
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
	}
}