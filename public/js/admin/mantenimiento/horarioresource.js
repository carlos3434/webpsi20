var instance = axios.create({
    baseURL: '/horarioresource'
});

var vm = new Vue({
    el: '#app',
    data: {
        datos: [],
        objHorarioResource: {
            estado: 1
        },
        type: 'Registrar',
        mensaje : '',
        items: [],
        fields: {
            dia: {
                label: 'Dia',
                sortable: true
            },
            rango_hora: {
                label: 'Rango Hora',
                sortable: true
            },
            hora_inicio: {
                label: 'Hora Inicio',
                sortable: true
            },
            hora_fin: {
                label: 'Hora Fin',
                sortable: true
            },
            estado: {
                label: 'Estado'
            },
            actions: {
                label: '¿Editar?'
            }
        },
        currentPage: 1,
        perPage: 10,
        filter: null,
    },
    mounted: function () {
        this.list();
        $('#horarioModal').on('show.bs.modal', function (event) {
            $('#form_horarioresource .dia_id').multiselect('select', vm.objHorarioResource.dia_id);
            $('#form_horarioresource .dia_id').multiselect('rebuild');


            // filtroSlct("form_horarioresource .empresa","simple",'E','.celula');

            $('#form_horarioresource .hora_inicio').val(vm.objHorarioResource.hora_inicio);
            $('#form_horarioresource .hora_fin').val(vm.objHorarioResource.hora_fin);

            
        });

        $('#horarioModal').on('hide.bs.modal', function (event) {
            $('#form_horarioresource .dia_id').multiselect('select', "");
            $('#form_horarioresource .dia_id').multiselect('rebuild');
            $('#form_horarioresource .hora_inicio').val("");
            $('#form_horarioresource .hora_fin').val("");
            vm.cleanForm();
        });
    },

    methods: {
        list: function () {
            instance.post('/list')
                .then((response) => {
                    this.datos = response.data.datos;
                })
        },

        find: function (obj) {
            this.type = "Actualizar";
            var copyObj = Object.assign({}, obj);
            this.objHorarioResource = copyObj;
        },

        action: function () {
            switch (this.type) {
                case "Actualizar": 
                    this.update();
                    break;
                case "Registrar": 
                    this.insert();
                    break;
            }
        },

        /*insert: function () {
            instance.post('/insert', this.objHorarioResource)
                .then((response) => {
                    if (response.data.rst == 1) {
                        swal("Exito", response.data.msj, "success");
                        this.list();
                        this.cleanForm();
                        $("#horarioModal").modal('hide');
                    } else {
                        swal("Error", "Error al registrar Horario Resource", "error");
                    }
                })
        },*/

        update: function () {
            if (this.objHorarioResource.hora_inicio > this.objHorarioResource.hora_fin) {
                swal("Error", "La Hora Inicio debe ser menor a Hora Fin", "error");
                return;
            }

            this.objHorarioResource.rango_hora = this.objHorarioResource.hora_inicio + " - " + this.objHorarioResource.hora_fin;
            instance.post('/update', this.objHorarioResource)
                .then((response) => {
                    if (response.data.rst == 1) {
                        swal("Exito", response.data.msj, "success");
                        this.list();
                        this.cleanForm();
                        $("#horarioModal").modal('hide');
                    } else {
                        swal("Error", "Error al registrar Horario Resource", "error");
                    }
                })
        },

        setEstado: function (obj) {
            this.objHorarioResource = obj;
            this.objHorarioResource.estado = (this.objHorarioResource.estado == 1) ? 0 : 1;
            instance.post('/update', this.objHorarioResource)
                .then((response) => {
                    if (response.data.rst == 1) {
                        this.cleanForm();
                    } else {
                        this.objHorarioResource.estado = (this.objHorarioResource.estado == 1) ? 0 : 1;
                        swal("Error", "Error al actualizar Horario Resource", "error");
                    }
                })
        },

        cleanForm: function () {
            this.type = "Registrar";
            this.objHorarioResource = {estado: 1};
        },

    }
})