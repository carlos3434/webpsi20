$(document).ready(function(){
   solicitudesT.listar();
   selectPremiun();
   validaCampos();
   $('.btnSaveALl').attr('disabled',true);
   $('.btnCancelALl').attr('disabled',true);
   $('.btnAdd').attr('disabled',true);
   //cambiarestado();
  $('#nuevaZonaPremium').on('shown.bs.modal', function (event) {
        //alert("hola");
        //::::::
        var button = $(event.relatedTarget);
        var id = button.data('id');
        //alert(id);
        //console.log(id);
        if(!id){
            //alert("guardar");
            $("#btn_guardar").attr('onclick','guardar()');
           $("#slct_estado_premium_modal option[value=1]").prop('selected',true);
           $("#slct_estado_premium_modal").multiselect("refresh");
           $("#slct_nodo_premium_modal").multiselect("refresh");
        
           $("#slct_troba_premium_modal").multiselect("refresh");
        }else{
            //alert("Actualizar");
            $("#btn_guardar").attr('onclick','editar()');
            $("#frm_tipificacion").append('<input type="hidden" value="'+id+'" name="txt_tipificacion_id" id="txt_tipificacion_id">');
        }
        ids = id;           
    }); 

    
    $('#nuevaZonaPremium').on('hide.bs.modal', function (event) {
        $("#frm_tipificacion input[type='hidden'],#frm_tipificacion input[type='password'],#frm_tipificacion input[type='text'],#frm_tipificacion input[type='file'],#frm_tipificacion select,#frm_tipificacion textarea").not('.mant').val('');
        $("#frm_tipificacion #txt_tipificacion_id").remove();
    }); 

    $(document).on('click', '.btn-detalle', function(event) {
        var tr = $(this).parent().parent().find('td');
        var zona = $(tr[0]).html();
        var nodo=$(tr[1]).html();
        var troba=$(tr[2]).html();
        var contrata=$(tr[3]).html();
        //var estado=$(tr[5]).html();
        var estado = $(tr[5]).find('span').attr('estado');  
        var est =(estado == 0) ? 1 : 0;

        $("#txt_zonaPremiumModal").val(zona);
        $("#slct_nodo_premium_modal").val(nodo);
        $("#slct_nodo_premium_modal").multiselect("refresh");
        $("#slct_troba_premium_modal").val(troba);
        $("#slct_troba_premium_modal").multiselect("refresh");
        $("#txt_contrata_premium_modal").val(contrata);
        //$("#slct_contrata_premium_modal").val(contrata);
        //$("#slct_contrata_premium_modal").multiselect("refresh");    
        $("#slct_estado_premium_modal option[value="+est+"]").prop('selected',true);
        $("#slct_estado_premium_modal").multiselect("refresh");
    });

});

filtrar=function(){
    solicitudesT.listar();
    var zona = $("#slct_zonal_premium").val()
    var nodo = $("#slct_nodo_premium").val()
    //alert(zona+ " " + nodo);
}

selectPremiun=function(){
    slctGlobalHtml('slct_estado_premium_modal','simple');
   
    var datos_={parametros:'1', prueba:'test'};

    var selects = [     
     "zonalPremium" ,     
     "nodoPremium", 
     'troba',
     "nodoPremium", 
     'troba'
     //'contratas'

     ];

    var slcts = [
      
     "slct_zonal_premium",     
     "slct_nodo_premium", 
      "slct_cod_troba",
      "slct_nodo_premium_modal",
      "slct_troba_premium_modal"
      //"slct_contrata_premium_modal"
    ];

    var tipo = [
     "multiple", "multiple", "multiple",
     "simple", "simple"//,"simple"
    ];

    var valarray = [
     null, null, null,null,
     null//,null
     
    ];
    var data = [
     {}, {}, {},{},{}//,{}
    ];
    var afectado = [
     0, 0, 0,0,0//,0
    ];
    var afectados = [
     "", "", "","",""//,""
    ];
    var slct_id = [
     "", "", "","",""//,""
    ];
    var es_lista =[
     true, true, true, true,true//,true
    ];
    solicitudesT.getSelects(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id,{},{},{}, es_lista);
}

cambiarestado = function(obj){
        
        var tipificacion_id = obj.getAttribute('tipificacion_id');
        var estado = obj.getAttribute('estado');


        if(tipificacion_id && estado){
            solicitudesT.CambiarEstado({tipificacion_id:tipificacion_id,estado:estado});
        }
}

guardar = function(){
   if(validarCamposVacios()){
    solicitudesT.guardar(1);
    
   }
    
}

editar = function(){  
  if(validarCamposVacios()){
    solicitudesT.guardar(2);   
   }       
}

function validarCamposVacios(){
     estado=true;
    if($("#txt_zonaPremiumModal").val() == ''){            
        swal('Ingrese Zonal');
        estado=false;            
    }else if($("#slct_nodo_premium_modal").val() == ''){
        swal('Ingrese Nodo');
        estado=false;  
    }else if($("#slct_troba_premium_modal").val() == ''){
        swal('Ingrese Troba');
        estado=false;  
    }else if($("#txt_contrata_premium_modal").val() == ''){
        swal('Ingrese Contrata');
        estado=false;  
    }else if($("#slct_estado_premium_modal").val() == ''){
        swal('Ingrese Estado');
        estado=false;  
    }
    //alert(estado);
    return estado;
}

 limpiar=function(){
        $("#txt_zonaPremiumModal").val("");
        $("#slct_nodo_premium_modal").val("");
        $("#slct_troba_premium_modal").val("");
        $("#txt_contrata_premium_modal").val("");
        $("#slct_estado_premium_modal").val("");
        $("#slct_nodo_premium_modal").multiselect("refresh");      
        $("#slct_troba_premium_modal").multiselect("refresh");       
        $("#slct_contrata_premium_modal").multiselect("refresh");        
        $("#slct_estado_premium_modal").multiselect("refresh");
    }
 validaCampos=function(){
    $("#txt_zonaPremiumModal").validCampoFranz(' 1234567890_abcdefghijklmnñopqrstuvwxyz');
    $("#txt_contrata_premium_modal").validCampoFranz(' 1234567890_abcdefghijklmnñopqrstuvwxyz');
 }

 $('#myTab a').click(function (e) {
  e.preventDefault();
  if(($(this).attr('href') == "#tb_carga")) {
    //$("#t_modulos").dataTable().fnDestroy();
    $('#tb_cargamasiva').hide();
    $('#tb_carga').fadeIn('slow');
  }
  if(($(this).attr('href') == "#tb_cargamasiva")) {
    $('#tb_carga').hide();
    $('#tb_cargamasiva').fadeIn('slow');
  }
});


$('#btnSubirExcel').click(function() {
    $('#btnHiddenExcel input').click();
    return false;
});

$("#btnHiddenExcel").ajaxUpload({
    action: 'zonapremiumcatv/uploadexcel',
    url: 'zonapremiumcatv/uploadexcel',
    name: 'excel',
    onSubmit: function(file, extension) {
        $('#btnSubirExcel').html('Cargando...').prop('disabled', true);
    },
    onComplete: function(result) {
        var result = JSON.parse(result);
        if( result.rst == 1){
            swal("Success","Excel cargado correctamente!","success");
            pintar_tablaCarga(result.data);
            $('#form_reportecargmasiva').fadeIn('slow')
            $('#btnSubirExcel').html('Listo').prop('disabled', true);
            $('.btnSaveALl').attr('disabled',false);
            $('.btnCancelALl').attr('disabled',false);
            $('.btnAdd').attr('disabled',false);
        }else{
            swal("Error",result.msj,"error");
            $('#btnSubirExcel').html('Subir Archivo Excel').prop('disabled', false);
        }
    }
});

pintar_tablaCarga= function(data){
    var table =$('#tbl_cargamasiva').DataTable({
        lengthChange: false,
        lengthMenu: [ [5, 10], [5, 10] ],
        pageLength: 10,
        paging: true,
        ordering: true,
        info: true,
        data: (Object.values(data)),
        searching: false,
        deferRender:true,
        scrollCollapse: true,
        scroller:false,
        destroy:true,
        columns: [
            { data: "zona"},
            { data: "nodo"},
            { data: "troba"},
            { data: "contrata"},
            {  "data": null,"sortable": false,
                render: function ( row, type, full, meta ) {
                    htmlButtons="";
                    htmlButtons ="  <a class='btn btn-danger btn-xs btnRemove' data-toggle='modal' title='Eliminar registro'><i class='glyphicon glyphicon-remove'></i></a>";
                    htmlButtons+="  <a class='btn btn-success btn-xs btnEdit' data-toggle='modal' title='Editar registro'><i class='glyphicon glyphicon-pencil'></i></a>";
                    return htmlButtons;
                }
            }      
        ]
    });
}

alert_modal = function(type,message){

  if(type=='error'){
    $('.alert_modal').html('<h5><i class="glyphicon glyphicon-info-sign"></i> Message</h5>'+message+'</div>');
    $('.alert_modal').fadeIn('slow');
    setTimeout(function(){$('.alert_modal').fadeOut('slow');},4000);
  }
  if(type=='success'){
    $('.alert_modal_success').html('<h5><i class="fa fa-check"></i> Success</h5>'+message+'</div>');
    $('.alert_modal_success').fadeIn('slow');
    setTimeout(function(){$('.alert_modal_success').fadeOut('slow');},6000);
  }

};

$('.btnCancelALl').click(function() {

    $('#btnSubirExcel').html('Subir Archivo Excel').prop('disabled', false);
    $('#form_reportecargmasiva').fadeOut('slow')
    $('.alert_modal').fadeOut('slow');
    $('.alert_modal_success').fadeOut('slow');
    $('.btnSaveALl').attr('disabled',true);
    $('.btnCancelALl').attr('disabled',true);
    $('.btnAdd').attr('disabled',true);
    pintar_tablaCarga('');

});

$(document).on('click', '.btnAdd', function(event) {

    event.preventDefault();
    if( $('.btnSaveAdd').is(":visible") || $('.btnUpdateEdit').is(":visible") ){
        swal("Mensaje",'Terminar accion anterior!');
    }else{
        $('.hd_index').val('');
        $('.txt_zona').val('');
        $('.txt_nodo').val('');
        $('.txt_troba').val('');
        $('.txt_contrata').val('');
        $('.hd_zona').val('');
        $('.hd_nodo').val('');
        $('.hd_troba').val('');
        $('.hd_contrata').val('');

        $('.btnSaveAdd').show();$('.btnCancelAdd').show();
        $('.btnUpdateEdit').hide();$('.btnCancelEdit').hide();

        $("#tbl_cargamasiva tfoot").find('.repeat').removeClass('hidden');
    }

});

$(document).on('click','.btnRemove',function(event){

    var tr = $(this).parent().parent().find('tr');
    var index=tr.prevObject[0]._DT_RowIndex;
    var table = $('#tbl_cargamasiva').DataTable();
    table.row( index ).remove().draw();
    Psi.mensaje('success', 'Datos Eliminados Correctamente!', 4000);

});

$(document).on('click', '.btnEdit', function(event) {

    if( $('.btnSaveAdd').is(":visible") || $('.btnUpdateEdit').is(":visible") ){
        swal("Mensaje",'Terminar accion anterior!');
    }else{
        var tds = $(this).parent().parent().find('td');
        var template = $("#tbl_cargamasiva tfoot").find('.repeat').clone().removeClass('repeat').removeClass('hidden');
        var data2=[];
        $.each(tds,function(key, val) {
            var data = (val.textContent).toUpperCase(); 
            template.find('td[id="'+val.id+'"] input');
            data2.push(data);            
        });            
        $(this).parent().parent().replaceWith(template);
        $('.txt_zona').val(data2[0]);
        $('.txt_nodo').val(data2[1]);
        $('.txt_troba').val(data2[2]);
        $('.txt_contrata').val(data2[3]);
        $('.hd_zona').val(data2[0]);
        $('.hd_nodo').val(data2[1]);
        $('.hd_troba').val(data2[2]);
        $('.hd_contrata').val(data2[3]);

        var tr = $(this).parent().parent().find('tr');
        var index=tr.prevObject[0]._DT_RowIndex;
        $('.hd_index').val(index);

        $('.btnUpdateEdit').show();$('.btnCancelEdit').show();
        $('.btnSaveAdd').hide();$('.btnCancelAdd').hide();
    }

});

$(document).on('click','.btnUpdateEdit',function(event){

    zona=($('.txt_zona').val()).toUpperCase();
    nodo=($('.txt_nodo').val()).toUpperCase();
    troba=($('.txt_troba').val()).toUpperCase();
    contrata=($('.txt_contrata').val()).toUpperCase();

    if(zona==''){swal("Mensaje", 'Campo Zona vacio!');return false;}
    if(nodo==''){swal("Mensaje", 'Campo Nodo vacio!');return false;}
    if(troba==''){swal("Mensaje", 'Campo Troba vacio!');return false;}
    if(contrata==''){swal("Mensaje", 'Campo Contrata vacio!');return false;}

    var cont=0;
    var table = $('#tbl_cargamasiva').DataTable();
    var data =[];
    data=table.data();
    $.each(data,function(key, val) {
        if((val.nodo).toUpperCase() == nodo && (val.troba).toUpperCase() ==troba ){
            cont++;
        }          
    });

    if( cont == 0 ){
        var index=$('.hd_index').val();
        table.row( index ).remove().draw();
        
        table.row.add({ 
            zona: zona,
            nodo: nodo,
            troba: troba,
            contrata: contrata
        }).draw();

        Psi.mensaje('success', 'Datos Actualizados Correctamente!', 4000);
    }else{
        Psi.mensaje('danger', 'Nodo y Troba ya existe!', 6000);
    }
});

$(document).on('click','.btnCancelEdit',function(event){

    zona=($('.hd_zona').val()).toUpperCase();
    nodo=($('.hd_nodo').val()).toUpperCase();
    troba=($('.hd_troba').val()).toUpperCase();
    contrata=($('.hd_contrata').val()).toUpperCase();
    
    var index=$('.hd_index').val();
    var table = $('#tbl_cargamasiva').DataTable();
    table.row( index ).remove().draw();
    
    table.row.add( { 
        zona: zona,
        nodo: nodo,
        troba: troba,
        contrata: contrata
    }).draw();

});

$(document).on('click','.btnCancelAdd',function(event){

   $("#tbl_cargamasiva tfoot").find('.repeat').addClass('hidden');

});

$(document).on('click','.btnSaveAdd',function(event){

    zona=($('.txt_zona').val()).toUpperCase();
    nodo=($('.txt_nodo').val()).toUpperCase();
    troba=($('.txt_troba').val()).toUpperCase();
    contrata=($('.txt_contrata').val()).toUpperCase();

    if(zona==''){swal("Mensaje", 'Campo Zona vacio!');return false;}
    if(nodo==''){swal("Mensaje", 'Campo Nodo vacio!');return false;}
    if(troba==''){swal("Mensaje", 'Campo Troba vacio!');return false;}
    if(contrata==''){swal("Mensaje", 'Campo Contrata vacio!');return false;}

    var cont=0;
    var table = $('#tbl_cargamasiva').DataTable();
    var data =[];
    data=table.data();
    $.each(data,function(key, val) {
        if((val.nodo).toUpperCase() == nodo && (val.troba).toUpperCase() ==troba ){
            cont++;
        }          
    });

    if( cont == 0 ){
        table.row.add( { 
            zona: zona,
            nodo: nodo,
            troba: troba,
            contrata: contrata
        }).draw();

        Psi.mensaje('success', 'Datos Registrados Correctamente!', 4000);
        $("#tbl_cargamasiva tfoot").find('.repeat').addClass('hidden');
    }else{
        Psi.mensaje('danger', 'Nodo y Troba ya existe!', 6000);
    }
});

$(document).on('click','.btnSaveALl',function(event){

    swal({
      title: "Esta usted seguro?",
      text: "Porfavor, elegir accion!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
        var table = $('#tbl_cargamasiva').DataTable();
        var data =[];
        data=table.data();
        solicitudesT.carga_masiva(data);
    }, function (dismiss) {
        if (dismiss === 'cancel') {}
  })

});

$(document).on('click','#link_descarga',function(event){
    var table = $('#tbl_cargamasiva').DataTable();
    var data =[];
    data=table.data();
    //$('#json').val(JSON.stringify(data));
    solicitudesT.ExportEcxel_CargaMasiva(data);
});
