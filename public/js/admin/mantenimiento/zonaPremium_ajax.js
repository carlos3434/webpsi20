  axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
  axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'; 
   var Datatable;
   var currentPage = 0;
   var search = '';
    var solicitudesT  = {
    //********
        listar: (function () {          
            var datos="";
            
            var targets=0;
            
            $('#tb_zonaPremium').dataTable().fnDestroy();
            //$('#tb_solicitudtecnica').dataTable().fnPageChange(currentPage,true);                          
            $('input[type=search]').val(search);
            $('input[type=search]').trigger('keyup');
            $('#tb_zonaPremium')
                    .on('page.dt', function () {
                      
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {                            
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                    
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            
                            solicitudesT.http_ajax(data,callback);
                           
                        },
                        "columns":[    
                            
                     
                            {data : function( row, type, val, meta) {
                               if (typeof row.zona!="undefined" && typeof row.zona!=undefined) {
                                    return row.zona;
                                } else return "";                               
                            }, name:'zona'},
                            
                            {data : function( row, type, val, meta) {
                                if (typeof row.nodo!="undefined" && typeof row.nodo!=undefined) {
                                    return row.nodo;
                                } else return "";                               
                            }, name:'nodo'},

                            {data : function( row, type, val, meta) {
                                if (typeof row.troba!="undefined" && typeof row.troba!=undefined) {
                                    return row.troba;
                                } else return "";                               
                            }, name:'troba'},

                            {data : function( row, type, val, meta) {
                                if (typeof row.contrata!="undefined" && typeof row.contrata!=undefined) {
                                    return row.contrata;
                                } else return "";                               
                            }, name:'contrata'},


                            {data : function( row, type, val, meta) {
                                if (typeof row.fecha_registro!="undefined" && typeof row.fecha_registro!=undefined) {
                                    return row.fecha_registro;
                                } else return "";                               
                            }, name:'fecha_registro'},

                            {data : function( row, type, val, meta) {

                                htmlEstado='<input type="hidden" value='+row.nodo+','+row.troba+','+row.contrata+' name="tipificacion_id"><span class="btn btn-danger btn-md" tipificacion_id="'+row.nodo+','+row.troba+','+row.contrata+'" estado="1" onclick="cambiarestado(this)">Inactivo</span>';
                                if(row.estado == 1){
                                    htmlEstado ='<input type="hidden" value='+row.nodo+','+row.troba+','+row.contrata+' name="tipificacion_id"><span class="btn btn-success btn-md" tipificacion_id="'+row.nodo+','+row.troba+','+row.contrata+'" estado="0" onclick="cambiarestado(this)">Activo</span>';
                                }                            
                                return htmlEstado;
                            }, name:'estado'},
                            {data : function( row, type, val, meta) {
                                  if(typeof meta == "undefined") {
                                    indice = -1;
                                } else {
                                    indice = meta.row;
                                }                                
                                htmlButtons = "<a class='btn btn-primary btn-md btn-detalle' data-toggle='modal' title='Detalle Incidencia' ";
                                htmlButtons+=" data-target='#nuevaZonaPremium' data-id='"+row.nodo+","+row.troba+","+row.contrata+"'>";
                                htmlButtons+=" <i class='glyphicon glyphicon-th-list'></i></a>";
                                return htmlButtons;
                            }, name:'botones'}                    
                         
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),
    CambiarEstado: function(data){

        //alert(data);
        $.ajax({
            url         : 'zonapremiumcatv/actualizar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                   solicitudesT.listar();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

      getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {

            var obj = response.data;
            
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
                vm.lista[ selects[i] ] = obj.data[i];
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
        
    http_ajax: function(request,callback){        
        
        var contador = 0;
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
       
        axios.post('zonapremiumcatv/listar',form).then(response => {
            callback(response.data);                       
            
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    guardar: function(tipo){

        var datos=$("#frm_tipificacion").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
               
        if(tipo == 1){
            url = "zonapremiumcatv/create";           
        }else{
            url = "zonapremiumcatv/modificar";
        }
        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            data        : datos,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('.:: Se Registro Correctamente ::.');
                    $("#nuevaZonaPremium").modal('hide');                    
                    solicitudesT.listar();
                    $("#slct_zonal_premium").multiselect("destroy");
                      selectPremiun();//location.reload();
                      limpiar();

                }else if(data.rst==3){
                    swal(data.msj);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    carga_masiva: function(data){

        zona=[];nodo=[];troba=[];contrata=[];
        $.each(data,function(key, val) {
            zona.push(val.zona);
            nodo.push(val.nodo); 
            troba.push(val.troba); 
            contrata.push(val.contrata);            
        });

        $.ajax({
            url         : 'zonapremiumcatv/cargamasiva',
            type        : 'POST',
            cache       : false,
            data        : {'zona':zona,'nodo':nodo,'troba':troba,'contrata':contrata},
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(response) {
                eventoCargaRemover();
                if( response.rst == 1 ){
                    swal("Success",response.msj,"success");
                    $('#form_reportecargmasiva').fadeOut('slow')
                    $('#btnSubirExcel').html('Subir Archivo Excel').prop('disabled', false);
                    $('.btnAdd').attr('disabled',true);
                    $('.btnSaveALl').attr('disabled',true);
                    $('.btnCancelALl').attr('disabled',true);
                    pintar_tablaCarga('');
                    solicitudesT.listar();
                }else{
                    swal("Error",response.msj,"error");
                }
            },
            error: function(){
                eventoCargaRemover();
                swal("Error","Error","error");
            }
        });
    },
    ExportEcxel_CargaMasiva: function (data) {
        zona=[];nodo=[];troba=[];contrata=[];
        $.each(data,function(key, val) {
            zona.push(val.zona);
            nodo.push(val.nodo); 
            troba.push(val.troba); 
            contrata.push(val.contrata);            
        });

        zona=JSON.stringify(zona);
        nodo=JSON.stringify(nodo);
        troba=JSON.stringify(troba);
        contrata=JSON.stringify(contrata);

        $("input[type='hidden']").remove();
        $("#form_reportecargmasiva").append("<input type='hidden' value='Excel' id='tipo' name='tipo'>");
        $("#form_reportecargmasiva").append("<input type='hidden' value='"+zona+"' id='zona' name='zona'>");
        $("#form_reportecargmasiva").append("<input type='hidden' value='"+nodo+"' id='nodo' name='nodo'>");
        $("#form_reportecargmasiva").append("<input type='hidden' value='"+troba+"' id='troba' name='troba'>");
        $("#form_reportecargmasiva").append("<input type='hidden' value='"+contrata+"' id='contrata' name='contrata'>");
        $("#form_reportecargmasiva").submit();
    }
 }