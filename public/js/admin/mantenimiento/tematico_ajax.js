axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
var isactivo = "is_pendiente";
var tematicos = [];
listMotivosOfsc = [];
var Tematico = {
	listar : function (isactivo) {
		var actividad_id = $("#slct_actividad_id_filtro").val();
		var tipo_legado = $("#slct_tipo_legado_filtro").val();
        var tipo_devolucion = $("#slct_tipodevolucion_filtro").val();
        var nombre_tematico = $("#txt_nombtematico").val();
	    return $("#t_tematicos").DataTable({
	        "destroy":true,
	        "processing": true,
	        "serverSide": true,
	        "stateSave": true,
	        "stateLoadCallback": function (settings) {
	        },
	        "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
	        },
	        ajax: function(data, callback, settings) {
	        	data.isactivo  = isactivo;
	        	data.actividad_id = actividad_id;
	        	data.tipo_legado = tipo_legado;
                data.tipodevolucion = tipo_devolucion;
                data.nombre_tematico = nombre_tematico;
	            Tematico.ajaxlistado(data,callback);
	        },
	        "columns":[
	        	{
                    "class":          "details-control",
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ""
                },
	            { data: 'nombre', name:'nombre'},
	            { data: 'padre', name:'parent'},
	            { data: 'legado', name:'tipo_legado'},
	            { data: 'actividad', name:'actividad_id'},
	            { data: 'tipodevolucion', name:'tipo'},
	            {data : function( row, type, val, meta) {
	                var htmlButtons = "";
	                if (row.estado == 1) {
	                	htmlButtons = "<a class='btn btn-success btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' title='Cambiar Estado'>"+row.estadotematico+"</a>";
	                } else {
	                	htmlButtons = "<a class='btn btn-danger btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' btn-md btn-cambiar-estado' title='Cambiar Estado'>"+row.estadotematico+"</a>";
	                }
	                return htmlButtons;
	            }, name: "estado"},
	            {data : function( row, type, val, meta) {
	            	var count=($("#t_tematicos").DataTable().data()).length;
	            	var htmlButtons = "";
	                htmlButtons = "<a class='btn btn-primary btn-sm btn-detalle' data-toggle='modal' title='Detalle Tematico' ";
	                htmlButtons+=" data-target='#tematicoModal' data-id='"+row.id+"' data-tipolegado='"+row.tipo_legado+"'";
	                htmlButtons+=" data-tipo='"+row.tipo+"' data-parent='"+row.parent+"'";
	                htmlButtons+=" data-actividadid='"+row.actividad_id+"'>";
	                htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
	                htmlButtons+="	<a class='btn btn-sm btn-danger btn-eliminar-tematico' data-id='"+row.id+"'><i class='fa fa-trash'></i></a>";
	                htmlButtons+="<input type='hidden' id='hdids"+(count-1)+"'>";
	                htmlButtons+="<input type='hidden' id='action"+(count-1)+"' value='0'>";
	                return htmlButtons;
	            }, name: "botones"},
	        ],
	        paging: true,
	        lengthChange: true,
	        searching: false,
	        ordering: true,
	        order: [[ 1, "asc" ]],
	        info: true,
	        autoWidth: true,
	        language: config
	        
	    });
	},
	ajaxlistado : function(request,callback){
        form="&isactivo="+request.isactivo;
        form+="&actividad_id="+request.actividad_id;
        form+="&tipo_legado="+request.tipo_legado;
        form+="&tipodevolucion="+request.tipodevolucion;
        form+="&_token="+document.querySelector('#token').getAttribute('value');
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+='&search='+request.nombre_tematico;//request.search.value;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        if(request.nombre_tematico==''){

            eventoCargaMostrar();
            axios.post('tematico/listar', form).then(response => {
                callback(response.data);
            }).catch(e => {
                vm.errors=e;
            }).then(() => {
                eventoCargaRemover();
            });

        }else{
            axios.post('tematico/buscartematicos', form).then(response => {

                busqueda=response.data;
                if(busqueda!=''){
                    form+="&busqueda="+busqueda;
                }else{
                    form+="&busqueda=00000";
                }

                eventoCargaMostrar();
                axios.post('tematico/listar', form).then(response => {
                    callback(response.data);
                }).catch(e => {
                    vm.errors=e;
                }).then(() => {
                    eventoCargaRemover();
                });

            }).catch(e => {
                vm.errors=e;
            })
        }
    },
    get : function(idtematico) {
    	eventoCargaMostrar();
    	var tematico = tematicos[idtematico];
    	if (typeof tematico == "undefined" || typeof tematico == undefined) {
    		form="&id="+idtematico;
	        form+="&_token="+document.querySelector('#token').getAttribute('value');
	        axios.get('tematico/editar?'+form).then(response => {
	        	var data = response.data;
	            if (data.rst == "2") {
		    		Psi.sweetAlertError(data.msj);
		    	}
		    	if (data.rst == "1") {
		    		detalle(data.obj);
		    		tematico[data.obj.id] = data.obj;
		    	}
	        }).catch(e => {
	            vm.errors=e;
	        }).then(() => {
	            eventoCargaRemover();
	        });
    	} else {
    		Tematico.detalle(tematico);
    		eventoCargaRemover();
    	}
    },
    guardarTematico : function() {
    	var form = $("#form_tematicos").serialize().split("txt_").join("").split("slct_").join("");
    	form+="&id="+idtematico;
    	axios.post('tematico/guardar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		    	//tematico[data.obj.id] = data.obj;
		    	Psi.sweetAlertConfirm(data.msj);
		    	$("#tematicoModal").modal("hide");
		    	var isactivo=$('#slct_situacion').val();
		    	listar(isactivo);
		    	Tematico.getParents();
		   	}
	    }).catch(e => {
	        vm.errors=e;
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    getParents : function() {
    	axios.post('tematico/allparent', {}).then(response => {
	       	var data = response.data;
	       	pintarPadres(data);
	    }).catch(e => {
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    eliminar : function(idtematico) {
    	var form="&id="+idtematico;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.post('tematico/eliminar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	}
		   	if (data.rst == "1") {
		    	Psi.sweetAlertConfirm(data.msj);
		    	var isactivo=$('#slct_situacion').val();
		    	listar(isactivo);
		    	Tematico.getParents();
		   	}
	    }).catch(e => {
	        vm.errors=e;
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    cambiarestado : function(idtematico, estado) {
    	var form="&id="+idtematico;
    	form+="&estado="+estado;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.get('tematico/cambiarestado?'+form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	}
		   	if (data.rst == "1") {
		    	Psi.sweetAlertConfirm(data.msj);
		    	var isactivo=$('#slct_situacion').val();
		    	listar(isactivo);
		    	Tematico.getParents();
		   	}
	    }).catch(e => {
	        vm.errors=e;
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    getMotivos : function() {
        var form="";
        form+="&_token="+document.querySelector('#token').getAttribute('value');
        axios.post('motivosofsc/list', form).then(response => {
            var data = response.data;
            listMotivosOfsc = data.datos;
            pintarMotivos();
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            //eventoCargaRemover();
        });
    },
    _tbltematicoshijos: function(id, idx ,callback){
        var datos = {"id":id};
        $.ajax({
            url: 'tematico/tematicoshijos',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
            },
            success: function(data) {
                var ids=($("#hdids"+idx).val())+','+id;
                $("#hdids"+idx).val('');
                $("#hdids"+idx).val(ids);

               	var html = '<table class="table table-condensed table-hover" id="t_tematicosDetalle">';
                    html += '<thead class="color-table-thead-success">';
                    html += '<tr>';
                    html += '<td colspan="7" style="background:#EBE8DB;color:#336699;">';
                    html += '<b style="margin-left:10px;"><i id="title'+idx+'"></i></b>';
                    html += '<a class="pull-left" style="cursor: pointer" title="Ir atras" idx="'+idx+'" onclick="Tematico.render_atras(this)"><i class="glyphicon glyphicon-circle-arrow-left"></i></a>'
                    html += '</td>'
                    html += '</tr>';
                    html += '<tr>';
                    html += '<th class="warning"></th>';
                    html += '<th class="warning">Nombre</th>';
                    html += '<th class="warning">Legado</th>';
                    html += '<th class="warning">Actividad</th>';
                    html += '<th class="warning">Tipo</th>';
                    html += '<th class="warning">Estado</th>';
                    html += '<th class="warning">[]</th>';
                    html += '</tr></thead>';
                $.each(data.data, function(key,row){
                	html += '<tbody>';
                    html += '<tr>';
                    html += '<td class="warning"><a class="pull-right" style="cursor: pointer" title="Ver hijos" idtematico="'+row.id+'" idx="'+idx+'" onclick="Tematico.render_tematicoshijos(this)"><i class="glyphicon glyphicon-circle-arrow-right"></i></a></td>';
                    html += '<td class="warning">'+(row.nombre).toUpperCase()+'</td>';
                    html += '<td class="warning">'+row.legado+'</td>';
                    html += '<td class="warning">'+row.actividad+'</td>';
                    html += '<td class="warning">'+row.tipodevolucion+'</td>';
                    if (row.estado == 1) {
	                	html += "<td class='warning'><a class='btn btn-success btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' title='Cambiar Estado'>"+row.estadotematico+"</a></td>";
	                } else {
	                	html += "<td class='warning'><a class='btn btn-danger btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' btn-md btn-cambiar-estado' title='Cambiar Estado'>"+row.estadotematico+"</a></td>";
	                }
	                html += "<td class='warning'><a class='btn btn-primary btn-sm btn-detalle' data-toggle='modal' title='Detalle Tematico' ";
	                html +=" data-target='#tematicoModal' data-id='"+row.id+"' data-tipolegado='"+row.tipo_legado+"'";
	                html +=" data-tipo='"+row.tipo+"' data-parent='"+row.parent+"'";
	                html +=" data-actividadid='"+row.actividad_id+"'>";
	                html +="<i class='fa fa-edit fa-xs'></i></a>";
	                html +="	<a class='btn btn-sm btn-danger btn-eliminar-tematico' data-id='"+row.id+"'><i class='fa fa-trash'></i></a></td>";
                    html += '</tbody><tr>';
                });
                
                callback(html);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
	render_tematicoshijos : function (element){
        var id=$(element).attr("idtematico");
        var idx=$(element).attr("idx");
        
        Tematico._tbltematicoshijos(id,idx,function(result){
	    	$("#t_tematicosDetalle").html(result);
	    	Tematico.title_padres($("#hdids"+idx).val(),idx);
    	});
    },
    render_atras : function (element){
        var idx=$(element).attr("idx");
        var ids=$("#hdids"+idx).val();

        var array = ids.split(',');
        for (var i = 0; i < array.length; i++) {
        	if( (array.length-1) == i ){
        		array.splice((array.length-1), 1);
        	}
        }
        
        var id=array[array.length-1];
		if(array.length>1){

        	Tematico._tbltematicoshijos(id,idx,function(result){
		    	$("#t_tematicosDetalle").html(result);
	    	});
	    	
			var cadena='';
	    	for (var i = 0; i < array.length; i++) {
	    		if( array[i] != '' ){
	    			cadena+=','+array[i];
	    		}	
	        }
	        setTimeout(function(){
	        	$("#hdids"+idx).val('');
	    		$("#hdids"+idx).val(cadena);
	    		Tematico.title_padres($("#hdids"+idx).val(),idx);	
	    	},500);
	    	
        }else{
        	swal("Mensaje","No puedes ir atras!");
        }
    },
    title_padres: function(ids,idx){
        var datos = {"padres":ids};
        $.ajax({
            url: 'tematico/titleparents',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,
            beforeSend: function() {
            },
            success: function(data) {
            	var cadena='';
            	$.each(data.nombres, function(key, val){
                    cadena+=' :: '+(val.nombre).toUpperCase();
                 });
            	$("#title"+idx).html(cadena);
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    carga_masiva: function(data){

        tipo_legado=[];actividad=[];motivos_ofsc=[];tipo_devolucion=[];tematico1=[];tematico2=[];
        tematico3=[];tematico4=[];pendiente=[];pre_devuelto=[];no_realizado=[];pre_liquidado=[];
        completado=[];
        $.each(data,function(key, val) {
            tipo_legado.push(val.tipo_legado);
            actividad.push(val.actividad); 
            motivos_ofsc.push(val.motivo_ofsc); 
            tipo_devolucion.push(val.tipo_devolucion);
            tematico1.push(val.tematico_1);
            tematico2.push(val.tematico_2);
            tematico3.push(val.tematico_3);
            tematico4.push(val.tematico_4);
            pendiente.push(val.pendiente);
            pre_devuelto.push(val.pre_devuelto);            
            no_realizado.push(val.no_realizado);
            pre_liquidado.push(val.pre_liquidado);
            completado.push(val.completado);
        });

        $.ajax({
            url         : 'tematico/cargamasiva',
            type        : 'POST',
            cache       : false,
            data        : {'tipo_legado':tipo_legado,'actividad':actividad,'motivos_ofsc':motivos_ofsc,'tipo_devolucion':tipo_devolucion,
        				   'tematico1':tematico1,'tematico2':tematico2,'tematico3':tematico3,'tematico4':tematico4,'pendiente':pendiente,
        				   'pre_devuelto':pre_devuelto,'no_realizado':no_realizado,'pre_liquidado':pre_liquidado,'completado':completado},
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(response) {
                eventoCargaRemover();
                if( response.rst == 1 ){
                    swal("Success",response.msj,"success");
                    $('#form_reportecargmasiva').fadeOut('slow')
                    $('#btnSubirExcel').html('Subir Archivo Excel').prop('disabled', false);
                    $('.btnAdd').attr('disabled',true);
                    $('.btnSaveALl').attr('disabled',true);
                    $('.btnCancelALl').attr('disabled',true);
                    pintar_tablaCarga('');
                    Tematico.listar('is_pendiente');
                    Tematico.getParents();
                }else{
                    swal("Error",response.msj,"error");
                }
            },
            error: function(){
                eventoCargaRemover();
                swal("Error","Error","error");
            }
        });
    }
}