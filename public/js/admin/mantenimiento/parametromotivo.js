var instance = axios.create({
	baseURL: '/parametromotivo'
});

var instanciaDetalle = axios.create({
	baseURL: '/paramotivodetalle'
});
// var instanceSelect = axios.create({
// 	baseURL: '/parametromotivo'
// });

var vm = new Vue({
	el: '#app',
	data: {
		datos: [],
		objParametroMotivo: {
			estado: 1
		},
		objParametroMotivoDetalle: {
			estado: 1
		},
		motivo: {},
		submotivo: {},
		contador: {},
		accion_contador: {},
		toolbox: {},
		actividad: {},
		empresa: {},
		type: 'Registrar',

		items: [],
		fields: {
			index: {
				label: 'Indice',
				sortable: true
			},
			nombre: {
				label: 'Nombre',
				sortable: true
			},
			tipo_legado: {
				label: 'Tipo Legado',
				sortable: true
			},
			bucket_nombre: {
				label: 'Bucket',
				sortable: true
			},
			estado: {
				label: 'Estado'
			},
			actions: {
				label: '¿Editar?'
			}
		},
		currentPage: 1,
		perPage: 10,
		filter: null,

		/*
		* tabla del detalle
		*/
		datosDetalle: [],
		fieldsDetalle: {
			nombre_motivo_ofsc
			: {
				label: 'Motivo',
				sortable: true
			},
			nombre_submotivo_ofsc: {
				label: 'SubMotivo',
				sortable: true
			},
			contador: {
				label: 'Contador',
				sortable: true
			},
			accion_contador: {
				label: 'Accion Contador',
				sortable: true
			},
			toolbox: {
				label: 'Toolbox',
				sortable: true
			},
			estado: {
				label: 'Estado'
			}
			/*,
			actions: {
				label: '¿Editar?'
			}*/
		},
		currentPageDetalle: 1,
		perPageDetalle: 10,
		filterDetalle: null,
	},

	mounted: function () {
		this.list();
		/*$('#parametroModal').on('show.bs.modal', function (event) {
            $('#form_parametros .slct_bucket').multiselect('select', vm.objParametroMotivo.bucket_id);
			$('#form_parametros .slct_bucket').multiselect('rebuild');

			$('#form_parametros .slct_estado_ofsc').multiselect('select', vm.objParametroMotivo.estado_ofsc_id);
			$('#form_parametros .slct_estado_ofsc').multiselect('rebuild');
        });*/

        $('#parametroModal').on('hide.bs.modal', function (event) {
            $('#form_parametros .slct_bucket').multiselect('select', "");
			$('#form_parametros .slct_bucket').multiselect('rebuild');
			$('#form_parametros .slct_estado_ofsc').multiselect('select', "");
			$('#form_parametros .slct_estado_ofsc').multiselect('rebuild');
			vm.cleanForm();
        });
	},

	methods: {
		list: function () {
			instance.post('/list')
				.then((response) => {
					this.datos = response.data.datos;
				})
		},

		find: function (obj) {
			this.type = "Actualizar";
			instance.post('/buscar', obj)
				.then((response) => {
					let data = response.data.datos[0].motivodetalle;
					$.each(data,function(index, el) {
						el.nombre_motivo_ofsc = (el.motivosofsc) ? el.motivosofsc.descripcion : '';
						el.nombre_submotivo_ofsc = (el.submotivosofsc) ? el.submotivosofsc.descripcion : '';
					});
					this.datosDetalle = data;
					this.objParametroMotivo = response.data.datos[0];

					$('#form_parametros .slct_bucket').multiselect('select', this.objParametroMotivo.bucket_id);
					$('#form_parametros .slct_bucket').multiselect('rebuild');
				})
		},
		update: function () {
			detalle = [];
			$("#tbDetalle tbody tr[class='new']").each(function(index, el) {
				key = index + 1;
				var obj = {};	
				obj.motivo_ofsc_id = $(el).find('#tdmotivo').find('#slct_motivo_ofsc'+key).val();
				obj.submotivo_ofsc_id = $(el).find('#tdsubmotivo').find('#slct_submotivo_ofsc'+key).val();
				obj.contador = $(el).find('#tdcontador').find('#txt_contador').val();
				obj.accion_contador = $(el).find('#tdaccioncont').find('#txt_accionContador').val();
				obj.toolbox = $(el).find('#tdtoolbox').find('#txt_toolbox').val();
				detalle.push(obj);
			});

			data = (this.objParametroMotivo.motivodetalle) ? delete this.objParametroMotivo.motivodetalle : '';
			if(detalle.length > 0){
				this.objParametroMotivo.detalle = detalle;
			}

			instance.post('/action', this.objParametroMotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
						this.cleanForm();
						$("#parametroModal").modal('hide');
					}
				})
		},

		setEstado: function (obj) {
			this.objParametroMotivo = obj;
			this.objParametroMotivo.estado = (this.objParametroMotivo.estado == 1) ? 0 : 1;
			instance.post('/action', this.objParametroMotivo)
				.then((response) => {
					if (response.data.rst == 1) {
						this.list();
						this.cleanForm();
					}
				})
		},
		setEstadoDetalle: function(obj){
			this.objParametroMotivoDetalle = obj;
			this.objParametroMotivoDetalle.estado = (this.objParametroMotivoDetalle.estado == 1) ? 0 : 1;
			instanciaDetalle.post('/update', this.objParametroMotivoDetalle)
				.then((response) => {
					if (response.data.rst == 1) {
						
					}
				})
		},
        cleanForm: function () {
			this.type = "Registrar";
			this.objParametroMotivo = {estado: 1};
			this.datosDetalle = [];
			$(".new").remove();
		},
		clone: function(){
			var rows = $(".tbDetalle tbody tr").length;
			var template = $(".tbDetalle tbody").find('.trAdd').clone().removeClass('trAdd').removeClass('hidden').addClass('new');
			var motivo_id = template.find('#slct_motivo_ofsc').attr('id') + rows;
			var submotivo_id = template.find('#slct_submotivo_ofsc').attr('id') + rows;
			template.find('#slct_motivo_ofsc').attr('id',motivo_id);
			template.find('#slct_submotivo_ofsc').attr('id',submotivo_id);
	        $(".tbDetalle tbody").append(template);

	        slctGlobal.listarSlctpost('motivosofsc','cargar','tbDetalle .new #'+motivo_id,'simple',null,null,0,1);
            slctGlobal.listarSlctpost('submotivoofsc','listar','tbDetalle .new #'+submotivo_id,'simple',null,null,0,1);
		}
	}
})