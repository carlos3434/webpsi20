var idtematico = 0;
var tabs = {};
$("#t_pendiente").DataTable().destroy();
$("#t_pendiente").DataTable();
$(document).ready(function(){
	$("#tabestados li").each(function(index, data){
		var filtro = $(this).data("filtro");
		tabs[filtro] = false;
	});
	$("#tabestados li").click(function(e){
		setTabActivo($(this));
	});
	$("#slct_actividad_id").change(function(e){
		visibleTipoDevolucion($(this).val());
	});
	$("#slct_opciones").change(function(e){
		var actividad = $("#slct_actividad_id option:selected");
		visibleTipoDevolucion(actividad.val());
	});
	$("#btn-buscar").click(function(e){
        var situaction=$('#slct_situacion').val();
        var actividad=$('#slct_actividad_id_filtro').val();
        var tipo_devolucion=$('#slct_tipodevolucion_filtro').val();
        var tipo_legado=$('#slct_tipo_legado_filtro').val();
        
        if( situaction == '' ){
            situaction = 'is_pendiente';
        }

        listar(situaction);
	});
	$(document).delegate(".btn-eliminar-tematico", "click", function(e){
		var idtematico = $(this).data("id");
		eliminar(idtematico);
	});
	$(document).delegate(".btn-cambiar-estado", "click", function(e){
		var idtematico = $(this).data("id");
		var estado = $(this).data("estado");
		cambiarestado(idtematico, estado);
	});
	slctGlobalHtml('slct_opciones','multiple');
	slctGlobalHtml('slct_tipo_legado', 'simple');
	slctGlobalHtml('slct_actividad_id', 'simple');
	slctGlobalHtml('slct_tipo', 'simple');
	slctGlobalHtml('slct_actividad_id_filtro', 'simple');
	slctGlobalHtml('slct_tipo_legado_filtro', 'simple');
    slctGlobalHtml('slct_tipodevolucion_filtro', 'simple');
    slctGlobalHtml('slct_situacion', 'simple');

    $("#btn_tematico").click(Tematico.guardarTematico);
	Tematico.getParents();
    Tematico.getMotivos();
    listar('is_pendiente');
    slctGlobalHtml('slct_situacion','simple', 'is_pendiente');
    pintarMotivos_masiva('','');
    $('.btnAdd,.btnSaveALl,.btnCancelALl').attr('disabled',true);
});

listar  = function (isactivo) {
	$("#t_tematicos").DataTable().destroy();
	Tematico.listar(isactivo);
};
eliminar = function(idtematico) {
	swal({
        title: '¿Desea eliminar el tematico?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Eliminar'
    }).then(function () {
        Tematico.eliminar(idtematico);
    });
};
cambiarestado = function(idtematico, estado) {
	swal({
        title: '¿Desea cambiar el estado del tematico?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Cambiar'
    }).then(function () {
        Tematico.cambiarestado(idtematico, estado);
    });
};
setTabActivo = function (fila) {
	var tabactivo = null;
	isactivo = fila.data("filtro");
	if (tabs[isactivo]) {
		return;
	}
	var id = fila.id;
	for (var i in tabs) {
		tabs[i] = false;
	}
	if (isactivo!=null) {
		tabs[isactivo] = true;
		listar(isactivo);
	}
};
detalle = function(tematico) {
    $("#txt_nombre").val(tematico.nombre);
    $("#txt_descripcion").val(tematico.descripcion);

    $("#slct_parent").multiselect('select', [tematico.parent]);
    $("#slct_parent").multiselect('rebuild');

    $("#slct_tipo_legado").multiselect('select', [tematico.tipo_legado]);
    $("#slct_tipo_legado").multiselect('rebuild');

    $("#slct_actividad_id").multiselect('select', [tematico.actividad_id]);
    $("#slct_actividad_id").multiselect('rebuild');

    var opciones = [];
    if (tematico.is_pendiente == 1) {
    	opciones.push("is_pendiente");
    }
    if (tematico.is_pre_devuelto == 1) {
    	opciones.push("is_pre_devuelto");
    	$("#div_tipo_devolucion").css("display", "block");
    	if (tematico.actividad_id == 1) {
    		$("#div_tipo_devolucion").css("display", "none");
    	}
    } else {
    	$("#div_tipo_devolucion").css("display", "none");
    }
    if (tematico.is_no_realizado == 1) {
    	opciones.push("is_no_realizado");
    	$("#div_tipo_devolucion").css("display", "block");
    	if (tematico.actividad_id == 1) {
    		$("#div_tipo_devolucion").css("display", "none");
    	}
    } else {
    	$("#div_tipo_devolucion").css("display", "none");
    }
    if (tematico.is_pre_liquidado == 1) {
    	opciones.push("is_pre_liquidado");
    }
    if (tematico.is_completado == 1) {
    	opciones.push("is_completado");
    }
    $("#slct_tipo").multiselect('select',[tematico.tipo]);
    $("#slct_opciones").multiselect('select', opciones);
    $("#slct_opciones").multiselect('rebuild');

    $("#tematicoModal .modal-title").text("Editar : "+tematico.nombre);
};
pintarPadres = function(padres) {
	html = "<option value=''>.:: Seleccione ::.</option>";
	for (var i in padres) {
		html+="<option value='"+padres[i].id+"'><b>"+padres[i].actividad+" : </b>"+padres[i].nombre+"|"+padres[i].padre+"</option>";
	}
	$("#slct_parent").multiselect("destroy");
	$("#slct_parent").html(html);
	slctGlobalHtml('slct_parent','simple');
};
pintarMotivos = function () {
    html = "<option value=''>.:: Seleccione ::.</option>";
    for (var i in listMotivosOfsc) {
        var motivoofsc = listMotivosOfsc[i];
        if (motivoofsc.actividad_id!=null && motivoofsc.actividad_id!="null" && motivoofsc.tipo_legado!="null" && motivoofsc.tipo_legado!=null) {
            var marcaactividad = "";
            if (parseInt(motivoofsc.actividad_id) == 1) {
                marcaactividad = "<b>Ave.</b>";
            }
            if (parseInt(motivoofsc.actividad_id) == 2) {
                marcaactividad = "<b>Prov.</b>";
            }
            if (marcaactividad!="") {
                html+="<option value='"+motivoofsc.id+"'>"+marcaactividad+" "+motivoofsc.descripcion+"</option>";
            }
        }
    }
    $("#slct_motivos_ofsc").html(html);
    slctGlobalHtml('slct_motivos_ofsc', 'simple');
    //slctGlobalHtml('slct_motivoofsc_masiva', 'simple');
};
pintarMotivos_masiva = function (tipo_legado,actividad) {
    //$(".slct_motivoofsc_masiva").multiselect('destroy');
    html = "<option value=''>.:: Seleccione ::.</option>";
    for (var i in listMotivosOfsc) {
        var motivoofsc = listMotivosOfsc[i];
        if (motivoofsc.actividad_id!=null && motivoofsc.actividad_id!="null" && motivoofsc.tipo_legado!="null" && motivoofsc.tipo_legado!=null) {
            if(tipo_legado==motivoofsc.tipo_legado && actividad==motivoofsc.actividad_id){
               html+="<option value='"+motivoofsc.descripcion+"'>"+motivoofsc.descripcion+"</option>"; 
            }
        }
    }
    $("#slct_motivoofsc_masiva").html(html);
    //slctGlobalHtml('slct_motivoofsc_masiva', 'simple');
};
visibleTipoDevolucion = function(val) {
	if (val == 1) {
			$("#div_tipo_devolucion").css("display", "none");
	} else {
		var opciones = [];
		$("#slct_opciones option:selected").each(function(){
			opciones.push($(this).val());
		});
		if ($.inArray("is_pre_devuelto", opciones) >= 0 || $.inArray("is_no_realizado", opciones) >= 0) {
			$("#div_tipo_devolucion").css("display", "block");
		} else {
			$("#div_tipo_devolucion").css("display", "none");
		}
	}
};
isactivo = "is_pendiente";
listar(isactivo);

var enforceModalFocusFn;
$('#tematicoModal').on('show.bs.modal', function (event) {
    enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    var button = $(event.relatedTarget);
    var id = button.data("id");
    if (id > 0) {
    	idtematico = id;
    	Tematico.get(id);
    }
});
$('#tematicoModal').on('hide.bs.modal', function (event) {
    $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    var modal = $(this);
    modal.find('.modal-body input[type=text]').val('');
    modal.find('.modal-body textarea').val('');
    $('#slct_parent, #slct_tipo_legado, #slct_actividad_id').multiselect('select', [""]);
    $('#slct_parent, #slct_tipo_legado, #slct_actividad_id').multiselect('rebuild');
    $("#slct_opciones option:selected").removeAttr('selected').prop('selected', false);
    $("#slct_opciones").multiselect('refresh');
    $("#div_tipo_devolucion").css("display", "none");
    modal.find(".modal-title").text("Nuevo Tematico");
    idtematico = 0;
});

 $('#myTab a').click(function (e) {
  e.preventDefault();
  if(($(this).attr('href') == "#tb_carga")) {
    $('#tb_cargamasiva').hide();
    $('#tb_carga').fadeIn('slow');
  }
  if(($(this).attr('href') == "#tb_cargamasiva")) {
    $('#tb_carga').hide();
    $('#tb_cargamasiva').fadeIn('slow');
  }
});

$(document).on('change','#slct_situacion',function(e) {
    e.preventDefault();
    var situaction=$('#slct_situacion').val();

    if( situaction == '' ){
        situaction = 'is_pendiente';
    }
    
    $('#slct_actividad_id_filtro,#slct_tipodevolucion_filtro,#slct_tipo_legado_filtro').multiselect('select', [""]);
    $('#slct_actividad_id_filtro,#slct_tipodevolucion_filtro,#slct_tipo_legado_filtro').multiselect('refresh');
    listar(situaction);
});


$('#t_tematicos tbody').on('click', 'td.details-control', function (e) {
    e.preventDefault();
    var tr = $(this).closest('tr');
    var row = $("#t_tematicos").DataTable().row( tr );
    var idx = $("#t_tematicos").DataTable().row( tr ).index();
    var count=($("#t_tematicos").DataTable().data()).length;

    var c=0;
    for (var i = 0; i < count; i++) {
        var action=$("#action"+i).val();
        if(action==1){
            c++;
        }
    }
    var action=$("#action"+idx).val();
    if(action==0 && c>0){
        swal("Mensaje","Terminar accion anterior!");
    }else{
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
            $("#hdids"+idx).val('');
            $("#action"+idx).val(0);
        }else {
            tr.addClass('shown');
            Tematico._tbltematicoshijos((row.data().id),idx,function(result){
                row.child(result).show();
                $("#action"+idx).val(1);
                Tematico.title_padres($("#hdids"+idx).val(),idx); 
            });
        }
    }
});


$('#btnSubir').click(function() {
    $('#btnHiddenExcel input').click();
    return false;
});

$("#btnHiddenExcel").ajaxUpload({
    action: 'tematico/uploadexcel',
    url: 'tematico/uploadexcel',
    name: 'excel',
    onSubmit: function(file, extension) {
        $('#btnSubir').html('Cargando...').prop('disabled', true);
    },
    onComplete: function(result) {
        var result = JSON.parse(result);
        if( result.rst == 1){
            swal("Success","Excel cargado correctamente!","success");
            pintar_tablaCarga(result.data);
            $('#form_reportecargmasiva').fadeIn('slow')
            $('#btnSubir').html('Listo').prop('disabled', true);
            $('.btnSaveALl').attr('disabled',false);
            $('.btnCancelALl').attr('disabled',false);
            $('.btnAdd').attr('disabled',false);
        }else{
            swal("Error",result.msj,"error");
            $('#btnSubir').html('Subir Archivo .Csv').prop('disabled', false);
        }
    }
});

pintar_tablaCarga= function(data){
    var table =$('#tbl_cargamasiva').DataTable({
        lengthChange: false,
        lengthMenu: [ [5, 10], [5, 10] ],
        pageLength: 10,
        paging: true,
        info: true,
        data: (Object.values(data)),
        searching: false,
        deferRender:true,
        scrollCollapse: true,
        scroller:false,
        destroy:true,
        columns: [
            { data: "tipo_legado"},
            { data: "actividad"},
            { data: "motivo_ofsc"},
            { data: "tipo_devolucion"},
            { data: "tematico_1"},
            { data: "tematico_2"},
            { data: "tematico_3"},
            { data: "tematico_4"},
            { data: "pendiente"},
            { data: "pre_devuelto"},
            { data: "no_realizado"},
            { data: "pre_liquidado"},
            { data: "completado"},
            {  "data": null,"sortable": false,
                render: function ( row, type, full, meta ) {
                    htmlButtons="";
                    htmlButtons ="  <a class='btn btn-danger btn-xs btnRemove' data-toggle='modal' title='Eliminar registro'><i class='glyphicon glyphicon-remove'></i></a>";
                    htmlButtons+="  <a class='btn btn-success btn-xs btnEdit' data-toggle='modal' title='Editar registro'><i class='glyphicon glyphicon-pencil'></i></a>";
                    return htmlButtons;
                }
            }      
        ],
        ordering: true,
        order: [[0, "asc"], [1, "asc"],[2, "asc"],[3, "asc"],[4, "asc"],[5, "asc"],[6, "asc"],[7, "asc"]]
    });
}

$('.btnCancelALl').click(function() {

    $('#btnSubir').html('Subir Archivo .Csv').prop('disabled', false);
    $('#form_reportecargmasiva').fadeOut('slow')
    $('.alert_modal').fadeOut('slow');
    $('.alert_modal_success').fadeOut('slow');
    $('.btnSaveALl').attr('disabled',true);
    $('.btnCancelALl').attr('disabled',true);
    $('.btnAdd').attr('disabled',true);
    pintar_tablaCarga('');

});

$(document).on('click', '.btnAdd', function(event) {

    event.preventDefault();
    if( $('.btnSaveAdd').is(":visible") || $('.btnUpdateEdit').is(":visible") ){
        swal("Mensaje",'Terminar accion anterior!');
    }else{
        fn_limpiar_masiva();

        $('.btnSaveAdd').show();$('.btnCancelAdd').show();
        $('.btnUpdateEdit').hide();$('.btnCancelEdit').hide();

        $("#tbl_cargamasiva tfoot").find('.repeat').removeClass('hidden');
    }

});

$(document).on('click','.btnRemove',function(event){

    var tr = $(this).parent().parent().find('tr');
    var index=tr.prevObject[0]._DT_RowIndex;
    var table = $('#tbl_cargamasiva').DataTable();
    table.row( index ).remove().draw();
    Psi.mensaje('success', 'Datos Eliminados Correctamente!', 4000);

});

$(document).on('click', '.btnEdit', function(event) {

    if( $('.btnSaveAdd').is(":visible") || $('.btnUpdateEdit').is(":visible") ){
        swal("Mensaje",'Terminar accion anterior!');
    }else{
        var tds = $(this).parent().parent().find('td');
        var template = $("#tbl_cargamasiva tfoot").find('.repeat').clone().removeClass('repeat').removeClass('hidden');
        var data2=[];
        $.each(tds,function(key, val) {
            var data = (val.textContent).toUpperCase(); 
            data2.push(data);            
        }); 
        $(this).parent().parent().replaceWith(template);

        //hidden
        $('.hd_tipolegado').val(data2[0]);$('.hd_actividad').val(data2[1]);
        $('.hd_motivoofsc').val(data2[2]);$('.hd_tipodevolucion').val(data2[3]);
        $('.hd_tematico1').val(data2[4]);$('.hd_tematico2').val(data2[5]);
        $('.hd_tematico3').val(data2[6]);$('.hd_tematico4').val(data2[7]);
        $('.hd_pendiente').val(data2[8]);$('.hd_predevuelto').val(data2[9]);
        $('.hd_norealizado').val(data2[10]);$('.hd_preliquidado').val(data2[11]);
        $('.hd_completado').val(data2[12]);

        //data
        tipo_legado='';
        if(data2[0]=='CMS'){tipo_legado=1;}
        if(data2[0]=='GESTEL'){tipo_legado=2;}
        actividad='';
        if(data2[1]=='AVERIA'){actividad=1;}
        if(data2[1]=='PROVISION'){actividad=2;}
        pintarMotivos_masiva(tipo_legado,actividad);

        $(".slct_tipolegado_masiva").val(data2[0]);
        $(".slct_actividad_masiva").val(data2[1]);
        $(".slct_motivoofsc_masiva").val(data2[2]);
        $(".slct_tipodevolucion_masiva").val(data2[3]);
        $('.txt_tematico1_masiva').val(data2[4]);
        $('.txt_tematico2_masiva').val(data2[5]);
        $('.txt_tematico3_masiva').val(data2[6]);
        $('.txt_tematico4_masiva').val(data2[7]);

        if(data2[8]=='SI'){
            document.getElementById("chk_pendiente_masiva").checked = true;  
        }else{
            document.getElementById("chk_pendiente_masiva").checked = false;  
        }

        if(data2[9]=='SI'){
            document.getElementById("chk_predevuelto_masiva").checked = true;  
        }else{
            document.getElementById("chk_predevuelto_masiva").checked = false;  
        }

        if(data2[10]=='SI'){
            document.getElementById("chk_norealizado_masiva").checked = true;
        }else{
            document.getElementById("chk_norealizado_masiva").checked = false;  
        }

        if(data2[11]=='SI'){
            document.getElementById("chk_preliquidado_masiva").checked = true;  
        }else{
            document.getElementById("chk_preliquidado_masiva").checked = false;  
        }

        if(data2[12]=='SI'){
            document.getElementById("chk_completado_masiva").checked = true;
        }else{
            document.getElementById("chk_completado_masiva").checked = false;
        }

        var tr = $(this).parent().parent().find('tr');
        var index=tr.prevObject[0]._DT_RowIndex;
        $('.hd_index').val(index);

        $('.btnUpdateEdit').show();$('.btnCancelEdit').show();
        $('.btnSaveAdd').hide();$('.btnCancelAdd').hide();
    }

});

$(document).on('click','.btnUpdateEdit',function(event){

    tipo_legado=$('.slct_tipolegado_masiva').val();
    actividad=$('.slct_actividad_masiva').val();
    motivo_ofsc=$('.slct_motivoofsc_masiva').val();
    tipo_devolucion=$('.slct_tipodevolucion_masiva').val();
    tematico_1=($('.txt_tematico1_masiva').val()).toUpperCase();
    tematico_2=($('.txt_tematico2_masiva').val()).toUpperCase();
    tematico_3=($('.txt_tematico3_masiva').val()).toUpperCase();
    tematico_4=($('.txt_tematico4_masiva').val()).toUpperCase();
    pendiente='NO';
    if ($('.chk_pendiente_masiva').is(':checked')) {pendiente='SI';}
    pre_devuelto='NO';
    if ($('.chk_predevuelto_masiva').is(':checked')) {pre_devuelto='SI';}
    no_realizado='NO';
    if ($('.chk_norealizado_masiva').is(':checked')) {no_realizado='SI';}
    pre_liquidado='NO';
    if ($('.chk_preliquidado_masiva').is(':checked')) {pre_liquidado='SI';}
    completado='NO';
    if ($('.chk_completado_masiva').is(':checked')) {completado='SI';}

    /*validar*/
    if(tipo_legado==''){swal("Mensaje", 'Seleccione "Tipo legado" vacio!');return false;}
    if(actividad==''){swal("Mensaje", 'Seleccione "Actividad"!');return false;}
    if(actividad=='PROVISION' && tipo_devolucion==''){swal("Mensaje", 'Seleccione "Tipo devolucion"!');return false;}
    if(tematico_1==''){swal("Mensaje", 'Campo "Tematico 1" vacio!');return false;}
    if(pendiente=='NO' && pre_devuelto=='NO' && no_realizado=='NO' && pre_liquidado=='NO' && completado=='NO'){
        swal("Mensaje", 'Elegir algun estado: <br>.::Pendiente::.<br>.::Pre devuelto::.<br>.::No realizado::.<br>.::Pre liquidado::.<br>.::Completado::.');return false;
    }

    if(pendiente=='SI' && (pre_devuelto=='SI' || no_realizado=='SI' || pre_liquidado=='SI' || completado=='SI')) {
        swal("Mensaje", 'Si estado es "Pendiente", no puede seleccionar otros estados!');return false;
    }

    if((pre_devuelto=='SI' || no_realizado=='SI') && (pre_liquidado=='SI' || completado=='SI')) {
        swal("Mensaje", 'Si estado es "Pre devuelto y/o No realizado", no puede seleccionar "Pre liquidado y/o Completado"!');return false;
    }

    var cont=0;
    var table = $('#tbl_cargamasiva').DataTable();
    var data =[];
    data=table.data();
    var index=$('.hd_index').val();

    $.each(data,function(key, val) {
        if( key!=index && (val.tipo_legado).toUpperCase() == tipo_legado && (val.actividad).toUpperCase() == actividad &&
           (val.tipo_devolucion).toUpperCase() == tipo_devolucion && (val.tematico_1).toUpperCase() == tematico_1 && 
           (val.tematico_2).toUpperCase() == tematico_2 && (val.tematico_3).toUpperCase() == tematico_3 && 
           (val.tematico_4).toUpperCase() == tematico_4 ){
            cont++;
        }
                  
    });

    if( cont == 0 ){
        table.row( index ).remove().draw();
        
        table.row.add({ 
            tipo_legado: tipo_legado,
            actividad: actividad,
            motivo_ofsc: motivo_ofsc,
            tipo_devolucion: tipo_devolucion,
            tematico_1:tematico_1,
            tematico_2:tematico_2,
            tematico_3:tematico_3,
            tematico_4:tematico_4,
            pendiente:pendiente,
            pre_devuelto:pre_devuelto,
            no_realizado:no_realizado,
            pre_liquidado:pre_liquidado,
            completado:completado
        }).draw();

        Psi.mensaje('success', 'Datos Actualizados Correctamente!', 4000);
    }else{
        Psi.mensaje('danger', 'Tipo legado, Actividad, Tipo devolucion y Tematicos ya existe!', 7000);
    }
});

$(document).on('click','.btnCancelEdit',function(event){

    tipo_legado=$('.hd_tipolegado').val();
    actividad=$('.hd_actividad').val();
    motivo_ofsc=$('.hd_motivoofsc').val();
    tipo_devolucion=$('.hd_tipodevolucion').val();
    tematico_1=$('.hd_tematico1').val();
    tematico_2=$('.hd_tematico2').val();
    tematico_3=$('.hd_tematico3').val();
    tematico_4=$('.hd_tematico4').val();
    pendiente=$('.hd_pendiente').val();
    pre_devuelto=$('.hd_predevuelto').val();
    no_realizado=$('.hd_norealizado').val();
    pre_liquidado=$('.hd_preliquidado').val();
    completado=$('.hd_completado').val();
    
    var index=$('.hd_index').val();
    var table = $('#tbl_cargamasiva').DataTable();
    table.row( index ).remove().draw();
    
    table.row.add({ 
        tipo_legado: tipo_legado,
        actividad: actividad,
        motivo_ofsc: motivo_ofsc,
        tipo_devolucion: tipo_devolucion,
        tematico_1:tematico_1,
        tematico_2:tematico_2,
        tematico_3:tematico_3,
        tematico_4:tematico_4,
        pendiente:pendiente,
        pre_devuelto:pre_devuelto,
        no_realizado:no_realizado,
        pre_liquidado:pre_liquidado,
        completado:completado
    }).draw();

});

$(document).on('click','.btnCancelAdd',function(event){

   $("#tbl_cargamasiva tfoot").find('.repeat').addClass('hidden');
   fn_limpiar_masiva();

});

$(document).on('change','.slct_tipolegado_masiva',function(e) {
    e.preventDefault();
    tipo_legado='';
    if($('.slct_tipolegado_masiva').val()=='CMS'){tipo_legado=1;}
    if($('.slct_tipolegado_masiva').val()=='GESTEL'){tipo_legado=2;}
    actividad='';
    if($('.slct_actividad_masiva').val()=='AVERIA'){actividad=1;}
    if($('.slct_actividad_masiva').val()=='PROVISION'){actividad=2;}
    
    pintarMotivos_masiva(tipo_legado,actividad);
});

$(document).on('change','.slct_actividad_masiva',function(e) {
    e.preventDefault();
    if($('.slct_tipolegado_masiva').val()=='CMS'){tipo_legado=1;}
    if($('.slct_tipolegado_masiva').val()=='GESTEL'){tipo_legado=2;}
    actividad='';
    if($('.slct_actividad_masiva').val()=='AVERIA'){actividad=1;}
    if($('.slct_actividad_masiva').val()=='PROVISION'){actividad=2;}
    
    pintarMotivos_masiva(tipo_legado,actividad);
});

$(document).on('click','.btnSaveAdd',function(event){

    tipo_legado=$('.slct_tipolegado_masiva').val();
    actividad=$('.slct_actividad_masiva').val();
    motivo_ofsc=$('.slct_motivoofsc_masiva').val();
    tipo_devolucion=$('.slct_tipodevolucion_masiva').val();
    tematico_1=($('.txt_tematico1_masiva').val()).toUpperCase();
    tematico_2=($('.txt_tematico2_masiva').val()).toUpperCase();
    tematico_3=($('.txt_tematico3_masiva').val()).toUpperCase();
    tematico_4=($('.txt_tematico4_masiva').val()).toUpperCase();
    pendiente='NO';
    if ($('.chk_pendiente_masiva').is(':checked')) {pendiente='SI';}
    pre_devuelto='NO';
    if ($('.chk_predevuelto_masiva').is(':checked')) {pre_devuelto='SI';}
    no_realizado='NO';
    if ($('.chk_norealizado_masiva').is(':checked')) {no_realizado='SI';}
    pre_liquidado='NO';
    if ($('.chk_preliquidado_masiva').is(':checked')) {pre_liquidado='SI';}
    completado='NO';
    if ($('.chk_completado_masiva').is(':checked')) {completado='SI';}

    /*validar*/
    if(tipo_legado==''){swal("Mensaje", 'Seleccione "Tipo legado"!');return false;}
    if(actividad==''){swal("Mensaje", 'Seleccione "Actividad"!');return false;}
    if(actividad=='PROVISION' && tipo_devolucion==''){swal("Mensaje", 'Seleccione "Tipo devolucion"!');return false;}
    if(tematico_1==''){swal("Mensaje", 'Campo "Tematico 1" vacio!');return false;}
    if(pendiente=='NO' && pre_devuelto=='NO' && no_realizado=='NO' && pre_liquidado=='NO' && completado=='NO'){
        swal("Mensaje", 'Elegir algun estado: <br>.::Pendiente::.<br>.::Pre devuelto::.<br>.::No realizado::.<br>.::Pre liquidado::.<br>.::Completado::.');return false;
    }

    if(pendiente=='SI' && (pre_devuelto=='SI' || no_realizado=='SI' || pre_liquidado=='SI' || completado=='SI')) {
        swal("Mensaje", 'Si estado es "pendiente", no puede seleccionar otros estados!');return false;
    }

    if((pre_devuelto=='SI' || no_realizado=='SI') && (pre_liquidado=='SI' || completado=='SI')) {
        swal("Mensaje", 'Si estado es "Pre devuelto y/o No realizado", no puede seleccionar "Pre liquidado y/o Completado"!');return false;
    }
    
    var cont=0;
    var table = $('#tbl_cargamasiva').DataTable();
    var data =[];
    data=table.data();
    $.each(data,function(key, val) {
        if((val.tipo_legado).toUpperCase() == tipo_legado && (val.actividad).toUpperCase() == actividad &&
           (val.tipo_devolucion).toUpperCase() == tipo_devolucion && (val.tematico_1).toUpperCase() == tematico_1 && 
           (val.tematico_2).toUpperCase() == tematico_2 && (val.tematico_3).toUpperCase() == tematico_3 && 
           (val.tematico_4).toUpperCase() == tematico_4){
            cont++;
        }          
    });

    if( cont == 0 ){
        table.row.add( { 
            tipo_legado: tipo_legado,
            actividad: actividad,
            motivo_ofsc: motivo_ofsc,
            tipo_devolucion: tipo_devolucion,
            tematico_1:tematico_1,
            tematico_2:tematico_2,
            tematico_3:tematico_3,
            tematico_4:tematico_4,
            pendiente:pendiente,
            pre_devuelto:pre_devuelto,
            no_realizado:no_realizado,
            pre_liquidado:pre_liquidado,
            completado:completado
        }).draw();

        Psi.mensaje('success', 'Datos Registrados Correctamente!', 4000);
        fn_limpiar_masiva();
        $("#tbl_cargamasiva tfoot").find('.repeat').addClass('hidden');
    }else{

        Psi.mensaje('danger', 'Tipo legado, Actividad, Tipo devolucion y Tematicos ya existe!', 7000);
    }
});

$(document).on('click','.btnSaveALl',function(event){

    swal({
      title: "Esta usted seguro?",
      text: "Porfavor, elegir accion!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
        var table = $('#tbl_cargamasiva').DataTable();
        var data =[];
        data=table.data();
        Tematico.carga_masiva(data);
    }, function (dismiss) {
        if (dismiss === 'cancel') {}
  })

});

fn_limpiar_masiva = function(){
    $('.slct_tipolegado_masiva,.slct_actividad_masiva,.slct_tipodevolucion_masiva').val('');
    pintarMotivos_masiva('','');        
    $('.txt_tematico1_masiva').val('');
    $('.txt_tematico2_masiva').val('');
    $('.txt_tematico3_masiva').val('');
    $('.txt_tematico4_masiva').val('');
    $('.chk_pendiente_masiva').attr('checked',false);
    $('.chk_predevuelto_masiva').attr('checked',false);
    $('.chk_norealizado_masiva').attr('checked',false);
    $('.chk_preliquidado_masiva').attr('checked',false);
    $('.chk_completado_masiva').attr('checked',false);
}
