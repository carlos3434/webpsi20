axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
listOpciones = {};
objperfil = {id :0};
var Perfil = {
	listar : function () {
	    return $("#t_perfil").DataTable({
	        "processing": true,
	        "serverSide": true,
	        "stateSave": true,
	        "stateLoadCallback": function (settings) {
	        },
	        "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
	        },
	        ajax: function(data, callback, settings) {
	            Perfil.ajaxlistado(data,callback);
	        },
	        "columns":[
	            { data: 'id', name:'id'},
	            { data: 'nombre', name:'nombre'},
	            {data : function( row, type, val, meta) {
	                var htmlButtons = "";
	                if (row.estado == 1) {
	                	htmlButtons = "<a class='btn btn-success btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' title='Cambiar Estado'>"+row.estadoperfil+"</a>";
	                } else {
	                	htmlButtons = "<a class='btn btn-danger btn-md btn-cambiar-estado' data-id='"+row.id+"' data-estado='"+row.estado+"' btn-md btn-cambiar-estado' title='Cambiar Estado'>"+row.estadoperfil+"</a>";
	                }
	                return htmlButtons;
	            }, name: "estado"},
	            {data : function( row, type, val, meta) {
	                var htmlButtons = "";
	                htmlButtons = "<a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' title='Detalle Motivos Usuario' ";
	                htmlButtons+=" data-target='#PerfilModal' data-id='"+row.id+"' data-nombre='"+row.nombre+"' style='margin-right:5px;' >";
	                htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
	                htmlButtons+="<a class='btn btn-xs btn-danger btn-eliminar-perfil' data-id='"+row.id+"'><i class='fa fa-trash'></i></a>";
	                return htmlButtons;
	            }, name: "botones"},
	        ],
	        paging: true,
	        lengthChange: true,
	        searching: true,
	        ordering: true,
	        order: [[ 0, "desc" ]],
	        info: true,
	        autoWidth: true,
	        language: config
	    });
	},
	ajaxlistado : function(request,callback){
        form="&_token="+document.querySelector('#token').getAttribute('value');
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+='&search='+request.search.value;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        form+="&opt=mantenimiento";
        eventoCargaMostrar();
        axios.post('perfil/listar', form).then(response => {
            callback(response.data);
        }).catch(e => {
            console.log(e);
        }).then(() => {
            eventoCargaRemover();
        });
    },
    get : function(idusuario) {
    	eventoCargaMostrar();
    	form="&id="+idusuario;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
	    axios.get('perfil/editar?'+form).then(response => {
	        var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		    }
		    if (data.rst == "1") {
		    	objperfil = data.obj;
		    	detalle(data.obj);
		    }
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    guardar : function() {
    	var form ="&_token="+document.querySelector('#token').getAttribute('value');
    	form+= $("#form_perfil").serialize().split("txt_").join("").split("slct_").join("").split("chck_").join("");
    	form+="&id="+idperfil;
    	axios.post('perfil/guardar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		    	Psi.sweetAlertConfirm(data.msj);
		    	$("#PerfilModal").modal("hide");
		    	listar();
		   	}
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    eliminar : function(id) {
    	var form="&id="+id;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.post('perfil/eliminar', form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	}
		   	if (data.rst == "1") {
		    	Psi.sweetAlertConfirm(data.msj);
		    	listar();
		   	}
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    cambiarestado : function(idperfil, estado) {
    	var form="&id="+idperfil;
    	form+="&estado="+estado;
	    form+="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.get('perfil/cambiarestado?'+form).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	}
		   	if (data.rst == "1") {
		    	Psi.sweetAlertConfirm(data.msj);
		    	listar();
		   	}
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    listOpciones : function() {
    	var form="&_token="+document.querySelector('#token').getAttribute('value');
    	axios.post('opcion/listar', form).then(response => {
	       	var data = response.data.datos;
	        $.each(data, function(index, data){
	        	listOpciones[data.id] = data;
	        });
	        pintarSelectOpcion();
	    }).catch(e => {
	        console.log(e);
	    }).then(() => {
	        eventoCargaRemover();
	    });
    }
}