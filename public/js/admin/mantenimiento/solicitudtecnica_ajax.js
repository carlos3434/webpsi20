   var Datatable;
   var currentPage = 0;
   var search = '';
    var solicitudesT  = {
    //********
        listar: (function () {          
            var datos="";
            
            var targets=0;
            
            $('#tb_solicitudtecnica').dataTable().fnDestroy();
            //$('#tb_solicitudtecnica').dataTable().fnPageChange(currentPage,true);                          
            $('input[type=search]').val(search);
            $('input[type=search]').trigger('keyup');
            $('#tb_solicitudtecnica')
                    .on('page.dt', function () {
                      
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {                            
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                    
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            
                            solicitudesT.http_ajax(data,callback);
                           
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.soli_tecnica!="undefined" && typeof row.soli_tecnica!=undefined) {
                                    return row.soli_tecnica;
                                } else return "";                               
                            }, name:'soli_tecnica'},
                            

                            {data : function( row, type, val, meta) {
                               if (typeof row.fec_creacion!="undefined" && typeof row.fec_creacion!=undefined) {
                                    return row.fec_creacion;
                                } else return "";                               
                            }, name:'fec_creacion'},                            
                            

                            {data : function( row, type, val, meta) {
                                if (typeof row.estado_gestion!="undefined" && typeof row.estado_gestion!=undefined) {
                                    return row.estado_gestion;
                                } else return "";                               
                            }, name:'estado_gestion'},

                            {data : function( row, type, val, meta) {
                                if (typeof row.tipo_opera!="undefined" && typeof row.tipo_opera!=undefined) {
                                    return row.tipo_opera;
                                } else return "";                               
                            }, name:'tipo_opera'},


                            {data : function( row, type, val, meta) {
                                if (typeof row.mesaje!="undefined" && typeof row.mesaje!=undefined) {
                                    return row.mesaje;
                                } else return "";                               
                            }, name:'mesaje'},

                             {data : function( row, type, val, meta) {
                                if (typeof row.des_error!="undefined" && typeof row.des_error!=undefined) {
                                    return row.des_error;
                                } else return "";                               
                            }, name:'des_error'},

                             {data : function( row, type, val, meta) {
                                if (typeof row.nombres!="undefined" && typeof row.nombres!=undefined) {
                                    return row.nombres;
                                } else return "";                               
                            }, name:'nombres'}                      
                         
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),             
        
    http_ajax: function(request,callback){        
        var rangoFechas =$("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");       
        var solicitudTecnica = $("#solicitud_tecnica").val();
        var contador = 0;
        var form = $('#form_tipificacion').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        if(rangoFechas !=''){
            form+="&fecha="+rangoFechas;
        }

        if(solicitudTecnica !=''){
            form+="&solicitudTecnica="+solicitudTecnica;
        }

        axios.post('solicitutecnicarecepcion/listar',form).then(response => {
            callback(response.data);                       
            
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    }
 }