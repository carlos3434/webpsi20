var grillaObj = [];

var Pretemporales={
    cargarPretemporal:  (function (busqueda,ticket = '') {
        var datos="";
        var targets=0;     
        grillaObj = [];

        $('#t_embajador').dataTable().fnDestroy();
        $('#t_embajador').on('page.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('search.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('order.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        })
        .dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                var trsimple;
                for(i=0;i<grillaObj.length;i++){
                    color= '#F7BE81';
                    if(grillaObj[i].estado_id == 1) color= '#FFFFFF';
                    if(grillaObj[i].estado == 'Cerrado') color= '#A9BCF5';

                    trsimple=$("#tb_embajador tr td").filter(function() {                
                        return $(this).text().split("(")[0] == grillaObj[i].ticket;
                    }).parent('tr');                                     
                    $(trsimple).find("td:eq(0)").css("background-color", color);
                }
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                Pretemporales.http_ajax(data,callback,busqueda,ticket);
            },
            "columns":[            
                {data : function( row, type, val, meta) {
                    html =  row.ticket+"<br>("+row.tipo_atencion+")";
                    return html;
                }, name:'ticket'},
                {data : function( row, type, val, meta) {
                    fecha_usuario = row.fecha_registro + " " +row.asignado;
                    return fecha_usuario;
                }, name:'fecha_usuario'},
                 {data : function( row, type, val, meta) {
                    if (typeof row.fechaultmov!="undefined" && typeof row.fechaultmov!=undefined) {
                        return row.fechaultmov;
                    } else return "";
                }, name:'fechaultmov'},
                {data : function( row, type, val, meta) {
                    if (typeof row.tipo_averia!="undefined" && typeof row.tipo_averia!=undefined) {
                        return row.tipo_averia;
                    } else return "";
                }, name:'tipo_averia'},
                {data : function( row, type, val, meta) {
                    if (typeof row.codcli!="undefined" && typeof row.codcli!=undefined) {
                        return row.codcli;
                    } else return "";
                }, name:'codcli'},
                {data : function( row, type, val, meta) {
                    if (typeof row.quiebre!="undefined" && typeof row.quiebre!=undefined) {
                        return row.quiebre;
                    } else return "";
                }, name:'quiebre'},
                {data : function( row, type, val, meta) {
                    if (typeof row.cliente_nombre!="undefined" && typeof row.cliente_nombre!=undefined) {
                        return row.cliente_nombre;
                    } else return "";
                }, name:'cliente_nombre'},
                {data : function( row, type, val, meta) {
                    if (typeof row.motivo!="undefined" && typeof row.motivo!=undefined) {
                        return row.motivo;
                    } else return "";
                }, name:'motivo'},
                  {data : function( row, type, val, meta) {
                    if(row.submotivo !="undefined" && typeof row.submotivo!=undefined ){
                        return row.submotivo;
                    }else{
                        return "";
                    }
                }, name:'submotivo'},
                {data : function( row, type, val, meta) {
                    if (typeof row.solucion!="undefined" && typeof row.solucion!=undefined) {
                        return row.solucion;
                    } else return "";
                }, name:'solucion'},
                {data : function( row, type, val, meta) {
                    if (typeof row.estado!="undefined" && typeof row.estado!=undefined) {
                        return row.estado;
                    } else return "";          
                }, name:'estado'},
                {data : function( row, type, val, meta) {
                     if (typeof row.codliq!="undefined" && typeof row.codliq!=undefined) {
                        return row.codliq;
                    } else return "";      
                }, name: "codliq"},
                {data : function( row, type, val, meta) {
                      if (typeof row.area!="undefined" && typeof row.area!=undefined) {
                        return row.area;
                    } else return "";      
                }, name:'area'},
                {data : function( row, type, val, meta) {
                     if (typeof row.cod_multigestion!="undefined" && typeof row.cod_multigestion!=undefined) {
                        return row.cod_multigestion;
                    } else return "";      
                }, name:'cod_multigestion'},
                {data : function( row, type, val, meta) {
                     if (typeof row.llamador!="undefined" && typeof row.llamador!=undefined) {
                        return row.llamador;
                    } else return "";      
                }, name:'llamador'},
                {data : function( row, type, val, meta) {
                     if (typeof row.titular!="undefined" && typeof row.titular!=undefined) {
                        return row.titular;
                    } else return "";      
                }, name:'titular'},
                {data : function( row, type, val, meta) {
                     if (typeof row.motivo_call!="undefined" && typeof row.motivo_call!=undefined) {
                        return row.motivo_call;
                    } else return "";      
                }, name:'motivo_call'},
                {data : function( row, type, val, meta) {
                    if(typeof meta == "undefined") {
                        indice = -1;
                    }else {
                        indice = meta.row;
                    }

                    grillaObj[indice] = row;
                    htmlEstado ="<button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#pretemporalModal' data-id='"+row.id+"'><i class='fa fa-desktop fa-lg'></i></button>";                          
                    return htmlEstado;  
                }, name:'botones'}
            ], 
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 1 , "desc" ]],
            info: true,
            autoWidth: true
        });
    }),
    http_ajax: function(request,callback,busqueda,ticket=''){
        var ficha_busqueda={};
        if (busqueda == 'P') {
            findTypeGLOBAL = 'P';
            ficha_busqueda=$("#form_Personalizado").serialize().split("txt_").join("").split("slct_").join("")+ '&PG=P&tipo_busqueda=P';
            if(ticket !== ''){
                ficha_busqueda+="&tipo=ticket&busqueda=E"+ticket;
            }
        } else if(busqueda=="G"){ // Filtro General
            findTypeGLOBAL = 'G';
            ficha_busqueda=$("#form_General").serialize().split("txt_").join("").split("slct_").join("")+ '&PG=G';
        } 

        var order = request.order[0];
        ficha_busqueda+='&column='+request.columns[ order.column ].name;
        ficha_busqueda+='&dir='+order.dir;
        ficha_busqueda+="&per_page="+request.length;
        ficha_busqueda+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('pretemporal/buscar',ficha_busqueda).then(response => {
            callback(response.data);
            //if (response.data.rst==2) {
            //    Psi.sweetAlertError(response.data.msj);
            //} else {
            //    
            //}
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    Listar:function(busqueda){
        var ficha_busqueda={};
        if (busqueda == 'P') {
            findTypeGLOBAL = 'P';
            ficha_busqueda = {
                PG: busqueda,
                tipo_busqueda: busqueda,
                tipo: $("#slct_tipo").val(),
                busqueda: $("#txt_buscar").val()
            };
        } else if(busqueda=="G"){ // Filtro General
            findTypeGLOBAL = 'G';
            ficha_busqueda=$("#form_General").serialize().split("txt_").join("").split("slct_").join("")+ '&PG=G';
        } 

        if ($("#slct_tipo").val() !== '' || busqueda =="G") {
            $.ajax({
                url         : 'pretemporal/buscar',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : ficha_busqueda,
                beforeSend : function() {
                    eventoCargaMostrar();
                },
                success : function(data) {
                    eventoCargaRemover();
                    if(data.rst==1){
                        HTMLListarPretemporal(data.datos);
                    }else{
                        swal('Mensaje',data.msj,'warning');
                        $("#tb_embajador").html('<tr><td colspan="12">Ningún dato disponible en esta tabla</td></tr>');
                    }
                },
                error: function(){
                    eventoCargaRemover();
                    alert("error");
                }
            });
        } else swal('Mensaje','Seleccione tipo de búsqueda','info');
    },
    Cargar: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/cargar',
            type        : 'GET',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarDataModal(data.ficha);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    Movimiento: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/listarmovimiento',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarMovimientos(data.data);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    Relistar:function(busqueda, ticket){
        var ficha_busqueda = {
            PG: busqueda,
            tipo: 'ticket',
            busqueda: ticket
        };
        $.ajax({
            url         : 'pretemporal/buscar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : ficha_busqueda,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLListarPretemporal(data.datos);
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    guardarMovimiento: function() {     
        var datos=$("#form_bandeja").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");
        datos+="&tipo_atencion="+$("#txt_tipo_atencion").val();
        $.ajax({
            url         : "pretemporal/recepcion",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    Pretemporales.cargarPretemporal('P',obj.ticket);
                    //Pretemporales.Relistar('P',obj.ticket);
                    swal("Mensaje",obj.msj,"success");
                    $('#pretemporalModal .modal-footer [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    ValidarLegados: function(id) {
        $.ajax({
            url         : "pretemporal/validarlegados",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : {id: id},
            beforeSend : function() {
            },
            success : function(obj) {
                if (obj.rst == 1) {
                    HTMLListarAveria(obj.data, id);
                    $("#content_averia").css('display', 'block');
                } else  $("#content_averia").css('display', 'none');
            },
            error: function(obj){
            }
        });
    },
    ValidarPermiso: function(id){
        var datos = {
            tipo: 'ticket',
            busqueda: id
        };
        $.ajax({
            url         : 'pretemporal/validarpermiso',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst == 1){
                    permisoGlobal = data.type;
                    validaPermiso();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },
    FinalizarPermiso: function(id, type){
        var datos = {
            busqueda: id,
            type: type
        };
        $.ajax({
            url         : 'pretemporal/finalizarpermiso',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    }
};