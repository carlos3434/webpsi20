    var route = 'pretemporal/';
    var PreTemporalMasiva = {
        listarLog: (function () {
            var datos="";
            var targets=0;
            $('#tb_solicitudes').dataTable().fnDestroy();
            $('#tb_solicitudes')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            PreTemporalMasiva.http_ajax(data,callback);
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.file_accept!="undefined" && typeof row.file_accept!=undefined) {
                                    return row.file_accept;
                                } else return "";
                            }, name:'file_accept'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.file_refused!="undefined" && typeof row.file_refused!=undefined) {
                                    return row.file_refused;
                                } else return "";
                            }, name:'file_refused'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.number_acept!="undefined" && typeof row.number_acept!=undefined) {
                                    return row.number_acept;
                                } else return "";
                            }, name:'number_acept'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.number_refused!="undefined" && typeof row.number_refused!=undefined) {
                                    return row.number_refused;
                                } else return "";
                            }, name:'number_refused'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.responsable!="undefined" && typeof row.responsable!=undefined) {
                                    return row.responsable;
                                } else return "";
                            }, name:'responsable'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.created_at!="undefined" && typeof row.created_at!=undefined) {
                                    return row.created_at;
                                } else return "";
                            }, name:'created_at'},
                            {data : function( row, type, val, meta) { //<a onclick="DescargarExcel();" class="btn btn-success"><i class="fa fa-download fa-lg"></i></a>
                                  if(typeof meta == "undefined") {
                                    indice = -1;
                                } else {
                                    indice = meta.row;
                                }                                
                                htmlButtons = '<a href="proceso_masivo/'+row.file_accept+'" class="btn btn-success" target="_blank"><i class="fa fa-download fa-lg"></i></a>';
                                return htmlButtons;
                            }, name:'botones'}
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 5 , "desc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),     
        subirArchivo: function (data) {
            $.ajax({
                url: route + 'procesouno',
                type: "POST",
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {

                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        
                        swal({
                            title: obj.msj,
                            text: 'Clic en "Continuar" para subir los pedidos',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Continuar',
                            cancelButtonText: "Cancelar",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                PreTemporalMasiva.procesarArchivo();
                            } else {
                                swal("Cancelado", "No se completo la subida masiva.", "warning");
                            }
                            $("#archivocolumna").fileinput('reset');
                        });

                    } else {
                        swal('Mensaje',obj.msj,'error');
                    }

                },
                error: function () {
                    eventoCargaRemover();
                    swal('Error','Ocurrió un error en la carga.','error');
                }
            });
        },
        procesarArchivo: function () {
            $.ajax({
                url: route + 'procesar-archivo-columna',
                type: "POST",
                data: '',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    alert = 'error';
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        alert = 'success';
                    }
                    swal('Mensaje',obj.msj,alert);
                }
            });
        },
        getMasiva: function () {
            $.ajax({
                url: route + 'masivas',
                type: "POST",
                data: '',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                   $('.btncarga').html('Cargando...').prop('disabled', true);
                },
                success: function (obj) {
                    alert = 'error';
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                       poblateMasivo(obj.data);
                       $('.btncarga').html('Listo').prop('disabled', true);
                    }
                }
            });
        },
        procesarMasivo: function (data,evento_id ='') {
            $.ajax({
                url: route + 'procesarmasivo',
                type: "POST",
                data: {
                    pedidos : data,
                    evento_id : evento_id
                    },
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    alert = 'error';
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        var html = '';
                        html+="Registrados:"+obj.registrados+", Fallidos:"+obj.fallo;
                        alert = 'success';
                        $("#tblData tbody").html('');
                        $(".informacion").text(html);
                        $(".informacion").removeClass('hidden');
                    }
                    swal('Mensaje',obj.msj,alert);
                }
            });
        },
        getInformation: function (url,name,data ='') {
            $.ajax({
                url: url,
                type: "POST",
                cache: false,
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    alert = 'error';
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        cargarHTML(obj.datos,name);
                    }
                }
            });
        },        
        http_ajax: function(request,callback){
            var contador = 0;
            var form = $('#form_movimiento').serialize().split('txt_').join("").split('slct_').join("");
            var order = request.order[0];
            form+='&column='+request.columns[ order.column ].name;
            form+='&dir='+order.dir;
            form+="&per_page="+request.length;
            form+="&page="+(request.start+request.length)/request.length;

            eventoCargaMostrar();
            axios.post('uploadlog/cargar',form).then(response => {
                callback(response.data);
                //if (response.data.rst==2) {
                //    Psi.sweetAlertError(response.data.msj);
                //} else {
                //    
                //}
            }).catch(e => {
                //vm.errors=e;
            }).then(() => {
                eventoCargaRemover();
            });
        }  
    };
