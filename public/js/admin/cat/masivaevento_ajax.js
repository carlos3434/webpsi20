var listado={
    listar:  (function () {
        var datos="";
        var targets=0;     
        $('#t_reporte').dataTable().fnDestroy();
        $('#t_reporte').on('page.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('search.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('order.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        })
        .dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                listado.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {
                    if (typeof row.nombre!="undefined" && typeof row.nombre!=undefined) {
                        return row.nombre;
                    } else return "";
                }, name:'nombre'},
                {data : function( row, type, val, meta) {
                    if (typeof row.nodo!="undefined" && typeof row.nodo!=undefined) {
                        return row.nodo;
                    } else return "";
                }, name:'nodo'},
                 {data : function( row, type, val, meta) {
                    if (typeof row.troba!="undefined" && typeof row.troba!=undefined) {
                        return row.troba;
                    } else return "";
                }, name:'troba'},
                {data : function( row, type, val, meta) {
                    if (typeof row.amplificador!="undefined" && typeof row.amplificador!=undefined) {
                        return row.amplificador;
                    } else return "";
                }, name:'amplificador'},
                {data : function( row, type, val, meta) {
                    if (typeof row.empresa!="undefined" && typeof row.empresa!=undefined) {
                        return row.empresa;
                    } else return "";
                }, name:'empresa'},
                {data : function( row, type, val, meta) {
                    if (typeof row.userCreated!="undefined" && typeof row.userCreated!=undefined) {
                        return row.userCreated;
                    } else return "";
                }, name:'userCreated'},
                {data : function( row, type, val, meta) {
                    if (typeof row.fecharegistro!="undefined" && typeof row.fecharegistro!=undefined) {
                        return row.fecharegistro;
                    } else return "";
                }, name:'fecharegistro'},
                {data : function( row, type, val, meta) {
                    if (typeof row.estadoActual!="undefined" && typeof row.estadoActual!=undefined) {
                        return row.estadoActual;
                    } else return "";
                }, name:'estadoActual'},
                  {data : function( row, type, val, meta) {
                    if(row.cantickets !="undefined" && typeof row.cantickets!=undefined ){
                        return row.cantickets;
                    }else{
                        return "";
                    }
                }, name:'cantickets'},
                {data : function( row, type, val, meta) {
                    if (typeof row.atendidos!="undefined" && typeof row.atendidos!=undefined) {
                        return row.atendidos;
                    } else return "";
                }, name:'atendidos'},
                {data : function( row, type, val, meta) {
                    var calc = 0;
                    if(row.cantickets != 0 && row.atendidos !=0){
                        if(row.cantickets != row.atendidos){
                            calc = Math.round((row.atendidos / row.cantickets) * 100);    
                        }else{
                            calc = 100; 
                        }
                    }
                    return calc;                  
                }, name:'calc'},
                {data : function( row, type, val, meta) {
                    estado = '';
                    if(row.estado != 2 && row.estado != 7){
                        if(row.estado == 1){
                            estado = '<span class="btn btn-success btn-sm estado" onClick="cambiarEstado('+row.id+',8)">Recepcionado</span>';
                        }else if(row.estado == 8){
                            estado = '<span class="btn btn-danger btn-sm estado" onClick="cambiarEstado('+row.id+',1)">Cerrado</span>';
                        }
                    }else{
                        if(row.estado == 2){
                            estado = '<span class="btn btn-success btn-sm estado" style="opacity:0.5">Validado</span>';
                        }else if(row.estado == 7){
                            estado = '<span class="btn btn-success btn-sm estado" style="opacity:0.5">En Proceso</span>';                   
                        }
                    }
                    return estado;
                }, name: "estado"},
                {data : function( row, type, val, meta) {
                    detalle='<td><span class="btn btn-primary btn-sm tdetalles" idevento="'+row.id+'" onClick="cargarInfo(this)"><i class="glyphicon glyphicon-th-list"></i></span></td>';
                    return detalle;
                }, name:'detalle'},
                {data : function( row, type, val, meta) {
                    averias='<td><span class="btn btn-primary btn-sm tadetalles" idevento="'+row.id+'" images="'+row.imagenes+'" onClick="cargaDetalles(this)">Averias</span></td>';
                    return averias;
                }, name:'averias'},
                {data : function( row, type, val, meta) {
                    if(row.cantickets <= 0){
                        htmlButtons='<td><span class="btn btn-primary btn-sm tcargar" id-evento="'+row.id+'" nodo="'+row.nodo+'" troba="'+ row.troba+'" onClick="cargarAverias(this)"><i class="glyphicon glyphicon-search"></i></span></span></td>';            
                    }else{
                        htmlButtons='<td></td>';        
                    }                 
                    return htmlButtons;
                }, name:'botones'}
            ], 
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 6 , "desc" ]],
            info: true,
            autoWidth: true
        });
    }),
    Userstovalidate: function(){
        $.ajax({
            url         : 'quiebregrupo/getuserbygrupo',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            success : function(data) {
                HTMLUsuariosValidar(data.data);
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    codactu: function(){
        datos=$("#form_configuracion").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'pretemporal/configuracion',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    swal('Mensaje',data.msj,'success');
                }else{
                    swal('Mensaje',data.msj,'warning');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    cargar: function(data){
        $.ajax({
            url         : 'eventomasivo/cargar',
            type        : 'POST',
            data        : data,
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();                
                if(data.length==1){
                    HTMLDetalle(data[0]);                
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },

    CambiarEstado: function(data){
        $.ajax({
            url         : 'eventomasivo/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    listado.listar();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
     getTicketsbyEvent: function(data,img = ''){
        $.ajax({
            url         : 'eventomasivo/getticketsbyevent',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLTickets(data.datos,img);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#form_Personalizado').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('eventomasivo/cargar',form).then( response => {
            callback(response.data);
        }).catch( e => {
            
        }).then(() => {
            eventoCargaRemover();
        });
    },
    getMasiva: function (data) {
        $.ajax({
            url: 'pretemporal/masivas',
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            beforeSend: function () {
               $('.btncarga').html('Cargando...').prop('disabled', true);
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    if(obj.data.length > 0){
                        /*get groups of 35 elements 'cause php is restricted' */
                        var registros_group = [];
                        var i,j,temparray,chunk = 32;
                        for (i=0,j=obj.data.length; i<j; i+=chunk) {
                            temparray = obj.data.slice(i,i+chunk);
                            registros_group.push(JSON.stringify(temparray));
                        }
                        listado.procesarMasivo(registros_group,data.id);                              
                        /*end */                            
                    }
                   $('.btncarga').html('Listo').prop('disabled', true);
                }
            }
        });
    },
    procesarMasivo: function (data,evento_id) {
        $.ajax({
            url: 'pretemporal/procesarmasivo',
            type: "POST",
            data: {
                evento_id : evento_id,
                pedidos : data,
                },
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    alert = 'success';
                    $("#nuevoEvento").modal('hide');
                }
                listado.cargar();
                swal('Mensaje','Registrados 12',alert);
            }
        });
    }
};
