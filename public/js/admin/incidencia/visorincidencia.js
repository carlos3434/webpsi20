var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 
var ids=0;
$(document).ready(function(){
    solicitudesT.getVisor();
    solicitudesT.Responsables();

    slctGlobal.listarSlct('empresa', 'slct_empresa_buscar', 'simple', null);
    slctGlobal.listarSlctpost('usuario','cargar','slct_usuarioresp_buscar', 'multiple', null,{nomcompleto:1});
    slctGlobal.listarSlct('tipificacion', 'slct_tipificacion,#slct_tipificacion_buscar,#slct_tipificacion_modal', 'multiple', null);   
    slctGlobalHtml('slct_tipotrabajo_buscar,#slct_tipotrabajo,#slct_tipotrabajo_modal','multiple');
    slctGlobalHtml('slct_tecnologia_buscar,#slct_tecnologia,#slct_tecnologia_modal','multiple');
    slctGlobalHtml('slct_tipo_legado_buscar,#slct_tipo_legado,#slct_tipo_legado_modal','multiple');
    slctGlobalHtml('slct_nivel_impacto_buscar,#slct_nivel_impacto,#slct_nivel_impacto_modal','multiple');
    slctGlobalHtml('slct_estado_validacion_buscar,#slct_estado_validacion,#slct_estado_validacion_modal','multiple');
    slctGlobalHtml('slct_prioridad_buscar,#slct_prioridad,#slct_prioridad_modal','multiple');
    slctGlobalHtml('slct_eactual_buscar,#slct_eactual,#slct_eactual_modal','multiple');
    slctGlobalHtml('slct_tipo,#slct_tipo_buscar,#slct_tipo_modal','multiple');
    slctGlobal.listarSlctpost('usuario','cargar','slct_usuarioresp', 'multiple', null,{nomcompleto:1});



    /*$('#txt_fecha_registro').val(
        moment().subtract(7, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );*/
    $('#txt_fecha_entrega').daterangepicker({        
        singleDatePicker: true,
        format: 'YYYY-MM-DD'
    });

    $(document).on('click', '#btn_general', function(event) {
        /*datos=$("#form_filtros").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");
        array_data = {};
        if(datos){
            $.each(datos,function(index, el) {
                var row = el.split("=");
                if(row[0] in array_data){
                    array_data[row[0]]+=","+row[1];
                }else{
                    array_data[row[0]] = row[1];
                }            
            });            
        }*/

        /*var jsonArray = JSON.parse(JSON.stringify(  ))
        console.log(jsonArray);
        solicitudesT.getVisor({data:JSON.stringify(array_data)});*/
    });

    $('#incidenciaGestion').on('shown.bs.modal', function (event) {

        var button = $(event.relatedTarget);
        var id = button.data('id');
        solicitudesT.getReqbyIncidencia({incidencia_id:id});
        solicitudesT.getIncidenciaById({incidencia_id:id});
        solicitudesT.listarMovimientos(id);
        $("#txt_incidencia_id").val(id);
        $("#txt_id_modal2").val(id);
        $("#tab_1,#tab_gestion_modal").addClass("active");
        ids = id;    
    });

    $('#incidenciaGestion').on('hide.bs.modal', function (event) {
        $("#frmIncidenciaEdit input[type='hidden'],#frmIncidenciaEdit input[type='password'],#frmIncidenciaEdit input[type='text'],#frmIncidenciaEdit input[type='file'],#frmIncidenciaEdit select,#frmIncidenciaEdit textarea").not('.mant').val('');
        $("#frmIncidenciaEdit select").multiselect("deselectAll", false); 
        /*$("#slct_usuarioresp").multiselect("deselectAll", false);*/
        $('.tab_1, .tab_2,.tab_3,.tab_4').removeClass("active");
        $('#tab_1, #tab_2,#tab_3,#tab_4').removeClass("active");
    });


    $(document).on('change', '.slct_estado_req', function(event) {
        var requerimiendo_id = $(this).attr('requerimiendo_id');
        var valor = $(this).val();
        if(requerimiendo_id && valor){
            solicitudesT.CambiarEstadoReq({requerimiendo_id:requerimiendo_id,estado_id:valor});
        }
    });

    $(document).on('click', '.tab_2', function(event) {        
        $(".bxslider").bxSlider();
    });


    $(document).on('click', '#btnAdd', function(event) {
        event.preventDefault();
        var template = $(".t_req[validate=0]").find('.trNuevo').clone().removeClass('trNuevo').removeClass('hidden');
        $(".t_req[validate=0] tbody").append(template);
    }); 

     $(document).on('click', '#btnAdd2', function(event) {
        event.preventDefault();
        var template = $(".t_req[validate=1]").find('.trNuevo').clone().removeClass('trNuevo').removeClass('hidden');
        $(".t_req[validate=1] tbody").append(template);
    }); 

    $("#collapseTwo").removeClass("in");
    $("#collapseTwo").prop("aria-expanded", false);

    /*registry new incident*/
    $("form[name='frmIncidencia']").submit(function(e) {
        e.preventDefault();

        if($.trim($("#txt_nombre").val()) == ""){
            swal('Escriba Nombre');
        }else if($.trim($("#slct_tipotrabajo").val()) == ""){
            swal('Seleccione tipo trabajo');
        }else if($.trim($("#slct_tecnologia").val()) == ""){
            swal('Seleccione sistema');
        }else if($("#slct_empresa").val() == ""){
             swal('Seleccione empresa');
        }else if($.trim($("#slct_tipo_legado").val()) == ""){
            swal('Seleccione tipo trabajo');
        }else if($.trim($("#slct_nivel_impacto").val()) == ""){
            swal('Seleccione un nivel de impacto');        
        }else if($("#slct_prioridad").val() == ""){
            swal('Seleccione un nivel de prioridad');
        }else if($("#slct_eactual").val() == ""){
            swal('Seleccione estado actual');
        }else if($("#txt_obervacion").val() == ""){
            swal('Escriba una observación');
        }else{

            /*validate req*/
            array_incompletos = [];array_req =[];
            if($("#incidencia_t").val()==''){
                $(".t_req[validate=1] tbody tr").each(function (index,el){ 
                    var tipo_valor = $(el).find('#td_tipovalor').find('#slct_tipovalor');
                    var valor = $(el).find('#td_valoi').find('#txt_valor');
                    var detalle = $(el).find('#td_detalle').find('#txt_observacion');
                    if($(tipo_valor).val()== ''|| $(valor).val()== '' || $(detalle).val()== ''){
                        index=index+1;
                        array_incompletos.push(index);
                    }else{
                        array_req.push({'tipo_valor':$(tipo_valor).val(),'valor':$(valor).val(),'descripcion':$(detalle).val()});
                    }
                });
            }
            /*end validate req*/

            if(array_incompletos.length > 0){
                swal('En la fila(s) '+array_incompletos.join(",") + ' hay campos vacios');
            }else{
                FormDatos = new FormData($(this)[0]);
                if(array_req.length > 0) FormDatos.append('req',JSON.stringify(array_req));               
                $.ajax({
                    type: "POST",
                    url: 'incidencia/create',
                    data: FormDatos,
                    processData: false,
                    contentType: false,
                    beforeSend : function() {
                        eventoCargaMostrar();
                    },
                    success: function (obj) {
                        eventoCargaRemover();
                        if(obj.rst==1){                  
                              $("#incidencia").modal('hide');
                              Datatable = solicitudesT.listarIncidencias();             
                        }else{
                            swal('Error al registrar incidencia');
                        }
                    }
                });
            }
        }
    });
    /* end registry new incident */

    /*edit incident*/
    $("form[name='frmIncidenciaEdit']").submit(function(e) {
        e.preventDefault();         
                $.ajax({
                    type: "POST",
                    url: 'incidencia/edit',
                    data: new FormData($(this)[0]),
                    processData: false,
                    contentType: false,
                    beforeSend : function() {
                        eventoCargaMostrar();
                    },
                    success: function (obj) {
                        eventoCargaRemover();
                        if(obj.rst==1){                  
                              $("#incidenciaGestion").modal('hide');
                              Datatable = solicitudesT.listarIncidencias();             
                        }else{
                            swal('Error al editar incidencia');
                        }
                    }
        });
    });
    /* end edit incident */

    $(document).on('change', '.slct_estadoe', function(event) {
        var incidencia_id= $(this).attr('id-incidencia');
        var valor = $(this).val();
        if(incidencia_id && valor){
            solicitudesT.CambiarEstado({incidencia_id:incidencia_id,estado_actual:valor});
        }else{
            swal('No se pudo actualizar el estado');
        }
    });

    $(document).on('change', '.slct_estado_validacione', function(event) {
        var incidencia_id= $(this).attr('id-incidencia');
        var valor = $(this).val();
        if(incidencia_id && valor){
            solicitudesT.CambiarEstado({incidencia_id:incidencia_id,estado_validacion:valor});
        }else{
            swal('No se pudo actualizar el estado');
        }
    });

    $(document).on('click', '.openFile', function(event) {
        var target = $(this).attr('target_f');
        $("#"+target).trigger('click');
    });

     $(".file").change(function() {
        readURLI(this, 'img',$(this).attr('numero'));
    });
    

    $('#incidencia').on('hide.bs.modal', function (event) {
        $("#frmIncidencia input[type='hidden'],#frmIncidencia input[type='password'],#frmIncidencia input[type='text'],#frmIncidencia input[type='file'],#frmIncidencia select,#frmIncidencia textarea").not('.mant').val('');
        $("#frmIncidencia select").multiselect('refresh');
        $(".t_req[validate=1] tbody").html('');
    });

    $('#incidencia').on('show.bs.modal', function (event) {
        $("#slct_eactual").val(4);
        $("#slct_estado_validacion").val(4);
        $("#slct_eactual,#slct_estado_validacion").multiselect('refresh');
    });

    function readURLI(input, tipo,numero) {
        if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (tipo == 'img') {
                        $('#txt_image'+numero).val(e.target.result);
                    }
                }
                reader.readAsDataURL(input.files[0]);
            }
        }



    $('#txt_fecha_registro').daterangepicker({
        format: 'YYYY-MM-DD',
        minDate: moment().subtract(35, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
    });
    slctGlobalHtml('slct_tipo_fecha','simple');
    //Mostrar 
    $("#generar_movimientos").click(function (){
        mostrarMovimiento();
    });
    //iCheck-helper

    $('.iCheck-helper').click(function(){
        selectFiltro();
    });

    $(document).on('click', '#mostrar', function(event) {
        if($.trim($("#fecha").val()) != ''){
            if($.trim($("#slct_tipo_fecha").val()) != ''){
                solicitudesT.listarSolicitudes();
            }else{
                swal('Seleccione campo a buscar');
            }
        }else{
            swal('Seleccione un rango de fechas');       
        }
    });

    DescargarExcel = function(){
        $("#form_movimiento").attr("action", "incidencia/listar");
        $("#form_movimiento").append("<input type='hidden' name='accion' value='download' id='accion' />");
        $("#form_movimiento").submit();
        $("#accion").remove();
        $("#form_movimiento").attr("action", "");              
    };

    HTMLDatosIncidencia = function(datos){          
        if(datos){
            var data = datos[0];
            $("#txt_id_modal").val(data.id);
            $("#incidencia_t").val(data.incidenciat);
            $("#txt_nombre_modal").val(data.nombre);
            $("#txt_obervacion_modal").val(data.observacion);
            $("#txt_fecha_entrega").val(data.fecha_entrega);
            
            $("#slct_tipotrabajo_modal").multiselect('select', data.actividad_id);
            $("#slct_tecnologia_modal").multiselect('select', data.sistema_id);        
            $("#slct_empresa_modal").multiselect('select', data.empresa_id);
            $("#slct_tipo_legado_modal").multiselect('select', data.tipo_legado);
            $("#slct_nivel_impacto_modal").multiselect('select', data.nivel_impacto);
            $("#slct_estado_validacion_modal").multiselect('select', data.estado_validacion);
            $("#slct_prioridad_modal").multiselect('select', data.prioridad);
            $("#slct_eactual_modal").multiselect('select', data.estado);
            $("#slct_tipificacion_modal").multiselect('select', data.tipificacion);
            $("#slct_tipo_modal").multiselect('select', data.tipo);
            $("#slct_usuarioresp").multiselect('select', data.usuario_responsable_id);
            /*$("#slct_usuarioresp").val(data.usuario_responsable_id).change();*/
            //$("#slct_usuarioresp").multiselect('refresh');

          if(data.imagenes){
                var src = '';
                var cont=0;
                var html = '';
                /*image = (imagenes.split(",").length > 1) ? imagenes.split(",") : imagenes;*/
                $.each(data.imagenes.split(","),function(index, el) {
                    html+='<li><img src="incidencia/'+el+'"/></li>';
                });
                $(".bxslider").html(html);
                $(".Imagenes").removeClass('hidden');
                /*slider = $('.bxslider').bxSlider();
                slider.destroySlider();*/                             
            }else{
                $(".Imagenes").addClass('hidden');
                $(".bxslider").html('');      
            }           
        }else{
            swal('no se encuentra informacion de la incidencia');
        }        
    }


    ListarVisor = function(obj) {
        var html = "";
        $.each(obj.count,function(index,data){
            var total1=0;total2=0;total3=0;total4=0;total5=0;total6=0;total7=0;total8=0;general=0;
            html+="<tr>";
            html+="<td style='vertical-align:middle;' rowspan="+data.row_count+"><p>"+data.actividad_name+"</p></td>";
            $.each(obj.data,function(index,data2){
                if (data2.actividad_id == data.actividad_id) {
                    html+="<td>"+data2.sistema+"</td>";
                    html+="<td>"+data2.f1+"</td>";
                    html+="<td>"+data2.f2+"</td>";
                    html+="<td>"+data2.f3+"</td>";
                    html+="<td>"+data2.f4+"</td>";
                    html+="<td>"+data2.f5+"</td>";
                    html+="<td>"+data2.f6+"</td>";
                    html+="<td>"+data2.f7+"</td>";
                    html+="<td>"+data2.f8+"</td>";
                    html+="<td style='background-color: #F7F7E2;'>"+data2.total+"</td>";
                    html+="</tr>";
                    total1+=parseInt(data2.f1);total2+=parseInt(data2.f2);total3+=parseInt(data2.f3);total4+=parseInt(data2.f4);total5+=parseInt(data2.f5);total6+=parseInt(data2.f6);
                    total7+=parseInt(data2.f7);total8+=parseInt(data2.f8);general+=parseInt(data2.total);
                }
            });
            html+="<tr>";
            html+=" <td></td>";
            html+=" <td>Totales</td>";
            html+=" <td>"+total1+"</td>";
            html+=" <td>"+total2+"</td>";
            html+=" <td>"+total3+"</td>";
            html+=" <td>"+total4+"</td>";
            html+=" <td>"+total5+"</td>";
            html+=" <td>"+total6+"</td>";
            html+=" <td>"+total7+"</td>";
            html+=" <td>"+total8+"</td>";
            html+=" <td style='background-color: #F7F7E2;'>"+general+"</td>";
            html+="</tr>";
        });
        $("#tb_visoroperaciones").html(html);        
    };

    VisorResponsables = function(obj) {
        var html = "";
        colspan = obj.sistema.length + 1;
        html+="<tr>";
        html+="<th colspan=2></th>";
        html+="<th colspan="+colspan+">INCIDENCIAS</th>"; 
        html+="<th colspan="+colspan+">O.MEJORA</th>";
        html+="</tr>";

        html+="<tr>";
        html+="<th>Actividad</th>"; 
        html+="<th>Responsable</th>"; 
        for(var i=0;i<2;i++){
            $.each(obj.sistema,function(index, el) {
                html+="<th>"+el.nombre+"</th>"; 
            });
            html+="<th style='width: 8%;background-color: #F5F6CE;'>Total</th>"; 
        }        
        html+="</tr>"; 
        $.each(obj.actividad,function(index,data){
            /*var total1=0;total2=0;total3=0;total4=0;total5=0;total6=0;total7=0;total8=0;general1=0;
            var total9=0;total10=0;total11=0;total12=0;total13=0;total14=0;total5=0;total6=0;general2=0;*/
            html+="<tr>";
            var rowspan = parseInt(data.row_count) + 1;
            html+="<td style='vertical-align:middle;' rowspan="+rowspan+"><p>"+data.actividad_name+"</p></td>";
            $.each(obj.data,function(index,data2){
                if (data2.actividad_id == data.actividad_id) {
                    html+="<td>"+data2.responsable+"</td>";
                    html+="<td>"+data2.f1+"</td>";
                    html+="<td>"+data2.f2+"</td>";
                    html+="<td>"+data2.f3+"</td>";
                    html+="<td>"+data2.f4+"</td>";
                    html+="<td>"+data2.f5+"</td>";
                    html+="<td>"+data2.f6+"</td>";
                    html+="<td>"+data2.f7+"</td>";
                    html+="<td>"+data2.f8+"</td>";
                    html+="<td>"+data2.f9+"</td>";
                    html+="<td>"+data2.f10+"</td>";
                    html+="<td>"+data2.f11+"</td>";
                    html+="<td>"+data2.f12+"</td>";
                    html+="<td>"+data2.f13+"</td>";
                    html+="<td>"+data2.f14+"</td>";
                    html+="<td>"+data2.f15+"</td>";
                    html+="<td>"+data2.f16+"</td>";
                    html+="<td>"+data2.f17+"</td>";
                    html+="<td>"+data2.f18+"</td>";
                    html+="</tr>";
                    /*total1+=parseInt(data2.f1);total2+=parseInt(data2.f2);total3+=parseInt(data2.f3);total4+=parseInt(data2.f4);total5+=parseInt(data2.f5);total6+=parseInt(data2.f6);
                    total7+=parseInt(data2.f7);general1+=parseInt(data2.f8);total8+=parseInt(data2.f9);total9+=parseInt(data2.f10);total11+=parseInt(data2.f11);total12+=parseInt(data2.f12);
                    total13+=parseInt(data2.f13);total14+=parseInt(data2.f14);total15+=parseInt(data2.f15);total16+=parseInt(data2.f16);general+=parseInt(data2.total);*/
                }
            });
            /*html+="<tr>";
            html+=" <td></td>";
            html+=" <td>Totales</td>";
            html+=" <td>"+total1+"</td>";
            html+=" <td>"+total2+"</td>";
            html+=" <td>"+total3+"</td>";
            html+=" <td>"+total4+"</td>";
            html+=" <td>"+total5+"</td>";
            html+=" <td>"+total6+"</td>";
            html+=" <td>"+total7+"</td>";
            html+=" <td>"+total8+"</td>";
            html+=" <td>"+total9+"</td>";
            html+=" <td>"+total10+"</td>";
            html+=" <td>"+total11+"</td>";
            html+=" <td>"+total12+"</td>";
            html+=" <td>"+total13+"</td>";
            html+=" <td>"+total14+"</td>";
            html+=" <td>"+total15+"</td>";
            html+=" <td>"+total16+"</td>";
            html+=" <td style='background-color: #F7F7E2;'>"+general+"</td>";
            html+="</tr>";*/
        });
        $("#t_responsable tbody").html(html);        
    };
});
