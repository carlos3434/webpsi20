var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 
var ids=0;
$(document).ready(function(){
    Datatable = solicitudesT.listar();
   
    $('#nuevaTipificacion').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        console.log(id);
        if(!id){
            $("#btn_guardar").attr('onclick','guardar()');
           $("#slct_estado option[value=1]").prop('selected',true);
        }else{
            $("#btn_guardar").attr('onclick','editar()');
            $("#frm_tipificacion").append('<input type="hidden" value="'+id+'" name="txt_tipificacion_id" id="txt_tipificacion_id">');
        }
        ids = id;           
    });

    $('#nuevaTipificacion').on('hide.bs.modal', function (event) {
        $("#frm_tipificacion input[type='hidden'],#frm_tipificacion input[type='password'],#frm_tipificacion input[type='text'],#frm_tipificacion input[type='file'],#frm_tipificacion select,#frm_tipificacion textarea").not('.mant').val('');
        $("#frm_tipificacion #txt_tipificacion_id").remove();
    });

    cambiarestado = function(obj){
        var tipificacion_id = obj.getAttribute('tipificacion_id');
        var estado = obj.getAttribute('estado');
        if(tipificacion_id && estado){
            solicitudesT.CambiarEstado({tipificacion_id:tipificacion_id,estado:estado});
        }
    }

    $(document).on('click', '.btn-detalle', function(event) {
        var tr = $(this).parent().parent().find('td');
        var nombre = $(tr[1]).html();
        var estado = $(tr[2]).find('span').attr('estado');  
        var est =(estado == 0) ? 1 : 0;

        $("#txt_nombre").val(nombre);
        $("#slct_estado option[value="+est+"]").prop('selected',true);
    });

    guardar = function(){
        if($("#txt_nombre").val() != ''){
            solicitudesT.guardar(1);            
        }else{
            swal('Ingrese nombre');
        }
    }

    editar = function(){        
        solicitudesT.guardar(2);
    }
});
