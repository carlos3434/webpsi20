    var Datatable;
    var solicitudesT = {
        listar: (function () {
            var datos="";
            var targets=0;
            $('#tb_tipificacion').dataTable().fnDestroy();
            $('#tb_tipificacion')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            solicitudesT.http_ajax(data,callback);
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.tip_id!="undefined" && typeof row.tip_id!=undefined) {
                                    return row.tip_id;
                                } else return "";
                            }, name:'tip_id'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.nombre!="undefined" && typeof row.nombre!=undefined) {
                                    return row.nombre;
                                } else return "";
                            }, name:'nombre'},
                             {data : function( row, type, val, meta) {
                                htmlEstado ='<span class="btn btn-danger btn-md" tipificacion_id="'+row.tip_id+'" estado="1" onclick="cambiarestado(this)">Inactivo</span>';
                                if(row.estado == 1){
                                    htmlEstado ='<span class="btn btn-success btn-md" tipificacion_id="'+row.tip_id+'" estado="0" onclick="cambiarestado(this)">Activo</span>';
                                }                            
                                return htmlEstado;
                            }, name:'estado'},
                            {data : function( row, type, val, meta) {
                                  if(typeof meta == "undefined") {
                                    indice = -1;
                                } else {
                                    indice = meta.row;
                                }                                
                                htmlButtons = "<a class='btn btn-primary btn-md btn-detalle' data-toggle='modal' title='Detalle Incidencia' ";
                                htmlButtons+=" data-target='#nuevaTipificacion' data-id='"+row.tip_id+"'>";
                                htmlButtons+=" <i class='glyphicon glyphicon-th-list'></i></a>";
                                return htmlButtons;
                            }, name:'botones'}
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),       
        CambiarEstado: function(data){
        $.ajax({
            url         : 'tipificacion/actualizar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                   solicitudesT.listar();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
     guardar: function(tipo){
        var datos=$("#frm_tipificacion").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
        url = "tipificacion/create";
        if(tipo == 2){
            url = "tipificacion/actualizar";
        }
        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            data        : datos,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    $("#nuevaTipificacion").modal('hide');
                    solicitudesT.listar();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#form_tipificacion').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('tipificacion/cargar',form).then(response => {
            callback(response.data);
            //if (response.data.rst==2) {
            //    Psi.sweetAlertError(response.data.msj);
            //} else {
            //    
            //}
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    }
    }