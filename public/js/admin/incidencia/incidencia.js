var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 
var ids=0;
$(document).ready(function(){
    slctGlobal.listarSlct('empresa', 'slct_empresa,#slct_empresa_buscar,#slct_empresa_modal', 'multiple', null);
    slctGlobal.listarSlctpost('usuario','cargar','slct_usuarioresp_buscar,#slct_usuarioresp', 'multiple', null,{nomcompleto:1});
    slctGlobal.listarSlct('tipificacion', 'slct_tipificacion,#slct_tipificacion_buscar,#slct_tipificacion_modal', 'multiple', null);

    slctGlobalHtml('slct_tipotrabajo_buscar,#slct_tipotrabajo,#slct_tipotrabajo_modal','multiple');
    slctGlobalHtml('slct_tecnologia_buscar,#slct_tecnologia,#slct_tecnologia_modal','multiple');
    slctGlobalHtml('slct_tipo_legado_buscar,#slct_tipo_legado,#slct_tipo_legado_modal','multiple');
    slctGlobalHtml('slct_nivel_impacto_buscar,#slct_nivel_impacto,#slct_nivel_impacto_modal','multiple');
    slctGlobalHtml('slct_estado_validacion_buscar,#slct_estado_validacion,#slct_estado_validacion_modal','multiple');
    slctGlobalHtml('slct_prioridad_buscar,#slct_prioridad,#slct_prioridad_modal','multiple');
    slctGlobalHtml('slct_eactual_buscar,#slct_eactual,#slct_eactual_modal','multiple');

    if(area_id != 52){
        $('#slct_tipo_buscar option[value="4"]').remove();
    }else{
        var option =  $('#slct_tipo_buscar option[value="4"]').text();
        if(!option){
            $('#slct_tipo_buscar').append($("<option></option>").attr("value",4).text('PROYECTO'));            
        }
    }
    slctGlobalHtml('slct_tipo,#slct_tipo_buscar,#slct_tipo_modal','multiple');

    $('#txt_fecha_entrega').daterangepicker({
        singleDatePicker: true,
        format: 'YYYY-MM-DD'
    });
    Datatable = Incidencia.listar();

    $(document).on( 'click', '.btn-eliminar', function(event) {
        var incidencia_id = $(this).attr('incidencia-id');
        if (incidencia_id) {
            var r = confirm("¿Esta seguro de eliminar el registro?");
            if (r == true) {
                Incidencia.Delete({incidencia_id:incidencia_id});
            }
        } else {
            swal('Error al eliminar');
        }
    });

    $(document).on( 'click', '.accordion-toggle', function(event) {
        var collapse = event.target.hash;
        if(collapse == "#collapse2") {
            $("#collapseTwo").removeClass("in");
            $("#collapseTwo").prop("aria-expanded", false);
        } else if(collapse == "#collapseTwo") {
            $("#collapse2").removeClass("in");
            $("#collapse2").prop("aria-expanded", false);
        }
    });

    $('#incidenciaGestion').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        Incidencia.listarById({incidencia_id:id});
        Incidencia.getById({incidencia_id:id});
        Incidencia.listarMovimientos(id);
        $("#txt_incidencia_id").val(id);
        $("#txt_id_modal2").val(id);
        $("#tab_1,#datos_incidencia").addClass("active");
        ids = id;
    });

    $('#incidenciaGestion').on('hide.bs.modal', function (event) {
        $("#frmIncidenciaEdit input[type='hidden'],#frmIncidenciaEdit input[type='password'],#frmIncidenciaEdit input[type='text'],#frmIncidenciaEdit input[type='file'],#frmIncidenciaEdit select,#frmIncidenciaEdit textarea").not('.mant').val('');
        $("#frmIncidenciaEdit select").multiselect("deselectAll", false);
        $("#frmIncidenciaEdit select").multiselect('refresh');
        $("#txt_ticket_asociado").val('');
        $(".ticketasociado").addClass('hidden');
        $('.tab_1, .tab_2,.tab_3,.tab_4').removeClass("active");
        $('#tab_1, #tab_2,#tab_3,#tab_4').removeClass("active");
        $(".bxslider").html('');
    });

    $(document).on('change', '.slct_estado_req', function(event) {
        var requerimiendo_id = $(this).attr('requerimiendo_id');
        var valor = $(this).val();
        if(requerimiendo_id && valor) {
            Incidencia.CambiarEstadoReq({requerimiendo_id:requerimiendo_id,estado_id:valor});
        }
    });

    $(document).on('click', '#btnAdd', function(event) {
        event.preventDefault();
        var template = $(".t_req[validate=0]").find('.trNuevo').clone().removeClass('trNuevo').removeClass('hidden');
        $(".t_req[validate=0] tbody").append(template);
    });

    var cont = 0;
    $(document).on('click', '#btnAdd2', function(event) {
        event.preventDefault();
        cont+=1;
        var template = $(".t_req[validate=1]").find('.trNuevo').clone().removeClass('trNuevo').removeClass('hidden');
        $(template).find('.file').attr('id','txtfile'+cont);
        $(template).find('.file').attr('name','txtfile'+cont);
        $(template).find('.file').attr('numero',cont);
        $(template).find('.image').attr('id','txt_image'+cont);
        $(template).find('.image').attr('name','txt_image'+cont);
        $(template).find('.openFile').attr('target_f','txtfile'+cont);
        $(".t_req[validate=1] tbody").append(template);
    });

    $("form[name='frmIncidencia']").submit(function(e) {
        e.preventDefault();
        if($.trim($("#slct_tipotrabajo").val()) == ""){
            swal('Seleccione tipo Actividad');
        } else if ($.trim($("#slct_tecnologia").val()) == "") {
            swal('Seleccione sistema');
        } else if ($("#slct_empresa").val() == "") {
             swal('Seleccione empresa');
        } else if ($.trim($("#slct_tipo_legado").val()) == "") {
            swal('Seleccione origen');
        } else if ($.trim($("#slct_nivel_impacto").val()) == "") {
            swal('Seleccione un nivel de impacto');
        } else if ($("#slct_prioridad").val() == "") {
            swal('Seleccione un nivel de prioridad');
        } else if ($("#slct_eactual").val() == "") {
            swal('Seleccione estado actual');
        } else if ($("#txt_obervacion").val() == "") {
            swal('Escriba una observación');
        } else {
            /*validate req*/
            array_incompletos = [];array_req =[];
            if($("#incidencia_t").val()==''){
                $(".t_req[validate=1] tbody tr").each(function (index,el){ 
                    var tipo_valor = $(el).find('#td_tipovalor').find('#slct_tipovalor');
                    var valor = $(el).find('#td_valoi').find('#txt_valor');
                    var detalle = $(el).find('#td_detalle').find('#txt_observacion');
                    if ($(tipo_valor).val()== ''|| $(valor).val()== '' || $(detalle).val()== '') {
                        index=index+1;
                        array_incompletos.push(index);
                    } else {
                        array_req.push({'tipo_valor':$(tipo_valor).val(),'valor':$(valor).val(),'descripcion':$(detalle).val()});
                    }
                });
            }
            /*end validate req*/

            if (array_incompletos.length > 0) {
                swal('En la fila(s) '+array_incompletos.join(",") + ' hay campos vacios');
            } else {
                FormDatos = new FormData($(this)[0]);
                if(array_req.length > 0)
                    FormDatos.append('req',JSON.stringify(array_req));
                Incidencia.create(FormDatos);
            }
        }
    });
    $("form[name='frmIncidenciaEdit']").submit( function(e) {
        e.preventDefault();
        Incidencia.Edit(new FormData($(this)[0]));
    });
    $(document).on('change', '.slct_estadoe', function(event) {
        var incidencia_id= $(this).attr('id-incidencia');
        var valor = $(this).val();
        if (incidencia_id && valor) {
            Incidencia.CambiarEstado({incidencia_id:incidencia_id,estado_actual:valor});
        } else {
            swal('No se pudo actualizar el estado');
        }
    });
    $(document).on('change', '.slct_estado_validacione', function(event) {
        var incidencia_id= $(this).attr('id-incidencia');
        var valor = $(this).val();
        if (incidencia_id && valor) {
            Incidencia.CambiarEstado({incidencia_id:incidencia_id,estado_validacion:valor});
        } else {
            swal('No se pudo actualizar el estado');
        }
    });
    $(document).on('click', '.openFile', function(event) {
        var target = $(this).attr('target_f');
        $("#"+target).trigger('click');
    });
    $(document).on('change', '.file', function(event) {
        readURLI(this, 1,$(this).attr('numero'));
    });
    $(document).on('change', '.fileP', function(event) {
        readURLI(this, 2,$(this).attr('numero'));
    });
    $(document).on('change', '.fileEdit', function(event) {
        readURLI(this, 3,$(this).attr('numero'));
    });

    $('#incidencia').on('hide.bs.modal', function (event) {
        $("#frmIncidencia input[type='hidden'],#frmIncidencia input[type='password'],#frmIncidencia input[type='text'],#frmIncidencia input[type='file'],#frmIncidencia select,#frmIncidencia textarea").not('.mant').val('');
        $("#frmIncidencia select").multiselect('refresh');
        $(".t_req[validate=1] tbody").html('');
    });

    $('#incidencia').on('show.bs.modal', function (event) {
        $("#slct_eactual").val(4);
        $("#slct_estado_validacion").val(4);
        $("#slct_eactual,#slct_estado_validacion").multiselect('refresh');
    });

    function readURLI(input, tipo,numero) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                if (tipo == 1) {
                    $('#txt_image'+numero).val(e.target.result);
                } else if(tipo == 2) {
                    $('#txt_imagep'+numero).val(e.target.result);
                } else {
                    $('#txt_image_edit'+numero).val(e.target.result);
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('click', '#btn_general', function(event) {
        if ($("#collapse2").css("display") == "block") {
            $("#form_movimiento").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
        } else if ($("#collapseTwo").css("display") == "block") {
            $("#form_movimiento").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
        }
        Datatable = Incidencia.listar();
        $("#tipo_busqueda").remove();
    });

    $('#txt_fecha_registro').daterangepicker({
        format: 'YYYY-MM-DD',
        minDate: moment().subtract(35, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
    });
    slctGlobalHtml('slct_tipo_fecha','simple');
    $("#generar_movimientos").click(function (){
        mostrarMovimiento();
    });
    $('.iCheck-helper').click(function(){
        selectFiltro();
    });

    DescargarExcel = function(){
        $("#form_movimiento").attr("action", "incidencia/listar");
        $("#form_movimiento").append("<input type='hidden' name='accion' value='download' id='accion' />");
        $("#form_movimiento").submit();
        $("#accion").remove();
        $("#form_movimiento").attr("action", "");
    };

    HTMLDatosIncidencia = function(datos){
        if (datos) {
            var data = datos[0];
            $("#txt_id_modal").val(data.id);
            $("#incidencia_t").val(data.incidenciat);
            $("#txt_obervacion_modal").val(data.observacion);
            $("#txt_fecha_entrega").val(data.fecha_entrega);
            $("#txt_num_requerimiento_modal").val(data.num_requerimiento);
            $("#slct_tipotrabajo_modal").multiselect('select', data.actividad_id);
            $("#slct_tecnologia_modal").multiselect('select', data.sistema_id);
            $("#slct_empresa_modal").multiselect('select', data.empresa_id);
            $("#slct_tipo_legado_modal").multiselect('select', data.tipo_legado);
            $("#slct_nivel_impacto_modal").multiselect('select', data.nivel_impacto);
            $("#slct_estado_validacion_modal").multiselect('select', data.estado_validacion);
            $("#slct_prioridad_modal").multiselect('select', data.prioridad);
            $("#slct_eactual_modal").multiselect('select', data.estado);
            $("#slct_tipificacion_modal").multiselect('select', data.tipificacion);
            $("#slct_tecnologia_modal").multiselect('select', data.sistema_id);
            $("#slct_tipo_modal").multiselect('select', data.tipo);
            $("#slct_usuarioresp").multiselect('select', data.usuario_responsable_id);
            $("#frmIncidenciaEdit select").multiselect('refresh');

            $("#txt_remedi").val(data.cod_remedi);
            $("#txt_detalle_modal").val(data.detalle);

            if (data.asociado_id) {
                $("#txt_ticket_asociado").val(data.asociado_id);
            } else {
                $("#txt_ticket_asociado").val(data.id);
            }

            if (data.imagenes) {
                var src = '';
                var cont=0;
                var html = '';
                html+='<ul class="bxslider">';
                $.each(data.imagenes.split(","),function(index, el) {
                    html+='<li><img src="incidencia/'+el+'"/></li>';
                });
                html+='</ul>';
                
                $(".slider").html(html);
                $(".Imagenes").removeClass('hidden');
                $(".bxslider").bxSlider();
            } else{
                $(".Imagenes").addClass('hidden');
                $(".bxslider").html('');
            }
        } else {
            swal('no se encuentra informacion de la incidencia');
        }
    };
    saveReq = function(obj) {
        var tr = obj.parentNode.parentNode;
        var tipo_valor = $(tr).find('.slct_tipovalor').val();
        var valor = $(tr).find('.txt_valor').val();
        var observacion = $(tr).find('.txt_observacion').val();
        var estado = $(tr).find('.slct_estado_req').val();
        if ($.trim($("#txt_incidencia_id").val()) != '' && tipo_valor != '' && valor != '') {
            Incidencia.createReq(
                {incidencia_id:$("#txt_incidencia_id").val(),
                tipo_valor:tipo_valor,
                valor:valor,
                observacion:observacion,
                estado:estado}
            );
        } else{
            swal('Faltan datos');
        }
    };

    Deletetr = function(obj){
        var tr = obj.parentNode.parentNode;
        var id_exoneracion = obj.getAttribute('id-exoneracion');
        var r = confirm("¿Esta seguro de cancelar el registro?");
        if (r == true) {
            if(id_exoneracion) {
              
            } else {
                tr.remove(tr);
            }
        }
    };

    pintarSelect = function(name,data,selected,option ,attribute){
        var html = '';
        if (data) {
            key = 0;
            html+='<select class="form-control '+name+'" name="'+name+'" id="'+name+'" '+option+' '+attribute+'>';
            html+=' <option value="0">.::Seleccione::.</option>';
            $.each( data,function(index, el) {
                key=index +1;
                if(key == selected) {
                    html+=' <option value="'+key+'" selected>'+el+'</option>';
                } else{
                    html+=' <option value="'+key+'">'+el+'</option>';
                }
            });
            html+='</select>';
            return html;
        }
    };

    HTMLCargarRequerimientos = function(data){
        html = '';
        if(data){
            $.each( data,function(index, el) {
                html+='<tr>';
                html+='<td>'+pintarSelect('slct_tipovalor',['REQUERIMIENTO','PETICION','SOLICITUD TECNICA','ORDEN TRABAJO','COD CLIENTE'],el.tipo_valor,'disabled')+'</td>';
                html+='<td>'+el.valor+'</td>';
                html+='<td>'+el.descripcion+'</td>';
                html+='<td>'+pintarSelect('slct_estado_req',['PENDIENTE','DOCUMENTACIÓN', 'POR VALIDAR','TERMINADO'],el.estado,"requerimiendo_id='"+el.id+"'")+'</td>';
                html+='<td><a href="incidencia/sub/'+el.image+'" data-fancybox data-caption="'+el.image+'"><span class="btn btn-primary btn-md"><i class="glyphicon glyphicon-eye-open"></i></span></a></td>';
                html+='<td style="text-align:center"><span id="btnDelete" name="btnDelete" class="btn btn-danger  btn-sm btnDelete" style="opacity:0.5"><i class="glyphicon glyphicon-remove"></i></span></td>';    
                html+='</tr>';
            });
            $(".t_req[validate=0] tbody").html(html);
        } else{
            $(".t_req[validate=0] tbody").html('');
        }
    };
});