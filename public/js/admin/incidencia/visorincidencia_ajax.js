    var Datatable;
    var solicitudesT = {
    getVisor: function(data){
        $.ajax({
            url         : 'incidencia/visor',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    ListarVisor(data);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    },
     Responsables: function(data){
        $.ajax({
            url         : 'incidencia/responsables',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                   VisorResponsables(data);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }
}