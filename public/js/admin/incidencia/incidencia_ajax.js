var Datatable;
var Incidencia = {
    listar: function () {
        var datos="";
        var targets=0;
        var estados =['.::Seleccione::.','PENDIENTE', 'POR VALIDAR','TERMINADO','REGISTRADO','EN DESARROLLO','DOCUMENTACIÓN'];
        var eseguimiento= estados;
        $('#tb_incidencia').dataTable().fnDestroy();
        return $('#tb_incidencia').on('page.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('search.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('order.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        })
        .DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                Incidencia.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {
                    if (typeof row.nombre!="undefined" && typeof row.nombre!=undefined) {
                        return row.nombre;
                    } else return "";
                }, name:'nombre'},
                {data : function( row, type, val, meta) {
                    if (typeof row.asociado_id!="undefined" && typeof row.asociado_id!=undefined) {
                        return row.asociado_id;
                    } else return "";
                }, name:'asociado_id'},
                 {data : function( row, type, val, meta) {
                    if (typeof row.tipo!="undefined" && typeof row.tipo!=undefined) {
                        return row.tipo;
                    } else return "";
                }, name:'tipo'},
                {data : function( row, type, val, meta) {
                    if (typeof row.actividad!="undefined" && typeof row.actividad!=undefined) {
                        return row.actividad;
                    } else return "";
                }, name:'actividad'},
                {data : function( row, type, val, meta) {
                    if (typeof row.empresa!="undefined" && typeof row.empresa!=undefined) {
                        return row.empresa;
                    } else return "";
                }, name:'empresa'},
                {data : function( row, type, val, meta) {
                    if (typeof row.usuario_responsable!="undefined" && typeof row.usuario_responsable!=undefined) {
                        return row.usuario_responsable;
                    } else return "";
                }, name:'usuario_responsable'},
                {data : function( row, type, val, meta) {
                    if (typeof row.impacto!="undefined" && typeof row.impacto!=undefined) {
                        return row.impacto;
                    } else return "";
                }, name:'impacto'},
                {data : function( row, type, val, meta) {
                    if (typeof row.prioridad!="undefined" && typeof row.prioridad!=undefined) {
                        return row.prioridad;
                    } else return "";
                }, name:'prioridad'},
                  {data : function( row, type, val, meta) {
                    if(row.detalle !="undefined" && typeof row.detalle!=undefined ){
                        return row.detalle;
                    }else{
                        return "";
                    }
                }, name:'detalle'},
                {data : function( row, type, val, meta) {
                    if (typeof row.observacion!="undefined" && typeof row.observacion!=undefined) {
                        return row.observacion;
                    } else return "";
                }, name:'observacion'},
                {data : function( row, type, val, meta) {
                    if (typeof row.fecha_registro!="undefined" && typeof row.fecha_registro!=undefined) {
                        return row.fecha_registro;
                    } else return "";
                }, name:'fecha_registro'},
                {data : function( row, type, val, meta) {
                    for (var index in estados) {
                        if (index == row.estado_validacion) {
                            return estados[index];
                        }
                    }
                    return '';
                }, name: "estado_validacion"},
                {data : function( row, type, val, meta) {
                    for (var index in estados) {
                        if (index == row.estado) {
                            return estados[index];
                        }
                    }
                    return '';
                }, name:'estado'},
                {data : function( row, type, val, meta) {
                    htmlButtons = "<a class='btn btn-primary btn-md btn-detalle' data-toggle='modal' title='Detalle Incidencia' ";
                    htmlButtons+=" data-target='#incidenciaGestion' data-id='"+row.id+"'>";
                    htmlButtons+=" <i class='glyphicon glyphicon-th-list'></i></a>";
                    htmlButtons+=" <a class='btn btn-danger btn-md btn-eliminar' title='Eliminar Incidencia' ";
                    htmlButtons+=" incidencia-id='"+row.id+"'>";
                    htmlButtons+=" <i class='glyphicon glyphicon-trash'></i></a>";

                    return htmlButtons;
                }, name:'botones'}
            ], 
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 10 , "desc" ]],
            info: true,
            autoWidth: true
        });
    },
    listarMovimientos: function (incidencia_id) {
        var datos="";
        var targets=0;
        $('#t_movimientos_inc').dataTable().fnDestroy();
        return $('#t_movimientos_inc').on('page.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('search.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        }).on('order.dt', function () {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        })
        .DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                Incidencia.ajax_mov(data,callback,incidencia_id);
            },
            "columns":[
                {data : function( row, type, val, meta) {
                    if (typeof row.mov_id!="undefined" && typeof row.mov_id!=undefined) {
                        return row.mov_id;
                    } else return "";
                }, name:'mov_id'},
                 {data : function( row, type, val, meta) {
                    if (typeof row.estado_validacion!="undefined" && typeof row.estado_validacion!=undefined) {
                        return row.estado_validacion;
                    } else return "";
                }, name:'estado_validacion'},
                {data : function( row, type, val, meta) {
                    if (typeof row.estado!="undefined" && typeof row.estado!=undefined) {
                        return row.estado;
                    } else return "";
                }, name:'estado'},
                  {data : function( row, type, val, meta) {
                    if (typeof row.sistema_id!="undefined" && typeof row.sistema_id!=undefined) {
                        return row.sistema_id;
                    } else return "";
                }, name:'sistema_id'},
                {data : function( row, type, val, meta) {
                    if (typeof row.observacion!="undefined" && typeof row.observacion!=undefined) {
                        return row.observacion;
                    } else return "";
                }, name:'observacion'},
                {data : function( row, type, val, meta) {
                    if (typeof row.usuario_created_at!="undefined" && typeof row.usuario_created_at!=undefined) {
                        return row.usuario_created_at;
                    } else return "";
                }, name:'usuario_created_at'},
                {data : function( row, type, val, meta) {
                    if (typeof row.created_at!="undefined" && typeof row.created_at!=undefined) {
                        return row.created_at;
                    } else return "";
                },name :'created_at'},
                {data : function( row, type, val, meta) {
                    if (typeof row.fecha_entrega!="undefined" && typeof row.fecha_entrega!=undefined) {
                        return row.fecha_entrega;
                    } else return "";
                },name :'fecha_entrega'} ,
                {data : function( row, type, val, meta) {
                    if (typeof row.usuario_responsable!="undefined" && typeof row.usuario_responsable!=undefined) {
                        return row.usuario_responsable;
                    } else return "";
                },name :'usuario_responsable'},
                {data : function( row, type, val, meta) {
                    if (typeof row.impacto!="undefined" && typeof row.impacto!=undefined) {
                        return row.impacto;
                    } else return "";
                },name :'impacto'},
                 {data : function( row, type, val, meta) {
                    if (typeof row.asociado_id!="undefined" && typeof row.asociado_id!=undefined) {
                        return row.asociado_id;
                    } else return "";
                },name :'asociado_id'}
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 5 , "desc" ]],
            info: true,
            autoWidth: true
        });
    },
    CambiarEstado: function(data){
        $.ajax({
            url         : 'incidencia/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    Datatable.ajax.reload(null,false);
                }
            },
            error: function(){
                eventoCargaRemover();
            }
        });
    }, 
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#form_movimiento').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('incidencia/listar',form).then( response => {
            callback(response.data);
        }).catch( e => {
            
        }).then(() => {
            eventoCargaRemover();
        });
    },
    ajax_mov: function(request,callback,incidencia_id){
        var contador = 0;
        var form = $('#form_Movimientos').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&incidencia_id="+incidencia_id;

        eventoCargaMostrar();
        axios.post('incidencia/getmovimientos',form).then(response => {
            callback(response.data);
        }).catch(e => {
            
        }).then(() => {
            eventoCargaRemover();
        });
    },
    getById: function(data){
        $.ajax({
            url         : 'incidencia/getbyid',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLDatosIncidencia(data.datos);
                }
            },
            error: function(){
                eventoCargaRemover();
            }
        });
    },
    createReq: function(data){
        $.ajax({
            url         : 'incidenciareq/create',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
               eventoCargaMostrar();
            },
            success : function(data) {
               eventoCargaRemover();
               if(data.rst==1){
                   Incidencia.listarById({incidencia_id:data.incidencia_id});
                   swal('Mensaje','Registrado','success');
               }else{
                   swal('Mensaje','Error al registrar','danger');
               }
            },
            error: function(){
               eventoCargaRemover();
            }
        });
    },
    listarById: function(data){
        $.ajax({
            url         : 'incidenciareq/listar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLCargarRequerimientos(data.datos);
                }
            },
            error: function(){
                eventoCargaRemover();
            }
        });
    },
    CambiarEstadoReq: function(data){
        $.ajax({
            url         : 'incidenciareq/cambiarestado',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){

                }
            },
            error: function(){
                eventoCargaRemover();
            }
        });
    },
    Delete: function(data){
        $.ajax({
            url         : 'incidencia/delete',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if (data.rst==1) {
                    Datatable.ajax.reload(null,false);
                    swal(data.msj);
                } else {
                    swal(data.msj);
                }
            },
            error: function(){
                eventoCargaRemover();
            }
        });
    },
    Edit: function(data){
        $.ajax({
            type: "POST",
            url: 'incidencia/edit',
            data: data,
            processData: false,
            contentType: false,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success: function (obj) {
                eventoCargaRemover();
                if(obj.rst==1) {
                    $("#incidenciaGestion").modal('hide');
                    Datatable.ajax.reload(null,false);
                } else{
                    swal('Error al editar incidencia');
                }
            }
        });
    },
    create: function(data){
        $.ajax({
            type: "POST",
            url: 'incidencia/create',
            data: FormDatos,
            processData: false,
            contentType: false,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success: function (obj) {
                eventoCargaRemover();
                if (obj.rst==1) {
                    $("#incidencia").modal('hide');
                    Datatable.ajax.reload(null,false);
                } else {
                    swal('Error al registrar incidencia');
                }
            }
        });
    }
};