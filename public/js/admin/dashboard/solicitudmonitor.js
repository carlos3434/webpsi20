$("#t_ultimasst").DataTable({"autoWidth": false});
$("#t_ultimasstintentos").DataTable({"autoWidth": false});
$("#t_ultimosmensajestoa").DataTable({"autoWidth": false});
$("#t_ultimosenvioslegados").DataTable({"autoWidth": false});
$(document).ready(function(){

});
SolicitudMonitor.listarUltimasST();
SolicitudMonitor.listarUltimoSTRecibidos();
SolicitudMonitor.listarUltimoMensajesToa();
SolicitudMonitor.listarUltimoEnvioLegado();
setInterval(function(){
	SolicitudMonitor.listarUltimasST();
	SolicitudMonitor.listarUltimoSTRecibidos();
    SolicitudMonitor.listarUltimoMensajesToa();
    SolicitudMonitor.listarUltimoEnvioLegado();
}, 300000);

pintarUltimaST = function() {
	
	$("#t_ultimasst").DataTable().destroy();
	$("#t_ultimasst").DataTable().clear().draw();
	var table = $("#t_ultimasst").DataTable();
	for (var i in listUtlimasST) {
		table.row.add( [
            listUtlimasST[i].created_at,
            listUtlimasST[i].id_solicitud_tecnica,
            listUtlimasST[i].tipo_operacion,
        ] ).draw( false );
	}
};

pintarUltimosSTIntentos = function() {
	
	$("#t_ultimasstintentos").DataTable().destroy();
	$("#t_ultimasstintentos").DataTable().clear().draw();
	var table = $("#t_ultimasstintentos").DataTable();
	for (var i in listUltimosLogRecepcion) {
		table.row.add( [
            listUltimosLogRecepcion[i].created_at,
            listUltimosLogRecepcion[i].id_solicitud_tecnica,
            listUltimosLogRecepcion[i].code_error,
        ] ).draw(true).node();
	}
};

pintarUltimosMensajesToa = function() {
    
    $("#t_ultimosmensajestoa").DataTable().destroy();
    $("#t_ultimosmensajestoa").DataTable().clear().draw();
    var table = $("#t_ultimosmensajestoa").DataTable();
    for (var i in listUltimosMensajesToa) {
        table.row.add( [
            listUltimosMensajesToa[i].created_at,
            listUltimosMensajesToa[i].updated_at,
            listUltimosMensajesToa[i].subject,
            listUltimosMensajesToa[i].estado
        ] ).draw( false );
    }
};

pintarUltimosEnviosLegados = function() {
    
    $("#t_ultimosenvioslegados").DataTable().destroy();
    $("#t_ultimosenvioslegados").DataTable().clear().draw();
    var table = $("#t_ultimosenvioslegados").DataTable();
    for (var i in listUltimosEnviosLegado) {
        table.row.add( [
            listUltimosEnviosLegado[i].created_at,
            listUltimosEnviosLegado[i].id_solicitud_tecnica,
            listUltimosEnviosLegado[i].host,
            listUltimosEnviosLegado[i].accion
        ] ).draw( false );
    }
};