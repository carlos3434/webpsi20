listUtlimasST = [];
listUltimosLogRecepcion = [];
listUltimosMensajesToa = [];
listUltimosEnviosLegado = [];
SolicitudMonitor = {
	listarUltimasST : function() {
		$.ajax({
			type : "GET",
			url	 : "stmonitor/ultimosst",
			success: function(obj) {
				listUtlimasST = obj;
				pintarUltimaST();
			}
		});
	},
	listarUltimoSTRecibidos : function() {
		$.ajax({
			type : "GET",
			url	 : "stmonitor/ultimosstintentos",
			success: function(obj) {
				listUltimosLogRecepcion = obj;
				pintarUltimosSTIntentos();
			}
		});
	},
	listarUltimoMensajesToa : function() {
		$.ajax({
			type : "GET",
			url	 : "stmonitor/ultimosmensajeoutbound",
			success: function(obj) {
				listUltimosMensajesToa = obj;
				pintarUltimosMensajesToa();
			}
		});
	},
	listarUltimoEnvioLegado : function() {
		$.ajax({
			type : "GET",
			url	 : "stmonitor/ultimossenviolegado",
			success: function(obj) {
				listUltimosEnviosLegado = obj;
				pintarUltimosEnviosLegados();
			}
		});
	}
};