
guardarTematico = function(tematicos,comentario,msj) {
    
    swal({
        title: '¿Desea enviar a Legado?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Enviar'

    }).then( function () {
        Legado.reenviarLegado(tematicos, comentario);
        vm.estadoBtnGuardarCierre = true;
    }, function (dismiss) {
        if (dismiss === 'cancel') {
            vm.estadoBtnGuardarCierre = true;
            swal(msj,'No se envió Actividad a Legado! y se registro los tematicos y sus observaciones','warning');
        }
    });
    $("#slct_tematico_id1").val("").change();
    $("#slct_tematico_id1").multiselect("rebuild");
    $("#comentario-tematico").val("");
};
buscarTematico = function(id) {
    for (var i in vm.lista.tematicos) {
        var tematicopadre = vm.lista.tematicos[i];
        for(var j in tematicopadre) {
            var tematicohijo = tematicopadre[j];
            if (tematicohijo.id == id) {
                return tematicohijo;
            }
        }
    }
    return null;
};

/**
* ejecutar al seleccionar horario, se establecen variables globales
* fecha_agenda "2015-03-09"
* horario_agenda "3"
* dia_agenda "09"
* hora_agenda "14pm - 16pm"
*/
afterCupos = function(){
    $("#horario td").click(function() {
        var color = $(this).css("background-color");

        //para IE8 ya que toma el color como lo pone
        if (color.indexOf('#')!=-1) {
            color = color;
        } else {
            color = hexcolor(color);
        }
        id_celda = $(this).attr("title");
        horario_celda = $(this);
        totales=$(this).data("total");
        if (color!="#ff0000" && color!="#f0e535" && color!="#49afcd" && totales>0) {
            //variables globales
            fecha_agenda=$(this).data("fec");
            horario_agenda=$(this).data("horario");
            dia_agenda=$(this).data("dia");
            hora_agenda=$(this).data("hora");

            hf_local=hf;
            clickHorario(horario_celda);
            if (hf===undefined || hf==='undefined' || hf==='') {
                hf=hf_local;
                return;
            }

            $("#horario td").each( function(){
                color = $(this).css("background-color");
                if(color.indexOf('#')!=-1){
                    color = color;
                } else{
                    color = hexcolor(color);
                }
                if(color!="#ff0000" && color!="#f0e535" && color!="#49afcd"){
                    $(this).css({"background":"","color":""});
                }
            });
            $(this).css({"background":"#5cb85c","color":"#fff"});
            $(".horario .help-inline").css("display","none");
            $(".fecha_error").html("").css("display","none");
        }
    });
};
hexcolor=function(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete(parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    color = '#' + parts.join('');
    return color;
};
clickHorario=function(horario_celda){
    fecha_agenda=horario_celda.data("fec");
    if (color!="#5cb85c") {
        $("#txt_fecha_agenda_toa_modal").val(fecha_agenda+ ' - '+fecha_agenda);
        // enviar peticion a ofsc
        $('#slct_agdsla').val();
        if( fecha_agenda==='' ){
            Psi.sweetAlertError('Seleccione Fechas a consultar capacidad');
        }
        else if( $.trim(vm.solicitud.actividad_tipo_id)==='' || $.trim(vm.solicitud.actividad_tipo_id)=='null' ){
            Psi.sweetAlertError('La actuacion no cuenta con Tipo de Actividad para el envio a OFSC');
        }
        else if ( $.trim(vm.solicitud.cod_nodo)==='' || $.trim(vm.solicitud.cod_nodo)=='null' ){
            Psi.sweetAlertError('La actuacion no cuenta con MDF/NODO para el envio a OFSC');
        } else {
            datos = ObtenerCapacityFiltro(fecha_agenda, horario_celda.data("hora"), capacityResponse);
            HTMLCapacity(datos, capacityResponse.duracion);
            if($("#bucket_id").val()!=="") {
                CupoxCapacidad=horario_celda.data("cupos");
                Psi.sweetAlertConfirm("Bucket "+$("#bucket_id").val()+" Seleccionado");
            } else {
                Psi.sweetAlertError("No se encontro Bucket, " + fecha_agenda);
            }
        }
    }
};
ObtenerCapacityFiltro=function(fecha, timeSlot, obj) {
    var timeslotTemp = '';
    var i = 0;
    var j = 0;
    var aA = [];
    var aB = [];
    var prop;
    for ( prop in obj.capacity) {
        if (prop == (fecha + '|' + timeSlot) ) {
            aA[i] = (obj.capacity)[prop];
        }
        i = i++;
    }
    for ( prop in obj.timeslot) {
        timeslotTemp = prop.split('|');
        if (timeslotTemp[1] !== undefined && timeslotTemp[1] == (timeSlot)) {
            aB[j] = (obj.timeslot)[prop];
        }
        j = j++;
    }
    return {'capacity': aA, 'time_slot_info' : aB };
};

HTMLCapacity= function(datos,duracion){
    if (datos === undefined || datos.capacity === undefined ||
    datos.capacity[0] === null  || datos.capacity[0] === undefined ||
    $.isEmptyObject(datos.capacity[0]))
    {
        hf='';
        $('#bucket_id').val('');
        return false;
    }
    $('#bucket_id').val(datos.capacity[0].location);
    var head="";
    var bodyhtml="";
    var capacity=datos.capacity;
    var time_slot_info=datos.time_slot_info;
    var bodypos=[]; var bp=-1;
    var bodytext=[];
    var fechas=[];
    var horas=[];
    $("#t_capacidad tbody,#t_capacidad thead").html("");
    if ( typeof time_slot_info.label != "undefined" ) {
        horas.push(time_slot_info.label+"||"+time_slot_info.name);
    } else {
        $.each(time_slot_info,function(index,data){
            if (horas.indexOf(data.label+"||"+data.name)<0){
                horas.push(data.label+"||"+data.name);
            }
        });
        horas.sort();
    }
    $.each(capacity,function(index,data){
        if( typeof data.time_slot!='undefined' && typeof data.work_skill!='undefined'){
            bodypos.push(data.date+"||"+data.location+"||"+data.time_slot);
            cupos=Math.floor( data.available/duracion );
            bodytext.push(cupos);
            if (fechas.indexOf(data.date+"||"+data.location)<0){
                fechas.push(data.date+"||"+data.location);
            }
        }
    });
    if( $("#slct_agdsla").val()=="sla" ) {
        for(j=0;j<fechas.length;j++){
            bodyhtml+="<tr>"+
                "<td>"+(j+1)+"</td>"+
                "<td style='padding: 0px !important;'>"+
                    "<label class='radio radiotd'>"+
                        "<input type='radio' name='rdb_check' value='"+fechas[j].split("||")[0]+"||"+fechas[j].split("||")[1]+"||0||0-0' class='flat-red' >"+fechas[j].split("||")[1]+
                    "</label>"+
                "</td>"+
                "</tr>";
        }
        $("#t_capacidad tbody").html(bodyhtml);
        head+=    "<tr>"+
                "<th style='text-align: center;'>Pos</td>"+
                "<th style='text-align: center;'>Location</td>"+
            "</tr>";
        $("#t_capacidad thead").html(head);
        $(".overlay,.loading-img").remove();
    } else {
        hf=fechas+"||"+horas;
    }
    $('input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-red',
        radioClass: 'iradio_flat-red'
    });
    $('input[name="rdb_check"]').first().iCheck('check');
    return true;
};
Limpiar=function(obj){
    $("#txt_valor").val(vm.solicitud.id_solicitud_tecnica);
    $("#panel_individual").click();
    $('#slct_tipo').multiselect('select',"id_solicitud_tecnica");
    $('#slct_tipo').multiselect('rebuild');
    $("input[name=data_gestion_ofsc").val("");
    $("#comentarios-agenda").val("");
    $("#BandejaModal").modal("hide");
    hf='';
};
afterConsultaCapacidad=function(response){
    if (response.rst==1 && !$.isEmptyObject(response.capacity)) {
        HTMLCapacityO(response);
        PestanaAgendaToa.verTematicos();
        existecapacidad = true;
    } else if (response.rst==1 && ((response.capacity).length || $.isEmptyObject(response.capacity))) {
        text = '(CM) No hay capacidad en la fecha y hora seleccionado. '+ response.msj;
        Psi.sweetAlertError(text);
        HTMLCapacityO(false);
        PestanaAgendaToa.ocultarTematicos();
        existecapacidad = false;
    } else {
        Psi.sweetAlertError(response.msj);
        HTMLCapacityO(false);
        PestanaAgendaToa.ocultarTematicos();
        existecapacidad = false;
    }
    if (vm.solicitud.estado_ofsc_id == 4 || vm.solicitud.estado_ofsc_id == 1) {
        if (vm.solicitud.estado_aseguramiento == 5 || vm.solicitud.estado_aseguramiento == 1) {
            if (existecapacidad == true) {
                $('#slct_agdsla').multiselect('enable');
                $("#txt_observacion_toa_modal").removeAttr("disabled");
                $('#btn_enviar_ofsc').removeAttr('disabled');
                PestanaAgendaToa.verTematicos();
            }
        } else {
            if (existecapacidad == false) {
                $('#slct_agdsla').multiselect('disable');
                $("#txt_observacion_toa_modal").prop("disabled", true);
                $('#btn_enviar_ofsc').prop('disabled', true);
                PestanaAgendaToa.ocultarTematicos();
            }
        }
    }
};
HTMLCapacityO=function(datos){
    capacityResponse = datos;
    if (capacityResponse === false) {
        $('#btn_enviar_ofsc').addClass('disabled');
        $('#slct_agdsla').multiselect('disable');
    } else {
        $('#btn_enviar_ofsc').removeClass('disabled');
        $('#slct_agdsla').multiselect('enable');
    }
};