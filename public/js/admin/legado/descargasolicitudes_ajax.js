var descargaST = {
    listarSlctMasivo: function (selects, slcts, tipo) {
        var request = {
            selects: JSON.stringify(selects),
            slcts: JSON.stringify(slcts),
        };
        $.ajax({
            url         : 'listado_lego/selects',
            type        : 'POST',
            cache       : false,
            data        : request,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(response) {
                eventoCargaRemover();
                var obj = {};
                for(var i = 0; i < response.data.length; i++){
                    obj.rst = response.rst;
                    obj.datos = response.data[i];
                    htmlListarSlct(obj, slcts[i], tipo[i], [], [], [], []);
                }
            },
            error: function() {
                eventoCargaRemover();
                alert("error")
            }
        });
    },
};