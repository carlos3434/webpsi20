var solicitudObj = [];
var grillaObj = [];
var fecha_agenda="";
var horario_agenda="";
var dia_agenda="";
var hora_agenda="";
var CupoxCapacidad="";
var dtBandeja;
var dtMovimientos;
var detalleMovimiento={};
var capacityResponse = true;
var infoWindows = [];
var tabmodalactivo = true;
var tabsorden = [];
var indice = "";

var Bandeja = {

Buscar: function () {
    solicitudObj = [];
    grillaObj = [];
    return $("#t_listados").DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "stateLoadCallback": function (settings) {
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        },
        "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
            var trsimple;
            for(i=0;i<grillaObj.length;i++){
                var estado = parseInt(grillaObj[i].estado_st);
                htmlestado = "";
                switch(estado) {
                    case 1:
                    htmlestado = "OK";
                    break;
                    case 0:
                    htmlestado = "Error Trama";
                    break;
                    case 2:
                    htmlestado = "Trat. Legados";
                    break;
                }
                trsimple=$("#tb_listado tr td").filter(function() {
                    return $(this).text() == grillaObj[i].id_solicitud_tecnica;
                }).parent('tr');
                $(trsimple).find("td:eq(0)").css("color", grillaObj[i].color_letra);
                $(trsimple).find("td:eq(0)").css("background-color", grillaObj[i].color_ofsc);
                var text = $(trsimple).find("td:eq(0)").text();
                $(trsimple).find("td:eq(0)").html(text+"<br><b>("+htmlestado+")</b>");

            }
            $('#t_listados').removeAttr("style");
            $(".overlay,.loading-img").remove();
        },
        ajax: function(data, callback, settings) {
            Legado.ListarST(data,callback);
        },
        "columns":[
            {data: 'id_solicitud_tecnica', name:'id_solicitud_tecnica'},
            {data : function( row, type, val, meta) {
                if (typeof row.orden_trabajo!="undefined" && typeof row.orden_trabajo!=undefined) {
                    return row.orden_trabajo;
                } else return "";
            }, name:'orden_trabajo'},
            {data : function( row, type, val, meta) {
                if (typeof row.num_requerimiento!="undefined" && typeof row.num_requerimiento!=undefined) {
                    var htmlreq = row.num_requerimiento;
                    if (row.numero_peticiion != 0) {
                        htmlreq+='<br><b>P: '+row.numero_peticiion+'</b>';
                    }
                    return htmlreq;
                } else return "";
            }, name:'num_requerimiento'},
            {data : function( row, type, val, meta) {
                if (typeof row.created_at!="undefined" && typeof row.created_at!=undefined) {
                    return row.created_at;
                } else return "";
            }, name:'created_at'},
            {data : function( row, type, val, meta) {
                if (typeof row.updated_at!="undefined" && typeof row.updated_at!=undefined) {
                    return row.updated_at;
                } else return "";
            }, name:'updated_at'},
            {data : function( row, type, val, meta) {
                htmltipoenvio = "";
                htmlfechaagenda = "";
                if (row.fecha_agenda != null) {
                    htmlfechaagenda = row.fecha_agenda+"<br>";
                }
                switch(parseInt(row.tipo_envio_ofsc)) {
                    case 1:
                        htmltipoenvio = "Agenda";
                    break;
                    case 2:
                        htmltipoenvio = "SLA";
                    break;
                }
                return htmlfechaagenda+htmltipoenvio;
            }, name:'fecha_agenda'},
            {data : function( row, type, val, meta) {
                var tipo_actividad='';
                if ( row.actividad_tipo_nombre!=="" ) {
                    tipo_actividad += row.actividad_tipo_nombre;
                }
                return tipo_actividad;
            }, name:'actividad_tipo_id'},

            {data : function( row, type, val, meta) {
                if (typeof row.quiebre!="undefined" && typeof row.quiebre!=undefined) {
                    return row.quiebre;
                } else return "";
            }, name:'quiebre'},
            {data : function( row, type, val, meta) {
                if (typeof row.parametro!="undefined" && typeof row.parametro!=undefined && row.parametro!='') {
                    htmlparametro = row.parametro+"<br>";
                    let htmlp_tipo = '';
                    if (row.parametro_tipo  !== '') {
                        htmlp_tipo = parametrizacion_tipos[row.parametro_tipo];
                    }
                    return htmlparametro + htmlp_tipo;
                } else return "sin masiva";
            }, name:'parametro'},
            {data : function( row, type, val, meta) {
                var fftt='';
                if ( row.zonal!=="") {
                    fftt = row.zonal;
                }
                if ( row.cod_nodo!=="" ) {
                    fftt += '_'+ row.cod_nodo;
                }
                if ( row.cod_troba!=="" ) {
                    fftt += '_'+ row.cod_troba;
                }
                return fftt;
            }, name:'fftt'},
            {data : function( row, type, val, meta) {
                var html = row.estado_aseguramiento_nombre;
                if (row.estado_aseguramiento == 3 || row.estado_aseguramiento == 5) {
                    if (row.tipodevolucion == "C") {
                        html += "<br><b>Comercial</b>";
                    }
                    if (row.tipodevolucion == "T") {
                        html += "<br><b>Tecnico</b>";
                    }
                }
                if (row.sistema_externo_id == "1") {
                    html += '<b style="color: red"><br>(TOOLBOX)</b>';
                }
                return html;
            }, name:'estado_aseguramiento'},

            { data: 'estado_ofsc', name:'estado_ofsc'},
            { data: 'motivo_ofsc_descripcion', name:'motivo_ofsc_descripcion'},
            {data : function( row, type, val, meta) {
                if(typeof meta == "undefined") {
                    indice = -1;
                } else {
                    indice = meta.row;
                }
                grillaObj[indice] = row;
                htmlButtons = "";
                if (row.estado_aseguramiento != 11) {
                    htmlButtons = "<a class='btn btn-primary btn-xs btn-detalle cierreSelect' data-toggle='modal' title='Detalle Solicitud'  onclick='Ocultar()'";
                    htmlButtons+=" data-target='#BandejaModal' data-id='"+indice+"' data-tipolegado='"+row.tipo_legado+"'";
                    htmlButtons+=" data-indice='"+indice+"' data-solicitudtecnicaid='"+row.solicitud_tecnica_id+"'";
                
                    htmlButtons+=" data-actividadid='"+row.actividad_id+"'>";
                    htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
                }    
                    
                if (row.estado_aseguramiento == 6) {
                    htmlButton = "<a style='display:none' class='btn btn-success btn-xs btn-envio-legado-grilla' onclick='enviarLegadoGrilla()'><i class='fa fa-paper-plane' aria-hidden='true'></i></a>";
                    htmlButtons = htmlButtons + htmlButton;
                }

                return htmlButtons;
            }, name: "botones"},
        ],
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        order: [[ 4, "desc" ]],
        info: true,
        autoWidth: true,
        language: config
    });
},
};