axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

var Legado = {
    gestionando: false,
    ListarST:function(request,callback){
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        var contador = 0;

        if(perfilId === '13'){
            $('#form_buscar').append('<input type="hidden" name="txt_asignado" id="txt_asignado" value="1">');
        }

        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        axios.post('bandejalegado/listar',form).then(response => {
            if (response.data.rst==2) {
                Psi.sweetAlertError(response.data.msj);
            } else {
                $("#txt_asignado").remove();
                callback(response.data);
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    quitarValidacion:function(){
        var request ={
            id_solicitud_tecnica    : vm.solicitud.id_solicitud_tecnica,
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('toa/quitarvalidacionst',request).then(response => {
            vm.lista.solicitudes=response.data.solicitudes;
            if(tipoPersonaId!=3){
                //$("#t_listados").DataTable().ajax.reload(null,false);
                BuscarSolicitud();
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    forzarValidacionCoordenadas:function(comentario){
        var request = {
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id, 
            comentario              : comentario,
            tecnico_id              : vm.solicitud.tecnico_id,
            aid                     : vm.solicitud.aid,
            codactu                 : vm.solicitud.num_requerimiento
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/forzarvalidacioncoordenadas',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);
            } else {
                Psi.sweetAlertError(response.data.msj);
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    reenvioOperacionDeco: function (id,solicitud_tecnica_id) {
        eventoCargaMostrar();
        var f='form_'+id;
        var form = $('#'+f).serialize().split('txt_').join("").split('slct_').join("");
        form+="&id="+id;
        form+="&solicitud_tecnica_id="+solicitud_tecnica_id;

        axios.post('bandejalegado/reenviooperaciones',form).then(response => {
            if (response.data.rst == 1) {
                Legado.ComunicacionLego();
                vm.fn_pintar_divedit();
                Psi.sweetAlertConfirm(response.data.msj);
            } else {
                Psi.sweetAlertError(response.data.msj);
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    reenviarLegado: function(tematicos, comentario) {
        var request ={
            solicitud_tecnica_id : vm.solicitud.solicitud_tecnica_id,
            tematicos            : tematicos,
            comentario           : vm.solicitud.observacion+', GO: '+comentario,
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/reenviarlegado',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);
                $("#BandejaModal").modal().hide();
            } else {
                Psi.sweetAlertError(response.data.msj);                
                $("#btn-envio-legado").removeAttr("disabled");                          
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    showCupos:function(capacityResponse, tipo=7){
        var request = {
            zona                    : vm.solicitud.zonal,
            contrata                : vm.solicitud.codigo_contrata,
            tipo                    : tipo, 
            quiebre                 : vm.solicitud.quiebre_id,
            actividad_tipo_id       : vm.solicitud.actividad_tipo_id,
            mdf                     : vm.solicitud.cod_nodo,
            oCapacity               : capacityResponse.capacity,
            oCapacityMsj            : capacityResponse.msj,
            location                : capacityResponse.location,
            duration                : capacityResponse.duracion
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('agenda/cupos',request).then(response => {
            if(response.data.rst==1){
                $("#html_horario_ofsc").html('').css("display","");
                $('#html_horario_ofsc').html(response.data.html);
                $('#html_horario_ofsc').after(afterCupos);
                $("#div_agenda, #div_agenda2").css("display","");
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    activarPreCierre: function() {
        var tematicos = [];
        $("#contenedor-tematico .slct_tematicos").each(function(){
            var tematico = buscarTematico($(this).val());
            if (tematico!=null && tematico!="") {
                tematicos.push(tematico);
            }
        });
        var request = {
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id,
            comentario              : $("#comentario-tematico").val(),
            tematicos               : tematicos
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/activarprecierre',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);
            } else {
                Psi.sweetAlertError(response.data.msj);
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    resetFormularioToa: function(comentario) {
        var tematicos = [];
        $("#contenedor-tematico .slct_tematicos").each( function(){
            var tematico = buscarTematico($(this).val());
            if (tematico!=null && tematico!="") {
                tematicos.push(tematico);
            }
        });
        var request = {
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id,
            comentario              : comentario,
            tematicos               : tematicos,
            tipo_devolucion         : vm.solicitud.tipodevolucion
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/resetformulariotoa',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);
                PestanaCierre.desactivarResetFormulario();
            }
            if (response.data.rst == 2) {
                Psi.sweetAlertError(response.data.msj);
                $("#btn_cierre").removeAttr('disabled');
                $("#btn_limpiar_formulario").removeAttr('disabled');
                $("#btn_stop_devuelta").removeAttr('disabled');
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    CierreLegado: function () {
        var request = $('#form_cierre').serialize().split('txt_').join("").split('slct_').join("");
        request+="&tecnico_id="+vm.solicitud.tecnico_id;
        request+="&motivo_ofsc_id="+vm.solicitud.motivo_ofsc_id;
        request+="&tipodevolucion="+vm.solicitud.tipodevolucion;
        request+="&actividad_id="+vm.solicitud.actividad_id;
        request+="&solicitud_tecnica_id="+vm.solicitud.solicitud_tecnica_id;
        request+="&tipo_legado="+vm.solicitud.tipo_legado;
        var tematicos = [];
        $("#contenedor-tematico .slct_tematicos").each(function(){
            var tematico = buscarTematico($(this).val());
            if (tematico!=null && tematico!="") {
                tematicos.push(tematico);
            }
        });
        request+="&tematicos="+JSON.stringify(tematicos);
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/cierre',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);                
                /*$("#btn_cierre").removeAttr('disabled');
                $("#btn_limpiar_formulario").removeAttr('disabled');
                $("#btn_stop_devuelta").removeAttr('disabled');*/
                //$("#BandejaModal").modal("hide");
            }
            else if(response.data.rst == 3) {
                Psi.sweetAlertError(response.data.msj);
                $("#btn_cierre").removeAttr('disabled');
                $("#btn_limpiar_formulario").removeAttr('disabled');
                $("#btn_stop_devuelta").removeAttr('disabled'); 
            }
            else {
                Psi.sweetAlertError(response.data.msj);
                $("#btn_cierre").removeAttr('disabled');
                $("#btn_limpiar_formulario").removeAttr('disabled');
                $("#btn_stop_devuelta").removeAttr('disabled');
                //$("#BandejaModal").modal("hide");
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    DetalleSolicitud:function(tabmodalactivo){
        var request = {
            id:             vm.solicitud.solicitud_tecnica_id,
            actividad_id:   vm.solicitud.actividad_id,
            idcms:          vm.solicitud.id,
            tipo_legado:    vm.solicitud.tipo_legado,
            carnet:         vm.solicitud.codigo_tecnico
        };
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
        axios.post('bandejalegado/detalle-solicitud',request).then(response => {
            if (response.data.rst!=1) {
                PestanaCierre.hide(); return;
            }
            var ocultarLoad=true;
            vm.componentes = response.data.componentes;
            vm.comunicacionlego = response.data.comunicacionlego;
            vm.logcambios = response.data.logcambios;
            for (var i = 0; i < response.data.logrecepcion.length; i++) {
                try {
                    
                    response.data.logrecepcion[i].descripcion_error = JSON.parse(response.data.logrecepcion[i].descripcion_error);
                    
                } catch (e) {
                   // Si llega aqui es porque "logrecepcion" no es un JSON
                }
            }

            vm.logrecepcion = response.data.logrecepcion;
            vm.movimientos = response.data.movimientos;
            vm.operaciones = response.data.operaciones;
            var detalle;
            //recorrer los movimientos
            vm.detalle =[];
            for (var i in vm.movimientos) {
                for (var j in vm.movimientos[i].detalle) {
                    vm.detalle.push(vm.movimientos[i].detalle[j]);
                }
            }
            //PestanaOrden.eventoCambios();
            if (vm.solicitud.tipo_legado == 1) {
                //PestanaOrden.pintarMapa();
            } else if (vm.solicitud.tipo_legado == 2) {
                //data = solicitudObj[indice];
            }
            PestanaOrden.validarVisibilidadForzarCoordenadas();
            //PestanaCierre.show();
            PestanaCierre.validarVisibilidad();
            if (tabmodalactivo) {
                PestanaAgendaToa.activarVisibilidad();
                $("#div_agenda").css('display','none');
                //Pendiente, Suspendida, Desprogramado, No Enviado, aseguramiento=3
                PestanaCierre.showBtnCierre();
                if (vm.solicitud.estado_ofsc_id=='1' || vm.solicitud.estado_ofsc_id=='4' ||
                    vm.solicitud.estado_ofsc_id=='7' || vm.solicitud.estado_ofsc_id=='8' ||
                    (vm.solicitud.estado_ofsc_id == '5' && vm.solicitud.estado_aseguramiento == '12')) {
                    if (vm.solicitud.estado_aseguramiento == 6) {
                        $('#btn_enviar_ofsc').addClass('disabled');
                        $('#slct_agdsla').multiselect('disable');
                    } else {
                        Legado.consultaCapacidad(afterConsultaCapacidad);
                        ocultarLoad=false;
                    }
                } else {
                    $('#btn_enviar_ofsc').addClass('disabled');
                    $('#slct_agdsla').multiselect('disable');
                    PestanaAgendaToa.ocultarTematicos();
                }
            } else {
                PestanaAgendaToa.desactivarVisibilidad();
                PestanaAgendaToa.ocultarTematicos();
                PestanaOrden.validarVisibilidadPanel();
            }
            if (ocultarLoad===true) {
                $(".overlay,.loading-img").remove();
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    consultaCapacidad:function(callback){
        var request = {
            actividad_tipo_id       : vm.solicitud.actividad_tipo_id,
            actividad_id            : vm.solicitud.actividad_id,
            json                    : 1,
            nodo                    : vm.solicitud.cod_nodo,
            troba                   : vm.solicitud.cod_troba,
            plano                   : vm.solicitud.cod_plano,
            ffttcobre               : vm.solicitud.ffttcobre,
            quiebre                 : vm.solicitud.quiebre_id,
            zonal                   : vm.solicitud.zonal,
            contrata                : vm.solicitud.codigo_contrata,
            solicitud               : vm.solicitud.id_solicitud_tecnica,
            tipo_legado             : vm.solicitud.tipo_legado,
            cabecera_mdf            : vm.solicitud.cabecera_mdf,
            cod_armario             : vm.solicitud.cod_armario,
            cable                   : vm.solicitud.cable
        };
        //eventoCargaMostrar();
        axios.post('bandejalegado/capacity-multiple',request).then(response => {
            callback(response.data);
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    validaGestionAjax: function(f_end) {
        var request = {solicitud: vm.solicitud};
        axios.post('toa/validagestionst',request).then(response => {
            acceso = response.data.acceso;
            if (acceso.gestionando == "true" || acceso.gestionando == true) {
                Legado.gestionando = true;
                Psi.sweetAlertError("El usuario : "+acceso.nombreusuario+" ya esta gestionando dicha solicitud! ");
                PestanaCierre.hide();
                PestanaAgendaToa.desactivarVisibilidad();
                var tipo_legado = parseInt(vm.solicitud.tipo_legado);
                var tipo_actividad = parseInt(vm.solicitud.actividad_id);
                eventoCargaRemover();
                return false;
            } else {
                Legado.gestionando = false;
            }
            f_end();
        }).catch(e => {
            vm.errors=e;
        });
    },
    validaGestion: function () {
        if ( Legado.gestionando == true ) {
            PestanaCierre.hide();
            PestanaAgendaToa.desactivarVisibilidad();
            $("#form_solicitud input[type=text]").prop("disabled", true);
            $("#btn_editar").prop("disabled", true);

            vm.accesousuario = false;
            return;
        }
        Legado.validaParametrizacion();
    },
    validaParametrizacion:function(){
        var request = {solicitud: vm.solicitud};
        axios.post('toa/validaparametrizacion',request).then(response => {
            acceso = response.data.acceso;
            var tabmodalactivo = true;

            if (acceso.parametrizacion == "false" || acceso.parametrizacion == false) {
                Psi.sweetAlertError("El requerimiento no cumple con los criterios de parametrizacion de envio a TOA!");
                tabmodalactivo = false;
                PestanaAgendaToa.desactivarVisibilidad();
                eventoCargaRemover();
            }
            Legado.DetalleSolicitud(tabmodalactivo);
        }).catch(e => {
            vm.errors=e;
        });
    },
    modalOpenProcess: function() {
        var existecapacidad = true;
        //para averias CMS, que no esten O, no permitir gestionar
        //if (vm.solicitud.estado_st!=1 && vm.solicitud.tipo_legado==1 && vm.solicitud.actividad_id==1 ) {
        //if (false) {
        PestanaOrden.showDatosGo();
        if(vm.solicitud.estado_st==1 || vm.solicitud.estado_st == 0){
            $('#liopt-datago').show();
            //tbDatosGo cms
            if (vm.solicitud.estado_st == 0) {
                Psi.sweetAlertError('Requerimiento con Error de Trama');
                $('#liopt-datago').show();
                PestanaAgendaToa.desactivarVisibilidad();
                PestanaOrden.validarVisibilidadPestana();
                Legado.DetalleSolicitud(false);
                $(".overlay,.loading-img").remove();
                return;
            }
            
        } else {
            PestanaAgendaToa.desactivarVisibilidad();
            PestanaOrden.validarVisibilidadPestana();
            Legado.DetalleSolicitud(false);
            eventoCargaRemover();
            
            if (vm.solicitud.estado_st == 2) {
                Psi.sweetAlertError('Requerimiento para Trabajar en Legados');
                $('#liopt-datago').hide();
                return;
            }
        }
        if ( tipoPersonaId != 3 ) {//telefonica =>3
            Legado.validaGestion();
        } else {
            if (vm.solicitud.estado_st == 1) {
                Legado.validaParametrizacion();
            }
        } /*else {
            Legado.DetalleSolicitud(true);
        } */
        
        $("#form_solicitud").append("<input type='hidden' value='"+vm.solicitud.id+"' name='id'>");
        $("#form_solicitud").append("<input type='hidden' value='"+vm.solicitud.solicitud_tecnica_id+"' name='solicitud_tecnica_id'>");
        
        $("#slct_quiebre_modal").val([vm.solicitud.quiebre_id]);
        $("#slct_quiebre_modal").multiselect("refresh");

        $("#slct_actividad_tipo_modal").val([vm.solicitud.actividad_tipo_id]);
        $("#slct_actividad_tipo_modal").multiselect("refresh");

        var url = 'componentes.operaciones/'+vm.solicitud.num_requerimiento+'/'+666+'/'+vm.solicitud.actividad_id+'/'+vm.solicitud.tipo_legado+'/'+vm.solicitud.id_solicitud_tecnica;
        $("#url_operaciones").attr('href', url);
        PestanaOrden.validarVisibilidadPestana();
        //activar Btn Stop
        if (vm.solicitud.estado_aseguramiento == 3 || vm.solicitud.estado_aseguramiento == 4) {
            $("#btn_stop_devuelta").removeAttr("disabled");
        }

        //tbOrden cms
        var tipo_legado=vm.solicitud.tipo_legado;
        if (tipo_legado==1) {
            if(perfilId==8){
              $('#btnVerBtn').show();
              pintarZonal();
              pintarContrata();
              pintarTiporeq();
              pintarMotivogen();
            }else{
              $('#btnVerBtn').hide();
            }
        }

        //tab toa
        if (vm.solicitud.estado_aseguramiento == 7) {

            $("#litabtoa").hide();
            $("#BandejaModal #tabtoa.tab-pane.fade").removeClass("active");
            $("#BandejaModal #tabtoa.tab-pane.fade").removeClass("in");
            $("#opt-ordencms").tab('show');

        }else{

            $("#litabtoa").show();
            $("#BandejaModal #tabtoa.tab-pane.fade").addClass("active");
            $("#BandejaModal #tabtoa.tab-pane.fade").addClass("in");
            $("#litabtoa").tab('show');
        }
    },
    modalOpen: function(button) {
        var request = {
            tipo_legado             : button.data('tipolegado'),
            solicitud_tecnica_id    : button.data('solicitudtecnicaid'),
            actividad_id            : button.data('actividadid')
        };
        eventoCargaMostrar();
        axios.post('bandejalegado/datasolicitud',request).then(response => {
            if (response.data!="null" && response.data!=null) {
                vm.solicitud = response.data;
                if ( tipoPersonaId != 3 ) {//telefonica =>3
                    Legado.validaGestionAjax(function () {
                        if (Legado.gestionando) {
                            return false;
                        }
                        Legado.modalOpenProcess();
                    });
                } else {
                    Legado.modalOpenProcess();
                }
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    EnvioOfsc:function(callback,hf_envio,tematicos,comentarios){
        var request = {
            hf                  : hf_envio,
            empresa_id          : vm.solicitud.codigo_contrata,
            actividad_tipo_id   : vm.solicitud.actividad_tipo_id,
            actividad_id        : vm.solicitud.actividad_id,
            quiebre             : vm.solicitud.quiebre_id,
            solicitud           : vm.solicitud.id_solicitud_tecnica,
            solicitud_tecnica_id: vm.solicitud.solicitud_tecnica_id,
            codactu             : vm.solicitud.num_requerimiento,
            ffttcobre           : vm.solicitud.ffttcobre,
            agdsla              : $("#slct_agdsla").val(),
            slaini              : $("#txt_slaini").val(),
            cupos               : CupoxCapacidad,
            fono1               : vm.solicitud.telefono1,
            fono2               : vm.solicitud.telefono2,
            fono3               : '',
            fono4               : '',
            fono5               : '',
            tematicos           : tematicos,
            observacion_toa     : comentarios,
        };
        eventoCargaMostrar();
        axios.post('bandejalegado/envioofsc',request).then(response => {
            if(response.data.rst==1){
                callback(response.data);
                Psi.sweetAlertConfirm(response.data.msj);
            } else{
                Psi.sweetAlertError('Error de envio Ofsc;'+ response.data.msj);
            }
        }).catch(e => {
            vm.errors=e;
            Psi.sweetAlertError('<?php echo trans("greetings.mensaje_error"); ?>');
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    getImagenes:function() {
        if (vm.imagenes===null || vm.imagenes.length===0 || vm.imagenes.length==undefined) {
            var request = {
                'aid' : vm.solicitud.aid,
                'tipo' : 'general'
            };
            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            axios.post('bandejalegado/imagenes',request).
                then(response => {
                    vm.imagenes = response.data.imagenes;

                    Vue.nextTick(function(){
                        $("#completadoCarousel").carousel();

                        // Enable Carousel Indicators
                        $(".item1").click(function(){
                            $("#completadoCarousel").carousel(0);
                        });
                        $(".item2").click(function(){
                            $("#completadoCarousel").carousel(1);
                        });
                        $(".item3").click(function(){
                            $("#completadoCarousel").carousel(2);
                        });
                        $(".item4").click(function(){
                            $("#completadoCarousel").carousel(3);
                        });

                        // Enable Carousel Controls
                        $(".left").click(function(){
                            $("#completadoCarousel").carousel("prev");
                        });
                        $(".right").click(function(){
                            $("#completadoCarousel").carousel("next");
                        });

                        $(".overlay,.loading-img").remove();
                    }.bind(vm));
                }).catch(e => {
                    vm.errors=e;
                    $(".overlay,.loading-img").remove();
                });
        }
    },
    stopOrden: function(comentario) {
        
        var tematicos = [];
        $("#contenedor-tematico .slct_tematicos").each( function(){
            var tematico = buscarTematico($(this).val());
            if (tematico!=null && tematico!="") {
                tematicos.push(tematico);
            }
        });
        var request = $('#form_cierre').serialize().split('txt_').join("").split('slct_').join("");
        request+="&solicitud_tecnica_id="+vm.solicitud.solicitud_tecnica_id;
        request+="&comentario="+comentario;
        request+="&tematicos="+tematicos;
        request+="&tipo_devolucion="+vm.solicitud.tipodevolucion;

        eventoCargaMostrar();
        axios.post('bandejalegado/stoporden',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm(response.data.msj);
                /*$("#btn_cierre").removeAttr('disabled');
                $("#btn_limpiar_formulario").removeAttr('disabled');
                $("#btn_stop_devuelta").removeAttr('disabled'); */  
                //$("#BandejaModal").modal("hide");
                //$("#litabcierre").css("display", "none");
                //$('.nav-pills a[href="#tabtoa"]').tab('show');
                
            }
            if (response.data.rst == 2) {
                Psi.sweetAlertError(response.data.msj);
                    $("#btn_cierre").removeAttr('disabled');
                    $("#btn_limpiar_formulario").removeAttr('disabled');
                    $("#btn_stop_devuelta").removeAttr('disabled');               
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    enviarSms: function(form) {
        var request = form.serialize();
        eventoCargaMostrar();
        axios.post('listado_lego/enviarsms',request).then(response => {
            if (response.data.rst == 1) {
                Psi.sweetAlertConfirm('Mensaje Enviado');
                $("#form-sms-legado textarea").val("");
            } else {
                Psi.sweetAlertError(response.data.msj);
            }
            $(".overlay,.loading-img").remove();
        }).catch(e => {
            vm.errors=e;
        });
    },
    getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {
            var obj = response.data;
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
                vm.lista[ selects[i] ] = obj.data[i];
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    guardarTematico: function(tematicos, comentario) {
  
        var request = {
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id,
            tematicos               : tematicos,
            comentario              : comentario,
            tel1                    :$("#txt_tel1").val(),
            tel2                    :$("#txt_tel2").val(),
            contacto                :$("#txt_contacto").val(),
            direccion               :$("#txt_direccion").val(),
            chk_freeview            :$('input:checkbox[name=chk_freeview]:checked').val(),
            chk_descuento           :$('input:checkbox[name=chk_descuento]:checked').val(),
            motivoDevolucion        :$("#slct_motivosdevolucion").val(),
            submotivoDevolucion     :$("#slct_submotivosdevolucion").val(),
            actividad_id            :vm.solicitud.actividad_id,
            tipoDevolucion          :$("#txt_tipoDevolucion").val(),
            tipo_devolucion         :vm.solicitud.tipodevolucion
        };
        eventoCargaMostrar();
        axios.post('bandejalegado/guardartematico',request).then(response => {
            if (response.data.rst == 1) {
                guardarTematico(tematicos,comentario,response.data.msj);                
               
            } else {
                $("#btn-envio-legado").removeAttr("disabled");
                Psi.sweetAlertError(response.data.msj);              
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    guardarDatosGo: function(form) {
        var request = {
            solicitud_tecnica_id    : vm.solicitud.solicitud_tecnica_id,
            quiebre_id              : $("#slct_quiebre_modal").val(),
            actividad_tipo_id       : $("#slct_actividad_tipo_modal").val(),
            contrata                : $("[name=slct_contrata_modal]").val(),
            nodo                    : $("#slct_nodo_modal_cms").val(),
            troba                   : $("#slct_troba_modal_cms").val()
        };
        eventoCargaMostrar();
        axios.post('bandejalegado/guardardatosgo',request).then(response => {
            if (response.data.rst == 1) {
                $("#t_listados").DataTable().ajax.reload(null,false);
                Psi.sweetAlertConfirm(response.data.msj);
            } else {
                Psi.sweetAlertError(response.data.msj);
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    getPerfil : function(perfilId) {
        eventoCargaMostrar();
        form="&id="+perfilId;
        form+="&_token="+document.querySelector('#token').getAttribute('value');
        axios.get('perfil/editar?'+form).then(response => {
            objperfil = response.data.obj;
        }).catch(e => {
            console.log(e);
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    updateCoords : function(data) {        
        eventoCargaMostrar();      
        console.log(data);
        axios.post('bandejalegado/updatecoords',data).then(response => {
            if(response.rst != 1){
                Psi.sweetAlertError(response.msj);
            }else{
                Psi.sweetAlertConfirm('Coodenadas Actualizadas');
            }
        }).catch(e => {
            console.log(e);
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    reenviarRespuestaST : function(solicitud) {
        eventoCargaMostrar();
        if (solicitud == "") {
            $(".overlay,.loading-img").remove();
            return false;
        }

        $.ajax({
            type: "POST",
            data: {solicitud: solicitud},
            url: 'bandejalegado/reenviarrespuesta',
            success: function(obj) {

            }
        });
        $(".overlay,.loading-img").remove();
        return false;
        /*var request = {
            solicitud        : solicitud
        };
        axios.post('bandejalegado/reenviarrespuesta',request).then(response => {
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });*/
    },
   ComunicacionLego:function(){
        var request = {
            id:             vm.solicitud.solicitud_tecnica_id,
            actividad_id:   vm.solicitud.actividad_id,
            idcms:          vm.solicitud.id,
            tipo_legado:    vm.solicitud.tipo_legado,
            carnet:         vm.solicitud.codigo_tecnico
        };
        eventoCargaMostrar();
        axios.post('bandejalegado/detalle-solicitud',request).then(response => {
            vm.comunicacionlego = response.data.comunicacionlego;
        });
    }
};