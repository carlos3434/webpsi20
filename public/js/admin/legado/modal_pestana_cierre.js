var PestanaCierre = {
    show : function() {        
        console.log('show');
        if(vm.solicitud.actividad_id == 2){
            $("#tema1").html("Tematico 1");
            $("#tema2").html("Tematico 2");
            $("#tema3").html("Tematico 3");
            $("#tema4").html("Tematico 4");
        }else {
            $("#tema1").html("Contactabilidad");
            $("#tema2").html("Conformidad");
            $("#tema3").html("Motivo de Conformidad");
            $("#tema4").html("Submotivo de Conformidad");
        }
        
        $("#litabcierre").css("display", "none");
        $("#tab8").css("display", "none");
        if (( vm.solicitud.estado_aseguramiento == 3 || vm.solicitud.estado_aseguramiento == 4) &&
            vm.solicitud.estado_ofsc_id == 2) {
            $("#litabcierre").css("display", "");
            ///Legado.getImagenes();
            $("#tab8").css("display", "");
        }
        $('#form_cierre #count_error_cierre').text(vm.solicitud.contador_error_liquidate);
        $('#form_cierre #txt_codactu').val(vm.solicitud.num_requerimiento);
        $('#form_cierre #txt_tipo_actividad').val(vm.solicitud.actividad_id);
        $(".bloque-datos-tecnico input[type=text]").removeAttr("disabled");
        $("#txt_carnet_tecnico").val(vm.solicitud.carnettecnico);
        $("#txt_datos_tecnico").val(vm.solicitud.nombretecnico);
        $("#txt_celular_tecnico").val(vm.solicitud.celulartecnico);
        $("#txt_submotivo_no_realizado").val(vm.solicitud.submotivoofsc);
        $("#txt_motivo_no_realizado").val(vm.solicitud.motivoofsc);
        $("#txt_tel1").val(vm.solicitud.tel1);
        $("#txt_tel2").val(vm.solicitud.tel2);
        $("#txt_contacto").val(vm.solicitud.contacto);                
        $("#txt_direccion").val(vm.solicitud.direccion);
        if (vm.solicitud.freeview == "1"){
            $('#chk_freeview').iCheck('check');
        }
        if (vm.solicitud.descuento == "1"){
            $('#chk_descuento').iCheck('check');
        }
        $("#div-activar-precierre, #div-btn-envio-legado").css("display", "none");
        if (vm.solicitud.actividad_id == 1) {//averia
            $("#div_tipo_cierre").css("display", "none");
        }
        $("#contenedor-tematico").css("display", "none");
        if (vm.solicitud.tipodevolucion == "C"){
            $("#txt_tipo_devolucion").val("Comercial");
        }
        if (vm.solicitud.tipodevolucion == "T"){
            $("#txt_tipo_devolucion").val("Tecnico");
        }
        if (vm.solicitud.estado_aseguramiento == 1 && vm.solicitud.estado_ofsc_id == 2) {
            $("#litabcierre").css("display", "");
            //Legado.getImagenes();
            $("#tab8").css("display", "");
            PestanaCierre.showActivarCierre();
        }
        if (vm.solicitud.estado_aseguramiento == 3 && vm.solicitud.estado_ofsc_id == 2) {
            PestanaCierre.showPreDevolucion();
            $("#opt-contrata-transferencia").css("display", "block");
            $("#opt-area-transferencia").css("display", "block");
        }
      
        if (vm.solicitud.estado_aseguramiento == 4 && vm.solicitud.estado_ofsc_id == 2) {
            PestanaCierre.showPreLiquidacion();
            $("#opt-contrata-transferencia").css("display", "none");
            $("#opt-area-transferencia").css("display", "none");
        }
        if ((vm.solicitud.estado_aseguramiento == 6 || vm.solicitud.estado_aseguramiento == 5) && (vm.solicitud.estado_ofsc_id == 4 || vm.solicitud.estado_ofsc_id == 6)) {
            $("#litabcierre").css("display", "");
            //Legado.getImagenes();
            $("#tab8").css("display", "");

              //*****************************************************
            if (vm.solicitud.estado_aseguramiento == 5 && vm.solicitud.actividad_id == 2) {
               //$("#btn-envio-legado").html("Confimar Devolucion");
               if (vm.solicitud.estado_ofsc_id == 6) {
                 PestanaCierre.showPreLiquidacionCompletada();
               } else {
                 PestanaCierre.showPreDevolucion();
               }
                //$("#opt-contrata-transferencia").css("display", "none");
                //$("#opt-area-transferencia").css("display", "none");
            }else{
            //*****************************************************            
                PestanaCierre.showActivarCierre();
            }
        }
        $(".bloque-datos-tecnico input[type=text]").prop("disabled", true);
    },
    hide : function() {
        $("#div-activar-precierre, #div-btn-cierre, #div-btn-limpiar-formulario, #div-btn-stop-devuelta").css("display", "inline-block");
        $("#div-activar-precierre").css("display", "none");
        $("#div-btn-envio-legado").css("display", "none");
        $("#panel-datos-tecnicos").css("display", "block");
        $("#enlace-cierre-panel").css("display", "block");
        $("#enlace-imagenes-panel").css("display", "block");
        $("#collapse_cierre_panel").removeClass("in");
        $("#collapse_cierre_panel").prop("aria-expanded", false);
        $("#litabcierre").css("display", "none");
        $("#litabcierre").removeClass("active");
        $("#tab8").css("display", "none");
        $('#form_cierre #count_error_cierre').text("0");
        $('#form_cierre input[type=text]').val("");
        $('#form_cierre input[type=hidden]').val("0");
        $("#div_tipo_cierre, #div_submotivo_cierre").css("display", "block");
        $("#div_tipo_devolucion").css("display", "inline-block");
        $("#lb_motivo_cierre").text("Motivo Devolucion");
        $("#lb_submotivo_cierre").text("Submotivo Devolucion");
        $("#slct_tipo_devolucion").val("");
        $("#slct_tipo_devolucion").multiselect("refresh");
        $("#slct_motivosdevolucion").val("");
        $("#slct_motivosdevolucion").multiselect("refresh");
        $(".slct_tematicos").val("");
        $(".slct_tematicos").multiselect("refresh");
        $("#contenedor-tematico").css("display", "none");
        $("#divtematico1", "#divtematico2, #divtematico3, #divtematico4").css("display", "none");
        $("#btn_cierre").removeAttr("disabled");
        $("#chk_freeview").iCheck('uncheck');
        $("#chk_descuento").iCheck('uncheck');
        $("#btn_cierre").removeAttr('disabled');
        $("#btn_limpiar_formulario").removeAttr('disabled');
        $("#btn_stop_devuelta").removeAttr('disabled');
        vm.visiblecierre = false;
    },
    desactivarResetFormulario: function() {
        $("#btn_limpiar_formulario").prop("disabled", true);
    },
    showPreDevolucion: function() {
        
        $("#title_cierre").text("Confirmar Devolucion");
        $(".opt-motivo-cierre").css("display", "none");
        $("#opt-motivo-cierre-devolucion").css("display", "block");
        $("#txt_tipo_cierre").val("1");
        $("#div_tipo_cierre").css("display", "block");
        if (vm.solicitud.submotivoofsc !="null" && vm.solicitud.submotivoofsc!=null) {
            $("#div_submotivo_cierre").css("display", "block");
        }
        $("#div_tipo_devolucion").css("display", "inline-block");
        $("#slct_tipo_devolucion").val(vm.solicitud.tipodevolucion);
        $("#slct_tipo_devolucion").multiselect("refresh");
        $("#lb_realizar_cierre").text("Confirmar Devolucion");
        $("#contenedor-tematico").css("display", "block");
        if (vm.solicitud.actividad_id == 1) {
            $("#div_tipo_devolucion").css("display", "none");
        }
        $("#divtematico2, #divtematico3, #divtematico4").css("display", "none");
        $("#contenedor-tematico").addClass("in");
        
        Modal.pintarMotivos();
        Modal.pintarSubMotivos();
        Modal.pintarTematicos(0, "slct_tematico_id1", "divtematico1", "div-error-tematico-cierre");
        if (vm.solicitud.estado_aseguramiento == 5 && vm.solicitud.actividad_id == 2) {
            $("#div-btn-envio-legado").css("display", "inline-block");                     
            $("#ddiv-activar-precierre").css("display", "none");
            $("#div-btn-cierre").css("display", "none");
            $("#div-btn-limpiar-formulario").css("display", "none");
            $("#div-btn-stop-devuelta").css("display", "none");
        }else{           
            PestanaCierre.showHideBtnTransferenciaArea();
            PestanaCierre.visibilidadBtnStop();
            PestanaCierre.visibilidadBtnReset();
            PestanaCierre.visibilidadConformidad();
        }
        
    },
    showPreLiquidacionCompletada: function() {
        $("#div-btn-envio-legado").css("display", "block");                    
        $("#ddiv-activar-precierre").css("display", "none");
        $("#div-btn-cierre").css("display", "none");
        $("#div-btn-limpiar-formulario").css("display", "none");
        $("#div-btn-stop-devuelta").css("display", "none");
        $("#opt-motivo-cierre-devolucion").css("display", "none");
        $("#opt-motivo-cierre-liquidacion").css("display", "none");
        $("#cont-tematico").css("display", "none");  
        $("#contenedor-tematico").css("display", "block");  
        $("#enlace-cierre-panel").css("display", "none");
    },
    showPreLiquidacion: function() {
        
        /*if (vm.solicitud.actividad_id == 2 ) {
            $("#btn_cierre").prop("disabled", true);
        }*/
        $("#divtematico2, #divtematico3, #divtematico4").css("display", "none");
        $("#contenedor-tematico").css("display", "block");
        Modal.pintarTematicos(0, "slct_tematico_id1", "divtematico1", "div-error-tematico-cierre");
        $("#title_cierre").text("Confirmar Liquidacion");
        $(".opt-motivo-cierre").css("display", "none");
        $("#enlace-cierre-panel").css("display", "block");
        $("#collapse_cierre_panel").removeClass("in");
        $("#txt_tipo_cierre").val("2");
        $("#lb_motivo_cierre").text("Motivo Liquidacion");
        $("#lb_submotivo_cierre").text("Submotivo Liquidacion");
        $("#div_tipo_devolucion").css("display", "none");
        $("#lb_realizar_cierre").text("Confirmar Cierre");
        PestanaCierre.visibilidadBtnStop();
        PestanaCierre.visibilidadBtnReset();
        PestanaCierre.visibilidadConformidad();
        PestanaCierre.showHideBtnTransferenciaArea();
    },
    showActivarCierre : function() {
        $("#divtematico2, #divtematico3, #divtematico4").css("display", "none");
        $("#contenedor-tematico").css("display", "block");
        $("#panel-datos-tecnicos").css("display", "none");
      
        $("#contenedor-tematico").addClass("in");
        $("#div-activar-precierre, #div-btn-envio-legado").css("display", "none");
        if (vm.solicitud.estado_aseguramiento == 1 && vm.solicitud.estado_ofsc_id == 2) {
            $("#collapse_cierre_panel").removeClass("in");
            $("#div-activar-precierre").css("display", "inline-block");
        }

        if (vm.solicitud.estado_aseguramiento == 6 || vm.solicitud.estado_aseguramiento == 5) {
            $("#enlace-imagenes-panel").css("display", "none");
            $("#enlace-cierre-panel").css("display", "none");
            $("#collapse_cierre_panel").removeClass("in");
            $("#div-btn-envio-legado").css("display", "inline-block");
        }

         if (vm.solicitud.estado_aseguramiento == 6 && vm.solicitud.actividad_id == 2) {           
            $("#contenedor-tematico").css("display", "none");
            $("#div-btn-envio-legado").css("display", "none");               
        }

        $("#div-btn-cierre, #div-btn-limpiar-formulario, #div-btn-stop-devuelta").css("display", "none");
        Modal.pintarTematicos(0, "slct_tematico_id1", "divtematico1", "div-error-tematico-cierre");
    },
    showHideBtnTransferenciaArea : function() {
        if (vm.solicitud.actividad_id == "2") {
            $("#opt-contrata-transferencia, #opt-area-transferencia").css("display", "none");
        }
        if (vm.solicitud.actividad_id == "1") {
            $("#opt-contrata-transferencia, #opt-area-transferencia").css("display", "inline-block");
        }
    },
    showBtnCierre : function() {
        $(".btn-envio-legado").css("display", "inline-block");
    },
    hideBtnCierre : function() {
        $(".btn-envio-legado").css("display", "none");
    },
    visibilidadBtnStop : function() {
        if (objperfil.opciones[0]!="null" && objperfil.opciones[0]!=null) {
            var valida = validaPermisoPerfil(2);
            if (valida) {
                $("#div-btn-stop-devuelta").css("display", "inline-block");
            } else {
                $("#div-btn-stop-devuelta").css("display", "none");
            }
        }
    },
    visibilidadBtnReset : function() {
        if (objperfil.opciones[0]!="null" && objperfil.opciones[0]!=null) {
            var valida = validaPermisoPerfil(3);
            if (valida) {
                $("#ddiv-btn-limpiar-formulario").css("display", "inline-block");
            } else {
                $("#div-btn-limpiar-formulario").css("display", "none");
            }
        }
    },
    visibilidadConformidad : function() {
        if (objperfil.opciones[0]!="null" && objperfil.opciones[0]!=null) {
            var valida = validaPermisoPerfil(4);
            if (valida) {
                $("#div-btn-cierre").css("display", "inline-block");
            } else {
                $("#div-btn-cierre").css("display", "none");
            }
        }
    },
    validarVisibilidad : function() {
        vm.visiblecierre = true;
        if (vm.solicitud.estado_ofsc_id == 2) {
            if (vm.solicitud.estado_aseguramiento == 1 || vm.solicitud.estado_aseguramiento >=5 ) {
                vm.visiblecierre = false;
            }
        }
        if (vm.solicitud.estado_ofsc_id == 4) {
            if (vm.solicitud.estado_aseguramiento == 5 || vm.solicitud.estado_aseguramiento == 6) {
                vm.visiblecierre = true;
            } else {
                vm.visiblecierre = false;
            }
        }
        if (vm.solicitud.estado_ofsc_id == 6) {
            if (vm.solicitud.estado_aseguramiento == 6) {
                vm.visiblecierre = true;
            } else {
                vm.visiblecierre = false;
            }
        }
        if (vm.solicitud.estado_st !=1) {
            vm.visiblecierre = false;
        }
        if(vm.solicitud.estado_aseguramiento == 5 && vm.solicitud.estado_ofsc_id == 6 && vm.solicitud.actividad_id  == 2) {
            vm.visiblecierre = true;
        }
        if (vm.visiblecierre) {
            PestanaCierre.show();
        } else {
            PestanaCierre.hide();
        }
    }
};
validarInputsCierre = function() {

    var intentos = $('#form_cierre #count_error_cierre').text();
    if (intentos >= 5) {
        Psi.sweetAlertError('solicitud excede maximo de envios');
        return;
    }
    var title = "";
    var text = "";

    if (vm.solicitud.estado_aseguramiento == 3) {
        if( $('#divsubmotivosdevolucion').is(":visible") ){
            if($('#slct_submotivosdevolucion').val()==''){
                Psi.sweetAlertError('Ingrese Submotivo');
                return;
            }
        }

        var regla = requiredTematicos();
        if (parseInt(regla) == 0) {
            Psi.sweetAlertError('Seleccionar tematicos');
            return;
        }

        if (!validarFormCierre()){
            return;
        }
        //*************************

        if ($("#form_cierre #comentario-tematico").val() == "" || 
            $("#form_cierre #comentario-tematico").val() == undefined) {
            Psi.sweetAlertError('Ingresar observacion para confirmar la Devuelta');
            return;
        }      

        title = "¿Desea confirmar la Devuelta?";
        text = "En TOA se cerrara como No Realizado y se Devuelve a Legado";

        //$("#btn_cierre").attr('disabled', 'disabled');
        //$("#btn_limpiar_formulario").attr('disabled', 'disabled');
        //$("#btn_stop_devuelta").attr('disabled', 'disabled');

        var motivosdevolucion = $('#form_cierre #slct_motivosdevolucion').val();
        var actividad = $('#form_cierre #txt_tipo_actividad').val();
        var tipocierre = $("#txt_tipo_cierre").val();

        if ( tipocierre == 1 ) { // 1 es devolucion, 2 es liquidacion
            if (motivosdevolucion == "" || motivosdevolucion == undefined) {
                Psi.sweetAlertError('Seleccionar motivo devolucion');
                return;
            }
        }
    } else {
        title = "¿Desea confirmar el Cierre?";
        text = "En TOA se cerrara como Completado y se Cierra en Legado";

    }
    swal({
        title: title,
        text: text,      
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Enviar'
    }).then(function () { 
        $("#btn_cierre").attr('disabled', 'disabled');
        $("#btn_limpiar_formulario").attr('disabled', 'disabled');
        $("#btn_stop_devuelta").attr('disabled', 'disabled');        
        Legado.CierreLegado();
    },function (dismiss) {
  
      if ( dismiss === 'cancel' ) {
        //$("#btn_cierre").removeAttr('disabled');
        //$("#btn_limpiar_formulario").removeAttr('disabled');
        //$("#btn_stop_devuelta").removeAttr('disabled');
      }

    });
    return false;
};
activarPreCierre = function() {
    swal({
        title: '¿Desea activar el Precierre?',
        text: "Se forzara la activacion del formulario de PreCierre en TOA",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Forzar'
    }).then(function () {
        Legado.activarPreCierre();
    });
};
stopOrden = function(){

    if( $('#divsubmotivosdevolucion').is(":visible") ){
        if($('#slct_submotivosdevolucion').val()==''){
            Psi.sweetAlertError('Ingrese Submotivo');
            return;
        }
    }

    var regla = requiredTematicos();
    if (parseInt(regla) == 0) {
        Psi.sweetAlertError('Seleccionar tematicos');
        return;
    }

    if (!validarFormCierre()){
            return;
    }

    stop_boton_name = 'cierre posterior';
    stop_swal_text = 'Comente el motivo por el que se realiza la confirmación de cierre posterior de la Orden';
    if(vm.solicitud.actividad_id == 2){
        stop_boton_name = 'volver a llamar';
        stop_swal_text = 'Comente el motivo por el que volverá a llamar al cliente';
    }       
    swal({
        title: 'Confirmación de ' + stop_boton_name,
        text: stop_swal_text,
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Detener',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function (textarea) {
        Legado.stopOrden(textarea); //
        $("#btn_cierre").attr('disabled', 'disabled');
        $("#btn_limpiar_formulario").attr('disabled', 'disabled');
        $("#btn_stop_devuelta").attr('disabled', 'disabled');
    },function (dismiss) {  
      if (dismiss === 'cancel') {
        $("#btn_cierre").removeAttr('disabled');
        $("#btn_limpiar_formulario").removeAttr('disabled');
        $("#btn_stop_devuelta").removeAttr('disabled');
      }
    });
};
resetFormularioToa = function(){
    var regla = requiredTematicos();
    if (parseInt(regla) == 0) {
        Psi.sweetAlertError('Seleccionar tematicos');
        return;
    }
    if (!validarFormCierre()){
            return;
    }
    swal({
        title: 'Reset Formulario de PreCierre TOA',
        text: 'Comente porque el tecnico debe continuar con la Orden',
        input: 'textarea',
        showCancelButton: true,
        confirmButtonText: 'Reset',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function (textarea) {
        Legado.resetFormularioToa(textarea);
        $("#btn_cierre").attr('disabled', 'disabled');
        $("#btn_limpiar_formulario").attr('disabled', 'disabled');
        $("#btn_stop_devuelta").attr('disabled', 'disabled');
    },function (dismiss) {  
      if (dismiss === 'cancel') {
        $("#btn_cierre").removeAttr('disabled');
        $("#btn_limpiar_formulario").removeAttr('disabled');
        $("#btn_stop_devuelta").removeAttr('disabled');
      }
    });
};
enviarLegado = function(){
  
    var bandera=true;
    var tematicos = [];   
    var comentario = "";
    // if (vm.solicitud.actividad_id == 1) {
        $("#contenedor-tematico .slct_tematicos").each(function(){
            var tematico = buscarTematico($(this).val());
            if (tematico!=null && tematico!="") {
                tematicos.push(tematico);
            }
        });
        comentario = $("#comentario-tematico").val();

        if (!validarFormCierre()){
            return;
        }

        if ($("#form_cierre #comentario-tematico").val() == "" || 
            $("#form_cierre #comentario-tematico").val() == undefined) {
            if (vm.solicitud.estado_ofsc_id == 6) {
                Psi.sweetAlertError('Ingresar Comentario para confirmar la liquidación');
            } else {
                Psi.sweetAlertError('Ingresar Comentario para confirmar la devuelta');
            }
            return;
        }

        if(bandera){            
            Legado.guardarTematico(tematicos, comentario, true);
            //$("#btn-envio-legado").attr('disabled', 'disabled');
        }
    
};


enviarLegadoGrilla = function(){
    swal({
        title: '¿Desea reenviar a Legado Nuevamente?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Enviar'
    }).then(function () {
        var tematicos = [];
        var comentario = "";
        Bandeja.enviarLegado(tematicos, comentario);
    });
};

requiredTematicos = function() {
    var display = $("#div-error-tematico-cierre").css("display");
    var required = 0;
    if (display == "block") {
        return 1;
    }

    $("#contenedor-tematico .slct_tematicos").each(function(){
        var tematicos = $(this).val();
        if (tematicos) {
            required++;
        }
    });
    return required;
}

validarFormCierre= function(){

     if ( vm.solicitud.actividad_id == 2 ) {
            if( $('#divtematico1').is(":visible") ){
                if($("#slct_tematico_id1").val()==""){
                    Psi.sweetAlertError('Ingrese tematico 1');
                    return false;
                    
                }
            }

            if( $('#divtematico2').is(":visible") ){
                if($("#slct_tematico_id2").val()==""){
                    Psi.sweetAlertError('Ingrese tematico 2');
                    return false;
                    
                }
            }


            if( $('#divtematico3').is(":visible") ){
                if($("#slct_tematico_id3").val()==""){
                    Psi.sweetAlertError('Ingrese tematico 3');
                    return false;
                    
                }
            }


            if( $('#divtematico4').is(":visible") ){
                if($("#slct_tematico_id4").val()==""){
                    Psi.sweetAlertError('Ingrese tematico 4');
                    return false;
                    
                }
            }
         }   

        //*************************

        if ( $("#slct_tematico_id1").val() == 1 ) {
            if($("#txt_tel1").val()==""){
                Psi.sweetAlertError('Ingrese Telefono 1');
                return false;
            } else if ($("#txt_tel1").val().length<7) {
                Psi.sweetAlertError('Ingrese Telefono 1 Valido');
                return false;
            } else if($("#txt_tel2").val()==""){
                Psi.sweetAlertError('Ingrese Telefono 2');
                return false;
            }else if ($("#txt_tel2").val().length<7) {
                Psi.sweetAlertError('Ingrese Telefono 2 Valido');
                return false;
            }
            else if($("#txt_contacto").val()==""){
                Psi.sweetAlertError('Ingrese Contacto');
                return false;
            }else if($("#txt_direccion").val()==""){
                Psi.sweetAlertError('Ingrese Direccion');
                return false;
            }               
        }

    return true;
}

$(document).ready(function(){   
    $("#BandejaModal").on('hidden.bs.modal', function () {
            $("#btn_cierre").removeAttr('disabled');
            $("#btn_limpiar_formulario").removeAttr('disabled');
            $("#btn_stop_devuelta").removeAttr('disabled');
            $("#btn-envio-legado").removeAttr("disabled");          

    });

});

