var gm = google.maps;

var config = {
    el: 'mapafftts',
    lat: -12.109129,
    lon: -77.016123,
    zoom: 15,
    minZoom: 15,
    type: gm.MapTypeId.ROADMAP
};

var spiderConfig = {
    keepSpiderfied: true,
    event: 'mouseover'
};

var mapOptions = {
    center: new gm.LatLng(config.lat, config.lon),
    zoom: config.zoom,
    mapTypeId: config.type
};
var markerSpiderfier;

removeMarkers = function() {
    for (var i = 0; i < vm.markers.length; i++) {
        vm.markers[i].setMap(null);
    }
    vm.markers=[];
};
addMarkerCoord = function(icon,detalle) {
    coordx_tecnico = parseFloat(detalle.coordx_tecnico);
    coordy_tecnico = parseFloat(detalle.coordy_tecnico);
    fecha_registro  = detalle.created_at.substring(0, 10);
    hora_registro = detalle.created_at.substring(11, 19);
    label = "<label>Validacion de coordenadas</label><br>"+
    "<label>Fecha: "+fecha_registro+'</label><br>'+
    "<label>Hora: "+hora_registro+'</label><br>'+
    "<label>Distancia: "+detalle.distancia+' metros</label><br>'+
    "<label>"+detalle.sms+"</label>";

    addMarker(coordy_tecnico, coordx_tecnico,label,icon);
};
addMarkerMov = function(icon,movimiento) {
    coordx_tecnico = parseFloat(movimiento.coordx_tecnico);
    coordy_tecnico = parseFloat(movimiento.coordy_tecnico);
    fecha_registro  = movimiento.created_at.substring(0, 10);
    hora_registro = movimiento.created_at.substring(11, 19);
    label = "<label>Estado: "+movimiento.estado+'</label><br>'+
    "<label>Fecha: "+fecha_registro+'</label><br>'+
    "<label>Hora: "+hora_registro+'</label><br>'+
    "<label>Origen: "+movimiento.usuario+'</label><br>'+
    "<label>Carnet: "+movimiento.resource_id+'</label>';

    addMarker(coordy_tecnico, coordx_tecnico,label,icon);
};
addMarker=function(coordy,coordx,label,icon){
    var location = new gm.LatLng(coordy, coordx);
    var marker = new MarkerWithLabel({
        position: location,
        icon: icon,
        map: vm.map,
        draggable: true,
        animation: google.maps.Animation.DROP,
    });

    vm.markers.push(marker);
    vm.bounds.extend(location);
  
    marker.infowindow = new gm.InfoWindow({content: label});
    gm.event.addListener(marker,'click', function() {
        if(infoWindows.length>0){
            for (var j=0;j<infoWindows.length;j++) {
                infoWindows[j].close();
            }
        }
        this.infowindow.open(vm.map,this);
        infoWindows.push(this.infowindow);
    });
    markerSpiderfier.addMarker(marker);
    dragged(marker);
};

dragged = function(marker){
    var lat_bucket = '';
    var long_bucket = '';
    gm.event.addListener(marker, 'dragend', function(event) {      
        lat_bucket_y = event.latLng.lat();
        long_bucket_x = event.latLng.lng();
        var confirm = window.confirm("¿Esta seguro de guardar nuevas coordenadas?");
        if (confirm){ 
            appointment = {};
            appointment.coordx = long_bucket_x;
            appointment.coordy = lat_bucket_y;
            var data ={'solicitud_tecnica_id':vm.solicitud.solicitud_tecnica_id,'num_requermiento': vm.solicitud.num_requerimiento,'appointment': JSON.stringify(appointment)};
            Legado.updateCoords(data);
        }
    });
}

addLine=function(movimiento, coordy_cliente, coordx_cliente){
    coordx_tecnico = parseFloat(movimiento.coordx_tecnico);
    coordy_tecnico = parseFloat(movimiento.coordy_tecnico);
    var locationTec =  new gm.LatLng(coordy_tecnico, coordx_tecnico);
    var locationCli =  new gm.LatLng(coordy_cliente, coordx_cliente);

    try {vm.line.setMap(null);}catch(c){}
    vm.line = new gm.Polyline({
        path: [
            locationCli,
            locationTec
        ],
        strokeColor: "#000000",
        strokeOpacity: 1.0,
        strokeWeight: 2,
        map: vm.map
    });

    var distancia = gm.geometry.spherical.computeDistanceBetween(locationCli,locationTec);
    distancia = distancia.toFixed(2);

    //Centro del polilyne Tecnico - Agenda
    var pathBounds = new gm.LatLngBounds();
    pathBounds.extend( locationCli );
    pathBounds.extend( locationTec );

    var marker = new MarkerWithLabel({
        position: pathBounds.getCenter(),
        map: vm.map,
        labelContent: "<div>"+distancia + " metros.</div>",
        labelAnchor: new gm.Point(22, 0),
        size: new gm.Size(20, 20),
        labelStyle: {opacity: 1, background: "#fff", padding: "10px"}
    });
    vm.markers.push(marker);
};