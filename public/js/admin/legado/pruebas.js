$(document).ready(function () {
    $("#txt_resultado").val('');
    $("#txt_resultadopcba").val('');
});

RealizarPrueba2=function(){
    var datos = $("#form_analizador").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
        $.ajax({
            url         : 'pcba/pruebacompletas',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                $("#txt_resultado, #txt_resultadoassia").val('');
                $("#tablaspcba").html('');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.responsepcba.rst==1){
                    MostrarResultadoPcba(obj.responsepcba.resultado);
                }
                if(obj.responseelec.rst==1){
                    $("#txt_resultado").val(obj.responseelec.resultado['interprete']);
                } else {
                    if(obj.responseelec.resultado['msj']!==undefined) {
                        $("#txt_resultado").val(obj.responseelec.resultado['msj']);
                    } 
                    if(obj.responseelec.resultado.descripcion!==undefined) {
                        $("#txt_resultado").val(obj.responseelec.resultado.descripcion);
                    }
                }
                if(obj.responseassia.rst==1){
                    $("#txt_resultadoassia").val(obj.responseassia.resultado['msj']);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                 Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    
};

MostrarResultadoPcba=function(resultado){
    //console.log(resultado);
    var html="";
    $.each(resultado, function( index, value ) {
        html+='<div class="box-body table-responsive">';
        if(index=='DatosEntrada'){
            html+='<p class="text-danger"><u><b>Datos Entrada</b></u></p>';
            html+='<table class="table table-bordered table-striped">';
            html+='<tbody class="text-center">';

            var i=1;
            var ultimo=value.length;
            $.each(value, function( indice, valor ) {
                if(i==1) { html+='<tr>'; }
    
                html+='<td><b>'+indice+':</b></td>';
                if( typeof valor !== 'object'){
                    html+='<td>'+valor+'</td>';
                } else {
                    html+='<td>'+JSON.stringify(valor)+'</td>';
                }
    
                if(i!=1 && i!=ultimo && i%2==0 ) { html+='</tr></tr>'; }
                if(i==ultimo) {html+='</tr>';}
    
                i++;
            });

        }  else if(index=='PruebaDslam'){
            html+='<p class="text-danger"><u><b>Prueba Dslam</b></u></p>';
            html+='<table class="table table-bordered table-striped">';
            html+='<tbody class="text-center">';

             $.each(value, function( indice, valor ) {

                var style='';
                var styled='';
                if(indice =='EstadoPrueba' || indice=='SenalRuidoBajada') {
                    if(valor.Estado!==undefined && valor.Estado=='OK'){
                        style="background:#16FF05"; //verde
                    } else {
                        style="background:#FF2605";
                    }
                }
                if(indice =='SenalRuidoBajada') {
                    //($senaldown!=0) && ($senaldown!='') && ($senaldown>12)
                    if(valor.Descripcion!==undefined 
                            && valor.Descripcion!=0
                             && valor.Descripcion!==''
                              && valor.Descripcion>12){
                        styled="background:#16FF05"; //verde
                    } else {
                        styled="background:#FF2605";
                    }
                }

                if(indice =='AtenuacionDownstream') {
                    //if(($atedown!=0) && ($atedown!='') && ($atedown!=6) && ($atedown<65))
                    if(valor.Descripcion!==undefined 
                            && valor.Descripcion!=0
                             && valor.Descripcion!==''
                              && valor.Descripcion!=6
                              && valor.Descripcion<65){
                        style=styled="background:#16FF05"; //verde
                    } else {
                        style=styled="background:#FF2605";
                    }
                }

                html+='<tr>';
                html+='<td style="'+style+'"><b>'+indice+':</b></td>';
                if(valor.Estado!==undefined) {
                    html+='<td style="'+style+'">'+valor.Estado+'</td>';
                } else {
                    html+='<td style="'+style+'"></td>';
                }
                html+='<td style="'+styled+'"><i>Descripción:</i></td>';
                if(valor.Descripcion!==undefined) {
                    if( typeof valor.Descripcion !== 'object'){
                        html+='<td style="'+styled+'">'+valor.Descripcion+'</td>';
                    } else {
                        html+='<td style="'+styled+'">'+JSON.stringify(valor.Descripcion)+'</td>';
                    }

                } else {
                    html+='<td style="'+styled+'"></td>';
                }
                html+='</tr>';
            });

        } else if(index=='PruebaRadian'){
            html+='<p class="text-danger"><u><b>Prueba Radian</b></u></p>';
            html+='<table class="table table-bordered table-striped">';
            html+='<tbody class="text-center">';
            $.each(value, function( indice, valor ) {
                html+='<td><b>Username</b></td>';
                html+='<td>'+valor.Descripcion+'</td>';
            });

        } else if(index=='LDAP'){
            html+='<p class="text-danger"><u><b>LDAP</b></u></p>';
            html+='<table class="table table-bordered table-striped">';
            html+='<tbody class="text-center">';

            var i=1;
            var ultimo=value.length;
            $.each(value, function( indice, valor ) {
                if(i==1) { html+='<tr>'; }
                var style='';
                if(indice =='NASPortId' || indice=='DescripcionServicio') {
                    if(valor.Descripcion!==undefined 
                        && typeof valor.Descripcion !== 'object' 
                        && valor.Descripcion!==''
                        && valor.Descripcion!=='/'){
                        style="background:#16FF05";
                    } else {
                        style="background:#FF2605";
                    }
                }

                html+='<td style="'+style+'"><b>'+indice+'</b></td>';

                if(indice!=='VirtualRouter') {
                    if( typeof valor.Descripcion !== 'object'){
                        html+='<td style="'+style+'">'+valor.Descripcion+'</td>';
                    } else {
                        html+='<td style="'+style+'">'+JSON.stringify(valor.Descripcion)+'</td>';
                    }
                } else {
                    html+='<td colspan="2">'+valor.Estado+'</td>';
                    html+='<td>'+valor.Descripcion+'</td>';
                }
    
                if(i!=1 && i!=ultimo && i%2==0 ) { html+='</tr></tr>'; }
                if(i==ultimo) {html+='</tr>';}

                i++;
            });
        } 

        html+='</tbody></table></div>';
        $("#tablaspcba").html(html);
    });
};