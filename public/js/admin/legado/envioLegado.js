$(document).ready(function(){
    Datatable = solicitudesT.listar();

    $('#rangoFechas').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        autoApply: true,
        format: 'YYYY-MM-DD'
     });

    var datos_={parametros:'1', prueba:'test'};

    var selects = [     
     "EnvioLegado_Host" ,     
     "EnvioLegado_Accion"
     ];

    var slcts = [
      
     "slct_EnvioLegado_Host",     
     "slct_EnvioLegado_Accion"
    ];

    var tipo = [
     "multiple", "multiple"
    ];

    var valarray = [
     null, null
     
    ];
    var data = [
     {}, {}
    ];
    var afectado = [
     0, 0
    ];
    var afectados = [
     "", ""
    ];
    var slct_id = [
     "", ""
    ];
    var es_lista =[
     true, true
    ];
    solicitudesT.getSelects(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id,{},{},{}, es_lista);
});
filtrar=function(){
    solicitudesT.listar();
    var host = $("slct_EnvioLegado_Host").val()
    var accion = $("slct_EnvioLegado_Accion").val()
}
//Funcion para mostrar los datos json a xml
levanta=function(id){
        solicitudesT.CargarEnviosLegado(id);
}   