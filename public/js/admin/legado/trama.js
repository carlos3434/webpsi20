  $(document).ready(function(){
     $('#txt_fchinstalacion_cms').daterangepicker({
        singleDatePicker: true,
        timePicker: false,
        format: 'YYYY-MM-DD'
      });

     $("#txt_hrainstalacion_cms").timepicker({
      showInputs: false,
      showMeridian: false,
      minuteStep: 1,
      showSeconds: true,
      secondStep: 1
     });
  });

  $('#BandejaModal').on('show.bs.modal', function (event) {
    fn_deshabilitarCamp_OrdenCms();

      setTimeout(function() {
        var dia=vm.solicitud.fecha_instalacion_alta;
        $('#txt_fchinstalacion_cms').daterangepicker({
        singleDatePicker: true,
        startDate: dia,
        timePicker: false,
        format: 'YYYY-MM-DD'
      });
    }, 1000);
    //fn_editar_campos();
  });

  //tabcms
  $(document).on('click', '#btnVerBtn', function(e){
      if( $('#btnActualizar').is(":visible") ){
          fn_deshabilitarCamp_OrdenCms();
      }else{
          fn_habilitarCamp_OrdenCms();
      }    
  });

  fn_habilitarCamp_OrdenCms = function(){
      $('#btnActualizar').fadeIn('slow');
      $('#btnActualizar_dtos').fadeIn('slow');
          
      $('#div_editNombres').fadeIn('slow');
      $('#div_Nombres').hide();
      $('#div_editPaterno').fadeIn('slow');
      $('#div_Paterno').hide();
      $('#div_editMaterno').fadeIn('slow');
      $('#div_Materno').hide();
      $('#div_editZonaOT').fadeIn('slow');
      $('#div_ZonaOT').hide();
      $('#div_editTiporeq').fadeIn('slow');
      $('#div_Tiporeq').hide();
      $('#div_editMotivoGen').fadeIn('slow');
      $('#div_MotivoGen').hide();
      $('#div_editContrata').fadeIn('slow');
      $('#div_Contrata').hide();
      $('#div_editTelefonos').fadeIn('slow');
      $('#div_Telefonos').hide();
      $('#div_editFchInstalacion').fadeIn('slow');
      $('#div_FchInstalacion').hide();
  }

  fn_deshabilitarCamp_OrdenCms = function(){

      $('#btnActualizar').fadeOut('fast');
      $('#btnActualizar_dtos').fadeOut('fast');

      $('#div_Nombres').fadeIn('slow');
      $('#div_editNombres').hide();
      $('#div_Paterno').fadeIn('slow');
      $('#div_editPaterno').hide();
      $('#div_Materno').fadeIn('slow');
      $('#div_editMaterno').hide();
      $('#div_ZonaOT').fadeIn('slow');
      $('#div_editZonaOT').hide();
      $('#div_Tiporeq').fadeIn('slow');
      $('#div_editTiporeq').hide();
      $('#div_MotivoGen').fadeIn('slow');
      $('#div_editMotivoGen').hide();
      $('#div_Contrata').fadeIn('slow');
      $('#div_editContrata').hide();
      $('#div_Telefonos').fadeIn('slow');
      $('#div_editTelefonos').hide();
      $('#div_FchInstalacion').fadeIn('slow');
      $('#div_editFchInstalacion').hide();
  }

  $(document).on('click', '#btnActualizar', function(e){
      swal({
          title: '¿Desea actualizar los datos OrdenCms / Requerimiento?',
          text: "Esta Accion puede tomar unos minutos",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText : 'No',
          confirmButtonText: 'Si, Actualizar'
      }).then(
          function () {
              trama.Actualizar_OrdenCms('requerimiento');
          }, 
          function (dismiss) {
              if (dismiss === 'cancel') {
                  $('#txt_nombres_cms').val(vm.solicitud.nom_cliente);
                  $('#txt_paterno_cms').val(vm.solicitud.ape_paterno);
                  $('#txt_materno_cms').val(vm.solicitud.ape_materno);
                  $('#txt_fchinstalacion_cms').val(vm.solicitud.fecha_instalacion_alta);
                  $('#txt_hrainstalacion_cms').val(vm.solicitud.hora_instalacion_alta);
                  //slctGlobalHtml('slct_zonal_cms','simple', vm.solicitud.zonal);
                  //slctGlobalHtml('slct_contrata_cms','simple', String(vm.solicitud.codigo_contrata));
                  //slctGlobalHtml('slct_tiporeq_cms','simple', vm.solicitud.tipo_requerimiento);
                  //slctGlobalHtml('slct_motivogen_cms','simple', vm.solicitud.cod_motivo_generacion);
              }
          }
      );

  });

  $(document).on('click', '#btnActualizar_dtos', function(e){
      swal({
          title: '¿Desea actualizar los datos Orden Cms / Datos?',
          text: "Esta Accion puede tomar unos minutos",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText : 'No',
          confirmButtonText: 'Si, Actualizar'
      }).then(
          function () {
              trama.Actualizar_OrdenCms('datos');
          }, 
          function (dismiss) {
              if (dismiss === 'cancel') {
                  $('#txt_telefono1_cms').val(vm.solicitud.telefono1);
                  $('#txt_telefono2_cms').val(vm.solicitud.telefono2);
              }
          }
      );

  });
  
  pintarZonal = function() {
    var zonalId=vm.solicitud.zonal;

    html = "<option value=''>.::Seleccione::.</option>"; 
    for (var i in vm.lista.zonal) {
      html+="<option value='"+vm.lista.zonal[i].id+"'>"+vm.lista.zonal[i].nombre+"</option>";
    }

    $('#slct_zonal_cms').multiselect('destroy');
    $("#slct_zonal_cms").html(html);
    slctGlobalHtml('slct_zonal_cms','simple', zonalId);
  }
  
  pintarContrata = function() {
    var contrataId=String(vm.solicitud.codigo_contrata);

    html = "<option value=''>.::Seleccione::.</option>"; 
    for (var i in vm.lista.contratascms) {
      html+="<option value='"+vm.lista.contratascms[i].id+"'>"+vm.lista.contratascms[i].nombre+"</option>";
    }

        $('#slct_contrata_cms').multiselect('destroy');
        $("#slct_contrata_cms").html(html);
        slctGlobalHtml('slct_contrata_cms','simple', contrataId);
  }
  
  pintarMotivogen = function() {
    var motivoId=vm.solicitud.cod_motivo_generacion;

    html = "<option value=''>.::Seleccione::.</option>"; 
    for (var i in vm.lista.motivogen) {
      html+="<option value='"+vm.lista.motivogen[i].id+"'>"+vm.lista.motivogen[i].nombre+"</option>";
    }

    $('#slct_motivogen_cms').multiselect('destroy');
    $("#slct_motivogen_cms").html(html);
    slctGlobalHtml('slct_motivogen_cms','simple', motivoId);
  }
  
  pintarTiporeq = function() {
    var tiporeqId=vm.solicitud.tipo_requerimiento;

    html = "<option value=''>.::Seleccione::.</option>"; 
    for (var i in vm.lista.tiporeq) {
      html+="<option value='"+vm.lista.tiporeq[i].id+"'>"+vm.lista.tiporeq[i].nombre+"</option>";
    }

    $('#slct_tiporeq_cms').multiselect('destroy');
    $("#slct_tiporeq_cms").html(html);
    slctGlobalHtml('slct_tiporeq_cms','simple', tiporeqId);
  }

  //tabComunicacion_legado
  fn_editar_campos= function() {
    console.log(vm.comunicacionlego);
  }
