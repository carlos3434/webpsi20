Vue.config.devtools = true;
var vm = new Vue({
    http: {
        root: '/bandejalegado'
    },
    el: '#BandejaModal',
    data: {
        errors:{},
        lista: {},
        imagenes: [],
        solicitud: [],
        componentes:[],
        comunicacionlego:[],
        logcambios:[],
        logrecepcion:[],
        movimientos:[],
        operaciones:[],
        detalle: [],
        estadoBtnGuardarCierre: false,
       
        campo1_min:37.0,
        campo1_max:55.0,

        campo2_min:-5.0,
        campo2_max:10.0,

        campo3_min:27.0,
        campo3_max:99.0,

        campo4_min:30.0,
        campo4_max:99.0,
        productos:[],
        dominios :[],
        fftt:[],
        pin:[],
        cobre:[],
        adsl:[],
        map:[],
        markers:[],
        bounds:[],
        line:[],
        nomarkers:[],
        cabs:{},
        posiblesmotivos : [103, 467, 115]
    },
    computed: {
        fechaEnvioToa:function (){
            for (let i = this.movimientos.length-1; i >=0; i--) {
                if (this.movimientos[i].estado=='Pendiente') {
                    return this.movimientos[i].created_at;
                }
            }
            return 'sin fecha';
        },
        //:disabled='noGestion'
        noGestion: function () {
            return false;
            //return this.solicitud.estado_st!=1 && this.solicitud.tipo_legado==1 && this.solicitud.actividad_id==1 ;
        },
        estadoOfscStyle: function () {
            return {
                'color': this.solicitud.color_letra,
                'background-color': this.solicitud.color_ofsc
            };
        },
        fullName: function () {
            return this.solicitud.nom_cliente +' '+ this.solicitud.ape_paterno + ' ' + this.solicitud.ape_materno;
        },
        fullFchInstalacion: function () {
            return this.solicitud.fecha_instalacion_alta +' '+ this.solicitud.hora_instalacion_alta;
        },
        tipoDevolucion: function () {
            var tipo = "";
            switch (this.solicitud.tipodevolucion) {
                case 'C':
                    tipo = "Comercial";
                    break;
                case 'T':
                    tipo = "Tecnico";
                    break;
                case 'S':
                    tipo = "Soporte de Campo";
                    break;
            }
            return tipo;
        },
        fullNameGestel: function () {
            return this.solicitud.nombres +' '+ this.solicitud.ape_paterno + ' ' + this.solicitud.ape_materno;
        },
        fullDireccionprov1: function () {
            if(this.solicitud.direccioncliente[0].direccion_completa!==undefined){
                return this.solicitud.direccioncliente[0].direccion_completa;
            } else {
                return '';
            }
        },
        distrito: function () {
            if(this.solicitud.direccioncliente[0]!==undefined){
                return this.solicitud.direccioncliente[0].desc_distrito;
            } else {
                return '';
            }
        },
        fullDireccionprov2: function () {
            if(this.solicitud.direccioncliente[1].direccion_completa!==undefined) {
            return this.solicitud.direccioncliente[1].direccion_completa; 
            } else {
                return '';
            }
        },
        fechaReg: function () {
            return this.solicitud.fecha_registro_requerimiento +' '+ this.solicitud.hora_registro_requerimiento;
        },
        fechaEnvio: function () {
            return this.solicitud.fecha_envio +' '+ this.solicitud.hora_envio;
        },
        direccionCliente: function () {
            return this.solicitud.tipo_via+' '+this.solicitud.nombre_via+
            ' '+this.solicitud.numero_via+' PISO:'+ this.solicitud.piso +' INT'+this.solicitud.interior +
            ' MNZ'+ this.solicitud.manzana +' LT'+ this.solicitud.lote;
        },
        direccionTab: function () {
            return this.solicitud.tap_via+' '+this.solicitud.tap_nombre_via+
            ' '+this.solicitud.tap_numero+' PISO:'+ this.solicitud.tap_piso +' INT'+this.solicitud.tap_interior +
            ' MNZ'+ this.solicitud.tap_manzana +' LT'+ this.solicitud.tap_lote;
        },
        urbanizacion: function () {
            return this.solicitud.tipo_urbanizacion +' '+this.solicitud.desc_urbanizacion;
        },
        telefonos: function () {
            var tele;
            if(this.solicitud.telefono1=="" && this.solicitud.telefono2=="" ){
                tele="sin telefono";
            }else{
                tele=this.solicitud.telefono1 +'\t'+this.solicitud.telefono2;
            }
            return tele;
        }
    },
    methods: {
        reenvioOperacionDeco:function(id,solicitud_tecnica_id){
            Legado.reenvioOperacionDeco(id,solicitud_tecnica_id);
            //console.log(form);
        },
        fn_btnEditComLeg: function(id){
            var edit='edit'+id;
            var show='show'+id;
            var estado='estado'+id;
     
            if( $('#'+edit).is(":visible") ){
                    $('#'+show).fadeIn('slow');
                    $('#'+edit).hide();
                    $('#'+estado).val(0);
            }else{  
                    this.fn_pintar_divedit(id);
                    $('#'+edit).fadeIn('slow');
                    $('#'+show).hide();
                    $('#'+estado).val(1);
                }
        },
        fn_pintar_divedit: function(id){
            //pintar div edit 
            var list=this.comunicacionlego
            for (var i in list) {
                var lego=this.comunicacionlego[i];
                cont=0;
                for (var j in lego) {
                    //console.log(lego[j].accion+'\n'+lego[j].request+'\n'+lego[j].xmlrequest);
                    var json=lego[j].request;
                    var div=lego[j].id;
                    array=JSON.parse(json);
                    var html='';
                    //console.log(array);
                    var html='<br>';
                    var html2='<br>';
                    
                    $.each(array, function(key, val){
                        if( key !=  'componente' && key !=  'tipo_operacion' ){
                            if( key ==  'IndicadorRespuesta' ){
                                var vl='';
                                cont=cont+1;
                                if( val == null ){var vl='';}else{vl=val;}
                                html+='<div class="form-group col-sm-4 bootstrap-timepicker">';
                                html+='<label for="'+key+'">'+key+':</label>';
                                html+='<select class="form-control input-sm '+key+'_'+cont+'" id="'+key+'_'+cont+'" name="'+key+'_'+cont+'"></select>';
                                html+='</div>';
                                var json='[{"id": 0,"nombre": "Error de Trama"},{"id": 1,"nombre": "Trabajar en Psi"},{"id": 2,"nombre": "Trabajar en Legados"}]';                 
                                vm.pintar_slct((key+'_'+cont),json,vl);
                            }else{
                                var vl='';
                                if( val == null ){var vl='';}else{vl=val;}
                                html+='<div class="form-group col-sm-4 bootstrap-timepicker">';
                                html+='<label for="'+key+'">'+key+':</label>';
                                html+='<input type="text" class="form-control timepicker" id="'+key+'" name="'+key+'" value="'+vl+'" placeholder="Ingresar '+key+'">';
                                html+='</div>';

                                //datepicker fechas
                                if( key.substr(0,5) ==  'fecha' ){
                                    setTimeout(function() {
                                    $('#'+key).daterangepicker({
                                        singleDatePicker: true,
                                        timePicker: false,
                                        format: 'DD/MM/YYYY'
                                    });},1000);
                                }

                                //timepicker tiempo
                                if( key.substr(0,4) ==  'hora' ){
                                    setTimeout(function() {
                                    $("#"+key).timepicker({
                                      showInputs: false,
                                      showMeridian: false,
                                      minuteStep: 1,
                                      showSeconds: true,
                                      secondStep: 1
                                    });},1000);
                                }
                            }
                        }else {
                            if( key == 'componente' ){
                                var componente=val;
                                $.each(componente, function(key, val){
                                    html2+='<fieldset class="scheduler-border form-group col-sm-6">';
                                    html2+='<legend class="scheduler-border color-text" style="margin-left:10px;">COMPONENTE '+(key+1)+'</legend>';
                                    var cont='_'+(key+1);
                                    $.each(val, function(k, v){
                                        var vl='';
                                        if( v == null ){var vl='';}else{vl=v;}
                                        html2+='<div class="form-group col-sm-6">';
                                        html2+='<label for="'+k+'">'+k+':</label>';
                                        html2+='<input type="text" class="form-control" id="'+k+cont+'" name="'+k+cont+'" value="'+vl+'" placeholder="Ingresar '+k+'">';  
                                        html2+='</div>';
                                    });
                                    html2+='</fieldset>';
                                });
                            }
                        }
                        $('#'+div).html(html);
                        $('#detalle'+div).html(html2);
                    });
                }
            }
        },
        pintar_slct: function(select,json,val) {
            var option=JSON.parse(json);
            html = "<option value=''>.::Seleccione::.</option>"; 
            for (var i in option) {
              html+="<option value='"+option[i].id+"'>"+option[i].nombre+"</option>";
            }

            setTimeout(function() {
                $('#'+select).multiselect('destroy');
                $('#'+select).html(html);
                slctGlobalHtml(select,'simple', (val).toString());
            },400);
        },
        Setvalores: function () {
            if(this.solicitud.productosadquiridos!==undefined)
                this.$set('productos', this.solicitud.productosadquiridos);
            if(this.solicitud.dominiogestel!==undefined)
                this.$set('dominios', this.solicitud.dominiogestel);
            if(this.solicitud.fftt!==undefined)
                this.$set('fftt', this.solicitud.fftt[0]);
            if(this.solicitud.ffttpin!==undefined)
                this.$set('pin', this.solicitud.ffttpin[0]);
            if(this.solicitud.ffttcobre!==undefined)
                this.$set('cobre', this.solicitud.ffttcobre[0]);
            if(this.solicitud.ffttadsl!==undefined)
                this.$set('adsl', this.solicitud.ffttadsl[0]);
            return;
        },
        tipoEnvio: function (tipo_envio_ofsc) {
            switch (tipo_envio_ofsc) {
                case 1:
                    tipo_envio_ofsc = "Agenda";
                    break;
                case 2:
                    tipo_envio_ofsc = "SLA";
                    break;
                default:
                    tipo_envio_ofsc = "";
                    break;
            }
            return tipo_envio_ofsc;
        },
        EstadoPrueba: function (estado) {
            if (estado==0) {
                return "Invalidas";
            }
            return "OK";
        },
        bitacoras : function (index) {
            return index.tipo==3 || index.tipo==4 || index.tipo==5 || index.tipo==6;
        },

        idClass: function (id) {
            var value = "deta_"+id;
            return value;
        },
        displayDetalleMovimientos: function (id) {
            var estado = $(".deta_"+id).css('display');
            if (estado == "none") {
                $(".deta_"+id).css('display', '');
                
                //validar cabeceras
                var cab={};
                $.each(this.movimientos, function(key, val){
                    
                    if(val.actividad==1){
                        cab={camp1:'Contactabilidad',camp2:'Conformidad',camp3:'Motivo de Conformidad',camp4:'Submotivo de Conformidad'};
                    }else if(val.actividad==2){
                        cab={camp1:'Tematico1',camp2:'Tematico2',camp3:'Tematico3',camp4:'Tematico4'};
                    }else{cab={camp1:'null',camp2:'null',camp3:'null',camp4:'null'};}

                });
                this.cabs=cab;

            } else {
                $(".deta_"+id).css('display', 'none');
            }
        }
    }
});