var PestanaAgendaToa = {
    verTematicos : function() {
        $("#divtematico2-agenda, #divtematico3-agenda, #divtematico4-agenda").css("display", "none");
        if (vm.solicitud.actividad_id == 1) {
            $("#accordion-tematicos-agenda").css("display", "block");
        }
        Modal.pintarTematicos(0, "slct_tematico_id1_agenda", "divtematico1-agenda", "div-error-tematico-agenda");
    },
    ocultarTematicos : function(){
        $(".slct_tematicos").val("");
        $(".slct_tematicos").multiselect("refresh");
        $("#divtematico1-agenda").css("display", "inline-block");
        $("#divtematico2-agenda, #divtematico3-agenda, #divtematico4-agenda").css("display", "none");
        $("#accordion-tematicos-agenda").css("display", "none");
    },
    activarVisibilidad : function() {
        var visualizar = true;
        if (objperfil.opciones[0]!="null" && objperfil.opciones[0]!=null) {
            if (!validaPermisoPerfil(1)) {
                visualizar = false;
            }
        } else {
            if ( vm.solicitud.estado_ofsc_id == 1 || vm.solicitud.estado_ofsc_id == 4 ||
             vm.solicitud.estado_ofsc_id == 8 || vm.solicitud.estado_ofsc_id == 7 ||
             (vm.solicitud.estado_ofsc_id == 5 && vm.solicitud.estado_aseguramiento == 12)) {
                visualizar = true;
            } else {
                visualizar = false;
            }
        }
        if (visualizar) {
            $("#BandejaModal ul.nav.nav-pills li").removeClass("active");
            $("#BandejaModal ul.nav.nav-pills li:first").addClass("active");
            $("#BandejaModal .tab-pane.fade").removeClass("active");
            $("#BandejaModal .tab-pane.fade").removeClass("in");
            $("#BandejaModal #tabtoa.tab-pane.fade").addClass("active");
            $("#BandejaModal #tabtoa.tab-pane.fade").addClass("in");
        } else {
            PestanaAgendaToa.desactivarVisibilidad();
        }
    },
    desactivarVisibilidad : function() {
        $("#litabtoa").removeClass("active");
        $("#litabtoa").css("display", "none");

        if (vm.solicitud.tipo_legado == 1) {
            $("#liopt-ordencms").addClass("active");
            $("#tabsolicitudcms").addClass("active");
            $("#tabsolicitudcms").addClass("in");
        }
        if (vm.solicitud.tipo_legado == 2) {
            if(vm.solicitud.actividad_id==1) {
                $("#liopt-ordengestel").addClass("active");
                $("#tabsolicitudgestel").addClass("active");
                $("#tabsolicitudgestel").addClass("in");
            } else {
               $("#liopt-ordengestelprov").addClass("active");
               $("#tabsolicitudgestelprov").addClass("active");
               $("#tabsolicitudgestelprov").addClass("in"); 
            }
        }
        $("#BandejaModal #tabtoa.tab-pane.fade").removeClass("active");
        $("#BandejaModal #tabtoa.tab-pane.fade").removeClass("in");
    }
};
OfscModal=function(){
    var datos={};
    var tematicos = [];
    var comentarios = $("#comentarios-agenda").val();
    $("#accordion-tematicos-agenda .slct_tematicos").each(function(){
        var obj = {};
        obj.nombre = $(this).val();
        if (obj.nombre) {
            tematicos.push(obj);
        }
    });
    if ( ValidarDatosOfsc() ){
        var hf_envio='';
        if( $("#slct_agdsla").val()=='sla'){
            hf_envio=$("input[name='rdb_check']:checked").val();
        } else {
            hf_envio=hf;
        }
        Legado.EnvioOfsc(Limpiar,hf_envio,tematicos,comentarios);
    }
};
ValidarDatosOfsc=function(){
    var r=true;
    if( $("#slct_agdsla").val()==='' ){
        Psi.sweetAlertError(".:: Seleccione Tipo de Envio ::.");
        r=false;
    } else if( $("#slct_agdsla").val()=='sla' && $("input[name='rdb_check']").is(':checked')==false ){
        Psi.sweetAlertError(".:: (SLA) Seleccione un horario disponible ::.");
        r=false;
    } else if ( $("#slct_agdsla").val()=='agenda' && (hf===undefined || hf==='undefined' || hf==='' ) ) {
        Psi.sweetAlertError(".:: (AGENDA) Seleccione un horario disponible ::.");
        r=false;
    }
    return r;
};
SlaF=function(val){
    $("#txt_fecha_agenda_toa_modal,#txt_slaini").val("");
    hf='';
    $('#bucket_id').val("");
    CupoxCapacidad='';
    if( val=='sla' ) {
        $("#div_horario").css("display","none");
        $('#slct_horario').multiselect('destroy');
        $(".fecage, #div_agenda, #div_agenda2, #html_horario_ofsc").css("display","none");
        //consultar inmediatamente la capacidad con respecto al dia de hoy
        var rpta = false;
        if (capacityResponse) {
            rpta = HTMLCapacity(capacityResponse.today, capacityResponse.duracion);
        }
        if(!rpta) {
            Psi.sweetAlertError('(SLA) No hay capacidad en TOA en la fecha actual.');
        }
        $("#div_sla").css("display","");
    } else if (val=='agenda') {
        var horadefault=7;
        slctGlobal.listarSlctpost('horariotipo','listargo' ,'slct_horario', 'simple', [horadefault],null,1);
        $("#div_sla").css("display","none");
        $("#div_horario").css("display","");
        //cargar tipo horario;
        if (capacityResponse) {
            Legado.showCupos(capacityResponse, horadefault);
        } else {
            Psi.sweetAlertError('(AGENDA) No hay capacidad en TOA.');
        }
    }
};

AgendaHorario=function(val){
    $("#div_horario").css("display","");
    //cargar tipo horario;
    if (capacityResponse) {
        Legado.showCupos(capacityResponse, val);
    } else {
        Psi.sweetAlertError('(AGENDA) No hay capacidad en TOA.');
    }
}
forzarCoordenadas = function() {
    var comentario = $("#comentarios-forzar-coordenadas").val();
    swal({
        title: '¿Desea forzar la validacion de coordenadas para el tecnico?',
        text: "Esta Accion puede tomar unos minutos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText : 'No',
        confirmButtonText: 'Si, Forzar'
    }).then(function () {
        Legado.forzarValidacionCoordenadas(comentario);
    });
    return false;
};

hideModal = function(){
    $("#div_horario").css("display","none");
    $('#slct_codigo_contrata').multiselect('destroy');
    //Pestana Orden
    $('#form_solicitud #slct_zonal,#form_solicitud #slct_cod_nodo, #form_solicitud #slct_tipo_requerimiento, #form_solicitud #slct_cod_motivo_generacion, #form_solicitud #slct_codigo_tecnico,#slct_cod_departamento,#slct_cod_provincia,#slct_cod_distrito,#slct_segmento,#slct_cod_jefatura,#slct_cod_troba,#cod_tap,#cod_amplificador,#slct_agdsla').multiselect('select','');
    $('#form_solicitud #slct_zonal,#form_solicitud #slct_cod_nodo, #form_solicitud #slct_tipo_requerimiento, #form_solicitud #slct_cod_motivo_generacion, #form_solicitud #slct_codigo_tecnico, #slct_cod_departamento,#slct_cod_provincia,#slct_cod_distrito,#slct_ind_vip,#slct_segmento,#slct_cod_jefatura,#slct_cod_troba,#cod_tap,#cod_amplificador,#slct_agdsla').multiselect('rebuild');
    $('#form_solicitud input[type="hidden"]').remove();
    $(".fieldset-forzar-validacion").css("display", "block");
    //Pestana Agenda Toa
    $("#div_agenda").css("display","none");
    $("#div_sla").css("display","none");
    PestanaAgendaToa.ocultarTematicos();

    //activar Reset Formulario
    if (vm.solicitud.estado_aseguramiento == 3 || vm.solicitud.estado_aseguramiento == 4) {
        $("#btn_limpiar_formulario").removeAttr("disabled");
    }
    PestanaCierre.hide();
    PestanaOrden.hideDatosGo();
    //desactivar Btn Stop
    $("#btn_stop_devuelta").prop("disabled", true);
    $("#BandejaModal ul.nav.nav-pills li").removeClass("active");
    $("#BandejaModal ul.nav.nav-pills li:first").addClass("active");
    $("#BandejaModal .tab-pane.fade").removeClass("active");
    $("#BandejaModal .tab-pane.fade").removeClass("in");
    $("#BandejaModal #tabtoa.tab-pane.fade").addClass("active");
    $("#BandejaModal #tabtoa.tab-pane.fade").addClass("in");
    $("#litabtoa").css("display", "inline-block");
    tabmodalactivo = true;
    $("#btn_editar").removeAttr("disabled");
    $("#form_solicitud input[type=tex]").removeAttr("disabled");
    Legado.quitarValidacion();
    indice = "";
    permiso = 0;
    vm.solicitud={};
    vm.movimientos={};
    vm.imagenes={};
    vm.componentes={};
    vm.comunicacionlego={};
    vm.logcambios={};
    vm.logrecepcion={};
    vm.movimientos={};
    vm.operaciones={};
    vm.detalle ={};
    $("#t_cambios").dataTable().fnDestroy();
    $('#t_componentes').dataTable().fnDestroy();
    $('#liopt-datago').hide();
    tabseleccionadoactive = "";
    // $('#t_movimientos').dataTable().fnDestroy();
    //markerSpiderfier.clearMarkers();
};
cleardate=function(){
    $("#txt_created_at").val('');
    $("#txt_fecha_agenda").val('');
};
ConfirmLeave =  function () {
    if (vm.solicitud.id>0) {
        Legado.quitarValidacion();
    }
    return "";
};
BuscarSolicitud=function(){
    if ($("#collapse2").css("display") == "block") {
        valor_busqueda = $("#txt_valor").val();
        if (valor_busqueda == "") {
            alert("Ingrese un valor para la busqueda!!!");
            return;
        } else {
            tipo_busqueda = $("#slct_tipo").val();
            switch(tipo_busqueda){
                case "id_solicitud_tecnica":
                    regex = new RegExp(/^[0-9a-zA-Z]+$/);
                    if (valor_busqueda.length > 11) {
                        alert("Excedio los 11 caracteres para buscar por solicitud tecnica");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo caracteres alfanumericos");
                        return;
                    }
                    break;
                case "num_requerimiento":
                    regex = new RegExp(/^[0-9a-zA-Z]+$/);
                    if (valor_busqueda.length > 15) {
                        alert("Excedio los 15 caracteres para buscar por codactu");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo caracteres alfanumericos");
                        return;
                    }
                    break;

                case "orden_trabajo":
                    regex = new RegExp(/^([0-9])/);
                    if (valor_busqueda.length > 15) {
                        alert("Excedio los 15 caracteres para buscar por orden de trabajo");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo numeros");
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
        $("#t_listados").dataTable().fnDestroy();
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
        dtBandeja = Bandeja.Buscar();
        $("#tipo_busqueda").remove();
    } else if ($("#collapse1").css("display") == "block") {
        $("#t_listados").dataTable().fnDestroy();
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
        dtBandeja = Bandeja.Buscar();
        $("#tipo_busqueda").remove();
    }
};
DescargarExcel = function(){
    $("#form_buscar").attr("action", "bandejalegado/listar");
    $("#form_buscar").append("<input type='hidden' name='accion' value='download' id='accion' />");
    if ($("#collapse2").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
    } else if ($("#collapse1").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
    }
    $("#form_buscar").submit();
    $("#accion").remove();
    $("#tipo_busqueda").remove();
    $("#form_buscar").attr("action", "");
};