var trama = {
    Actualizar_OrdenCms: function(form) {

        var datos={
            form:form,id:vm.solicitud.id,
            nombres:($('#txt_nombres_cms').val()).toUpperCase(),paterno:($('#txt_paterno_cms').val()).toUpperCase(),
            materno:($('#txt_materno_cms').val()).toUpperCase(),zonal:$('#slct_zonal_cms').val(),
            contrata:$('#slct_contrata_cms').val(),tiporeq:$('#slct_tiporeq_cms').val(),
            motigogen:$('#slct_motivogen_cms').val(),fchinstalacion:$('#txt_fchinstalacion_cms').val(),
            hrainstalacion:$('#txt_hrainstalacion_cms').val(),telefono1:$('#txt_telefono1_cms').val(),
            telefono2:$('#txt_telefono2_cms').val()
        };

        $.ajax({
            url: 'trama/actualizarordencms',
            type: 'POST',
            cache: false,
            dataType: 'json',
            data: datos,

            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success: function(data) {
                $(".overlay,.loading-img").remove();
                if(data.estado==true){
                    
                    Psi.mensaje('success', data.msj, 6000);
                    if(data.form=='requerimiento'){
                        $('#txt_nombres_cms').val(data.campos.nombres);
                        $('#txt_paterno_cms').val(data.campos.paterno);
                        $('#txt_materno_cms').val(data.campos.materno);
                        $('#txt_fchinstalacion_cms').val(data.campos.fecha_instalacion);
                        $('#txt_hrainstalacion_cms').val(data.campos.hora_instalacion);

                        $('#lbl_nombres_cms').html(data.campos.nombres);
                        $('#lbl_paterno_cms').html(data.campos.paterno);
                        $('#lbl_materno_cms').html(data.campos.materno);
                        $('#lbl_zonal_cms').html(data.campos.zonal);
                        $('#lbl_codcontrata_cms').html(data.campos.codcontrata);
                        $('#lbl_desccontrata_cms').html(data.campos.desccontrata);
                        $('#lbl_tiporeq_cms').html(data.campos.codrequerimiento);
                        $('#lbl_desctiporeq_cms').html(data.campos.descrequerimiento);
                        $('#lbl_motivogen_cms').html(data.campos.codmotivo);
                        $('#lbl_descmotivogen_cms').html(data.campos.descmotivo);
                        $('#lbl_motivoact_cms').html(data.campos.codmotivo);
                        $('#lbl_descmotivoact_cms').html(data.campos.descmotivo);
                        $('#lbl_fchinstalacion_cms').html(data.campos.fecha_instalacion+' '+data.campos.hora_instalacion);
                    }

                    if(data.form=='datos'){
                        $('#txt_telefono1_cms').val(data.campos.telefono1);
                        $('#txt_telefono2_cms').val(data.campos.telefono2);

                        var telefonos='';
                        if(data.campos.telefono1=="" && data.campos.telefono2=="" ){
                            telefonos="sin telefono";
                        }else{
                            telefonos=data.campos.telefono1 +'\t'+data.campos.telefono2;
                        }
                        $('#lbl_telefonos_cms').html(telefonos);
                        $('#lbltelefonos_tbcr').val(telefonos);
                    }
                }else{Psi.mensaje('danger',data.msj, 6000);}
            },
            error: function() {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    }
}