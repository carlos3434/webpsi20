var instance = axios.create({
	baseURL: '/bandejalegado'
});

Vue.use(VueHighcharts);

var vm = new Vue({
	el: "#app",
	data: {
		estadosObj: {},
		datosObj: {},
		options: {}
	},
	mounted: function () {
		$('#rangoFechas').val(
            moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
            moment(new Date()).format("YYYY-MM-DD")
        );
	},
	methods: {
		listar: function () {
			var rangoFechas = document.getElementById("rangoFechas").value;
			var rangoFechas = rangoFechas.split(" - ");
			var request = {
				rangoFechas: rangoFechas,
			};
			instance.post('/visor', request)
				.then((response) => {
					estadosGLOBAL = this.estadosObj = response.data.estados;
					datosGLOBAL = this.datosObj = response.data.datos;
					var dias = diasGLOBAL = response.data.dias;

					var htmlHead = this.headHTML(dias);
					var htmlBody = this.bodyHTML(this.estadosObj, this.datosObj, dias);
					this.grafica(this.datosObj, dias);

					$("#visorHeadHTML").html(htmlHead);
					$("#visorHTML").html(htmlBody);
				})
				.catch(function (error) {

				})
		},

		headHTML: function (dias) {
			var htmlHead = '';
			htmlHead += '<th>E. OFSC</th>';
			htmlHead += '<th>E. Aseguramiento</th>';
			for (var k = 0; k <= dias; k++) {
				htmlHead += '<th style="width: 80px;">'+moment(rangoFechas[1]).subtract(k, 'days').format("DD-MM")+'</th>';
			}
			htmlHead += '<th style="width: 80px; background:#eac5c5" title="= ST (Error) + ST (Ok) + ST (Legado)">TOTAL</th>';
			htmlHead += '<th style="width: 80px;">ST (Error)</th>';
			htmlHead += '<th style="width: 80px;">ST (Ok)</th>';
			htmlHead += '<th style="width: 80px;">ST (Legado)</th>';
			return htmlHead;
		},

		bodyHTML: function (estadosObj, datosObj, dias) {
			var html = "";
			for (var i in estadosObj) {
				html += '<tr>';
				html += '<td rowspan="'+ estadosObj[i].countrow +'" class="text-center" style="vertical-align:middle; background:'+ estadosObj[i].color +'; color:'+ estadosObj[i].color_letra +'">'+ estadosObj[i].nombre +'</td>';
				for (var j in datosObj) {
					if (estadosObj[i].estado_ofsc_id == datosObj[j].estado_ofsc_id) {
						html += '<td>'+ datosObj[j].nombre +'</td>';
						for (var k = 0; k <= dias; k++) {
							html += '<td>'+ eval('datosObj[j].f'+k) +'</td>';
						}
						html += '<td style="background:#eac5c5">'+ datosObj[j].total +'</td>';
						html += '<td style="background:#b9dff3">'+ datosObj[j].estado_0 +'</td>';
						html += '<td style="background:#b9f3d4">'+ datosObj[j].estado_1 +'</td>';
						html += '<td style="background:#ecf3b9">'+ datosObj[j].estado_2 +'</td>';
						html += '</tr>';
					}
				}
			}
			return html;
		},

		grafica: function (datosObj, dias) {
			var series = [];
			for (var j in datosObj) {
				var data = [];
				for (var k = 0; k <= dias; k++) {
					data.push(parseInt(eval('datosObj[j].f'+k)));
				}
				var estadoOFSC = "";
				var colorsOFSC = "";
				switch (datosObj[j].estado_ofsc_id) {
					case 1: 
						estadoOFSC = "PENDIENTE";
						colorsOFSC = "#EFFF12";
						break;
					case 2: 
						estadoOFSC = "INICIADA";
						colorsOFSC = "#5DBE3F";
						break;
					case 3: 
						estadoOFSC = "SUSPENDIDA";
						colorsOFSC = "#ABFFBC";
						break;
					case 4: 
						estadoOFSC = "NO REALIZADA";
						colorsOFSC = "#1414FF";
						break;
					case 5: 
						estadoOFSC = "CANCELADA";
						colorsOFSC = "#80FF80";
						break;
					case 6: 
						estadoOFSC = "COMPLETADA";
						colorsOFSC = "#9BB6C2";
						break;
					case 7: 
						estadoOFSC = "DESPROGRAMADA";
						colorsOFSC = "";
						break;
					case 8: 
						estadoOFSC = "NO ENVIADO";
						colorsOFSC = "#f39c12";
						break;
					default:
						estadoOFSC = "";
						colorsOFSC = "";
						break;
				}
				series.push({name: estadoOFSC+" - "+datosObj[j].nombre, color: colorsOFSC, data: data});
			}

			var fechas = [];
			for (var k = 0; k <= dias; k++) {
				fechas.push(moment(rangoFechas[1]).subtract(k, 'days').format("DD-MM"));
			}

			var options = {
				credits: {
                    text: "Webpi20",
                },
				title: {
					text: 'Visor Legado',
					x: -20
				},
				xAxis: {
					categories: fechas
				},
				yAxis: {
					title: {
					  text: 'Cantidad (Uni)'
					},
					plotLines: [{
					  value: 0,
					  width: 1,
					  color: '#808080'
					}]
				},
				tooltip: {
					valueSuffix: ''
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'middle',
					borderWidth: 0
				},
				series: series
			};

			this.options = options;
			$(".highcharts").css('display', '');
		}
	}
});