    var solicitudesT = {
        listar: (function () {
            var datos="";
            var targets=0;
            $('#tb_legado').dataTable().fnDestroy();
            $('#tb_legado')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            solicitudesT.http_ajax(data,callback);
                        },

                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.usuario!="undefined" && typeof row.usuario!=undefined) {
                                    return row.usuario;
                                } else return "";
                            }, name:'usuario'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.accion!="undefined" && typeof row.accion!=undefined) {
                                    return row.accion;
                                } else return "";
                            }, name:'accion'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.host!="undefined" && typeof row.host!=undefined) {
                                    return row.host;
                                } else return "";
                            }, name:'host'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.fech_creacion!="undefined" && typeof row.fech_creacion!=undefined) {
                                    return row.fech_creacion;
                                } else return "";
                            }, name:'fech_creacion'},
                            {data: function(row,type,val,meta){
                                
                                htmlEstado = '<a class="btn btn-primary btn-sm" id="#envioLegado" data-target="#envioLegado" onclick="levanta('+row.id+')" data-toggle="modal" data-toggle="modal"><i class="glyphicon glyphicon-search"></i></a>'
                                
                                return htmlEstado;
                            }},
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),       
    http_ajax: function(request,callback){
        var contador = 0;
        var rangoFechas =$("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&fecha="+rangoFechas;

        eventoCargaMostrar();
        axios.post('envioLegado/cargar',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
      },
    
    
    CargarEnviosLegado:function(id){
         $.ajax({
            url         : 'envioLegado/listardetalle',
            type        : 'POST',
            cache       : false,
            data    : {
                id: id
            },           
            success : function(obj) {
                    $('#request').text(obj.request);
                    $('#response').text(obj.response);
                    $('#envioLegado').modal('show');
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });  
        event.stopImmediatePropagation();  //prvents the other on click from firing that fires up the inline editor
    },
    getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {

            var obj = response.data;
            
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
                vm.lista[ selects[i] ] = obj.data[i];
            }
        }).then(() => {
            eventoCargaRemover();
        });
    }   
}