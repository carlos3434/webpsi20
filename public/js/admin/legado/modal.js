/*
* Script que gestionara funcionalidades 
* de uso general en la Bandeja de Legados
*/
tabseleccionadoactive = "";
var Modal = {
    pintarMotivos : function() {
        var seleccionado = $("#slct_tipo_devolucion option:selected").val();
        var motivoseleccionado = vm.solicitud.motivo_ofsc_id;
        var actividad_id = vm.solicitud.actividad_id;
        var tipolegado = vm.solicitud.tipo_legado;
        var valarray = [];
        html = "<option value=''>.::Seleccione::.</option>";
        for (var i in vm.lista.motivosdevolucion) {
            if (actividad_id == 2 && seleccionado!=="" &&
                vm.lista.motivosdevolucion[i].tipo == seleccionado && 
                vm.lista.motivosdevolucion[i].tipo_legado == tipolegado ) {
                if (vm.lista.motivosdevolucion[i].id == motivoseleccionado &&
                    vm.lista.motivosdevolucion[i].actividad_id == actividad_id) {
                    valarray.push(vm.lista.motivosdevolucion[i].id);
                }
                if (perfilId == 14 || perfilId == 15) {
                    if (perfilId == 14) {
                        if ($.inArray(parseInt(vm.lista.motivosdevolucion[i].id), vm.posiblesmotivos) < 0) {
                            html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                        }
                    }
                    if (perfilId == 15) {
                        if ($.inArray(parseInt(vm.lista.motivosdevolucion[i].id), vm.posiblesmotivos) >= 0) {
                            html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                        }
                    }
                } else {
                    html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                }
            }
            if (actividad_id == 1 && vm.lista.motivosdevolucion[i].tipo == seleccionado &&
                vm.lista.motivosdevolucion[i].tipo_legado == tipolegado) {

                if (vm.lista.motivosdevolucion[i].id == motivoseleccionado &&
                    vm.lista.motivosdevolucion[i].actividad_id == actividad_id) {
                    valarray.push(vm.lista.motivosdevolucion[i].id);
                }
                //
                html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
            }
        }
        $("#slct_motivosdevolucion").multiselect('destroy');
        $("#slct_motivosdevolucion").html(html);
        slctGlobalHtml('slct_motivosdevolucion','simple', valarray); 
    },
    pintarSubMotivos : function() {
        var motivoid=vm.solicitud.motivo_ofsc_id;
        var submotivoid=vm.solicitud.submotivo_ofsc_id;
        var motivoseleccionado = vm.solicitud.motivo_ofsc_id;
        var valarray = [];
        var contador=0;

        html = "<option value=''>.::Seleccione::.</option>"; 
        for (var i in vm.lista.submotivosdevolucion) {
            if(vm.lista.submotivosdevolucion[i].motivo_ofsc_id == motivoseleccionado){
                html+="<option value='"+vm.lista.submotivosdevolucion[i].id+"'>"+vm.lista.submotivosdevolucion[i].descripcion+"</option>";
                valarray.push(vm.lista.submotivosdevolucion[i].id);
                contador=contador+1;
            }

        }

        $('#slct_submotivosdevolucion').multiselect('destroy');
        $("#slct_submotivosdevolucion").html(html);
        slctGlobalHtml('slct_submotivosdevolucion','simple', submotivoid);

        if(contador>0){$("#divsubmotivosdevolucion").show();}
        else{$("#divsubmotivosdevolucion").hide();}
    },
    pintarTematicos : function(parent, idelementpintado, iddivtematico, iddiverror) {
        var tematicos = vm.lista.tematicos[parent];
        var seleccionado = $("#slct_tipo_devolucion option:selected").val();
        var motivoseleccionado = vm.solicitud.motivo_ofsc_id;
        var tipodevolucion = vm.solicitud.tipodevolucion;
        var tipolegado = vm.solicitud.tipo_legado;
        var actividad_id = vm.solicitud.actividad_id;
        var estado_ofsc_id = vm.solicitud.estado_ofsc_id;
        var estado_aseguramiento = vm.solicitud.estado_aseguramiento;
        var valarray = [];
        var nuevo_motivoseleccionado = $("#slct_motivosdevolucion option:selected").val();
        
        if(typeof seleccionado != undefined && seleccionado !== ''){
            tipodevolucion  = seleccionado;
        }

        html = "<option value=''>.::Seleccione::.</option>";
        var cont = 0;
        for (var i in tematicos) {
            var tematico = tematicos[i];
            var pintartematico = false;
            if (tematico.actividad_id == actividad_id && tematico.tipo_legado == tipolegado) {
                if (estado_ofsc_id == 2) { // si es iniciado
                    if (estado_aseguramiento == 3) { // si es predevolucion
                        if (tematico.is_pre_devuelto == 1) {
                            pintartematico = true;
                        }
                    } else if (estado_aseguramiento == 4) { // si es preliquidado
                        if (tematico.is_pre_liquidado == 1) {
                            pintartematico = true;
                        }
                    }
                } else if (estado_ofsc_id == 1) { // si es pendiente
                    if (tematico.is_pendiente == 1) {
                        pintartematico = true;
                    }
                } else if (estado_ofsc_id == 4) { // si es no realizado
                    if (tematico.is_no_realizado == 1) {
                        pintartematico = true;
                    }
                } else if (estado_ofsc_id == 6) { // si es completado
                    if (tematico.is_completado == 1) {
                        pintartematico = true;
                    }
                }

                if (pintartematico) {
                    if (actividad_id == 1) {
                        html+="<option value='"+tematico.id+"'>"+tematico.nombre+"</option>";
                        cont ++;
                    }
                    if (actividad_id == 2) {
                        if (tematico.tipo == tipodevolucion) {
                            let t_pintar = true; 
                            if(tipodevolucion == 'T' && parent == 0){

                                if(tematico.motivo_ofsc_id == nuevo_motivoseleccionado){
                                   t_pintar = true; 
                                } else {
                                    t_pintar = false;
                                }
                            }
                            if(t_pintar){
                                html+="<option value='"+tematico.id+"'>"+tematico.nombre+"</option>";
                                cont ++;
                            }
                        }
                    }
                }
            }
        }
        $("#"+idelementpintado).multiselect('destroy');
        $("#"+idelementpintado).html(html);
        if (typeof tematicos!="undefined" && typeof tematicos!=undefined) {
            if (cont > 0) {
                slctGlobalHtml(idelementpintado,'simple', valarray);
                $("#"+iddiverror).css("display", "none");
                if (parent == 0) {
                    $("#"+iddivtematico).css("display", "inline-block");
                }
            } else {
                $("#"+idelementpintado).css("display", "none");
                $("#"+iddivtematico).css("display", "none");
                if (parent == 0) {
                    $("#"+iddiverror).css("display", "block");
                }
            }
        } else {
            $("#"+idelementpintado).css("display", "none");
            $("#"+iddivtematico).css("display", "none");
            if (parent == 0) {
                $("#"+iddiverror).css("display", "block");
            }
        }
    },
    pintarNodosCms : function() {
        var contador=0;
        var nodoId=vm.solicitud.cod_nodo;
        var tipolegado = vm.solicitud.tipo_legado;

        html = "<option value=''>.::Seleccione::.</option>"; 
        for (var i in vm.lista.nodo) {

            html+="<option value='"+vm.lista.nodo[i].id+"'>"+vm.lista.nodo[i].nombre+"</option>";
            contador=contador+1;
        }

        $('#slct_nodo_modal_cms').multiselect('destroy');
        $("#slct_nodo_modal_cms").html(html);
        slctGlobalHtml('slct_nodo_modal_cms','simple', nodoId);

        if(contador>0 && tipolegado==1){$("#div_nodo_modal_cms").show();}
        else{$("#div_nodo_modal_cms").hide();}
    },
    pintarTrobasCms : function() {
        var contador=0;
        var trobaId=vm.solicitud.cod_troba;
        var tipolegado = vm.solicitud.tipo_legado;

        html = "<option value=''>.::Seleccione::.</option>"; 
        for (var i in vm.lista.troba) {

            html+="<option value='"+vm.lista.troba[i].id+"'>"+vm.lista.troba[i].nombre+"</option>";
            contador=contador+1;
        }

        $('#slct_troba_modal_cms').multiselect('destroy');
        $("#slct_troba_modal_cms").html(html);
        slctGlobalHtml('slct_troba_modal_cms','simple', trobaId);
        
        if(contador>0 && tipolegado==1){$("#div_troba_modal_cms").show();}
        else{$("#div_troba_modal_cms").hide();}
    },
    tbDatosGo_InputsCms : function(){

        Modal.pintarNodosCms();
        Modal.pintarTrobasCms();
    
    }
};
$(document).ready(function(){

    $("#slct_tipo_legado").change(function(){
        renderMotivoDevolucionFilter($(this).val());
    });
    $("#btn-reenviar-st").click(function(e){
        var solicitudtecnica = $("#solicitudtecnica").val();
        if (solicitudtecnica == "") {
            return false;
        }
        Legado.reenviarRespuestaST(solicitudtecnica);
    });
    
    $("#slct_tipo_devolucion").change( function(){
        $('#grupoOculto').hide();
        $('#divtematico2').hide();
        $('#divtematico3').hide();
        $('#divtematico4').hide();
        $('#slct_tematico_id2').val('');
        $('#slct_tematico_id3').val('');
        $('#slct_tematico_id4').val('');
    });
    validarDatosBitacora();

    $("#slct_tipo_devolucion").change(Modal.pintarMotivos);
    $(".slct_tematicos").change(function(e){
        var id = e.target.id;
        var val = $(this).val();
        switch(id) {
            case "slct_tematico_id1":
                $("#divtematico2, #divtematico3, #divtematico4").css("display", "none");
                $("#divtematico2").css("display", "inline-block");                
                if($("#slct_tematico_id1").val()==1){                    
                    $("#grupoOculto").show();                     
                }else{
                    $("#grupoOculto").hide(); 
                    //LimpiarBitacora();                   
                }
                Modal.pintarTematicos(val, "slct_tematico_id2", "divtematico2");
                break;
            case "slct_tematico_id2":
                $("#divtematico3, #divtematico4").css("display", "none");
                $("#divtematico3").css("display", "inline-block");
                Modal.pintarTematicos(val, "slct_tematico_id3", "divtematico3");
                break;
            case "slct_tematico_id3":
                $("#divtematico4").css("display", "none");
                $("#divtematico4").css("display", "inline-block");
                Modal.pintarTematicos(val, "slct_tematico_id4", "divtematico4");
                break;
            default:
                break;
        }
    });
    $("#btn_activar_precierre").click(activarPreCierre);

    slctGlobalHtml("slct_tematico_id1_agenda","simple");
    slctGlobalHtml("slct_tematico_id2_agenda","simple");
    slctGlobalHtml("slct_tematico_id3_agenda","simple");
    slctGlobalHtml("slct_tematico_id4_agenda","simple");
    $(".slct_tematicos").change(function(e){
        var id = e.target.id;
        var val = $(this).val();
        if (val!="") {
            switch(id) {
                case "slct_tematico_id1_agenda":
                    $("#divtematico2-agenda, #divtematico3-agenda, #divtematico4-agenda").css("display", "none");
                    $("#divtematico2-agenda").css("display", "inline-block");
                    Modal.pintarTematicos(val, "slct_tematico_id2_agenda", "divtematico2-agenda");
                    break;
                case "slct_tematico_id2_agenda":
                    $("#divtematico3-agenda, #divtematico4-agenda").css("display", "none");
                    $("#divtematico3-agenda").css("display", "inline-block");
                    Modal.pintarTematicos(val, "slct_tematico_id3_agenda", "divtematico3-agenda");
                    break;
                case "slct_tematico_id3_agenda":
                    $("#divtematico4-agenda").css("display", "none");
                    $("#divtematico4-agenda").css("display", "inline-block");
                    Modal.pintarTematicos(val, "slct_tematico_id4_agenda", "divtematico4-agenda");
                    break;
                default:
                    break;
            }
        }
    });
    $('#st-detalle a').on('shown.bs.tab', function(e){
        if ($(this)[0].hash=='#mapa') {
            if (vm.solicitud.tipo_legado == 1) {//cms
                PestanaOrden.pintarMapa();
            }
        }
    });

    $("#listopciones a").on("shown.bs.tab", function(e){
        tabseleccionadoactive = $(this)[0].hash;
        switch(tabseleccionadoactive) {
            case "#tab8" : //tab cierre
                Legado.getImagenes();
                break;
            default:
                break;
        }
    });

    $("#liopt-componentes").on('click', function(event) {
        $("#t_componentes").DataTable();
    });
    $("#liopt-cambios").on('click', function(event) {
        $("#t_cambios").DataTable();
    });
    $("#liopt-movimientos").on('click', function(event) {
        //$("#t_movimientos").DataTable();
    });
    $("#btn_enviar_ofsc").click(OfscModal);
    //$(".cierreSelect").click(OfscModal);   
    //$("#btn_editar").click(Editar);
    $("#btn_stop_devuelta").click(stopOrden);
    $("#btn_limpiar_formulario").click(resetFormularioToa);
    $("#btn_cierre").click(validarInputsCierre);
    $(".btn-envio-legado").click(enviarLegado);
    $(".btn-forzar-coordenadas").click(forzarCoordenadas);
    $("#btn_editar_datago").click(guardarDatosGo);
    $("#form-sms-legado").on('submit', function(event) {
        event.preventDefault();
        Legado.enviarSms($(this));
    });
    $(".numeros_telefonicos").on('input', function (event) { 
        valor = $(this).val().replace(/[^\d,]/g, '');
        $(this).val(valor); 
    });
    $("#t_listados").DataTable({paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        order: [[ 0, "desc" ]],
        info: true,
        autoWidth: true,
        language: config
    });

    $("#btn-buscar").click(BuscarSolicitud);
    $("#btn-descargar").click(DescargarExcel);
    $("#txt_valor").on('keypress', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            BuscarSolicitud();
        }
    });
    slctGlobalHtml('slct_tipo','simple');
    $("#slct_tipo").change(function(e){
        idtipo = $(this).val();
        $("#txt_valor").val("");
        $("#txt_valor").focus();
    });
    $('#txt_created_at, #txt_fecha_agenda').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });
    $('#txt_fecha_hora_registro_requerimiento, #txt_fecha_hora_asignacion, #txt_fecha_hora_instalacion_alta').daterangepicker({
        timePicker: true,
        singleDatePicker: true,
        maxViewMode: 1,
        showDropdowns: true,
        format: 'YYYY-MM-DD HH:mm',
    });
    slctGlobalHtml('slct_agdsla','simple');
    slctGlobalHtml('slct_estado_st','multiple');
    slctGlobalHtml('slct_actividad','multiple');
    slctGlobalHtml('slct_tipo_envio_ofsc','multiple');
    slctGlobalHtml('slct_tipo_devolucion','simple');
    slctGlobalHtml('slct_motivosdevolucion','simple');
    slctGlobalHtml('slct_tipo_devolucion_filtro','multiple');
    slctGlobalHtml('slct_tipo_legado','multiple');
    $("#slct_estado_aseguramiento").change(function(){
        /*$("#slct_estado_aseguramiento option").each(function(){
            if ($(this).val() == 3 || $(this).val() == 5) {
                if ($(this).is(":checked")) {
                    $("#div_tipo_devolucion_filtro").css("display", "inline-block");
                } else {
                    $("#div_tipo_devolucion_filtro").css("display", "none");
                }
            }
        });*/
        if( perfilId == 13 || perfilId == 14 || perfilId == 15 ) {
            return;
        }
        var situacion=$('#slct_estado_aseguramiento').val();
        if(situacion!=null){
            for(var i=0;i<situacion.length;i++){
                if(situacion[i]==3 || situacion[i]==5){
                    $("#div_tipo_devolucion_filtro").css("display", "inline-block");
                }else{$("#div_tipo_devolucion_filtro").css("display", "none");}
            }
        } else {
            $("#div_tipo_devolucion_filtro").css("display", "none");
        }
        
    });

    $(document).on('change','#slct_motivosdevolucion',function(e) {
        e.preventDefault();
        var motseleccionado = $("#slct_motivosdevolucion").val();
        var contador = 0;

        html = "<option value=''>.::Seleccione::.</option>"; 
        for (var i in vm.lista.submotivosdevolucion) {
            if(vm.lista.submotivosdevolucion[i].motivo_ofsc_id == motseleccionado){
                html+="<option value='"+vm.lista.submotivosdevolucion[i].id+"'>"+vm.lista.submotivosdevolucion[i].descripcion+"</option>";
                contador=contador+1;
            }
        }

        $('#slct_submotivosdevolucion').multiselect('destroy');
         $("#slct_submotivosdevolucion").html(html);
         slctGlobalHtml('slct_submotivosdevolucion','simple', 0);
         Modal.pintarTematicos(0, "slct_tematico_id1", "divtematico1", 'div-error-tematico-cierre');//cambio
         $('#divtematico2').hide();
         $('#divtematico3').hide();
         $('#divtematico4').hide();
         $('#slct_tematico_id2').val('');
         $('#slct_tematico_id3').val('');
         $('#slct_tematico_id4').val('');
         if(contador>0){$("#divsubmotivosdevolucion").fadeIn('slow');}
         else{$("#divsubmotivosdevolucion").hide();}
    });


    var datos_={parametros:'1', prueba:'test'};

    var selects = [
     "zonal", "nodo", "operacion",'troba',
     'geotap', "estadoofsc", "contratascms","actividadtipo",
     "mdf", "motivosliquidacion", "estadoaseguramiento","quiebre",
     'actividadtipo', "quiebre", "contratasgestel", "bucket",
     "masiva", "geocerca",
     'solicitudes', 'motivosdevolucion', 'tematicos','submotivosdevolucion',
     'tiporeq','motivogen'];

    var slcts = [
     "slct_zonal", "slct_cod_nodo", "slct_tipo_operacion", 'slct_cod_troba',
     'slct_cod_tap', 'slct_estado_ofsc', 'slct_contrata_modal_cms','slct_actividad_tipo_modal',
     'slct_cabecera_mdf', 'slct_motivosliquidacion', "slct_estado_aseguramiento","slct_quiebre_modal",
     'slct_actividad_tipo_id', "slct_quiebre", 'slct_contrata_modal_gestel',"slct_bucket",
     "slct_parametro", "slct_workzone",
     'slct_submotivosdevolucion', 'slct_tiporeq_cms','slct_motivogen_cms'
    ];

    var tipo = [
     "multiple", "multiple", "multiple", "multiple",
     "multiple", "multiple", "multiple", "multiple",
     "multiple", "multiple", "multiple", "simple",
     'multiple', "multiple", "multiple", "simple", 
     "multiple", "multiple"
    ];

    var valarray = [
     null, null, null, null,
     null, null, null, null,
     null, null, [], null,
     null, null, null, null, null,
     null
    ];
    var data = [
     {}, {}, datos, {},
     {}, {}, {}, {},
     {}, {}, {}, {},
     {}, {}, {}, {},
     {}, {}, {}, {},
     {}, {}, {}, {}
    ];
    var afectado = [
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0, 0, 0,
     0, 0
    ];
    var afectados = [
     "", "", "", "",
     "", "", "", "",
     "", "", "", "",
     "", "", "", "",
     "", ""
    ];
    var slct_id = [
     "", "", "", "",
     "", "", "", "",
     "", "", "", "",
     "", "", "", "",
     "", ""
    ];
    var es_lista =[
     true, true, true, true,
     true, true, true, true,
     true, true, true, true,
     true, true, true, true,
     true, true,
     false, false, false,
    ];
    if( perfilId == 13 || perfilId == 14 || perfilId == 15 ) {
        $("#btn-descargar").css('display','none');
        $("#liopt-cambios").css("display", "none");
        slctGlobalHtml("slct_estado_aseguramiento", "multiple");
        slctGlobalHtml("slct_motivo_devolucion_f", "multiple");
        Legado.getSelects(["motivosdevolucion" ,"tematicos"], null, null, null, null, null, null, null, null,{},{}, [false, false]);
    } else {
        $("#div_personalizado").css('display','');
        $("#btn-descargar").css('display','');
        $("#liopt-cambios").css("display", '');
        Legado.getSelects(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id,{},{},{}, es_lista);
        
    }

    var enforceModalFocusFn;
    $('#BandejaModal').on('show.bs.modal', function (event) {
        enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        var button = $(event.relatedTarget);
        Legado.modalOpen(button);
    });
    $('#BandejaModal').on('hide.bs.modal', function (event) {
        $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
        var modal = $(this);
        modal.find('.modal-body input[type=text]').val('');
        hideModal();
    });
    $(window).on('mouseover', (function () {
        window.onbeforeunload = null;
    }));
    $(window).on('mouseout', (function () {
        window.onbeforeunload = ConfirmLeave;
    }));
    var prevKey="";
    $(document).keydown(function (e) {
        var key = e.key.toUpperCase();
        if ( key=="F5" ||
            (key== "W" && prevKey == "CONTROL") ||
            (key== "R" && prevKey == "CONTROL") ||
            (key== "F4" && prevKey == "ALT") ||
            (key== "F4" && prevKey == "CONTROL")
        ) {
            window.onbeforeunload = ConfirmLeave;
        }
        prevKey = e.key.toUpperCase();
    });
    Legado.getPerfil(perfilId);  
});
validaPermisoPerfil = function(tipo) {
    var opciones = objperfil.opciones;
    var valoresevaluar = [];
    var respuesta = false;
    var resultado = [];
    objcriterios = {};
    for (var i in opciones) {
        var opcion = opciones[i].opcion;
        if (opcion.tipo == tipo) {
                objcriterios[opcion.id] = opciones[i].criterios;
        }
    }
    for (var i in objcriterios) {
        var criterios = objcriterios[i];
        respuesta = false;
        for (var j in criterios) {
            var criterio = criterios[j];
            if (criterio.obj!="null" && criterio.obj!=null) {
                if (vm.solicitud[criterio.obj.nombre_gestion] == criterio.valor) {
                    respuesta = true;
                }
            }
        }
        resultado.push(respuesta);
    }
    if ($.inArray(true, resultado) >= 0) {
        return true;
    } else {
        return false;
    }
}

Ocultar=function(){
    $("#grupoOculto").hide();
}

LimpiarBitacora=function(){
    $("#txt_tel1").val("");
    $("#txt_tel2").val("");
    $("#txt_contacto").val("");
    $("#txt_direccion").val("");
}
validarDatosBitacora=function(){
    $("#txt_tel1").validCampoFranz('1234567890');
    $("#txt_tel2").validCampoFranz('1234567890');
    $("#txt_contacto").validCampoFranz(' 1234567890_abcdefghijklmnñopqrstuvwxyz-');
    $("#txt_direccion").validCampoFranz(' 1234567890_abcdefghijklmnñopqrstuvwxyz#-');
}
renderMotivoDevolucionFilter = function(tipo_legado){
    if( tipo_legado == "" ){
        $("#slct_motivo_devolucion_f").parent().addClass("hide");
    } else {
        $("#slct_motivo_devolucion_f").parent().removeClass("hide");
    }


    var tipomotivo = "";

    if (perfilId == 14 || perfilId == 15){
        tipomotivo = "T";
    }

    if (perfilId == 13){
        tipomotivo = "C";
    }

    var valarray = [''];
    html = "";

    for (var i in vm.lista.motivosdevolucion) {

        if( tipomotivo == vm.lista.motivosdevolucion[i].estado_ofsc ){

            if(vm.lista.motivosdevolucion[i].tipo_legado == tipo_legado ){
                if (perfilId == 14) {
                    if ($.inArray(parseInt(vm.lista.motivosdevolucion[i].id), vm.posiblesmotivos) < 0) {
                       html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                    }
                }
                if (perfilId == 15) {
                    if ($.inArray(parseInt(vm.lista.motivosdevolucion[i].id), vm.posiblesmotivos) >= 0) {
                       html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                    }
                }

                if (perfilId == 13) {
                    html+="<option value='"+vm.lista.motivosdevolucion[i].id+"'>"+vm.lista.motivosdevolucion[i].descripcion+"</option>";
                }
            }
        
        }
    }

    if( perfilId == 15 ){
        valarray.push("103", "467", "115");
    }

    $("#slct_motivo_devolucion_f").multiselect('destroy');
    $("#slct_motivo_devolucion_f").html(html);
    slctGlobalHtml('slct_motivo_devolucion_f','multiple', valarray);
}
