var PestanaOrden = {
    validarVisibilidadPestana : function(){
        $("#liopt-ordengestel, #liopt-ordencms, #liopt-ordengestelprov").css("display", "none");
        if (vm.solicitud.tipo_legado == 1) {
            $("#liopt-ordencms").css("display", "inline-block");
        }
        if (vm.solicitud.tipo_legado == 2) {
            if(vm.solicitud.actividad_id==1) {
                $("#liopt-ordengestel").css("display", "inline-block");
            } else {
                $("#liopt-ordengestelprov").css("display", "inline-block");
            }
        }
        if (vm.solicitud.estado_ofsc_id == 1 || vm.solicitud.estado_ofsc_id == 8) {
            $("#btn_editar").removeAttr("disabled");
        } else {
            $("#btn_editar").prop("disabled", true);
        }
    },
    validarVisibilidadPanel : function(){
        $("#BandejaModal .tab-pane.fade").removeClass("active");
        $("#BandejaModal .tab-pane.fade").removeClass("in");
        if (vm.solicitud.tipo_legado == 1) {
            $("#BandejaModal #tabsolicitudcms.tab-pane.fade").addClass("active");
            $("#BandejaModal #tabsolicitudcms.tab-pane.fade").addClass("in");
        }
        if (vm.solicitud.tipo_legado == 2) {
            if(vm.solicitud.actividad_id==1) {
                $("#BandejaModal #tabsolicitudgestel.tab-pane.fade").addClass("active");
                $("#BandejaModal #tabsolicitudgestel.tab-pane.fade").addClass("in");
            } else {
                $("#BandejaModal #tabsolicitudgestelprov.tab-pane.fade").addClass("active");
                $("#BandejaModal #tabsolicitudgestelprov.tab-pane.fade").addClass("in");  
            }
        }
    },
    validarVisibilidadForzarCoordenadas : function() {
        if (vm.solicitud.estado_ofsc_id == 1) {
            $(".fieldset-forzar-validacion").css("display", "block");
        } else {
            $(".fieldset-forzar-validacion").css("display", "none");
        }
    },
    pintarMapa : function () {
        try { markerSpiderfier.clearMarkers(); }catch(c){}
        removeMarkers();
        vm.map = new gm.Map(
            document.getElementById("mapafftts"),
            mapOptions
        );
        markerSpiderfier = new OverlappingMarkerSpiderfier(vm.map, spiderConfig);

        vm.bounds = new gm.LatLngBounds();
        var icon = "img/icons/tec_0e8499.png";
        var coordx_cliente = parseFloat(vm.solicitud.coordx_cliente);
        var coordy_cliente = parseFloat(vm.solicitud.coordy_cliente);
        var coordx_tap = parseFloat(vm.solicitud.coordx_direccion_tap);
        var coordy_tap = parseFloat(vm.solicitud.coordy_direccion_tap);
        for (var i = vm.movimientos.length - 1; i >= 0; i--) {
            if (vm.movimientos[i].estado_ofsc_id==1 && vm.movimientos[i].estado_aseguramiento==1) {
                var detalle = vm.movimientos[i].detalle;
                if (detalle.length>0) {
                    for (var k = detalle.length - 1; k >= 0; k--) {
                        if (detalle[k].tipo==1 && detalle[k].coordx_tecnico && detalle[k].coordx_tecnico!=0 &&
                            detalle[k].coordy_tecnico && detalle[k].coordy_tecnico!=0
                            ) {
                            addMarkerCoord(icon, detalle[k]);
                        }
                    }
                }
                //continue;
            }
            if ((vm.movimientos[i].coordx_tecnico && vm.movimientos[i].coordy_tecnico &&
                 vm.movimientos[i].coordx_tecnico!=0 && vm.movimientos[i].coordy_tecnico!=0) &&
                ((vm.movimientos[i].estado_ofsc_id==2  && vm.movimientos[i].estado_aseguramiento==1 ) ||//iniciado
                (vm.movimientos[i].estado_ofsc_id==4 /*&& vm.movimientos[i].estado_aseguramiento!=2*/ ) ||//no realizado
                (vm.movimientos[i].estado_ofsc_id==6 /*&& vm.movimientos[i].estado_aseguramiento!=2*/ ) //completado
                )) {
                addMarkerMov(icon,vm.movimientos[i]);
                // agregando linea entre punto de inicio y la actividad
                if (vm.movimientos[i].estado_ofsc_id == 2) {
                    addLine(vm.movimientos[i], coordy_cliente, coordx_cliente);
                }
            }
        }
        var markerCluster = new MarkerClusterer(vm.map, vm.markers);
        markerCluster.setMaxZoom(config.minZoom);

        icon = {
            path: fontawesome.markers.CALENDAR,
            scale: 0.5,
            strokeWeight: 0.3,
            strokeColor: '#000',
            strokeOpacity: 1,
            fillColor: vm.solicitud.color_ofsc,
            fillOpacity: 1
        };
        label = "<div><b>"+vm.fullName+"</b></div>";
        addMarker( coordy_cliente, coordx_cliente, label, icon);

        icon = "/img/icons/tap.png";
        label = "<label><b>TAP</b></label>";
        addMarker( coordy_tap, coordx_tap, label, icon);

        vm.map.fitBounds(vm.bounds);
    },
    showDatosGo : function () {
        $("#div_contrata_modal_cms, #div_contrata_modal_gestel").css("display", "none");
        if (vm.solicitud.tipo_legado == 1) {
            $("#div_contrata_modal_cms").css("display", "block");
            $("#slct_contrata_modal_cms").val([vm.solicitud.codigo_contrata]);
            $("#slct_contrata_modal_cms").multiselect("refresh");
        }
                
        if (vm.solicitud.tipo_legado == 2) {
            $("#div_contrata_modal_gestel").css("display", "block");
            $("#slct_contrata_modal_gestel").val([vm.solicitud.contrata]);
            $("#slct_contrata_modal_gestel").multiselect("refresh");
        }
        Modal.tbDatosGo_InputsCms();
    },
    hideDatosGo : function () {
        $("#div_contrata_modal_cms, #div_contrata_modal_gestel").css("display", "block");
        $("#slct_contrata_modal_cms, #slct_contrata_modal_gestel").val();
        $("#slct_contrata_modal_cms, #slct_contrata_modal_gestel").multiselect("refresh"); 
    },

    validar_modalOrden :function(perfil,tipo_legado){

        if(tipo_legado==1){

            if(perfil==8){
                $('#btnVerBtn').show();
            }else{
                $('#btnVerBtn').hide();
            }
            
        }

    }
};
validarTabOrden = function() {
    if (vm.solicitud.tipo_legado == 1) {
        return true;
    }
    if (vm.solicitud.tipo_legado == 2) {
        if(vm.solicitud.actividad_id==1) {
            return true;
        } else {
            return true;
        }
    }
    return false;
};
guardarDatosGo = function() {
    if ( validarDatosGo() ) {
        swal({
            title: '¿Desea actualizar los datos registraos en GO para TOA?',
            text: "Esta Accion puede tomar unos minutos",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText : 'No',
            confirmButtonText: 'Si, Actualizar'
        }).then(function () {
            var form = $("#form_bandeja_datosgo");
            Legado.guardarDatosGo(form);
        });
        return false;

    }
};

validarDatosGo = function() {
        var datos_go_v = {
            quiebre_id              : $("#slct_quiebre_modal").val(),
            actividad_tipo_id       : $("#slct_actividad_tipo_modal").val(),
            contrata                : $("[name=slct_contrata_modal]").val(),
            nodo                    : $("#slct_nodo_modal_cms").val(),
            troba                   : $("#slct_troba_modal_cms").val()
        };

        var empties = [];
        //Validación
        if ( datos_go_v.quiebre_id === null || datos_go_v.quiebre_id == "" ) {
            empties.push("Quiebre");
        }

        if ( datos_go_v.actividad_tipo_id === null || datos_go_v.actividad_tipo_id == "" ) {
            empties.push("Actividad tipo");
        }

        if ( datos_go_v.contrata === null || datos_go_v.contrata == "" ) {
            empties.push("Contrata");
        }

        if ( datos_go_v.nodo === null || datos_go_v.nodo == "" ) {
            empties.push("Nodo");
        }

        if ( datos_go_v.troba === null || datos_go_v.troba == "" ) {
            empties.push("Troba");
        }

        if (parseInt(vm.solicitud.tipo_legado) == 1) {
            if ( empties.length > 0 ) {
                swal({
                    title: 'Completar los siguientes campos:',
                    text: empties.join(", "),
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Entendido"
                }).then(function () {
                    return;
                });

                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
}