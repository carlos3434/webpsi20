$(document).ready(function() {
    slctGlobalHtml('slct_estado_st','multiple');
    slctGlobalHtml('slct_actividad','multiple');
    var selects = ["bucket", "estadoofsc", "actividadtipo"];
    var slcts = ["form_descarga .slct_bucket", "form_descarga .slct_estados_ofsc", "form_descarga .slct_actividad_tipo_id"];
    var tipo = ["multiple", "multiple", "multiple"];
    descargaST.listarSlctMasivo(selects, slcts, tipo);
    $('#created_at').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        // minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        // startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });

    $("#btn-descargar").click(DescargarExcel);
});

DescargarExcel = function () {
    var fecha = $("#created_at").val();
    if (!fecha) {
        Psi.sweetAlertError("Seleccionar rango de Fecha");
        return;
    } else {
        $("#form_descarga").attr("action", "gestionesSt/descargarjson");
        $("#form_descarga").submit();
        $("#form_descarga").attr("action", "");
    }
};