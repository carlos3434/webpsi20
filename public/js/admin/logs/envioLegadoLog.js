$(document).ready(function(){
    envioLegadoLog.listar();
        $('#rangoFechas').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        autoApply: true,
        format: 'YYYY-MM-DD'
    });
    $('#rangoFechas').val(
        moment().subtract(15, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );
    $("#solicitud_tecnica").validCampoFranz('1234567890_abcdefghijklmnñopqrstuvwxyz'); 
    slctGlobalHtml("slct_accion","multiple");
})

verEnvio = function (id) {
    console.log(id);       
	envioLegadoLog.verEnvioLegado(id);
}
filtrar=function(){
	envioLegadoLog.listar();	
}
descargar = function() {
    var rangoFechas = $("#rangoFechas").val();
    var rangoFechas = rangoFechas.split(" - "); 
    $("#form_buscar").attr("action", "enviolegadolog/listar");
    $("#form_buscar").append("<input type='hidden' name='accion' value='download' id='accion' />");
    $("#form_buscar").append("<input type='hidden' name='solicitudTecnica' value='"+$("#solicitud_tecnica").val()+"' id='solicitudTecnica' />");
    $("#form_buscar").append("<input type='hidden' name='fecha' value='"+rangoFechas+"' id='fecha' />");
    if ($("#collapse1").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
    } else if ($("#collapse2").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
    }
    $("#form_buscar").submit();
    $("#accion").remove();
    $("#tipo_busqueda").remove();
    $("#solicitudTecnica").remove();
    $("#form_buscar").attr("action", "");   
}
cleardate = function() {
    $("#rangoFechas").val("");
    $("#fecha").val("");
}
