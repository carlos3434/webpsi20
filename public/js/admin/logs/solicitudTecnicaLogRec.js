$(document).ready(function(){
    solicitudTecnicaLogREc.listar();
        $('#rangoFechas').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(1, 'days'),
        endDate: moment(),
        autoApply: true,
        format: 'YYYY-MM-DD'
    });
    $('#rangoFechas').val(
        moment().subtract(1, 'days').format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );

    $("#solicitud_tecnica").validCampoFranz('1234567890_abcdefghijklmnñopqrstuvwxyz'); 

})

verEnvio = function (trama) {
	solicitudTecnicaLogREc.verEnvioLegado(trama);
}

filtrar = function(){

    solicitudTecnicaLogREc.listar();

}

function validaFecha(){

    var input_fecha = $("#rangoFechas").val();

    if ( input_fecha == "" || typeof input_fecha == 'undefined' ) {
        alert("No seleccionó un rango de fechas");
        return false;
    }

    var fechas = input_fecha.split(" - ");


    var f_ini = moment(fechas[0]);

    var f_fin = moment(fechas[1]);

    var horas_restantes = f_fin.diff(f_ini, "hours");

    if ( horas_restantes > 24 ) {
        alert("Seleccione un rango de fechas máximo de 2 días.");
        return false;
    } else {
        return true;
    }



}

descargar = function() {

    if ( $("#collapse1").css("display") == "block" ) {

        if( $("#solicitud_tecnica").val() == "" ) {
            alert("No seleccionó ningún filtro");
            return;
        }
        
    } else if ($("#collapse2").css("display") == "block") {
        if ( validaFecha() == false ) {
            return;     
        }
    }

    var rangoFechas = $("#rangoFechas").val();
    var rangoFechas = rangoFechas.split(" - "); 
    $("#form_buscar").attr("action", "solicitudtecnicalogrec/listar");
    $("#form_buscar").append("<input type='hidden' name='accion' value='download' id='accion' />");
    $("#form_buscar").append("<input type='hidden' name='solicitudTecnica' value='"+$("#solicitud_tecnica").val()+"' id='solicitudTecnica' />");
    $("#form_buscar").append("<input type='hidden' name='fecha' value='"+rangoFechas+"' id='fecha' />");
    if ($("#collapse1").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
    } else if ($("#collapse2").css("display") == "block") {
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
    }
    $("#form_buscar").submit();
    $("#accion").remove();
    $("#tipo_busqueda").remove();
    $("#solicitudTecnica").remove();
    $("#form_buscar").attr("action", "");    
}
cleardate = function() {
    $("#rangoFechas").val("");
    $("#fecha").val("");
}
