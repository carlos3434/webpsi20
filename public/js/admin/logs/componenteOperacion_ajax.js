var envioLegadoLog = {
	listar : function() {
		$("#tb_componenteOperacion").dataTable({
			"processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                envioLegadoLog.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.fecha_log;}, name:'fecha_log'},
                {data : function( row, type, val, meta) {return row.fecha_respuesta;}, name:'fecha_respuesta'},
                {data : function( row, type, val, meta) {return row.id_solicitud_tecnica;}, name:'id_solicitud_tecnica'},
                {data : function( row, type, val, meta) {return row.codreq;}, name:'codreq'},
                {data : function( row, type, val, meta) {return row.observacion;}, name:'observacion'},
                {data : function( row, type, val, meta) {return row.operacion;}, name:'operacion'},
                {data : function( row, type, val, meta) {return row.proceso;}, name:'proceso'},
                {data : function( row, type, val, meta) {return row.descripcion;}, name:'descripcion'},                
                                
                /*{data : function( row, type, val, meta) {
                 htmlButtons=" <button onclick='verEnvio("+row.request+","+row.response+")' type='button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button>";
                 return htmlButtons;
                }},*/
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
		})
	},
	http_ajax: function(request,callback){

        if ( $("#collapse1").css("display") == "block" ) {

            if( $("#solicitud_tecnica").val() == "" ) {
                $("#tipo_busqueda").val(0);
            } else {
                $("#tipo_busqueda").val(1);
            }

            
        } else if ($("#collapse2").css("display") == "block") {
            $("#tipo_busqueda").val(2);
        }


        var contador = 0;             
        var solicitudTecnica = $("#solicitud_tecnica").val();
        var rangoFechas = $("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form += '&column='+request.columns[ order.column ].name;
        form += '&dir='+order.dir;
        form += "&per_page="+request.length;
        form += "&page="+(request.start+request.length)/request.length;        
        if ( rangoFechas != '' ) {
            form += "&fecha="+rangoFechas;
        }
        if ( solicitudTecnica != '' ) {
            form += "&solicitudTecnica="+solicitudTecnica;
        }
        eventoCargaMostrar();
        axios.post('componenteoperacion/listar',form).then(response => {
            callback(response.data);            
        }).catch(e => {           
        }).then(() => {
            eventoCargaRemover();
        });
    },
   verEnvioLegado: function(envio,respuesta) {
        form2 = 'envio='+envio;
        form2 += '&respuesta='+respuesta;

        eventoCargaMostrar();
        axios.post('enviolegadolog/mostarrespuesta',form2).then(response => {            
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
   },

   postTipooperacion: function() {
        eventoCargaMostrar();
        axios.post('componenteoperacion/tipooperacion').then(response => {
            console.log(response.data);
            tipoOperacion=response.data;            
            $("#slct_operacion").append()
        }).catch(e => {           
        }).then(() => {
            eventoCargaRemover();
        });
   }
}
