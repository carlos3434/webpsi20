var envioLegadoLog = {
    listar_cabeceras: function(){
        $.ajax({
            url         : 'dashboard/listarcabeceras',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                last_update();
                eventoCargaRemover();

                var html='';
                var date=new Date();
                fecha_actual= date.getFullYear() + "-" + (date.getMonth() +1) +"-"+ date.getDate();
                tiempo_act= date.getHours() +':'+ date.getMinutes() +':'+ date.getSeconds();

                var tiempo_actual = new Date("2015-02-12 "+tiempo_act);
                var fecha_act = moment(fecha_actual);
                $.each(data, function(key, val){
                    
                    var fecha = moment(val.fecha_ultimo);
                    var diffecha=fecha_act.diff(fecha, 'days');

                    html+='<tr>';

                    var tiempo_ultimo = new Date("2015-02-12 "+val.tiempo_ultimo);
                    //La diferencia se da en milisegundos así que debes dividir entre 1000
                    var diftiempo = ((tiempo_actual-tiempo_ultimo)/1000);

                    if(diffecha > 0){
                        html+='<td border="1" class="small" style="color:white;background:#86ADC3;"><i class="glyphicon glyphicon-star-empty"></i> <b>'+val.nomb_flujo+'</b></td>';
                        html+='<td class="danger"><b>'+val.actividad+'</b></td>';
                        html+='<td class="danger"><b>'+val.cantidad+'</b></td>';
                        html+='<td class="danger"><b>'+val.fecha_tiempo+'</b></td>';
                        //html+='<td ><img id="imagen_actual" width="30px" src="http://webpsi20/imagenes/alarma.gif"/></td>';
                    }else{

                        if(diftiempo > 900){
                            html+='<td border="1" class="small" style="color:white;background:#86ADC3;"><i class="glyphicon glyphicon-star-empty"></i> <b>'+val.nomb_flujo+'</b></td>';
                            html+='<td class="danger"><b>'+val.actividad+'</b></td>';
                            html+='<td class="danger"><b>'+val.cantidad+'</b></td>';
                            html+='<td class="danger"><b>'+val.fecha_tiempo+'</b></td>';
                            //html+='<td ><img id="imagen_actual" width="30px" src="http://webpsi20/imagenes/alarma.gif"/></td>';
                        }else{
                            html+='<td border="1" class="small" style="color:white;background:#86ADC3;"><i class="glyphicon glyphicon-star"></i> <b>'+val.nomb_flujo+'</b></td>';
                            html+='<td><b>'+val.actividad+'</b></td>';
                            html+='<td><b>'+val.cantidad+'</b></td>';
                            html+='<td><b>'+val.fecha_tiempo+'</b></td>';
                        }

                    }

                    html+='</tr>';
                });
                $("#tbl_cabecera tbody").html(html);
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
    listar : function() {
        $("#tb_envioLegadoLog").dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                envioLegadoLog.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.created_at;}, name:'created_at'},
                {data : function( row, type, val, meta) {return row.solicitud;}, name:'solicitud'},                
                {data : function( row, type, val, meta) {return row.accion;}, name:'accion'},
                {data : function( row, type, val, meta) {return row.code_error;}, name:'code_error'},
                {data : function( row, type, val, meta) {return row.descripcion_error;}, name:'descripcion_error'},                               
                /*{data : function( row, type, val, meta) {

                htmlButtons=" <button onclick='verEnvio("+row.id+")' type='button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button>";                
                return htmlButtons;
                }},*/
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        })
    },
    http_ajax: function(request,callback){
        if ( $("#collapse1").css("display") == "block" ) {

            if( $("#solicitud_tecnica").val() == "" ) {
                $("#tipo_busqueda").val(0);
            } else {
                $("#tipo_busqueda").val(1);
            }

            
        } else if ($("#collapse2").css("display") == "block") {
            $("#tipo_busqueda").val(2);
        }
        
        var contador = 0;           
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form += '&column='+request.columns[ order.column ].name;        
        form += '&dir='+order.dir;
        form += "&per_page="+request.length;
        form += "&page="+(request.start+request.length)/request.length;         
        eventoCargaMostrar();
        axios.post('enviolegadolog/listar',form).then(response => {
            callback(response.data);
        }).catch(e => {            
        }).then(() => {
            eventoCargaRemover();
        });
    },
    listar_recepcion : function() {
        $("#tb_solicitudTecnicaLogRec").dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                envioLegadoLog.http_ajax_recepcion(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.created_at;}, name:'created_at'},
                {data : function( row, type, val, meta) {return row.id_solicitud_tecnica;}, name:'id_solicitud_tecnica'},
                {data : function( row, type, val, meta) {return row.code_error;}, name:'code_error'},
                {data : function( row, type, val, meta) {return row.descripcion_error;}, name:'descripcion_error'},
                {data : function( row, type, val, meta) {return row.nombre;}, name:'nombre'},
                /*{data : function( row, type, val, meta) {
                    htmlButtons=" <button onclick='verEnvio("+row.trama+")' type='button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button>";
                    return htmlButtons;
                }},*/
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        })
    },
    informacion: function(tipo){
        $.ajax({
            url         : 'informacion/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    //data.datos
                    mostrar(data.datos[0]);
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
    http_ajax_recepcion: function(request,callback){
        if ( $("#collapse1").css("display") == "block" ) {

            if( $("#solicitud_tecnica").val() == "" ) {
                $("#tipo_busqueda").val(0);
            } else {
                $("#tipo_busqueda").val(1);
            }

            
        } else if ($("#collapse2").css("display") == "block") {
            $("#tipo_busqueda").val(2);
        }
        
        var contador = 0;
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form += '&column='+request.columns[ order.column ].name;
        form += '&dir='+order.dir;
        form += "&per_page="+request.length;
        form += "&page="+(request.start+request.length)/request.length;
        eventoCargaMostrar();
        axios.post('solicitudtecnicalogrec/listar',form).then(response => {
            callback(response.data);
            //console.log(response);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    cargar: (function () {
                    var oTable = $('#datatable_mensaje')
                            .on('order.dt', function () {
                                eventoCargaMostrar();
                            })
                            .on('search.dt', function () {
                                eventoCargaMostrar();
                            })
                            .on('page.dt', function () {
                                eventoCargaMostrar();
                            })
                            .DataTable({
                                dom: "<'box'"+
                                    "<'box-header'<l>r>"+
                                    "<'box-body table-responsive no-padding't>"+
                                    "<'row'<'col-xs-6'i><'col-xs-6'p>>"+
                                    ">",
                                "processing": true,
                                "serverSide": true,
                                //"responsive": true,
                                "ajax": {
                                    url: "mensaje/listar",
                                    type: "POST",
                                    data: function (d) {
                                        d.txt_rangofecha = $('input[name=txt_rangofecha]').val();
                                        d.id_msg = $('input[name=id_msg]').val();
                                        d.asunto = $('select[name=asunto]').val();
                                        d.estado = $('select[name=estado]').val();
                                    }
                                },
                                "columns": [
                                    {data: 'id', name: 'id', visible: false},
                                    {data: 'message_id', name: 'message_id'},
                                    {data: 'company_id', name: 'company_id', visible: false},
                                    {data: 'subject', name: 'subject'},
                                    {data: 'body', name: 'body', visible: false, orderable: false, searchable: false},
                                    {data: 'created_at', name: 'created_at'},
                                    {data: 'updated_at', name: 'updated_at'},
                                    {data: 'estado', name: 'estado'}
                                    /*{data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}*/
                                ],
                                "order": [[6, 'desc']],
                                searchDelay: 750,
                                initComplete: function (settings, json) {
                                    this.api().columns().eq(0).each(function (index) {
                                        if (index == 12) {
                                            return false;
                                        }
                                        var column = this.column(index);
                                        var input = document.createElement("input");
                                        var title = $(column.footer()).text();
                                        $(input).css('width', '100%')
                                                .attr('placeholder', title)
                                                .addClass('form-control input-sm')
                                                .appendTo($(column.footer()).empty())
                                                .on('change', function () {
                                                    column.search($(this).val(), false, false, true).draw();
                                                });
                                    });
                                },
                                drawCallback: function (settings) {
                                    $(".overlay,.loading-img").remove();
                                },
                                "createdRow": function (row, data, dataIndex) {
                                    if ((data['deleted_at']) != null) {
                                        $('td', row).css('color','#DF0101'); //.eq(1)
                                    }
                                }
                            });
                    return oTable;
                })
}
