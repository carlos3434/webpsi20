var solicitudTecnicaLogREc = {
	listar : function() {
		$("#tb_solicitudTecnicaLogRec").dataTable({
			"processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                solicitudTecnicaLogREc.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.created_at;}, name:'created_at'},
                {data : function( row, type, val, meta) {return row.id_solicitud_tecnica;}, name:'id_solicitud_tecnica'},
                {data : function( row, type, val, meta) {return row.code_error;}, name:'code_error'},
                {data : function( row, type, val, meta) {return row.descripcion_error;}, name:'descripcion_error'},
                {data : function( row, type, val, meta) {return row.nombre;}, name:'nombre'},
                {data : function( row, type, val, meta) {
                    htmlButtons=" <button onclick='verEnvio("+row.trama+")' type='button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span></button>";
                    return htmlButtons;
                }},
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
		})
	},
	http_ajax: function(request,callback){
        if ( $("#collapse1").css("display") == "block" ) {

            if( $("#solicitud_tecnica").val() == "" ) {
                $("#tipo_busqueda").val(0);
            } else {
                $("#tipo_busqueda").val(1);
            }

            
        } else if ($("#collapse2").css("display") == "block") {
            $("#tipo_busqueda").val(2);
        }
        
        var contador = 0;
        var rangoFechas = $("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");
        var solicitudTecnica = $("#solicitud_tecnica").val();
        var rangoFechas = $("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form += '&column='+request.columns[ order.column ].name;
        form += '&dir='+order.dir;
        form += "&per_page="+request.length;
        form += "&page="+(request.start+request.length)/request.length;
        form += "&fecha_inicio="+rangoFechas[0];
        form += "&fecha_fin="+rangoFechas[1];
        if ( rangoFechas != '' ) {
            form += "&fecha="+rangoFechas;
        }
        if ( solicitudTecnica != '' ) {
            form += "&solicitudTecnica="+solicitudTecnica;
        }
        eventoCargaMostrar();
        axios.post('solicitudtecnicalogrec/listar',form).then(response => {
            callback(response.data);
            //console.log(response);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
   verEnvioLegado: function(trama){
        form2 = {trama:trama};       
        eventoCargaMostrar();
        axios.post('solicitudtecnicalogrec/mostarrespuesta',form2).then(response => {
            $("#trama").text(response.data.request);
            $('#ItemPopup').modal('show');
        }).catch(e => {            
            console.log("error");
        }).then(() => {  
            eventoCargaRemover();
        });
   }
}
