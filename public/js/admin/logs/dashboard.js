var sonido_alerta = "";
$(document).ready(function(){
    envioLegadoLog.listar_cabeceras(); 
    //sonido_alerta = document.getElementById("audio-alerta");
    $(".navbar-btn.sidebar-toggle").click();
    //envioLegadoLog.informacion();   
    setInterval(function(){ 
        envioLegadoLog.informacion();
        envioLegadoLog.listar_cabeceras();         
    }, 90000);

    last_update();
});

function last_update() {
    var tiempo = new Date();
    var hora = tiempo.getHours();
    var minuto = tiempo.getMinutes();
    var segundo = tiempo.getSeconds();

    str_hora = new String(hora);
    if (str_hora.length == 1) {
        hora = '0' + hora;
    }
    str_minuto = new String(minuto);
    if (str_minuto.length == 1) {
        minuto = '0' + minuto;
    }
    str_segundo = new String(segundo);
    if (str_segundo.length == 1) {
        segundo = '0' + segundo;
    }

    $("#last_update").text("("+hora+":"+minuto+":"+segundo+")");
}

mostrar = function(data){
    sonido_alerta.pause();
    sonido_alerta.currentTime = 0;
    alert(data);
    $("#cmsPsiLabel").text("("+data.fecha_cms_psi+")");
    $("#psiToaLabel").text("("+data.fecha_psi_toa+")");
    $("#psiCmsLabel").text("("+data.fecha_psi_cms+")");
    $("#toaPsiLabel").text("("+data.fecha_toa_psi+")");
    $("#psiCmsCierre").text("("+data.fecha_psi_cierre_cms+")");
    $("#cmsPsiCierre").text("("+data.fecha_cms_cierre_psi+")");
}

alert = function(data){
    var i = 0;
    $.each(data,function(key,value){
        a = new Date(value);
        b = new Date();    
        c = Math.ceil(Math.abs(b-a)/(1000 * 60));
        console.log(a);
        if(c > 15){
            i++;
            $("."+key).css('background','crimson');
        } else {
            $("."+key).css('background','green');
        }

        if (i > 0) {
            sonido_alerta.play();
        } else {
            sonido_alerta.pause();
            sonido_alerta.currentTime = 0;
        }
    });    
}

verEnvio = function (id) {      
    envioLegadoLog.verEnvioLegado(id);
}
