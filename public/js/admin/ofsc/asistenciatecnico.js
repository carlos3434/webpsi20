urlMapsBusqueda = "https://google.com/maps?q=";
totalTecnicos = 0;
rangoDecimalColor = 16777215;
coloresTecnicos = [];
objTables = {};
var markerSpiderfier;
$(document).ready(function(){
    Asistencia.getEstadosOfsc();
    slctGlobal.listarSlct('recursoofsc', 'slct_bucket_filtro', 'multiple');
    slctGlobal.listarSlct('recursoofsc', 'slct_bucket_filtro_opt', 'multiple');
	$("#btn-buscar").click(function(e){
		listar();
	});
    $("#descargar").click(descargarReporte);
    $("#txt_fecha_filtro_opt").datepicker({ format: 'yyyy-mm-dd', language : 'es' });
    $("#txt_fecha_filtro").daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });
    $(document).delegate(".listmarcamanual", "click", function(){
        var carnet = $(this).data("carnet");
        var bucket = $(this).data("bucket");
        if (typeof listTecnicos[bucket][carnet]!="undefined") {
            listTecnicos[bucket][carnet].asistio = $(this).val();
        }
    });
    $(document).delegate(".check_fila_bucket_sms", "change", function(){
        if ($(this).is(":checked")) {
            $("#slct_bucket_tecnico_"+$(this).val()+" option").attr("selected", "selected").prop('selected', true);
            $("#slct_bucket_tecnico_"+$(this).val()).multiselect('refresh');
            var tecnicosBucket = listTecnicos[listBucketChecked[$(this).val()]];
            for (var i in tecnicosBucket) {
                listTecnicosSms[$(this).val()][i] = tecnicosBucket[i];
            }
        } else {
            $("#slct_bucket_tecnico_"+$(this).val()+" option:selected").removeAttr('selected').prop('selected', false);
            $("#slct_bucket_tecnico_"+$(this).val()).multiselect('refresh');
            listTecnicosSms[$(this).val()] = [];
        }
    });
	$("#btn_asistencia").click(Asistencia.guardar);
    $("#form_sms").submit(enviarSmsMultiple);
    $(document).delegate(".btn-envio-sms", "click", function(e){
        enviarSms($(this).data("celular"), $(this).data("name"));
    });
    $(document).delegate(".btn-detalle-carnet", "click", function(e){
        detalleTecnico($(this).data("carnet"), $(this).data("bucket"));
    });
    $(document).delegate(".btn-detalle-agendas", "click", function(e){
        var tableid = $(this).data("idtable");
        var carnet = $(this).data("carnet");
        var bucket = $(this).data("bucket");
        var tr = $(this).parent().closest('tr');
        htmlrequerimientosTecnico(carnet, bucket, tableid, tr);

    });
    $(document).delegate(".btn-ubicacion-requerimiento", "click", function(e){
        var indicemarker = $(this).data("indicemarker");
        var tablaabuelo = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
        var indice = tablaabuelo.data("indice");
        var bucket = $(this).data("bucket");
        $("#asistenciatecnico_"+indice).removeClass("active");
        $("#mapatecnico_"+indice).addClass("active");
        $("#nav-modal-detalle-"+indice+" > li:nth-child(1)").removeClass("active");
        $("#nav-modal-detalle-"+indice+" > li:nth-child(2)").addClass("active");
        var tecnicos = {};
            var actividades = {};
            tecnicos[bucket] = listTecnicos[bucket];
            actividades[bucket] = listActividades[bucket];
            coloresTecnicos = [];
            Mapa.listColores(Object.keys(listTecnicos[bucket]).length);
            Mapa.draw("mapatecnicomodal_"+indice, tecnicos, actividades, indicemarker);
    });
    $(document).delegate(".btn-ubicacion-tecnico", "click", function(e){
        var indicemarker = $(this).data("indicemarker");
        var tablaabuelo = $(this).parent().parent().parent().parent();
        var indice = tablaabuelo.data("indice");
        var bucket = $(this).data("bucket");
        $("#asistenciatecnico_"+indice).removeClass("active");
        $("#mapatecnico_"+indice).addClass("active");
        $("#nav-modal-detalle-"+indice+" > li:nth-child(1)").removeClass("active");
        $("#nav-modal-detalle-"+indice+" > li:nth-child(2)").addClass("active");
        var tecnicos = {};
        var actividades = {};
        tecnicos[bucket] = listTecnicos[bucket];
        actividades[bucket] = listActividades[bucket];
        coloresTecnicos = [];
        Mapa.listColores(Object.keys(listTecnicos[bucket]).length);
        Mapa.draw("mapatecnicomodal_"+indice, tecnicos, actividades, indicemarker);
    });
    $('.nav-modal a').on('shown.bs.tab', function(e){
        if ($(this)[0].hash=='#mapa') {
            eventoCargaMostrar();
            coloresTecnicos = [];
            Mapa.listColores(totalTecnicos);
            Mapa.draw("mapamodal", listTecnicos, listActividades);
        }
    });
    $(document).on('shown.bs.tab', '.nav-modal-detalle a[data-toggle="tab"]', function (e) {
        var indice = $(this).data("indice");
        var bucket = listBucketChecked[indice];
        if ($(this)[0].hash=='#mapatecnico_'+indice) {
            var tecnicos = {};
            var actividades = {};
            tecnicos[bucket] = listTecnicos[bucket];
            actividades[bucket] = listActividades[bucket];
            coloresTecnicos = [];
            if (typeof listTecnicos[bucket]!="undefined" && typeof listTecnicos[bucket]!=undefined) {
               Mapa.listColores(Object.keys(listTecnicos[bucket]).length); 
            }
            Mapa.draw("mapatecnicomodal_"+indice, tecnicos, actividades);
        }
    });
});

htmlrequerimientosTecnico = function(carnet, bucket, tableid, tr) {
    var row = objTables[tableid].row( tr );
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('shown');
    } else {
        var htmlchild = "<table class='table table-bordered table-striped dataTable no-footer table-requerimiento-tecnico'>";
            htmlchild+="<thead>";
                htmlchild+="<tr>";
                    htmlchild+="<td>Req.</td>";
                    htmlchild+="<td>F. Agenda</td>";
                    htmlchild+="<td>Estado</td>";
                    htmlchild+="<td>[]</td>";
                htmlchild+="</tr>";
            htmlchild+="</thead>";
            htmlchild+="<tbody>";
                if (typeof listActividades[bucket]!="undefined" && typeof listActividades[bucket]!=undefined) {
                    var actividades = listActividades[bucket][carnet];
                    for (var k in actividades) {
                        var requerimiento = listActividades[bucket][carnet][k];
                        if (requerimiento.coordx!=0 && requerimiento.coordy!=0 && requerimiento.status!='deleted' 
                            && requerimiento.status!='cancelled' && requerimiento.date!='3000-01-01'){
                            htmlchild+="<tr>";
                                htmlchild+="<td>"+requerimiento.appt_number+"<span class='orden-requerimiento'>"+requerimiento.orden+"</span></td>";
                                htmlchild+="<td>"+requerimiento.start_time+"</td>";
                                htmlchild+="<td style='background: "+listEstadosOfsc[requerimiento.status].color+"; color: ";
                                htmlchild+=listEstadosOfsc[requerimiento.status].color_letra+" '>";
                                htmlchild+=requerimiento.status+"</td>";
                                htmlchild+="<td>";
                                htmlchild+="<a data-indicemarker='R_"+requerimiento.appt_number+"' ";
                                htmlchild+=" data-bucket='"+bucket+"' ";
                                htmlchild+=" class='btn btn-danger btn-ubicacion-requerimiento'>";
                                htmlchild+="<i class='fa fa-map-marker'></i>";
                                htmlchild+="</a>";
                                htmlchild+="</td>";
                            htmlchild+="</tr>";
                        }
                    }
                } else {
                    htmlchild+="<tr><td colspan='4'>No hay requerimientos</td></tr>";
                }
                
            htmlchild+="</tbody>";
        htmlchild+="</table>";
        row.child(htmlchild).show();
        tr.addClass('shown');
    }
}

listar  = function () {
	$("#t_asistencia").DataTable().destroy();
	Asistencia.listar();
};
listar();
descargarReporte = function () {
    $("#form-filtro").attr("action", "asistenciatecnicoofsc/listar");
    $("#form-filtro").append("<input type='hidden' name='flagdownload' value='true'/>");
    $("#form-filtro").append("<input type='hidden' id='bucket-hidden' value='"+$("#slct_bucket_filtro").val()+"' name='bucket' />");
    $("#form-filtro").append("<input type='hidden' id='fecha-hidden' value='"+$("#txt_fecha_filtro").val()+"' name='fecha' />");
    $("#form-filtro").submit();
    $("#form-filtro").attr("action", "");
    $("#form-filtro").remove("flagdownload");
};
detalle = function() {
    var detalle = [];
    if (objasistencia != null && objasistencia != "null") {
        if (cantidadbucketschecked <= 1 && datafrom!="view") {
            if (typeof objasistencia.id!="undefined" && typeof objasistencia.id != undefined) {
                idasistencia = objasistencia.id;
                for(var j in objasistencia.detalle) {
                    var tecnico = objasistencia.detalle[j];
                    detalle[tecnico.carnet] = tecnico;
                }
            } 
        }
        Modal.drawModal(detalle);
    } else {
        Modal.drawModal([]);
    } 
};
enviarSms = function(celular, nombretecnico) {
    if (celular==""){
        Psi.sweetAlertError("El tecnico No tiene un celular asignado");
        return;
    }
    bootbox.prompt({
        title: "Enviar Sms Tecnico :"+ nombretecnico, 
        inputType: 'textarea',
        callback : function(result){
            if (result) {
                var celulares = [];
                celulares.push(celular);
                Asistencia.enviarSms(celulares, result);
            }
        }
    });
};
enviarSmsMultiple = function() {
    var celulares = [];
    var mensaje = $("#txt_mensaje_texto").val();
    for (var i in listTecnicosSms) {
        for (var j in listTecnicosSms[i]) {
            celulares.push(listTecnicosSms[i][j]);
        }
    }
    Asistencia.enviarSms(celulares, mensaje);
    return false;
}
detalleTecnico = function(carnet, bucket){
    var tecnico = listTecnicos[bucket][carnet];
    var html = "";
    html+="<b>"+tecnico.name+"</b>";
    html+="<div class='row'>";
    var datos = tecnico.datos;
    if (datos!=null && datos!="null") {
        html+="<div class='col-md-4'><b>Nombres</b></div>";
        html+="<div class='col-md-8'>"+datos.nombres+"</div>";
        html+="<div class='col-md-4'><b>A. Paterno</b></div>";
        html+="<div class='col-md-8'>"+datos.ape_paterno+"</div>";
        html+="<div class='col-md-4'><b>Celular PSI :</b></div>";
        html+="<div class='col-md-8'>"+datos.celular+"</div>";
        html+="<div class='col-md-4'><b>Celular TOA</b></div>";
        html+="<div class='col-md-8'>"+tecnico.phone+"</div>";
        html+="<div class='col-md-4'><b>DNI</b></div>";
        html+="<div class='col-md-8'>"+datos.dni+"</div>";
    }
    html+="</div>";
    bootbox.alert({
        message: html,
        backdrop: true
    });
};