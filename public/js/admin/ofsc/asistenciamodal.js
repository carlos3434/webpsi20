Modal = {
	drawModal : function(detalle) {		
	    var htmlmodal = "";
	    var bucket = $("#slct_bucket_filtro_opt").val();
	    if (idasistencia == 0) {
	        if (cantidadbucketschecked <=1 && datafrom!="view") {
	            $(".modal-title").html("Registro de Asistencia de Bucket : "+bucket);
	            $("#btn_asistencia").html("Registrar Asistencia");
	        } else {
	            $(".modal-title").html("Vista de Tecnicos de Bucket : "+bucket);
	            $("#btn_asistencia").css("display", "none");
	        }
	    } else {
	        if (cantidadbucketschecked <= 1 && datafrom!="view") {
	            bucket = objasistencia.bucket;
	            $(".modal-title").html("Edicion de Asistencia de Bucket : "+bucket);
	            $("#btn_asistencia").html("Actualizar Asistencia");
	        } else {
	        	bucket = objasistencia.bucket;
	            $(".modal-title").html("Vista de Tecnicos de Buckets : "+bucket);
	            $("#btn_asistencia").html("Actualizar Asistencia");
	            $("#btn_asistencia").css("display", "none");
	        }
	    }
	    htmlmodal ="<div class='panel-group' style='margin-bottom: 0px;'>";
	    htmlmodal+="</div>";
	    $("#contenedor-registro-asistencia").html(htmlmodal);
	        for (var i in listBucketChecked) {	        	
	            var htmlcontenedor = $("#contenedor-registro-asistencia").html();
	            htmlmodal="<div class='panel panel-default'>";

	                htmlmodal+="<div class='panel-heading box box-primary' style='margin-bottom: 0px;'>";
	                htmlmodal+="<h4 class='panel-title'>";
	                htmlmodal+="<a class='accordion-toggle' data-toggle='collapse' data-parent='#' ";
	                htmlmodal+=" href='#detalle_"+i+"' id='panel_"+i+"'>"+listBucketChecked[i]+"</a>";
	                htmlmodal+="</h4>";
	                htmlmodal+="</div>";

	                htmltable = "<div class='table-responsive'>";
	                var tableid = "t_asistencia_tecnico_"+i;
	                htmltable+= "<table id='"+tableid+"' class='table table-bordered table-striped table-bucket' data-indice='"+i+"'>";
	                    htmltable+="<thead>";
	                    htmltable+="<tr>";
	                    htmltable+="<th style='width: 150px;'>Carnet</th>";
	                    htmltable+="<th style='width: 50px;'>OT</th>";
	                    htmltable+="<th style='width: 50px;'>TOA</th>";
	                    if (datafrom != "view" && cantidadbucketschecked<=1) {
	                        htmltable+="<th>¿Asistio?</th>";
	                    }
	                    htmltable+="<th>[]</th>";
	                    htmltable+="</tr>";
	                    htmltable+="</thead>";
	                    htmltable+="<tbody class='tbody-asistencia-ofsc'>";
	                    htmltable+="</tbody>";
	                htmltable+= "</table>";
	                htmltable+= "</div>";

	                htmlmodal+="<div id='detalle_"+i+"' class='panel-collapse collapse'>";
	                
	                htmlmodal+="<ul class='nav nav-tabs nav-modal-detalle' id='nav-modal-detalle-"+i+"'>";
	                htmlmodal+="<li class='active'>";
	                htmlmodal+="<a href='#asistenciatecnico_"+i+"' data-toggle='tab'>Tecnicos</a>";
	                htmlmodal+="</li>";
	                htmlmodal+="<li>";
	                htmlmodal+="<a href='#mapatecnico_"+i+"' data-indice = '"+i+"' data-toggle='tab'>Mapa de Bucket</a>";
	                htmlmodal+="</li>";
	                htmlmodal+="</ul>";
	                htmlmodal+="<div class='tab-content '>";
	                htmlmodal+="<div class='tab-pane active' id='asistenciatecnico_"+i+"'>";
	                htmlmodal+="<div class='box'>";
	                htmlmodal+="<div class='row'>";
	                htmlmodal+="<div class='form-group'>";
	                htmlmodal+=htmltable;
	                htmlmodal+="</div>";
	                htmlmodal+="</div>";
	                htmlmodal+="</div>";
	                htmlmodal+="</div>";
	                htmlmodal+="<div class='tab-pane' id='mapatecnico_"+i+"'>";
	                htmlmodal+="<div id='mapatecnicomodal_"+i+"' style='height: 400px;'>";
	                            
	                htmlmodal+="</div>";
	                htmlmodal+="</div>";
	                htmlmodal+="</div>";
	            htmlmodal+="</div>";
	            
	            htmlmodal+="</div>";
	            $("#contenedor-registro-asistencia .panel-group").append(htmlmodal);

	                
	                var tecnicosBuckets = listTecnicos[listBucketChecked[i]];
	                var contadorfila = 0;
	                for (var j in tecnicosBuckets) {	                	
	                    var tecnico = tecnicosBuckets[j];
	                    var htmlot = "";
	                    var htmltoa = "";
	                    var htmlopciones = "";
	                    var selectmarca = "";
	                    var color = "#9FA8DA";
	                    var colortoa = "#9FA8DA";
	                    var colorot = "#9FA8DA";
	                    if (tecnico.estado == 1) {
	                        color = "#A5D6A7";
	                    }
	                    if (tecnico.estado_toa == 1) {
	                        colortoa = "#A5D6A7";
	                    }
	                    if (tecnico.estado_ot == 1) {
	                        colorot = "#A5D6A7";
	                    }
	                    if (tecnico.estado == 2) {
	                        color = "#FFCC80";
	                    }
	                    if (tecnico.estado_toa == 2) {
	                        colortoa = "#FFCC80";
	                    }
	                    if (tecnico.estado_ot == 2) {
	                        colorot = "#FFCC80";
	                    }
	                    if (tecnico.estado == 3) {
	                        color = "#EF9A9A";
	                    }
	                    if (tecnico.estado_toa == 3) {
	                        colortoa = "#EF9A9A";
	                    }
	                    if (tecnico.estado_ot == 3) {
	                        colorot = "#EF9A9A";
	                    }
	                    if (tecnico.estado == 4) {
	                        color = "#9FA8DA";
	                    }
	                    if (tecnico.estado_toa == 4) {
	                        colortoa = "#9FA8DA";
	                    }
	                    if (tecnico.estado_ot == 4) {
	                        colorot = "#9FA8DA";
	                    }
	                    if (typeof tecnico.timeOt!="undefined" && typeof tecnico.timeOt!=undefined) {
	                        if (moment(tecnico.timeOt).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
	                        	htmlot+="<label style='text-align: center; width: 100%;' class='label-tiempo'>"+moment(tecnico.timeOt).format('HH:mm:ss')+"</label>";
	                        	htmlot+="<a data-bucket='"+listBucketChecked[i]+"' class='btn btn-danger btn-ubicacion-tecnico' style='display:block' ";
		                        htmlot+=" data-indicemarker='T_"+j+"' >";
		                        htmlot+="<i class='fa fa-map-marker'></i></a>";
	                        }
	                    }
	                    
	                    if (typeof tecnico.timeToa!="undefined" && typeof tecnico.timeToa!=undefined) {
	                        if (moment(tecnico.timeToa).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
	                        	htmltoa+="<label style='text-align: center; width: 100%;' class='label-tiempo'>"+moment(tecnico.timeToa).format('HH:mm:ss')+"</label>";
	                        	htmltoa+="<a class='btn btn-danger btn-ubicacion-tecnico' style='display:block' data-bucket='"+listBucketChecked[i]+"' ";
		                        htmltoa+=" data-indicemarker='T_"+j+"' >";
		                        htmltoa+="<i class='fa fa-map-marker'></i></a>";
	                        }
	                    }
	                    selectmarca+="<label>  </label>";
	                    var checkedok = "";
	                    var checkednook = "";
	                    var checkvacaciones = "";
	                    var checkpermiso = "";
	                    if (typeof detalle[j] != "undefined" && detalle[j].asistio !="null" && detalle[j].asistio!=null) {
	                        checkedok = " checked='checked' ";
	                        listTecnicos[listBucketChecked[i]][j].asistio = detalle[j].asistio;
	                        if (detalle[j].asistio == 0) {
	                            checkednook = " checked='checked' ";
	                        }
	                        if (detalle[j].asistio == 1) {
	                            checkedok = " checked='checked' ";
	                        }
	                        if (detalle[j].asistio == 2) {
	                            checkvacaciones = " checked='checked' ";
	                        }
	                        if (detalle[j].asistio == 3) {
	                            checkpermiso = " checked='checked' ";
	                        }
	                    }
	                    if (typeof detalle[j] == undefined || typeof detalle[j] == "undefined") {
	                        checkedok = " checked='checked' ";
	                        listTecnicos[listBucketChecked[i]][j].asistio = 0;
	                    } else {
	                        if (typeof detalle[j].asistio == undefined || typeof detalle[j].asistio == "undefined") {
	                            checkedok = " checked='checked' ";
	                            listTecnicos[listBucketChecked[i]][j].asistio = 0;
	                        } else if (detalle[j].asistio == 0 || detalle[j].asistio == null || detalle[j].asistio == "null") {
	                            checkedok = " checked='checked' ";
	                            listTecnicos[listBucketChecked[i]][j].asistio = 0;
	                        }
	                    }
	                    selectmarca+="<div class=''>";

	                        selectmarca+="<div><input type='radio' name='asistencia["+j+"]' data-carnet='"+j+"' ";
	                        selectmarca+=" data-bucket='"+listBucketChecked[i]+"' "+checkedok+" ";
	                        selectmarca+=" class='listmarcamanual optmarca_"+j+" ' id='marcamanual_ok_"+j+"' value='1' />";
	                        selectmarca+="<span>Si</span></div>";
	                        
	                        selectmarca+="<div><input type='radio' name='asistencia["+j+"]' data-carnet='"+j+"' ";
	                        selectmarca+=" data-bucket='"+listBucketChecked[i]+"' "+checkednook+" ";
	                        selectmarca+="class='listmarcamanual optmarca_"+j+" ' id='marcamanual_nook_"+j+"' value='0' />";
	                        selectmarca+="<span>No</span></div>";

	                        selectmarca+="<div><input type='radio' name='asistencia["+j+"]' data-carnet='"+j+"' ";
	                        selectmarca+=" data-bucket='"+listBucketChecked[i]+"' "+checkvacaciones+" ";
	                        selectmarca+="class='listmarcamanual optmarca_"+j+" ' id='marcamanual_nook_"+j+"' value='2' />";
	                        selectmarca+="<span>Vacaciones</span></div>";

	                        selectmarca+="<div><input type='radio' name='asistencia["+j+"]' data-carnet='"+j+"' ";
	                        selectmarca+=" data-bucket='"+listBucketChecked[i]+"' "+checkpermiso+" ";
	                        selectmarca+="class='listmarcamanual optmarca_"+j+" ' id='marcamanual_nook_"+j+"' value='3' />";
	                        selectmarca+="<span>Permiso</span></div>";

	                    selectmarca+="</div>";
	                    
	                    htmlopciones+="<a class='btn-xs btn-envio-sms btn-success' data-celular='"+tecnico.phone+"' ";
	                    htmlopciones+=" data-name='"+tecnico.name+"'>";
	                    htmlopciones+= "<i class='fa fa-envelope-o' ></i></a>";
	                    htmlopciones+= "<a class='btn-xs btn-detalle-carnet btn-warning' ";
	                    htmlopciones+=" data-carnet='"+j+"' data-bucket='"+listBucketChecked[i]+"'>";
	                    htmlopciones+="<i class='fa fa-male' ></i></a>";
	                    htmlopciones+= "<a class='btn-xs btn-detalle-agendas btn-danger' ";
	                    htmlopciones+=" data-carnet='"+j+"' data-bucket='"+listBucketChecked[i]+"' ";
	                    htmlopciones+=" data-idtable='"+tableid+"' >";
	                    var numagendas = 0;	                    
	                    var con=0;	                    
	                    if (typeof listActividades[listBucketChecked[i]]!="undefined" && typeof listActividades[listBucketChecked[i]]!=undefined) {
	                    	if (typeof listActividades[listBucketChecked[i]][j] !="undefined" && typeof listActividades[listBucketChecked[i]][j] != undefined) {
	                    		con=0;
	                    		for (var z in listActividades[listBucketChecked[i]][j] ) {	                    			
	                    			if (listActividades[listBucketChecked[i]][j][z].status != "cancelled" && listActividades[listBucketChecked[i]][j][z].status!='deleted' 
		                    			 && listActividades[listBucketChecked[i]][j][z].date!='3000-01-01'){	                    				
	                    				con ++;	                    				
	                    			}
	                    		}                   			                    			                    		
	                    		var numagendas = Object.keys(listActividades[listBucketChecked[i]][j]).length;
	                    	}
	                    }
	                    htmlopciones+="<i class='fa' >"+con+"</i></a>";

	                    if (datafrom == "view" || cantidadbucketschecked >1) {
	                        var htmlfila = "<tr>";
	                            htmlfila+="<td>"+tecnico.name+"</td>";
	                            htmlfila+="<td>"+htmlot+"</td>";
	                            htmlfila+="<td>"+htmltoa+"</td>";
	                            htmlfila+="<td>"+htmlopciones+"</td>";
	                        htmlfila+="</tr>";
	                    } else {
	                        var htmlfila = "<tr>";
	                            htmlfila+="<td>"+tecnico.name+"</td>";
	                            htmlfila+="<td>"+htmlot+"</td>";
	                            htmlfila+="<td>"+htmltoa+"</td>";
	                            htmlfila+="<td>"+selectmarca+"</td>";
	                            htmlfila+="<td>"+htmlopciones+"</td>";
	                        htmlfila+="</tr>";
	                    }
	                    $("#"+tableid+" tbody").append(htmlfila);
	                    $("#"+tableid+" tbody tr:eq("+contadorfila+") td:eq(1)").css("background", colorot);
	                    $("#"+tableid+" tbody tr:eq("+contadorfila+") td:eq(1)").addClass("fondo_estado");
	                    $("#"+tableid+" tbody tr:eq("+contadorfila+") td:eq(2)").css("background", colortoa);
	                    contadorfila++;
	                    totalTecnicos++;

	                }
	                var tableelement = $("#"+tableid).DataTable({
	                        "paging": false,
	                    });
	                
	                objTables[tableid] = tableelement;
	        }
	    Mapa.listColores(totalTecnicos);
	    Modal.drawBucketsSms();
	    eventoCargaRemover();
	},
	drawBucketsSms : function() {
	    var html = "";
	    for (var i in listBucketChecked) {
	        var tecnicosBucket = listTecnicos[listBucketChecked[i]];
	        var numtecnicos = 0;
	        var htmlselect = "<select name='slct_bucket_tecnico[]' id='slct_bucket_tecnico_"+i+"' data-fila='"+i+"' multiple>";
	        listTecnicosSms[i]  = {};
	        for(var j in tecnicosBucket) {
	            htmlselect+="<option value='"+j+"' data-fila='"+i+"' >"+tecnicosBucket[j].name+"</option>";
	            numtecnicos++;
	            listTecnicosSms[i][j] = tecnicosBucket[j].phone;
	        }
	        htmlselect+="</select>";
	        if (numtecnicos > 0) {
	            html+="<tr>";
	                html+="<td><input type='checkbox' checked value='"+i+"' style='margin-right: 10px;' ";
	                html+=" class='check_fila_bucket_sms' id='check_fila_bucket_sms_"+i+"'/><b>"+listBucketChecked[i]+"</b></td>",
	                html+="<td>"+htmlselect+"</td>";
	            html+="</tr>";
	        }
	        
	    }
	    $("#tb_sms_tecnicos").html(html);
	    for (var i in listBucketChecked) {
	        var dataseleccionados = [];
	        var tecnicosBucket = listTecnicos[listBucketChecked[i]];
	        for (var j in tecnicosBucket) {
	            dataseleccionados.push(j);
	        }
	        slctGlobalHtml("slct_bucket_tecnico_"+i, 'multiple', dataseleccionados, null, null, null, null, null, funcionesBucketTecnicos);
	    }
	    $("#t_sms_tecnicos").DataTable({"pageLength": 5});

	}
};
funcionesBucketTecnicos = {
    change : function(value, checked, option) {
        var fila = option.data("fila");
        var bucketSms = listTecnicosSms[fila];
        if (typeof bucketSms!="undefined" && typeof bucketSms!=undefined) {
            if (checked) {
                listTecnicosSms[fila][value] = listTecnicos[listBucketChecked[fila]][value].phone;
            } else {
                listTecnicosSms[fila][value] = "";
                $("#check_fila_bucket_sms_"+fila).removeAttr("checked");
            }
        }
    },
    selectedall : function(select) {
        if (typeof select[0]!="undefined" && typeof select[0]!=undefined) {
            var fila = select[0].fila;
            var bucketTecnicos = listTecnicos[listBucketChecked[fila]];
            for (var i in bucketTecnicos) {
                listTecnicosSms[fila][i] = bucketTecnicos[i].phone;
            }
            $("#check_fila_bucket_sms_"+fila).prop("checked", true);
        }
    }
};
var enforceModalFocusFn;
$('#asistenciaModal').on('show.bs.modal', function (event) {
    enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    var button = $(event.relatedTarget);
    var id = button.data("id");
    var j = 0;
    var bucket = $("#slct_bucket_filtro_opt").val();
    $("#slct_bucket_filtro_opt option:selected").each(function(e){
        listBucketChecked.push($(this).val());
        j++;
        cantidadbucketschecked++;
    });
    datafrom = button.data("from");
    if (id > 0) {
    	idasistencia = id;
        listBucketChecked = [];
    	Asistencia.get(id);
    } else {
        if (j == 0) {
            Psi.sweetAlertError("Elija por lo menos un bucket!!!");
            return false;
        }
        if (datafrom == "view") {
            $("#btn_asistencia").css("display", "none");
        }
        Asistencia.get();
    }
    listTecnicos = [];
});
$('#asistenciaModal').on('hide.bs.modal', function (event) {
    $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    var modal = $(this);
    modal.find('.modal-body input[type=text]').val('');
    modal.find('.modal-body textarea').val('');
    $("#div_tipo_devolucion").css("display", "none");
    modal.find(".modal-title").text("Registro de Asistencia");
    idasistencia = 0;
    objasistencia = null;
    $("#btn_asistencia").css("display", "inline-block");
    $("#contenedor-registro-asistencia table.table-bucket").DataTable().clear().draw();
    $("#contenedor-registro-asistencia table.table-bucket").DataTable().destroy();
    $("#contenedor-registro-asistencia").html("");
    datafrom = "";
    cantidadbucketschecked = 0;
    listBucketChecked = [];
    coloresTecnicos = [];
    listActividades = [];
    listRutas = {};
    directionsDisplay = [];
    totalTecnicos = 0;
    Mapa.removeMarkers();
    $(".nav-modal li").removeClass("active");
    $(".nav-modal li:eq(0)").addClass("active");
    $("#asistencia, #mapa").removeClass("active");
    $("#asistencia").addClass("active");
    $("#t_sms_tecnicos").DataTable().clear().draw();
    $("#t_sms_tecnicos").DataTable().destroy();
    map = null;
});