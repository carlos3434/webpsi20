axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common.csrftoken = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
listTecnicos = [];
listTecnicosSms = [];
listActividades = [];
listBucketChecked = [];
listEstadosOfsc = [];
objasistencia = null;
datafrom = "";
cantidadbucketschecked = 0;
idasistencia = 0;
Asistencia = {
	listar : function () {
		var actividad_id = "";
		var tipo_legado = "";
	    return $("#t_asistencia").DataTable({
	        "processing": true,
	        "serverSide": true,
	        "stateSave": true,
	        "stateLoadCallback": function (settings) {
	        },
	        "stateSaveCallback": function (settings) { // Cuando finaliza el ajax
	        },
	        ajax: function(data, callback, settings) {
	        	data.actividad_id = actividad_id;
	        	data.tipo_legado = tipo_legado;
	            Asistencia.ajaxlistado(data,callback);
	        },
	        "columns":[
	            { data: 'id', name:'id'},
	            { data: 'created_at', name:'created_at'},
	            { data: 'bucket', name:'bucket'},
	            { data: 'totaltecnicos', name:'totaltecnicos'},
	            { data: 'totalasistio', name:'totalasistio'},
	            { data: 'totalausente', name:'totalausente'},
	            { data: 'totalpermiso', name:'totalpermiso'},
	            { data: 'totalvacaciones', name:'totalvaciones'},
	            { data: 'usuario', name:'usuario'},
	            {data : function( row, type, val, meta) {
	                var htmlButtons = "";
	                htmlButtons = "<a class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' title='Detalle Asistencia' ";
	                htmlButtons+=" data-target='#asistenciaModal' data-id='"+row.id+"' data-tipolegado='"+row.tipo_legado+"'";
	                htmlButtons+=" data-tipo='"+row.tipo+"' data-parent='"+row.parent+"'";
	                htmlButtons+=" data-actividadid='"+row.actividad_id+"'>";
	                htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
	                return htmlButtons;
	            }, name: "botones"},
	        ],
	        paging: true,
	        lengthChange: true,
	        searching: true,
	        ordering: true,
	        order: [[ 1, "desc" ]],
	        info: true,
	        autoWidth: true,
	        language: config
	    });
	},
	ajaxlistado : function(request,callback){
        var order = request.order[0];
        form=$("#form-filtro").serialize().split('txt_').join("").split('slct_').join("");
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+='&search='+request.search.value;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        eventoCargaMostrar();
        axios.post('asistenciatecnicoofsc/listar', form).then(response => {
            callback(response.data);
        }).catch(e => {
        }).then(() => {
            eventoCargaRemover();
        });
    },
    get : function() {
    	var request = {
    		_token 		: document.querySelector('#token').getAttribute('value'),
    		fecha 		: $("#txt_fecha_filtro_opt").val(),
    		bucket 		: listBucketChecked,
    		id          : idasistencia
    	};
    	axios.post('asistenciatecnicoofsc/editar', request).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
	        	$('#asistenciaModal').modal("hide");
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		   		objasistencia = data.obj;
		   		if (idasistencia > 0) {
		   			listBucketChecked.push(objasistencia.bucket);
		   			if (objasistencia.fecha!=moment().format("YYYY-MM-DD")) {
		   				datafrom = "view";
		   			}
		   		}
		    	Asistencia.getTecnicos();
		    	//listar();
		   	}
		   	listActividades = data.actividades;
	    }).catch(e => {
	    	console.log(e);
	    	$('#asistenciaModal').modal("hide");
	        Psi.sweetAlertError("Error al Intentar consultar Detalle");
	        eventoCargaRemover();
	    }).then(() => {
	        
	    });
    },
    getTecnicos : function() {
    	eventoCargaMostrar();
    	var bucket = $("#slct_bucket_filtro_opt").val();
    	if (bucket =="") {
    		if (idasistencia > 0) {
    			bucket = objasistencia.bucket;
    		}
    	} else {
    		if (idasistencia > 0) {
    			bucket = objasistencia.bucket;
    		}
    	}
    	//return false;
    	axios.post('tecnicosofsc/listadotecnico', {buckets : listBucketChecked}).then(response => {
	   		listTecnicos = response.data;
	   		detalle();
	    }).catch(e => {
	    	$('#asistenciaModal').modal("hide");
	        Psi.sweetAlertError("Error al Consultar!!!");
	    }).then(() => {
	         eventoCargaRemover();
	    });
    },
    guardar : function() {
    	var request = {
    		_token 		: document.querySelector('#token').getAttribute('value'),
    		asistencias : listTecnicos,
    		bucket 		: listBucketChecked,
    		id          : idasistencia
    	};
    	axios.post('asistenciatecnicoofsc/guardar', request).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		    	//tematico[data.obj.id] = data.obj;
		    	idasistencia = data.obj.id;
		    	Psi.sweetAlertConfirm(data.msj);
		    	$("#asistenciaModal").modal("hide");
		    	listar();
		   	}
	    }).catch(e => {
	    	$('#asistenciaModal').modal("hide");
	        Psi.sweetAlertError("Error al Intentar Guardar");
	    }).then(() => {
	        eventoCargaRemover();
	    });
    },
    enviarSms : function(celulares, mensaje) {
    	if (celulares!="") {
    		var request = {
	    		_token 					: document.querySelector('#token').getAttribute('value'),
	    		numeros_telefonicos 	: celulares.join(","),
	    		contenido_sms 		    : mensaje+","
	    	};

	    	axios.post('bandejalegado/enviarsms', request).then(response => {
	       	var data = response.data;
		        if (data.rst == "2") {
			    	Psi.sweetAlertError(data.msj);
			   	} else {
			    	Psi.sweetAlertConfirm(data.msj);
			   	}
		    }).catch(e => {
		        Psi.sweetAlertError("Error al Intentar consultar Detalle");
		        eventoCargaRemover();
		    }).then(() => {
		        
		    });
    	}
    },
    getEstadosOfsc : function() {
    	var request = {
    		_token 		: document.querySelector('#token').getAttribute('value')
    	};
    	axios.post('estadoofsc/listarbyname', request).then(response => {
	       	var data = response.data;
	        listEstadosOfsc = data.datos;
	    }).catch(e => {
	    	$('#asistenciaModal').modal("hide");
	        Psi.sweetAlertError("Error al Intentar consultar Detalle");
	        eventoCargaRemover();
	    }).then(() => {
	        
	    });
    },
}