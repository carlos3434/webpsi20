directionsDisplay = [];
gm = google.maps;
map = null;
bounds = null;
infoWindows = [];
markers = [];
config = {
    el: 'mapafftts',
    lat: -12.109129,
    lon: -77.016123,
    zoom: 15,
    minZoom: 15,
    type: gm.MapTypeId.ROADMAP
};
spiderConfig = {
    keepSpiderfied: true,
    event: 'click'
};
mapOptions = {
    center: new gm.LatLng(config.lat, config.lon),
    zoom: config.zoom,
    mapTypeId: config.type
};
directionsService= new google.maps.DirectionsService;
listRutas = {};
flightPaths = {};
Mapa = {
	addMarker : function(propiedades){
	    var location = new gm.LatLng(propiedades.coordy, propiedades.coordx);
	    var marker = new MarkerWithLabel({
	        position: location,
	        icon: propiedades.icon,
	        map: map,
	        labelContent: propiedades.labelContent
	    });
	    if (typeof propiedades.indice == "undefined" 
	    	|| typeof propiedades.indice == undefined) {
	        markers.push(marker);
	    } else {
	        markers[propiedades.indice] = marker;
	    }
	    
	    bounds.extend(location);
	    marker.infowindow = new gm.InfoWindow({content: propiedades.label});

	    gm.event.addListener(marker,'click', function() {
	        if(infoWindows.length>0){
	            for (var j=0;j<infoWindows.length;j++) {
	                infoWindows[j].close();
	            }
	        }
	        marker.infowindow.open(map,marker);
	        infoWindows.push(marker.infowindow);
	    });
	    markerSpiderfier.addMarker(marker);
	},
	removeMarkers : function() {
	    for(var i in markers) {
	    	markers[i].setMap(null);
	    }
	    markers=[];
	},
	addRoute: function(rutasMapa, color) {

		var rutas = [];
		for (var i in rutasMapa) {
			if (i!="numero") {
				rutas.push(rutasMapa[i]);
			}
		}
		var final=rutas.length -1;
		rutasMapa.numero = rutas.length;
        directionsDisplay[rutasMapa.numero] = new google.maps.DirectionsRenderer;
        directionsDisplay[rutasMapa.numero].setMap(map);
            var waypts = [];
            
            for (var i in rutas) {
                waypts.push(
                    {
                      location: rutas[i],
                      stopover: true
                    }
                 );
            }
            var request = {
                origin: rutas[0],
                destination: rutas[final],
                waypoints: waypts,
                optimizeWaypoints: false,
                travelMode: google.maps.TravelMode.DRIVING
            };
        directionsService.route(
            request, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay[rutasMapa.numero].setDirections(response);
                    var lineSymbol = {
                      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                      strokeOpacity: 1,
                      scale: 2
                    };
                directionsDisplay[rutasMapa.numero].setOptions(
                        {
                        suppressMarkers: true,
                        polylineOptions: {
                            strokeWeight: 4,
                            strokeOpacity: 0,
                            strokeColor: rutas[final].color,
                            icons: [{
                                icon: lineSymbol,
                                offset: '0',
                                repeat: '20px'
                            }],
                        }
                        }
                    );
            } else {
                //Psi.sweetAlertError("Solicitud de direcciones suspendida debido a" + status);
                }
            }
        ); 
    },
     addLine: function(rutasLinea, color) {
     	var rutas = [];
     	for(var i in rutasLinea) {
     		if (i!="numero") {
     			rutas.push(rutasLinea[i]);
     		}
     	}
        flightPath = new google.maps.Polyline(
            {
                path: rutas,
                geodesic: true,
                strokeColor: color,
                strokeOpacity: 1.0,
                strokeWeight: 2
            }
        );
       	flightPath.setMap(map);
    },
	draw : function(idmapa, tecnicos, actividades, indicemarkercentrar) {
	    Mapa.removeMarkers();
	    map = new gm.Map(
	        document.getElementById(idmapa),
	        mapOptions
	    );
	    markerSpiderfier = new OverlappingMarkerSpiderfier(map, spiderConfig);
	    bounds = new gm.LatLngBounds();

	    var contador = 0;
	    for (var i in tecnicos) {
	        var bucketTecnico = tecnicos[i];
	        for(var j in bucketTecnico) {
	            var tecnico = bucketTecnico[j];
	            var color = coloresTecnicos[contador];
	            var labeltext = "<b>Bucket : </b>"+i;
	            labeltext+= "<br><b>Tecnico : </b>"+tecnico.name;
	            labeltext+= "<br><b>U. Ubicacion : </b>"+tecnico.time;
	            labeltext+= "<br><b>Carnet : </b>"+j;
	            labeltext+= "<br><b>Fuente : </b>"+tecnico.fuente;
	            iconTecnico = {
	                path: fontawesome.markers.MALE,
	                scale: 0.6,
	                strokeWeight: 0.3,
	                strokeColor: 'black',
	                strokeOpacity: 1,
	                fillColor: "#"+color,
	                fillOpacity: 1
	            };
	            var propiedades = {
	            	coordx : tecnico.coord_x,
	            	coordy : tecnico.coord_y,
	            	indice : "T_"+j,
	            	icon   : iconTecnico,
	            	label  : labeltext
	            };
	            Mapa.addMarker(propiedades);
	            tecnicos[i][j].color = color;
	            contador++;
	        }
	    }
	    var contador = 0;
	    if (typeof actividades !="undefined" && typeof actividades !=undefined) {
	        for (var i in actividades) {
	        	if (typeof listRutas[i] == "undefined" || typeof listRutas[i] == undefined) {
	        		listRutas[i] = {};
	        	}
	            for (var j in actividades[i]) {
	                var actividadagrupadas = actividades[i][j];
	                if (typeof listRutas[i][j] == "undefined" || typeof listRutas[i][j] == undefined) {
		        		listRutas[i][j] = {};
		        	}
		        	var cont = 0;
		        	var color = "000000";
	                for (var k in actividadagrupadas) {
	                    var actividad = actividadagrupadas[k];	                    
	                    if (actividad.coordx!=0 && actividad.coordy!=0 && actividad.status!='deleted' 
		                    && actividad.status!='cancelled' && actividad.date!='3000-01-01') {
		                    
		                    var labeltext = "<b>Requerimiento : </b>"+actividad.appt_number;
		                    
		                    labeltext+= "<br><b>Cliente : </b>"+actividad.name;
		                    labeltext+= "<br><b>Direccion : </b>"+actividad.address;
		                    labeltext+= "<br><b>Tecnico : </b>"+actividad.resource_id;
		                    labeltext+= "<br><b>Estado : </b>"+listEstadosOfsc[actividad.status].nombre;
		                    iconActividad = {
		                        path: fontawesome.markers.CALENDAR,
		                        scale: 0.6,
		                        strokeWeight: 0.3,
		                        strokeColor: 'black',
		                        strokeOpacity: 1,
		                        fillColor: "#"+color,
		                        fillOpacity: 1
		                    };
		                    if (typeof tecnicos[i]!="undefined" && typeof tecnicos[i]!=undefined) {
		                    	if (typeof tecnicos[i][j] !="undefined" && typeof tecnicos[i][j]!=undefined) {
		                    		iconActividad.fillColor =  "#"+tecnicos[i][j].color;
		                    		color = "#"+tecnicos[i][j].color;
		                    	}
		                    }
		                    var propiedades = {
				            	coordx : actividad.coordx,
				            	coordy : actividad.coordy,
				            	indice : "R_"+actividad.appt_number,
				            	icon   : iconActividad,
				            	label  : labeltext,
				            	labelContent : "<div class='orden-requerimiento-mapa'"
				            };
				            propiedades.labelContent+=" style='background-color: "+listEstadosOfsc[actividad.status].color+" !important; color:"+listEstadosOfsc[actividad.status].color_letra+" '>";
				            propiedades.labelContent+=actividad.orden+"</div>";
				            var tipo = actividad.XA_APPOINTMENT_SCHEDULER == 'TEL' ? 'SLA' : 'Ag';
				            propiedades.labelContent+="<div style='background: #fff; padding: 5px; font-weight: 700;'><i style='color: blue;'>"+tipo+"</i> - "+actividad.appt_number+"</div>";

		                    Mapa.addMarker(propiedades);
		                	
		                	listRutas[i][j][cont] = {
		                		'lng':parseFloat(actividad.coordx),
	                            'lat':parseFloat(actividad.coordy),
	                            'orden': actividad.orden,
	                            'color': color
	                        }
	                        if (typeof tecnicos[i]!="undefined" && typeof tecnicos[i]!=undefined) {
		                    	if (typeof tecnicos[i][j] !="undefined" && typeof tecnicos[i][j]!=undefined) {
		                    		listRutas[i][j][cont].color = "#"+tecnicos[i][j].color;
		                    	}
		                    }
	                        cont++;
	                      }
		                }
	                var rutasactividades = listRutas[i][j];
	                //Mapa.addRoute(rutasactividades, "#"+coloresTecnicos[contador]);
	                Mapa.addLine(rutasactividades, color);
	                contador++;
	            }
	        }
	    }
	    var markerCluster = new MarkerClusterer(map, markers);
	    markerCluster.setMaxZoom(config.minZoom);

	    map.fitBounds(bounds);
	    if (typeof indicemarkercentrar!="undefined" && typeof indicemarkercentrar!=undefined) {
	        var markercentrar = [];
	        markercentrar.push(markers[indicemarkercentrar]);
	        Mapa.centrarMarker(markercentrar);
	    }
	    eventoCargaRemover();
	},
	centrarMarker : function(markersseleccionados) {
	    boundstmp = new gm.LatLngBounds();
	    for (var i in markersseleccionados) {
	        var latLng = markersseleccionados[i].getPosition(); // returns LatLng object
	        map.setCenter(latLng); // setCenter takes a LatLng object
	        var infoWindow = markersseleccionados[i].infoWindow; // retrieve the InfoWindow object
	        boundstmp.extend(latLng);
	        markersseleccionados[i].infowindow.open(map,markersseleccionados[i]);
	    }

	    map.fitBounds(boundstmp);
	    
	},

	listColores : function(numtecnicos) {
	    colorinicial= "000000";
	    if (numtecnicos > 0) {
	        saltoColor = parseInt(rangoDecimalColor / numtecnicos);
	    } else {
	        saltoColor = 0;
	    }
	    if (saltoColor > 0) {
	        cont = 0;
	        while (cont <=numtecnicos) {
	            colorinicial = parseInt(saltoColor*cont);
	            colorHex = colorinicial.toString(16);
	            if (cont == 0) {
	            	coloresTecnicos.push("000000");
	            } else {
	            	coloresTecnicos.push(colorHex);
	            }
	            
	            cont++;
	        }
	    }
	}
}