Vue.config.debug = true;
var app = new Vue(
    {
    http: {
        root: '/root',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },
    el: '#mapatecnicosController',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },
    data: {
        bucketseleccionado : {id : 'BK_LARI_LA_MOLINA_RESTO'},
        loaded: false,
        showLeft: false,
        showRight: false,
        showModal: false,
        showModalAsistencia: false,
        resource_id: [],//'BK_LARI_LA_MOLINA_RESTO',
        map:[],
        tec_actividades:[],
        actividadesTipo: {
           'bucket':'En Bucket' ,
           'tecnico':'En Tecnico'
        },
        resources_id: {},
        bucketIco:'/img/icons/ofsc/lari/ACT_LARI.png',
        inicioIco:'/img/icons/tap.png',
        actividadesTipoSeleccionados: ['tecnico','bucket'],
        tecnicosSeleccionados: [],
        markers: [],
        flightPath: [],
        flightPaths: [],
        infoWindows: [],
        directionsService: [],
        directionsDisplay: [],
        mensaje: '',
        pathIconsOfsc: '/img/icons/ofsc/',
        tipo:'completo',
        colores: ['00FF00','00FFFF','0000FF','000000','000080','008000',
        '008080','800000','800080','808000','808080','C0C0C0','FF00FF','FF0000',
        'FF8800','FFFF00','FFFFFF','80FF00','DAF7A6','581845','6B3735','03927E'
        ],
        coloresActividad: ['A52A2A','DEB887','5F9EA0','7FFF00','D2691E','FF7F50',
        '6495ED','FF00FF','800000','66CDAA','0000CD','BA55D3','9370DB','006400','BDB76B',
        '8B008B','556B2F','FF8C00','9932CC','8B0000','E9967A','FFDEAD','000080','FDF5E6',
        '808000','6B8E23','FFA500','FF4500','4B0082','F0E68C','7CFC00','FFFACD','ADD8E6',
        '00FF7F','4682B4','D2B48C','008080','D8BFD8','FF6347','40E0D0','EE82EE'],
        abecedario:['A','B','C','D','E','F','G','H','I','J','K','M','N','L','O',
        'P','Q','R','S','T','U','V','W','x','Y','Z',],
        trazos: ['lineas'],
        idHistoricoProceso: 0,
        long_bucket:long_bucket,
        lat_bucket:lat_bucket,
        pestaniaUno:[],
        pestaniaDos:[],
        entorno:entorno,
        rangoDecimalColor: 16777215
    },
    methods: {
        asignacion: function (tipo_asignacion) {
          if (tipo_asignacion==1) {
             return 'Manual';
          }
          return 'Automático';
        },
        envio: function (tipo_envio) {
          if (tipo_envio=='CLI') {
             return 'Agenda';
          }
          return 'SLA';
        },
        onFileChange: function(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;
            if (e.target.id=='actividades_csv') {
                this.leerCsvActividades(e.target.files[0]);
            } else if (e.target.id=='tecnicos_csv') {
                this.leerCsvTecnicos(e.target.files[0]);
            }
        },
        leerCsvActividades: function(file){
            this.loaded = true;
            var reader = new FileReader();
            reader.onload = function (event) {
                var actividades= event.target.result.split("\n");
                var objData = [];
                for (var i = 1; i < actividades.length; i++) {
                    fila=actividades[i].split(",");
                    row={
                        id:fila[0],
                        appt_number:fila[1],
                        start_time:fila[2],
                        coordx:fila[3],
                        coordy:fila[4],
                        resource_id:fila[5],
                        date:fila[6],
                        status:fila[7],
                        address:fila[8],
                        XA_APPOINTMENT_SCHEDULER:fila[9],
                        A_ASIGNACION_MANUAL_FLAG:fila[10]
                    };
                    objData.push(row);
                }
                app.actividades= objData;
                app.loaded = false;
            };
            reader.readAsText(file);
        },
        leerCsvTecnicos: function(file){
            this.loaded = true;
            var reader = new FileReader();
            reader.onload = function (event) {
                var tecnicos= event.target.result.split("\n");
                var objData =[];
                for (var i = 1; i < tecnicos.length; i++) {
                    fila=tecnicos[i].split(",");
                    objData[fila[0]] = {
                        //carnet:fila[0],
                        name:fila[1],
                        phone:fila[2],
                        status:fila[3],
                        time:fila[4],
                        x:fila[5],
                        y:fila[6],
                        color:fila[7],
                        fuente:fila[8]
                    };
                }
                app.tecnicos= objData;//para el combo
                //app.tecnicosJson= tecnicosJson;//para el json
                app.tecnicosSeleccionados[0]='todos';
                app.selectAllTecnicos();
                app.loaded = false;
            };
            reader.readAsText(file);
        },
        verMapaCsv: function(){
            document.querySelector('#actividades_csv').value='';
            document.querySelector('#tecnicos_csv').value='';
            //this.resource_id[0]='';
            this.pintarMapa(app.actividades,null);
        },
        descargarXls: function(){
            this.exportarXls(app.actividades);
        },
        descargarKml: function() {
            this.loaded = true;
            var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", '/repofsc/exportarmapakml');
                form.setAttribute("target", "_blank");
            //bucket
            var json = document.createElement("input");
                json.setAttribute("name", "json");
            var jsonActividades = JSON.stringify( app.actividades);
                json.setAttribute("value", jsonActividades);
                form.appendChild(json);

            document.body.appendChild(form);
            form.submit();
            this.loaded = false;
        },
        exportarXls: function(actividades){
            this.loaded = true;
            var jsonAct = JSON.stringify( actividades );
            //form
            var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", '/repofsc/exportarjsonxls');
                form.setAttribute("target", "_blank");

            //token
            var json = document.createElement("input");
                json.setAttribute("name", "json");
                json.setAttribute("value", jsonAct);
                form.appendChild(json);
            document.body.appendChild(form);
            form.submit();
            this.loaded = false;
        },
        analizarMapa: function () {
            this.loaded = true;
            //validar si se cargo actividades
            actividades = this.$get('actividades');
            tecnicos = this.$get('tecnicos');
            // se cargo desde buscar
            if (actividades==undefined) {
                //mapa vacio
                this.loaded = false;
                app.mostrarMensaje('No hay actividades a analizar');
                return;
            }

            if (tecnicos==undefined) {
                //mapa vacio
                this.loaded = false;
                app.mostrarMensaje('No hay tecnicos a analizar');
                return;
            }
            var carnet;
            var jsonTecnicos = [];
            for (carnet in tecnicos) {
                if (carnet==='') continue;
                //tecnico = tecnicos[key];
                tecnicos[carnet].carnet=carnet;
                jsonTecnicos.push(tecnicos[carnet]);
            }
            var jsonActividades = JSON.stringify(actividades);
            jsonTecnicos = JSON.stringify(jsonTecnicos);

            var request = {
                'jsonActividades' : jsonActividades,
                'jsonTecnicos' : jsonTecnicos,
                //'resource_id' : this.resource_id[0],
                'latbucket' : this.lat_bucket,
                'lonbucket' : this.long_bucket
            };
            this.$http.post(
                '/repofsc/estadisticomapa' ,request, function (response) {
                    this.pestaniaUno=response.pestaniaUno;
                    this.pestaniaDos=response.pestaniaDos;
                app.showModal=true;
                this.loaded=false;
                }
            );
        },
        selectAllTecnicos: function (){
            //seleccionar todos los tecnicos
            //validar cuando no se haya seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            if (todos>=0) {
                for ( var carnet in this.$get('tecnicos')) {
                    if (carnet==='') continue;
                    this.tecnicosSeleccionados.push(carnet);
                }
            } else {
                this.tecnicosSeleccionados=[];
            }
        },
        selectTecnico: function (carnet){
            //validar si esta seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            var tecnico = this.tecnicosSeleccionados.indexOf(carnet);
            if (tecnico <0 && todos>=0 ) {
                //quitar la marca e todos en tecnicos seleccionados
                this.tecnicosSeleccionados.splice(0, 1);
            }
        },
        selectBucket: function(bucket){
            this.bucketseleccionado = bucket;
            buckettext = bucket.nombre;
            this.showTecnicos();
            this.consultarJsonHistorico();
            /*if (app.resource_id.length>0) {
                
            } else {
                this.tecnicos = [];
            } */
        },
        mostrarMensaje: function(mensaje){
            this.handle = setInterval(
                ( ) => {
                app.mensaje='';
                },4000
            );
            this.mensaje=mensaje;
        },
        clickOpciones: function(functionCall,cargarActividades){
            
            document.getElementById('pintaMapa').style.display = 'block';
            this.loaded=true;
            if (this.tecnicos == undefined) {
                this.mostrarMensaje('Seleccione tecnicos');
                this.loaded=false;
                return 0;
            }
            if (cargarActividades) {
                this.showActividades(functionCall);
            } else {
                functionCall();
            }
        },
        asistenciaTecnicos: function(functionCall){
            this.loaded=true;
            if (this.tecnicos == undefined) {
                this.mostrarMensaje('Seleccione tecnicos');
                this.loaded=false;
                return 0;
            }
            functionCall();
        },
        guardarConformidadAsistencia : function() {
            this.loaded = true;
        },
        verTecnicoMapa : function(coordx , coordy) {
            this.map.setCenter(new google.maps.LatLng(coordy, coordx));
        },
        verFormularioAsistencia: function(){
            this.loaded = true;
            //validar si se cargo actividades
            actividades = this.$get('actividades');
            tecnicos = this.$get('tecnicos');
            // se cargo desde buscar

            if (tecnicos==undefined) {
                //mapa vacio
                this.loaded = false;
                app.mostrarMensaje('No hay tecnicos a analizar');
                return;
            }
            app.showModalAsistencia=true;
            this.loaded=false;
        },
        addRoute: function(rutasMapa) {
            this.directionsDisplay[rutasMapa.numero] = new google.maps.DirectionsRenderer;
            this.directionsDisplay[rutasMapa.numero].setMap(this.map);
            var waypts = [];
            var final=rutasMapa.length -1;
            for (var i = 1; i < final; i++) {
                if (rutasMapa[i]) {
                  waypts.push(
                      {
                      location: rutasMapa[i],
                      stopover: true
                      }
                  );
                }
            }
            var request = {
                origin: rutasMapa[0],
                destination: rutasMapa[final],
                waypoints: waypts,
                optimizeWaypoints: false,
                travelMode: google.maps.TravelMode.DRIVING
            };
            this.directionsService.route(
                request, function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    app.directionsDisplay[rutasMapa.numero].setDirections(response);
                    var lineSymbol = {
                      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                      strokeOpacity: 1,
                      scale: 2
                    };
                    app.directionsDisplay[rutasMapa.numero].setOptions(
                        {
                        suppressMarkers: true,
                        polylineOptions: {
                            strokeWeight: 4,
                            strokeOpacity: 0,
                            strokeColor: rutasMapa.color,
                            icons: [{
                                icon: lineSymbol,
                                offset: '0',
                                repeat: '20px'
                            }],
                        }
                        }
                    );
                } else {
                    app.mostrarMensaje('Solicitud de direcciones suspendida debido a' + status);
                }
                }
            );
        },
        addLine: function(rutasMapa) {
            this.flightPath = new google.maps.Polyline(
                {
                    path: rutasMapa,
                    geodesic: true,
                    strokeColor: rutasMapa.color,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                }
            );
            this.flightPath.setMap(this.map);
            //array de lineas
            this.flightPaths.push(this.flightPath);
        },
        addMarker: function(labelContent,labelClass,location, icon , info, punto) {
            //var componenteIcon = window.location.origin+icon;
            zIndexRep=0;
            var marker = new MarkerWithLabel(
                {
                position: location,
                icon: icon,
                map: this.map,
                title: labelContent,
                zIndex: zIndexRep++,
                labelContent: punto,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: labelClass,
                labelStyle: {opacity: 0.85}
                }
            );
            var infowindow = new google.maps.InfoWindow(
                {
                content: info
                }
            );
            this.markers.push(marker);
            //click en marcadores
            marker.addListener(
                'click', function(event) {
                if(app.infoWindows.length>0){
                    for (var i=0;i<app.infoWindows.length;i++) {
                        app.infoWindows[i].close();
                    }
                }
                infowindow.open(this.map, marker);
                app.infoWindows.push(infowindow);
                }
            );
        },
        setMapOnAll: function() {
            this.removeRoutes();
            this.removeLines();
            this.removeMarkers();
        },
        removeRoutes: function() {
            //recorrer rutas
            for (var i in this.directionsDisplay) {
                this.directionsDisplay[i].setDirections({routes: []});
                this.directionsDisplay[i].setMap(null);
            }
        },
        removeLines: function(){
            for (var i = 0; i < this.flightPaths.length; i++) {
                this.flightPaths[i].setMap(null);
            }
        },
        removeMarkers: function() {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },
        showBuckets: function() {
            this.$http.get(
                '/recursoofsc/index' , function (recursosofsc) {
                this.$set('resources_id', recursosofsc);
                this.loaded=false;
                }
            );
        },
        showActividades: function(functionCall) {
            this.$http.get(
                '/actividadesofsc/' + this.bucketseleccionado.id, function (actividades) {
                this.loaded=false;
                functionCall(actividades);//pintarMapa:function
                }
            );
        },
        showTecnicos: function() {
            this.$http.get(
                '/tecnicosofsc/' + this.bucketseleccionado.id, function (tecnicos) {
                var j=0;
                for ( var i in tecnicos) {
                    tecnicos[i].color=this.colores[j];
                    j++;
                }
                this.$set('tecnicos', tecnicos);
                }
            );
        },
        refrescarActividades : function() {
            if (typeof this.bucketseleccionado  == undefined || typeof this.bucketseleccionado == "undefined" ) {
                app.mostrarMensaje('Debe Seleccionar un Bucket!!!');
                return;
            }
            this.loaded=true;
            this.$http.get('/actividadesofsc/' + this.bucketseleccionado.id, {refresh:  true} ).then(
                function (response) {
                    //look into the routes file and format your response
                    this.showTecnicos();
                    this.pintarMapa(response.data);
                    this.loaded=false;
                }, function (error) {
                    app.mostrarMensaje('Error en la Aplicacion');
                    this.loaded=false;
                    return;
                });
        },
        guardarMapaToJson: function() {
            var nombreJson = prompt("Por favor ingrese nombre de archivo a guardar");
            if (nombreJson === "" || nombreJson == undefined) {
                app.mostrarMensaje('Ingresar nombre');
                return;
            } else {
                var request = {nombreJson: nombreJson,
                    actividades: this.actividades,
                    tecnicos: this.tecnicos,
                    resource_id: this.bucketseleccionado.id,
                    entorno: entorno
                };
                this.$http.post(
                    '/historico_mapatecnico/crear', request, function (response) {
                    app.mostrarMensaje('Mapa Registrado');
                    }
                );
            }
            this.loaded = false;
        },
        consultarJsonHistorico : function () {
            if (this.resource_id[0]==='' || undefined==this.resource_id[0]) {
                this.mostrarMensaje('Seleccione bucket');
                return 0;
            }
            var request = {entorno: entorno,resource_id:this.resource_id[0]};
            this.$http.get(
                '/historico_mapatecnico/listar', request, function (response) {
                this.$set('json_historicos', response.datos);
                }
            );
        },
        /**
         * se llama desde el radio de seleccion de proceso automatico
         * sleccionar listado por dia
         * sleccionar listado por hora
         *
        */
        listarHistoricoProceso : function (fecha) {
            if (this.resource_id[0]==='' || undefined==this.resource_id[0]) {
                this.mostrarMensaje('Seleccione bucket');
                return 0;
            }
            var request = {fecha: fecha, resource_id: this.resource_id[0]};
            this.$http.get(
                '/repofsc/actividadesofschistorico', request, function (response) {
                    if (fecha === "") {
                        this.$set('historico_proceso', response.datos);
                        this.$set('historico_proceso_detalle', []);
                    } else {
                        this.$set('historico_proceso_detalle', response.datos);
                    }
                }
            );
        },
        /**
         * consulta la data guardada del mapa de tecnicos
         * en el proceso programado
         */
        verMapaProcAutomatico:function(){
        //busqueda historica de proceso
            if (this.resource_id[0]==='' || undefined==this.resource_id[0]) {
                this.mostrarMensaje('Seleccione bucket');
                return 0;
            }
            if (this.historicoProceso=='.: Seleccione :.') {
                this.mostrarMensaje('Seleccione fecha de proceso');
                return 0;
            }
            if (this.$get("historicoProcesoDetalle")=='.: Seleccione :.') {
                this.mostrarMensaje('Seleccione hora de proceso');
                return 0;
            }
            this.showLeft = false;
            this.loaded=true;
            json = this.$get("historicoProcesoDetalle");
            this.pintarMapa(JSON.parse(json), this.historicoProceso);
            this.loaded = false;
        },
        /**
         * consulta la data guardada del mapa de tecnicos
         *
         */
        verMapaHistorico : function (json_historicos) {
            if (this.resource_id[0]==='' || undefined==this.resource_id[0]) {
                this.mostrarMensaje('Seleccione bucket');
                return 0;
            }
            if (json_historicos=='.: Seleccione :.') {
                this.mostrarMensaje('Seleccione historico');
                return 0;
            }
            this.loaded=true;
            //busqueda historica
            var request = {json:json_historicos, resource_id:this.resource_id[0]};
            this.$http.get(
                '/repofsc/historico', request, function (response) {
                    this.$set('tecnicos', JSON.parse(response.tecnicos));
                    this.pintarMapa(JSON.parse(response.actividades), null);
                }
            );
        },
        pintarMapa : function (actividades, fecha) {
            if (actividades.length == 0) {
                app.mostrarMensaje('Archivo Actividades sin registros');
                this.loaded=false;
                return;
            }
            //actualizar variable global
            this.$set('actividades', actividades);
            this.setMapOnAll();
            var bounds = new google.maps.LatLngBounds();
            var carnet, info, icon, start, time;
            var contenedor='';
            var color;
            var index;
            var actividadaid;
            var rutas=[];
            var actividadesTec=[];
            var actividadesBucket=[];
            var tipoActividad;
            var esTecnicoSelec;
            var tipo;
            var procedenciaAsignacion;
            //recorrer actividades para pintarlos en el mapa las que tienen bucket

            for ( actividadaid in actividades) {
                actividad = actividades[actividadaid];
                //filtrar x y validos , y actividades no borradas
                if (actividad.coordx!=0 && actividad.coordy!=0 && actividad.status!='deleted' 
                    && actividad.status!='cancelled' &&
                    actividad.date!='3000-01-01') {

                    actividadLocation = new google.maps.LatLng(actividad.coordy,actividad.coordx);
                    info='<label>Fecha Agenda: </label>&nbsp;&nbsp;<t>' +actividad.date+'<br>';
                    info+='<label>Estado: </label>&nbsp;&nbsp;&nbsp;'+actividad.status+'<br>';
                    info+='<label>Actuacion: </label>&nbsp;&nbsp;&nbsp;'+actividad.appt_number+'<br>';
                    //CLI o TEL
                    if (actividad.XA_APPOINTMENT_SCHEDULER =='TEL') {
                        tipo='SLA';
                        info+='<label>Tipo: </label>&nbsp;&nbsp;&nbsp;SLA<br>';
                    } else {
                        tipo='Ag';
                        info+='<label>Tipo: </label>&nbsp;&nbsp;&nbsp;Agenda<br>';
                    }
                    if (actividad.A_ASIGNACION_MANUAL_FLAG == "1" || actividad.A_ASIGNACION_MANUAL_FLAG == 1) {
                        info+='<label>Asignacion: </label>&nbsp;&nbsp;&nbsp;MANUAL<br>';
                        procedenciaAsignacion = "m";
                    } else {
                        procedenciaAsignacion = "r";
                        info+='<label>Asignacion: </label>&nbsp;&nbsp;&nbsp;ROUTING<br>';
                    }
                    tipoActividad = this.actividadesTipoSeleccionados;
                    //filtro por bucket
                    if (this.resource_id[0]==actividad.resource_id && tipoActividad.indexOf('bucket')>=0) {
                        actividadesBucket.push(1);
                        this.addMarker(actividad.appt_number,'',actividadLocation,this.bucketIco,info,'');
                        bounds.extend(actividadLocation);
                    //filtrar por tecnico
                    } else if (this.resource_id[0]!=actividad.resource_id && tipoActividad.indexOf('tecnico')>=0) {
                        esTecnicoSelec = this.tecnicosSeleccionados.indexOf(actividad.resource_id);
                        info+='<label>Tecnico: </label>&nbsp;&nbsp;&nbsp;'+actividad.resource_id+'<br>';
                        if (esTecnicoSelec>=0) {
                            //validar si existe start_time
                            //buscar marcador de actividad para este tecnico
                            color=this.coloresActividad[Math.floor(Math.random()*(40-0))+0];
                            if (this.tecnicosSeleccionados[actividad.resource_id]!=undefined) {
                                color = this.$get('tecnicos')[actividad.resource_id].color;
                            }
                            //para crear el array de rutas solo se considera las actividades con start_time
                            start='sin hora';
                            var orden=0;
                            if (typeof actividad.start_time == 'string' && actividad.start_time.length !='0') {
                                start = actividad.start_time.substring(11, 19);
                                orden = actividad.start_time.substring(11, 13)+actividad.start_time.substring(14, 16);
                                if (rutas[actividad.resource_id]==undefined) {
                                    //inizializando rutas
                                    rutas[actividad.resource_id]=[];
                                }
                                //añadir al array de rutas
                                rutas[actividad.resource_id].push({
                                    'lng':parseFloat(actividad.coordx),
                                    'lat':parseFloat(actividad.coordy),
                                    'orden':orden,
                                    'color':color
                                });
                            }
                            info+='<label>Hora: </label>&nbsp;&nbsp;&nbsp;'+start;
                            if (actividadesTec[actividad.resource_id]==undefined) {
                                //inizializando actividadesTec
                                actividadesTec[actividad.resource_id]=[];
                            }
                            icon=this.pathIconsOfsc+color+'.png';
                            actividadesTec[actividad.resource_id].push(
                                {
                                'actividad':actividad.appt_number,
                                'status':actividad.status,
                                'location':actividadLocation,
                                'icon':icon,
                                'info':info,
                                'hora':start,
                                'orden':orden,
                                'tipo':tipo,
                                'asignacion':procedenciaAsignacion
                                }
                            );
                            bounds.extend(actividadLocation);
                        }
                    }
                }
            }
            //recorer actividades con tecnicos para pintarlas en el mapa
            this.tec_actividades=[];
            for ( carnet in actividadesTec) {
                if (carnet==='') continue;
                var actividadTec=actividadesTec[carnet];
                actividadTec.sort( function(a,b){
                    return a.orden - b.orden;
                });
                var abc=0;
                this.tec_actividades[carnet]=[];
                this.tec_actividades[carnet]=actividadTec.length;
                for ( index in actividadTec) {
                    var colorText = 'style="color: white"';
                    //letras negras cuando el color sea muy claro
                    if (actividadTec[index].status == "started"  ||
                        actividadTec[index].status == "pending" ||
                        actividadTec[index].status=='cancelled') {
                        colorText = 'style="color: black"';
                    }
                    var punto ='<div '+colorText+'>'+this.abecedario[abc]+
                                '<br>'+actividadTec[index].tipo+
                                '<br><div class="'+actividadTec[index].asignacion+'">'+actividadTec[index].asignacion+'</div></div>';
                    this.addMarker(
                        actividadTec[index].actividad,
                        actividadTec[index].status+' caja-estado',
                        actividadTec[index].location,
                        actividadTec[index].icon,
                        actividadTec[index].info,
                        punto
                    );
                    abc++;
                }
            } 
            // determinar los colores de los tecnicos
            color= "000000";
            numtecnicos = 0;
            coloresTecnicos = [];
            for (var i in this.$get("tecnicos")) {
                numtecnicos ++;
            }
            iconTecnico = {
                path: fontawesome.markers.MALE,
                scale: 0.6,
                strokeWeight: 0.3,
                strokeColor: 'black',
                strokeOpacity: 1,
                fillColor: "#"+color,
                fillOpacity: 1
            };
            if (numtecnicos > 0) {
                saltoColor = parseInt(this.rangoDecimalColor / numtecnicos);
            } else {
                saltoColor = 0;
            }
            cont = 1;
            if (saltoColor > 0) {
                while (color < this.rangoDecimalColor) {
                    color = parseInt(saltoColor*cont);
                    colorHex = color.toString(16);
                    coloresTecnicos.push(colorHex);
                    cont ++ ;
                }
            }
            j = 0;
            //recorrer tecnicos para pintarlos en el mapa
            var tecnicos=[];
            for ( carnet in this.$get('tecnicos')) {
                if (carnet==='') continue;
                tecnico = this.$get('tecnicos')[carnet];
                esTecnicoSelec = this.tecnicosSeleccionados.indexOf(carnet);
                //tecnico seleccionado en el select
                if (typeof tecnico!="undefined" && typeof tecnico!=undefined) {
                    if (esTecnicoSelec>=0) {
                        if (tecnico.coord_x!=0 && tecnico.coord_y!=0 && typeof tecnico.coord_x !="undefined" && tecnico.coord_y!="undefined") {
                            color = tecnico.color;
                            icon=this.pathIconsOfsc+color+'_tec.png';
                            /*if (typeof coloresTecnicos[j]!="undefined") {
                                icon = iconTecnico;
                                icon.fillColor = "#"+coloresTecnicos[j];
                            }*/
                            tecnicoLocation = new google.maps.LatLng(tecnico.coord_y,tecnico.coord_x);
                            info='<label>Carnet: </label>&nbsp;&nbsp;<t>' +carnet+'<br>';
                            info+='<label>Fecha Ubicacion: </label>&nbsp;&nbsp;&nbsp;'+tecnico.time;
                            //validar la hora de localizcion del tecnico
                            time = new Date(tecnico.time);
                            hora = new Date(ahora);
                            time = time.getTime()/1000;
                            hora = hora.getTime()/1000;
                            diferencia = hora - time;

                            //20 minutos
                            if (diferencia<1200) {
                                status = 'tecnicoVerde';
                                style='background-color: #A5D6A7; padding: 5px 5px 5px 5px;';
                            //2 horas
                            } else if (diferencia<7200) {
                                status = 'tecnicoNaranja';
                                style='background-color: #FFCC80; padding: 5px 5px 5px 5px;';
                            //4 horas
                            } else if (diferencia<14400) {
                                status = 'tecnicoRojo';
                                style='background-color: #EF9A9A; padding: 5px 5px 5px 5px;';
                            //mas de 4 horas
                            } else {
                                status = 'tecnicoAzul';
                                style='background-color: #9FA8DA; padding: 5px 5px 5px 5px;';
                            }
                            if(this.tec_actividades[carnet]){
                                aorden=this.tec_actividades[carnet];
                            } else {
                                aorden=0;
                            }
                            this.addMarker(carnet,'',tecnicoLocation,icon,info,'');
                            bounds.extend(tecnicoLocation);
                            tecnicos.push({'cant':aorden,'des':"<li><div class='izquierda'><img src='"+this.pathIconsOfsc+ tecnico.color+"_tec.png' ></div><div class='derecha'><span class='nombre-tecnico'><p>"+tecnico.name+" ( "+aorden+" )</p></span><span class='tiempo-orden'><strong style='"+style+"'>"+moment(tecnico.time).format("HH:mm:ss")+' ('+tecnico.fuente+")</strong></span></div></li>"});
                        } else {
                        //tecnico sin localizacion
                            if(this.tec_actividades[carnet]){
                                aorden=this.tec_actividades[carnet];
                            } else {
                                aorden=0;
                            }
                            tecnicos.push({'cant':aorden,'des':"<li><div class='izquierda'><img src='"+ this.pathIconsOfsc + tecnico.color+"_tec.png' ></div><div class='derecha'><span class='nombre-tecnico'><p>"+tecnico.name+" ( "+aorden+" )</p></span><span class='tiempo-orden'><strong>sin coordenadas</strong></span></li>"});
                        }
                    }
                }

                j++;
            }
            //ordenar tecnicos en funcion a su cantidad de actividades
            tecnicos.sort(
                function(a,b){
                 return  b.cant - a.cant;
                }
            );
            //pintar bucket en aside
            contenedor+="<li><div class='izquierda'><img src='"+this.bucketIco+"' ></div><div class='derecha'><span class='nombre-tecnico'><p>"+this.resource_id[0]+" : ("+actividadesBucket.length+")</p></span></div></li>";
            //pintar tecnicos en aside
            for ( index in tecnicos ) {
                contenedor+=tecnicos[index].des;
            }
            document.getElementById('tecnicos').innerHTML = '<ul>'+contenedor+'</ul>';

            var ruta, numeroRuta=1;
            //dibujando las rutas
            for ( carnet in rutas) {
                if (carnet==='') continue;
                ruta=rutas[carnet];
                ruta.sort( function(a,b){
                    return a.orden - b.orden;
                });
                var rutasMapa=[];
                for ( index in ruta) {
                    color=  ruta[index].color ;
                    // establecer color de marcador de tecnico
                    rutasMapa.push(ruta[index]);
                }
                rutasMapa.numero= numeroRuta;
                // establecer color de marcador de tecnico
                rutasMapa.color= '#' + color;
                if (this.trazos.indexOf('lineas')>=0) {
                    this.addLine(rutasMapa);
                }
                if (this.trazos.indexOf('rutas')>=0) {
                    this.addRoute(rutasMapa);
                }
                numeroRuta++;
            }
            if (icon!='' && icon!=undefined ) {
                this.map.fitBounds(bounds);
            }
            this.loaded=false;
            this.showLeft = false;
        },
    },
    ready: function(){
        this.showBuckets();
    },/*
    destroyed(){
        clearInterval(this.handle);
    },*/
    events: {
        'google.maps:init': function() {
            this.loaded=true;
            this.directionsService= new google.maps.DirectionsService;
            var myLatlng = new google.maps.LatLng(this.lat_bucket,this.long_bucket);
            this.map = new google.maps.Map(
                document.getElementById('map'), {
                center: myLatlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );

            bucketLocation = new google.maps.LatLng(this.lat_bucket, this.long_bucket);
            var marker = new google.maps.Marker({
                map: this.map,
                draggable:true,
                animation: google.maps.Animation.DROP,
                position: bucketLocation,
                icon: this.inicioIco,
                title: 'Bucket'
            });
            var infowindow = new google.maps.InfoWindow({
                content: '<b>Bucket</b>'
            });
            marker.addListener('click', function() {
                infowindow.open(this.map, marker);
            });

            google.maps.event.addListener(marker, 'dragend', function(event) {
                app.lat_bucket = event.latLng.lat();
                app.long_bucket = event.latLng.lng();
            });

            this.loaded=false;
        }
    }
    }
);
window.initMap = function() {
    app.$emit('google.maps:init');
};