
var solicitudesT = {
    listar: function () {
        var datos="";
        var targets=0;
        $('#tb_actividadPorTecnico').dataTable().fnDestroy();
        $('#tb_actividadPorTecnico').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,                
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                solicitudesT.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.bucket;}},
                {data : function( row, type, val, meta) {return row.nombre_tecnico;}},
                {data : function( row, type, val, meta) {return row.fecha_activacion;}},
                {data : function( row, type, val, meta) {return row.fecha_inicio_primeraActividad;}},
                {data : function( row, type, val, meta) {return row.fecha_fin_primeraActividad;}},
                {data : function( row, type, val, meta) {return row.fecha_fin_ultimaActividad;}},
                {data : function( row, type, val, meta) {return row.coordx;}},
                {data : function( row, type, val, meta) {return row.coordy;}},
                {data : function( row, type, val, meta) {return row.cantidad;}},
                {data : function( row, type, val, meta) {return row.pendiente;}},
                {data : function( row, type, val, meta) {return row.iniciada;}},
                {data : function( row, type, val, meta) {return row.completada;}},
                {data : function( row, type, val, meta) {return row.norealizada;}},
            ],
            paging: false,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        }); 
    },
    modalOpenCoordendas: function(button) {
        
        eventoCargaMostrar();
        axios.post('coordenadasTecnicas/datasolicitud',request).then(response => {
           
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            //eventoCargaRemover();
        });
    },       
    http_ajax: function(request,callback){
        var contador = 0;
        var rangoFechas =$("#fecha").val();
        //var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&fecha_inicio="+rangoFechas
        //form+="&fecha_fin="+rangoFechas[1];
        
        eventoCargaMostrar();
        axios.post('actividadPorTecnico/cargar',form).then(response => {
            callback(response.data);
            //console.log(response);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    ExportEcxel: function () {
        $("input[type='hidden']").remove();
        $("#form_reporte").append("<input type='hidden' value='excel' id='tipo' name='tipo'>");
        var rangoFechas =$("#fecha").val();        

        //empresas=$("#slct_empresas").val();
        //actividades=$("#slct_actividades").val();
        buckets=$("#slct_bucketss").val();
        $("#form_reporte").append("<input type='hidden' value='"+rangoFechas+"' id='fch_inicio' name='fch_inicio'>");
        //$("#form_reporte").append("<input type='hidden' value='"+rangoFechas[1]+"' id='fch_fin' name='fch_fin'>");
        //$("#form_reporte").append("<input type='hidden' value='"+empresas+"' id='empresas' name='empresas'>");
        $("#form_reporte").append("<input type='hidden' value='"+buckets+"' id='buckets' name='buckets'>");    
        $("#form_reporte").submit();
    },
    getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {

            var obj = response.data;
            
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
                vm.lista[ selects[i] ] = obj.data[i];
            }
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
}