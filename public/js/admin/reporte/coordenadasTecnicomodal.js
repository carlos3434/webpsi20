$('#st-detalle a').on('shown.bs.tab', function(e){
        if ($(this)[0].hash=='#mapa') {
            if (vm.solicitud.tipo_legado == 1) {
                PestanaOrden.pintarMapa();
            }
        }
    });
var enforceModalFocusFn;
    $('#BandejaModala').on('show.bs.modal', function (event) {
        enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        var button = $(event.relatedTarget);
        Legado.modalOpenCoordendas(button);
    });
    $('#BandejaModala').on('hide.bs.modal', function (event) {
        $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
        var modal = $(this);
        modal.find('.modal-body input[type=text]').val('');
        hideModal();
    });