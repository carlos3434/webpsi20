$(document).ready(function(){
    solicitudesProc.listar();

    $('#txt_rangoFechas').daterangepicker({
      format: 'YYYY-MM-DD'});

    slctGlobal.listarSlct('solicitudesenproceso','slct_buckets','multiple',null,{'tipo':'buckets'});
    slctGlobal.listarSlct('solicitudesenproceso','slct_tipo','simple',[1],{'tipo':'tipo'});
    //slctGlobalHtml('slct_tipo','simple', '1');
});

$(document).on('click', '#btnBuscar', function(e){
    tipo=$('#slct_tipo').val();
    fechas=$('#txt_rangoFechas').val();

    if(tipo==''){
      swal("Mensaje","Seleccione 'tipo' de Reporte!");
      return false;
    }

    solicitudesProc.listar();
});

$(document).on('click', '#btnExportar', function(e){
  solicitudesProc.ExportEcxel();
});