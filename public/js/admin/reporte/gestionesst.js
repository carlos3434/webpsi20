$(document).ready(function(){
    //gestionesSt.listar();

    $('#txt_rangoFechas').daterangepicker({
      format: 'YYYY-MM-DD'});

    slctGlobal.listarSlct('gestionesSt','slct_tipo_legado','multiple',null,{'tipo':'tipo_legado'});
    slctGlobal.listarSlct('gestionesSt','slct_actividad','multiple',null,{'tipo':'actividad'});
    slctGlobal.listarSlct('gestionesSt','slct_buckets','multiple',null,{'tipo':'bucket'});

});

$(document).on('click', '#btnBuscar', function(e){
        var rangoFechas =$("#txt_rangoFechas").val();
  var rangoFechas = rangoFechas.split(" - ");
  var fecha_inicio = moment(rangoFechas[0]);
  var fecha_final = moment(rangoFechas[1]);

    var num_dias=fecha_final.diff(fecha_inicio, 'days')+1;
    if(num_dias>31){swal("Mensaje",'Rango de fecha no debe ser mayor a 31 dias!');return false;}

    gestionesSt.listar();
});