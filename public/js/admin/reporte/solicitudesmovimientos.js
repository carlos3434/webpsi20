var fecha_ini, fecha_fin;
$(document).ready(function() {
    $('#fecha').daterangepicker({
        format: 'YYYY-MM-DD'
    });
    $("#generar_critico").click(function (){
        mostrarCritico();
    });
});
descargaCmsAveria=function(){
    var envio=validaParametros();
    $("input[type='hidden']").remove();
    if( envio ){
        $("#form_solicitudes").append("<input type='hidden' value='1' name='actividad_id' id='actividad_id'>");
        $("#form_solicitudes").append("<input type='hidden' value='"+fecha_ini+"' name='fechaIni' id='fechaIni'>");
        $("#form_solicitudes").append("<input type='hidden' value='"+fecha_fin+"' name='fechaFin' id='fechaFin'>");
        $("#form_solicitudes").submit();
    }
};
descargaCmsProvision=function(){
    var envio=validaParametros();
    $("input[type='hidden']").remove();
    if( envio ){
        $("#form_solicitudes").append("<input type='hidden' value='2' name='actividad_id' id='actividad_id'>");
        $("#form_solicitudes").append("<input type='hidden' value='"+fecha_ini+"' name='fechaIni' id='fechaIni'>");
        $("#form_solicitudes").append("<input type='hidden' value='"+fecha_fin+"' name='fechaFin' id='fechaFin'>");
        $("#form_solicitudes").submit();
    }
};

descargaAverias=function(){
    
};
descargaProvision=function(){
    
};
validaParametros=function(){
    var fecha = $("#fecha").val();
    fecha_ini = fecha.substring(0,10);
    fecha_fin = fecha.substring(13);
    
    if (fecha_ini==="" && fecha_fin===""){
        alert("Indique la Fecha Registro correctamente");
        return false;
    }
    
    return true;
};