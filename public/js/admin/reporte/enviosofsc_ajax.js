var EnviosOfsc={
     listar: (function () {
            var datos="";
            var targets=0;
            $('#t_enviosofs').dataTable().fnDestroy();
            $('#t_enviosofs')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();

                        },
                        ajax: function(data, callback, settings) {
                            EnviosOfsc.http_ajax(data,callback);
                        },
                        "columnDefs":[
                            {
                                "targets": [ 6 ],
                                "visible": false,
                                "searchable": false
                            },
                            {
                                "targets": [ 7 ],
                                "visible": false
                            },
                            {
                                "targets": [ 8 ],
                                "orderable": false
                            }  
                        ],
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.accion!="undefined" && typeof row.accion!=undefined) {
                                    return row.accion;
                                } else return "";
                            }, name:'accion'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.codactu!="undefined" && typeof row.codactu!=undefined) {
                                    return row.codactu;
                                } else return "";
                            }, name:'codactu'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.created_at!="undefined" && typeof row.created_at!=undefined) {
                                    return row.created_at;
                                } else return "";
                            }, name:'created_at'},
                              {data : function( row, type, val, meta) {
                                if (typeof row.usuario!="undefined" && typeof row.usuario!=undefined) {
                                    return row.usuario;
                                } else return "";
                            }, name:'usuario'},
                              {data : function( row, type, val, meta) {
                                if (typeof row.enviado!="undefined" && typeof row.enviado!=undefined) {
                                    if(row.enviado.length > 50) enviadoFinal = row.enviado.substring(0, 50) + '...'; else enviadoFinal = row.enviado;
                                    return enviadoFinal;
                                } else return "";
                            }, name:'enviadoFinal'},
                               {data : function( row, type, val, meta) {
                                if (typeof row.enviado!="undefined" && typeof row.enviado!=undefined) {
                                    if(row.respuesta.length > 50) respuestaFinal = row.respuesta.substring(0, 50) + '...'; else respuestaFinal = row.respuesta;
                                    return respuestaFinal;
                                } else return "";
                            }, name:'respuestaFinal'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.enviado!="undefined" && typeof row.enviado!=undefined) {
                                    return row.enviado;
                                } else return "";
                            }, name:'enviado'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.respuesta!="undefined" && typeof row.respuesta!=undefined) {
                                    return row.respuesta;
                                } else return "";
                            }, name:'respuesta'},
                            {data : function( row, type, val, meta) {                            
                                htmlButtons = "<button type='button' class='btn btn-default btn-xs' enviado='"+row.enviado+"' respuesta='"+row.respuesta+"'><span class='glyphicon glyphicon-search'></span></button>";
                                return htmlButtons;
                            }, name:'botones'}
                        ],
                        paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 2 , "desc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),       
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#FormBuscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('enviosofsc/reporteofsc',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    }    
};