var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 
var header = [];
$(document).ready(function(){
    getHeader(2);

    $('#fecha').daterangepicker({
        format: 'YYYY-MM-DD',
        minDate: moment().subtract(35, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
    });
    slctGlobalHtml('slct_tipo_fecha','simple');
    //Mostrar 
    $("#generar_movimientos").click(function (){
        mostrarMovimiento();
    });
    //iCheck-helper

    $(document).on('click', '#mostrar', function(event) {
        if($.trim($("#fecha").val()) != ''){
            solicitudesT.listarSolicitudes(header);
        }else{
            swal('Seleccione un rango de fechas');       
        }
    });

    $('#chk_cms').on('ifChanged', function(event){
        var selected = event.target.checked;      
        cabecera = (selected) ? 1 : 2;
        getHeader(cabecera);          
    });

    DescargarExcel = function(){
         if($.trim($("#fecha").val()) != ''){
                $("#form_movimiento").attr("action", "reporte/movimientoultimo");
                $("#form_movimiento").append("<input type='hidden' name='accion' value='download' id='accion' />");
                $("#form_movimiento").submit();
                $("#accion").remove();
                $("#form_movimiento").attr("action", "");
         }else{
            swal('Seleccione un rango de fechas');
         }      
    };
});

getHeader = function(cabecera){
        if(cabecera != ''){
            var html='';
            $.post('reporte/movimientoultimo', {cabecera: cabecera}, function(data, textStatus, xhr) {
            var result = JSON.parse(data).header;
                if(result){
                    header = [];
                    html+='<table id="tb_solicitudes" class="table table-bordered table-hover">';
                    html+=' <thead>';
                    html+='    <tr>';
                    $.each(result,function(index, el) {
                        if(el != 'usuario_updated_at' && el != 'usuario_created_at' && el != 'created_at' && el != 'updated_at' && el != 'id'){
                            html+='<th>'+el+'</th>';
                            header.push(el);
                        }
                     });
                    html+='    </tr>';
                    html+=' </thead>';
                    html+=' <tbody></tbody>';
                    html+='</table>';              
                    $(".dv_solicitudes").html(html);
                }
            });            
        }
    }

