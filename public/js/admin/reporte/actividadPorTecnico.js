$(document).ready(function(){
    //solicitudesT.listar();
    $('#rangoFechas').daterangepicker(
     {format: 'YYYY-MM-DD'});
      var today = moment();
      $('#fecha').daterangepicker({
            singleDatePicker: true,
            showDropdowns: false,
            startDate: today,
            format: 'YYYY-MM-DD'
        });

        fecha=$('#fecha').val(today.format('YYYY-MM-DD'));        
        slctGlobal.listarSlct('actividadPorTecnico','slct_bucketss','multiple',null,{'tipo':'slct_bucketss'});
    
});

$(document).on('click', '#btnExportExcel', function(e){
	var fechas=$('#rangoFechas').val();
	if(fechas==''){swal("Mensaje",'Seleccione un rango de fechas!');return false;}
   solicitudesT.ExportEcxel();
});

$(document).on('click', '#btnBuscar', function(e){
	var fechas=$('#rangoFechas').val();
	if(fechas==''){swal("Mensaje",'Seleccione un rango de fechas!');return false;}
   solicitudesT.listar();
});


