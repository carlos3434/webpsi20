     var solicitudesT = {
        listar: (function () {
            var datos="";
            var targets=0;
            $('#tb_solicitudes').dataTable().fnDestroy();
            $('#tb_solicitudes')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            solicitudesT.http_ajax(data,callback);
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.solicitud_tecnica_id!="undefined" && typeof row.solicitud_tecnica_id!=undefined) {
                                    return row.solicitud_tecnica_id;
                                } else return "";
                            }, name:'solicitud_tecnica_id'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.num_req!="undefined" && typeof row.num_req!=undefined) {
                                    return row.num_req;
                                } else return "";
                            }, name:'num_req'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.CMS!="undefined" && typeof row.CMS!=undefined) {
                                    return row.CMS;
                                } else return "";
                            }, name:'CMS'},
                             {data : function( row, type, val, meta) {
                                 if (typeof row.PSI!="undefined" && typeof row.PSI!=undefined) {
                                    return row.PSI;
                                } else return "";
                            }, name:'PSI'},
                             {data : function( row, type, val, meta) {
                                 if (typeof row.TOA!="undefined" && typeof row.TOA!=undefined) {
                                    return row.TOA;
                                } else return "";
                            }, name:'TOA'}
                        ],
                          paging: false,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),   
    http_ajax: function(request,callback){
            var contador = 0;
            var form = $('#form_movimiento').serialize().split('txt_').join("").split('slct_').join("");
            var order = request.order[0];
            form+='&column='+request.columns[ order.column ].name;
            form+='&dir='+order.dir;
            form+="&per_page="+request.length;
            form+="&page="+(request.start+request.length)/request.length;

            eventoCargaMostrar();
            axios.post('reporte/trazasolicitudes',form).then(response => {
                callback(response.data);
            }).catch(e => {
                //vm.errors=e;
            }).then(() => {
                eventoCargaRemover();
            })
    } 
}   

/*  var columnDefs = [
                {
                    "targets": 0,
                    "data": "num_req",
                    "name": "num_req"
                }
            ];
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "codcli",
                                "name": "codcli"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "nomCliente",
                                "name": "nomCliente"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_st",
                                "name": "estado_st"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "solicitud_tecnica_id",
                                "name": "solicitud_tecnica_id"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_ofsc_id",
                                "name": "estado_ofsc_id"
                            });          */ 