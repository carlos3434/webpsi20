var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 

$(document).ready(function(){
    $('#fecha').daterangepicker({
     singleDatePicker: true,
     format: 'YYYY-MM-DD',
     endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
        },function (startDate, endDate, period) {
            var diff = daydiff(parseDate(startDate.format('L')), parseDate(endDate.format('L')))
            if(diff > 8){
                var last = parseDate(startDate.format('L'));
                last.setDate(last.getDate() + 7);
                
                $('#fecha').val(
                    moment(startDate).format("YYYY-MM-DD") + ' - ' +
                    moment(last).format("YYYY-MM-DD")
                );               
                swal('El rango es maximo de 7 días');          
            }
    });
    
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    slctGlobalHtml('slct_situado','simple');
    slctGlobalHtml('slct_estado','simple');

    $('.iCheck-helper').click(function(){
        selectFiltro();
    });

    $(document).on('click', '#mostrar', function(event) {
        if($.trim($("#fecha").val()) != ''){
            if($.trim($("#slct_estado").val()) != ''){
                solicitudesT.listar();
            }else{
                swal('Seleccione un estado');    
            }
        }else{
            swal('Seleccione un rango de fechas');       
        }
    });

    DescargarExcel = function(){
         if($.trim($("#fecha").val()) != ''){
                $("#form_movimiento").attr("action", "reporte/trazasolicitudes");
                $("#form_movimiento").append("<input type='hidden' name='accion' value='download' id='accion' />");
                $("#form_movimiento").submit();
                $("#accion").remove();
                $("#form_movimiento").attr("action", "");
         }else{
            swal('Seleccione un rango de fechas');
         }      
    };
});
