$(document).ready(function(){
	$('#txt_fecha_filtro').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });
    slctGlobal.listarSlct('recursoofsc', 'slct_bucket_filtro', 'simple');
    $("#btn-buscar").click(AsistenciaReporte.get);
});
AsistenciaReporte.get();
crearVista = function() {
	//$("#t_asistencia").DataTable().destroy();
	eventoCargaMostrar();
	var htmlhead ="";
	var htmlbody = "";
	htmlhead+="<td>Carnet</td>";
	htmlhead+="<td>Carnet</td>";

	
	for(var i in listAsistencias) {
		htmlbody+="<tr>";
			htmlbody+="<td>"+i+"</td>";
			for (var j in listAsistencias[i]) {
				if (listAsistencias[i][j].asistio == 1) {
					htmlbody+="<td>Asistio</td>";
				} else {
					if (listAsistencias[i][j].asistio!=null && listAsistencias[i][j].asistio!="null") {
						htmlbody+="<td>Ausente</td>";
					} else {
						htmlbody+="<td>Ausente</td>";
					}
				}
				
			}
		htmlbody+="</tr>";
	}
	$("#t_asistencia thead").html(htmlhead);
	$("#t_asistencia tbody").html(htmlbody);
	eventoCargaRemover();
	//$("#t_asistencia").DataTable();
}