
addMarker=function(coordy,coordx,label,icon){                    
    var location = new gm.LatLng(coordy, coordx);
    var marker = new MarkerWithLabel({
        position: location,
        icon: icon,
        map: map,
        //labelContent: label,
        //size: new gm.Size(20, 20),
        //labelStyle: {opacity: 1, background: "#fff", padding: "5px"}
    });
    markers.push(marker);
    //bounds.extend(location);
    marker.infowindow = new gm.InfoWindow({content: label});
    
    gm.event.addListener(marker,'click', function() {
        if(infoWindows.length>0){
            for (var j=0;j<infoWindows.length;j++) {
                infoWindows[j].close();
            }
        }
        this.infowindow.open(map,this);
        infoWindows.push(this.infowindow);
    });

    markerSpiderfier.addMarker(marker);
};
removeMarkers = function() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers=[];
};
addLine=function(movimiento, coordy_cliente, coordx_cliente){
    coordx_tecnico = parseFloat(movimiento.coordx_tecnico);
    coordy_tecnico = parseFloat(movimiento.coordy_tecnico);
    var locationTec =  new gm.LatLng(coordy_tecnico, coordx_tecnico);
    var locationCli =  new gm.LatLng(coordy_cliente, coordx_cliente);

    try {line.setMap(null);}catch(c){}
    line = new gm.Polyline({
        path: [
            locationCli,
            locationTec
        ],
        strokeColor: "#EA4444",
        strokeOpacity: 1.0,
        strokeWeight: 7,
        map: map
    });

    var distancia = gm.geometry.spherical.computeDistanceBetween(locationCli,locationTec);
    distancia = distancia.toFixed(2);

    //Centro del polilyne Tecnico - Agenda
    var pathBounds = new gm.LatLngBounds();
    pathBounds.extend( locationCli );
    pathBounds.extend( locationTec );

    var marker = new MarkerWithLabel({
        position: pathBounds.getCenter(),
        map: map,
        labelContent: "<div>"+distancia + " metros.</div>",
        labelAnchor: new gm.Point(22, 0),
        size: new gm.Size(20, 20),
        labelStyle: {opacity: 1, background: "#fff", padding: "10px"}
    });
    markers.push(marker);
};

var PestanaOrden = {
    pintarMapa : function (element) {
        $('#mapa10002').remove();
        $('#contendor_mapa').html('<div id="mapa10002" style="width: 850px; height: 500px;margin-left: 100px;margin-right: 20px;"></div>');
        $('#MostrarMapa').modal();
        try { markerSpiderfier.clearMarkers(); }catch(c){}

        setTimeout(function(){  

        removeMarkers();
        map = new gm.Map(
            document.getElementById("mapa10002"),
            mapOptions
        );
        markerSpiderfier = new OverlappingMarkerSpiderfier(map, spiderConfig);
        
       
        var coordx_cliente = parseFloat($(element).attr("coordx_cli"));
        var coordy_cliente = parseFloat($(element).attr("coordy_cli"));
        var coordx_direcc_tecnico = parseFloat($(element).attr("coordx_direcc_tecnico"));
        var coordy_direcc_tecnico = parseFloat($(element).attr("coordy_direcc_tecnico"));
        var nombre_cliente = ($(element).attr("nombre_cliente"));
        var nombre_tecnico = ($(element).attr("nombre_tecnico")); 
        var carnet = ($(element).attr("carnet"));
        var fecha_val = ($(element).attr("fch_val")); 
        var fecha_agenda=($(element).attr("fch_agenda"));  

        var markerCluster = new MarkerClusterer(map, markers);
        
        //cliente
        icon = {
            path: fontawesome.markers.CALENDAR,
            scale: 0.5,
            strokeWeight: 0.3,
            strokeColor: '#000',
            strokeOpacity: 1,
            fillOpacity: 1
        };
        var label = "<label>-Cliente:"+nombre_cliente+"<br>-Fecha Validacion:"+fecha_val+"<br>-Fecha Agenda:"+fecha_agenda+"</label>";
        addMarker( coordy_cliente, coordx_cliente,label, icon);

        //tenico
        icon = "/img/icons/tec_0e8499.png";
        var label = "<label>-Tecnico: "+(nombre_tecnico).toUpperCase()+"<br>-Carnet :"+(carnet).toUpperCase()+"<br></label>";
        markerCluster.setMaxZoom(config.minZoom);
        addMarker( coordy_direcc_tecnico, coordx_direcc_tecnico,label, icon);
        
        //linea
        addLine({'coordx_tecnico':coordx_direcc_tecnico,'coordy_tecnico': coordy_direcc_tecnico}, coordy_cliente, coordx_cliente)
        //bounds = new gm.LatLngBounds();
        //console.log(limits);
        //map.fitBounds(limits);
        
       }, 1000);
    }
};

function openModalMap(element){
   
    return PestanaOrden.pintarMapa(element);
}