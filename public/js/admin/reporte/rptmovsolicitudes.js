var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 

$(document).ready(function(){
    $('#fecha').daterangepicker({
        format: 'YYYY-MM-DD',
        minDate: moment().subtract(35, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
    });
    slctGlobalHtml('slct_tipo_fecha','simple');
    //Mostrar 
    $("#generar_movimientos").click(function (){
        mostrarMovimiento();
    });
    //iCheck-helper

    $('.iCheck-helper').click(function(){
        selectFiltro();
    });

    $(document).on('click', '#mostrar', function(event) {
        if($.trim($("#fecha").val()) != ''){
            if($.trim($("#slct_tipo_fecha").val()) != ''){
                solicitudesT.listarSolicitudes();
            }else{
                swal('Seleccione campo a buscar');
            }
        }else{
            swal('Seleccione un rango de fechas');       
        }
    });

    DescargarExcel = function(){
         if($.trim($("#fecha").val()) != ''){
            if($.trim($("#slct_tipo_fecha").val()) != ''){
                $("#form_movimiento").attr("action", "reporte/rptsolicitudes");
                $("#form_movimiento").append("<input type='hidden' name='accion' value='download' id='accion' />");
                $("#form_movimiento").submit();
                $("#accion").remove();
                $("#form_movimiento").attr("action", "");
            }else{
                swal('Seleccione campo a buscar');
            }
         }else{
            swal('Seleccione un rango de fechas');
         }      
    };
});
