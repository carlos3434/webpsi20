    var solicitudesT = {
        listarSolicitudes: (function (header) {
            var datos="";
            var targets=0;
            var columnDefs = [
                {
                    "targets": 0,
                    "data": "id_solicitud_tecnica",
                    "name": "id_solicitud_tecnica"
                }
            ];
            /*indexes*/
            if(header){
                $.each(header,function(index, el) {
                    if(el != 'id_solicitud_tecnica'){
                        targets++;
                        columnDefs.push({
                            "targets": targets,
                            "data": el,
                            "name": el
                        });                            
                    }
                });
            }      
            /*end indexes */
            $('#tb_solicitudes').dataTable().fnDestroy();               
            $('#tb_solicitudes')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,
                        "searchable": true,                       
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        "ajax": {
                            url: "reporte/movimientoultimo",
                            type: "POST",
                            cache: false,
                            "data": function(d){
                                var contador=0;
                                datos=$("#form_movimiento").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");

                                for (var i = datos.length - 1; i >= 0; i--) {
                                    if( datos[i].split("[]").length>1 ){
                                        d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                        contador++;
                                    }
                                    else{
                                        d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                                    }
                                };
                            },
                        },
                        columnDefs
                    }); 
        }),
    }