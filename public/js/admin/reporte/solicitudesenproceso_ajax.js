var solicitudesProc = {
    listar: function () {
        var datos="";
        var targets=0;
        //$('#tb_cor_tecnicos').dataTable().fnDestroy();
        $('#tb_gestionesSt').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                solicitudesProc.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.bucket;}},
                {data : function( row, type, val, meta) {return row.appt_number;}},
                {data : function( row, type, val, meta) {return row.xa_identificator_st;}},
                {data : function( row, type, val, meta) {return row.status;}},
                {data : function( row, type, val, meta) {return row.date;}},
                {data : function( row, type, val, meta) {return row.a_control;}},
                {data : function( row, type, val, meta) {return row.estado_actual_cms;}},
                {data : function( row, type, val, meta) {return row.fecha_liquidacion_cms;}},
                {data : function( row, type, val, meta) {return row.rango_fechas;}},
                {data : function( row, type, val, meta) {return row.fecha_creacion;}}
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: false,
            destroy:true
        });
    },
    http_ajax: function(request,callback){
        var rangoFechas =$("#txt_rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        form+="&fecha_inicio="+rangoFechas[0]
        form+="&fecha_fin="+rangoFechas[1];
        form+="&buckets="+$('#slct_buckets').val();
        form+="&tab="+$('#slct_tipo').val();

        eventoCargaMostrar();
        axios.post('solicitudesenproceso/cargar',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    ExportEcxel: function () {
        $("input[type='hidden']").remove();
        $("#form_reporte").append("<input type='hidden' value='Excel' id='tipo' name='tipo'>");
        var rangoFechas =$("#txt_rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");

        empresas=$("#slct_empresas").val();
        actividades=$("#slct_actividades").val();
        buckets=$("#slct_buckets").val();
        tipo=$("#slct_tipo").val();

        $("#form_reporte").append("<input type='hidden' value='"+rangoFechas[0]+"' id='fecha_inicio' name='fecha_inicio'>");
        $("#form_reporte").append("<input type='hidden' value='"+rangoFechas[1]+"' id='fecha_fin' name='fecha_fin'>");
        $("#form_reporte").append("<input type='hidden' value='"+buckets+"' id='buckets' name='buckets'>");    
        $("#form_reporte").append("<input type='hidden' value='"+tipo+"' id='tab' name='tab'>");    
        $("#form_reporte").submit();
    }
}