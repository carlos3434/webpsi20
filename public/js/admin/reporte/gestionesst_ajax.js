var gestionesSt = {
    listar: function () {
        var datos="";
        var targets=0;
        //$('#tb_cor_tecnicos').dataTable().fnDestroy();
        $('#tb_gestionesSt').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                gestionesSt.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.fecha;}},
                {data : function( row, type, val, meta) {return row.legado;}},
                {data : function( row, type, val, meta) {return row.actividad;}},
                {data : function( row, type, val, meta) {return row.cantidad;}},
                {data : function( row, type, val, meta) {

                    return "<a href='" + row.csv_location + "' id='"+row.id+"' class='btn btn-success btn-sm btn-detalle' data-toggle='modal' title='Descargar .xls'><i class='fa fa-file-excel-o'></i></a>";
                }},
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        });
    },
    http_ajax: function(request,callback){
        var rangoFechas =$("#txt_rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&start="+request.start;
        form+="&fecha_inicio="+rangoFechas[0]
        form+="&fecha_fin="+rangoFechas[1];

        eventoCargaMostrar();
        axios.post('gestionesSt/cargar',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    ExportEcxel: function (element) {
        var id=$(element).attr("id");

        $("input[type='hidden']").remove();
        $("#form_reporte").append("<input type='hidden' value='Excel' id='tipo' name='tipo'>");
        $("#form_reporte").append("<input type='hidden' value='"+id+"' id='id' name='id'>");
        $("#form_reporte").submit();
    }
}