    var solicitudesT = {
        listarSolicitudes: (function () {
            var datos="";
            var targets=0;
            var columnDefs = [
                {
                    "targets": 0,
                    "data": "id_solicitud_tecnica",
                    "name": "id_solicitud_tecnica"
                }
            ];
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "orden_trabajo",
                                "name": "orden_trabajo"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "tipo_requerimiento",
                                "name": "tipo_requerimiento"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "tipo_operacion",
                                "name": "tipo_operacion"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "contrata",
                                "name": "contrata"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "quiebre",
                                "name": "quiebre"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "cod_cliente",
                                "name": "cod_cliente"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "peticion",
                                "name": "peticion"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "FFTT",
                                "name": "FFTT"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_registro_cms",
                                "name": "fecha_registro_cms"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_regist_cms",
                                "name": "fecha_regist_cms"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "hora_regist_cms",
                                "name": "hora_regist_cms"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Envio_psi",
                                "name": "Envio_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Fec_envio_psi",
                                "name": "Fec_envio_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "Hora_envio_psi",
                                "name": "Hora_envio_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_cms",
                                "name": "estado_cms"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_registro_psi",
                                "name": "fecha_registro_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_regist_psi",
                                "name": "fecha_regist_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "hora_regist_psi",
                                "name": "hora_regist_psi"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_aseguramiento",
                                "name": "estado_aseguramiento"
                            });
            targets++;
             columnDefs.push({
                                "targets": targets,
                                "data": "descripcion_error",
                                "name": "descripcion_error"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_registro_toa",
                                "name": "fecha_registro_toa"
                            });
            targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "fecha_regist_toa",
                                "name": "fecha_regist_toa"
                            });

             targets++;

            columnDefs.push({
                                "targets": targets,
                                "data": "hora_regist_toa",
                                "name": "hora_regist_toa"
                            });

             targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_toa",
                                "name": "estado_toa"
                            });
             targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "estado_solicitud_tecnica",
                                "name": "estado_solicitud_tecnica"
                            });
             targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "num_requerimiento",
                                "name": "num_requerimiento"
                            });
             targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "cms_to_envio",
                                "name": "cms_to_envio"
                            });
             targets++;
            columnDefs.push({
                                "targets": targets,
                                "data": "cms_to_psi",
                                "name": "cms_to_psi"
                            });
             targets++;
             columnDefs.push({
                                "targets": targets,
                                "data": "cms_to_toa",
                                "name": "cms_to_toa"
                            });
              targets++;
              columnDefs.push({
                                "targets": targets,
                                "data": "psi_to_toa",
                                "name": "psi_to_toa"
                            });
               targets++;
               columnDefs.push({
                                "targets": targets,
                                "data": "tipo_requerimiento",
                                "name": "tipo_requerimiento"
                            });
                targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "cod_motivo_generacion",
                                "name": "cod_motivo_generacion"
                            });
                 targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "segmento",
                                "name": "segmento"
                            });
                 targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "quiebre_id",
                                "name": "quiebre_id"
                            });
                 targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "nombre",
                                "name": "nombre"
                            });
                 targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "apellido",
                                "name": "apellido"
                            });
                targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "fecha_cierre_toa",
                                "name": "fecha_cierre_toa"
                            });
                targets++;
                columnDefs.push({
                                "targets": targets,
                                "data": "fecha_cierre_legado",
                                "name": "fecha_cierre_legado"
                            });


            $('#tb_solicitudes').dataTable().fnDestroy();
            $('#tb_solicitudes')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,
                        "searchable": true,                       
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        "ajax": {
                            url: "reporte/rptsolicitudes",
                            type: "POST",
                            cache: false,
                            "data": function(d){
                                var contador=0;
                                datos=$("#form_movimiento").serialize().split("txt_").join("").split("slct_").join("").split("%5B%5D").join("[]").split("+").join(" ").split("%7C").join("|").split("&");

                                for (var i = datos.length - 1; i >= 0; i--) {
                                    if( datos[i].split("[]").length>1 ){
                                        d[ datos[i].split("[]").join("["+contador+"]").split("=")[0] ] = datos[i].split("=")[1];
                                        contador++;
                                    }
                                    else{
                                        d[ datos[i].split("=")[0] ] = datos[i].split("=")[1];
                                    }
                                };
                            },
                        },
                        columnDefs
                    }); 
        }),
    }