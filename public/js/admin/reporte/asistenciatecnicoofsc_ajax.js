axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common.csrftoken = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
listAsistencias = [];
AsistenciaReporte = {
	get : function() {
		var request = {
    		_token 		: document.querySelector('#token').getAttribute('value'),
    		fecha 		: $("#txt_fecha_filtro").val(),
    		bucket 		: $("#slct_bucket_filtro").val(),
    	};
    	axios.post('asistenciatecnicoofsc/reporte', request).then(response => {
	       	var data = response.data;
	        if (data.rst == "2") {
	        	$('#asistenciaModal').modal("hide");
		    	Psi.sweetAlertError(data.msj);
		   	} else {
		   		listAsistencias = data.obj;
		   		console.log(listAsistencias);
		   		crearVista();
		    	//listar();
		   	}
	    }).catch(e => {
	    	console.log(e);
	        Psi.sweetAlertError("Error al Intentar consultar Detalle");
	        eventoCargaRemover();
	    }).then(() => {
	    });
	}
}