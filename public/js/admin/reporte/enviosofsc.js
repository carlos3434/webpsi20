$(document).ready(function() {
    slctGlobalHtml('slct_tipoaccion','multiple');

    $('#myTab a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
      //cuadno hace click en lista actualizar los datos , si hace click en tab TABLA reiniar filtros
    });

    $("#fecha_calen").daterangepicker({
        format: 'YYYY-MM-DD',
        endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
        },function (startDate, endDate, period) {
            var diff = daydiff(parseDate(startDate.format('L')), parseDate(endDate.format('L')))
            if(diff > 34){
                var last = parseDate(startDate.format('L'));
                last.setDate(last.getDate() + 31);
                
                $('#fecha_calen').val(
                    moment(startDate).format("YYYY-MM-DD") + ' - ' +
                    moment(last).format("YYYY-MM-DD")
                );               
                alert('El rango es maximo de 31 días');          
            }
    });

    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[0]-1, mdy[1]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    $("#mostrar").click(validarInicio);

    $('#t_enviosofs tbody ').on('click', 'button', function (event) {

       //var table = $('#t_enviosofs').DataTable();
       var envio = $(this).attr('enviado');
       var respuesta = $(this).attr('respuesta');

        //var row = $(this).closest("tr").get(0);
        //var aData = table.row(row).data();
     
         $.ajax({
            url         : 'enviosofsc/listardetalle',
            type        : 'POST',
            cache       : false,
            contentType : "application/x-www-form-urlencoded",
            data    : {
                e          : envio,
                r          : respuesta
            },         
            success : function(obj) {
                    $('#ItemPopup').modal('show');
                    $("#cmodal").html(obj);
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });  


        event.stopImmediatePropagation();  //prvents the other on click from firing that fires up the inline editor
    });  
});
/*
$.extend( true, $.fn.dataTable.defaults, {
    "language": {
        "lengthMenu": "Mostrar _MENU_ Registros por página",
        "zeroRecords": "Registros No encontrados",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay Registros disponibles",
        "search":         "Buscar:",
        "infoFiltered": "(Filtrado desde _MAX_ total de registros)",
        "paginate": {
        "first":      "Primero",
        "last":       "Ultimo",
        "next":       "Siguiente",
        "previous":   "Anterior"
    },
    }
    
} );*/

validarInicio = function () {
    if ( $.trim($("#fecha_calen").val()) === '') {
        alert('Seleccione la Fecha');
        
        // if($.trim($("#fecha_recepcion_ini").val()) == '' && $.trim($("#fecha_recepcion_fin").val()) == '') $("#fecha_recepcion_ini").focus();
        // if($.trim($("#fecha_recepcion_ini").val()) == '' && $.trim($("#fecha_recepcion_fin").val()) != '') $("#fecha_recepcion_ini").focus();
        // if($.trim($("#fecha_recepcion_ini").val()) != '' && $.trim($("#fecha_recepcion_fin").val()) == '') $("#fecha_recepcion_fin").focus();

    } else {
        EnviosOfsc.listar();            
    }
};