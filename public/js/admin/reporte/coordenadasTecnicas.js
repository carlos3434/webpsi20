$(document).ready(function(){
    solicitudesT.listar();
    $('#rangoFechas').daterangepicker(
     {format: 'YYYY-MM-DD'});

    slctGlobal.listarSlct('usuario','slct_buckets','multiple',null);
    slctGlobal.listarSlct('coordenadasTecnicas','slct_empresas','multiple',null,{'tipo':'empresas'});
    slctGlobal.listarSlct('coordenadasTecnicas','slct_actividades','multiple',null,{'tipo':'actividades'});
});

$(document).on('click', '#btnExportExcel', function(e){
	var fechas=$('#rangoFechas').val();
	if(fechas==''){swal("Mensaje",'Seleccione un rango de fechas!');return false;}
   solicitudesT.ExportEcxel();
});

$(document).on('click', '#btnBuscar', function(e){
	var fechas=$('#rangoFechas').val();
	if(fechas==''){swal("Mensaje",'Seleccione un rango de fechas!');return false;}
   solicitudesT.listar();
});


