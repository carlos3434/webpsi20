axios.defaults.headers.common.token = document.querySelector('#token').getAttribute('value');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'; 
var recursosVsActividad = {
	CargarRecursos: function(){
		$("#tb_recursos").dataTable({
			"processing": false,
            "serverSide": false,
            "stateSave": false,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                recursosVsActividad.http_ajaxRecursos(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.bucket;}, name:'bucket'},
                {data : function( row, type, val, meta) {return row.tecnico;}, name:'tecnico'},
                {data : function( row, type, val, meta) {return row.estatus;}, name:'estatus'},
                {data : function( row, type, val, meta) {return row.tipo;}, name:'tipo'},
                {data : function( row, type, val, meta) {return row.celular;}, name:'celular'},  

            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
		})
	},
	http_ajaxRecursos: function(request,callback){
        
        var contador = 0;         
        var form = $('#form_buscarRecursos').serialize().split('txt_').join("").split('slct_').join("");             
        eventoCargaMostrar();
        axios.post('recursosVsActividad/listarrecursos',form).then(response => {
            callback(response.data);
        }).catch(e => {            
        }).then(() => {
            eventoCargaRemover();
        });
    },

    getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {

            var obj = response.data;
            
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
                //console.log(obj.data[i]);
                //vm.lista[ selects[i] ] = obj.data[i];

            }
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },

    CargarTecnicos: function(){
		$("#tb_tecnico").dataTable({
			"processing": false,
            "serverSide": false,
            "stateSave": false,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                recursosVsActividad.http_ajaxTecnicos(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.bucket;}, name:'bucket'},
                {data : function( row, type, val, meta) {return row.nombre_tecnico;}, name:'nombre_tecnico'},
                {data : function( row, type, val, meta) {return row.resource_id;}, name:'resource_id'},
                {data : function( row, type, val, meta) {return row.fecha_activacion;}, name:'fecha_activacion'},
                {data : function( row, type, val, meta) {return row.programado;}, name:'programado'},
                {data : function( row, type, val, meta) {return row.estado;}, name:'estado'},
                {data : function( row, type, val, meta) {return row.cliente;}, name:'cliente'},
                {data : function( row, type, val, meta) {return row.direccion;}, name:'direccion'},
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
		})
	},
	http_ajaxTecnicos: function(request,callback){
        var contador = 0;
        var form = $('#form_buscarTecnico').serialize().split('txt_').join("").split('slct_').join("");             
        eventoCargaMostrar();
        axios.post('recursosVsActividad/listartecnico',form).then(response => {
            callback(response.data);
        }).catch(e => {
        }).then(() => {
            eventoCargaRemover();
        });
    },

    CargarCantidad: function(){
    	$("#tb_cantidad").dataTable({
			"processing": false,
            "serverSide": false,
            "stateSave": false,
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                recursosVsActividad.http_ajaxCantidad(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.bucket;}, name:'bucket'},
                {data : function( row, type, val, meta) {return row.resource_id;}, name:'resource_id'},
                {data : function( row, type, val, meta) {return row.nombre_tecnico;}, name:'nombre_tecnico'},
                {data : function( row, type, val, meta) {return row.cantidad;}, name:'cantidad'},
                {data : function( row, type, val, meta) {return row.programada;}, name:'programada'},
                {data : function( row, type, val, meta) {return row.noprogramada;}, name:'noprogramada'},              
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            order: [[ 0 , "desc" ]],
            info: true,
            autoWidth: true,
            destroy:true
		})
    },

    http_ajaxCantidad: function(request,callback){
        var contador = 0;
        var form = $('#form_buscarCantidad').serialize().split('txt_').join("").split('slct_').join("");             
        eventoCargaMostrar();
        axios.post('recursosVsActividad/listarcantidad',form).then(response => {
            callback(response.data);
        }).catch(e => {
        }).then(() => {
            eventoCargaRemover();
        });
    },
}