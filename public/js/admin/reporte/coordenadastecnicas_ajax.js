var gm = google.maps;

var config = {
    el: 'mapafftts',
    lat: -12.109129,
    lon: -77.016123,
    zoom: 15,
  
    minZoom: 15,
    type: gm.MapTypeId.ROADMAP
};

var spiderConfig = {
    keepSpiderfied: true,
    event: 'mouseover'
};

var map=[];
var markers=[];
var bounds;
var line;
var nomarkers;
var infoWindows = [];
var mapOptions = {
    center: new gm.LatLng(config.lat, config.lon),
    zoom: config.zoom,
    mapTypeId: config.type
};
var markerSpiderfier;
var solicitudesT = {
    listar: function () {
        var datos="";
        var targets=0;
        //$('#tb_cor_tecnicos').dataTable().fnDestroy();
        $('#tb_cor_tecnicos').dataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,                
            "stateLoadCallback": function (settings) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            "stateSaveCallback": function (settings) {
                $(".overlay,.loading-img").remove();
            },
            ajax: function(data, callback, settings) {
                solicitudesT.http_ajax(data,callback);
            },
            "columns":[
                {data : function( row, type, val, meta) {return row.nombre_tecnico;}},
                {data : function( row, type, val, meta) {return row.sms;}},
                {data : function( row, type, val, meta) {return row.distancia;}},
                {data : function( row, type, val, meta) {return row.num_requerimiento;}},
                {data : function( row, type, val, meta) {return row.solicitud_tecnica_id;}},
                {data : function( row, type, val, meta) {return row.bucket;}},
                {data : function( row, type, val, meta) {return row.actividad;}},
                {data : function( row, type, val, meta) {return row.fecha_validacion;}},
                {data : function( row, type, val, meta) {
                //grillaObj[indice] = row;
                    htmlButtons ="<a onclick='openModalMap(this)' fch_agenda='"+row.fecha_agenda+"' fch_val='"+row.created_at+"' carnet='"+row.carnet+"' coordx_cli='"+row.coordx_cliente+"' coordy_cli='"+row.coordy_cliente+"' coordx_direcc_tecnico='"+row.coordx_tecnico+"' coordy_direcc_tecnico='"+row.coordy_tecnico+"' nombre_cliente='"+row.nom_cliente+"' nombre_tecnico='"+row.nombre_tecnico+" ' class='btn btn-primary btn-xs btn-detalle' data-toggle='modal' title='Detalle Solicitud' ";
                    htmlButtons+=" data-solicitudtecnicaid='"+row.solicitud_tecnica_id+"'";
                    htmlButtons+=" data-actividadid='"+row.actividad_id+"'>";
                    htmlButtons+="<i class='fa fa-edit fa-xs'></i></a>";
                    return htmlButtons;
                }},
            ],
            paging: true,
            lengthChange: false,
            searching: false,
            ordering: true,
            order: [[ 0 , "asc" ]],
            info: true,
            autoWidth: true,
            destroy:true
        }); 
    },
    modalOpenCoordendas: function(button) {
        
        eventoCargaMostrar();
        axios.post('coordenadasTecnicas/datasolicitud',request).then(response => {
           
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            //eventoCargaRemover();
        });
    },       
    http_ajax: function(request,callback){
        var contador = 0;
        var rangoFechas =$("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");   
        var form = $('#form_buscar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;
        form+="&fecha_inicio="+rangoFechas[0]
        form+="&fecha_fin="+rangoFechas[1];

        eventoCargaMostrar();
        axios.post('coordenadasTecnicas/cargar',form).then(response => {
            callback(response.data);
            //console.log(response);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    ExportEcxel: function () {
        $("input[type='hidden']").remove();
        $("#form_reporte").append("<input type='hidden' value='Excel' id='tipo' name='tipo'>");
        var rangoFechas =$("#rangoFechas").val();
        var rangoFechas = rangoFechas.split(" - ");

        empresas=$("#slct_empresas").val();
        actividades=$("#slct_actividades").val();
        buckets=$("#slct_buckets").val();

        $("#form_reporte").append("<input type='hidden' value='"+rangoFechas[0]+"' id='fch_inicio' name='fch_inicio'>");
        $("#form_reporte").append("<input type='hidden' value='"+rangoFechas[1]+"' id='fch_fin' name='fch_fin'>");
        $("#form_reporte").append("<input type='hidden' value='"+empresas+"' id='empresas' name='empresas'>");
        $("#form_reporte").append("<input type='hidden' value='"+actividades+"' id='actividades' name='actividades'>");
        $("#form_reporte").append("<input type='hidden' value='"+buckets+"' id='buckets' name='buckets'>");    
        $("#form_reporte").submit();
    }
}