var route = 'conformidad/';
var Conformidad = {
    subirArchivo: function (data) {
        $.ajax({
            url: route + 'procesouno',
            type: "POST",
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {

                eventoCargaRemover();
                if (obj.rst == 1) {
                    
                    swal({
                        title: obj.msj,
                        text: 'Clic en "Continuar" para subir los pedidos',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Continuar',
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            Conformidad.procesarArchivo();
                        } else {
                            swal("Cancelado", "No se completo la subida masiva.", "warning");
                        }
                        $("#archivocolumna").fileinput('reset');
                    });

                } else {
                    swal('Mensaje',obj.msj,'error');
                }

            },
            error: function () {
                eventoCargaRemover();
                swal('Error','Ocurrió un error en la carga.','error');
            }
        });
    },
    procesarArchivo: function () {
        $.ajax({
            url: route + 'procesar-archivo-columna',
            type: "POST",
            data: '',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    alert = 'success';
                }
                swal('Mensaje',obj.msj,alert);
            }
        });
    },
    getMasiva: function () {
        $.ajax({
            url: route + 'masivas',
            type: "POST",
            data: '',
            cache: false,
            dataType: 'json',
            beforeSend: function () {
               $('.btncarga').html('Cargando...').prop('disabled', true);
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                   poblateMasivo(obj.data);
                   $('.btncarga').html('Listo').prop('disabled', true);
                }
            }
        });
    },
    procesarMasivo: function (data,evento_id ='') {
        console.log(route);
        $.ajax({

            url: route + 'procesarexcel',
            type: "POST",
            data: {
                pedidos : data,
                evento_id : evento_id
                },
            cache: false,
            dataType: 'json',
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    var html = '';
                    html+="Registrados:"+obj.registrados+", Fallidos:"+obj.fallo;
                    alert = 'success';
                    $("#tblData tbody").html('');
                    $(".informacion").text(html);
                    $(".informacion").removeClass('hidden');
                }
                swal('Mensaje',obj.msj,alert);
            }
        });
    },
    getInformation: function (url,name,data ='') {
        $.ajax({
            url: url,
            type: "POST",
            cache: false,
            data: data,
            dataType: 'json',
            beforeSend: function () {
                eventoCargaMostrar();
            },
            success: function (obj) {
                alert = 'error';
                eventoCargaRemover();
                if (obj.rst == 1) {
                    cargarHTML(obj.datos,name);
                }
            }
        });
    }
};
