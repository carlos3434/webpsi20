var permisoGlobal = 0;

$(document).ready(function(){
        var ids = '';
        $("#btn_gestion_modal").click(gestionModal);

        $('#pretemporalModal').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('id');
            Pretemporales.ValidarPermiso(id);
            Pretemporales.Cargar(id);
            Pretemporales.Movimiento(id);
            Pretemporales.ValidarLegados(id);
            ids = id;
            $("#txt_observacion_modal").val('');
            $("#txt_codactu_modal").prop('readonly', true);
        });

        $('#pretemporalModal').on('hide.bs.modal', function (event) {
            if (permisoGlobal == 0) {
                Pretemporales.FinalizarPermiso(ids, permisoGlobal);
            }
            $("#tab_gestion_modal").css("display","none");
            $("#tab_datos_averia_modal").css("display","none");
            $("#content_averia").css('display', 'none')
            $('#tab_1, #tab_2,#tab_3,#tab_4').removeClass("active");
            $('.tab_1, .tab_2,.tab_3,.tab_4').removeClass("active");
            $("#form_bandeja .solucionado").css('display', 'none');
            //$("#slct_solucionado_modal").multiselect('destroy');

            $("#averiaForm input[type='text']").val('');
        });
});

 validacionCod = function(){
        var tipoNegocio = $(".tipo_averia_modal").val();
        if(tipoNegocio == 4){ //televisor
             $("#txt_codactu_modal").inputmask("99999999");
         }else if(tipoNegocio == 5 || tipoNegocio == 6){ //telefono
            if(isNaN(parseInt($("#txt_codactu_modal").val().substring(0, 3)))){ //if its string
                $("#txt_codactu_modal").inputmask("[aaa]9999999")
            }else{ //its number
                $("#txt_codactu_modal").inputmask("99999999")
            }                           
        }
    }
    

    gestionModal=function(){
        codactu = $.trim($("#txt_codactu_modal").val());
        if (codactu.indexOf('_') > -1){
            codactu = codactu.replace("_","");            
        }

        if( $("#slct_motivo_modal").val()==='' ){
            swal("Alerta", "Seleccione Motivo","warning");
        } else if( $("#slct_submotivo_modal").val()==='' ){
            swal("Alerta", "Seleccione Sub Motivo","warning");
        } else if( $("#slct_estado_modal").val()==='' ){
            swal("Alerta", "Seleccione Estado","warning");
        } 
        else if( $.trim($("#txt_observacion_modal").val())==='' ){
            swal("Alerta", "Ingrese una observación","warning");
        }
        else if($("#slct_submotivo_modal").val()==60 && $.trim($("#txt_codactu_modal").val()) === '' || 
            $("#slct_submotivo_modal").val()==75 && $.trim($("#txt_codactu_modal").val()) === ''){                  
            swal("Alerta", "Ingrese el Código de Avería","error");
        }else if(($("#slct_submotivo_modal").val()==60 || $("#slct_submotivo_modal").val()==75) && ($(".tipo_averia_modal").val() == 4 && $.trim(codactu).length < 8)){
               swal('Se requiere 8 digitos como codigo de averia'); 
        }else if(($("#slct_submotivo_modal").val()==60 || $("#slct_submotivo_modal").val()==75) && ($(".tipo_averia_modal").val() == 5 || $(".tipo_averia_modal").val() == 6)){ //telefono
            if(isNaN(parseInt($("#txt_codactu_modal").val().substring(0, 3))) && $.trim(codactu).length < 10){ //if its string
                swal('Se requiere 10 digitos como codigo de averia'); 
            }else if(!isNaN(parseInt($("#txt_codactu_modal").val().substring(0, 3))) && $.trim(codactu).length < 8){ //its number
                swal('Se requiere 8 digitos como codigo de averia'); 
            }else{
                Pretemporales.guardarMovimiento();
            }
        }else{ 
            if($("#slct_estado_modal").val() == 17) {
            swal({
                    title: "Se guardará con estado Cerrado",
                    text: "La gestión del ticket ya no podrá realizar nuevos movimientos.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Continuar',
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        Pretemporales.guardarMovimiento();
                    } else {
                        swal("Cancelado", "No se realizo ninguna operación", "error");
                    }
                });
            } else if ($("#slct_motivo_modal").val() == 24 && $("#slct_submotivo_modal").val() == 59) {
                if ($("input[name='rad_codactu_val']:radio").is(':checked')) {
                    Pretemporales.guardarMovimiento();
                } else swal("Alerta", "Seleccione la fila con la avería correspondiente.", "warning");
            } else {
                Pretemporales.guardarMovimiento();
            }
        }
    };

    validaPermiso = function() {
        if (permisoGlobal == 0) {
            $("#tab_1 #btn_gestion_modal").attr("disabled", false);
        } else {
            var valores = permisoGlobal.split("|");
            $("#tab_1 #btn_gestion_modal").attr("disabled", true);
            swal("Alerta", "Ticket ya está siendo trabajado por el usuario "+valores[1], "warning");
        }
    };

