$(document).ready(function() {
    var data={};var data2={};
    data = { requerimiento: '0-0', mas: '1' };
    data2 = { requerimiento: '0-0', mas: '1', quiebre_id: '46,55', interface: 'pretemporal' };
    var ids = [];
    slctGlobal.listarSlct('motivo','slct_motivo','multiple',ids,data2,0,'#slct_submotivo,#slct_estado','M');
    slctGlobal.listarSlct('submotivo','slct_submotivo','multiple',ids,data,1,'#slct_estado','S','slct_motivo','M');
    slctGlobal.listarSlct('listado', 'slct_quiebre', 'multiple', null);

    slctGlobalHtml('slct_filtro','simple');
    $('#fecha_timefrom').daterangepicker({
        format: 'YYYY-MM-DD'
    });

    $("#fecha_registro").daterangepicker({
        format: 'YYYY-MM-DD',
        endDate: moment(),
        startDate: moment(),
        maxDate:  moment()
    });

     $('#fecha_registro').val(
        moment().format("YYYY-MM-DD") + ' - ' +
        moment(new Date()).format("YYYY-MM-DD")
    );

    $(".excel-tickets").click(function(){
        $("#form_excel_actividades").append("<input type='hidden' name='flag' id='flag' value='1' />");
        $("#form_excel_actividades").submit();
        $("#flag").remove();
        return false;
    });

    $(".excel-movimientos-tickets").click(function(){
        $("#form_excel_actividades").append("<input type='hidden' name='flag' id='flag' value='2' />");
        $("#form_excel_actividades").submit();
        $("#flag").remove();
        return false;
    });

    $(".csv-movimientos-tickets").click(function(){
        $("#form_excel_actividades").append("<input type='hidden' name='flag' id='flag' value='2' />");
        $("#form_excel_actividades").append("<input type='hidden' name='csv' id='csv' value='1' />");
        $("#form_excel_actividades").submit();
        $("#flag").remove();
        return false;
    });

    Reporte.grafica(1);
});

graficarPie = function(data) {
        total = data.anterior + data.hoy;
        $('#grafica-legados').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: data.titulo
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Pedidos',
                colorByPoint: true,
                data: [{
                    name: 'Mas de 1 Dia ('+(parseInt(data.anterior/total))+')',
                    y: parseFloat(data.anterior/total)*100
                }, {
                    name: 'Ultimas 24 Horas ('+(parseInt(data.hoy/total))+')',
                    y: parseFloat(data.hoy/total)*100,
                    sliced: true,
                    selected: true
                }]
            }]
    });
};
