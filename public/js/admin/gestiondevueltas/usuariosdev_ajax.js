var zonales_selec=[],usuario_id, modulos_selec=[], UsuarioObj;
var Usuario={
     listar: (function () {
            var datos="";
            var targets=0;
            $('#t_usuarios').dataTable().fnDestroy();
            $('#t_usuarios')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            Usuario.http_ajax(data,callback);
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {                              
                                if (typeof row.apellido!="undefined" && typeof row.apellido!=undefined) {
                                    return row.apellido;
                                } else return "";
                            }, name:'apellido'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.nombre!="undefined" && typeof row.nombre!=undefined) {
                                    return row.nombre;
                                } else return "";
                            }, name:'nombre'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.usuario!="undefined" && typeof row.usuario!=undefined) {
                                    return row.usuario;
                                } else return "";
                            }, name:'usuario'},
                             {data : function( row, type, val, meta) {
                                boton_estado='<span id="'+row.id+'" onClick="cambiarEstado('+row.usuario_id +', 0)" class="btn btn-success send_val_prop">Activo</span>';
                                if(row.estado_asignacion == 0){
                                    boton_estado='<span id="'+row.id+'" onClick="cambiarEstado('+row.usuario_id +', 1)" class="btn btn-danger send_val_prop">Inactivo</span>';
                                }                  
                                return boton_estado;
                            }, name:'estado'}                            
                        ],
                        paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),      
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#form_exportar').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('gestordevueltas/listar',form).then(response => {           
            callback(response.data);
            //if (response.data.rst==2) {
            //    Psi.sweetAlertError(response.data.msj);
            //} else {
            //    
            //}
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    }, 
    CargarUsuarios:function(evento){
        $.ajax({
            url         : 'gestordevueltas/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                slctGlobal.listarSlct('modulo','slct_modulos','simple');//para que cargue antes el modulo
            },
            success : function(obj) {
                if(obj.rst==1){
                    HTMLCargarUsuario(obj.datos);
                    UsuarioObj=obj.datos;
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);

            }
        });
    },

    CambiarEstadoUsuarios:function(id,AD){
        $("#form_gestion").append("<input type='hidden' value='"+id+"' name='user_id'>");
        $("#form_gestion").append("<input type='hidden' value='"+AD+"' name='estado'>");
        var datos=$("#form_gestion").serialize().split("txt_").join("").split("slct_").join("");
        $.ajax({
            url         : 'gestordevueltas/cambiarestado',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    $('#t_usuarios').dataTable().fnDestroy();
                    Usuario.listar();

                    $('#usuarioModal .modal-footer [data-dismiss="modal"]').click();
                    Psi.mensaje('success', obj.msj, 6000);
                }
                else{ 
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display','');
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);

            }
        });
    },
    Asignar:function(evento){
        $.ajax({
            url         : 'gestordevueltas/reasignar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                slctGlobal.listarSlct('modulo','slct_modulos','simple');//para que cargue antes el modulo
            },
            success : function(obj) {                
                var html = '';
                html+='Asignados:'+obj.asignados;
                html+=', Quedan Pendientes:'+obj.pendientes;
                Psi.mensaje('Mensaje','Asignados:'+html,'success');
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
};
