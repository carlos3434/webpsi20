var submodulos_id=[];
$(document).ready(function() {
    //slctGlobal.listarSlctpost('gestordevueltas', 'listarusuarios','slct_usuario','multiple',null,{libres:1});
    $(document).on('change', '#slct_tiporea', function(event){
        if($(this).val() == 1) {
            $('.rea_usuario').removeClass('hidden');
        } else{
            $('.rea_usuario').addClass('hidden');
        }
    });
    
    $(document).on('click','.btnAsignar',function(event){
        Usuario.Asignar();
    });

    activarTabla=function(){
        $('#t_usuarios thead th').each( function () {
            var title = $('#t_usuarios thead th').eq( $(this).index() ).text();
            if (title=='Perfil' ) {
                $(this).html( '<input id="filter_perfil" type="text" placeholder="Buscar '+title+'" />' );
            } else if ( title=='Empresa'){
                $(this).html( '<input id="filter_empresa" type="text" placeholder="Buscar '+title+'" />' );
            }
        } );
        $("#t_usuarios").dataTable();
    };

    Usuario.listar(activarTabla);

    HTMLCargarUsuario=function(datos){
        var html="";
        $('#t_usuario').dataTable().fnDestroy();
        //PRIVILEGIO AGREGAR
        
        $.each(datos,function(index,data){//UsuarioObj
            boton_estado='<span id="'+data.id+'" onClick="cambiarEstado('+data.usuario_id +', 0)" class="btn btn-success send_val_prop">Activo</span>';
            if(data.estado_asignacion == 0){
                boton_estado='<span id="'+data.id+'" onClick="cambiarEstado('+data.usuario_id +', 1)" class="btn btn-danger send_val_prop">Inactivo</span>';
            }

            html+="<tr>"+
                "<td>"+data.apellido+"</td>"+
                "<td>"+data.nombre+"</td>"+
                "<td>"+data.usuario+"</td>"+
                "<td>"+boton_estado+"</td>";
            html+="</tr>";
        });
        $("#tb_usuarios").html(html);          
        activarTabla();
    };
    cambiarEstado = function(id, estado ){
        Usuario.CambiarEstadoUsuarios(id, estado);
    }

    descargarReporte = function () {   
        $("#form_exportar").append("<input type='hidden' name='accion' id='accion' value='accion'>");
        $("#form_exportar").submit();
    };
});

