    var Datatable;
    var solicitudesT = {
        listar: (function () {
            var datos="";
            var targets=0;
            $('#tb_tipificacion').dataTable().fnDestroy();
            $('#tb_tipificacion')
                    .on('page.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('search.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .on('order.dt', function () {
                        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                    })
                    .dataTable({
                        "processing": true,
                        "serverSide": true,
                        "stateSave": true,                    
                        "stateLoadCallback": function (settings) {
                            $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                        },
                        "stateSaveCallback": function (settings) {
                            $(".overlay,.loading-img").remove();
                        },
                        ajax: function(data, callback, settings) {
                            solicitudesT.http_ajax(data,callback);
                        },
                        "columns":[
                            {data : function( row, type, val, meta) {
                                if (typeof row.id!="undefined" && typeof row.id!=undefined) {
                                    return row.id;
                                } else return "";
                            }, name:'id'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.nombre!="undefined" && typeof row.nombre!=undefined) {
                                    return row.nombre;
                                } else return "";
                            }, name:'nombre'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.estado_ofsc!="undefined" && typeof row.estado_ofsc!=undefined) {
                                    return row.estado_ofsc;
                                } else return "";
                            }, name:'estado_ofsc'},
                            {data : function( row, type, val, meta) {
                                if (typeof row.estado_st!="undefined" && typeof row.estado_st!=undefined) {
                                    return row.estado_st;
                                } else return "";
                            }, name:'estado_st'},
                             {data : function( row, type, val, meta) {
                                htmlEstado ='<span class="btn btn-danger btn-md" estado_asignacion_id="'+row.id+'" estado="1" onclick="cambiarestado(this)">Inactivo</span>';
                                if(row.estado == 1){
                                    htmlEstado ='<span class="btn btn-success btn-md" estado_asignacion_id="'+row.id+'" estado="0" onclick="cambiarestado(this)">Activo</span>';
                                }                            
                                return htmlEstado;
                            }, name:'estado'},
                            {data : function( row, type, val, meta) {
                                  if(typeof meta == "undefined") {
                                    indice = -1;
                                } else {
                                    indice = meta.row;
                                }                                
                                htmlButtons = "<a class='btn btn-primary btn-md btn-detalle' data-toggle='modal' title='Editar' ";
                                htmlButtons+=" data-target='#nuevaTipificacion' data-id='"+row.id+"'>";
                                htmlButtons+=" <i class='glyphicon glyphicon-pencil'></i></a>";
                                return htmlButtons;
                            }, name:'botones'}
                        ],
                          paging: true,
                        lengthChange: false,
                        searching: false,
                        ordering: true,
                        order: [[ 0 , "asc" ]],
                        info: true,
                        autoWidth: true                         
                    }); 
        }),       
        CambiarEstado: function(data){
        $.ajax({
            url         : 'estadoasignacion/actualizar',
            type        : 'POST',
            cache       : false,
            data        : data,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                   solicitudesT.listar();
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
   getSelects: function (selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones,es_lista) {
        var request = {
            selects         : JSON.stringify(selects),
            datos           : JSON.stringify(data)
        };
        eventoCargaMostrar();
        axios.post('listado_lego/selects',request).then(response => {
            var obj = response.data;
            for(i = 0; i < obj.data.length; i++){
                obj.datos = obj.data[i];
                if (es_lista[i]==true) {
                    htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
                }
            }
        }).catch(e => {
            vm.errors=e;
        }).then(() => {
            $(".overlay,.loading-img").remove();
        });
    },
    guardar: function(tipo){
        var datos=$("#form_estadoAsignacion").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
        url = "estadoasignacion/create";
        if(tipo == 2){
            url = "estadoasignacion/actualizar";
        }
        $.ajax({
            url         : url,
            type        : 'POST',
            cache       : false,
            data        : datos,
            dataType    : 'json',
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    $("#nuevaTipificacion").modal('hide');
                    solicitudesT.listar();
                }else{
                     swal("Mensaje",obj.msj,"warning");
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error")
            }
        });
    }, 
    http_ajax: function(request,callback){
        var contador = 0;
        var form = $('#formEstadoAsignacion').serialize().split('txt_').join("").split('slct_').join("");
        var order = request.order[0];
        form+='&column='+request.columns[ order.column ].name;
        form+='&dir='+order.dir;
        form+="&per_page="+request.length;
        form+="&page="+(request.start+request.length)/request.length;

        eventoCargaMostrar();
        axios.post('estadoasignacion/cargar',form).then(response => {
            callback(response.data);
        }).catch(e => {
            //vm.errors=e;
        }).then(() => {
            eventoCargaRemover();
        });
    },
    getEstado : function(data){
        $.ajax({
            url         : 'estadoasignacion/cargar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : data,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.length > 0){                   
                    $("#txt_nombre").val(data[0].nombre);
                    $("#slct_estadoofsc").val(data[0].estado_ofsc_id);
                    $("#slct_estadost").val(data[0].estado_aseguramiento_id);
                    $("#slct_estado").val(data[0].estado);
                    $("#form_estadoAsignacion select").multiselect('refresh');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    }
}