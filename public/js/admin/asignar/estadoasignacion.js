var filtro_fecha, filtro_averia, fecha_ini, fecha_fin, file; 
var ids=0;
$(document).ready(function(){
    Datatable = solicitudesT.listar();  

    slctGlobalHtml('slct_estado','simple');
    var selects = ["estadoofsc", "estadoaseguramiento"];
    var slcts = ["slct_estadoofsc", "slct_estadost"];
    var tipo = ["simple", "simple"];
    var valarray = [null, null];
    var data = [{}, {}];
    var afectado = [0, 0];
    var afectados = ["", ""];
    var slct_id = ["", ""];
    var es_lista =[true,true];
    solicitudesT.getSelects(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id,{},{},{}, es_lista);
   
    $('#nuevaTipificacion').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');   
        if(!id){
            $("#btn_guardar").attr('onclick','guardar()');
           $("#slct_estado option[value=1]").prop('selected',true);
        }else{
            $("#btn_guardar").attr('onclick','editar()');
            $("#form_estadoAsignacion").append('<input type="hidden" value="'+id+'" name="txt_estado_asignacion_id" id="txt_estado_asignacion_id">');
        }       
    });

    $('#nuevaTipificacion').on('hide.bs.modal', function (event) {
        $("#form_estadoAsignacion input[type='hidden'],#form_estadoAsignacion input[type='password'],#form_estadoAsignacion input[type='text'],#form_estadoAsignacion input[type='file'],#form_estadoAsignacion select,#form_estadoAsignacion textarea").not('.mant').val('');
        $("#form_estadoAsignacion #txt_estado_asignacion_id").remove();
        $("#form_estadoAsignacion select").multiselect('refresh');       
    });

    cambiarestado = function(obj){
        var id = obj.getAttribute('estado_asignacion_id');
        var estado = obj.getAttribute('estado');
        if(id && estado){
            solicitudesT.CambiarEstado({estado_asignacion_id:id,estado:estado});
        }
    }

    $(document).on('click', '.btn-detalle', function(event) {
        var id = $(this).attr('data-id');
        if(id){
            solicitudesT.getEstado({estado_asignacion_id:id});
        }else{
            swal('Error al mostrar detalle');
        }
    });

    guardar = function(){
        if($("#txt_nombre").val() == ''){
            swal('Ingrese Nombre');
        }else if($.trim($("#slct_estadoofsc").val()) == ''){
            swal('Seleccione Estado Ofsc');
        }else if($.trim($("#slct_estadost").val()) == ''){
            swal('Seleccione Estado Aseguramiento');
        }else if($.trim($("#slct_estado").val()) == ''){
            swal('Seleccione estado');
        }else{
            solicitudesT.guardar(1);            
        }
    }

    editar = function(){        
        solicitudesT.guardar(2);
    }
});
