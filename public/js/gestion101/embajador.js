var vm = new Vue({
    http: {
      root: '/embajador',
      headers: {
        'csrftoken': document.querySelector('#token').getAttribute('value')
      }
    },
    el: '#embajador',
    data: {
        ficha:{},
        datos_averia: false,
        datos_ficha: false,
        modal:{},
        gestion:'embajador',
    },
    methods: {
        Busqueda: function() {
            if (this.tipo_busqueda != '') {
                var request = {
                    tipo: this.tipo_busqueda,
                    busqueda: this.busqueda,
                    gestion: this.gestion,
                };
                this.eventoCargaMostrar();
                this.$http.get('buscarembajador', request, function (response) {
                    if (response.rst==1) {
                        this.$set('datos', response.datos);
                    } else {
                        this.$set('datos', 0);
                        swal('Mensaje',response.msj,'warning');
                    }
                    this.eventoCargaRemover();
                }).error(function(errors) {
                    alert('error');
                }); 
            } else swal('Mensaje','Seleccione tipo de búsqueda','info');
        },
        Cargar: function (ticket) {
            var request = {
                tipo: 'ticket',
                busqueda: ticket
            };
            this.modal={};
            this.eventoCargaMostrar();
            this.$http.get('cargar', request, function (response) {
                if (response.rst==1) {
                    this.datos_averia = false;
                    this.datos_ficha = false;
                    if (response.ficha.length != 0){
                        this.modal=response.ficha;
                        this.datos_ficha = true;
                        if (response.estado_legado!=undefined) {
                            this.datos_averia = true;
                        }
                    }
                    this.$set('movimientos', response.movimientos);
                    this.eventoCargaRemover();
                    $("#modal_embajador").modal();
                } else {
                    swal('Mensaje',response.msj,'warning');
                }
            }).error(function(errors) {
                alert('error');
            });
        },
        Enviar: function () {
            this.eventoCargaMostrar();
            var request = this.ficha;
            if (request.actividad_tipo_id==6 || request.actividad_tipo_id==5)
                request.cliente_telefono = request.codcli;
            request.quiebre_id=46;//embajador
            request.actividad_id=1;
            request.gestion=this.gestion;
            this.$http.post('registrar' , request,  function (response) {
                if (response.rst==1) {
                    this.eventoCargaRemover();
                    swal('N° Ticket: '+response.ticket, response.msj, "success");
                    this.ficha={};
                } else {
                    swal("Mensaje", response.msj, "warning");
                    this.eventoCargaRemover();
                }
            }).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "warning");
            });
        },
        eventoCargaMostrar: function () {
            if ( $( ".loading-img" ).length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }
        },
        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        },
        /*changeTipoActividad:function(){
            var id = this.ficha.actividad_tipo_id;
            if (id==4) {
                $("#codactu").attr('pattern', "[0-9]+");
                
            } else if (id==5 || id==6) {
                $("#codactu").attr('pattern', "[A-Z]{3}[0-9]{7}");
            }
            document.getElementById('codactu').focus();
            document.getElementById('codactu').value='';
        },*/
        changeBusqueda:function(){
            document.getElementById('busqueda').focus();
            document.getElementById('busqueda').value='';
        },
    },
    computed: {
      classPlazo: function () {
        return {
          'verde': this.modal.plazo=='Dentro del Plazo',
          'rojo': this.modal.plazo=='Fuera del Plazo',
        };
      }
    },
    ready: function () {
        $('[data-toggle="tooltip"]').tooltip();
    }
});
