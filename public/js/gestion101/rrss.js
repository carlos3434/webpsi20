var vm = new Vue({
    http: {
      root: '/root',
      headers: {
        'csrftoken': document.querySelector('#token').getAttribute('value')
      }
    },
    el: '#rrss',
    data: {
        btn_reforzar: 0,
        averia: 1,
        quiebre_id: 30, //rrss
        actividad: 1,//Averia
        tipo_averia: '',
        success: false,
        danger: false,
        msj: '',
        critico: 0,
        resultado: '',
        tipo_result: '',
        plazo: '',

        identificador: '',
        identificador_label: '',
        estado: '',
        fecha_registro: '',
        datos_averia: false,
        datos_ficha: false,

        f_codactu: '',
        f_estado : '',
        f_tipoaveria : '',
        f_ticket: '',
        f_nomcli: '',
        f_celcli: '',
        f_telcli: '',
        f_correocli: '',
        f_dnicli: '',
        f_codcli: '',
        f_nomcon: '',
        f_celcon: '',
        f_telcon: '',
        f_correocon: '',
        f_dnicon: '',
        f_nomemb: '',
        f_celemb: '',
        f_correoemb: '',
        f_dnioemb: '',
        f_comentario : '',

        a_codactu:'',
        a_fecha: '',
        a_estado: '',
        a_telefono: '',
        a_cliente: '',
        a_area: '',
        a_estacion : '',
        a_rubro : '',
        a_quiebre: '',
        a_zonal: '',
        a_contrata: ''
        
    },
    methods: {

        Busqueda: function(busqueda) {

           // if (busqueda == 'i') {
                var ficha_busqueda = {
                    tipo_busqueda: busqueda,
                    tipo: this.tipo_busqueda,
                    busqueda: this.busqueda,
                    gestion: this.gestion,
                }
            // } else {
            //     var ficha_busqueda = {
            //         tipo_busqueda: busqueda,
            //         nomcli: $.trim(this.nomcli),
            //         celcli: $.trim(this.celcli),
            //         telcli: $.trim(this.telcli),
            //         correocli: $.trim(this.correocli),
            //         dnicli: $.trim(this.dnicli),
            //         codcli: $.trim(this.codcli),
            //         nomemb: $.trim(this.nomemb),
            //         celemb: $.trim(this.celemb),
            //         correoemb: $.trim(this.correoemb)
            //     }
            // } 

            if (this.tipo_busqueda != '' || busqueda == 'a') {
                this.$http.get('/embajador/buscar', ficha_busqueda, function (data) {
                    this.eventoCargaMostrar();
                    console.log(data); 

                    if (data.rst==1) {
                        this.tipo_result = data.tipo;
                        this.datos_averia = false;
                        this.datos_ficha = false;

                        if (data.tipo == 'i'){

                            //completo formulario de ficha
                            if (data.ficha.length != 0){
                                ticket = data.ficha;
                                this.datos_ficha = true;
                                this.identificador_label = 'N° Ticket';
                                this.identificador = 'E' +pad(ticket.id, 5);
                                this.estado = ticket.estado;
                                this.fecha_registro = ticket.created_at;
                                
                                this.f_estado=ticket.estado;
                                this.f_tipoaveria = ticket.tipo_averia;
                                this.f_codactu=ticket.codactu;
                                this.f_ticket= 'E' +pad(ticket.id, 5);

                                this.f_nomcli=ticket.cliente_nombre;
                                this.f_celcli =ticket.cliente_celular;
                                this.f_telcli = ticket.cliente_telefono;
                                this.f_correocli = ticket.cliente_correo;
                                this.f_dnicli = ticket.cliente_dni;
                                this.f_codcli =ticket.codcli;

                                this.f_nomcon=ticket.contacto_nombre;
                                this.f_celcon =ticket.contacto_celular;
                                this.f_telcon = ticket.contacto_telefono;
                                this.f_correocon = ticket.contacto_correo;
                                this.f_dnicon = ticket.contacto_dni;
                                
                                this.f_nomemb=ticket.embajador_nombre;
                                this.f_celemb =ticket.embajador_celular;
                                this.f_correoemb = ticket.embajador_correo;
                                this.f_dniemb = ticket.embajador_dni;

                                this.f_comentario = ticket.comentario;
                            } else {
                                $("#ficha_results input[type=text]").val('');
                                $("#ficha_results textarea").val('');
                            }


                            if (data.averia.length != 0){
                                averia = data.averia;
                                this.datos_averia = true;
                                if (averia.plazo == 'Dentro') {
                                    this.plazo= 'Dentro del Plazo';
                                    this.btn_reforzar = 0;
                                    $("#plazo").addClass('verde').removeClass('rojo');
                                } else if (averia.plazo == 'Fuera') {
                                    this.plazo= 'Fuera del Plazo';
                                    this.btn_reforzar = 1;
                                    $("#plazo").addClass('rojo').removeClass('verde');
                                }

                                this.identificador_label = 'Codigo Avería';
                                this.identificador = averia.codactu;
                                this.estado = averia.estado;
                                this.fecha_registro = averia.fecha_registro;

                                this.a_estado= averia.estado;
                                this.a_fecha= averia.fecha_registro;
                                this.a_codactu= averia.codactu;
                                this.a_telefono= averia.telefono;
                                this.a_cliente= averia.nombre_cliente;
                                this.a_tipo_averia= averia.tipo_averia;
                                this.a_tipo_servicio= averia.tipo_servicio;
                                this.a_zonal= averia.zonal;
                                this.a_contrata= averia.contrata;
                                this.a_distrito= averia.distrito;
                                this.a_area= averia.area;
                                this.a_estacion= averia.area2;
                                this.a_quiebre= averia.quiebre;
                            } else {
                                $("#averia_results input[type=text]").val('');
                            }
                            $("#myModal").modal();
                        } else if (data.tipo == 'a') {
                            //$('#t_resultados').dataTable().fnDestroy();
                            this.$set('datos', data.datos);
                            //$('#t_resultados').DataTable();
                        }
                    } else {
                        this.$set('datos', 0);
                        swal('Mensaje',data.msj,'warning');
                    }
                    this.eventoCargaRemover();
                    //this.resultado = document.write(data.datos);
                    //$("#resultado").html(data.datos);
                }).error(function(errors) {
                    alert('error');
                }); 
            } else swal('Mensaje','Seleccione tipo de búsqueda','info');
            
        },
        Cargar: function (ticket) {

            var ficha_busqueda = {
                tipo: 'ticket',
                busqueda: ticket
            }
            this.$http.get('/embajador/cargar', ficha_busqueda, function (data) {
                if (data.rst==1) {
                    this.datos_averia = false;
                    this.datos_ficha = false;
                    this.tipo_result = data.tipo;

                    //completo formulario de ficha
                    if (data.ficha.length != 0){
                        ticket = data.ficha;
                        this.datos_ficha = true;

                        this.identificador_label = 'N° Ticket';
                        this.identificador = 'E' +pad(ticket.id, 5);
                        this.estado = ticket.estado;
                        this.fecha_registro = ticket.created_at;
                        
                        this.f_estado=ticket.estado;
                        this.f_tipoaveria = ticket.tipo_averia;
                        this.f_codactu=ticket.codactu;
                        this.f_ticket= 'E' +pad(ticket.id, 5);

                        this.f_nomcli=ticket.cliente_nombre;
                        this.f_celcli =ticket.cliente_celular;
                        this.f_telcli = ticket.cliente_telefono;
                        this.f_correocli = ticket.cliente_correo;
                        this.f_dnicli = ticket.cliente_dni;
                        this.f_codcli =ticket.codcli;

                        this.f_nomcon=ticket.contacto_nombre;
                        this.f_celcon =ticket.contacto_celular;
                        this.f_telcon = ticket.contacto_telefono;
                        this.f_correocon = ticket.contacto_correo;
                        this.f_dnicon = ticket.contacto_dni;
                        
                        this.f_nomemb=ticket.embajador_nombre;
                        this.f_celemb =ticket.embajador_celular;
                        this.f_correoemb = ticket.embajador_correo;
                        this.f_dniemb = ticket.embajador_dni;

                        this.f_comentario = ticket.comentario;
                    } else {
                        $("#ficha_results input[type=text]").val('');
                        $("#ficha_results textarea").val('');
                    }

                    if (data.averia.length != 0){
                        averia = data.averia;
                        this.datos_averia = true;
                        
                        if (averia.plazo == 'Dentro') {
                            this.plazo= 'Dentro del Plazo';
                            this.btn_reforzar = 0;
                            $("#plazo").addClass('verde').removeClass('rojo');
                        } else if (averia.plazo == 'Fuera') {
                            this.plazo= 'Fuera del Plazo';
                            this.btn_reforzar = 1;
                            $("#plazo").addClass('rojo').removeClass('verde');
                        }

                        this.identificador_label = 'Codigo Avería';
                        this.identificador = averia.codactu;
                        this.estado = averia.estado;
                        this.fecha_registro = averia.fecha_registro;

                        this.a_estado= averia.estado;
                        this.a_fecha= averia.fecha_registro;
                        this.a_codactu= averia.codactu;
                        this.a_telefono= averia.telefono;
                        this.a_cliente= averia.nombre_cliente;
                        this.a_tipo_averia= averia.tipo_averia;
                        this.a_tipo_servicio= averia.tipo_servicio;
                        this.a_zonal= averia.zonal;
                        this.a_contrata= averia.contrata;
                        this.a_distrito= averia.distrito;
                        this.a_area= averia.area;
                        this.a_estacion= averia.area2;
                        this.a_quiebre= averia.quiebre;
                    } else {
                        $("#averia_results input[type=text]").val('');
                    }
                    this.eventoCargaRemover();
                    $("#myModal").modal();
                    
                } else {
                    swal('Mensaje',data.msj,'warning');
                }
            }).error(function(errors) {
                alert('error');
            }); 
        },
        Enviar: function () {
            this.eventoCargaMostrar();
            var ficha_ingreso = {
                tipo_averia: this.tipo_averia, //producto
                tipo_fuente: this.tipo_fuente,
                tipo_atencion: this.tipo_atencion,
                codactu:this.cod_averia,
                codcli:this.cod_cliente,
                quiebre_id: this.quiebre_id, //rrss
                actividad: this.actividad, // Averia
                gestion: this.gestion,
                //cliente
                cliente_nom: this.cliente_nom,
                cliente_cel: this.cliente_cel,
                cliente_correo: this.cliente_correo,
                cliente_tel: this.cliente_tel,
                cliente_dni: this.cliente_dni,
                //contacto
                contacto_nom: this.contacto_nom,
                contacto_cel: this.contacto_cel,
                contacto_correo: this.contacto_correo,
                contacto_tel: this.contacto_tel,
                contacto_dni: this.contacto_dni,
                //embajador
                embajador_nom: this.emb_nom,
                embajador_cel: this.emb_cel,
                embajador_correo: this.emb_correo,
                embajador_dni: this.emb_dni,
                comentario: this.comentario,
                critico: this.critico,

            };

            this.$http.post('/embajador' , ficha_ingreso,  function (data) {
                this.eventoCargaMostrar();
                if (data.rst==1) {
                    this.eventoCargaRemover();
                    swal('N°Ticket: '+data.ticket, data.msj, "success");
                    $("#ficha_form input[type=text]").val('');
                    $("#ficha_form textarea").val('');
                } else {
                    swal("Error", data.msj, "danger");
                }

            }).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "danger");
            }); 
        },
        eventoCargaMostrar: function () {
            if ( $( ".loading-img" ).length == 0 ) {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            }   
        },
        eventoCargaRemover: function () {
            $(".overlay,.loading-img").remove();
        }
    },
    ready: function () {
        //$('#t_resultados').DataTable();
        $('[data-toggle="tooltip"]').tooltip();
    }
});