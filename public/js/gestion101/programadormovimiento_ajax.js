var days_name = ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];


var Comando ={

	Listar:function(busqueda = ''){
		
		datos = busqueda;
		$.ajax({
            url         : "programadormovimiento/listar",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(data) {
                eventoCargaRemover();
                if(data.rst==1){
                    HTMLListarComandos(data.datos);
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
	},
	guardarComando: function(){
            
		var datos = $("#frmCrearcomando").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");	
        $.ajax({
            url         : "programadormovimiento/guardarcomando",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    Comando.Listar();
                    swal("Mensaje",obj.msj,"success");
                    $('#nuevoEvento .modal-header [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
	},

	cambiarEstado: function(id, value){
		$("#frmCrearcomando").append("<input type='hidden' value='"+id+"' name='id'>");
        $("#frmCrearcomando").append("<input type='hidden' value='"+value+"' name='estado'>");
        var datos=$("#frmCrearcomando").serialize().split("txt_").join("").split("slct_").join("");
		$.ajax({
            url         : "programadormovimiento/cambiarestado",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    Comando.Listar();
                    swal("Mensaje",obj.msj,"success");
                    $('#nuevoEvento .modal-header [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
	},


	obtenerDatos: function(id){
        $("#txt_id").val(id);
		var datos=$("#frmCrearcomando").serialize().split("txt_").join("").split("slct_").join("");


        $.ajax({
			url	: "programadormovimiento/obtenerdatos",
			type: 'POST',
			cache: false,
			dataType: 'json',
			data: datos,
			beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    var comandoInfo = obj.data;
                    Comando.pintarDatos(obj.data);
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
		
		});
	},

	pintarDatos: function(data){
		$("input[name=text_id]").val(data.id);
        $("input[name=txt_nombre]").val(data.nombre);
        $("input[name=txt_observacion]").val(data.observacion);
		$("#slct_tipo_fecha").multiselect('select', data.tipo_fecha);
		$("#slct_tipo_fecha").multiselect("refresh");
		$("#slct_estado").multiselect('select', data.estado);
		$("#slct_estado").multiselect("refresh");
        $('#slct_estado_programacion').multiselect('select', data.estado_programacion);
        $('#slct_estado_programacion').multiselect('refresh');
        $('#slct_motivo').multiselect('select', data.motivo_id);
        $('#slct_motivo').multiselect('refresh');
        $('#slct_submotivo').multiselect('select', data.submotivo_id);
        $('#slct_submotivo').multiselect('refresh');
        $('#slct_estado_pre').multiselect('select', data.estado_pretemporal_ultimo_id);
        $('#slct_estado_pre').multiselect('refresh'); 
        $('#slct_solucionado').multiselect('select', data.solucionado_pretemporal_id);
        $('#slct_solucionado').multiselect('refresh');

        let fecha_final_comando = moment();
        let fecha_inicio_comando = moment().subtract(1, 'days');
        let fecha_inicio_busqueda = moment();
        let fecha_final_busqueda = moment().subtract(1, 'days');

        if( data.estado_programacion == '0') {
            $('.form-dias').hide();
        } else {
            $('.form-dias').show();
        }

        if( data.fecha_inicio_comando != null){
            fecha_inicio_comando = moment(data.fecha_inicio_comando);
        }
        if( data.fecha_final_comando != null){
            fecha_final_comando = moment(data.fecha_final_comando);
        }
        if( data.fecha_inicio_busqueda != null){
            fecha_inicio_busqueda = moment(data.fecha_inicio_busqueda);
        }
        if( data.fecha_final_busqueda != null){
            fecha_final_busqueda = moment(data.fecha_final_busqueda);
        }
        $("#txt_fechas_comando").val( fecha_inicio_comando.format('YYYY-MM-DD') + ' - ' + fecha_final_comando.format('YYYY-MM-DD'));
        $("#txt_fechas_busqueda").val( fecha_inicio_busqueda.format('YYYY-MM-DD') + ' - ' + fecha_final_busqueda.format('YYYY-MM-DD'));
        $("#txt_fechas_busqueda").daterangepicker({
            format: 'YYYY-MM-DD',
            endDate: fecha_final_busqueda.format('YYYY-MM-DD'),
            startDate: fecha_inicio_busqueda.format('YYYY-MM-DD')
        });
        
         $("#txt_fechas_comando").daterangepicker({
            format: 'YYYY-MM-DD',
            endDate: fecha_final_comando.format('YYYY-MM-DD'),
            startDate: fecha_inicio_comando.format('YYYY-MM-DD')
        });

        $("#txt_dias_hora_inicio").timepicker('setTime', data.dias_hora_inicio);
        $("#txt_dias_hora_fin").timepicker('setTime', data.dias_hora_fin);

        quiebres_seleccionados = [];
        atenciones_seleccionados = [];
        dias_seleccionados = [];
        if ( data.dias != null && data.dias != undefined ){
            for(let i = 0; i < data.dias.length; i++){
                dias_seleccionados.push(data.dias[i].dia);
            }
        }

        if ( data.quiebres != null && data.quiebres != undefined ){
            for(let i = 0; i < data.quiebres.length; i++){
                quiebres_seleccionados.push(data.quiebres[i].quiebre_id);
            }
        }

        if ( data.atenciones != null && data.atenciones != undefined ){
            for(let i = 0; i < data.atenciones.length; i++){
                atenciones_seleccionados.push(data.atenciones[i].atencion_id);
            }
        }

        $("#slct_dias").val(dias_seleccionados);
        $("#slct_dias").multiselect("refresh");
        $("#slct_quiebre").val(quiebres_seleccionados);
        $("#slct_quiebre").multiselect("refresh");
        $("#slct_atencion").val(atenciones_seleccionados);
        $("#slct_atencion").multiselect("refresh");

        if ("M"+$("#slct_motivo").val() == "M23" && "S"+$("#slct_submotivo").val() == "S58" ) {
            $(".solucionado").show();
        } else {
            $(".solucionado").hide();
        }       
	},

    editarComando: function(){

        var datos = $("#frmCrearcomando").serialize().split("txt_").join("").split("slct_").join("").split("rad_").join("").split("_modal").join("");   
        $.ajax({
            url         : "programadormovimiento/editarcomando",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    Comando.Listar();
                    swal("Mensaje",obj.msj,"success");
                    $('#nuevoEvento .modal-header [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    },

    eliminar: function(id){
        $("#frmCrearcomando").append("<input type='hidden' value='"+id+"' name='id'>");
        var datos=$("#frmCrearcomando").serialize().split("txt_").join("").split("slct_").join("");

        $.ajax({
            url         : "programadormovimiento/eliminarcomando",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                eventoCargaMostrar();
            },
            success : function(obj) {
                eventoCargaRemover();
                if(obj.rst==1){
                    Comando.Listar();
                    swal("Mensaje",obj.msj,"success");
                    $('#nuevoEvento .modal-header [data-dismiss="modal"]').click();
                }
                else{
                    swal('Error',obj.msj,'error');
                }
            },
            error: function(){
                eventoCargaRemover();
                alert("error");
            }
        });
    }
}
