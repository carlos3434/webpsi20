$(document).ready(function () {

});

ObtenerPruebas=function(){
    var datos = $("#form_analizador").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");

        $.ajax({
            url         : 'analizador/obtener',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                $("#divresultado, #divpruebas").css("display",'none');
                $("#txt_resultado").val('');
                $("#divoption").html("");
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    MostrarPruebas(obj.pruebas);
                } else {
                    $("#divpruebas").css("display",'none');
                    $("#divoption").html("");
                    Psi.mensaje('danger', obj.msj, 6000);
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                 Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
};

MostrarPruebas=function(pruebas){

    $("#divpruebas").css("display",'');
    var  _html="";
    var com="'";
    //_html='<select name="slct_pruebas">';
    //_html+="<option value=''>.::Seleccione::.</option>";
    $.each(pruebas, function( index, value ) {
        //_html+="<option value='"+value.descripcion+"'>"+value.tecla+"</option>";
        _html+='<div class="col-sm-3" style="margin-bottom:15px"><input class="form-control btn-info" type="button" value="'+value.tecla
        +'" onclick="RealizarPrueba('+com+value.descripcion+com+')" style="min-width:70px"></div>';
    });
    //_html+="</select>";
    $("#divoption").html(_html);

};

RealizarPrueba=function(prueba){

    var datos = $("#form_analizador").serialize().split("txt_").join("").split("slct_").join("").split("_modal").join("");
        datos+="&pruebas="+prueba;

        $.ajax({
            url         : 'analizador/probar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                $("#divresultado").css("display",'none');
                $("#txt_resultado").val('');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    MostrarResultado(obj.resultado);    
                } else {
                    Psi.mensaje('danger', obj.msj, 6000);                    
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                 Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    
};

MostrarResultado=function(resultado){
    $("#divresultado").css("display",'');
    $("#txt_resultado").val(resultado['interprete']);
};