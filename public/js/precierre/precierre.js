    $(document).ready(function () {

    // initAutocomplete();

        $("input[type=checkbox]").each(function() {
            //activarCombo($(this));
        });

        $('#allcheckbox').on('click', function (event) {
            if ($('#allcheckbox').prop('checked')) {
                $("input[type=checkbox]").prop("checked", true);
                $("input[type=checkbox]").each(function() {
                    activarCombo($(this));
                });
            } else {
                $("input[type=checkbox]").removeAttr("checked");
                $("input[type=checkbox]").each(function() {
                    activarCombo($(this));
                });
            }
        });

        $('#form_componentes').submit(function(e) {
            e.preventDefault();
            var datos = $("#form_componentes").serialize();
        });

        $(document).delegate("#tb_componente input[type=checkbox]","change", function(){
            activarCombo($(this));
        });

        $('#search').on('click', function() {
            // Obtenemos la dirección y la asignamos a una variable
            var address = $('#address').val();
            if (address.length == 0) {
                Psi.mensaje('warning', 'Ingresa dirección', 3000);
            }
            var request = {
                address: address,
                componentRestrictions: {
                    country: 'PE'
                }
            }
            // Creamos el Objeto Geocoder
            //var geocoder = new google.maps.Geocoder();
            // Hacemos la petición indicando la dirección e invocamos la función
            // geocodeResult enviando todo el resultado obtenido
            //geocoder.geocode(request, geocodeResult);
        });


    });
