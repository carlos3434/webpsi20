var d = new Date();
var todayJS = d.getFullYear() + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-0' + d.getDate(); 
var app = new Vue({
    http: {
        root: '/root',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },
    el: '#contenedor-mapaLocalizacionRecursos',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
    },
    data: {
        loaded: false,
        showLeft: false,
        showRight: false,
        resource_id: 'BK_EZENTIS_ESTE_ZARATE',
        map:[],
        tec_actividades:[],
        actividadesTipo: {
           'bucket':'En Bucket' ,
           'tecnico':'En Tecnico'
        },
        resources_id: {
           'BK_EZENTIS_ESTE_ZARATE':'BK_EZENTIS_ESTE_ZARATE'
        },
        actividadesTipoSeleccionados: ['tecnico','bucket'],
        tecnicosSeleccionados: [],
        inicio: todayJS,
        fin: todayJS,
        markers: [],
        circles: [],
        infoWindows: [],
        directionsService: [],
        directionsDisplay: [],
        mensaje: '',
        colores: [],
    },
    methods: {
        mostrarMensaje: function(mensaje){
            this.handle = setInterval( ( ) => {
                app.mensaje='';
            },4000);
            this.mensaje=mensaje;
        },
        setDimensionesMapa: function(){
            element = document.getElementById("mapaLocalizacionRecursos");
            anchomapa = element.offsetWidth;
            altopantalla = window.innerHeight;
            element.style.height = anchomapa+" px";
            //document.getElementById("mapaLocalizacionRecursos").height = window.innerHeight - 300 + "px";
            $("#mapaLocalizacionRecursos").css("height", window.innerHeight*0.7);
        },
        addMarker: function(labelContent,labelClass,location, icon , info, punto, arrastrar, customInfo, id, addCircle) {
            if (punto === undefined) {
               punto='';
            }
             if (customInfo === undefined) {
               customInfo='';
            }
             if (id === undefined) {
               id='actividad';
            }
            var componenteIcon = window.location.origin+icon;
            zIndexRep=0;
            var marker = new MarkerWithLabel({
                position: location,
                icon: icon,
                map: this.map,
                draggable: arrastrar,
                title: "",
                zIndex: zIndexRep++,
                labelContent:  labelContent,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: labelClass,
                labelStyle: {opacity: 0.85},
                customInfo: customInfo,
                id: id
            });

            var infowindow = new google.maps.InfoWindow({
                content: info
            });
            this.markers.push(marker);

            //click en marcadores
            marker.addListener('click', function(event) {
                if(app.infoWindows.length>0){
                    for (var i=0;i<app.infoWindows.length;i++) {
                        app.infoWindows[i].close();
                    }
                }
                infowindow.open(this.map, marker);
                app.infoWindows.push(infowindow);
                app.map.setCenter(marker.getPosition());
                if (addCircle === true) {
                   app.addCircle(marker);
                }

            });

            google.maps.event.addListener(marker, 'dragend', function(evt) {
                console.log(evt.latLng.lat());
                latitudNueva = evt.latLng.lat();
                longitudNueva = evt.latLng.lng();
                console.log(evt.latLng.lng());

                 if (marker.customInfo!==""){
                    app.updateLocation(marker, latitudNueva, longitudNueva);
                    }
                 });
        },
        addCircle: function(marker, location, icon){
            this.removeCircles();
            circle = new google.maps.Circle({
                  strokeColor: marker.getIcon().fillColor,
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: marker.getIcon().fillColor,
                  fillOpacity: 0.35,
                  map: marker.getMap(),
                  center: marker.getPosition(),
                  radius: 1415
                });
            this.circles.push(circle);
        },
        setMapOnAll: function() {
            this.removeMarkers();
        },

        removeMarkers: function() {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },
        removeCircles: function() {
            for (var i = 0; i < this.circles.length; i++) {
                this.circles[i].setMap(null);
            }
        },
        updateLocation : function(marker, latitudNueva, longitudNueva) {
            data = marker.customInfo;
            latitudAnterior = data.latitud;
            longitudAnterior = data.longitud;

            if (confirm("¿Desea Actualizar las Coordenadas?")){
                data.latitud = latitudNueva;
                data.longitud = longitudNueva;
                 this.$http.patch(publicurl +"/locationtecnicoofsc/" + data.id, data, function (data) {
                marker.customInfo = data;
                this.addCircle(marker);
                
                alert("Ubicación de Tecnico "+data.resource_id+" Actualizada!!!");
                });
            } else {
                var newLatLng = new google.maps.LatLng( latitudAnterior , longitudAnterior);
                marker.setPosition(newLatLng);
                this.addCircle(marker);
            }
        },
        mostrarInfoTecnico: function(carnet) {
            console.log(carnet);
            for (var i in this.markers) {
                marker = this.markers[i];
                if (marker.id === carnet){
                    google.maps.event.trigger(marker, 'click');
                }
            }
        },
        fecthActividades: function(resource_id) {
          this.$http.get(publicurl+'/actividadesofsc/' + resource_id, function (actividades) {
                this.$set('actividades', actividades);
                this.loaded=false;
                this.pintarMapa();
            });
        },
        showTecnicos: function(resource_id) {
           this.$http.get(publicurl +'/tecnicosofsc/' + resource_id, function (tecnicos) {
                var j=0;
                for ( var i in tecnicos) {
                    color = this.randomHexColor();
                    tecnicos[i].color=color;
                    tecnicos[i].styleObject = { color: color};
                    tecnicos[i].carnet = i;
                    j++;
                }
                this.$set('tecnicos', tecnicos);
            });
        },
         /*actualizarCoordenadas: function(localizacion){
           this.$http.patch(publicurl+"/locationtecnicoofsc/" + data.id, data, function (data) {
                console.log(data);
            });
        },*/
        setInfoWindow: function(){

        },
        randomHexColor: function(){
            random = 0;
            do {
                    random = Math.random(2, 12)*100 + 5;
                    color = '#' + ("405893" + random.toString(16).slice(2, 10).toUpperCase()).slice(-6);
                    return color;
                }
                while (random === 0);
            
        },
        pintarMapa: function() {
            loaded = true;
            actividades = this.$get("actividades");
           
            this.setMapOnAll();

            var bounds = new google.maps.LatLngBounds();
            var carnet, info, icon, start, time;
            var contenedor='';
            var color;
            var actividadaid;
            var tipoActividad;
            var esTecnicoSelec;

            
         // recorrer tecnicos
        tecnicos = this.$get("tecnicos");
        
         this.$http.get(publicurl +"/locationtecnicoofsc/1",  function (data){
                tecnicosXml = this.$get("tecnicos");

                 //recorrer actividades
                iconActividad = {
                    path: fontawesome.markers.CALENDAR,
                    scale: 0.4,
                    strokeWeight: 0.3,
                    strokeColor: '#000',
                    strokeOpacity: 1,
                    fillColor: '#000',
                    fillOpacity: 1
                };

                for ( actividadaid in actividades) {
                actividad = actividades[actividadaid];

                //filtrar x y validos , y actividades no borradas
                if (actividad.coordx!=0 && actividad.coordy!=0 && actividad.status =='pending' ) {
                    // -- test -- console.log(actividad.date  + ' ----> ' + this.inicio + ' - ' + this.fin);
                    if (this.inicio<=actividad.date && actividad.date<=this.fin && actividad.date!='3000-01-01') {
                        actividadLocation = new google.maps.LatLng(actividad.coordy,actividad.coordx);
                        info='<label>Fecha Agenda: </label>&nbsp;&nbsp;<t>' +actividad.date+'<br>';
                        info+='<label>Estado: </label>&nbsp;&nbsp;&nbsp;'+actividad.status+'<br>';
                        info+='<label>Actuacion: </label>&nbsp;&nbsp;&nbsp;'+actividad.appt_number+'<br>';
                        resource_id = actividad.resource_id;
                        if (typeof tecnicosXml[resource_id]!=="undefined"){
                            iconActividad.fillColor = tecnicosXml[resource_id].color;
                        }
                        icon= iconActividad;
                        draggable =false;
                        this.addMarker(actividad.appt_number,actividad.status+" etiqueta-actividad",actividadLocation,icon,info, '', draggable);
                         bounds.extend(actividadLocation);
                    }
                }
            }

            if (data.length > 0) {
                    

                    for( var i in data){
                        tecnico = data[i];
                        //console.log(tecnico);
                        if (typeof tecnicosXml[tecnico.resource_id]!="undefined"){
                             
                                tecnicoLocation = new google.maps.LatLng(tecnico.latitud, tecnico.longitud);
                                labelContent = "<div class='etiqueta'>"+tecnico.resource_id+"</div>";
                                labelClass = "etiqueta-tecnicos"
                                info = "<label>"+tecnicosXml[tecnico.resource_id].name+"</label>";
                                info+="<div><label>X (Lng)</label>: "+tecnico.longitud+"</div>";
                                info+="<div><label>Y (Lat)</label>: "+tecnico.latitud+"</div>";
                                estilos = tecnicosXml[tecnico.resource_id].styleObject;
                               
                                  icon = {
                                        path: fontawesome.markers.MALE,
                                        scale: 0.6,
                                        strokeWeight: 0.3,
                                        strokeColor: 'black',
                                        strokeOpacity: 1,
                                        fillColor: estilos.color,
                                        fillOpacity: 1
                                    };
                                customInfo = tecnico;
                                this.addMarker(labelContent, labelClass, tecnicoLocation,icon,info, '', true, customInfo,  tecnico.resource_id, true);
                                bounds.extend(tecnicoLocation);
                        }
                    }
                }

        });

     },
        
    },
    
    ready: function(){
        this.setDimensionesMapa();
        this.showTecnicos(this.resource_id);
        this. fecthActividades(this.resource_id);
       
    },
    events: {
        'google.maps:init': function() {
            this.loaded=true;
            this.directionsService = new google.maps.DirectionsService;
            var myLatlng = new google.maps.LatLng(-12.109047,-76.94072);
            this.map = new google.maps.Map( document.getElementById('mapaLocalizacionRecursos'), {
                center: myLatlng,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
    }
});

window.initMap = function() {
    app.$emit('google.maps:init');
};

