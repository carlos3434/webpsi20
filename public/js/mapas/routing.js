var app = new Vue({
    http: {
        root: '/root',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },
    el: '#mapatecnicosController',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
    },
    data: {
        loaded: false,
        showLeft: false,
        showRight: false,
        resource_id: 'BK_LARI_LA_MOLINA_RESTO',
        map:[],
        tec_actividades:[],
        actividadesTipo: {
           'bucket':'En Bucket' ,
           'tecnico':'En Tecnico'
        },
        resources_id: {
           'BK_LARI_LA_MOLINA_RESTO':'Lari Movistar Uno'
        },
        actividadesTipoSeleccionados: ['tecnico','bucket'],
        tecnicosSeleccionados: [],
        inicio: hoy,
        fin: hoy,
        markers: [],
        flightPath: [],
        flightPaths: [],
        infoWindows: [],
        directionsService: [],
        directionsDisplay: [],
        mensaje: '',
        tipo:'completo',
        colores: [],
        abecedario:['A','B','C','D','E','F','G','H','I','J','K','M','N','L','O','P','Q','R','S','T','U','V','W','x','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AM','AN','AL','AO','AP','AQ','AR','AS','AT','AU','AV'],
        trazos: ['lineas'],
    },
    methods: {
        selectAllTecnicos: function (){
            //seleccionar todos los tecnicos
            //validar cuando no se haya seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            if (todos>=0) {
                for ( carnet in this.$get('tecnicos')) {
                    this.tecnicosSeleccionados.push(carnet);
                }
            } else {
                this.tecnicosSeleccionados=[];
            }
        },
        selectTecnico: function (carnet){
            //validar si esta seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            var tecnico = this.tecnicosSeleccionados.indexOf(carnet);
            if (tecnico <0 && todos>=0 ) {
                //quitar la marca e todos en tecnicos seleccionados
                this.tecnicosSeleccionados.splice(0, 1);
            }
        },
        mostrarMensaje: function(mensaje){
            this.handle = setInterval( ( ) => {
                app.mensaje='';
            },4000);
            this.mensaje=mensaje;
        },
        filtro: function(){
            if (this.inicio>this.fin) {
                this.mostrarMensaje('Rango de fechas no validas');
                return;
            }
            this.setMapOnAll();
            var bounds = new google.maps.LatLngBounds();
            var carnet, info, icon, start, time;
            var contenedor='';
            var color;
            var actividadaid;
            var rutas=[];
            var rutas22=[];
            var vbucket=[];
            var vbucketico='';
            // var orden_xactividad=[];
            var tipoActividad;
            var esTecnicoSelec;

            //recorrer actividades
            for ( actividadaid in this.$get('actividades')) {

                actividad = this.$get('actividades')[actividadaid];
                //filtrar x y validos , y actividades no borradas
                if (actividad.coordx!=0 && actividad.coordy!=0) {
                    //filtrar fecha
                    actividadLocation = new google.maps.LatLng(actividad.coordy,actividad.coordx);
                    info='<label>Fecha Agenda: </label>&nbsp;&nbsp;<t>' +actividad.date+'<br>';
                    info+='<label>Estado: </label>&nbsp;&nbsp;&nbsp;'+actividad.status+'<br>';
                    info+='<label>Actuacion: </label>&nbsp;&nbsp;&nbsp;'+actividad.appt_number+'<br>';
                    tipoActividad = this.actividadesTipoSeleccionados;
                    icon = {
                    path: fontawesome.markers.CALENDAR,
                    scale: 0.4,
                    strokeWeight: 0.3,
                    strokeColor: '#000',
                    strokeOpacity: 1,
                    fillColor: actividad.color,
                    fillOpacity: 1
                };
                    vbucket.push(actividad.appt_number);
                    vbucketico=icon;
                    this.addMarker(actividad.appt_number,'',actividadLocation,icon,info,'');
                    bounds.extend(actividadLocation);
                }
            }
            
        },
        addMarker: function(labelContent,labelClass,location, icon , info, punto) {

            if (punto === undefined) {
               punto='';
            }

            var componenteIcon = window.location.origin+icon;
            zIndexRep=0;
            var marker = new MarkerWithLabel({
                position: location,
                icon: icon,
                map: this.map,
                title: labelContent,
                zIndex: zIndexRep++,
                labelContent: labelContent,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: labelClass,
                labelStyle: {opacity: 0.85}
            });

            var infowindow = new google.maps.InfoWindow({
                content: info
            });
            this.markers.push(marker);
            //click en marcadores
            marker.addListener('click', function(event) {
                if(app.infoWindows.length>0){
                    for (var i=0;i<app.infoWindows.length;i++) {
                        app.infoWindows[i].close();
                    }
                }
                infowindow.open(this.map, marker);
                app.infoWindows.push(infowindow);
            });
        },
        setMapOnAll: function() {
            this.removeMarkers();
        },
        removeMarkers: function() {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },
        showActividades: function(resource_id) {
            this.$http.get(publicurl+'/routingoofsc/' + resource_id, function (actividades) {
                
                this.$set('actividades', actividades);
                this.filtro();
                this.loaded=false;
            });

        },
        showTecnicos: function(resource_id) {
            this.$http.get(publicurl+'/tecnicosofsc/' + resource_id, function (tecnicos) {
                colores = ['00FF00','00FFFF','0000FF','000000',
                '000080','008000','008080','800000','800080','808000',
                '808080','C0C0C0','FF00FF','FF0000','FF8800','FFFF00',
                'FFFFFF','80FF00'];
                var j=0;
                // console.log(tecnicos);
                for ( var i in tecnicos) {
                    tecnicos[i].color=colores[j];
                    j++;
                }
                this.$set('tecnicos', tecnicos);
            });
        }
    },
    ready: function(){
        this.showActividades(this.resource_id);
    },
    events: {
        'google.maps:init': function() {
            this.loaded=true;
            this.directionsService= new google.maps.DirectionsService;

            var myLatlng = new google.maps.LatLng(-12.109047,-76.94072);
            this.map = new google.maps.Map( document.getElementById('map'), {
                center: myLatlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
    }
});

window.initMap = function() {
    app.$emit('google.maps:init');
};
