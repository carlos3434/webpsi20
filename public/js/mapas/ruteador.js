// rutas coordenadas
// (mapa, array(coordenaaadas {}), panel detalle)

function initMaprouter(objMap, xyArray,panel) {
  
  var directionsService= new google.maps.DirectionsService;
  var directionsDisplay= new google.maps.DirectionsRenderer;
  directionsDisplay.setMap(objMap);

  calculateAndDisplayRoute(directionsService,directionsDisplay,xyArray,objMap,panel);

}

function calculateAndDisplayRoute(directionsService,directionsDisplay,checkboxArray,objMap,panel) {
  
  var waypts = [];
  // solo permite 8 hitos ademas de inicio y fin, para mas puntos se debe contratar
  // el servicio web Direcciones API de Google Maps
  var final=checkboxArray.length -1;

  for (var i = 1; i < final; i++) { // no tocará ni la primera ni la ultima
    if (checkboxArray[i]) {
      waypts.push(
          {
          location: checkboxArray[i],
          stopover: true //true: significa que el x,y es una parada obligatoria
          }
      );
    }
  }

  directionsService.route(
      {
      origin: checkboxArray[0], // primera coordenada
      destination: checkboxArray[final], // ultima coordenada
      waypoints: waypts,
      optimizeWaypoints: false, // true: permite la reordenación de los hitos de forma más eficiente
      travelMode: google.maps.TravelMode.DRIVING
      }, function(response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
      
      directionsDisplay.setDirections(response);
      var route = response.routes[0]; 
      var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,//google.maps.SymbolPath.CIRCLE, //'M 0,-1 0,1',
        strokeOpacity: 1,
        scale: 2 //4
      };
      directionsDisplay.setOptions(
          { 
          suppressMarkers: true,  //true: ocultar los markers
          polylineOptions: {
            strokeWeight: 4,
            strokeOpacity: 0,
            strokeColor: '#' + Math.floor(Math.random()*16777215).toString(16),
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '20px'
            }],
          }
          }
      );
      
      /*if (panel!=null && panel!='') { // mostrara un div con detalles
        var summaryPanel = document.getElementById(panel);
        summaryPanel.innerHTML = '';
        // For each route, display summary information.
        for (var i = 0; i < route.legs.length; i++) {
          var routeSegment = i + 1;
          summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
              '</b><br>';
          summaryPanel.innerHTML += route.legs[i].start_address + ' - a - ';
          summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
          summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
        } 
      } */
 
      } else {
      window.alert('Directions request failed due to ' + status);
      }
      }
  );
}
