Vue.config.debug = true;
var app = new Vue(
    {
    http: {
        root: '/root',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },
    el: '#mapatecnicosController',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab,
        'carousel': VueStrap.carousel,
        'slider': VueStrap.slider
    },
    data: {
        loaded: false,
        imagen:{
            x:'',
            y:''
        },
        showLeft: false,
        showRight: false,
        showModal: false,
        showModalEdicion: false,
        resource_id: [],//'BK_LARI_LA_MOLINA_RESTO',
        map:[],
        imagenes:[],
        tec_actividades:[],
        actividadesTipo: {
           'bucket':'En Bucket' ,
           'tecnico':'En Tecnico'
        },
        resources_id: {},
        bucketIco:'/img/icons/ofsc/lari/ACT_LARI.png',
        inicioIco:'/img/icons/tap.png',
        actividadesTipoSeleccionados: ['tecnico','bucket'],
        tecnicosSeleccionados: [],
        markers: [],
        flightPath: [],
        flightPaths: [],
        infoWindows: [],
        directionsService: [],
        directionsDisplay: [],
        mensaje: '',
        tipo:'completo',
        colores: ['00FF00','00FFFF','0000FF','000000','000080','008000',
        '008080','800000','800080','808000','808080','C0C0C0','FF00FF','FF0000',
        'FF8800','FFFF00','FFFFFF','80FF00','DAF7A6','581845','6B3735','03927E'
        ],
        abecedario:['A','B','C','D','E','F','G','H','I','J','K','M','N','L','O',
        'P','Q','R','S','T','U','V','W','x','Y','Z',],
        trazos: ['lineas'],
        idHistoricoProceso: 0,
        long_bucket:long_bucket,
        lat_bucket:lat_bucket,
        pestaniaUno:[],
        pestaniaDos:[],
        entorno:entorno,
        rangoDecimalColor: 16777215
    },
    methods: {
        mostrarMensaje: function(mensaje){
            this.handle = setInterval(
                ( ) => {
                app.mensaje='';
                },4000
            );
            this.mensaje=mensaje;
        },
        addLine: function(rutasMapa) {
            this.flightPath = new google.maps.Polyline(
                {
                    path: rutasMapa,
                    geodesic: true,
                    strokeColor: rutasMapa.color,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                }
            );
            this.flightPath.setMap(this.map);
            //array de lineas
            this.flightPaths.push(this.flightPath);
        },
        addMarker: function(title,labelClass,location, icon , info, punto) {
            zIndexRep=0;

            var marker = new MarkerWithLabel(
                {
                position: location,
                //icon: image,
                map: this.map,
                title: title,
                zIndex: zIndexRep++,
                labelContent: punto,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: labelClass,
                labelStyle: {opacity: 0.85}
                }
            );
            var infowindow = new google.maps.InfoWindow(
                {
                content: info
                }
            );

            this.markers.push(marker);
            //click en marcadores
            marker.addListener( 'click', function(event) {
                if(app.infoWindows.length>0){
                    for (var i=0;i<app.infoWindows.length;i++) {
                        app.infoWindows[i].close();
                    }
                }
                infowindow.open(this.map, marker);
                app.infoWindows.push(infowindow);
            });
        },
        setMapOnAll: function() {
            this.removeMarkers();
        },
        removeMarkers: function() {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },
        showEmergencias: function() {
            var request={carnet:'70',date: hoy};
            this.$http.post( '/visorgps/tracking',request , function (emergencias) {
                this.$set('emergencias', emergencias.data);
                this.loaded=false;
                app.pintarMapa(emergencias.data);
            });
        },
        cargarModal: function(id) {
            app.showModal=true;
            //buscar imagenes de esta emergencia
            for ( var i in app.emergencias) {
                if (app.emergencias[i].id == id ) {
                    this.imagenes = app.emergencias[i].emergencia_imagen;
                    /*for ( var j in app.emergencias[i].emergencia_imagen) {
                        imagen = app.emergencias[i].emergencia_imagen[j];
                    }*/
                }
            }
        },
        cargarModalEdicion: function(id) {
            app.showModalEdicion=true;
            //buscar imagenes de esta emergencia
            for ( var i in app.emergencias) {
                if (app.emergencias[i].id == id ) {
                    this.imagenes = app.emergencias[i].emergencia_imagen;
                    app.imagen = app.emergencias[i];
                    /*for ( var j in app.emergencias[i].emergencia_imagen) {
                        imagen = app.emergencias[i].emergencia_imagen[j];
                    }*/
                }
            }
        },
        actualizacionEmergencia:function() {
            //console.log("update");
            this.loaded=true;
            //this.$http.get( '/emergencia/index' , function (emergencias) {
            this.$http.post( '/emergencia/update' ,this.imagen, function (response) {
                if (response.rst) {
                    app.mostrarMensaje(response.msj);
                }
                app.showEmergencias();
                app.showModalEdicion=false;
                this.loaded=false;
            });
            //this.showModal = false;
        },
        pintarMapa : function (emergencias) {
            if (emergencias.length == 0) {
                app.mostrarMensaje('Archivo emergencias sin registros');
                this.loaded=false;
                return;
            }
            //actualizar variable global
            this.$set('emergencias', emergencias);
            this.setMapOnAll();
            var bounds = new google.maps.LatLngBounds();
            var info='', icon,rutasMapa=[];
            //recorrer actividades para pintarlos en el mapa las que tienen bucket

            for ( var emergenciaid in emergencias) {
                emergencia = emergencias[emergenciaid];

                actividadLocation = new google.maps.LatLng(parseFloat(emergencia.Y.replace(',','.')),parseFloat(emergencia.X.replace(',','.')) );
                //info='<label>Direccion: </label>&nbsp;&nbsp;<t>' +emergencia.address+'<br>';
                //info+='<label>Observacion: </label>&nbsp;&nbsp;&nbsp;'+emergencia.obs+'<br>';
                //info+='<label>Emergencia: </label>&nbsp;&nbsp;&nbsp;'+emergencia.emergencia+'<br>';
                info='<label>Fecha: </label>&nbsp;&nbsp;&nbsp;'+emergencia.t+'<br>';
                //info+='<a id="imagenes" onclick="app.cargarModal('+emergencia.id+')" href="javascript:void(0);">imagenes</a>';
                //info+='<a id="imagenes" onclick="app.cargarModalEdicion('+emergencia.id+')" href="javascript:void(0);">editar</a>';

                this.addMarker('','',actividadLocation,icon,info,'');
                rutas={
                                    'lng':parseFloat(emergencia.X.replace(',','.')),
                                    'lat':parseFloat(emergencia.Y.replace(',','.')),
                                    'orden':emergenciaid,
                                    'color':'#000000'
                                };

                rutasMapa.push(rutas);
                bounds.extend(actividadLocation);
            }
            if (emergenciaid>0) {
                
                this.addLine(rutasMapa);
            }


            this.map.fitBounds(bounds);
            this.loaded=false;
            this.showLeft = false;
        },
    },
    ready: function(){
        this.showEmergencias();
    },/*
    destroyed(){
        clearInterval(this.handle);
    },*/
    events: {
        'google.maps:init': function() {
            this.loaded=true;
            this.directionsService= new google.maps.DirectionsService;
            var myLatlng = new google.maps.LatLng(this.lat_bucket,this.long_bucket);
            this.map = new google.maps.Map(
                document.getElementById('map'), {
                center: myLatlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
            this.loaded=false;
        }
    }
    }
);
window.initMap = function() {
    app.$emit('google.maps:init');
};