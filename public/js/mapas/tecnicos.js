var app = new Vue({
    http: {
        root: '/root',
        headers: {
            'csrftoken': document.querySelector('#token').getAttribute('value')
        }
    },
    el: '#mapatecnicosController',
    components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
    },
    data: {
        loaded: false,
        showLeft: false,
        showRight: false,
        resource_id: 'LARI_INS_MOVISTAR_UNO',
        map:[],
        tec_actividades:[],
        actividadesTipo: {
           'bucket':'En Bucket' ,
           'tecnico':'En Tecnico'
        },
        resources_id: {
           'LARI_INS_MOVISTAR_UNO':'Lari Movistar Uno'
        },
        actividadesTipoSeleccionados: ['tecnico','bucket'],
        tecnicosSeleccionados: [],
        inicio: hoy,
        fin: hoy,
        markers: [],
        flightPath: [],
        flightPaths: [],
        infoWindows: [],
        directionsService: [],
        directionsDisplay: [],
        mensaje: '',
        tipo:'completo',
        colores: [],
        abecedario:['A','B','C','D','E','F','G','H','I','J','K','M','N','L','O','P','Q','R','S','T','U','V','W','x','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AM','AN','AL','AO','AP','AQ','AR','AS','AT','AU','AV'],
        trazos: ['lineas'],
    },
    methods: {
        downloadExcel: function(){
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "/reporte/excelactividadesofsc");
            form.setAttribute("target", "_blank");
            if (this.tipo == 'completo') {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "rango_busqueda[]");
                hiddenField.setAttribute("value", "1");
                form.appendChild(hiddenField);
            } else {
                var hiddenFieldUno = document.createElement("input");
                var hiddenFieldDos = document.createElement("input");
                hiddenFieldUno.setAttribute("name", "fecha_agenda");
                hiddenFieldUno.setAttribute("value", hoy + " - " + hoy);
                form.appendChild(hiddenFieldUno);
                var hiddenFieldDos = document.createElement("input");
                hiddenFieldDos.setAttribute("name", "bucket");
                hiddenFieldDos.setAttribute("value", app.resource_id);
                form.appendChild(hiddenFieldDos);
            }
            document.body.appendChild(form);
            // window.open('');
            form.submit();
        },
        selectAllTecnicos: function (){
            //seleccionar todos los tecnicos
            //validar cuando no se haya seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            if (todos>=0) {
                for ( carnet in this.$get('tecnicos')) {
                    this.tecnicosSeleccionados.push(carnet);
                }
            } else {
                this.tecnicosSeleccionados=[];
            }
        },
        selectTecnico: function (carnet){
            //validar si esta seleccionado
            var todos = this.tecnicosSeleccionados.indexOf('todos');
            var tecnico = this.tecnicosSeleccionados.indexOf(carnet);
            if (tecnico <0 && todos>=0 ) {
                //quitar la marca e todos en tecnicos seleccionados
                this.tecnicosSeleccionados.splice(0, 1);
            }
        },
        mostrarMensaje: function(mensaje){
            this.handle = setInterval( ( ) => {
                app.mensaje='';
            },4000);
            this.mensaje=mensaje;
        },
        mostrarright: function(){
            this.cerrarright_actividades();
            div = document.getElementById('flotante');
            div.style.display = '';
        },
        mostrarright_actividades: function(){
            this.cerrarright();
            div = document.getElementById('f_actividades');
            div.style.display = '';
        },
        cerrarright: function(){
            div = document.getElementById('flotante');
            div.style.display = 'none';
        },
        cerrarright_actividades: function(){
            div = document.getElementById('f_actividades');
            div.style.display = 'none';
        },
        filtro: function(){
            if (this.inicio>this.fin) {
                this.mostrarMensaje('Rango de fechas no validas');
                return;
            }
            this.setMapOnAll();
            var bounds = new google.maps.LatLngBounds();
            var carnet, info, icon, start, time;
            var contenedor='';
            var color;
            var actividadaid;
            var rutas=[];
            var rutas22=[];
            var vbucket=[];
            var vbucketico='';
            // var orden_xactividad=[];
            var tipoActividad;
            var esTecnicoSelec;





            //recorrer actividades
            for ( actividadaid in this.$get('actividades')) {

                actividad = this.$get('actividades')[actividadaid];

                //filtrar x y validos , y actividades no borradas
                if (actividad.coordx!=0 && actividad.coordy!=0 && actividad.status!='deleted' ) {
                    //filtrar fecha
                    if (this.inicio<=actividad.date && actividad.date<=this.fin && actividad.date!='3000-01-01') {
                        actividadLocation = new google.maps.LatLng(actividad.coordy,actividad.coordx);
                        info='<label>Fecha Agenda: </label>&nbsp;&nbsp;<t>' +actividad.date+'<br>';
                        info+='<label>Estado: </label>&nbsp;&nbsp;&nbsp;'+actividad.status+'<br>';
                        info+='<label>Actuacion: </label>&nbsp;&nbsp;&nbsp;'+actividad.appt_number+'<br>';
                        tipoActividad = this.actividadesTipoSeleccionados;
                        //filtro por bucket
                        if (this.resource_id==actividad.resource_id && tipoActividad.indexOf('bucket')>=0) {
                            // this.$set('actividadesBucket', actividad);
                            icon='/img/icons/ofsc/lari/ACT_LARI.png';
                            vbucket.push(actividad.appt_number);
                            vbucketico=icon;
                            this.addMarker(actividad.appt_number,'',actividadLocation,icon,info,'');
                            bounds.extend(actividadLocation);
                        //filtrar por tecnico
                        } else if (this.resource_id!=actividad.resource_id && tipoActividad.indexOf('tecnico')>=0) {
                            esTecnicoSelec = this.tecnicosSeleccionados.indexOf(actividad.resource_id);
                            info+='<label>Tecnico: </label>&nbsp;&nbsp;&nbsp;'+actividad.resource_id+'<br>';
                            if (esTecnicoSelec>=0) {
                                //validar si existe start_time
                                //buscar marcador de actividad para este tecnico
                                color = this.$get('tecnicos')[actividad.resource_id].color;

                                if (typeof actividad.start_time == 'string' && actividad.start_time.length !='0') {
                                    start = actividad.start_time.substring(11, 19);
                                    info+='<label>Hora: </label>&nbsp;&nbsp;&nbsp;'+start;
                                    //añadir al array de rutas
                                    if (typeof  rutas[actividad.resource_id] == undefined || typeof  rutas[actividad.resource_id]=='undefined' || typeof  rutas22[actividad.resource_id] == undefined || typeof  rutas22[actividad.resource_id]=='undefined') {
                                        rutas[actividad.resource_id]=[];
                                        rutas22[actividad.resource_id]=[];
                                    }
                                    rutas[actividad.resource_id].push(actividad.coordx+','+actividad.coordy+','+actividad.start_time+','+color);
                                }

                                icon='/img/icons/ofsc/'+color+'.png';
                                if(rutas22[actividad.resource_id]!=undefined){
                                    rutas22[actividad.resource_id].push({'actividad':actividad.appt_number,'status':actividad.status,'location':actividadLocation,'icon':icon,'info':info,'hora':actividad.start_time});
                                }


                                bounds.extend(actividadLocation);
                            }
                        }
                    }
                }
            }

            // console.log(vbucket);

            this.tec_actividades=[];
            for (var i22 in rutas22) {
                var array_o=rutas22[i22];
                array_o.sort(function(a,b){
                    return new Date(a.hora) - new Date(b.hora);
                });
                var vi=0;
                this.tec_actividades[i22]=[]
                this.tec_actividades[i22]=array_o.length;
                for ( var ar in array_o){
                    this.addMarker(array_o[ar]['actividad'],array_o[ar]['status']+' caja-estado',array_o[ar]['location'],array_o[ar]['icon'],array_o[ar]['info'],'<div class="ordenactividades">'+this.abecedario[vi]+'</div> ');
                    vi++;
                }
            }


            var ordent=[];
            for ( carnet in this.$get('tecnicos')) {
                tecnico = this.$get('tecnicos')[carnet];
                esTecnicoSelec = this.tecnicosSeleccionados.indexOf(carnet);
                //todos = this.tecnicosSeleccionados.indexOf('todos');
                 // ordent[this.tec_actividades[carnet]]=[];
                if (esTecnicoSelec>=0) {
                    if (tecnico.x!=0 && tecnico.y!=0) {
                        color = tecnico.color;
                        icon='/img/icons/ofsc/'+color+'_tec.png';
                        tecnicoLocation = new google.maps.LatLng(tecnico.y,tecnico.x);
                        info='<label>Carnet: </label>&nbsp;&nbsp;<t>' +carnet+'<br>';
                        info+='<label>Fecha Ubicacion: </label>&nbsp;&nbsp;&nbsp;'+tecnico.time;
                        //validar la hora de localizcion del tecnico
                        time = new Date(tecnico.time);

                        hora = new Date(ahora);

                        time = time.getTime()/1000;
                        hora = hora.getTime()/1000;
                        diferencia = hora - time;

                        //20 minutos
                        if (diferencia<1200) {
                            status = 'tecnicoVerde';
                            style='background-color: #A5D6A7; padding: 5px 5px 5px 5px;';
                        //2 horas
                        } else if (diferencia<7200) {
                            status = 'tecnicoNaranja';
                            style='background-color: #FFCC80; padding: 5px 5px 5px 5px;';
                        //4 horas
                        } else if (diferencia<14400) {
                            status = 'tecnicoRojo';
                            style='background-color: #EF9A9A; padding: 5px 5px 5px 5px;';
                        //mas de 4 horas
                        } else {
                            status = 'tecnicoAzul';
                            style='background-color: #9FA8DA; padding: 5px 5px 5px 5px;';
                        }
                        var aorden=''
                        if(this.tec_actividades[carnet]){
                            aorden=this.tec_actividades[carnet];
                        }else{
                            aorden=0;
                        }
                        this.addMarker(carnet,'',tecnicoLocation,icon,info,'');
                        bounds.extend(tecnicoLocation);
                        ordent.push({'cant':aorden,'des':"<li><div class='izquierda'><img src='/img/icons/ofsc/"+ tecnico.color+"_tec.png' ></div><div class='derecha'><span class='nombre-tecnico'><p>"+tecnico.name+" ( "+aorden+" )</p></span><span class='tiempo-orden'><strong style='"+style+"'>"+tecnico.time.substring(11,16)+' ('+tecnico.fuente+")</strong></span></div></li>"});
                        // contenedor+="<p class='car'><img src='/img/icons/ofsc/"+ tecnico.color+"_tec.png' >"+tecnico.name+":&nbsp;&nbsp;&nbsp;&nbsp; #Orden:<br><strong style='"+style+"'>Hora:"+tecnico.time.substring(11,16)+' ('+tecnico.fuente+")</strong></p>";
                    } else {
                        ordent.push({'cant':aorden,'des':"<li><div class='izquierda'><img src='/img/icons/ofsc/"+ tecnico.color+"_tec.png' ></div><div class='derecha'><span class='nombre-tecnico'><p>"+tecnico.name+"&nbsp;&nbsp;&nbsp;&nbsp;</p></span><span class='tiempo-orden'><strong>sin coordenadas</strong></span></li>"});
                        // contenedor+="<p class='car'><img src='/img/icons/ofsc/"+ tecnico.color+"_tec.png' >"+tecnico.name+":&nbsp;&nbsp;&nbsp;&nbsp;<br><strong>sin coordenadas</strong></p>";
                    }
                }
            }

            ordent.sort(function(a,b){
                 return  b.cant - a.cant;
            });

            if(vbucket.length!=0){
                contenedor+="<li><div class='izquierda'><img src='"+vbucketico+"' ></div><div class='derecha'>Bucket :"+vbucket.length+"</div></li>";
            }


            for ( io in ordent ){
                contenedor+=ordent[io]['des'];
            }
            document.getElementById('tecnicos').innerHTML = '<ul>'+contenedor+'</ul>';


            var ruta, numeroRuta=1;
            //dibujando las rutas
            for (var i in rutas) {
                var rutas2=[];
                for (var j = rutas[i].length - 1; j >= 0; j--) {
                    ruta={};
                    ruta.lng= parseFloat( rutas[i][j].split(',')[0] );
                    ruta.lat= parseFloat( rutas[i][j].split(',')[1] );
                    ruta.orden= rutas[i][j].split(',')[2];
                    color=  rutas[i][j].split(',')[3] ;
                    rutas2.push(ruta);
                }
                rutas2.numero= numeroRuta;
                // establecer color de marcador de tecnico

                rutas2.color= '#' + color;
                rutas2.sort(function(a,b){
                    return new Date(a.orden) - new Date(b.orden);
                });
                if (this.trazos.indexOf('lineas')>=0) {
                    this.addLine(rutas2);
                }
                if (this.trazos.indexOf('rutas')>=0) {
                    this.addRoute(rutas2);
                }
                numeroRuta++;
            }

            if (icon!='' && icon!=undefined ) {
                this.map.fitBounds(bounds);
            }
        },
        addRoute: function(rutas2) {
            this.directionsDisplay[rutas2.numero] = new google.maps.DirectionsRenderer;
            this.directionsDisplay[rutas2.numero].setMap(this.map);

            var waypts = [];
            var final=rutas2.length -1;
            for (var i = 1; i < final; i++) {
                if (rutas2[i]) {
                  waypts.push({
                      location: rutas2[i],
                      stopover: true
                  });
                }
            }
            var request = {
                origin: rutas2[0],
                destination: rutas2[final],
                waypoints: waypts,
                optimizeWaypoints: false,
                travelMode: google.maps.TravelMode.DRIVING
            };

            this.directionsService.route(request, function(response, status) {

                if (status === google.maps.DirectionsStatus.OK) {
                    app.directionsDisplay[rutas2.numero].setDirections(response);
                    var lineSymbol = {
                      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                      strokeOpacity: 1,
                      scale: 2
                    };
                    app.directionsDisplay[rutas2.numero].setOptions({
                        suppressMarkers: true,
                        polylineOptions: {
                            strokeWeight: 4,
                            strokeOpacity: 0,
                            strokeColor: rutas2.color,
                            icons: [{
                                icon: lineSymbol,
                                offset: '0',
                                repeat: '20px'
                            }],
                        }
                    });
                } else {
                    app.mostrarMensaje('Solicitud de direcciones suspendida debido a' + status);
                }
            });
        },
        addLine: function(rutas2) {
            // console.log(rutas2);
            this.flightPath = new google.maps.Polyline({
                    path: rutas2,
                    geodesic: true,
                    strokeColor: rutas2.color,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
            this.flightPath.setMap(this.map);
            //array de lineas
            this.flightPaths.push( this.flightPath );
        },
        addMarker: function(labelContent,labelClass,location, icon , info, punto) {

            if (punto === undefined) {
               punto='';
            }

            var componenteIcon = window.location.origin+icon;
            zIndexRep=0;
            var marker = new MarkerWithLabel({
                position: location,
                icon: componenteIcon,
                map: this.map,
                title: labelContent,
                zIndex: zIndexRep++,
                labelContent: punto,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: labelClass,
                labelStyle: {opacity: 0.85}
            });

            var infowindow = new google.maps.InfoWindow({
                content: info
            });
            this.markers.push(marker);
            //click en marcadores
            marker.addListener('click', function(event) {
                if(app.infoWindows.length>0){
                    for (var i=0;i<app.infoWindows.length;i++) {
                        app.infoWindows[i].close();
                    }
                }
                infowindow.open(this.map, marker);
                app.infoWindows.push(infowindow);
            });
        },
        setMapOnAll: function() {
            this.removeRoutes();
            this.removeLines();
            this.removeMarkers();
        },
        removeRoutes: function() {
            //recorrer rutas
            for (var i in this.directionsDisplay) {
                this.directionsDisplay[i].setDirections({routes: []});
                this.directionsDisplay[i].setMap(null);

            }
        },
        removeLines: function(){
            for (var i = 0; i < this.flightPaths.length; i++) {
                this.flightPaths[i].setMap(null);
            }
        },
        removeMarkers: function() {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        },
        showActividades: function(resource_id) {
            this.$http.get('/actividadesofsc/' + resource_id, function (actividades) {
                this.$set('actividades', actividades);
                this.loaded=false;
            });

        },
        showTecnicos: function(resource_id) {
            this.$http.get('/tecnicosofsc/' + resource_id, function (tecnicos) {
                colores = ['00FF00','00FFFF','0000FF','000000',
                '000080','008000','008080','800000','800080','808000',
                '808080','C0C0C0','FF00FF','FF0000','FF8800','FFFF00',
                'FFFFFF','80FF00'];
                var j=0;
                // console.log(tecnicos);
                for ( var i in tecnicos) {
                    tecnicos[i].color=colores[j];
                    j++;
                }
                this.$set('tecnicos', tecnicos);
            });
        }
    },
    ready: function(){
        this.showTecnicos(this.resource_id);
        this.showActividades(this.resource_id);
    },/*
    destroyed(){
        clearInterval(this.handle);
    },*/
    events: {
        'google.maps:init': function() {
            this.loaded=true;
            this.directionsService= new google.maps.DirectionsService;

            var myLatlng = new google.maps.LatLng(-12.109047,-76.94072);
            this.map = new google.maps.Map( document.getElementById('map'), {
                center: myLatlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
    }
});

window.initMap = function() {
    app.$emit('google.maps:init');
};
