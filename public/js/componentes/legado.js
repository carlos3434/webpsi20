var vm = new Vue({
    http: {
      root: '/root',
      headers: {
        'csrftoken': document.querySelector('#token').getAttribute('value')
      }
    },
    el: '#componenteController',

    data: {
        id: '',
        success: false,
        danger: false,
        msj: '',
        componente:{
            accion:'',
            actividad_id:'',
            num_requerimiento:'',
            numser:'',
            codmat:'',
            numtar:'',
            telefonoOrigen:'',
            rst:'',
            hashg:'',
            interfaz:2,
        }
    },

    methods: {
        EjecutarDecoLegado: function(){
            this.$http.post('/api/operation' , this.componente,  function (data) {
                if (data.rst==1) {
                    vm.componente.rst=1;
                } else {
                    vm.componente.rst=0;
                    vm.ShowMensaje(data.msj, 5, false, true);
                }
                setTimeout(function () {
                    vm.componente.rst='';
                }, 5000);

            }).error(function(errors) {
                vm.componente.rst=0;
            }); 
        },
        ShowMensaje: function(msj, time, success, danger) {
            this.msj=msj;
            self = this;
            this.danger = danger;
            this.success = success;
            setTimeout(function () {
                self.danger = false;
                self.success = false;
            }, time*1000);
        }
    },
    ready: function () {
    }
});