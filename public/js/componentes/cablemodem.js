    $(document).ready(function () {

        
        $('#form_cablemodem').submit(function(e) {
            e.preventDefault();
            Cablemodem.Consulta();
        });

    });

    eventoCargaMostrar = function () {
        $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
    }
    eventoCargaRemover = function () {
        $(".overlay,.loading-img").remove();
    }


    var Cablemodem = {
        Consulta: function (evento) {
            var mac = $("#mac").val();
            var token = document.querySelector('#token').getAttribute('value');
            $.ajax({
                url: '../cablemodem/consulta',
                type: 'POST',
                cache: false,
                dataType: 'json',
                data: {mac: mac},
                headers: {
                    'csrftoken':token
                },
                beforeSend: function () {
                    eventoCargaMostrar();
                },
                success: function (obj) {
                    eventoCargaRemover();
                    if (obj.rst == 1) {
                        $("#result").html(obj.datos);
                    } else
                        Psi.mensaje('warning', obj.datos, 6000);
                },
                error: function () {
                    $("#tb_componentes").html('');
                    eventoCargaRemover();
                }
            });
        }
    };