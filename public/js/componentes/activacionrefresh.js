var vm = new Vue({
    http: {
      root: '/root',
      headers: {
        'csrftoken': document.querySelector('#token').getAttribute('value')
      }
    },
    el: '#componenteController',

    data: {
        id: '',
        carnet: '',
        success: false,
        danger: false,
        msj: ''
    },

    methods: {
        EjecutarDecos: function (componentes, modems) {
            var enviados= 0,msj='', modem = 0;
            for (var i = 0; i < componentes.length; i++) {
                if (componentes[i].accion =='activacion' && 
                    componentes[i].serie !=='' &&
                    componentes[i].tarjeta !=='') {
                    this.EjecutarDeco(componentes[i],0);
                    enviados++;
                } else if (componentes[i].accion =='refresh' && 
                    componentes[i].serie !=='') {
                    this.EjecutarDeco(componentes[i],0);
                    enviados++;
                }
            }
            for (var i = 0; i < modems.length; i++) {
                var mac = modems[i].mac;
                if (mac !=='' && mac !== undefined) {
                    this.EjecutarDeco(modems[i],1);
                    modem++;
                }
            }

            msj='';
            if (enviados>0 || modem>0) {
                if (enviados>0) {
                    msj+=enviados+' componentes enviados. ';
                }
                if (modem>0) {
                    msj+=modem+' modem enviado.';
                }
                this.ShowMensaje(msj, 5, true, false);
            }

            if (enviados==0 && modem==0) {
                msj='no se enviaron componentes.';
                this.ShowMensaje(msj, 5, false, true);
            }
        },
        EjecutarDeco: function (componente, tipo) {

            if (tipo == 1) {
                var deco = {
                    id:componente.id,
                    mac:componente.mac,
                    gestion_id:this.id,
                    accion:'modem',
                    telefono: this.tecnico.celular
                };
            } else {
                var deco = {
                    id:componente.id,
                    serie:componente.serie,
                    tarjeta:componente.tarjeta,
                    gestion_id:this.id,
                    accion:componente.accion,
                    telefono: this.tecnico.celular
                };
            }

            this.$http.post('/decodificador' , deco,  function (data) {
                if (data.rst==1) {
                    componente.rst=1;
                } else {
                    componente.rst=0;
                    //this.ShowMensaje(data.msj, 5, false, true);
                    alert(data.msj);
                }
                setTimeout(function () {
                    componente.rst='';
                }, 5000);

            }).error(function(errors) {
                componente.rst=0;
            }); 
        },
        ShowDeco: function (id) {
            this.$http.get('/decodificador/' + id, function (datos) {
               this.$set('modems', datos.modem);
               this.$set('componentes', datos.componentes);
               
            });
        },
        ShowTecnico: function (carnet) {
            this.$http.get('/tecnicos/' + carnet, function (tecnico) {
                this.$set('tecnico', tecnico);
            });
        },
        ShowMensaje: function(msj, time, success, danger) {
            this.msj=msj;
            self = this;
            this.danger = danger;
            this.success = success;
            setTimeout(function () {
                self.danger = false;
                self.success = false;
            }, time*1000);
        }
    },
    ready: function () {
        this.ShowTecnico(this.carnet);
        this.ShowDeco(this.id);
    }
});