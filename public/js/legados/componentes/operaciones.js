Vue.config.debug = true;
Vue.config.devtools = true;

var vm = new Vue({
	http: {
		root: '/componentelegado',
		headers: {
			'csrftoken': document.querySelector('#token').getAttribute('value')
		}
	},
	el: 'body',
	
	components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },
	data: {
		modemsmarca : {},
		modemsmodelo : {},
		equipotelefonicosmarca : {},
		equipotelefonicosmodelo : {},
		codactu : '',
		usuario : '',
		tipost : '',
		tipolegado : '',
		idsolicitudtecnica: '',
		flag : 0,
		componentes : {},
		decos : {},
		tarjetas : {},
		modems : {},
		lineavoips : {},
		equipotelefonicos : {},
		tipo : { 
			cambiaraveria : '',
			revertiraveria : '',
			reposicion : '',
			revertirreposicion : '',
		},
		trama2 : {
			cambiaraveria : {},
			revertiraveria : {},
			reposicion : {},
			revertirreposicion : {},
		},
		datachecked : {},
		tarjetasAlmacen : {},
		tipoaverias : {},
		problemaaverias : {},
		codtipoaveria : '',
		codproblemaaveria : '',
		tipocomponente : '',
		componentesRA: {},
		componentesRR: {},
		chk_cambio_averia: {},
		errors: {
			cambiaraveria_deco: {},
			cambiaraveria_tarjeta: {},
			reposicion_deco: {},
			reposicion_tarjeta: {},
			refresh: []
		},
		collapsed : true,


	},

	methods: {
		ListarComponentes: function(codactu, usuario, tipost, tipolegado) {
			this.eventoCargaMostrar();
			var request = {
                codactu: codactu,
                usuario: usuario,
                tipost: tipost,
                tipolegado : tipolegado,
                idsolicitudtecnica: this.idsolicitudtecnica
            };
			this.$http.get('cargar', request, function(datos){
				if (datos.rst == 2) {
					swal("Error", datos.msj, "error");
					this.eventoCargaRemover();
					return;
				}
				this.$set('decos', datos.decos);
				this.$set('tarjetas', datos.tarjetas);
				this.$set('modems', datos.modems);
				this.$set('lineavoips', datos.lineavoip);
				this.$set('equipotelefonicos', datos.equipotelefonico);
				this.$set('componentes', datos.componentes);
				this.eventoCargaRemover();
			})
		},

		ListarComponentesReversionAveria: function() {
			var request = {
                codactu: this.codactu,
                usuario: this.usuario
            };
			this.$http.post('listarrevertiraveria', request, function(datos){
				this.$set('componentesRA', datos.componentes);
			})
		},

		ListarComponentesReversionReposicion: function() {
			var request = {
                codactu: this.codactu,
                usuario: this.usuario
            };
			this.$http.post('listarrevertirreposicion', request, function(datos){
				this.$set('componentesRR', datos.componentes);
			})
		},

		OperacionDeco: function(deco, tarjeta, operation, index) { // asignacion, desasignacion, refresh
			index = typeof index !== 'undefined' ? index : '';
			var validacion = this.validarInputs(deco, tarjeta, operation);
			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}

			var nameOperation = this.typeOperation(operation);
			this.eventoCargaMostrar();
			var request = {
				codreq : deco.num_requerimiento, // num_requerimiento (ST)
				indorigreq : deco.ind_origen_requerimiento, // Indicador de requerimiento (ST)
				numcompxreq : deco.numero_requerimiento, // numero_requerimiento DECO (C)
				codmat : deco.codigo_material, // codigo_material DECO
				numser : deco.numero_serie, // serie DECO
				codtar : tarjeta.codigo_material, // codigo_material TARJETA
				numtar : tarjeta.numero_serie, // serie TARJETA
				deco_componente_id: deco.id, // id componente DECO
				tarjeta_componente_id: tarjeta.id, // id componente TARJETA
				id_solicitud_tecnica: deco.id_solicitud_tecnica,
				solicitud_tecnica_id: deco.solicitud_tecnica_id,
				usuario: this.usuario,
			};

			this.$http.post(nameOperation, request, function(obj){
				if (obj.rst == 1) {
					swal("Exito", obj.msj, "success");
					if (operation == 1){
						this.decos[index]['numero_serie'] = '';
						this.decos[index]['flag'] = '';
						this.tarjetas[index]['numero_serie'] = '';
						this.decos[index]['codigo_material'] = '';
					}
				} else {
					//console.log(obj.msj);
					swal("Error", obj.msj, "error");
				}

				//this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();


			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		GuardarEquipos: function() {
			var equipos = [];
			for (var i in this.equipotelefonicos) {
				if (!this.equipotelefonicos[i].numero_serie ||
					this.equipotelefonicos[i].numero_serie == 0) {
					swal("Error", 'No se permite serie vacia o con valor "0"', "error");
					return;
				}
				equipos.push({
					id: this.equipotelefonicos[i].id, 
					numero_serie: this.equipotelefonicos[i].numero_serie,
					marca: this.equipotelefonicos[i].marca,
					modelo: this.equipotelefonicos[i].modelo
				});
			}

			for (var j in this.modems) {
				if (!this.modems[j].numero_serie ||
					this.modems[j].numero_serie == 0) {
					swal("Error", 'No se permite serie vacia o con valor "0"', "error");
					return;
				}
				equipos.push({
					id: this.modems[j].id, 
					numero_serie: this.modems[j].numero_serie,
					marca: this.modems[j].marca,
					modelo: this.modems[j].modelo
				});
			}

			this.$http.post('modem', {equipos: equipos}, function(datos) {
				if (datos.rst == 1) {
					this.flag = 1;
					swal("Ok", datos.msj, "success");
				}
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
            });
		},

		GuardarModem: function(modem,equipo) {
			var request = {
				id : modem.id,
				serie : modem.numero_serie,
			};
			this.$http.post('modem', request, function(datos){
				if (datos.rst == 1) {
					this.flag = 1;
					swal("Exito", equipo + " Registrado" , "success");
				} else {
					this.flag = 2;
					swal("Error", "Error en el registro de serie", "error");
				}
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
            });
		},
		
		CapturarTrama: function(trama, operation, componente) {
			this.datachecked = {};
			this.datachecked = trama;
			switch(operation) {
	    		case 2:
	    			this.tipo.cambiaraveria = "";
	    			this.tipo.cambiaraveria = componente;
	    			break;
	    		case 3:
	    			this.tipo.revertiraveria = "";
	    			this.tipo.revertiraveria = componente;
	    			break;
	    		case 4:
	    			this.tipo.reposicion = "";
	    			this.tipo.reposicion = componente;
	    			break;
	    		case 5:
	    			this.tipo.revertirreposicion = "";
	    			this.tipo.revertirreposicion = componente;
	    			break;
	    	}
		},

		OperacionCambioAveria2: function() {
			this.eventoCargaMostrar();
			var request = [];
			var msj = "";
			for (var i in this.componentes) {
				if (this.componentes[i].checkbox) {
					request.push({
						codreq : this.componentes[i].num_requerimiento,
						indorigreq : this.componentes[i].ind_origen_requerimiento,
						numcompxreq : this.componentes[i].numero_requerimiento,
						numcompxsrv : this.componentes[i].numero_servicio,
						codmat : this.componentes[i].codigo_material_new_deco,
						numser : this.componentes[i].numero_serie_new_deco,
						numserold : this.componentes[i].numero_serie,
						codmatold : this.componentes[i].codigo_material,
						codmatpar : this.componentes[i].par_tarjeta.codigo_material, // codmat par
						numserpar : this.componentes[i].par_tarjeta.numero_serie, // numser par
						componente_id: this.componentes[i].id,
						id_solicitud_tecnica: this.componentes[i].id_solicitud_tecnica,
						solicitud_tecnica_id: this.componentes[i].solicitud_tecnica_id,
						descripcion: this.componentes[i].descripcion,
					});

					if (this.componentes[i].checkboxtarjeta) {
						request.push({
							codreq : this.componentes[i].par_tarjeta.num_requerimiento,
							indorigreq : this.componentes[i].par_tarjeta.ind_origen_requerimiento,
							numcompxreq : this.componentes[i].par_tarjeta.numero_requerimiento,
							numcompxsrv : this.componentes[i].par_tarjeta.numero_servicio,
							codmat : this.componentes[i].codigo_material_new_tarjeta,
							numser : this.componentes[i].numero_serie_new_tarjeta,
							numserold : this.componentes[i].par_tarjeta.numero_serie,
							codmatold : this.componentes[i].par_tarjeta.codigo_material,
							codmatpar : this.componentes[i].codigo_material, // codmat par
							numserpar : this.componentes[i].numero_serie, // numser par
							componente_id: this.componentes[i].par_tarjeta.id,
							id_solicitud_tecnica: this.componentes[i].par_tarjeta.id_solicitud_tecnica,
							solicitud_tecnica_id: this.componentes[i].par_tarjeta.solicitud_tecnica_id,
							descripcion : this.componentes[i].par_tarjeta.descripcion,
						});
					}
				}
			}

			for (var k in request) {
				this.$http.post('cambioaveria', request[k], function(datos){
					msj += "Serie: <i style='color: #23709e'>"+datos.numserold+"</i> | ";
					msj += datos.rst == 1 ? "<b style='color: green'>OK</b> | "+datos.msj+"<br>" : "<b style='color: red'>ERROR</b> | "+datos.msj+"<br>";
				}).error(function(errors) {
			        swal("Error", 'Ocurrió un error durante el proceso.', "error");
			    });
			}

			setTimeout(function () {
				vm.eventoCargaRemover();
                swal({
                    title: "VERIFICAR<br>La confirmación de la(s) operacion(es) <b>'OK'</b> llegara(n) a su celular en breve",
                    text: msj,
                    type: "warning",
                    html: true,
                    confirmButtonClass: "btn-success",
                });
                vm.ListarComponentes(vm.codactu, vm.usuario, vm.tipost, vm.tipolegado);
            }, 5000);
		},

		OperacionCambioAveria: function(trama, componente) {
			var validacion = 0;
			var contador = this.validarCheck("rdb_cambio_averia");
			if (contador == 0) {
				swal("Error", 'Seleccionar componente', "error");
				return;
			}

			if (componente == "deco") {
				validacion = this.validarInputs(trama, '', 2);
			} else {
				validacion = this.validarInputs('', trama, 2);
			}
			if (validacion == 1 || validacion == 2) {
				return;
			}
			this.eventoCargaMostrar();
			var componentePar = (componente == "deco") ? trama.par_tarjeta : trama.par_deco;
			var par = componentePar.split("|");
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				numcompxreq : this.datachecked.numero_requerimiento,
				numcompxsrv : this.datachecked.numero_servicio,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta, // material nueva
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta, // serie nueva
				numserold : this.datachecked.numero_serie,
				codmatold : this.datachecked.codigo_material,
				codmatpar : par[0], // codmat par
				numserpar : par[1], // numser par
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario : this.usuario,
			};
			this.$http.post('cambioaveria', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionRevertirAveria: function(trama, componente) {
			var validacion = 0;
			var contador = this.validarCheck("rdb_revertir_averia");
			if (contador == 0) {
				swal("Error", 'Seleccionar componente', "error");
				return;
			}
			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				codmat : this.datachecked.codigo_material,
				numser : this.datachecked.numero_serie,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario: this.usuario,
				numero_serie_antigua : trama.numero_serie_antigua,
			};
			this.$http.post('revertiraveria', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionReposicion: function(trama, componente) {
			var validacion = 0;
			var contador = this.validarCheck("rdb_reposicion");
			if (contador == 0) {
				swal("Error", 'Seleccionar componente', "error");
				return;
			}

			if (componente == "deco") {
				validacion = this.validarInputs(trama, '', 4);
			} else {
				validacion = this.validarInputs('', trama, 4);
			}
			if (validacion == 1 || validacion == 2) {
				return;
			}

			this.eventoCargaMostrar();
			var componentePar = (componente == "deco") ? trama.par_tarjeta : trama.par_deco;
			var par = componentePar.split("|");
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				numcompxreq : this.datachecked.numero_requerimiento,
				numcompxsrv : this.datachecked.numero_servicio,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta,
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta,
				numserold : this.datachecked.numero_serie,
				codmatold : this.datachecked.codigo_material,
				codmatpar : par[0], // codmat par
				numserpar : par[1], // numser par
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario : this.usuario,
			};
			this.$http.post('reposicion', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionRevertirReposicion: function(trama, componente) {
			var validacion = 0;
			var contador = this.validarCheck("rdb_revertir_reposicion");
			if (contador == 0) {
				swal("Error", 'Seleccionar componente', "error");
				return;
			}
			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				codmat : this.datachecked.codigo_material,
				numser : this.datachecked.numero_serie,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario: this.usuario,
			};
			this.$http.post('revertireposicion', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		eventoCargaMostrar: function () {
			var loading = document.getElementsByClassName("loading-img");
	        if ( loading.length == 0 ) {
	        	$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
	        }
	    },

	    eventoCargaRemover: function () {
        	$(".overlay,.loading-img").remove();
	    },

	    validarInputs: function (deco, tarjeta, operation) {
	    	switch(operation) {
	    		case 0: case 1: case 6:
	    			var numser = deco.numero_serie;
			    	var numtar = tarjeta.numero_serie;
					var numserR = /^\d+$/.test(numser);
					var numtarR = /^\d+$/.test(numtar);
					if (numser == '' || numtar == '') {
						return 1;
					} else if (!numserR || !numtarR) {
						return 2;
					} else {
						return 3;
					}
					break;
				case 2: case 4:
					var codigo_material_new = '';
			    	var numero_serie_new = '';
			    	var valor = [];

			    	if (deco == '') {
			    		codigo_material_new = tarjeta.codigo_material_new_tarjeta;
				    	numero_serie_new = tarjeta.numero_serie_new_tarjeta;
			    	} else {
			    		codigo_material_new = deco.codigo_material_new_deco;
				    	numero_serie_new = deco.numero_serie_new_deco;
			    	}
					
					if (codigo_material_new == undefined || (codigo_material_new.length != 8 && codigo_material_new.length != 11)) {
						swal("Error", 'El codigo de material debe tener 8 dígitos', "error");
						return 2;
					} else if (numero_serie_new == undefined || numero_serie_new.length != 12) {
						swal("Error", 'La serie debe tener 12 dígitos', "error");
						return 2;
					} else {
						return 3;
					}
					break;
	    	}
	    },

	    typeOperation: function (type) {
	    	var nameOperation = '';
	    	switch(type) {
	    		case 0:
	    			nameOperation = 'activar';
	    			break;
	    		case 1:
	    			nameOperation = 'desasignar';
	    			break;
	    		case 2:
	    			nameOperation = 'cambioaveria';
	    			break;
	    		case 3:
	    			nameOperation = 'revertiraveria';
	    			break;
	    		case 4:
	    			nameOperation = 'reposicion';
	    			break;
	    		case 5:
	    			nameOperation = 'revertirreposicion';
	    			break;
	    		case 6:
	    			nameOperation = 'refresh';
	    			break;
	    	}
	    	return nameOperation;
	    },

	    TarjetasAlmacen: function () {
	    	this.$http.get('tarjetas', function(datos){
				this.$set('tarjetasAlmacen', datos);
			});
	    },

	    validarCheck: function (inputName) {
	    	var contador = 0;
			var rdb_button = document.getElementsByName(inputName);
			for (var i=0; i < rdb_button.length; i++) {
				if (rdb_button[i].checked) {
					contador = 1;
				}
			}
			return contador;
	    },
		
		keyValidation: function(event, button_name, key, type, multi_items = 'normal'){
			let span_error_name = key + button_name + '_' + type;
			console.log(span_error_name);
			let error_message = document.getElementById( span_error_name );

			if(multi_items  == 'normal' ){
				switch( button_name ){
					case 'button_averia':
						error_bag = this.errors.cambiaraveria_deco;
						value = this.trama2.cambiaraveria.numero_serie_new_deco;
						if( key == 0 ) {
							value = this.trama2.cambiaraveria.codigo_material_new_deco;
						}
						error = document.getElementById('averia_error');
						break;
					case 'button_averia_2':
						error_bag = this.errors.cambiaraveria_tarjeta;
						value = this.trama2.cambiaraveria.numero_serie_new_tarjeta;
						break;
					case 'button_reposicion':
						error_bag = this.errors.reposicion_deco
						value = this.trama2.reposicion.numero_serie_new_deco;
						if( key == 0 ) {
							value = this.trama2.reposicion.codigo_material_new_deco;
						}
						error = document.getElementById('reposicion_error');
						break;
					case 'button_reposicion_2':
						error_bag = this.errors.reposicion_tarjeta;
						value = this.trama2.reposicion.numero_serie_new_tarjeta;
						break;
				}
			}
			

			button = document.getElementById(button_name);
			button_status = false;

			max_value_length = [8, 11];
			if(type == 'deco'){
				max_value_length = [12, 12];
			}

			if(value.length == max_value_length[0] || value.length == max_value_length[1]  || value.length == 0){
				error_message.style.display = "none";
				error_bag[key] = false;
				//this.trama2.cambiaraveria.error = false;
			}
			else{
				error_message.style.display = "block";
				error_bag[key] = true;
				//this.trama2.cambiaraveria.error = true;
			}
			
			for( i in error_bag ){
				if( error_bag[i] != undefined ){
					if( error_bag[i] == true ){
						button_status = true;
						break;
					}
				}	
			}
			button.disabled = button_status;	
		},

		keyValidationMultiple: function(event, index, button_name, key, type){
			let span_error_name = key + 'refresh_error' + index;

			let button = document.getElementById( button_name );
			let error_message = document.getElementById( span_error_name );
			button_status = false;

			if( type == 'tarjeta' ) {
				value = this.tarjetas[index].numero_serie;
			} else {
				value = this.decos[index].numero_serie;
			}

			if( typeof this.errors.refresh[index]  !== 'object'){
				this.errors.refresh[index] = {};
			}
			

			if( value.length == 12 || value.length == 0) {
				this.errors.refresh[index][key] = false;
				error_message.style.display = "none";
			}
			else{
				this.errors.refresh[index][key] = true;
				error_message.style.display = "block";
			}
			
			for ( i in this.errors.refresh[index] ) {
				if(this.errors.refresh[index] != undefined) {
					if(this.errors.refresh[index][i] == true) {
						button_status = true;
						break;
					}
				}
			}

			button.disabled = button_status;
		},



		limpiarCheck: function(inputName) {
			var elemento = document.getElementsByName(inputName);
	    	for (var i=0; i<elemento.length; i++) {
				elemento[i].checked = false;
	    	}
			return;
		},

		ModemsMarcaModelo: function () {
			this.$http.get('modemmarcamodelo', function(datos){
				this.$set('modemsmarca', datos.marca);
				this.$set('modemsmodelo', datos.modelo);
			});
		},

		EquipoTelefonicosMarcaModelo: function () {
			this.$http.get('equipotelefonicomarcamodelo', function(datos){
				this.$set('equipotelefonicosmarca', datos.marca);
				this.$set('equipotelefonicosmodelo', datos.modelo);
			});
		},
	},

	ready: function() {
		this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
		this.TarjetasAlmacen();
		// this.ModemsMarcaModelo();
		// this.EquipoTelefonicosMarcaModelo();
        $("div > div > h4 > a").on('click', function (e) {
    		if ($(this).text() == "REVERTIR AVERIA") {
            	$(this).addClass("activate");
                vm.ListarComponentesReversionAveria();
            } else if ($(this).text() == "REVERTIR REPOSICION") {
            	vm.ListarComponentesReversionReposicion();
            }
        });
	}
});