var vm = new Vue({
	http: {
		root: '/componentelegado',
		headers: {
			'csrftoken': document.querySelector('#token').getAttribute('value')
		}
	},

	el: 'body',

	data: {
		codactu : '',
		rst : 0,
	},

	methods: {
		ListarComponentes: function(codactu) {
			var request = {
                codactu: codactu
            };
			this.$http.get('cargar', request, function(datos){
				this.$set('componentes', datos.data);
			})
		},

		ActivarDeco: function(componente) {
			this.eventoCargaMostrar();
			var request = {
				codreq : this.codactu, // codigo requerimiento
				indorigreq : 0, // Indicador de requerimiento
				numcompxreq : componente.componente_cod, // Numero de componente de requerimiento del deco
				codmat : componente.codigo_material, // Material Deco
				numser : componente.seriedeco, // Serie Deco
				codtar : 0, // Material tarjeta
				numtar : componente.serietarjeta, // serie tarjeta
				codact : 0 // Código de activación
			};

			this.$http.post('activar',request, function(datos){
				this.rst = datos.rst;
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "danger");
            });
		},

		eventoCargaMostrar: function () {
			var loading = document.getElementsByClassName("loading-img");
	        if ( loading.length == 0 ) {
	        	$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
	        }
	    },

	    eventoCargaRemover: function () {
	        $(".overlay,.loading-img").remove();
	    }
	},

	ready: function() {
		this.ListarComponentes(this.codactu);
	}
})