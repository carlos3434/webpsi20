Vue.config.debug = true;
Vue.config.devtools = true;

Vue.component('list-componentes', {
    template: '#deta_componentes',
    props: ['componentes', 'tipocomponente', 'datachecked', 'tipo'],
    methods: {
		limpiarCheck: function (inputName) {
			var elemento = document.getElementsByName(inputName);
	    	for (var i=0; i<elemento.length; i++) {
				elemento[i].checked = false;
	    	}
			return;
		},

		CapturarTrama: function(trama, operation, componente) {
			// ******************** nuevo cambio **************************
			if (componente == "" || componente == undefined) {
				swal("Error", 'Seleccionar tipo de componente', "error");
				var ele = document.getElementsByName("rdb_cambio_averia");
				for(var i=0;i<ele.length;i++)
					ele[i].checked = false;
				return;
			}
			console.log(trama);
			// ************************************************************
			this.datachecked = trama;
			switch(operation) {
	    		case 2:
	    			this.tipo.cambiaraveria = "";
	    			this.tipo.cambiaraveria = componente;
	    			break;
	    		case 3:
	    			this.tipo.revertiraveria = "";
	    			this.tipo.revertiraveria = componente;
	    			break;
	    		case 4:
	    			this.tipo.reposicion = "";
	    			this.tipo.reposicion = componente;
	    			break;
	    		case 5:
	    			this.tipo.revertirreposicion = "";
	    			this.tipo.revertirreposicion = componente;
	    			break;
	    	}
		},
	}
});

new Vue({
	http: {
		root: '/componentelegado',
		headers: {
			'csrftoken': document.querySelector('#token').getAttribute('value')
		}
	},
	el: 'body',
	
	components: {
        'v-select': VueStrap.select,
        'v-option': VueStrap.option,
        'checkbox-group': VueStrap.checkboxGroup,
        'checkbox': VueStrap.checkboxBtn,
        'accordion': VueStrap.accordion,
        'datepicker': VueStrap.datepicker,
        'alert': VueStrap.alert,
        'modal': VueStrap.modal,
        'aside': VueStrap.aside,
        'panel': VueStrap.panel,
        'spinner': VueStrap.spinner,
        'tabs': VueStrap.tabset,
        'tab': VueStrap.tab
    },
	data: {
		codactu : '',
		usuario : '',
		tipost : '',
		tipolegado : '',
		flag : 0,
		componentes : {},
		decos : {},
		tarjetas : {},
		modems : {},
		lineavoips : {},
		equipotelefonicos : {},
		tipo : { 
			cambiaraveria : '',
			revertiraveria : '',
			reposicion : '',
			revertirreposicion : '',
		},
		trama2 : {
			cambiaraveria : {},
			revertiraveria : {},
			reposicion : {},
			revertirreposicion : {},
		},
		datachecked : {},
		tarjetasAlmacen : {},
		tipoaverias : {},
		problemaaverias : {},
		codtipoaveria : '',
		codproblemaaveria : '',
		tipocomponente : ''
	},

	methods: {
		ListarComponentes: function(codactu, usuario, tipost, tipolegado) {
			this.eventoCargaMostrar();
			var request = {
                codactu: codactu,
                usuario: usuario,
                tipost: tipost,
                tipolegado : tipolegado
            };
			this.$http.get('cargar', request, function(datos){
				if (datos.rst == 2) {
					swal("Error", datos.msj, "error");
					this.eventoCargaRemover();
					return;
				}
				this.$set('decos', datos.decos);
				this.$set('tarjetas', datos.tarjetas);
				this.$set('modems', datos.modems);
				this.$set('lineavoips', datos.lineavoip);
				this.$set('equipotelefonicos', datos.equipotelefonico);
				this.$set('componentes', datos.componentes);
				this.eventoCargaRemover();
			})
		},

		OperacionDeco: function(deco, tarjeta, operation) { // asignacion, desasignacion, refresh
			var validacion = this.validarInputs(deco, tarjeta, operation);
			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}

			var nameOperation = this.typeOperation(operation);
			this.eventoCargaMostrar();
			var request = {
				codreq : deco.num_requerimiento, // num_requerimiento (ST)
				indorigreq : deco.ind_origen_requerimiento, // Indicador de requerimiento (ST)
				numcompxreq : deco.numero_requerimiento, // numero_requerimiento DECO (C)
				codmat : deco.codigo_material, // codigo_material DECO
				numser : deco.numero_serie, // serie DECO
				codtar : tarjeta.codigo_material, // codigo_material TARJETA
				numtar : tarjeta.numero_serie, // serie TARJETA
				deco_componente_id: deco.id, // id componente DECO
				tarjeta_componente_id: tarjeta.id, // id componente TARJETA
				id_solicitud_tecnica: deco.id_solicitud_tecnica,
				solicitud_tecnica_id: deco.solicitud_tecnica_id,
				usuario: this.usuario,
			};

			this.$http.post(nameOperation, request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		GuardarModem: function(modem) {
			var request = {
				id : modem.id,
				serie : modem.numero_serie
			};
			this.$http.post('modem', request, function(datos){
				if (datos.rst == 1) {
					this.flag = 1;
					swal("Exito", datos.msj, "success");
				} else {
					this.flag = 2;
					swal("Error", "Error en el registro de serie", "error");
				}
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
            });
		},
		
		CapturarTrama: function(trama, typecomponente) {
			// ******************** nuevo cambio **************************
			if (typecomponente == "" || typecomponente == undefined) {
				swal("Error", 'Seleccionar tipo de componente', "error");
				var ele = document.getElementsByName("rdb_cambio_averia");
				for(var i=0;i<ele.length;i++)
					ele[i].checked = false;
				return;
			}
			// ************************************************************
			this.datachecked = {};
			this.datachecked = trama;
		},

		OperacionCambioAveria: function(trama, componente) {
			var validacion = 0;
			if (componente == "deco") {
				validacion = this.validarInputs(trama, '', 2);
			} else {
				validacion = this.validarInputs('', trama, 2);
			}
			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}
			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				numcompxreq : this.datachecked.numero_requerimiento,
				numcompxsrv : this.datachecked.numero_servicio,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta, // material nueva
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta, // serie nueva
				numserold : this.datachecked.numero_serie, // numero serie actual
				codmatpar : (componente == "deco") ? trama.codmatpar_tarjeta : trama.codmatpar_deco,
				numserpar : (componente == "deco") ? trama.numserpar_tarjeta : trama.numserpar_deco,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario : this.usuario,
				// codtipoaveria : this.codtipoaveria,
				// codproblemaaveria : this.codproblemaaveria,
			};
			this.$http.post('cambioaveria', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionRevertirAveria: function(trama, componente) {
			var validacion = 0;
			if (componente == "deco") {
				validacion = this.validarInputs(trama, '', 3);
			} else {
				validacion = this.validarInputs('', trama, 3);
			}

			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}
			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta,
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta,
				codmatpar : (componente == "deco") ? trama.codmatpar_tarjeta : trama.codmatpar_deco,
				numserpar : (componente == "deco") ? trama.numserpar_tarjeta : trama.numserpar_deco,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario: this.usuario,
				numero_serie_antigua : trama.numero_serie_antigua,
			};
			this.$http.post('revertiraveria', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionReposicion: function() {
			var validacion = 0;
			if (this.tipocomponente == "deco") {
				validacion = this.validarInputs(this.datachecked, '', 4);
			} else {
				validacion = this.validarInputs('', this.datachecked, 4);
			}

			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}

			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				numcompxreq : this.datachecked.numero_requerimiento,
				numcompxsrv : this.datachecked.numero_servicio,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta,
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta,
				numserold : this.datachecked.numero_serie,
				codmatpar : (componente == "deco") ? trama.codmatpar_tarjeta : trama.codmatpar_deco,
				numserpar : (componente == "deco") ? trama.numserpar_tarjeta : trama.numserpar_deco,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario : this.usuario,
			};
			this.$http.post('reposicion', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		OperacionRevertirReposicion: function(trama, componente) {
			var validacion = 0;
			if (componente == "deco") {
				validacion = this.validarInputs(trama, '', 5);
			} else {
				validacion = this.validarInputs('', trama, 5);
			}

			if (validacion == 1) {
				swal("Error", 'Las series son requeridas', "error");
				return;
			} else if (validacion == 2) {
				swal("Error", 'Las series deben de ser numérico', "error");
				return;
			}
			this.eventoCargaMostrar();
			var request = {
				codreq : this.datachecked.num_requerimiento,
				indorigreq : this.datachecked.ind_origen_requerimiento,
				codmat : (componente == "deco") ? trama.codigo_material_new_deco : trama.codigo_material_new_tarjeta,
				numser : (componente == "deco") ? trama.numero_serie_new_deco : trama.numero_serie_new_tarjeta,
				componente_id: this.datachecked.id,
				id_solicitud_tecnica: this.datachecked.id_solicitud_tecnica,
				solicitud_tecnica_id: this.datachecked.solicitud_tecnica_id,
				usuario: this.usuario,
				numero_serie_antigua : trama.numero_serie_antigua,
			};
			this.$http.post('revertireposicion', request, function(datos){
				if (datos.rst == 1) {
					swal("Exito", datos.msj, "success");
				} else {
					swal("Error", datos.msj, "error");
				}
				this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
				this.eventoCargaRemover();
			}).error(function(errors) {
                swal("Error", 'Ocurrió un error durante el proceso.', "error");
                this.eventoCargaRemover();
            });
		},

		eventoCargaMostrar: function () {
			var loading = document.getElementsByClassName("loading-img");
	        if ( loading.length == 0 ) {
	        	$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
	        }
	    },

	    eventoCargaRemover: function () {
        	$(".overlay,.loading-img").remove();
	    },

	    validarInputs: function (deco, tarjeta, operation) {
	    	switch(operation) {
	    		case 0: case 1: case 6:
	    			var numser = deco.numero_serie;
			    	var numtar = tarjeta.numero_serie;
					var numserR = /^\d+$/.test(numser);
					var numtarR = /^\d+$/.test(numtar);
					if (numser == '' || numtar == '') {
						return 1;
					} else if (!numserR || !numtarR) {
						return 2;
					} else {
						return 3;
					}
					break;
				case 2: case 3: case 4:
					var codigo_material_new = '';
			    	var numero_serie_new = '';
			    	var codmatpar = '';
			    	var numserpar = '';

			    	// if (operation == 2 || operation == 4) {
			    	// 	if (this.codtipoaveria == "" || this.codproblemaaveria == "") {
			    	// 		return 0;
			    	// 	}
			    	// }

			    	if (deco == '') {
			    		codigo_material_new = tarjeta.codigo_material_new_tarjeta;
				    	numero_serie_new = tarjeta.numero_serie_new_tarjeta;
				    	codmatpar = tarjeta.codmatpar_deco;
				    	numserpar = tarjeta.numserpar_deco;
			    	} else {
			    		codigo_material_new = deco.codigo_material_new_deco;
				    	numero_serie_new = deco.numero_serie_new_deco;
				    	codmatpar = deco.codmatpar_tarjeta;
				    	numserpar = deco.numserpar_tarjeta;
			    	}

			    	var R1 = /^\d+$/.test(codigo_material_new);
					var R2 = /^\d+$/.test(numero_serie_new);
					var R3 = /^\d+$/.test(codmatpar);
					var R4 = /^\d+$/.test(numserpar);
					if (codigo_material_new == '' || 
						numero_serie_new == ''|| 
						codmatpar == ''|| numserpar == '') {
						return 1;
					} else if (!R1 || !R2 || !R3 || !R4) {
						return 2;
					} else {
						return 3;
					}
					break;
				case 5:
					var codigo_material_new = '';
			    	var numero_serie_new = '';

			    	if (deco == '') {
			    		codigo_material_new = tarjeta.codigo_material_new_tarjeta;
				    	numero_serie_new = tarjeta.numero_serie_new_tarjeta;
			    	} else {
			    		codigo_material_new = deco.codigo_material_new_deco;
				    	numero_serie_new = deco.numero_serie_new_deco;
			    	}
					var R1 = /^\d+$/.test(codigo_material_new);
					var R2 = /^\d+$/.test(numero_serie_new);
					if (codigo_material_new == '' || 
						numero_serie_new == '') {
						return 1;
					} else if (!R1 || !R2) {
						return 2;
					} else {
						return 3;
					}
					break;
	    	}
	    },

	    typeOperation: function (type) {
	    	var nameOperation = '';
	    	switch(type) {
	    		case 0:
	    			nameOperation = 'activar';
	    			break;
	    		case 1:
	    			nameOperation = 'desasignar';
	    			break;
	    		case 2:
	    			nameOperation = 'cambioaveria';
	    			break;
	    		case 3:
	    			nameOperation = 'revertiraveria';
	    			break;
	    		case 4:
	    			nameOperation = 'reposicion';
	    			break;
	    		case 5:
	    			nameOperation = 'revertirreposicion';
	    			break;
	    		case 6:
	    			nameOperation = 'refresh';
	    			break;
	    	}
	    	return nameOperation;
	    },

	    TarjetasAlmacen: function () {
	    	this.$http.get('tarjetas', function(datos){
				this.$set('tarjetasAlmacen', datos);
			});
	    },

		limpiarCheck: function(inputName) {
			var elemento = document.getElementsByName(inputName);
	    	for (var i=0; i<elemento.length; i++) {
				elemento[i].checked = false;
	    	}
			return;
		}

	  //   TipoAveria: function () {
	  //   	this.$http.get('tipoaveria', function(datos){
			// 	this.$set('tipoaverias', datos);
			// });
	  //   },

	  //   ProblemaAveria: function () {
	  //   	this.$http.get('problemaaveria', function(datos){
			// 	this.$set('problemaaverias', datos);
			// });
	  //   }
	},

	ready: function() {
		this.ListarComponentes(this.codactu, this.usuario, this.tipost, this.tipolegado);
		this.TarjetasAlmacen();
		// this.TipoAveria();
		// this.ProblemaAveria();
	}
});