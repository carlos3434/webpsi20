$(document).ready(function () {
   $("#btnbusqueda").click(BusquedaMaterial);
    Materiales.CargaListado();
    
    slctGlobal.listarSlct('tecnico','slct_tecnico_id','simple',null,null,1);
    slctGlobal.listarSlct('material','slct_material_id','simple',null,null,1);

    slctGlobal.listarSlct('tecnico','slct_tecnico','multiple',null,null,1);
    slctGlobal.listarSlct('material','slct_material','multiple',null,null,1);
    slctGlobalHtml('slct_tipmat','multiple',null,1,'#slct_material','T');

    var enforceModalFocusFn;
        $('#materialModal').on('show.bs.modal', function (event) {
            enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
            var button = $(event.relatedTarget); 
            var titulo = button.data('titulo'); 
            var modal = $(this); 

            modal.find('.modal-title').text(titulo + ' Material');
            slctGlobalHtml('slct_tipo_material','simple',null,1,'#slct_material_id','T');
            if (titulo == 'Nuevo') {
                modal.find('.modal-body .btn-success').text('Guardar Material');
                modal.find('.modal-body .btn-success').attr('onClick', 'GuardarMaterial();');
                $("#form_cargamasiva").css("display","");
            } else {
                modal.find('.modal-body .btn-success').text('Actualizar Material');
                modal.find('.modal-body .btn-success').attr('onClick', 'editarMaterial();');
                $("#form_cargamasiva").css("display","none");

                material_id = button.data('id');
                $('#form_materiales #txt_serie').val(materialObj[material_id].serie);
                $('#form_materiales #txt_cantidad').val(materialObj[material_id].cantidad);
                $('#form_materiales #slct_tipo_material').multiselect('select',
                                        materialObj[material_id].material.tipmat);
                $('#form_materiales #slct_material_id').multiselect('select',
                                        materialObj[material_id].material.id);
                $('#form_materiales #slct_tecnico_id').multiselect('select',
                                        materialObj[material_id].tecnico_id);

                $('#slct_tipo_material,#slct_material_id,#slct_tecnico_id').multiselect('rebuild');
               

                $("#form_materiales").append("<input type='hidden' id='id' value='" + materialObj[material_id].id + "' name='id'>");
            }
        });

        $('#materialModal').on('hide.bs.modal', function (event) {
            $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
            var modal = $(this); //captura el modal
            modal.find('.modal-body input').val('');
            material_id='' ;
            $("#form_materiales input[type='hidden']").remove();
            $('#slct_material_id,#slct_tecnico_id').multiselect('select','');
            $('#slct_material_id,#slct_tecnico_id').multiselect('rebuild');
            $('#slct_tipo_material option:selected').removeAttr("selected");
            $('#slct_tipo_material').multiselect('destroy');         
        });

});

function BusquedaMaterial() {
    Materiales.CargaListado();
}

function CargarArchivos() {
    if ($("#txt_file_plan").val()=='') {
        swal("Error", "Cargue archivo", "error");
        return false;
    }
    
    Materiales.CargarArchivo();
}

function GuardarMaterial() {

    Materiales.GuardarMaterial();
}

function editarMaterial() {
    Materiales.editarMaterial();
}

function HTMLCargarListado(datos){
    var html="";
    $('#t_listado').dataTable().fnDestroy();

    $.each(datos,function(index,data){
        var estado='<span id="'+data.id+'" onClick="activar('+data.id+')" class="btn btn-danger">Inactivo</span>';
        if(data.estado==1){
            estado='<span id="'+data.id+'" onClick="desactivar('+data.id+')" class="btn btn-success">Activo</span>';
        }

        var estadohtml='<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#materialModal" data-id="' + index + '" data-titulo="Editar"><i class="fa fa-edit fa-lg"></i> </a>';

         html+="<tr>"+
            "<td>"+data.id+"</td>"+
            "<td>"+data.tecnico.carnet_tmp+"</td>"+
            "<td>"+data.material.codmat+"</td>"+
            "<td>"+data.material.deslarmat+"</td>"+
            "<td>"+data.material.tipmat+"</td>"+
            "<td>"+data.cantidad+"</td>"+
            "<td>"+data.created_at+"</td>"+
            "<td>"+estado+"</td>"+
            "<td>"+estadohtml+"</td>";

        html+="</tr>";
    });
    $("#tb_listado").html(html);

    $("#t_listado").dataTable();
}

activar=function(id){
    Materiales.CambiarEstado(id,1);
}
desactivar=function(id){
    Materiales.CambiarEstado(id,0);
}