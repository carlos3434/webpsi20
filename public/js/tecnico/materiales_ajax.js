
var material_id, materialObj;
var Materiales={

    CargarArchivo:function(){
        var inputFile = document.getElementById("txt_file_plan");
        var file = inputFile.files[0];
        var data = new FormData();
        data.append('archivo', file);    
        var dataToDownload = "data:application/octet-stream,"; 
    
        $.ajax({
            url: "tecnicomaterial/cargaplana",
            type: "POST",
            cache: false,
            data: data,
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            error: function(data) {
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            },
            success: function(obj) {
                $(".overlay,.loading-img").remove();  
                
                if(obj.rst==1) {
                    //direccionesobj= obj.csv;
                    //HTMLCargarDirecciones();
                    swal("Éxito", obj.msj, "success");
                    Materiales.CargaListado();
                    $("#btn_close").click();
                } else {
                    swal("warning", obj.msj, "warning");
                }
            }
        });
    },

    GuardarMaterial:function(opcion=0){
        var datos=$("#form_materiales").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        datos+="&opcion="+opcion;
        $.ajax({
            url         : "tecnicomaterial/guardarmaterial",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    swal("Éxito", obj.msj, "success");
                    $("Input[type=text]").val("");
                    Materiales.CargaListado(opcion);
                    $("#btn_close").click();
                } else {
                    var msj;
                    if(typeof obj.msj=='string') {
                        msj=obj.msj;
                    } else {
                        $.each(obj.msj,function(index,datos){
                            msj=datos;
                        });
                        msj=msj.join();
                    }
                    Psi.mensaje('danger', msj, 6000);
                }
            },
            error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    editarMaterial:function(opcion=0){
        var datos=$("#form_materiales").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        datos+="&opcion="+opcion;
        $.ajax({
            url         : "tecnicomaterial/editarmaterial",
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    swal("Éxito", obj.msj, "success");
                    $("Input[type=text]").val("");
                    Materiales.CargaListado(opcion);
                    $("#btn_close").click();
                } else {
                    var msj;
                    if(typeof obj.msj=='string') {
                        msj=obj.msj;
                    } else {
                        $.each(obj.msj,function(index,datos){
                            msj=datos;
                        });
                        msj=msj.join();
                    }
                    Psi.mensaje('danger', msj, 6000);
                }
            },
            error: function(){
                 $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    CargaListado:function(opcion=0){
        var datos=$("#form_busqueda").serialize().split("txt_").join("").split("slct_").join("").split("chk_").join("").split("_modal").join("");
        datos+='&opcion='+opcion;
        $.ajax({
            url         : 'tecnicomaterial/listar',
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : datos,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                if(obj.rst==1){
                    HTMLCargarListado(obj.datos);
                    materialObj = obj.datos;
                }
                $(".overlay,.loading-img").remove();
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
            }
        });
    },
    CambiarEstado:function(id,estado,opcion=0){
            $.ajax({
                url         : 'tecnicomaterial/editarmaterial',
                type        : 'POST',
                cache       : false,
                dataType    : 'json',
                data        : {estado:estado, id:id, opcion:opcion}, 
                beforeSend : function() {
                    $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
                },
                success : function(obj) {
                    if(obj.rst==1){
                        Psi.mensaje('success', obj.msj, 6000);
                        Materiales.CargaListado(opcion);
                    }
                    $(".overlay,.loading-img").remove();
                },
                error: function(){
                    $(".overlay,.loading-img").remove();
                    Psi.mensaje('danger', 'Ocurrio una interrupción en el proceso,Favor de intentar nuevamente.', 6000);
                }
            });
        },
}   