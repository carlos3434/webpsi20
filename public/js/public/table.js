var instance = axios.create({
	baseURL: '/cierretest'
});

var vm = new Vue({
	el: '#app',
	data: {
		datos: [],
		heads: [
			{
				filterHead: 'nombre',
				nameHead: 'Nombre',
			}, {
				filterHead: 'ape_pat',
				nameHead: 'Ape. Paterno',
			}, {
				filterHead: 'ape_mat',
				nameHead: 'Ape. Materno',
			}, {
				filterHead: 'telecl_1',
				nameHead: 'Telefono',
			},
		],
		currentPage: 1,
		perPage: 10,
		filter: null,
		totalRows: 0,
		filterSelected: 'nombre',
		action: 'create',
		object: {},
	tabIndex: null,
    tabs: [],
    tabCounter: 0
	},

	mounted: function() {
		this.listar();
		this.$root.$on('hidden::modal', function (e) {
			(e == 'modal1') ? this.cleanForm() : '';
        });
	},

	methods: {
	  	listar: function () {
	  		this.eventoCargaMostrar();
	        var start = 0;
	        if (this.currentPage != undefined && this.currentPage != 1) {
	          start = (this.currentPage - 1) * this.perPage;
	        }
	        
	        var request = {
	        	currentPage : start,
	        	perPage : this.perPage,
	            filtro : this.filter,
	            filterSelected : this.filterSelected
	        };

	        instance.post("/listar", request)
	        	.then((response) => {
	        		this.datos = [];
	        		this.datos = response.data.datos["datos"];
	        		this.totalRows = response.data.datos["total"];
	        		this.eventoCargaRemover();
	        	})
	        	.catch(function (error) {
	        		this.eventoCargaRemover();
	        	})
	    },
	    
	    detalle: function (obj) {
	    	this.action = 'update';
	    	this.object = obj;
	    },

	    operation: function () {
	    	console.log(this.action);
	    	if (this.action == 'create') {
	    		this.create();
	    	} else {
	    		this.update();
	    	}
	    },

	    create: function () {
	    	this.eventoCargaMostrar();
	    	console.log("objeto creado");
	    	console.log(this.object);
	    	swal("Exito", "Registro se realizó de manera correcta", "success");
	    	this.eventoCargaRemover();
	    	// instance.post("/crear", this.object)
	    	// 	.then((response) => {
	    	// 		this.eventoCargaRemover();
	    	// 	})
	    	// 	.catch(function (error) {
	    	// 		this.eventoCargaRemover();
	    	// 	})
	    },

	    update: function () {
	    	this.eventoCargaMostrar();
	    	console.log("objeto editado");
	    	console.log(this.object);
	    	swal("Error", "Falla al editar registro", "error");
	    	this.eventoCargaRemover();
	    	// instance.post("/editar", this.object)
	    	// 	.then((response) => {
	    	// 		this.eventoCargaRemover();
	    	// 	})
	    	// 	.catch(function (error) {
	    	// 		this.eventoCargaRemover();
	    	// 	})
	    },

	    cleanForm: function () {
    		this.object = {};
    		this.action = 'create';
	    },

		eventoCargaMostrar: function () {
			var loading = document.getElementsByClassName("loading-img");
	        if ( loading.length == 0 ) {
	        	$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
	        }
	    },

	    eventoCargaRemover: function () {
        	$(".overlay,.loading-img").remove();
	    }
	}
});