var instance = axios.create({
	baseURL: '/assia'
});

var vm = new Vue({
	el: '#app',
	data: {
		telefono: '',
		tiempo: '',
		respuesta: ''
	},

	mounted: function() {
		
	},

	methods: {
		diagnostico: function () {
			this.eventoCargaMostrar();
			var request = {
				telefono: this.telefono,
				tiempo: this.tiempo
			}
			instance.post("/assia", request)
				.then((response) => {
					this.respuesta = response.data.msj;
					this.eventoCargaRemover();
				})
				.catch(function (error) {
					this.eventoCargaRemover();
				})
		},

		eventoCargaMostrar: function () {
			var loading = document.getElementsByClassName("loading-img");
	        if ( loading.length == 0 ) {
	        	$("body").append('<div class="overlay"></div><div class="loading-img"></div>');
	        }
	    },

	    eventoCargaRemover: function () {
        	$(".overlay,.loading-img").remove();
	    }
	}
});