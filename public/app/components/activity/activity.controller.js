(function() {
    'use strict';

    angular
        .module('psi')
        .controller('ActivityController', ActivityController);

    //ActivityService.$inject = [];

    /* @ngInject */
    function ActivityController($http,$state,ActivityService){
        var activity = this;        
        activity.getActivity = getActivity;
        
        function getActivity() {
            ActivityService.getActivity(activity.id)
                .then(showActivity)
                .catch(error);
            function showActivity (data){
                if(data.rst == 1){
                    activity.id = data.id;
                    activity.nombre = data.nombre;
                }
            }
            function error (error){
                console.log(error);
            }
        }
    }
})();
