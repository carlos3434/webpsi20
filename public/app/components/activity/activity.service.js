(function() {
    'use strict';

    angular
        .module('psi')
        .factory('ActivityService', ActivityService);

    /* ActivityService.$inject = [''];

     /* @ngInject */
    function ActivityService($http,$state){
        var service = {
            getActivity : getActivity
        };

        return service;

        function getActivity(idActivity) {
            return $http.get("ofscangular/"+idActivity)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        }
    }
})();
