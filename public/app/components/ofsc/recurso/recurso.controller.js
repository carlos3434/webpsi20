(function () {
    'use strict';

    angular
        .module('psi')
        .controller('RecursoController', RecursoController);

    //RecursoController.$inject = [];

    /* @ngInject */
    function RecursoController(RecursoService) {
        var recurso = this;
        recurso.eliminar = eliminar;
        recurso.registrar = registrar;
        init();

        function init() {
            RecursoService.getResources()
                .then(success);

            function success(response) {
                recurso.resource = response;
            }
        }

        function eliminar(recurso) {
            console.log(recurso);
        }

        function registrar(recurso) {
            console.log(recurso);
        }
    }


})();