(function () {
    'use strict';

    angular
        .module('psi')
        .factory('RecursoService', RecursoService);

    /* RecursoService.$inject = [''];

     /* @ngInject */
    function RecursoService($http, $state) {
        var service ={
            getResources: getResources
        };

        return service;

        function getResources() {
            return $http.get('recursoofsc/arbol')
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;
            };

            function failedRequest(error) {
                return error;
            };
        }
    }
})();