(function () {
    'use strict';

    angular
        .module('psi')
        .factory('UsuarioService', UsuarioService);

    /* UsuarioService.$inject = [''];

     /* @ngInject */
    function UsuarioService($http, $state) {
        var service = {
            getUsers: getUsers,
            buscarUsuario: buscarUsuario,
            getResources: getResources,
            modificar: modificar,
            registrar: registrar
        };

        return service;

        function getUsers(startPage) {
            var start = 0;
            if (startPage != undefined && startPage != 1) {
                start = (startPage - 1) * 10;
            }
            return $http.get("usuarioofsc/usuarios/" + start)
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;

            };

            function failedRequest(error) {
                return error;
            };
        };

        function buscarUsuario(id) {
            return $http.get("usuarioofsc/usuario/" + id)
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;

            };

            function failedRequest(error) {
                return error;
            };
        };

        function getResources() {
            return $http.get("recursoofsc/comborecursos")
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;

            };

            function failedRequest(error) {
                return error;
            };
        };

        function modificar(datos) {
            return $http.post('usuarioofsc/modificar', datos)
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;

            };

            function failedRequest(error) {
                return error;
            };
        };

        function registrar(datos) {
            return $http.post('usuarioofsc/registrar',{data:datos})
                .then(successRequest)
                .catch(failedRequest);

            function successRequest(response) {
                return response.data;

            };

            function failedRequest(error) {
                return error;
            };
        }
    }
})();
