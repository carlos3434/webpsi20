(function () {
    'use strict';

    angular
        .module('psi')
        .controller('UsuarioController', UsuarioController);

    //UsuarioController.$inject = [];

    /* @ngInject */
    function UsuarioController(UsuarioService) {
        var usuario = this;

        usuario.listar = listar;
        usuario.sortType = 'paso'; // set the default sort type
        usuario.sortReverse = false;  // set the default sort order
        usuario.accionModal = accionModal;
        usuario.getUser = getUser;
        usuario.cambiarEstadousuario = cambiarEstadousuario;
        usuario.actualizarUsuario = actualizarUsuario;
        usuario.crearUsuario = crearUsuario;
        usuario.cboEstado = [
            {value: 'inactive', texto: "Inactivo"},
            {value: 'active', texto: "Activo"}
        ];

        usuario.resources = [
            {value: 'GR_CALATEL_M1', texto: "GR_CALATEL_MOVISTAR1"},
            {value: 'GR_COBRA_MOVISTAR1', texto: "GR_COBRA_MOVISTAR1"},
            {value: 'BK_DOMINION_INS_MOVISTAR1', texto: "Dominion_Movistar1"},
            {value: 'LARI_INS_MOVISTAR_UNO', texto: "LARI_MOVISTAR_UNO"},
            {value: 'BK_CALATEL_INS_DIGITALIZACION', texto: "Calatel_Exclusivas"},
            {value: 'BK_COBRA_INS_DIGITALIZACION', texto: "Cobra_Exclusivas"},
            {value: 'BK_DOMINION_INS_DIGITALIZACION', texto: "Dominion_Exclusivas"},
            {value: 'BK_LARI_INS_DIGITALIZACION', texto: "Lari_Exclusivas"}
        ];

        function listar() {
            UsuarioService.getResources()
                .then(successResource);

            UsuarioService.getUsers(usuario.bigCurrentPage)
                .then(success);

            function success(data) {
                if (data.rst == 1) {
                    usuario.bigTotalItems = data.total;
                    usuario.datos = data.usuarios;
                    usuario.tableFiltro = '';
                    usuario.tableMaxSize = 5;
                }
            };

            function successResource(response) {
                usuario.cboResources = response;
            }
        };

        function accionModal(accion, id) {
            if (accion == 0) {
                // crear
                usuario.nuevo = accion;
                usuario.id = '';
                usuario.nombre = '';
                usuario.es_ec = 1;
                usuario.estado = 1;
                usuario.btnAccion = 0;
                usuario.inp = true;
            } else {
                // actualizar
                usuario.nuevo = accion;
                usuario.btnAccion = 1;
                usuario.getUser(id);
                usuario.inp = false;
            }
        };

        function getUser(id) {

            UsuarioService.buscarUsuario(id)
                .then(showUsuario);

            function showUsuario(data) {
                if (data.rst == 1) {
                    usuario.id = data.datos.login;
                    usuario.nombre = data.datos.name;
                    usuario.estado = data.datos.status;
                    $("#usuarioModal").modal('show');
                }
            }
        };

        function cambiarEstadousuario(estado, id) {
            var datos = {
                'datos': {
                    'status': estado
                },
                'id': id
            };

            UsuarioService.modificar(datos)
                .then(success);

            function success(response) {
                if (response.rst == 1) {
                    Psi.mensaje('success', response.msj, 6000);
                    usuario.listar();
                } else {
                    Psi.mensaje('error', response.msj, 6000);
                }
            };
        };

        function actualizarUsuario() {
            var datos = {
                'datos': {
                    'name': usuario.nombre,
                    'status': usuario.estado,
                    'password': usuario.password
                },
                'id': usuario.id
            };

            UsuarioService.modificar(datos)
                .then(success);

            function success(response) {
                if (response.rst == 1) {
                    Psi.mensaje('success', response.msj, 6000);
                    usuario.listar();
                } else {
                    Psi.mensaje('error', response.msj, 6000);
                }
            }
        };

        function crearUsuario() {
            var data = {
                'login' : usuario.login,
                'name' : usuario.nombre,
                'status' : usuario.status,
                'resources' : [usuario.resource],
                'password' : usuario.password
            };

            UsuarioService.registrar(data)
                .then(success);

            function success(response) {
                if (response.rst == 1) {
                    Psi.mensaje('success', response.msj, 6000);
                    usuario.listar();
                } else {
                    Psi.mensaje('error', response.msj, 6000);
                }
            }
        };
    }
})();
