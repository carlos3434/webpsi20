(function () {
    'use strict';

    angular
        .module('psi')
        .factory('OfficetrackService', OfficetrackService);

    /* ActivityService.$inject = [''];

     /* @ngInject */
    function OfficetrackService($http, $state) {
        var service = {
            getTasks: getTasks,
            getErrors: getErrors
        };

        return service;

        function getTasks(startPage) {
            var start = 0;
            if (startPage != undefined && startPage != 1) {
                start = (startPage - 1) * 10;
            }
            return $http.get("officetrack/tasks/"+start)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        }

        function getErrors(startPage) {
            var start = 0;
            if (startPage != undefined && startPage != 1) {
                start = (startPage - 1) * 10;
            }
            return $http.get("officetrack/errors/"+start)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        }
    }
})();
