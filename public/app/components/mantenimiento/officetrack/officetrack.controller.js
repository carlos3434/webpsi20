(function () {
    'use strict';

    angular
        .module('psi')
        .controller('OfficetrackController', OfficetrackController);

    //OfficetrackController.$inject = [];

    /* @ngInject */
    function OfficetrackController(OfficetrackService) {
        var office = this;
        office.sortType     = 'paso'; // set the default sort type
        office.sortReverse  = false;  // set the default sort order
        office.searchFish   = '';
        office.sortTypeError     = 'paso'; // set the default sort type
        office.sortReverseError  = false;  // set the default sort order
        office.searchFishError   = '';
        office.getTareas = getTareas;
        office.getErrores = getErrores;

        function getTareas() {
            OfficetrackService.getTasks(office.bigCurrentPage)
                .then(showTasks);
            function showTasks(data) {
                if (data.rst == 1) {
                    office.bigTotalItems = data.datos["total"];
                    office.tasks = data.datos["tareas"];
                    office.tableFiltro   = '';
                    office.tableMaxSize = 5;
                }
            }
        };

        function getErrores(){
            OfficetrackService.getErrors(office.bigCurrentPageError)
                .then(showErrors);
            function showErrors(data) {
                if (data.rst == 1) {
                    office.bigTotalItemsError = data.datos["total"];
                    office.errors = data.datos["errors"];
                    office.tableFiltroError   = '';
                    office.tableMaxSizeError = 5;
                }
            }
        };

    }
})();
