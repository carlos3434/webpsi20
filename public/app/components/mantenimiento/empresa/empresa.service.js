(function() {
    'use strict';

    angular
        .module('psi')
        .factory('EmpresaService', EmpresaService);
    
    function EmpresaService($http,$state) {
        var empresa = {
            getEmpresa : getEmpresa,
            crearEmpresa : crearEmpresa,
            buscarEmpresa : buscarEmpresa,
            actualizarEmpresa : actualizarEmpresa,
            cambiarEstadoEmpresa: cambiarEstadoEmpresa
        };

        return empresa;

        function getEmpresa(startPage, nombre) {
            var start = 0;
            if (startPage != undefined && startPage != 1) {
                start = (startPage - 1) * 10;
            }
            
            return $http({
                    method: "GET",
                    url: "empresa/demolistar",
                    params: {start: start, nombre: nombre},
                }).then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        };

        function crearEmpresa(datos) {
            return $http.post("empresa/crear", datos)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        };

        function buscarEmpresa(datos) {
            return $http.post("empresa/buscar", datos)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        };

        function actualizarEmpresa(datos) {
            return $http.post("empresa/editar", datos)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        };

        function cambiarEstadoEmpresa(datos) {
            return $http.post("empresa/cambiarestado", datos)
                .then(requestComplete)
                .catch(requestFailed);

            function requestComplete(response) {
                return response.data;
            }

            function requestFailed(error) {
                return error;
            }
        };
    }
})();