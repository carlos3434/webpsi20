(function() {
    'use strict';
    angular
        .module('psi')
        .controller('EmpresaController', EmpresaController);

    function EmpresaController ($http,$state,EmpresaService,$parse,$timeout) {

        var empresa = this;

        // *************************** SELECT **************************
        empresa.cboEs_ec = [
            { value: 0, texto: "No" },
            { value: 1, texto: "Si" }
        ];

        empresa.cboEstado = [
            { value: 0, texto: "Inactivo" },
            { value: 1, texto: "Activo" }
        ];

        empresa.accionModal = function(accion, id) {
            if (accion == 0) {
                // crear
                empresa.id = '';
                empresa.nombre = '';
                empresa.es_ec = 1;
                empresa.estado = 1;
                empresa.btnAccion = 0;
            } else {
                // actualizar
                empresa.btnAccion = 1;
                empresa.buscarEmpresa(id);
            }
        };

        empresa.listarEmpresa = function() {
            eventoCargaMostrar();
            EmpresaService.getEmpresa(empresa.bigCurrentPage, empresa.tableFiltro)
                .then(showEmpresa);

            function showEmpresa(data) {
                if (data.rst == 1) {
                    empresa.bigTotalItems = data.datos["total"];
                    empresa.datos = data.datos["empresas"];
                    empresa.tableMaxSize = 5;
                    eventoCargaRemover();
                }
            }
        };

        empresa.crearEmpresa = function() {
            var datos = {nombre: empresa.nombre, descripcion: empresa.descripcion, es_ec: empresa.es_ec, estado: empresa.estado};
            EmpresaService.crearEmpresa(datos)
                .then(showEmpresa);

            function showEmpresa(data) {
                if (data.rst == 1) {
                    Psi.mensaje('success', data.msj, 6000);
                    $("#empresaModal").modal('hide');
                    empresa.listarEmpresa();
                } else {
                    angular.forEach(data.msj, function (datos, index) {
                        var error = 'error_'+index;
                        var varError = $parse(error);
                        varError.assign(empresa, datos);

                        $timeout(function () {
                            varError.assign(empresa, '');
                        }, 3000);
                    });
                }
            }
        };

        empresa.buscarEmpresa = function(id) {
            var datos = {id: id};
            EmpresaService.buscarEmpresa(datos)
                .then(showEmpresa);

            function showEmpresa(data) {
                if (data.rst == 1) {
                    empresa.id = data.datos["id"];
                    empresa.nombre = data.datos["nombre"];
                    empresa.es_ec = data.datos["es_ec"];
                    empresa.estado = data.datos["estado"];
                    $("#empresaModal").modal('show');
                }
            }
        };

        empresa.actualizarEmpresa = function() {
            var datos = {id: empresa.id, nombre: empresa.nombre, es_ec: empresa.es_ec, estado: empresa.estado};
            EmpresaService.actualizarEmpresa(datos)
                .then(showEmpresa);

            function showEmpresa(data) {
                if (data.rst == 1) {
                    Psi.mensaje('success', data.msj, 6000);
                    $("#empresaModal").modal('hide');
                    empresa.listarEmpresa();
                } else {
                    angular.forEach(data.msj, function (datos, index) {
                        var error = 'error_'+index;
                        var varError = $parse(error);
                        varError.assign(empresa, datos);
                        
                        $timeout(function () {
                            varError.assign(empresa, '');
                        }, 3000);
                    });
                }
            }
        };

        empresa.cambiarEstadoEmpresa = function(estado, id) {
            var datos = {estado: estado, id: id};
            EmpresaService.cambiarEstadoEmpresa(datos)
                .then(showEmpresa);

            function showEmpresa(data) {
                if (data.rst == 1) {
                    Psi.mensaje('success', data.msj, 6000);
                    empresa.listarEmpresa();
                }
            }
        };
    }
})();