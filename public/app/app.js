(function () {
    'use strict';

    angular
        .module('psi', [
            'ui.router',
            'oc.lazyLoad',
            'ui.bootstrap',
            'ngSanitize',
            'ngResource',
            'ngMessages',
            'ui.tree'
        ])
        .config(stateConfig)
        .run(stateRun);

    stateConfig.$inject = ['$urlRouterProvider', '$stateProvider', '$ocLazyLoadProvider'];
    stateRun.$inject = ['$state'];

    /* @ngInject */
    function stateConfig(url, router, $lazyLoad) {
        $lazyLoad.config({
            debug: true
        });

        router.state('home', {
            url: '/ofscangular',
            templateUrl: 'app/components/activity/home.view.html',
            controller: 'ActivityController',
            controllerAs: 'activity',
            resolve: {
                deps: ['$ocLazyLoad', function (lazy) {
                    return lazy.load([{
                        files: [
                            'app/components/activity/activity.controller.js',
                            'app/components/activity/activity.service.js'
                        ]
                    }]);
                }]
            }
        })
        .state('empresa', {
            url: '/empresa',
            templateUrl: 'app/components/mantenimiento/empresa/empresa.view.html',
            controller: 'EmpresaController',
            controllerAs: 'empresa',
            resolve: {
                deps: ['$ocLazyLoad', function (lazy) {
                    return lazy.load([{
                        files: [
                            'app/components/mantenimiento/empresa/empresa.controller.js',
                            'app/components/mantenimiento/empresa/empresa.service.js'
                        ]
                    }]);
                }]
            }
        })
        .state('officetrack', {
            url: '/officetrack',
            templateUrl: 'app/components/mantenimiento/officetrack/officetrack.view.html',
            controller: 'OfficetrackController',
            controllerAs: 'office',
            resolve: {
                deps: ['$ocLazyLoad', function (lazy) {
                    return lazy.load([{
                        files: [
                            'app/components/mantenimiento/officetrack/officetrack.controller.js',
                            'app/components/mantenimiento/officetrack/officetrack.service.js'
                        ]
                    }]);
                }]
            }
        })
        .state('usuario', {
            url: '/usuario',
            templateUrl: 'app/components/ofsc/usuario/usuario.view.html',
            controller: 'UsuarioController',
            controllerAs: 'usuario',
            resolve: {
                deps: ['$ocLazyLoad', function (lazy) {
                    return lazy.load([{
                        files: [
                            'app/components/ofsc/usuario/usuario.controller.js',
                            'app/components/ofsc/usuario/usuario.service.js'
                        ]
                    }]);
                }]
            }
        })
        .state('recurso', {
            url: '/recurso',
            templateUrl: 'app/components/ofsc/recurso/recurso.view.html',
            controller: 'RecursoController',
            controllerAs: 'recurso',
            resolve: {
                deps: ['$ocLazyLoad', function (lazy) {
                    return lazy.load([{
                        files: [
                            'app/components/ofsc/recurso/recurso.controller.js',
                            'app/components/ofsc/recurso/recurso.service.js'
                        ]
                    }]);
                }]
            }
        });
    };

    function stateRun(state) {

    };

})();