<?php
class Database
{
    private $_connection;
    private static $_instance; //The single instance
    private $_host = '10.226.44.223';
    private $_username = 'webpsi';
    private $_password = 'webpsi59u';
    private $_database = 'webpsi_officetrack';
    private $_port = '3306';

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        // If no instance then make one
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Constructor
    private function __construct()
    {
        $this->_connection = new mysqli(
            $this->_host,
            $this->_username,
            $this->_password,
            $this->_database
        );
        // Error handling
        if (mysqli_connect_error()) {
            trigger_error(
                "Failed to conencto to MySQL: " .
                mysql_connect_error(),
                E_USER_ERROR
            );
        }
    }

    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {

    }

    // Get mysqli connection
    public function getConnection()
    {
        return $this->_connection;
    }
}
/**
*
*/
class Procesos
{
    public $mysqli;
    function __construct()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $this->mysqli=$mysqli;
    }

    public function insertInto($id, $tipoImagen, $name)
    {
        $actuaciones=Toa::ValidaPermisoProceso();
    }

    public function update($tabla, $pasoId, $campo)
    {
        
    }

    public function clear()
    {

    }

    public function exec()
    {
        set_time_limit(0);

        $query = 'SELECT * FROM webpsi_officetrack.tareas
                  where date(fecha_recepcion) = subdate(curdate(),interval 1 day) ';
        $result = $this->mysqli->query($query);

    }
}

$newProces= new Procesos();
$newProces->exec();
$newProces->clear();
