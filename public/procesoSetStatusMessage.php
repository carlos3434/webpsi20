<?php
class Database
{
    private $_connection;
    private static $_instance; //The single instance
    /*private $_host = '10.226.44.223';
    private $_username = 'webpsi';
    private $_password = 'webpsi59u';
    private $_database = 'psi';
    private $_port = '3306';*/
    private $_host = '192.168.1.2';
    private $_username = 'jrojas3';
    private $_password = '123456';
    private $_database = 'psi';
    private $_port = '3306';

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        // If no instance then make one
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Constructor
    private function __construct()
    {
        $this->_connection = new mysqli(
            $this->_host,
            $this->_username,
            $this->_password,
            $this->_database
        );
        // Error handling
        if (mysqli_connect_error()) {
            trigger_error(
                "Failed to conencto to MySQL: " .
                mysql_connect_error(),
                E_USER_ERROR
            );
        }
    }

    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {

    }

    // Get mysqli connection
    public function getConnection()
    {
        return $this->_connection;
    }
}


/**
* 
*/
class Ofsc
{
    protected $_wsdl='https://telefonica-pe.test.toadirect.com/soap/outbound/?wsdl';
    protected $_client;
    public function iniciarCliente()
    {
        $opts = array('http' => array('protocol_version' => '1.0'));
        $context = stream_context_create($opts);
        /*
        $wsOptArray = array(
                        "proxy_host" => "10.226.159.191",
                        "proxy_port" => 8590,
                        "trace" => 1, 
                        "exception" => 0
                    );
        */
        $wsOptArray =array(
            "trace" => 1, 
            "exception" => 0
        );
        $wsOptArray["stream_context"] = $context;
        
        $soapClient = new \SoapClient(
            $this->_wsdl,
            $wsOptArray
        );
        return $soapClient;
    }
    private function getAuthString($now)
    {
        $authString = $now 
                      . md5('arg3ntina');
        return md5($authString);
    }
    protected function getAuthArray()
    {
        $now = date("c");
        $xmlArray = array(
            "user"=>array(
                "now" => $now,
                "login" => ('soap'),
                "company" => ('telefonica-pe.test'),
                "auth_string" => $this->getAuthString($now)
            )
        );
        
        return $xmlArray;
    }
    protected function doAction($action, $setArray)
    {
        $response = new \stdClass();

        $response->error = false;
        $requestArray = array_merge($this->getAuthArray(), $setArray);
        $result = $this->_client->__soapCall(
            $action, array("request"=>$requestArray)
        );
        return $result;
    }
}

class Outbound extends Ofsc
{
    public function __construct()
    {
        $this->_client = $this->iniciarCliente();

    }
    /**
     * este metodo se envia a TOAdirect para informar del estado de los mensajes
     * 
     */
    public function setMessageStatus($messages=array())
    {
        try {
            $setArray = array(
                "messages" => $messages
            );

            return $this->doAction('set_message_status', $setArray);
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }


}


/**
*
*/
class Procesos
{
    public $mysqli;
    function __construct()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $this->mysqli=$mysqli;
    }
    public function exec()
    {
        set_time_limit(0);

        $query = "SELECT id, message_id
                    FROM mensajes
                    WHERE estado='sending' 
                    AND TIMESTAMPDIFF(MINUTE, updated_at, NOW())>=3";
        $result = $this->mysqli->query($query);

        
        foreach ($result as $key => $value) {
            $messageId=$value['message_id'];
            $Outbound = new Outbound();
            $request=array('message'=>
                array(
                    'message_id'=>$messageId,
                    'status'=>'delivered'
                    )
                );
            $response=$Outbound->setMessageStatus($request);
            $body=serialize($response);

            $query ="INSERT INTO log_ofsc
                (process, type, body, created_at)
                values ($messageId, 'respuesta_set_message', '$body', now() )";

            $result = $this->mysqli->query($query);

            if (!$response->error) {
                //var_dump( $response->data->message_response->result->code );
                $code = $response->data->message_response->result->code;
                if ($code=='OK') {
                    $query ="UPDATE mensajes SET estado='delivered'
                            WHERE message_id=$messageId";

                    $result = $this->mysqli->query($query);
                } else {
                    print_r($response);
                }
            }
            
        }
        return true;
    }
}

$newProces= new Procesos();
$newProces->exec();
