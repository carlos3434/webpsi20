#!/usr/bin/perl
use DBI;

#my $dbh = DBI->connect('dbi:mysql:webpsi','root','jMuW8l')
#        or die "Connection Error: $DBI::errstr\n";
#$dbh->{AutoCommit} = 0;

my $total_argumentos = int(@ARGV);
my $celular = $ARGV[0];
my $texto = $ARGV[1];
my $id_user = '1';
my $estado = '2';


if ($total_argumentos<2)
{
        print "\n"."No se envio el numero adecuado de parametros.";
        exit;
}

use File::Copy;
use Frontier::Client;

my $client = Frontier::Client->new (
        url => 'https://gaudi:despacho22@10.10.128.60/x2m/rpc2',
        debug => 1
);


sub trim{
	my $str =shift;
        $str=~ s/^\s+//;
        $str=~ s/\s+$//;
        return $str;
};

my $log;

push @params,
{'celular' => Frontier::RPC2::String->new(trim($celular)),
 'texto'   => Frontier::RPC2::String->new(trim($texto)),
 'fecha'   => Frontier::RPC2::String->new('')
};

$log.="Celular: ".$celular."Mensaje:".$texto."\n";

my $res = $client->call('Tiaxa.SendMsg',[@params]);

($seg, $min, $hora, $dia, $mes, $anho, @zape) = localtime(time);
$mes++;
$anho+=1900;
$fecha_actual= "$anho-$mes-$dia $hora:$min:$seg";

my $status_envio = $res->[0];
my $mensaje_enviado = $res->[1];

#my $sql_ins = 'INSERT INTO mensajes_enviados_libre(fecha_mov,id_user,mensaje,celular,estado,respuesta_moviles) VALUES (?,?,?,?,?,?)';
#my $res_ins = $dbh->prepare_cached($sql_ins);

#die "No se puedo realizar el PREPARE del query. Terminando." unless defined $res_ins;

my $success = -1;
#$success &&= $res_ins->execute($fecha_actual,$id_user,$texto, $celular, $estado,'Predeterminado') ;


#my $result = ($success ? $dbh->commit : $dbh->rollback);
#unless ($result) {
#        die "No se pudo realizar transaccion por error: " . $dbh->errstr
#}

print "\n".$fecha_actual." Respuesta: ".$res->[0]."-".$res->[1]."\nDatos Enviados:\n".$log;
