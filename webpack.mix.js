let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
		'resources/assets/js/slct_global.js',
		'resources/assets/js/bandeja.js',
		'resources/assets/js/bandeja_ajax.js',
		'resources/assets/js/app.js',
		], 'public/js/app.js');