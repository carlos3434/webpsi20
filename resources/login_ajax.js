Login={
    IniciarLogin:function(){
        var datos=$("#logForm").serialize();
        var url_login=$("#logForm").attr("action");
        var trama = datos;
        $.ajax({
            url         : url_login,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : trama,
            beforeSend : function() {
                $(".load").show();
            },
            success : function(obj) {
                $(".load").hide();
                
                if(obj.rst==1 && obj.estado==1){
                    window.location='admin.inicio';
                }
                else if(obj.rst==1){
                    MostrarMensaje(obj.msj);
                }
                else if(obj.rst==2){
                    MostrarMensaje(obj.msj);
                }
                else if(obj.rst == 5){
                    $("#usuario, #password, #btnIniciar").prop("disabled", true);
                    
                    $("#msj2").css("display", "block");
                    $("#divPassword").css("display", "block");
                    $("#divNewPassword1").css("display", "block");
                    $("#divNewPassword2").css("display", "block");
                    $("#divAccion").css("display", "block");
                }
            },
            error: function(){
                $(".load").hide();
            }
        });
    },
    
    MisDatos:function(){
        var datos=$("#form_misdatos").serialize().split("txt_").join("").split("slct_").join("");
        var usuario = $("#usuario").val();
        var password = $("#password").val();
        var newpassword = $("#txt_newpassword").val();
        var confirm_new_password = $("#txt_confirm_new_password").val();
        
        var accion="check/misdatosvalida";

        $.ajax({
            url         : accion,
            type        : 'POST',
            cache       : false,
            dataType    : 'json',
            data        : "usuario="+usuario+"&password="+password+"&newpassword="+newpassword+"&confirm_new_password="+confirm_new_password,
            beforeSend : function() {
                $("body").append('<div class="overlay"></div><div class="loading-img"></div>');
            },
            success : function(obj) {
                $(".overlay,.loading-img").remove();
                if(obj.rst==1){
                    
                    $("#txt_password,#txt_newpassword,#txt_confirm_new_password,#password").val('');
                    $("#msj2").css("display", "none");
                    $("#divPassword").css("display", "none");
                    $("#divNewPassword1").css("display", "none");
                    $("#divNewPassword2").css("display", "none");
                    $("#divAccion").css("display", "none");
                    
                    $("#usuario, #password, #btnIniciar").prop("disabled", false);
                    
                    MostrarMensaje("Actualización Exitosa");
                }
                else if(obj.rst==3){
                    $("#error_newpassword").text("value", obj.msj);
                    $("#error_newpassword").fadeOut(10000);
                    $("#txt_password,#txt_newpassword,#txt_confirm_new_password").val('');
                }
                else{
                    $.each(obj.msj,function(index,datos){
                        $("#error_"+index).attr("data-original-title",datos);
                        $('#error_'+index).css('display',''); 
                        $("#txt_"+index+",#slct_"+index).focus();
                        $("#error_newpassword").text(datos);
                        $("#error_newpassword").fadeOut(10000);
                    });
                }
            },
            error: function(){
                $(".overlay,.loading-img").remove();
                Psi.mensaje('danger', '<?php echo trans("greetings.mensaje_error"); ?>', 6000);
            }
        });
    },
    
    Candelar:function(){
         window.location='login';
    }
}