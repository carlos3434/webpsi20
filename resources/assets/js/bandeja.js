
$(document).ready(function() {
    Slcts.slctGlobalHtml('slct_estado_st','multiple');
    Slcts.slctGlobalHtml('slct_tipo_envio_ofsc','multiple');
    Slcts.slctGlobalHtml('slct_tipo_legado','multiple');
    Slcts.slctGlobalHtml('slct_actividad','multiple');
    Slcts.slctGlobalHtml('slct_tipo','simple');

    $('#txt_created_at, #txt_fecha_agenda').daterangepicker({
        singleDatePicker: false,
        maxViewMode: 2,
        showDropdowns: true,
        minDate: moment().subtract(30, 'days'),
        maxDate:  moment(),
        startDate: moment().subtract(15, 'days'),
        endDate: moment(),
        format: 'YYYY-MM-DD'
    });

    $("#slct_tipo").change( function(e) {
        idtipo = $(this).val();
        $("#txt_valor").val("");
        $("#txt_valor").focus();
    });

    $("#btn-buscar").click(BuscarSolicitud);
    $('#BandejaModal').on('show.bs.modal', function (event) {
        enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        var button = $(event.relatedTarget);
        
        let request = {
            tipo_legado             : button.data('tipolegado'),
            solicitud_tecnica_id    : button.data('solicitudtecnicaid'),
            actividad_id            : button.data('actividadid')
        };

        app.getSolicitud(request);
        Slcts.slctGlobalHtml('slct_agdsla','simple');
        Slcts.slctGlobalHtml('slct_tematico_id1_agenda','simple');
        //Legado.modalOpen(button);
    });
    $('#BandejaModal').on('hidden.bs.modal', function () {
        console.log('closed');
        $(".tabs-popup").removeClass('active');
        $(".tab-pane").removeClass('active in');
    });
    
});



BuscarSolicitud = function() {
	
    if ($("#collapse2").css("display") == "block") {
        valor_busqueda = $("#txt_valor").val();
        if (valor_busqueda == "") {
            alert("Ingrese un valor para la busqueda!!!");
            return;
        } else {
            tipo_busqueda = $("#slct_tipo").val();
            switch(tipo_busqueda){
                case "id_solicitud_tecnica":
                    regex = new RegExp(/^[0-9a-zA-Z]+$/);
                    if (valor_busqueda.length > 11) {
                        alert("Excedio los 11 caracteres para buscar por solicitud tecnica");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo caracteres alfanumericos");
                        return;
                    }
                    break;
                case "num_requerimiento":
                    regex = new RegExp(/^[0-9a-zA-Z]+$/);
                    if (valor_busqueda.length > 15) {
                        alert("Excedio los 15 caracteres para buscar por codactu");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo caracteres alfanumericos");
                        return;
                    }
                    break;

                case "orden_trabajo":
                    regex = new RegExp(/^([0-9])/);
                    if (valor_busqueda.length > 15) {
                        alert("Excedio los 15 caracteres para buscar por orden de trabajo");
                        return;
                    }
                    if (!regex.test(valor_busqueda)) {
                        alert("Solo numeros");
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
        $("#t_listados").dataTable().fnDestroy();
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='1' id='tipo_busqueda' />");
        Bandeja.PintarTabla();
        $("#tipo_busqueda").remove();
    } else if ($("#collapse1").css("display") == "block") {
        $("#t_listados").dataTable().fnDestroy();
        $("#form_buscar").append("<input type='hidden' name='tipo_busqueda' value='2' id='tipo_busqueda' />");
        Bandeja.PintarTabla();
        $("#tipo_busqueda").remove();
    }
};

