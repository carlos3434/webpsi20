/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


//require('./bootstrap');
//require('../../public/lib/bootstrap-multiselect/dist/js/bootstrap-multiselect.js');

//import moment from 'moment/moment.js';
//import 'moment/locale/es';
//moment.locale('es');
//import swal from 'sweetalert';
require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/ExampleComponent.vue'));
var datos= {parametros:'1', prueba:'test'};

var selects = [
 "zonal", "nodo", "operacion",'troba',
 'geotap', "estadoofsc", "contratascms","actividadtipo",
 "mdf", "motivosliquidacion", "estadoaseguramiento","quiebre",
 'actividadtipo', "quiebre", "contratasgestel",
 'solicitudes', 'motivosdevolucion', 'tematicos','submotivosdevolucion',
 'tiporeq','motivogen'];

var slcts = [
 "slct_zonal", "slct_cod_nodo", "slct_tipo_operacion", 'slct_cod_troba',
 'slct_cod_tap', 'slct_estado_ofsc', 'slct_contrata_modal_cms','slct_actividad_tipo_modal',
 'slct_cabecera_mdf', 'slct_motivosliquidacion', "slct_estado_aseguramiento","slct_quiebre_modal",
 'slct_actividad_tipo_id', "slct_quiebre", 'slct_contrata_modal_gestel','slct_submotivosdevolucion',
 'slct_tiporeq_cms','slct_motivogen_cms'
];

var tipo = [
 "multiple", "multiple", "multiple", "multiple",
 "multiple", "multiple", "multiple", "multiple",
 "multiple", "multiple", "multiple", "simple",
 'multiple', "multiple", "simple"
];

var valarray = [
 null, null, null, null,
 null, null, null, null,
 null, null, [], null,
 null, null, null
];
var data = [
 {}, {}, datos, {},
 {}, {}, {}, {},
 {}, {}, {}, {},
 {}, {}, {}, {},
 {}, {}, {}, {},
 {}
];
var afectado = [
 0, 0, 0, 0,
 0, 0, 0, 0,
 0, 0, 0, 0,
 0, 0
];
var afectados = [
 "", "", "", "",
 "", "", "", "",
 "", "", "", "",
 "", "", ""
];
var slct_id = [
 "", "", "", "",
 "", "", "", "",
 "", "", "", "",
 "", "", ""
];
var es_lista =[
 true, true, true, true,
 true, true, true, true,
 true, true, true, true,
 true, true, true,
 false, false, false,
];

Vue.component('toa-component', require('./components/ToaComponent/ToaComponent.vue'));
Vue.component('go-component', require('./components/GoComponent/GoComponent.vue'));
Vue.component('cms-component', require('./components/CmsComponent/CmsComponent.vue'));
Vue.component('movimiento-component', require('./components/MovimientoComponent/Movimiento.vue'));
Vue.component('regcambios-component', require('./components/RegcambiosComponent/Regcambios.vue'));
Vue.component('sms-component', require('./components/SmsComponent/Sms.vue'));
Vue.component('tramaenvio-component', require('./components/TramaenvioComponent/Tramaenvio.vue'));
Vue.component('comunicacionlegados-component', require('./components/ComunicacionlegadosComponent/Comunicacionlegados.vue'));
//Vue.component('cierre-component', require('./components/CierreComponent/CierreComponent.vue'));
//Vue.component('toa-cms', require('./components/CmsComponent/CmsComponent.vue'));
/*Vue.component('toa-component',{
			template:`
				<div class='col-lg-4'>
					<ul>
					    <li>{{ number }}</li>
					    <li>About</li>
					    <li>Service</li>
					    <li>Bitfumes</li>
					</ul>
					<a class='btn btn-info'>{{ number }}</a>
				</div>
			`,
			props:[
				'solicitud_data'
			]
		});*/

app = new Vue({

    el: '#app',

    data: {
    	loading_img: false,
    	value: 123,
    	solicitud: {
    		id_solicitud_tecnica: '',
    		full_name: '',
    		nom_cliente: '',
    		zonal: '',
    		tipo_requerimiento: '',

    	},
    	estado_gestionando: false,
    	btn_cierre_visibilidad: false,
    	btn_toa_visibilidad: false,
    	bth_go_visibilidad: false,
    	btn_gestel_visibilidad: false,
    	lista: {
    		nodo: [],
    		quiebre: [],
    		troba: [],
    		actividadtipo: [],
    		zonal: [],
    		tiporeq: [],
    		motivogen: [],
    		contratascms: []
    	},
    	movimientos_st: [],
    	active_cms: false,
    	logcambios_st: [],
    	logrecepcion_st: [],
    	comunicacionlego_st: {}

    },

    created: function () {
	    this.cargarSlcts(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, {}, {}, {}, es_lista);
	},

    methods: {

    	cargarSlcts: function(selects, slcts, tipo, valarray, data, afectado, afectados, slct_id, slctant, slctant_id, funciones, es_lista) {
	      	    //console.log(selects);
            let request = {
	            selects         : JSON.stringify(selects),
	            datos           : JSON.stringify(data)
	        };
	        this.loading_img = true;

	      	axios.post('listado_lego/selects', request).then((response) => {
				let obj = response.data;
				for(let i = 0; i < obj.data.length; i++){
	                obj.datos = obj.data[i];
	                if (es_lista[i] == true) {
	                    Slcts.htmlListarSlct(obj, slcts[i], tipo[i], valarray[i], afectado[i], afectados[i], slct_id[i]);
	                }
	                this.lista[ selects[i] ] = obj.data[i];
	            }
	            console.log(this.lista,'qwe');
				this.loading_img = false;
			}).catch((err) => {
				console.log(err);
			});
	      
	    },

	    getSolicitud: function(request) {
	    	this.showModal();
	    	axios.post('bandejalegado/datasolicitud', request).then( response => {
	    		console.log(tipoPersonaId, response.data);
                if (response.data !== "null" && response.data !== null) {
                	this.solicitud = response.data;
	                if ( tipoPersonaId !== 3 ) {//telefonica =>3
	                	this.validarGestion();

	                	/*if (this.solicitud.estado_st == 0){
	                		this.btn_toa_visibilidad == false;

	                	}
	                    Legado.validaGestionAjax(function () {
	                        if (Legado.gestionando) {
	                            return false;
	                        }
	                        Legado.modalOpenProcess();
	                    });*/
	                    
	                } else {
	                    if (this.solicitud.estado_st == 1) {
	                    	this.validarParametrizacion();
	                    }
	                }
	            }
            }).catch( e => {
          
            });
	    },

	    consultarCapacidad: function(){
    		console.log('mostrar');
	    },


	    showModal:function(){
	    	console.log('mostrar modal');
	    },

	    validarGestion: function(){
	    	var request = {solicitud: this.solicitud};
	    	axios.post('toa/validagestionst',request).then(response => {
	            let acceso = response.data.acceso;
	            console.log(acceso);
	            this.validarModalActive();
	            if (acceso.gestionando == "true" || acceso.gestionando == true) {
	            	console.log('solicitud siendo gestionada por otra persona');
	                this.estado_gestionando = true;
	                this.validarVisibilidadBtns();
	                
	                //Legado.gestionando = true;
	                //Psi.sweetAlertError("El usuario : "+acceso.nombreusuario+" ya esta gestionando dicha solicitud! ");
	                //PestanaCierre.hide();
	                //PestanaAgendaToa.desactivarVisibilidad();
	                //var tipo_legado = parseInt(vm.solicitud.tipo_legado);
	                //var tipo_actividad = parseInt(vm.solicitud.actividad_id);
	                //eventoCargaRemover();

	            } else {
	            	this.estado_gestionando = false;
	            	this.validarVisibilidadBtns();
	                //Legado.gestionando = false;
	            }
	            //f_end();
	        });log_cambios
	    },
	    validarParametrizacion: function() {
	    	axios.post('toa/validaparametrizacion',request).then(response => {
	            acceso = response.data.acceso;
	            if (acceso.parametrizacion == "false" || acceso.parametrizacion == false) {
	                //Psi.sweetAlertError("El requerimiento no cumple con los criterios de parametrizacion de envio a TOA!");
	                this.btn_toa_visibilidad = false;
	                //eventoCargaRemover();
	            }
	            //Legado.DetalleSolicitud(tabmodalactivo);  no es necesario
	        });
	    },

	    validarVisibilidadBtns: function() {
	    	if (this.estado_gestionando == true) {
	    		this.btn_cierre_visibilidad = false;
	    		this.btn_toa_visibilidad = false;


	    	} else {
	    		if (this.solicitud.estado_st == 0) {
	    			this.btn_toa_visibilidad = true;
	    		}
	    		this.btn_cierre_visibilidad = true;
	    	}
	    },

	    limpiarModal: function() {
	    	this.visibilidadBtnsDefault();
	    	this.active_cms = false;
	    	this.solicitud = {};
	    },


	    visibilidadBtnsDefault: function() {
	    	this.btn_cierre_visibilidad = false;
	    	this.btn_toa_visibilidad = false;
	    	this.estado_gestionando = false;
	    },
	    //detalle solicitud
	    obtenerMovimientos: function() {
	    	let request = {
	    		id: this.solicitud.solicitud_tecnica_id
	    	};
	    	axios.post('bandejalegado/movimientos',request).then(response => {
	        	this.movimientos_st = response.data.movimientos;
	        });	
	    },
	    obtenerLogcambios: function() {
	    	let request = {
	    		id: this.solicitud.solicitud_tecnica_id,
	    	};
	    	axios.post('bandejalegado/logcambios',request).then(response => {
	        	this.logcambios_st = response.data.logcambios;
	        });
	    },
	    validarModalActive: function() {
	    	if (this.solicitud.tipo_legado == 1) {
	    		this.active_cms = true;
	    	} 
	    },
	    obtenerLogrecepcion: function() {
	    	let request = {
	    		id: this.solicitud.solicitud_tecnica_id,
	    	};
	    	axios.post('bandejalegado/logrecepcion',request).then(response => {
	        	console.log(response.data);
	        	this.logrecepcion_st = response.data.logrecepcion;
	        });
	    },
	    obtenerComunicacionlego: function() {
	    	let request = {
	    		id: this.solicitud.solicitud_tecnica_id,
	    	};
	    	axios.post('bandejalegado/comunicacionlego',request).then(response => {
	        	
	        	this.comunicacionlego_st = response.data.comunicacionlego;

	        	console.log(this.comunicacionlego_st);
	        });
	    },

	    

	},

	mounted: function() {
        $(this.$refs.BandejaModal).on("hidden.bs.modal", this.limpiarModal);
    },

});
