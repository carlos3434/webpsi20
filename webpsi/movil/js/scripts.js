(function () {

//crea el aplicativo  , etiqueta principal
    var webpsi = angular.module("webpsi", // nombre del aplicativo principal
        [   'webpsi.filters', // modulos agregados al cargar el aplicativo principal
            'ngRoute',
            'ngSanitize',
            'ngAnimate',
            'angularSpinner'
        ]);


webpsi.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'views/home.html',
                controller: 'AsistenciaCtrl'
            }).
            when('/asistencia', {
                templateUrl: 'views/asistencia.html',
                controller: 'AsistenciaCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);



//crea lso controladores a partir del app
//$scope : todas la variables y funciones que pueden ser llamadas dentro del controlladores anexado a una parte
//del html
//$http : Permite hacer peticiones ajax
webpsi.controller("AsistenciaCtrl",function($scope, $http, $sce, usSpinnerService){

    $scope.showFilters = true; // muestra los filtros al comienzo
    $scope.showAsistencia = false; // oculta la tabla de asistencia
    $scope.tecnicos = {};  // datos de tecnicos a mostrar , inicia vacio
    $scope.contratas = {} // litado de contratas obtenido por ajax
    $scope.celulas = {}  // listado de celulas obtenido luego de hacer un change en contratas
    $scope.contrata_seleccionda  = "" // nombre de la contrata seleccionada
    $scope.celula_seleccionada  = "" // nombre de la celula seleccionada
    $scope.filtros = {"contrata": "", "celula":""}  // valores por defecto de los filtros

    //aGREGA LAS CONTRATAS A LA VARIABLE $scope.contratas
    $http.get('/modulos/public/movil.ajax.php?accion=getContratas').
        success(function(data, status, headers, config) {
            $scope.contratas = data.data
            //al ser llenado , automaticamente actualiza el html
        });


    $scope.MostrarFiltros = function(){
        $scope.showFilters = true;
    }

    $scope.CerrarFiltros = function(){
        $scope.showFilters = false;

    }
    //al hacer un change en empresa , debe actualizar las celulas
    $scope.UpdateCelulas = function(){
        $http.get('/modulos/public/movil.ajax.php?accion=getCelulas&idempresa=' + $scope.filtros.contrata ).
            success(function(data, status, headers, config) {
                $scope.celulas = data.data;
                //el filtro se queda pegado , por eso es encesario colocar el valor manualemnte
                //luego de actualizar las celulas
                $scope.filtros.celula = "";
            });
    };


    //actualiza el texto que muestra que filtros han sido escogidos
    $scope.UpdateData = function(){
        //angular.element : permite usar jquery
        $scope.contrata_seleccionda = angular.element("#empresas option:selected").text();
        $scope.celula_seleccionada = angular.element("#celulas option:selected").text()

    }

    $scope.UpdateData();

    $scope.MostrarAsistencia = function(){
        //actualiza todos los datos necesarios
        $scope.showFilters = false; //esconde el formulario de filtros
        $scope.UpdateData(); // actualiza los labels que tienen los filtros escogidos
        $scope.tecnicos = {}; // limpia la tabla al borrar toda la informacion
        $scope.showAsistencia = true;//muestra la tabla de asistencia
        $scope.mensaje = "";
        //muestra el spinner
        //para mostrar que se esta cargando
        usSpinnerService.spin('spinner-1');
        $http.get('/modulos/public/movil.ajax.php?accion=mostrarAsistencia' +
            '&idempresa=' + $scope.filtros.contrata +
            '&idcelula=' + $scope.filtros.celula
        ).
            success(function(data, status, headers, config) {
                //detiene el spinner
                usSpinnerService.stop('spinner-1');

                if(data.error == 1){
                    $scope.mensaje = data.data;
                }else{
                    //agrega  todos los tecnicos
                    $scope.tecnicos = data.data;
                }
            });

    }

    $scope.toggleDetalle = function(index){
      var valorActual;
        //los valores llegan como "0" en primera instancia por eso es necesario hacer su equivalencia a false
      if($scope.tecnicos[index].mostrarDetalle  === "0"){
          valorActual  = false;
      }else{
          valorActual = $scope.tecnicos[index].mostrarDetalle;
      }
        //cambia el valor true a false y viceversa
        $scope.tecnicos[index].mostrarDetalle = !valorActual;

        //SI SE ESTA ABRIENDO , CALCULAR LAS TAREAS PENDIENTES
        if(!valorActual){  // si el valor actual es falso , es por que se sta queriendo ver los detalles

            $http.get('/modulos/public/movil.ajax.php?accion=mostrarTareasPendientes' +
                '&carnet=' + $scope.tecnicos[index].carnet
            ).
            success(function(data, status, headers, config) {
                    $scope.tecnicos[index].tareas_pendientes = data;
            });

            $scope.tecnicos[index].mostrarOfficetrack = false;
            //INFO DEL TAREAS CERRADAS DEL OFFICETRACK DEL DIA
            $http.get('/modulos/public/movil.ajax.php?accion=mostrarTareasCerradas' +
                '&carnet=' + $scope.tecnicos[index].carnet +
                '&idempresa=' + $scope.filtros.contrata +
                '&idcelula=' + $scope.filtros.celula
            ).
                success(function(data, status, headers, config) {

                    if(data !== "null"){
                        $scope.tecnicos[index].mostrarOfficetrack = true;
                        $scope.tecnicos[index].officetrack = data;
                    }else{
                        $scope.tecnicos[index].officetrack = {};

                    }

                });

        }


    };



});

})();