(function () {
    angular.module('webpsi.filters',
        [
            'ngSanitize'
        ])
        .filter('normalize', function () {
            return function (input) {
                if (!input) return "";
                input = input
                    .replace('♀', 'f')
                    .replace('♂', 'm')
                    .replace(/\W+/g, "");
                return input.toLowerCase();
            };
        })
        .filter('imageify', ['$filter', function ($filter) {
            return function (input) {
                var url = "img/pokemons/" + $filter('normalize')(input) + ".jpg";
                return url;
            };
        }])
        .filter('MarcarTelefono', ['$filter','$sce', function ($filter,$sce) {
            return function (input) {
                var url = "";
                if(input !== null )  url = "<a href='tel:"+input.substr(-9)+"'>" + input + "</a>";

                return $sce.trustAsHtml(url);
            };
        }])
        .filter('soloHora', ['$filter','$sce', function ($filter,$sce) {
            return function (input) {
                if(input === null) input = ""
                return input.substr(-8);
            };
        }])
    ;
})();