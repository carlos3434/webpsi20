<?php
require_once("debug.php");

require_once("cabecera.php");
require_once("clases/class.Perfil.php");
require_once("clases/class.Area.php");
//var_dump($_SESSION);

$perfil = new Perfil();

$arrPerfiles = $perfil->ListadoPerfiles();

$area = new Area();

$arrAreas = $area->ListadoAreas();

?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <title>PSI - Web SMS - Mensajes Libres e Individuales</title>

            <?php include ("includes.php") ?>         

            <link href="sms_libre.css" rel="stylesheet" type="text/css" media="all"/>
            <script type="text/javascript">

                $(document).ready(function() {
                    iniciar();
	
                });

                function iniciar()
                {
                    $("#countdown").val(140);
                    $("#celular").val("");
                    $("#mensaje").val("");
                }


                function ingresar()
                {
                    var nombres = $("#nombres").val();
                    var apellidos = $("#apellidos").val();
                    var usuario = $("#usuario").val();
					var password=$("#password").val();
					var dni = $("#dni").val();
					var perfil = $("#cmb_perfil").val();
					var area = $("#cmb_area").val();
				
					
					
                    $.ajax({
                        type: "POST",
                        url: "ingresar_usuario_ajax.php",
                        data: { 
                            ingresa_usuario: 1,
                            nombres: nombres,
                            apellidos: apellidos,
                            usuario: usuario,
							password: password,
							dni: dni,
							perfil: perfil,
							area: area
                        }
                    }).done(function( msg ) {
                        //var res = msg.split("|");
                        //r = $.trim(res[1])
						var r = $.trim(msg)
                        //alert(r);
                        if (r == "1") {
                            $("#div_res").html("Se envio el mensaje correctamente.");
                            iniciar();
                        }
                        else
                            $("#div_res").html("Ocurrio un error, no se pudo enviar el mensaje.");
                    });
                }

                function limitText() {
                    var limitField = document.getElementById("mensaje");
                    var limitCount = document.getElementById("countdown");
                    var limitNum = 140;
	
                    if (limitField.value.length > limitNum) {
                        limitField.value = limitField.value.substring(0, limitNum);
                    } else {
                        limitCount.value = limitNum - limitField.value.length;
                    }
                }

				

            </script>
    </head>

    <body>
        <input type="hidden" value="<?php echo $IDUSUARIO?>" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <?php echo pintar_cabecera(); ?>
            <div id="main-content">
                <div id="id0" class="div0" >
                <table>
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">&nbsp;Registrar Usuarios</th>
                    </tr>	
                </thead>

                <tbody>
				    <tr>
                        <th scope="row" class="column1">Nombres:</th>
                        <td class="column2"><input name="nombres" id="nombres" type="text" value="<?php echo $nombre?>"  /></td>
                    </tr>
				
	                <tr>
                        <th scope="row" class="column1">Apellidos:</th>
                        <td class="column2"><input name="apellidos" id="apellidos" type="text" value="<?php echo $apellido?>"  /></td>
                    </tr>

                    <tr>
                        <th scope="row" class="column1">Usuario:</th>
                        <td class="column2"><input name="usuario" id="usuario" type="text" value="<?php echo $usuario?>"  /></td>
                    </tr>

                    <tr>
                        <th scope="row" class="column1">Password:</th>
                        <td class="column2"><input name="password" id="password" type="password" value="<?php echo $password?>"  /></td>
                    </tr>					
					
					<tr>
                        <th scope="row" class="column1">DNI:</th>
                        <td class="column2"><input name="dni" id="dni" type="text" value="<?php echo $dni?>"  /></td>
                    </tr>
					
					<tr>
                        <th scope="row" class="column1">Perfil:</th>
                        <td>
                        <select name='cmb_perfil' id='cmb_perfil' class="combo_perfil">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php
                                                    foreach ($arrPerfiles as $fila) {
                                                    ?>
                                                    <option value='<?php echo $fila[0] ?>'><?php echo $fila[1] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                        </select>
                        </td>
                    </tr>
					
					<tr>
                        <th scope="row" class="column1">Area:</th>
                        <td>
                        <select name='cmb_area' id='cmb_area' class="combo_area">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php
                                                    foreach ($arrAreas as $fila) {
                                                    ?>
                                                    <option value='<?php echo $fila[0] ?>'><?php echo $fila[1] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                        </td>
                    </tr>
           
                    <tr>
                        <td colspan="2" class="td_center"><input name="enviar" type="button" value="Ingresar" onclick="ingresar()"/></td>
                    </tr>
                    </tbody>
                    </table>
                </div>

                <div id="div_res" class="div0"></div>                


        <?php
		/*  
		*/
		
        /*
            <div id="feature-content">

            <div id="feature-left">
            <h1><a href="#">Ten Ways To Improve Your Code</a></h1>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>

            <div id="feature-right">
            <div class="feature-mini">
            <h2><a href="#">Ten Ways To Improve Your Code</a></h2>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>
            <div class="feature-mini">
            <h2><a href="#">Ten Ways To Improve Your Code</a></h2>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>
            <div class="clear"></div>
            </div>
            <div class="clear"></div>
            </div>
            */
        ?>
        </div>

        <div id="footer">
            &copy;      2013 PSI - Planificacion de Soluciones Informaticas
        </div>

        </div>

    </body>
</html>