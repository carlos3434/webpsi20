
var esIE = false;
var scriptIE6 = "";
if (navigator.appVersion.indexOf("MSIE") != -1) { esIE = true; }
swIE60 = (navigator.appVersion.indexOf("6.0") != -1) ? 1 : 0;
swIE80 = (navigator.appVersion.indexOf("8.0") != -1);

    
var submenu = { 
    load: function (my_class) {
        $(my_class + " div.submenu_01").each(function () {
		var item = $("#" + this.id + " .submenu");
                var classitem = item.attr("class");
                var classitemover = classitem + "_over";
                var itemop = $("#" + this.id + " .submenu_sel");
                var classitemop = itemop.attr("class");
                var classitemopover = classitemop + "_over";
                $(this).bind("mouseenter", function () {
                    submenu.mostrar(this.id, item, classitemover, itemop, classitemopover);
                }).bind("mouseleave", function () {
                    submenu.ocultar(this.id, item, classitem, itemop, classitemop)
                    });
                });
            },
    ocultar: function (my_id, item, classitem, itemop, classitemop) {
        $("#" + my_id).removeClass("overmenu").css("position", "static");
        item.attr("class", classitem);
        itemop.attr("class", classitemop);
        if (swIE60 == 1) $(".toinvisible").css("visibility", "visible");
    },
    mostrar: function (my_id, item, classitem, itemop, classitemop) {
        $("#" + my_id).addClass("overmenu").css("position", "relative");
        item.attr("class", classitem);
        itemop.attr("class", classitemop);
        if (swIE60 == 1) $(".toinvisible").css("visibility", "hidden");
    }
    }


var init = {
    funciones: function () {
        if ($(".topmenu").size() != 0) submenu.load(".topmenu");
    }
}


var view_submodulos=function(idmodulo){

    $(".bloque_submodulos").css("display","none");

    var box_change=$('#box_detalle');
    box_change.text('');
    var bloque_submodulo=$('#modulo'+idmodulo);
    bloque_submodulo.css("display","block");
}

$(document).ready(function () {
    init.funciones();
});


