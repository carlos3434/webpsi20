function soloNumeros(evento) {
    var charCode = (evento.which) ? evento.which : evento.keyCode
    if(charCode == 8) return true;   // Tecla: Backspace
    if(charCode == 9) return true;   // Tecla: TAB
    if(charCode == 13) return true;  // Tecla: Enter
    if(charCode == 37) return true;  // Tecla: Flecha hacia la izquierda
    if(charCode == 39) return true;  // Tecla: Flecha hacia la derecha

    // Numeros: 0-9
    if (charCode >= 48 && charCode <= 57) {
        return true
    }
    return false;
}

function soloNumTelefono(evento) {
    var charCode = (evento.which) ? evento.which : evento.keyCode
	if(charCode == 8) return true;   // Tecla: Backspace
    	if(charCode == 9) return true;   // Tecla: TAB
    	if(charCode == 13) return true;  // Tecla: Enter
	if(charCode == 37) return true;  // Tecla: Flecha hacia la izquierda
	if(charCode == 39) return true;  // Tecla: Flecha hacia la derecha
    	if(charCode == 45) return true;  // Tecla: -

	// Numeros: 0-9
	if (charCode >= 48 && charCode <= 57) {
		return true
    }
	return false;
}

function soloLetras(evento){
    var charCode = (evento.which) ? evento.which : evento.keyCode;
    if(charCode == 8) return true;      // Tecla: Backspace
    if(charCode == 9) return true;      // Tecla: TAB
    if(charCode == 13) return true;     // Tecla: Enter
    if(charCode == 32) return true;     // Tecla: Espacio
    if(charCode == 37) return true;     // Tecla: Flecha hacia la izquierda
    if(charCode == 39) return true;     // Tecla: Flecha hacia la derecha

    // Letras con tilde
    if(charCode == 233) return true;    // Tecla: e
    if(charCode == 225) return true;    // Tecla: a
    if(charCode == 237) return true;    // Tecla: i
    if(charCode == 243) return true;    // Tecla: o
    if(charCode == 250) return true;    // Tecla: u

    /* Letras: a-z A-Z */
    if(65<=charCode && charCode<=90 || 97<=charCode && charCode<=122){
        return true;
    } else{
        return false;
    }
 }

