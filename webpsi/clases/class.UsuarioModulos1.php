<?php
include_once $_SERVER['DOCUMENT_ROOT']."/webpsi/clases/class.Conexion.php";


class UsuarioModulos {
    private $id = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }
	
    public function getId_Area() {
        return $this->Id_Area;
    }

    public function setId_Area($x) {
        $this->Id_Area = $x;
    }

    /*public function ListadoModulosUsuario($idUsuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
		$cad = "SELECT m.modulo, m.idmodulo FROM `submodulos` s, `modulos` m
		WHERE s.`idmodulo`=m.`idmodulo` GROUP BY 1 ORDER BY 1 ; ";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function ListadoSubModulosUsuario($idModulo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
		$cad = "SELECT m.modulo, s.submodulo, s.`path` FROM `submodulos` s, `modulos` m
				WHERE s.`idmodulo`=m.`idmodulo` 
				AND s.idmodulo = ".$idModulo." 
				ORDER BY 1,2; ";

        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
	*/
	
	public function ListadoModulosUsuario($idUsuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
		$cad = "SELECT 
			  m.modulo,
			  m.idmodulo 
			FROM
			  `submodulos` s,
			  `modulos` m, `usuarios_submodulos` h
			WHERE s.`idmodulo` = m.`idmodulo` 
			AND h.`idsubmodulo`=s.`idsubmodulo`
			AND h.`idusuario` = ".$idUsuario."
			GROUP BY 1 
			ORDER BY 1 ;" ;

		//echo $cad;
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            //$arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
      //  return $arr;
    }

    public function ListadoSubModulosUsuario($idModulo, $idUsuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

		$cad = "SELECT m.modulo, s.submodulo, s.`path` 
				FROM `submodulos` s, `modulos` m, `usuarios_submodulos` h
				WHERE s.`idmodulo`=m.`idmodulo` 
				AND s.idmodulo = ".$idModulo."
				AND h.`idsubmodulo`=s.`idsubmodulo`
				AND h.`idusuario` = ".$idUsuario."
				ORDER BY 1,2; ";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
	
	

	
}