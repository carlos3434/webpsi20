<?php

include_once "class.Conexion.php";

class Area {
    private $id = '';
    private $id_area = '';
    private $estado = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getIdArea() {
        return $this->id_area;
    }

    public function setIdArea($x) {
        $this->id_area = $x;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($x) {
        $this->estado = $x;
    }


    public function ListadoAreas() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT id, area, estado FROM `tb_area`  ORDER BY area ASC;";

        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

}