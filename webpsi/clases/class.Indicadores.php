<?php

include_once "class.Conexion.php";

class Indicadores {

	public static $BD_MAIN = "webpsi";
	public static $BD_FFTT = "webpsi_fftt";
	

    /**************************************************  
    /* Functiones de FECHAS DE ACTUALIZACION DE TABLAS
    ***************************************************  */
    public function getFechaUpdateBasica() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

    public function getFechaUpdatePendientesProvisionTba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";

        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }    

    public function getFechaUpdatePendientesProvisionAdsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'data_macros_fin' 
              AND TABLE_NAME = 'pro_pdt_pais_adsl' LIMIT 1 ";

        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }   

    public function getFechaUpdatePendientesProvisionCatv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    /********************* 
    /* Functiones de AVERIAS Pendientes
    ********************* */

    public function getPendAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT mdf, COUNT(*) FROM schedulle_sistemas.pen_pais_tba WHERE  negocio='STB' ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getPendAveriasTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT mdf, COUNT(*) FROM schedulle_sistemas.pen_pais_tba WHERE  negocio='STB' ".$filtroMdf."
            ".$filtroTecnico." GROUP BY 1 ORDER BY 1; "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getDetallePendAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT telefono, averia as actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s') as fecreg, mdf as fftt, carea as estado, 
                  CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,       
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY, cod_tecnico 
                FROM schedulle_sistemas.pen_pais_tba a 
                WHERE  negocio='STB' ".$filtroMdf."
                ORDER BY fecreg ASC; "; 
        //echo $cad; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    


    public function getPendAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT mdf, COUNT(*) FROM schedulle_sistemas.pen_pais_adsl WHERE  negocio='ADSL' ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; ";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    

    public function getPendAveriasAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT mdf, COUNT(*) FROM schedulle_sistemas.pen_pais_adsl WHERE  negocio='ADSL' ".$filtroMdf."
             ".$filtroTecnico." GROUP BY 1 ORDER BY 1; ";
        
        //echo $cad;
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    



    public function getDetallePendAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " AND mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT telefono, averia as actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s') as fecreg, mdf as fftt,carea as estado, 
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,                  
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,                
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY, cod_tecnico as cod_tecnico
                FROM schedulle_sistemas.pen_pais_adsl a 
                WHERE  negocio='ADSL' ".$filtroMdf."
                ORDER BY fecreg ASC ";
		//echo $cad;
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    

    public function getPendAveriasCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
		
		
        $listaMdfs = "'".$nodo."'";
        $filtroMdf = "  mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT mdf, COUNT(*) FROM schedulle_sistemas.pen_pais_catv WHERE ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; ";
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    

	public function getPendAveriasCatv_ConSinTenico($nodo, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
		
        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND codigo_tecnico!='SIN-TECNICO' ";
        else  // con tecnico NO
            $filtroTecnico = " AND codigo_tecnico='SIN-TECNICO' ";		
		
		
        $listaMdfs = "'".$nodo."'";
        $filtroMdf = "  nodo IN (".$listaMdfs.") ";
        
        $cad = "SELECT nodo, COUNT(*) FROM `data_macros_fin`.view_aver_pen_catv_pais_tecnico 
			WHERE ".$filtroMdf." ".$filtroTecnico."
            GROUP BY 1 ORDER BY 1; ";
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    
	
    public function getDetallePendAveriasCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$nodo."'";
        $filtroMdf = "  mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT negocio, inscripcion, mdf as fftt, averia as actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s') as fecreg, '' as estado, 
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,  
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                csegmento as segmentoY  
                FROM schedulle_sistemas.pen_pais_catv a 
                WHERE ".$filtroMdf."
                ORDER BY fecreg ASC ";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }       

    public function getDetallePendAveriasCatv_ConSinTecnico($nodo, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

				
        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND codigo_tecnico!='SIN-TECNICO' ";
        else  // con tecnico NO
            $filtroTecnico = " AND codigo_tecnico='SIN-TECNICO' ";		
		
		
        $listaMdfs = "'".$nodo."'";
        $filtroMdf = "  nodo IN (".$listaMdfs.") ";
        
        $cad = "SELECT 'CATV' as negocio, codigodelcliente, nodo as fftt, codigo_req as actuacion, 
                date_format(fecharegistro, '%d-%m-%Y %H:%i:%s') as fecreg, '' as estado, 
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecharegistro)) < 31 AND DATEDIFF(NOW(), fecharegistro)>0 
                    THEN CONCAT('V:', HOUR(fecharegistro) ) ELSE '' END as marca,  
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.codigo_req) AS fec_agenda,
                indicador_vip as segmentoY  
                FROM `data_macros_fin`.view_aver_pen_catv_pais_tecnico a 
                WHERE ".$filtroMdf." ".$filtroTecnico."
                ORDER BY fecharegistro ASC ";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    } 	
	
	
    /*************************************
    /* Functiones de PROVISION Pendientes
    ***************************************
     */
    public function getPendProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT MDF, COUNT(*) FROM schedulle_sistemas.tmp_gaudi_tba WHERE   ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getPendProvisionTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT MDF, CIP, COUNT(*) FROM schedulle_sistemas.tmp_gaudi_tba WHERE   ".$filtroMdf."
            ".$filtroTecnico." GROUP BY 1,2 ORDER BY 1,2; "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getDetallePendProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT FONO as telefono, ESTADO as estado, ZONAL, 
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis as actuacion, 
                date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.pedidoatis) AS fec_agenda,
                SEGMENTO as segmentoY,
                zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,                
                CIP as cod_tecnico
                FROM schedulle_sistemas.tmp_gaudi_tba a
                WHERE   ".$filtroMdf."             
                ORDER BY F_REG ASC;  ";  
        //echo $cad;

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getPendProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT MDF, COUNT(*) FROM schedulle_sistemas.tmp_gaudi_adsl WHERE   ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getPendProvisionAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT MDF, CIP, COUNT(*) FROM schedulle_sistemas.tmp_gaudi_adsl WHERE   ".$filtroMdf."
             ".$filtroTecnico."  GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getPendProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT Mdf, COUNT(*) FROM data_macros_fin.pro_pdt_pais_adsl WHERE   ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    

    public function getDetallePendProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT FONO as telefono, ESTADO as estado, ZONAL, 
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis as actuacion, 
                date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,
                 zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                 SEGMENTO as segmentoY,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				CIP as cod_tecnico
                FROM schedulle_sistemas.tmp_gaudi_adsl WHERE   ".$filtroMdf."
                ORDER BY F_REG ASC; "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getDetallePendProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT Telefono as telefono, Estado as estado, Zonal, Mdf as fftt, Petic as actuacion, F_Emi_Atis as fecreg,
                CASE DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_Emi_Atis) ) ELSE '' END as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.Petic) AS fec_agenda
                FROM data_macros_fin.pro_pdt_pais_adsl a 
                WHERE   ".$filtroMdf."
                ORDER BY F_Emi_Atis DESC; "; 
        echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }


    public function getPendProvisionCatv($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT MDF, COUNT(*) FROM schedulle_sistemas.tmp_gaudi_catv WHERE   ".$filtroMdf."
            GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

	
	
    public function getDetallePendProvisionCatv($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " cms_nodo IN (".$listaMdfs.") ";
        
        $cad = "SELECT negocio, ESTADO as estado, AREA, 
                date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                cms_nodo AS fftt, codreq AS actuacion, data11 as inscripcion,
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.codreq) AS fec_agenda,
                SEGMENTO as segmentoY,
                CONCAT(cms_nodo, cms_troba, LPAD(cms_amp,2,'0'), LPAD(cms_tap,2,'0')) AS llavexy,
                (SELECT numcoo_x FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordX, 
                (SELECT numcoo_y FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordY                  
                FROM schedulle_sistemas.tmp_gaudi_catv a
                WHERE ".$filtroMdf."
             ORDER BY F_REG ASC; "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    

    /*********************************/
    public function listarAveriasBasica() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT MDF, NOMBRE FROM ".self::$BD_FFTT.".`mdfs_eecc_regiones` WHERE ACTIVO=1";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    /*********************************/

    /*************************************
    /* Por VENCER PROVISION
    **************************************
     */

    public function getPorVencerProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0) "; 
        // 0 = vence hoy , 1 = vence mañana

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    

    public function getPorVencerProvisionTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_tba` WHERE ".$filtroMdf." 
                ".$filtroTecnico." AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0) "; 
        // 0 = vence hoy , 1 = vence mañana

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }       

    public function getDetallePorVencerProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                FONO as telefono, ESTADO as estado, MDF as mdf, C6 as armario, C7 as cable,
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis as actuacion, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,    
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.pedidoatis) AS fec_agenda,
                zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                SEGMENTO as segmentoY,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				CIP as cod_tecnico

                FROM schedulle_sistemas.`tmp_gaudi_tba` a
                WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0) 
                ORDER BY F_REG"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }    

    public function getPorVencerProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0)"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getPorVencerProvisionAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";        

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                ".$filtroTecnico."  AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0)"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    

    public function getDetallePorVencerProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg,  
                FONO as telefono, ESTADO as estado, MDF as mdf, C6 as armario, C7 as cable,
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis as actuacion,
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,                    
                zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                SEGMENTO as segmentoY,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				CIP as cod_tecnico

                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0)
                ORDER BY F_REG"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }

    public function getPorVencerProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";
        
        $cad = "SELECT count(*)
                FROM data_macros_fin.pro_pdt_pais_adsl WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) in (0)"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getDetallePorVencerProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        $cad = "SELECT F_Emi_Atis as fecreg, Telefono as telefono, Estado as estado, Mdf as mdf, '' as armario, '' as cable,
                CONCAT(Mdf, ' ', '', ' ', '') as fftt, Petic as actuacion,
                CASE DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_Emi_Atis) ) ELSE '' END as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.Petic) AS fec_agenda                                
                FROM data_macros_fin.pro_pdt_pais_adsl  a 
                WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) in (0)"; 
        //echo $cad;
        //die();
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }


    public function getPorVencerProvisionCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_catv` WHERE ".$filtroNodo." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0) "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getDetallePorVencerProvisionCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                data11 AS inscripcion, ESTADO AS estado, MDF AS mdf, C6 AS armario, C7 AS cable,
                CONCAT(MDF, ' ', C6, ' ', C7) AS fftt, codreq AS actuacion, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,    
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.data17) AS fec_agenda,
                SEGMENTO as segmentoY,
                CONCAT(cms_nodo, cms_troba, LPAD(cms_amp,2,'0'), LPAD(cms_tap,2,'0')) AS llavexy,
                (SELECT numcoo_x FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordX, 
                (SELECT numcoo_y FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordY  
                FROM schedulle_sistemas.`tmp_gaudi_catv` a
                WHERE ".$filtroNodo." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) in (0) 
                ORDER BY F_REG"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }    

    /*************************************
    /* Por VENCER AVERIAS
    **************************************
     */
    public function getPorVencerAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_tba` WHERE ".$filtroMdf." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24   AND DATEDIFF(NOW(), fecreg)>0   "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getPorVencerAveriasTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_tba` WHERE ".$filtroMdf." 
                ".$filtroTecnico." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24   AND DATEDIFF(NOW(), fecreg)>0   "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getDetallePorVencerAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT telefono, carmario AS armario, ccable AS cable, mdf, averia AS actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s') as fecreg,
                concat(mdf, ' ', carmario, ' ', ccable) as fftt, 
 
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,

                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY, cod_tecnico				
                FROM schedulle_sistemas.`pen_pais_tba` a 
                WHERE ".$filtroMdf." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24  AND DATEDIFF(NOW(), fecreg)>0  
                ORDER by fecreg ASC "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }    

    public function getPorVencerAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_adsl` WHERE ".$filtroMdf." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24  AND DATEDIFF(NOW(), fecreg)>0   "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getPorVencerAveriasAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_adsl` WHERE ".$filtroMdf." 
                ".$filtroTecnico." AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24  AND DATEDIFF(NOW(), fecreg)>0   "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getDetallePorVencerAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT telefono, carmario AS armario, ccable AS cable, mdf, averia AS actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s') as fecreg,
                concat(mdf, ' ', carmario, ' ', ccable) as fftt, 

                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,                                      
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda ,
                
                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY, cod_tecnico
                FROM schedulle_sistemas.`pen_pais_adsl` a 
                WHERE ".$filtroMdf." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24   AND DATEDIFF(NOW(), fecreg)>0 
                ORDER BY fecreg ASC "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }


    public function getPorVencerAveriasCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodos = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_catv` WHERE ".$filtroNodos." 
                AND  HOUR(TIMEDIFF(NOW(), fecreg)) < 24  AND DATEDIFF(NOW(), fecreg)>0   "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

	public function getPorVencerAveriasCatv_ConSinTecnico($nodo, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
		
        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND codigo_tecnico!='SIN-TECNICO' ";
        else  // con tecnico NO
            $filtroTecnico = " AND codigo_tecnico='SIN-TECNICO' ";		
			
		
        $listaNodos = "'".$nodo."'";
        $filtroNodos = " nodo    IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM `data_macros_fin`.view_aver_pen_catv_pais_tecnico  
				WHERE ".$filtroNodos." ".$filtroTecnico." 
                AND  HOUR(TIMEDIFF(NOW(), fecharegistro)) < 24  AND DATEDIFF(NOW(), fecharegistro)>0   "; 
		//echo $cad;	
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }
	
    public function getDetallePorVencerAveriasCatv_ConSinTecnico($nodo, $fecha, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND codigo_tecnico!='SIN-TECNICO' ";
        else  // con tecnico NO
            $filtroTecnico = " AND codigo_tecnico='SIN-TECNICO' ";		
			
				
        $listaNodos = "'".$nodo."'";
        $filtroNodos = " nodo IN (".$listaNodos.") ";
        
        $cad = "SELECT 'CATV' as negocio, codigodelcliente as inscripcion, nodo as mdf, codigo_req as actuacion, 
				fecharegistro as fecreg, '' as estado,
                CONCAT(nodo, ' ', troba, ' ', lex) AS fftt, 
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecharegistro)) < 31 AND DATEDIFF(NOW(), fecharegistro)>0 
                    THEN CONCAT('V:', HOUR(fecharegistro) ) ELSE '' END as marca,                      
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.codigo_req) AS fec_agenda,
                indicador_vip as segmentoY                 
                FROM  `data_macros_fin`.view_aver_pen_catv_pais_tecnico  a 
                WHERE ".$filtroNodos." ".$filtroTecnico."
                AND  HOUR(TIMEDIFF(NOW(), fecharegistro)) < 24  AND DATEDIFF(NOW(), fecharegistro)>0   
                ORDER BY fecharegistro "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error()." ".$cad);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }


    /*************************************
    /* Functiones de PROVISION Vencidas
    **************************************
     */
    public function getVencidasProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasProvisionTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_tba` WHERE ".$filtroMdf." 
                ".$filtroTecnico."  AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    

    public function getVencidasDetalleProvisionTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT FONO AS telefono, date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg,  
                MDF AS mdf, C6 AS armario, C7 AS cable, 
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis AS actuacion, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,    
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.pedidoatis) AS fec_agenda,
                SEGMENTO as segmentoY,
                zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				CIP as cod_tecnico

                FROM schedulle_sistemas.`tmp_gaudi_tba` a
                WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0 
                ORDER BY F_REG ASC"; 
        
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }


    public function getVencidasProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasProvisionAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND CIP!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND CIP='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                ".$filtroTecnico." AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasDetalleProvisionAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT FONO AS telefono, 
                date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, MDF AS mdf, C6 AS armario, C7 AS cable, 
                CONCAT(MDF, '/',gestel_armario, ' ', gestel_cable_primario) as fftt, pedidoatis AS actuacion,
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,    
                zonal, gestel_armario AS armario, gestel_cable_primario AS cable, gestel_terminal AS terminal,
                SEGMENTO as segmentoY,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,   
				CIP as cod_tecnico
                FROM schedulle_sistemas.`tmp_gaudi_adsl` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0
                ORDER BY F_REG ASC"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }      

    
    public function getVencidasProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM data_macros_fin.pro_pdt_pais_adsl WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) < 0"; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasDetalleProvisionAdsl2($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        $cad = "SELECT F_Emi_Atis as fecreg, Telefono as telefono, Estado as estado, Mdf as mdf, '' as armario, '' as cable,
                CONCAT(Mdf, ' ', '', ' ', '') as fftt, Petic as actuacion,
                ' ' as marca,
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.Petic) AS fec_agenda                
                FROM data_macros_fin.pro_pdt_pais_adsl a
                WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_Emi_Atis , INTERVAL 7 DAY ), NOW()) < 0";              

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }    


    public function getVencidasProvisionCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`tmp_gaudi_catv` WHERE ".$filtroNodo." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0  "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getVencidasDetalleProvisionCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT data11 AS inscripcion, 
                date_format(F_REG, '%d-%m-%Y %H:%i:%s' ) as fecreg, 
                MDF AS mdf, C6 AS troba, C7 AS ct, 
                CONCAT(MDF, ' ', C6, ' ', C7) AS fftt, codreq AS actuacion, 
                CASE DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) WHEN 0 THEN CONCAT('V:', HOUR(F_REG) ) ELSE '' END as marca,    
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_provision` WHERE PEDIDO=a.codreq) AS fec_agenda,
                SEGMENTO as segmentoY,
                    CONCAT(cms_nodo, cms_troba, LPAD(cms_amp,2,'0'), LPAD(cms_tap,2,'0')) AS llavexy,
                (SELECT numcoo_x FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordX, 
                (SELECT numcoo_y FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordY  
                FROM schedulle_sistemas.`tmp_gaudi_catv` a 
                WHERE ".$filtroNodo." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 0 
                ORDER BY F_REG "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }


    /*************************************
    /* Functiones de AVERIAS Vencidas
    **************************************
     */

    public function getVencidasAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_tba` WHERE ".$filtroMdf." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasAveriasTba_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_tba` WHERE ".$filtroMdf." 
                ".$filtroTecnico." AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getVencidasDetalleAveriasTba($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT telefono, mdf, carmario, ccable AS cable, 
                CONCAT(mdf, ' ', carmario, ' ', ccable) AS fftt, averia AS actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s' ) as fecreg,
                
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,                      
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY , cod_tecnico

                FROM schedulle_sistemas.`pen_pais_tba` a 
                WHERE ".$filtroMdf." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                ORDER BY fecreg ASC "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getVencidasAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_adsl` WHERE ".$filtroMdf." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getVencidasAveriasAdsl_ConSinTenico($mdf, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cod_tecnico!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cod_tecnico='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_adsl` WHERE ".$filtroMdf." 
                ".$filtroTecnico." AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getVencidasDetalleAveriasAdsl($mdf) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT telefono, mdf, carmario, ccable AS cable, 
                CONCAT(mdf, ' ', carmario, ' ', ccable) AS fftt, averia AS actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s' ) as fecreg,
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,                                      
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                zonal, carmario as armario, ccable as cable, cbloque as bloque, terminal,
                
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY,
                csegmento as segmentoY, cod_tecnico
                FROM schedulle_sistemas.`pen_pais_adsl` a 
                WHERE ".$filtroMdf." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                ORDER BY fecreg ASC "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getVencidasAveriasCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodos = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM schedulle_sistemas.`pen_pais_catv` WHERE ".$filtroNodos." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                 "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

    public function getVencidasAveriasCatv_ConSinTecnico($nodo, $contecnico) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
		
        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND codigo_tecnico!='SIN-TECNICO' ";
        else  // con tecnico NO
            $filtroTecnico = " AND codigo_tecnico='SIN-TECNICO' ";		
		
		
        $listaNodos = "'".$nodo."'";
        $filtroNodos = " nodo IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT count(*)
                FROM `data_macros_fin`.view_aver_pen_catv_pais_tecnico 	
				WHERE ".$filtroNodos." ".$filtroTecnico." 
                AND ( HOUR(TIMEDIFF(NOW(), fecharegistro)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecharegistro))>0 )
                 "; 
		//echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }	
	
	
    public function getVencidasDetalleAveriasCatv($nodo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodos = " MDF IN (".$listaNodos.") ";
        
       /*$cad = "SELECT F_REG, DATE_ADD(F_REG , INTERVAL 7 DAY ), DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW())
                FROM `tmp_gaudi_tba` WHERE ".$filtroMdf." 
                AND DATEDIFF(DATE_ADD(F_REG , INTERVAL 7 DAY ), NOW()) < 2 "; 
        */
        $cad = "SELECT inscripcion,
                telefono, mdf, carmario, ccable AS cable, 
                CONCAT(mdf, ' ', carmario, ' ', ccable) AS fftt, averia AS actuacion, 
                date_format(fecreg, '%d-%m-%Y %H:%i:%s' ) as fecreg,
                CASE WHEN HOUR(TIMEDIFF(NOW(), fecreg)) < 31 AND DATEDIFF(NOW(), fecreg)>0 
                    THEN CONCAT('V:', HOUR(fecreg) ) ELSE '' END as marca,      
                (SELECT FECHA_AGENDA_FIN FROM ".self::$BD_MAIN.".`rpt_agendadas_averias` WHERE AVERIA=a.averia) AS fec_agenda,
                csegmento as segmentoY                 
                FROM schedulle_sistemas.`pen_pais_catv` a 
                WHERE ".$filtroNodos." 
                AND ( HOUR(TIMEDIFF(NOW(), fecreg)) >= 24 AND MINUTE(TIMEDIFF(NOW(), fecreg))>0 )
                ORDER BY fecreg ASC "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }

    /*************************************
    /* Functiones de PROVISION Liquidadas
    **************************************
     */

    public function getLiqDiaProvisionTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        $cad = "SELECT COUNT(*) FROM schedulle_sistemas.liquidadas_averias_provision_prov_stb 
            WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';"; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getLiqDiaProvisionTba_ConSinTenico($mdf, $contecnico, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cip!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cip='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        $cad = "SELECT COUNT(*) FROM schedulle_sistemas.liquidadas_averias_provision_prov_stb 
            WHERE ".$filtroMdf." ".$filtroTecnico. " AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';"; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getDetalleLiqDiaProvisionTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        $cad = "SELECT codcli AS inscripcion, fono AS telefono, fecreg, mdf, pedidoatis AS actuacion
                FROM schedulle_sistemas.liquidadas_averias_provision_prov_stb 
                WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';"; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }


    public function getLiqDiaProvisionAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        $cad = "SELECT COUNT(*) FROM schedulle_sistemas.liquidadas_averias_provision_prov_adsl 
            WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';";    
     


        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }


    public function getLiqDiaProvisionAdsl2($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        /*$cad = "SELECT COUNT(*) FROM schedulle_sistemas.liquidadas_averias_provision_prov_adsl 
            WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';";    
        */
        $cad = "SELECT COUNT(*) FROM data_macros_fin.`pro_liq_pais_adsl_acumulado` WHERE ".$filtroMdf."
                AND DATE_FORMAT(F_Ejecucion, '%Y-%m-%d')='$fecha'; ";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    

    public function getLiqDiaProvisionAdsl2_ConSinTenico($mdf, $contecnico, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND Carnet_Tecn!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND Carnet_Tecn='' ";


        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        /*$cad = "SELECT COUNT(*) FROM schedulle_sistemas.liquidadas_averias_provision_prov_adsl 
            WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';";    
        */
        $cad = "SELECT COUNT(*) FROM data_macros_fin.`pro_liq_pais_adsl_acumulado` WHERE ".$filtroMdf."
                ".$filtroTecnico. "AND DATE_FORMAT(F_Ejecucion, '%Y-%m-%d')='$fecha'; ";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    


    public function getDetalleLiqDiaProvisionAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        $cad = "SELECT codcli AS inscripcion, fono AS telefono, fecreg, mdf, pedidoatis AS actuacion 
            FROM schedulle_sistemas.liquidadas_averias_provision_prov_adsl 
            WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha';"; 
        
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }    


    public function getDetalleLiqDiaProvisionAdsl2($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " Mdf IN (".$listaMdfs.") ";

        $cad = "SELECT Inscripcion as inscripcion, Telefono as telefono, F_Form_Gestel as fecreg, Mdf as mdf, trim(Petic) as actuacion,
                concat(Mdf, ' ') as fftt, 'Liquidado' as estado
                FROM data_macros_fin.`pro_liq_pais_adsl_acumulado` WHERE ".$filtroMdf."
                AND DATE_FORMAT(F_Ejecucion, '%Y-%m-%d')='$fecha'; ";

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }        

    public function getLiqDiaProvisionCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " nodo IN (".$listaNodos.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.prov_liq_catv_pais 
                WHERE ".$filtroNodo." AND DATE_FORMAT(fecha_liquidacion, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getDetalleLiqDiaProvisionCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " nodo IN (".$listaNodos.") ";

        $cad = "SELECT codigo_req AS actuacion, codigo_del_cliente AS inscripcion, fecha_registro AS fecreg, nodo AS mdf 
                FROM schedulle_sistemas.prov_liq_catv_pais 
                WHERE ".$filtroNodo." AND DATE_FORMAT(fecha_liquidacion, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }

    /*********************************/
	
    /*************************************
    /* Functiones de AVERIAS Liquidadas
    **************************************
     */

    public function getLiqDiaAveriasCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " nodo IN (".$listaNodos.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.aver_liq_catv_pais
                WHERE ".$filtroNodo." AND DATE_FORMAT(fecha_liquidacion, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    

    public function getDetalleLiqDiaAveriasCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaNodos = "'".$nodo."'";
        $filtroNodo = " nodo IN (".$listaNodos.") ";

        $cad = "SELECT codigoreq AS actuacion, codigodelcliente AS inscripcion, nodo AS mdf, fecharegistro AS fecreg 
                FROM schedulle_sistemas.aver_liq_catv_pais
                WHERE ".$filtroNodo." AND DATE_FORMAT(fecha_liquidacion, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }

    public function getLiqDiaAveriasTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.liquidadas_averias_provision_aver_stb 
                WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getLiqDiaAveriasTba_ConSinTenico($mdf, $contecnico, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cip!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cip='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.liquidadas_averias_provision_aver_stb 
                WHERE ".$filtroMdf." ".$filtroTecnico." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }

    public function getDetalleLiqDiaAveriasTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT actu AS actuacion, fono AS telefono, fecreg, mdf 
                FROM schedulle_sistemas.liquidadas_averias_provision_aver_stb 
                WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }
    
    public function getLiqDiaAveriasAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.liquidadas_averias_provision_aver_adsl 
                WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }    
    

    
    public function getLiqDiaAveriasAdsl_ConSinTecnico($mdf, $contecnico, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($contecnico=="1")  // con tecnico SI
            $filtroTecnico = " AND cip!='' ";
        else  // con tecnico NO
            $filtroTecnico = " AND cip='' ";

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT count(*) FROM schedulle_sistemas.liquidadas_averias_provision_aver_adsl 
                WHERE ".$filtroMdf." ".$filtroTecnico." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];

    }        
    public function getDetalleLiqDiaAveriasAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT actu AS actuacion, mdf, fono AS telefono, fecreg  
                FROM schedulle_sistemas.liquidadas_averias_provision_aver_adsl 
                WHERE ".$filtroMdf." AND DATE_FORMAT(fecliq, '%Y-%m-%d')='$fecha' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;

    }   

    /*********************************/    


    /*************************************
    /* Functiones de AVERIAS Agendadas
    **************************************
     */
    
    public function getAgendasDiaAveriasTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        //$mdf = "CRU3";
        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_averias` 
                WHERE PRODUCTO='STB' AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroMdf." 
                GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    }     

    public function getAgendasDiaAveriasAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_averias` 
                WHERE PRODUCTO='ADSL' AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroMdf." 
                GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    }     

    public function getAgendasDiaAveriasCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        //$nodo = "MI";
        $listaNodos = "'".$nodo."'";
        $filtroNodos = " mdf IN (".$listaNodos.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_averias` 
                WHERE PRODUCTO NOT IN ('STB', 'ADSL') AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroNodos." 
                GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    }   

    /*************************************
    /* Functiones de PROVISION Agendadas
    **************************************
     */

    public function getAgendasDiaProvisionAdsl($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_provision` 
                WHERE PRODUCTO='ADSL' AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroMdf." 
                GROUP BY 1 ORDER BY 1; "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    } 

    public function getAgendasDiaProvisionTba($mdf, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        //$mdf = "CRU3";
        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " mdf IN (".$listaMdfs.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_provision` 
                WHERE PRODUCTO='STB' AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroMdf." 
                GROUP BY 1 ORDER BY 1; "; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    }     

    public function getAgendasDiaProvisionCatv($nodo, $fecha) {   
        // Fecha en YYYY-mm-dd
        $db = new Conexion();
        $cnx = $db->conectarBD();
        //$nodo = "MI";
        $listaNodos = "'".$nodo."'";
        $filtroNodos = " mdf IN (".$listaNodos.") ";

        $cad = "SELECT LEFT(SUBSTR(FECHA_AGENDA_FIN, 12),2) AS HORA_TURNO, COUNT(*) 
                FROM `rpt_agendadas_provision` 
                WHERE PRODUCTO NOT IN ('STB', 'ADSL') AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$fecha'
                AND ".$filtroNodos." 
                GROUP BY 1 ORDER BY 1; "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;

    }   


    // Detalle de 1 actuacion
    // AVERIAS
    /*function getActuacionPendienteDetalle_AveriasTba($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, terminal AS xterminal, borne AS xborne, averia AS xaveria, fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3
                FROM schedulle_sistemas.pen_pais_tba 
                WHERE averia='$actuacion' "; 
        //echo $cad; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    
    */

    /*function getActuacionPendienteDetalle_AveriasAdsl($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, terminal AS xterminal, borne AS xborne, averia AS xaveria, fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3
                FROM schedulle_sistemas.pen_pais_adsl 
                WHERE averia='$actuacion' "; 
        //echo $cad; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    
    */

    function getActuacionPendienteDetalle_AveriasCatv($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 'CATV' AS xnegocio, codigodelcliente AS xinscripcion, 
                codigodelcliente AS xtelefono, CONCAT(apellidopaterno, ' ', apellidomaterno, ' ', nombres) AS xcliente, 
                CONCAT(tipodevia, ' ', nombredelavia, ' ', numero, ' ', piso, ' ', interior, ' ', manzana, ' ', lote) AS xdireccion, 
                distrito AS xdistrito, indicador_vip AS xsegmento1, 
                '' AS xclase, nodo AS xmdf, troba AS xarmario, lex AS xcable, '' AS xparprimario, 
                '' AS xbloque, tap AS xterminal, borne AS xborne, codigo_req AS xaveria, 
                date_format(fecharegistro, '%d-%m-%Y %H:%i:%s') AS xfecreg, '' AS xobs,
                '' AS xarea, oficina_administrativa AS xzonal, categoria_cliente AS xsegmento2, oficina_administrativa AS xjefatura, 
                contrata AS xeecc, 
                '' AS xcod_ciudad, '' AS xsegmento3
                FROM schedulle_sistemas.`aver_pen_catv_pais`
                WHERE codigo_req='$actuacion' "; 
        //echo $cad; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }  


    /********* DETALLE INDIVIDUAL  **********/
    // PROVISION TBA
    function getActuacionPendienteDetalle_ProvisionTba($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT  NEGOCIO AS xnegocio, DATA11 AS xinscripcion, FONO AS xtelefono, CLIENTE AS xcliente, 
                DIR AS xdireccion, '' AS xdistrito, SEGMENTO AS xsegmento1, 
                '' AS xclase, MDF AS xmdf, gestel_armario AS xarmario, gestel_cable_primario AS xcable,
                gestel_par_primario AS xparprimario, 
                gestel_cable_secundario AS xbloque, gestel_terminal AS xterminal, 
                gestel_par_secundario AS xborne, DATA17 AS xactuacion, F_REG AS xfecreg, '' AS xobs,
                AREA as xarea, ZONAL AS xzonal, '' AS xsegmento2, '' AS xjefatura, GRP AS xeecc, 
                CIUDAD AS xcod_ciudad, '' AS xsegmento3,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                    CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY  

                FROM schedulle_sistemas.tmp_gaudi_tba 
                WHERE DATA17='$actuacion' "; 
        //echo "cad = ".$cad; 
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }  

    // PROVISION ADSL
    function getActuacionPendienteDetalle_ProvisionAdsl($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT  NEGOCIO AS xnegocio, DATA11 AS xinscripcion, FONO AS xtelefono, CLIENTE AS xcliente, 
                DIR AS xdireccion, '' AS xdistrito, SEGMENTO AS xsegmento1, 
                '' AS xclase, MDF AS xmdf, gestel_armario AS xarmario, gestel_cable_primario AS xcable,
                gestel_par_primario AS xparprimario, 
                gestel_cable_secundario AS xbloque, gestel_terminal AS xterminal, 
                gestel_par_secundario AS xborne, DATA17 AS xactuacion, F_REG AS xfecreg, '' AS xobs,
                AREA as xarea, ZONAL AS xzonal, '' AS xsegmento2, '' AS xjefatura, GRP AS xeecc, 
                CIUDAD AS xcod_ciudad, '' AS xsegmento3,
                IF (gestel_armario='', CONCAT(ZONAL, MDF, gestel_cable_primario,LPAD(gestel_terminal, 3, '0')), 
                    CONCAT(ZONAL, MDF, gestel_armario,LPAD(gestel_terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY  
                FROM schedulle_sistemas.tmp_gaudi_adsl
                WHERE DATA17='$actuacion' "; 

        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }     

    // PROVISION CATV
    function getActuacionPendienteDetalle_ProvisionCatv($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT  NEGOCIO AS xnegocio, DATA11 AS xinscripcion, DATA11 AS xtelefono, CLIENTE AS xcliente, 
                DIR AS xdireccion, '' AS xdistrito, 
                CASE SEGMENTO WHEN 'S' THEN 'SI-VIP' WHEN 'N' THEN 'NO-VIP' ELSE '' END AS xsegmento1, 
                '' AS xclase, MDF AS xmdf, gestel_armario AS xarmario, gestel_cable_primario AS xcable,
                gestel_par_primario AS xparprimario, 
                gestel_cable_secundario AS xbloque, gestel_terminal AS xterminal, 
                gestel_par_secundario AS xborne, codreq AS xactuacion, F_REG AS xfecreg, '' AS xobs,
                AREA as xarea, ZONAL AS xzonal, '' AS xsegmento2, '' AS xjefatura, GRP AS xeecc, 
                CIUDAD AS xcod_ciudad, '' AS xsegmento3, 
                CONCAT(cms_nodo, cms_troba, LPAD(cms_amp,2,'0'), LPAD(cms_tap,2,'0')) AS llavexy,
                (SELECT numcoo_x FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordX, 
                (SELECT numcoo_y FROM maestro_clientes.taps_xy_gis t WHERE llavexy = t.codtap ) AS coordY  
                FROM schedulle_sistemas.tmp_gaudi_catv
                WHERE codreq='$actuacion' "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }  

    /********* DETALLE INDIVIDUAL  **********/
    // AVERIAS TBA por Averia
    function getActuacionPendienteDetalle_AveriasTba($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT  negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, LPAD(terminal, 3, '0') AS xterminal, borne AS xborne, averia AS xaveria, averia as xactuacion, 
                fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario, LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				fe_inst as fechaInstalacion, reitera_30 as reiteradaLegado,
				(SELECT campo1 FROM call_101.`atenuacion_adsl_total` WHERE llave = llavexy ) AS sugerenciaCaja
                FROM schedulle_sistemas.pen_pais_tba
                WHERE averia='$actuacion' "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }        

    // AVERIAS ADSL por Averia
    function getActuacionPendienteDetalle_AveriasAdsl($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, LPAD(terminal, 3, '0') AS xterminal, borne AS xborne, 
                averia AS xaveria, averia as xactuacion,
                fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario, LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY,
				fe_inst as fechaInstalacion, reitera_30 as reiteradaLegado,
				(SELECT campo1 FROM call_101.`atenuacion_adsl_total` WHERE llave = llavexy ) AS sugerenciaCaja
                FROM schedulle_sistemas.pen_pais_adsl 
                WHERE averia='$actuacion'";

        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    } 

    // AVERIAS TBA por Telefono
    function getActuacionPendienteDetalle_AveriasTba_xFono($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if (substr($actuacion, 0, 1)=="1")  // si es de lima y le ponen 1 delante
            $actuacion = substr($actuacion, 1, 20);

        
        $cad = "SELECT  negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, LPAD(terminal, 3, '0') AS xterminal, borne AS xborne, averia AS xaveria, averia as xactuacion, 
                fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario, LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY  
                FROM schedulle_sistemas.pen_pais_tba
                WHERE telefono = '$actuacion' "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }      

    // AVERIAS ADSL por Averia
    function getActuacionPendienteDetalle_AveriasAdsl_xFono($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT negocio AS xnegocio, inscripcion AS xinscripcion, telefono AS xtelefono, cliente AS xcliente, 
                direccion AS xdireccion, des_distrito AS xdistrito, csegmento AS xsegmento1, 
                clase AS xclase, mdf AS xmdf, carmario AS xarmario, ccable AS xcable, parpri AS xparprimario, 
                cbloque AS xbloque, LPAD(terminal, 3, '0') AS xterminal, borne AS xborne, 
                averia AS xaveria, averia as xactuacion,
                fecreg AS xfecreg, obs AS xobs,
                carea as xarea, zonal AS xzonal, segmento_ AS xsegmento2, jefatura AS xjefatura, eecc AS xeecc, 
                cod_ciudad AS xcod_ciudad, csegmento_legado AS xsegmento3,
                IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                    CONCAT(zonal, mdf, carmario, LPAD(terminal, 3, '0'))) AS llavexy,
                (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordX, 
                (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy = t.mtgespktrm ) AS coordY                 
                FROM schedulle_sistemas.pen_pais_adsl 
                WHERE telefono='$actuacion'";

        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    } 

    // AVERIAS CATV por Averia
    function getActuacionPendienteDetalle_AveriasCatv_xFono($actuacion) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT 'CATV' as xnegocio, codigodelcliente as xinscripcion, codigodelcliente as xtelefono, 
                CONCAT(apellidopaterno, ' ', apellidomaterno, ' ', nombres) as xcliente,
                CONCAT(tipodevia, ' ', nombredelavia, ' ', numero, ' ', piso, ' ', interior, ' ', manzana, ' ', lote) as xdireccion,
                distrito as xdistrito, indicador_vip as xsegmento1,
                categoria_cliente as xclase, nodo as xmdf, troba as xarmario, lex as xcable, '' as xparprimario,
                '' as xbloque, tap as xterminal, borne as xborne,
                codigo_req as xaveria, codigo_req as xactuacion,
                fecharegistro as xfecreg, '' as xobs,
                estacion as xarea, oficina_administrativa as xzonal, '' as xsegmento2, '' as xjefatura, contrata as xeecc,
                departamento as xcod_ciudad, '' as xsegmento3,
                CONCAT(nodo, troba, LPAD(lex, 2, '0'), LPAD(tap, 2, '0')) AS llavexy,
                (SELECT numcoo_x FROM maestro_clientes.`taps_xy_gis` t WHERE llavexy = t.codtap ) AS coordX, 
                (SELECT numcoo_y FROM maestro_clientes.`taps_xy_gis` t WHERE llavexy = t.codtap ) AS coordY                 
                FROM  schedulle_sistemas.`aver_pen_catv_pais`
                WHERE codigodelcliente='$actuacion'";

        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    } 




    public function getAgendasDetalleAverias($mdf, $hoy, $negocio) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $listaMdfs = "'".$mdf."'";
        $filtroMdf = " MDF IN (".$listaMdfs.") ";

        
        $cad = "SELECT MDF, TELEFONO as telefono, AVERIA AS actuacion, 
                date_format(FECHA_REGISTRO_LEGADO, '%d-%m-%Y %H:%i:%s' ) as fecreg, 'Agendada' as estado1,
                
                CASE WHEN HOUR(TIMEDIFF(NOW(), FECHA_REGISTRO_LEGADO)) < 31 AND DATEDIFF(NOW(), FECHA_REGISTRO_LEGADO)>0 
                    THEN CONCAT('V:', HOUR(FECHA_REGISTRO_LEGADO) ) ELSE '' END as marca,                      
                FECHA_AGENDA_FIN AS fec_agenda,
                zonal as zonal
                FROM ".self::$BD_MAIN.".rpt_agendadas_averias a 
                WHERE ".$filtroMdf." AND PRODUCTO='$negocio' 
                AND DATE_FORMAT(FECHA_AGENDA_FIN, '%Y-%m-%d')='$hoy'
                ORDER BY FECHA_REGISTRO_LEGADO ASC "; 
        //echo $cad;
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        $i = 0;
        while ($row = mysql_fetch_array($res))
        {
            $arr[$i]["mdf"] = $row["MDF"];
            $arr[$i]["telefono"] = $row["telefono"];
            $arr[$i]["actuacion"] = $row["actuacion"];
            $arr[$i]["fecreg"] = $row["fecreg"];
            $arr[$i]["marca"] = $row["marca"];
            $arr[$i]["fec_agenda"] = $row["fec_agenda"];
            $arr[$i]["zonal"] = $row["zonal"];


            if ($negocio == 'adsl'){

                $cad2 = "SELECT mdf, carmario as armario, ccable as cable, parpri, cbloque as bloque, 
                            terminal, borne, carea as area, csegmento as xsegmento1,
                            CONCAT(mdf, ' ', carmario, ' ', ccable) as fftt,
                            IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                                CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,  
                            (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                            (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY                                              
                         FROM schedulle_sistemas.pen_pais_adsl    
                         WHERE averia='".$row["actuacion"]."' LIMIT 1";

            }
            else if ($negocio == 'tba'){
                $cad2 = "SELECT mdf, carmario as armario, ccable as cable, parpri, cbloque as bloque, 
                        terminal, borne, carea as area, csegmento as xsegmento1,
                        CONCAT(mdf, ' ', carmario, ' ', ccable) as fftt,
                        IF (carmario='', CONCAT(zonal, mdf, ccable,LPAD(terminal, 3, '0')), 
                            CONCAT(zonal, mdf, carmario,LPAD(terminal, 3, '0'))) AS llavexy,  
                        (SELECT `x` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordX,
                        (SELECT `y` FROM webunificada_fftt.`fftt_terminales` t WHERE llavexy =  t.mtgespktrm  ) AS coordY                                              
                     FROM schedulle_sistemas.pen_pais_tba
                     WHERE averia='".$row["actuacion"]."' LIMIT 1";
            }

            //echo $cad2;
            $res2 = mysql_query($cad2) or die(mysql_error()." ".$cad2);

            //var_dump($arr);
            $c = mysql_num_rows($res2);
            $row2 = mysql_fetch_array($res2); //echo "Armario = ".$row2["armario"];
            echo "Total = ".$c;
            if ($c>0) {
                $arr[$i]["armario"] = $row2["armario"];
                $arr[$i]["cable"] = $row2["cable"];
                $arr[$i]["bloque"] = $row2["bloque"];
                $arr[$i]["terminal"] = $row2["terminal"];
                $arr[$i]["borne"] = $row2["borne"];
                $arr[$i]["area"] = $row2["area"];
                $arr[$i]["xsegmento1"] = $row2["xsegmento1"];
                $arr[$i]["fftt"] = $row2["fftt"];
                $arr[$i]["llavexy"] = $row2["llavexy"];
                $arr[$i]["coordY"] = $row2["coordY"];
                $arr[$i]["estado"] = "Agendado";
            }
            else
            {
                $arr[$i]["estado"] = "<span style='color: red;'>No pendiente</span>";
            }
            $i++;       

        }
        //var_dump($arr);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }


}