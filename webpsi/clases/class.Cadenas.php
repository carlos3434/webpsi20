<?php
/**
 * User: Sergio Miranda Cornejo
 * Date: 10/01/14
 * Time: 09:55 AM
 */

class Cadenas {

    public function limpia_cadena($value)
    {
        $nopermitidos = array("'",'\\','<','>',"\"",";");
        $nueva_cadena = str_replace($nopermitidos, "", $value);
        return $nueva_cadena;
    }

    public function limpia_campo_mysql_text($text)
    {
        $text = preg_replace('/<br\\\\s*?\\/??>/i', "\\n", $text);
        return str_replace("<br />","\n",$text);
    }


    public function limpia_cadena_rf($value) {
        $input = trim( preg_replace( '/\s+/', ' ', $value ) );
        return $input;
    }

    public function multidimensionalArrayMap( $func, $arr )
    {
        $newArr = array();
        foreach( $arr as $key => $value )
        {
            $newArr[ $key ] = ( is_array( $value ) ? multidimensionalArrayMap( $func, $value ) : $func( $value ) );
        }
        return $newArr;
    }


}
