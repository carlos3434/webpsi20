<?php

include_once "class.Conexion.php";

class Perfil {
    private $id = '';
    private $id_perfil = '';
    private $estado = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getIdPerfil() {
        return $this->id_perfil;
    }

    public function setIdPerfil($x) {
        $this->id_perfil = $x;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($x) {
        $this->estado = $x;
    }


    public function ListadoPerfiles() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT id, perfil, estado FROM `tb_perfil`  ORDER BY perfil ASC;";

        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

}