<?php

include_once "class.Conexion.php";

class FacilidadesTecnicas {

    private $bdFFTT= "webpsi_fftt";
    private $bdFFTT_WU = "webunificada_fftt";

    private $tablaClientesXY = "clientes_xy_terminal";

    public function getZonales() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $bd = $this->bdFFTT;
        $i = 0;
        
        $cad = "SELECT idzonal, zonal, descripcion FROM ".$bd.".zonales WHERE activo=1 ORDER by zonal ";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[$i]['idzonal'] = $row[0];
            $arr[$i]['zonal'] = $row[1];
            $arr[$i]['descripcion'] = $row[2];
            $i++;
        }
        
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function getMdfsxZonal($zonal) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $bd = $this->bdFFTT;
        $i = 0;
        
        $cad = "SELECT MDF, NOMBRE, REGION, EECC FROM ".$bd.".`mdfs_eecc_regiones` 
                WHERE TIPO='MDF' AND ZONAL='$zonal' AND ACTIVO=1
                ORDER BY MDF ASC; ";
        $res = mysql_query($cad, $cnx); // or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[$i]['mdf'] = $row[0];
            $i++;
        }
        
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
    
   public function getTipoRed() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $bd = $this->bdFFTT;
        $i = 0;
        
        $cad = "SELECT idtipo_red, tipo_red, descripcion FROM ".$bd.".tipo_red WHERE activo=1 ORDER by tipo_red ";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[$i]['idtipo_red'] = $row[0];
            $arr[$i]['tipo_red'] = $row[1];
            $arr[$i]['descripcion'] = $row[2];
            $i++;
        }
        
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }
	

    public function getCableArmario($zonal, $mdf, $tipoRed)
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }

        $sql = "SELECT DISTINCT " . $campo . " AS cablearmario
                FROM webunificada_fftt.fftt_terminales 
                WHERE mdf = '" . $mdf . "' 
                AND tipo_red = '" . $tipoRed . "'";
        $res = mysql_query($sql); // or die(mysql_error());
        $array = array();
        $i=0;

        while ($row = mysql_fetch_row($res)) {
            $array[$i]['cable_armario'] = $row[0];
            $i++;
        }

        $cnx = NULL;
        $db = NULL;
        return $array;
    }

    public function getTerminales($zonal, $mdf, $tipoRed, $cableArmario )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }

        $sql = "SELECT zonal, mdf, cable, armario, tipo_red, `x`, `y`, direccion, qcapcaja, qparlib, 
                qparres, qdistrib, tipo_caja, caja
                FROM webunificada_fftt.fftt_terminales 
                WHERE mdf = '" . $mdf . "' 
                AND tipo_red = '" . $tipoRed . "' 
                AND " . $campo . " = '" . $cableArmario . "' 
                " . $strWhere . " ORDER BY caja";
        $res = mysql_query($sql); // or die(mysql_error());
        $array = array();
        $i=0;

        while ($row = mysql_fetch_row($res)) {
            $array[$i]['zonal'] = $row[0];
            $array[$i]['mdf'] = $row[1];
            $array[$i]['cable'] = $row[2];
            $array[$i]['armario'] = $row[3];            
            $array[$i]['tipo_red'] = $row[4];            
            $array[$i]['x'] = $row[5];      
            $array[$i]['y'] = $row[6];    
            $array[$i]['direccion'] = $row[7];       
            $array[$i]['qcapcaja'] = $row[8];  
            $array[$i]['qparlib'] = $row[9];     
            $array[$i]['qparres'] = $row[10];     
            $array[$i]['qdistrib'] = $row[11];     
            $array[$i]['tipo_caja'] = $row[12];     
            $array[$i]['caja'] = $row[13]; 
            $i++;
        }        

        $cnx = NULL;
        $db = NULL;
        return $array;

    }


    public function getTerminalXY($zonal, $mdf, $tipoRed, $cableArmario, $cajaTerminal )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }

        $sql = "SELECT zonal, mdf, cable, armario, tipo_red, `x`, `y`, direccion, qcapcaja, qparlib, 
                qparres, qdistrib, tipo_caja, caja
                FROM webunificada_fftt.fftt_terminales 
                WHERE mdf = '" . $mdf . "' 
                AND tipo_red = '" . $tipoRed . "' 
                AND " . $campo . " = '" . $cableArmario . "' 
                " . $strWhere . " AND caja='".$cajaTerminal."' ORDER BY caja LIMIT 1 ";
        //echo $sql;
        $res = mysql_query($sql) ; //or die(mysql_error());
        $array = array();
        $i=0;
        while ($row = mysql_fetch_array($res)) {
            $array[$i]['X'] = $row['x'];
            $array[$i]['Y'] = $row['y'];
            $i++;
        }        

        $cnx = NULL;
        $db = NULL;
        return $array;

    }


    public function getTapXY($nodo, $troba, $tap )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();


        $sql = "SELECT codtap, mtcodnod as nodo, CONCAT(mttiptro,mttroncal) as troba, 
                mtextlin as amplificador, mttap as tap, 
                CONCAT(mttipvia, ' ', mtdesdir, ' ', mtnumero, ' ', mtpiso, ' ', mtint, ' ', mtmanzan, ' ', mtlote) as direccion, 
                '' as qcapcaja, '' as qparlib, '' as qparres, '' as qdistrib, numcoo_x as tap_x, numcoo_y as tap_y
                FROM  maestro_clientes.taps_xy_gis
                WHERE mtcodnod = '" . $nodo . "' 
                AND mtnumpla = '" . $troba . "' 
                AND mttap='".$tap."' ORDER BY mttap LImiT 1 ";
        //echo $sql;
        $res = mysql_query($sql) or die(mysql_error());
        $array = array();
        $i=0;
        while ($row = mysql_fetch_array($res)) {
            $array[$i]['X'] = $row['tap_x'];
            $array[$i]['Y'] = $row['tap_y'];
            $i++;
        }        

        $cnx = NULL;
        $db = NULL;
        return $array;

    }    

    public function getTerminalDatos($zonal, $mdf, $tipoRed, $cableArmario, $cajaTerminal )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }

        $sql = "SELECT zonal, mdf, cable, armario, tipo_red, `x`, `y`, direccion, qcapcaja, qparlib, 
                qparres, qdistrib, tipo_caja, caja
                FROM webunificada_fftt.fftt_terminales 
                WHERE mdf = '" . $mdf . "' 
                AND tipo_red = '" . $tipoRed . "' 
                AND " . $campo . " = '" . $cableArmario . "' 
                " . $strWhere . " AND caja='".$cajaTerminal."' ORDER BY caja LIMIT 1 ";
        //echo $sql;
        $res = mysql_query($sql) ; //or die(mysql_error());
        $array = array();
        $i=0;
        while ($row = mysql_fetch_array($res)) {
            $array[$i]['X'] = $row['x'];
            $array[$i]['Y'] = $row['y'];
            $array[$i]['direccion'] = $row['direccion'];
            $array[$i]['qcapcaja'] = $row['qcapcaja'];

            $i++;
        }        

        $cnx = NULL;
        $db = NULL;
        return $array;

    }

    public function getClientesPorTerminal($zonal, $mdf, $tipo_red, $cable_armario, $caja )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $strWhere .= " AND cli.Caja = '" . $caja . "' ";

        $order = " 1 ";

        $sql = "SELECT cli.cliente, cli.direccion, cli.inscripcion, 
                       cli.`cable-armario` as armario_cable, cli.caja, cli.tipo  ";
        $sql .= " FROM webunificada_fftt.clientes_xy_terminal cli 
                WHERE cli.zonalgestel = '" . $zonal . "' AND cli.mdf = '" . $mdf . "' AND cli.`cable-armario` = '" . $cable_armario . "' 
                AND cli.caja = '".$caja."' ORDER BY " . $order;
        //echo $sql;
        $array = array();
        $i=0;
        $res = mysql_query($sql) ; //or die(mysql_error());

        while ($row = mysql_fetch_row($res)) {
            $array[$i]['cliente'] = $row[0];
            $array[$i]['direccion'] = $row[1];
            $array[$i]['inscripcion'] = $row[2];
            $array[$i]['zonal'] = $zonal;
            $i++;
        }

        $cnx = NULL;
        $db = NULL;
        return $array;

    }


    public function updateClienteXY($inscripcion, $zonal, $coordX, $coordY) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $bd = $this->bdFFTT_WU;
        $tabla = $this->tablaClientesXY;

        try {
            $coordX = trim($coordX);
            $coordY = trim($coordY);
            
            $cnx->beginTransaction();   
            $cad = "UPDATE  ".$bd.".".$tabla." 
                    SET x_cliente = :cx, y_cliente = :cy
                    WHERE inscripcion=:inscripcion and zonalgestel=:zonal LIMIT 1 ;";

            //echo $coordX. " - ".$coordY;
            $res = $cnx->prepare($cad);
            $res->bindParam(":cx", $coordX);
            $res->bindParam(":cy", $coordY);
            $res->bindParam(":inscripcion", $inscripcion);
            $res->bindParam(":zonal", $zonal );
            $res->execute();

            $cnx->commit();
            return "1";
        }
        catch (PDOException $e)
        {
            $cnx->rollBack();
            print ("Transaction failed: " . $e->getMessage () . "\n");
            return "0";
        }

        $cnx = NULL;
        $db = NULL;
        return $arr;
    }



    public function getTerminalCercanosXY($zonal, $mdf, $coordX, $coordY, $minDistancia, $maxDistancia, $maxElementos )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }


        $sql = "SELECT mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, direccion, qcapcaja, qparlib, qparres, qdistrib, 
                (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180) AS distancia, b.campo1 AS sugerencia,
                b.tipoDslam_AO as tipoDslam, mtgespktrm as llave, `x`, `y`, campo1 as SugerenciaCaja
                FROM webunificada_fftt.fftt_terminales a LEFT JOIN call_101.`atenuacion_adsl_total` b
                ON b.llave = a.mtgespktrm
                WHERE mdf = '$mdf'  
                AND (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180 ) <= $maxDistancia 
                AND (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180 ) >= $minDistancia
                ORDER by distancia LIMIT $maxElementos ";

        //echo "<b>".$sql."</b>";
        
        $res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      

        $cnx = NULL;
        $db = NULL;
        return $array;

    }    

    public function getTerminalCercanosXY_porXY($coordX, $coordY, $minDistancia, $maxDistancia, $maxElementos )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        if ($tipoRed == 'D') {
            $campo = " cable ";
        } elseif ($tipoRed == 'F') {
            $campo = " armario ";
        }


        $sql = "SELECT mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, direccion, qcapcaja, qparlib, qparres, qdistrib, 
                (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180) AS distancia, b.campo1 AS sugerencia,
                b.tipoDslam_AO as tipoDslam, mtgespktrm as llave, `x`, `y`
                FROM webunificada_fftt.fftt_terminales a LEFT JOIN call_101.`atenuacion_adsl_total` b
                ON b.llave = a.mtgespktrm
                WHERE 
                (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180 ) <= $maxDistancia 
                AND (  SQRT( ( POW(($coordX-`x`),2 ) +  POW(($coordY-`y`),2) )  ) * 111180 ) >= $minDistancia
                ORDER by distancia LIMIT $maxElementos ";

        //echo "<b>".$sql."</b>";
        
        $res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      

        $cnx = NULL;
        $db = NULL;
        return $array;

    }  

    public function getClientesPorTerminal2($zonal, $mdf, $caja, $cable_armario, $tipored) 
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        //echo $tipored;
        if ($tipored == 'D') {

            $sql = "SELECT ParAlimentador AS par, EstadoPar AS estado1, DescEstadoPar AS estado2, Telefono AS telefono,
					descripcionModalidadGestel as modalidad, Negocio as negocio
                    FROM webunificada_fftt.`fftt_directa` 
                    where MDF='$mdf' AND Caja='$caja' AND Cable='$cable_armario' ORDER BY ParAlimentador ASC";
            //echo $sql;
        }
        else {
            $sql = "SELECT Pardistribuidor AS par, EstadoPar AS estado1, DescEstadoPar AS estado2 , Telefono as telefono,
					descripcionModalidadGestel as modalidad, Negocio as negocio
					FROM webunificada_fftt.`fftt_secundaria` 
                    WHERE Caja='$caja' AND MDF='$mdf' AND Armario='$cable_armario' ORDER BY Pardistribuidor ";   
        }
        
        //echo $sql;
        $res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }

        $cnx = NULL;
        $db = NULL;
        return $array;

    }    



    public function getTapsCercanosXY($coordX, $coordY, $minDistancia, $maxDistancia, $maxElementos )
    {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $sql = "SELECT codtap as mtgespktrm, mtcodnod as nodo, CONCAT(mttiptro,mttroncal) as troba, 
                mtextlin as amplificador, mttap as tap, 
                CONCAT(mttipvia, ' ', mtdesdir, ' ', mtnumero, ' ', mtpiso, ' ', mtint, ' ', mtmanzan, ' ', mtlote) as direccion, 
                '' as qcapcaja, '' as qparlib, '' as qparres, '' as qdistrib, 
                (  SQRT( ( POW(($coordX-`numcoo_x`),2 ) +  POW(($coordY-`numcoo_y`),2) )  ) * 111180) AS distancia, 
                codtap as llave, numcoo_x as x, numcoo_y as y
                FROM maestro_clientes.taps_xy_gis a 
                WHERE 
                (  SQRT( ( POW(($coordX-`numcoo_x`),2 ) +  POW(($coordY-`numcoo_y`),2) )  ) * 111180 ) <= $maxDistancia 
                AND (  SQRT( ( POW(($coordX-`numcoo_x`),2 ) +  POW(($coordY-`numcoo_y`),2) )  ) * 111180 ) >= $minDistancia
                ORDER by distancia LIMIT $maxElementos ";

        //echo "<b>".$sql."</b>";
        
        $res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      

        $cnx = NULL;
        $db = NULL;
        return $array;

    }   





	
}