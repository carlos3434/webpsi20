<?php

include_once "class.Conexion.php";

class FibraGestion {

    private $BD = "webpsi";

    public function listarMotivosArea($idArea, $idAreaEstadoOrigen) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        global $BD;
        
        //echo "Id area = ".$idArea." , idMotivo = ".$idMotivo." , idAreaEstadoOrigen = ".$idAreaEstadoOrigen;

        $cad = "SELECT DISTINCT b.`idmotivo`, m.`motivo`
                FROM $BD.`fibrages_area_motivo` a, $BD.`fibrages_submotivos` b, $BD.`fibrages_motivos` m
        WHERE a.`idarea_origen` = :idarea AND a.idarea_estado_origen = :idareaEstadoOrigen
        AND m.`idmotivo` = b.`idmotivo`
        AND a.`idsubmotivo`=b.`idsubmotivo` 
        AND b.`activo`=1 AND a.`activo`=1 AND m.`activo`=1; ";

        //echo $cad;
        $arr = array();
        $res = $cnx->prepare($cad);
        $res->bindParam(":idarea", $idArea);
        $res->bindParam(":idareaEstadoOrigen", $idAreaEstadoOrigen);
        
        $res->execute();
        while ($row = $res->fetch()) {                
            $arr[] = $row;
        }
        
        $res = null;
        $cnx = null;
                
        return $arr;
    }

    public function listarSubMotivosArea($idArea, $idMotivo, $idAreaEstadoOrigen) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        global $BD;
        
        //echo "Id area = ".$idArea." , idMotivo = ".$idMotivo." , idAreaEstadoOrigen = ".$idAreaEstadoOrigen;
        
        $cad = "SELECT b.`idsubmotivo`, b.`submotivo`
        FROM $BD.`fibrages_area_motivo` a, $BD.`fibrages_submotivos` b
        WHERE a.`idarea_origen` = :idarea AND b.idmotivo = :idmotivo 
        AND a.idarea_estado_origen = :idareaEstadoOrigen
        AND a.`idsubmotivo`=b.`idsubmotivo` AND b.`activo`=1 AND a.`activo`=1;";

        //echo $cad;
        $arr = array();
        $res = $cnx->prepare($cad);
        $res->bindParam(":idarea", $idArea);
        $res->bindParam(":idmotivo", $idMotivo);
        $res->bindParam(":idareaEstadoOrigen", $idAreaEstadoOrigen);
        $res->execute();
        while ($row = $res->fetch()) {                
            $arr[] = $row;
        }
        
        $res = null;
        $cnx = null;
                
        return $arr;
    }

	
    public function listarEstados() {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        global $BD;
        
        $cad = "SELECT id_fibrages_estado, estado
        FROM $BD.`fibrages_estados` WHERE `activo`=1;";

        $arr = array();
        $res = $cnx->query($cad);
        $res->execute();
        while ($row = $res->fetch()) {                
            $arr[] = $row;
        }
        
        $res = null;
        $cnx = null;
                
        return $arr;
    }


	

    public function registroActuacion($datos) {

        try {

            $db = new Conexion();
            $cnx = $db->conectarPDO();
            $cnx->beginTransaction();

            global $BD;

            $cad1 = "INSERT INTO ".$BD.".fibrages_cabecera (peticion, inscripcion, telefono, nombre_cliente,
                    direccion, zonal, nombre_contacto, telef1_contacto, telef2_contacto,
                    dni_contacto,  nombre_gestor, rpm_gestor, idestado, idarea_estado, ideecc )
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
            $res1 = $cnx->prepare($cad1);

            extract($datos);

            $fechaMov = date("Y-m-d H:i:s");
            $area_estado = "1";


            $idAreaOrigen = 1 ; // Comercial
            $idAreaMotivo = 1;  
            $idAreaDestino = 2;  // Destino
            $idEstado = 1; // Pre Agendado
            $idAreaEstado = 1; // Asignaciones Pre-Agendado
            
            /*$arr = array("peticion", "inscripcion", "telefono", "cliente", "direccion", "zonal", "contacto_nombre",
                "contacto_fono1", "contacto_fono2", "contacto_dni", "gestor_nombre",
                "gestor_rpm", "observaciones", "eecc", "fecha_agenda", "agenda_idturno",
                "idusuario" );
            */

            $res1->bindParam(1, $peticion);
            $res1->bindParam(2, $inscripcion);
            $res1->bindParam(3, $telefono);
            $res1->bindParam(4, $cliente);
            $res1->bindParam(5, $direccion);
            $res1->bindParam(6, $zonal);
            $res1->bindParam(7, $contacto_nombre);
            $res1->bindParam(8, $contacto_fono1);
            $res1->bindParam(9, $contacto_fono2);
            $res1->bindParam(10, $contacto_dni);
            $res1->bindParam(11, $gestor_nombre);
            $res1->bindParam(12, $gestor_rpm);

            $res1->bindParam(13, $idEstado);
            $res1->bindParam(14, $idAreaEstado);
            $res1->bindParam(15, $eecc);

            $res1->execute();

            $IDactuacion = $cnx->lastInsertId();
            
            $codActuacion = "ATF-".date("Y")."-".str_pad($IDactuacion, 5, "0", STR_PAD_LEFT);
            $cad2 = "UPDATE ".$BD.".fibrages_cabecera SET cod_actuacion=?
                        WHERE id_fibrages=?";
            $res2 = $cnx->prepare($cad2);
            $res2->bindParam(1, $codActuacion);
            $res2->bindParam(2, $IDactuacion);
            $res2->execute();


            $cad3 = "INSERT INTO ".$BD.".fibrages_detalle (id_fibrages, fecha_mov, idusuario,
                    estado_area, idarea_estado, ideecc, fecha_agenda, idturno_agenda, observaciones,
                    idarea_origen, idarea_destino, idarea_motivo )
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";

            $res3 = $cnx->prepare($cad3);
            $res3->bindParam(1, $IDactuacion);
            $res3->bindParam(2, $fechaMov );
            $res3->bindParam(3, $idusuario);
            $res3->bindParam(4, $idEstado);
            $res3->bindParam(5, $idAreaEstado);
            $res3->bindParam(6, $eecc);
            $res3->bindParam(7, $fecha_agenda);
            $res3->bindParam(8, $agenda_idturno);
            $res3->bindParam(9, $observaciones);

            
            $res3->bindParam(10, $idAreaOrigen);
            $res3->bindParam(11, $idAreaDestino);
            $res3->bindParam(12, $idAreaMotivo);
            
            $res3->execute();
            $cnx->commit();

            return "1|".$codActuacion;
        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            $cnx->rollback();                   # failure
            return "0|".$e->getMessage() ;
        }
        $cnx = null;
        $db = null;
    }




    public function getData_AreaMotivo($idSubMotivo, $idAreaOrigen) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT a.`idsubmotivo`, a.`idarea_destino`, a.`idestado`, 
                    a.`idarea_motivo`, a.`idarea_estado`
                    FROM `fibrages_area_motivo` a
                    WHERE a.`idsubmotivo`= :idsubmotivo AND a.`idarea_origen` = :idarea_origen LIMIT 1;";
                    
            
            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idsubmotivo", $idSubMotivo);
            $res->bindParam(":idarea_origen", $idAreaOrigen);
            $res->execute();

            $row = $res->fetchAll();
            $arr["idarea_destino"] = $row[0]["idarea_destino"];
            $arr["idarea_motivo"] = $row[0]["idarea_motivo"];
            $arr["idestado"] = $row[0]["idestado"];
            $arr["idarea_estado"] = $row[0]["idarea_estado"];
            
            return $arr;


        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }


    public function getData_Actual($idFibraGes) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT  ideecc, `fecha_agenda`, `idturno_agenda`
                    FROM `fibrages_detalle` 
                    WHERE `id_fibrages`=:idfibra_ges 
                    ORDER BY `fecha_mov` DESC LIMIT 1;";
                    
            
            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idfibra_ges", $idFibraGes);
            $res->execute();

            $row = $res->fetchAll();
            $arr["ideecc"] = $row[0]["ideecc"];
            $arr["fecha_agenda"] = $row[0]["fecha_agenda"];
            $arr["idturno_agenda"] = $row[0]["idturno_agenda"];
            
            return $arr;


        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }

	

    public function getData_UltMov($idFibraGes) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT b.*,  
                u.usuario, fba.area, fbe.estado, e.eecc, b.fecha_agenda,
                  b.idturno_agenda, t.`turno`
                FROM
                  `fibrages_detalle` b, `fibrages_area_estados` c, `tb_usuario` u, `tb_eecc` e,
                  `fibrages_estados` fbe, `fibrages_areas` fba, `turnos` t
                WHERE b.`id_fibrages` = :idfibra_ges 
                    AND b.idarea_estado=c.id
                    AND u.id=b.idusuario AND e.id=b.ideecc
                    AND fbe.id_fibrages_estado=c.idestado
                    AND fba.id_fibrages_areas=c.idarea
					AND t.`idturno`=b.`idturno_agenda`
                ORDER BY `fecha_mov` DESC 
                LIMIT 1 ";

            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idfibra_ges", $idFibraGes);
            $res->execute();

            while ($row = $res->fetch()) {
                $arr[] = $row;
            }
                        
            return $arr;


        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }

	public function getData_FirstMov($idFibraGes) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT b.*,  
                u.usuario, fba.area, fbe.estado, e.eecc, b.fecha_agenda,
                  b.idturno_agenda
                FROM
                  `fibrages_detalle` b, `fibrages_area_estados` c, `tb_usuario` u, `tb_eecc` e,
                  `fibrages_estados` fbe, `fibrages_areas` fba
                WHERE b.`id_fibrages` = :idfibra_ges 
                    AND b.idarea_estado=c.id
                    AND u.id=b.idusuario AND e.id=b.ideecc
                    AND fbe.id_fibrages_estado=c.idestado
                    AND fba.id_fibrages_areas=c.idarea
                ORDER BY `fecha_mov` ASC 
                LIMIT 1 ";

            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idfibra_ges", $idFibraGes);
            $res->execute();

            while ($row = $res->fetch()) {
                $arr[] = $row;
            }
                        
            return $arr;


        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }
	
	
	public function getDescTurno($idTurno) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT turno
                FROM
                  $BD.turnos 
                WHERE  idturno = :idturno  ";
				

            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idturno", $idTurno);
            $res->execute();
			
            $row = $res->fetchAll();

            return $row[0]["turno"];

        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }


	public function getDataUsuario($idUsuario) {

        try {
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT *
                FROM
                  $BD.tb_usuario 
                WHERE  id = :id  ";
				

            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":id", $idUsuario);
            $res->execute();
			
            while ($row = $res->fetch()) {
				$arr[] = $row;
			}

            return $arr;

        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }
	
	
    public function isAgendable_AreaMotivo($idAreaMotivo) {

        try {
            global $BD;
            $db = new Conexion();
            $cnx = $db->conectarPDO();
        
            $cad = "SELECT mantener_agenda
                FROM
                  ".$BD.".fibrages_area_motivo 
                WHERE  idarea_motivo = :idarea_motivo LIMIT 1 ";
            $arr = array();
            $res = $cnx->prepare($cad);
            $res->bindParam(":idarea_motivo", $idAreaMotivo);
            $res->execute();

            $row = $res->fetchAll();
            //echo $idAreaMotivo." - ".$row[0]["mantener_agenda"]."<br/>";
            
            return $row[0]["mantener_agenda"];

        }
        catch (PDOException $e)
        {
            print ("Transaction failed: " . $e->getMessage () . "\n");
            //return "0|".$e->getMessage() ;
            return "0";
        }

        
        $res = null;
        $cnx = null;
    }




    public function registroMovimiento($datos) {

        try {

            $db = new Conexion();
            $cnx = $db->conectarPDO();
            $cnx->beginTransaction();

            global $BD;

            extract($datos);

            //print_r($datos);
            $fechaMov = date("Y-m-d H:i:s");

            if ( $this->isAgendable_AreaMotivo($idAreaMotivo)=="0" ) {
                $fechaAgenda = "";
                $idTurnoAgenda = 6 ;  // No Agendado
            }

            //echo "FLAG AGENDABLE para idareamotivo ($idAreaMotivo) = ".$this->isAgendable_AreaMotivo($idAreaMotivo); die();

            $cad3 = "INSERT INTO ".$BD.".fibrages_detalle (id_fibrages, fecha_mov, idusuario,
                    estado_area, idarea_estado, ideecc, fecha_agenda, idturno_agenda, observaciones,
                    idarea_origen, idarea_destino, idarea_motivo )
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";

            $res3 = $cnx->prepare($cad3);
            $res3->bindParam(1, $idFibraGes);
            $res3->bindParam(2, $fechaMov );
            $res3->bindParam(3, $idusuario);
            $res3->bindParam(4, $idEstado);
            $res3->bindParam(5, $idAreaEstado);
            $res3->bindParam(6, $idEecc);
            $res3->bindParam(7, $fechaAgenda);
            $res3->bindParam(8, $idTurnoAgenda);
            $res3->bindParam(9, $observaciones);
            
            $res3->bindParam(10, $idAreaOrigen);
            $res3->bindParam(11, $idAreaDestino);
            $res3->bindParam(12, $idAreaMotivo);
            
            $res3->execute();
            
            $IDactuacion_DET = $cnx->lastInsertId();
            
            $cad4 = "UPDATE $BD.fibrages_cabecera SET idestado=:idestado, idarea_estado=:idarea_estado
                    WHERE id_fibrages = :idfibra_ges ";
            $res4 = $cnx->prepare($cad4);
            
            $res4->bindParam(":idestado", $idEstado);
            $res4->bindParam(":idarea_estado", $idAreaEstado);
            $res4->bindParam(":idfibra_ges", $idFibraGes);
            $res4->execute();
            
            
            $codigoSisego = "";
            $cad5 = "INSERT INTO $BD.fibrages_fftt (id_fibrages_det, fecha_mov, codigo_sisego, nodo, odf,
                    cable, nap, pelo ) 
                    VALUES (?,?,?,?,?,?,?,? ); ";
            $res5 = $cnx->prepare($cad5);
            $res5->bindParam(1, $IDactuacion_DET);
            $res5->bindParam(2, $fechaMov);
            $res5->bindParam(3, $codigoSisego);
            $res5->bindParam(4, $Nodo);
            $res5->bindParam(5, $Odf);
            $res5->bindParam(6, $Cable);
            $res5->bindParam(7, $Nap);
            $res5->bindParam(8, $Pelo);
            
            $res5->execute();
            
            $cnx->commit();

            return "1" ;
        }
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            $cnx->rollback();                   # failure
            return "0";
            //return "0|".$e->getMessage() ;
        }
        $cnx = null;
        $db = null;
    }





    public function getDatosActuacion($id) {
        $db = new Conexion();
        $cnx = $db->conectarBD_mysqli();

        global $BD;

        $cad = "SELECT a.id_fibrages, a.cod_actuacion, a.peticion, a.inscripcion, a.telefono, a.nombre_cliente,
          a.direccion, a.zonal, a.nombre_contacto, a.telef1_contacto, a.telef2_contacto, a.dni_contacto,
          a.nombre_gestor, a.rpm_gestor,
          b.fecha_mov, u.usuario, fba.area, fbe.estado, e.eecc, b.fecha_agenda,
          b.idturno_agenda, b.observaciones
        FROM `fibrages_cabecera` a, `fibrages_detalle` b, `fibrages_area_estados` c, `tb_usuario` u, `tb_eecc` e,
          `fibrages_estados` fbe, `fibrages_areas` fba
        WHERE a.id_fibrages=b.id_fibrages AND a.id_fibrages=$id
            AND b.estado_area=c.id
            AND u.id=b.idusuario AND e.id=b.ideecc
            AND fbe.id_fibrages_estado=c.idestado
            AND fba.id_fibrages_areas=c.idarea
        ORDER BY b.fecha_mov DESC ";

        $arr = array();
        if ($res = $cnx->query($cad)) {
            while ($row = $res->fetch_assoc()) {
                $arr[] = $row;
            }
            $res->free();
        }
        return $arr;
    }

    public function getDatosActuacionMovimientos($id) {
        $db = new Conexion();
        $cnx = $db->conectarBD_mysqli();

        global $BD;

        //echo "ID = ".$id;
        $cad = "SELECT DATE_FORMAT(b.fecha_mov, '%d-%m-%Y %H:%i:%s') as fecha_mov,
                u.usuario, fba.area, fbe.estado, e.eecc,
                DATE_FORMAT(b.fecha_agenda, '%d-%m-%Y') as fecha_agenda,
                b.idturno_agenda,
                t.turno, b.observaciones
              FROM `fibrages_detalle` b, `fibrages_area_estados` c, `tb_usuario` u, `tb_eecc` e,
                `fibrages_estados` fbe, `fibrages_areas` fba, `turnos` t
              WHERE b.id_fibrages=$id
                AND b.idarea_estado=c.id
                AND u.id=b.idusuario AND e.id=b.ideecc
                AND fbe.id_fibrages_estado=c.idestado
                AND fba.id_fibrages_areas=c.idarea
                AND b.idturno_agenda = t.idturno
              ORDER BY b.fecha_mov DESC ";
        //echo $cad;
        $arr = array();
        if ($res = $cnx->query($cad)) {
            while ($row = $res->fetch_assoc()) {
                $arr[] = $row;
            }
            $res->free();
        }
        return $arr;
    }


}

?>