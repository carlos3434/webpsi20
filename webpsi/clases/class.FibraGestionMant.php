<?php

include_once "class.Conexion.php";

class FibraGestionMant {

    private $BD = "webpsi";

    public function getIdAreaMotivo($idAreaDestino, $idEstadoDestino ) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        global $BD;
        
        //echo "Id areaDestino = ".$idAreaDestino.", idEstadoDestino = ".$idEstadoDestino;

        $cad = "SELECT a.`id`
				FROM `fibrages_area_estados` a 
				WHERE idarea=? 
				AND idestado=? 
				AND activo=1 LIMIT 1; ";

        $arr = array();
        $res = $cnx->prepare($cad);
        $res->bindParam("1", $idAreaDestino);
		$res->bindParam("2", $idEstadoDestino);
        $res->execute();
        while ($row = $res->fetch(PDO::FETCH_ASSOC)) {                
            $arr[] = $row;
        }
        
        $res = null;
        $cnx = null;
                
        return $arr[0]["id"];
    }
	
	public function listarSubMotivosSimple($idMotivo) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        global $BD;
        
       
        $cad = "SELECT idsubmotivo, idmotivo, submotivo, activo
        FROM $BD.`fibrages_submotivos` 
        WHERE idmotivo = :idmotivo 
		AND `activo`=1 ORDER BY submotivo ASC ";

        $arr = array();
        $res = $cnx->prepare($cad);
        $res->bindParam(":idmotivo", $idMotivo);
        $res->execute();
        while ($row = $res->fetch()) {                
            $arr[] = $row;
        }
        
        $res = null;
        $cnx = null;
                
        return $arr;
    }	
	
	function insertAreaMotivo($idSubmotivo, $idAreaOrigen, $idAreaDestino, $idEstado, $idAreaEstadoOrigen, $idAreaEstadoDestino,
		$esAgenda, $mantenerAgenda, $activo) {
       
        try {

            $db = new Conexion();
            $cnx = $db->conectarPDO();
            $cnx->beginTransaction();

			global $BD;

			$cad = "INSERT INTO ".$BD.".fibrages_area_motivo ( idsubmotivo, idarea_origen, idarea_destino, idestado,
					idarea_estado_origen, idarea_estado, es_agenda, mantener_agenda, activo )
					VALUES (?,?,?,?,?,?,?,?,?)";

            $res1 = $cnx->prepare($cad);
			$res1->bindParam(1, $idSubmotivo);
            $res1->bindParam(2, $idAreaOrigen);
            $res1->bindParam(3, $idAreaDestino );
            $res1->bindParam(4, $idEstado);
            $res1->bindParam(5, $idAreaEstadoOrigen);
            $res1->bindParam(6, $idAreaEstadoDestino);
            $res1->bindParam(7, $esAgenda);
            $res1->bindParam(8, $mantenerAgenda);
			$res1->bindParam(9, $activo);
			
			$res1->execute();
			
			$cnx->commit();
			
			return "1|";

		}
        catch (PDOException $e)
        {
            //print ("Transaction failed: " . $e->getMessage () . "\n");
            $cnx->rollback();                   # failure
            return "0|".$e->getMessage() ;
        }
        $cnx = null;
        $db = null;
		
	}


}

?>