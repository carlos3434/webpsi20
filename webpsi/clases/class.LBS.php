<?php

include_once "class.Conexion.php";

class LBS {

	private $bdFFTT= "webpsi_fftt";

    private $tablaClientesXY = "clientes_xy_terminal";

    public function getTecnicosCercanos($coordX, $coordY, $minDistancia, $maxDistancia) {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

        /*
        $sql="SELECT celular,nombre,lugar,fecha,lon_x,lat_y FROM webpsi_fftt.tecnicos_xy 
				WHERE
				(  SQRT( ( POW(($coordX-lon_x),2 ) +  POW(($coordY-lat_y),2) )  ) * 111180 ) <= $maxDistancia
				AND (  SQRT( ( POW(($coordX-lon_x),2 ) +  POW(($coordY-lat_y),2) )  ) * 111180 ) >= $minDistancia
				GROUP BY 1 ORDER BY 4 DESC ";
        */
        $sql="SELECT lbs_ws_seg.celular,CONCAT(tb_persona.nombre,' ',tb_persona.apellido_p,' ',tb_persona.apellido_m) AS nombre,
                    lbs_ws_seg.direccion AS lugar,CONCAT(lbs_ws_seg.fechaWS,' ',lbs_ws_seg.horaWS) AS fecha,
                    lbs_ws_seg.longitud AS lon_x,lbs_ws_seg.latitud AS lat_y FROM webpsi.lbs_ws_seg 
                LEFT JOIN webpsi.tb_persona_contacto ON lbs_ws_seg.idpersona_contacto=tb_persona_contacto.id_persona
                LEFT JOIN webpsi.tb_persona ON tb_persona_contacto.id_persona=tb_persona.id
                WHERE
                (  SQRT( ( POW(($coordX-lbs_ws_seg.longitud),2 ) +  POW(($coordY-lbs_ws_seg.latitud),2) )  ) * 111180 ) <= $maxDistancia
                AND (  SQRT( ( POW(($coordX-lbs_ws_seg.longitud),2 ) +  POW(($coordY-lbs_ws_seg.latitud),2) )  ) * 111180 ) >= $minDistancia
                AND lbs_ws_seg.fechaWS=DATE_FORMAT(NOW(),'%Y-%m-%d')
                GROUP BY 1 ORDER BY 3,4 DESC ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }

}