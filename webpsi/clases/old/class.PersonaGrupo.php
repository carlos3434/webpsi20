<?php

include_once "class.Conexion.php";

class PersonaGrupo {
    private $id = '';
    private $id_persona = '';
    private $id_grupo = '';
    private $estado = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getIdPersona() {
        return $this->id_persona;
    }

    public function setIdPersona($x) {
        $this->id_persona = $x;
    }

    public function getIdGrupo() {
        return $this->id_grupo;
    }

    public function setIdGrupo($x) {
        $this->id_grupo = $x;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($x) {
        $this->estado = $x;
    }


    public function ListadoPersonaGrupo($idGrupo) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT  pg.id, pg.id_persona, pg.id_grupo, pg.estado, 
                p.`dni`, p.`id_eecc`, p.`nombre`, p.`apellido_p`, p.`apellido_m`, p.`dni`, e.`eecc`
                FROM `tb_persona_grupo` pg, `tb_persona` p, `tb_eecc` e
                WHERE p.`id`=pg.`id_persona` AND pg.id_grupo=$idGrupo AND e.`id`=p.`id_eecc`
                AND p.estado=1 AND pg.estado=1
                ORDER BY p.`apellido_p` ASC; ";
        
        //echo $cad;

        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }


	
}