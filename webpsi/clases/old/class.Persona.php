<?php

include_once "class.Conexion.php";

class Persona {
    private $id = '';
    private $Nombre = '';
    private $Apellido_P = '';
    private $Apellido_M = '';
    private $Dni = '';
    private $Id_EECC = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function setNombre($x) {
        $this->Nombre = $x;
    }

    public function getApellido_P() {
        return $this->Apellido_P;
    }

    public function setApellido_P($x) {
        $this->Apellido_P = $x;
    }

    public function getApellido_M() {
        return $this->Apellido_M;
    }

    public function setApellido_M($x) {
        $this->Apellido_M = $x;
    }
    
    public function getDni() {
        return $this->Dni;
    }

    public function setDni($x) {
        $this->Dni = $x;
    }
    
    public function getId_EECC() {
        return $this->Id_EECC;
    }

    public function setId_EECC($x) {
        $this->Id_EECC = $x;
    }

    public function LlenarDatosPersona() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        $idPersona = $this->id;
        
        $cad = "SELECT * FROM `tb_persona` WHERE id=$idPersona";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $this->Apellido_M = $row["apellido_m"];
            $this->Apellido_P = $row["apellido_p"];
            $this->Nombre = $row["nombre"];
            $this->Dni = $row["dni"];
            $this->Id_EECC = $row["id_eecc"];
            //$arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    
    public function BuscarCelular($idPersona) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT contacto FROM `tb_persona_contacto` 
                WHERE id_tipo_contacto=1 and id_persona=$idPersona 
                AND defecto=1 LIMIT 1;";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_array($res);
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        //echo "celu=".$arr["contacto"];
        return $row["contacto"];
    }

	
}