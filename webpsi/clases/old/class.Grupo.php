<?php

include_once "class.Conexion.php";

class Grupo {
    private $id = '';
    private $id_persona = '';
    private $id_grupo = '';
    private $estado = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getIdPersona() {
        return $this->id_persona;
    }

    public function setIdPersona($x) {
        $this->id_persona = $x;
    }

    public function getIdGrupo() {
        return $this->id_grupo;
    }

    public function setIdGrupo($x) {
        $this->id_grupo = $x;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($x) {
        $this->estado = $x;
    }


    public function ListadoGrupos() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT id, grupo FROM `tb_grupo`  ORDER BY grupo ASC;";

        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }


	
}