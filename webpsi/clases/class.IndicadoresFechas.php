<?php

include_once "class.Conexion.php";

class IndicadoresFechas{


    /**************************************************  
    /* Functiones de FECHAS DE ACTUALIZACION DE TABLAS
    ***************************************************  */


    /*-----------------------Fecha-Actual---------------------*/
     public function getFechaUpdateBasica() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }
        /**-------------------------------------------Provision ---------------------------*/
    /* ------------------------- provision - TBA-----------------------*/
    public function getFechaUpdateProvTba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
               date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }
/**-----------------------------provision - ADSL-----------------------------------------------*/
        public function getFechaUpdateProvAdsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }
/*-----------------------------------provision - catv------------------------------------------*/
          public function getFechaUpdateProvCatv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

        /*-----------------------------------------------Averias-----------------------------------*/

/*----------------------------Averias - Tba---------------------------------------------------*/
         public function getFechaUpdateAveTba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

/*-----------------------------Averias - Adsl--------------------------------------------------*/
       public function getFechaUpdateAveAdsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

/*-----------------------------Averias - Catv--------------------------------------------------*/
    public function getFechaUpdateAveCatv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }

    /*------------------------------------Actualizacion de fechas para Pendientes -------------------------*/
    /*--------------------Pendientes - Tba ---------------**/

    public function getFechaUpdatePendientesProvisionTba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";

        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }    



    /*-------------------Pendientes - Adsl------------------*/

    public function getFechaUpdatePendientesProvisionAdsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'data_macros_fin' 
              AND TABLE_NAME = 'pro_pdt_pais_adsl' LIMIT 1 ";

        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }   
    /*-----------------------Pendientes - Provision----------*/

    public function getFechaUpdatePendientesProvisionCatv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    


/*------------------------------------Liquidadas - Provision ---------------------------*/
/*-----------------------------------liquidadas-Provision de tba  ---------------------------------*/
public function getFechaUpdateLiqui_Prov_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'liquidadas_averias_provision_prov_stb' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    

/*------------------------------------Liquidadas-Provision de Adsl---------------------------*/

public function getFechaUpdateLiqui_Prov_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'liquidadas_averias_provision_prov_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    


/*------------------------------------Liquidadas-Provision de Catv---------------------------*/
public function getFechaUpdateLiqui_Prov_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'prov_liq_catv_pais' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    


/*----------------------------------------Liquidadas - Averias------------------------*/
/*------------------------------------Liquidadas-Averias Tba---------------------------*/
public function getFechaUpdateLiqui_Ave_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'liquidadas_averias_provision_aver_stb' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    

/*------------------------------------Liquidadas-Averias Adsl---------------------------*/
public function getFechaUpdateLiqui_Ave_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
            date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'liquidadas_averias_provision_aver_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    

/*------------------------------------Liquidadas-Averias Catv---------------------------*/
public function getFechaUpdateLiqui_Ave_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'aver_liq_catv_pais' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

    

/*----------------------------------------------------POR VENCER - ---------------------------*/
/*---------------------------------------POR VENCER - PROVISION - tba---------------------------*/
public function getFechaUpdatePorVencer_Prov_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  


/*---------------------------------------POR VENCER - PROVISION - Adsl---------------------------*/
public function getFechaUpdatePorVencer_Prov_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  


/*---------------------------------------POR VENCER - PROVISION - Catv---------------------------*/
public function getFechaUpdatePorVencer_Prov_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  


/*---------------------------------------POR VENCER - Averias - tba---------------------------*/
public function getFechaUpdatePorVencer_Ave_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  
/*---------------------------------------POR VENCER - Averias - Adsl---------------------------*/
public function getFechaUpdatePorVencer_Ave_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  
/*---------------------------------------POR VENCER - Averias - Catv---------------------------*/
public function getFechaUpdatePorVencer_Ave_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    }  

/*---------------------------------------VENCIDAS - ---------------------------*/
/*---------------------------------------VENCIDAS - PROVISION---------------------------*/
/*---------------------------------------VENCIDAS - PROVISION - Tba---------------------------*/
public function getFechaUpdateVencidas_Prov_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 

/*---------------------------------------VENCIDAS - PROVISION - Adsl---------------------------*/
public function getFechaUpdateVencidas_Prov_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 

/*---------------------------------------VENCIDAS - PROVISION - Catv---------------------------*/
public function getFechaUpdateVencidas_Prov_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'tmp_gaudi_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
    /*---------------------------------------VENCIDAS - Averias - Tba---------------------------*/
    public function getFechaUpdateVencidas_Ave_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_tba' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 

    /*---------------------------------------VENCIDAS - Averias - Adsl---------------------------*/
    public function getFechaUpdateVencidas_Ave_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_adsl' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 

    /*---------------------------------------VENCIDAS - Averias - Catv---------------------------*/
    public function getFechaUpdateVencidas_Ave_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'schedulle_sistemas' 
              AND TABLE_NAME = 'pen_pais_catv' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
    /*-------------------------------------------------Agendas --------------------------------------------------*/
    /*----------------------------------------Agendas- Provision ---------------------------------------------------*/
/*----------------------------------------------------Agendas- Provision - Tba----------------------------------------*/
    public function getFechaUpdateAgendas_Prov_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_provision' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
/*----------------------------------------------------Agendas- Provision - Adsl----------------------------------------*/
    public function getFechaUpdateAgendas_Prov_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_provision' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
/*----------------------------------------------------Agendas- Provision - Catv----------------------------------------*/
    public function getFechaUpdateAgendas_Prov_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_provision' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
        /*----------------------------------------Agendas- Averias ---------------------------------------------------*/
            /*----------------------------------------Agendas- Averias - Tba ---------------------------------------------------*/
    public function getFechaUpdateAgendas_Ave_Tba() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_averias' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
            /*----------------------------------------Agendas- Averias - Adsl ---------------------------------------------------*/
                public function getFechaUpdateAgendas_Ave_Adsl() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
             date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_averias' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
                /*----------------------------------------Agendas-Averias-Catv ---------------------------------------------------*/
            public function getFechaUpdateAgendas_Ave_Catv() {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "SELECT 
              date_format(UPDATE_TIME, '%d-%m-%Y %H:%i:%s') as UPDATE_TIME 
            FROM
              information_schema.tables 
            WHERE TABLE_SCHEMA = 'webpsi' 
              AND TABLE_NAME = 'rpt_agendadas_averias' LIMIT 1 ";
        $res = mysql_query($cad, $cnx);
        $row = mysql_fetch_row($res);
        $cnx = NULL;
        $db = NULL;
        return $row[0];
    } 
  }
