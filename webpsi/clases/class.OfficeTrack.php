<?php

include_once "class.Conexion.php";

class OfficeTrack {

	public static $BD_MAIN = "webpsi";
	public static $BD_FFTT = "webpsi_fftt";

    public static $dias_liquidadas = 120;

	public function array_mysql_datesort($a, $b)
	{
	    if ($a == $b) {
	        return 0;
	    }

	    return ($a->date_field < $b->date_field) ? -1 : 1; //custom check here
	}


    public function getTramasTarea($idTarea) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $cad = "SELECT * FROM webpsi_officetrack.tareas WHERE task_id=$idTarea ORDER BY fecha_recepcion ASC;";

        $res = $cnx->query($cad); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
		//print_r($arr);
		return $arr;
        $cnx = NULL;
        $db = NULL;
    } 	

	public function getPaso1($idTrama) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $cad = "SELECT * FROM webpsi_officetrack.paso_uno WHERE task_id=$idTrama ";

        $res = $cnx->query($cad); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
		//print_r($arr);
		return $arr;
        $cnx = NULL;
        $db = NULL;
    } 	
	
	public function getPaso2($idTrama) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $cad = "SELECT * FROM webpsi_officetrack.paso_dos WHERE task_id=$idTrama ";

        $res = $cnx->query($cad); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
		//print_r($arr);
		return $arr;
        $cnx = NULL;
        $db = NULL;
    } 	

	public function getPaso3($idTrama) {
        $db = new Conexion();
        $cnx = $db->conectarPDO();

        $cad = "SELECT * FROM webpsi_officetrack.paso_tres WHERE task_id=$idTrama ";

        $res = $cnx->query($cad); 
        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $arr[] = $row;
        }
		//print_r($arr);
		return $arr;
        $cnx = NULL;
        $db = NULL;
    } 		
	
}