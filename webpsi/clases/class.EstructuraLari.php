<?php

include_once "class.Conexion.php";

class EstructuraLari {

	private $bdFFTT= "webpsi_estructuras";

    public function getLariCategorias() {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

        $sql = "SELECT idpersona_categoria, categoria, status FROM webpsi.tb_persona_categorias 
			ORDER BY categoria ASC ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }

    public function getEmpresas() {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

        $sql = "SELECT id, eecc, estado FROM webpsi.tb_eecc where estado=1
			ORDER BY eecc ASC ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }	
	
    public function getEstructura() {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

		$sql = "SELECT idlari_cdc, cdc, status FROM webpsi_estructuras.lari_cdc ORDER BY 1 ASC ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }		
	
	public function getEstructuraCdc($idCDC) {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

		$sql = "SELECT intlari_cdc_sub, idlari_cdc, cdc_sub, sortShow 
				FROM webpsi_estructuras.`lari_cdc_sub` WHERE idlari_cdc=$idCDC AND STATUS=1 ORDER by sortShow ASC; ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }		
	
	
	
	
    public function getEstructura_old() {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

        $sql = "SELECT c.`idlari_cdc`, c.`cdc`, b.`intlari_cdc_sub`,  b.`cdc_sub`
				FROM webpsi_estructuras.`lari_cdc_sub_personas` a,
				webpsi_estructuras.`lari_cdc_sub` b,
				webpsi_estructuras.`lari_cdc` c, webpsi.`tb_persona` p,
				webpsi.`tb_persona_categorias` pc
				WHERE c.`idlari_cdc`=b.`idlari_cdc` AND a.`idlari_cdc_sub`=b.`intlari_cdc_sub`
				AND p.`id`=a.`idpersona` AND p.`idcategoria`=pc.`idpersona_categoria`
				AND a.`status`=1 AND p.`estado`=1
				GROUP BY 1,3
				ORDER BY c.`idlari_cdc` ASC, b.`sortShow` ASC, a.`idpersona` ASC
				 ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }	
	
	public function getCDC($idCdc, $idSubCdc) {

    	$db = new Conexion();
        $cnx = $db->conectarBD();

        $sql = "SELECT c.`idlari_cdc`, c.`cdc`, b.`intlari_cdc_sub`,  b.`cdc_sub`, a.`idpersona`,  p.`apellido_p`, 
				p.`apellido_m`, p.`nombre`, pc.`categoria`,
				(SELECT contacto FROM webpsi.`tb_persona_contacto` c2 WHERE c2.id_persona=a.`idpersona` AND c2.id_tipo_contacto=1  ) AS celular,
				(SELECT contacto FROM webpsi.`tb_persona_contacto` c2 WHERE c2.id_persona=a.`idpersona` AND c2.id_tipo_contacto=2 ) AS rpm,
				(SELECT contacto FROM webpsi.`tb_persona_contacto` c2 WHERE c2.id_persona=a.`idpersona` AND c2.id_tipo_contacto=3 ) AS correo
				FROM webpsi_estructuras.`lari_cdc_sub_personas` a,
				webpsi_estructuras.`lari_cdc_sub` b,
				webpsi_estructuras.`lari_cdc` c, webpsi.`tb_persona` p,
				webpsi.`tb_persona_categorias` pc
				WHERE c.`idlari_cdc`=b.`idlari_cdc` AND a.`idlari_cdc_sub`=b.`intlari_cdc_sub`
				AND p.`id`=a.`idpersona` AND p.`idcategoria`=pc.`idpersona_categoria`
				AND a.`status`=1 AND p.`estado`=1 AND b.`idlari_cdc`=$idCdc AND b.intlari_cdc_sub=$idSubCdc
				ORDER BY c.`idlari_cdc` ASC, b.`sortShow` ASC, a.`idpersona` ASC
				 ";

		$res = mysql_query($sql) or die(mysql_error(). " ".$sql);
        $array = array();
        while ($row = mysql_fetch_array($res))
        {
            $array[] = $row;
        }
      
        $cnx = NULL;
        $db = NULL;
        return $array;
    }
	
	
}