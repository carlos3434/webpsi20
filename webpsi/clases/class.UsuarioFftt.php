<?php

include_once "class.Conexion.php";

class UsuarioFftt {

    private $BD = "webpsi";

    public function listarMdfsUsuario($idusuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        global $BD;
        
        $cad = "SELECT mdf FROM $BD.`usuarios_mdfs` WHERE idusuario=$idusuario AND activo=1";
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function listarNodosUsuario($idusuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        global $BD;
        
        $cad = "SELECT nodo FROM $BD.`usuarios_nodos` where idusuario=$idusuario AND activo=1 ";
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function listarTipoActuacionUsuario($idusuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        global $BD;
        
        $cad = "SELECT a.idtipo_actuacion, b.tipo_actuacion FROM $BD.usuarios_tipoactuacion a, $BD.tipos_actuacion b 
                where a.idtipo_actuacion=b.idtipo_actuacion
                AND a.idusuario=$idusuario AND b.activo=1 ";
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        while ($row = mysql_fetch_row($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }    


	
}