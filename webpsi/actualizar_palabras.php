<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>

<link href="estilos.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	iniciar();
});

function iniciar()
{
	$("#countdown").val(140);
	$("#celular").val("");
	$("#mensaje").val("");
}


function enviar()
{
	var celular = $("#celular").val();
	var mensaje = $("#mensaje").val();
	
	var modo = $("#modo").val();
	var grupo = $("#grupo").val();
	
	if (celular.length<9)
	{
		alert("Numero celular no valido. Debe tener 9 digitos.");
		return;
	}

	if (mensaje.length<4)
	{
		alert("Mensaje debe tener minimo 4 letras.");
		return;
	}
	
	
	$.ajax({
	  type: "POST",
	  url: "enviar_individual_ajax.php",
	  data: { 
		enviar_sms: 1,
		celular: celular,
		mensaje: mensaje,
		modo: modo
	  }
	}).done(function( msg ) {
		var res = msg.split("|");
		r = $.trim(res[1])
		//alert(r);
		if (r == "1") {
			$("#div_res").html("Se envio el mensaje correctamente.");
			iniciar();
		}
		else
			$("#div_res").html("Ocurrio un error, no se pudo enviar el mensaje.");
	});
}

function limitText() {
	var limitField = document.getElementById("mensaje");
	var limitCount = document.getElementById("countdown");
	var limitNum = 140;
	
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


</script>

</head>
    
<body background="imagenes/fondo.gif">
        <input type="hidden" value="" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <div id="main-content">
                <div id="id0" class="div0">
                <form method="post" action="listado_03.php">
				<table border="2">
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">Actualizacion de Palabras</th>
                    </tr>	
                </thead>
                <tbody>
				    <tr>
                        <th scope="row" class="column1">Codigo:</th>
                        <td class="column2"><input name="codigo" id="codigo" type="text" maxlength="10" /></td>
                    </tr>
					
                    <tr>
                        <th scope="row" class="column1">Descripcion:</th>
                        <td class="column2"><input name="descripcion" id="descripcion" type="text" maxlength="10" /></td>
                    </tr>
					
                    <tr>
                        <th scope="row" class="column1">Grupo:</th>
                        <td class="column2"><input name="grupo" id="grupo" type="text" maxlength="10" /></td>

                    </tr>
					
					<tr>
					   <th scope="row" class="column1">Estado:</th>
                        <td class="column2">
						<select>
						<option>Habilitado</option>
						<option>Deshabilitado</option>
						</select>
						</td>
					</tr>
					
                    <tr>
                        <td colspan="2" class="td_center">
						<input name="actualizar" type="button" value="Actualizar" onclick="ingresar()"/>
						<input name="regresar" type="submit" value="Regresar"/>
						</td>
                    </tr>
                </tbody>
                </table>
				</form>
                </div>

                <div id="div_res" class="div0"></div>                

        </div>

        </div>
</body>
</html>
