<?php
require_once("../cabecera.php");

//echo $RUTA;

//$x = $RUTA."/clases/class.Persona.php";
//echo $x;

require ("../clases/class.Persona.php");

$arr = $_REQUEST["arrIdPersonas"];

$persona = new Persona();

$arrIdPersona = split(",", $arr);

$i=0;
foreach ($arrIdPersona as $IdPersona) {
    //echo $IdPersona." y su celular = ".$persona->BuscarCelular($IdPersona)."<br/>";
    $celular[$i] = $persona->BuscarCelular($IdPersona);
    $celularComas .= $persona->BuscarCelular($IdPersona).", ";
    $i++;
}

//echo $celularComas;
$persona->setId($x);
$persona->LlenarDatosPersona();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <title>PSI - Web SMS - Mensajes Libres e Individuales</title>


            <?php include ("../includes.php") ?>         

            <link href="<?=$RUTA?>/sms_libre.css" rel="stylesheet" type="text/css" media="all"/>
            <script type="text/javascript">

                $(document).ready(function() {
                    iniciar();
	
                });

                function iniciar()
                {
                    $("#countdown").val(140);
                    $("#celular").val("");
                    $("#mensaje").val("");
                }


                function enviar_grupal()
                {
                    var celulares = $("#celular_grupal").val();
                    //alert(celular);
                    var mensaje = $("#mensaje").val();
                    var iduser = $("#txt_idusuario").val();
	
                    if (mensaje.length<4)
                    {
                        alert("Mensaje debe tener minimo 4 letras.");
                        return;
                    }
	
	
                    $.ajax({
                        type: "POST",
                        url: "sms_enviar_grupal_ajax.php",
                        data: { 
                            enviar_sms: 1,
                            celular: celulares,
                            iduser: iduser,
                            mensaje: mensaje
                        }
                    }).done(function( msg ) {
                        var res = msg.split("|");
                        r = $.trim(res[1])
                        
                        /*if (r == "1") {
                            $("#div_res").html("Se envio el mensaje correctamente.");
                            iniciar();
                        }
                        else
                            $("#div_res").html("Ocurrio un error, no se pudo enviar el mensaje.");
                        */

                        $("#div_res").html("Se envio el mensaje correctamente.");
                    });
                }

                function limitText() {
                    var limitField = document.getElementById("mensaje");
                    var limitCount = document.getElementById("countdown");
                    var limitNum = 140;
	
                    if (limitField.value.length > limitNum) {
                        limitField.value = limitField.value.substring(0, limitNum);
                    } else {
                        limitCount.value = limitNum - limitField.value.length;
                    }
                }


            </script>


    </head>

    <body >
        <input type="hidden" value="<?php echo $IDUSUARIO?>" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <?php //echo pintar_cabecera(); ?>
            <div id="main-content">
                <div id="id0" class="div0" >
                <table>
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">&nbsp;Envio de SMS Individuales</th>
                    </tr>	
                </thead>

                <tbody>
                    <tr>
                        <th scope="row" class="column1">Numero Celular:</th>
                        <td class="column2" style="color: black">
                            <input type="hidden" name="celular_grupal" id="celular_grupal" style="color: black" 
                                 value="<?=$celularComas?>" />
                            
                            <?php echo $celularComas ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="column1">Mensaje SMS:</th>
                        <td class="column2"><textarea id="mensaje" name="mensaje" class="caja_texto2"  onKeyDown="limitText();" 
                                        onKeyUp="limitText();"></textarea></td>

                    </tr>
                    <tr>
                        <td colspan="2" class="td_center">
                            <font size="1">(Max caracteres: 140)<br>
                                    Tienes <input readonly type="text" name="countdown" id="countdown" size="3" value="140" style="width:40px; font-size: 10px;"> caracteres por escribir.</font>

                                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="td_center"><input name="enviar" 
                                type="button" 
                                value="Enviar SMS" onclick="enviar_grupal()"/></td>
                    </tr>
                    </tbody>
                    </table>
                </div>

                <div id="div_res" class="div0"></div>                


        <?php
        /*
            <div id="feature-content">

            <div id="feature-left">
            <h1><a href="#">Ten Ways To Improve Your Code</a></h1>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>

            <div id="feature-right">
            <div class="feature-mini">
            <h2><a href="#">Ten Ways To Improve Your Code</a></h2>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>
            <div class="feature-mini">
            <h2><a href="#">Ten Ways To Improve Your Code</a></h2>
            <p>
            Ut nulla. Vivamus bibendum, nulla ut congue fringilla, lorem ipsum ultricies risus, ut rutrum velit tortor vel purus. In hac habitasse platea dictumst. Duis fermentum, metus sed congue gravida, arcu dui ornare urna, ut imperdiet enim odio dignissim ipsum. Nulla facilisi. Cras magna ante, bibendum sit amet, porta vitae, laoreet ut, justo. Nam tortor sapien, pulvinar nec, malesuada in, ultrices in, tortor. Cras ultricies placerat eros. Quisque odio eros, feugiat non, iaculis nec, lobortis sed, arcu. Pellentesque sit amet sem et purus pretium consectetuer.
            </p>
            </div>
            <div class="clear"></div>
            </div>
            <div class="clear"></div>
            </div>
            */
        ?>
        </div>

        <div id="footer">
            &copy;      2012 PSI - Planificacion de Soluciones Informaticas
        </div>

        </div>

    </body>
</html>