<?php

require_once("../clases/class.PersonaGrupo.php");
require_once("../clases/class.Persona.php");

$pg = new PersonaGrupo();
$idgrupo = $_REQUEST["idgrupo"];

$arr = $pg->ListadoPersonaGrupo($idgrupo);

$persona = new Persona();


?>
<script type="text/javascript">

  $('#chk_todos').change(function () {
        $('input:checkbox').prop('checked', this.checked);    
    
    });

</script>

<div id="div_res_grupal" class="div_res_grupal">
<table class="tabla_res_grupal" >
<form name="form1" id="form1">
    <thead>
    <tr>
        <th class="th_res_grupal"><input type='checkbox' id='chk_todos' name='chk_todos' /></td>
        <th class="th_res_grupal">Apellido Paterno</td>
        <th class="th_res_grupal">Apellido Materno</td>
        <th class="th_res_grupal">Nombres</td>
        <th class="th_res_grupal">DNI</td>
        <th class="th_res_grupal">EECC</td>
        <th class="th_res_grupal">Celular Principal</td>

    </tr>
    </thead>
    <?php
    $i = 1;

    foreach ($arr as $fila) {
        $cbox = "<input type='checkbox' name='pg_checkboxs' value='".$fila["id_persona"]."' />";
        $celular_mostrar = $persona->BuscarCelular($fila["id_persona"]);
      ?>
        <tr>
            <td class="td_res_grupal" style="width:10px"><?php echo $cbox?></td>
            <td class="td_res_grupal" style="width:120px" ><?php echo $fila["apellido_p"]?></td>
            <td class="td_res_grupal" style="width:120px"><?php echo $fila["apellido_m"]?></td>
            <td class="td_res_grupal" style="width:80px"><?php echo $fila["nombre"]?></td>
            <td class="td_res_grupal" style="width:20px"><?php echo $fila["dni"]?></td>
            <td class="td_res_grupal" style="width:20px"><?php echo $fila["eecc"]?></td>
            <td class="td_res_grupal" style="width:120px"><?php echo $celular_mostrar?></td>
        </tr>
        <?php

        $i++;
    }

    ?>
        
        <tr>
            
            <td><input type="button" value="Enviar SMS" onclick="validar_envio_grupal('pg_checkboxs')"/>
        </tr>
</form>
</table>
    
</div>
