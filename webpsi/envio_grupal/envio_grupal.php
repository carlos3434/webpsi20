<?php
require_once("../cabecera.php");
require_once("../clases/class.Grupo.php");

$grupo = new Grupo();

//echo "ID USUARIO = ".$IDUSUARIO;

$arrGrupos = $grupo->ListadoGruposUsuario($IDUSUARIO);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>PSI - Web SMS - Mensajes Grupales</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
            <meta name="author" content="Sergio Miranda Cornejo" />
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

                <?php include ("../includes.php") ?>                

                <script type="text/javascript" src="../js/js.js"></script>
                <script type="text/javascript" src="../js/ed.js"></script>

                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#cmb_red").val(0);
                        $("#cmb_mdf").val(0);
                        $("#cmb_zonal").val(0);

                        $('#chk_todos').change(function () {
                            alert('changed');
                        });

                    });
                    
                    
                    function combo_grupo(obj) {
                        //alert(obj.value);
                        $("#div_busqueda").load("pintar_personasgrupo.php", {
                            idgrupo: obj.value
                        });
                    }
                 
</script>

<link rel="stylesheet" type="text/css" href="../estilos.css">
</head>

<body>
	<input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>


	<div id="page-wrap">
		<?php echo pintar_cabecera(); ?>
                        <input type="hidden" value="<?php echo $IDUSUARIO ?>" name="txt_idusuario" id="txt_idusuario"/>
                        <div id="page-wrap">

                            <div id="main-content">
                                <div id="id0" class="div0" >
                                    <table style="tabla_res_grupal">
                                        <tr>
                                            <td>Grupo:</td>
                                            <td>
                                                <select name='cmb_zonal' id='cmb_zonal' class="combo_grupal1"
                                                        onchange="combo_grupo(this)">
                                                    <option value="0">Seleccionar...</option>
                                                    <?php
                                                    foreach ($arrGrupos as $fila) {
                                                        ?>
                                                        <option value='<?php echo $fila[0] ?>'><?php echo $fila[1] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </td>
                                            <?php /*
                                            <td>Mdf:</td>
                                            <td>
                                                <div id="div_mdf"><select name='cmb_mdf' id='cmb_mdf' onchange="cmb_red()">
                                                        <option value='0'>Seleccionar...</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tipo Red:</td>
                                            <td>
                                                <select name='cmb_red' id='cmb_red' onchange="cmb_cablearmario()">
                                                    <option value='0'>Seleccionar ...</option>
                                                    <option value='1'>Directa</option>
                                                    <option value='2'>Flexible</option>
                                                </select>
                                            </td>
                                            <td>Cable / Armario:</td>
                                            <td>
                                                <div id="div_cablearmario">&nbsp;xxxx</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align='center' colspan='4' style="text-align: center;">
                                                <input type='button' value='Buscar' onclick="buscar()"/>
                                            </td>
                                        </tr>
                                             
                                             */
                                            ?>
                                        </tr>
                                    </table>
                                </div>

                        <div id='div_busqueda' class="div_res"></div>                                    
                                
                            </div>
                            
                        

                            <div id="footer">
                                &copy;      2012 PSI - Planificacion de Soluciones Informaticas
                            </div>

                        </div>

        </body>
        </html>

