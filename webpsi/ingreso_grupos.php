<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMS Libre</title>
<link rel="stylesheet" href="jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />
             <script src="jquery-ui-1.10.3/jquery-1.9.1.js"></script>
             <script src="jquery-ui-1.10.3/ui/jquery-ui.js"></script>

<script type="text/javascript">

function ingreso()
{
	var codigo = $("#codigo").val();
	var descripcion = $("#descripcion").val();
	$.ajax({
	  type: "POST",
	  url: "ingresar_grupo_ajax.php",
	  data: { 
		ingresa_grupo: 1,
		codigo: codigo,
		descripcion: descripcion
	  }
	}).done(function( msg ) {
		alert("Datos Ingresados");
	});
}

</script>

</head>
    
<body background="imagenes/fondo.gif">
        <input type="hidden" value="" name="txt_idusuario" id="txt_idusuario"/>
        <div id="page-wrap">
            <div id="main-content">
                <div id="id0" class="div0">
                <form method="post" action="listado_02.php">
				<table border="2">
                <thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">Ingreso de Grupos</th>
                    </tr>	
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" class="column1">Codigo:</th>
                        <td class="column2"><input name="codigo" id="codigo" type="text" maxlength="10" /></td>
                    </tr>
					
                    <tr>
                        <th scope="row" class="column1">Descripcion:</th>
                        <td class="column2"><input name="descripcion" id="descripcion" type="text" maxlength="20" /></td>

                    </tr>
					
                    <tr>
                        <td colspan="2" class="td_center">
						<input name="ingresar" type="button" value="Ingresar" onclick="ingreso();"/>
						<input name="regresar" type="submit" value="Regresar"/>
						</td>
                    </tr>
                </tbody>
                </table>
				</form>
                </div>

                <div id="div_res" class="div0"></div>                

        </div>

        </div>
</body>
</html>
