#!/usr/bin/perl

# Parametro 0 = Numero de celular
# Parametro 1 = Mensaje SMS a enviar

#print int(@ARGV),"\n";
#print $ARGV[0],"\n";
#print $ARGV[1],"\n";
# print $ARGV[2],"\n";

my $total_argumentos = int(@ARGV);
my $celular = $ARGV[0];
my $texto = $ARGV[1];

if ($total_argumentos<2) 
{
	print "\n"."No se envio el numero adecuado de parametros.";
	exit;
}

use File::Copy;
use Frontier::Client;

#my $client = Frontier::Client->new (
#	url => 'http://gaudi:despacho22@10.10.167.34/x2m/rpc2',
#	debug => 1
#);

my $client = Frontier::Client->new (
        url => 'http://gaudi:despacho22@10.10.128.60/x2m/rpc2',
        debug => 1
);


sub trim{
	my $str =shift;
	$str=~ s/^\s+//;
	$str=~ s/\s+$//;
	return $str;
};

my $log;
 
push @params,
{'celular' => Frontier::RPC2::String->new(trim($celular)),
 'texto'   => Frontier::RPC2::String->new(trim($texto)),
 'fecha'   => Frontier::RPC2::String->new('')
};

$log.="Celular: ".$celular."Mensaje:".$texto."\n";

my $res = $client->call('Tiaxa.SendMsg',[@params]);		
    
($seg, $min, $hora, $dia, $mes, $anho, @zape) = localtime(time);
$mes++;
$anho+=1900;
$fecha_actual= "$anho-$mes-$dia $hora:$min:$seg";

my $status_envio = $res->[0];
my $mensaje_enviado = $res->[1];

#open (MYFILE, ">>".$sitedata);
#print MYFILE "\n".$fecha_actual." Respuesta: ".$status_envio."-".$mensaje_enviado."\nDatos Enviados:\n".$log;
#close (MYFILE);

print "\n".$fecha_actual." Respuesta: ".$res->[0]."-".$res->[1]."\nDatos Enviados:\n".$log;


