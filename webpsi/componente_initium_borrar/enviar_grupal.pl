#!/usr/bin/perl
use DBI;

# Conectar a Base de datos.
# Sintaxis
# $dbh = DBI->connect('dbi:mysql:DATABASE_NAME', USERNAME, PASSWORD)
my $dbh = DBI->connect('dbi:mysql:envio_sms','root','jMuW8l') 
	or die "Connection Error: $DBI::errstr\n";
$dbh->{AutoCommit} = 0;
###

# Parametro 0 = Numero de celular
# Parametro 1 = Mensaje SMS a enviar

#print int(@ARGV),"\n";
#print $ARGV[0],"\n";
#print $ARGV[1],"\n";
# print $ARGV[2],"\n";

my $total_argumentos = int(@ARGV);
my $celular = $ARGV[0];
my $texto = $ARGV[1];
my $id_user = $ARGV[2];
my $estado = $ARGV[3];


if ($total_argumentos<4) 
{
	print "\n"."No se envio el numero adecuado de parametros.";
	exit;
}

use File::Copy;
use Frontier::Client;


#my $client = Frontier::Client->new (
#	url => 'http://gaudi:despacho22@10.10.167.34/x2m/rpc2',
#	debug => 1
#);
#

my $client = Frontier::Client->new (
        url => 'http://gaudi:despacho22@10.10.128.60/x2m/rpc2',
        debug => 1
);


sub trim{
	my $str =shift;
	$str=~ s/^\s+//;
	$str=~ s/\s+$//;
	return $str;
};

my $log;
 
push @params,
{'celular' => Frontier::RPC2::String->new(trim($celular)),
 'texto'   => Frontier::RPC2::String->new(trim($texto)),
 'fecha'   => Frontier::RPC2::String->new('')
};

$log.="Celular: ".$celular."Mensaje:".$texto."\n";

my $res = $client->call('Tiaxa.SendMsg',[@params]);		
    
($seg, $min, $hora, $dia, $mes, $anho, @zape) = localtime(time);
$mes++;
$anho+=1900;
$fecha_actual= "$anho-$mes-$dia $hora:$min:$seg";

my $status_envio = $res->[0];
my $mensaje_enviado = $res->[1];


## Forma de insertar en TRANSACCIONES en Base de Datos.
my $sql_ins = 'INSERT INTO mensajes_enviados_libre(fecha_mov,id_user,mensaje,celular,estado,respuesta_moviles) VALUES (?,?,?,?,?,?)';
my $res_ins = $dbh->prepare_cached($sql_ins);

die "No se puedo realizar el PREPARE del query. Terminando." unless defined $res_ins;

my $success = -1;
$success &&= $res_ins->execute($fecha_actual,$id_user,$texto, $celular, $estado,'Predeterminado') ;


my $result = ($success ? $dbh->commit : $dbh->rollback);
unless ($result) { 
	die "No se pudo realizar transaccion por error: " . $dbh->errstr 
}




print "\n".$fecha_actual." Respuesta: ".$res->[0]."-".$res->[1]."\nDatos Enviados:\n".$log;
