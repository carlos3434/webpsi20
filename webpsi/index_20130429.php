<?php
session_start();

if (isset($_SESSION["usuario"]) && isset($_SESSION["persona"]) && isset($_SESSION["grupo"]) && isset($_SESSION["online"])) {
	header ("Location: main.php");
}
else
	header("location:login/login.html");	//@ redirect

//@ is authorized?
/*if(empty($_SESSION['exp_user']) || @$_SESSION['exp_user']['expires'] < time()) {
	//header("location:login/login.html");	//@ redirect
	echo "ss";
} else {
	$_SESSION['exp_user']['expires'] = time()+(45*60);	//@ renew 45 minutes
}
*/


//@ if logoff
if(!empty($_GET['logoff'])) { $_SESSION = array(); }


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Envio de SMS</title>

<link href="css/estilos.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

<script type="text/javascript">

function set(user, pass){
	$("#usuario").val(user);
	$("#password").val(pass);
}

function login()
{
	var usuario = $("#usuario").val();
	var password = $("#password").val();
	
	$.post("index.php", {login_sms: 1, usuario: usuario, password: password}, function (data){
		$("#all").html(data);
	});
	
}

function password(e){
	$("#div_resultado").html(e);
}

</script>

</head>
    
<body bgcolor='#DBE3FF'>

<br/>

<div id="all">
<div id="id_div" class="div0" >
<table>
	<thead>
	<tr class="odd">
		<th scope="col" abbr="Home" colspan="2">&nbsp;Login - Envio de SMS</th>
	</tr>	
	</thead>

<tbody>
<tr>
	<th scope="row" class="column1">Usuario:</th>
	<td><input name="usuario" id="usuario" type="text"/></td>
</tr>
<tr>
	<th scope="row" class="column1">Password:</th>
	<td><input id="password" name="password" type="password"/></td>
</tr>
<tr>
	<td colspan="2" class="td_center"><input name="login" type="button" value="Login" onclick="login()"/></td>
</tr>
</tbody>
</table>
</div>

<div id="div_resultado" class="div0" style="color:#FF0000;"></div>

</div>
</body>
</html>

<?php

	include ("conexion.php");
	
	session_start();
	
	if (isset($_POST["login_sms"]) ) {
		// obtenemos los datos del archivo
		$usuario = $_POST["usuario"];
		$password = $_POST["password"];
				
		if($usuario == "" && $password == ""){
			echo "<script type='text/javascript'>
					$(document).ready(function() {
						set('".$usuario."','".$password."');
						password('Por favor, ingrese sus datos.');						
					});				
			</script>";
			exit;
		}else{
			if($password == ""){
				echo "<script type='text/javascript'>
						$(document).ready(function() {
							set('".$usuario."','".$password."');
							password('Por favor, ingrese su password.');		
						});						
				</script>";
				exit;
			}
		}	
		
		$query = "SELECT id,usuario,password,dni FROM tb_usuario WHERE usuario = '".$usuario."'";
		$exec_query = mysql_query($query);

		$num = mysql_num_rows($exec_query);
		
		if($num == 1){
			while($row=mysql_fetch_array($exec_query)){			
				if($password == ""){
					echo "<script type='text/javascript'>
							$(document).ready(function() {
								set('".$usuario."','".$password."');
								password('Por favor, ingrese su password.');		
							});						
					</script>";
				}else{
					if(sha1($password) == $row["password"]){
					
						$_SESSION["usuario"] = $row["id"];
						
						$query_1 = "SELECT id FROM tb_persona WHERE dni = '".$row['dni']."'";
						$exec_query_1 = mysql_query($query_1);
						while($row1=mysql_fetch_array($exec_query_1)){
							$_SESSION["persona"] = $row1["id"];
						}
						
						$query_2 = "SELECT id_grupo FROM tb_persona_grupo WHERE id_persona = '".$_SESSION["persona"]."'";
						$exec_query_2 = mysql_query($query_2);
						while($row2=mysql_fetch_array($exec_query_2)){
							$_SESSION["grupo"] = $row2["id_grupo"];
						}
						
						$_SESSION["online"]=1;
						
						$query_3 = "UPDATE tb_usuario SET online = '1' WHERE usuario = '".$usuario."'";
						$exec_query_3 = mysql_query($query_3);
						
						$id = $row["id"];
						$fecha = date('Y-m-d H:i:s', time());
												
						// Funcion que extrae ip visitante 
						function getRealIpAddr()
						{
							if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
							{
							  $ip=$_SERVER['HTTP_CLIENT_IP'];
							}
							elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
							{
							  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
							}
							else
							{
							  $ip=$_SERVER['REMOTE_ADDR'];
							}
							return $ip;
						}

						$ip = getRealIpAddr();
						
						$query_4 = "INSERT INTO tb_usuario_login(id_usuario, fecha, ip, accion) VALUES ('".$id."','".$fecha."','".$ip."','1')";
						echo  $query_4;
						$exec_query_4 = mysql_query($query_4);
						
						header ("Location: main.php");
					}else{
						echo "<script type='text/javascript'>
								$(document).ready(function() {
									set('".$usuario."','".$password."');
									password('Password incorrecto.');		
								});						
						</script>";
					}
				}
			}
		}else{
			echo "<script type='text/javascript'>
					$(document).ready(function() {
						set('".$usuario."','".$password."');
						password('Usuario incorrecto.');		
					});						
			</script>";
		}

	}	
	
	function ConsultaDatos($modelo, $contrata, $zonal){
	global $conexion;
	include 'consulta.php';
	}
	
	function DatosNuevos($modelo,$estado){
	global $conexion;
	include 'DatosNuevos.php';
	}
	function RegistraDatos($usuario,$password,$nombre,$apellido,$dni,$area){
	global $conexion;
	include 'registro.php';
	}
	
	function RegistroCorrecto($nombre,$apellido,$usuario,$password,$dni,$area){
	global $conexion;
	$sql5 = "INSERT INTO tb_usuario(nombre,apellido,usuario,password,dni,id_area)";
    $sql5 .= "VALUES ('".$nombre."','".$apellido."', '".$usuario."', '".$password."', '".$dni.",$area)";                 
	$ejecutar_consulta5 = mysql_query($sql5);
	echo "datos grabados correctamente";
	}
	
	function addModelo($codigo, $descripcion, $cantidad, $modeloequipo, $numguia){
	global $conexion;
	
	}
	
	function ConsultaPedidos($nroguia, $correlativo){
	global $conexion;
	}
	
	
	$opcion = $_GET['cmd'];
	switch($opcion){
	case 'ConsultaDatos':
	      ConsultaDatos($_POST['modelo'],$_POST['contrata'],$_POST['zonal']);
		  break;
    case 'Datos_nuevos':
		  DatosNuevos($_POST['modelo'],$_POST['estado']);
		  break;
	case 'RegistraCorrecto':
		  RegistraCorrecto($_POST['nombre'],$_POST['apellido'],$_POST['usuario'],$_POST['password'],$_POST['dni'],$_POST['id_area']);
		  break;
	case 'addModelo':
		  addModelo($_POST['codigo'],$_POST['descripcion'],$_POST['cantidad'],$_POST['modeloequipo'],$_POST['numguia']);
		  break;
    case 'ConsultaPedidos':
	      ConsultaPedidos($_POST['nroguia'], $_POST['correlativo']);
		  break; 
	
		  
	}
?>