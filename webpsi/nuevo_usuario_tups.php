<?php 
	include("conexion.php");
	require_once("cabecera.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Creacion de Usuarios - TUPS</title>
 <?php include ("includes.php") ?>  
<link href="sms_libre.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="jquery-1.7.2.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	
	$("#dni").keydown(function(event) {
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
			(event.keyCode == 67 && event.ctrlKey === true) || 
			(event.keyCode == 86 && event.ctrlKey === true) || 
			(event.keyCode == 88 && event.ctrlKey === true) || 
			(event.keyCode >= 35 && event.keyCode <= 39)) {
				 return;
		}
		else {
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	$("#celular").keydown(function(event) {
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
			(event.keyCode == 67 && event.ctrlKey === true) || 
			(event.keyCode == 86 && event.ctrlKey === true) || 
			(event.keyCode == 88 && event.ctrlKey === true) || 
			(event.keyCode >= 35 && event.keyCode <= 39)) {
				 return;
		}
		else {
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});

});
	
function registro()
{
	$.post("nuevo.php", {registro_sms: 1, usuario: usuario, password_1: password_1, password_2: password_2, nombre: nombre, apellido: apellido, dni: dni, celular: celular, rpm: rpm, area: area}, function (data){
		$("#id_div").html(data);
	});
}

function alerta(e){
	$("#div_resultado").html(e);
}

function set(user, pass1, pass2, nomb, apel, dni, cell, rpm, area){
	$("#usuario").val(user);
	$("#password_1").val(pass1);
	$("#password_2").val(pass2);
	$("#nombre").val(nomb);
	$("#apellido").val(apel);
	$("#dni").val(dni);
	$("#celular").val(cell);
	$("#rpm").val(rpm);
	$("#area").val(area);
}
	
</script>

</head>
    
<body bgcolor='#DBE3FF'>
  <div id="page-wrap">
 <?php echo pintar_cabecera(); ?>
<br/>
<form action="index.php?cmd=RegistraDatos" method="post" name="post">
 <div id="main-content">
<div id="id_div" class="div0" >

<table>
	<thead>
                    <tr class="odd">
                        <th scope="col" abbr="Home" colspan="4" class="col_titulo">&nbsp; REGISTRO TUPS</th>
                    </tr>	
                </thead>

<tbody>
<tr>
	<th scope="row" class="column1">Usuario:</th>
	<td><input name="usuario" id="usuario" type="text"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">Contrase�a:</th>
	<td><input id="password_1" name="password" type="password"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">Repetir Contrase�a:</th>
	<td><input id="password_2" name="password" type="password"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">Nombres:</th>
	<td><input id="nombre" name="nombre" type="text"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">Apellidos:</th>
	<td><input id="apellido" name="apellido" type="text"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">DNI:</th>
	<td><input id="dni" name="dni" type="text" maxlength="8"/> ( * )</td>
</tr>
<tr>
	<th scope="row" class="column1">Area:</th>
	<td><select name="area" id="area">
	<option value="">seleccione</option>";
		<?php 
			$query = "SELECT id,area FROM tb_area WHERE estado='1' ORDER BY area";
			$exec_query = mysql_query($query);
			while($row = mysql_fetch_array($exec_query)){
				echo "<option value='".$row['id_area']."'>".$row['area']."</option>";
			}
		?>
	</select> ( * )</td>
</tr>
<tr>
	<td colspan="2" class="td_center"><input type='button' value="Aceptar" onClick="registro_tups()"/></td>
</tr>
</tbody>
</table>
</form>
</div>
<div id="div_campos" class="div0">( * ) Campos Obligatorios.</div>
<div id="div_resultado" class="div0" style="color:#FF0000;"></div>
<div id="resultado"></div>
            <div  style=" width:360px; position: relative;">
                <div id="paginacion"></div>
            </div>

 </div>
 


        <div id="footer">
            &copy;      2012 PSI - Planificacion de Soluciones Informaticas
        </div>
		   </div>
</body>
</html>

<?php


	$user 		= $_POST["usuario"];
	$password_1 = $_POST["password_1"];
	$password_2 = $_POST["password_2"];
	$nombre 	= $_POST["nombre"];
	$apellido 	= $_POST["apellido"];
	$dni 		= $_POST["dni"];
	$area 		= $_POST["area"];
	
	if(isset($user) && isset($password_1) && isset($password_2) && isset($nombre) && isset($apellido) && isset($dni) && isset($area)){
		if($password_1 == $password_2){
			$pass = md5($password_1);
			$query = "INSERT INTO tb_usuarios(user,password,nombre,apellido,dni,celular,rpm,cod_area) VALUES (".$user.",".$pass.",".$nombre.",".$apellido.",".$dni.",".$celular.",".$rpm.",".$area.")";
			$exec_query = mysql_query($query);
			echo "<script type='text/javascript'>
						$(document).ready(function() {
							set('".$user."','".$password_1."','".$password_2."','".$nombre."','".$apellido."','".$dni."','".$cod_area."');
							alerta('Grabaci�n Satisfactoria.');		
						});						
				</script>";
		}else{
			echo "<script type='text/javascript'>
						$(document).ready(function() {
							set('".$user."','".$password_1."','".$password_2."','".$nombre."','".$apellido."','".$dni."','".$cod_area."');
							alerta('No coinciden los passwords.');		
						});						
				</script>";
		}	
	}else{
		echo "<script type='text/javascript'>
					$(document).ready(function() {
						set('".$user."','".$password_1."','".$password_2."','".$nombre."','".$apellido."','".$dni."','".$cod_area."');
						alerta('Faltan dato(s) obligatorio(s).');		
					});						
			</script>";
	}


?>
