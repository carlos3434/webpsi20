<?php
session_start();
require_once '../../clases/class.Conexion.php';
require_once 'clases/class.AvetecAveria.php';

$Conexion = new Conexion();
$cnx = $Conexion->conectarPDO();

if ( isset($_POST["action"]) ) {
    
    if ( $_POST["action"] === "loadMarker" ) {
        $AvetecAveria = new AvetecAveria();
        $data = $AvetecAveria->loadDetail($cnx);
        echo json_encode($data);
    }
    
}