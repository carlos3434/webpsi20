<?php
require_once("../../cabecera.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title></title>
        <style>
            body, html {
                height: 100%;
                width: 100%;
            }

            #geo_map {
                width: 100%;
                height: 100%;
                float: right;
                background-color: papayawhip;
            }
        </style>


        <?php include ("../../includes.php") ?>
        <script src="js/jquery.md5.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&v=3.exp&sensor=true"></script>
        <script>
            var map;
            var bounds;
            var infowindow;
            var geocoder;
            var mapObjects = {};
            //Por defect0o amarillo o antiguo
            var color = 'FFFF00';

            //Inicializar Google Maps
            function mapInit() {
                bounds = new google.maps.LatLngBounds();
                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(-12.033284, -77.0715493)
                };
                map = new google.maps.Map(document.getElementById('geo_map'),
                        mapOptions);

                infowindow = new google.maps.InfoWindow({
                    content: "Loading..."
                });

                //Geocoder
                geocoder = new google.maps.Geocoder();

                return true;
            }

            /**
             * Carga puntos cada ms minutos (ms=milisegundos)
             * 
             * @returns {undefined}
             */
            function checkNewLauncher() {
                var ms = 10000;
                setInterval(function() {
                    loadMarker();
                }, ms);
                return true;
            }
            
            /**
             * Limpia elementos del mapa
             * contenidos en mapObjects
             * 
             * @returns {undefined}
             */
            function clearMap(){
                for ( index in mapObjects ) {
                    if ( mapObjects.hasOwnProperty(index) ) {
                        mapObjects[index].setMap(null);
                        delete mapObjects[index];
                    }
                }
            }

            /**
             * Envía petición y retorna puntos
             * En amarillo puntos antiguos
             * En rojo puntos nuevos
             * 
             * @returns {undefined}
             */
            function loadMarker() {
                var headers = "action=loadMarker"
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "avetec.request.php",
                    data: headers,
                    dataType: 'json',
                    success: function(datos) {
                        
                        //Limpiar mapa
                        clearMap();
                        
                        $.each(datos, function(id, val) {

                            var pt = new google.maps.LatLng(
                                    this.Latitude, this.Longitude
                                    );
                            bounds.extend(pt);

                            //Menos de 10 minutos color rojo
                            color = 'FFFF00';
                            if (this.minutos < 10) { //10 minutos
                                color = 'FF0000';
                            }
                            
                            //Crear marcador
                            var marker = new google.maps.Marker({
                                position: pt,
                                map: map,
                                icon: "http://chart.apis.google.com/chart"
                                        + "?chst=d_map_pin_letter"
                                        + "&chld=" + (id + 1) + "|" + color + "|000000",
                                animation: google.maps.Animation.DROP
                            });
                            
                            //Objetos creados (marcadores)
                            mapObjects[ $.md5( this.id_detalle ) ] = marker;
                                                        
                            var src = "data:image/png;base64," + this.imagen;
                            var img = "<img src=\"" + src + "\" style=\"float: left\" />";
                            var table = "<div  style=\"float: right; margin: 5px\"><div>" + this.Elemento + "</div>"
                                    + "<div>" + this.Motivo + "</div>"
                                    + "<div>" + this.Punto + "</div>"
                                    + "<div>" + this.Observacion + "</div></div>";
                            //Infowindow del marcador
                            google.maps.event.addListener(marker, "click", function(event) {
                                infowindow.setPosition(pt);
                                infowindow.setContent(table + img);
                                infowindow.open(self.map);
                            });

                            map.fitBounds(bounds);
                        });
                        
                        $('html, body').animate({scrollTop: $(document).height()}, 800);
                    }
                });
            }

            $(document).ready(function() {
                //Cargar mapa
                mapInit();
                //Cargar puntos
                loadMarker();
                checkNewLauncher();
            });
        </script>


        <body>
            <?php echo pintar_cabecera(); ?>

            <div id="geo_map"></div>

        </body>
</html>