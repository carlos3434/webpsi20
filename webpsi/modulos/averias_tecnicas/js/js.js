// Funciones de los botones

function revisar(){
	$("#resultado").html("<br /><br /><span>Realizando busqueda <img src=\"img/loading.gif\" /></span> ");
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$("#resultado").load(
		"codigo.php",
		{
			mdf:mdf,
			nodo:nodo
		}
	);

}

function revisar2(){
	$("#resultado").html("<br /><br /><span>Realizando busqueda <img src=\"img/loading.gif\" /></span> ");
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$("#resultado").load(
		"codigo.php",
		{
			mdf:mdf,
			nodo:nodo
		}
	);

}

//funcion para el boton actualizar
function actualizar(){
	$("#resultado").html("<br /><br /><span>Realizando prueba <img src=\"img/loading.gif\" /></span> ");
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$("#resultado").load(
		"pintarIndicadores3.php",
		{
			mdf:mdf,
			nodo:nodo
		}
	);

}

function reporteador(){
	$('#cronometro_prueba').hide();
	window.open("reporteExcel.php");

}

function mostrarphp(){
	$('#cronometro_prueba').hide();
	window.open("codigo.php");

}

function verDetalle(tipoDetalle, tipoActuacion, negocio) {
	//verDetalle('porvencer','provision','catv')
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();



	$( "#divDetalle1" ).load(
		"pintarIndicadores2.php",
		{
			mdf:mdf,
			nodo:nodo,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio
		}
	);

}

function verDetalle2(tipoDetalle, tipoActuacion, negocio, ConSinTecnico) {
	//verDetalle('porvencer','provision','catv')
	
	var mdf = $("#cmbMdf").val();
	var nodo = $("#cmbNodo").val();

	$( "#divDetalle1" ).load(
		"pintarIndicadores2.php",
		{
			mdf:mdf,
			nodo:nodo,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio,
			ConSinTecnico: ConSinTecnico
		}
	);

}

function verDetalleActuacion(actuacion,tipoDetalle, tipoActuacion, negocio) {

	$( "#divDetalleActuacion" ).load(
		"detalleActuacion.php",
		{
			actuacion:actuacion,
			tipoDetalle: tipoDetalle,
			tipoActuacion: tipoActuacion,
			negocio: negocio
		}
	);

}