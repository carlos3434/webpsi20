<?php

class AvetecAveria {

    protected $_db = 'webpsi';
    protected $_table = 'android_averias';
    
    
    public $id_detalle;
    public $id_android_averias;
    public $Elemento;
    public $Motivo;
    public $Puntovar;
    public $Observacion;
    public $Latitude;
    public $Longitude;
    public $precisio;
    public $imagen;
    public $fecha_grabacion;
    public $estado;
    
    /**
     * Lista detalle de averias tecnicas (todos los registros)
     * 
     * @param type $conexion
     * @return type
     */
    public function loadDetail($conexion){
        
        try {
            $sql = "SELECT 
                        ad.id_detalle,
                        ad.id_android_averias,
                        e.Elemento,
                        m.Motivo,
                        ad.Punto,
                        ad.Observacion,
                        ad.Latitude,
                        ad.Longitude,
                        ad.precisio,
                        ad.imagen,
                        ad.fecha_grabacion,
                        ad.estado,
                        (
                            UNIX_TIMESTAMP(NOW()) 
                            - UNIX_TIMESTAMP(ad.fecha_grabacion)
                        )/60 minutos
                    FROM 
                        android_averias_detalle ad, 
                        tb_elementos e, tb_motivos m
                    WHERE 
                        ad.motivo=m.codigo AND ad.elemento=e.codigo
                    ORDER BY ad.fecha_grabacion DESC ";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $data["imagen"] = base64_encode($data["imagen"]);
                $reporte[] = $data;
            }
            return $reporte;
        } catch (PDOException $e) {
            exit();
        }
        
        
    }

}
