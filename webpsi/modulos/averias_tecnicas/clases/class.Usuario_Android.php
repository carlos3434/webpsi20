<?php

include ("..clases/class.Conexion.php");

class Usuario_Android {
    
    private $Id = '';
    private $Capas = '';
    private $Punto = '';
    private $Latitude= '';
    private $Longitude= '';
    private $Precision='';
    private $Usuario= '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }

    public function getCapas() {
        return $this->Capas;
    }

    public function setCapas($x) {
        $this->Capas = $x;
    }

    public function getLatitude() {
        return $this->Latitude;
    }

    public function setLatitude($x) {
        $this->Latitude = $x;
    }

    public function getLongitude() {
        return $this->Longitude;
    }

    public function setLongitude($x) {
        $this->Longitude = $x;
    }
    
	public function getPrecision() {
        return $this->Precision;
    }

    public function setPrecision($x) {
        $this->Precision = $x;
    }
    
	public function getUsuario() {
        return $this->Usuario;
    }

    public function setUsuario($x) {
        $this->Usuario = $x;
    }


////Funcion de insertar 
    public function IngresarDatosUsuario($elemento,$motivo,$punto,$observacion,$latitude,$longitude,$precision,$usuario,$imagen) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $cad = "INSERT INTO `envio_sms`.`usuario_android` 
	(`id`, 
	`Elemento`,
        `Motivo`,	
	`Punto`,
        `Observacion`,	
	`Latitude`, 
	`Longitude`, 
	`Precision`, 
	`Usuario`,
	`imagen`
	)
	VALUES
	(null, 
	'$elemento',
    '$motivo',	
	'$punto', 
	'$observacion',
	'$latitude', 
	'$longitude', 
	'$precision', 
	'$usuario',
	'$imagen'
	)";
       
		$res = mysql_query($cad, $cnx) or die(mysql_error());
        if($res)
			$x = 1;
	    else
			$x = 0;

        $cnx = NULL;
        $db = NULL;
		
        return $x;
	
    }
/// clase para listar la informacion de android

     public function ListarAverias() {
        $db = new Conexion();
        $cnx = $db->conectarBD();

        $cad = "SELECT  id,Motivo,Punto,Observacion,Latitude,Longitude,Usuario,imagen
                FROM  usuario_android";
                
        //echo $cad;
        $res = mysql_query($cad, $cnx) ;
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($arr);
        $cnx = NULL;
        $db = NULL;
        return $arr;
        
    }   

   //// Clase para ingresar datos a una tabla 
    
   public function IngresarAverias($nombre,$apellido,$usuario,$password,$dni,$id_perfil,$id_area) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
        $claveSHA = sha1($password);
        
        $cad = "INSERT INTO `tb_usuario` (nombre, apellido,usuario,password,dni,online,
            id_perfil,id_area,status) 
            VALUES ('$nombre','$apellido','$usuario',
            '$claveSHA',$dni,0,$id_perfil,$id_area,1)";
        
        $res = mysql_query($cad, $cnx) or die(mysql_error());
        if($res)
            $x = 1;
        else
            $x = 0;

        $cnx = NULL;
        $db = NULL;
        
        return $x;
    
    } 


	
}