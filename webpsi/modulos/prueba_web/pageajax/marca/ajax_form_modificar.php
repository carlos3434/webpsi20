<?


	include "../../librerias/conexion.php";
	include "../../librerias/basico.php";

	$sql = sprintf("select * from marca where idMarca=%d",
		(int)$_POST['idMarca']
	);
	$per = mysql_query($sql);
	$num_rs_per = mysql_num_rows($per);
	if ($num_rs_per==0){
		echo "No existen rubros con ese IDE";
		exit;
	}
	
	$rs_per = mysql_fetch_assoc($per);
	
?>
<h1>Editar Marca</h1>
<form action="javascript: fn_modificar();" method="post" id="frm_per">
	<input type="hidden" id="idmarca" name="idmarca" value="<?=$rs_per['idMarca']?>" />
    <table class="formulario">
        <tbody>
            <tr>
                <td>Codigo</td>
                <td><strong><?=$rs_per['idMarca']?></strong></td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td>
                <input name="nomMarca" type="text" id="nomMarca" size="40" class="required" value="<?=$rs_per['nombreMarca']?>" />
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <input name="modificar" type="submit" id="modificar" value="Modificar" />
                    <input name="cancelar" type="button" id="cancelar" value="Cancelar" onclick="fn_cerrar();" />
                </td>
            </tr>
        </tfoot>
    </table>
</form>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		$("#frm_per").validate({
			submitHandler: function(form) {
				var respuesta = confirm('\xBFDesea realmente modificar a esta nueva persona?')
				if (respuesta)
					form.submit();
			}
		});
	});
	
	function fn_modificar(){
		var str = $("#frm_per").serialize();
		$.ajax({
			url: 'pages/marca/ajax_modificar.php',
			data: str,
			type: 'post',
			success: function(data){
				if(data != "")
					alert(data);
				fn_cerrar();
				fn_buscar();
			}
		});
	};
</script>