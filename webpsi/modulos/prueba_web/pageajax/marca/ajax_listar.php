<?
	include "../../pageajax/librerias/conexion.php";
	include "../../pageajax/librerias/basico.php";
	include "../../pageajax/librerias/PHPPaging.lib.php";

	header ("Expires: Fri, 14 Mar 1980 20:53:00 GMT"); //la pagina expira en fecha pasada
	header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); //ultima actualizacion ahora cuando la cargamos
	header ("Cache-Control: no-cache, must-revalidate"); //no guardar en CACHE
	header ("Pragma: no-cache"); //PARANOIA, NO GUARDAR EN CACHE


	$paging = new PHPPaging;
	$sql = "select * from marcas";
	if (isset($_GET['criterio_usu_per']))
		$sql .= " where nomMarca like '%".fn_filtro(substr($_GET['criterio_usu_per'], 0, 16))."%'";
	if (isset($_GET['criterio_ordenar_por']))
		$sql .= sprintf(" order by %s %s", fn_filtro($_GET['criterio_ordenar_por']), fn_filtro($_GET['criterio_orden']));
	else
		$sql .= " order by Codigo desc";
	$paging->agregarConsulta($sql); 
	$paging->div('div_listar');
	$paging->modo('desarrollo'); 
	if (isset($_GET['criterio_mostrar']))
		$paging->porPagina(fn_filtro((int)$_GET['criterio_mostrar']));
	$paging->verPost(true);
	$paging->mantenerVar("criterio_usu_per", "criterio_ordenar_por", "criterio_orden", "criterio_mostrar");
	$paging->ejecutar();
?>
<table id="grilla" class="lista" width="420">
  <thead>
        <tr>
            <th width="97">C&oacute;digo</th>
            <th width="138">Nombre Marca</th>
            <th width="69"></th>
            <th width="96" align="left"><a href="javascript: fn_mostrar_frm_agregar();"><img src="img/mantenimiento/add.png" border="0"></a></th>
        </tr>
    </thead>
    <tbody>
    <?
        while ($rs_per = $paging->fetchResultado()){
    ?>
        <tr id="tr_<?=$rs_per['Codigo']?>">
            <td><?=$rs_per['Codigo']?></td>
            <td><?=$rs_per['nomMarca']?></td>
            <td><a href="javascript:fn_mostrar_frm_modificar(<?=$rs_per['nomMarca']?>);"><img src="img/mantenimiento/page_edit.png" border="0" /></a></td>
            <td><a href="javascript:fn_eliminar(<?=$rs_per['nomMarca']?>);"><img src="img/mantenimiento/delete.png" border="0" /></a></td>
        </tr>
    <? } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3">
				<? /*
					-- Aqui MOSTRAMOS MAS DETALLADAMENTE EL PAGINADO
					P�gina: <?=$paging->numEstaPagina()?>, de <?=$paging->numTotalPaginas()?><br />
					Mostrando: <?=$paging->numRegistrosMostrados()?> registros, del <?=$paging->numPrimerRegistro()?> al <?=$paging->numUltimoRegistro()?><br />
					De un total de: <?=$paging->numTotalRegistros()?><br />
                */ ?>
                <?=$paging->fetchNavegacion()?>
            </td>
        </tr>
    </tfoot>
</table>