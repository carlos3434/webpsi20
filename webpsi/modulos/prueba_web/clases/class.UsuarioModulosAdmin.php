<?php
include_once $_SERVER['DOCUMENT_ROOT']."class.Conexion.php";


class UsuarioModulosAdmin {
    private $id = '';

    public function getId() {
        return $this->id;
    }

    public function setId($x) {
        $this->id = $x;
    }
	
    public function getId_Area() {
        return $this->Id_Area;
    }

    public function setId_Area($x) {
        $this->Id_Area = $x;
    }

    public function ListadoModulosAdminUsuario($idUsuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();
        
		/*$cad = "SELECT m.modulo_admin, m.idmodulo_admin FROM `submodulos_admin` s, `modulos_admin` m
		WHERE s.`idmodulo_admin`=m.`idmodulo_admin` GROUP BY 1 ORDER BY 1 ; ";
		*/
		$cad = "SELECT 
			  m.modulo_admin,
			  m.idmodulo_admin 
			FROM
			  `submodulos_admin` s,
			  `modulos_admin` m, `usuarios_submodulos_admin` h
			WHERE s.`idmodulo_admin` = m.`idmodulo_admin` 
			AND h.`idsubmodulo_admin`=s.`idsubmodulo_admin`
			AND h.`idusuario` = ".$idUsuario."
			GROUP BY 1 
			ORDER BY 1 ;" ;

		//echo $cad;
		$arr = array();
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

    public function ListadoSubModulosAdminUsuario($idModulo, $idUsuario) {
        $db = new Conexion();
        $cnx = $db->conectarBD();

		$cad = "SELECT m.modulo_admin, s.submodulo_admin, s.`path` 
				FROM `submodulos_admin` s, `modulos_admin` m, `usuarios_submodulos_admin` h
				WHERE s.`idmodulo_admin`=m.`idmodulo_admin` 
				AND s.idmodulo_admin = ".$idModulo."
				AND h.`idsubmodulo_admin`=s.`idsubmodulo_admin`
				AND h.`idusuario` = ".$idUsuario."
				ORDER BY 1,2; ";
		//echo $cad;
        $res = mysql_query($cad, $cnx);
        while ($row = mysql_fetch_array($res))
        {
            $arr[] = $row;
        }
        //var_dump($row);
        $cnx = NULL;
        $db = NULL;
        return $arr;
    }

	
}