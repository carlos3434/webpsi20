Historia
Somos una organizaci�n peruana que se fund� en el a�o 1994 como una nueva alternativa en la provisi�n de productos y servicios de tecnolog�a inform�tica. A lo largo de estos a�os nuestro crecimiento se ha orientado al desarrollo, implementaci�n de infraestructura y soluciones Integradas de Inform�tica de diversas organizaciones de diferentes sectores. Esta valiosa experiencia nos permite seguir fortaleciendo a nuestros clientes en sus procesos de modernizaci�n tecnol�gica, optimizando sus procesos, potenciando sus recursos e incrementando su rentabilidad. 

Mision
Ser un grupo de profesionales capaces, ofreciendo e implementando con una metodolog�a adecuada las mejores soluciones tecnol�gicas que aporte el valor agregado necesario las empresas, elevando su productividad y de esta manera ayudarlos a mejorar sus procesos de negocios para que puedan alcanzar sus metas y obtener la rentabilidad deseada.
Vision
Servir cada vez a un mayor n�mero de usuarios como l�der, al ofrecer la mejor experiencia de compra para el cliente y el mejor lugar para trabajar para nuestros colaboradores, derivado de una constante innovaci�n. Asimismo convertirnos en la mejor empresa de comercializaci�n de productos inform�ticos.

