<?php

if(!defined('VALID_ACL_')) exit('direct access is not allowed.');

$ACL_LANG = array (
		'USERNAME'			=>	'Usuario',
		'EMAIL'				=>	'',
		'PASSWORD'			=>	'Password',
		'LOGIN'				=>	'Ingresar',
		'SESSION_ACTIVE'	=>	'Su sesion ya esta activa actualmente. click <a href="'.SUCCESS_URL.'">aqui</a> para continuar.',
		'LOGIN_SUCCESS'		=>	'Se ha logueado correctamente, click <a href="'.SUCCESS_URL.'">aqui</a> para continuar.',
		'LOGIN_FAILED'		=>	'Error de ingreso:  '.((LOGIN_METHOD=='user'||LOGIN_METHOD=='both')?'Usuario ':''). 
								((LOGIN_METHOD=='both')?'/':'').
								((LOGIN_METHOD=='e-mail'||LOGIN_METHOD=='both')?'email':'').
								' y clave .',
	);
?>