// Funciones de los botones

function pcba(){
	$('#cronometro_prueba').show();
	IniciarCrono();
	
	var datos = $("#form").serialize();
	
	$("#resultadoPrueba").html("<br /><br /><span>Realizando prueba <img src=\"img/loading.gif\" /></span> ");
	
	$.post("pcba.php", datos, function (data){
		$("#resultadoPrueba").html(data);		
		DetenerCrono();
	});	
	
}
		
function historico(){
	$('#cronometro_prueba').hide();
	var datos = $("#form").serialize();
	$("#resultadoPrueba").html("<br /><br /><span>Realizando prueba <img src=\"img/loading.gif\" /></span> ");
	
	$.post("historico.php", datos, function (data){
		$("#resultadoPrueba").html(data);
	});
}

function buscar_pcba(id){
	$('#cronometro_prueba').hide();
	var datos = $("#form").serialize();

	$("#resultadoPrueba").html("<br /><br /><span>Realizando prueba <img src=\"img/loading.gif\" /></span> ");
	
	$.post("pcba_bd.php", {id:id} , function (data){
		$("#resultadoPrueba").html(data);
	});
}

function reporteador(){
	$('#cronometro_prueba').hide();
	window.open("reporteExcel_pcba.php");

}

