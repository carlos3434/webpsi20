<?php

	function leerxmldatos($campo){

		$pcbas = $_SESSION["xml"] -> getElementsByTagName("PRUEBAS_PCBA");
		$i = 0;
		while($pcba = $pcbas->item($i++)){
			$dslams = $pcba -> getElementsByTagName("DatosEntrada");
			$j = 0;
				while($dslam = $dslams->item($j++)){
					$senals = $dslam -> getElementsByTagName($campo);
					$k = 0;
						while($senal = $senals->item($k++)){
							return $senal->nodeValue;
						}
				}
		}

	}
	
	function leerxmlprueba($campo1,$campo2,$campo3){
		
		$pcbas = $_SESSION["xml"] -> getElementsByTagName("PRUEBAS_PCBA");
		$i = 0;
		while($pcba = $pcbas->item($i++)){
			$dslams = $pcba -> getElementsByTagName($campo1);
			$j = 0;
			while($dslam = $dslams->item($j++)){
				$senals = $dslam -> getElementsByTagName($campo2);
				$k = 0;
				while($senal = $senals->item($k++)){
					$estados = $senal -> getElementsByTagName($campo3);
					$m = 0;
					while($estado = $estados->item($m++)){
						 return $estado->nodeValue;
					}
				}
			}
		}
	}
	
	function resultado($campo){

	$error=0;
	$cadena="";
	$texto="";
	$br="";

	$senaldown=leerxmlprueba("PruebaDslam","SenalRuidoBajada","Descripcion");
	$atedown=leerxmlprueba("PruebaDslam","AtenuacionDownstream","Descripcion");

	if($senaldown==0){
		$cadena = $cadena." Error: senal de ruido de bajada = 0";
		$texto = $texto.$br." Error: senal de ruido de bajada = 0";
		$br="<br>";
		$error=1;
	}else{
		if($senaldown==''){
			$cadena = $cadena." Error: senal de ruido de bajada tiene valor nulo.";
			$texto = $texto.$br." Error: senal de ruido de bajada tiene valor nulo.";
			$br="<br>";
			$error=1;
		}
	}

	if($atedown==0){
		$cadena = $cadena." Error: atenuacion downstream = 0";
		$texto = $texto.$br." Error: atenuacion downstream = 0";
		$br="<br>";
		$error=1;
	}else{
		if($atedown==''){
			$cadena = $cadena." Error: atenuacion downstream de valor nulo.";
			$texto = $texto.$br." Error: atenuacion downstream de valor nulo.";
			$br="<br>";
			$error=1;
		}else{
			if($atedown==6){
				$cadena = $cadena." Error: atenuacion downstream = 6";
				$texto = $texto.$br." Error: atenuacion downstream = 6";
				$br="<br>";
				$error=1;
			}
		}
	}

	if($error==0){
		if($senaldown>12){
			$cadena = $cadena." Senal de Ruido de Bajada -> OK";
			$texto = $texto.$br." Senal de Ruido de Bajada -> OK";
			$br="<br>";
			$error1=1;
		}else{
			$cadena = $cadena." No paso la prueba. Senal de Ruido de Bajada < 12";
			$texto = $texto.$br." No paso la prueba. Senal de Ruido de Bajada < 12";
			$br="<br>";
			$error1=0;
		}
		if($atedown<65){
			$cadena = $cadena." Atenuacion Downstream -> OK";
			$texto = $texto.$br." Atenuacion Downstream -> OK";
			$br="<br>";
			$error2=1;
		}else{
			$cadena = $cadena." No paso la prueba. Atenuacion Downstream > 65";
			$texto = $texto.$br." No paso la prueba. Atenuacion Downstream > 65";
			$br="<br>";
			$error2=0;
		}	
	}
	
	$ee=$error1+$error2;
	
	if($ee==2){
		$resumen="OK";
	}else{
		$resumen="NO OK";
	}
	
	if($campo==1){
		return $cadena;
	}else{
		if($campo==2){
			return $resumen;
		}else{
			return $texto;
		}
	}
	
	}
	
?>