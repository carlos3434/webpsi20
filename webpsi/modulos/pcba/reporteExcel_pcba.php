<?php
header("Content-type: text/csv");  
header("Cache-Control: no-store, no-cache");  
header('Content-Disposition: attachment; filename="pcba_historico_reporte.csv"');  
  
$file = fopen("php://output",'w'); 

require_once("../../clases/class.Conexion.php");

$objCnx = new Conexion();
$db = $objCnx->conectarBD();


function fputcsv_comilla($filePointer,$dataArray,$delimiter,$enclosure)
  {
  // Write a line to a file
  // $filePointer = the file resource to write to
  // $dataArray = the data to write out
  // $delimeter = the field separator
 
  // Build the string
  $string = "";
 
  // No leading delimiter
  $writeDelimiter = FALSE;
  foreach($dataArray as $dataElement)
   {
    // Replaces a double quote with two double quotes
    $dataElement=str_replace("\"", "\"\"", $dataElement);
   
    // Adds a delimiter before each field (except the first)
    if($writeDelimiter) $string .= $delimiter;
   
    // Encloses each field with $enclosure and adds it to the string
    $string .= $enclosure . $dataElement . $enclosure;
   
    // Delimiters are used every time except the first.
    $writeDelimiter = TRUE;
   } // end foreach($dataArray as $dataElement)
 
  // Append new line
  $string .= "\n";
 
  // Write the string to the file
  fwrite($filePointer,$string);
}


$arr_titulos = array( 
				'codArea',
				'telefono',
				'horaInicio',
				'horaTermino',
				'solId',
				'pruebaEstado',
				'pruebaDescripcion',
				'perfilEstado',
				'perfilDescripcion',
				'velocidadActualSubidaEstado',
				'velocidadActualSubidaDescripcion',
				'velocidadActualBajadaEstado',
				'velocidadActualBajadaDescripcion',
				'velocidadMaximaSubidaDescripcion',
				'velocidadMaximaBajadaDescripcion',
				'senalRuidoSubidaEstado',
				'senalRuidoSubidaDescripcion',
				'senalRuidoBajadaEstado',
				'senalRuidoBajadaDescripcion',
				'atenuacionUpStreamDescripcion',
				'atenuacionDownStreamDescripcion',
				'ocupacionUpStreamEstado',
				'ocupacionUpStreamDescripcion',
				'ocupacionDownStreamEstado',
				'ocupacionDownStreamDescripcion',
				'vendorDescripcion',
				'dslamDescripcion',
				'posicionDescripcion',
				'mdfDescripcion',
				'portTypeDescripcion',
				'callingStationIdDescripcion',
				'vpiVciModemDescripcion',
				'vpiVciRedDescripcion',
				'userNameDescripcion',
				'ispLoginDescripcion',
				'ispPasswordDescripcion',
				'nasIpAddressERXDescripcion',
				'numeroInscripcionDescripcion',
				'estadoCuentaDescripcion',
				'nasPortIdDescripcion',
				'codigoProductoDescripcion',
				'descripcionServicioDescripcion',
				'virtualRouterEstado',
				'virtualRouterDescripcion',
				'resultado',
				'resumen',
				'resultadoHtml',
				'tiempoTranscurrido'
			);

$reqExcel = "SELECT 
			codArea,
			telefono,
			horaInicio,
			horaTermino,
			solId,
			pruebaEstado,
			pruebaDescripcion,
			perfilEstado,
			perfilDescripcion,
			velocidadActualSubidaEstado,
			velocidadActualSubidaDescripcion,
			velocidadActualBajadaEstado,
			velocidadActualBajadaDescripcion,
			velocidadMaximaSubidaDescripcion,
			velocidadMaximaBajadaDescripcion,
			senalRuidoSubidaEstado,
			senalRuidoSubidaDescripcion,
			senalRuidoBajadaEstado,
			senalRuidoBajadaDescripcion,
			atenuacionUpStreamDescripcion,
			atenuacionDownStreamDescripcion,
			ocupacionUpStreamEstado,
			ocupacionUpStreamDescripcion,
			ocupacionDownStreamEstado,
			ocupacionDownStreamDescripcion,
			vendorDescripcion,
			dslamDescripcion,
			posicionDescripcion,
			mdfDescripcion,
			portTypeDescripcion,
			callingStationIdDescripcion,
			vpiVciModemDescripcion,
			vpiVciRedDescripcion,
			userNameDescripcion,
			ispLoginDescripcion,
			ispPasswordDescripcion,
			nasIpAddressERXDescripcion,
			numeroInscripcionDescripcion,
			estadoCuentaDescripcion,
			nasPortIdDescripcion,
			codigoProductoDescripcion,
			descripcionServicioDescripcion,
			virtualRouterEstado,
			virtualRouterDescripcion,
			resultado,
			resumen,
			resultadoHtml,
			tiempoTranscurrido
			-- ,b.usuario
			-- ,CONCAT(b.apellido, ' ', b.nombre) as nombre_usuario
			FROM pcba.pruebatecnicaadsl a
			-- , envio_sms.tb_usuario b WHERE b.id=a.idusuario ; ";
			
$resultat = mysql_query($reqExcel, $db) or die(mysql_error($db));


// Escribir titulos
fputcsv_comilla($file, $arr_titulos, ",", "\"");

// Escribir data
while ($row = mysql_fetch_row($resultat)) {
      fputcsv_comilla($file, $row, ",", "\"");
}




fclose($file);

?>

