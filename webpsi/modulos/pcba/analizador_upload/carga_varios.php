<?php
/**
* Proceso de carga de Averias Liquidadas (BASICA, CATV y ADSL) a la tabla LIQUIDADAS_AVERIA_PROVISION_[MESANO]
* toma como origen de datos las tablas cargadas desde los txt generados por Sistemas
*
* @autor Sergio Miranda C.
* @fecha 2013-09-30
* @modificado 2014-02-05

*/

/* Inicia el proceso */
echo date("d-m-Y H:i:s") . " - INICIO \n";


$mensaje = "";
$resultado = null;
$conexion = null;
$fechaUpload = date("Y-m-d H:i:s");
$host = 'localhost';
$userHost = "initium_procesos";
$pwHost = "oRovvk";
$dbHost = "pruebas_electricas";

$rutaArchivoPruebas = "files/";
$tablaDiario = "BD_pruebas_diario";
$tablaEstructura = "BD_pruebas_estructura"; 

function conectarbd($host, $userHost, $pwHost, $dbHost) {
    try {
        $db = new PDO("mysql:host=$host;dbname=$dbHost", "$userHost", "$pwHost");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return($db);
    } catch (PDOException $e) {
        //cabecera('Error grave');
        print "Error: No puede conectarse con la base de datos.\n";
        //pie();
        exit();
    }
}

function listar_archivos($carpeta) {
    $arr[] = "";
    if(is_dir($carpeta)){
        if($dir = opendir($carpeta)){
            while(($archivo = readdir($dir)) !== false){
                if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
                    $arr[] = $archivo;
                }
            }
            closedir($dir);
        }
    }
    return $arr;
}

function primerDiaMes($fecha)
{
    $fecha= strtotime($fecha); //Recibimos la fecha y la convertimos a tipo fecha
    $d = date("d",$fecha); //Obtenemos el dia
    $m = date("m",$fecha); //Obtenemos el mes
    $Y = date("Y",$fecha); //Obtenemos el año
    $primerDia = date("Y-m-d", mktime(0, 0, 0,$m, $d-$d +1,$Y)); //Obtenemos el primer dia del mes
    return $primerDia; //Regresamos el valor obtenido
}

function ultimoDiaMes($fecha)
{
    $fecha= strtotime($fecha); //Recibimos la fecha y la convertimos a tipo fecha
    $d = date("d",$fecha); //Obtenemos el dia
    $m = date("m",$fecha); //Obtenemos el mes
    $Y = date("Y",$fecha); //Obtenemos el año
    $ultimoDia = date("Y-m-d", mktime(0, 0, 0,$m+1,$d-$d,$Y)); //Obtenemos el ultimo dia del mes
    return $ultimoDia; //Regresamos el valor obtenido
}



$cnx = conectarbd($host, $userHost, $pwHost, $dbHost);
$tablaDiario = $dbHost.".".$tablaDiario;

$arrFiles = listar_archivos($rutaArchivoPruebas);
//var_dump($arrFiles); //die();

try {
	$cnx->beginTransaction();
		
	foreach ($arrFiles as $archivo) {

		echo "\n".$archivo;
		if ($archivo=="") 
			echo "\n\nVACIO\n";
		else {
		
			$archivoPruebas = $rutaArchivoPruebas.$archivo;

			echo "\n\n**********************************************";
			echo "\n** Archivo: ".$archivoPruebas."\n"; 

			echo "\n## Borrando tabla del dia --> ".$tablaDiario;
			$c_borrar_diario = "TRUNCATE TABLE ".$tablaDiario;
			$c_borrar_diario = $cnx->query($c_borrar_diario);

			echo "\n## Llenando tabla del dia --> ".$tablaDiario;

			$c_load = "LOAD DATA INFILE :archivo
						IGNORE INTO TABLE ".$tablaDiario."
						FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\r\n' SET fecha_upload=:fechaUpload  ;";

			$r_load = $cnx->prepare($c_load);
			$r_load->bindParam(":archivo", $archivoPruebas );
			$r_load->bindParam(":fechaUpload", $fechaUpload );
			$r_load->execute();

			$c_load2 = "DELETE FROM ".$tablaDiario." WHERE ID_PRUEBA='' OR TIPO='TIPO' ;";
			$r_load2 = $cnx->query($c_load2);

			echo "\n## Llenando tabla del dia --> ".$tablaDiario." --> OK";


			echo "\n## Buscando año y mes del archivo diario ## ";

			$c1 = "SELECT YEAR(STR_TO_DATE(FECHA_PRUE, '%d-%m-%Y')) AS anyo_prueba, 
			MONTH(STR_TO_DATE(FECHA_PRUE, '%d-%m-%Y')) AS mes_prueba,  
			DAY(STR_TO_DATE(FECHA_PRUE, '%d-%m-%Y')) AS dia_prueba
			FROM ".$tablaDiario." LIMIT 1;" ;

			$rs1 = $cnx->query($c1);

			while ($rw1 = $rs1->fetch()) {
				$anyoDia = $rw1[0];
				$mesDia = $rw1[1];
			}

			if (strlen($mesDia)<2)
				$mesDia = "0".$mesDia;

			echo "\n## Buscando año y mes del archivo diario -->  ".$anyoDia." - ".$mesDia;
			echo "\n";

			$tablaAnyoMesActual = "BD_pruebas_";
			$tablaAnyoMesActual = $tablaAnyoMesActual.$anyoDia.$mesDia;

			echo "\n## Tabla a trabajar --> ".$tablaAnyoMesActual;
			echo "\n";

			$c2 = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbHost'
					AND table_name = '".$tablaAnyoMesActual."';";
			$rs2 = $cnx->query($c2);
			$cw2 = 0;

			while ($rw2 = $rs2->fetch()) {
				$cw2++;
			}

			echo "\n";

			if ($cw2 < 1) {
				echo "\n## Tabla ".$tablaAnyoMesActual." NO encontrada. ##";
				echo "\n## Se creara nueva tabla ".$tablaAnyoMesActual;

				$c_create = "CREATE TABLE ".$dbHost.".".$tablaAnyoMesActual." LIKE ".$tablaEstructura;
				$r_create = $cnx->query($c_create);
			}

			echo "\n";
			echo "\n## Se insertaran registros nuevos del dia a tabla acumulada ".$tablaAnyoMesActual;
			echo " --> OK";

			$c_ins = "INSERT IGNORE INTO   ".$dbHost.".".$tablaAnyoMesActual." SELECT * FROM ".$tablaDiario." ; ";
			
			$c_ins = "INSERT IGNORE INTO  ".$dbHost.".".$tablaAnyoMesActual." SELECT *, 0 as AISLA_OK, 0 as VOLTAJE_OK, '' AS PRUEBA_OK
					FROM ".$tablaDiario." ; ";
			
			$r_ins = $cnx->query($c_ins);
			
			
			$c_upd1 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET AISLA_OK=1 WHERE RESULTADO LIKE '%Aislamiento OK%' ";
			$r_upd1 = $cnx->query($c_upd1);

			$c_upd2 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET VOLTAJE_OK=1 WHERE RESULTADO LIKE '%Voltaje OK%'; ";
			$r_upd2 = $cnx->query($c_upd2);

			$c_upd3 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET PRUEBA_OK='OK' WHERE VOLTAJE_OK=1 AND AISLA_OK=1; ";
			$r_upd3 = $cnx->query($c_upd3);

			echo "\n## FIN DE SUBIDA DE DATA A TABLAS DIARIAS Y DE MES ## ";
			echo "\n\n";

	

		}
	}
	
	/*if ($mesDia=="12")
		$mesDiaNext = "01";
	else {
		$mesDiaNext = $mesDia+1;
		if (strlen($mesDiaNext)<2)
			$mesDiaNext = "0".$mesDiaNext;
	}
	*/

	$fecInicial = $anyoDia."-".$mesDia."-"."01";
	$ultDiaMesNext = ultimoDiaMes($fecInicial);

	//echo "\n\n".$ultDiaMesNext;
	$fecFinal   = $ultDiaMesNext;

	echo "\n## CREANDO TABLAS FUENTES DE LIQUIDADAS DE RANGO DE FECHAS: $fecInicial y $fecFinal ## ";
	

	$r_liqayer = $cnx->query("CALL ".$dbHost.".sp_tablas_liq_ayer ('$fecInicial', '$fecFinal'); ");

	echo "-->OK";

	// CALL `sp_liq_ayer_con_prueba`("BD_pruebas_201309");
	echo "\n\n## CREANDO TABLAS DE LIQUIDADAS DE AYER CoN PRUEBAS ## ";
	$r_liqayer = $cnx->query("CALL sp_liq_ayer_con_prueba('".$tablaAnyoMesActual."' , '".$anyoDia.$mesDia."'   ); ");
	echo " -->OK";

	echo "\n\n## CREANDO TABLAS DE LIQUIDADAS DE AYER SiN PRUEBAS ## ";
	$r_liqayer = $cnx->query("CALL sp_liq_ayer_sin_prueba('".$tablaAnyoMesActual."' , '".$anyoDia.$mesDia."'  );");
	echo " -->OK";	
	
	
	$cnx->commit();
	
}
catch (PDOException $e)
{
	echo "\n\n ### ERROR ###";
	print ("Transaction failed: " . $e->getMessage () . "\n");
	$cnx->rollback ();                   # failure
}
	
$cnx = null;


// //ProcessBdException::cerrarConexion();

echo date("d-m-Y H:i:s")." - FIN\n";

