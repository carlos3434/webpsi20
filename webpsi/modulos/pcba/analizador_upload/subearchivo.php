<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("../../../clases/class.Conexion.php");

function listar_archivos($carpeta) {
    $arr[] = "";
    if(is_dir($carpeta)){
        if($dir = opendir($carpeta)){
            while(($archivo = readdir($dir)) !== false){
                if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
                    $arr[] = $archivo;
                }
            }
            closedir($dir);
        }
    }
    return $arr;
}

function primerDiaMes($fecha)
{
    $fecha= strtotime($fecha); //Recibimos la fecha y la convertimos a tipo fecha
    $d = date("d",$fecha); //Obtenemos el dia
    $m = date("m",$fecha); //Obtenemos el mes
    $Y = date("Y",$fecha); //Obtenemos el año
    $primerDia = date("Y-m-d", mktime(0, 0, 0,$m, $d-$d +1,$Y)); //Obtenemos el primer dia del mes
    return $primerDia; //Regresamos el valor obtenido
}

function ultimoDiaMes($fecha)
{
    $fecha= strtotime($fecha); //Recibimos la fecha y la convertimos a tipo fecha
    $d = date("d",$fecha); //Obtenemos el dia
    $m = date("m",$fecha); //Obtenemos el mes
    $Y = date("Y",$fecha); //Obtenemos el año
    $ultimoDia = date("Y-m-d", mktime(0, 0, 0,$m+1,$d-$d,$Y)); //Obtenemos el ultimo dia del mes
    return $ultimoDia; //Regresamos el valor obtenido
}




function boton_regresar()
{ ?>
    <p>
        <a href="#" onclick="javascript:inicio()">Regresar</a>
    </p>
<?php    
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>

<script type="text/javascript">
function inicio()
{
    window.location.href = "index.php";
}        
</script>        
        
    </head>
    <body bgcolor='#DBE3FF'>
    <center>
<?php
$fec = '';
$status = "";

if ($_POST["action"] == "upload") {
    // obtenemos los datos del archivo
    $tamano = $_FILES["archivo"]['size'];
    $tipo = $_FILES["archivo"]['type'];
    $archivo = $_FILES["archivo"]['name'];
    $prefijo = substr(md5(uniqid(rand())), 0, 6);
    
	echo $tipo;
	
    $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv', 'application/download');
    if(!in_array($tipo,$mimes)){
        echo "El archivo seleccionado no cumple con el formato requerido.";
        //echo $_FILES['file']['type'];
        boton_regresar();
        die();
    }
    
    
    //if ($archivo != "" and ($tipo == "application/vnd.ms-excel") {
	if ($archivo != "" ) {
        // guardamos el archivo a la carpeta files
        $destino = "files/pruebas_analizador.csv";
        if (copy($_FILES['archivo']['tmp_name'], $destino)) {
            $status1 = 'Data Cargada con extito';
            $status2 = 1;
        } else {
            $status1 = "Error al subir el archivo";
            $status2 = 2;
        }
    } else {
        $status1 = "Error al subir archivo - No eligió archivo o la extensión no es CSV";
        $status2 = 2;
    }
}

echo "<br/>" . $status1;


/* Inicia el proceso */
echo date("d-m-Y H:i:s") . " - INICIO \n";


$mensaje = "";
$resultado = null;
$conexion = null;
$fechaUpload = date("Y-m-d H:i:s");
$dbHost = "pruebas_electricas";

$rutaArchivoPruebas = "files/";
$tablaDiario = "BD_pruebas_diario";
$tablaEstructura = "BD_pruebas_estructura"; 


$tablaDiario = $dbHost.".".$tablaDiario;


echo "<br/>### Listado archivos ..";


$objCnx = new Conexion();
$cnx = $objCnx->conectarPDO();

$archivo = $destino;

try {
	$cnx->beginTransaction();

	echo "\n".$archivo;
	if ($archivo=="") 
		echo "\n\nVACIO\n";
	else {
	
		$archivoPruebasTmp = $archivo;

		echo "<br/>**********************************************";
		echo "<br/>** Archivo: ".$archivoPruebasTmp."<br/>"; 

		copy($archivoPruebasTmp, '/tmp/pruebas_analizador.csv');
		
		$archivoPruebas = '/tmp/pruebas_analizador.csv';
		
		echo "<br/>** Archivo: ".$archivoPruebas."\n"; 
		
		echo "<br/>## Borrando tabla del dia --> ".$tablaDiario;
		$c_borrar_diario = "DELETE FROM ".$tablaDiario;
		$c_borrar_diario = $cnx->query($c_borrar_diario);

		
		$c_load2 = "DELETE FROM ".$tablaDiario." WHERE ID_PRUEBA='' OR TIPO='TIPO' ;";
		$r_load2 = $cnx->query($c_load2);

		
		echo "<br/>## Llenando tabla del dia --> ".$tablaDiario;


		
		$fh = fopen($archivoPruebas, "r") ;
		$i=1;
		while (($data = fgetcsv($fh))) {
			$query = sprintf('INSERT INTO '.$tablaDiario.'  
					VALUES ("%s", "%s", "%s","%s", "%s", "%s","%s", "%s", "%s","%s", "%s", "%s","", "", NOW() )', 
					$data[0], $data[1],$data[2],$data[3],$data[4],$data[5],$data[6], $data[7],$data[8],
					$data[9], $data[10],$data[11] );
			//echo "<br/>".$query;
			$result_q = $cnx->query($query); 
			//echo "<br/>## $i";
			$i++;
		}
		fclose($fh);
		
		echo "<br/>## Llenando tabla del dia --> ".$tablaDiario." --> OK  --- $i registros";


		
		echo "<br/>## Borrando data sucia de la tabla --> ".$tablaDiario;
		$c_borrar_sucia = "DELETE FROM ".$tablaDiario." WHERE TIPO!='PRUEBA'";
		$r_borrar_sucia = $cnx->query($c_borrar_sucia);
		
		echo "<br/>## Buscando año y mes del archivo diario ## ";

		$c1 = "SELECT YEAR(STR_TO_DATE(FECHA_PRUE, '%d/%m/%Y')) AS anyo_prueba, 
		MONTH(STR_TO_DATE(FECHA_PRUE, '%d/%m/%Y')) AS mes_prueba,  
		DAY(STR_TO_DATE(FECHA_PRUE, '%d/%m/%Y')) AS dia_prueba
		FROM ".$tablaDiario." LIMIT 1;" ;

		$rs1 = $cnx->query($c1);

		while ($rw1 = $rs1->fetch()) {
			$anyoDia = $rw1[0];
			$mesDia = $rw1[1];
		}

		if (strlen($mesDia)<2)
			$mesDia = "0".$mesDia;

		echo "<br/>## Buscando año y mes del archivo diario -->  ".$anyoDia." - ".$mesDia;
		echo "<br/>";

		$tablaAnyoMesActual = "BD_pruebas_";
		$tablaAnyoMesActual = $tablaAnyoMesActual.$anyoDia.$mesDia;

		echo "<br/>## Tabla a trabajar --> ".$tablaAnyoMesActual;
		echo "<br/>";

		$c2 = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbHost'
				AND table_name = '".$tablaAnyoMesActual."';";
		$rs2 = $cnx->query($c2);
		$cw2 = 0;

		while ($rw2 = $rs2->fetch()) {
			$cw2++;
		}

		echo "\n";

		if ($cw2 < 1) {
			echo "<br/>## Tabla ".$tablaAnyoMesActual." NO encontrada. ##";
			echo "<br/>## Se creara nueva tabla ".$tablaAnyoMesActual;

			$c_create = "CREATE TABLE ".$dbHost.".".$tablaAnyoMesActual." LIKE ".$tablaEstructura;
			$r_create = $cnx->query($c_create);
		}

		echo "<br/>";
		echo "<br/>## Se insertaran registros nuevos del dia a tabla acumulada ".$tablaAnyoMesActual;
		echo " --> OK";

		$c_ins = "INSERT IGNORE INTO   ".$dbHost.".".$tablaAnyoMesActual." SELECT * FROM ".$tablaDiario." ; ";
		
		$c_ins = "INSERT IGNORE INTO  ".$dbHost.".".$tablaAnyoMesActual." SELECT *, 0 as AISLA_OK, 0 as VOLTAJE_OK, '' AS PRUEBA_OK
				FROM ".$tablaDiario." ; ";
		
		$r_ins = $cnx->query($c_ins);
		
		
		$c_upd1 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET AISLA_OK=1 WHERE RESULTADO LIKE '%Aislamiento OK%' ";
		$r_upd1 = $cnx->query($c_upd1);

		$c_upd2 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET VOLTAJE_OK=1 WHERE RESULTADO LIKE '%Voltaje OK%'; ";
		$r_upd2 = $cnx->query($c_upd2);

		$c_upd3 = "UPDATE ".$dbHost.".".$tablaAnyoMesActual." SET PRUEBA_OK='OK' WHERE VOLTAJE_OK=1 AND AISLA_OK=1; ";
		$r_upd3 = $cnx->query($c_upd3);

		echo "<br/>## FIN DE SUBIDA DE DATA A TABLAS DIARIAS Y DE MES ## ";
		echo "<br/><br/>";



	}

	$fecInicial = $anyoDia."-".$mesDia."-"."01";
	$ultDiaMesNext = ultimoDiaMes($fecInicial);

	$fecFinal   = $ultDiaMesNext;

	echo "<br/>## CREANDO TABLAS FUENTES DE LIQUIDADAS DE RANGO DE FECHAS: $fecInicial y $fecFinal ## ";
	

	$r_liqayer = $cnx->query("CALL ".$dbHost.".sp_tablas_liq_ayer ('$fecInicial', '$fecFinal'); ");

	echo "-->OK";

	// CALL `sp_liq_ayer_con_prueba`("BD_pruebas_201309");
	echo "<br/><br/>## CREANDO TABLAS DE LIQUIDADAS DE AYER CoN PRUEBAS ## ";
	$r_liqayer = $cnx->query("CALL ".$dbHost.".sp_liq_ayer_con_prueba('".$tablaAnyoMesActual."' , '".$anyoDia.$mesDia."'   ); ");
	echo " -->OK";

	echo "<br/><br/>## CREANDO TABLAS DE LIQUIDADAS DE AYER SiN PRUEBAS ## ";
	$r_liqayer = $cnx->query("CALL ".$dbHost.".sp_liq_ayer_sin_prueba('".$tablaAnyoMesActual."' , '".$anyoDia.$mesDia."'  );");
	echo " -->OK";	
	
	
	$cnx->commit();
	
}
catch (PDOException $e)
{
	echo "<br/><br/> ### ERROR ###";
	print ("Transaction failed: " . $e->getMessage () . "\n");
	$cnx->rollback ();                   # failure
}
	
$cnx = null;

boton_regresar();
	

?>
    
    </center>
</body>
</html>



