<?php

include ("../../includes.php") ; 

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript">

function cargaInicial() {
	data_content = "action=cargaInicial";
	$.ajax({
	    type:   "POST",
	    url:    "mapagoogle_receptor.php",
	    data:   data_content,
	    dataType: "json",
	    success: function(datos) {
	    	alert(datos);
	        var tipoPoligonos = datos['arrTipoPoligonos'];
	        var tipoRutas = datos['arrTipoRutas'];
	        var zonales = datos['arrZonales'];
	        var distritos = datos['arrZonales'];
	        var dptos = datos['arrDptos'];
	        var provs = datos['arrProvs'];
	        var dists = datos['arrDists'];
	        var tipoPedidos = datos['arrTipoPedido'];

	        //wsandoval 28/06/2012
	        arr_georef = datos['arrGeoRef'];
	        //fin wsandoval 28/06/2012

	        //carga combo de tipo de poligonos
	        var selectTipoPoligono = '';
	        for(var i in tipoPoligonos) {
	            selectTipoPoligono += '<option value="' + tipoPoligonos[i]['idpoligono_tipo'] + '">' + tipoPoligonos[i]['nombre'] + '</option>'
	        }
	        $("#tipocapaPoligono").append(selectTipoPoligono);

	        //carga combo de tipo de rutas
	        var selectTipoRuta = '';
	        for(var i in tipoRutas) {
	            selectTipoRuta += '<option value="' + tipoRutas[i]['idpoligono_tipo'] + '">' + tipoRutas[i]['nombre'] + '</option>'
	        }
	        $("#tipocapaRuta").append(selectTipoRuta);

	        //carga combo de zonales
	        var selectZonales = '';
	        var selZonales = '';
	        for(var i in zonales) {
	            selectZonales += '<option ' + sel + ' value="' + zonales[i]['desc_zonal'] + '">' + zonales[i]['desc_zonal'] + '</option>'
	            selZonales += '<option ' + sel + ' value="' + zonales[i]['abv_zonal'] + '">' + zonales[i]['abv_zonal'] + '-' + zonales[i]['desc_zonal'] + '</option>'
	        }
	        //Agregando listado de zonales 
	        //div direcion 
	        $("#zonal").append(selectZonales);
	        //div Busq. de terminales  
	        $("#selZonal").append(selZonales);
	        $("#selZonal_AsignarXY_Clientes").append(selZonales);

	        //carga combo de distritos
	        var selectDistritos = '';
	        for(var i in distritos) {
	            selectDistritos += '<option value="' + distritos[i]['distrito'] + '">' + distritos[i]['distrito'] + '</option>'
	        }
	        $("#selDistrito").append(selectDistritos);

	        //carga combo de dptos
	        var selectDptos = '';
	        for(var i in dptos) {
	            var sel = '';
	            if ( dptos[i]['nombre'] == 'LIMA' ) {
	                sel = 'selected';
	            }
	            selectDptos += '<option ' + sel + ' value="' + dptos[i]['coddpto'] + '">' + dptos[i]['nombre'] + '</option>'
	        }
	        $("#dpto").append(selectDptos);
	        $("#ubigeodpto").append(selectDptos);

	        //carga combo de provs
	        var selectProvs = '';
	        for(var i in provs) {
	            var sel = '';
	            if ( provs[i]['nombre'] == 'LIMA' ) {
	                sel = 'selected';
	            }
	            selectProvs += '<option ' + sel + ' value="' + provs[i]['codprov'] + '">' + provs[i]['nombre'] + '</option>'
	        }
	        $("#prov").append(selectProvs);
	        $("#ubigeoprov").append(selectProvs);

	        //carga combo de dists 
	        var selectDists = '';
	        for(var i in dists) {
	            selectDists += '<option value="' + dists[i]['coddist'] + '">' + dists[i]['nombre'] + '</option>'
	        }
	        $("#dist").append(selectDists);
	        $("#ubigeodist").append(selectDists);

	        //carga combo de tipo pedidos 
	        var selectTipoPedido = '';
	        for(var i in tipoPedidos) {
	        	selectTipoPedido += '<option value="' + tipoPedidos[i]['tipoPedido'] + '">' + tipoPedidos[i]['tipoPedido'] + '</option>'
	        }
	        $("#tipo_pedido").append(selectTipoPedido);

	    }
	});
}



function mostrarMapaInicial() {
	var latlng = new google.maps.LatLng(23.1411, -82.487);
	var myOptions = {
		zoom: 14,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(
		document.getElementById("mapa"),
		myOptions
	);

}

$(document).ready(function () {
	/*myLayout = $('body').layout({
	    // enable showOverflow on west-pane so popups will overlap north pane
	    west__showOverflowOnHover: true,
	    west: {size:370}
	});
*/
	mostrarMapaInicial();

	 btnSearchComponentes = function() {
		alert("1");
	    if(parent.$(".capas").is(':checked')) { 
	    } else {  
	        alert("Ingrese algun criterio de busqueda");  
	        return false;  
	    }  

	    var x = parent.$("#x").attr('value');
	    var y = parent.$("#y").attr('value');

	    var query = parent.$("input.capas, select.capas").serializeArray();

	    parent.$("#msgBuscomp").show();
	    data_content = {
	        'action'    : 'search',
	        'x'        : x,
	        'y'        : y,
	        'comp'        : {'trm':'1'}
	    };
		alert("2");
	    $.ajax({
	        type:   "POST",
	        url:    "v_buscomp_popup.php?action=search&x=" + x + "&y=" + y,
	        data:     query,
	        dataType: "json",
	        success: function(data) {

	            //clear markers
	            //L.clearMarkers();

	            var sites = data['sites'];
	            capa_ico = data['capa_ico'];

	            infowindow = new google.maps.InfoWindow({
	                content: "loading..."
	            });

	            if( sites.length > 0) {
	                M.setMarkers(map, sites);
	            }


	            //rutas encontradas 
	            /*
	            var rutas = data['xy_rutas'];
	            if( rutas.length > 0 ) {
	                //pintamos los rutas
	                for (var i in rutas) {
	                    M.loadPolilinea(rutas[i], i);
	                }
	            }
	            */

	            parent.$("#msgBuscomp").hide();
	            parent.$("#childModal").dialog('close');

	        }
	    });        
	}	

var M = {
    initialize: function() {

        if (typeof (google) == "undefined") {
            alert('Verifique su conexion a maps.google.com');
            return false;
        }

        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
        var myOptions = {
            zoom: 11,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


        //carga inicial de combos
        cargaInicial();

        
        google.maps.event.addListener(map, "dblclick", function(event) {
            //clear markers
            M.clearMarkers();
            M.clearMakeMarkers(); //wil

            marker = new google.maps.Marker({
                position: event.latLng,
                map: map
            });
            markers_arr.push(marker);
            var Coords = event.latLng;
            var x = Coords.lng();
            var y = Coords.lat();

            //seteo los x,y actuales 
            x_actual = x;
            y_actual = y;

            //popup, pregunta si quieres buscar o agregar componente
            M.popupAddSearchComponentes(x,y);
        });
        

        //Creacion de ruta 
        google.maps.event.addListener(map, "click", function(event) {

            if (activar_ruta) {
            	var Coords = event.latLng;
                var x = Coords.lng();
                var y = Coords.lat();
            
                xy_arr.push(x + '|' + y);
                
                M.clearRutas();
                M.crearRutaPoligono();

                distancia_ruta = 0;
                M.calculaDistanciaRuta();
            }
        });
        
        google.maps.event.addListener(map, "rightclick", function(event) {

        	if (activar_ruta) {
        		xy_arr.pop();
                
                M.clearRutas();
                M.crearRutaPoligono();

                distancia_ruta = 0;
                M.calculaDistanciaRuta();
        	}
        });
        //FIN Creacion de ruta 
        
    },

    toRad: function(Value) {
        /** Converts numeric degrees to radians */
        return Value * Math.PI / 180;
    },
    
    distanciaEntreDosPuntos: function(lon1, lat1, lon2, lat2) {
    	var R = 6371 * 1000; // km * 1000 = metros 
    	var dLat = M.toRad(lat2-lat1);
    	var dLon = M.toRad(lon2-lon1);
    	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    	        Math.cos(M.toRad(lat1)) * Math.cos(M.toRad(lat2)) *
    	        Math.sin(dLon/2) * Math.sin(dLon/2);
    	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    	var d = R * c;

    	return d;
    },

    calculaDistanciaRuta: function() {
        
        if ( xy_arr.length > 1 ) {
            var xy_array = new Array();

            for( i in xy_arr ) {

                var n_i = parseInt(i) + 1;
            	
            	if ( xy_arr[n_i] != undefined ) {

            		var xy_values = new Array();
                    xy_values = xy_arr[i].split('|');
                    xy_values2 = xy_arr[n_i].split('|');
                    
                    xy_values['x'] = xy_values[0];
                    xy_values['y'] = xy_values[1];

                    var d = M.distanciaEntreDosPuntos(xy_values[0], xy_values[1], xy_values2[0], xy_values2[1]);

                    distancia_ruta = distancia_ruta + d;

            	}
            }

            $('#distancia_ruta').val(distancia_ruta.toFixed(2));
                
        } else {
        	$('#distancia_ruta').val('0.00');
        }
    },

    generaColorPoligono: function() {
        var valores_arr = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
        var color = '';
        for( i=1; i<=6; i++ ) {
            var rand =  Math.ceil(Math.random()*15);
            color += valores_arr[rand];
        }
        return '#' + color;
    },

    clearRutas: function() {
        for (var n = 0, rutaDibujar; rutaDibujar = rutaDibujar_array[n]; n++) {
        	rutaDibujar.setMap(null);
        }
    },

    agregarTerminalARuta: function(mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, x, y) {
        
        if( xy_arr.length < 2 ) {
            alert("Debe crear su ruta antes de agregar el terminal.");
            return false;
        }

        //limpio array donde almaceno los arrays
        comp_trm_arr = [];
        num_trm = 0;

        for( var u=0 in comp_trm_arr ) {
            if( comp_trm_arr[u] == mtgespktrm ) {
                alert("Terminal ya agregado");
                return false;
            }
        }
        num_trm++;
        comp_trm_arr.push(mtgespktrm);

        var cableArmario = '';
        if ( tipo_red == 'D' ) {
        	cableArmario = cable;
        } else if ( tipo_red == 'F' ) {
        	cableArmario = armario;
        }
        
        var content = '';
        content += '<tr>';
        content += '<td>' + num_trm + '</td>';
        content += '<td>' + zonal + ' - ' + mdf + ' - ' + cableArmario + ' - ' + caja + ' - ' + tipo_red + '</td>';
        content += '</tr>';

        //seteando las fftt 
        $('#a_zonal').val(zonal);
        $('#a_mdf').val(mdf);
        $('#a_cable').val(cable);
        $('#a_armario').val(armario);
        $('#a_caja').val(caja);
        $('#a_tipo_red').val(tipo_red);
        $('#a_terminal_x').val(x);
        $('#a_terminal_y').val(y);

        //$("#tb_resultado_trm tbody").append(content);
        $("#tb_resultado_trm tbody").html(content);

    },
    
    loadRuta: function(xy_array) {

        var datos           = xy_array['coords'];
        var color_polilinea = xy_array['color'];
        var polilineaCoords = [];

        if ( datos.length > 0) {
            // SERGIO 2013-10-14
            for (var i in datos) {
                var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                polilineaCoords.push(pto_polilinea);
            }

            // Construct the polyline                
            polilineaDibujar = new google.maps.Polyline({
                path: polilineaCoords,
                strokeColor: color_polilinea,
                strokeOpacity: 1.0,
                strokeWeight: 4
            });

            //agrego al array las capas seleccionadas
            rutaDibujar_array.push(polilineaDibujar);

            polilineaDibujar.setMap(map);                
        }    
    },
    
    crearRutaPoligono: function() {
        
        if ( xy_arr.length > 0 ) {
            var xy_array = new Array();

            for( i in xy_arr ) {
                var xy_values = new Array();
                xy_values = xy_arr[i].split('|');
                
                xy_values['x'] = xy_values[0];
                xy_values['y'] = xy_values[1];
                
                xy_array.push(xy_values);
            }
            
            var xy_total_array = new Array();
            
            xy_total_array['coords'] = xy_array;
            //alert(M.generaColorPoligono());
            xy_total_array['color'] = M.generaColorPoligono();

            
            M.loadRuta(xy_total_array);
                
        }
        else {
            alert('No existen puntos seleccionados para crear la ruta');
        }
    
    },

    grabarRutaAcometida: function() {

    	var chkPedido = 0;
    	if($("#chkPedido").is(':checked')) { 
    		chkPedido = 1;
        }

    	//validando que exista la ruta
        if ( xy_arr.length < 2 ) {
        	alert("No tiene ingresada una ruta");
            return false;
        }
        if( $("#x_cliente").val() == '' && $("#y_cliente").val() == '' ) {
            alert("No se ha registrado el x,y del cliente");
            return false;
        }
        if( $("#a_caja").val() == '' ) {
            alert("No se ha registrado el terminal");
            return false;
        }
        if( $("#distancia_ruta").val() == '' ) {
            alert("No se ha registrado la distancia de la ruta");
            return false;
        }
        if( $("#tipo_pedido").val() == '' ) {
            alert("Ingrese el tipo pedido");
            return false;
        }
        if( $("#pedido").val() == '' ) {
            alert("Ingrese el pedido");
            return false;
        }

        if ( !confirm("Seguro de grabar los cambios?") ) {
            return false;
        }
        
        var data_content = {
        	cliente_x                   : $("#x_cliente").val(),
        	cliente_y                   : $("#y_cliente").val(),
            zonal                       : $("#a_zonal").val(),
	    	mdf                         : $("#a_mdf").val(),
	    	cable                       : $("#a_cable").val(),
	    	armario                     : $("#a_armario").val(),
	    	terminal                    : $("#a_caja").val(),
	    	tipo_red                    : $("#a_tipo_red").val(),
	    	terminal_x                  : $("#a_terminal_x").val(),
	    	terminal_y                  : $("#a_terminal_y").val(),
	    	ruta                        : xy_arr,
	    	distancia_acometida         : $("#distancia_ruta").val(),
	    	tipo_pedido                 : $("#tipo_pedido").val(),
	    	pedido                      : $("#pedido").val(),
	    	chkPedido                   : chkPedido
        };


        $("#box_loading2").show();
        $("input[id=btnGrabarRutaAcometida]").attr('disabled','disabled');
        pr = $.post("../gescomp.poligono.php?action=grabarRutaAcometida", data_content, function(data){

            if ( data != null ) {
            
                if (data['flag'] == '1'){
                    alert(data['mensaje'] + "\n\nCodigo: " + data['codGen']);
                    
                    $('#divBuscarComponentes').show();
                    $('#divCrearRutaAcometida').hide();

                    //limpiando las variables 
                    $('.clsAcometida').val('');
                    $('#xy_cliente').html('');
                    $('#tb_resultado_trm tbody').empty();
                    $('#tipo_pedido').val('');
                    x_actual = null;
                    y_actual = null;
                    xy_arr = [];
                    activar_ruta = 0;

                    //limpiando la ruta 
                    M.clearRutas();

                    //limpiando los markers 
                    M.clearMarkers();

                    //desactivo los checked de 
                    $('#activar_ruta').attr('checked', false);
                    $('#crear_ruta_acometida').attr('checked', false);

                } else if(data['flag'] == '2') {
                	alert(data['mensaje']);
                } else if(data['flag'] == '3') {
                	alert(data['mensaje']);
                } else{
                    alert(data['mensaje']);
                }
            } else {
                alert('Se registro un pedido no valido o se produjo un error al grabar los datos');
            }
            
            $("#box_loading2").hide();
            $("input[id=btnGrabarRutaAcometida]").attr('disabled','');
        },'json');

    },

    setMarkers: function(map, markers) {

        var avg = {
            lat: 0,
            lng: 0
        };

        for (var i = 0; i < markers.length; i++) {
            var sites = markers[i];
            var siteLatLng = new google.maps.LatLng(sites['y'], sites['x']);

            var ico = capa_ico[sites['tip']];


            var icono = '';
            var est_caja = '';
            if( sites['tip'] == 'trm' ) {
                est_caja = sites['colorTrm'];
                icono = "img/map/terminal/trm_" + sites['title'] + "--" + est_caja + ".gif";
            }else if( sites['tip'] == 'exa' ) {
                icono = "img/map/posiblesclientes/posiblecliente-3.gif";
            }else if( sites['tip'] == 'arm' ) {
                icono = "img/map/armario/arm_" + sites['title'] + "--" + sites['estado'] + ".gif";
            }else if( sites['tip'] == 'mdf' ) {
                icono = "img/map/mdf/mdf_" + sites['title'] + "--" + sites['estado'] + ".gif";
            }else if( sites['tip'] == 'trb' ) {
                icono = "img/map/troba/trb-r.gif";
            }else if( sites['tip'] == 'tap' ) {
                icono = "img/map/tap/tap_" + sites['title'] + "--" + sites['estado'] + ".gif";
            }else if( sites['tip'] == 'edi' ) {
                icono = "img/map/edificio/edificio-" + sites['estado'] + ".gif";
            }else if( sites['tip'] == 'com' ) {
                icono = "img/map/competencia/" + ico;
            }else if( sites['tip'] == 'esb' ) {
                icono = "img/map/estacionbase/esb-1.gif";
            }else if( sites['tip'] == 'pcl' ) {
                icono = "img/map/posiblesclientes/posiblecliente-3.gif";
            }else if( sites['tip'] == 'loc' ) {
                icono = "img/map/localidades/localidades.png";
            }else if( sites['tip'] == 'tpi' ) {
                icono = "img/map/tpi/tpi-" + sites['estado'] + ".gif";
            }else if( sites['tip'] == 'dad' ) {
                icono = "img/map/dunaadsl/pirata-1.gif";
            }
            else {
                icono = "vistas/icons/" + ico;
            }                

            var marker = new google.maps.Marker({
                position: siteLatLng,
                map: map,
                title: sites['title'],
                zIndex: sites['i'],
                html: sites['detalle'],
                icon: icono
            });

            markers_arr.push(marker);

            var contentString = "Some content";

            google.maps.event.addListener(marker, "click", function () {
                //alert(this.html);
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            });

            avg.lat += siteLatLng.lat();
            avg.lng += siteLatLng.lng();
        }

        // Center map.
        map.setCenter(new google.maps.LatLng(avg.lat / markers.length, avg.lng / markers.length));

    },

    buscarProvincias: function(alias) {
        $("select[name=" + alias + "prov]").html("<option value=''>Seleccione</option>");
        $("select[name=" + alias + "dist]").html("<option value=''>Seleccione</option>");
        data_content = {
            'action'    : 'buscarProvincias',
            'dpto'      : $('#' + alias + 'dpto').val()
        };
        $.ajax({
            type:   "POST",
            url:    "../gescomp.poligono.php",
            data:   data_content,
            dataType: "json",
            success: function(data) {

                $.each(data, function(i, item) {
                    $("select[name=" + alias + "prov]").append("<option value='"+data[i]['codprov']+"'>"+data[i]['nombre']+"</option>")
                });

            }});
    },

    buscarDistritos: function(alias) {
        $("select[name=" + alias + "dist]").html("<option value=''>Seleccione</option>");
        data_content = {
            'action'    : 'buscarProvicniasDistritos',
            'dpto'      : $('#' + alias + 'dpto').val(),
            'prov'      : $('#' + alias + 'prov').val()
        };
        $.ajax({
            type:   "POST",
            url:    "../gescomp.poligono.php",
            data:   data_content,
            dataType: "json",
            success: function(data) {

                $.each(data, function(i, item) {
                    $("select[name=" + alias + "dist]").append("<option value='"+data[i]['coddist']+"'>"+data[i]['nombre']+"</option>")
                });

            }});
    },

    SearchXY: function() {
        var xy = $("#xy").attr('value');
        var punto = xy.split(',');

        //validando si es una coordenada valida
        if( !esCoordenada(xy) ) {
            alert("Coordenada no valida!");
            return false;
        }
        $('#box_loading').show();

        M.clearMarkers();
        M.clearMakeMarkers(); //wil

        //setea el zoom y el centro
        map.setCenter(new google.maps.LatLng(punto[1], punto[0]));
        map.setZoom(16);

        var pto_xy = new google.maps.LatLng(punto[1], punto[0]);

        marker = new google.maps.Marker({
            position: pto_xy,
            map: map,
            title: 'Punto seleccionado',
            html: 'Punto Seleccionada',
            icon: "../../../modulos/maps/vistas/icons/markergreen.png"
        });
        markers_arr.push(marker);

        $('#box_loading').hide();
    },

    clearMarkers: function() {
        for (var n = 0, marker; marker = markers_arr[n]; n++) {
            marker.setVisible(false);
        }
    },

    clearMakeMarkers: function() {
        for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
            makeMarker.setVisible(false);
        }
    },

    clearPoligonos: function() {
        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
            poligonoDibujar.setMap(null);
        }
    },

    clearPolilineas: function() {
        for (var n = 0, polilineaDibujar; polilineaDibujar = polilineasDibujar_array[n]; n++) {
            polilineaDibujar.setMap(null);
        }
    },

    popupComponentes: function(x, y) {
		alert("9");
        var zoom_selected = map.getZoom();

        $("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");
		alert("popupComponentes");
        $.post("v_buscomp_popup.php", {
            x: x,
            y: y, 
            zoom_selected: zoom_selected
        },
        function(data){
            $("#childModal").html(data);
        }
        );

        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
    },

    popupAddSearchComponentes: function(x, y) {
        var zoom_selected = map.getZoom();
		
		// SERGIO 2013-05-06
        //parent.$("#childModal").html('');
        //parent.$("#childModal").css("background-color","#FFFFFF");
		$("#childModal").html('');
        $("#childModal").css("background-color","#FFFFFF");

        $.post("v_buscaragregar_comp_popup.php", {
            x: x,
            y: y, 
            zoom_selected: zoom_selected
        },
        function(data){
			// SERGIO 2013-05-06
			//parent.$("#childModal").html(data);
            $("#childModal").html(data);
        }
        );
		// SERGIO 2013-05-06
        //parent.$("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'INFORMACI&Oacute;N DE LAS CAPAS', position:'top'});
		$("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'INFORMACI&Oacute;N DE LAS CAPAS', position:'top'});
    },

    loadPoligono: function(xy_array, z) {

        var datos             = xy_array['coords'];
        var color_poligono     = xy_array['color'];
        var tipo             = xy_array['tipo'];
        var nombre             = xy_array['nombre'];
        var poligonoCoords     = [];

        if( datos.length > 0) {
            // SERGIO 2013-10-14
            for (var i in datos) {
                var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                poligonoCoords.push(pto_poligono);
            }

            poligonoDibujar = new google.maps.Polygon({
                paths: poligonoCoords,
                strokeColor: color_poligono,
                strokeOpacity: 0.6,
                strokeWeight: 3,
                fillColor: color_poligono,
                fillOpacity: 0.5
            });

            poligonosDibujar_array.push(poligonoDibujar);

            poligonoDibujar.setMap(map);


            var infowindow = new google.maps.InfoWindow();

            // Add a listener for the click event
            google.maps.event.addListener(poligonoDibujar, 'rightclick', function(event) {
                infowindow.close();

                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                infowindow.setContent(contentString);
                infowindow.setPosition(event.latLng);

                infowindow.open(map);
            });


            //agrego el evento click en cada capa
            google.maps.event.addListener(poligonoDibujar, 'click', function(capaEvent) {
       
                //clear markers
                M.clearMarkers();
                M.clearMakeMarkers();

                marker = new google.maps.Marker({
                    position: capaEvent.latLng,
                    map: map
                });

                markers_arr.push(marker);
                var Coords = capaEvent.latLng;
                var x = Coords.lng();
                var y = Coords.lat();

                //popup, pregunta si quieres buscar o agregar componente
                M.popupAddSearchComponentes(x,y);
            });

            //seteamos que la capa existe
            capa_exists = 1;

        }
        else {
            //capa_exists = 0;
            //M.clearPoligonos();
        }        
    },

    loadPolilinea: function(xy_array) {

        var datos             = xy_array['coords'];
        var color_polilinea = xy_array['color'];
        var tipo             = xy_array['tipo'];
        var nombre             = xy_array['nombre'];
        var polilineaCoords = [];

        if( datos.length > 0) {
            // SERGIO 2013-10-14
            for (var i in datos) {
                var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                polilineaCoords.push(pto_polilinea);
            }

            // Construct the polyline                
            polilineaDibujar = new google.maps.Polyline({
                path: polilineaCoords,
                strokeColor: color_polilinea,
                strokeOpacity: 1.0,
                strokeWeight: 4
            });

            //agrego al array las capas seleccionadas
            polilineasDibujar_array.push(polilineaDibujar);

            polilineaDibujar.setMap(map);


            var infowindow = new google.maps.InfoWindow();

            // Add a listener for the click event
            google.maps.event.addListener(polilineaDibujar, 'mouseover', function(event) {
                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                infowindow.setContent(contentString);
                infowindow.setPosition(event.latLng);

                infowindow.open(map);
            });

            google.maps.event.addListener(polilineaDibujar, 'mouseout', function(event) {
                infowindow.close();
            });

        }else {
            //M.clearPolilineas();
        }
    }

};        


});



</script>
<body>

<div class="ui-layout-center">

    <div id="box_resultado">

        <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        <div id="divLeyendaAllTerminales" style="position:fixed;bottom:0;right:0;background-color: #FFFFFF;margin:0 0 1px 0;display:block;">
        <div id="divLeyenda" style="margin:4px;"><a href="javascript:void(0);">Ver leyenda terminales</a></div>
        <div id="divLeyendaTerminales" style="display:none;">
            <table cellpadding="1" cellspacing="0" style="width:300px;font-size:10px;">
            <tr>
                <td style="width:150px;" valign="top">
                    <fieldset>
                        <legend>Capacidad</legend>
                        <table cellpadding="2" cellspacing="0" style="width:100%;">
                        <tr>
                            <td width="25"><div style="background-color:#DE4533; width:18px; height:10px;"></div></td>
                            <td>0 disponibles</td>
                        </tr>
                        <tr>
                            <td><div style="background-color:#FFF858; width:18px; height:10px;"></div></td>
                            <td>1-2 disponibles</td>
                        </tr>
                        <tr>
                            <td><div style="background-color:#79B094; width:18px; height:10px;"></div></td>
                            <td>mas de 3 disponibles</td>
                        </tr>
                        </table>
                    </fieldset>
                </td>
                <td style="width:150px;" valign="top">
                    <fieldset>
                        <legend>Atenuaci&oacute;n</legend>
                        <table cellpadding="1" cellspacing="0" style="width:100%;font-size:10px;">
                        <tr>
                            <td width="25"><div style="border:1px solid #221D1A;background-color:#ffffff; width:16px; height:8px;"></div></td>
                            <td>Sin valor</td>
                        </tr>
                        <tr>
                            <td><div style="border:1px solid #221D1A;background-color:#ffffff; width:16px; height:8px;"></div></td>
                            <td>0-7</td>
                        </tr>
                        <tr>
                            <td><div style="background-color:#79B094; width:18px; height:10px;"></div></td>
                            <td>8-60</td>
                        </tr>
                        <tr>
                            <td><div style="background-color:#FFF858; width:18px; height:10px;"></div></td>
                            <td>61-65</td>
                        </tr>
                        <tr>
                            <td><div style="background-color:#DE4533; width:18px; height:10px;"></div></td>
                            <td>66 a mas</td>
                        </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            </table>
            </div>
        </div>
        
        
        <?php
        //Validando si es Contrata
		
		$tieneAccesoPorEecc = 1;
		$tieneAccesoPorArea = 1;

		if (  $tieneAccesoPorEecc && $tieneAccesoPorArea )  {	
        ?>
        <div id="divSearchAddress" style="float:right;width:150px;position:fixed;background:#ffffff;">
            <div id="divDireccion" class="closed">
                <div style="display: block;">
                    <input type="checkbox" name="activar_ruta" id="activar_ruta" /> 
                    Crear ruta
                </div>
                <div style="display: block;">
                    <input type="checkbox" name="crear_ruta_acometida" id="crear_ruta_acometida" /> 
                    Crear ruta acometida
                </div>
            </div>
        </div>
        <?php 
        }
        ?>

    </div>

</div>
        
<div id="parentModal" style="display: none;">
    <div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>

<div id="mapa" style="width:512px;height:256px;"></div> 



</body>
