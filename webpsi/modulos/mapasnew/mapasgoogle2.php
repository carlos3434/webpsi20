<?php

include ("../../includes.php") ; 

require_once("../../clases/class.FacilidadesTecnicas.php");

$objFFTT = new FacilidadesTecnicas();

$direccionEnMapa = "";

if (isset($_REQUEST["buscarCoordenadas"])) {
	$coordx = $_REQUEST["coordx"];
	$coordy = $_REQUEST["coordy"];
	$zonal = $_REQUEST['zonal'];
	$mdf = $_REQUEST['mdf'];
	$tipo_red = $_REQUEST['tipo_red'];
	$cable_armario = $_REQUEST['cable_armario'];
	$caja_terminal = $_REQUEST['caja_terminal'];
	$arrCajaT = $objFFTT->getTerminalDatos($zonal, $mdf, $tipo_red, $cable_armario, $caja_terminal ) ;

	foreach ($arrCajaT as $fila) {
		$direccionEnMapa = $fila["direccion"];
	}
	
}
else
{
	//$coordx = "-34.397";
	//$coordy = "150.644";
	$direccionEnMapa = "";

	$coordy = "-12.20484"; $coordx = "-76.98883000000001";
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>-->


<style type="text/css">
#divModalMapa {
	font-family: "Tahoma";
	font-size: 12px;
	border-top-width: 0px;
	border-right-width: 0px;
	border-bottom-width: 0px;
	border-left-width: 0px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	letter-spacing: normal;
	text-align: left;
	vertical-align: middle;
	word-spacing: normal;
}

</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<script type="text/javascript">

	function initialize() {
		var coordX = $("#txtCoordX").val();
		var coordY = $("#txtCoordY").val();
		var direccionEnMapa = $("#txtDireccionEnMapa").val();

		// LATITUD es X
		// LONGITUD es Y
		var myLatlng = new google.maps.LatLng(coordY,coordX);


		var mapOptions = {
			zoom: 15,
			center: myLatlng
		}

		var map = new google.maps.Map(document.getElementById("mapa"), mapOptions);

		var image = 'img/map/terminal/terminal-1.gif';

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: direccionEnMapa,
			icon: image
		});

		/*google.maps.event.addListener(marker, 'click', function() {
            map.setZoom(15);
            map.setCenter(marker.getPosition());
            alert(marker.getPosition()  );
        });
*/
		var marker_posX = marker.getPosition();
		var infoWindow = new google.maps.InfoWindow();
		
		infoWindow.setContent([
			'<div id=\"divModalMapa\">',
			'Posicion XY: '+marker_posX
		].join(''));


		google.maps.event.addListener(marker, 'click', function() {
		    infoWindow.open(map, marker);
		});

		google.maps.event.addListener(map, "click", function(evento) {
			//Al hacer click en el MAPA, obtengo las coordenadas separadas
			var latitudY = evento.latLng.lat();
			var longitudX = evento.latLng.lng();
			var coordenadas = longitudX + ", " + latitudY;
			//alert(coordenadas);

			//$("#txtCoordXY_Mapa",top.document).val("CHAUUU");

			var cajaFoco;
			if($("input,textarea", top.document).is(":focus")){
				cajaFoco = $("*:focus", top.document);
				//cajaFocoId = $("*:focus", top.document).attr('id');
				cajaFoco.val("");
				cajaFoco.val(coordenadas);
			}
			

		});


		/*var beachMarker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: direccionEnMapa
		});
		*/

		// To add the marker to the map, call setMap();
		//beachMarker.setMap(map);
	}

	/*
	function initializeGoogleAPI() {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
		document.body.appendChild(script);
	}
	*/

function buscarXY() {
	var coordX = $("#txtCoordX").val();
	var coordY = $("#txtCoordY").val();

	initialize();

}
/*
function mostrarMapaInicial() {
	var mapOptions = {
          center: new google.maps.LatLng(-34.397, 150.644),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(
        	document.getElementById("mapa"), 
        	mapOptions
        );

}
*/

$(document).ready(function () {
	//initializeGoogleAPI() ;

});


google.maps.event.addDomListener(window, 'load', initialize);

</script>
<body >
	<input type="hidden" value="<?=$coordx?>" id="txtCoordX" name="txtCoordX" />
	<input type="hidden" value="<?=$coordy?>" id="txtCoordY" name="txtCoordY" />
	<input type="hidden" value="<?=$direccionEnMapa?>" id="txtDireccionEnMapa" name="txtDireccionEnMapa" />
	<!--<input type="button" value="buscar" onclick="buscarXY();" />-->

<div id="mapa" style="width: 650px; height: 500px; line-height:normal;"></div>


</body>
