<?php
/**
 * Asignacion de XY a clientes de una misma Caja Terminal
 *
 * @package    geoAsignaXY_clientes.php
 * @author     Sergio Miranda C.
 * @copyright  2013
 */


require_once("../../cabecera.php");
require_once("../../clases/class.FacilidadesTecnicas.php");

$idusuario_modelo = $_REQUEST["idusuario"];

function comboZonales() {
	$objZonales = new FacilidadesTecnicas();
	$arrZonales = $objZonales->getZonales();

	$x = "<select name='cmb_zonales' id='cmb_zonales'  >";
	$x .= "<option value='0' >Seleccione...</option>";
	foreach ($arrZonales as $fila) {
		$idZonal = $fila['zonal'];
		$zonal = $fila['zonal'];
		$x .= "<option value=".$idZonal.">".$zonal."</option>";
	}	
	$x .= "</select>";

	return $x;
}




?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>PSI - Web SMS - Cambio de Clave</title>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
            <meta name="author" content="Sergio MC" />
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

            <?php include ("../../includes.php") ?>                


<script type="text/javascript">

function validar_requerido(field,alerttxt)
{
	var value = $(field).val();
	if (value==null||value==""||value==0)
		{alert(alerttxt);return false}
	else {return true}
}

function changeMdf() {

	$( "#div_tipo_red" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeMdf',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val()
		}
	);

}

function changeTipoRed() {

	$( "#div_cable_armario" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeTipoRed',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val(),
			tipo_red: $('#cmb_tipored').val()
		}
	);

}


function changeCableArmario() {

	$( "#div_caja_terminal" ).load(
		"pintacombos.php",
		{
			tipo_combo: 'changeCableArmario',
			zonal: $('#cmb_zonales').val(),
			mdf: $('#cmb_mdfs').val(),
			tipo_red: $('#cmb_tipored').val(),
			cable_armario: $('#cmb_cable_armario').val()
		}
	);

}

$(document).ready(function(){

	/*$('#cmb_zonales').val("LIM");
	$('#cmb_mdfs').val("MI");
	$('#cmb_tipored').val("D");
	$('#cmb_cable_armario').val();
	$('#cmb_caja_terminal').val();
*/

	$("#txtActualPasswd").focus(); //
	
	$('#btnsalir').click(function(){
        $("#childModal").dialog('close');
    });


	$('#cmb_zonales').change(function(){
		$( "#div_combo_mdf" ).load(
				"pintacombos.php",
				{
					tipo_combo: 'changeZonal',
					zonal: $(this).val()
				}
			);
    });


	
	$('#btnGuardarCambios').click(function(){
		$( "#div_resultados" ).load(
			"pintaresultados.php",
			{
				zonal: $('#cmb_zonales').val(),
				mdf: $('#cmb_mdfs').val(),
				tipo_red: $('#cmb_tipored').val(),
				cable_armario: $('#cmb_cable_armario').val(),
				caja_terminal: $('#cmb_caja_terminal').val(),

				/*zonal: 'LIM',
				mdf: 'MI',
				tipo_red: 'D',
				cable_armario: 'D/01',
				caja_terminal: '011'
				*/

			},
			function() {
				var cx = $("#txtCoordX").val();
				var cy = $("#txtCoordY").val();
				//alert("X = "+cx+"  Y = "+cy);

				var zonal =  $('#cmb_zonales').val();
				var mdf = $('#cmb_mdfs').val();
				var tipo_red =  $('#cmb_tipored').val();
				var cable_armario = $('#cmb_cable_armario').val();
				var caja_terminal =  $('#cmb_caja_terminal').val();			

				/*
				var zonal = "LIM";
				var mdf = "MI";
				var tipo_red = "D";
				var cable_armario = "D/01";
				var caja_terminal = "011";
				cx = '-77.06876';
				cy = '-12.04404';				
				*/

				//alert(cx);
				$("#frame_mapa").attr('src', "mapasgoogle2.php?buscarCoordenadas=1&coordx="+cx+"&coordy="+cy+"&zonal="+zonal+"&mdf="+mdf+"&tipo_red="+tipo_red+"&cable_armario="+cable_armario+"&caja_terminal="+caja_terminal);
				//$("#frame_mapa").load("mapasgoogle2.php?buscarCoordenadas=1&coordx="+cx+"&coordy="+cy);
			}			
		);
	});


});
         

function grabarXY(indice) {
	//Grabar valores XY
	var xyCliente = $("#txtXY_Cliente"+indice).val();
	if (xyCliente=='') {
		alert("Debe ubicar primero un punto X/Y en el mapa.")
		return;
	}
	var inscripcion = $("#txtInscripcion_Cliente"+indice).val();
	var zonal = $("#txtZonal_Cliente"+indice).val();

	var pagina="receptor_grabarxy.php";
	$.ajax({
        type: "POST",
        url: pagina,
        data: {
			xyCliente: xyCliente,
			zonal: zonal,
			inscripcion: inscripcion
		},
        success: function(html){
			alert(html);
        }
    });



}
         
</script>

<!--<link rel="stylesheet" type="text/css" href="../../estilos.css">-->
<link rel="stylesheet" type="text/css" href="estiloMapas.css">
<link rel="stylesheet" type="text/css" href="buttons.css">
</head>

<body>
<?php echo pintar_cabecera(); ?>
<p>
	<br/>
</p>
<input type="hidden" value="<?php echo $IDUSUARIO?>" name="txt_idusuario" id="txt_idusuario"/>
	
<div id="divMain" class="div_main" style="width: 90%;  ">

	<div id="div_Clonar" class="divClonar" 
		style="float:left; margin-right: 0px; margin-top:0; width: 45%; padding-right: 20px; border: 0px red solid; ">

	<table class="tablaClonar" style="width: 100%;"  >
	<tr>
		<td class="celda_titulo">Zonal:</td>
		<td class="celda_res"  colspan="1">
			<?php echo comboZonales() ?>
		</td>
		<td class="celda_titulo">Mdf:</td>
		<td class="celda_res"  colspan="1"><div id="div_combo_mdf">
			<select name='cmb_mdfs' id='cmb_mdfs' ><option value=0>Seleccione ...</option></select></div>
		</td>
	</tr>
	<tr>
		<td class="celda_titulo">Tipo de Red:</td>
		<td class="celda_res"  colspan="1"><div id="div_tipo_red">
			<select name='cmb_tipored' id='cmb_tipored' ><option value=0>Seleccione ...</option></select></div>
		</td>
		<td class="celda_titulo">Cable / Armario:</td>
		<td class="celda_res"  colspan="1"><div id="div_cable_armario">
			<select name='' id='' ><option value=0>Seleccione ...</option></select></div>
		</td>
	</tr>	
	<tr>
		<td class="celda_titulo">Caja Terminal:</td>
		<td class="celda_res"  colspan="1"><div id="div_caja_terminal">
			<select name='' id='' ><option value=0>Seleccione ...</option></select></div>
		</td>
		<td class="celda_res"  align="center" colspan="2">
			<button id="btnGuardarCambios" class="action blue" title="Buscar Clientes">
				<span class="label">Buscar Clientes</span>
			</button>
			<?php
			/*<button id="btnsalir" class="action red" title="Cancelar">
				<span class="label">Salir</span>
			</button>
			*/
			?>
		
		</td>
	</tr>

	</table>

	<div id="div_resultados" class="divResultados" 
		style="position:relative; float:left;  width: 100%; " >
	</div>

	</div>


	
	<div id="divmapa" style="position:relative; float:left; width: 53%; ">
		<iframe src="mapasgoogle2.php" id="frame_mapa" width="670px" height="510px"></iframe> 
	</div>




	<!--<div  id="frame_mapa" style="width: 400px; height: 400px;" ></div>-->
</div>

</body>
</html>
