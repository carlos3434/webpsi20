<?php

session_start();
require_once '../../clases/class.Conexion.php';
require_once './clases/class.Data.php';
require_once './clases/class.GeoNoxy.php';
require_once '../georeferencia/clases/class.GeoTap.php';
require_once '../georeferencia/clases/class.GeoTerminald.php';
require_once '../georeferencia/clases/class.GeoTerminalf.php';

$Conexion = new Conexion();
$Data = new Data();
$GeoNoxy = new GeoNoxy();
$GeoTap = new GeoTap();
$GepTerminald = new GeoTerminald();
$GepTerminalf = new GeoTerminalf();

//Obj DB
$db = $Conexion->conectarPDO();

$sql = "SELECT 
            id, tipo, fftt, caso, coord_x, coord_y 
        FROM
            webpsi.geo_noxy 
        WHERE
            estado=0";


$bind = $db->prepare($sql);
$bind->execute();

$reporte = array();
while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
    //Caso
    $fftt = explode("|", $data["fftt"]);
    
    if ( $data["caso"] == "nodata" ) {
        if ($data["tipo"]=="geo_tap") {
            $sql = "INSERT into webpsi.{$data["tipo"]} 
                    (zonal, nodo, troba, 
                    amplificador, tap, coord_x, 
                    coord_y) 
                    VALUES 
                    ('{$fftt[0]}', '{$fftt[1]}', '{$fftt[2]}', 
                    '{$fftt[3]}', '{$fftt[4]}', '{$data["coord_x"]}', 
                    '{$data["coord_y"]}')";
            //echo $sql . "<br>";
        }
        if ($data["tipo"]=="geo_terminalf") {
            $sql = "INSERT into webpsi.{$data["tipo"]} 
                    (zonal, mdf, armario, 
                    terminalf, coord_x, coord_y) 
                    VALUES 
                    ('{$fftt[0]}', '{$fftt[1]}', '{$fftt[2]}', 
                    '{$fftt[3]}', '{$data["coord_x"]}', '{$data["coord_y"]}')";
            //echo $sql . "<br>";
        }
        if ($data["tipo"]=="geo_terminald") {
            $sql = "INSERT into webpsi.{$data["tipo"]} 
                    (zonal, mdf, cable, 
                    terminald, coord_x, coord_y) 
                    VALUES 
                    ('{$fftt[0]}', '{$fftt[1]}', '{$fftt[2]}', 
                    '{$fftt[3]}', '{$data["coord_x"]}', '{$data["coord_y"]}')";
            //echo $sql . "<br>";
        }        
    }
    if ( $data["caso"] == "noxy" ) {
        if ($data["tipo"]=="geo_tap") {
            $sql = "UPDATE 
                        webpsi.{$data["tipo"]} 
                    SET 
                        coord_x='{$data["coord_x"]}', 
                        coord_y='{$data["coord_y"]}' 
                    WHERE 
                        zonal='{$fftt[0]}' 
                        AND nodo='{$fftt[1]}' AND troba='{$fftt[2]}'
                        AND amplificador='{$fftt[3]}' AND tap='{$fftt[4]}'";
            //echo $sql . "<br>";
        }
        if ($data["tipo"]=="geo_terminalf") {
            $sql = "UPDATE 
                        webpsi.{$data["tipo"]} 
                    SET 
                        coord_x='{$data["coord_x"]}', 
                        coord_y='{$data["coord_y"]}' 
                    WHERE 
                        zonal='{$fftt[0]}' 
                        AND mdf='{$fftt[1]}' AND armario='{$fftt[2]}'
                        AND terminalf='{$fftt[3]}'";
            //echo $sql . "<br>";
        }
        if ($data["tipo"]=="geo_terminald") {
            $sql = "UPDATE 
                        webpsi.{$data["tipo"]} 
                    SET 
                        coord_x='{$data["coord_x"]}', 
                        coord_y='{$data["coord_y"]}' 
                    WHERE 
                        zonal='{$fftt[0]}' 
                        AND mdf='{$fftt[1]}' AND cable='{$fftt[2]}'
                        AND terminald='{$fftt[3]}'";
            //echo $sql . "<br>";
        }
    }
    
    echo $sql . "<br>";
        $db->exec($sql);

        //Update state
        $sql = "UPDATE webpsi.geo_noxy SET estado=1 WHERE estado=0 and id={$data["id"]}";
        $db->exec($sql);
        echo $sql . "<br>";
    
}