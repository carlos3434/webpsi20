<?php

class GeoNoxy
{

    private $_db = "webpsi";
    private $_table = "geo_noxy";
        
    public $tipo;
    public $fftt;
    public $caso;
    public $coord_x;
    public $coord_y;
    public $estado;
    
    public function registrar($db, $tipo, $fftt, $caso, $tactu, $actu, $dir, $cli){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "INSERT INTO 
                        $this->_db.$this->_table
                        (tipo, fftt, caso, tipoactu, codactu, direccion, codcli)
                    VALUES
                        ('$tipo', '$fftt', '$caso', '$tactu', '$actu', '$dir', '$cli')";
                        
            $bind = $db->prepare($sql);
            $bind->execute();
            
            $reporte = array();
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Registro correcto";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    /**
     * Obtiene ultimas localizaciones de los tecnicos
     * 
     * @param Object $db Objeto de conexion a DB
     * @param Array $codArray Arreglo de codigos de tecnicos
     * @param Array $numArray Arreglo de numeros telefonicos
     * @param String $date Dia de consulta '2014-09-23'
     * @return Array
     */
    public function getLocations($db, $codArray, $numArray, $date) {
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            
            $sql = "SELECT
                        a.id, a.LastX X, a.LastY Y, a.MobileNumber, 
                        a.EmployeeNum, a.LastBattery Battery, 
                        DATE_FORMAT(a.TIMESTAMP, '%Y-%m-%d %H:%i:%s') t
                    FROM
                        $this->_db.$this->_table a
                        JOIN (
                            SELECT 
                                MAX(id) id 
                            FROM 
                                $this->_db.$this->_table
                            WHERE
                                DATE(TIMESTAMP)='$date'
                            GROUP BY 
                                EmployeeNum
                        ) mx ON a.id = mx.id";
            
            if (!empty($codArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("EmployeeNum", $codArray)
                            . ")";
                $this->_data = $codArray;
            }
            
            if (!empty($numArray))
            {
                $sql .= " AND (" 
                            . $this->employeeList("MobileNumber", $numArray)
                            . ")";
                $this->_data = $numArray;
            }
            
            if ( $date !== "" )
            {
                $sql .= " AND TimeStamp LIKE '$date%'";
            }
            
            //Eliminar a Moty y JR (temporal)
            $sql .= " AND !(a.EmployeeNum = 89745 OR a.EmployeeNum = 564856) ";
            
            //Agrupar por empleado
            //$sql .= " GROUP BY EmployeeNum";
            
            $bind = $db->prepare($sql);
            $bind->execute($this->_data);
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    private function employeeList($field, $array)
    {
        $sql = "";
        
        foreach ($array as $val)
        {
            $sql .= " OR $field=?";
        }
        return substr($sql, 4);
    }
    
    public function getPath($db, $date, $code, $fromTime, $toTime){
        try {
            //Iniciar transaccion
            $db->beginTransaction();
            $sql = "SELECT 
                        LastX X, LastY Y, MobileNumber, 
                        EmployeeNum, LastBattery Battery, 
                        DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') t                        
                    FROM 
                        $this->_db.$this->_table 
                    WHERE 
                        DATE(TIMESTAMP)=? AND EmployeeNum=? 
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') >= ?
                        AND DATE_FORMAT(TIMESTAMP, '%Y-%m-%d %H:%i:%s') <= ?
                    ORDER BY t";            
            
            $bind = $db->prepare($sql);
            $bind->execute( array($date, $code, $fromTime, $toTime) );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getAgendaAveria(
            $db, $empresa, $celula, $fini, $fend, $carnet, $estado
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.averia codactu, g.inscripcion codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion_instalacion direccion, 
                        g.quiebre, g.lejano, g.llave, 
                        g.paquete, g.eecc_final, 
                        g.microzona, g.tipo_averia tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_averia g
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=gm.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm.id_horario
                    WHERE 
                        gm.fecha_movimiento IN (
                            SELECT
                                MAX(gm2.fecha_movimiento)
                            FROM 
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE 
                                gm2.id_gestion=g.id_gestion
                            GROUP 
                                BY gm2.id_gestion
                            )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }
            
            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getAgendaProvision(
            $db, $empresa, $celula, $fini, $fend, $carnet, $estado
    ){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.codigo_req codactu, g.codigo_del_cliente codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion, g.quiebre, 
                        g.lejano, g.llave, g.paquete, g.eecc_final, 
                        g.microzona, g.origen tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_provision g
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=gm.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm.id_horario
                    WHERE 
                        gm.fecha_movimiento IN (
                            SELECT
                                MAX(gm2.fecha_movimiento)
                            FROM 
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE 
                                gm2.id_gestion=g.id_gestion
                            GROUP BY 
                                gm2.id_gestion
                            )";
            
            if ($empresa !== "") {
                $sql .= " AND gm.id_empresa=?";
                $this->_data[] = $empresa;
            }
            
            if ($celula !== "") {
                $sql .= " AND t.idcedula=?";
                $this->_data[] = $celula;
            }
            
            if ($fini !== "" and $fend !== "") {
                $sql .= " AND gm.fecha_agenda BETWEEN ? AND ?";
                $this->_data[] = $fini;
                $this->_data[] = $fend;
            }
                        
            if ( $carnet !== "" ) 
            {
                $sql .= " AND t.carnet_critico=?";
                $this->_data[] = $carnet;
            }
            
            if ( $estado !== "" ) 
            {
                $sql .= $this->concatOr($estado, " OR gm.id_estado = ?");
            }
            
            //Ordenamiento: carnet, fecha, id
            $sql .= " ORDER BY carnet_critico, fecha_agenda, id";

            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
        
    private function concatOr($data, $condition){
        $this->_sql = "";
        $this->_array = explode(",", $data);
        
        if ($data === '_negative') 
        {
            $this->_sql = substr( str_replace("= ?", "IS NULL", $condition), 4);
        } else {
            foreach ($this->_array as $val) {
                $this->_sql .= " $condition";
                $this->_data[] = $val;
            }
            $this->_sql = substr($this->_sql, 4);
        }
        return " AND ( $this->_sql )";
    }

}
