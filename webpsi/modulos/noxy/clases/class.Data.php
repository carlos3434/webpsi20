<?php

class Data
{

    private $_data;
    
    public function getAgenda($db){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.averia codactu, g.inscripcion codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion_instalacion direccion, 
                        g.quiebre, g.lejano, g.llave, 
                        g.paquete, g.eecc_final, 
                        g.microzona, g.tipo_averia tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_averia g
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=gm.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm.id_horario
                    WHERE 
                        gm.fecha_movimiento IN (
                            SELECT
                                MAX(gm2.fecha_movimiento)
                            FROM 
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE 
                                gm2.id_gestion=g.id_gestion
                            GROUP 
                                BY gm2.id_gestion
                            )
                    AND gm.fecha_agenda <> '0000-00-00'
                            
                UNION ALL 

                    SELECT 
                        gc.id, gc.id_atc, gc.nombre_cliente_critico, 
                        gc.observacion, gm.tecnico, gm.id_empresa, 
                        t.idcedula, t.carnet_critico, h.horario, 
                        g.codigo_req codactu, g.codigo_del_cliente codcli, 
                        g.telefono, g.mdf, 
                        g.fftt, g.direccion, g.quiebre, 
                        g.lejano, g.llave, g.paquete, g.eecc_final, 
                        g.microzona, g.origen tipoactu, 
                        gm.fecha_agenda, gm.id_estado
                    FROM 
                        webpsi_criticos.gestion_provision g
                    INNER JOIN 
                        webpsi_criticos.gestion_criticos gc 
                        ON gc.id=g.id_gestion
                    INNER JOIN 
                        webpsi_criticos.gestion_movimientos gm 
                        ON g.id_gestion=gm.id_gestion
                    INNER JOIN 
                        webpsi_criticos.estados e 
                        ON e.id=gm.id_estado
                    INNER JOIN 
                        webpsi_criticos.tecnicos t 
                        ON t.id=gm.idtecnico
                    INNER JOIN 
                        webpsi_criticos.horarios h 
                        ON h.id=gm.id_horario
                    WHERE 
                        gm.fecha_movimiento IN (
                            SELECT
                                MAX(gm2.fecha_movimiento)
                            FROM 
                                webpsi_criticos.gestion_movimientos gm2
                            WHERE 
                                gm2.id_gestion=g.id_gestion
                            GROUP BY 
                                gm2.id_gestion
                            )
                    AND gm.fecha_agenda <> '0000-00-00'";
            
            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    
    public function getTemporal($db){
        try {
            $this->_data = array();
            //Iniciar transaccion
            $db->beginTransaction();
            
            //Query total, agendado en curso + liquidado            
            $sql = "SELECT 
                        a.tipo_averia tipoactu, a.horas_averia horas_actu, 
                        a.fecha_registro, a.ciudad,
                        a.averia codactu, a.inscripcion, a.fono1,
                        a.telefono, a.mdf, a.observacion_102,
                        a.segmento, a.area_, a.direccion_instalacion,
                        a.codigo_distrito, a.nombre_cliente, a.orden_trabajo,
                        a.veloc_adsl, a.clase_servicio_catv, a.codmotivo_req_catv,
                        a.total_averias_cable, a.total_averias_cobre, a.total_averias,
                        a.fftt, a.llave, a.dir_terminal,
                        a.fonos_contacto, a.contrata, a.zonal,
                        a.wu_nagendas, a.wu_nmovimientos, a.wu_fecha_ult_agenda,
                        a.total_llamadas_tecnicas, a.total_llamadas_seguimiento, 
                        a.llamadastec15dias, a.llamadastec30dias, a.quiebre, a.lejano,
                        a.distrito, a.eecc_zona, a.zona_movistar_uno,
                        a.paquete, a.data_multiproducto, a.averia_m1,
                        a.fecha_data_fuente, a.telefono_codclientecms, a.rango_dias,
                        a.sms1, a.sms2, a.area2,
                        a.velocidad_caja_recomendada, a.tipo_servicio, 
                        a.tipo_actuacion, a.eecc_final, a.microzona,
                        b.averia no_averia
                    FROM
                        webpsi_coc.averias_criticos_final a
                    LEFT JOIN 
                        webpsi_criticos.gestion_averia b 
                        ON a.averia=b.averia
                    WHERE
                        a.tipo_averia IS NOT NULL
                    HAVING no_averia IS NULL    
                        
                UNION ALL

                    SELECT
                        a.origen tipoactu, a.horas_pedido horas_actu, 
                        a.fecha_Reg fecha_registro, 
                        a.ciudad, a.codigo_req codactu, a.codigo_del_cliente, 
                        a.fono1, a.telefono, a.mdf, 
                        a.obs_dev, a.codigosegmento, a.estacion, 
                        a.direccion direccion_instalacion, a.distrito, 
                        a.nomcliente nombre_cliente, 
                        a.orden, a.veloc_adsl, a.servicio, 
                        a.tipo_motivo, a.tot_aver_cab, a.tot_aver_cob, 
                        a.tot_averias, a.fftt, a.llave, 
                        a.dir_terminal, a.fonos_contacto, a.contrata, 
                        a.zonal, a.wu_nagendas, a.wu_nmovimient, 
                        a.wu_fecha_ult_age, a.tot_llam_tec, a.tot_llam_seg, 
                        a.llamadastec15d, a.llamadastec30d, a.quiebre, 
                        a.lejano, a.des_distrito, a.eecc_zon, 
                        a.zona_movuno, a.paquete, a.data_multip, 
                        a.aver_m1, a.fecha_data_fuente, a.telefono_codclientecms, 
                        a.rango_dias, a.sms1, a.sms2, 
                        a.area2, a.veloc_caja_recomen, a.tipo_servicio, 
                        a.tipo_actuacion, a.eecc_final, a.microzona,
                        b.codigo_req no_cod_req

                    FROM 
                        webpsi_coc.tmp_provision a
                    LEFT JOIN 
                        webpsi_criticos.gestion_provision b 
                        ON a.codigo_req=b.codigo_req
                    WHERE
                        a.codigo_req IS NOT NULL 
                    HAVING no_cod_req IS NULL";
            
            $bind = $db->prepare($sql);
            $bind->execute( $this->_data );
            
            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                foreach ($data as $key=>$val) {
                    $data[$key] = utf8_encode($val);
                }
                $reporte[] = $data;
            }            
            
            $db->commit();
            $result["estado"] = true;
            $result["msg"] = "Datos recuperados";
            $result["data"] = $reporte;
            return $result;
        } catch (PDOException $error) {
            //var_dump($error);
            $db->rollback();
            $result["estado"] = false;
            $result["msg"] = $error->getMessage();
            return $result;
        }
    }
    

}
