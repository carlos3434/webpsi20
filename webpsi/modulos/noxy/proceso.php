<?php
session_start();
require_once '../../clases/class.Conexion.php';
require_once './clases/class.Data.php';
require_once './clases/class.GeoNoxy.php';
require_once '../georeferencia/clases/class.GeoTap.php';
require_once '../georeferencia/clases/class.GeoTerminald.php';
require_once '../georeferencia/clases/class.GeoTerminalf.php';

$Conexion       = new Conexion();
$Data           = new Data();
$GeoNoxy        = new GeoNoxy();
$GeoTap         = new GeoTap();
$GepTerminald   = new GeoTerminald();
$GepTerminalf   = new GeoTerminalf();

//Obj DB
$db = $Conexion->conectarPDO();

//ID usuario
$idUser = $_SESSION["exp_user"]["id"];

//Data agendas
$agdArray = $Data->getAgenda($db);

//Data temporales
$tmpArray = $Data->getTemporal($db);

function getActuCoord($agenda, $db, $GeoTap, $GepTerminald, $GepTerminalf, $GeoNoxy){
    foreach ($agenda["data"] as $key=>$val) {
        $x = "";
        $y = "";
        $geoTabla = "";
        $strFftt = "";
        
        $tipoactu   = $val["tipoactu"];
        $codactu    = $val["codactu"];
        $direccion  = $val["distrito"] . " " . $val["direccion_instalacion"];
        $codcli     = $val["codcli"];
        
        /**
         * Primera condicion: Gestionadas (-catv-)
         * Segunda condicion: Temporales (_catv)
         */
        if (
                strpos($val["tipoactu"], "-catv-") !== false  or 
                strpos($val["tipoactu"], "_catv") !== false 
            )
        {
            //Tipo CATV -> fftt obtener xy del tap
            $ffttArray = explode("|", $val["fftt"]);
            
            $geoTabla = "geo_tap";
            
            /**
             * las tablas temporales retornan 5 datos
             */
            $e_troba = $ffttArray[2];
            $e_amplificador = $ffttArray[3];
            $e_tap = $ffttArray[4];
            
            if ( count($ffttArray) === 5 )
            {
                $e_troba = $ffttArray[1];
                $e_amplificador = $ffttArray[2];
                $e_tap = $ffttArray[3];
            }
            
            if (ctype_digit($e_amplificador) ) {
                $e_amplificador = intval($e_amplificador);
            }
            if (ctype_digit($e_tap) ) {
                $e_tap = intval($e_tap);
            }
            
            $tap = $GeoTap->listar(
                    $db, 
                    array(
                            "LIM",
                            $ffttArray[0], 
                            $e_troba,
                            $e_amplificador,
                            $e_tap
                        )
                    );
            
            $strFftt = "LIM" 
                        . "|" 
                        . $ffttArray[0] 
                        . "|"
                        . $e_troba
                        . "|"
                        . $e_amplificador
                        . "|"
                        . $e_tap;
            
            if ( !empty( $tap ) )
            {
                $x = trim($tap[0]["coord_x"]);
                $y = trim($tap[0]["coord_y"]);
                
                if ($x=="" and $y==""){
                    if ( trim($ffttArray[0]) !="" and trim($e_troba) != "" and trim($e_amplificador) != "" and intval($ffttArray[4]) != "") 
                    {
                        $GeoNoxy->registrar($db, $geoTabla, $strFftt, "noxy", $tipoactu, $codactu, $direccion, $codcli);
                    }
                }
            } else {
                if ( trim($ffttArray[0]) !="" and trim($e_troba) != "" and trim($e_amplificador) != "" and intval($ffttArray[4]) != "") 
                {
                    $GeoNoxy->registrar($db, $geoTabla, $strFftt, "nodata", $tipoactu, $codactu, $direccion, $codcli);
                }                
            }
            $agenda["data"][$key]["x"] = $x;
            $agenda["data"][$key]["y"] = $y;
        } else {
            //Tipo BASICA / ADSL -> llave obtener xy del terminal
            $ffttArray = explode("|", $val["fftt"]);
            
            /**
             * Algunas llaves tienen 4 datos
             * MDF|ARMARIO|CABLE|TERMINAL
             * Las tablas temporal de provision tiene este formato
             */
            $e1 = "";
            $e2 = "";
            $e3 = "";
            $e4 = "";
            
            if ( count($ffttArray)===4 )
            {
                $e_ter = str_pad($ffttArray[3], 3, "0", STR_PAD_LEFT);
            } elseif( count($ffttArray)>=4 ) {
                $e_ter = str_pad($ffttArray[6], 3, "0", STR_PAD_LEFT);
            }

            $terminal = array();

            //Sin armario red directa, usa cable
            // CRU0||P/23|649||0|065|9 -> Directa
            // MAU2|A003|P/07|385|S/03|25|014|25 -> Flexible
            
            if ( count($ffttArray)>=4 )
            {
                if (trim($ffttArray[1])==="") 
                {
                    $geoTabla = "geo_terminald";

                    $terminal = $GepTerminald->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[2],
                                $e_ter
                                )
                            );

                    $strFftt = "LIM" 
                            . "|" 
                            . $ffttArray[0] 
                            . "|"
                            . $ffttArray[2]
                            . "|"
                            . $e_ter;
                    $e1 = $ffttArray[0];
                    $e2 = $ffttArray[2];
                    $e3 = $e_ter;
                } else {
                    //Con armario red flexible
                    $geoTabla = "geo_terminalf";

                    $terminal = $GepTerminalf->listar(
                            $db, 
                            array(
                                "LIM",
                                $ffttArray[0], 
                                $ffttArray[1],
                                $e_ter
                                )
                            );

                    $strFftt = "LIM" 
                            . "|" 
                            . $ffttArray[0] 
                            . "|"
                            . $ffttArray[1]
                            . "|"
                            . $e_ter;
                    $e1 = $ffttArray[0];
                    $e2 = $ffttArray[1];
                    $e3 = $e_ter;
                }
            }
            
            

            if ( !empty( $terminal ) )
            {
                $x = trim($terminal[0]["coord_x"]);
                $y = trim($terminal[0]["coord_y"]);
                
                if ($x=="" and $y==""){
                    if ( trim($e1) !="" and trim($e2) != "" and trim($e3) != "") 
                    {
                        $GeoNoxy->registrar($db, $geoTabla, $strFftt, "noxy", $tipoactu, $codactu, $direccion, $codcli);
                    }
                }
            } else {
                if ( trim($e1) !="" and trim($e2) != "" and trim($e3) != "") 
                {
                    $GeoNoxy->registrar($db, $geoTabla, $strFftt, "nodata", $tipoactu, $codactu, $direccion, $codcli);
                }
            }
            $agenda["data"][$key]["x"] = $x;
            $agenda["data"][$key]["y"] = $y;
        }

    }
    return $agenda["data"];
}

$agdXy = getActuCoord($agdArray, $db, $GeoTap, $GepTerminald, $GepTerminalf, $GeoNoxy);

$tmpXy = getActuCoord($tmpArray, $db, $GeoTap, $GepTerminald, $GepTerminalf, $GeoNoxy);


//print_r($agdXy);