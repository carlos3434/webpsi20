<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.edificio.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');
    
    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }
    
    return '#' . $color;
}

function save()
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();

    $arrEdificios = $objTerminales->getEdificiosByZonalAndMdf($conexion, $_REQUEST['IDzonal'], $_REQUEST['IDmdf']);

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    $arrEdificiosSelected = array();
    foreach ($_POST['xy'] as $pkedi => $valedi) {
        if ($valedi['x'] != '' || $valedi['y'] != '') {

            $arrEdificiosSelected[$pkedi] = array(
                'x' => $valedi['x'],
                'y' => $valedi['y']
            );
        }
    }

    //update solo a los que surgieron cambios en fftt_edificios
    $flag = 1;
    foreach ($arrEdificios as $edificio) {

        if (array_key_exists($edificio['idedificio'], $arrEdificiosSelected)) {
            if ($edificio['x'] != $arrEdificiosSelected[$edificio['idedificio']]['x'] ||
                    $edificio['y'] != $arrEdificiosSelected[$edificio['idedificio']]['y']) {

                //grabando los xy en fftt_edificios
                $result = $objTerminales->updateEdificioXY(
                    $conexion, $edificio['idedificio'], 
                    $arrEdificiosSelected[$edificio['idedificio']]['x'], 
                    $arrEdificiosSelected[$edificio['idedificio']]['y'], 
                    $idUsuario
                );
                if (!$result) {
                    $flag = 0;
                    break;
                }
            }
        }
    }

    $arrDataResult = ($flag) ? array('success' => 1, 'msg' => 'Datos grabados correctamente.') : array('success' => 0, 'msg' => 'Error al grabar los datos.');

    echo json_encode($arrDataResult);
    exit;
} 

function saveEstado($_POST)
{
    global $conexion;
    
    $objCapas = new Data_FfttCapas();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    //grabando estado de armarios en fftt_edificios
    $result = $objCapas->updateEstadoEdificio($conexion, $_POST['IDedi'], $_POST['estado'], $idUsuario);

    $arrDataResult = ($result) ? array('success' => 1, 'msg' => 'Datos grabados correctamente.') : array('success' => 0, 'msg' => 'Error al grabar los datos.');

    echo json_encode($arrDataResult);
    exit;
} 

function buscarMdf()
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByZonalAndIdUsuario($conexion, $_POST['IDzon'], $idUsuario);

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
            'IDmdf' => $mdf['mdf'],
            'Descmdf' => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
} 

function buscarEdificios()
{
    global $conexion;

    $objZonal              = new Data_Zonal();
    $objTerminales         = new Data_FfttTerminales();
    $objEdificio           = new Data_FfttEdificio();
    $objEdificioEstado    = new Data_FfttEdificioEstado();
    
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    //carga de datos (x,y) de Zonal
    $arrDataZonal = array();
    if ( isset($_POST['IDzonal']) && $_POST['IDzonal'] != '' ) {
        $arrZonal = $objZonal->listar($conexion, '', $_POST['IDzonal']);
        $arrDataZonal = array(
            'x' => $arrZonal[0]->__get('_x'),
            'y' => $arrZonal[0]->__get('_y')
        );
    }
    
    //carga de datos (x,y) de Mdf
    $arrDataMdf = array();
    if ( isset($_POST['IDmdf']) && $_POST['IDmdf'] != '' ) {
        $arrMdf = $objTerminales->getMdfByCodMdf($conexion, $_POST['IDmdf']);
        $arrDataMdf = array(
            'x' => $arrMdf[0]['x'],
            'y' => $arrMdf[0]['y']
        );
    }


    $arrEdificios = $objEdificio->getEdificiosByZonalAndMdf($conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['datosXY'], $_POST['Item']);

    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrEdificios as $edificios) {
        if ($edificios['y'] != '' && $edificios['x'] != '') {
            $arrDataMarkersConXy[] = $edificios;
        }

        $arrDataMarkers[] = $edificios;
        $i++;
    }


    $arrPoligonos = array();
    $promX = $promY = 0;
    // ========================================== DATA DE POLIGONOS ======================================= //
    if (isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si') {
        $objCapasarea = new Data_FfttCapasArea();

        //$idMdf = $_POST['IDmdf'];
        //seteamos 'ANU0-ANCON' porque falta actualizar el campo elemento = mdf de tabla fftt_capas_area

        $idMdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';

        //$idMdf = 'ANU0-ANCON';

        $arrPuntosXyMdf = $objCapasarea->getXysByTipoCapaAndElemento($conexion, 'MDF', $idMdf);
        $arrPoligonos[] = array(
            'coords' => $arrPuntosXyMdf,
            'color' => generaColorPoligono()
        );


        $countXy = count($arrPoligonos[0]['coords']);

        $sumX = $sumY = 0;
        foreach ($arrPoligonos[0]['coords'] as $puntoXY) {
            $sumX += $puntoXY['x'];
            $sumY += $puntoXY['y'];
        }

        $promX = $sumX / $countXy;
        $promY = $sumY / $countXy;
    }
    // ========================================== FIN DATA DE POLIGONOS ======================================= //

    $arrEstadosEdificio   = array();
    $arrEstados           = array();
    $arrObjEdificioEstado    = $objEdificioEstado->obtenerEdificioEstado($conexion);
    if ( !empty($arrObjEdificioEstado) ) {
        foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
            $arrEstadosEdificio[] = array(
                'id'    => $objEdificioEstado->__get('_codEdificioEstado'),
                'val'   => $objEdificioEstado->__get('_desEstado')
            );
            
            $arrEstados[] = array(
                $objEdificioEstado->__get('_codEdificioEstado') => $objEdificioEstado->__get('_desEstado')
            );
        }
    }

    $arrDataResult = array(
        'zonal' => $arrDataZonal,
        'mdf' => $arrDataMdf,
        'estados' => $arrEstadosEdificio,
        'est' => $arrEstados,
        'edificios' => $arrDataMarkers,
        'edificios_con_xy' => $arrDataMarkersConXy,
        'xy_poligono' => $arrPoligonos,
        'center_map' => array(
            'x' => $promX,
            'y' => $promY
        )
    );

    echo json_encode($arrDataResult);
    exit;
}

function buscar()
{
    global $conexion;

    $arrPoligonos = array();
    $promX = $promY = 0;
    // ========================================== DATA DE POLIGONOS ======================================= //
    if (isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si') {
        $objCapasarea = new Data_FfttCapasArea();

        $idMdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';

        $arrPuntosXyMdf = $objCapasarea->getXysByTipoCapaAndElemento($conexion, 'MDF', $idMdf);
        $arrPoligonos[] = array(
            'coords' => $arrPuntosXyMdf,
            'color' => generaColorPoligono()
        );

        $countXy = count($arrPoligonos[0]['coords']);

        $sumX = $sumY = 0;
        foreach ($arrPoligonos[0]['coords'] as $puntoXY) {
            $sumX += $puntoXY['x'];
            $sumY += $puntoXY['y'];
        }

        $promX = $sumX / $countXy;
        $promY = $sumY / $countXy;

        $arrDataResult = array(
            'xy_poligono' => $arrPoligonos,
            'center_map' => array(
                'x' => $promX,
                'y' => $promY
            )
        );

        echo json_encode($arrDataResult);
        exit;
    }
    // ========================================== FIN DATA DE POLIGONOS ======================================= //
}

function listarZonales()
{
    $arrZonal = $_SESSION['USUARIO_ZONAL'];

    $arrData = array();
    foreach ($arrZonal as $objZonal) {
        $arrData[] = array(
                'abvZonal'     => $objZonal->__get('_abvZonal'),
                'descZonal'    => $objZonal->__get('_descZonal')
        );
    }
    echo json_encode($arrData);
    exit;
}

function vistaInicial()
{
    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_edificio.ifr.php" width="100%" height="480"></iframe>';

}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'save':
        save($_POST, $_GET);
        break;
    case 'saveEstado':
        saveEstado($_POST);
        break;
    case 'buscarMdf':
        buscarMdf($_POST);
        break;
    case 'buscarEdificios':
        buscarEdificios($_POST);
        break;
    case 'buscar':
        buscar($_POST);
        break;
    case 'listarZonales':
        listarZonales();
        break;
    default:
        vistaInicial();
        break;
}