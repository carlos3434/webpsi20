<style>
em {
    color: #ff0000;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}


table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

.da_editar {
    width: 90%;
}

</style>
<script type="text/javascript" src="js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="modulos/maps/js/edificios.fftt.js"></script>

<!-- Cuerpo principal -->
<div id="content">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">
			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td class="form_titulo" width="30%">
					<img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Bandeja Control de Edificios
				</td>
				<td width="60%" align="left">
                    Estado
                    <select name="estado" id="estado">
                        <option value="">Seleccione</option>
                        <?php
                        foreach ($arrObjEdificioEstado as $objEdificioEstado) { 
                        ?>
                            <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado') ?>" ><?php echo $objEdificioEstado->__get('_desEstado') ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    
                </td>
                <td width="10%" align="right">
                    <a class="hd_orden_desc" href="javascript:void(0);" onclick="nuevoEdificio()">Registrar Nuevo</a>
                </td>
			</tr>
			<tr>
				<td colspan="3">
					<div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                        <div style="border-bottom:1px solid #999999;margin-bottom:3px;padding-bottom:3px;">
    						<table border="0" width="100%" cellspacing="0" cellpadding="2">
    						<tr>
                                <td width="10%" align="left">Buscar por:</td>
                                <td width="90%" align="left">
                                    Zonal
                                    <select name="zonal" id="zonal" onchange="obtenerMdfsZonalUsuario();">
                                        <option value="">Seleccione</option>
                                        <?php foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) { ?>
                                            <option value="<?php echo $objZonal->__get('_abvZonal') ?>" ><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                                        <?php } ?>
                                    </select>
                                    &nbsp;
                                    URA
                                    <select name="mdf" id="mdf">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                    <input type="button" id="btn_buscar" name="buscar" value="Buscar"/>
                                </td>
    						</tr>
                            </table>
                        </div>
                                            
                        <div>
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                                <td width="10%" align="left">Filtrar por:</td>
                                <td width="90%" align="left">
                                    Departamento
                                    <select name="departamento" id="departamento" onchange="provinciasUbigeo();">
                                        <option value="">Todos</option>
                                        <?php
                                        foreach( $arrObjDepartamento as $objDepartamento ) {
                                        ?>
                                        <option value="<?php echo $objDepartamento->__get('_codDpto')?>"><?php echo $objDepartamento->__get('_nombre')?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    &nbsp;
                                    Provincia
                                    <select name="provincia" id="provincia" onchange="distritosUbigeo();">
                                        <option value="">Seleccione</option>
                                    </select>
                                    &nbsp;
                                    Distrito
                                    <select name="distrito" id="distrito">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
    						</tr>
    						</table>
                        </div>
                                            
					</div>
				</td>
			</tr>
			</table>

			<div style="width: 100%;" id="divResultado" >
                            
                <table id="tb_sup_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                <tr>
                <td style="width:55%; height: 30px; text-align: left;">
                        Han sido listado(s) <b><span id="cont_num_resultados_head"><?php echo $totalRegistros ?></span></b> registro(s) &nbsp;|&nbsp; 
                        <a onclick="descargarEdificios();" href="javascript:void(0)">Descargar</a>
                        <a onclick="descargarEdificios();" href="javascript:void(0)"><img src="img/ico_excel.png" title="Descargar en Excel"></a> &nbsp; 
                        <span style="margin: 10px 0 0 0; display: none; font-size: 9px; color: #000000;" id="msgDescarga">
                            <img src="img/loading.gif"> Descargando reporte
                        </span>
                </td>
                <td style="width:45%;">
                        <div  style=" width:360px; position: relative;">
                                <div id="paginacion"></div>
                        </div>
                </td>
                </tr>
                </table>

                <input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
                <input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros;?>"/>
			
                <table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;display:none;">
                <thead>
                    <tr style="line-height:13px;" class="headFilter">
                        <th class="headTbl" width="2%">#</th>
                        <th class="headTbl" width="5%">Opciones</th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="item" orderType="asc">Item</a></th>
                        <th class="headTbl" width="6%">Tipo proyecto</th>
                        <th class="headTbl" width="10%">Proyecto</th>
                        <th class="headTbl" width="6%"><a href="javascript:void(0)" orderField="fecha_registro_proyecto" orderType="asc">Fec. reg. proy</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="zonal" orderType="asc">Zonal</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="ura" orderType="asc">URA</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="sector" orderType="asc">Sector</a></th>
                        <th class="headTbl" width="4%">Mza. Tdp</th>
                        <th class="headTbl" width="27%"><a href="javascript:void(0)" orderField="direccion_obra" orderType="asc">Direcci&oacute;n obra</a></th>
                        <th class="headTbl" width="4%">N&uacute;mero</th>
                        <th class="headTbl" width="6%">Tipo CCHH</th>
                        <th class="headTbl" width="6%">CCHH</th>
                        <th class="headTbl" width="8%"><a href="javascript:void(0)" orderField="estado" orderType="asc">Estado</a></th>
                    </tr>
                </thead>
                <tbody></tbody>
                </table>
                            
                <br />
                <table id="tb_pie_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                <tr>
                   <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot"><?php echo $totalRegistros; ?></span></b> registro(s)  <br/><br/></td>
                </tr>
                </table>

			</div>

		</td>
	</tr>
	</table>
	<br /><br />
</div>

<div id="footer"></div>

<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
    <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->