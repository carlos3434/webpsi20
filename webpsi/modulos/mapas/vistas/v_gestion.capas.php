<style>
em {
    color: #ff0000;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}


table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}


#pirata_image {
    float: left;
    width: 400px;
    display: block;
}
#pirata_image img{
    border: 1px solid #CACED1;
}

#pirata_thumbs {
	/*overflow: hidden;*/
	float: left;
    width: 140px;
}
#pirata_thumbs img {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    cursor: pointer;
    /*height: 75px;*/
    margin: 0 5px 5px 10px;
    padding: 2px;
    width: 70px;
}
#pirata_thumbs p{
    margin: 5px 0 0 0;
}
#pirata_thumbs span {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    margin: 30px 10px 12px 0;
    padding: 2px;
    width: 77px;
}

#pirata_icos {
    /*overflow: hidden;*/
    width: 500px;
    height: 140px;
}
#pirata_icos p{
	margin: 5px 0 0 0;
}
#pirata_icos span{
	float: left;
    width: 400px;
}
#pirata_icos span {
	border: 1px solid #CACED1;
    margin: 30px 10px 12px 0;
    padding: 2px;
    width: 77px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

</style>

<script type="text/javascript">
var pr = null;
var order_field = '';
var order_type = '';
$(document).ready(function(){

    $("#cancelar_ajax").click(function(){
        pr.abort();
        loader("end");
    });

    $("#btn_buscar").click(function() {
        order_field = '';
        order_type = '';
        $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
        
        D.BuscarCapas();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            order_field = '';
            order_type = '';
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
            
            D.BuscarCapas();
        }
    });
    
    $('#tb_resultado thead tr.headFilter a').click(function(event) {
        order_field  = $(this).attr("orderField");
        order_type   = $(this).attr("orderType");
        
        $(this).attr("orderType","desc");
        if( order_type == 'desc' ) {
            $(this).attr("orderType","asc");
        }
        
        D.BuscarCapas();
    });
    
    $('#tb_resultado thead tr.headFilter a').toggle(
        function() {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc");
            $(this).addClass("hd_orden_desc"); 
        }, function () {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_desc");
            $(this).addClass("hd_orden_asc");
        }
    );
        
    buscarCapas = function() {
        D.BuscarCapas();
    }
    
    editarCapa = function(idcapa) {
        D.EditarCapa(idcapa);
    }
    
    agregarCapa = function() {
        D.AgregarCapa();
    }
    
    updateDatosCapa = function() {
        D.UpdateDatosCapa();
    }
    
    insertDatosCapa = function() {
        D.InsertDatosCapa();
    }
    
    buscarAbvCapa = function() {
        D.BuscarAbvCapa();
    }
    
    
    
    
    fotosArmario = function(mtgespkarm) {
        D.FotosArmario(mtgespkarm);
    }
    
    editarArmario = function(mtgespkarm) {
        D.EditarArmario(mtgespkarm);
    }
    
    
    
    listadoFotosArmario = function(mtgespkarm) {
        D.ListadoFotosArmario(mtgespkarm);
    }
    
    verFotoDetalle = function(idarmarios_foto) {
        D.VerFotoDetalle(idarmarios_foto);
    }
    
    verMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).hide();
        $("#ocultarMas_" + mtgespkarm).show();
        $("#mas_" + mtgespkarm).show();
    }
    ocultarMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).show();
        $("#ocultarMas_" + mtgespkarm).hide();
        $("#mas_" + mtgespkarm).hide();
    }
    
    cerrarVentana = function() {
        $("#childModal").dialog('close');
    }

    limpiarFoto = function(id) {
        //$("#" + id).attr('value','');
        $("#" + id).replaceWith('<input id="' + id + '" class="frm_fotos" type="file" size="25" name="imagen_armario[]">');
    }

    var D = {

        BuscarCapas: function() {
            var campo_valor = $.trim($("#campo_valor").val());

            var page    = $("#pag_actual").val();
            var tl      = $("#tl_registros").val();
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                campo_valor : campo_valor,
                order_field : order_field,
                order_type  : order_type
            };

            loader("start");
            pr = $.post("modulos/maps/gestion.capas.php?cmd=filtroBusqueda", data_content, function(data){
                $("#tb_sup_tl_registros").show();
                $("#tb_pie_tl_registros").show();
                
                $("#tb_resultado").show();
                $("#tb_resultado tbody").empty();
                $("#tb_resultado tbody").append(data);
                loader("end");
            });

        },
        
        BuscarAbvCapa: function() {
            
            var abv_capa = $("#abv_capa").val();
            if(abv_capa.length != 3) {
                alert("La abreviatura debe tener 3 caracteres");
                $("#msg_buscarAbvCapa").hide();
                return false;
            }
            
            var data_content = {
                abv_capa    : abv_capa
            }

            pr = $.post("modulos/maps/gestion.capas.php?cmd=buscarAbvCapa", data_content, function(data){
                //si capa existe
                if(data['flag'] == '0') {
                    $("#abv_capa").val('');
                    $("#btn_grabar").attr('disabled','disabled');
                    $('#msg_buscarAbvCapa').show();
                    $('#msg_buscarAbvCapa').html(data['msg']);

                }else{
                    $("#btn_grabar").attr('disabled','');
                    $('#msg_buscarAbvCapa').show();
                    $('#msg_buscarAbvCapa').html(data['msg']);
                }
            },'json');

        },
        
        EditarCapa: function(idcapa) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/gestion.capas.php?cmd=editarCapa", {
                idcapa: idcapa
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'60%', hide: 'slide', title: 'INFORMACION DE LA CAPA', position:'top'});

        },
        
        AgregarCapa: function() {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/gestion.capas.php?cmd=agregarCapa", {},
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'60%', hide: 'slide', title: 'AGREGAR NUEVA CAPA', position:'top'});

        },
        
        UpdateDatosCapa: function() {
            
            var data_content = {
                idcapa      : $("#idcapa").val(),
                nombre      : $("#nombre").val(),
                ico         : $("#archivo1").val()
            }
            
            for (i=1; i<=20; i++) {
                if( $("#campo" + i).val() != '' ) {
                    data_content['campo' + i] = $("#campo" + i).val();
                }
            }

            if( data_content['nombre'] == '' ) {
                alert("Ingrese el campo Capa.");
                return false;
            }
            if( data_content['ico'] == '' ) {
	        alert('Seleccionar una imagen para la capa.');
	        return false;
	    }
 
            $("#msg").show();
            $("input[name=btn_guardar]").attr('disabled','disabled');
            pr = $.post("modulos/maps/gestion.capas.php?cmd=updateDatosCapa", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    //refresca la bandeja
                    buscarCapas();
                    
                    //cerramos la ventana
                    cerrarVentana();

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_guardar]").attr('disabled','');
            },'json');

        },
        
        InsertDatosCapa: function() {
            
            var data_content = {
                nombre      : $("#nombre").val(),
                abv_capa    : $("#abv_capa").val(),
                ico         : $("#archivo1").val()
            }
            
            for (i=1; i<=20; i++) {
                if( $("#campo" + i).val() != '' ) {
                    data_content['campo' + i] = $("#campo" + i).val();
                }
            }

            if( data_content['nombre'] == '' ) {
                alert("Ingrese el campo Capa.");
                return false;
            }
            
            if( data_content['abv_capa'] == '' ) {
	        alert('Ingrese el campo abreviatura capa.');
	        return false;
	    }
            if(data_content['abv_capa'].length != 3) {
                alert("La abreviatura debe tener 3 caracteres");
                return false;
            }
            
            if( data_content['ico'] == '' ) {
	        alert('Seleccionar una imagen para la capa.');
	        return false;
	    }
 
            $("#msg").show();
            $("input[name=btn_guardar]").attr('disabled','disabled');
            pr = $.post("modulos/maps/gestion.capas.php?cmd=insertDatosCapa", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    //refresca la bandeja
                    buscarCapas();
                    
                    //cerramos la ventana
                    cerrarVentana();

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_guardar]").attr('disabled','');
            },'json');

        }
        
    };
    
    //carga por default
    buscarCapas();
});
</script>

<!-- Cuerpo principal -->
<div id="content">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">
			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td class="form_titulo" width="30%">
                                    <img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Gesti&oacute;n de Capas
				</td>
                                <td width="70%" align="right">
                                    <a class="hd_orden_desc" onclick="agregarCapa()" href="javascript:void(0);">Agregar capa</a>
                                </td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                        <div>
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
                            <td width="10%" align="left">Buscar por nombre:</td>
                            <td width="90%" align="left">
                                <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                <input type="button" id="btn_buscar" name="buscar" value="Buscar"/>
                            </td>
						</tr>
                        </table>
                        </div>
					</div>
				</td>
			</tr>
			</table>

			
				  
			<div style="width: 100%;" id="divResultado" >
                            
                            <table id="tb_sup_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                            <tr>
                            <td style="width:55%; height: 30px; text-align: left;">
                                    Han sido listado(s) <b><span id="cont_num_resultados_head"><?php echo $totalRegistros ?></span></b> registro(s)
                            </td>
                            <td style="width:45%;">
                                    <div  style=" width:360px; position: relative;">
                                            <div id="paginacion"></div>
                                    </div>
                            </td>
                            </tr>
                            </table>

                            <input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
                            <input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros;?>"/>
			
                            <table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;display:none;">
                            <thead>
                                <tr style="line-height:13px;" class="headFilter">
                                    <th class="headTbl" width="2%">#</th>
                                    <th class="headTbl" width="5%">Opciones</th>
                                    <th class="headTbl" width="35%"><a href="javascript:void(0)" orderField="nombre" orderType="asc">Capa</a></th>
                                    <th class="headTbl" width="15%">Abreviatura</th>
                                    <th class="headTbl" width="13%">Icono</th>
                                    <th class="headTbl" width="15%"><a href="javascript:void(0)" orderField="fecha_insert" orderType="asc">Fecha registro</a></th>
                                    <th class="headTbl" width="15%"><a href="javascript:void(0)" orderField="fecha_update" orderType="asc">Fecha ult. modif.</a></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            </table>
                            
                            <br />
                            <table id="tb_pie_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                            <tr>
                               <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot"><?php echo $totalRegistros; ?></span></b> registro(s)  <br/><br/></td>
                            </tr>
                            </table>

			</div>
			
			

		</td>
	</tr>
	</table>
	<br /><br />
</div>

<div id="footer"></div>

<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
    <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->