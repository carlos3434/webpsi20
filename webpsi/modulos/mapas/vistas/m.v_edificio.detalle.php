<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<!-- <script type="text/javascript" src="../../js/jquery/jquery.js"></script> -->
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript">
$(document).ready(function() {

	var perteneceEmpresa = '<?php echo $perteneceEmpresa?>';
	
    $(".opc_orden_down").click(function() {
    	$(this).removeClass('opc_orden_down');
    	$(this).addClass('opc_orden_up');
    	$('#divOpciones').show();
    });
    $(".opc_orden_up").click(function() {
    	$(this).removeClass('opc_orden_up');
    	$(this).addClass('opc_orden_down');
    	$('#divOpciones').hide();
    });

    $('#divOpcEdificio a').toggle(
        function() {
            $('#divOpcEdificio a').removeClass("opc_orden_up");
            $(this).addClass("opc_orden_down"); 
            $('#divOpciones').show();
        }, function () {
            $('#divOpcEdificio a').removeClass("opc_orden_down");
            $(this).addClass("opc_orden_up");
            $('#divOpciones').hide();
        }
    );
    
});
</script>

<style>
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 2px;
}

table.tb_detalle_edi input {
    width: 95%;
}
table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

table.tb_detalle_opc {
    background-color: #CDCDCD;
    font-size: 12px;
}
table.tb_detalle_opc td {
    background-color: #FFFFFF;
    padding: 12px;
    text-align: left;
    
}

.opc_orden_up {
    background-image: url("../../img/arrow_up_s.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 25px;
}

.opc_orden_down {
    background-image: url("../../img/arrow_down_s.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 25px;
}

.box_descripcion_err {
    background-color: #FFEEEE;
    border: 2px solid #DD0000;
    border-radius: 5px 5px 5px 5px;
    color: #000000;
    font-family: arial;
    font-size: 11px;
    line-height: 20px;
    margin: 4px 2px;
    padding: 6px;
    z-index: 0;
    text-align: center;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Detalle Componente</td>
    <td class="titulo">
        <?php 
        if ( $perteneceEmpresa ) {
        ?>
        <div style="text-align: right; width: 130px; float: right; padding: 2px 5px; border: 1px solid;">
            <div id="divOpcEdificio"><a class="opc_orden_up">Opciones</a></div>
            <div id="divOpciones" style="display:none;">
                <div id="lisOpcionesEdi" style="position:absolute; background-color:#E6EEEE; width:180px; right:0;">
                    <table class="tb_detalle_opc" cellspacing="1" cellpadding="0">
                    <tr>
                        <td><a href="m.buscomp.add.componente.php?cmd=listadoFotosEdificio&capa=edi&idedificio=<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>">Ver historial de fotos</a></td>
                    </tr>
                    <tr>
                        <td><a href="m.buscomp.add.componente.php?cmd=adjuntarFotosEdificio&capa=edi&idedificio=<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>">Adjuntar fotos del Edificio</a></td>
                    </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php 
        }
        ?>
    <td>
</tr>
</table>

<?php 
if ( !$perteneceEmpresa ) {
?>
<div class="box_descripcion_err">
    <i>El edificio que desea ver/editar no pertenece a una o alguna de las empresas de su pertenecia,</i> <br />
    <b>Solo tiene acceso a ver el detalle del edificio</b>
</div>
<?php 
}
?>

<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">

    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N EDIFICIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="30%" class="da_subtitulo">ITEM</td>
        <td width="70%" class="da_import"><?php echo $arrObjEdificio[0]->__get('_item')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N DE OBRAS</td>
        <td class="da_import"><?php echo $gestionObra?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMPRESA</td>
        <td class="da_import" colspan="3"><?php echo $arrObjEdificio[0]->__get('_descEmpresa')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">C&Oacute;DIGO PROYECTO WEB</td>
        <td class="da_import2"><?php echo strtoupper($arrObjEdificio[0]->__get('_codigoProyectoWeb'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td class="da_import"><?php echo $estadoEdificio?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">REGI&Oacute;N</td>
        <td class="da_import">                                <?php 
            if (!empty($arrRegion)) {
            foreach ($arrRegion as $region) {
                if ($region['idregion'] == $arrObjEdificio[0]->__get('_idRegion')) {
                    echo $region['nomregion'] . ' - ' . $region['detregion'];
                    break; 
                } 
            }
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FUENTE DE IDENTIFICACI&Oacute;N</td>
        <td class="da_import"><?php echo $fuenteIdentificacion ?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONAL</td>
        <td><?php echo $arrObjEdificio[0]->__get('_zonal')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">URA</td>
        <td><?php echo $arrObjEdificio[0]->__get('_ura')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA CABLEADO</td>
        <td>
        <?php 
        if ( $arrObjEdificio[0]->__get('_fechaCableado') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaCableado') != '' ) {
            echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaCableado'))); 
        } else {
            echo '';    
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ARMARIO</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_armario'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
        <td>
        <?php 
        if ( $arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '' ) {
            echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaRegistroProyecto'))); 
        } else {
            echo '';    
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SECTOR</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_sector'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA (TDP)</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_mzaTdp'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO V&Iacute;A</td>
        <td><?php echo $tipoVia ?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_direccionObra'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">N&Uacute;MERO</td>
        <td><?php echo $arrObjEdificio[0]->__get('_numero')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA</td>
        <td><?php echo $arrObjEdificio[0]->__get('_mza')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOTE</td>
        <td><?php echo $arrObjEdificio[0]->__get('_lote')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO CCHH</td>
        <td><?php echo $tipoCchh?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CCHH</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_cchh'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DEPARTAMENTO</td>
        <td><?php echo $departamento?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PROVINCIA</td>
        <td><?php echo $provincia?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DISTRITO</td>
        <td><?php echo $distrito?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOCALIDAD</td>
        <td><?php echo $localidad?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SEGMENTO</td>
        <td><?php echo $segmento?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO PROYECTO</td>
        <td><?php echo $tipoProyecto?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE PROYECTO</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreProyecto'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreConstructora'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RUC CONSTRUCTORA</td>
        <td><?php echo $arrObjEdificio[0]->__get('_rucConstructora')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NIVEL SOCIOECON&Oacute;MICO</td>
        <td><?php echo $nivelSocioeconomico?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PERSONA CONTACTO</td>
        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_personaContacto'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONA COMPETENCIA</td>
        <td><?php echo $zonaCompetencia?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N COMPETENCIA</td>
        <td><?php echo $gestionCompetencia?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 2G</td>
        <td><?php echo $coberturaMovil2g?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 3G</td>
        <td><?php echo $coberturaMovil3g?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">P&Aacute;GINA WEB</td>
        <td><?php echo $arrObjEdificio[0]->__get('_paginaWeb')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMAIL</td>
        <td><?php echo $arrObjEdificio[0]->__get('_email')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
        <td valign="top"><?php echo $arrObjEdificio[0]->__get('_nroBlocks')?></td>
    </tr>
    <tr style="text-align: left;">
        <td colspan="2" valign="top">
            <div id="divBlock">
                <div style="width:180px;">
                    <span style="float:left; width:90px;">Nro. pisos</span>
                    <span style="float:left; width:90px;">Nro. dptos</span>
                </div>
                <div style="width:180px;">
                    <span style="float:left; width:90px; padding:2px 0;"><?php echo trim($arrObjEdificio[0]->__get('_nroPisos1'))?></span>
                    <span style="float:left; width:90px; padding:2px 0;"><?php echo trim($arrObjEdificio[0]->__get('_nroDptos1'))?></span>
                </div>
            </div>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VIVEN</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_viven') == '1' ) {
                echo 'Si'; 
            } if ( $arrObjEdificio[0]->__get('_viven') == '0' ) {
                echo 'No'; 
            } else {
                echo '';
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
        <td><?php echo $arrObjEdificio[0]->__get('_nroDptosHabitados')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
        <td><?php echo $tipoInfraestructura?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MEGAPROYECTO</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_megaproyecto') == '1' ) {
                echo 'Si';
            } elseif ( $arrObjEdificio[0]->__get('_megaproyecto') == '0' ) {
                echo 'No';
            } else {
                echo '';
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">% AVANCE</td>
        <td><?php echo $arrObjEdificio[0]->__get('_avance')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N / QUINCENA</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '' ) {
                echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
            } else {
                echo '';    
            }
            ?>
            <?php 
            $quincena = '';
            if ($arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00' &&
                    $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '') {
                $diaQuincena = date('d', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                $mesQuincena = date('m', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                $anoQuincena = date('y', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                $dia = '1Q';
                if ( (int)$diaQuincena > 15 ) {
                    $dia = '2Q';
                }
                $quincena = $dia . '-' . $mesQuincena . '-' . $anoQuincena;
            }
            echo ' / <span style="color:#ff0000;">(' . $quincena . ')</span>';
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MONTANTE</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_montante') == 'S' ) {
                echo 'Si';
            } elseif ( $arrObjEdificio[0]->__get('_montante') == 'N' ) {
                echo 'No';
            } else {
                echo '-';
            }
            ?> &nbsp; 
            Fecha: <?php if ($arrObjEdificio[0]->__get('_montanteFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_montanteFecha'))?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DUCTERIA INTERNA</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'S' ) {
                echo 'Si';
            } elseif ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'N' ) {
                echo 'No';
            } else {
                echo '-';
            }
            ?> &nbsp; 
            Fecha: <?php if ($arrObjEdificio[0]->__get('_ducteriaInternaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_ducteriaInternaFecha'))?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PUNTO DE ENERGIA</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'S' ) {
                echo 'Si';
            } elseif ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'N' ) {
                echo 'No';
            } else {
                echo '-';
            }
            ?> &nbsp; 
            Fecha: <?php if ($arrObjEdificio[0]->__get('_puntoEnergiaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_puntoEnergiaFecha'))?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FACILIDADES DEL POSTE</td>
        <td><?php echo $facilidadPoste?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RESPONSABLES CAMPO</td>
        <td><?php echo $responsableCampo?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VALIDADO POR CALL</td>
        <td>
            <?php 
            if ( $arrObjEdificio[0]->__get('_validacionCall') == '1' ) {
                echo 'Si';
            } elseif ( $arrObjEdificio[0]->__get('_validacionCall') == '0' ) {
                echo 'No';
            } else {
                echo '';
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TRATAMIENTO CALL</td>
        <td>
        <?php 
        if ( $arrObjEdificio[0]->__get('_fechaTratamientoCall') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaTratamientoCall') != '' ) {
            echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaTratamientoCall')));
        } else {
            echo '';    
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
        <td>
        <?php 
        if ( $arrObjEdificio[0]->__get('_fechaSeguimiento') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaSeguimiento') != '' ) {
            echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaSeguimiento')));
        } else {
            echo '';    
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
        <td><?php echo nl2br($arrObjEdificio[0]->__get('_observacion'))?></td>
    </tr>
    </tbody>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody><tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px; float:left;">
                <input type="hidden" name="idedificio" id="idedificio" value="<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>" />
                <input type="button" value="Regresar" name="regresar" title="Regresar" id="btn_regresar" class="submit" onclick="javascript: history.back();" />
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
    
</div>

</body>
</html>
