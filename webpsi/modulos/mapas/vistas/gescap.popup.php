<?php
require_once "../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;


if( isset($_POST['action']) && $_POST['action'] == 'cmdEditar' ) {

	$obj_capas = new data_ffttCapas();
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$data_result = $obj_capas->__updateCapa($conexion, $_POST, $idusuario);
	
	echo '<pre>';
	print_r($_POST);
	echo '</pre>';exit;
	
	$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');

	echo json_encode($result);
	exit;
	
}
elseif( isset($_POST['action']) && $_POST['action'] == 'editar' ) {

	$btn_grabar = 'cmdEditar';

	$obj_capas = new data_ffttCapas();
	
	$capa = $obj_capas->get_capa_by_id($conexion, $_POST['idcapa']);
	$capa_campos = $obj_capas->get_campos_by_capa($conexion, $_POST['idcapa']);
	
	$array_campos = array();
	foreach( $capa_campos as $campo ) {
		$array_campos[$campo['campo_nro']] = $campo['campo'];
	}

}else {

	$btn_grabar = 'cmdNuevo';
}
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#cancelar").click(function() {
    	$("#childModal").dialog('close');
	});
	
	$("#cmdNuevo, #cmdEditar").click(function() {

		var idcapa 	 = $("#idcapa").val();
		var nombres  = $("#nombres").val();
		var abv_capa = $("#abv_capa").val();
		var campos = "";
		
		for (i=1; i<=20; i++) {
			if( $("#campo" + i).val() != '' ) {
				campos += "&campo" + i + "=" + $("#campo" + i).val();
			}
		}

		var action = "cmdNuevo";
		if(idcapa != '') {
			action = "cmdEditar";	
		}
		
		data_content = "action=" + action + "&idcapa=" + idcapa + "&nombre=" + nombres + "&abv_capa=" + abv_capa + campos;
        $.ajax({
            type:   "POST",
            url:    "gescap.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
   				if(datos['success'] == 1) {
   					$("#msg").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
   				}else {
   					$("#msg").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
   				}
   				setTimeout("cerrarPopup()", 1000);
            }
        });
		
    	
	});
	
});
function cerrarPopup() {
	$("#childModal").dialog('close');
}
</script>


<form method="post" action="modules/maps/gescap.php" >
<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td>Capa:</td>
	<td><input type="text" name="nombres" id="nombres" value="<?php echo $capa[0]['nombre']?>" /></td>
</tr>
<tr>
	<td>Abreviatura:</td>
	<td><input type="text" name="abv_capa" id="abv_capa" maxlength="3" value="<?php echo $capa[0]['abv_capa']?>" /></td>
</tr>
<tr>
	<td>Imagen:</td>
	<td><input type="file" /></td>
</tr>
<?php 
for( $i=1; $i<=20; $i++ ) {

$readonly = ($array_campos[$i] != '' ) ? 'readonly="readonly"' : '';

if($i < 15) {
	$text = 'varchar';
}elseif( $i > 14 && $i < 18) {
	$text = 'int';
}else {
	$text = 'date';
}

?>
<tr>
	<td>Campo <?php echo $i?>:</td>
	<td>
		<input type="text" class="campo" <?php echo $readonly?> name="campo<?php echo $i?>" id="campo<?php echo $i?>" value="<?php echo $array_campos[$i]?>" /> &nbsp;
		(<?php echo $text?>)
	</td>
</tr>
<?php 
}
?>
<tr>
	<td colspan="2" height="20">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" name="<?php echo $btn_grabar?>" id="<?php echo $btn_grabar?>" value="Grabar datos" />
		<input type="button" id="cancelar" value="Cancelar" />
	</td>
</tr>
</table>
<input type="hidden" name="idcapa" id="idcapa" value="<?php echo $_POST['idcapa']?>" />
</form>
<div id="msg"></div>







