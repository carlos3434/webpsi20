<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


$obj_terminales = new data_ffttTerminales();
$obj_DescargaData = new Data_DescargaData();

$idusuario = $_SESSION['USUARIO']->__get('idusuario');

$array_mdf = $obj_terminales->get_mdf_by_eecc_zonal_and_idusuario($conexion, $_POST['eecc'], $_POST['zonal'], $idusuario);

//flexible
$imprimir_list = array();
foreach ($array_mdf as $mdf) {

	$listado_arr = $obj_DescargaData->__listSelect($conexion, $_POST['eecc'], $_POST['zonal'], $mdf['mdf'], 'F');
	
	if( !empty($listado_arr) ) {
		$imprimir_list[] = $listado_arr;
	}
}

//directa
$imprimir_list_dir = array();
foreach ($array_mdf as $mdf) {

	$listado_arr_dir = $obj_DescargaData->__listSelect($conexion, $_POST['eecc'], $_POST['zonal'], $mdf['mdf'], 'D');
	
	if( !empty($listado_arr_dir) ) {
		$imprimir_list_dir[] = $listado_arr_dir;
	}
}

/*echo '<pre>';
print_r($imprimir_list);
echo '</pre>';*/






?>


<div id="tb_listado" style="height: 480px; width: 440px; overflow: auto;">
<table border="1" rules="all" width="420" style="font-size:10px;">
<tr>
	<th colspan="6" style="background-color: #EFEFEF; font-weight: bold; height: 28px;">Concentraci&oacute;n de Averias - Red Flexible</th>
</tr>
<tr>
	<td style="font-weight: bold;">#</td>
	<td style="font-weight: bold;">EECC</td>
	<td style="font-weight: bold;">ZONAL</td>
	<td style="font-weight: bold;">MDF</td>
	<td style="font-weight: bold;">ARMARIO</td>
	<td style="font-weight: bold;"># AVERIAS</td>
</tr>
<?php
if( !empty($imprimir_list) ) {
	foreach( $imprimir_list as $imprimir_arr) {
	$w = 1;
	$n_eecc = count($imprimir_arr);
	foreach( $imprimir_arr as $imprimir ) {
	?>
	<tr>
		<td><?php echo $w?></td>
		<?php
		if( $w == 1 ) {
		?>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['eecc']?></td>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['zonal']?></td>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['mdf']?></td>
		<?php
		}
		?>
		<td><?php echo $imprimir['armario_cable']?></td>
		<td><?php echo $imprimir['num_averias']?></td>
	</tr>
	<?php
	$w++;
	}
	}
}
else {
?>
<tr>
	<td colspan="6" style="height: 40px;">Ningun registro encontrado con los criterios de busqueda ingresados.</th>
</tr>
<?php
}
?>
</table>

<div style="height:20px;"></div>

<table border="1" rules="all" width="420" style="font-size:10px;">
<tr>
	<th colspan="6" style="background-color: #EFEFEF; font-weight: bold; height: 28px;">Concentraci&oacute;n de Averias - Red Directa</th>
</tr>
<tr>
	<td style="font-weight: bold;">#</td>
	<td style="font-weight: bold;">EECC</td>
	<td style="font-weight: bold;">ZONAL</td>
	<td style="font-weight: bold;">MDF</td>
	<td style="font-weight: bold;">CABLE</td>
	<td style="font-weight: bold;"># AVERIAS</td>
</tr>
<?php
if( !empty($imprimir_list_dir) ) {
	foreach( $imprimir_list_dir as $imprimir_arr_dir) {
	$w = 1;
	$n_eecc = count($imprimir_arr_dir);
	foreach( $imprimir_arr_dir as $imprimir ) {
	?>
	<tr>
		<td><?php echo $w?></td>
		<?php
		if( $w == 1 ) {
		?>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['eecc']?></td>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['zonal']?></td>
			<td rowspan="<?php echo $n_eecc?>"><?php echo $imprimir['mdf']?></td>
		<?php
		}
		?>
		<td><?php echo $imprimir['armario_cable']?></td>
		<td><?php echo $imprimir['num_averias']?></td>
	</tr>
	<?php
	$w++;
	}
	}
}
else {
?>
<tr>
	<td colspan="6" style="height: 40px;">Ningun registro encontrado con los criterios de busqueda ingresados.</th>
</tr>
<?php
}
?>
</table>

</div>

<script>
$(document).ready(function () {
	$("#tb_listado").printArea();
});

</script>
