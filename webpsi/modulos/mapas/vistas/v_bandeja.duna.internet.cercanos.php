<?php 
if ( !empty($arrClientesSpeedy) ) {
?>
<thead>
	<tr style="line-height: 13px;">
		<th class="headTbl" width="2%">#</th>
		<th class="headTbl" width="3%">Opc</th>
		<th class="headTbl" width="25%">Cliente</th>
		<th class="headTbl" width="30%">Direccion</th>
		<th class="headTbl" width="7%">Telefono</th>
		<th class="headTbl" width="10%">FFTT</th>
		<th class="headTbl" width="15%">Producto</th>
		<th class="headTbl" width="8%">
		<a class="hd_orden_<?php echo $ordenIco?>" href="javascript:void(0);"
		onclick="ordenaClientesCercanos('distancia', '<?php echo $orden?>')"
		style="color: #0086C3;">Distancia</a></th>
	</tr>
</thead>
<?php
if ( !empty($arrClientesSpeedy) ) {
?>
<tbody>
<?php
    $item = 1;
    foreach ($arrClientesSpeedy as $clienteSpeedy) {
        $flagExists = 0;
        if ( !empty($arrClientesAsociados) ) {
            if ( array_key_exists($clienteSpeedy['ddn_telefono'], $arrClientesAsociados) ) {
                $flagExists = 1;
            }
        }
    
        $cableArmario = ($clienteSpeedy['tipored'] == 'F') ? $clienteSpeedy['Armario'] : $clienteSpeedy['Cable'];
?>
	<tr <?php if($flagExists) echo 'class="tr_selected"'; ?>
		id="row_<?php echo $clienteSpeedy['ddn_telefono']?>|<?php echo $clienteSpeedy['tipored'] ?>">
		<td class="celdaX"><?php echo $item++; ?></td>
		<td class="celdaX">
			<!--
            <input <?php if($flagExists) echo 'checked="checked"'; ?> type="checkbox" class="lista_chk" id="<?php echo $clienteSpeedy['ddn_telefono']?>|<?php echo $clienteSpeedy['tipored']?>" name="<?php echo $clienteSpeedy['ddn_telefono']?>|<?php echo $clienteSpeedy['tipored']?>" value="<?php echo $clienteSpeedy['ddn_telefono']?>|<?php echo $clienteSpeedy['tipored']?>">
            --> 
            <?php
            if (!$flagExists) {
            ?> 
            <a href="javascript:void(0)" onclick="asociarPirataAbonado('<?php echo $idComponente?>','<?php echo $clienteSpeedy['tipored']?>','<?php echo $clienteSpeedy['ddn_telefono']?>')"
			title="Ver detalle"> <img width="13" height="13"
			title="Asociar cliente al Pirata" src="img/btn_enter_data.png">
		    </a>
		    <?php
            } else {
            ?>
            <img width="13" height="13" title="Cliente ya asociado al Pirata"
			src="img/btn_enter_data_disabled.png">
			<?php
            }
            ?>
		</td>
		<td class="celdaX" align="left"><?php echo $clienteSpeedy['Cliente']; ?></td>
		<td class="celdaX" align="left"><?php echo $clienteSpeedy['Direccion']; ?></td>
		<td class="celdaX"><?php echo $clienteSpeedy['ddn_telefono']; ?></td>
		<td class="celdaX"><?php echo $clienteSpeedy['MDF'] . ' - ' . $cableArmario . ' - ' . $clienteSpeedy['Caja']; ?></td>
		<td class="celdaX"><?php echo $clienteSpeedy['PRODUCTO']; ?></td>
		<td class="celdaX"><?php echo $clienteSpeedy['distancia'] . ' m.'; ?>
		</td>
	</tr>
	<?php
        }
    ?>
</tbody>
<?php
    }
}
?>       
<script type="text/javascript">
$(document).ready(function(){
    $("#spnNumClientes").html('<?php echo count($arrClientesSpeedy)?>');
});
</script>