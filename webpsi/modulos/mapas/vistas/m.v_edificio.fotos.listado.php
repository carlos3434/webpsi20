<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<!-- <script type="text/javascript" src="../../js/jquery/jquery.js"></script> -->
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	verFotoDetalle = function(idedificiofoto) {
		window.top.location = 'm.buscomp.add.componente.php?cmd=detalleFotosEdificio&capa=edi&idedificiofoto=' + idedificiofoto;
	}
    
});
</script>

<style>
em {
    color: #ff0000;
}
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 12px;
    font-size: 12px;
}

table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Listado de fotos del edificio</td>
</tr>
</table>

<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    
    <div style="width:100%;">
        <table id="tb_lista_fotos" class="tb_detalle_edi" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%; font-size:10px;">
        <thead>
            <tr style="line-height:13px;">
                <td class="da_subtitulo" width="2%">#</td>
                <td class="da_subtitulo" width="3%">OPC</td>
                <td class="da_subtitulo" width="3%">&nbsp;</td>
                <td class="da_subtitulo" width="10%">FECHA FOTO</td>
                <td class="da_subtitulo" width="10%">ESTADO</td>
                <td class="da_subtitulo" width="72%">OBSERVACIONES</td>
            </tr>
        </thead>
        <tbody>     
            
        <?php
        if ( !empty($arrObjEdificioFotos) ) {
        
            $itemA = 1;
            foreach ($arrObjEdificioFotos as $objEdificioFoto) {
                $imgMov = 'application_add.png';
                if ( $objEdificioFoto->__get('_ingreso') == '2' ) {
                    $imgMov = 'mobile_phone.png';
                }
            ?>
                <tr>
                    <td><?php echo $itemA++; ?></td>
                    <td onclick="verFotoDetalle('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')">
                        <img width="16" height="16" title="Ver fotos" src="../../img/ico_detalle.jpg" style="margin:3px;" />
                    </td>
                    <td align="center"><img src="../../img/<?php echo $imgMov?>" width="16" height="16" alt="" title="" /></td>
                    <td><?php echo obtenerFecha($objEdificioFoto->__get('_fechaFoto')); ?></td>
                    <td><?php echo $objEdificioFoto->__get('_desEstado'); ?></td>
                    <td style="text-align:left;">
                        <?php
                        if ( strlen($objEdificioFoto->__get('_observaciones')) > 50  ) {
                        ?>
                            <?php echo substr($objEdificioFoto->__get('_observaciones'), 0, 50); ?><a style="color:blue;" href="javascript:void(0)" id="verMas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>" onclick="verMas('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')">m&aacute;s...</a><span style="display:none;" id="mas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>"><?php echo substr($objEdificioFoto->__get('_observaciones'), 50);?></span> <a style="color:blue;display:none;" href="javascript:void(0)" id="ocultarMas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>" onclick="ocultarMas('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')">ocultar</a>
                        <?php
                        } else {
                            echo $objEdificioFoto->__get('_observaciones');
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        } else {
        ?>
            <tr>
                <td colspan="6" style="padding:20px; text-align:center;">No hay fotos para este edificio.</td>
            </tr>
        <?php 
        }
        ?>
        </tbody>
        </table>
    </div>
    <div style="text-align:right; margin:20px 0 0 0;">
        <strong>LEYENDA:</strong>
        <img width="14" height="14" title="Web" alt="Web" src="../../img/application_add.png"> Web
        <img width="14" height="14" title="Movil" alt="Movil" src="../../img/mobile_phone.png"> Movil 
    </div>    
    
</div>

</body>
</html>
