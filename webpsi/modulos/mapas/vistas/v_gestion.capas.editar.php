<style>
#divUploadImagen {
    background-color: #FFFFFF;
    background-position: center center;
    background-repeat: no-repeat;
    border: 1px solid #CCCCCC;
    display: none;
    height: 180px;
    left: 0;
    margin-left: 320px;
    margin-right: auto;
    margin-top: 140px;
    position: absolute;
    text-align: center;
    top: 0;
    width: 350px;
}
#divCerrarUploadImagen {
    cursor: pointer;
    float: right;
    font-weight: bold;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
    $("#divCerrarUploadImagen").click(function() {
        $("#divUploadImagen").hide();
    });
    $(".upload").click(function() {
        $("#divUploadImagen").show();
        var archivo = $(this).attr('id');

        $("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=ico&archivo=archivo1" width="100%" height="100%" style="border: 0px;"></iframe>');
    });
    
});
</script>

<div id="divUploadImagen">
    <div id="divCerrarUploadImagen">[X]</div>
    <div id="divFormUploadImagen">

    </div>
</div>


<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    
    <div style="height:350px;overflow:auto;">
    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N DE LA CAPA</td>
    </tr>
    <tr style="text-align: left;">
        <td width="30%" class="da_subtitulo">CAPA</td>
        <td width="70%"><input type="text" name="nombre" id="nombre" value="<?php echo $arrObjCapa[0]->__get('_nombre')?>"></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ABREVIATURA CAPA</td>
        <td><?php echo $arrObjCapa[0]->__get('_abvCapa')?></td>
    </tr>
    
    <tr style="text-align: left;">
    <td class="da_subtitulo">IMAGEN</td>
    <td>
            <input type="text" name="archivo1" id="archivo1" value="<?php echo $arrObjCapa[0]->__get('_ico')?>" readonly="readonly"/>
            <input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
    </td>
    </tr>
    <?php 
    for ( $i=1; $i<=50; $i++ ) {

    $readonly = ( isset($arrayCampos[$i]) && $arrayCampos[$i] != '' ) ? '' : '';
    
    if ($i <= 40) {
        $text = 'varchar(100)';
    } elseif ( $i > 40 && $i <= 45) {
        $text = 'int';
    } elseif ( $i > 45 && $i <= 50) {
        $text = 'date';
    }
    
    ?>
    <tr style="text-align: left;">
            <td class="da_subtitulo">CAMPO <?php echo $i?></td>
            <td>
                <input type="text" size="40" class="campo" <?php echo $readonly?> name="campo<?php echo $i?>" id="campo<?php echo $i?>" value="<?php echo ( isset($arrayCampos[$i]) ) ?  $arrayCampos[$i] : '';?>" /> &nbsp;
                <?php echo $text?>
            </td>
    </tr>
    <?php 
    }
    ?>
    </table>
    </div>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody>
    <tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px">
                <input type="hidden" name="idcapa" id="idcapa" value="<?php echo $arrObjCapa[0]->__get('_idCapa')?>" />
                <input type="button" value="Grabar" name="btn_grabar" title="Grabar" id="btn_grabar" onclick="javascript: updateDatosCapa();" class="boton">&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" onclick="javascript: cerrarVentana();" class="boton">
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
    <div style="margin: 10px 0 0 0; display: none;" id="msg">
        <img alt="" src="img/loading.gif"> <b>Grabando datos.</b>
    </div>
</div>