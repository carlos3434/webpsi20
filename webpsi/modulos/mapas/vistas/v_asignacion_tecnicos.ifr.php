<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

        <script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
        <script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>
        <script type="text/javascript" src="../../../js/administracion/Util.js"></script>

        <script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.filter.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.autocomplete.min.js"></script>

        <script type="text/javascript" src="../../../js/cv.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.PrintArea.js"></script>

        <link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../css/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" media="all"/>
        <link rel="stylesheet" href="../../../css/jquery.autocomplete.css" type="text/css" />

        <script type="text/javascript">
            $(function(){
                $("select.clsMultiple").multiselect().multiselectfilter();
            });
        </script>
        <script type="text/javascript">

            var infoWindow = null;

            var myLayout;
            var map = null;
            var poligonosDibujarNodo_array = [];
            var poligonosDibujarTroba_array = [];

            var ptos_arr = [];
            var markers_arr = [];
            var makeMarkers_arr = [];

            var infoWindow_arr = [];

            var capaNodo_exists = 0;
            var capaTroba_exists = 0;

            var ptos_xy_trm = [];
            var ptos_xy_arm = [];

            $(document).ready(function () {
                
                myLayout = $('body').layout({
                    // enable showOverflow on west-pane so popups will overlap north pane
                    west__showOverflowOnHover: true,
                    west: {size:420}
                });

                load_array = function(element){
                    var grupo=new Array();
                    $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
                    return grupo;
                }
                
                cambiaEmpresa = function() {
                    $("input[name=tecnico]").unbind();
                    autocomplete();
                    
                    var negocio     = $("#negocio").val();
                    var zonal       = $("#zonal").val();
                    var empresa     = $("#empresa").val();
        
                    M.cambiarZonal(negocio, zonal, empresa);
                }
                
                autocomplete = function() {
                    $("input[name=tecnico]").autocomplete("../asignacion.tecnicos.php",{
                        extraParams :{
                            action  : 'buscarTecnico',
                            zonal   : $('#zonal').val(),
                            empresa : $('#empresa').val()
                        },
                        minChars    : 3,
                        cacheLength : 0,
                        dataType    : "json",
                        width       : 320,
                        parse: function(data) {
                            return $.map(data, function(row) {

                                return {
                                    data: row,
                                    value: row.cip,
                                    result: row.cip
                                }
                            });
                        },
                        formatItem: function(row) {
                            return "<div style='font-size:11px;'>CIP: <span style='font-weight:bold;'>"+row.cip+"</span> - "+row.nombre+"</div>";
                        }
                    }).result(function(e, item) {
                        //alert("fin");
                    });
                }
    
                
    
                cambiaNegocio = function() {
                    $("input[name='multiselect_nodo[]']").removeAttr('checked');
                    $("input[name='multiselect_troba[]']").removeAttr('checked');
                    $("input[name='multiselect_mdf[]']").removeAttr('checked');
        
                    //setea el combo zonal a LIM
                    $("#zonal").val('LIM');
        
                    var negocio = $("#negocio").val();
                    var empresa = $("#empresa").val();
        
                    M.cambiarZonal(negocio, 'LIM', empresa);
        
                    if (negocio == 'CATV') {
                        $(".clsCatv").show();
                        $(".clsStb").hide();
                    }else if (negocio == '') {
                        $(".clsCatv").hide();
                        $(".clsStb").hide();
                    }else {
                        $(".clsCatv").hide();
                        $(".clsStb").show();
                    }
                }
    
                cambiarZonal = function() {
                    var negocio     = $("#negocio").val();
                    var zonal       = $("#zonal").val();
                    var empresa     = $("#empresa").val();
        
                    M.cambiarZonal(negocio, zonal, empresa);
                    
                    $("input[name=tecnico]").unbind();
                    autocomplete();
                }
   
                $("#troba").multiselect({
                    close: function(){
                        $("#poligono_troba").removeAttr('checked');
                    }
                });

                $("#divAverias").click(function() {
                    M.asignacion_multiple();
                });
	
                $("#clear").click(function() {
                    M.clear();
                });
    
                $("#poligono_nodo").click(function() {
                    M.dibujaMapaNodo();
                });
        
                $("#poligono_troba").click(function() {
                    M.dibujaMapaTroba();
                });
        
	
                $('#selTodos')
                .filter(':has(:checkbox:checked)')
                .end()
                .click(function(event) {
                    if ($("#selTodos").is(':checked')) { 
                        $(".lista_chk").attr('checked', 'checked');
                    }else {  
                        $(".lista_chk").removeAttr('checked');
                    }		
                });
    
    
                AveriasTerminal = function(zonal, mdf, carmario, cbloque, terminal) {
                    M.AveriasTerminal(zonal, mdf, carmario, cbloque, terminal)
                }
                
                asignarTecnicoPedido = function(zonal, tipo, empresa, celula, strActu) {
                    M.asignarTecnicoPedido(zonal, tipo, empresa, celula, strActu);
                }
    
                imprimirEnMapaXY = function() {
                    M.imprimirEnMapaXY();
                }
    
                AsignacionMultiple = function(zonal, mdf, carmario, cbloque, terminal) {
                    M.AsignacionMultiple(zonal, mdf, carmario, cbloque, terminal);
                }
    
    
                $('#btnFiltrar').click(function(){
                    listar_resultados();
                });
    
    
                var M = {
                    initialize: function() {

                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com');
                            return false;
                        }
    	
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var myOptions = {
                            zoom: 11,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                    },

                    clearMarkers: function() {
                        for (var n = 0, marker; marker = markers_arr[n]; n++) {
                            marker.setVisible(false);
                        }
                    },
		
                    clearMakeMarkers: function() {
                        for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                            makeMarker.setVisible(false);
                        }
                    },

                    clearPoligonosNodo: function() {
                        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujarNodo_array[n]; n++) {
                            poligonoDibujar.setMap(null);
                        }
                    },
        
                    clearPoligonosTroba: function() {
                        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujarTroba_array[n]; n++) {
                            poligonoDibujar.setMap(null);
                        }
                    },

                    loadPoligono: function(xy_array, tipo) {

                        var datos 		= xy_array['coords'];
                        var color_poligono 	= xy_array['color'];
                        var poligonoCoords 	= [];

                        if ( datos.length > 0) {
                            for (var i in datos) {
                                var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                poligonoCoords.push(pto_poligono);
                            }

                            // Construct the polygon
                            // Note that we don't specify an array or arrays, but instead just
                            // a simple array of LatLngs in the paths property
                            poligonoDibujar = new google.maps.Polygon({
                                paths: poligonoCoords,
                                strokeColor: color_poligono,
                                strokeOpacity: 0.6,
                                strokeWeight: 3,
                                fillColor: color_poligono,
                                fillOpacity: 0.5
                            });

                            //agrego al array las capas seleccionadas
                            if ( tipo == 'nodo') {
                                poligonosDibujarNodo_array.push(poligonoDibujar);
                                //seteamos que la capa existe
                                capaNodo_exists = 1;
                            }else if ( tipo == 'troba') {
                                poligonosDibujarTroba_array.push(poligonoDibujar);
                                //seteamos que la capa existe
                                capaTroba_exists = 1;
                            }

                            //seteo la capa en el mapa
                            poligonoDibujar.setMap(map);
                        }
                        else {
                            if ( tipo == 'nodo') {
                                capaNodo_exists = 0;
                            }else if ( tipo == 'troba') {
                                capaTroba_exists = 1;
                            }
                        }		
                    },
        
                    cambiarZonal: function(negocio, zonal, empresa) {
                        var data_content = {
                            'action'  : 'cambiarZonal',
                            'negocio' : negocio,
                            'zonal'   : zonal,
                            'empresa' : empresa
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../asignacion.tecnicos.php",
                            data:   data_content,
                            dataType: "html",
                            success: function(datos) {
                                if ( datos != '' ) {
                        
                                    if (negocio == 'CATV') {
                                        var data_arr = datos.split("|");

                                        $("#divNodo").html(data_arr[0]);
                                        $("#divTroba").html(data_arr[1]);
                                    }else {
                                        $("#divMdf").html(datos);
                                    }
                                }
                            }
                        });
                    },
        
                    dibujaMapaNodo: function() {
                        if ($("#poligono_nodo").is(':checked')) { 

                            var arreglo_nodo = new Array();
                            arreglo_nodo = load_array('multiselect_nodo[]');

                            var data_content = {
                                'action'        : 'dibujaMapaNodo',
                                'area_mdf'      : 'si',
                                'arreglo_nodo'  : arreglo_nodo
                            };
                            $.ajax({
                                type:   "POST",
                                url:    "../asignacion.tecnicos.php",
                                data:   data_content,
                                dataType: "json",
                                success: function(data) {
                                    if ( data['success'] == '1' ) {
                                        if ( capaNodo_exists ) {
                                            //limpiamos todas las capas del mapa
                                            M.clearPoligonosNodo();
                                        }

                                        var poligonos = data['xy_poligono'];
                                        if ( poligonos.length > 0 ) {
                                            //pintamos los poligonos
                                            for (var i in poligonos) {
                                                M.loadPoligono(poligonos[i], 'nodo');
                                            }

                                            //setea el zoom y el centro
                                            map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                            map.setZoom(13);
                                        }else {
                                            alert("No ha seleccionado ningun nodo para dibujar en el mapa");
                                            return false;
                                        }
                                    }
                                    else {
                                        alert(data['msg']);
                                        return false;
                                    }                                    
                                }
                            });

                        }else {  
                            if ( capaNodo_exists ) {
                                //limpiamos todas las capas del mapa
                                M.clearPoligonosNodo();
                            }
                        }
                    },
        
                    dibujaMapaTroba: function() {
                        if ($("#poligono_troba").is(':checked')) { 

                            var arreglo_nodo    = new Array();
                            var arreglo_troba   = new Array();
                            arreglo_nodo    = load_array('multiselect_nodo[]');
                            arreglo_troba   = load_array('multiselect_troba[]');

                            if ( arreglo_nodo.length < 1 ) {
                                alert("Debe seleccionar algun nodo o troba");
                                return false;
                            }

                            var data_content = {
                                'action'        : 'dibujaMapaTroba',
                                'area_troba'    : 'si',
                                'arreglo_troba' : arreglo_troba
                            };
                            $.ajax({
                                type:   "POST",
                                url:    "../asignacion.tecnicos.php",
                                data:   data_content,
                                dataType: "json",
                                success: function(data) {
                                    
                                    if ( data['success'] == '1' ) {
                                        if ( capaTroba_exists ) {
                                            //limpiamos todas las capas del mapa
                                            M.clearPoligonosTroba();
                                        }

                                        var poligonos = data['xy_poligono'];
                                        if ( poligonos.length > 0 ) {
                                            //pintamos los poligonos
                                            for (var i in poligonos) {
                                                M.loadPoligono(poligonos[i], 'troba');
                                            }

                                            //setea el zoom y el centro
                                            map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                            map.setZoom(15);
                                        }else {
                                            alert("No ha seleccionado ninguna troba para dibujar en el mapa");
                                            return false;
                                        }
                                    }
                                    else {
                                        alert(data['msg']);
                                        return false;
                                    }

                                    
                                }
                            });

                        }else {
                            if ( capaTroba_exists ) {
                                //limpiamos todas las capas del mapa
                                M.clearPoligonosTroba();
                            }
                        }
                    },

                    asignacion_multiple: function() {
                        parent.$("#childModal").html("Cargando...");
                        parent.$("#childModal").css("background-color","#FFFFFF");
                        //$.post("v_averias_terminal.popup.php", {
                        $.post("../asignacion.tecnicos.php", {
                            action              : 'averiasTerminalMultiple',
                            asignacion_multiple : 1
                        },
                        function(data){
                            parent.$("#childModal").html(data);
                        }
                    );

                        parent.$("#childModal").dialog({modal:true, width:"500px", minHeight:300, hide: "slide", title: "Averias seleccionadas: ", position:"top"});
       
                    },
        
                    clear: function() {
                        var data_content = {
                            'action'	: 'clear'
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../asignacion.tecnicos.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {

                                if ( data['result'] == '1' ) {

                                    alert("Limpiando los taps seleccionados.");					
                                    $('#num_averias').html(0);
                                    $('#divNumTaps').hide('slide', {direction: 'up'}, 1000);
                                }
                            }
                        });
                    },
        
                    AveriasTerminal: function(zonal, mdf, carmario, cbloque, terminal) {
                        parent.$("#childModal").html("Cargando...");
                        parent.$("#childModal").css("background-color","#FFFFFF");
                        //$.post("v_averias_terminal.popup.php", {
                        $.post("../asignacion.tecnicos.php", {
                            action              : 'averiasTerminal',
                            zonal               : zonal,
                            mdf                 : mdf,
                            carmario            : carmario,
                            cbloque             : cbloque,
                            terminal            : terminal,
                            asignacion_multiple : 0
                        },
                        function(data) {
                            parent.$("#childModal").html(data);
                        }
                        );

                        parent.$("#childModal").dialog({modal:true, width:"500px", minHeight:300, hide: "slide", title: "PEDIDOS ENCONTRADOS EN ESTE TERMINAL: " + terminal, position:"top"});

                    },
                    
                    asignarTecnicoPedido: function(zonal, tipo, empresa, celula, strActu) {
                        parent.$("#childModal").html("Cargando...");
                        parent.$("#childModal").css("background-color","#FFFFFF");
                        $.post("../../../modulos/gescom/asignacion.tecnicos.php?cmd=vistaAsignarTecnicoVisor&rdn=4.949174452645028", {
                                zonal       : zonal,
                                tipo        : tipo,
                                empresa     : empresa,
                                celula      : celula,
                                strActu     : strActu,
                                v_origen    : 'mapas'
                            },
                            function(data) {
                                parent.$("#childModal").html(data);
                            }
                        );

                        parent.$("#childModal").dialog({modal:true, width:"500px", minHeight:300, hide: "slide", title: "ASIGNAR TECNICOS", position:"top"});

                    },
        
                    imprimirEnMapaXY: function() {
                        var chk_val = contar_check_temas(".lista_chk");

                        var averias_arr = [];

                        for( i in chk_val) {
                            var cod_averia = chk_val[i].split("|");
                            averias_arr.push(cod_averia[0]);
                        }

                        if (chk_val.length > 35) {
                            alert("Solo se pueden imprimir en Mapa hasta 35 averias.");

                        }else {
                            if (chk_val.length>0){           
                                parent.$("#childModal").html("Cargando...");                
                                $.post("v_gescomp_actuacionesCATV_imprimir_mapa.php", {
                                    printAll : "ok",
                                    averias  :  averias_arr
                                },function(data){            
                                    parent.$("#childModal").html(data);
                                });
                                parent.$("#childModal").dialog({
                                    modal : true,
                                    width : '750px',
                                    maxHeight : '500px',
                                    hide : 'slide',
                                    position : 'top',
                                    title : "Imprimir Mapa de averias"
                                });      
                            }else{
                                alert("Ninguna Averia seleccionada para imprimir en mapa!");
                            }
                        }
                    },

                    AsignacionMultiple: function(zonal, mdf, carmario, cbloque, terminal) {

                        var data_content = {
                            'action'	: 'asignacionMultiple',
                            'zonal'	: zonal,
                            'mdf'	: mdf,
                            'carmario'	: carmario,
                            'cbloque'	: cbloque,
                            'terminal'	: terminal
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../asignacion.tecnicos.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                                if ( data['averias'].length > 0 ) {

                                    var datos = data['averias']; 
                                    var str_averias = '';
                                    for (var i in datos) {
                                        var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                        str_averias += "\n" + datos[i]['averia'];
                                    }
                                    alert("Averia(s) agregada(s): " + str_averias);

                                    $('#num_averias').html(data['num_taps']);

                                    if ( data['num_taps'] > 0 ) {
                                        $('#divNumTaps').show('slide', {direction: 'down'}, 1000);
                                    }

                                }else {
                                    alert('Ninguna Averia agregada');
                                }
                            }
                        });
                    }
        
        
                };
	

                M.initialize();
                
                autocomplete();
               
            });


            function listar_resultados() {
                
                if ( $('#tipo').val() == '' ) {
                    alert('Ingrese Tipo');
                    return false;
                }
                if ( $('#negocio').val() == '' ) {
                    alert('Ingrese Negocio');
                    return false;
                }


                infoWindow = new google.maps.InfoWindow();
                var markerBounds = new google.maps.LatLngBounds();
                var markerArray = [];


                function makeMarker(options) {
                    var pushPin = null;
                    
                    if ( options.position != '(0, 0)' ) {
                        pushPin = new google.maps.Marker({
                            map: map
                        });

                        makeMarkers_arr.push(pushPin);

                        pushPin.setOptions(options);
                        google.maps.event.addListener(pushPin, "click", function(){
                            infoWindow.setOptions(options);
                            infoWindow.open(map, pushPin);
                            if (this.sidebarButton)this.sidebarButton.button.focus();
                        });
                        var idleIcon = pushPin.getIcon();

                        markerBounds.extend(options.position);
                        markerArray.push(pushPin);
                    }else {
         	
                        pushPin = new google.maps.Marker({
                            map: map
                        });
			
                        makeMarkers_arr.push(pushPin);
			
                        pushPin.setOptions(options);
                    }
        
                    //almaceno los infowindows
                    infoWindow_arr.push(infoWindow);
         
                    if (options.sidebarItem){
                        pushPin.sidebarButton = new SidebarItem(pushPin, options);
                        pushPin.sidebarButton.addIn("sidebar");
                    }

                    return pushPin;       	
                }

                google.maps.event.addListener(map, "click", function(){
                    infoWindow.close();
                });


                /*
                 * Creates an sidebar item 
                 * @param marker
                 * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                function SidebarItem(marker, opts) {
                    var tag = opts.sidebarItemType || "div";
                    var row = document.createElement(tag);
                    row.innerHTML = opts.sidebarItem;
                    row.className = opts.sidebarItemClassName || "sidebar_item";  
                    row.style.display = "block";
                    row.style.width = opts.sidebarItemWidth || "390px";
                    row.onclick = function(){
                        google.maps.event.trigger(marker, 'click');
                    }
                    row.onmouseover = function(){
                        google.maps.event.trigger(marker, 'mouseover');
                    }
                    row.onmouseout = function(){
                        google.maps.event.trigger(marker, 'mouseout');
                    }
                    this.button = row;
                }
                // adds a sidebar item to a <div>
                SidebarItem.prototype.addIn = function(block){
                    if (block && block.nodeType == 1)this.div = block;
                    else
                        this.div = document.getElementById(block)
                        || document.getElementById("sidebar")
                        || document.getElementsByTagName("body")[0];
                    this.div.appendChild(this.button);
                }
	 
	
                $('#num_averias').html(0);
                $('#divNumTaps').hide();
                $("#selTodos").removeAttr('checked');

                var arreglo_nodo  = new Array();
                var arreglo_troba = new Array();
                var arreglo_mdf   = new Array();

                arreglo_nodo 	 = load_array('multiselect_nodo[]');
                arreglo_troba 	 = load_array('multiselect_troba[]');
                arreglo_mdf 	 = load_array('multiselect_mdf[]');

                var data_content = {
                    'action'        : 'buscarPendientes',
                    'tipo'          : $('#tipo').val(),
                    'negocio'       : $('#negocio').val(),
                    'zonal'         : $('#zonal').val(),
                    'arreglo_nodo'  : arreglo_nodo,
                    'arreglo_troba' : arreglo_troba,
                    'arreglo_mdf'   : arreglo_mdf,
                    'tecnico'       : $('#tecnico').val(),
                    'empresa'       : $('#empresa').val(),
                    'prioridad'     : $('#prioridad').val()
                };

                //$('#btnFiltrar').attr('disabled', 'disabled');
                $('#box_loading').show();
                $('#sidebar').empty();
                var tabla = '';
                var detalle = '';
                $.ajax({
                    type:   "POST",
                    url:    "../asignacion.tecnicos.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {
		 
                        clearMarkers();
                        clearMakeMarkers();
            
                        clearInfoWindow();

                        ptos_xy_trm = [];
                        ptos_xy_arm = [];

                        if ( typeof(data['pedidos']) == 'undefined' ) {
                            alert(data['msg']);
                            $('#btnFiltrar').removeAttr('disabled');
                            $('#box_loading').hide();
                            $('#spanNum').html('0');
                            $('#sidebar').empty();
                        }
                        else {
                            
                            if ( data['pedidos'].length > 0 ) {
			
                                if ( data['pedidos_con_xy'].length == 0 ) {

                                    var datos = data['pedidos']; 

                                    //x1, y1 por defecto en Lima
                                    var x1 = -77.0407839355469;
                                    var y1 = -12.0692083678587;
                                    if ( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
                                        x1 = data['zonal']['x'];
                                        y1 = data['zonal']['y'];
                                    }

                                    var num = 1;
                                    for (var i in datos) {

                                        var cip = style_color = '';
                                        if ( datos[i]['cip'] != null ) {
                                            cip = datos[i]['cip'];
                                            style_color = 'style="background-color:#FF0000; color:#FFFFFF; padding:2px; font-weight:normal;"';
                                        }
                                        
                                        tabla = '<div style="width:25px; float:left; text-align:center; background-color:#ff0000; color:#ffffff;">' + num + '</div>';
                                        tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" class="lista_chk" type="checkbox" value="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" name="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS"></div>';
                                        tabla += '<div style="width:70px; float:left; font-weight:bold;">' + datos[i]['pedido'] + '<br /><span style="font-weight:normal;">Nodo: ' + datos[i]['mdf'] +'<br />Troba: ' + datos[i]['carmario'] +  '<br />Amp.: ' + datos[i]['cbloque'] + '<br />Tap.: ' + datos[i]['terminal'] + '</span><br /><a style="font-weight:normal;" title="Asignar / Re-asignar t&eacute;cnico" href="javascript:AveriasTerminal(\''+ datos[i]['zonal'] +'\', \''+ datos[i]['mdf'] +'\', \''+ datos[i]['carmario'] +'\', \''+ datos[i]['cbloque'] +'\', \''+ datos[i]['terminal'] +'\');">Asig. t&eacute;cnico</a></div>';
                                        tabla += '<div style="width:215px; float:left;">';
                                        tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['cliente'] + '</div>';
                                        tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion'] + ' - ' + datos[i]['des_distrito'] + ' - <span style="color:#000000;">' + datos[i]['zonal'] + '<span></div>';
                                        tabla += '<div style="font-size:10px;color:#555555;">' + datos[i]['telefono'] + '</div>';
                                        tabla += '<div style="font-size:10px;margin-top:5px;"><span style="color:#555555;">Fec.Reg.</span> <span style="color:#1155CC;">' + datos[i]['fec_reg'] + '</span> <span style="color:#ff0000;">( ' + datos[i]['dias_pend'] + ' dias )</span></div>';
                                        tabla += '</div>';
                                        tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="' + datos[i]['nomtecnico'] + '" ' + style_color + '>' + cip + '</span></div>';

                                        /* markers and info window contents */
                                        makeMarker({
                                            draggable: false,
                                            position: new google.maps.LatLng(y1, x1),
                                            title: '',
                                            sidebarItem: tabla,
                                            //content: detalle,
                                            icon: "../../../img/markergreen.png"
                                        });

                                        num++;
                                    }

                                    //centra el mapa
                                    map.setZoom(12);

                                }else {


                                    var datos = data['pedidos']; 

                                    var num = 1;
                                    var x_ini = y_ini = '';
                                    var flag_captura_xy = 1;
                                    for (var i in datos) {

                                        var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                        var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                        //capturo el primer punto de la fftt para centrar el mapa
                                        if ( flag_captura_xy && x != '' && y != '' ) {
                                            x_ini = datos[i]['x'];
                                            y_ini = datos[i]['y'];
                                            flag_captura_xy = 0;
                                        }

                                        var style_num = "background-color:#ff0000;color:#ffffff;border:1px solid #ff0000;";
                                        if ( x != '' && y != '' ) {
                                            style_num = "border:1px solid #3D3D3D;";
                                        }

                                        var cip = style_color = '';
                                        if ( datos[i]['cip'] != null ) {
                                            cip = datos[i]['cip'];
                                            style_color = 'style="background-color:#FF0000; color:#FFFFFF; padding:2px; font-weight:normal;"';
                                        }

                                        var estado_trm = 1;
                                        if ($("#chkPrioridad").is(':checked')) { 
                                            if ( datos[i]['fec_reg'] != null ) {
                                                if ( datos[i]['dias_pend'] < 2 ) {
                                                    estado_trm = 1;
                                                }else if ( datos[i]['dias_pend'] >= 2 && datos[i]['dias_pend'] <= 3 ) {
                                                    estado_trm = 2;
                                                }else if ( datos[i]['dias_pend'] > 3 ) {
                                                    estado_trm = 3;
                                                }
                                            }
                                        }


                                        var con_tecnico = '';
                                        if ( datos[i]['flag'] == '1' ) {
                                            con_tecnico = 'T';
                                        }

                                        var celula = ( datos[i]['idcelula'] == null ) ? '' : datos[i]['idcelula'];
                                        
                                        var idgestion_actuaciones = ( datos[i]['idgestion_actuaciones'] == null ) ? '0' : '1';
                                        var idgestion             = ( datos[i]['idgestion_actuaciones'] == null ) ? '' : datos[i]['idgestion_actuaciones'];
                                        
                                        //detectando numero de averias por fftt
                                        var averias_list = datos[i]['averias_list'];
                                        var num_averias = averias_list.length;
                                        
                                        var strActu = '';
                                        var str_averias = '<tbody>';
                                        for(var a in averias_list) {
                                            var l_cip = '&nbsp;';
                                            if ( averias_list[a]['l_cip'] != null ) {
                                                l_cip = averias_list[a]['l_cip'];
                                            }
                                            str_averias += '<tr><td class="celdaX" align="center" style="color:#ff0000;"><span title="' + averias_list[a]['l_nomtecnico'] + '">' + l_cip + '</span></td><td class="celdaX" align="center">' + averias_list[a]['l_averia'] + '</td><td class="celdaX">' + averias_list[a]['l_cliente'] + '</td><td class="celdaX" align="center">' + averias_list[a]['l_fecreg'] + '</td></tr>';
                                            
                                            //listado de averias del terminal/tap
                                            strActu += ',' + datos[i]['pedido'] + '|' + idgestion_actuaciones + '|' + $('#tipo').val() + '|' + idgestion + '|' + datos[i]['idempresa_webu'] + '|' + datos[i]['zonal'] + '|'  + datos[i]['negocio'] + '|' + datos[i]['tabla_origen'] + '|' + datos[i]['fecha_agenda_full'] + '|' + datos[i]['actu'];
                                        }
                                        str_averias += '</tbody>';

                                        

                                        //llenando array para Imprimir en Mapa
                                        ptos_xy_trm.push(datos[i]['terminal'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '||' + datos[i]['carmario'] + '|' + datos[i]['cbloque'] + '|' + datos[i]['des_ave']);
                                        //ptos_xy_trm.push(datos[i]['caja'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '|' + datos[i]['ccable'] + '|' + datos[i]['carmario'] + '|' + datos[i]['tipo_red'] + '|' + datos[i]['des_ave']);

                                        tabla = '<div style="width:25px; float:left; font-size:10px; text-align:center; ' + style_num + '">' + num + '</div>';
                                        tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" class="lista_chk" type="checkbox" value="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" name="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS"></div>';
                                        if ( $('#negocio').val() != 'CATV' ) {
                                            tabla += '<div style="width:70px; float:left; font-weight:bold;">' + datos[i]['pedido'] + '<br /><span style="font-weight:normal;">Mdf: ' + datos[i]['mdf'] +'<br />Arm.: ' + datos[i]['carmario'] +  '<br />Ter.: ' + datos[i]['terminal'] + '</span><br /><a style="font-weight:normal;" title="Asignar / Re-asignar t&eacute;cnico" href="javascript:asignarTecnicoPedido(\''+ datos[i]['zonal'] +'\', \''+ $('#tipo').val() +'\', \''+ datos[i]['idempresa_webu'] +'\', \''+ celula +'\', \''+ strActu +'\');">Asig. t&eacute;cnico</a></div>';
                                        }else {
                                            tabla += '<div style="width:70px; float:left; font-weight:bold;">' + datos[i]['pedido'] + '<br /><span style="font-weight:normal;">Nodo: ' + datos[i]['mdf'] +'<br />Troba: ' + datos[i]['carmario'] +  '<br />Amp.: ' + datos[i]['cbloque'] + '<br />Tap.: ' + datos[i]['terminal'] + '</span><br /><a style="font-weight:normal;" title="Asignar / Re-asignar t&eacute;cnico" href="javascript:AveriasTerminal(\''+ datos[i]['zonal'] +'\', \''+ datos[i]['mdf'] +'\', \''+ datos[i]['carmario'] +'\', \''+ datos[i]['cbloque'] +'\', \''+ datos[i]['terminal'] +'\');">Asig. t&eacute;cnico</a></div>';
                                        }
                                        tabla += '<div style="width:215px; float:left;">';
                                        tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['cliente'] + '</div>';
                                        tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion'] + ' - ' + datos[i]['des_distrito'] + ' - <span style="color:#000000;">' + datos[i]['zonal'] + '<span></div>';
                                        tabla += '<div style="font-size:10px;color:#555555;">' + datos[i]['telefono'] + '</div>';
                                        tabla += '<div style="font-size:10px;margin-top:5px;"><span style="color:#555555;">Fec.Reg.</span> <span style="color:#1155CC;">' + datos[i]['fec_reg'] + '</span> <span style="color:#ff0000;">( ' + datos[i]['dias_pend'] + ' dias )</span></div>';
                                        tabla += '</div>';
                                        tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="' + datos[i]['nomtecnico'] + '" ' + style_color + '>' + cip + '</span></div>';


                                        detalle = '<div style="width:400px;">';
                                        detalle += '<div>';
                                        detalle += '<table width="100%">';
                                        detalle += '<tr><td class="subtit_info">CONTRATA:</td><td class="det_info">' + datos[i]['eecc'] + '</td><td class="subtit_info">ZONAL:</td><td class="det_info">' + datos[i]['zonal'] + '</td></tr>';
                                        if ( $('#negocio').val() != 'CATV' ) {
                                            detalle += '<tr><td class="subtit_info">MDF:</td><td class="det_info">' + datos[i]['mdf'] + '</td><td class="subtit_info">PAR PRIM.:</td><td class="det_info">' + datos[i]['cbloque'] + '</td></tr>';
                                            detalle += '<tr><td class="subtit_info">ARMARIO:</td><td class="det_info">' + datos[i]['carmario'] + '</td><td class="subtit_info">TERMINAL:</td><td class="det_info">' + datos[i]['terminal'] + '</td></tr>';
                                        }else {
                                            detalle += '<tr><td class="subtit_info">NODO:</td><td class="det_info">' + datos[i]['mdf'] + '</td><td class="subtit_info">TROBA:</td><td class="det_info">' + datos[i]['carmario'] + '</td></tr>';
                                            detalle += '<tr><td class="subtit_info">AMPLIFICADOR:</td><td class="det_info">' + datos[i]['cbloque'] + '</td><td class="subtit_info">TAP:</td><td class="det_info">' + datos[i]['terminal'] + '</td></tr>';
                                        }
                                        detalle += '</table>';
                                        detalle += '</div>';
                                        detalle += '<div style="font-size: 12px;margin: 5px 0 0 0;"><span style="color:#777777;">AVERIAS:</span> <span style="color:#1155CC;">' + num_averias + ' encontrada(s)</span></div>';
                                        detalle += '<div style="margin: 5px 0 0 0;display:block;float:left;">';
                                        detalle += '<table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0">';
                                        detalle += '<thead><tr><th class="headTbl" width="50">Tecnico</th><th class="headTbl" width="60">Averia</th><th class="headTbl" width="200">Cliente</th><th class="headTbl" width="90">Fec.registro</th></tr></thead>';

                                        detalle += str_averias;

                                        detalle += '</table></div>';

                                        detalle += '<div style="margin:15px 0 0 0; float:left; display:block; border-top:1px solid #F0F0E7; padding: 10px 0 0 0; font-size:12px;">';
                                        //detalle += '<a title="Asignar / Re-asignar t&eacute;cnico" href="javascript:AveriasTerminal(\''+ datos[i]['zonal'] +'\', \''+ datos[i]['mdf'] +'\', \''+ datos[i]['carmario'] +'\', \''+ datos[i]['cbloque'] +'\', \''+ datos[i]['terminal'] +'\');">Asignar / Re-asignar t&eacute;cnico</a> &nbsp; &nbsp; ';
                                        detalle += '<a title="Asignar / Re-asignar t&eacute;cnico" href="javascript:asignarTecnicoPedido(\''+ datos[i]['zonal'] +'\', \''+ $('#tipo').val() +'\', \''+ datos[i]['idempresa_webu'] +'\', \''+ celula +'\', \''+ strActu +'\');">Asignar / Re-asignar t&eacute;cnico</a> &nbsp; &nbsp; ';
                                        detalle += '<a title="Asignaci&oacute;n multiple" href="javascript:AsignacionMultiple(\''+ datos[i]['zonal'] +'\', \''+ datos[i]['mdf'] +'\', \''+ datos[i]['carmario'] +'\', \''+ datos[i]['cbloque'] +'\', \''+ datos[i]['terminal'] +'\');">Asignaci&oacute;n multiple</a>';
                                        detalle += '</div>';
                                        detalle += '</div>';

                                        var ico_map = "../../../img/map/tap/tap_" + datos[i]['terminal'] + "--" + estado_trm + con_tecnico + ".gif";
                                        var tipo_trm = "Tap:";
                                        if ( $('#negocio').val() != 'CATV' ) {
                                            var ico_prov = '';
                                            if ( datos[i]['terminal'].length == 4 ) {
                                                ico_prov = datos[i]['terminal'].substr(1,3);
                                            }if ( datos[i]['terminal'].length == 3 ) {
                                                ico_prov = datos[i]['terminal'];
                                            }if ( datos[i]['terminal'].length == 2 ) {
                                                ico_prov = '0' + datos[i]['terminal'];
                                            }if ( datos[i]['terminal'].length == 1 ) {
                                                ico_prov = '00' + datos[i]['terminal'];
                                            }
                                            
                                            ico_map = "../../../img/map/terminal/trm_" + ico_prov + "--" + estado_trm + con_tecnico + ".gif";
                                            tipo_trm = "Caja:";
                                        }

                                        /* markers and info window contents */
                                        makeMarker({
                                            draggable: false,
                                            position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                            title: tipo_trm + datos[i]['terminal'] + " -> " + num_averias + " averia(s) detectada(s)",
                                            sidebarItem: tabla,
                                            content: detalle,
                                            icon: ico_map
                                        });

                                        num++;
                                    }

                                    /* fit viewport to markers */
                                    map.fitBounds(markerBounds);

                                    //map.setZoom(16);
                                    //centra en el primer
                                    map.setCenter(new google.maps.LatLng(y_ini, x_ini));

                                }

                            }
                            
                            //setTimeout("hideLoader()", 1000);
                            $('#box_loading').hide();
                            $('#btnFiltrar').removeAttr('disabled');
                            $('#spanNum').html(data['pedidos'].length);
                            
                        }
                        
                    }
                    
                });
            }

            function clearMarkers() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }
    
            function clearInfoWindow() {
    
                for(var i=0; i<infoWindow_arr.length; i++){
                    infoWindow_arr[i].close();
                }
            }

            function clearMakeMarkers() {
                for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                    makeMarker.setVisible(false);
                }
            }

        </script>
        <style type="text/css">
            .subtit_info {
                font-weight: bold;
                width: 100px;
            }
            .det_info {
                color: #666666;
            }
            .sidebar_item {
                float: left;
                padding: 5px 0;
                border-bottom: 1px solid #DDDDDD;
            }
            .ipt{
                border:none;
                color:#0086C3;
            }
            .ipt:focus{
                border:1px solid #0086C3;
            }
            body{
                font-family: Arial;
                font-size: 11px;
            }
            .ui-layout-pane {
                background: #FFF; 
                border: 1px solid #BBB; 
                padding: 7px; 
                overflow: auto;
            } 

            .ui-layout-resizer {
                background: #DDD; 
            } 

            .ui-layout-toggler {
                background: #AAA; 
            }
            .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
            .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
            .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
            .slt{
                border:1px solid #1E9EF9;
                background-color: #FAFAFA;
                height: 18px;
                font-size: 11px;
                color:#0B0E9D;
            }
            .box_checkboxes{
                border:1px solid #4C5E60;
                background: #E7F7F7;
                width:170px;
                height: 240px;
                overflow: auto;
                display:none;
                position: absolute;
                top:0px;
                left:0px;
                opacity:0.9;
            }
            .tb_data_box{
                width: 100%;
                border-collapse:collapse;
                font-family: Arial;
                font-size: 9px;
            }
            .showResult {
                position: fixed; 
                bottom: 0; 
                right: 0; 
                padding: 8px; 
                border: 1px solid #000000; 
                margin: 2px; 
                color: #FFFFFF;
                font-size: 12px;
            }
            .divOpacity {
                background-color: #000000;
                -moz-opacity: 0.5;
                opacity:.50;
                filter: alpha(opacity=50);
            }

            #divAverias, #clear {
                cursor: pointer;
            }



            table.tablesorter {
                background-color: #CDCDCD;
                font-family: Arial,Helvetica,sans-serif;
                font-size: 8pt;
            }
            table.tablesorter thead tr th, table.tablesorter tfoot tr th {
                background-color: #E6EEEE;
                border: 1px solid #FFFFFF;
                font-size: 8pt;
            }
            .headTbl {
                background-image: url("../../../img/bg_head_js.gif");
                /*background-position: center top;*/
                background-repeat: repeat-x;
                color: #000000;
                height: 20px;
                font-weight: normal;
            }
            table.tablesorter tbody td {
                background-color: #FFFFFF;
                color: #3D3D3D;
                vertical-align: middle;
                font-size: 10px;
            }

            .clsCatv, .clsStb {
                display: none;
            }

        </style>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
        <div class="ui-layout-west">


            <div id="divPoligono">

                <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
                    <tr>
                        <td colspan="2"><span class="l_tit">Asignaci&oacute;n de T&eacute;cnicos</span></td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Tipo:</td>
                        <td align="left">
                            <select id="tipo" name="tipo" style="font-size:11px;">
                                <option value="" selected="selected" >Seleccione</option>
                                <?php
                                $objSesionUsuarioTipoPedido = $_SESSION['USUARIO_TIPO_PEDIDO'];
                                if ($objSesionUsuarioTipoPedido) {
                                    $objTipoPedido = new data_TipoPedido();

                                    foreach ($objSesionUsuarioTipoPedido as $objeto) {
                                        $objTipoPedido = $objTipoPedido->offSetGet($conexion, $objeto->__get('_idTipoPedido'));
                                        ?>
                                        <option value="<?php echo $objTipoPedido->__get('_tipoPedido') ?>"><?php echo $objTipoPedido->__get('_tipoPedido') ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            <span id="callback"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Negocio:</td>
                        <td align="left">
                            <select id="negocio" name="negocio" style="font-size:11px;" onchange="cambiaNegocio();">
                                <option value="">Seleccione</option>
                                <?php
                                $objSesionUsuarioNegocio = $_SESSION['USUARIO_TIPO_NEGOCIO'];
                                if ($objSesionUsuarioNegocio) {
                                    $objTipoNegocio = new data_TipoNegocio();
                                    foreach ($objSesionUsuarioNegocio as $objeto) {
                                        $objTipoNegocio = $objTipoNegocio->offSetGet($conexion, $objeto->__get('_idTipoNegocio'));
                                        ?>
                                        <option value="<?php echo $objTipoNegocio->__get('_codNegocio') ?>"><?php echo $objTipoNegocio->__get('_negocio') ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            <span id="callback"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Empresa:</td>
                        <td align="left">
                            <select name="empresa" id="empresa" style="font-size:11px;" onchange="cambiaEmpresa()">
                                <?php
                                if ( !empty($_SESSION['USUARIO_EMPRESA']) && count($_SESSION['USUARIO_EMPRESA']) > 1 ) {
                                ?>
                                    <option value="">Todos</option>
                                <?php
                                }
                                $objEmpresa = new data_Empresa();
                                foreach ($_SESSION['USUARIO_EMPRESA'] as $empresa) {
                                    $objEmpresas = $objEmpresa->offSetGet($conexion, $empresa->__get('_idEmpresa'));
                                    ?>
                                    <option value="<?php echo $objEmpresas->__get('_idEmpresa') ?>"><?php echo $objEmpresas->__get('_descEmpresa') ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Zonal:</td>
                        <td align="left">
                            <select name="zonal" id="zonal" style="font-size:11px;" onchange="cambiarZonal();">
                                <?php
                                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                                    $sel = "";
                                    if ($objZonal->__get('_abvZonal') == 'LIM') {
                                        $sel = " selected ";
                                    }
                                    ?>
                                    <option value="<?php echo $objZonal->__get('_abvZonal') ?>" <?php echo $sel ?>><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                                    <?php
                                }
                                ?>
                            </select>

                            <span id="callback"></span>
                        </td>
                    </tr>
                    <tr class="clsCatv">
                        <td align="left" class="label">Nodo:</td>
                        <td align="left">
                            <div id="divNodo">
                                <select id="nodo" name="nodo" class="clsMultiple" multiple="multiple">
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr class="clsCatv">
                        <td align="left" class="label">Troba:</td>
                        <td align="left">
                            <div id="divTroba">
                                <select id="troba" name="troba" class="clsMultiple" multiple="multiple">
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr class="clsStb">
                        <td align="left" class="label">Mdf:</td>
                        <td align="left">
                            <div id="divMdf">
                                <select name="mdf" id="mdf" class="clsMultiple" multiple="multiple">
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">T&eacute;cnico:</td>
                        <td align="left">
                            <input type="text" name="tecnico" id="tecnico" onkeypress="return validarTextoNumero(event)" maxlength="10" size="10" style=" text-transform: uppercase;font-weight: bold; font-size: 12px; color: BLUE;" >
                                <span style="font-size:9px;">Ingrese CIP del t&eacute;cnico</span>    
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Prioridad:</td>
                        <td align="left">
                            <select style="font-size:11px;" name="prioridad" id="prioridad">
                                <option value="">Todos</option>
                                <option value="0">hoy</option>
                                <option value="1">1 dia</option>
                                <option value="2">2-3 dias</option>
                                <option value="3">> 3 dias</option>
                            </select> &nbsp; 
                            <input type="checkbox" name="chkPrioridad" id="chkPrioridad"> Mostrar mapa de prioridades 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="checkbox" name="poligono_nodo" id="poligono_nodo"> Mostrar Nodo 
                                <input type="checkbox" name="poligono_troba" id="poligono_troba"> Mostrar Troba
                                    </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%">
                                            <tr>
                                                <td width="30%"><input type="button" name="btnFiltrar" id="btnFiltrar" style="font-size:11px;" value="Buscar"></td>
                                                <td width="30%"><div id="box_loading" style="margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div></td>
                                                <td width="40%">
                                                    <div style="float:left;">Leyenda:</div> <div style="background-color: #FF0000;float: left;height: 12px;margin: 2px 5px;width: 25px;"></div> <div style="float:left;">Sin x,y</div>
                                                </td>
                                            </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    </table>

                                    </div>

                                    <div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">
                                        <div id="divInfoResult" style="margin-bottom: 8px; width: 385px;">
                                            <div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
                                            <div style="float:right;"><a href="javascript:imprimirSeleccionXY();">Imprimir seleccion</a> | <a href="javascript:imprimirEnMapaXY();">Imprimir en mapa</a></div>
                                        </div>
                                        <div id="divTitle" style="background-color: #003366;border: 1px solid #000000;color: #FFFFFF;float: left;padding: 3px 0;width: 385px;">
                                            <div style="width:25px;float:left;text-align:center;">#</div>
                                            <div style="width:30px;float:left;"><input type="checkbox" name="selTodos" id="selTodos"></div>
                                            <div style="width:70px;float:left;">Pedido</div>
                                            <div style="width:215px;float:left;">Cliente</div>
                                            <div style="width:40px;float:left;">Tecnico</div>
                                        </div>
                                        <div id="sidebar"></div>
                                    </div>

                                    </div>
                                    <div class="ui-layout-center">

                                        <div id="box_resultado">

                                            <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
                                            <div id="divNumTaps" class="showResult divOpacity" style="display: none;">
                                                <div id="divAverias"><span id="num_averias">0</span> tap(s) seleccionado(s). </div><br />
                                                <span style="float:right;" id="clear">Limpiar</span>
                                            </div>

                                        </div>


                                        <div id="parentModal" style="display: none;">
                                            <div id="childModal" style="padding: 10px; background: #fff;"></div>
                                        </div>
                                    </div>

                                    </body>
                                    </html>