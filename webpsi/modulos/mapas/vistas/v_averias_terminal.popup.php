<?php
require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


$obj_penPais = new data_PenPais();


$asignacion_multiple = ( isset($_POST['asignacion_multiple']) ) ? 1 : 0;
if( $asignacion_multiple ) {
	$titulo_averias = 'AVERIAS SELECCIONADAS - ASIGNACION DE TECNICO';

	$averias_array = array();
	foreach( $_SESSION['TECNICO_AVERIAS'] as $cod_averia => $averias ) {
		$averias_array[] = $cod_averia;
	}

	$clientes = $obj_penPais->__listarAveriasTerminal($conexion, '', '', '', '', '', $averias_array);

}else {
	$titulo_averias = 'AVERIAS ENCONTRADAS EN ESTE TERMINAL: ' . $_POST['terminal'];
	$clientes = $obj_penPais->__listarAveriasTerminal($conexion, $_POST['zonal'], $_POST['mdf'], $_POST['carmario'], $_POST['cbloque'], $_POST['terminal']);
}
	
?>
<html>
<head>
<script type="text/javascript">
$(document).ready(function() {
	$("#btnAsignar").click(function() {
	
		var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

		if (chx == "") {
			//alert("Debe seleccionar al menos una actuacion 222");
		} else {
			$("#btnAsignar").attr('disabled', 'disabled');
			$("#box_loading").show();
		}
	});
});
</script>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css"/>

</head>
<body>
<div style="height:200px; overflow:auto;">
<table id="tb_resultado" class="tableBorder" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
<tr>
	<td style=" background-color: #70C72A;color: #FFFFFF;font-size: 11px;font-weight: bold; line-height: 25px; text-align:center;" colspan="12"><?php echo $titulo_averias?></td>
</tr>
<tr>
	<th width="3%" style="text-align:center;">#</th>
	<th width="7%" style="text-align:center;">Opc</th>
	<th width="10%" style="text-align:center;">Tecnico</th>
	<th width="15%" style="text-align:center;">Averia</th>
	<th width="45%" style="text-align:left;">Cliente</th>
	<!--<th width="30%" style="text-align:left;">Direccion</th>-->
	<th width="20%" style="text-align:center;">Fec. registro</th>
	<!--
	<th width="10%" style="text-align:center;">Zonal</th>
	<th width="10%" style="text-align:center;">Nodo</th>
	<th width="12%" style="text-align:center;">Troba</th>
	<th width="12%" style="text-align:center;">Amplificador</th>
	<th width="12%" style="text-align:center;">Terminal</th>
	-->
</tr>
<?php 
if( !empty($clientes) ) {

	$obj_gestionActuaciones = new data_GestionActuaciones();
	
	$i = 1;
	foreach( $clientes as $cliente ) {
	
		//Verifico si existe en webunificada.gestion_actuaciones 
		$gest_actuacion_exists = $obj_gestionActuaciones->__getIdGestionActuacion($conexion, $cliente['averia']);
		
		$flag_averia = 0;
		$id_gestion_actu = '';
		if( $gest_actuacion_exists ) {
			$flag_averia = 1;
			$id_gestion_actu = $gest_actuacion_exists;
		}
		
		$style_color = '';
		if( $cliente['flag'] == 1 ) {
			$style_color = "background-color:#FF0000; color:#FFFFFF;";
		}
	
	?>
	<tr>
		<td style="text-align:center;"><?php echo $i?></td>
		<td style="text-align:center;"><input checked="checked" id="<?php echo $cliente['averia']?>|<?php echo $flag_averia?>|Averia|<?php echo $id_gestion_actu?>|<?php echo $cliente['idempresa_webu']?>|PEN_PAIS" class="lista_chk" type="checkbox" value="<?php echo $cliente['averia']?>|<?php echo $flag_averia?>|Averia|<?php echo $id_gestion_actu?>|<?php echo $cliente['idempresa_webu']?>|PEN_PAIS" name="<?php echo $cliente['averia']?>|<?php echo $flag_averia?>|Averia|<?php echo $id_gestion_actu?>|<?php echo $cliente['idempresa_webu']?>|PEN_PAIS"></td>
		<td style="text-align:center;<?php echo $style_color?>" ><span title="<?php echo $cliente['nomtecnico']?>"><?php echo ($cliente['cip'] != '') ? $cliente['cip'] : '&nbsp;'?></span></td>
		<td style="text-align:center;"><?php echo ($cliente['averia'] != '') ? $cliente['averia'] : '&nbsp;'?></td>
		<td style="text-align:left;"><?php echo ($cliente['cliente'] != '') ? $cliente['cliente'] : '&nbsp;'?></td>
		<!--<td style="text-align:left;"><?php echo ($cliente['direccion'] != '') ? $cliente['direccion'] : '&nbsp;'?></td>-->
		<td style="text-align:center;"><?php echo ($cliente['fecreg'] != '') ? obtenerFecha($cliente['fecreg']) : '&nbsp;'?></td>
		<!--
		<td style="text-align:center;"><?php echo ($cliente['zonal'] != '') ? $cliente['zonal'] : '&nbsp;'?></td>
		<td style="text-align:center;"><?php echo ($cliente['mdf'] != '') ? $cliente['mdf'] : '&nbsp;'?></td>
		<td style="text-align:center;"><?php echo ($cliente['carmario'] != '') ? $cliente['carmario'] : '&nbsp;'?></td>
		<td style="text-align:center;"><?php echo ($cliente['cbloque'] != '') ? $cliente['cbloque'] : '&nbsp;'?></td>
		<td style="text-align:center;"><?php echo ($cliente['terminal'] != '') ? $cliente['terminal'] : '&nbsp;'?></td>
		-->
	</tr>
	<?php 
	$i++;
	}
}
else {
?>
	<tr>
		<td colspan="7" style="text-align:center; height:50px;">No se encontro ningun cliente para este terminal.</td>
	</tr>
<?php 
}
?>
</table>
</div>

<div>
	<input type="button" style="float:left;" onclick="javascript:asignarTecnicoActuacionTdpMapa();" id="btnAsignar" value="Asignar / Re-asignar t&eacute;cnico"> &nbsp; 
	<div id="box_loading" style="float:left;margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none;">procesando.<img src="img/loading.gif" alt="" title="" /></div>
</div>

</body>
</html>




