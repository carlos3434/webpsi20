<script type="text/javascript">
var idComponente = <?php echo $arrObjPirata[0]->__get('_idDuna')?>;
var x = <?php echo ($arrObjPirata[0]->__get('_x') != '') ? $arrObjPirata[0]->__get('_x') : '""'?>;
var y = <?php echo ($arrObjPirata[0]->__get('_y') != '') ? $arrObjPirata[0]->__get('_y') : '""'?>;
$(document).ready(function () {
    $("#tabsDetalle").tabs();    
    $("#tabMapaPirata").click(function() {
        $("#tdMapaPirata").empty();        
        if( x != '' && y != '') {
            $("#tdMapaPirata").html('<iframe id="ifrMapaPirata" name="ifrMapaPirata" src="modulos/maps/vistas/v_bandeja.duna.internet.mapa.php?idComponente=' + idComponente + '" width="100%" height="360"></iframe>');
        }else {
            $("#tdMapaPirata").html('<p style="margin:40px;"><b>El pirata seleccionado no esta georeferenciado (no tiene x,y)</b><br />No puede visualizar su ubicaci&oacute;n.</p>');
        }
    });
});
</script>
<div id="content_pirata_detalle">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 5px; padding-left: 5px;">
            <div id="tabsDetalle" style="width:99%;">
			<ul>
				<li><a id="tabDetallePirata" href="#cont_pirata_detalle" style="color:#1D87BF; font-weight: 600">Datos del Pirata</a></li>
                <li><a id="tabMapaPirata" href="#cont_pirata_mapa" style="color:#1D87BF; font-weight: 600">Mapa de Ubicaci&oacute;n</a></li>
			</ul>                       
			<div id="cont_pirata_detalle">
                <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
                <tbody>
                <tr>
                        <td class="da_titulo_gest" colspan="6">DATOS DEL PIRATA</td>
                </tr>
                <tr style="text-align: left;">
                    <td width="15%" class="da_subtitulo">CODIGO</td>
                    <td width="20%"><?php echo strtoupper($arrObjPirata[0]->__get('_codigo'))?></td>
                    <td width="15%" class="da_subtitulo">TIPO ANTENA</td>
                    <td width="20%" class="da_import"><?php echo strtoupper($arrObjPirata[0]->__get('_tipo'))?></td>
                    <td width="15%"class="da_subtitulo">FECHA DETECCI&Oacute;N</td>
                    <td width="15%"><?php echo obtenerFecha($arrObjPirata[0]->__get('_fechaInsert'))?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">ITEM</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_item'))?></td>
                    <td class="da_subtitulo">ZONAL</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_zonal'))?></td>
                    <td class="da_subtitulo">CIUDAD</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_ciudad'))?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">DEPARTAMENTO</td>
                    <td><?php echo $arrObjDepartamento[0]->__get('_nombre')?></td>
                    <td class="da_subtitulo">PROVINCIA</td>
                    <td><?php echo $arrObjProvincia[0]->__get('_nombre')?></td>
                    <td class="da_subtitulo">DISTRITO</td>
                    <td><?php echo $arrObjDistrito[0]->__get('_nombre')?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">FFTT</td>
                    <td><?php echo $arrObjPirata[0]->__get('_fftt')?></td>
                    <td class="da_subtitulo">FILTRO LINEA</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_filtroLinea'))?></td>
                    <td class="da_subtitulo">STATUS</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_estatus'))?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">DIRECCI&Oacute;N ANTENA</td>
                    <td colspan="3"><?php echo strtoupper($arrObjPirata[0]->__get('_direccion'))?></td>
                    <td class="da_subtitulo">TELEFONO REFERENCIAL</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_telefonoReferencial'))?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">DIRECCI&Oacute;N REFERENCIAL</td>
                    <td colspan="3"><?php echo strtoupper($arrObjPirata[0]->__get('_direccionReferencial'))?></td>
                    <td class="da_subtitulo">TELEFONO DETECTADO</td>
                    <td><?php echo strtoupper($arrObjPirata[0]->__get('_telefonoDetectado'))?></td>
                </tr>
                <tr>
                    <td colspan="6" style="height:5px;"></td>
                </tr>
                <tr>
                     <td class="da_titulo_gest" colspan="6">CLIENTES ASOCIADOS AL PIRATA</td>
                </tr>
                <tr>
                    <td colspan="6">                        
                        <div style="width:100%; overflow-y:scroll; height:100px;">
                            <table id="tb_resultado_clientes_asociados" class="tablesorter" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 140%; font-size:10px;">
                            <thead>
                                <tr style="line-height:13px;">
                                    <th class="headTbl" width="2%">#</th>
                                    <th class="headTbl" width="10%">Expediente</th>
                                    <th class="headTbl" width="10%">Fec. expediente</th>
                                    <th class="headTbl" width="25%">Cliente</th>
                                    <th class="headTbl" width="30%">Direccion</th>
                                    <th class="headTbl" width="10%">Telefono</th>
                                    <th class="headTbl" width="13%">Producto</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($arrObjClientesAsociados)) {
                                $itemA = 1;
                                foreach ($arrObjClientesAsociados as $objDunaAsociado) {
                            ?>
                                    <tr>
                                    <td class="celdaX"><?php echo $itemA++; ?></td>
                                    <td class="celdaX"><?php echo $objDunaAsociado->__get('_expediente'); ?></td>
                                    <td class="celdaX"><?php echo $objDunaAsociado->__get('_fechaExpediente'); ?></td>
                                    <td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_titular'); ?></td>
                                    <td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_direccionTelefono'); ?></td>
                                    <td class="celdaX"><?php echo $objDunaAsociado->__get('_telefono'); ?></td>
                                    <td class="celdaX"><?php echo $objDunaAsociado->__get('_speedyContratado'); ?></td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                            </tbody>
                            </table>
                        </div>
                        
                    </td>
                </tr>
                </table>
			</div>
            <div id="cont_pirata_mapa">
                <table id="tbl_detalle_mapa" class="tb_detalle_actu" width="100%">
                <tbody>
                    <tr>
                        <td class="da_titulo_gest" colspan="6">MAPA DE UBICACI&Oacute;N</td>
                    </tr>
                    <tr>
                        <td id="tdMapaPirata">
                        <!--Dibuja el mapa con el pirata-->                                   
                        </td>
                    </tr>
                </table>
			</div>
        </div>
		</td>
	</tr>
	</table>
	<br />
</div>