<?php
if ($totalRegistros>0) {
?>
<thead>
	<tr style="line-height: 13px;">
		<th class="headTbl" width="2%">#</th>
		<th class="headTbl" width="7%">Opciones</th>
		<th class="headTbl" width="5%">Codigo</th>
		<th class="headTbl" width="7%">Estatus</th>
		<th class="headTbl" width="5%">Filtro linea</th>
		<th class="headTbl" width="45%">Direcci&oacute;n pirata</th>
		<th class="headTbl" width="6%">Tipo antena</th>
		<th class="headTbl" width="9%">Fecha Registro</th>
		<th class="headTbl" width="12%">Estado</th>
	</tr>
</thead>
<tbody>
	<?php
    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
    foreach ($arrObjDuna as $objDuna) {
    ?>
	<tr>
		<td class="celdaX"><?php echo $item++; ?></td>
		<td class="celdaX">
        <a href="javascript:void(0)"
			onclick="javascript:detallePirata('<?php echo $objDuna->__get('_idDuna')?>')"
			title="Ver detalle"> <img width="12" height="12" title="Ver detalle"
            src="img/ico_detalle.jpg">
		</a>
		<a href="javascript:cambiarEstadoPirata('285022','Provision','1','ASEG')"
            title="Cambiar estado"> <img width="13" height="13"
		    title="Cambiar estado" src="img/ico_actualizar_estado.png">
		</a>
		<a title="Fotos"
			onclick="fotosPirataMain('<?php echo $objDuna->__get('_idDuna')?>')"
			href="javascript:void(0)"> <?php
            if ($objDuna->__get('_foto1') != '') {
            ?> <img width="14" height="14" title="Fotos" src="img/foto.png">
			<?php
            } else {
            ?> 
			<img width="14" height="14" title="Sin fotos" src="img/foto_disabled.png">
			<?php
            }
            ?>
		</a>
		<?php
        // ESTADO 05 = NO ASOCIADO
        if ($objDuna->__get('_tipo') == 'EMISOR' 
            && $objDuna->__get('_estado') != '05' 
            && $objDuna->__get('_x') != '' 
            && $objDuna->__get('_y') != '' ) {
        ?>
        <a title="Asociar"
			onclick="asociarPirata('<?php echo $objDuna->__get('_idDuna')?>')"
			href="javascript:void(0)"> <img width="12" height="12"
            title="Asociar" src="img/ico_gestionar.jpg">
		</a>
		<?php
        } else {
        ?>
        <img width="12" height="12" title="No puede Asociar" src="img/ico_gestionar_disabled.jpg">
        <?php
        }
        ?>
		</td>
		<td class="celdaX"><?php echo $objDuna->__get('_codigo'); ?></td>
		<td class="celdaX"><?php echo $objDuna->__get('_estatus'); ?></td>
		<td class="celdaX"><?php echo $objDuna->__get('_filtroLinea'); ?></td>
		<td class="celdaX" align="left"><?php echo strtoupper($objDuna->__get('_direccion')); ?>
		</td>
		<td class="celdaX"><?php echo $objDuna->__get('_tipo'); ?></td>
		<td class="celdaX"><?php echo obtenerFecha($objDuna->__get('_fechaInsert')); ?>
		</td>
		<td class="celdaX strPeq"><?php echo $objDuna->__get('_estadoDuna'); ?>
		</td>
	</tr>
	<?php
    }
    ?>
</tbody>
<?php
} else {
?>
<tr>
	<td style="height: 30px">Ningun registro encontrado con los criterios de b&uacute;squeda ingresados</td>
</tr>
<?php
}
?>