<script type="text/javascript">
$(document).ready(function () {
    $('#box_men_adm tr.link').click(function(event) {
        var href = $(this).find("a").attr("url");
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/" + href,
            success: function(datos) {
                $("#content").html(datos);
            }
        });
    });
});
</script>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="95%">
<tr>
    <td width="100%" valign="top">
        
        <table id="tblSearch" border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
        <tr>
            <td class="form_titulo" colspan="4">
                <img border="0" src="img/plus_16.png">&nbsp;Gestion de Componentes
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div style="height: 5px"></div>
            </td>
        </tr>
        </table>
        
        <div style="margin: 10px; text-align:left; border: 1px solid #CCCCCC; background-color: #F2F2F2; height: 380px; width:100%;">
            <div id="box_men_adm" style="width:100%;">
                <?php 
                if ( !empty($_SESSION['USUARIO_CAPAS']) ) {
                    $flagOtrasCapas = 0;
                    foreach ( $_SESSION['USUARIO_CAPAS'] as $objCapa ) {
                        if ( isset($objCapa) ) {
                            if ( $objCapa->__get('_indCapa') == 'S' && $objCapa->__get('_tieneTabla') == 'S' && $objCapa->__get('_url') != '' ) {
                                ?>
                                <div>
                                    <table width="100%" border="0">
                                    <tr class="link">
                                        <td width="80%"><a href="javascript:void(0);" url="<?php echo $objCapa->__get('_url')?>" style=" line-height: 20px; margin-left: 10px; font-weight:bold; font-size:11px;">Mantenimiento de <?php echo ucwords(strtolower($objCapa->__get('_nombre')))?></a></td>
                                        <td width="10%"><a href="javascript:void(0);" url="<?php echo $objCapa->__get('_url')?>" style="float: right;"><img src="img/search_16.png" alt="Mantenimiento de <?php echo ucwords(strtolower($objCapa->__get('_nombre')))?>" title="Mantenimiento de <?php echo ucwords(strtolower($objCapa->__get('_nombre')))?>" border="0"/></a></td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>
                                    </table>
                                </div>
                                <?php
                            }else {
                                $flagOtrasCapas = 1;
                            }
                        }
                    }
                }
                ?> 
            </div>
        </div>
    </td>
</tr>
</table>