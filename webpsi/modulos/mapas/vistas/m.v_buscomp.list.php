<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
$(document).ready(function() {

    $(".clsComp").click(function() {
        var comp_selected = $(this).attr('id');
        var comp_arr = comp_selected.split('_');
        
        var abv_comp = comp_arr[0];
        var pos_comp = comp_arr[1];
        
        window.top.location = "m.buscomp.detalle.php?action=detalle&abv_comp=" + abv_comp + "&pos_comp=" + pos_comp;
    });

});
</script>

</head>
<body>

<?php include '../../m.header.php';?>

<form method="post" action="m.buscomp.vermapa.php">

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="titulo">Resultados de la b&uacute;squeda</td>
</tr>
</table>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="clsSubtitle">X: <input type="text" size="15" name="x" id="x" value="<?php echo $_REQUEST['x']?>" readonly="readonly" /><td>
</tr>
<tr>
    <td class="clsSubtitle">Y: <input type="text" size="15" name="y" id="y" value="<?php echo $_REQUEST['y']?>" readonly="readonly" /><td>
</tr>
</table>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="descPrincipal">Listado de componentes encontrados cercanos al punto x,y seleccionado</td>
</tr>
</table>

<table id="tblPrincipal" class="principal" width="100%">
<?php
if ( !empty($arrDataMarkers) ) {
    
    foreach ($arrDataMarkers as $pkComp => $componentes) {
?>
    <tr>
        <td class="subtitulo"><?php echo $componentes['name']?></td>
    </tr>
<?php
        $x = 1;
        foreach ( $componentes['data'] as $marker ) {

            if ( $marker['tip'] == 'trm' ) {
                if ( $marker['qparlib'] == 0 ) {
                    $estCaja = 3;
                    $colorTrm = '#DF4A36';
                } elseif ( $marker['qparlib'] > 0 && $marker['qparlib'] < 3 ) {
                    $estCaja = 2;
                    $colorTrm = '#FFF85D';
                } elseif ( $marker['qparlib'] > 2 ) {
                    $estCaja = 1;
                    $colorTrm = '#79B094';
                }
            } else {
                $colorTrm = '#79B094';
            }
            ?>
                <tr class="lnk">
                    <td class="list">
                        <div class="clsNum"><?php echo $x?></div>
                        <span class="clsComp" id="<?php echo $marker['tip']?>_<?php echo $marker['pos']?>"  style="cursor: pointer; font-size: 18px; border: 1px solid #000000; padding: 4px 8px; background: <?php echo $marker['color']?>"><?php echo $marker['title']?></span>
                        <span style="font-size: 12px;">(<?echo number_format(($marker['distance'] * 1000), 2)?> m.)</span>
                        <?php echo $marker['lnk']?>
                    </td>
                </tr>
            <?php
            $x++;
        }
    }
}
?>
</table>

</body>
</html>