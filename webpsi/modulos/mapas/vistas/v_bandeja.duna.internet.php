<style type="text/css">
.headTbl {
	background-image: url("img/bg_head_js.gif");
	background-position: center top;
	background-repeat: repeat-x;
	color: #000000;
	height: 22px;
}

table.tablesorter tbody tr:hover td {
	background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
	background-color: #FFC444;
}

table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}

.tb_detalle_actu td {
	background-color: #FFFFFF;
}

table.tb_detalle_actu .da_titulo {
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}

table.tb_detalle_actu .da_titulo_gest {
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}

table.tb_detalle_actu .da_subtitulo {
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}

table.tb_detalle_actu .da_import {
	font-size: 11px;
	color: red;
	font-weight: bold;
}

table.tb_detalle_actu .da_agenda {
	font-size: 11px;
	font-weight: bold;
}

table a.link_movs {
	color: #FFFFFF;
	font-weight: normal;
}

.boton {
	background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
	border: 0 none;
	color: #FFFFFF;
	cursor: pointer;
	font-size: 11px;
	font-weight: bold;
	height: 30px;
	width: 80px;
}

#pirata_image {
	float: left;
	width: 400px;
	display: block;
}

#pirata_image img {
	border: 1px solid #CACED1;
}

#pirata_thumbs { /*overflow: hidden;*/
	float: left;
	width: 140px;
}

#pirata_thumbs img {
	float: left;
	width: 400px;
	border: 1px solid #CACED1;
	cursor: pointer;
	/*height: 75px;*/
	margin: 0 5px 5px 10px;
	padding: 2px;
	width: 70px;
}

#pirata_thumbs p {
	margin: 5px 0 0 0;
}

#pirata_thumbs span {
	float: left;
	width: 400px;
	border: 1px solid #CACED1;
	margin: 30px 10px 12px 0;
	padding: 2px;
	width: 77px;
}

#pirata_icos { /*overflow: hidden;*/
	width: 500px;
	height: 140px;
}

#pirata_icos p {
	margin: 5px 0 0 0;
}

#pirata_icos span {
	float: left;
	width: 400px;
}

#pirata_icos span {
	border: 1px solid #CACED1;
	margin: 30px 10px 12px 0;
	padding: 2px;
	width: 77px;
}

.strPeq {
	font-size: 8px;
}

.hd_orden_asc {
	background-image: url("img/desc.gif");
	background-position: right center;
	background-repeat: no-repeat;
	padding-right: 16px;
}

.hd_orden_desc {
	background-image: url("img/asc.gif");
	background-position: right center;
	background-repeat: no-repeat;
	padding-right: 16px;
}
</style>
<script type="text/javascript" src="js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="modulos/maps/js/functions.js"></script>
<script type="text/javascript">
    var pr = null;
    $(document).ready(function(){
    	$("#cancelar_ajax").click(function(){
        $("#cont_num_resultados_head").html('0');
        $("#cont_num_resultados_foot").html('0');
        $("#tl_registros").attr("value", "0");
        $("#paginacion").empty();
        pr.abort();
    });

    $("#btn_buscar").click(function() {
        D.BuscarDunaInternet();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            D.BuscarDunaInternet();
        }
    });
    
    
    limpiarFoto = function(id) {
        $("#" + id).attr('value','');
    }

    descargaDunaInternet = function() {
        alert("pronto el reporte");
        return false;       
    }

    cerrarVentanaAsociarForm = function() {
        $("#content_pirata_asociar_form").dialog('close');
    }
    
    cerrarVentanaAsociar = function() {
        $("#childModal").dialog('close');
    }

    registrarPirata = function() {
        D.RegistrarPirata();
    }
    
    eliminarClienteAsociado = function(idduna_asociado, idduna) {
        D.EliminarClienteAsociado(idduna_asociado, idduna);
    }
    
    ordenaClientesCercanos = function(campo, orden) {
        var cant_terminales  = $("#cant_terminales").val();
        var tipo_listado    = $("#tipo_listado:checked").val();
        var idcomponente    = $("#idcomponente").val();
        D.ListadoClientesCercanos(tipo_listado, idcomponente, cant_terminales, orden, campo);
    }
    
    cantidadTerminales = function() {
        var cant_terminales  = $("#cant_terminales").val();
        var tipo_listado    = $("#tipo_listado:checked").val();
        var idcomponente    = $("#idcomponente").val();
        D.ListadoClientesCercanos(tipo_listado, idcomponente, cant_terminales,'','');
    }
    
    listadoClientesCercanos = function(tipo_listado, idcomponente) {
        var num_terminales  = $("#cant_terminales").val();
        D.ListadoClientesCercanos(tipo_listado, idcomponente, num_terminales,'','');
    }
    
    listadoClientesAsociados = function(idcomponente) {
        D.ListadoClientesAsociados(idcomponente);
    }
    
    detallePirata = function(idcomponente) {
        D.DetallePirata(idcomponente);
    }
    
    asociarPirata = function(idcomponente) {
        D.AsociarPirata(idcomponente);
    }

    fotosPirataMain = function(idcomponente) {
        D.FotosPirataMain(idcomponente);
    }
    
    fotosPirata = function(idcomponente) {
        D.FotosPirata(idcomponente);
    }

    eliminarFotoPirata = function(idcomponente, campo) {
        D.EliminarFotoPirata(idcomponente, campo);
    }

    agregarFotoPirata = function(idcomponente) {
        D.AgregarFotoPirata(idcomponente);
    }

    adjuntarFotosPirata = function(idcomponente) {
        D.AdjuntarFotosPirata(idcomponente);
    }

    asociarPirataAbonado = function(idcomponente, tipored, ddn_telefono) {
        D.AsociarPirataAbonado(idcomponente, tipored, ddn_telefono);
    }
    
    NoasociarPirataAbonado = function() {
        D.NoAsociarPirataAbonado();
    }

    cerrarAsociarForm = function() {
        D.CerrarAsociarForm();
    }
    
    buscarDunaInternet = function() {
        D.BuscarDunaInternet();
    }

    var D = {
        BuscarDunaInternet: function() {
            var tipo            = $("#tipo").val();
            var estado          = $("#estado").val();
            var foto            = $("#foto").val();
            var campo_filtro    = $("#campo_filtro").val();
            var campo_valor     = $.trim($("#campo_valor").val());
            
            var departamento    = $("#departamento").val();
            var provincia       = $("#provincia").val();
            var distrito        = $("#distrito").val();

            var page = 1;
            var tl = $("#tl_registros").val();
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                tipo        : tipo, 
                estado      : estado, 
                foto        : foto, 
                campo_filtro: campo_filtro, 
                campo_valor : campo_valor,
                departamento: departamento,
                provincia   : provincia,
                distrito    : distrito
            };

            loader("start");
            pr = $.post("modulos/maps/bandeja.duna.internet.php?cmd=filtroBusqueda", data_content, function(data){
                $("#tb_resultado").empty();
                $("#tb_resultado").append(data);
                loader("end");
            });

        },
        
        ListadoClientesCercanos: function(tipo_listado, idcomponente, num_terminales, orden, campo) {            
            $("#loading_clientes").show();
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=listadoClientesCercanos", {
                tipo_listado    : tipo_listado,
                idcomponente    : idcomponente,
                num_terminales  : num_terminales,
                orden           : orden,
                campo           : campo
            },
            function(data){
                $("#tb_resultado_clientes").empty();
                $("#tb_resultado_clientes").append(data);
                $("#loading_clientes").hide();
            });

        },
        
        ListadoClientesAsociados: function(idcomponente) {            
            $("#loading_clientes").show();
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=listadoClientesAsociados", {
                idcomponente    : idcomponente
            },
            function(data){
                $("#tb_resultado_clientes_asociados").empty();
                $("#tb_resultado_clientes_asociados").append(data);
                $("#loading_clientes").hide();
            });
        },
        
        DetallePirata: function(idcomponente) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.duna.internet.php?cmd=detallePirata", {
                idcomponente: idcomponente
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'85%', hide: 'slide', title: 'DETALLE DEL PIRATA', position:'top'});

        },
		
        AsociarPirata: function(idcomponente) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.duna.internet.php?cmd=asociarPirata", {
                idcomponente: idcomponente
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'85%', hide: 'slide', closeOnEscape: false, title: 'ASOCIAR PIRATA', position:'top'});

        },
        
        AsociarPirataAbonadoBAK: function() {
            var chk_val = contar_check_temas(".lista_chk");
            var idcomponente = $("#idcomponente").attr('value');

            var clientes_arr = [];

            for( i in chk_val) {
                var cliente = chk_val[i].split("|");
                clientes_arr.push(cliente[0] + "|" + cliente[1]);
            }

            if (chk_val.length > 0) {    
                loader('start');
                $("#content_pirata_asociar").hide();
                $.post("modulos/maps/bandeja.duna.internet.php?cmd=asociarPirataAbonado", {
                    idcomponente    : idcomponente,
                    clientes 	: clientes_arr
                },function(data){            
                    loader('end');
                    $("#content_pirata_asociar").hide();
                    $("#content_pirata_asociar_form").html(data);
                });

            } else{
                alert("Ningun cliente seleccionado para asociar al pirata");
            }
            
        },
        
        AsociarPirataAbonado: function(idcomponente, tipored, ddn_telefono) {

            loader('start');
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=asociarPirataAbonado", {
                idcomponente    : idcomponente,
                tipored         : tipored,
                ddn_telefono    : ddn_telefono
            },function(data){    
                $("#content_pirata_asociar_form").html('');
                $("#content_pirata_asociar_form").html(data);
                loader('end');
            });

            $("#content_pirata_asociar_form").dialog({modal:true, width:'82%', hide: 'slide', closeOnEscape: false, title: 'ASOCIAR CLIENTE AL PIRATA', position:'top'});

        },
        
        NoAsociarPirataAbonado: function() {

            var chk_val = contar_check_temas(".lista_chk");
            var idcomponente = $("#idcomponente").attr('value');

            var clientes_arr = [];

            for( i in chk_val) {
                var cliente = chk_val[i].split("|");
                clientes_arr.push(cliente[0] + "|" + cliente[1]);
            }

            /*if (chk_val.length > 0) {  
                alert("No debe seleccionar ningun cliente.");
                return false;
            }*/

            if( confirm("Esta seguro de marcar como \"NO ASOCIADO\" a este pirata?") ) {

                $("#msg").show();
                $("#NoasociarPirataAbonado").attr('disabled', 'true');
                $("#asociarPirataAbonado").attr('disabled', 'true');
                $("#adjuntarFotosPirata").attr('disabled', 'true');
                $.ajax({
                    type:     "POST",
                    dataType: "json",
                    url:      "modulos/maps/bandeja.duna.internet.php?cmd=noAsociarPirataAbonado",
                    data:     {idcomponente: idcomponente},
                    success:  function(datos) {
                        if( !datos['success'] ) {
                            $("#msg").hide();
                            alert(datos['msg']);
                        }else {
                            $("#msg").html('<img src="img/accept.png"> <b>Datos registrados.</b>');
                            D.BuscarDunaInternet();
                        }
                        setTimeout("cerrarVentanaAsociar()", 1500);
                    }
                });
            }            
        },
		
        FotosPirataMain: function(idcomponente) {
                loader('start');
                $("#childModalPirata").html('Cargando...');
                $("#childModalPirata").css("background-color","#FFFFFF");
                $.post("modulos/maps/bandeja.duna.internet.php?cmd=fotosPirataMain", {
                        idcomponente: idcomponente
                },
                function(data){
                        loader('end');
                        $("#childModalPirata").html(data);
                });

                $("#childModalPirata").dialog({modal:true, width:'580px', hide: 'slide', title: 'FOTOS DEL PIRATA', position:'top'});

        },
        
        FotosPirata: function(idcomponente) {
            $("#divFotos").html('Cargando fotos...');
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=fotosPirata", {
                idcomponente: idcomponente
            },
            function(data){
                $("#divFotos").html(data);
            });
        },
		
        AdjuntarFotosPirata: function(idcomponente) {
            loader('start');
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=adjuntarFotosPirata", {
                    idcomponente: idcomponente
            },
            function(data){
                loader('end');
                $("#divFotosAdjuntar").html(data);
            });

        },
		
        EliminarFotoPirata: function(idcomponente, campo) {
            if( confirm("Esta seguro de eliminar esta foto del pirata?") ) {
                loader('start');
                $.post("modulos/maps/bandeja.duna.internet.php?cmd=eliminarFotoPirata", {
                    idcomponente: idcomponente, 
                    campo   : campo
                },
                function(data){
                    var flag_ok = data.split("|");

                    if( $.trim(flag_ok[0]) == 1 ) {
                        $("#pirata_icos").html(flag_ok[1]);
                        
                        D.BuscarDunaInternet();
                    }else {
                        alert("Error al eliminar la foto. Vuelva a intentarlo");
                    }
                    loader('end');
                });
            }
        },
		
        AgregarFotoPirata: function(idcomponente) {
            var foto = $("#file1").attr('value');

            if( $.trim(foto) == '' ){
                alert("Debe adjuntar una foto antes de asociarlo a un pirata");
                return false;
            }

            loader('start');
            $("#msg").show();
            $.post("modulos/maps/bandeja.duna.internet.php?cmd=agregarFotoPirata", {
                idcomponente    : idcomponente, 
                foto		: foto
            },
            function(data){
                loader('end');
                $("#file1").attr('value','');

                var flag_ok = data.split("|");

                if( $.trim(flag_ok[0]) == '1' ) {
                    $("#msg").hide();
                    $("#divUploadImagen").hide();
                    $("#pirata_icos").html(flag_ok[1]);
                    
                    D.BuscarDunaInternet();
                }else if( $.trim(flag_ok[0]) == '2' ) {
                    alert("Solo puede subir 4 fotos como maximo");
                    $("#msg").hide();
                }else {
                    alert("Error al agregar la foto. Vuelva a intentarlo");
                    $("#msg").hide();
                    $("#divUploadImagen").hide();
                }
            });

        },
		
        RegistrarPirata: function() {
            if( $.trim($("#nro_expediente").attr('value')) == '' ) {
                alert("Ingrese el numero de expediente");
                return false;
            }

            var query = $("input.cli").serializeArray();

            if( confirm("Esta seguro de asociar los clientes seleccionados al pirata?") ) {
                $("#msg").show();
                $("#btn_guardar").attr('disabled', 'true');
                $.ajax({
                    type:     "POST",
                    dataType: "json",
                    url:      "modulos/maps/bandeja.duna.internet.php?cmd=registrarPirata",
                    data:     query,
                    success:  function(datos) {
                        alert("registro grabadddooooo");
                        return false;
                        if( !datos['success'] ) {
                            alert(datos['msg']);
                        }else {
                            $("#msg").html('<img src="img/accept.png"> <b>Datos registrados.</b>');
                            D.BuscarDunaInternet();
                        }
                        setTimeout("cerrarVentana()", 2000);
                    }
                });
            }
        },
        
        EliminarClienteAsociado: function(idduna_asociado, idduna) {

            if( confirm("Esta seguro de eliminar este cliente asociado al pirata?") ) {
                loader('start');
                $.post("modulos/maps/bandeja.duna.internet.php?cmd=eliminarClienteAsociado", {
                    idduna_asociado : idduna_asociado,
                    idduna          : idduna
                },
                function(data){
                    if( data['flag'] ) {
                        alert(data['mensaje']);
                    
                        //para deshabilitar el boton de No Asociado
                        if(data['flag_asociados']) {
                            $("#NoasociarPirataAbonado").attr('disabled', 'disabled');
                        }else {
                            $("#NoasociarPirataAbonado").attr('disabled', '');
                        }
                    
                        //actualiza la lista de clientes cercanos
                        listadoClientesCercanos('trm', idduna);

                        //actualiza listado de clientes asociados
                        listadoClientesAsociados(idduna);
                        buscarDunaInternet();
                        loader('end');
                    }else {
                        alert(data['mensaje']);
                        loader('end');
                    }
                    
                },'json');
            }
        },
		
        CerrarAsociarForm: function() {
                $("#content_pirata_asociar").show();
                $("#content_pirata_asociar_form").html('');
        }
    };
});
</script>
<!-- Cuerpo principal -->
<div id="content">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td class="form_titulo" width="30%">
					<img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Bandeja de Duna-Internet
				</td>
				<td width="70%" align="left">
					<div style="display:block;">
						Tipo
						<select name="tipo" id="tipo">
							<option value="">Todos</option>
							<option value="Emisor">Emisor</option>
							<option value="Receptor">Receptor</option>
						</select>
                        &nbsp;
                        Estado
                        <select name="estado" id="estado">
                            <option value="">Todos</option>
                            <?php
                            foreach ( $arrObjDunaEstado as $objDunaEstado ) {
                            ?>
							<option value="<?php echo $objDunaEstado->__get('_idEstado') ?>"><?php echo $objDunaEstado->__get('_nombre') ?></option>
                            <?php
                            }
                            ?>
						</select>
						&nbsp;
						Foto
						<select name="foto" id="foto">
							<option value="">Todos</option>
							<option value="S">Con foto</option>
							<option value="N">Sin foto</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                        <div style="border-bottom:1px solid #999999;margin-bottom:3px;padding-bottom:3px;">
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
                            <td width="10%" align="left">Buscar por:</td>
                            <td width="90%" align="left">
                                <select name="campo_filtro" id="campo_filtro">
                                    <option value="f_direccion" selected="selected">DIRECCION ANTENA</option>
                                </select>
                                <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                <input type="button" id="btn_buscar" name="buscar" value="Buscar"/>
                            </td>
						</tr>
                        </table>
                        </div>
                        <div>
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                                <td width="10%" align="left">Filtrar por:</td>
                                <td width="90%" align="left">
                                    Departamento
                                    <select name="departamento" id="departamento" onchange="provinciasUbigeo();">
                                        <option value="">Todos</option>
                                        <?php
                                        foreach ($arrObjDepartamento as $objDepartamento) {
                                        ?>
                                        <option value="<?php echo $objDepartamento->__get('_codDpto')?>"><?php echo $objDepartamento->__get('_nombre')?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    &nbsp;
                                    Provincia
                                    <select name="provincia" id="provincia" onchange="distritosUbigeo();">
                                        <option value="">Seleccione</option>
                                    </select>
                                    &nbsp;
                                    Distrito
                                    <select name="distrito" id="distrito">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
						    </tr>
						    </table>
                        </div>
					</div>
				</td>
			</tr>
			</table>
			<?php if ($totalRegistros>0):?>
			<table id="tb_sup_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
			<tr>
			<td style="width:55%; height: 30px; text-align: left;">
				Han sido listado(s) <b><span id="cont_num_resultados_head">
				<?php echo $totalRegistros ?></span></b> registro(s) &nbsp;|&nbsp; 
				<a onclick="descargaDunaInternet();" href="javascript:void(0)">Descargar</a>
				<a onclick="descargaDunaInternet();" href="javascript:void(0)">
				<img src="img/ico_excel.png" title="Descargar excel"></a>
			</td>
			<td style="width:45%;">
				<div  style=" width:360px; position: relative;">
					<div id="paginacion"></div>
				</div>
			</td>
			</tr>
			</table>
			<?php endif;?>			
			<input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
			<input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros; ?>"/>
			<!--<div style="width: 100%;  min-height: 300px;">-->
			<div style="width: 100%;">			
				<table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;">
				<thead>
					<tr style="line-height:13px;">
						<th class="headTbl" width="2%">#</th>
						<th class="headTbl" width="7%">Opciones</th>
                        <th class="headTbl" width="5%">Codigo</th>
                        <th class="headTbl" width="7%">Estatus</th>
                        <th class="headTbl" width="5%">Filtro linea</th>
                        <th class="headTbl" width="45%">Direcci&oacute;n pirata</th>
						<th class="headTbl" width="6%">Tipo antena</th>
						<th class="headTbl" width="9%">Fecha Registro</th>
						<th class="headTbl" width="12%">Estado</th>
					</tr>
				</thead>
				<tbody>
		            <?php
                    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                    foreach ($arrObjDuna as $objDuna) {
                    ?>
					<tr>
					<td class="celdaX"><?php echo $item++; ?></td>
					<td class="celdaX">
						<a href="javascript:void(0)" onclick="javascript:detallePirata('<?php echo $objDuna->__get('_idDuna') ?>')" title="Ver detalle">
							<img width="12" height="12" title="Ver detalle" src="img/ico_detalle.jpg">
						</a>
                        <a href="javascript:cambiarEstadoPirata('285022','Provision','1','ASEG')" title="Cambiar estado">
							<img width="13" height="13" title="Cambiar estado" src="img/ico_actualizar_estado.png">
						</a>						
						<a title="Fotos" onclick="fotosPirataMain('<?php echo $objDuna->__get('_idDuna')?>')" href="javascript:void(0)">
                        <?php
                        if ($objDuna->__get('_foto1') != '') {
                        ?>
                        <img width="14" height="14" title="Fotos" src="img/foto.png">
                        <?php
                        } else {
                        ?>
                        <img width="14" height="14" title="Sin fotos" src="img/foto_disabled.png">
                        <?php
                        }
                        ?>
						</a>
						<?php
                        // ESTADO 05 = NO ASOCIADO
                        if ($objDuna->__get('_tipo') == 'EMISOR' 
                            && $objDuna->__get('_estado') != '05' 
                            && $objDuna->__get('_x') != '' 
                            && $objDuna->__get('_y') != '') {
                        ?>
						<a title="Asociar" onclick="asociarPirata('<?php echo $objDuna->__get('_idDuna')?>')" href="javascript:void(0)">
							<img width="12" height="12" title="Asociar" src="img/ico_gestionar.jpg">
						</a>
						<?php
                        } else {
                        ?>
				        <img width="12" height="12" title="No puede Asociar" src="img/ico_gestionar_disabled.jpg">
						<?php
                        }
                        ?>
					</td>
                    <td class="celdaX"><?php echo $objDuna->__get('_codigo'); ?></td>
                    <td class="celdaX"><?php echo $objDuna->__get('_estatus'); ?></td>
                    <td class="celdaX"><?php echo $objDuna->__get('_filtroLinea'); ?></td>
					<td class="celdaX" align="left"><?php echo strtoupper($objDuna->__get('_direccion')); ?></td>
					<td class="celdaX"><?php echo $objDuna->__get('_tipo'); ?></td>
					<td class="celdaX"><?php echo obtenerFecha($objDuna->__get('_fechaInsert')); ?></td>
					<td class="celdaX strPeq"><?php echo $objDuna->__get('_estadoDuna'); ?></td>
					</tr>
                    <?php
                    }
                    ?>
				</tbody>
				</table>
			</div>
			
			<br />
			<?php if($totalRegistros>0): ?>
			<table id="tb_pie_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
			<tr>
			   <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot">
			   <?php echo $totalRegistros; ?></span></b> registro(s) <br/><br/></td>
			</tr>
			</table>
			<?php endif;?>

		</td>
	</tr>
	</table>
	<br /><br />
</div>
<div id="footer"></div>
<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
<div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->
<input type="hidden" value="1" id="emisor" name="emisor" />
<div id="parentModalPirata" style="display: none;">
<div id="childModalPirata" style="padding: 10px; background: #fff;"></div>
</div>
