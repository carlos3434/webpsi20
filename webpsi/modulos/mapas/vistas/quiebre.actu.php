<?php
require_once "../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

if($_SESSION['tmp_map']['_MAP'] and !empty($_SESSION['tmp_map']['marks'])){
	$markers = "";
	$_coords = $_SESSION['tmp_map']['coords'];
	//$_GET['getClosestTo'] : actu
	if(isset($_SESSION['tmp_map']['coords'])){

		$sql = "SELECT 	TRIM(a.actu) AS actu,
				TRIM(a.x) AS x1,
				TRIM(a.y) AS y1, 
				'{$_coords['y']}' AS x2, 
				'{$_coords['x']}' AS y2,
				a.tipo, a.negocio, a.fono, a.f_reg,
				SQRT(POW((TRIM(a.x)-({$_coords['y']})),2) 
				+ POW((TRIM(a.y)-({$_coords['x']})),2))*100000 AS distancia
				FROM {$_SESSION['tmp_map']['table']} a
				WHERE TRIM(a.x) <> '' AND TRIM(a.y)<>''";
	}else{

		$_SESSION['ipsi']['tmp']['actu'] = mysql_real_escape_string($_GET['getClosestTo']);
		if(isset($_GET['org'])){
			$_SESSION['ipsi']['tmp']['actu_geo'] = mysql_real_escape_string($_GET['getClosestTo']);
		}

		$sql = "SELECT 	trim(a.actu) AS actu,
				trim(b.actu) AS actu1,
				trim(a.x) AS x1,
				trim(a.y) AS y1, 
				trim(b.x) AS x2, 
				trim(b.y) AS y2,
				a.tipo, a.negocio, a.fono, a.f_reg, 
				SQRT(POW((a.x-b.x),2) + POW((a.y-b.y),2))*100000 AS distancia
				FROM  gaudi_pruebas.pendientes_actual_tmp a,(SELECT * FROM  gaudi_pruebas.pendientes_actual_tmp 
				WHERE ACTU='{$_SESSION['ipsi']['tmp']['actu']}') b
				WHERE a.zonal=b.zonal AND a.x <>0 AND a.actu<>b.actu";
	}

	/**
	 * Restriccion de datos de sesion
	 */

	$n = "A";
	$alist = "";
	$zoom = 16;
	$latlan = "";
	$setMark = "";
	$contents = "";
	$infowin = "";
	$winlistener = "";
	$iconimage = "";
	$endpoint = array();

	foreach($_SESSION['tmp_map']['marks'] as $mkey=>$row){

		$markers .= "&markers=color:blue|label:$n|{$row['y1']},{$row['x1']}";
		$x2 = $row['x2'];
		$y2 = $row['y2'];
		$distanciar = number_format($row['distancia'], 0);
		$distancia = number_format($row['distancia'] * 1.4, 0);
		$alist .= "<tr><td style=\"font-family: verdana; font-size: 12px;\">$n. <a href=\"javascript:void(0)\" onclick=\"goToPopActu('{$row['actu']}')\">{$row['actu']}</a></td><td style=\"text-align: center\">$distanciar</td><td style=\"text-align: center\">$distancia</td><td>{$row['tipo']}</td><td style=\"text-align: center\">{$row['negocio']}</td></tr>";

		$endpoint[$n] = trim($row['y1']) . "," . trim($row['x1']);

		$iconimage .= "var image$n = '../../../img/map-icons-collection-2.0/numeric/black$n.png';";

		$contents .= "var contentString$n = '<div id=\"content\" style=\"font-family: verdana; font-size: 12px;\"><a href=\"javascript:void(0)\" onclick=\"goToPopActu(\'{$row['actu']}\')\" style=\"color: #000000\">{$row['actu']}</a> <br />Distancia: $distancia <br />Tipo: {$row['tipo']} <br />Negocio: {$row['negocio']} <br /> Telefono: {$row['fono']} <br /> F. Registro: {$row['f_reg']} </div>';";

		$infowin .= "var infowindow$n = new google.maps.InfoWindow({
					    content: contentString$n
					});";

		$latlan .= "var latlng$n = new google.maps.LatLng('{$row['y1']}', '{$row['x1']}');";

		$setMark .= "var marker$n = new google.maps.Marker({
				        position: latlng$n, 
				        map: map, 
				        icon: image$n,
				        title:\"$n\"
				    });";

		$winlistener .= "google.maps.event.addListener(marker$n, 'click', function() {
						  infowindow$n.open(map,marker$n);
						});";

		$n++;

		if($distancia>580){
			$zoom = 15;
		}
		if($distancia>1200){
			$zoom = 14;
		}

	}
	$xmark = "&markers=color:red|label:X|{$y2},{$x2}";
}
?>

<html>
<head>

<script type="text/javascript">
	function goToPopActu(actu){
		$.post("popup.ajax.php",{setVars:"ok",actu:actu},function(data){})
		window.location = 'detActu001.php?ispop=ok';
	}
	</script>
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript">
	var map;
	var stepDisplay;
	var markerArray = [];
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	
	  function initialize() {

		directionsDisplay = new google.maps.DirectionsRenderer();
		
	    var latlng = new google.maps.LatLng('<?php echo $_coords['x'];?>', '<?php echo $_coords['y'];?>');

		<?php echo $latlan;?>
	    
	    var myOptions = {
	      zoom: 16,
	      center: latlng,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	    directionsDisplay.setMap(map);
	    directionsDisplay.setPanel(document.getElementById("directionsPanel"));

	    google.maps.event.addListener(map, 'click', function(event) {
			/*
		    var conf = confirm("Se buscaran actuaciones cercanas al punto seleccionado. Desea continuar?")
		    if(conf){
		    	var myCoords = event.latLng;
			    //alert("En construccion" + myCoords);
			    window.location = '?getClosestTo=' + myCoords + '&mycoords=ok';
		    }
		    */
	      });

	    <?php echo $iconimage;?>
	    
	    <?php echo $contents;?>

	    <?php echo $infowin;?>

	    var marker = new google.maps.Marker({
	        position: latlng, 
	        map: map, 
	        title:"Hello World!"
	    });

	    <?php echo $setMark;?>
	    	    
	    <?php echo $winlistener;?>
	    
	  }

	  function calcRoute() {
		  	var start = document.getElementById("start").value;
			var end = document.getElementById("end").value;
			var request = {
				origin:start,
				destination:end,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};
			directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					showSteps(response);
				}
			});
		}	
	</script>

<style type="text/css">
body {
	font-family: verdana;
	font-size: 10px;
}

tr td {
	font-family: verdana;
	font-size: 12px;
}

th {
	font-family: verdana;
	font-size: 12px;
}
</style>

</head>
<body onload="initialize()">

<table>
	<tr>
		<td>
		<div id="map_canvas" style="width: 600; height: 600" onclick=""></div>
		</td>
		<td>
		<table>
			<tr>
				<td colspan="5">
				<table>
					<tr>
						<td rowspan="2">Actuaci&oacute;n original: <a
							href="?getClosestTo=<?php echo $_SESSION['ipsi']['tmp']['actu_geo'];?>&org=ok"><?php echo $_SESSION['ipsi']['tmp']['actu_geo'];?></a>
						</td>
						<th colspan="2">Trazar ruta</th>
					</tr>
					<tr>
						<td><b>Origen:</b> <select id="start" onchange="calcRoute();">
							<option value="<?php echo $y2 . "," . $x2?>">Pto. referencia</option>
							<?php
							foreach($endpoint as $key=>$val){
								echo "<option value=\"$val\">$key</option>";
							}
							?>
						</select></td>
						<td><b>Destino:</b> <select id="end" onchange="calcRoute();">
							<option value="<?php echo $y2 . "," . $x2?>">Pto. referencia</option>
							<?php
							foreach($endpoint as $key=>$val){
								echo "<option value=\"$val\">$key</option>";
							}
							?>
						</select></td>
					</tr>
					<tr>
						<td colspan="3">
						<div id="directionsPanel" style="float: right; width: 100%;"></div>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="5">
				<hr />
				</td>
			</tr>
			<tr>
				<th>Actuaci&oacute;n</th>
				<th>Dist. real</th>
				<th>Dist. (aprox.)</th>
				<th>Tipo</th>
				<th>Negocio</th>
			</tr>
			<?php echo $alist;?>
		</table>
		</td>
	</tr>
</table>

</body>
</html>
