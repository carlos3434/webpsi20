<?php
if ($totalRegistros>0) {
          
    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                    
    foreach ($arrObjEdificio as $objEdificio) {
        ?>

        <tr>
            <td class="celdaX"><?php echo $item++; ?></td>
            <td class="celdaX">
                <a title="Ver detalle" onclick="javascript:detalleEdificio('<?php echo $objEdificio->__get('_idEdificio')?>')" href="javascript:void(0)">
                    <img width="12" height="12" src="img/ico_detalle.jpg" title="Ver detalle">
                </a>
                <a onclick="editarEdificio('<?php echo $objEdificio->__get('_idEdificio')?>')" href="javascript:void(0)">
                    <img width="13" height="13" title="Editar" border="0" src="img/pencil.png">
                </a>
                <a href="javascript:void(0)" onclick="fotosEdificio('<?php echo $objEdificio->__get('_idEdificio')?>')" title="Fotos">
                    <img width="14" height="14" border="0" src="img/foto<?php if (trim($objEdificio->__get('_foto1')) == '') echo '_disabled';?>.png" title="Fotos">
                </a>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_item'); ?></td>
            <td class="celdaX"><?php if ($objEdificio->__get('_idTipoProyecto') != '30') echo $objEdificio->__get('_tipoProyecto'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_nombreProyecto'); ?></td>
            <td class="celdaX">
                <?php
                if ( $objEdificio->__get('_fechaRegistroProyecto') != '0000-00-00' && $objEdificio->__get('_fechaRegistroProyecto') != '' ) {
                    echo date('d/m/Y', strtotime($objEdificio->__get('_fechaRegistroProyecto')));
                } else {
                    echo '';
                }
                ?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_zonal'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_ura'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_sector'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_mzaTdp'); ?></td>
            <td class="celdaX" align="left">
                <?php echo strtoupper($objEdificio->__get('_tipoVia') . ' ' . substr($objEdificio->__get('_direccionObra'), 0, 50)); ?><?php if (strlen($objEdificio->__get('_direccionObra')) > 50) echo '...'?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_numero'); ?></td>
            <td class="celdaX">
                <?php if ($objEdificio->__get('_idTipoCchh') != '24') echo $objEdificio->__get('_tipoCchh'); ?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_cchh'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_gestionObra'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_desEstado'); ?></td>
        </tr>
 <?php
    }

} else {
    ?>
    <tr>
        <td style="height:30px" colspan="16">Ningun registro encontrado con los criterios de b&uacute;squeda ingresados</td>
    </tr>
<?php
}