<?php
require_once "../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;


if( isset($_POST['search']) ) {

	//echo '<pre>';
	//print_r($_POST);
	//echo '</pre>';

	$obj_terminales = new data_ffttTerminales();
	$obj_capas 		= new data_ffttCapas();

	$data_markers 	  = array();
	$data_markers_img = array();
	
	//marker seleccionado -> icon por default
	$data_markers_img['pto'] = 'markergreen.png';
	$data_markers[] = array(
		'title' 	=> 'pto',
		'y'	 		=> $_POST['y'],
		'x'	 		=> $_POST['x'],
		'i'	 		=> 1,
		'detalle'	=> 'Punto seleccionado',
		'tip'		=> 'pto'
	);
	
	$i = 2;
	if( isset($_POST['comp']['trm']) ) {
	
		$data_markers_img['trm'] = 'mobilephonetower.png';
		//echo 'aaa';
		//carga datos de terminales
		$array_terminales = $obj_terminales->get_terminales_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		//echo 'bbb';
		//echo '<pre>';
		//print_r($array_terminales);
		//echo '</pre>';exit;
		foreach ($array_terminales as $terminales) {
	
			if( $terminales['y'] != '' && $terminales['x'] != '' ) {
			
				$detalle = '<b>Zonal:</b> ' . $terminales['zonal'] . '<br />';
				$detalle .= '<b>Mdf:</b> ' . $terminales['mdf'] . '<br />';
				if( $terminales['tipo_red'] == 'D' ) {
					$detalle .= '<b>Cable:</b> ' . $terminales['cable'] . '<br />';
				}elseif( $terminales['tipo_red'] == 'F' ) {
					$detalle .= '<b>Armario:</b> ' . $terminales['armario'] . '<br />';
				}
				$detalle .= '<b>Caja:</b> ' . $terminales['caja'] . '<br />';
				$detalle .= '<b>Capacidad:</b> ' . $terminales['qcapcaja'] . '<br />';
				$detalle .= '<b>Pares libres:</b> ' . $terminales['qparlib'] . '<br />';
				$detalle .= '<b>Pares reserv.:</b> ' . $terminales['qparres'] . '<br />';
				$detalle .= '<b>Pares distrib.:</b> ' . $terminales['qdistrib'] . '<br /><br />';
				$detalle .= '<b>X,Y:</b> ' . $terminales['x'] . ',' . $terminales['y'] . '<br />';
				$detalle .= '<a title="Ver clientes asignados a este terminal: ' . $terminales['caja'] . '" href="javascript:clientesTerminal(\''. $terminales['zonal1'] .'\',\''. $terminales['mdf'] .'\',\''. $terminales['cable'] .'\',\''. $terminales['armario'] .'\',\''. $terminales['caja'] .'\',\''. $terminales['tipo_red'] .'\');">Ver mas</a>';
			
				//$data_markers[] = array($terminales['mtgespktrm'], $terminales['y'], $terminales['x'], $i, 'Terminal: ' . $terminales['mtgespktrm'], 'trm');
				$data_markers[] = array(
					'fftt'		=> 'S',
					'dir'		=> 'terminal',
					'title' 	=> $terminales['caja'],
					'estado' 	=> $terminales['estado'],
					'qparlib'	=> $terminales['qparlib'],
					'y'	 		=> $terminales['y'],
					'x'	 		=> $terminales['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'trm'
				);
				$i++;
			}
		}
	}
	
	if( isset($_POST['comp']['arm']) ) {
	
		$data_markers_img['arm'] = 'powersubstation.png';
		
		//carga datos de armarios
		$array_armarios = $obj_terminales->get_armarios_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		foreach ($array_armarios as $armarios) {
	
			if( $armarios['y'] != '' && $armarios['x'] != '' ) {

				$detalle = '<b>Zonal:</b> ' . $armarios['zonal'] . '<br />';
				$detalle .= '<b>Mdf:</b> ' . $armarios['mdf'] . '<br />';
				$detalle .= '<b>Armario:</b> ' . $armarios['armario'] . '<br />';
				$detalle .= '<b>Capacidad:</b> ' . $armarios['qcaparmario'] . '<br />';
				$detalle .= '<b>Pares libres:</b> ' . $armarios['qparlib'] . '<br />';
				$detalle .= '<b>Pares reserv.:</b> ' . $armarios['qparres'] . '<br />';
				$detalle .= '<b>Pares distrib.:</b> ' . $armarios['qdistrib'] . '<br /><br />';
				$detalle .= '<b>X,Y:</b> ' . $armarios['x'] . ',' . $armarios['y'] . '<br />';
			
				//$data_markers[] = array($armarios['mtgespkarm'], $armarios['y'], $armarios['x'], $i, 'Armario: ' . $armarios['mtgespkarm'], 'arm');
				$data_markers[] = array(
					'fftt'		=> 'S',
					'dir'		=> 'armario',
					'title' 	=> $armarios['armario'],
					'estado' 	=> $armarios['estado'],
					'y'	 		=> $armarios['y'],
					'x'	 		=> $armarios['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'arm'
				);
				$i++;
			}
		}
	}
	
	if( isset($_POST['comp']['mdf']) ) {
	
		$data_markers_img['mdf'] = 'tent.png';
	
		//carga datos de mdfs
		$array_mdfs = $obj_terminales->get_mdfs_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		foreach ($array_mdfs as $mdfs) {
	
			if( $mdfs['y'] != '' && $mdfs['x'] != '' ) {
			
				$detalle = '<b>Zonal:</b> ' . $mdfs['zonal'] . '<br />';
				$detalle .= '<b>Mdf:</b> ' . $mdfs['mdf'] . '<br />';
				$detalle .= '<b>Direccion:</b> ' . $mdfs['direccion'] . '<br /><br />';
				$detalle .= '<b>X,Y:</b> ' . $mdfs['x'] . ',' . $mdfs['y'] . '<br />';
			
				//$data_markers[] = array($mdfs['mdf'], $mdfs['y'], $mdfs['x'], $i, 'MDF: ' . $mdfs['mdf'], 'mdf');
				$data_markers[] = array(
					'fftt'		=> 'S',
					'dir'		=> 'mdf',
					'title' 	=> $mdfs['mdf'],
					'estado' 	=> $mdfs['estado'],
					'y'	 		=> $mdfs['y'],
					'x'	 		=> $mdfs['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'mdf'
				);
				$i++;
			}
		}
	}
	
	if( isset($_POST['comp']['edi']) ) {
	
		$data_markers_img['edi'] = 'edificio';
	
		//carga datos de edificios
		$array_edificios = $obj_terminales->get_edificios_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		foreach ($array_edificios as $edificios) {
	
			if( $edificios['y'] != '' && $edificios['x'] != '' ) {
			
				//$detalle = '<img src="../../../img/fftt/edi/' . $edificios['foto1'] . '" width="120"><br />';
				$detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . $edificios['foto1'] . '&dircomp=edi&w=120"><br />';
				$detalle .= '<b>Edificio:</b> ' . $edificios['item'] . '<br />';
				$detalle .= '<b>X,Y:</b> ' . $edificios['x'] . ',' . $edificios['y'] . '<br />';
				//$detalle .= '<a title="Ver mas detalles para este edificio" href="javascript:detalleEdificio(\''. $edificios['idedificio'] .'\');">Ver mas</a> | ';
				$detalle .= '<a title="Editar Edificio" href="javascript:editarEdificio(\''. $edificios['idedificio'] .'\',\''. $edificios['item'] .'\',\''. $edificios['x'] .'\',\''. $edificios['y'] .'\');">Editar Edificio</a> | ';
				$detalle .= '<a title="Agregar nuevas imagenes para este edificio" href="javascript:nuevasImagenesEdificio(\''. $edificios['idedificio'] .'\',\''. $edificios['item'] .'\',\''. $edificios['x'] .'\',\''. $edificios['y'] .'\');">Agregar nuevas imagenes</a>';
			
				//$data_markers[] = array($edificios['mdf'], $edificios['y'], $edificios['x'], $i, 'MDF: ' . $edificios['mdf'], 'mdf');
				$data_markers[] = array(
					'fftt'		=> 'N',
					'dir'		=> 'edificio',
					'title' 	=> $edificios['item'],
					'estado' 	=> $edificios['estado'],
					'y'	 		=> $edificios['y'],
					'x'	 		=> $edificios['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'edi'
				);
				$i++;
			}
		}
	}
	
	if( isset($_POST['comp']['com']) ) {
	
		$data_markers_img['com'] = 'competencia.png';
	
		//carga datos de competencias
		$array_competencias = $obj_terminales->get_competencias_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		foreach ($array_competencias as $competencias) {
	
			if( $competencias['y'] != '' && $competencias['x'] != '' ) {
			
				$detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . $competencias['foto1'] . '&dircomp=com&w=120"><br />';
				$detalle .= '<b>Competencia:</b> ' . $competencias['idcompetencia'] . '<br />';
				$detalle .= '<b>X,Y:</b> ' . $competencias['x'] . ',' . $competencias['y'] . '<br />';
				$detalle .= '<a title="Editar Competencia" href="javascript:editarEdificio(\''. $competencias['idedificio'] .'\',\''. $competencias['item'] .'\',\''. $competencias['x'] .'\',\''. $competencias['y'] .'\');">Editar Competencia</a> | ';
			
				//$data_markers[] = array($competencias['mdf'], $competencias['y'], $competencias['x'], $i, 'MDF: ' . $competencias['mdf'], 'mdf');
				$data_markers[] = array(
					'fftt'		=> 'N',
					'dir'		=> 'competencia',
					'title' 	=> $competencias['idcompetencia'],
					'estado' 	=> 'estado',
					'y'	 		=> $competencias['y'],
					'x'	 		=> $competencias['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'com'
				);
				$i++;
			}
		}
	}
	
	
	if( isset($_POST['comp']['esb']) ) {
	
		$data_markers_img['esb'] = 'estacionbase';
	
		//carga datos de edificios
		$array_edificios = $obj_terminales->get_estacionbases_by_xy($conexion, $_POST['x'], $_POST['y'], 5, 10);
		foreach ($array_edificios as $edificios) {
	
			if( $edificios['y'] != '' && $edificios['x'] != '' ) {
			
				//$detalle = '<img src="../../../img/fftt/edi/' . $edificios['foto1'] . '" width="120"><br />';
				//$detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . $edificios['foto1'] . '&dircomp=edi&w=120"><br />';
				$detalle = '<b>Estacion Base:</b> ' . $edificios['nombre'] . '<br />';
				$detalle .= '<b>Direccion:</b> ' . $edificios['direccion'] . '<br />';
				$detalle .= '<b>Departamento:</b> ' . $edificios['departamento'] . '<br />';
				$detalle .= '<b>Provincia:</b> ' . $edificios['provincia'] . '<br />';
				$detalle .= '<b>Distrito:</b> ' . $edificios['distrito'] . '<br />';
				$detalle .= '<b>X,Y:</b> ' . $edificios['x'] . ',' . $edificios['y'] . '<br />';
				//$detalle .= '<a title="Ver mas detalles para este edificio" href="javascript:detalleEdificio(\''. $edificios['idedificio'] .'\');">Ver mas</a> | ';
				//$detalle .= '<a title="Editar Edificio" href="javascript:editarEdificio(\''. $edificios['idedificio'] .'\',\''. $edificios['item'] .'\',\''. $edificios['x'] .'\',\''. $edificios['y'] .'\');">Editar Edificio</a> | ';
				//$detalle .= '<a title="Agregar nuevas imagenes para este edificio" href="javascript:nuevasImagenesEdificio(\''. $edificios['idedificio'] .'\',\''. $edificios['item'] .'\',\''. $edificios['x'] .'\',\''. $edificios['y'] .'\');">Agregar nuevas imagenes</a>';
			
				//$data_markers[] = array($edificios['mdf'], $edificios['y'], $edificios['x'], $i, 'MDF: ' . $edificios['mdf'], 'mdf');
				$data_markers[] = array(
					'fftt'		=> 'N',
					'dir'		=> 'estacionbase',
					'title' 	=> $edificios['nombre'],
					'estado' 	=> 'estado',
					'y'	 		=> $edificios['y'],
					'x'	 		=> $edificios['x'],
					'i'	 		=> $i,
					'detalle'	=> $detalle,
					'tip'		=> 'esb'
				);
				$i++;
			}
		}
	}
	
	
	/*echo '<pre>';
	print_r($_POST);
	echo '</pre>';*/
	
	if( isset($_POST['capa']) && !empty($_POST['capa']) ) {
	
		$capas_arr = $obj_capas->get_capas($conexion);
		$capa_array = array();
		foreach( $capas_arr as $capa ) {
			$capa_array[$capa['idcapa']] = array(
				'nombre'	=> $capa['nombre'],
				'abv_capa'	=> $capa['abv_capa'],
				'ico'		=> $capa['ico']
			);
		}
		
	
		foreach( $_POST['capa'] as $idcapa => $abv_capa ) {
		
			//almaceno las imagenes segun la capa
			$data_markers_img[$abv_capa] = $capa_array[$idcapa]['ico'];
		
			//carga de campos segun la capa
			$campos_capa_arr = $obj_capas->get_campos_by_capa($conexion, $idcapa);
			$data_campos = array();
			foreach( $campos_capa_arr as $campo_capa ) {
				$data_campos['campo' . $campo_capa['campo_nro']] = $campo_capa['campo'];
			}

			//carga datos de capa = casa, tienda, cabina
			$array_capas = $obj_capas->get_capas_by_xy($conexion, $idcapa, $data_campos, $_POST['x'], $_POST['y'], 10, 10);
			foreach ($array_capas as $capas) {

				//$detalle = '<img src="../../../img/fftt/cas/' . $capas['foto1'] . '" width="120"><br />';
				$detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . $capas['foto1'] . '&dircomp=' . $capa_array[$idcapa]['abv_capa'] . '&w=120"><br />';
				foreach($data_campos as $nroCampo => $campo) {
					$detalle .= $campo . ': ' . $capas[$nroCampo] . '<br/>';
				}
	
				if( $capas['y'] != '' && $capas['x'] != '' ) {
					//$data_markers[] = array($capas['idcomponente'], $capas['y'], $capas['x'], $i, 'CASA: ' . $capas['idcomponente'] . $img_src, 'cas');
					$data_markers[] = array(
						'title' 	=> $capas['campo1'],
						'y'	 		=> $capas['y'],
						'x'	 		=> $capas['x'],
						'i'	 		=> $i,
						'detalle'	=> $detalle,
						'tip'		=> $abv_capa
					);
					$i++;
				}
			}	
		}
	}
	

	/*echo '<pre>';
	print_r($_POST);
	echo '</pre>';*/
	
	$capa_selected = array(
		'capa'	=> '',
		'zoom_sel' => 11
	);
	
	if( isset($_POST['capa_selected']) && $_POST['capa_selected'] != '' ) {
		$capa_selected = array(
			'capa'	=> $_POST['capa_selected'],
			'zoom_sel' => $_POST['zoom_selected']
		);
	}
	

}


include 'principal/buscomp.php';    

?>

