<script type="text/javascript" src="modulos/maps/js/getData.js?time=<?php echo mktime();?>"></script>
<script type="text/javascript" src="modulos/maps/js/registro.trabajo.fftt.js?time=<?php echo mktime();?>"></script>

<script type="text/javascript" src="js/jquery/jquery-ui-timepicker-addon.js"></script>

<link rel="stylesheet" href="modulos/maps/css/style.general.css?time=<?php echo mktime();?>" type="text/css" />
<style>
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody .fueraPlazo {
    background-color: #ff0000;
    color: #ffffff;
}
</style>
<script type="text/javascript">
    
var pr = null;
$(document).ready(function(){

	$("#btn_buscar").click(function() {
            D.BuscarRegistroTrabajo();
	});
	
	$("#campo_valor").keyup(function(e) {
            if(e.keyCode == 13) {
                D.BuscarRegistroTrabajo();
            }
        });
        
        $("#cancelar_ajax").click(function(){
            pr.abort();
        });


        cerrarVentana = function(idcomponente) {
            $("#childModal").dialog('close');
	}
        
        ShowFechaRealSolucion = function() {
            if( $("#c_estado").val() == 'LIQUIDADO' ) {
                $("#trFechaRealSolucion").show();
            }else {
                $("#trFechaRealSolucion").hide();
            }
	}
        
        DetalleRegistroTrabajo = function(id) {
            D.DetalleRegistroTrabajo(id);
        }
        
        CambiarFechaRegistroTrabajo = function(id) {
            D.CambiarFechaRegistroTrabajo(id);
        }
        
        GestionarRegistroTrabajo = function(id) {
            D.GestionarRegistroTrabajo(id);
        }
        
        RegistrarCambioEstado = function() {
            D.RegistrarCambioEstado();
        }
        
        RegistrarCambioFecha = function() {
            D.RegistrarCambioFecha();
        }


	var D = {

            BuscarRegistroTrabajo: function() {
                var codjefatura     = $("#codjefatura").val();
                var estado          = $("#estado").val();
                var negocio         = $("#cmb_tipoNegocio").val();
                var tipored         = $("#cmb_tipo_red").val();
                var codelementoptr  = $("#cmb_elemento").val();
                
                var campo_filtro    = $("#campo_filtro").val();
                var campo_valor     = $.trim($("#campo_valor").val());

                var page = 1;
                var tl = $("#tl_registros").val();

                loader("start");
                pr = $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=FiltroBusqueda", { 
                    pagina: page, total: tl, codjefatura: codjefatura, estado: estado, negocio: negocio, tipored: tipored, codelementoptr: codelementoptr, campo_filtro: campo_filtro, campo_valor: campo_valor }, 
                    function(data){
                        $("#tb_resultado").empty();
                        $("#tb_resultado").append(data);
                        loader("end");
                });
            },
            
            DetalleRegistroTrabajo: function(id_registro_trabajo_planta) {
                loader('start');
                $("#childModal").html('Cargando...');
                $("#childModal").css("background-color","#FFFFFF");
                pr = $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=viewDetalleRegistroTrabajo", {
                        id_registro_trabajo_planta : id_registro_trabajo_planta
                    },
                    function(data){
                        loader('end');
                        $("#childModal").html(data);
                });

                $("#childModal").dialog({modal:true, width:'80%', hide: 'slide', title: 'DETALLE DEL TRABAJO EN PLANTA Y AVERIAS MASIVAS', position:'top'});

            },
            
            CambiarFechaRegistroTrabajo: function(id_registro_trabajo_planta) {
                loader('start');
                $("#childModal").html('Cargando...');
                $("#childModal").css("background-color","#FFFFFF");
                pr = $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=viewCambiarFechaRegistroTrabajo", {
                        id_registro_trabajo_planta : id_registro_trabajo_planta
                    },
                    function(data){
                        loader('end');
                        $("#childModal").html(data);
                });

                $("#childModal").dialog({modal:true, width:'70%', hide: 'slide', title: 'CAMBIAR FECHAS DE TRABAJO EN PLANTA Y AVERIAS MASIVAS', position:'top'});

            },
            
            GestionarRegistroTrabajo: function(id_registro_trabajo_planta) {
                loader('start');
                $("#childModal").html('Cargando...');
                $("#childModal").css("background-color","#FFFFFF");
                pr = $.post("modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=viewGestionarRegistroTrabajo", {
                        id_registro_trabajo_planta : id_registro_trabajo_planta
                    },
                    function(data){
                        loader('end');
                        $("#childModal").html(data);
                });

                $("#childModal").dialog({modal:true, width:'80%', hide: 'slide', title: 'GESTIONAR TRABAJO EN PLANTA Y AVERIAS MASIVAS', position:'top'});

            },
            
            RegistrarCambioEstado: function() {

                var id_registro_trabajo_planta  = $("#id_registro_trabajo_planta").val();
                var estado                      = $("#c_estado").val();
                var fecha_fin_real_solucion     = $("#fecha_fin_real_solucion").val();
                var observaciones               = $("#comentarios").val();

                if( estado == "" ) {
                    alert("Seleccione el estado a cambiar");
                    return false;
                }
                
                if( estado == 'LIQUIDADO' ) {
                    if( fecha_fin_real_solucion == "" ) {
                        alert("Ingrese la fecha de la solucion del trabajo");
                        return false;
                    }
                }
 
                if( confirm("Esta seguro de grabar los datos?") ) {
                    $("#box_loading").show();
                    $("#RegistrarCambioEstado").attr('disabled', 'true');
                    $.ajax({
                        type:     "POST",
                        dataType: "json",
                        url:      "modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=RegistrarCambioEstado",
                        data:     {id_registro_trabajo_planta: id_registro_trabajo_planta, estado: estado, fecha_fin_real_solucion: fecha_fin_real_solucion, observaciones: observaciones},
                        success:  function(datos) {
                            if( !datos['flag'] ) {
                                alert(datos['mensaje']);
                            }else {
                                //alert(datos['msg']);
                                //$("#box_loading").hide();
                                $("#box_loading").html('<img src="img/accept.png"> <b>' + datos['mensaje'] + '</b>');
                                D.BuscarRegistroTrabajo();
                            }
                            setTimeout("cerrarVentana()", 1500);
                        }
                    });

                }
			
            },
            
            RegistrarCambioFecha: function() {

                var id_registro_trabajo_planta  = $("#id_registro_trabajo_planta").val();
                var fecha_inicio_trabajo        = $("#fecha_inicio_trabajo").val();
                var fecha_fin_trabajo           = $("#fecha_fin_trabajo").val();
 
                if( confirm("Esta seguro de cambiar las fechas de inicio y fin del trabajo?") ) {
                    $("#box_loading").show();
                    $("#RegistrarCambioFecha").attr('disabled', 'true');
                    $.ajax({
                        type:     "POST",
                        dataType: "json",
                        url:      "modulos/maps/bandeja.registro.trabajo.fftt.php?cmd=RegistrarCambioFecha",
                        data:     {id_registro_trabajo_planta: id_registro_trabajo_planta, fecha_inicio_trabajo: fecha_inicio_trabajo, fecha_fin_trabajo: fecha_fin_trabajo},
                        success:  function(datos) {
                            if( !datos['flag'] ) {
                                alert(datos['mensaje']);
                            }else {
                                //alert(datos['msg']);
                                //$("#box_loading").hide();
                                $("#box_loading").html('<img src="img/accept.png"> <b>' + datos['mensaje'] + '</b>');
                                D.BuscarRegistroTrabajo();
                            }
                            setTimeout("cerrarVentana()", 1500);
                        }
                    });

                }
			
            }
            
        }
});
		
</script>

<!-- Cuerpo principal -->
<div id="content">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">
			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td class="form_titulo" width="35%">
					<img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Bandeja de trabajos en planta y aver&iacute;as masivas
				</td>
				<td width="65%" align="left">
                                    
                                        <?php
                                        if( $user_permitido == '1' ) {
                                        ?>
                                    
					<div style="display:block;">
						Jefatura                                                
                                                <select id="codjefatura" name="codjefatura" style="font-size:11px;">
                                                    <option value="" selected="selected" >Todos</option>
                                                <?php
                                                    foreach ($arreglo_jefatura as $row):
                                                        $nomzonal = ($row->__get('zonal')=="LIM")?"LIMA":$row->__get('jefatura_desc');
                                                        echo '<option value="'.$row->__get('jefatura').'" title="'.$row->__get('zonal').'" class="'.$row->__get('idempresa').'" lang="'.$nomzonal.'">'.$row->__get('jefatura_desc').'</option>';
                                                    endforeach;
                                                ?>     
                                                </select>
						&nbsp;
						Estado
                                                <select name="estado" id="estado">
                                                    <option value="">Todos</option>
                                                    <option value="PENDIENTE">PENDIENTE</option>
                                                    <option value="CANCELADO">CANCELADO</option>
                                                    <option value="LIQUIDADO">LIQUIDADO</option>
                                                </select>
					</div>
                                        <?php
                                        }
                                        ?>
				</td>
			</tr>
			<tr>
                            <td colspan="2">
                                
                                <?php
                                if( $user_permitido == '1' ) {
                                ?>
                            
                                <div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                                    <div style="display: block; border-bottom: 1px solid #999999;">
                                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td width="10%" align="left">Buscar por:</td>
                                            <td width="90%" align="left">
                                                <select name="campo_filtro" id="campo_filtro">
                                                        <option value="f_cod_requerimiento" selected="selected">COD. REQUERIMIENTO</option>
                                                </select>
                                                <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                                <input type="submit" id="btn_buscar" name="buscar" value="Buscar"/>
                                            </td>
                                        </tr>
                                        </table>
                                    </div>
                                    
                                    <div style="display: block;">
                                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td width="10%" align="left">Filtrar por:</td>
                                            <td width="90%" align="left">
                                                <input type="hidden" id="cmb_tipoRegistro" value="P">
                                                <input type="hidden" id="is_bandeja" value="1">
                                                Tipo de negocio:
                                                <select id="cmb_tipoNegocio" name="cmb_tipoNegocio" style="width: 120px;">
                                                    <option value="" selected="selected" >Todos</option>
                                                <?php 
                                                foreach ($arreglo_negocio_pex as $row):  
                                                    echo '<option value="'.$row->__get('negocio').'">'.$row->__get('negocio').'</option>';
                                                endforeach; 
                                                ?>        
                                                </select>
                                                <span class="hide tipored filtroElemento">
                                                    Tipo Red:
                                                    <select id="cmb_tipo_red" name="cmb_tipo_red">
                                                        <option value="" selected="selected">Todos</option>
                                                        <option value="D">RED DIRECTA</option>
                                                        <option value="F">RED FLEXIBLE</option>
                                                    </select>
                                                </span>    
                                                <span class="hide elemento filtroElemento">
                                                    Elementos de Red:
                                                    <select id="cmb_elemento" name="cmb_elemento" style="width: 20%" disabled="disabled">
                                                        <option value="" selected="selected">Todos</option>
                                                    </select>
                                                </span>

                                            </td>
                                        </tr>
                                        </table>
                                    </div>
                                </div>
                                
                                <?php
                                }
                                ?>
                            </td>
			</tr>
			</table>

                    <?php
                    if( $user_permitido == '1' ) {
                    ?>
                    
			<?php if($total_registros>0):?>
			<table id="tb_sup_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
			<tr>
			<td style="width:55%; height: 30px; text-align: left;">
				Han sido listado(s) <b><span id="cont_num_resultados_head"><?php echo $total_registros ?></span></b> registro(s) 
                                <!--&nbsp;|&nbsp; 
				<a onclick="descargaDunaInternet();" href="javascript:void(0)">Descargar</a>
				<a onclick="descargaDunaInternet();" href="javascript:void(0)"><img src="img/ico_excel.png" title="Descargar excel"></a>
                                -->
			</td>   
			<td style="width:45%;">
				<div  style=" width:360px; position: relative;">
					<div id="paginacion"></div>
				</div>
			</td>
			</tr>
			</table>
			<?php endif;?>
			
			<input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
			<input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $total_registros;?>"/>
				  
			<!--<div style="width: 100%;  min-height: 300px;">-->
                        
			<div style="width: 100%;">
			
				<table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;">
				<thead>
                                    <tr style="line-height:13px;">
                                        <th class="headTbl center" width="2%">#</th>
                                        <th class="headTbl center" width="5%">Opciones</th>
                                        <th class="headTbl center" width="8%">Cod. req.</th>
                                        <th class="headTbl center" width="5%">Zonal</th>
                                        <th class="headTbl center" width="8%">Jefatura</th>
                                        <th class="headTbl center" width="8%">Negocio</th>
                                        <th class="headTbl center" width="7%">Mdf</th>
                                        <th class="headTbl center" width="10%">Elem. Red</th>
                                        <th class="headTbl center" width="8%">Elemento</th>
                                        <th class="headTbl center" width="10%">Fec. registro</th>
                                        <th class="headTbl center" width="10%">Inicio trab.</th>
                                        <th class="headTbl center" width="10%">Fin trab.</th>
                                        <th class="headTbl center" width="10%">Estado</th>
                                    </tr>
				</thead>
				<tbody>
                                <?php
				$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
				foreach($trabajos_planta as $trabajo_planta){
                                    
                                    $flag_fueraPlazo = 'celdaX';
                                    $alt_fueraPlazo = '';
                                    if( $trabajo_planta['fecha_fin_trabajo'] < date('Y-m-d H:i:s') && $trabajo_planta['estado'] == 'PENDIENTE' ) {
                                        $flag_fueraPlazo = 'celdaY fueraPlazo';
                                        $alt_fueraPlazo = 'title = "fuera de plazo"';
                                    }
                                    
                                    /*$elemento = '';
                                    if($trabajo_planta['codplantaptr'] == 'B'){ //STB/ADSL
                                        
                                        if( $trabajo_planta['tipored'] == 'D' ) {
                                            if( $trabajo_planta['codelementoptr'] == '19' ) { //Cable Alimentador
                                                $elemento = $trabajo_planta['ccable'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '3' ) { //terminal
                                                $elemento = $trabajo_planta['terminal'];
                                            }
                                        }else if( $trabajo_planta['tipored'] == 'F' ) {
                                            if( $trabajo_planta['codelementoptr'] == '19' ) { //Cable Alimentador
                                                $elemento = $trabajo_planta['parpri'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '8' ) { //armario
                                                $elemento = $trabajo_planta['carmario'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '23' ) { //cable secundario
                                                $elemento = $trabajo_planta['cbloque'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '3' ) { //terminal
                                                $elemento = $trabajo_planta['terminal'];
                                            }
                                        }

                                    }else if($trabajo_planta['codplantaptr'] == 'C'){ //CATV
                                        
                                        if( $trabajo_planta['codelementoptr'] == '11' ) { //troba
                                            $elemento = $trabajo_planta['carmario'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '2' ) { //amplificador
                                            $elemento = $trabajo_planta['cbloque'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '1' ) { //tap
                                            $elemento = $trabajo_planta['terminal'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '20' ) { //borne
                                            $elemento = $trabajo_planta['borne'];
                                        }
                                    }*/
                                    
                                    $elemento = obtenerElementoRegistroTrabajoFFTT($trabajo_planta);
                                    
                                    ?>
                                    <tr>
                                    <td class="<?php echo $flag_fueraPlazo?>" <?php echo $alt_fueraPlazo?>><?php echo $item++; ?></td>
                                    <td class="celdaX">
                                        
                                        <a href="javascript:DetalleRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" title="Ver detalle">
                                            <img width="12" height="12" title="Ver detalle" src="img/ico_detalle.jpg">
                                        </a>
                                        <?php
                                        if( $trabajo_planta['estado'] == 'PENDIENTE' ) {
                                        ?>
                                        <a href="javascript:CambiarFechaRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" title="Cambiar fechas de Registro de Trabajo">
                                            <img width="12" height="12" title="Cambiar fechas de Registro de Trabajo" src="img/icon_horario_psi.png">
                                        </a>
                                        <a title="Gestionar" onclick="GestionarRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" href="javascript:void(0)">
                                            <img width="13" height="13" title="Gestionar" src="img/ico_gestionar.jpg">
                                        </a>
                                        <?php
                                        }else {
                                        ?>
                                            <img width="12" height="12" title="Cambiar fechas de Registro de Trabajo" src="img/icon_horario_psi_disabled.png">
                                            <img width="13" height="13" title="Gestionar" src="img/ico_gestionar_disabled.jpg">
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="celdaX"><?php echo 'RTP-' . $trabajo_planta['id_registro_trabajo_planta']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['zonal']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['jefatura_desc']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['negocio']; ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['mdf']; ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['descripcion']; ?></td>
                                    <td class="celdaX"><?php echo $elemento; ?></td>
                                    <td class="celdaX"><?php echo obtenerFecha($trabajo_planta['fecha_insert']); ?></td>
                                    <td class="celdaX"><?php echo obtenerFecha($trabajo_planta['fecha_inicio_trabajo']); ?></td>
                                    <td class="<?php echo $flag_fueraPlazo?>" <?php echo $alt_fueraPlazo?>><?php echo obtenerFecha($trabajo_planta['fecha_fin_trabajo']); ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['estado']; ?></td>
                                    </tr>
			 <?php
			   }
			 ?>
				</tbody>
				</table>
			</div>
			
			<br />
			<?php if($total_registros>0):?>
			<table id="tb_pie_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
			<tr>
			   <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot"><?php echo $total_registros; ?></span></b> registro(s)  <br/><br/></td>
			</tr>
			</table>
			<?php endif;?>
                        
                    <?php
                    }else {
                    ?>    
                        <div style="padding:20px; text-align: center; font-weight: bold;">Disculpe, pero usted no pertenece a un area asignada para realizar Marcaci&oacute;n de trabajos en planta y aver&iacute;as masivas.</div>
                    <?php
                    }
                    ?>

		</td>
	</tr>
	</table>
	<br /><br />
</div>

<div id="footer"></div>

<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
    <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->
<input type="hidden" value="1" id="emisor" name="emisor" />


<div id="parentModalPirata" style="display: none;">
	<div id="childModalPirata" style="padding: 10px; background: #fff;"></div>
</div>
