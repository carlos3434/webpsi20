<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>


<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<script type="text/javascript" src="../../js/jquery/jquery.js"></script>
<!--<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>-->
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">
$(document).ready(function() {

	var idgestion_obra = '<?php echo $arrObjEdificio[0]->__get('_idGestionObra')?>';
	var arrZonal = <?php echo $jsonZonal?>;

	validaGestionObra = function() {
	    if ( idgestion_obra == '2' ) {
	    	$(".c_required").removeAttr("style");
	    }
	}

	//validacion de campos cuando gestion de obras = 2 = En curso 
	validaGestionObra();
	
	$("#fecha_cableado, #fecha_registro_proyecto, #montante_fecha, #ducteria_interna_fecha, #punto_energia_fecha, #fecha_tratamiento_call, #fecha_seguimiento, #fecha_termino_construccion").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy'
    });
    
    $(".limpiar_fecha").click(function() {
        var campo = $(this).attr('c_fecha');
        $("#" + campo).attr('value', '');
    });

    $("#idgestion_obra").change(function() {
        if ( $("#idgestion_obra").val() == '2' ) {
            $(".c_required").removeAttr("style");
        } else {
        	$(".c_required").attr("style", "display:none;");
        }
    });

    var mapEdi = null;
    var markers_arr = [];
    
    var x_coord = '<?php echo (isset($_REQUEST['x'])) ? $_REQUEST['x'] : '';?>';
    var y_coord = '<?php echo (isset($_REQUEST['y'])) ? $_REQUEST['y'] : '';?>';
    
    var x = '';
    var y = '';
    if( x_coord != '' && y_coord != '' ) {
        x = x_coord;
        y = y_coord;
    }

    var M = {
        initialize: function() {
            //seteando el xy de lima 
        	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
            if ( arrZonal.length > 0 ) {
            	latlng = new google.maps.LatLng(arrZonal[0]['y'], arrZonal[0]['x']);
            }
            
            var zoom = 14;
            if( x != '' && y != '' ) {
                latlng = new google.maps.LatLng(y, x);
                zoom = 17;
            } else {
            	//muestro el mensaje que no tiene x,y
            	$('#divMsgMapaEdificio').show();
            }
            var myOptions = {
                zoom: zoom,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                navigationControl: true,
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.ANDROID,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            };
            mapEdi = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            //solo si tiene xy pongo el marker 
            if( x != '' && y != '' ) {
                marker = new google.maps.Marker({
                    position: latlng,
                    map: mapEdi,
                    icon: 'vistas/icons/markergreen.png'
                });
                markers_arr.push(marker);
            }

            google.maps.event.addListener(mapEdi, "click", function(event) {

            	if(!confirm("Desea establecer este nuevo punto x,y?")) {
                    return false;
                }
                
                //clear markers
                M.clearMarkers();
	
                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: mapEdi,
                    icon: 'vistas/icons/markergreen.png'
                });
                markers_arr.push(marker);

                var Coords = event.latLng;
                var x = Coords.lng();
                var y = Coords.lat();
	
                mapEdi.setCenter(event.latLng);
	
                $("#x").attr('value', x);
                $("#y").attr('value', y);
	
            });
        },

        clearMarkers: function() {
            for (var n = 0, marker; marker = markers_arr[n]; n++) {
                marker.setVisible(false);
            }
        }

    };

    //carga inicial 
    M.initialize();
    
});
</script>

<style>
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 2px;
}

table.tb_detalle_edi input {
    width: 95%;
}
table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Editar Componente</td>
</tr>
</table>

<?php 
if ( !empty($arrObjEdificio) ) {
?>

<div id="map_canvas" style="width:100%;height:300px;"></div>

<div id="divMsgMapaEdificio" style="color:#FF0000; text-align:left; display:none;"> Este edificio no registra X,Y para poder visualizarlo en el mapa </div>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td>X1: <input type="text" name="x" id="x" value="<?php echo $_REQUEST['x']?>" readonly="readonly" /><td>
</tr>
<tr>
    <td>Y1: <input type="text" name="y" id="y" value="<?php echo $_REQUEST['y']?>" readonly="readonly" /><td>
</tr>
</table>


<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">

    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N EDIFICIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="30%" class="da_subtitulo">ITEM 
        <?php 
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
        ?>
        <span class="da_import">(<?php echo $arrObjEdificio[0]->__get('_item')?>)</span>
        <?php 
        } 
        ?>
        </td>
        <td width="70%" class="da_import">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <div style="float:left;">
                <input type="hidden" name="item_inicial" id="item_inicial" size="4" value="<?php echo $arrObjEdificio[0]->__get('_item')?>" />
                <input type="text" name="item" id="item" size="8" value="<?php echo $item?>" />&nbsp;
                <select name="item_etapa" id="item_etapa">
                    <option value="">Seleccione</option>
                    <?php
                    foreach ($arrEtapaProyecto as $idetapa => $etapaStr) {
                    ?>
                        <option value="<?php echo $idetapa ?>" <?php if ($idetapa == $etapa) echo 'selected'; ?>><?php echo $etapaStr ?></option>
                    <?php
                    }
                    ?>
                </select>
                </div>    
                <div id="msgItem" style="float: left; width: 80px; margin-left: 5px;"></div>
            <?php 
            } else {
            ?>
                <input type="hidden" name="item" id="item" value="<?php echo $arrObjEdificio[0]->__get('_item')?>" />
                <?php echo $arrObjEdificio[0]->__get('_item')?>
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N DE OBRAS</td>
        <td class="da_import">
        <?php 
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
        ?>
            <select name="idgestion_obra" id="idgestion_obra">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjGestionObra) ) {
                foreach ( $arrObjGestionObra as $objGestionObra ) {
                ?>
                <option value="<?php echo $objGestionObra->__get('_idGestionObra')?>" <?php if ( $objGestionObra->__get('_idGestionObra') == $arrObjEdificio[0]->__get('_idGestionObra') ) echo 'selected'; ?> ><?php echo $objGestionObra->__get('_desGestionObra')?></option>
                <?php 
                }
                }
                ?>
            </select>
        <?php
        } else {
            if ( !empty($arrObjGestionObra) ) {
                foreach ( $arrObjGestionObra as $objGestionObra ) {
                    if ( $objGestionObra->__get('_idGestionObra') == $arrObjEdificio[0]->__get('_idGestionObra') ) {
                        echo $objGestionObra->__get('_desGestionObra');
                        break;
                    }
                }
            }
        ?>
            <input type="hidden" name="idgestion_obra" id="idgestion_obra" value="<?php echo $arrObjEdificio[0]->__get('_idGestionObra')?>" />
        <?php
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMPRESA</td>
        <td class="da_import">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <select name="idempresa" id="idempresa">
                    <option value="">Seleccione</option>
                    <?php 
                    if ( !empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
                    foreach ( $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa ) {
                    ?>
                    <option value="<?php echo $objEmpresa->__get('_idEmpresa')?>" <?php if ( $arrObjEdificio[0]->__get('_idEmpresa') == $objEmpresa->__get('_idEmpresa') ) echo 'selected';?> ><?php echo $objEmpresa->__get('_descEmpresa')?></option>
                    <?php
                    }
                    }
                    ?>
                </select> <em>*</em>
            <?php
            } else {
                echo $arrObjEdificio[0]->__get('_descEmpresa');
            ?>
                <input type="hidden" name="idempresa" id="idempresa" value="<?php echo $arrObjEdificio[0]->__get('_idEmpresa')?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">C&Oacute;DIGO PROYECTO WEB</td>
        <td class="da_import2">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
            <input type="text" name="codigo_proyecto_web" id="codigo_proyecto_web" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_codigoProyectoWeb'))?>" />
            <?php 
            } else {
                echo $arrObjEdificio[0]->__get('_codigoProyectoWeb');
            ?>
                <input type="hidden" name="codigo_proyecto_web" id="codigo_proyecto_web" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_codigoProyectoWeb'))?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td class="da_import">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <select name="estado" id="estado">
                    <option value="">Seleccione</option>
                    <?php 
                    if ( !empty($arrObjEdificioEstado) ) {
                    foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
                    ?>
                    <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado')?>" <?php if ( $arrObjEdificio[0]->__get('_estado') == $objEdificioEstado->__get('_codEdificioEstado') ) echo 'selected';?> ><?php echo $objEdificioEstado->__get('_desEstado')?></option>
                    <?php
                    }
                    }
                    ?>
                </select>
            <?php
            } else {
                if ( !empty($arrObjEdificioEstado) ) {
                    foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
                        if ( $arrObjEdificio[0]->__get('_estado') == $objEdificioEstado->__get('_codEdificioEstado') ) {
                            echo $objEdificioEstado->__get('_desEstado');
                            break;
                        }
                    }
                }
            ?>
                <input type="hidden" name="estado" id="estado" value="<?php echo $arrObjEdificio[0]->__get('_estado')?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">REGI&Oacute;N</td>
        <td class="da_import">
            <select name="cmb_region" id="cmb_region" onchange="listarZonalesPorRegionUsuario();">
                <option value="">Seleccione</option>
                <?php 
                if (!empty($arrRegion)) {
                foreach ($arrRegion as $region) {
                ?>
                    <option value="<?php echo $region['idregion'] ?>" <?php if ($region['idregion'] == $arrObjEdificio[0]->__get('_idRegion')) echo 'selected="selected"';?> ><?php echo $region['nomregion'] . ' - ' . $region['detregion'] ?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FUENTE DE IDENTIFICACI&Oacute;N</td>
        <td class="da_import">
            <select name="idfuente_identificacion" id="idfuente_identificacion">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjFuenteIdentificacion) ) {
                foreach ( $arrObjFuenteIdentificacion as $objFuenteIdentificacion ) {
                ?>
                <option value="<?php echo $objFuenteIdentificacion->__get('_idFuenteIdentificacion')?>" <?php if ($objFuenteIdentificacion->__get('_idFuenteIdentificacion') == $arrObjEdificio[0]->__get('_idFuenteIdentificacion')) echo 'selected="selected"'; ?> ><?php echo $objFuenteIdentificacion->__get('_desFuenteIdentificacion')?></option>
                <?php 
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONAL</td>
        <td>
            <select name="cmb_zonal" id="cmb_zonal" onchange="ListarMdfsZonalUsuario();">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($_SESSION['USUARIO_ZONAL']) ) {
                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) { 
                ?>
                    <option value="<?php echo $objZonal->__get('_abvZonal') ?>" <?php if ($objZonal->__get('_abvZonal') == $arrObjEdificio[0]->__get('_zonal')) echo 'selected="selected"'; ?> ><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                <?php 
                } 
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">URA</td>
        <td>
            <select name="cmb_ura" id="cmb_ura">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrMdf) ) {
                foreach ( $arrMdf as $mdf ) {
                ?>
                <option value="<?php echo $mdf['mdf']?>" <?php if ($mdf['mdf'] == $arrObjEdificio[0]->__get('_ura')) echo 'selected="selected"';?> ><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA CABLEADO</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="fecha_cableado" id="fecha_cableado" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_fechaCableado') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaCableado'))?>" readonly="readonly" />
                <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_cableado">Limpiar</a>
            <?php 
            } else {
                if ($arrObjEdificio[0]->__get('_fechaCableado') != '0000-00-00') {
                    echo obtenerFecha($arrObjEdificio[0]->__get('_fechaCableado'));
                }
            ?>
                <input type="hidden" name="fecha_cableado" size="11" id="fecha_cableado" value="<?php if ($arrObjEdificio[0]->__get('_fechaCableado') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaCableado'))?>" readonly="readonly" />
            <?php 
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ARMARIO</td>
        <td><input type="text" name="armario" id="armario" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_armario'))?>" /></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
        <td>
            <input type="text" name="fecha_registro_proyecto" id="fecha_registro_proyecto" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaRegistroProyecto'))?>" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_registro_proyecto">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SECTOR</td>
        <td>
            <input type="text" name="sector" id="sector" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_sector'))?>" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA (TDP)</td>
        <td>
            <input type="text" name="mza_tdp" id="mza_tdp" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_mzaTdp'))?>" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO V&Iacute;A</td>
        <td>
            <select name="idtipo_via" id="idtipo_via">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoVia) ) {
                foreach ( $arrObjTipoVia as $objTipoVia ) {
                ?>
                <option value="<?php echo $objTipoVia->__get('_idTipoVia')?>" <?php if ( $objTipoVia->__get('_idTipoVia') == $arrObjEdificio[0]->__get('_idTipoVia') ) echo 'selected';?> ><?php echo $objTipoVia->__get('_desTipoVia')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
        <td>
            <input type="text" name="direccion_obra" id="direccion_obra" class="da_editar" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_direccionObra'))?>" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">N&Uacute;MERO</td>
        <td>
            <input type="text" name="numero" id="numero" value="<?php echo $arrObjEdificio[0]->__get('_numero')?>" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA</td>
        <td>
            <input type="text" name="mza" id="mza" value="<?php echo $arrObjEdificio[0]->__get('_mza')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOTE</td>
        <td>
            <input type="text" name="lote" id="lote" value="<?php echo $arrObjEdificio[0]->__get('_lote')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO CCHH</td>
        <td>
            <select name="idtipo_cchh" id="idtipo_cchh">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoCchh) ) {
                foreach ( $arrObjTipoCchh as $objTipoCchh ) {
                ?>
                <option value="<?php echo $objTipoCchh->__get('_idTipoCchh')?>" <?php if ( $objTipoCchh->__get('_idTipoCchh') == $arrObjEdificio[0]->__get('_idTipoCchh') ) echo 'selected';?> ><?php echo $objTipoCchh->__get('_desTipoCchh')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CCHH</td>
        <td>
            <input type="text" name="cchh" id="cchh" class="da_editar" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_cchh'))?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DEPARTAMENTO</td>
        <td>
            <select name="cmb_departamento" id="cmb_departamento" onchange="ListarProvinciasUbigeoEdi();">
                <option value="">Seleccione</option>
                <?php
                if ( !empty($arrObjDepartamento) ) {
                foreach ( $arrObjDepartamento as $objDepartamento ) {
                ?>
                <option value="<?php echo $objDepartamento->__get('_codDpto')?>" <?php if ($codDepartamento == $objDepartamento->__get('_codDpto')) echo 'selected="selected"';?>><?php echo $objDepartamento->__get('_nombre')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PROVINCIA</td>
        <td>
            <select name="cmb_provincia" id="cmb_provincia" onchange="ListarDistritosUbigeoEdi();">
                <option value="">Seleccione</option>
                <?php
                if ( !empty($arrObjProvincia) ) {
                foreach ( $arrObjProvincia as $objProvincia ) {
                ?>
                <option value="<?php echo $objProvincia->__get('_codProv')?>" <?php if ($codProvincia == $objProvincia->__get('_codProv')) echo 'selected="selected"';?>><?php echo $objProvincia->__get('_nombre')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DISTRITO</td>
        <td>
            <select name="cmb_distrito" id="cmb_distrito" onchange="ListarLocalidadesUbigeo();">
                <option value="">Seleccione</option>
                <?php
                if ( !empty($arrObjDistrito) ) {
                foreach ( $arrObjDistrito as $objDistrito ) {
                ?>
                <option value="<?php echo $objDistrito->__get('_codDist')?>" <?php if ($codDistrito == $objDistrito->__get('_codDist')) echo 'selected="selected"';?>><?php echo $objDistrito->__get('_nombre')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOCALIDAD</td>
        <td>
            <select name="cmb_localidad" id="cmb_localidad">
                <option value="">Seleccione</option>
                <?php
                if ( !empty($arrObjLocalidad) ) {
                foreach ( $arrObjLocalidad as $objLocalidad ) {
                ?>
                <option value="<?php echo $objLocalidad->__get('_ubigeo')?>" <?php if ($codLocalidad == $objLocalidad->__get('_ubigeo')) echo 'selected="selected"';?>><?php echo $objLocalidad->__get('_localidad')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SEGMENTO</td>
        <td class="da_import">
            <select name="cmb_segmento" id="cmb_segmento" onchange="ListarTipoProyectoPorSegmento();">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjSegmento) ) {
                foreach ( $arrObjSegmento as $objSegmento ) {
                ?>
                <option value="<?php echo $objSegmento->__get('_idSegmento')?>" <?php if ($arrObjEdificio[0]->__get('_idSegmento') == $objSegmento->__get('_idSegmento')) echo 'selected'; ?>><?php echo $objSegmento->__get('_desSegmento')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO PROYECTO</td>
        <td class="da_import">
            <select name="cmb_tipo_proyecto" id="cmb_tipo_proyecto">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoProyecto) ) {
                foreach ( $arrObjTipoProyecto as $objTipoProyecto ) {
                ?>
                <option value="<?php echo $objTipoProyecto->__get('_idTipoProyecto')?>" <?php if ($arrObjEdificio[0]->__get('_idTipoProyecto') == $objTipoProyecto->__get('_idTipoProyecto')) echo 'selected'; ?> ><?php echo $objTipoProyecto->__get('_desTipoProyecto')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE PROYECTO</td>
        <td>
            <input type="text" name="nombre_proyecto" id="nombre_proyecto" class="da_editar" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_nombreProyecto'))?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
        <td>
            <input type="text" name="nombre_constructora" id="nombre_constructora" class="da_editar" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_nombreConstructora'))?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RUC CONSTRUCTORA</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="ruc_constructora" id="ruc_constructora" class="da_editar" onkeypress="return soloNumeros(event)" value="<?php echo $arrObjEdificio[0]->__get('_rucConstructora')?>" />
            <?php 
            } else {
                echo $arrObjEdificio[0]->__get('_rucConstructora');
            ?>
                <input type="hidden" name="ruc_constructora" id="ruc_constructora" class="da_editar" onkeypress="return soloNumeros(event)" value="<?php echo $arrObjEdificio[0]->__get('_rucConstructora')?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NIVEL SOCIOECON&Oacute;MICO</td>
        <td>
            <select name="cmb_nivel_socioeconomico" id="cmb_nivel_socioeconomico">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjNivelSocioeconomico) ) {
                foreach ( $arrObjNivelSocioeconomico as $objNivelSocioeconomico ) {
                ?>
                <option value="<?php echo $objNivelSocioeconomico->__get('_idNivelSocioeconomico')?>" <?php if ($arrObjEdificio[0]->__get('_idNivelSocioeconomico') == $objNivelSocioeconomico->__get('_idNivelSocioeconomico')) echo 'selected'; ?>><?php echo $objNivelSocioeconomico->__get('_desNivelSocioeconomico')?></option>
                <?php 
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PERSONA CONTACTO</td>
        <td>
            <input type="text" name="persona_contacto" id="persona_contacto" class="da_editar" value="<?php echo strtoupper($arrObjEdificio[0]->__get('_personaContacto'))?>" /> <em class="c_required" style="display:none;">*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONA COMPETENCIA</td>
        <td class="da_import">
            <select name="cmb_zona_competencia" id="cmb_zona_competencia">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjZonaCompetencia) ) {
                foreach ( $arrObjZonaCompetencia as $objZonaCompetencia ) {
                ?>
                <option value="<?php echo $objZonaCompetencia->__get('_idZonaCompetencia')?>" <?php if ($arrObjEdificio[0]->__get('_idZonaCompetencia') == $objZonaCompetencia->__get('_idZonaCompetencia')) echo 'selected'; ?>><?php echo $objZonaCompetencia->__get('_desZonaCompetencia')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N COMPETENCIA</td>
        <td class="da_import">
            <select name="cmb_gestion_competencia" id="cmb_gestion_competencia">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjZonaCompetencia) ) {
                foreach ( $arrObjGestionCompetencia as $objGestionCompetencia ) {
                ?>
                <option value="<?php echo $objGestionCompetencia->__get('_idGestionCompetencia')?>" <?php if ($arrObjEdificio[0]->__get('_idGestionCompetencia') == $objGestionCompetencia->__get('_idGestionCompetencia')) echo 'selected'; ?>><?php echo $objGestionCompetencia->__get('_desGestionCompetencia')?></option>
                <?php 
                }
                }
                ?>
            </select> <em class="c_required" style="display:none;">*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 2G</td>
        <td class="da_import">
            <select name="cmb_cobertura_movil_2g" id="cmb_cobertura_movil_2g">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjCoberturaMovil2G) ) {
                foreach ( $arrObjCoberturaMovil2G as $objCoberturaMovil2G ) {
                ?>
                <option value="<?php echo $objCoberturaMovil2G->__get('_idCoberturaMovil2G')?>" <?php if ($arrObjEdificio[0]->__get('_idCoberturaMovil2G') == $objCoberturaMovil2G->__get('_idCoberturaMovil2G')) echo 'selected'; ?>><?php echo $objCoberturaMovil2G->__get('_desCoberturaMovil2G')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 3G</td>
        <td class="da_import">
            <select name="cmb_cobertura_movil_3g" id="cmb_cobertura_movil_3g">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjCoberturaMovil3G) ) {
                foreach ( $arrObjCoberturaMovil3G as $objCoberturaMovil3G ) {
                ?>
                <option value="<?php echo $objCoberturaMovil3G->__get('_idCoberturaMovil3G')?>" <?php if ($arrObjEdificio[0]->__get('_idCoberturaMovil3G') == $objCoberturaMovil3G->__get('_idCoberturaMovil3G')) echo 'selected'; ?>><?php echo $objCoberturaMovil3G->__get('_desCoberturaMovil3G')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">P&Aacute;GINA WEB</td>
        <td>
            <input type="text" name="pagina_web" id="pagina_web" class="da_editar" value="<?php echo $arrObjEdificio[0]->__get('_paginaWeb')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMAIL</td>
        <td>
            <input type="text" name="email" id="email" class="da_editar" value="<?php echo $arrObjEdificio[0]->__get('_email')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
        <td valign="top">
            <input type="text" size="5" name="nro_blocks" id="nro_blocks" onkeypress="return soloNumeros(event)" value="<?php echo $arrObjEdificio[0]->__get('_nroBlocks')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td colspan="2" valign="top">
            <div id="divBlock">
                <div style="width:180px;">
                    <span style="float:left; width:90px;">Nro. pisos</span>
                    <span style="float:left; width:90px;">Nro. dptos</span>
                </div>
                <div style="width:180px;">
                    <span style="float:left; width:90px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(event)" name="nro_pisos_1" id="nro_pisos_1" size="5" value="<?php echo trim($arrObjEdificio[0]->__get('_nroPisos1'))?>" /></span>
                    <span style="float:left; width:90px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(event)" name="nro_dptos_1" id="nro_dptos_1" size="5" value="<?php echo trim($arrObjEdificio[0]->__get('_nroDptos1'))?>" /></span>
                </div>
            </div>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VIVEN</td>
        <td>
            <select name="viven" id="viven">
                <option value="">Seleccione</option>
                <option value="1" <?php if( $arrObjEdificio[0]->__get('_viven') == '1' ) echo 'selected'; ?>>Si</option>
                <option value="0" <?php if( $arrObjEdificio[0]->__get('_viven') == '0' ) echo 'selected'; ?>>No</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
        <td>
            <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="<?php echo $arrObjEdificio[0]->__get('_nroDptosHabitados')?>" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
        <td>
            <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoInfraestructura) ) {
                foreach ( $arrObjTipoInfraestructura as $objTipoInfraestructura ) {
                ?>
                <option value="<?php echo $objTipoInfraestructura->__get('_idTipoInfraestructura')?>" <?php if ( $objTipoInfraestructura->__get('_idTipoInfraestructura') == $arrObjEdificio[0]->__get('_idTipoInfraestructura') ) echo 'selected';?> ><?php echo $objTipoInfraestructura->__get('_desTipoInfraestructura')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MEGAPROYECTO</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
            <select name="megaproyecto" id="megaproyecto">
                <option value="">Seleccione</option>
                <option value="1" <?php if( $arrObjEdificio[0]->__get('_megaproyecto') == '1' ) echo 'selected'; ?>>Si</option>
                <option value="0" <?php if( $arrObjEdificio[0]->__get('_megaproyecto') == '0' ) echo 'selected'; ?>>No</option>
            </select>
            <?php 
            } else {
                if ( $arrObjEdificio[0]->__get('_megaproyecto') == '1' ) {
                    echo 'Si';
                } elseif ( $arrObjEdificio[0]->__get('_megaproyecto') == '0' ) {
                    echo 'No';
                } else {
                    echo '-';
                }
            ?>
                <input type="hidden" name="megaproyecto" id="megaproyecto" value="<?php echo $arrObjEdificio[0]->__get('_megaproyecto')?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">% AVANCE</td>
        <td>
            <input type="text" name="avance" id="avance" value="<?php echo $arrObjEdificio[0]->__get('_avance')?>" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N</td>
        <td>
            <input type="text" name="fecha_termino_construccion" id="fecha_termino_construccion" class="clsTxtFecha" size="11" readonly="readonly" value="<?php if ($arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaTerminoConstruccion'))?>" /> <em>*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_termino_construccion">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MONTANTE</td>
        <td>
            <select name="montante" id="montante" style="width:80px;">
                <option value="">Seleccione</option>
                <?php
                $selMontanteS = ( $arrObjEdificio[0]->__get('_montante') == 'S' ) ? "selected" : "";
                $selMontanteN = ( $arrObjEdificio[0]->__get('_montante') == 'N' ) ? "selected" : "";
                ?>
                <option value="S" <?php echo $selMontanteS?>>Si</option>
                <option value="N" <?php echo $selMontanteN?>>No</option>
            </select> <em class="c_required" style="display:none;">*</em> &nbsp; 
            Fecha: <input type="text" size="11" name="montante_fecha" id="montante_fecha" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_montanteFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_montanteFecha'))?>" readonly="readonly" /> <em class="c_required" style="display:none;">*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="montante_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DUCTERIA INTERNA</td>
        <td>
            <select name="ducteria_interna" id="ducteria_interna" style="width:80px;">
                <option value="">Seleccione</option>
                <?php
                $selDucteriaS = ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'S' ) ? "selected" : "";
                $selDucteriaN = ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'N' ) ? "selected" : "";
                ?>
                <option value="S" <?php echo $selDucteriaS?>>Si</option>
                <option value="N" <?php echo $selDucteriaN?>>No</option>
            </select> <em class="c_required" style="display:none;">*</em> &nbsp; 
            Fecha: <input type="text" size="11" name="ducteria_interna_fecha" id="ducteria_interna_fecha" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_ducteriaInternaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_ducteriaInternaFecha'))?>" readonly="readonly" /> <em class="c_required" style="display:none;">*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="ducteria_interna_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PUNTO DE ENERGIA</td>
        <td>
            <select name="punto_energia" id="punto_energia" style="width:80px;">
                <option value="">Seleccione</option>
                <?php
                $selPuntoEnergiaS = ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'S' ) ? "selected" : "";
                $selPuntoEnergiaN = ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'N' ) ? "selected" : "";
                ?>
                <option value="S" <?php echo $selPuntoEnergiaS?>>Si</option>
                <option value="N" <?php echo $selPuntoEnergiaN?>>No</option>
            </select> &nbsp; 
            Fecha: <input type="text" size="11" name="punto_energia_fecha" id="punto_energia_fecha" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_puntoEnergiaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_puntoEnergiaFecha'))?>" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="punto_energia_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FACILIDADES DEL POSTE</td>
        <td>
            <select name="idfacilidad_poste" id="idfacilidad_poste">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjFacilidadPoste) ) {
                foreach ( $arrObjFacilidadPoste as $objFacilidadPoste ) {
                ?>
                <option value="<?php echo $objFacilidadPoste->__get('_idFacilidadPoste')?>" <?php if ($objFacilidadPoste->__get('_idFacilidadPoste') == $arrObjEdificio[0]->__get('_idFacilidadPoste')) echo 'selected'; ?> ><?php echo $objFacilidadPoste->__get('_desFacilidadPoste')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RESPONSABLES CAMPO</td>
        <td>
            <select name="idresponsable_campo" id="idresponsable_campo">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjResponsableCampo) ) {
                foreach ( $arrObjResponsableCampo as $objResponsableCampo ) {
                ?>
                <option value="<?php echo $objResponsableCampo->__get('_idResponsableCampo')?>" <?php if ($objResponsableCampo->__get('_idResponsableCampo') == $arrObjEdificio[0]->__get('_idResponsableCampo')) echo 'selected'; ?> ><?php echo $objResponsableCampo->__get('_desResponsableCampo')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VALIDADO POR CALL</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
            <select name="validacion_call" id="validacion_call">
                <option value="">Seleccione</option>
                <option value="1" <?php if( $arrObjEdificio[0]->__get('_validacionCall') == '1' ) echo 'selected'; ?>>Si</option>
                <option value="0" <?php if( $arrObjEdificio[0]->__get('_validacionCall') == '0' ) echo 'selected'; ?>>No</option>
            </select>
            <?php 
            } else {
                if ( $arrObjEdificio[0]->__get('_validacionCall') == '1' ) {
                    echo 'Si';
                } elseif ( $arrObjEdificio[0]->__get('_validacionCall') == '0' ) {
                    echo 'No';
                } else {
                    echo '';
                }
            ?>
                <input type="hidden" name="validacion_call" id="validacion_call" value="<?php echo $arrObjEdificio[0]->__get('_validacionCall')?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TRATAMIENTO CALL</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="fecha_tratamiento_call" id="fecha_tratamiento_call" class="clsTxtFecha" readonly="readonly" value="<?php if ($arrObjEdificio[0]->__get('_fechaTratamientoCall') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaTratamientoCall'))?>" />
                <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_tratamiento_call">Limpiar</a>
            <?php 
            } else {
                if ($arrObjEdificio[0]->__get('_fechaTratamientoCall') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaTratamientoCall'));
            ?>
                <input type="hidden" name="fecha_tratamiento_call" id="fecha_tratamiento_call" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_fechaTratamientoCall') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaTratamientoCall'))?>" />
            <?php 
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
        <td>
            <input type="text" size="11" name="fecha_seguimiento" id="fecha_seguimiento" class="clsTxtFecha" value="<?php if ($arrObjEdificio[0]->__get('_fechaSeguimiento') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaSeguimiento'))?>" readonly="readonly" /> <em>*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_seguimiento">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
        <td>
            <textarea name="observacion" id="observacion" style="width:95%;height:100px;"><?php echo $arrObjEdificio[0]->__get('_observacion')?></textarea>
        </td>
    </tr>
    </tbody>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody><tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px; float:left;">
                <input type="hidden" name="idedificio" id="idedificio" value="<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>" />
                <input type="button" value="Grabar" name="btn_grabar" title="Grabar" id="btn_grabar" class="submit" onclick="javascript: updateDatosEdificio();" />&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" class="submit" onclick="javascript: history.back();" />
            </div>
            <div style="margin: 10px 0 0 20px; display: none; float:left;" id="msg">
                <img alt="" src="../../img/loading.gif"> <b>Grabando datos.</b>
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
</div>

<?php 
} else {
?>
    
    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tr>
        <td colspan="6" style="padding:20px;">No existe o no se ha encontrado el Edificio</td>
    </tr>
    </table>
    
<?php 
}
?>

</body>
</html>
