<style>
em {
    color: #ff0000;
}
.limpiar_fecha {
    color: #0000FF !important;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}


table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

.da_editar {
    width: 90%;
}

</style>

<script type="text/javascript">
var idedificio = <?php echo $arrObjEdificio[0]->__get('_idEdificio')?>;
var x = <?php echo ($arrObjEdificio[0]->__get('_x') != '') ? $arrObjEdificio[0]->__get('_x') : '""'?>;
var y = <?php echo ($arrObjEdificio[0]->__get('_y') != '') ? $arrObjEdificio[0]->__get('_y') : '""'?>;
$(document).ready(function () {
    $("#tabsDetalle").tabs();
    
    $("#tabMapaEdificio").click(function() {
        $("#tdMapaEdificio").empty();
        
        $("#tdMapaEdificio").html('<iframe id="ifrMapaPirata" name="ifrMapaPirata" src="modulos/maps/vistas/v_bandeja.control.edificio.mapa.php?idedificio=' + idedificio + '" width="100%" height="400"></iframe>');

    });
});
</script>

<div id="content_pirata_detalle">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td valign="top" style="padding-top: 5px; padding-left: 5px;">

            <div id="tabsDetalle" style="width:99%;">
                <ul>
                    <li><a id="tabDetalleEdificio" href="#cont_edificio_detalle" style="color:#1D87BF; font-weight: 600">Datos del Edificio</a></li>
                    <li><a id="tabMapaEdificio" href="#cont_edificio_mapa" style="color:#1D87BF; font-weight: 600">Mapa de Ubicaci&oacute;n</a></li>
                </ul>

                <div id="cont_edificio_detalle" style="margin: 0;">

                    <div id="divDetalle">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr style="text-align: right;">
                            <td colspan="4">
                                <a class="link_movs" href="javascript:verMovimientosEdificio('<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>')">
                                <img width="130" height="26" title="Ver movimientos" alt="Ver movimientos" src="img/btn_movimientos.png">
                                </a>
                            </td>
                        </tr>
                        </table>
                        <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
                        <tbody>
                        <tr>
                            <td class="da_titulo_gest" colspan="4">INFORMACI&Oacute;N EDIFICIO</td>
                        </tr>
                        <tr style="text-align: left;">
                            <td width="18%" class="da_subtitulo">ITEM</td>
                            <td width="32%" class="da_import"><?php echo $arrObjEdificio[0]->__get('_item')?></td>
                            <td width="18%" class="da_subtitulo">GESTI&Oacute;N DE OBRAS</td>
                            <td width="32%" class="da_import"><?php echo $gestionObra?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">EMPRESA</td>
                            <td class="da_import" colspan="3"><?php echo $arrObjEdificio[0]->__get('_descEmpresa')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">C&Oacute;DIGO PROYECTO WEB</td>
                            <td class="da_import2"><?php echo strtoupper($arrObjEdificio[0]->__get('_codigoProyectoWeb'))?></td>
                            <td class="da_subtitulo">ESTADO</td>
                            <td class="da_import"><?php echo $estadoEdificio?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">REGI&Oacute;N</td>
                            <td class="da_import">                                <?php 
                                if (!empty($arrRegion)) {
                                foreach ($arrRegion as $region) {
                                    if ($region['idregion'] == $arrObjEdificio[0]->__get('_idRegion')) {
                                        echo $region['nomregion'] . ' - ' . $region['detregion'];
                                        break; 
                                    } 
                                }
                                }
                                ?>
                            </td>
                            <td class="da_subtitulo">FUENTE DE IDENTIFICACI&Oacute;N</td>
                            <td class="da_import"><?php echo $fuenteIdentificacion ?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">ZONAL</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_zonal')?></td>
                            <td class="da_subtitulo">URA</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_ura')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">FECHA CABLEADO</td>
                            <td>
                            <?php 
                            if ( $arrObjEdificio[0]->__get('_fechaCableado') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaCableado') != '' ) {
                                echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaCableado'))); 
                            } else {
                                echo '';    
                            }
                            ?>
                            </td>
                            <td class="da_subtitulo">ARMARIO</td>
                            <td><?php echo strtoupper($arrObjEdificio[0]->__get('_armario'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
                            <td>
                            <?php 
                            if ( $arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '' ) {
                                echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaRegistroProyecto'))); 
                            } else {
                                echo '';    
                            }
                            ?>
                            </td>
                            <td class="da_subtitulo">SECTOR</td>
                            <td><?php echo strtoupper($arrObjEdificio[0]->__get('_sector'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">MANZANA (TDP)</td>
                            <td><?php echo strtoupper($arrObjEdificio[0]->__get('_mzaTdp'))?></td>
                            <td class="da_subtitulo">TIPO V&Iacute;A</td>
                            <td><?php echo $tipoVia ?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
                            <td colspan="3"><?php echo strtoupper($arrObjEdificio[0]->__get('_direccionObra'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">N&Uacute;MERO</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_numero')?></td>
                            <td class="da_subtitulo">MANZANA</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_mza')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">LOTE</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_lote')?></td>
                            <td class="da_subtitulo">TIPO CCHH</td>
                            <td><?php echo $tipoCchh?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">CCHH</td>
                            <td colspan="3"><?php echo strtoupper($arrObjEdificio[0]->__get('_cchh'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">DEPARTAMENTO</td>
                            <td><?php echo $departamento?></td>
                            <td class="da_subtitulo">PROVINCIA</td>
                            <td><?php echo $provincia?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">DISTRITO</td>
                            <td><?php echo $distrito?></td>
                            <td class="da_subtitulo">LOCALIDAD</td>
                            <td><?php echo $localidad?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">SEGMENTO</td>
                            <td><?php echo $segmento?></td>
                            <td class="da_subtitulo">TIPO PROYECTO</td>
                            <td><?php echo $tipoProyecto?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">NOMBRE PROYECTO</td>
                            <td colspan="3"><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreProyecto'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
                            <td><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreConstructora'))?></td>
                            <td class="da_subtitulo">RUC CONSTRUCTORA</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_rucConstructora')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">NIVEL SOCIOECON&Oacute;MICO</td>
                            <td><?php echo $nivelSocioeconomico?></td>
                            <td class="da_subtitulo">PERSONA CONTACTO</td>
                            <td><?php echo strtoupper($arrObjEdificio[0]->__get('_personaContacto'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">ZONA COMPETENCIA</td>
                            <td><?php echo $zonaCompetencia?></td>
                            <td class="da_subtitulo">GESTI&Oacute;N COMPETENCIA</td>
                            <td><?php echo $gestionCompetencia?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">COBERTURA MOVIL 2G</td>
                            <td><?php echo $coberturaMovil2g?></td>
                            <td class="da_subtitulo">COBERTURA MOVIL 3G</td>
                            <td><?php echo $coberturaMovil3g?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">P&Aacute;GINA WEB</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_paginaWeb')?></td>
                            <td class="da_subtitulo">EMAIL</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_email')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
                            <td valign="top"><?php echo $arrObjEdificio[0]->__get('_nroBlocks')?></td>
                            <td colspan="2" valign="top">
                                <div id="divBlock">
                                    <div style="width:180px;">
                                        <span style="float:left; width:90px;">Nro. pisos</span>
                                        <span style="float:left; width:90px;">Nro. dptos</span>
                                    </div>
                                    <div style="width:180px;">
                                        <span style="float:left; width:90px; padding:2px 0;"><?php echo trim($arrObjEdificio[0]->__get('_nroPisos1'))?></span>
                                        <span style="float:left; width:90px; padding:2px 0;"><?php echo trim($arrObjEdificio[0]->__get('_nroDptos1'))?></span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">VIVEN</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_viven') == '1' ) {
                                    echo 'Si'; 
                                } if ( $arrObjEdificio[0]->__get('_viven') == '0' ) {
                                    echo 'No'; 
                                } else {
                                    echo '';
                                }
                                ?>
                            </td>
                            <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_nroDptosHabitados')?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
                            <td><?php echo $tipoInfraestructura?></td>
                            <td class="da_subtitulo">MEGAPROYECTO</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_megaproyecto') == '1' ) {
                                    echo 'Si';
                                } elseif ( $arrObjEdificio[0]->__get('_megaproyecto') == '0' ) {
                                    echo 'No';
                                } else {
                                    echo '';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">% AVANCE</td>
                            <td><?php echo $arrObjEdificio[0]->__get('_avance')?></td>
                            <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N / QUINCENA</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '' ) {
                                    echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                                } else {
                                    echo '';    
                                }
                                ?>
                                <?php 
                                $quincena = '';
                                if ($arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00' &&
                                        $arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '') {
                                    $diaQuincena = date('d', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                                    $mesQuincena = date('m', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                                    $anoQuincena = date('y', strtotime($arrObjEdificio[0]->__get('_fechaTerminoConstruccion')));
                                    $dia = '1Q';
                                    if ( (int)$diaQuincena > 15 ) {
                                        $dia = '2Q';
                                    }
                                    $quincena = $dia . '-' . $mesQuincena . '-' . $anoQuincena;
                                }
                                echo ' / <span style="color:#ff0000;">(' . $quincena . ')</span>';
                                ?>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">MONTANTE</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_montante') == 'S' ) {
                                    echo 'Si';
                                } elseif ( $arrObjEdificio[0]->__get('_montante') == 'N' ) {
                                    echo 'No';
                                } else {
                                    echo '-';
                                }
                                ?> &nbsp; 
                                Fecha: <?php if ($arrObjEdificio[0]->__get('_montanteFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_montanteFecha'))?>
                            </td>
                            <td class="da_subtitulo">DUCTERIA INTERNA</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'S' ) {
                                    echo 'Si';
                                } elseif ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'N' ) {
                                    echo 'No';
                                } else {
                                    echo '-';
                                }
                                ?> &nbsp; 
                                Fecha: <?php if ($arrObjEdificio[0]->__get('_ducteriaInternaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_ducteriaInternaFecha'))?>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">PUNTO DE ENERGIA</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'S' ) {
                                    echo 'Si';
                                } elseif ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'N' ) {
                                    echo 'No';
                                } else {
                                    echo '-';
                                }
                                ?> &nbsp; 
                                Fecha: <?php if ($arrObjEdificio[0]->__get('_puntoEnergiaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_puntoEnergiaFecha'))?>
                            </td>
                            <td class="da_subtitulo"></td>
                            <td></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">FACILIDADES DEL POSTE</td>
                            <td><?php echo $facilidadPoste?></td>
                            <td class="da_subtitulo">RESPONSABLES CAMPO</td>
                            <td><?php echo $responsableCampo?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">VALIDADO POR CALL</td>
                            <td>
                                <?php 
                                if ( $arrObjEdificio[0]->__get('_validacionCall') == '1' ) {
                                    echo 'Si';
                                } elseif ( $arrObjEdificio[0]->__get('_validacionCall') == '0' ) {
                                    echo 'No';
                                } else {
                                    echo '';
                                }
                                ?>
                            </td>
                            <td class="da_subtitulo">FECHA TRATAMIENTO CALL</td>
                            <td>
                            <?php 
                            if ( $arrObjEdificio[0]->__get('_fechaTratamientoCall') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaTratamientoCall') != '' ) {
                                echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaTratamientoCall')));
                            } else {
                                echo '';    
                            }
                            ?>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
                            <td colspan="3">
                            <?php 
                            if ( $arrObjEdificio[0]->__get('_fechaSeguimiento') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaSeguimiento') != '' ) {
                                echo date('d/m/Y', strtotime($arrObjEdificio[0]->__get('_fechaSeguimiento')));
                            } else {
                                echo '';    
                            }
                            ?>
                            </td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
                            <td colspan="3"><?php echo nl2br($arrObjEdificio[0]->__get('_observacion'))?></td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo">USUARIO INSERT | FECHA INSERT</td>
                            <td>
                            <?php 
                            echo $arrObjEdificio[0]->__get('_loginInsert') . ' | ';
                            if ( $arrObjEdificio[0]->__get('_fechaInsert') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaInsert') != '' ) {
                                echo date('d/m/Y H:i', strtotime($arrObjEdificio[0]->__get('_fechaInsert')));
                            } else {
                                echo '';
                            }
                            ?>
                            </td>
                            <td class="da_subtitulo">USUARIO UPDATE | FECHA UPDATE</td>
                            <td>
                            <?php 
                            echo $arrObjEdificio[0]->__get('_loginUpdate') . ' | ';
                            if ( $arrObjEdificio[0]->__get('_fechaUpdate') != '0000-00-00' && $arrObjEdificio[0]->__get('_fechaUpdate') != '' ) {
                                echo date('d/m/Y H:i', strtotime($arrObjEdificio[0]->__get('_fechaUpdate')));
                            } else {
                                echo '';
                            }
                            ?>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                        
                    </div>
                    
                    <div id="divMovimientos" style="display: none;">

                    </div>
                    
                </div>
                
                
                <div id="cont_edificio_mapa">
                    <table id="tbl_detalle_mapa" class="tb_detalle_actu" width="100%">
                    <tbody>
                    <tr>
                        <td class="da_titulo_gest" colspan="6">MAPA DE UBICACI&Oacute;N</td>
                    </tr>
                    <tr>
                        <td id="tdMapaEdificio">
                            <!--Dibuja el mapa con el Edificio-->

                        </td>
                    </tr>
                    </table>
                </div>
                
            </div>
        </td>
    </tr>
    </table>
</div>