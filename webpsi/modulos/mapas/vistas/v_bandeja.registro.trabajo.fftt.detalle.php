<style type="text/css">
em {
	color: #FF0000;
	margin-left: 4px;
}
#tbl_detalle td {
    padding: 3px;
}

#tb_resultado .tr_selected .celdaX {
	background-color: #C4F4FF;
}
table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_subtitulo_bold{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 11px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}
</style>

<script>
$(document).ready(function() {
    $("#fecha_fin_real_solucion").datetimepicker({
        yearRange:'-1:+1',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy'
    });
});
</script>


<div id="content_pirata_fotos">

	<?php
	if( !empty($registro_trabajo) ) {
            $readonly_comentarios = 'readonly="readonly"';
            
            /*$elemento = '';
            if($registro_trabajo[0]['codplantaptr'] == 'B'){ //STB/ADSL

                if( $registro_trabajo[0]['tipored'] == 'D' ) {
                    if( $registro_trabajo[0]['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                        $elemento = $registro_trabajo[0]['mdf'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '19' ) { //Cable Alimentador
                        $elemento = $registro_trabajo[0]['ccable'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '3' ) { //terminal
                        $elemento = $registro_trabajo[0]['terminal'];
                    }
                }else if( $registro_trabajo[0]['tipored'] == 'F' ) {
                    if( $registro_trabajo[0]['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                        $elemento = $registro_trabajo[0]['mdf'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '19' ) { //Cable Alimentador
                        $elemento = $registro_trabajo[0]['parpri'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '8' ) { //armario
                        $elemento = $registro_trabajo[0]['carmario'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '23' ) { //cable secundario
                        $elemento = $registro_trabajo[0]['cbloque'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '3' ) { //terminal
                        $elemento = $registro_trabajo[0]['terminal'];
                    }
                }

            }else if($registro_trabajo[0]['codplantaptr'] == 'C'){ //CATV

                if( $registro_trabajo[0]['codelementoptr'] == '28' ) { //Nodo //Agregado 12/12/2011
                    $elemento = $registro_trabajo[0]['mdf'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '11' ) { //troba
                    $elemento = $registro_trabajo[0]['carmario'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '2' ) { //amplificador
                    $elemento = $registro_trabajo[0]['cbloque'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '1' ) { //tap
                    $elemento = $registro_trabajo[0]['terminal'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '20' ) { //borne
                    $elemento = $registro_trabajo[0]['borne'];
                }
            }*/
            
            $elemento = obtenerElementoRegistroTrabajoFFTT($registro_trabajo[0]);
            
            
	?>

	<div id="cont_actu_detalle">
		<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
		<tbody>
		<tr>
                    <td class="da_titulo_gest" colspan="4">DATOS DEL TRABAJO: RTP-<?php echo $registro_trabajo[0]['id_registro_trabajo_planta'] ?></td>
		</tr>
                <tr style="text-align: left;">
                    <td width="25%" class="da_subtitulo">CASUISTICA</td>
                    <td width="75%" colspan="3" style="color: #ff0000; font-size:12px;font-weight:bold;"><?php echo strtoupper($registro_trabajo[0]['casuistica'])?></td>
		</tr>
		<tr style="text-align: left;">
                    <td width="25%" class="da_subtitulo">ZONAL</td>
                    <td width="25%"><?php echo strtoupper($registro_trabajo[0]['zonal'])?></td>
                    <td width="25%" class="da_subtitulo">JEFATURA</td>
                    <td width="25%"><?php echo strtoupper($registro_trabajo[0]['jefatura_desc'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">NEGOCIO</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['negocio'])?></td>
                    <td class="da_subtitulo">MDF</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['mdf'])?></td>
		</tr>
                <?php
                if($registro_trabajo[0]['codplantaptr'] == 'B'){ //STB/ADSL
                    if( $registro_trabajo[0]['tipored'] == 'D' ) {
                ?>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">CABLE ALIMENTADOR</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['ccable'])?></td>
                        <td class="da_subtitulo">TERMINAL</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['terminal'])?></td>
                    </tr>
                <?php
                    }
                    else if( $registro_trabajo[0]['tipored'] == 'F' ) {
                ?>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">CABLE ALIMENTADOR</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['parpri'])?></td>
                        <td class="da_subtitulo">ARMARIO</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['carmario'])?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">CABLE SECUNDARIO</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['cbloque'])?></td>
                        <td class="da_subtitulo">TERMINAL</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['terminal'])?></td>
                    </tr>
                <?php
                    }
                    
                }else if($registro_trabajo[0]['codplantaptr'] == 'C'){ //CATV
                ?>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">TROBA</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['carmario'])?></td>
                        <td class="da_subtitulo">AMPLIFICADOR</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['cbloque'])?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">TAP</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['terminal'])?></td>
                        <td class="da_subtitulo">BORNE</td>
                        <td><?php echo strtoupper($registro_trabajo[0]['borne'])?></td>
                    </tr>
                    
                
                <?php
                }
                ?>
                
                <tr style="text-align: left;">
                    <td class="da_subtitulo_bold">ELEMENTO DE RED</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['descripcion'])?></td>
                    <td class="da_subtitulo_bold">ELEMENTO</td>
                    <td><?php echo strtoupper($elemento)?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">FECHA REGISTRO</td>
                    <td><?php echo obtenerFecha($registro_trabajo[0]['fecha_insert'])?></td>
                    <td class="da_subtitulo">FECHA ULTIMO MOVIMIENTO</td>
                    <td><?php echo obtenerFecha($registro_trabajo[0]['fecha_update'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">FECHA INICIO TRABAJO</td>
                    <td><?php echo obtenerFecha($registro_trabajo[0]['fecha_inicio_trabajo'])?></td>
                    <td class="da_subtitulo">FECHA FIN TRABAJO</td>
                    <td><?php echo obtenerFecha($registro_trabajo[0]['fecha_fin_trabajo'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">ESTADO ACTUAL</td>
                    <td colspan="3" style="color: #ff0000; font-size:12px;font-weight:bold;"><?php echo strtoupper($registro_trabajo[0]['estado'])?></td>
		</tr>
                <?php
                if($registro_trabajo[0]['estado'] == 'LIQUIDADO') {
                ?>
                <tr style="text-align: left;">
                    <td class="da_subtitulo_bold">FECHA FIN REAL SOLUCION</td>
                    <td colspan="3"><?php echo strtoupper($registro_trabajo[0]['fecha_fin_real_solucion'])?></td>
		</tr>
                <?
                }
                ?>
                <tr style="text-align: left;">
                    <td class="da_subtitulo" valign="top">OBSERVACIONES REGISTRO</td>
                    <td colspan="3" style="color: #ff0000;">
                        <textarea name="observaciones" id="observaciones" readonly="readonly" rows="3" style="width:90%;"><?php echo strtoupper($registro_trabajo[0]['observaciones'])?></textarea>
                    </td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo" valign="top">OBSERVACIONES FINAL</td>
                    <td colspan="3" style="color: #ff0000;">
                        <textarea name="comentarios" id="comentarios" rows="3" cols="60" <?php echo $readonly_comentarios?> style="text-transform: uppercase; width:90%;"><?php echo strtoupper($registro_trabajo[0]['comentarios'])?></textarea>
                        <input type="hidden" id="id_registro_trabajo_planta" value="<?php echo $registro_trabajo[0]['id_registro_trabajo_planta']?>">
                    </td>
		</tr>
                </tbody>
		</table>
                <br />
            
                <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
		<tbody>
                <tr>
                    <td colspan="5" class="da_titulo_gest">MOVIMIENTO DE TRABAJOS</td>
                </tr>
		<tr>
                    <td class="da_subtitulo" width="2%">#</td>
                    <td class="da_subtitulo" width="15%">ESTADO</td>
                    <td class="da_subtitulo" width="60%">OBSERVACIONES</td>
                    <td class="da_subtitulo" width="10%">USUARIO REGISTRO MOV.</td>
                    <td class="da_subtitulo" width="13%">FECHA MOV.</td>
		</tr>
                <?php
                if( !empty($arr_obj_registro_trabajo_mov) ) {
                $i = 1;
                foreach( $arr_obj_registro_trabajo_mov as $obj_registro_trabajo_mov ) {
                ?>
                    <tr style="text-align: left;">
                        <td align="center"><?php echo $i?></td>
                        <td align="center">&nbsp;<?php echo strtoupper($obj_registro_trabajo_mov->__get('estado'))?></td>
                        <td>&nbsp;<?php echo strtoupper($obj_registro_trabajo_mov->__get('observaciones'))?></td>
                        <td align="center">&nbsp;<?php echo $obj_registro_trabajo_mov->__get('login')?></td>
                        <td align="center">&nbsp;<?php echo obtenerFecha($obj_registro_trabajo_mov->__get('fecha_insert'))?></td>
                    </tr>
                <?php
                $i++;
                }
                }
                else {
                ?>
                    <tr>
                        <td colspan="5" style="padding: 10px;">No se encontro ningun registro.</td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
                
                
	</div>
        <?php
        }
        ?>

</div>

