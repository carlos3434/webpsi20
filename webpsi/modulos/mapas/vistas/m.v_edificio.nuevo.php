<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<!-- <script type="text/javascript" src="../../js/jquery/jquery.js"></script> -->
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#fecha_cableado, #fecha_registro_proyecto, #montante_fecha, #ducteria_interna_fecha, #punto_energia_fecha, #fecha_tratamiento_call, #fecha_seguimiento, #fecha_termino_construccion").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy'
    });
    
    $(".limpiar_fecha").click(function() {
        var campo = $(this).attr('c_fecha');
        $("#" + campo).attr('value', '');
    });

    $("#btn_cerrar").click(function() {
        var x = $("#x").val();
        var y = $("#y").val();
        
        window.top.location = '../../modulos/maps/m.buscomp.add.php?action=add&x=' + x + '&y=' + y;
    });

    $("#idgestion_obra").change(function() {
        if ( $("#idgestion_obra").val() == '2' ) {
            $(".c_required").removeAttr("style");
        } else {
        	$(".c_required").attr("style", "display:none;");
        }
    });
    
});
</script>

<style>
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 2px;
}

table.tb_detalle_edi input {
    width: 95%;
}
table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Agregar Componente</td>
</tr>
</table>


<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td>X: <input type="text" name="x" id="x" value="<?php echo $_REQUEST['x']?>" readonly="readonly" /><td>
</tr>
<tr>
    <td>Y: <input type="text" name="y" id="y" value="<?php echo $_REQUEST['y']?>" readonly="readonly" /><td>
</tr>
</table>


<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">

    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N EDIFICIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="30%" class="da_subtitulo">ITEM</td>
        <td width="70%" class="da_import">(AUTO)</td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N DE OBRAS</td>
        <td class="da_import">
        <?php 
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
        ?>
            <select name="idgestion_obra" id="idgestion_obra">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjGestionObra) ) {
                foreach ( $arrObjGestionObra as $objGestionObra ) {
                ?>
                <option value="<?php echo $objGestionObra->__get('_idGestionObra')?>" ><?php echo $objGestionObra->__get('_desGestionObra')?></option>
                <?php 
                }
                }
                ?>
            </select>
        <?php
        } else {
        ?>
            <input type="hidden" name="idgestion_obra" id="idgestion_obra" value="<?php echo $idGestionObra?>" />
        <?php
        }
        ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMPRESA</td>
        <td class="da_import">
            <select name="idempresa" id="idempresa">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
                foreach ( $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa ) {
                ?>
                <option value="<?php echo $objEmpresa->__get('_idEmpresa')?>" <?php if ( $_SESSION['USUARIO']->__get('_idEmpresa') == $objEmpresa->__get('_idEmpresa') ) echo 'selected';?> ><?php echo $objEmpresa->__get('_descEmpresa')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">C&Oacute;DIGO PROYECTO WEB</td>
        <td class="da_import2">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="codigo_proyecto_web" id="codigo_proyecto_web" value="" />
            <?php 
            } else {
            ?>
                <input type="hidden" name="codigo_proyecto_web" id="codigo_proyecto_web" value="" />
            <?php 
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td class="da_import">
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <select name="estado" id="estado">
                    <option value="">Seleccione</option>
                    <?php 
                    if ( !empty($arrObjEdificioEstado) ) {
                    foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
                    ?>
                    <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado')?>" ><?php echo $objEdificioEstado->__get('_desEstado')?></option>
                    <?php
                    }
                    }
                    ?>
                </select>
            <?php
            } else {
            ?>
                <input type="hidden" name="estado" id="estado" value="<?php echo $codEstado?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">REGI&Oacute;N</td>
        <td class="da_import">
            <select name="cmb_region" id="cmb_region" onchange="listarZonalesPorRegionUsuario();">
                <option value="">Seleccione</option>
                <?php 
                if (!empty($arrRegion)) {
                foreach ($arrRegion as $region) {
                ?>
                    <option value="<?php echo $region['idregion'] ?>" ><?php echo $region['nomregion'] . ' - ' . $region['detregion'] ?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FUENTE DE IDENTIFICACI&Oacute;N</td>
        <td class="da_import">
            <select name="idfuente_identificacion" id="idfuente_identificacion">
                <option value="">Seleccione</option>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONAL</td>
        <td>
            <select name="cmb_zonal" id="cmb_zonal" onchange="ListarMdfsZonalUsuario();">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($_SESSION['USUARIO_ZONAL']) ) {
                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) { 
                ?>
                    <option value="<?php echo $objZonal->__get('_abvZonal') ?>" ><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                <?php 
                } 
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">URA</td>
        <td>
            <select name="cmb_ura" id="cmb_ura">
                <option value="">Seleccione</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA CABLEADO</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="fecha_cableado" size="11" id="fecha_cableado" class="clsTxtFecha" value="" readonly="readonly" />
                <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_cableado">Limpiar</a>
            <?php 
            } else {
            ?>
                <input type="hidden" name="fecha_cableado" size="11" id="fecha_cableado" value="" readonly="readonly" />
            <?php 
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ARMARIO</td>
        <td><input type="text" name="armario" id="armario" value="" /></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
        <td>
            <input type="text" name="fecha_registro_proyecto" size="11" id="fecha_registro_proyecto" class="clsTxtFecha" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_registro_proyecto">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SECTOR</td>
        <td><input type="text" name="sector" id="sector" value="" /> <em>*</em></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA (TDP)</td>
        <td>
            <input type="text" name="mza_tdp" id="mza_tdp" value="" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO V&Iacute;A</td>
        <td>
            <select name="idtipo_via" id="idtipo_via">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoVia) ) {
                foreach ( $arrObjTipoVia as $objTipoVia ) {
                ?>
                <option value="<?php echo $objTipoVia->__get('_idTipoVia')?>" ><?php echo $objTipoVia->__get('_desTipoVia')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
        <td>
            <input type="text" name="direccion_obra" id="direccion_obra" class="da_editar" value="" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">N&Uacute;MERO</td>
        <td>
            <input type="text" name="numero" id="numero" value="" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA</td>
        <td>
            <input type="text" name="mza" id="mza" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOTE</td>
        <td>
            <input type="text" name="lote" id="lote" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO CCHH</td>
        <td>
            <select name="idtipo_cchh" id="idtipo_cchh">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoCchh) ) {
                foreach ( $arrObjTipoCchh as $objTipoCchh ) {
                ?>
                <option value="<?php echo $objTipoCchh->__get('_idTipoCchh')?>" ><?php echo $objTipoCchh->__get('_desTipoCchh')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CCHH</td>
        <td>
            <input type="text" name="cchh" id="cchh" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DEPARTAMENTO</td>
        <td>
            <select name="cmb_departamento" id="cmb_departamento" onchange="ListarProvinciasUbigeoEdi();">
                <option value="">Seleccione</option>
                <?php
                if ( !empty($arrObjDepartamento) ) {
                foreach ( $arrObjDepartamento as $objDepartamento ) {
                ?>
                <option value="<?php echo $objDepartamento->__get('_codDpto')?>" ><?php echo $objDepartamento->__get('_nombre')?></option>
                <?php
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PROVINCIA</td>
        <td>
            <select name="cmb_provincia" id="cmb_provincia" onchange="ListarDistritosUbigeoEdi();">
                <option value="">Seleccione</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DISTRITO</td>
        <td>
            <select name="cmb_distrito" id="cmb_distrito" onchange="ListarLocalidadesUbigeo();">
                <option value="">Seleccione</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOCALIDAD</td>
        <td>
            <select name="cmb_localidad" id="cmb_localidad">
                <option value="">Seleccione</option>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">SEGMENTO</td>
        <td class="da_import">
            <select name="cmb_segmento" id="cmb_segmento" onchange="ListarTipoProyectoPorSegmento();">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjSegmento) ) {
                foreach ( $arrObjSegmento as $objSegmento ) {
                ?>
                <option value="<?php echo $objSegmento->__get('_idSegmento')?>" ><?php echo $objSegmento->__get('_desSegmento')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO PROYECTO</td>
        <td class="da_import">
            <select name="cmb_tipo_proyecto" id="cmb_tipo_proyecto">
                <option value="">Seleccione</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE PROYECTO</td>
        <td>
            <input type="text" name="nombre_proyecto" id="nombre_proyecto" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
        <td>
            <input type="text" name="nombre_constructora" id="nombre_constructora" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RUC CONSTRUCTORA</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="ruc_constructora" id="ruc_constructora" class="da_editar" onkeypress="return soloNumeros(event)" value="" />
            <?php 
            } else {
            ?>
                <input type="hidden" name="ruc_constructora" id="ruc_constructora" class="da_editar" onkeypress="return soloNumeros(event)" value="" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NIVEL SOCIOECON&Oacute;MICO</td>
        <td>
            <select name="cmb_nivel_socioeconomico" id="cmb_nivel_socioeconomico">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjNivelSocioeconomico) ) {
                foreach ( $arrObjNivelSocioeconomico as $objNivelSocioeconomico ) {
                ?>
                <option value="<?php echo $objNivelSocioeconomico->__get('_idNivelSocioeconomico')?>" ><?php echo $objNivelSocioeconomico->__get('_desNivelSocioeconomico')?></option>
                <?php 
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PERSONA CONTACTO</td>
        <td>
            <input type="text" name="persona_contacto" id="persona_contacto" class="da_editar" value="" /> <em class="c_required" style="display:none;">*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONA COMPETENCIA</td>
        <td class="da_import">
            <select name="cmb_zona_competencia" id="cmb_zona_competencia">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjZonaCompetencia) ) {
                foreach ( $arrObjZonaCompetencia as $objZonaCompetencia ) {
                ?>
                <option value="<?php echo $objZonaCompetencia->__get('_idZonaCompetencia')?>" ><?php echo $objZonaCompetencia->__get('_desZonaCompetencia')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">GESTI&Oacute;N COMPETENCIA</td>
        <td class="da_import">
            <select name="cmb_gestion_competencia" id="cmb_gestion_competencia">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjZonaCompetencia) ) {
                foreach ( $arrObjGestionCompetencia as $objGestionCompetencia ) {
                ?>
                <option value="<?php echo $objGestionCompetencia->__get('_idGestionCompetencia')?>" ><?php echo $objGestionCompetencia->__get('_desGestionCompetencia')?></option>
                <?php 
                }
                }
                ?>
            </select> <em class="c_required" style="display:none;">*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 2G</td>
        <td class="da_import">
            <select name="cmb_cobertura_movil_2g" id="cmb_cobertura_movil_2g">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjCoberturaMovil2G) ) {
                foreach ( $arrObjCoberturaMovil2G as $objCoberturaMovil2G ) {
                ?>
                <option value="<?php echo $objCoberturaMovil2G->__get('_idCoberturaMovil2G')?>" ><?php echo $objCoberturaMovil2G->__get('_desCoberturaMovil2G')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">COBERTURA MOVIL 3G</td>
        <td class="da_import">
            <select name="cmb_cobertura_movil_3g" id="cmb_cobertura_movil_3g">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjCoberturaMovil3G) ) {
                foreach ( $arrObjCoberturaMovil3G as $objCoberturaMovil3G ) {
                ?>
                <option value="<?php echo $objCoberturaMovil3G->__get('_idCoberturaMovil3G')?>" ><?php echo $objCoberturaMovil3G->__get('_desCoberturaMovil3G')?></option>
                <?php 
                }
                }
                ?>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">P&Aacute;GINA WEB</td>
        <td>
            <input type="text" name="pagina_web" id="pagina_web" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">EMAIL</td>
        <td>
            <input type="text" name="email" id="email" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
        <td valign="top">
            <input type="text" size="5" name="nro_blocks" id="nro_blocks" onkeypress="return soloNumeros(event)" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td colspan="2" valign="top">
            <div id="divBlock">
                <div style="width:180px;">
                    <span style="float:left; width:90px;">Nro. pisos</span>
                    <span style="float:left; width:90px;">Nro. dptos</span>
                </div>
                <div style="width:180px;">
                    <span style="float:left; width:90px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(event)" name="nro_pisos_1" id="nro_pisos_1" size="5" value="" /></span>
                    <span style="float:left; width:90px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(event)" name="nro_dptos_1" id="nro_dptos_1" size="5" value="" /></span>
                </div>
            </div>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VIVEN</td>
        <td>
            <select name="viven" id="viven">
                <option value="">Seleccione</option>
                <option value="1">Si</option>
                <option value="0">No</option>
            </select> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
        <td>
            <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
        <td>
            <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjTipoInfraestructura) ) {
                foreach ( $arrObjTipoInfraestructura as $objTipoInfraestructura ) {
                ?>
                <option value="<?php echo $objTipoInfraestructura->__get('_idTipoInfraestructura')?>" ><?php echo $objTipoInfraestructura->__get('_desTipoInfraestructura')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MEGAPROYECTO</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
            <select name="megaproyecto" id="megaproyecto">
                <option value="">Seleccione</option>
                <option value="1">Si</option>
                <option value="0">No</option>
            </select>
            <?php 
            } else {
            ?>
                <input type="hidden" name="megaproyecto" id="megaproyecto" value="<?php echo $megaproyecto?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">% AVANCE</td>
        <td>
            <input type="text" name="avance" id="avance" value="" /> <em>*</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N</td>
        <td>
            <input type="text" name="fecha_termino_construccion" id="fecha_termino_construccion" class="clsTxtFecha" size="11" readonly="readonly" value="" /> <em>*</em> 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_termino_construccion">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MONTANTE</td>
        <td>
            <select name="montante" id="montante" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> <em class="c_required" style="display:none;">*</em> &nbsp; 
            Fecha: <input type="text" size="11" name="montante_fecha" id="montante_fecha" class="clsTxtFecha" value="" readonly="readonly" /> <em class="c_required" style="display:none;">*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="montante_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DUCTERIA INTERNA</td>
        <td>
            <select name="ducteria_interna" id="ducteria_interna" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> <em class="c_required" style="display:none;">*</em> &nbsp; 
            Fecha: <input type="text" size="11" name="ducteria_interna_fecha" id="ducteria_interna_fecha" class="clsTxtFecha" value="" readonly="readonly" /> <em class="c_required" style="display:none;">*</em> &nbsp; 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="ducteria_interna_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PUNTO DE ENERGIA</td>
        <td>
            <select name="punto_energia" id="punto_energia" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> &nbsp; 
            Fecha: <input type="text" size="11" name="punto_energia_fecha" id="punto_energia_fecha" class="clsTxtFecha" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="punto_energia_fecha">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FACILIDADES DEL POSTE</td>
        <td>
            <select name="idfacilidad_poste" id="idfacilidad_poste">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjFacilidadPoste) ) {
                foreach ( $arrObjFacilidadPoste as $objFacilidadPoste ) {
                ?>
                <option value="<?php echo $objFacilidadPoste->__get('_idFacilidadPoste')?>" ><?php echo $objFacilidadPoste->__get('_desFacilidadPoste')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">RESPONSABLES CAMPO</td>
        <td>
            <select name="idresponsable_campo" id="idresponsable_campo">
                <option value="">Seleccione</option>
                <?php 
                if ( !empty($arrObjResponsableCampo) ) {
                foreach ( $arrObjResponsableCampo as $objResponsableCampo ) {
                ?>
                <option value="<?php echo $objResponsableCampo->__get('_idResponsableCampo')?>" ><?php echo $objResponsableCampo->__get('_desResponsableCampo')?></option>
                <?php
                }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">VALIDADO POR CALL</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
            <select name="validacion_call" id="validacion_call">
                <option value="">Seleccione</option>
                <option value="1">Si</option>
                <option value="0">No</option>
            </select>
            <?php 
            } else {
            ?>
                <input type="hidden" name="validacion_call" id="validacion_call" value="<?php echo $validacionCall?>" />
            <?php
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA TRATAMIENTO CALL</td>
        <td>
            <?php 
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
            ?>
                <input type="text" name="fecha_tratamiento_call" id="fecha_tratamiento_call" class="clsTxtFecha" size="11" readonly="readonly" value="" />
                <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_tratamiento_call">Limpiar</a>
            <?php 
            } else {
            ?>
                <input type="hidden" name="fecha_tratamiento_call" id="fecha_tratamiento_call" class="clsTxtFecha" size="11" value="<?php echo $fechaTratamientoCall?>" />
            <?php 
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
        <td>
            <input type="text" size="11" name="fecha_seguimiento" id="fecha_seguimiento" class="clsTxtFecha" value="<?php echo date('d/m/Y')?>" readonly="readonly" /> <em>*</em> 
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_seguimiento">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
        <td>
            <textarea name="observacion" id="observacion" style="width:95%;height:100px;"></textarea>
        </td>
    </tr>
    </tbody>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody><tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px; float:left;">
                <input type="button" value="Grabar" name="btn_grabar" title="Grabar" id="btn_grabar" class="submit" onclick="javascript: insertDatosEdificio();" />&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" class="submit" />
            </div>
            <div style="margin: 10px 0 0 20px; display: none; float:left;" id="msg">
                <img alt="" src="../../img/loading.gif"> <b>Grabando datos.</b>
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
    
</div>

</body>
</html>
