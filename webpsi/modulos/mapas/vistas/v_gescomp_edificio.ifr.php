<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

        <script type="text/javascript" src="../../../js/jquery/jquery-latest.js"></script>
        <script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

        <script type="text/javascript" src="../../../js/jquery/prettify.js"></script>

        <link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
        <link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
        
        <script type="text/javascript" src="../../../modulos/maps/js/edificios.fftt.js"></script>
        <script type="text/javascript" src="../../../js/jquery/actuaciones.js"></script>

        <script type="text/javascript">

            var poligonosDibujar_array = [];
            var capa_exists = 0;

            var myLayout;
            var map = null;
            var markers_arr = [];
            var makeMarkers_arr = [];
            var ptos_arr = [];

            $(document).ready(function () {
        
                myLayout = $('body').layout({
                    // enable showOverflow on west-pane so popups will overlap north pane
                    west__showOverflowOnHover: true,
                    west: {size:400}
                });

                listarZonales = function() {
                    data_content = "action=listarZonales";
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.edificio.main.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            var selectZonal = '';
                            for(var i in datos) {
                                selectZonal += '<option value="' + datos[i]['abvZonal'] + '">' + datos[i]['abvZonal'] + ' - ' + datos[i]['descZonal'] + '</option>'
                            }
                              $("#selZonal").append(selectZonal);
                        }
                    });
                }

                var M = {
                    initialize: function() {

                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com');
                            return false;
                        }
    	
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var myOptions = {
                            zoom: 11,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                        //carga el combo zonales
                        listarZonales();

                    }
                };
	
	
                $("#selZonal").change(function() {
                    IDzon = this.value;
                    if(IDzon == "") {
                        return false;
                    }
                    data_content = "action=buscarMdf&IDzon=" + IDzon;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.edificio.main.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            var selectMdf = '<option value="">Seleccione</option>';
                            for(var i in datos) {
                                selectMdf += '<option value="' + datos[i]['IDmdf'] + '">' + datos[i]['IDmdf'] + ' - ' + datos[i]['Descmdf'] + '</option>'
                            }
                            $("#selMdf").html(selectMdf);
                        }
                    });

                    $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
                    $("#selCableArmario").html('<option value="">Seleccione</option>');
                    $("#poligono").removeAttr('disabled');
                });
	
                $("#poligono").click(function() {
                    if($("#poligono").is(':checked')) { 
			
                        var IDmdf    = $("#selMdf").val();
			
                        data_content = "action=buscar&area_mdf=si&IDmdf=" + IDmdf;
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.edificio.main.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
				 
                                if( capa_exists ) {
                                    //limpiamos todas las capas del mapa
                                    clearPoligonos();
                                }

                                var poligonos = data['xy_poligono'];
                                if( poligonos.length > 0 ) {
                                    //pintamos los poligonos
                                    for (var i in poligonos) {
                                        loadPoligono(poligonos[i], i);
                                    }
                                }
                            }
                        });

                    }else {  
			
                        if( capa_exists ) {
                            //limpiamos todas las capas del mapa
                            clearPoligonos();
                        }
                    }  
                });
	
                $("#btnBuscar").click(function() {
                    // lista y agregar los markers en divs "sidebar" y "map"
                    listar_edificios();
                });


                M.initialize(); 
            });

            function clearPoligonos() {
                for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
                    poligonoDibujar.setMap(null);
                }
            }

            function loadPoligono(xy_array, z) {

                var datos 			= xy_array['coords'];
                var color_poligono 	= xy_array['color'];
                var poligonoCoords 	= [];

                if( datos.length > 0) {
                    for (var i in datos) {
                        var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                        poligonoCoords.push(pto_poligono);
                    }

                    // Construct the polygon
                    // Note that we don't specify an array or arrays, but instead just
                    // a simple array of LatLngs in the paths property
                    poligonoDibujar = new google.maps.Polygon({
                        paths: poligonoCoords,
                        strokeColor: color_poligono,
                        strokeOpacity: 0.6,
                        strokeWeight: 3,
                        fillColor: color_poligono,
                        fillOpacity: 0.5
                    });

                    //agrego al array las capas seleccionadas
                    poligonosDibujar_array.push(poligonoDibujar);

                    //seteo la capa en el mapa
                    poligonoDibujar.setMap(map);

                    //seteamos que la capa existe
                    capa_exists = 1;

                }
                else {
                    capa_exists = 0;
                }		
            }


            function listar_edificios() {

                var IDzonal	 = $("#selZonal").val();
                var IDmdf    = $("#selMdf").val();
                var Item    = $("#item").val();
                var datosXY = $("#selDatosXY").val();
	
                var area_mdf = 'no';
                if($("#poligono").is(':checked')) { 
                    area_mdf = 'si';
                }

                if( Item == '') {
                    if(IDzonal == "" || IDmdf == "") {
                        alert("Ingrese todos los criterios de busqueda.");
                        return false;
                    }
                }

                /**
                 * makeMarker() ver 0.2
                 * creates Marker and InfoWindow on a Map() named 'map'
                 * creates sidebar row in a DIV 'sidebar'
                 * saves marker to markerArray and markerBounds
                 * @param options object for Marker, InfoWindow and SidebarItem
                 */
                var infoWindow = new google.maps.InfoWindow();
                var markerBounds = new google.maps.LatLngBounds();
                var markerArray = [];
          
                function makeMarker(options){
                    var pushPin = null;

			
                    if( options.position != '(0, 0)' ) {
                        pushPin = new google.maps.Marker({
                            map: map
                        });

                        makeMarkers_arr.push(pushPin);

                        pushPin.setOptions(options);
                        google.maps.event.addListener(pushPin, "click", function(){
                            infoWindow.setOptions(options);
                            infoWindow.open(map, pushPin);
                            if(this.sidebarButton)this.sidebarButton.button.focus();
                        });
                        var idleIcon = pushPin.getIcon();

                        markerBounds.extend(options.position);
                        markerArray.push(pushPin);
                    }else {
                        pushPin = new google.maps.Marker({
                            map: map
                        });
			
                        makeMarkers_arr.push(pushPin);
			
                        pushPin.setOptions(options);
                    }
         
                    if(options.sidebarItem){
                        pushPin.sidebarButton = new SidebarItem(pushPin, options);
                        pushPin.sidebarButton.addIn("sidebar");
                    }
         
                    return pushPin;       	
                }

                google.maps.event.addListener(map, "click", function(){
                    infoWindow.close();
                });

                /*
                 * Creates an sidebar item 
                 * @param marker
                 * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                function SidebarItem(marker, opts) {
                    //var tag = opts.sidebarItemType || "button";
                    var tag = opts.sidebarItemType || "div";
                    var row = document.createElement(tag);
                    row.innerHTML = opts.sidebarItem;
                    row.className = opts.sidebarItemClassName || "sidebar_item";  
                    row.style.display = "block";
                    row.style.width = opts.sidebarItemWidth || "365px";
                    row.onclick = function(){
                        google.maps.event.trigger(marker, 'click');
                    }
                    row.onmouseover = function(){
                        google.maps.event.trigger(marker, 'mouseover');
                    }
                    row.onmouseout = function(){
                        google.maps.event.trigger(marker, 'mouseout');
                    }
                    this.button = row;
                }
                // adds a sidebar item to a <div>
                SidebarItem.prototype.addIn = function(block){
                    if(block && block.nodeType == 1)this.div = block;
                    else
                        this.div = document.getElementById(block)
                        || document.getElementById("sidebar")
                        || document.getElementsByTagName("body")[0];
                    this.div.appendChild(this.button);
                }


                data_content = "action=buscarEdificios&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&Item=" + Item + "&datosXY=" + datosXY + "&area_mdf=" + area_mdf;
                $("#box_loading").show();
                $('#spanNum').html('0');
                $("#btnBuscar").attr('disabled', 'disabled');
                $("#sidebar").html("");
                var tabla = '';
                var detalle = '';
                $.ajax({
                    type:   "POST",
                    url:    "../gescomp.edificio.main.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {
		 
                        clearMarkers();
                        clearMakeMarkers();
			
			
                        if( capa_exists ) {
                            //limpiamos todas las capas del mapa
                            clearPoligonos();
                        }

                        var poligonos = data['xy_poligono'];

                        if( poligonos.length > 0 ) {
                            //pintamos los poligonos
                            for (var i in poligonos) {
                                loadPoligono(poligonos[i], i);
                            }
				
                            //setea el zoom y el centro
                            map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                            //map.setZoom(13);
                        }

			
                        if( data['edificios'].length > 0 ) {


                            if( data['edificios_con_xy'].length == 0 ) {

                                var datos = data['edificios']; 
                                var estados = data['estados'];

                                var x1 = y1 = null;
                                if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                    x1 = data['mdf']['x'];
                                    y1 = data['mdf']['y'];
                                }else if( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
                                    x1 = data['zonal']['x'];
                                    y1 = data['zonal']['y'];
                                }
					
                                for (var i in datos) {
                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                                    tabla = '<div id="row_' + datos[i]['idedificio'] + '" class="unselected">';
                                    tabla += '<div style="width:30px; float:left;"><img onclick="ir_mapa(\'' + datos[i]['idedificio'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></div>';
                                    tabla += '<div style="width:50px; float:left; font-weight:bold;' + color + '">' + datos[i]['item'] + '</div>';
                                    tabla += '<div style="width:240px; float:left;">';
                                    tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['tipo_via'] + ' ' + datos[i]['direccion_obra'] + ' ' + datos[i]['numero'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">' + datos[i]['nom_distrito'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">';
                                    tabla += '<input id="x' + datos[i]['idedificio'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idedificio'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                                    tabla += '<input id="y' + datos[i]['idedificio'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idedificio'] + '][y]" size="12" readonly="readonly">';
                                    tabla += '</div>';
                                    tabla += '</div>';
                                    tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span style="color:#ff0000;font-size:8px;">' + datos[i]['des_estado'] + '</span></div>';
                                    tabla += '</div>';
                                    
                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(y1, x1),
                                        title: '',
                                        sidebarItem: tabla,
                                        //content: detalle,
                                        icon: "../../../img/markergreen.png"
                                    });
                		
                                }
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);
                                //map.setZoom(10);

                            }else {

    	 		
                                var datos = data['edificios']; 
                                var estados = data['estados'];
	    	     	
                                for (var i in datos) {
                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
	            		
                                    var color = "color: #FF0000;";
                                    if( x != '' && y != '' ) {
                                        color = "color: #000000;";
                                    }

                                    tabla = '<div id="row_' + datos[i]['idedificio'] + '" class="unselected">';
                                    tabla += '<div style="width:30px; float:left;"><img onclick="ir_mapa(\'' + datos[i]['idedificio'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></div>';
                                    tabla += '<div style="width:50px; float:left; font-weight:bold;' + color + '">' + datos[i]['item'] + '</div>';
                                    tabla += '<div style="width:240px; float:left;">';
                                    tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['tipo_via'] + ' ' + datos[i]['direccion_obra'] + ' ' + datos[i]['numero'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">' + datos[i]['nom_distrito'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">';
                                    tabla += '<input id="x' + datos[i]['idedificio'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idedificio'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                                    tabla += '<input id="y' + datos[i]['idedificio'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idedificio'] + '][y]" size="12" readonly="readonly">';
                                    tabla += '</div>';
                                    tabla += '</div>';
                                    tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span style="color:#ff0000;font-size:8px;">' + datos[i]['des_estado'] + '</span></div>';
                                    tabla += '</div>';
                                    
	
                                    detalle = '<img border="0" src="../../../pages/imagenComponente.php?imgcomp=' + datos[i]['foto1'] + '&dircomp=edi&w=120"><br />';
                                    detalle += '<b>Zonal:</b> LIMA<br />';
                                    detalle += '<b>Mdf:</b> ' + datos[i]['ura'] + '<br />';
                                    detalle += '<b>Edificio:</b> ' + datos[i]['item'] + '<br />';
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br /><br />';
                                    //detalle += '<a href="javascript:editarEdificio(\'' + datos[i]['idedificio'] + '\',\'' + datos[i]['item'] + '\',\'' + datos[i]['x'] + '\',\'' + datos[i]['y'] + '\');">Editar Edificio</a> | ';
                                    detalle += '<a href="javascript:detalleEdificioParent(\'' + datos[i]['idedificio'] + '\');">Ver Edificio</a> | ';
                                    detalle += '<a href="javascript:editarEdificioParent(\'' + datos[i]['idedificio'] + '\');">Editar Edificio</a>';
                                    //detalle += '<a href="javascript:agregarImagenesEdificio(\'' + datos[i]['idedificio'] + '\', \'' + datos[i]['item'] + '\', \'' + datos[i]['x'] + '\', \'' + datos[i]['y'] + '\')">Agregar nuevo grupo de imagenes</a>';
	            		
                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['item'],
                                        //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/edificio/edificio-" + datos[i]['estado'] + ".gif"
                                    });
	            		
                                }
	
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);

                            }
    	 		
                        }else {
				
                            var x_arm = y_arm = null;
                            if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                x_arm = data['mdf']['x'];
                                y_arm = data['mdf']['y'];
                            }else {
                                x_arm = data['zonal']['x'];
                                y_arm = data['zonal']['y'];
                            }
				
                            var latlng = new google.maps.LatLng(y_arm, x_arm);

                            /* markers and info window contents */
                            makeMarker({
                                draggable: false,
                                position: new google.maps.LatLng(y_arm, x_arm),
                                title: '',
                                icon: "../../../img/markergreen.png"
                            });
				
                            map.setCenter(latlng);

                        }
         
                        $("#btnBuscar").attr('disabled', '');
                        $("#btnGrabar").attr('disabled', '');
                        $("#box_loading").hide();
                        $('#spanNum').html(data['edificios'].length);
                    }
                });
            }

            function ir_mapa(terminal_sel) {
                $("#trm_selected").val(terminal_sel);
                
                $(".unselected").attr("class", "unselected");
                $(".selected").attr("class", "unselected");
                $("#row_" + terminal_sel).attr("class", "selected");

                google.maps.event.addListener(map, "click", function(event) {

                    clearMarkers();
		
                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map, 
                        title: terminal_sel
                    });
                    markers_arr.push(marker);
		
                    if( $("#trm_selected").val() == terminal_sel ) {
                        var Coords = event.latLng;
                        
                        $("#x" + terminal_sel).val(Coords.lng());
                        $("#y" + terminal_sel).val(Coords.lat());
                    }
                });
            }

            function clearMarkers() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }

            function clearMakeMarkers() {
                for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                    makeMarker.setVisible(false);
                }
            }

            function agregarImagenesEdificio(idedificio, item, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/agregarImagenesEdificio.popup.php", {
                    idedificio	: idedificio,
                    item		: item,
                    x			: x,
                    y			: y
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Agregar nuevo grupo de imagenes para este Edificio', position:'top'});
	
            }


            function changeEstado(edificio, estado) {

                if( confirm("Esta seguro de cambiar el estado para este edificio?") ) {
                    IDedi	 = edificio;
                    data_content = "action=saveEstado&IDedi=" + IDedi + "&estado=" + estado;
	
                    $("#fongoLoader").show();
                    $.ajax({
                        type:     "POST",
                        dataType: "json",
                        url:      "../gescomp.edificio.main.php",
                        data:	  data_content,
                        success:  function(datos) {
                            if( !datos['success'] ) {
                                alert(datos['msg']);
                            }else {
                                listar_edificios();
                            }
                        }
                    });
                    $("#fongoLoader").hide();
                }
            }

            function Grabar() {

                if( confirm("Esta seguro de guardar los cambios para este edificio?") ) {
	
                    IDzonal	 = $("#selZonal").val();
                    IDmdf    = $("#selMdf").val();
	
                    var query = $("input.latlon").serializeArray();
                    json = {};
		
                    for (i in query) {
                        json[query[i].name] = query[i].value;
                    } 
                    $("#fongoLoader").show();
                    $.ajax({
                        type:     "POST",
                        dataType: "json",
                        url:      "../gescomp.edificio.main.php?action=save&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf,
                        data:     query,
                        success:  function(datos) {
                            if( !datos['success'] ) {
                                alert(datos['msg']);
                            }else {
                                listar_edificios();
                            }
                        }
                    });
                    $("#fongoLoader").hide();
                }
            }


        </script>
        <script>
	$(function() {
            $("#btnBuscar, #btnGrabar").button();
	});
	</script>
        <style type="text/css">
            .unselected {
                background: none repeat scroll 0 0 #FFFFFF;
                padding: 4px 0;
                float: left;
            }
            .selected {
                background: none repeat scroll 0 0 #DBEDFE;   
                padding: 4px 0;
                float: left;
            }
            .ipt{
                border:none;
                color:#0086C3;
            }
            .ipt:focus{
                border:1px solid #0086C3;
            }
            body{
                font-family: Arial;
                font-size: 11px;
            }
            .ui-layout-pane {
                background: #FFF; 
                border: 1px solid #BBB; 
                padding: 7px; 
                overflow: auto;
            } 
            .sidebar_item {
                float: left;
                padding: 5px 0;
                border-bottom: 1px solid #DDDDDD;
            }
            .ui-layout-resizer {
                background: #DDD; 
            } 

            .ui-layout-toggler {
                background: #AAA; 
            }
            .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
            .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
            .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
            .slt{
                border:1px solid #1E9EF9;
                background-color: #FAFAFA;
                height: 18px;
                font-size: 11px;
                color:#0B0E9D;
            }
            .box_checkboxes{
                border:1px solid #4C5E60;
                background: #E7F7F7;
                width:170px;
                height: 240px;
                overflow: auto;
                display:none;
                position: absolute;
                top:0px;
                left:0px;
                opacity:0.9;
            }
            .tb_data_box{
                width: 100%;
                border-collapse:collapse;
                font-family: Arial;
                font-size: 9px;
            }

        </style>
    </head>
    <body>
        <div class="ui-layout-west">
            
            <div id="divPoligono">
                
                <table cellpadding="2" cellspacing="0" border="0" style="width:100%; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><span class="l_tit">Mantenimiento Edificios</span></td>
                </tr>
                <tr>
                    <td width="150" class="label">Zonal:</td>
                    <td colspan="2">
                        <select id="selZonal">
                            <option value="">Seleccione</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">Mdf:</td>
                    <td colspan="2">
                        <div id="divMdf">
                            <select id="selMdf">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="label">
                    <td>Con X,Y:</td>
                    <td colspan="2">
                        <select id="selDatosXY">
                            <option value="">Todos</option>
                            <option value="conXY">Terminales con X,Y</option>
                            <option value="sinXY">Terminales sin X,Y</option>
                        </select>
                    </td>
                </tr>
                <tr class="label">
                    <td>Item:</td>
                    <td colspan="2">
                        <input type="text" name="item" id="item" size="6" />
                    </td>
                </tr>
                <!--
                <tr>
                    <td colspan="3"><input type="checkbox" name="poligono" id="poligono" disabled="disabled"> Mostrar poligono(Mdf)</td>
                </tr>
                -->
                <tr>
                    <td>
                        <div style="float:left;"><button id="btnBuscar">Buscar</button></div>  
                    </td>
                    <td>
                        <div id="box_loading" style="float:left; margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                    </td>
                    <td style="text-align:right;">
                        <button id="btnGrabar" onclick="Grabar()">Grabar Datos</button>
                        <input type="hidden" id="trm_selected" readonly="readonly">
                    </td>
                </tr>
                </table>
                
            </div>
            
            <div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">
			
                <div id="divInfoResult" style="margin-bottom: 8px; width: 365px;">
                    <div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
                </div>

                <div id="divTitle" style="background-color: #003366;border: 1px solid #000000;color: #FFFFFF;float: left;padding: 3px 0;width: 365px;">
                        <div style="width:30px;float:left;">&nbsp;</div>
                        <div style="width:50px;float:left;">Item</div>
                        <div style="width:240px;float:left;">Direcci&oacute;n pirata</div>
                        <div style="width:40px;float:left;">Estado</div>
                </div>
                <div id="sidebar"></div>
            </div>

            <input type="hidden" id="trm_selected" readonly="readonly">

        </div>
                    
        <div class="ui-layout-center">
            <div id="box_resultado">

                <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>

            </div>

        </div>

        <div id="parentModal" style="display: none;">
            <div id="childModal" style="padding: 10px; background: #fff;"></div>
        </div>

        <!-- Divs efecto Loader -->
        <div id="fongoLoader" style="display:none" class="bgLoader">
            <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
        </div>
        <!-- fin  -->
        
        </body>
        </html>