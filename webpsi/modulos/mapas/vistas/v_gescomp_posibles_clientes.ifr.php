<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


$obj_terminales = new data_ffttTerminales();

$idusuario = $_SESSION['USUARIO']->__get('idusuario');
$array_mdf = $obj_terminales->get_mdf_by_zonal_and_idusuario($conexion, 'LIM', $idusuario);

$mdf_arr = array();
foreach ($array_mdf as $mdf) {
    $mdf_arr[] = array(
        'IDmdf' 	=> $mdf['mdf'],
        'Descmdf'	=> $mdf['nombre']
    );
}




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.multiselect.filter.js"></script>

<script type="text/javascript" src="../../../js/cv.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.PrintArea.js"></script>

<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
<link href="../../../css/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript">
$(function(){
	$("select.clsMultiple").multiselect().multiselectfilter();
});
</script>
<script type="text/javascript">

var infoWindow = null;

var myLayout;
var map = null;
var poligonosDibujarMdf_array = [];

var ptos_arr = [];
var markers_arr = [];
var makeMarkers_arr = [];

var infoWindow_arr = [];

var capaMdf_exists = 0;
var capaTroba_exists = 0;

var ptos_xy_trm = [];
var ptos_xy_arm = [];

$(document).ready(function () {


    $("#tabs").tabs();

    myLayout = $('body').layout({
            // enable showOverflow on west-pane so popups will overlap north pane
            west__showOverflowOnHover: true,
            west: {size:420}
    });

    load_array=function(element){
        var grupo=new Array();
        $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
        return grupo;
    }
    
    $('#selTodos')
        .filter(':has(:checkbox:checked)')
        .end()
        .click(function(event) {
            if($("#selTodos").is(':checked')) { 
                $(".lista_chk").attr('checked', 'checked');
            }else {  
                $(".lista_chk").removeAttr('checked');
            }		
    });
    
    $("#mdf").multiselect({
        close: function(){
            $("#poligono_mdf").removeAttr('checked');
        }
    });
    
    
    $("#zonal").change(function() {
        M.MdfsxZonal();
    });
    
    $("#poligono_mdf").click(function() {
        M.DibujarMdf();
    });
    
    $('#btnFiltrar').click(function(){
        listar_resultados();
    });
    
    imprimirEnMapaXY = function() {
        M.imprimirEnMapaXY();
    }
    
    GrabarDatos = function() {
        M.GrabarDatos();
    }
    
    CambiarEstado = function(estado) {
        M.CambiarEstado(estado);
    },
    
    map_pointer = function(terminal) {
        M.GetXYMapa(terminal);
    }
    
    
    var M = {
    	initialize: function() {

            if (typeof (google) == "undefined") {
                alert('Verifique su conexion a maps.google.com');
                return false;
            }
    	
            var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
            var myOptions = {
                zoom: 11,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        },

        clearMarkers: function() {
            for (var n = 0, marker; marker = markers_arr[n]; n++) {
                marker.setVisible(false);
            }
        },
		
        clearMakeMarkers: function() {
            for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                makeMarker.setVisible(false);
            }
        },

        clearPoligonosMdf: function() {
            for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujarMdf_array[n]; n++) {
                poligonoDibujar.setMap(null);
            }
        },
        
        clearInfoWindow :function () {
            for(var i=0; i<infoWindow_arr.length; i++){
                infoWindow_arr[i].close();
            }
        },

        loadPoligono: function(xy_array, tipo) {

            var datos 		= xy_array['coords'];
            var color_poligono 	= xy_array['color'];
            var poligonoCoords 	= [];

            if( datos.length > 0) {
                for (var i in datos) {
                    var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                    poligonoCoords.push(pto_poligono);
                }

                // Construct the polygon
                // Note that we don't specify an array or arrays, but instead just
                // a simple array of LatLngs in the paths property
                poligonoDibujar = new google.maps.Polygon({
                    paths: poligonoCoords,
                    strokeColor: color_poligono,
                    strokeOpacity: 0.6,
                    strokeWeight: 3,
                    fillColor: color_poligono,
                    fillOpacity: 0.5
                });

                //agrego al array las capas seleccionadas
                if( tipo == 'mdf') {
                    poligonosDibujarMdf_array.push(poligonoDibujar);
                    //seteamos que la capa existe
                    capaMdf_exists = 1;
                }

                //seteo la capa en el mapa
                poligonoDibujar.setMap(map);

            }
            else {
                if( tipo == 'mdf') {
                    capaMdf_exists = 0;
                }
            }		
        },
        
        MdfsxZonal: function() {
            var zonal = $("#zonal").val();

            var data_content = {
                'action'  : 'MdfsxZonal',
                'zonal'   : zonal
            };
            $.ajax({
                type:   "POST",
                url:    "../gescomp.posibles.clientes",
                data:   data_content,
                dataType: "html",
                success: function(datos) {
                    if( datos != '' ) {

                        $("#divMdf").html(datos);
                    }
                }
            });
        },
        
        DibujarMdf: function() {
            if($("#poligono_mdf").is(':checked')) { 

                var arreglo_mdf = new Array();

                arreglo_mdf 	 = load_array('multiselect_mdf[]');

                var data_content = {
                    'action'        : 'DibujarMdf',
                    'area_mdf'      : 'si',
                    'arreglo_mdf'  : arreglo_mdf
                };
                $.ajax({
                    type:   "POST",
                    url:    "../gescomp.posibles.clientes.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {

                        if( capaMdf_exists ) {
                            //limpiamos todas las capas del mapa
                            M.clearPoligonosMdf();
                        }

                        var poligonos = data['xy_poligono'];
                        if( poligonos.length > 0 ) {
                            //pintamos los poligonos
                            for (var i in poligonos) {
                                M.loadPoligono(poligonos[i], 'mdf');
                            }

                            //setea el zoom y el centro
                            map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                            map.setZoom(13);
                        }
                    }
                });

            }else {  

                if( capaMdf_exists ) {
                    //limpiamos todas las capas del mapa
                    M.clearPoligonosMdf();
                }
            }

        },
        
        GetXYMapa: function(terminal_sel) {
            $("#trm_selected").val(terminal_sel);
	
            $(".unselected").attr("class", "unselected");
            $(".selected").attr("class", "unselected");
            $("#row_" + terminal_sel).attr("class", "selected");

            google.maps.event.addListener(map, "click", function(event) {
                M.clearMarkers();

                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map, 
                    title: terminal_sel
                });
                markers_arr.push(marker);

                if( $("#trm_selected").val() == terminal_sel ) {
                    var Coords = event.latLng;

                    $("#x" + terminal_sel).val(Coords.lng());
                    $("#y" + terminal_sel).val(Coords.lat());
                }
            });

            //para capturar el x,y por cada poligono
            for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujarMdf_array[n]; n++) {
                //poligonoDibujar.setMap(null);
                
                google.maps.event.addListener(poligonoDibujar, "click", function(event) {
                    M.clearMarkers();

                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map, 
                        title: terminal_sel
                    });
                    markers_arr.push(marker);

                    if( $("#trm_selected").val() == terminal_sel ) {
                        var Coords = event.latLng;

                        $("#x" + terminal_sel).val(Coords.lng());
                        $("#y" + terminal_sel).val(Coords.lat());
                    }
                });
                
            }

        },
        
        GrabarDatos: function() {
            var query = $("input.latlon").serializeArray();
            json = {};

            for (i in query) {
                json[query[i].name] = query[i].value;
                //alert("name=" + query[i].name + ", value=" + query[i].value);
            } 
            
            if( query.length == 0 ) {
                alert("Ningun cliente listado para ser actualizado");
                return false;
            }
            
            if( confirm("Esta seguro de guardar los cambios para estos posibles clientes?") ) {
	
		var zonal       = $("#zonal").val();
                var arreglo_mdf = new Array();

                arreglo_mdf     = load_array('multiselect_mdf[]');

                var data_content = {
                    'action'        : 'GrabarDatos',
                    'zonal'         : zonal,
                    'arreglo_mdf'   : arreglo_mdf
                };
                
                $("#fongoLoader").show();
                $.ajax({
                    type:     "POST",
                    dataType: "json",
                    url:      "../gescomp.posibles.clientes.php?action=GrabarDatos&zonal=" + zonal + "&arreglo_mdf=" + arreglo_mdf,
                    data:     query,
                    success:  function(datos) {
                        if( !datos['success'] ) {
                            alert(datos['msg']);
                        }else {
                            listar_resultados();
                        }
                    }
                });
                //setTimeout("hideLoader()", 1000);
                $("#fongoLoader").hide();
            }
        },

        CambiarEstado: function(estado) {
            var chk_val = contar_check_temas(".lista_chk");

            var ids_arr = []; 

            for( i in chk_val) {
                ids_arr.push(chk_val[i]);
            }
             
            if (chk_val.length > 0) {  
                
                if( confirm("Esta seguro de cambiar el estado para estos pedidos?") ) {
                    
                    var data_content = {
                        'action'    : 'CambiarEstado',
                        'ids_arr'   : ids_arr,
                        'estado'    : estado
                    };
 
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.posibles.clientes.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            if( !datos['success'] ) {
                                alert(datos['msg']);
                            }else {
                                listar_resultados();
                            }
                        }
                    });
                    
                }
            }else{
                alert("Ningun pedido seleccionado para cambiar el estado");
            }
        }
        
    };


    M.initialize();
               
});




function listar_resultados() {

    /**
    * makeMarker() ver 0.2
    * creates Marker and InfoWindow on a Map() named 'map'
    * creates sidebar row in a DIV 'sidebar'
    * saves marker to markerArray and markerBounds
    * @param options object for Marker, InfoWindow and SidebarItem
    */
    infoWindow = new google.maps.InfoWindow();
    var markerBounds = new google.maps.LatLngBounds();
    var markerArray = [];


    function makeMarker(options){
        var pushPin = null;
        /*var pushPin = new google.maps.Marker({
            map: map
        });
        pushPin.setOptions(options);*/
			
     	if( options.position != '(0, 0)' ) {
            pushPin = new google.maps.Marker({
                map: map
            });

            makeMarkers_arr.push(pushPin);

            pushPin.setOptions(options);
            google.maps.event.addListener(pushPin, "click", function(){
                infoWindow.setOptions(options);
                infoWindow.open(map, pushPin);
                if(this.sidebarButton)this.sidebarButton.button.focus();
            });
            var idleIcon = pushPin.getIcon();

            markerBounds.extend(options.position);
            markerArray.push(pushPin);
     	}else {
         	
            pushPin = new google.maps.Marker({
                map: map
            });
			
            makeMarkers_arr.push(pushPin);
			
            pushPin.setOptions(options);
     	}
        
        //almaceno los infowindows
        infoWindow_arr.push(infoWindow);
         
        if(options.sidebarItem){
            pushPin.sidebarButton = new SidebarItem(pushPin, options);
            pushPin.sidebarButton.addIn("sidebar");
        }

        return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });
     

     

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         //var tag = opts.sidebarItemType || "span";
		 var tag = opts.sidebarItemType || "div";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "390px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }
	 

    $("#selTodos").removeAttr('checked');

    var datosXY     = $("#selDatosXY").val();
    var arreglo_mdf = new Array();

    arreglo_mdf 	 = load_array('multiselect_mdf[]');

    data_content = {
        'action'        : 'ListarPosiblesClientes',
        'zonal'         : $('#zonal').val(),
        'arreglo_mdf' 	: arreglo_mdf,
        'datosXY'       : datosXY
    };

     $('#box_loading').show();
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "../gescomp.posibles.clientes.php",
         data:   data_content,
         dataType: "json",
         success: function(data) {
		 
            clearMarkers();
            clearMakeMarkers();
            
            clearInfoWindow();

            ptos_xy_trm = [];
            ptos_xy_arm = [];


            if( data['edificios'].length > 0 ) {
			
                if( data['edificios_con_xy'].length == 0 ) {

                    var datos = data['edificios']; 
                    
                    var x1 = y1 = null;
                    if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                        x1 = data['mdf']['x'];
                        y1 = data['mdf']['y'];
                    }else if( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
                        x1 = data['zonal']['x'];
                        y1 = data['zonal']['y'];
                    }

                    var num = 1;
                    var x_ini = y_ini = '';
                    var flag_captura_xy = 1;
                    for (var i in datos) {
                        
                        var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                        var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                        if( flag_captura_xy && x != '' && y != '' ) {
                            x_ini = datos[i]['x'];
                            y_ini = datos[i]['y'];
                            flag_captura_xy = 0;
                        }

                        var color = "color: #FF0000;";
                        if( x != '' && y != '' ) {
                            color = "color: #000000;";
                        }
                        
                        var estado = "1";
                        if( datos[i]['estado'] == 'N' ) {
                            estado = "3";
                        }

                        tabla = '<div id="row_' + datos[i]['idposible_cliente'] + '" class="unselected">';
                        tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['idposible_cliente'] + '" class="lista_chk" type="checkbox" value="' + datos[i]['idposible_cliente'] + '" name="' + datos[i]['idposible_cliente'] + '"><br /><img style="width:12px;padding:0 0 0 4px;" src="../../../img/map/posiblesclientes/posiblecliente-' + estado + '.gif"></div>';
                        tabla += '<div style="width:70px; float:left; font-weight:bold;' + color + '">' + datos[i]['cod_pedido'] + '<br /><span style="font-weight:normal;">Zonal: ' + datos[i]['zonal'] +'<br />Mdf: ' + datos[i]['mdf'] +  '<br /><img onclick="map_pointer(\'' + datos[i]['idposible_cliente'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></span></div>';
                        tabla += '<div style="width:240px; float:left;">';
                        tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['cliente'] + '</div>';
                        tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion'] + ' - ' + datos[i]['des_distrito'] + ' - <span style="color:#000000;">' + datos[i]['zonal'] + '<span></div>';
                        tabla += '<div style="font-size:10px;color:#555555;">Telf. ' + datos[i]['num_telef_contacto'] + '</div>';
                        tabla += '<div style="font-size:10px;margin-top:5px;"><span style="color:#555555;">Fec.Reg.</span> <span style="color:#1155CC;">' + datos[i]['fecha_registro'] + '</span></div>';
                        tabla += '<div style="font-size:10px;color:#555555;">';
                        tabla += '<input id="x' + datos[i]['idposible_cliente'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idposible_cliente'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                        tabla += '<input id="y' + datos[i]['idposible_cliente'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idposible_cliente'] + '][y]" size="12" readonly="readonly">';
                        tabla += '</div>';
                        tabla += '</div>';
                        tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="' + datos[i]['producto'] + '" >' + datos[i]['producto'] + '</span></div>';
                        tabla += '</div>';

                        /* markers and info window contents */
                        makeMarker({
                            draggable: false,
                            position: new google.maps.LatLng(y1, x1),
                            title: '',
                            sidebarItem: tabla,
                            //content: detalle,
                            icon: "../../../img/markergreen.png"
                        });

                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    map.setZoom(14);
                    

    	 			
                }else {
			

                    var datos = data['edificios']; 

                    var num = 1;
                    var x_ini = y_ini = '';
                    var flag_captura_xy = 1;
                    for (var i in datos) {

                        var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                        var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                        if( flag_captura_xy && x != '' && y != '' ) {
                            x_ini = datos[i]['x'];
                            y_ini = datos[i]['y'];
                            flag_captura_xy = 0;
                        }

                        var color = "color: #FF0000;";
                        if( x != '' && y != '' ) {
                            color = "color: #000000;";
                        }
                        
                        var estado = "1";
                        if( datos[i]['estado'] == 'N' ) {
                            estado = "3";
                        }

                        //llenando array para Imprimir en Mapa
                        ptos_xy_trm.push(datos[i]['terminal'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '||' + datos[i]['carmario'] + '|' + datos[i]['cbloque'] + '|' + datos[i]['des_ave']);
                        //ptos_xy_trm.push(datos[i]['caja'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '|' + datos[i]['ccable'] + '|' + datos[i]['carmario'] + '|' + datos[i]['tipo_red'] + '|' + datos[i]['des_ave']);

                        tabla = '<div id="row_' + datos[i]['idposible_cliente'] + '" class="unselected">';
                        tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['idposible_cliente'] + '" class="lista_chk" type="checkbox" value="' + datos[i]['idposible_cliente'] + '" name="' + datos[i]['idposible_cliente'] + '"><br /><img style="width:12px;padding:0 0 0 4px;" src="../../../img/map/posiblesclientes/posiblecliente-' + estado + '.gif"></div>';
                        tabla += '<div style="width:70px; float:left; font-weight:bold;' + color + '">' + datos[i]['cod_pedido'] + '<br /><span style="font-weight:normal;">Zonal: ' + datos[i]['zonal'] +'<br />Mdf: ' + datos[i]['mdf'] +  '<br /><img onclick="map_pointer(\'' + datos[i]['idposible_cliente'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></span></div>';
                        tabla += '<div style="width:240px; float:left;">';
                        tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['cliente'] + '</div>';
                        tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion'] + ' - ' + datos[i]['des_distrito'] + ' - <span style="color:#000000;">' + datos[i]['zonal'] + '<span></div>';
                        tabla += '<div style="font-size:10px;color:#555555;">Telf. ' + datos[i]['num_telef_contacto'] + '</div>';
                        tabla += '<div style="font-size:10px;margin-top:5px;"><span style="color:#555555;">Fec.Reg.</span> <span style="color:#1155CC;">' + datos[i]['fecha_registro'] + '</span></div>';
                        tabla += '<div style="font-size:10px;color:#555555;">';
                        tabla += '<input id="x' + datos[i]['idposible_cliente'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idposible_cliente'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                        tabla += '<input id="y' + datos[i]['idposible_cliente'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idposible_cliente'] + '][y]" size="12" readonly="readonly">';
                        tabla += '</div>';
                        tabla += '</div>';
                        tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="' + datos[i]['producto'] + '" >' + datos[i]['producto'] + '</span></div>';
                        tabla += '</div>';


                        detalle = '<div style="width:400px;">';
                        detalle += '<div>';
                        detalle += '<table width="100%">';
                        detalle += '<tr><td class="subtit_info">PEDIDO:</td><td class="det_info">' + datos[i]['cod_pedido'] + '</td><td class="subtit_info">PRODUCTO:</td><td class="det_info">' + datos[i]['producto'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">ZONAL:</td><td class="det_info">' + datos[i]['zonal'] + '</td><td class="subtit_info">MDF:</td><td class="det_info">' + datos[i]['mdf'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">TEL.REF:</td><td class="det_info">' + datos[i]['num_telef_contacto'] + '</td><td class="subtit_info">SEGMENTO:</td><td class="det_info">' + datos[i]['segmento'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">CLIENTE:</td><td class="det_info" colspan="3">' + datos[i]['cliente'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">DIRECCION:</td><td class="det_info" colspan="3">' + datos[i]['direccion'] + '</td></tr>';
                        detalle += '</table>';
                        detalle += '</div>';
                        detalle += '<div style="font-size: 12px;margin: 5px 0 0 0;"><span style="color:#777777;">X,Y:</span> <span style="color:#1155CC;">' + datos[i]['x'] + ',' + datos[i]['y'] + '</span></div>';
                        detalle += '</div>';


                        /* markers and info window contents */
                        makeMarker({
                                draggable: false,
                                position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                title: "Pedido: " + datos[i]['cod_pedido'],
                                sidebarItem: tabla,
                                content: detalle,
                                icon: "../../../img/map/posiblesclientes/posiblecliente-" + estado + ".gif"
                        });
						
                        num++;
                    }
					
                    /* fit viewport to markers */
                    map.fitBounds(markerBounds);

                    //map.setZoom(16);
                    map.setCenter(new google.maps.LatLng(y_ini, x_ini));
				
                }


            }

            //setTimeout("hideLoader()", 1000);
            $('#box_loading').hide();
            $('#spanNum').html(data['edificios'].length);
         }
     });
}

function clearMarkers() {
    for (var n = 0, marker; marker = markers_arr[n]; n++) {
        marker.setVisible(false);
    }
}
    
function clearInfoWindow() {    
    for(var i=0; i<infoWindow_arr.length; i++){
        infoWindow_arr[i].close();
    }
}



function clearMakeMarkers() {
    for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
        makeMarker.setVisible(false);
    }
}

</script>
<style type="text/css">
.unselected {
    background: none repeat scroll 0 0 #FFFFFF;
    padding: 4px 0;
    float: left;
}
.selected {
    background: none repeat scroll 0 0 #DBEDFE;   
    padding: 4px 0;
    float: left;
}
.latlon {
    font-size: 11px;
}
.subtit_info {
    font-weight: bold;
    width: 80px;
}
.det_info {
    color: #666666;
}
.sidebar_item {
    float: left;
    padding: 5px 0;
    border-bottom: 1px solid #DDDDDD;
}
.ipt{
    border:none;
    color:#0086C3;
}
.ipt:focus{
    border:1px solid #0086C3;
}
body{
    font-family: Arial;
    font-size: 11px;
}
.ui-layout-pane {
    background: #FFF; 
    border: 1px solid #BBB; 
    padding: 7px; 
    overflow: auto;
} 

.ui-layout-resizer {
        background: #DDD; 
} 

.ui-layout-toggler {
        background: #AAA; 
}
.label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
.l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
.l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
.slt{
    border:1px solid #1E9EF9;
    background-color: #FAFAFA;
    height: 18px;
    font-size: 11px;
    color:#0B0E9D;
}
.box_checkboxes{
    border:1px solid #4C5E60;
    background: #E7F7F7;
    width:170px;
    height: 240px;
    overflow: auto;
    display:none;
    position: absolute;
    top:0px;
    left:0px;
    opacity:0.9;
}
.tb_data_box{
    width: 100%;
    border-collapse:collapse;
    font-family: Arial;
    font-size: 9px;
}
.showResult {
    position: fixed; 
    bottom: 0; 
    right: 0; 
    padding: 8px; 
    border: 1px solid #000000; 
    margin: 2px; 
    color: #FFFFFF;
    font-size: 12px;
}
.divOpacity {
    background-color: #000000;
    -moz-opacity: 0.5;
    opacity:.50;
    filter: alpha(opacity=50);
}

#divAverias, #clear {
    cursor: pointer;
}



table.tablesorter {
    background-color: #CDCDCD;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 8pt;
}
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #E6EEEE;
    border: 1px solid #FFFFFF;
    font-size: 8pt;
}
.headTbl {
    background-image: url("../../../img/bg_head_js.gif");
    /*background-position: center top;*/
    background-repeat: repeat-x;
    color: #000000;
    height: 20px;
    font-weight: normal;
}
table.tablesorter tbody td {
    background-color: #FFFFFF;
    color: #3D3D3D;
    vertical-align: middle;
    font-size: 10px;
}

#divEstado span {
    display: none;
    margin-left: 1px;
    padding: 2px 2px;
    width: 80px;
}
#divEstado:hover span {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #004350;
    color: #6C6C6C;
    display: inline;
    position: absolute;
}
#divEstado span p {
    padding: 0;
    margin:0;
    cursor: pointer;
}
#divEstado span p:hover {
    background: none repeat scroll 0 0 #DBEDFE;
}
        
</style>
<script>
	$(function() {
            $("#btnFiltrar, #btnGrabar").button();
	});
	</script>
    </head>
    <body>
    <input type="hidden" value="1" name="emisor" id="emisor" />
    <div class="ui-layout-west">
	

		<div id="divPoligono">
	
			<table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
			<tr>
				<td colspan="2"><span class="l_tit">Posibles Clientes</span></td>
			</tr>
			<tr>
				<td align="left" class="label">Zonal:</td>
				<td align="left">
					<select name="zonal" id="zonal" style="font-size:11px;">
					<?php 
						foreach( $_SESSION['USUARIO_ZONAL'] as $obj_zonal ) {
						$sel = "";
						if( $obj_zonal->__get('abv_zonal') == 'LIM' ) {
							$sel = " selected ";
						}
					?>
						<option value="<?php echo $obj_zonal->__get('abv_zonal')?>" <?php echo $sel?>><?php echo $obj_zonal->__get('abv_zonal') . ' - ' . $obj_zonal->__get('desc_zonal')?></option>
					<?php 
						}
					?>
					</select>
					
					<span id="callback"></span>
				</td>
			</tr>
			<tr>
				<td align="left" class="label">Mdf:</td>
				<td align="left">
                                    <div id="divMdf">
					<select name="mdf" id="mdf" class="clsMultiple" multiple="multiple">
					<?php 
                                            foreach( $mdf_arr as $mdf ) {
					?>
                                            <option value="<?php echo $mdf['IDmdf']?>"><?php echo $mdf['IDmdf'] . '-' .  $mdf['Descmdf']?></option>
					<?php 
                                            }
					?>
					</select>
                                    </div>
				</td>
			</tr>
                        <tr>
				<td align="left" class="label">Con x,y:</td>
				<td align="left">
                                    <select id="selDatosXY" style="font-size:11px;">
                                        <option value="">Todos</option>
                                        <option value="conXY">con X,Y</option>
                                        <option value="sinXY">sin X,Y</option>
                                    </select>
				</td>
			</tr> 
                            
			
			<tr>
				<td colspan="2">
                                    <input type="checkbox" name="poligono_mdf" id="poligono_mdf"> Mostrar Mdf 
                                </td>
			</tr>
			<tr>
                            <td>
                                <div style="float:left;"><button id="btnFiltrar">Buscar</button></div>  
                                <div id="box_loading" style="float:left; margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                            </td>
                            <td align="right">
                                <button id="btnGrabar" onclick="GrabarDatos()">Grabar Datos</button>
                                <input type="hidden" id="trm_selected" readonly="readonly">
                            </td>
			</tr>
			</table>
			
		</div>

		<div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">
			
                        <div id="divInfoResult" style="margin-bottom: 8px; width: 385px;">
				<div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
				<div style="float:right;">
                                    <div id="divEstado" style="line-height: 20px; background-image: url(../../../img/asc.gif); background-repeat: no-repeat; background-position: left center; padding-left: 20px; color:#0099CC; width: 80px;">
                                        <div id="btn_estado_lbl" style="display: block;">Cambiar estado</div>
                                        <span>
                                            <p onclick="CambiarEstado('S')">
                                                <img width="12" border="0" src="../../../img/map/posiblesclientes/posiblecliente-1.gif"> Estado S
                                            </p>
                                            <p onclick="CambiarEstado('N')">
                                                <img width="12" border="0" src="../../../img/map/posiblesclientes/posiblecliente-3.gif"> Estado N
                                            </p>
                                        </span>
                                    </div>
                                </div>
			</div>
                        
			<div id="divTitle" style="background-color: #003366;border: 1px solid #000000;color: #FFFFFF;float: left;padding: 3px 0;width: 385px;">
				<div style="width:30px;float:left;"><input type="checkbox" name="selTodos" id="selTodos"></div>
				<div style="width:70px;float:left;">Pedido</div>
				<div style="width:240px;float:left;">Cliente</div>
				<div style="width:40px;float:left;">Prod.</div>
			</div>
			<div id="sidebar"></div>
		</div>
			
    </div>
    <div class="ui-layout-center">
        
        <div id="box_resultado">
                      
            <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>

        </div>
        
    </div>
        
	<div id="parentModal" style="display: none;">
    	<div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>