<style>
em {
    color: #ff0000;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}

table.tablesorter tbody td {
    margin: 0;
    padding: 1px;
}


table.tb_detalle_actu {
    background-color: #F2F2F2;
    font-size: 11px;
}
.tb_detalle_actu td {
    background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
    color: #FFFFFF;
    background-color: #006495;
    font-weight: bold;
    font-size: 11px;
    line-height: 20px;
    text-align: left;
    padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
    color: #FFFFFF;
    background-color: #1E9EBB;
    font-weight: bold;
    font-size: 11px;
    line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
    color: #000000;
    font-weight: bold;
    background-color: #E1E2E2;
    font-size: 10px;
}
table.tb_detalle_actu .da_import{
    font-size: 11px;
    color: red;
    font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
    font-size: 11px;
    font-weight: bold;
}
table a.link_movs{
    color: #FFFFFF;
    font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}

#pirata_image {
    float: left;
    width: 400px;
    display: block;
}
#pirata_image img{
    border: 1px solid #CACED1;
}

#pirata_thumbs {
    /*overflow: hidden;*/
    float: left;
    width: 140px;
}
#pirata_thumbs img {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    cursor: pointer;
    /*height: 75px;*/
    margin: 0 5px 5px 10px;
    padding: 2px;
    width: 70px;
}
#pirata_thumbs p{
    margin: 5px 0 0 0;
}
#pirata_thumbs span {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    margin: 30px 10px 12px 0;
    padding: 2px;
    width: 77px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

.da_editar {
    width: 90%;
}

</style>
<script type="text/javascript" src="js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="modulos/maps/js/edificios.fftt.js"></script>

<!-- Cuerpo principal -->
<div>
    <table border="0" width="95%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td valign="top">
        
        <?php 
        if ( !$acceso ) {
        ?>
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
                <td class="form_titulo" width="100%">
                    <img border="0" title="SubModulo" alt="SubModulo" src="img/plus_16.png"> &nbsp;Bandeja Nuevas Edificaciones
                </td>
            </tr>
            </table>
            
            <div style="width:100%; padding:40px 0; text-align:center;" id="divResultado" >
                Su cuenta no tiene los permisos necesarios para acceder a este modulo.<br />
                <b>Comuniquese con su administrador</b>
            </div>
        <?php    
        } else {
        ?>
            
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
                <td class="form_titulo" width="30%">
                    <img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Bandeja Nuevas Edificaciones
                </td>
                <td width="40%" align="left">
                    Gesti&oacute;n de Obras
                    <select name="gestionObra" id="gestionObra">
                        <option value="">Seleccione</option>
                        <?php
                        if ( !empty($arrObjGestionObra) ) {
                        foreach ($arrObjGestionObra as $objGestionObra) { 
                        ?>
                            <option value="<?php echo $objGestionObra->__get('_idGestionObra') ?>" ><?php echo $objGestionObra->__get('_desGestionObra') ?></option>
                        <?php
                        }
                        }
                        ?>
                    </select>
                    &nbsp;
                    Estado 
                    <select name="cmb_estado" id="cmb_estado">
                        <option value="">Seleccione</option>
                        <?php 
                        if ( !empty($arrObjEdificioEstado) ) {
                        foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
                        ?>
                        <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado')?>" ><?php echo $objEdificioEstado->__get('_desEstado')?></option>
                        <?php
                        }
                        }
                        ?>
                    </select>
                </td>
                <td width="30%" align="right">
                
                    <?php 
                    //solo SuperUser, tipo perfil = adm, Usuario que tiene empresa = 1 = TDP 
					
					// SERGIO 2013-05-08 -- QUITAR VALIDACION DE PERMISO A SUBMODULO	
                    /*if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spu' || ( $_SESSION['USUARIO']->__get('_idEmpresa') == 1 
						&& $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) ) {
                    ?>
                    <a class="hd_orden_desc" href="javascript:void(0);" onclick="cargaMasiva()">Carga Masiva</a> |
                    <?php 
                    }
					*/
					?>
					<a class="hd_orden_desc" href="javascript:void(0);" onclick="cargaMasiva()">Carga Masiva</a> |
					<?php
                    //if (!empty($_SESSION['USUARIO_ZONAL'])) {
                    if ( $flagValidoBandeja ) {
                    ?> 
                    <a class="hd_orden_desc" href="javascript:void(0);" onclick="nuevoEdificio()">Registrar Nuevo</a>
                    <?php 
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                        <div style="border-bottom:1px solid #999999;margin-bottom:3px;padding-bottom:3px;">
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                                <td width="10%" align="left">Buscar por:</td>
                                <td width="90%" align="left">
                                    <select name="campo_filtro" id="campo_filtro">
                                        <option value="f_item">ITEM</option>
                                        <option value="f_codProy">C&Oacute;DIGO PROYECTO WEB</option>
                                    </select>
                                    <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                    <input type="button" id="btn_buscar" name="buscar" value="Buscar" <?php if ( !$flagValidoBandeja ) echo 'disabled="disabled"'; ?> />
                                </td>
                            </tr>
                            </table>
                        </div>
                                            
                        <div>
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                                <td width="10%" align="left">Filtrar por:</td>
                                <td width="90%" align="left">
                                    Empresa
                                    <select name="empresa" id="empresa">
                                        <option value="">Seleccione</option>
                                        <?php 
                                        if (!empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
                                        foreach ($_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa) { 
                                        ?>
                                            <option value="<?php echo $objEmpresa->__get('_idEmpresa') ?>" ><?php echo $objEmpresa->__get('_descEmpresa') ?></option>
                                        <?php 
                                        }
                                        }
                                        ?>
                                    </select>
                                    &nbsp;
                                    Regi&oacute;n
                                    <select name="region" id="region" onchange="obtenerZonalesPorRegionUsuario();">
                                        <option value="">Seleccione</option>
                                        <?php 
                                        if (!empty($arrRegion)) {
                                        foreach ($arrRegion as $region) {
                                        ?>
                                            <option value="<?php echo $region['idregion'] ?>" ><?php echo $region['nomregion'] . ' - ' . $region['detregion'] ?></option>
                                        <?php 
                                        }
                                        }
                                        ?>
                                    </select>
                                    &nbsp;
                                    Zonal
                                    <select name="zonal" id="zonal" onchange="obtenerMdfsZonalUsuario();">
                                        <option value="">Seleccione</option>
                                        <?php 
                                        if (!empty($_SESSION['USUARIO_ZONAL'])) {
                                        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) { 
                                        ?>
                                            <option value="<?php echo $objZonal->__get('_abvZonal') ?>" ><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                                        <?php
                                        } 
                                        }
                                        ?>
                                    </select>
                                    &nbsp;
                                    URA
                                    <select name="mdf" id="mdf">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"></td>
                                <td align="left">
                                    Departamento
                                    <select name="departamento" id="departamento" onchange="provinciasUbigeoEdi();">
                                        <option value="">Todos</option>
                                    </select>
                                    &nbsp;
                                    Provincia
                                    <select name="provincia" id="provincia" onchange="distritosUbigeoEdi();">
                                        <option value="">Seleccione</option>
                                    </select>
                                    &nbsp;
                                    Distrito
                                    <select name="distrito" id="distrito">
                                        <option value="">Seleccione</option>
                                    </select>
                                    &nbsp;
                                    &nbsp;
                                    |
                                    &nbsp;
                                    &nbsp;
                                    Georeferencia
                                    <select name="georeferencia" id="georeferencia">
                                        <option value="">Todos</option>
                                        <option value="1">Con XY</option>
                                        <option value="0">Sin XY</option>
                                    </select>
                                    &nbsp;
                                    Fotos
                                    <select name="fotos" id="fotos">
                                        <option value="">Todos</option>
                                        <option value="1">Con fotos</option>
                                        <option value="0">Sin fotos</option>
                                    </select>
                                    
                                </td>
                            </tr>
                            </table>
                        </div>
                                            
                    </div>
                </td>
            </tr>
            </table>

            <div style="width: 100%;" id="divResultado" >
            
                <?php 
                if ( !$flagValidoBandeja ) {
                ?>
                    <div id="msgEdificios" style="padding: 30px 0;">
                        <p align="center">No tiene asignado <?php echo $strValidacionBandeja?> para gestionar edificios <br /><b>comun&iacute;quese con su administrador</b>.</p>
                    </div>
                <?php
                } 
                ?>
                            
                <table id="tb_sup_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                <tr>
                <td style="width:55%; height: 30px; text-align: left;">
                        Han sido listado(s) <b><span id="cont_num_resultados_head"><?php echo $totalRegistros ?></span></b> registro(s) &nbsp;|&nbsp; 
                        <?php 
                        if ( $flagValidoBandeja ) {
                        ?>
                        <a onclick="descargarEdificios();" href="javascript:void(0)">Descargar</a>
                        <a onclick="descargarEdificios();" href="javascript:void(0)"><img src="img/ico_excel.png" title="Descargar edificios unico por Item"></a> &nbsp; | &nbsp;
                        <a onclick="descargarEdificiosMov();" href="javascript:void(0)">Descargar movimientos</a>
                        <a onclick="descargarEdificiosMov();" href="javascript:void(0)"><img src="img/ico_excel.png" title="Descargar edificios y sus movimientos"></a> &nbsp;
                        <?php 
                        }
                        ?>
                        <span style="margin: 10px 0 0 0; display: none; font-size: 9px; color: #000000;" id="msgDescarga">
                            <img src="img/loading.gif"> Descargando reporte
                        </span>
                </td>
                <td style="width:45%;">
                        <div  style=" width:360px; position: relative;">
                                <div id="paginacion"></div>
                        </div>
                </td>
                </tr>
                </table>

                <input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
                <input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros;?>"/>
            
                <table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;display:none;">
                <thead>
                    <tr style="line-height:13px;" class="headFilter">
                        <th class="headTbl" width="2%">#</th>
                        <th class="headTbl" width="5%">Opciones</th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="item" orderType="asc">Item</a></th>
                        <th class="headTbl" width="11%">Tipo proyecto</th>
                        <th class="headTbl" width="9%">Proyecto</th>
                        <th class="headTbl" width="6%"><a href="javascript:void(0)" orderField="fecha_registro_proyecto" orderType="asc">Fec. reg. proy</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="zonal" orderType="asc">Zonal</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="ura" orderType="asc">URA</a></th>
                        <th class="headTbl" width="4%"><a href="javascript:void(0)" orderField="sector" orderType="asc">Sector</a></th>
                        <th class="headTbl" width="4%">Mza. Tdp</th>
                        <th class="headTbl" width="18%"><a href="javascript:void(0)" orderField="direccion_obra" orderType="asc">Direcci&oacute;n obra</a></th>
                        <th class="headTbl" width="4%">N&uacute;mero</th>
                        <th class="headTbl" width="6%">Tipo CCHH</th>
                        <th class="headTbl" width="6%">CCHH</th>
                        <th class="headTbl" width="8%"><a href="javascript:void(0)" orderField="idgestion_obra" orderType="asc">Gesti&oacute;n obra</a></th>
                        <th class="headTbl" width="5%"><a href="javascript:void(0)" orderField="estado" orderType="asc">Estado</a></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                            
                <br />
                <table id="tb_pie_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                <tr>
                   <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot"><?php echo $totalRegistros; ?></span></b> registro(s)  <br/><br/></td>
                </tr>
                </table>

            </div>
            
        <?php 
        }
        ?>

        </td>
    </tr>
    </table>
    <br />
</div>

<!-- 
<div id="footer"></div>
-->

<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
    <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->