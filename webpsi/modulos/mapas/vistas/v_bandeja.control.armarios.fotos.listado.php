<?php
if ( !empty($arrObjArmarioFotos) ) {

    $itemA = 1;
    foreach ($arrObjArmarioFotos as $objArmarioFoto) {
    ?>
        <tr>
            <td class="celdaX"><?php echo $itemA++; ?></td>
            <td class="celdaX">
                <a href="javascript:void(0)" onclick="verFotoDetalle('<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>')" title="Ver fotos">
                    <img width="11" height="11" title="Ver fotos" src="img/ico_detalle.jpg" style="margin:3px;" />
                </a>
            </td>
            <td class="celdaX"><?php echo obtenerFecha($objArmarioFoto->__get('_fechaEstado')); ?></td>
            <td class="celdaX"><?php echo $objArmarioFoto->__get('_estado'); ?></td>
            <td class="celdaX"><?php echo obtenerFecha($objArmarioFoto->__get('_fechaInsert')); ?></td>
            <td class="celdaX" style="text-align:left;">
                <?php
                if ( strlen($objArmarioFoto->__get('_observaciones')) > 50  ) {
                ?>
                    <?php echo substr($objArmarioFoto->__get('_observaciones'), 0, 50); ?><a style="color:blue;" href="javascript:void(0)" id="verMas_<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>" onclick="verMas('<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>')">m&aacute;s...</a><span style="display:none;" id="mas_<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>"><?php echo substr($objArmarioFoto->__get('_observaciones'),50);?></span> <a style="color:blue;display:none;" href="javascript:void(0)" id="ocultarMas_<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>" onclick="ocultarMas('<?php echo $objArmarioFoto->__get('_idArmarioFoto')?>')">ocultar</a>
                <?} else {?>
                    <?php echo $objArmarioFoto->__get('_observaciones');?>
                <?php
                }
                ?>
            </td>
        </tr>
    <?php
    }
}
?>