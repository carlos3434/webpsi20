<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


$array_zonal = $_SESSION['USUARIO_ZONAL'];

$obj_capasarea = new data_ffttCapasArea();
$array_capas_mdf = $obj_capasarea->get_capas_by_tipocapa($conexion, 'MDF');

$obj_pen_pais = new data_PenPais();
$array_eecc_list = $obj_pen_pais->__listarEECC($conexion);

$array_eecc = array();
foreach( $_SESSION['USUARIO_EMPRESA_DETALLE'] as $empresa_obj ) {
	foreach( $array_eecc_list as $eecc_obj ) {
		if( $eecc_obj->__get('eecc') == $empresa_obj->__get('desc_empresa') ) {
			$array_eecc[$empresa_obj->__get('idempresa')] = $eecc_obj->__get('eecc');
			break;
		}
	}
}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<!--<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>-->
<script type="text/javascript" src="../../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
<script type="text/javascript" src="../../../js/cv.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.PrintArea.js"></script>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

var poligonosDibujar_array = [];
var capa_exists = 0;

var myLayout;
var map = null;
var markers_arr = [];
var makeMarkers_arr = [];
var ptos_arr = [];

var ptos_xy_trm = [];
var ptos_xy_arm = [];

$(document).ready(function () {
	
	myLayout = $('body').layout({
		// enable showOverflow on west-pane so popups will overlap north pane
		west__showOverflowOnHover: true
	
	//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
	});

	var M = {
    	initialize: function() {

			if (typeof (google) == "undefined") {
				alert('Verifique su conexion a maps.google.com');
				return false;
			}
    	
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 11,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        }
    };
	
	$("#btnImprimirArmarioAveria").click(function() {
		var IDeecc	  = $("#selEECC option:selected").text();
		var IDzonal	  = $("#selZonal").val();
		var IDmdf  	  = $("#selMdf").val();
		var IDtipred  = $("#selTipRed").val();
		
		if( IDeecc == '' ) {
			alert("Ingrese la EECC para generar el reporte.");
			return false;
		}
	
		parent.$("#childModal").html("Cargando...");                
        $.post("../../../modulos/maps/vistas/v_imprimirArmarioAveria.php", {
            printAll 	: "Ok",
			eecc 		: IDeecc,
			zonal		: IDzonal,
			mdf			: IDmdf,
			tipo_red	: IDtipred
        },function(data){            
            parent.$("#childModal").html(data);
        });
        parent.$("#childModal").dialog({
            modal : false,
            width : '520px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Concentracion de Averias"
        });        
	});
	
	
	$("#btnImprimirArmarioAveriaTodos").click(function() {
		var IDeecc	  = $("#selEECC option:selected").text();
		var IDzonal	  = $("#selZonal").val();
		var IDmdf  	  = $("#selMdf").val();
		var IDtipred  = $("#selTipRed").val();
		
		if( IDeecc == '' ) {
			alert("Ingrese la EECC para generar el reporte.");
			return false;
		}
	
		parent.$("#childModal").html("Cargando...");                
        $.post("../../../modulos/maps/vistas/v_imprimirArmarioAveria_todos.php", {
            printAll 	: "Ok",
			eecc 		: IDeecc,
			zonal		: IDzonal,
			tipo_red	: IDtipred
        },function(data){            
            parent.$("#childModal").html(data);
        });
        parent.$("#childModal").dialog({
            modal : false,
            width : '520px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Concentracion de todas las Averias"
        });        
	});
	
	
	$("#btnImprimirMapa").click(function() {
		parent.$("#childModal").html("Cargando...");                
        $.post("../../../modulos/maps/vistas/v_gescomp_actuaciones_imprimir_mapa.php", {
            printAll 	: "Ok",
			markers_trm :  ptos_xy_trm,
			markers_arm :  ptos_xy_arm,
        },function(data){            
            parent.$("#childModal").html(data);
        });
        parent.$("#childModal").dialog({
            modal : true,
            width : '750px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Imprimir Mapa de averias"
        });        
	});
	
	$("#selEECC").change(function() {
		$("#btnImprimirArmarioAveria").attr('disabled', 'disabled');
		
		var IDeecc 	= $("#selEECC").val();
		if( IDeecc == '' ) {
			return false;
		}
		
		data_content = "action=buscarZonales&IDeecc=" + IDeecc;
        $.ajax({
            type:   "POST",
            url:    "../gescomp.actuaciones.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selectZonal = '<option value="">Seleccione</option>';
                for(var i in datos) {
                    selectZonal += '<option value="' + datos[i]['IDzonal'] + '">' + datos[i]['Desczonal'] + '</option>';
                }
              	$("#selZonal").html(selectZonal);
            }
        });

		$("#selMdf").html('<option value="">Seleccione</option>');
        $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
        $("#selCableArmario").html('<option value="">Seleccione</option>');
    });
	
	$("#selZonal").change(function() {
		$("#btnImprimirArmarioAveria").attr('disabled', 'disabled');
        var IDzon 	= this.value;
		var EECC 	= $("#selEECC").val();
        if(IDzon == "" && EECC == "") {
			alert("Debe ingresar la EECC para listar sus mdfs.");
            return false;
        }
        data_content = "action=buscarMdf&IDzon=" + IDzon + "&EECC=" + EECC;
        $.ajax({
            type:   "POST",
            url:    "../gescomp.actuaciones.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selectMdf = '';
                for(var i in datos) {
                    selectMdf += '<option value="' + datos[i]['IDmdf'] + '">' + datos[i]['IDmdf'] + ' - ' + datos[i]['Descmdf'] + '</option>';
                }
              	$("#selMdf").html(selectMdf);
            }
        });

        $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
        $("#selCableArmario").html('<option value="">Seleccione</option>');
		$("#poligono").removeAttr('disabled');
    });

	$("#selMdf").change(function() {
		$("#btnImprimirArmarioAveria").attr('disabled', 'disabled');
		var IDzonal	  = $("#selZonal").val();
		var IDmdf  	  = $("#selMdf").val();
		var IDtipred  = $("#selTipRed").val();
        if(IDzonal == "" || IDmdf == "" || IDtipred == "") {
            return false;
        }
        $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
        $("#selCableArmario").html('<option value="">Seleccione</option>');
    });

	$("#selTipRed").change(function() {
		$("#btnImprimirArmarioAveria").removeAttr('disabled');
		$("#btnImprimirArmarioAveriaTodos").removeAttr('disabled');
		var IDzonal	  = $("#selZonal").val();
		var IDmdf  	  = $("#selMdf").val();
		var IDtipred  = $("#selTipRed").val();
        if(IDzonal == "" || IDmdf == "" || IDtipred == "") {
            return false;
        }
        data_content = "action=buscarCableArmario&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&IDtipred=" + IDtipred;
        $.ajax({
            type:   "POST",
            url:    "../gescomp.actuaciones.main.php",
            data:   data_content,
            success: function(datos) {
                $("#divCableArmario").html(datos);
            }
        });
    });
	
	$("#poligono").click(function() {
    	if($("#poligono").is(':checked')) { 
			
			var IDmdf    = $("#selMdf").val();
			
			data_content = "action=buscar&area_mdf=si&IDmdf=" + IDmdf;
			$.ajax({
				type:   "POST",
				url:    "../gescomp.actuaciones.main.php",
				data:   data_content,
				dataType: "json",
				success: function(data) {
				 
					if( capa_exists ) {
						//limpiamos todas las capas del mapa
						clearPoligonos();
					}

					var poligonos = data['xy_poligono'];
					if( poligonos.length > 0 ) {
						//pintamos los poligonos
						for (var i in poligonos) {
							loadPoligono(poligonos[i], i);
						}
						
						//setea el zoom y el centro
						//map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
						//map.setZoom(13);
					}
				}
			});

        }else {  
			
			if( capa_exists ) {
				//limpiamos todas las capas del mapa
				clearPoligonos();
			}
        }  
	});
	
	$("#btnBuscar").click(function() {
        // lista y agregar los markers en divs "sidebar" y "map"
        listar_edificios();
    });


	M.initialize(); 
});

function clientesTerminal(zonal, mdf, cable, armario, caja, tipo_red) {
	$("#childModal").html("Cargando...");
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/clientes_terminal.popup.php", {
        	zonal: zonal,
	        mdf: mdf,
	        cable: cable,
	        armario: armario,
	        caja: caja,
	        tipo_red: tipo_red
	    },
    	function(data){
        	parent.$("#childModal").html(data);
    	}
	);

    parent.$("#childModal").dialog({modal:true, width:"800px", hide: "slide", title: "Clientes asignados para este terminal: " + caja, position:"top"});

}

function clearPoligonos() {
	for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
		poligonoDibujar.setMap(null);
	}
}

function loadPoligono(xy_array, z) {

	var datos 			= xy_array['coords'];
	var color_poligono 	= xy_array['color'];
	var poligonoCoords 	= [];

	if( datos.length > 0) {
		for (var i in datos) {
			var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
			poligonoCoords.push(pto_poligono);
		}

		// Construct the polygon
		// Note that we don't specify an array or arrays, but instead just
		// a simple array of LatLngs in the paths property
		poligonoDibujar = new google.maps.Polygon({
			paths: poligonoCoords,
			strokeColor: color_poligono,
			strokeOpacity: 0.6,
			strokeWeight: 3,
			fillColor: color_poligono,
			fillOpacity: 0.5
		});

		//agrego al array las capas seleccionadas
		poligonosDibujar_array.push(poligonoDibujar);

		//seteo la capa en el mapa
		poligonoDibujar.setMap(map);

		//seteamos que la capa existe
		capa_exists = 1;

	}
	else {
		//alert('aaa');
		capa_exists = 0;
	}		
}

function listar_edificios() {

	var EECC	 = $("#selEECC option:selected").text();
    var IDzonal	 = $("#selZonal").val();
    var IDmdf    = $("#selMdf").val();
    var IDtipred = $("#selTipRed").val();
    var IDcableArmario = $("#selCableArmario").val();
    var datosXY = $("#selDatosXY").val();
	
	var tipo = $("#selTipo").val();

	var area_mdf = 'no';
	if($("#poligono").is(':checked')) { 
		area_mdf = 'si';
	}

	
    if(IDzonal == "" || IDmdf == "" || IDtipred == "" || IDcableArmario == "") {
        alert("Ingrese todos los criterios de busqueda.");
        return false;
    }
	
	/**
     * makeMarker() ver 0.2
     * creates Marker and InfoWindow on a Map() named 'map'
     * creates sidebar row in a DIV 'sidebar'
     * saves marker to markerArray and markerBounds
     * @param options object for Marker, InfoWindow and SidebarItem
     */
     var infoWindow = new google.maps.InfoWindow();
     var markerBounds = new google.maps.LatLngBounds();
     var markerArray = [];
	 
          
     function makeMarker(options){
		var pushPin = null;
			/*var pushPin = new google.maps.Marker({
             map: map
         });
         pushPin.setOptions(options);*/
			
     	if( options.position != '(0, 0)' ) {
	            pushPin = new google.maps.Marker({
	                map: map
	            });
				
				makeMarkers_arr.push(pushPin);
				
	            pushPin.setOptions(options);
	            google.maps.event.addListener(pushPin, "click", function(){
	                infoWindow.setOptions(options);
	                infoWindow.open(map, pushPin);
	                if(this.sidebarButton)this.sidebarButton.button.focus();
	            });
	            var idleIcon = pushPin.getIcon();

	            markerBounds.extend(options.position);
	            markerArray.push(pushPin);
     	}else {
         	
     		pushPin = new google.maps.Marker({
                map: map
            });
			
			makeMarkers_arr.push(pushPin);
			
            pushPin.setOptions(options);
     	}
         
         if(options.sidebarItem){
             pushPin.sidebarButton = new SidebarItem(pushPin, options);
             pushPin.sidebarButton.addIn("sidebar");
         }
         
         return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         var tag = opts.sidebarItemType || "span";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "290px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }

     data_content = "action=buscarTerminales&EECC=" + EECC + "&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&IDtipred=" + IDtipred + "&IDcableArmario=" + IDcableArmario + "&datosXY=" + datosXY + "&area_mdf=" + area_mdf;
     $("#fongoLoader").show();
     //$("#btnBuscar").attr('disabled', 'disabled');
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "../gescomp.actuaciones.main.php",
         data:   data_content,
         dataType: "json",
         success: function(data) {
		 
			clearMarkers();
			clearMakeMarkers();
			
			ptos_xy_trm = [];
			ptos_xy_arm = [];
			
			
			if( capa_exists ) {
				//limpiamos todas las capas del mapa
				clearPoligonos();
			}

			var poligonos = data['xy_poligono'];

			if( poligonos.length > 0 ) {
				//pintamos los poligonos
				for (var i in poligonos) {
					loadPoligono(poligonos[i], i);
				}
				
				//setea el zoom y el centro
				map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
				//map.setZoom(13);
			}
			
			

    	 	if( data['edificios'].length > 0 ) {

    	 		if( data['edificios_con_xy'].length == 0 ) {

    	 			var datos = data['edificios']; 
    	 			var estados = data['estados'];

    	 			var x1 = y1 = null;
					if( data['armario']['x'] != null && data['armario']['y'] != null ) {
						x1 = data['armario']['x'];
    	 				y1 = data['armario']['y'];
					}
    	 			else if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
    	 				x1 = data['mdf']['x'];
    	 				y1 = data['mdf']['y'];
    	 			}else {
    	 				x1 = data['zonal']['x'];
    	 				y1 = data['zonal']['y'];
    	 			}

                	for (var i in datos) {
                		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
        				tabla += '<tr id="row' + datos[i]['averia'] + '" class="unselected">';
        				tabla += '<td width="70">';
        				tabla += '<span id="' + datos[i]['averia'] + '" onclick="ir_mapa(\'' + datos[i]['averia'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
        				for(var e in estados) {
        					//tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}

        				tabla += '<td width="70">' + datos[i]['averia'] + '</td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['averia'] + '" name="xy[' + datos[i]['averia'] + '][x]" value="' + x + '"></td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['averia'] + '" name="xy[' + datos[i]['averia'] + '][y]" value="' + y + '"></td>';
        				tabla += '<td width="20"><img border="0" title="" src="../../../img/map/terminal/terminal-1.gif" width="12"></td>';
        				tabla += '</tr>';
        				tabla += '</table>';

                        /* markers and info window contents */
                        makeMarker({
                        	draggable: false,
                            position: new google.maps.LatLng(y1, x1),
                            title: '',
                            sidebarItem: tabla,
                            //content: detalle,
                            icon: "../../../img/markergreen.png"
                        });
						
						
                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    //map.setZoom(10);
                    

    	 			
    	 		}else {

    	 		
	    	 		var datos = data['edificios']; 
	    	 		var estados = data['estados'];
	    	     	
					var num = 1;
	            	for (var i in datos) {
	
	            		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
	            		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
						
						//if( x != '' && y != '' ) {
							ptos_xy_trm.push(datos[i]['caja'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '|' + datos[i]['ccable'] + '|' + datos[i]['carmario'] + '|' + datos[i]['tipo_red'] + '|' + datos[i]['des_ave']);
						//}
	            		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
	    				tabla += '<tr id="row' + datos[i]['averia'] + '" class="unselected">';
						tabla += '<td width="50">' + num + '</td>';
	    				tabla += '<td width="70">';
	    				//tabla += '<span id="' + datos[i]['averia'] + '" onclick="ir_mapa(\'' + datos[i]['averia'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
	    				tabla += datos[i]['caja'] + '</td>';
						for(var e in estados) {
        					//tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
	    				tabla += '<td width="150"><div style="display: none;"><input type="checkbox" class="list_chk_trm" value="' + datos[i]['caja'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '"><input type="checkbox" class="lista_chk" checked="checked" name="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" id="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS" value="' + datos[i]['averia'] + '|0|Averia|||PEN_PAIS"></div>' + datos[i]['averia'] + '</td>';
	    				tabla += '<td width="120"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['averia'] + '" name="xy[' + datos[i]['averia'] + '][x]" value="' + x + '"></td>';
	    				tabla += '<td width="120"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['averia'] + '" name="xy[' + datos[i]['averia'] + '][y]" value="' + y + '"></td>';
	    				tabla += '</tr>';
	    				tabla += '</table>';

						detalle = '<b>Averia:</b> ' + datos[i]['averia'] + '<br />';
						detalle += '<b>Negocio:</b> ' + datos[i]['negocio'] + '<br />';
						detalle += '<b>Telefono:</b> ' + datos[i]['telefono'] + '<br />';
						detalle += '<b>Cliente:</b> ' + datos[i]['cliente'] + '<br />';
						detalle += '<b>Direccion:</b> ' + datos[i]['direccion'] + '<br /><br />';
						detalle += '<b>Zonal:</b> ' + datos[i]['zonal'] + '<br />';
						detalle += '<b>Mdf:</b> ' + datos[i]['mdf'] + '<br />';
						if( datos[i]['tipo_red'] == 'D' ) {
							detalle += '<b>Cable:</b> ' + datos[i]['ccable'] + '<br />';
						}else if( datos[i]['tipo_red'] == 'F' ) {
							detalle += '<b>Armario:</b> ' + datos[i]['carmario'] + '<br />';
						}
						detalle += '<b>Caja:</b> ' + datos[i]['caja'] + '<br />';
						detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
						detalle += '<a title="Registrar pedido" href="javascript:void(0);">Registrar pedido</a>';		

						
	                    /* markers and info window contents */
	                    makeMarker({
	                    	draggable: false,
	                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
	                        title: datos[i]['caja'],
	                        sidebarItem: tabla,
	                        content: detalle,
	                        icon: "../../../img/map/terminal/trm_" + datos[i]['caja'] + "--1.gif"
	                    });
						
						num++;
	                }
					
					/*
					var xMDF = yMDF = null;
    	 			if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
    	 				xMDF = data['mdf']['x'];
    	 				yMDF = data['mdf']['y'];
						
						detalleMDF = '<b>Mdf:</b> ' + data['mdf']['abv'] + ' - ' + data['mdf']['name'] + '<br />';
						detalleMDF += '<b>Zonal:</b> ' + data['mdf']['zonal'] + '<br />';
						detalleMDF += '<b>Direccion:</b> ' + data['mdf']['direccion'] + '<br /><br />';
						detalleMDF += '<b>X,Y:</b> ' + xMDF + ',' + yMDF;
						
						makeMarker({
							draggable: false,
							position: new google.maps.LatLng(yMDF, xMDF),
							title: data['mdf']['abv'] + ' - ' + data['mdf']['name'],
							content: detalleMDF,
							icon: "../../../img/map/mdf/mdf_" + data['mdf']['abv'] + "--" + data['mdf']['estado'] + ".gif"
						});
    	 			}
					*/

					var xArmario = yArmario = null;
    	 			if( data['armario']['x'] != null && data['armario']['y'] != null ) {
					
    	 				xArmario = data['armario']['x'];
    	 				yArmario = data['armario']['y'];
						
						//ptos_xy_mapa += 'armario[]=' + data['armario']['armario'] + '|' + xArmario + '|' + yArmario;
						ptos_xy_arm.push(data['armario']['armario'] + '|' + xArmario + '|' + yArmario);
						
						detalleArmario = '<b>Armario:</b> ' + data['armario']['armario'] + '<br />';
						detalleArmario += '<b>Mdf:</b> ' + data['armario']['mdf'] + '<br />';
						detalleArmario += '<b>Zonal:</b> ' + data['armario']['zonal'] + '<br />';
						detalleArmario += '<b>X,Y:</b> ' + xArmario + ',' + yArmario;
						
						/* markers and info window contents */
						makeMarker({
							draggable: false,
							position: new google.maps.LatLng(yArmario, xArmario),
							title: data['armario']['armario'],
							content: detalleArmario,
							icon: "../../../img/map/armario/arm_" + data['armario']['armario'] + "--4.gif"
						});
    	 			}
	
	                /* fit viewport to markers */
	                map.fitBounds(markerBounds);

    	 		}
    	 		
 	 		}else {
				
				var x1 = y1 = null;
				if( data['armario']['x'] != null && data['armario']['y'] != null ) {
					x1 = data['armario']['x'];
					y1 = data['armario']['y'];
				}
				else if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
					x1 = data['mdf']['x'];
					y1 = data['mdf']['y'];
				}else {
					x1 = data['zonal']['x'];
					y1 = data['zonal']['y'];
				}
				
				var latlng = new google.maps.LatLng(y1, x1);

				/* markers and info window contents */
				makeMarker({
					draggable: false,
					position: new google.maps.LatLng(y1, x1),
					title: '',
					icon: "../../../img/markergreen.png"
				});
				
				map.setCenter(latlng);
				
 	 		
 	 		}

            $("#btnBuscar").removeAttr('disabled');
            $("#btnGrabar").removeAttr('disabled');
            //setTimeout("hideLoader()", 1000);
            $("#fongoLoader").hide();
         }
     });
}

function ir_mapa(terminal_sel) {
	$("#trm_selected").val(terminal_sel);
	
	$(".unselected").attr("class", "unselected");
	$(".selected").attr("class", "unselected");
	$("#row" + terminal_sel).attr("class", "selected");

	google.maps.event.addListener(map, "click", function(event) {

		clearMarkers();
		
		marker = new google.maps.Marker({
            position: event.latLng,
            map: map, 
            title: terminal_sel
        });
		markers_arr.push(marker);
		
		if( $("#trm_selected").val() == terminal_sel ) {
			var Coords = event.latLng;
	        //alert("term sel=" + terminal_sel + ", coord=" + Coords);
			
			//var confirmar = confirm("Desea Registrar este lat, lng?");
			//if(confirmar) {
				$("#x" + terminal_sel).val(Coords.lng());
				$("#y" + terminal_sel).val(Coords.lat());
			//}	
		}
  	});
}

function clearMarkers() {
	for (var n = 0, marker; marker = markers_arr[n]; n++) {
		marker.setVisible(false);
	}
}

function clearMakeMarkers() {
	for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
		makeMarker.setVisible(false);
	}
}

function Grabar() {

	if( confirm("Esta seguro de guardar los cambios para estos terminales?") ) {
	
        IDzonal	 = $("#selZonal").val();
        IDmdf    = $("#selMdf").val();
        IDtipred = $("#selTipRed").val();
        IDcableArmario = $("#selCableArmario").val();
	
		var query = $("input.latlon").serializeArray();
		json = {};
		
		for (i in query) {
			json[query[i].name] = query[i].value;
			//alert("name=" + query[i].name + ", value=" + query[i].value);
		} 
		$("#fongoLoader").show();
        $.ajax({
            type:     "POST",
            dataType: "json",
            url:      "../gescomp.terminal.main.php?action=save&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&IDtipred=" + IDtipred + "&IDcableArmario=" + IDcableArmario,
            data:     query,
            success:  function(datos) {
        		//alert("Datos Grabados");
                //$("#divResult").html(datos);
                if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listar_edificios();
	            }
            }
        });
        $("#fongoLoader").hide();
	}
}


</script>
<style type="text/css">
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 7px; 
		overflow: auto;
	} 

	.ui-layout-resizer {
		background: #DDD; 
	} 

	.ui-layout-toggler {
		background: #AAA; 
	}
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
</head>
<body>
    <div class="ui-layout-west" style="width: 400px;">
        <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
            <tr>
                <td colspan="2"><span class="l_tit">Mantenimiento Actuaciones</span></td>
            </tr>
			<tr>
				<td>EECC:</td>
				<td>
					<select id="selEECC">
						<option value="">Seleccione</option>
						<?php foreach ($array_eecc as $ideecc => $eecc_desc) { ?>
							<option value="<?php echo $ideecc?>"><?php echo $eecc_desc?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Zonal:</td>
				<td>
					<div id="divZonal">
						<select id="selZonal">
							<option value="">Seleccione</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>Mdf:</td>
				<td>
					<div id="divMdf">
						<select id="selMdf">
							<option value="">Seleccione</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>Tipo de Red:</td>
				<td>
					<div id="divTipRed">
						<select id="selTipRed">
							<option value="">Seleccione</option>
							<option value="D">Directa</option>
							<option value="F">Flexible</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>Cable/Armario:</td>
				<td>
					<div id="divCableArmario">
						<select id="selCableArmario">
							<option value="">Seleccione</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>Con X,Y:</td>
				<td>
					<select id="selDatosXY">
						<option value="">Todos</option>
						<option value="conXY">Terminales con X,Y</option>
						<option value="sinXY">Terminales sin X,Y</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="checkbox" name="poligono" id="poligono" disabled="disabled"> Mostrar poligono(Mdf)</td>
			</tr>
            <tr>
                <td colspan="2">
                    <input type="button" id="btnBuscar" value="Buscar"> &nbsp;
					<input type="button" id="btnImprimirArmarioAveria" value="Imprimir Averias por armario" disabled="disabled"> &nbsp;
					<input type="button" id="btnImprimirArmarioAveriaTodos" value="Imprimir todas las averias" disabled="disabled"> &nbsp;
                </td>
            </tr>
        </table>
		
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="sidebarHeader">
			<table border="0" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="510">
			<tr>
				<!--<th width="70">#</th>-->
				<th width="50">#</th>
				<th width="70">Trm</th>
				<th width="150">Averia</th>
				<th width="120">X</th>
				<th width="120">Y</th>
			</tr>
			</table>
		</div>
		<div style="height:220px; width:100%; overflow:auto;">
			<div id="sidebar"></div>
		</div>
		<!--<input type="button" name="btnGrabar" id="btnGrabar" onclick="Grabar()" value="Grabar Datos" disabled="disabled"> &nbsp;-->
		<input type="button" name="btnImprimir" id="btnImprimir" onclick="imprimirSeleccionXY()" value="Imprimir Averias">
		<input type="button" name="btnImprimirMapa" id="btnImprimirMapa" value="Imprimir en Mapa">
		<input type="hidden" id="trm_selected" readonly="readonly">
		
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
        
    </div>
    <div class="ui-layout-center">
        <div id="box_resultado">

           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        </div>
        
    </div>
        
	<div id="parentModal" style="display: none;">
    	<div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>