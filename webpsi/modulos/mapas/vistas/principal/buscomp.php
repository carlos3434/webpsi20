<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<link href="../../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>

<script type="text/javascript">
if ($.browser.msie){ 
    if ($.browser.version < 8) {
      window.location="requerimientos.php";
    }
}
</script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript">


$(document).ready(function() {

    var map = null;
    markers_arr = [];
    capa_selected = '';
    capa_exists = 0;
    
    var M = {
    	initialize: function() {
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 6,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	    	
	        google.maps.event.addListener(map, "click", function(event) {
		        //clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });
        },

        clearMarkers: function() {
        	for (var n = 0, marker; marker = markers_arr[n]; n++) {
        		marker.setVisible(false);
        	}
        },

        popupComponentes: function(x, y) {

        	var zoom_selected = map.getZoom();
        	//alert('zoom=' + zoom_selected);
            
	        $("#childModal").html('');
	        $("#childModal").css("background-color","#FFFFFF");
	        $.post("principal/buscomp.popup.php", {
	    	        x: x,
	    	        y: y, 
	    	        capa_selected: capa_selected,
	    	        zoom_selected: zoom_selected
	    	    },
	        	function(data){
	            	$("#childModal").html(data);
	        	}
	    	);
	
	        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
        }

    };

    var L = {
    	loadComponentes: function() {
    	
			var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
			//var zoomMap = 6;

			var zoomMap = parseInt(capa_kml['zoom_sel']);
			//alert('zoomMap = ' + zoomMap);
			
		    if( sites.length > 0) {
				latlng = new google.maps.LatLng(punto_sel['y'], punto_sel['x']);
				//zoomMap = 15;
			}else {
				alert("No se encontro ningun punto.");
			}

		    var myOptions = {
		      	zoom: zoomMap,
		      	center: latlng,
		      	mapTypeId: google.maps.MapTypeId.ROADMAP
		    };

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

			infowindow = new google.maps.InfoWindow({
		    	content: "loading..."
		    });

			google.maps.event.addListener(map, "click", function(event) {
				//clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
				
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });

			if( sites.length > 0) {
				L.setMarkers(map, sites);
			}


			if( capa_kml['capa'] != '' ) {

				capa_exists = 1;

				//agrega el archivo kml al mapa 'map'
				var capa_sel = capa_kml['capa'] + '.kmz';
				
				ctaLayer = new google.maps.KmlLayer('http://www.webunificada.com/webunificada/modulos/maps/' + capa_sel, {suppressInfoWindows: true, preserveViewport: true});
				ctaLayer.setMap(map);
			}
			
        },
        
        setMarkers: function(map, markers) {

	  		var avg = {
		    	lat: 0,
		      	lng: 0
		    };
	
	        for (var i = 0; i < markers.length; i++) {
	            var sites = markers[i];
	            var siteLatLng = new google.maps.LatLng(sites['y'], sites['x']);

	            var ico = capa_ico[sites['tip']];
	            
				var icono = '';
				var est_caja = '';
	            if( sites['tip'] == 'trm' ) {
	            	
            		if( sites['qparlib'] == 0 ) {
            			est_caja = 3;
            		}else if( sites['qparlib'] > 0 && sites['qparlib'] < 3 ) {
            			est_caja = 2;
            		}else if( sites['qparlib'] > 2 ) {
            			est_caja = 1;
            		}
		            
		            icono = "../../../img/map/terminal/trm_" + sites['title'] + "--" + est_caja + ".gif";
	            }else if( sites['tip'] == 'arm' ) {
		            icono = "../../../img/map/armario/arm_" + sites['title'] + "--" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'mdf' ) {
		            icono = "../../../img/map/mdf/mdf_" + sites['title'] + "--" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'edi' ) {
		            icono = "../../../img/map/edificio/edificio-" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'com' ) {
		            icono = "../../../img/map/competencia/" + ico;
	            }else if( sites['tip'] == 'esb' ) {
		            icono = "../../../img/map/zonal/zonal-1.gif";
	            }else {
	            	//icono = "../../../img/map-icons-collection-2.0/icons/" + ico;
	            	icono = "../../../modulos/maps/vistas/icons/" + ico;
	            }	            
	            
	            var marker = new google.maps.Marker({
	                position: siteLatLng,
	                map: map,
	                title: sites['title'],
	                zIndex: sites['i'],
	                html: sites['detalle'],
	                //icon: "../../../img/map-icons-collection-2.0/icons/" + ico
	                icon: icono
	            });
	
	            var contentString = "Some content";
	
	            google.maps.event.addListener(marker, "click", function () {
	                //alert(this.html);
	                infowindow.setContent(this.html);
	                infowindow.open(map, this);
	            });
	            
	            //falta implementar
	            //google.maps.event.trigger(marker, "click");
	            
	            avg.lat += siteLatLng.lat();
      			avg.lng += siteLatLng.lng();
	        }
	        
	        // Center map.
    		map.setCenter(new google.maps.LatLng(avg.lat / markers.length, avg.lng / markers.length));
	        
	     }

    };

    $("#search").click(function() {
    	if($(".capas").is(':checked')) { 

        } else {  
            alert("Ingrese al menos algun criterio de busqueda");  
            return false;  
        }  
	});

    $(".capasKml").click(function() {

    	capa_selected = $(this).attr('value');
        
    	//clear markers
    	M.clearMarkers();

    	var capa = '';
        if( $(this).attr('checked') ) {
        	//alert('si');
            $(".capasKml").attr("checked", "");
            $(this).attr('checked', 'checked');

            if( $(this).attr('value') == 'distritos' ) {
				capa = 'distritos.kmz';
			}else if( $(this).attr('value') == 'nodos' ) {
				capa = 'nodos.kmz';
			}else if( $(this).attr('value') == 'mdfs' ) {
				capa = 'mdfs.kmz';
			}

        }else {
            //alert('no');
            capa_selected = '';
            $(".capasKml").attr("checked", "");
            $(this).attr('checked', '');            
        }
		//alert(capa);

    	/*var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
        var myOptions = {
          	zoom: 11,
          	center: latlng,
          	mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);*/

		if( capa == '' ) {
			ctaLayer.setMap(null);
		}else {

			if( capa_exists ) {
				ctaLayer.setMap(null);
			}
			
			//agrega el archivo kml al mapa 'map'
			ctaLayer = new google.maps.KmlLayer('http://www.webunificada.com/webunificada/modulos/maps/' + capa, {suppressInfoWindows: true, preserveViewport: true});
			ctaLayer.setMap(map);

			google.maps.event.addListener(ctaLayer, 'click', function(kmlEvent) {
		   		//alert("aaaaaa");
		   		
		   		//clear markers
			    M.clearMarkers();

		   		marker = new google.maps.Marker({
		            position: kmlEvent.latLng,
		            map: map
		        });
		    	markers_arr.push(marker);
		    	var Coords = kmlEvent.latLng;
		    	var x = Coords.lng();
		    	var y = Coords.lat();
		    	M.popupComponentes(x, y);
		    	//alert("x=" + x);
		    });

			//seteamos que la capa existe
			capa_exists = 1;
		}

		//alert('Capa cargada.');

		

	});

<?php 
if( isset($_POST['search']) ) {
?>
	var sites = <?php echo json_encode($data_markers)?>;
	var punto_sel = {x: <?php echo $_POST['x']?>, y: <?php echo $_POST['y']?>};

	var capa_ico = <?php echo json_encode($data_markers_img)?>;

	var capa_kml = <?php echo json_encode($capa_selected)?>;

	L.loadComponentes();

<?php
}
else {
?>
	//carga inicial
    M.initialize();
<?php 
}
?>

});

function clientesTerminal(zonal, mdf, cable, armario, caja, tipo_red) {
	$("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/clientes_terminal.popup.php", {
        	zonal: zonal,
	        mdf: mdf,
	        cable: cable,
	        armario: armario,
	        caja: caja,
	        tipo_red: tipo_red
	    },
    	function(data){
        	$("#childModal").html(data);
    	}
	);

    $("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Clientes asignados para este terminal: ' + caja, position:'top'});

}

function nuevasImagenesEdificio(idedificio, item, x, y) {
	//alert("edi=" + idedificio);
	$("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/agregarImagenesEdificio.popup.php", {
        	idedificio	: idedificio,
        	item		: item,
        	x		  	: x,
        	y			: y
	    },
    	function(data){
        	$("#childModal").html(data);
    	}
	);

    $("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Registrar nuevas imagenes para este edificio', position:'top'});
	
}

function editarEdificio(idedificio, item, x, y) {
	//alert('111');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/editarEdificio.popup.php", {
	    	idedificio	: idedificio,
	    	item		: item,
	    	x		  	: x,
	    	y			: y
	    },
    	function(data){
	    	parent.$("#childModal").html(data);
    	}
	);

    parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Edificio', position:'top'});
}

</script>

</head>

<body style="margin: 0px" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td><b>CAPAS KML:</b> &nbsp; 
		<input type="checkbox" class="capasKml" name="capa[1]" id="sel1" value="distritos" <?php echo ( isset($_POST['capa_selected']) && $_POST['capa_selected'] == 'distritos' ) ? 'checked' : '' ?> /> Distritos &nbsp; 
		<input type="checkbox" class="capasKml" name="capa[2]" id="sel2" value="nodos"     <?php echo ( isset($_POST['capa_selected']) && $_POST['capa_selected'] == 'nodos' ) ? 'checked' : '' ?> /> Nodos &nbsp; 
		<input type="checkbox" class="capasKml" name="capa[3]" id="sel3" value="mdfs"      <?php echo ( isset($_POST['capa_selected']) && $_POST['capa_selected'] == 'mdfs' ) ? 'checked' : '' ?> /> Mdfs &nbsp; 
	</td>
</tr>
<tr>
	<td class="form_titulo" colspan="4">
		<img border="0" src="../../../img/plus_16.png">&nbsp;
		<a href="buscomp.main.php">Busqueda de componentes</a> | 
		<a href="agregarcomp.main.php">Agregar componente</a>
	</td>
</tr>
</table>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td>
		<div id="map_canvas" style="width: 100%; height: 470px; float: left; background-color: #FFFFFF;"></div>
	</td>
</tr>
</table>

<div id="parentModal" style="display: none;">
	<div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>
</body>
</html>