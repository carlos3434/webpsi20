<?php
require_once "../../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;


if( isset($_POST['action']) && $_POST['action'] == 'buscarProv' ) {

	$obj_capas = new data_ffttCapas();

	$array_prov = $obj_capas->get_provincias_by_dpto($conexion, $_POST['IDdpto']);
	
	$data_arr = array();
	foreach ($array_prov as $prov) {
    	$data_arr[] = array(
    		'codprov' 	=> $prov['codprov'],
    		'nombre'	=> $prov['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;

}
elseif( isset($_POST['action']) && $_POST['action'] == 'buscarDist' ) {

	$obj_capas = new data_ffttCapas();

	$array_dist = $obj_capas->get_distritos_by_prov($conexion, $_POST['IDdpto'], $_POST['IDprov']);
	
	$data_arr = array();
	foreach ($array_dist as $dist) {
    	$data_arr[] = array(
    		'coddist' 	=> $dist['coddist'],
    		'nombre'	=> $dist['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'searchItem' ) {

	$obj_capas = new data_ffttCapas();
	
	$item_exists = $obj_capas->get_edificio_by_item($conexion, $_POST['item'], $_POST['item_inicial']);
	
	if( empty( $item_exists ) ) {
		$result = array(
			'success' 	=> 1,
			'msg'		=> 'Ok'
		);
	}else {
		$result = array(
			'success' 	=> 0,
			'msg'		=> 'Item existe'
		);
	}
	
	echo json_encode($result);
	exit;

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaEdificio' ) {

	$obj_capas = new data_ffttCapas();
	
	//formateando fecha para insert a db
	$_POST['fecha_registro_proyecto'] 	 = ( $_POST['fecha_registro_proyecto'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_registro_proyecto'])) : '0000-00-00 00:00:00';
	$_POST['fecha_termino_construccion'] = ( $_POST['fecha_termino_construccion'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_termino_construccion'])) : '0000-00-00 00:00:00';
	$_POST['fecha_seguimiento'] 		 = ( $_POST['fecha_seguimiento'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_seguimiento'])) : '0000-00-00 00:00:00';

	$_POST['montante_fecha'] 			= ( $_POST['montante_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['montante_fecha'])) : '0000-00-00 00:00:00';
	$_POST['ducteria_interna_fecha'] 	= ( $_POST['ducteria_interna_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['ducteria_interna_fecha'])) : '0000-00-00 00:00:00';
	$_POST['punto_energia_fecha'] 		= ( $_POST['punto_energia_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['punto_energia_fecha'])) : '0000-00-00 00:00:00';
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$data_result = $obj_capas->__updateEdificioComponente($conexion, $_POST, $idusuario);

	$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');

	
	echo json_encode($result);
	exit;
	
}
else {

	$obj_capas = new data_ffttCapas();
	$obj_terminales = new data_ffttTerminales();
	
	$tipo_cchh_list 			= $obj_capas->get_descriptor_by_grupo($conexion, 1);
	$tipo_proyecto_list 		= $obj_capas->get_descriptor_by_grupo($conexion, 2);
	$tipo_infraestructura_list 	= $obj_capas->get_descriptor_by_grupo($conexion, 3);
	$tipo_via_list 				= $obj_capas->get_descriptor_by_grupo($conexion, 4);
	
	$mdfs_array = $obj_terminales->get_mdfs_by_xy($conexion, $_POST['x'], $_POST['y'], 10, 5);
	
	$edificio = $obj_capas->get_edificio_by_id($conexion, $_POST['idedificio']);
	/*echo '<pre>';
	print_r($edificio);
	echo '</pre>';*/
	if( empty($edificio) ) {
		echo '<p>Edificio no existe</p>';
		exit;
	}
	
	
	$cod_dep 	= substr($edificio[0]['distrito'],0,2);
	$cod_prov	= substr($edificio[0]['distrito'],2,2);
	$dpto_list  = $obj_capas->get_departamentos($conexion);
	$prov_list  = $obj_capas->get_provincias_by_dpto($conexion, $cod_dep);
	$dist_list  = $obj_capas->get_distritos_by_prov($conexion, $cod_dep, $cod_prov);
	
	$estado_arr = array(
		'I' => 'Iniciado',
		'P' => 'En proceso',
		'T' => 'Terminado'
	);
	
	
	$etapa_arr = array(
		'A' => 'A',
		'B' => 'B',
		'C' => 'C',
		'D' => 'D',
		'E' => 'E',
		'F' => 'F',
		'G' => 'G',
		'H' => 'H',
		'I' => 'I',
		'J' => 'J',
		'K' => 'K',
		'L' => 'L',
		'M' => 'M',
		'N' => 'N',
		'O' => 'O',
		'P' => 'P',
		'Q' => 'Q',
		'R' => 'R',
		'S' => 'S',
		'T' => 'T',
		'U' => 'U',
		'V' => 'V',
		'W' => 'W',
		'X' => 'X',
		'Y' => 'Y',
		'Z' => 'Z'
	);
	
	
}


?>
<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(document).ready(function() {

	var item_ok = 1;

    $("#fecha_registro_proyecto").datepicker({
		yearRange:'-20:+10',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });
    $("#fecha_termino_construccion").datepicker({
		yearRange:'-20:+20',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });
    $("#fecha_seguimiento").datepicker({
		yearRange:'-20:+20',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });

    $("#montante_fecha").datepicker({
		yearRange:'-10:+10',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });
    $("#ducteria_interna_fecha").datepicker({
		yearRange:'-10:+10',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });
    $("#punto_energia_fecha").datepicker({
		yearRange:'-10:+10',
		changeYear: true,
		changeMonth: true,
		numberOfMonths: 1
    });
    
    $("#nro_blocks").change(function() {
        nro_blocks = this.value;
        if(nro_blocks == "") {
        	$("#divBlock").html('');
        }else {
	        $("#divBlock").html('');
	        selhtml = '<div style="width:180px;">';
	        selhtml += '<span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>';
	        selhtml += '<span style="float:left; width:80px;">Nro. dptos</span>';
	        selhtml += '</div>';
	        for(var i=1; i<=nro_blocks; i++) {
	        	selhtml += '<div style="width:180px;">';
	        	selhtml += '<span style="float:left; width:20px;">#' + i + ' </span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_' + i + '" id="nro_pisos_' + i + '" size="5" value="" /></span>';
	        	selhtml += '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_' + i + '" id="nro_dptos_' + i + '" size="5" value="" /></span>';
	        	selhtml += '</div>';
	        }
	        $("#divBlock").html(selhtml);
        }
    });

    $("#selDpto").change(function() {
        IDdpto = this.value;
        if(IDdpto == "") {
            return false;
        }
        data_content = "action=buscarProv&IDdpto=" + IDdpto;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/editarEdificio.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
				var selProv = ''
                for (var i in datos) {
					selProv += '<option value="' + datos[i]['codprov'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selProv").html(selProv);
                $("#selDist").html('<option value="">Seleccione</option>');
            }
        });
    });

    $("#selProv").change(function() {
    	IDdpto = $("#selDpto").val()
        IDprov = this.value;
        if(IDdpto == "" && IDprov == "") {
            return false;
        }
        data_content = "action=buscarDist&IDdpto=" + IDdpto + "&IDprov=" + IDprov;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/editarEdificio.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                
				var selDist = ''
                for (var i in datos) {
                	selDist += '<option value="' + datos[i]['coddist'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selDist").html(selDist);
            }
        });
    });
	
	$("#item, #item_etapa").change(function() {
		if( $("#item").val() == "" ) {
        	$("#msgItem").html('');
			return false;
        }
        data_content = {
			'item' 			: $("#item").val() + $("#item_etapa").val(),
			'item_inicial'	: $("#item_inicial").val()
        };
        $("#fongoLoader").show();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/editarEdificio.popup.php?action=searchItem",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                if(datos['success'] == 1) {
					//ok
   					$("#msgItem").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
					item_ok = 1;
   				}else {
					//item existe
   					$("#msgItem").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
					item_ok = 0;
   					//$("#item").val('');
   					//$("#item").focus();
   				}
                $("#fongoLoader").hide();
            }
        });
    });

    $("#Grabar").click(function() {
	
		if( !item_ok ) {
			alert("El Item que desea registrar ya existe, ingrese uno nuevo.");
			$("#item").focus();
			return false;
		}
	
        var data = {
			x	 					: $("#x").val(),
        	y	 					: $("#y").val(),
        	estado					: $("#estado").val(),
        	item					: $("#item").val(),
        	item_etapa				: $("#item_etapa").val(),
	    	fecha_registro_proyecto	: $("#fecha_registro_proyecto").val(),
	    	ura	 					: $("#ura").val(),
	    	sector	 				: $("#sector").val(),
	    	mza_tdp	 				: $("#mza_tdp").val(),
	    	idtipo_via	 			: $("#idtipo_via").val(),
	    	direccion_obra	 		: $("#direccion_obra").val(),
	    	numero	 				: $("#numero").val(),
	    	mza	 					: $("#mza").val(),
	    	lote	 				: $("#lote").val(),
	    	idtipo_cchh	 			: $("#idtipo_cchh").val(),
	    	cchh	 				: $("#cchh").val(),
	    	distrito	 			: $("#selDpto").val() + $("#selProv").val() + $("#selDist").val(),
	    	nombre_constructora	 	: $("#nombre_constructora").val(),
	    	nombre_proyecto	 		: $("#nombre_proyecto").val(),
	    	idtipo_proyecto	 		: $("#idtipo_proyecto").val(),
	    	persona_contacto	 	: $("#persona_contacto").val(),
	    	pagina_web	 			: $("#pagina_web").val(),
	    	email	 				: $("#email").val(),
	    	nro_blocks	 			: $("#nro_blocks").val(),
	    	nro_pisos_1	 			: $("#nro_pisos_1").val(),
	    	nro_dptos_1	 			: $("#nro_dptos_1").val(),
	    	nro_pisos_2	 			: $("#nro_pisos_2").val(),
	    	nro_dptos_2	 			: $("#nro_dptos_2").val(),
	    	nro_pisos_3	 			: $("#nro_pisos_3").val(),
	    	nro_dptos_3	 			: $("#nro_dptos_3").val(),
	    	nro_pisos_4	 			: $("#nro_pisos_4").val(),
	    	nro_dptos_4	 			: $("#nro_dptos_4").val(),
	    	nro_pisos_5	 			: $("#nro_pisos_5").val(),
	    	nro_dptos_5	 			: $("#nro_dptos_5").val(),
	    	idtipo_infraestructura	: $("#idtipo_infraestructura").val(),
	    	nro_dptos_habitados	 	: $("#nro_dptos_habitados").val(),
	    	avance	 				: $("#avance").val(),
	    	fecha_termino_construccion : $("#fecha_termino_construccion").val(),
	    	montante	 			: $("#montante").val(),
	    	montante_fecha	 		: $("#montante_fecha").val(),
	    	montante_obs 			: $("#montante_obs").val(),
	    	ducteria_interna	 	: $("#ducteria_interna").val(),
	    	ducteria_interna_fecha 	: $("#ducteria_interna_fecha").val(),
	    	ducteria_interna_obs 	: $("#ducteria_interna_obs").val(),
	    	punto_energia	 		: $("#punto_energia").val(),
	    	punto_energia_fecha		: $("#punto_energia_fecha").val(),
	    	punto_energia_obs 		: $("#punto_energia_obs").val(),
	    	fecha_seguimiento	 	: $("#fecha_seguimiento").val(),
	    	observacion	 			: $("#observacion").val(),
	    	idedificio				: $("#idedificio").val()
        };

        if( data['estado'] == "" ) {
			alert("Ingrese el campo Estado del Edificio.");
			return false;
        }
        if( data['item'] == "" ) {
			alert("Ingrese el campo Item.");
			return false;
        }
        if( data['fecha_registro_proyecto'] == "" ) {
			alert("Ingrese el campo Fecha Registro Proyecto.");
			return false;
        }
        if( data['ura'] == "" ) {
			alert("Ingrese el campo URA.");
			return false;
        }
        if( data['idtipo_via'] == "" ) {
			alert("Ingrese el campo Tipo Via.");
			return false;
        }
        if( data['idtipo_cchh'] == "" ) {
			alert("Ingrese el campo Tipo CCHH.");
			return false;
        }
        if( $("#selDpto").val() == "" ) {
			alert("Ingrese el campo Departamento.");
			return false;
        }
        if( $("#selProv").val() == "" ) {
			alert("Ingrese el campo Provincia.");
			return false;
        }
        if( $("#selDist").val() == "" ) {
			alert("Ingrese el campo Distrito.");
			return false;
        }
        if( data['idtipo_proyecto'] == "" ) {
			alert("Ingrese el campo Tipo Proyecto.");
			return false;
        }
        if( data['nro_blocks'] == "" ) {
			alert("Ingrese el campo Nro. de Blocks.");
			return false;
        }

        var query = $("input.clsBlocks").serializeArray();
		for (i in query) {
			if( query[i].value == "" ) {
				alert("Ingrese todos los datos en Nro. pisos y Nro. dptos.");
				$("#" + query[i].name).focus();
				return false;
			}
		} 
		
        if( data['idtipo_infraestructura'] == "" ) {
			alert("Ingrese el campo Tipo Infraestructura.");
			return false;
        }
    	
        //data_content = "action=Grabar&idcapa=" + idcapa;
        data_content = data;
        $("#fongoLoader").show();
    	//$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/editarEdificio.popup.php?action=GrabaEdificio",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);

                $("#fongoLoader").hide();
                
                if(datos['success'] == 1) {
   					$("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
   					setTimeout("$(\"#childModal\").dialog('close');", 2000);
   				}else {
   					$("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
   					setTimeout("$(\"#childModal\").dialog('close');", 2000);
   				}
            }
        });
    });
	
	$("#Cancelar").click(function() {
    	$("#childModal").dialog('close');
	});

});

function soloNumeros(obj) {
	var re = /^(-)?[0-9]*$/;
    if (!re.test(obj.value)) {
    	obj.value = '';
    }
}


</script>
</head>
<body>

<div id="fongoLoader" class="bgLoader">
	<div class="imgLoader">
		<span>Cargando...</span>
	</div>
</div>



<div id="formCapa">

<div id="divCamposEdificio">

	<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
	<tr>
		<td valign="top">
		
			<fieldset style="padding:5px;">
				<legend style="font-weight: bold;">Informaci&oacute;n principal</legend>
		
				<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
				<tr>
					<td>Estado del Edificio:</td>
					<td>
						<select name="estado" id="estado">
							<?php 
							foreach( $estado_arr as $id=>$val ) {
							$selected = "";
							if( $id == $edificio[0]['estado'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $id?>" <?php echo $selected?>><?php echo $val?></option>
							<?php 
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<?php 
						$etapa_found 	= substr($edificio[0]['item'], -1, 1);
						if( array_key_exists($etapa_found, $etapa_arr) ) {
							$item 	= substr($edificio[0]['item'], 0, -1);
							$etapa 	= substr($edificio[0]['item'], -1, 1);
						}else {
							$item 	= $edificio[0]['item'];
							$etapa 	= '';
						}
						
					?>
					<td>Item: (<b><?php echo $edificio[0]['item']?></b>)</td>
					<td>
						<input type="hidden" name="item_inicial" id="item_inicial" size="5" value="<?php echo $edificio[0]['item']?>" /> &nbsp;
						<input type="text" name="item" id="item" size="5" value="<?php echo $item?>" style="float: left;" /> &nbsp;
						<select name="item_etapa" id="item_etapa" style="width:50px; float:left; margin:0 0 0 5px;">
							<option value="">Seleccione</option>
							<?php
							foreach( $etapa_arr as $idetapa=>$etapa_str ) { 
							$sel_etapa = "";
							if( $idetapa == $etapa ) {
								$sel_etapa = "selected";
							}
							?>
							<option value="<?php echo $idetapa?>" <?php echo $sel_etapa?>><?php echo $etapa_str?></option>
							<?php 
							}
							?>
						</select>
						<span id="msgItem" style="float: left; width: 80px; margin-left: 5px;"></span>
					</td>
				</tr>
				<tr>
					<td width="150">Fecha Registro Proyecto:</td>
					<td>
						<input type="text" name="fecha_registro_proyecto" size="11" id="fecha_registro_proyecto" value="<?php echo ( $edificio[0]['fecha_registro_proyecto'] != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($edificio[0]['fecha_registro_proyecto'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>URA:</td>
					<td>
						<!-- <input type="text" name="ura" id="ura" value="" /> -->
						<select name="ura" id="ura">
							<?php 
							foreach( $mdfs_array as $mdf ) {
							$selected = "";
							if( $edificio[0]['ura'] == $mdf['mdf'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $mdf['mdf']?>" <?php echo $selected?>><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Sector:</td>
					<td>
						<input type="text" name="sector" id="sector" value="<?php echo $edificio[0]['sector']?>" />
					</td>
				</tr>
				<tr>
					<td>Mza. (TdP):</td>
					<td>
						<input type="text" name="mza_tdp" id="mza_tdp" value="<?php echo $edificio[0]['mza_tdp']?>" />
					</td>
				</tr>
				<tr>
					<td>Tipo Via:</td>
					<td>
						<select name="idtipo_via" id="idtipo_via">
							<?php 
							foreach( $tipo_via_list as $tipo_via ) {
							$selected = "";
							if( $edificio[0]['idtipo_via'] == $tipo_via['iddescriptor'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $tipo_via['iddescriptor']?>" <?php echo $selected?>><?php echo $tipo_via['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Direcci&oacute;n de Obra (Nombre de Via ):</td>
					<td>
						<input type="text" name="direccion_obra" id="direccion_obra" value="<?php echo $edificio[0]['direccion_obra']?>" />
					</td>
				</tr>
				<tr>
					<td>N&deg;:</td>
					<td>
						<input type="text" name="numero" id="numero" value="<?php echo $edificio[0]['numero']?>" />
					</td>
				</tr>
				<tr>
					<td>Mza.:</td>
					<td>
						<input type="text" name="mza" id="mza" value="<?php echo $edificio[0]['mza']?>" />
					</td>
				</tr>
				<tr>
					<td>Lote:</td>
					<td>
						<input type="text" name="lote" id="lote" value="<?php echo $edificio[0]['lote']?>" />
					</td>
				</tr>
				<tr>
					<td>Tipo CCHH:</td>
					<td>
						<select name="idtipo_cchh" id="idtipo_cchh">
							<?php 
							foreach( $tipo_cchh_list as $tipo_cchh ) {
							$selected = "";
							if( $edificio[0]['idtipo_cchh'] == $tipo_cchh['iddescriptor'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $tipo_cchh['iddescriptor']?>" <?php echo $selected?>><?php echo $tipo_cchh['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>CCHH:</td>
					<td>
						<input type="text" name="cchh" id="cchh" value="<?php echo $edificio[0]['cchh']?>" />
					</td>
				</tr>
				<tr>
					<td>Departamento:</td>
					<td>
						<select name="selDpto" id="selDpto">
							<?php 
							foreach( $dpto_list as $dpto ) {
							$selected = "";
							if( substr($edificio[0]['distrito'], 0, 2) == $dpto['coddpto'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $dpto['coddpto']?>" <?php echo $selected?>><?php echo $dpto['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Provincia:</td>
					<td>
						<div id="divProv">
							<select id="selProv">
								<?php 
								foreach( $prov_list as $prov ) {
								$selected = "";
								if( substr($edificio[0]['distrito'], 2, 2) == $prov['codprov'] ) {
									$selected = "selected";
								}
								?>
								<option value="<?php echo $prov['codprov']?>" <?php echo $selected?>><?php echo $prov['nombre']?></option>
								<?php
								}
								?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>Distrito:</td>
					<td>
						<div id="divDist">
							<select id="selDist">
								<?php 
								foreach( $dist_list as $dist ) {
								$selected = "";
								if( substr($edificio[0]['distrito'], 4, 2) == $dist['coddist'] ) {
									$selected = "selected";
								}
								?>
								<option value="<?php echo $dist['coddist']?>" <?php echo $selected?>><?php echo $dist['nombre']?></option>
								<?php
								}
								?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>Nombre de Constructora:</td>
					<td>
						<input type="text" name="nombre_constructora" id="nombre_constructora" value="<?php echo $edificio[0]['nombre_constructora']?>" />
					</td>
				</tr>
				<tr>
					<td>Nombre del proyecto:</td>
					<td>
						<input type="text" name="nombre_proyecto" id="nombre_proyecto" value="<?php echo $edificio[0]['nombre_proyecto']?>" />
					</td>
				</tr>
				<tr>
					<td>Tipo Proyecto:</td>
					<td>
						<select name="idtipo_proyecto" id="idtipo_proyecto">
							<?php 
							foreach( $tipo_proyecto_list as $tipo_proyecto ) {
							$selected = "";
							if( $edificio[0]['idtipo_proyecto'] == $tipo_proyecto['iddescriptor'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $tipo_proyecto['iddescriptor']?>" <?php echo $selected?>><?php echo $tipo_proyecto['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Persona de Contacto:</td>
					<td>
						<input type="text" name="persona_contacto" id="persona_contacto" value="<?php echo $edificio[0]['persona_contacto']?>" />
					</td>
				</tr>
				<tr>
					<td>P&aacute;gina Web:</td>
					<td>
						<input type="text" name="pagina_web" id="pagina_web" value="<?php echo $edificio[0]['pagina_web']?>" />
					</td>
				</tr>
				<tr>
					<td>E-mail:</td>
					<td>
						<input type="text" name="email" id="email" value="<?php echo $edificio[0]['email']?>" />
					</td>
				</tr>
				<tr>
					<td>N&deg; de Blocks:</td>
					<td>
						<!--<input type="text" name="nro_blocks" id="nro_blocks" value="" />-->
						<select id="nro_blocks" name="nro_blocks" style="width:80px;">
							<option value="">Seleccione</option>
							<?php 
							for($i=1;$i<=5;$i++) {
							$selected = "";
							if( $i == $edificio[0]['nro_blocks'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $i?>" <?php echo $selected?>><?php echo $i?></option>
							<?php 
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div id="divBlock">
							<?php 
							if( $edificio[0]['nro_blocks'] > 0 ) {
							?>
							<div style="width:180px;">
						        <span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>
						        <span style="float:left; width:80px;">Nro. dptos</span>
					        </div>
					        <?php 
					        for($i=1; $i<=$edificio[0]['nro_blocks']; $i++) {
					        ?>
					        	<div style="width:180px;">
					        		<span style="float:left; width:20px;">#<?php echo $i?></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_<?php echo $i?>" id="nro_pisos_<?php echo $i?>" size="5" value="<?php echo $edificio[0]['nro_pisos_' . $i . '']?>" /></span>
					        		<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_<?php echo $i?>" id="nro_dptos_<?php echo $i?>" size="5" value="<?php echo $edificio[0]['nro_dptos_' . $i . '']?>" /></span>
					        	</div>
					        <?php 
					        }
							}
					        ?>
						</div>
					</td>
				</tr>
				<tr>
					<td>Tipo de Infraestructura:</td>
					<td>
						<select name="idtipo_infraestructura" id="idtipo_infraestructura">
							<?php 
							foreach( $tipo_infraestructura_list as $tipo_infraestructura ) {
							$selected = "";
							if( $edificio[0]['idtipo_infraestructura'] == $tipo_infraestructura['iddescriptor'] ) {
								$selected = "selected";
							}
							?>
							<option value="<?php echo $tipo_infraestructura['iddescriptor']?>" <?php echo $selected?>><?php echo $tipo_infraestructura['nombre']?></option>
							<?php
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>N&deg; Dptos Habitados:</td>
					<td>
						<input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="<?php echo $edificio[0]['nro_dptos_habitados']?>" />
					</td>
				</tr>
				<tr>
					<td>% Avance:</td>
					<td>
						<input type="text" name="avance" id="avance" value="<?php echo $edificio[0]['avance']?>" />
					</td>
				</tr>
				<tr>
					<td>Fecha de Termino Construcci&oacute;n:</td>
					<td>
						<input type="text" size="11" name="fecha_termino_construccion" id="fecha_termino_construccion" value="<?php echo ( $edificio[0]['fecha_termino_construccion'] != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($edificio[0]['fecha_termino_construccion'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Montante:</td>
					<td>
						<select name="montante" id="montante" style="width:80px;">
							<option value="">Seleccione</option>
							<?php
							$sel_montante_s = ( $edificio[0]['montante'] == 'S' ) ? "selected" : "";
							$sel_montante_n = ( $edificio[0]['montante'] == 'N' ) ? "selected" : "";
							?>
							<option value="S" <?php echo $sel_montante_s?>>Si</option>
							<option value="N" <?php echo $sel_montante_n?>>No</option>
						</select> &nbsp; 
						Fecha: <input type="text" size="11" name="montante_fecha" id="montante_fecha" value="<?php echo ( $edificio[0]['montante_fecha'] != '0000-00-00 00:00:00' && $edificio[0]['montante_fecha'] != '' ) ? date('m/d/Y', strtotime($edificio[0]['montante_fecha'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Montante (Observacion):</td>
					<td>
						<textarea name="montante_obs" id="montante_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['montante_obs']?></textarea>
					</td>
				</tr>
				<tr>
					<td>Ducteria Interna:</td>
					<td>
						<select name="ducteria_interna" id="ducteria_interna" style="width:80px;">
							<option value="">Seleccione</option>
							<?php
							$sel_ducteria_s = ( $edificio[0]['ducteria_interna'] == 'S' ) ? "selected" : "";
							$sel_ducteria_n = ( $edificio[0]['ducteria_interna'] == 'N' ) ? "selected" : "";
							?>
							<option value="S" <?php echo $sel_ducteria_s?>>Si</option>
							<option value="N" <?php echo $sel_ducteria_n?>>No</option>
						</select> &nbsp; 
						Fecha: <input type="text" size="11" name="ducteria_interna_fecha" id="ducteria_interna_fecha" value="<?php echo ( $edificio[0]['ducteria_interna_fecha'] != '0000-00-00 00:00:00' && $edificio[0]['ducteria_interna_fecha'] != '' ) ? date('m/d/Y', strtotime($edificio[0]['ducteria_interna_fecha'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Ducteria Interna (Observacion):</td>
					<td>
						<textarea name="ducteria_interna_obs" id="ducteria_interna_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['ducteria_interna_obs']?></textarea>
					</td>
				</tr>
				<tr>
					<td>Punto de Energia:</td>
					<td>
						<select name="punto_energia" id="punto_energia" style="width:80px;">
							<option value="">Seleccione</option>
							<?php
							$sel_punto_energia_s = ( $edificio[0]['punto_energia'] == 'S' ) ? "selected" : "";
							$sel_punto_energia_n = ( $edificio[0]['punto_energia'] == 'N' ) ? "selected" : "";
							?>
							<option value="S" <?php echo $sel_punto_energia_s?>>Si</option>
							<option value="N" <?php echo $sel_punto_energia_n?>>No</option>
						</select> &nbsp; 
						Fecha: <input type="text" size="11" name="punto_energia_fecha" id="punto_energia_fecha" value="<?php echo ( $edificio[0]['punto_energia_fecha'] != '0000-00-00 00:00:00' && $edificio[0]['punto_energia_fecha'] != '' ) ? date('m/d/Y', strtotime($edificio[0]['punto_energia_fecha'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Punto de Energia (Observacion):</td>
					<td>
						<textarea name="punto_energia_obs" id="punto_energia_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['punto_energia_obs']?></textarea>
					</td>
				</tr>
				<tr>
					<td>Fecha de seguimiento:</td>
					<td>
						<input type="text" size="11" name="fecha_seguimiento" id="fecha_seguimiento" value="<?php echo ( $edificio[0]['fecha_seguimiento'] != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($edificio[0]['fecha_seguimiento'])) : '';?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Observaci&oacute;n:</td>
					<td>
						<textarea name="observacion" id="observacion" style="width:200px;height:50px;"><?php echo $edificio[0]['observacion']?></textarea>
					</td>
				</tr>
				</table>
			</fieldset>
			
		</td>
		<td valign="top">
		
			<fieldset style="padding: 5px;">
				<legend style="font-weight: bold;">Informaci&oacute;n adicional</legend>
		
				<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
				<tr>
					<td width="150">Tipo de trabajo programado:</td>
					<td>
						<select name="idtipo_trabajo_programado" id="idtipo_trabajo_programado">
							<option value="">Seleccione</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Estado de env&iacute;o a dise&ntilde;o:</td>
					<td>
						<select name="idestado_envio_diseno" id="idestado_envio_diseno">
							<option value="">Seleccione</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Fecha de env&iacute;o a Dise&ntilde;o:</td>
					<td>
						<input type="text" size="11" name="fecha_envio_diseno" id="fecha_envio_diseno" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Zona Residencial:</td>
					<td>
						<input type="text" name="zona_residencial" id="zona_residencial" value="" />
					</td>
				</tr>
				<tr>
					<td>Segmento Constructora:</td>
					<td>
						<input type="text" name="segmento_constructora" id="fecha_envio_diseno" value="" />
					</td>
				</tr>
				<tr>
					<td>Grupo Asignado:</td>
					<td>
						<input name="grupo_asignado" id="grupo_asignado" value="" />
					</td>
				</tr>
				<tr>
					<td>Gesti&oacute;n de Obras:</td>
					<td>
						<input type="text" name="gestion_obras" id="gestion_obras" value="" />
					</td>
				</tr>
				<tr>
					<td>Prioridad:</td>
					<td>
						<input type="text" name="prioridad" id="prioridad" value="" />
					</td>
				</tr>
				<tr>
					<td>Quincena:</td>
					<td>
						<input type="text" name="quincena" id="quincena" value="" />
					</td>
				</tr>
				<tr>
					<td>Etapa:</td>
					<td>
						<input type="text" name="etapa" id="etapa" value="" />
					</td>
				</tr>
				<tr>
					<td>Fecha cableado real:</td>
					<td>
						<input type="text" size="11" name="fecha_cableado_real" id="fecha_cableado_real" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>OT B&aacute;sica:</td>
					<td>
						<input type="text" name="ot_basica" id="ot_basica" value="" />
					</td>
				</tr>
				<tr>
					<td>OT CATV:</td>
					<td>
						<input type="text" name="ot_catv" id="ot_catv" value="" />
					</td>
				</tr>
				<tr>
					<td>Fin de etapa actual:</td>
					<td>
						<input type="text" name="fin_etapa_actual" id="fin_etapa_actual" value="" />
					</td>
				</tr>
				<tr>
					<td>Fin cableado programado:</td>
					<td>
						<input type="text" size="11" name="fin_cableado_programado" id="fin_cableado_programado" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Bandeja:</td>
					<td>
						<input type="text" size="11" name="bandeja" id="bandeja" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Indicencia:</td>
					<td>
						<input type="text" size="11" name="indicencia" id="indicencia" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Estado edificio:</td>
					<td>
						<input type="text" size="11" name="estado_edificio" id="estado_edificio" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Observaci&oacute;n Obras:</td>
					<td>
						<input type="text" name="observacion_obras" id="observacion_obras" value="" />
					</td>
				</tr>
				</table>	
			
			</fieldset>
			
		</td>
	</tr>
	</table>

	<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
	<tr>
		<td colspan="2" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" name="Grabar" id="Grabar" value="Grabar Datos" />
			<input type="button" id="Cancelar" value="Cancelar" />
		</td>
	</tr>
	</table>
	

</div>

<div id="divMsgFormCapa"></div>
	
</div>

<input type="hidden" name="idedificio" id="idedificio" value="<?php echo $_POST['idedificio']?>" />
<input type="hidden" name="x" id="x" value="<?php echo $edificio[0]['x']?>" />
<input type="hidden" name="y" id="y" value="<?php echo $edificio[0]['y']?>" />

</body>
</html>



