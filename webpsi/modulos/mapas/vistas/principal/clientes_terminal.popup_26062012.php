<?php

/**
 * @package     modulos/maps/
 * @name        clientes_terminal.popup.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 */

require_once "../../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


$objTerminales = new Data_FfttTerminales();

$arrClientes = $objTerminales->getClientesByTerminal($conexion, $_POST['zonal'], $_POST['mdf'], $_POST['cable'], $_POST['armario'], $_POST['caja'], $_POST['tipo_red']);

?>
<script type="text/javascript">
$(document).ready(function() {
    $("#Cerrar").click(function() {
        $("#childModal").dialog('close');
    });
});
function cerrarPopup() {
    $("#childModal").dialog('close');
}
</script>
<link href="css/maps.css" rel="stylesheet" type="text/css"/>

<div style="height:350px; overflow:auto;">
<table class="tableBorder" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
<tr>
    <td style=" background-color: #70C72A;color: #FFFFFF;font-size: 11px;font-weight: bold; line-height: 25px; text-align:center;" colspan="12">CLIENTES ASOCIADOS A ESTE TERMINAL: <?php echo $_POST['caja']?></td>
</tr>
<tr>
    <th width="3%" style="text-align:center;">#</th>
    <th width="5%" style="text-align:center;">Par</th>
    <th width="25%" style="text-align:left;">Cliente</th>
    <th width="28%" style="text-align:left;">Direccion</th>
    <th width="8%" style="text-align:center;">Circuito</th>
    <th width="8%" style="text-align:center;">Telefono</th>
    <th width="10%" style="text-align:center;">Estado</th>
    <th width="5%" style="text-align:center;">Speedy</th>
    <th width="8%" style="text-align:center;">Producto</th>
</tr>
<?php 
if ( !empty($arrClientes) ) {

    $i = 1;
    foreach ( $arrClientes as $cliente ) {
    ?>
    <tr>
        <td style="text-align:center;"><?php echo $i?></td>
        <td style="text-align:center;"><?php echo ($cliente['Par'] != '') ? $cliente['Par'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Cliente'] != '') ? $cliente['Cliente'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Direccion'] != '') ? $cliente['Direccion'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Circuito'] != '') ? $cliente['Circuito'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Telefono'] != '') ? $cliente['Telefono'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['DescEstadoPar'] != '') ? $cliente['DescEstadoPar'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['marca_spd'] == 'S') ? 'SI' : 'NO'?></td>
        <td style="text-align:center;"><?php echo ($cliente['PRODUCTO'] != '') ? $cliente['PRODUCTO'] : '&nbsp;'?></td>
    </tr>
    <?php 
    $i++;
    }
} else {
?>
    <tr>
        <td colspan="7" style="text-align:center; height:50px;">No se encontro ningun cliente para este terminal.</td>
    </tr>
<?php 
}
?>
</table>
</div>

<div></div>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
    <td colspan="2">
        <input type="button" id="Cerrar" value="Cerrar" />
    </td>
</tr>
</table>