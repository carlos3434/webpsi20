<?php
//require_once "../../../../config/web.config.php";

include_once '../js.php';

require_once APP_DIR . 'autoload.php';
//require_once APP_DIR . 'session.php';



global $conexion;


$obj_capas = new data_ffttCapas();

//$array_capas = $obj_capas->get_capas($conexion);
$array_capas = $_SESSION['USUARIO_CAPAS'];

?>
<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
$(document).ready(function() {
	$("#search").click(function() {
    	if($(".capas").is(':checked')) { 
        } else {  
            alert("Ingrese algun criterio de busqueda");  
            return false;  
        }  
	});
	$("#cancelar").click(function() {
    	$("#childModal").dialog('close');
	});
	$("#sel").click(function() {
    	if($("#sel").is(':checked')) { 
    		$(".capas").attr('checked', 'checked');
    		$("#selTodos").html('Quitar todos');
        }else {  
        	$(".capas").attr('checked', '');
        	$("#selTodos").html('Seleccionar todos');
        }  
	});
	
});
</script>
</head>
<body>

<h3>X,Y seleccionado:</h3>
<?php echo $_POST['x'] . ',' . $_POST['y'] ?>
<br />&nbsp;

<form method="post" action="">
<h3>Seleccione las capas que desea buscar:</h3>
<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td width="30"><input type="checkbox" class="capas" id="sel" value="" /></td>
	<td><span id="selTodos">Seleccionar todos</span></td>
</tr>
<!-- 
<tr>
	<td><input type="checkbox" class="capas" name="comp[mdf]" value="mdf" /></td>
	<td>MDFS</td>
</tr>
<tr>
	<td><input type="checkbox" class="capas" name="comp[arm]" value="arm" /></td>
	<td>ARMARIOS</td>
</tr>
<tr>
	<td><input type="checkbox" class="capas" name="comp[trm]" value="trm" /></td>
	<td>TERMINALES</td>
</tr>
<tr>
	<td colspan="2"><hr /></td>
</tr>
-->
<?php
if( !empty($array_capas) ) {
foreach($array_capas as $obj_capa) {
	if( $obj_capa->__get('tiene_tabla') == 'S' ) {
	?>
		<tr>
			<td><input type="checkbox" class="capas" name="comp[<?php echo $obj_capa->__get('abv_capa')?>]" value="<?php echo $obj_capa->__get('abv_capa')?>" /></td>
			<td><?php echo $obj_capa->__get('nombre')?></td>
		</tr>
	<?php
	}else {
	?>
		<tr>
			<td><input type="checkbox" class="capas" name="capa[<?php echo $obj_capa->__get('idcapa')?>]" value="<?php echo $obj_capa->__get('abv_capa')?>" /></td>
			<td><?php echo $obj_capa->__get('nombre')?></td>
		</tr>
	<?php 
	}
}
}
?>
<tr>
	<td colspan="2" height="20">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" name="search" id="search" value="Buscar componentes" />
		<input type="button" id="cancelar" value="Cancelar" />
	</td>
</tr>
</table>
<input type="hidden" name="x" id="x" value="<?php echo $_POST['x']?>" />
<input type="hidden" name="y" id="y" value="<?php echo $_POST['y']?>" />
<input type="hidden" name="capa_selected" id="capa_selected" value="<?php echo $_POST['capa_selected']?>" />
<input type="hidden" name="zoom_selected" id="zoom_selected" value="<?php echo $_POST['zoom_selected']?>" />
</form>

</body>
</html>






