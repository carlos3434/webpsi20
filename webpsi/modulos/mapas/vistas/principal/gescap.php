<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<link href="../../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />
<link href="../../../css/style.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.paginate.js"></script>

<script type="text/javascript">
if ($.browser.msie){ 
    if ($.browser.version < 8) {
      window.location="requerimientos.php";
    }
}
</script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>


<script type="text/javascript">
$(document).ready(function(){

	$("#Buscar").click(function() {
		buscar();
	});

	$("#nombre").keyup(function(e) {
        if(e.keyCode == 13) {
			buscar();
        }
    });

	$("#nuevo").click(function() {
		$("#childModal").html('');
	    $("#childModal").css("background-color","#FFFFFF");
	    $.post("principal/gescap.popup.php", {
		        action: 'nuevo'
		    },
	    	function(data){
		    	parent.$("#childModal").html(data);
	    	}
		);

	    parent.$("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Nueva Capa', position:'top'});
			
	});

});

function buscar() {
	var nombre 	 = $("#nombre").val();
	$("#fongoLoader").show();
	$("#pag_actual").attr("value", 1);
	data_content = "action=cambiarPaginaCapas&nombre=" + nombre;
    $.ajax({
        type:   "POST",
        url:    "gescap.main.php",
        data:   data_content,
        success: function(datos) {
			
        	var data = datos.split("||");
				$(".tb_listado").empty();
            $(".tb_listado").append(data[0]);

            $("#paginacion").html('');
            $("#cont_num_resultados_head").html(data[1]);
            $("#fongoLoader").hide();
        }
    });
}

function editCapa(idcapa) {
	//var idcapa = $(this).attr("id");

	$("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/gescap.popup.php", {
	        idcapa: idcapa,
	        action: 'editar'
	    },
    	function(data){
        	parent.$("#childModal").html(data);
    	}
	);

    parent.$("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Editar Capa', position:'top'});

}
</script>
</head>

<body style="margin: 0px; background: #FFFFFF;" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="95%">
<tr>
	<td width="100" valign="top">
	
		<?php //include 'vistaMenu.php'; ?>
		
		<table id="tblSearch" border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
		<tr>
			<td class="form_titulo" colspan="4">
				<img border="0" src="../../../img/plus_16.png">&nbsp;Mantenimiento de Capas
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="height: 5px"></div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<fieldset class="fld" style="width:35%; padding:5px;">
					<legend class="lgd">Buscar por </legend>
		            <table>
			        <tr>
		            	<td colspan="2" style="font-weight: bold; text-align: left">Descripcion &nbsp;
		                	<input type="text" id="nombre" name="nombre" size="20"/>
		                    <input type="submit" id="Buscar" name="Buscar" value="Buscar"/>
		                </td>
		            </tr>
		            </table>
		        </fieldset>
				
				<?php if($total_registros > 0):?>
				<table id="tb_sup_tl_registros" style="margin-left:0px;" border="0" width="90%" cellspacing="1" cellpadding="0" align="center">
			    <tr>
			        <td style="width:55%; height: 30px; text-align: left;">
			            Registros encontrados : <b><span id="cont_num_resultados_head"><?php echo $total_registros ?></span> capa(s)</b>
			        </td>
			        <td style="width:45%;">
			            <div  style=" width:360px; position: relative;">
			                <div id="paginacion"></div>
			            </div>
			        </td>
			    </tr>
				</table>
				<?php endif;?>
				
				<table style="margin-left:0px;" border="0" width="90%" cellspacing="1" cellpadding="0" align="center">
				<tr>
			    	<td>
			    		<input type="button" id="nuevo" value="Agregar nueva capa" style="float: left;" /> &nbsp;
			    		<span id="msgUploadCapa" style="float: left;color:#ff0000;"><?php print_r($errors);?></span>
			    	</td>
			    </tr>
			    </table>
				
				<input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
				<input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $total_registros;?>"/>
				
			
			</td>
		</tr>
		</table>
		
		
		
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="divResult">
			<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">
			<thead>
			<tr>
				<th>#</th>
				<th height="24">Capa</th>
				<th>Abreviatura</th>
				<th>Ico</th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php 
			$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
			foreach( $array_capas as $capa ) {
			?>
			<tr>
				<td><?php echo $item?></td>
				<td height="24"><?php echo $capa['nombre']?></td>
				<td><?php echo $capa['abv_capa']?></td>
				<td><img width="25" src="icons/<?php echo $capa['ico']?>" /></td>
				<td>
					<a href="javascript:editCapa(<?php echo $capa['idcapa']?>)"><img title="Editar capa" src="../../../img/sigma_icon_edit.png" /></a>
				</td>
			</tr>
			<?php 
				$item++;
			}
			?>
			</tbody>
			</table>
		
		</div>

	</td>
</tr>
</table>

<!-- 
<div id="parentModal" style="display: none;">
	<div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>
 -->

<script type="text/javascript">
$(function() {

	$("#paginacion").paginate({
    	count                   : <?php echo $num_paginas?>,
        start                   : 1,
        display                 : 10,
        border                  : true,
        border_color            : "#FFFFFF",
        text_color              : "#FFFFFF",
        background_color        : "#0080AF",
        border_hover_color      : "#CCCCCC",
        text_hover_color        : "#000000",
        background_hover_color  : "#FFFFFF",
        images                  : false,
        mouse                   : "press",
        onChange : function(page){
    		$("#fongoLoader").show();
            $("#pag_actual").attr("value", page);
            var tl=$("#tl_registros").attr("value");
            $.post("gescap.main.php?action=cambiarPaginaCapas", { pagina: page, total: tl }, function(data){
	            $(".tb_listado").empty();
	            $(".tb_listado").append(data);
	            $("#fongoLoader").hide();
      		});
       	}
	});
});
</script>


</body>
</html>