<?php

/**
 * @package     modulos/maps/vistas/principal/
 * @name        agregarcomp.popup.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/25
 */

require_once "../../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


if ( isset($_POST['action']) && $_POST['action'] == 'buscarProv' ) {

    $objCapas = new Data_FfttCapas();

    $arrayProv = $objCapas->getProvinciasByDpto($conexion, $_POST['IDdpto']);
    
    $dataArray = array();
    foreach ($arrayProv as $prov) {
        $dataArray[] = array(
            'codprov'   => $prov['codprov'],
            'nombre'    => $prov['nombre']
        );
    }
    echo json_encode($dataArray);
    exit;

} elseif ( isset($_POST['action']) && $_POST['action'] == 'buscarDist' ) {

    $objCapas = new Data_FfttCapas();

    $arrayDist = $objCapas->getDistritosByProv(
        $conexion, $_POST['IDdpto'], $_POST['IDprov']
    );
    
    $dataArray = array();
    foreach ($arrayDist as $dist) {
        $dataArray[] = array(
            'coddist'   => $dist['coddist'],
            'nombre'    => $dist['nombre']
        );
    }
    echo json_encode($dataArray);
    exit;

} elseif ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaEdificio' ) {

    $etapaArray = array(
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
        'F' => 'F',
        'G' => 'G',
        'H' => 'H',
        'I' => 'I',
        'J' => 'J',
        'K' => 'K',
        'L' => 'L',
        'M' => 'M',
        'N' => 'N',
        'O' => 'O',
        'P' => 'P',
        'Q' => 'Q',
        'R' => 'R',
        'S' => 'S',
        'T' => 'T',
        'U' => 'U',
        'V' => 'V',
        'W' => 'W',
        'X' => 'X',
        'Y' => 'Y',
        'Z' => 'Z'
    );
    

    $objCapas = new Data_FfttCapas();
    
    //formateando fecha para insert a db
    $_POST['fecha_registro_proyecto']    = ( $_POST['fecha_registro_proyecto'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_registro_proyecto'])) : '0000-00-00';
    $_POST['fecha_termino_construccion'] = ( $_POST['fecha_termino_construccion'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_termino_construccion'])) : '0000-00-00';
    $_POST['fecha_seguimiento']          = ( $_POST['fecha_seguimiento'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_seguimiento'])) : '0000-00-00';
    
    $_POST['montante_fecha']             = ( $_POST['montante_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['montante_fecha'])) : '0000-00-00';
    $_POST['ducteria_interna_fecha']     = ( $_POST['ducteria_interna_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['ducteria_interna_fecha'])) : '0000-00-00';
    $_POST['punto_energia_fecha']        = ( $_POST['punto_energia_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['punto_energia_fecha'])) : '0000-00-00';
    
    
    $codExisteAutogenerado = $objCapas->getExisteCodigoAutogeneradoEdificios($conexion);
    if ( empty($codExisteAutogenerado) ) {
        //item por default para inicio de ingreso de nuevos edificios
        $item = '50001';
    } else {
        $etapaFound = substr($codExisteAutogenerado[0]['max_item'], -1, 1);
        if ( array_key_exists($etapaFound, $etapaArray) ) {
            $item  = substr($codExisteAutogenerado[0]['max_item'], 0, -1);
            $etapa = substr($codExisteAutogenerado[0]['max_item'], -1, 1);
        } else {
            $item  = $codExisteAutogenerado[0]['max_item'];
            $etapa = '';
        }
    }
    
    $_POST['fecha']   = date('Y-m-d');
    $_POST['item']    = $item + 1;
    

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $lastID = $objCapas->insertEdificio($conexion, $_POST, $idUsuario);

    if ( $lastID ) {
        $result = array('success'=>1, 'msg'=>'Datos grabados correctamente.');
        
        if ( $_POST['archivo1'] != '' || $_POST['archivo2'] != '' || 
            $_POST['archivo3'] != '' || $_POST['archivo3'] != '' ) {
        
            $_POST['idedificio'] = $lastID;
            $arrDataResult = $objCapas->insertEdificioFoto(
                $conexion, $_POST, $idUsuario
            );
    
            $result = ($arrDataResult) ? 
                array('success'=>1, 'msg'=>'Datos grabados correctamente.') : 
                array('success'=>0, 'msg'=>'Error al grabar los datos.');
        }
        
    } else {
        $result = array('success'=>0, 'msg'=>'Error al grabar los datos.');
    }
    
    echo json_encode($result);
    exit;
    
} elseif ( isset($_REQUEST['action']) && 
    $_REQUEST['action'] == 'GrabaCompetencia' ) {

    $objCapas = new Data_FfttCapas();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $arrDataResult = $objCapas->insertCompetencia(
        $conexion, $_POST, $idUsuario
    );

    $result = ($arrDataResult) ? 
        array('success'=>1, 'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0, 'msg'=>'Error al grabar los datos.');

    echo json_encode($result);
    exit;
    
} elseif ( isset($_REQUEST['action']) && 
    $_REQUEST['action'] == 'GrabaDunaAdsl' ) {
    try {
    $objDunaAdsl = new Data_DunaAdsl();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
    //Estados duna adsl
    //01 DETECTADO RECEPTOR
    //03 DETECTADO EMISOR CON FOTO
    //04 DETECTADO EMISOR SIN FOTO
    if ( $_POST['tipo_antena'] == 'RECEPTOR' ) {
        $_POST['estado'] = '01';
    } else {
        if ( $_POST['foto1'] == '' ) {
            $_POST['estado'] = '04';
        } else {
            $_POST['estado'] = '03';
        }
    }
    
    $arrDataResult = $objDunaAdsl->insertarDunaAdsl(
        $conexion, $_POST, $idUsuario
    );

    $result = ($arrDataResult) ? 
        array('success'=>1, 'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0, 'msg'=>'Error al grabar los datos.');

    echo json_encode($result);
    exit;
    
    }catch(PDOException $e) {
       
        $result = array('success'=>0, 'msg'=>'Error al grabar los datos.');
        echo json_encode($result);
        exit;
    }
    
} elseif ( isset($_REQUEST['action']) && 
    $_REQUEST['action'] == 'GrabaEstacionBase' ) {

    $objCapas = new Data_FfttCapas();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $arrDataResult = $objCapas->insertEstacionBase(
        $conexion, $_POST, $idUsuario
    );

    $result = ($arrDataResult) ? 
        array('success'=>1,'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0,'msg'=>'Error al grabar los datos.');

    echo json_encode($result);
    exit;
    
} elseif ( isset($_POST['action']) && $_POST['action'] == 'GrabaComponente' ) {

    $objCapas = new Data_FfttCapas();
    
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    //capturo los campos ingresados a traves del formulario
    $data = array();
    foreach ($_POST as $idcampo => $valcampo) { 
        if ( $valcampo != '' && (substr($idcampo, 0, 5) == 'campo') ) {
            $data[$idcampo] = $valcampo;
        }
    }
    //completando el array de datos
    $data['idcapa'] = $_POST['idcapa'];
    if ( $_POST['foto1'] != '') 
        $data['foto1']  = $_POST['foto1'];
    if ( $_POST['foto2'] != '') 
        $data['foto2']  = $_POST['foto2'];
    if ( $_POST['foto3'] != '') 
        $data['foto3']  = $_POST['foto3'];
    if ( $_POST['foto4'] != '') 
        $data['foto4']  = $_POST['foto4'];
    $data['x']      = $_POST['x'];
    $data['y']      = $_POST['y'];
    

    $arrDataResult = $objCapas->insertComponente($conexion, $data, $idUsuario);
    
    $result = ($arrDataResult) ? 
        array('success'=>1, 'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0, 'msg'=>'Error al grabar los datos.');

    echo json_encode($result);
    exit;
    
} elseif ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'searchItem' ) {

    $objCapas = new Data_FfttCapas();
    
    $edificio = $objCapas->getEdificioByItem($conexion, $_POST['item']);
    
    $result = ( !empty($edificio) ) ? 
        array('success'=>0, 'msg'=>'Item ya existe.') : 
        array('success'=>1, 'msg'=>'Ok.');

    echo json_encode($result);
    exit;

} elseif ( isset($_POST['action']) && $_POST['action'] == 'buscarCamposCapa' ) {

    $objCapas = new Data_FfttCapas();

    //carga de campos segun la capa
    $arrayCamposCapa = $objCapas->getCamposByCapa($conexion, $_POST['idcapa']);
    $arrDataCampos = array();
    foreach ( $arrayCamposCapa as $campoCapa ) {
        $arrayCamposMultiple = array();
        if ( $campoCapa['multiple'] == 'S' ) {
            $arrayCamposMultiple = $objCapas->getCamposMultipleByCapaDetalle(
                $conexion, $campoCapa['idcapa_detalle']
            );
        }
        
        $arrDataCampos[] = array(
            'campo_nro'        => $campoCapa['campo_nro'],
            'campo'            => $campoCapa['campo'], 
            'multiple'         => $campoCapa['multiple'], 
            'campos_multiple'  => $arrayCamposMultiple
        );
    }
    
    echo json_encode($arrDataCampos);
    exit;

} else {

    $objCapas = new Data_FfttCapas();
    $objTerminales = new Data_FfttTerminales();
    $objEdificioEstado = new Data_FfttEdificioEstado();

    $arrayCapas = $_SESSION['USUARIO_CAPAS'];
    
    $flagEdificios = $flagCompetencias = 0;
    $arrayAbv = array();
    if ( !empty($arrayCapas) ) {
        foreach ( $arrayCapas as $objCapa ) {
            $arrayAbv[$objCapa->__get('_idCapa')] = $objCapa->__get('_abvCapa');
        }
    
        foreach ( $arrayCapas as $objCapa ) {
            if ( $objCapa->__get('_abvCapa') == 'edi' ) {
                $flagEdificios = 1;
                break;
            }
        }
        foreach ( $arrayCapas as $objCapa ) {
            if ( $objCapa->__get('_abvCapa') == 'com' ) {
                $flagCompetencias = 1;
                break;
            }
        }
        
    }
    
    if ( $flagEdificios ) {
        $dptoList  = $objCapas->getDepartamentos($conexion);
    
        //opciones multiple -> edificio
        $arrayTipoCchh = $objCapas->getDescriptorByGrupo($conexion, 1);
        $arrayTipoCchh = $objCapas->getDescriptorByGrupo($conexion, 2);
        $arrayTipoInfraestructura = $objCapas->getDescriptorByGrupo(
            $conexion, 3
        );
        $arrayTipoVia = $objCapas->getDescriptorByGrupo($conexion, 4);
        
        $arrayMdfs = $objTerminales->getMdfsByXy(
            $conexion, $_POST['x'], $_POST['y'], 10, 5
        );
        
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );
        
        $arrayEstado = array();
        foreach ($arrObjEdificioEstado as $objEdificioEstado) {
            $arrayEstado[$objEdificioEstado->__get('_codEdificioEstado')] = $objEdificioEstado->__get('_desEstado');
        }
        
        /*$arrayEstado = array(
            'I' => 'Iniciado',
            'P' => 'En proceso',
            'T' => 'Terminado'
        );*/
    }
    
    if ( $flagCompetencias ) {
        //opciones multiple -> competencia
        $arrayCompetencia = $objCapas->getDescriptorByGrupo($conexion, 5);
        $arrayElementoEncontrado = $objCapas->getDescriptorByGrupo(
            $conexion, 6
        );
        
        $arrayMdfs = $objTerminales->getMdfsByXy(
            $conexion, $_POST['x'], $_POST['y'], 10, 5
        );
    }
    
}


?>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var abv = 'edi';

$(document).ready(function() {

    $("#fecha_registro_proyecto").datepicker({
        yearRange:'-20:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#fecha_termino_construccion").datepicker({
        yearRange:'-20:+20',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#fecha_seguimiento").datepicker({
        yearRange:'-20:+20',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });

    $("#montante_fecha").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#ducteria_interna_fecha").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    $("#punto_energia_fecha").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    
    $("#nro_blocks").change(function() {
        nro_blocks = this.value;
        if (nro_blocks == "") {
            $("#divBlock").html('');
        } else {
            $("#divBlock").html('');
            selhtml = '<div style="width:180px;">';
            selhtml += '<span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>';
            selhtml += '<span style="float:left; width:80px;">Nro. dptos</span>';
            selhtml += '</div>';
            for(var i=1; i<=nro_blocks; i++) {
                selhtml += '<div style="width:180px;">';
                selhtml += '<span style="float:left; width:20px;">#' + i + ' </span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_' + i + '" id="nro_pisos_' + i + '" size="5" value="" /></span>';
                selhtml += '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_' + i + '" id="nro_dptos_' + i + '" size="5" value="" /></span>';
                selhtml += '</div>';
            }
            $("#divBlock").html(selhtml);
        }
    });

    $("#selDpto").change(function() {
        IDdpto = this.value;
        if (IDdpto == "") {
            return false;
        }
        data_content = "action=buscarProv&IDdpto=" + IDdpto;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selProv = ''
                for (var i in datos) {
                    selProv += '<option value="' + datos[i]['codprov'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selProv").html(selProv);
                $("#selDist").html('<option value="">Seleccione</option>');
            }
        });
    });

    $("#selDpto1").change(function() {
        IDdpto = this.value;
        if (IDdpto == "") {
            return false;
        }
        data_content = "action=buscarProv&IDdpto=" + IDdpto;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selProv = ''
                for (var i in datos) {
                    selProv += '<option value="' + datos[i]['codprov'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selProv1").html(selProv);
                $("#selDist1").html('<option value="">Seleccione</option>');
            }
        });
    });

    $("#selProv").change(function() {
        IDdpto = $("#selDpto").val()
        IDprov = this.value;
        if (IDdpto == "" && IDprov == "") {
            return false;
        }
        data_content = "action=buscarDist&IDdpto=" + IDdpto + "&IDprov=" + IDprov;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                
                var selDist = ''
                for (var i in datos) {
                    selDist += '<option value="' + datos[i]['coddist'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selDist").html(selDist);
            }
        });
    });

    $("#selProv1").change(function() {
        IDdpto = $("#selDpto1").val()
        IDprov = this.value;
        if (IDdpto == "" && IDprov == "") {
            return false;
        }
        data_content = "action=buscarDist&IDdpto=" + IDdpto + "&IDprov=" + IDprov;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                
                var selDist = ''
                for (var i in datos) {
                    selDist += '<option value="' + datos[i]['coddist'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selDist1").html(selDist);
            }
        });
    });

    $("#item").blur(function() {
        if ( $("#item").val() == "" ) {
            $("#msgItem").html('');
            return false;
        }
        data_content = {
            'item' : $("#item").val()
        };
        $("#fongoLoader").show();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php?action=searchItem",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                if (datos['success'] == 1) {
                       $("#msgItem").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                   } else {
                       $("#msgItem").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       $("#item").val('');
                       $("#item").focus();
                   }
                $("#fongoLoader").hide();
            }
        });
    });
    
    $("#Grabar").click(function() {
        var data = {
            x                         : $("#x").val(),
            y                         : $("#y").val(),
            estado                    : $("#estado").val(),
            archivo1                 : $("#archivo1").val(),
            archivo2                 : $("#archivo2").val(),
            archivo3                 : $("#archivo3").val(),
            archivo4                 : $("#archivo4").val(),
            fecha_registro_proyecto    : $("#fecha_registro_proyecto").val(),
            ura                         : $("#ura").val(),
            sector                     : $("#sector").val(),
            mza_tdp                     : $("#mza_tdp").val(),
            idtipo_via                 : $("#idtipo_via").val(),
            direccion_obra             : $("#direccion_obra").val(),
            numero                     : $("#numero").val(),
            mza                     : $("#mza").val(),
            lote                     : $("#lote").val(),
            idtipo_cchh                 : $("#idtipo_cchh").val(),
            cchh                     : $("#cchh").val(),
            distrito                 : $("#selDpto").val() + $("#selProv").val() + $("#selDist").val(),
            nombre_constructora         : $("#nombre_constructora").val(),
            nombre_proyecto             : $("#nombre_proyecto").val(),
            idtipo_proyecto             : $("#idtipo_proyecto").val(),
            persona_contacto         : $("#persona_contacto").val(),
            pagina_web                 : $("#pagina_web").val(),
            email                     : $("#email").val(),
            nro_blocks                 : $("#nro_blocks").val(),
            nro_pisos_1                 : $("#nro_pisos_1").val(),
            nro_dptos_1                 : $("#nro_dptos_1").val(),
            nro_pisos_2                 : $("#nro_pisos_2").val(),
            nro_dptos_2                 : $("#nro_dptos_2").val(),
            nro_pisos_3                 : $("#nro_pisos_3").val(),
            nro_dptos_3                 : $("#nro_dptos_3").val(),
            nro_pisos_4                 : $("#nro_pisos_4").val(),
            nro_dptos_4                 : $("#nro_dptos_4").val(),
            nro_pisos_5                 : $("#nro_pisos_5").val(),
            nro_dptos_5                 : $("#nro_dptos_5").val(),
            idtipo_infraestructura    : $("#idtipo_infraestructura").val(),
            nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
            avance                     : $("#avance").val(),
            fecha_termino_construccion : $("#fecha_termino_construccion").val(),
            montante                 : $("#montante").val(),
            montante_fecha             : $("#montante_fecha").val(),
            montante_obs             : $("#montante_obs").val(),
            ducteria_interna         : $("#ducteria_interna").val(),
            ducteria_interna_fecha     : $("#ducteria_interna_fecha").val(),
            ducteria_interna_obs     : $("#ducteria_interna_obs").val(),
            punto_energia             : $("#punto_energia").val(),
            punto_energia_fecha        : $("#punto_energia_fecha").val(),
            punto_energia_obs         : $("#punto_energia_obs").val(),
            fecha_seguimiento         : $("#fecha_seguimiento").val(),
            observacion                 : $("#observacion").val()
        };

        if ( data['estado'] == "" ) {
            alert("Ingrese el campo Estado del Edificio.");
            return false;
        }
        if ( data['fecha_registro_proyecto'] == "" ) {
            alert("Ingrese el campo Fecha Registro Proyecto.");
            return false;
        }
        if ( data['ura'] == "" ) {
            alert("Ingrese el campo URA.");
            return false;
        }
        if ( data['idtipo_via'] == "" ) {
            alert("Ingrese el campo Tipo Via.");
            return false;
        }
        if ( data['idtipo_cchh'] == "" ) {
            alert("Ingrese el campo Tipo CCHH.");
            return false;
        }
        if ( $("#selDpto").val() == "" ) {
            alert("Ingrese el campo Departamento.");
            return false;
        }
        if ( $("#selProv").val() == "" ) {
            alert("Ingrese el campo Provincia.");
            return false;
        }
        if ( $("#selDist").val() == "" ) {
            alert("Ingrese el campo Distrito.");
            return false;
        }
        if ( data['idtipo_proyecto'] == "" ) {
            alert("Ingrese el campo Tipo Proyecto.");
            return false;
        }
        if ( data['nro_blocks'] == "" ) {
            alert("Ingrese el campo Nro. de Blocks.");
            return false;
        }

        var query = $("input.clsBlocks").serializeArray();
        for (i in query) {
            if ( query[i].value == "" ) {
                alert("Ingrese todos los datos en Nro. pisos y Nro. dptos.");
                $("#" + query[i].name).focus();
                return false;
            }
        } 

        if ( data['idtipo_infraestructura'] == "" ) {
            alert("Ingrese el campo Tipo Infraestructura.");
            return false;
        }

        //data_content = "action=Grabar&idcapa=" + idcapa;
        data_content = data;
        $("#fongoLoader").show();
        //$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php?action=GrabaEdificio",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);

                $("#fongoLoader").hide();
                
                if (datos['success'] == 1) {
                       $("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   } else {
                       $("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   }
            }
        });
    });

    $("#GrabarCompetencia").click(function() {
        var data = {
            x                         : $("#x").val(),
            y                         : $("#y").val(),
            archivo1                 : $("#file1").val(),
            archivo2                 : $("#file2").val(),
            archivo3                 : $("#file3").val(),
            archivo4                 : $("#file4").val(),
            competencia             : $("#competencia").val(),
            competencia_otro         : $("#competencia_otro").val(),
            elementoencontrado         : $("#elementoencontrado").val(),
            elementoencontrado_otro    : $("#elementoencontrado_otro").val(),
            ura                        : $("#mdf").val(),
            direccion                : $("#direccion").val(),
            observacion                : $("#obs").val()
        };

        /*if ( data['ura'] == "" ) {
            alert("Ingrese el campo URA.");
            return false;
        }*/
        if ( data['competencia'] == "" ) {
            alert("Ingrese el campo Competencia.");
            return false;
        }
        if ( data['elementoencontrado'] == "" ) {
            alert("Ingrese el campo Elemento Encontrado.");
            return false;
        }

        //data_content = "action=Grabar&idcapa=" + idcapa;
        data_content = data;
        $("#fongoLoader").show();
        //$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php?action=GrabaCompetencia",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);

                $("#fongoLoader").hide();
                
                if (datos['success'] == 1) {
                       $("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   } else {
                       $("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   }
            }
        });
    });
    
    $("#GrabarDunaAdsl").click(function() {
        var data = {
            x           : $("#x").val(),
            y         : $("#y").val(),
            foto1     : $("#fotoduna1").val(),
            foto2     : $("#fotoduna2").val(),
            foto3    : $("#fotoduna3").val(),
            foto4    : $("#fotoduna4").val(),
            departamento: $("#departamento").val(),
            provincia    : $("#provincia").val(),
            distrito    : $("#distrito").val(),
            direccion   : $("#direccionduna").val(),
            direccion_referencial : $("#direccionduna_referencial").val(),
            tipo_antena : $("#tipo_antena").val()
        };

        if ( data['departamento'] == "" ) {
            alert("Ingrese el campo Departamento.");
            return false;
        }
        if ( data['provincia'] == "" ) {
            alert("Ingrese el campo provincia.");
            return false;
        }
        if ( data['distrito'] == "" ) {
            alert("Ingrese el campo distrito.");
            return false;
        }
        if ( data['direccion'] == "" ) {
            alert("Ingrese el campo direccion.");
            return false;
        }

        //data_content = "action=Grabar&idcapa=" + idcapa;
        data_content = data;
        $("#fongoLoader").show();
        //$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php?action=GrabaDunaAdsl",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);

                $("#fongoLoader").hide();
                
                if (datos['success'] == 1) {
                    $("#divMsgFormCapa").html('<div style="color:#0E774A;font-size:11px;font-weight:bold;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                    setTimeout("$(\"#childModal\").dialog('close');", 2000);
                } else {
                    $("#divMsgFormCapa").html('<div style="color:#FF0000;;font-size:11px;font-weight:bold;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                }
            }
        });
    });

    $("#GrabarEstacionBase").click(function() {
        var data = {
            x                         : $("#x").val(),
            y                         : $("#y").val(),
            archivo1                 : $("#foto1").val(),
            archivo2                 : $("#foto2").val(),
            archivo3                 : $("#foto3").val(),
            archivo4                 : $("#foto4").val(),
            nombre                     : $("#estacionbase").val(),
            departamento             : $("#selDpto1 option:selected").text(),
            provincia                 : $("#selProv1 option:selected").text(),
            distrito                 : $("#selDist1 option:selected").text(),
            direccion                : $("#direccionest").val()
        };

        if ( data['nombre'] == "" ) {
            alert("Ingrese el campo Nombre.");
            return false;
        }

        //data_content = "action=Grabar&idcapa=" + idcapa;
        data_content = data;
        $("#fongoLoader").show();
        //$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php?action=GrabaEstacionBase",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);

                $("#fongoLoader").hide();
                
                if (datos['success'] == 1) {
                       $("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   } else {
                       $("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                       setTimeout("$(\"#childModal\").dialog('close');", 2000);
                   }
            }
        });
    });
    
    $("#idcapa").change(function() {
        idcapa     = $("#idcapa").val();
        if (idcapa == "") {
            alert("Seleccione la capa.");
            return false;
        }

        abv = abv_capas[idcapa];
        //solo para edificios
        if ( abv == 'edi') {
            $("#divCamposCapa").html('');
            $("#divCamposEdificio").show();
            $("#divCamposCompetencia").hide();
                $("#divCamposDunaAdsl").hide();
            $("#divEstacionBase").hide();
            $("#divImagenCargada").html('');
            $("#abv_capa").val('edi');
            return false;
        }
        //para competencia
        else if ( abv == 'com' ) {
            $("#divCamposCapa").html('');
            $("#divCamposEdificio").hide();
            $("#divCamposCompetencia").show();
                $("#divCamposDunaAdsl").hide();
            $("#divEstacionBase").hide();
            $("#divImagenCargada").html('');
            $("#abv_capa").val('com');
            return false;
        }
        //para estacion base
        else if ( abv == 'esb' ) {
            $("#divCamposCapa").html('');
            $("#divCamposEdificio").hide();
            $("#divCamposCompetencia").hide();
                $("#divCamposDunaAdsl").hide();
            $("#divEstacionBase").show();
            $("#divImagenCargada").html('');
            $("#abv_capa").val('est');
            return false;
        }
        //para Duna Adsl
        else if ( abv == 'dad' ) {
            $("#divCamposCapa").html('');
            $("#divCamposEdificio").hide();
            $("#divCamposCompetencia").hide();
                $("#divCamposDunaAdsl").show();
            $("#divEstacionBase").hide();
            $("#divImagenCargada").html('');
            $("#abv_capa").val('est');
            return false;
        } else {
            $("#divCamposEdificio").hide();
            $("#divCamposCompetencia").hide();
                $("#divCamposDunaAdsl").hide();
            $("#divEstacionBase").hide();
            $("#abv_capa").val(abv_capas[idcapa]);
        }

        //alert("abv=" + abv);
        
        $("#formCapa input:text, #formCapa textarea").val("");
        $("#divImagenCargada").html('');
        
        data_content = "action=buscarCamposCapa&idcapa=" + idcapa;
        $("#fongoLoader").show();
        $("#idcapa").attr('disabled', 'disabled');
        $("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var inputCampos = '<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">';
                inputCampos += '<tr><td width="120">Foto 1</td><td><input type="text" name="archivo1" id="archivo1" value="" readonly="readonly"/><input type="button" name="archivo-1" id="archivo-1" onclick="mostrarUploadImagen(\'archivo1\');" value=".." /></td></tr>';
                inputCampos += '<tr><td width="120">Foto 2</td><td><input type="text" name="archivo2" id="archivo2" value="" readonly="readonly"/><input type="button" name="archivo-2" id="archivo-2" onclick="mostrarUploadImagen(\'archivo2\');" value=".." /></td></tr>';
                inputCampos += '<tr><td width="120">Foto 3</td><td><input type="text" name="archivo3" id="archivo3" value="" readonly="readonly"/><input type="button" name="archivo-3" id="archivo-3" onclick="mostrarUploadImagen(\'archivo3\');" value=".." /></td></tr>';
                inputCampos += '<tr><td width="120">Foto 4</td><td><input type="text" name="archivo4" id="archivo4" value="" readonly="readonly"/><input type="button" name="archivo-4" id="archivo-4" onclick="mostrarUploadImagen(\'archivo4\');" value=".." /></td></tr>';
                for(var i in datos) {
                    if ( datos[i]['multiple'] == 'N' ) {
                        inputCampos += '<tr>';
                        inputCampos += '<td>' + datos[i]['campo'] + ': </td>' + '<td><input type="text" name="campo' + datos[i]['campo_nro'] + '" id="campo' + datos[i]['campo_nro'] + '" /></td>';
                        inputCampos += '</tr>';
                    }
                    else if ( datos[i]['multiple'] == 'S' ) {
                    
                        var campos_multiple = datos[i]['campos_multiple']
                    
                        inputCampos += '<tr>';
                        inputCampos += '<td>' + datos[i]['campo'] + ': </td>';
                        inputCampos += '<td>';
                        inputCampos += '<select class="' + datos[i]['campo'] + '" name="multiple' + datos[i]['campo_nro'] + '" id="multiple' + datos[i]['campo_nro'] + '">';
                        inputCampos += '<option value="">Seleccione</option>';
                        for( var m in campos_multiple ) {
                            inputCampos += '<option value="' + campos_multiple[m]['idcapa_detalle_multiple']  + '">' + campos_multiple[m]['opcion']  + '</option>';
                        }
                        inputCampos += '</select>';
                        inputCampos += '</td>';
                        inputCampos += '</tr>';
                    }
                }
                inputCampos += '<tr><td colspan="2"><input type="button" name="GrabaComponente" id="GrabaComponente" onclick="grabarComponente();" value="Grabar Datos" on /><input type="button" id="cancelar" value="Cancelar" /></td></tr>';
                inputCampos += '</table>';
                    
                  $("#divCamposCapa").html(inputCampos);
                
                $("#idcapa").attr('disabled', '');
                $("#fongoLoader").hide();
            }
        });
    });
    $("#Cancelar, .Cancelar").click(function() {
        $("#childModal").dialog('close');
    });

    /*
    $("#upload").click(function() {
        //alert("ssss= " + abv);
        $("#divUploadImagen").show();

        $("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=' + abv + '" width="100%" height="100%" style="border: 0px;"></iframe>');
    });
    */

    $(".upload").click(function() {
        $("#divUploadImagen").show();
        var archivo = $(this).attr('id');

        $("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=' + abv + '&archivo=' + archivo + '" width="100%" height="100%" style="border: 0px;"></iframe>');
    });

    $("#divCerrarUploadImagen").click(function() {
        $("#divUploadImagen").hide();
    });
    
});

function soloNumeros(obj) {
    var re = /^(-)?[0-9]*$/;
    if (!re.test(obj.value)) {
        obj.value = '';
    }
}

function mostrarUploadImagen(archivo) {
    $("#divUploadImagen").show();
    $("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=' + abv + '&archivo=' + archivo + '" width="100%" height="100%" style="border: 0px;"></iframe>');
}

function grabarComponente() {
    var x         = $("#x").val();
    var y         = $("#y").val();
    var idcapa     = $("#idcapa").val();
    var foto1     = $("#archivo1").val();
    var foto2     = $("#archivo2").val();
    var foto3     = $("#archivo3").val();
    var foto4     = $("#archivo4").val();    
    
    var campos = "";
    for (i=1; i<=50; i++) {
        if ( $("#campo" + i).val() != '' && $("#campo" + i).val() != undefined ) {
            campos += "&campo" + i + "=" + $("#campo" + i).val();
        }
        else {
            if ( $("#multiple" + i).val() == '' ) {
                alert("Ingrese el campo: " + $('#multiple' + i).attr('class'));
                return false;
            }
            else {
                if ( $("#multiple" + i).val() != '' && $("#multiple" + i).val() != undefined ) {
                    campos += "&campo" + i + "=" + $("#multiple" + i + " option:selected").text();
                }
            }
        }
    }
    
    data_content = "action=GrabaComponente&x=" + x + "&y=" + y + "&idcapa=" + idcapa + "&foto1=" + foto1 + "&foto2=" + foto2 + "&foto3=" + foto3 + "&foto4=" + foto4 + campos;
    $("#fongoLoader").show();
    $("#divCamposCapa").hide();
    $.ajax({
        type:   "POST",
        url:    "modulos/maps/vistas/principal/agregarcomp.popup.php",
        data:   data_content,
        dataType: "json",
        success: function(datos) {
            //alert(datos);

            $("#fongoLoader").hide();
            
            if (datos['success'] == 1) {
                    $("#divMsgFormCapa").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                    setTimeout("$(\"#childModal\").dialog('close');", 2000);
                } else {
                    $("#divMsgFormCapa").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                }

        }
    });
}

var abv_capas = <?php echo json_encode($arrayAbv)?>;
</script>

<script type="text/javascript" src="modulos/maps/js/functions.js"></script>
<style>
    em {
        margin-left: 5px;
        color: #ff0000;
    }
</style>


<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
    <td width="400" valign="top">
        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
        <tr>
            <td colspan="2">
                <p style="font-weight:bold;">X,Y seleccionado:</p>
                <?php echo $_POST['x'] . ',' . $_POST['y'] ?><br />&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="font-weight:bold;">Seleccione la capa que desee agregar:</p>
                <br />
            </td>
        </tr>
        <tr>
            <td width="120">Capa:</td>
            <td width="220">
                <select name="idcapa" id="idcapa">
                    <option value="">Seleccione capa</option>
                    <?php
                    foreach ($arrayCapas as $objCapa) {
                        if ( $objCapa->__get('_esFftt') == 'N' && $objCapa->__get('_indCapa') == 'S' ) {
                        ?>
                        <option value="<?php echo $objCapa->__get('_idCapa')?>"><?php echo $objCapa->__get('_nombre')?></option>
                        <?php 
                        }
                    }
                    ?>
                </select>    
            </td>
        </tr>
        </table>
        
    </td>
    <td valign="top">
        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
        <tr>
            <td>
                <div id="divImagenCargada"></div>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>


<br />

<div id="fongoLoader" class="bgLoader">
    <div class="imgLoader">
        <span>Cargando...</span>
    </div>
</div>

<div id="divUploadImagen">
    <div id="divCerrarUploadImagen">[X]</div>
    <div id="divFormUploadImagen">
        
    </div>
</div>

<div id="formCapa">

<div id="divCamposCapa"></div>

<div id="divCamposEdificio" style="display:none;">

    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td valign="top">
        
            <fieldset style="padding:5px;">
                <legend style="font-weight: bold;">Informaci&oacute;n principal</legend>
        
                <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                <tr>
                    <td>Estado del Edificio:</td>
                    <td>
                        <select name="estado" id="estado">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayEstado as $id=>$val ) {
                            ?>
                            <option value="<?php echo $id?>"><?php echo $val?></option>
                            <?php 
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Foto 1:</td>
                    <td>
                        <input type="text" name="archivo1" id="archivo1" value="" readonly="readonly"/>
                        <input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 2:</td>
                    <td>
                        <input type="text" name="archivo2" id="archivo2" value="" readonly="readonly"/>
                        <input type="button" name="archivo-2" id="archivo-2" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 3:</td>
                    <td>
                        <input type="text" name="archivo3" id="archivo3" value="" readonly="readonly"/>
                        <input type="button" name="archivo-3" id="archivo-3" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 4:</td>
                    <td>
                        <input type="text" name="archivo4" id="archivo4" value="" readonly="readonly"/>
                        <input type="button" name="archivo-4" id="archivo-4" class="upload" value=".." />
                    </td>
                </tr>
                <!--
                <tr>
                    <td>Item:</td>
                    <td>
                        <input type="text" name="item" size="5" id="item" value="" style="float: left;" /> &nbsp;
                        <span id="msgItem" style="float: left; width: 80px; margin-left: 5px;"></span>
                    </td>
                </tr>
                 -->
                <tr>
                    <td width="150">Fecha Registro Proyecto:</td>
                    <td>
                        <input type="text" name="fecha_registro_proyecto" size="11" id="fecha_registro_proyecto" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>URA:</td>
                    <td>
                        <!-- <input type="text" name="ura" id="ura" value="" /> -->
                        <select name="ura" id="ura">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayMdfs as $mdf ) {
                            ?>
                            <option value="<?php echo $mdf['mdf']?>"><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Sector:</td>
                    <td>
                        <input type="text" name="sector" id="sector" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Mza. (TdP):</td>
                    <td>
                        <input type="text" name="mza_tdp" id="mza_tdp" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Tipo Via:</td>
                    <td>
                        <select name="idtipo_via" id="idtipo_via">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayTipoVia as $tipoVia ) {
                            ?>
                            <option value="<?php echo $tipoVia['iddescriptor']?>"><?php echo $tipoVia['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Direcci&oacute;n de Obra (Nombre de Via ):</td>
                    <td>
                        <input type="text" name="direccion_obra" id="direccion_obra" value="" />
                    </td>
                </tr>
                <tr>
                    <td>N&deg;:</td>
                    <td>
                        <input type="text" name="numero" id="numero" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Mza.:</td>
                    <td>
                        <input type="text" name="mza" id="mza" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Lote:</td>
                    <td>
                        <input type="text" name="lote" id="lote" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Tipo CCHH:</td>
                    <td>
                        <select name="idtipo_cchh" id="idtipo_cchh">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayTipoCchh as $tipoCchh ) {
                            ?>
                            <option value="<?php echo $tipoCchh['iddescriptor']?>"><?php echo $tipoCchh['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>CCHH:</td>
                    <td>
                        <input type="text" name="cchh" id="cchh" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Departamento:</td>
                    <td>
                        <select name="selDpto" id="selDpto">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $dptoList as $dpto ) {
                            ?>
                            <option value="<?php echo $dpto['coddpto']?>"><?php echo $dpto['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Provincia:</td>
                    <td>
                        <div id="divProv">
                            <select id="selProv">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Distrito:</td>
                    <td>
                        <div id="divDist">
                            <select id="selDist">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Nombre de Constructora:</td>
                    <td>
                        <input type="text" name="nombre_constructora" id="nombre_constructora" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Nombre del proyecto:</td>
                    <td>
                        <input type="text" name="nombre_proyecto" id="nombre_proyecto" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Tipo Proyecto:</td>
                    <td>
                        <select name="idtipo_proyecto" id="idtipo_proyecto">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayTipoCchh as $tipoProyecto ) {
                            ?>
                            <option value="<?php echo $tipoProyecto['iddescriptor']?>"><?php echo $tipoProyecto['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Persona de Contacto:</td>
                    <td>
                        <input type="text" name="persona_contacto" id="persona_contacto" value="" />
                    </td>
                </tr>
                <tr>
                    <td>P&aacute;gina Web:</td>
                    <td>
                        <input type="text" name="pagina_web" id="pagina_web" value="" />
                    </td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td>
                        <input type="text" name="email" id="email" value="" />
                    </td>
                </tr>
                <tr>
                    <td>N&deg; de Blocks:</td>
                    <td>
                        <!--<input type="text" name="nro_blocks" id="nro_blocks" value="" />-->
                        <select id="nro_blocks" name="nro_blocks" style="width:80px;">
                            <option value="">Seleccione</option>
                            <?php 
                            for ($i=1;$i<=5;$i++) {
                            ?>
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php 
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div id="divBlock"></div>
                    </td>
                </tr>
                <tr>
                    <td>Tipo de Infraestructura:</td>
                    <td>
                        <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayTipoInfraestructura as $tipoInfraestructura ) {
                            ?>
                            <option value="<?php echo $tipoInfraestructura['iddescriptor']?>"><?php echo $tipoInfraestructura['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>N&deg; Dptos Habitados:</td>
                    <td>
                        <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="" />
                    </td>
                </tr>
                <tr>
                    <td>% Avance:</td>
                    <td>
                        <input type="text" name="avance" id="avance" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Fecha de Termino Construcci&oacute;n:</td>
                    <td>
                        <input type="text" size="11" name="fecha_termino_construccion" id="fecha_termino_construccion" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Montante:</td>
                    <td>
                        <select name="montante" id="montante" style="width:80px;">
                            <option value="">Seleccione</option>
                            <option value="S">Si</option>
                            <option value="N">No</option>
                        </select> &nbsp; 
                        Fecha: <input type="text" size="11" name="montante_fecha" id="montante_fecha" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Montante (Observacion):</td>
                    <td>
                        <textarea name="montante_obs" id="montante_obs" style="width:200px;height:50px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Ducteria Interna:</td>
                    <td>
                        <!-- <input type="text" name="ducteria_interna" id="ducteria_interna" value="" /> -->
                        <select name="ducteria_interna" id="ducteria_interna"  style="width:80px;">
                            <option value="">Seleccione</option>
                            <option value="S">Si</option>
                            <option value="N">No</option>
                        </select> &nbsp; 
                        Fecha: <input type="text" size="11" name="ducteria_interna_fecha" id="ducteria_interna_fecha" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Ducteria Interna (Observacion):</td>
                    <td>
                        <textarea name="ducteria_interna_obs" id="ducteria_interna_obs" style="width:200px;height:50px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Punto de energ&iacute;a:</td>
                    <td>
                        <!-- <input type="text" name="punto_energia" id="punto_energia" value="" /> -->
                        <select name="punto_energia" id="punto_energia"  style="width:80px;">
                            <option value="">Seleccione</option>
                            <option value="S">Si</option>
                            <option value="N">No</option>
                        </select> &nbsp; 
                        Fecha: <input type="text" size="11" name="punto_energia_fecha" id="punto_energia_fecha" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Punto de energia (Observacion):</td>
                    <td>
                        <textarea name="punto_energia_obs" id="punto_energia_obs" style="width:200px;height:50px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Fecha de seguimiento:</td>
                    <td>
                        <input type="text" size="11" name="fecha_seguimiento" id="fecha_seguimiento" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Observaci&oacute;n:</td>
                    <td>
                        <textarea name="observacion" id="observacion" style="width:200px;height:50px;"></textarea>
                    </td>
                </tr>
                </table>
            </fieldset>
            
        </td>
        <td valign="top">
        
            <fieldset style="padding: 5px;">
                <legend  style="font-weight: bold;">Informaci&oacute;n adicional</legend>
        
                <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                <tr>
                    <td width="150">Tipo de trabajo programado:</td>
                    <td>
                        <select name="idtipo_trabajo_programado" id="idtipo_trabajo_programado">
                            <option value="">Seleccione</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Estado de env&iacute;o a dise&ntilde;o:</td>
                    <td>
                        <select name="idestado_envio_diseno" id="idestado_envio_diseno">
                            <option value="">Seleccione</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Fecha de env&iacute;o a Dise&ntilde;o:</td>
                    <td>
                        <input type="text" size="11" name="fecha_envio_diseno" id="fecha_envio_diseno" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Zona Residencial:</td>
                    <td>
                        <input type="text" name="zona_residencial" id="zona_residencial" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Segmento Constructora:</td>
                    <td>
                        <input type="text" name="segmento_constructora" id="fecha_envio_diseno" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Grupo Asignado:</td>
                    <td>
                        <input name="grupo_asignado" id="grupo_asignado" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Gesti&oacute;n de Obras:</td>
                    <td>
                        <input type="text" name="gestion_obras" id="gestion_obras" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Prioridad:</td>
                    <td>
                        <input type="text" name="prioridad" id="prioridad" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Quincena:</td>
                    <td>
                        <input type="text" name="quincena" id="quincena" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Etapa:</td>
                    <td>
                        <input type="text" name="etapa" id="etapa" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Fecha cableado real:</td>
                    <td>
                        <input type="text" size="11" name="fecha_cableado_real" id="fecha_cableado_real" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>OT B&aacute;sica:</td>
                    <td>
                        <input type="text" name="ot_basica" id="ot_basica" value="" />
                    </td>
                </tr>
                <tr>
                    <td>OT CATV:</td>
                    <td>
                        <input type="text" name="ot_catv" id="ot_catv" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Fin de etapa actual:</td>
                    <td>
                        <input type="text" name="fin_etapa_actual" id="fin_etapa_actual" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Fin cableado programado:</td>
                    <td>
                        <input type="text" size="11" name="fin_cableado_programado" id="fin_cableado_programado" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Bandeja:</td>
                    <td>
                        <input type="text" size="11" name="bandeja" id="bandeja" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Indicencia:</td>
                    <td>
                        <input type="text" size="11" name="indicencia" id="indicencia" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Estado edificio:</td>
                    <td>
                        <input type="text" size="11" name="estado_edificio" id="estado_edificio" value="" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>Observaci&oacute;n Obras:</td>
                    <td>
                        <input type="text" name="observacion_obras" id="observacion_obras" value="" />
                    </td>
                </tr>
                </table>    
            
            </fieldset>
            
        </td>
    </tr>
    </table>

    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td colspan="2" height="20">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" name="Grabar" id="Grabar" value="Grabar Datos" />
            <input type="button" id="Cancelar" value="Cancelar" />
        </td>
    </tr>
    </table>
    

</div>

<div id="divCamposCompetencia" style="display:none;">

    <table border="0" cellspacing="1" cellpadding="0" align="center" width="60%">
    <tr>
        <td valign="top">
        
            <fieldset style="padding:5px;">
                <legend style="font-weight: bold;">Competencia</legend>
        
                <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                <tr>
                    <td>Foto 1:</td>
                    <td>
                        <input type="text" name="file1" id="file1" value="" readonly="readonly"/>
                        <input type="button" name="file-1" id="file-1" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 2:</td>
                    <td>
                        <input type="text" name="file2" id="file2" value="" readonly="readonly"/>
                        <input type="button" name="file-2" id="file-2" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 3:</td>
                    <td>
                        <input type="text" name="file3" id="file3" value="" readonly="readonly"/>
                        <input type="button" name="file-3" id="file-3" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 4:</td>
                    <td>
                        <input type="text" name="file4" id="file4" value="" readonly="readonly"/>
                        <input type="button" name="file-4" id="file-4" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>URA:</td>
                    <td>
                        <!-- <input type="text" name="ura" id="ura" value="" /> -->
                        <select name="mdf" id="mdf">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayMdfs as $mdf ) {
                            ?>
                            <option value="<?php echo $mdf['mdf']?>"><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Direcci&oacute;n:</td>
                    <td>
                        <input type="text" name="direccion" id="direccion" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Competencia:</td>
                    <td>
                        <select name="competencia" id="competencia">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayCompetencia as $competencia ) {
                            ?>
                            <option value="<?php echo $competencia['iddescriptor']?>"><?php echo $competencia['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select> (*)
                    </td>
                </tr>
                <tr>
                    <td>Otro:</td>
                    <td>
                        <input type="text" name="competencia_otro" id="competencia_otro" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Elemento Encontrado:</td>
                    <td>
                        <select name="elementoencontrado" id="elementoencontrado">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $arrayElementoEncontrado as $elementoEncontrado ) {
                            ?>
                            <option value="<?php echo $elementoEncontrado['iddescriptor']?>"><?php echo $elementoEncontrado['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select> (*)
                    </td>
                </tr>
                <tr>
                    <td>Otro:</td>
                    <td>
                        <input type="text" name="elementoencontrado_otro" id="elementoencontrado_otro" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Observaci&oacute;n:</td>
                    <td>
                        <textarea name="obs" id="obs" style="width:200px;height:50px;"></textarea>
                    </td>
                </tr>
                </table>
            
            </fieldset>
        </td>
    </tr>
    </table>
    
    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td colspan="2" height="20">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" name="GrabarCompetencia" id="GrabarCompetencia" value="Grabar Competencia" />
            <input type="button" id="Cancelar" value="Cancelar" />
        </td>
    </tr>
    </table>

</div>

<div id="divCamposDunaAdsl" style="display:none;">

    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td valign="top">
        
            <fieldset style="padding:5px;">
                <legend style="font-weight: bold;">DUNA ADSL</legend>
        
                <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                <tr>
                    <td>FOTO 1:</td>
                    <td>
                        <input type="text" name="fotoduna1" id="fotoduna1" value="" readonly="readonly"/>
                        <input type="button" name="fotoduna-1" id="fotoduna-1" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>FOTO 2:</td>
                    <td>
                        <input type="text" name="fotoduna2" id="fotoduna2" value="" readonly="readonly"/>
                        <input type="button" name="fotoduna-2" id="fotoduna-2" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>FOTO 3:</td>
                    <td>
                        <input type="text" name="fotoduna3" id="fotoduna3" value="" readonly="readonly"/>
                        <input type="button" name="fotoduna-3" id="fotoduna-3" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>FOTO 4:</td>
                    <td>
                        <input type="text" name="fotoduna4" id="fotoduna4" value="" readonly="readonly"/>
                        <input type="button" name="fotoduna-4" id="fotoduna-4" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>DEPARTAMENTO:</td>
                    <td>                                         
                        <select name="departamento" id="departamento" onchange="provinciasUbigeo();">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $dptoList as $dpto ) {
                            ?>
                            <option value="<?php echo $dpto['coddpto']?>"><?php echo $dpto['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select><em>*</em>
                    </td>
                </tr>
                <tr>
                    <td>PROVINCIA:</td>
                    <td>
                        <select name="provincia" id="provincia" onchange="distritosUbigeo();">
                            <option value="">Seleccione</option>
                        </select><em>*</em>
                    </td>
                </tr>
                <tr>
                    <td>DISTRITO:</td>
                    <td>
                        <select name="distrito" id="distrito">
                            <option value="">Seleccione</option>
                        </select><em>*</em>
                    </td>
                </tr>
                <tr>
                    <td>DIRECCI&Oacute;N:</td>
                    <td>
                        <input type="text" name="direccionduna" id="direccionduna" value="" size="60" style="text-transform: uppercase;" /><em>*</em>
                    </td>
                </tr>
                                <tr>
                    <td>DIRECCI&Oacute;N REFERENCIAL:</td>
                    <td>
                        <input type="text" name="direccionduna_referencial" id="direccionduna_referencial" value="" size="60" style="text-transform: uppercase;" />
                    </td>
                </tr>
                <tr>
                    <td>TIPO ANTENA:</td>
                    <td>
                        <select name="tipo_antena" id="tipo_antena">
                            <option value="EMISOR">Emisor</option>
                            <option value="RECEPTOR">Receptor</option>
                        </select><em>*</em>
                    </td>
                </tr>
                
                </table>
            
            </fieldset>
        </td>
    </tr>
    </table>
    
    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td colspan="2" height="20">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" name="GrabarDunaAdsl" id="GrabarDunaAdsl" value="Grabar Datos" />
            <input type="button" id="Cancelar" value="Cancelar" />
            <em>*</em> campos marcados con * son obligatorios
                            
        </td>
    </tr>
    </table>

</div>

<div id="divEstacionBase" style="display:none;">

    <table border="0" cellspacing="1" cellpadding="0" align="center" width="60%">
    <tr>
        <td valign="top">
        
            <fieldset style="padding:5px;">
                <legend style="font-weight: bold;">Troba</legend>
        
                <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                <tr>
                    <td>Foto 1:</td>
                    <td>
                        <input type="text" name="foto1" id="foto1" value="" readonly="readonly"/>
                        <input type="button" name="foto-1" id="foto-1" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 2:</td>
                    <td>
                        <input type="text" name="ffoto2" id="foto2" value="" readonly="readonly"/>
                        <input type="button" name="foto-2" id="foto-2" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 3:</td>
                    <td>
                        <input type="text" name="foto3" id="foto3" value="" readonly="readonly"/>
                        <input type="button" name="foto-3" id="foto-3" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Foto 4:</td>
                    <td>
                        <input type="text" name="foto4" id="foto4" value="" readonly="readonly"/>
                        <input type="button" name="foto-4" id="foto-4" class="upload" value=".." />
                    </td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <td>
                        <input type="text" name="estacionbase" id="estacionbase" value="" />
                    </td>
                </tr>
                <tr>
                    <td>Departamento:</td>
                    <td>
                        <select name="selDpto1" id="selDpto1">
                            <option value="">Seleccione</option>
                            <?php 
                            foreach ( $dptoList as $dpto ) {
                            ?>
                            <option value="<?php echo $dpto['coddpto']?>"><?php echo $dpto['nombre']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Provincia:</td>
                    <td>
                        <div id="divProv1">
                            <select id="selProv1">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Distrito:</td>
                    <td>
                        <div id="divDist1">
                            <select id="selDist1">
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Direcci&oacute;n:</td>
                    <td>
                        <input type="text" name="direccionest" id="direccionest" value="" />
                    </td>
                </tr>
                </table>
            
            </fieldset>
        </td>
    </tr>
    </table>
    
    <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
    <tr>
        <td colspan="2" height="20">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" name="GrabarEstacionBase" id="GrabarEstacionBase" value="Grabar Estacion Base" />
            <input type="button" class="Cancelar" id="Cancelar" value="Cancelar" />
        </td>
    </tr>
    </table>

</div>


<div id="divMsgFormCapa" style="margin:10px 5px;"></div>

</div>

<input type="hidden" name="abv_capa" id="abv_capa" />
<input type="hidden" name="x" id="x" value="<?php echo $_POST['x']?>" />
<input type="hidden" name="y" id="y" value="<?php echo $_POST['y']?>" />