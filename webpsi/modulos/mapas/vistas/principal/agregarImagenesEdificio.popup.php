<?php
require_once "../../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;



if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaEdificioFoto' ) {

	$obj_capas = new data_ffttCapas();
	
	//formateando fecha para insert a db
	$_POST['fecha'] = date('Y-m-d', strtotime($_POST['fecha']));

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$result_upd = $obj_capas->__updateEdificio($conexion, $_POST, $idusuario);

	if( $result_upd ) {
		$data_result = $obj_capas->__insertEdificioFoto($conexion, $_POST, $idusuario);
	
		$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
	}else {
		$result = array('success'=>0,'msg'=>'Error al grabar los datos.');
	}
	
	echo json_encode($result);
	exit;
	
}
?>
<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var abv = 'edi';

$(document).ready(function() {

	$("#fecha").datepicker({
		yearRange:'-20:+0',
		changeYear: true,
		numberOfMonths: 1
    });
    
    $("#Grabar").click(function() {
        var data = {
        	idedificio 	: $("#idedificio").val(),
        	fecha		: $("#fecha").val(),
	    	archivo1 	: $("#archivo1").val(),
	    	archivo2 	: $("#archivo2").val(),
	    	archivo3	: $("#archivo3").val(),
	    	archivo4	: $("#archivo4").val()
        };

        if( data['fecha'] == "" ) {
			alert("Ingrese el campo Fecha.");
			return false;
        }
      	if( data['archivo1'] == "" ) {
			alert("Ingrese al menos una foto en campo Foto 1.");
			return false;
        }
        
        data_content = data;
    	//$("#divCamposEdificio").hide();
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/agregarImagenesEdificio.popup.php?action=GrabaEdificioFoto",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                //alert(datos);
                
                if(datos['success'] == 1) {
   					$("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
   					setTimeout("$(\"#childModal\").dialog('close');", 2000);
   				}else {
   					$("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
   					setTimeout("$(\"#childModal\").dialog('close');", 2000);
   				}
            }
        });
    });
	
	
	$("#Cancelar").click(function() {
    	$("#childModal").dialog('close');
	});

	$(".upload").click(function() {
    	$("#divUploadImagen").show();
    	var archivo = $(this).attr('id');
    	
    	$("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=' + abv + '&archivo=' + archivo + '" width="100%" height="100%" style="border: 0px;"></iframe>');
	});

	$("#divCerrarUploadImagen").click(function() {
    	$("#divUploadImagen").hide();
	});
	
});

</script>
</head>
<body>


<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td width="400" valign="top">
		<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
		<tr>
			<td colspan="2">
				<p style="font-weight:bold;">X,Y seleccionado:</p>
				<?php echo $_POST['x'] . ',' . $_POST['y'] ?><br />&nbsp;
			</td>
		</tr>
		</table>
		
	</td>
	<td valign="top">
		<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
		<tr>
			<td>
				<div id="divImagenCargada"></div>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>


<br />

<div id="fongoLoader" class="bgLoader">
	<div class="imgLoader">
		<span>Cargando...</span>
	</div>
</div>

<div id="divUploadImagen">
	<div id="divCerrarUploadImagen">[X]</div>
	<div id="divFormUploadImagen">
		
	</div>
</div>

<div id="formCapa">

<div id="divCamposCapa"></div>

<div id="divCamposEdificio">

	<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
	<tr>
		<td valign="top">
		
			<fieldset style="padding:5px;">
				<legend>Nuevas fotos para este edificio</legend>
		
				<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
				<tr>
					<td>Item:</td>
					<td>
						<input type="text" name="item" id="item" size="11" value="<?php echo $_POST['item']?>" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td width="150">Fecha:</td>
					<td>
						<input type="text" name="fecha" size="11" id="fecha" value="" readonly="readonly" />
					</td>
				</tr>
				<tr>
					<td>Foto 1:</td>
					<td>
						<input type="text" name="archivo1" id="archivo1" value="" readonly="readonly"/>
						<input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
					</td>
				</tr>
				<tr>
					<td>Foto 2:</td>
					<td>
						<input type="text" name="archivo2" id="archivo2" value="" readonly="readonly"/>
						<input type="button" name="archivo-2" id="archivo-2" class="upload" value=".." />
					</td>
				</tr>
				<tr>
					<td>Foto 3:</td>
					<td>
						<input type="text" name="archivo3" id="archivo3" value="" readonly="readonly"/>
						<input type="button" name="archivo-3" id="archivo-3" class="upload" value=".." />
					</td>
				</tr>
				<tr>
					<td>Foto 4:</td>
					<td>
						<input type="text" name="archivo4" id="archivo4" value="" readonly="readonly"/>
						<input type="button" name="archivo-4" id="archivo-4" class="upload" value=".." />
					</td>
				</tr>
				</table>
			</fieldset>
			
		</td>
		
	</tr>
	</table>

	<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
	<tr>
		<td colspan="2" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" name="Grabar" id="Grabar" value="Grabar Datos" />
			<input type="button" id="Cancelar" value="Cancelar" />
		</td>
	</tr>
	</table>
	

</div>

<div id="divMsgFormCapa"></div>

</div>

<input type="hidden" name="idedificio" id="idedificio" value="<?php echo $_REQUEST['idedificio']?>" />

</body>
</html>



