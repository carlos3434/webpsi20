<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<link href="../../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>

<script type="text/javascript">
if ($.browser.msie){ 
    if ($.browser.version < 8) {
      window.location="requerimientos.php";
    }
}
</script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript">


$(document).ready(function() {

    var map = null;
    markers_arr = [];
    
    var M = {
    	initialize: function() {
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 6,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	    	
	        google.maps.event.addListener(map, "click", function(event) {
		        //clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });
        },

        clearMarkers: function() {
        	for (var n = 0, marker; marker = markers_arr[n]; n++) {
        		marker.setVisible(false);
        	}
        },

        popupComponentes: function(x, y) {
	        $("#childModal").html('');
	        $("#childModal").css("background-color","#FFFFFF");
	        $.post("principal/agregarcomp.popup.php", {
	    	        x: x,
	    	        y: y
	    	    },
	        	function(data){
	    	    	parent.$("#childModal").html(data);
	        	}
	    	);
	
	        parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Agregar Componente', position:'top'});
        }

    };

	//carga inicial
    M.initialize();

});
</script>

</head>

<body style="margin: 0px" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td class="form_titulo" colspan="4">
		<img border="0" src="../../../img/plus_16.png">&nbsp;
		<!-- <a href="buscomp.main.php">Busqueda de componentes</a> | 
		<a href="agregarcomp.main.php">Agregar componente</a>--> 
		Agregar Componente
	</td>
</tr>
</table>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td>
		<div id="map_canvas" style="width: 100%; height: 470px; float: left; background-color: #FFFFFF;"></div>
	</td>
</tr>
</table>
<!-- 
<div id="parentModal" style="display: none;">
	<div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>
 -->
</body>
</html>