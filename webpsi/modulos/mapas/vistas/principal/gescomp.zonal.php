<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>


<script type="text/javascript">
var map = null;
var terminal_sel = 0;
var markers_arr = [];
$(document).ready(function(){

	/*$.ajax({
        type:   "POST",
        url:    "gescomp.zonal.main.php",
        data:   "action=buscarZonales",
        success: function(datos) {
            $("#divResult").html(datos);
        }
    });*/

	listar_edificios();

	$(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });

});

function listar_edificios() {
	
	/*var mapOpts = {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
        scaleControl: true,
        scrollwheel: false
    }*/
	var mapOpts = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
	        scaleControl: true
	    }
    map = new google.maps.Map(document.getElementById("map"), mapOpts);
	
	/**
     * makeMarker() ver 0.2
     * creates Marker and InfoWindow on a Map() named 'map'
     * creates sidebar row in a DIV 'sidebar'
     * saves marker to markerArray and markerBounds
     * @param options object for Marker, InfoWindow and SidebarItem
     */
     var infoWindow = new google.maps.InfoWindow();
     var markerBounds = new google.maps.LatLngBounds();
     var markerArray = [];
          
     function makeMarker(options){
		var pushPin = null;
			/*var pushPin = new google.maps.Marker({
             map: map
         });
         pushPin.setOptions(options);*/
			
     	if( options.position != '(0, 0)' ) {
	            pushPin = new google.maps.Marker({
	                map: map
	            });
	            pushPin.setOptions(options);
	            google.maps.event.addListener(pushPin, "click", function(){
	                infoWindow.setOptions(options);
	                infoWindow.open(map, pushPin);
	                if(this.sidebarButton)this.sidebarButton.button.focus();
	            });
	            var idleIcon = pushPin.getIcon();

	            markerBounds.extend(options.position);
	            markerArray.push(pushPin);
     	}else {
         	
     		pushPin = new google.maps.Marker({
                map: map
            });
            pushPin.setOptions(options);
     	}
         
         if(options.sidebarItem){
             pushPin.sidebarButton = new SidebarItem(pushPin, options);
             pushPin.sidebarButton.addIn("sidebar");
         }
         
         return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         var tag = opts.sidebarItemType || "span";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "290px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }


     $("#fongoLoader").show();
     $("#btnBuscar").attr('disabled', 'disabled');
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "gescomp.zonal.main.php",
         data:   "action=buscarZonales",
         dataType: "json",
         success: function(data) {

    	 	if( data['edificios'].length > 0 ) {

    	 		if( data['edificios_con_xy'].length == 0 ) {

    	 			var datos = data['edificios']; 

    	 			var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);

    	 			var myOptions = {
    	 				zoom: 13,
    	 			    center: latlng,
    	 			    mapTypeId: google.maps.MapTypeId.ROADMAP
    	 			};
    	 			map = new google.maps.Map(document.getElementById("map"), myOptions);
        	     	
                	for (var i in datos) {
                		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
        				tabla += '<tr id="row' + datos[i]['abv_zonal'] + '" class="unselected">';
        				tabla += '<td width="70">';
        				tabla += '<span id="' + datos[i]['abv_zonal'] + '" onclick="ir_mapa(\'' + datos[i]['abv_zonal'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
        				tabla += '<td width="70">' + datos[i]['abv_zonal'] + '</td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['abv_zonal'] + '" name="xy[' + datos[i]['abv_zonal'] + '][x]" value="' + x + '"></td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['abv_zonal'] + '" name="xy[' + datos[i]['abv_zonal'] + '][y]" value="' + y + '"></td>';
        				tabla += '<td width="20"><img border="0" title="" src="../../../img/map/zonal/zonal-1.gif" width="12"></td>';
        				tabla += '</tr>';
        				tabla += '</table>';

                        /* markers and info window contents */
                        makeMarker({
                        	draggable: false,
                            position: new google.maps.LatLng(-12.0692083678587, -77.0407839355469),
                            title: datos[i]['desc_zonal'],
                            //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
                            sidebarItem: tabla,
                            //content: detalle,
                            icon: "../../../img/markergreen.png"
                        });
                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    //map.setZoom(10);
                    

    	 			
    	 		}else {

    	 		
	    	 		var datos = data['edificios']; 
	    	     	
	            	for (var i in datos) {
	
	            		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
	            		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
	            		
	            		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
	    				tabla += '<tr id="row' + datos[i]['abv_zonal'] + '" class="unselected">';
	    				tabla += '<td width="70">';
	    				tabla += '<span id="' + datos[i]['abv_zonal'] + '" onclick="ir_mapa(\'' + datos[i]['abv_zonal'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
	    				tabla += '<td width="70">' + datos[i]['abv_zonal'] + '</td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['abv_zonal'] + '" name="xy[' + datos[i]['abv_zonal'] + '][x]" value="' + x + '"></td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['abv_zonal'] + '" name="xy[' + datos[i]['abv_zonal'] + '][y]" value="' + y + '"></td>';
	    				tabla += '<td width="20"><img border="0" title="" src="../../../img/map/zonal/zonal-1.gif" width="12"></td>';
	    				tabla += '</tr>';
	    				tabla += '</table>';

						detalle = '<b>Zonal:</b> ' + datos[i]['desc_zonal'] + '<br />';
						detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
						
						
	                    /* markers and info window contents */
	                    makeMarker({
	                    	draggable: false,
	                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
	                        title: datos[i]['desc_zonal'],
	                        //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
	                        sidebarItem: tabla,
	                        content: detalle,
	                        icon: "../../../img/map/zonal/zon_" + datos[i]['abv_zonal'] + "--1.gif"
	                    });
	            		
	                }
	
	                /* fit viewport to markers */
	                map.fitBounds(markerBounds);

    	 		}
    	 		
 	 		}else {

 	 			var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	
	 			var myOptions = {
	 				zoom: 13,
	 			    center: latlng,
	 			    mapTypeId: google.maps.MapTypeId.ROADMAP
	 			};
	 			map = new google.maps.Map(document.getElementById("map"), myOptions);
 	 		
 	 		}

            //$("#divResult").html(datos);
            $("#btnBuscar").attr('disabled', '');
            $("#btnGrabar").attr('disabled', '');
            //setTimeout("hideLoader()", 1000);
            $("#fongoLoader").hide();
         }
     });
}


function ir_mapa(terminal_sel) {
	//alert("aaa=" + terminal_sel);
	
	//var terminal_sel = $(this).attr("id");
	$("#trm_selected").val(terminal_sel);
	
	//////var terminal_class = $(this).attr("class");

	//alert("terminal_actual2 = " + terminal_sel);
	
	$(".unselected").attr("class", "unselected");
	$(".selected").attr("class", "unselected");
	$("#row" + terminal_sel).attr("class", "selected");

	google.maps.event.addListener(map, "click", function(event) {

		clearMarkers();
		
		marker = new google.maps.Marker({
            position: event.latLng,
            map: map, 
            title: terminal_sel
        });
		markers_arr.push(marker);
		
		if( $("#trm_selected").val() == terminal_sel ) {
			var Coords = event.latLng;
	        //alert("term sel=" + terminal_sel + ", coord=" + Coords);
			
			//var confirmar = confirm("Desea Registrar este lat, lng?");
			//if(confirmar) {
				$("#x" + terminal_sel).val(Coords.lng());
				$("#y" + terminal_sel).val(Coords.lat());
			//}	
		}
  	});
}

function clearMarkers() {
	for (var n = 0, marker; marker = markers_arr[n]; n++) {
		marker.setVisible(false);
	}
}

function Grabar() {

    if( confirm("Esta seguro de guardar los cambios para estas zonales?") ) {
    	
    	var query = $("input.latlon").serializeArray();
		json = {};
		
		for (i in query) {
			json[query[i].name] = query[i].value;
			//alert("name=" + query[i].name + ", value=" + query[i].value);
		} 
		$("#fongoLoader").show();
        $.ajax({
            type:     "POST",
            dataType: "json",
            url:      "gescomp.zonal.main.php?action=save",
            data:     query,
            success:  function(datos) {
	        	//alert("Datos Grabados");
                //$("#divResult").html(datos);
                if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listar_edificios();
	            }
            }
        });
        $("#fongoLoader").hide();
        
    }

}
</script>
</head>

<body style="margin: 0px" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%" height="100%">
<tr>
	<td width="20%" valign="top">
	
		<?php include 'vistaMenuComponente.php'; ?>
		
		<table id="tblSearch" border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
		<tr>
			<td class="form_titulo" colspan="4">
				<img border="0" src="../../../img/plus_16.png">&nbsp;Mantenimiento de Zonales
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="height: 5px"></div>
			</td>
		</tr>
		</table>
		
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="sidebarHeader">
			<table border="0" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">
			<tr>
				<th width="70">#</th>
				<th width="70">Zonal</th>
				<th width="85">X</th>
				<th width="85">Y</th>
				<th width="40">Est</th>
			</tr>
			</table>
		</div>
		<div style="height:280px; width:100%; overflow:scroll;">
			<div id="sidebar"></div>
		</div>
		<input type="button" name="btnGrabar" id="btnGrabar" onclick="Grabar()" value="Grabar Datos" disabled="disabled">
		<input type="hidden" id="trm_selected" readonly="readonly">

	</td>
	<td width="80%" valign="top">
		<div id="map" style="width: 100%; height: 490px; float: left; background-color: #FFFFFF;"></div>
	</td>
</tr>
</table>

<div id="parentModal" style="display: none;">
	<div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>

</body>
</html>