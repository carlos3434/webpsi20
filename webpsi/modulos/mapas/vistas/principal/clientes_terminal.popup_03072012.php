<?php

/**
 * @package     modulos/maps/
 * @name        clientes_terminal.popup.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 */

require_once "../../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


$objTerminales = new Data_FfttTerminales();

$arrClientes = $objTerminales->getClientesByTerminal(
    $conexion, $_POST['zonal'], $_POST['mdf'], $_POST['cable'], 
    $_POST['armario'], $_POST['caja'], $_POST['tipo_red']
);

?>
<link href="css/maps.css" rel="stylesheet" type="text/css"/>
<style>
.tableBorder tbody tr:hover td {
    background-color: #DBEDFE;
}
</style>

<div style="height:380px; width:780px; overflow:auto;">
<table class="tableBorder" border="0" cellspacing="1" cellpadding="4" align="center" width="150%">
<tr>
    <td style=" background-color: #70C72A;color: #FFFFFF;font-size: 11px;font-weight: bold; line-height: 25px; text-align:center;" colspan="21">CLIENTES ASOCIADOS A ESTE TERMINAL: <?php echo $_POST['caja']?></td>
</tr>
<tr>
    <th width="5%" style="text-align:center;">#</th>
    <th width="8%" style="text-align:center;">Par</th>
    <th width="30%" style="text-align:left;">Cliente</th>
    <th width="30%" style="text-align:left;">Direcci&oacute;n</th>
    <th style="text-align:center;">Circuito</th>
    <th style="text-align:center;">Tel&eacute;fono</th>
    <th style="text-align:center;">Estado</th>
    <th style="text-align:center;">Speedy</th>
    <th style="text-align:center;">Producto</th>
    
    <th style="text-align:center; background-color:#FFFD8B;">Tipo Dslam</th>
    <th style="text-align:left; background-color:#FFFD8B;">Atenuaci&oacute;n de bajada</th>
    <th style="text-align:left; background-color:#FFFD8B;">Ruido de bajada</th>
    <th style="text-align:center; background-color:#FFFD8B;">Recomendaci&oacute;n de velocidad assia a 12db</th>
    <th width="20%" style="text-align:center; background-color:#FFFD8B;">Producto actual contratado por el cliente</th>
    <th style="text-align:center; background-color:#FFFD8B;">Sugerencia</th>
    <th style="text-align:center; background-color:#FFFD8B;">campo1</th>
    <th style="text-align:center; background-color:#FFFD8B;">campo2</th>
    <th style="text-align:center; background-color:#FFFD8B;">campo3</th>
</tr>
<tbody>
<?php 
if ( !empty($arrClientes) ) {

    $i = 1;
    foreach ( $arrClientes as $cliente ) {
    ?>
    <tr>
        <td style="text-align:center;"><?php echo $i?></td>
        <td style="text-align:center;"><?php echo ($cliente['Par'] != '') ? $cliente['Par'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Cliente'] != '') ? $cliente['Cliente'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Direccion'] != '') ? $cliente['Direccion'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Circuito'] != '') ? $cliente['Circuito'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Telefono'] != '') ? $cliente['Telefono'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['DescEstadoPar'] != '') ? $cliente['DescEstadoPar'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['marca_spd'] == 'S') ? 'SI' : 'NO'?></td>
        <td style="text-align:center;"><?php echo ($cliente['PRODUCTO'] != '') ? $cliente['PRODUCTO'] : '&nbsp;'?></td>
        
        <td style="text-align:center;"><?php echo ($cliente['tipodslam'] != '') ? $cliente['tipodslam'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['att_down'] != '') ? $cliente['att_down'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['snr_down'] != '') ? $cliente['snr_down'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['rservice_m12'] != '') ? $cliente['rservice_m12'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Mpproducto'] != '') ? $cliente['Mpproducto'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['sugerencia'] != '') ? $cliente['sugerencia'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['campo1'] != '') ? $cliente['campo1'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['campo2'] != '') ? $cliente['campo2'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['campo3'] != '') ? $cliente['campo3'] : '&nbsp;'?></td>
    </tr>
    <?php 
    $i++;
    }
} else {
?>
    <tr>
        <td colspan="7" style="text-align:center; height:50px;">No se encontro ningun cliente para este terminal.</td>
    </tr>
<?php 
}
?>
</tbody>
</table>
</div>
&nbsp;