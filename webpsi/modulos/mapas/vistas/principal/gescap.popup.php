<?php
require_once "../../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;






if( isset($_POST['action']) && $_POST['action'] == 'editar' ) {

	$btn_grabar = 'cmdEditar';

	$obj_capas = new data_ffttCapas();
	
	$capa = $obj_capas->get_capa_by_id($conexion, $_POST['idcapa']);
	$capa_campos = $obj_capas->get_campos_by_capa($conexion, $_POST['idcapa']);
	
	$array_campos = array();
	foreach( $capa_campos as $campo ) {
		$array_campos[$campo['campo_nro']] = $campo['campo'];
	}

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'searchAbv' ) {

	$obj_capas = new data_ffttCapas();
	
	$capa = $obj_capas->get_capa_by_abv($conexion, $_POST['abv_capa']);
	
	$result = ( !empty($capa) ) ? array('success'=>0,'msg'=>'Abreviatura ya existe.') : array('success'=>1,'msg'=>'Ok.');

	echo json_encode($result);
	exit;

}
elseif( isset($_POST['action']) && $_POST['action'] == 'cmdNuevo' ) {
	//echo 'aaaaaaaaaaa';exit;
	$obj_capas = new data_ffttCapas();
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	//capturo los campos ingresados a traves del formulario
	$data = array();
	foreach($_POST as $idcampo => $valcampo) { 
		if( $valcampo != '' && (substr($idcampo,0,5) == 'campo') ) {
			$data[substr($idcampo, 5)] = $valcampo;
		}
	}

	//insert de tabla fftt_capa, con upload de imagen
	$lastID = $obj_capas->__insertCapa($conexion, $_POST, $idusuario);
	
	//insert tabla fftt_capa_detalle
	foreach( $data as $idcampo => $valcampo ) {
		$data_result3 = $obj_capas->__insertCapaDetalle($conexion, $lastID, $idcampo, $valcampo, $idusuario);
	}
	
	//crear el directorio para la capa
	mkdir(FFTT . $_POST['abv_capa']);
	
	
	$result = ( $lastID ) ? array('success'=>1,'msg'=>'Datos grabados correctamente') : array('success'=>0,'msg'=>'Error al guardar los datos.');

	echo json_encode($result);
	exit;

}
elseif( isset($_POST['action']) && $_POST['action'] == 'cmdEditar' )  {

	$obj_capas = new data_ffttCapas();
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	//capturo los campos ingresados a traves del formulario
	$data = array();
	foreach($_POST as $idcampo => $valcampo) { 
		if( $valcampo != '' && (substr($idcampo,0,5) == 'campo') ) {
			$data[substr($idcampo, 5)] = $valcampo;
		}
	}

	
	//todos los campos de la capa
	$campos_capa = $obj_capas->get_campos_by_capa($conexion, $_POST['idcapa']);
	
	$array_campos = array();
	foreach( $campos_capa as $campo ) {
		$array_campos[$campo['campo_nro']] = $campo['campo'];
	}
	
	//veo cuales campos ya estan y cuales son nuevos, para una capa
	$array_campos_ins = array();
	$array_campos_upd = array();
	foreach( $data as $idcampo => $valcampo ) {
		if( array_key_exists( $idcampo, $array_campos ) ) {
		
			if( trim($valcampo) != trim($array_campos[$idcampo]) ) {
				$array_campos_upd[$idcampo] = $valcampo;
			}
		}else {
			$array_campos_ins[$idcampo] = $valcampo;
		}
	}
	
	
	//update de tabla fftt_capa
	$data_result = $obj_capas->__updateCapa($conexion, $_POST, $idusuario);
	
	//update tabla fftt_capa_detalle
	foreach( $array_campos_upd as $idcampo => $valcampo ) {
		$data_result2 = $obj_capas->__updateCapaDetalle($conexion, $_POST['idcapa'], $idcampo, $valcampo, $idusuario);
	}
	
	//insert tabla fftt_capa_detalle
	foreach( $array_campos_ins as $idcampo => $valcampo ) {
		$data_result3 = $obj_capas->__insertCapaDetalle($conexion, $_POST['idcapa'], $idcampo, $valcampo, $idusuario);
	}
	
	
	$result = ( $data_result ) ? array('success'=>1,'msg'=>'Datos grabados correctamente') : array('success'=>0,'msg'=>'Error al guardar los datos.');
	

	echo json_encode($result);
	exit;
	
}
else {

	$btn_grabar = 'cmdNuevo';
}

?>

<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(document).ready(function() {
	$("#cancelar").click(function() {
    	$("#childModal").dialog('close');
	});

	/*$("#upload").click(function() {
		//alert("ssss= " + abv);
    	$("#divUploadImagen").show();

    	$("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=ico" width="100%" height="100%" style="border: 0px;"></iframe>');
	});
	*/

	$(".upload").click(function() {
		//alert("ssss= " + abv);
    	$("#divUploadImagen").show();
    	var archivo = $(this).attr('id');

    	$("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=ico&archivo=archivo1" width="100%" height="100%" style="border: 0px;"></iframe>');
	});

	$("#divCerrarUploadImagen").click(function() {
    	$("#divUploadImagen").hide();
	});

	
	$("#cmdNuevo, #cmdEditar").click(function() {

		var idcapa 	 = $("#idcapa").val();
		var nombres  = $("#nombres").val();
		var abv_capa = $("#abv_capa").val();
		var ico 	 = $("#archivo1").val();

		if( nombres == '' ) {
			alert("Ingrese el campo Capa.");
			return false;
		}
		if( abv_capa == '' ) {
			alert("Ingrese el campo Abreviatura.");
			return false;
		}else {
			if( abv_capa.length != 3 ) {
				alert("La abreviatura debe tener 3 caracteres.");
				return false;
			}
		}

	    if( ico == '' ) {
	        alert('Seleccionar una imagen para la capa.');
	        return false;
	    }
		
		var campos = "";
		for (i=1; i<=20; i++) {
			if( $("#campo" + i).val() != '' ) {
				campos += "&campo" + i + "=" + $("#campo" + i).val();
			}
		}

		var action = "cmdNuevo";
		if(idcapa != '') {
			action = "cmdEditar";	
		}
		
		data_content = "action=" + action + "&idcapa=" + idcapa + "&nombre=" + nombres + "&abv_capa=" + abv_capa + "&ico=" + ico + campos;
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/vistas/principal/gescap.popup.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
   				if(datos['success'] == 1) {
   					$("#msg").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
   				}else {
   					$("#msg").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
   				}
   				
   				parent.$("#fongoLoader").show();
   				parent.$("#pag_actual").attr("value", 1);
   				data_content = "action=cambiarPaginaCapas";
   			    $.ajax({
   			        type:   "POST",
   			        url:    "modulos/maps/vistas/gescap.main.php",
   			        data:   data_content,
   			        success: function(datos) {
   						
   			        	var data = datos.split("||");
   			        	parent.$(".tb_listado").empty();
   			        	parent.$(".tb_listado").append(data[0]);

   			        	parent.$("#paginacion").html('');
   			        	parent.$("#cont_num_resultados_head").html(data[1]);
   			        	parent.$("#fongoLoader").hide();
   			        }
   			    });


   	         	setTimeout("cerrarPopup()", 1000);
            }
        });
	});
	
	
});

function cerrarPopup() {
	$("#childModal").dialog('close');
}

//$("#abv_capa").blur(function() {
function changeAbvCapa() {
    if( $("#abv_capa").val() == "" ) {
    	$("#msgAbv").html('');
		return false;
    }
    data_content = {
		'abv_capa' : $("#abv_capa").val()
    };
    $.ajax({
        type:   "POST",
        url:    "modulos/maps/vistas/principal/gescap.popup.php?action=searchAbv",
        data:   data_content,
        dataType: "json",
        success: function(datos) {
            if(datos['success'] == 1) {
					$("#msgAbv").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
				}else {
					$("#msgAbv").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
					$("#abv_capa").val('');
					$("#abv_capa").focus();
				}
        }
    });
}
</script>

</head>
<body>

<div id="divUploadImagen">
	<div id="divCerrarUploadImagen">[X]</div>
	<div id="divFormUploadImagen">
		
	</div>
</div>

<!-- 
<form method="post" action="modulos/maps/vistas/gescap.main.php" enctype="multipart/form-data" onsubmit="return validaCapa()">
 -->
<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td width="100">Capa:</td>
	<td><input type="text" name="nombres" id="nombres" value="<?php echo $capa[0]['nombre']?>" /></td>
</tr>
<tr>
	<td>Abreviatura:</td>
	<td>
		<?php 
		$readonly = ( $_POST['action'] == 'editar' ) ? 'readonly="readonly"' : 'onblur="changeAbvCapa();"' ;
		?>
		<input type="text" name="abv_capa" id="abv_capa" <?php echo $readonly?> maxlength="3" style="float: left;" value="<?php echo $capa[0]['abv_capa']?>" /> &nbsp;
		<span id="msgAbv" style="float: left; width: 200px; margin-left: 5px;"></span>
	</td>
</tr>
<tr>
	<td>Imagen:</td>
	<td>
		<input type="text" name="archivo1" id="archivo1" value="<?php echo $capa[0]['ico']?>" readonly="readonly"/>
		<!-- <input type="button" name="upload" id="upload" value=".." /> -->
		<input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
	</td>
</tr>
<?php 
for( $i=1; $i<=50; $i++ ) {

	//$readonly = ($array_campos[$i] != '' ) ? 'readonly="readonly"' : '';
	$readonly = ($array_campos[$i] != '' ) ? '' : '';
	
	if($i <= 40) {
		$text = 'varchar(100)';
	}elseif( $i > 40 && $i <= 45) {
		$text = 'int';
	}elseif( $i > 45 && $i <= 50) {
		$text = 'date';
	}
	
	?>
	<tr>
		<td>Campo <?php echo $i?>:</td>
		<td>
			<input type="text" size="40" class="campo" <?php echo $readonly?> name="campo<?php echo $i?>" id="campo<?php echo $i?>" value="<?php echo $array_campos[$i]?>" /> &nbsp;
			<?php echo $text?>
		</td>
	</tr>
<?php 
}
?>
<tr>
	<td colspan="2" height="20">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
		<input type="button" name="<?php echo $btn_grabar?>" id="<?php echo $btn_grabar?>" value="Grabar datos" />
		<input type="button" id="cancelar" value="Cancelar" />
	</td>
</tr>
</table>
<input type="hidden" name="idcapa" id="idcapa" value="<?php echo $_POST['idcapa']?>" />
<!-- 
</form>
 -->
<div id="msg"></div>

</body>
</html>






