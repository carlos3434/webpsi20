<html>
<head>
<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>

<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript">
var map = null;
var terminal_sel = 0;
var markers_arr = [];
$(document).ready(function(){

	$("#selZonal").change(function() {
        IDzon = this.value;
        if(IDzon == "") {
            return false;
        }
        data_content = "action=buscarNodo&IDzon=" + IDzon;
        $.ajax({
            type:   "POST",
            url:    "gescomp.troba.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selectNodo = '<option value="">Seleccione</option>';
                for(var i in datos) {
                    selectNodo += '<option value="' + datos[i]['IDnodo'] + '">' + datos[i]['IDnodo'] + ' - ' + datos[i]['Descnodo'] + '</option>'
                }
              	$("#selNodo").html(selectNodo);
            }
        });

        $("#sidebar").html('');
    });

	$("#selNodo").change(function() {
        $("#sidebar").html('');
    });


	$("#btnBuscar").click(function() {
		
        // lista y agregar los markers en divs "sidebar" y "map"
        listar_edificios();
    });

	$(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });
    
    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });

});


function listar_edificios() {

    IDzonal	 = $("#selZonal").val();
    IDnodo   = $("#selNodo").val();
    datosXY = $("#selDatosXY").val();
    if(IDzonal == "" || IDnodo == "") {
        alert("Ingrese todos los criterios de busqueda.");
        return false;
    }
	

    if (typeof (google) == "undefined") {
		alert('Verifique su conexion a maps.google.com');
		return false;
	}
    
	var mapOpts = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
	        scaleControl: true
	    }
    map = new google.maps.Map(document.getElementById("map"), mapOpts);
	
	/**
     * makeMarker() ver 0.2
     * creates Marker and InfoWindow on a Map() named 'map'
     * creates sidebar row in a DIV 'sidebar'
     * saves marker to markerArray and markerBounds
     * @param options object for Marker, InfoWindow and SidebarItem
     */
     var infoWindow = new google.maps.InfoWindow();
     var markerBounds = new google.maps.LatLngBounds();
     var markerArray = [];
          
     function makeMarker(options){
		var pushPin = null;
			
     	if( options.position != '(0, 0)' ) {
	            pushPin = new google.maps.Marker({
	                map: map
	            });
	            pushPin.setOptions(options);
	            google.maps.event.addListener(pushPin, "click", function(){
	                infoWindow.setOptions(options);
	                infoWindow.open(map, pushPin);
	                if(this.sidebarButton)this.sidebarButton.button.focus();
	            });
	            var idleIcon = pushPin.getIcon();

	            markerBounds.extend(options.position);
	            markerArray.push(pushPin);
     	}else {
         	
     		pushPin = new google.maps.Marker({
                map: map
            });
            pushPin.setOptions(options);
     	}
         
         if(options.sidebarItem){
             pushPin.sidebarButton = new SidebarItem(pushPin, options);
             pushPin.sidebarButton.addIn("sidebar");
         }
         
         return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         var tag = opts.sidebarItemType || "span";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "290px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }


     //data_content = "action=buscarEdificios&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&datosXY=" + datosXY;
     data_content = "action=buscarTrobas&IDzonal=" + IDzonal + "&IDnodo=" + IDnodo + "&datosXY=" + datosXY;
     $("#fongoLoader").show();
     $("#btnBuscar").attr('disabled', 'disabled');
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "gescomp.troba.main.php",
         data:   data_content,
         dataType: "json",
         success: function(data) {

    	 	if( data['edificios'].length > 0 ) {


    	 		if( data['edificios_con_xy'].length == 0 ) {

    	 			var datos = data['edificios']; 
    	 			var estados = data['estados'];

    	 			var x1 = y1 = null;
    	 			//if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
    	 			//	x1 = data['mdf']['x'];
    	 			//	y1 = data['mdf']['y'];
    	 			//}else {
    	 				x1 = data['zonal']['x'];
    	 				y1 = data['zonal']['y'];
    	 			//}

    	 			var myOptions = {
    	 				zoom: 13,
    	 			    center: latlng,
    	 			    mapTypeId: google.maps.MapTypeId.ROADMAP
    	 			};
    	 			map = new google.maps.Map(document.getElementById("map"), myOptions);
        	     	
                	for (var i in datos) {
                		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
        				tabla += '<tr id="row' + datos[i]['troba'] + '" class="unselected">';
        				tabla += '<td width="70">';
        				tabla += '<span id="' + datos[i]['troba'] + '" onclick="ir_mapa(\'' + datos[i]['troba'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
        				//tabla += '&nbsp;<a class="tooltip" href="#"><img title="Cambiar estado" border="0" src="../../../img/edit_agenda.png"><span>';
        				for(var e in estados) {
        					//tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'I\')"><img src="../../../img/map/edificio/edificio-I.gif" width="12"> Iniciado</p>';
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'P\')"><img src="../../../img/map/edificio/edificio-P.gif" width="12"> En proceso</p>';
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'T\')"><img src="../../../img/map/edificio/edificio-T.gif" width="12"> Terminados</p>';
        				//tabla += '</span></a></td>';
        				tabla += '<td width="70">' + datos[i]['troba'] + '</td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['troba'] + '" name="xy[' + datos[i]['troba'] + '][x]" value="' + x + '"></td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['troba'] + '" name="xy[' + datos[i]['troba'] + '][y]" value="' + y + '"></td>';
        				tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/terminal/terminal-1.gif" width="12"></td>';
        				tabla += '</tr>';
        				tabla += '</table>';

                        /* markers and info window contents */
                        makeMarker({
                        	draggable: false,
                            position: new google.maps.LatLng(y1, x1),
                            title: datos[i]['troba'],
                            //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
                            sidebarItem: tabla,
                            //content: detalle,
                            icon: "../../../img/markergreen.png"
                        });
                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    //map.setZoom(10);
                    

    	 			
    	 		}else {

    	 		
	    	 		var datos = data['edificios']; 
	    	 		var estados = data['estados'];
	    	     	
	            	for (var i in datos) {
	
	            		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
	            		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

	            		/*var est_caja = "";
	            		if( datos[i]['qparlib'] == 0 ) {
	            			est_caja = 3;
	            		}else if( datos[i]['qparlib'] > 0 && datos[i]['qparlib'] < 3 ) {
	            			est_caja = 2;
	            		}else if( datos[i]['qparlib'] > 2 ) {
	            			est_caja = 1;
	            		}*/

	            		var est_caja = 1;
	            		
	            		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
	    				tabla += '<tr id="row' + datos[i]['troba'] + '" class="unselected">';
	    				tabla += '<td width="70">';
	    				tabla += '<span id="' + datos[i]['troba'] + '" onclick="ir_mapa(\'' + datos[i]['troba'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
	    				//tabla += '&nbsp;<a class="tooltip" href="#"><img border="0" title="Cambiar estado" src="../../../img/edit_agenda.png"><span>';
	    				for(var e in estados) {
        					//tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'I\')"><img src="../../../img/map/edificio/edificio-I.gif" width="12"> Iniciado</p>';
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'P\')"><img src="../../../img/map/edificio/edificio-P.gif" width="12"> En proceso</p>';
        				//tabla += '<p onclick="changeEstado(\'' + datos[i]['idedificio'] + '\',\'T\')"><img src="../../../img/map/edificio/edificio-T.gif" width="12"> Terminados</p>';
	    				//tabla += '</span></a></td>';
	    				tabla += '<td width="70">' + datos[i]['troba'] + '</td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['troba'] + '" name="xy[' + datos[i]['troba'] + '][x]" value="' + x + '"></td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['troba'] + '" name="xy[' + datos[i]['troba'] + '][y]" value="' + y + '"></td>';
	    				tabla += '<td width="20"><img border="0" title="' + data['est'][est_caja] + '" src="../../../img/map/terminal/terminal-' + est_caja + '.gif" width="12"></td>';
	    				tabla += '</tr>';
	    				tabla += '</table>';
	
	    				//detalle = '<img border="0" src="../../../pages/imagenComponente.php?imgcomp=' + datos[i]['foto1'] + '&dircomp=edi&w=120"><br />';
	    				//detalle = '<b>Zonal:</b> LIMA<br />';
	    				//detalle += '<b>Mdf:</b> ' + datos[i]['mdf'] + '<br />';
	    				//detalle += '<b>Caja:</b> ' + datos[i]['caja'] + '<br />';
	    				//detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br /><br />';
	    				//detalle += '<a href="javascript:editarEdificio(\'' + datos[i]['idedificio'] + '\',\'' + datos[i]['item'] + '\',\'' + datos[i]['x'] + '\',\'' + datos[i]['y'] + '\');">Editar Edificio</a> | ';
	    				//detalle += '<a href="javascript:agregarImagenesEdificio(\'' + datos[i]['idedificio'] + '\', \'' + datos[i]['item'] + '\', \'' + datos[i]['x'] + '\', \'' + datos[i]['y'] + '\')">Agregar nuevo grupo de imagenes</a>';
	            		

						detalle = '<b>Nodo:</b> ' + datos[i]['nodo'] + '<br />';
						detalle += '<b>Troba:</b> ' + datos[i]['troba'] + '<br />';
						detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
						//detalle += '<a title="Ver clientes asignados a este terminal" href="javascript:clientesTerminal(\''+ datos[i]['zonal1'] +'\',\''+ datos[i]['mdf'] +'\',\''+ datos[i]['cable'] +'\',\''+ datos[i]['armario'] +'\',\''+ datos[i]['caja'] +'\',\''+ datos[i]['tipo_red'] +'\');">Ver clientes</a>';		

						
	                    /* markers and info window contents */
	                    makeMarker({
	                    	draggable: false,
	                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
	                        title: datos[i]['troba'],
	                        //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
	                        sidebarItem: tabla,
	                        content: detalle,
	                        //icon: "../../../img/map/tap/tap_" + datos[i]['tap'] + "--" + est_caja + ".gif"
	                        icon: "../../../img/map/terminal/terminal-1.gif"
	                    });
	            		
	                }
	
	                /* fit viewport to markers */
	                map.fitBounds(markerBounds);

    	 		}
    	 		
 	 		}else {

	 	 		//if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
	 			//	var latlng = new google.maps.LatLng(data['mdf']['y'], data['mdf']['x']);
	 			//}else {
	 				var latlng = new google.maps.LatLng(data['zonal']['y'], data['zonal']['x']);
	 			//}
	
	 			var myOptions = {
	 				zoom: 13,
	 			    center: latlng,
	 			    mapTypeId: google.maps.MapTypeId.ROADMAP
	 			};
	 			map = new google.maps.Map(document.getElementById("map"), myOptions);
 	 		
 	 		}

            //$("#divResult").html(datos);
            $("#btnBuscar").attr('disabled', '');
            $("#btnGrabar").attr('disabled', '');
            //setTimeout("hideLoader()", 1000);
            $("#fongoLoader").hide();
         }
     });
}


function ir_mapa(terminal_sel) {
	//alert("aaa=" + terminal_sel);
	
	//var terminal_sel = $(this).attr("id");
	$("#trm_selected").val(terminal_sel);
	
	$(".unselected").attr("class", "unselected");
	$(".selected").attr("class", "unselected");
	$("#row" + terminal_sel).attr("class", "selected");

	google.maps.event.addListener(map, "click", function(event) {

		clearMarkers();
		
		marker = new google.maps.Marker({
            position: event.latLng,
            map: map, 
            title: terminal_sel
        });
		markers_arr.push(marker);
		
		if( $("#trm_selected").val() == terminal_sel ) {
			var Coords = event.latLng;
	        //alert("term sel=" + terminal_sel + ", coord=" + Coords);
			
			//var confirmar = confirm("Desea Registrar este lat, lng?");
			//if(confirmar) {
				$("#x" + terminal_sel).val(Coords.lng());
				$("#y" + terminal_sel).val(Coords.lat());
			//}	
		}
  	});
}

function clearMarkers() {
	for (var n = 0, marker; marker = markers_arr[n]; n++) {
		marker.setVisible(false);
	}
}

function Grabar() {

	IDzonal	 		= $("#selZonal").val();
	IDnodo	 		= $("#selNodo").val();

	if(IDzonal == "" || IDnodo == "") {
        alert("Ingrese todos los criterios de busqueda.");
        return false;
    }

	if( confirm("Esta seguro de guardar los cambios para estas trobas?") ) {

		var query = $("input.latlon").serializeArray();
		json = {};
		
		for (i in query) {
			json[query[i].name] = query[i].value;
		} 
		$("#fongoLoader").show();
        $.ajax({
            type:     "POST",
            dataType: "json",
            url:      "gescomp.troba.main.php?action=save&IDnodo=" + IDnodo,
            data:     query,
            success:  function(datos) {
                if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listar_edificios();
	            }
            }
        });
        $("#fongoLoader").hide();
	}
}



</script>

</head>

<body style="margin: 0px" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%" height="100%">
<tr>
	<td width="20%" valign="top">
	
		<?php include 'vistaMenuComponente.php'; ?>
		
		<table id="tblSearch" border="0" cellspacing="1" cellpadding="0" align="center" width="300">
		<thead>
		<tr>
			<td class="form_titulo" colspan="4">
				<img border="0" src="../../../img/plus_16.png">&nbsp;Mantenimiento de Taps
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="height: 5px"></div>
			</td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Zonal:</td>
			<td>
				<select id="selZonal">
					<option value="">Seleccione</option>
					<?php foreach ($array_zonal as $obj_zonal) { ?>
                    	<option value="<?php echo $obj_zonal->__get('abv_zonal') ?>"><?php echo $obj_zonal->__get('abv_zonal') . ' - ' . $obj_zonal->__get('desc_zonal') ?></option>
                    <?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Nodo:</td>
			<td>
				<div id="divNodo">
					<select id="selNodo">
						<option value="">Seleccione</option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td>Con datos X,Y:</td>
			<td>
				<select id="selDatosXY">
					<option value="">Todos</option>
					<option value="conXY">Terminales con X,Y</option>
					<option value="sinXY">Terminales sin X,Y</option>
				</select>
			</td>
		</tr>
		<!--<tr>
			<td>Cable:</td>
			<td>
				<select id="SelCable">
					<option value="">Seleccione</option>
				</select>
			</td>
		</tr>-->
		<tr>
			<td></td>
			<td><input type="button" id="btnBuscar" value="Buscar terminales"></td>
		</tr>
		</tbody>
		</table>
	
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="sidebarHeader">
			<table border="0" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">
			<tr>
				<th width="70">#</th>
				<th width="70">Tap</th>
				<th width="85">X</th>
				<th width="85">Y</th>
				<th width="40">Est</th>
			</tr>
			</table>
		</div>
		<div style="height:220px; width:100%; overflow:scroll;">
			<div id="sidebar"></div>
		</div>
		<input type="button" name="btnGrabar" id="btnGrabar" onclick="Grabar()" value="Grabar Datos" disabled="disabled">
		<input type="hidden" id="trm_selected" readonly="readonly">

	</td>
	<td width="80%" valign="top">
		<div id="map" style="width: 100%; height: 490px; float: left; background-color: #FFFFFF;"></div>
	</td>
</tr>
</table>

<div id="parentModal" style="display: none;">
	<div id="childModal" style="padding: 10px; background: #fff;"></div>
</div>

</body>
</html>