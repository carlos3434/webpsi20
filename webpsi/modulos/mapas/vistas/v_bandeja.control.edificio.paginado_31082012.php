<?php
if($totalRegistros>0){
          
	$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                    
	foreach($arrObjEdificio as $objEdificio){
		?>

		<tr>
			<td class="celdaX"><?php echo $item++; ?></td>
			<td class="celdaX">
                <a title="Ver detalle" onclick="javascript:detalleEdificio('<?php echo $objEdificio->__get('_idEdificio')?>')" href="javascript:void(0)">
                    <img width="12" height="12" src="img/ico_detalle.jpg" title="Ver detalle">
                </a>
                <a onclick="editarEdificio('<?php echo $objEdificio->__get('_idEdificio')?>')" href="javascript:void(0)">
                    <img width="13" height="13" title="Editar" border="0" src="img/pencil.png">
                </a> 
                                    
			</td>
            <td class="celdaX"><?php echo $objEdificio->__get('_item'); ?></td>
            <td class="celdaX"><?php if($objEdificio->__get('_idTipoProyecto') != '30') echo $objEdificio->__get('_tipoProyecto'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_nombreProyecto'); ?></td>
            <td class="celdaX">
                <?php echo substr(obtenerFecha($objEdificio->__get('_fechaRegistroProyecto')),0,10); ?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_zonal'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_ura'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_sector'); ?></td>
            <td class="celdaX"><?php echo $objEdificio->__get('_mzaTdp'); ?></td>
            <td class="celdaX" align="left">
                <?php echo strtoupper(substr($objEdificio->__get('_direccionObra'),0,50)); ?><?php if(strlen($objEdificio->__get('_direccionObra')) > 50) echo '...'?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_numero'); ?></td>
            <td class="celdaX">
                <?php if($objEdificio->__get('_idTipoCchh') != '24') echo $objEdificio->__get('_tipoCchh'); ?>
            </td>
            <td class="celdaX"><?php echo $objEdificio->__get('_cchh'); ?></td>
            <td class="celdaX">
                <span title="Doble click para cambiar el estado" class="spn_estado" id="spn_estado_<?php echo $objEdificio->__get('_idEdificio'); ?>" ondblclick="viewCambiarEstado('<?php echo $objEdificio->__get('_idEdificio'); ?>')"><?php if( $objEdificio->__get('_estado') != '' ) echo $objEdificio->__get('_desEstado'); ?></span>
                <select class="chg_estado" onchange="cambiarEstado('<?php echo $objEdificio->__get('_idEdificio'); ?>')" name="chg_estado_<?php echo $objEdificio->__get('_idEdificio'); ?>" id="chg_estado_<?php echo $objEdificio->__get('_idEdificio'); ?>" style="display:none;">
                    <?php foreach($arrObjEdificioEstado as $objEdificioEstado) { ?>
                        <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado') ?>" <?php if($objEdificioEstado->__get('_codEdificioEstado') == $objEdificio->__get('_estado') ) echo 'selected="selected"'; ?> ><?php echo $objEdificioEstado->__get('_desEstado') ?></option>
                    <?php 
                    } 
                    ?>
                </select>
            </td>
		</tr>
 <?php
   }

} else {
    ?>
    <tr>
        <td style="height:30px" colspan="15">Ningun registro encontrado con los criterios de b&uacute;squeda ingresados</td>
    </tr>
<?php
}
?>