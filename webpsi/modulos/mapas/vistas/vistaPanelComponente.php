<?php
require_once "../../../config/web.config.php";

require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


global $conexion;

?>

<html>
<head>

<link href="../../../css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="../../../css/header.css" rel="stylesheet" type="text/css" media="all"/>


<script type="text/javascript" src="../../../js/jquery/jquery-1.3.2.js"></script>

<script type="text/javascript">
if ($.browser.msie){ 
    if ($.browser.version < 8) {
      window.location="requerimientos.php";
    }
}
</script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

</head>

<body style="margin: 0px; background: #FFFFFF;" oncontextmenu="return false">

<table border="0" cellspacing="1" cellpadding="0" align="center" width="95%">
<tr>
	<td width="100%" valign="top">
		
		<table id="tblSearch" border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
		<tr>
			<td class="form_titulo" colspan="4">
				<img border="0" src="../../../img/plus_16.png">&nbsp;Gestion de Componentes
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="height: 5px"></div>
			</td>
		</tr>
		</table>
		

		<div style="margin: 10px; text-align:left; border: 1px solid #CCCCCC; background-color: #F2F2F2; height: 380px; width:100%;">
			<div id="box_men_adm" style="width:100%;">
			    <?php 
			    if( !empty($_SESSION['USUARIO_CAPAS']) ) {
				    foreach( $_SESSION['USUARIO_CAPAS'] as $obj_capa ) {
				    	if( $obj_capa->__get('tiene_tabla') == 'S' ) {
					    	if( $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'adm' || $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'spu' ) {
						    ?>
						    <div>
						        <table width="100%" border="0">
						            <tr>
						                <td width="80%"><a href="<?php echo $obj_capa->__get('url')?>" style=" line-height: 20px; margin-left: 10px; font-weight:bold; font-size:11px;">Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?></a></td>
						                <td width="10%"><a href="<?php echo $obj_capa->__get('url')?>" style="float: right;"><img src="../../../img/search_16.png" alt="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" title="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" border="0"/></a></td>
						                <td width="10%">&nbsp;</td>
						            </tr>
						        </table>
						    </div>
						    <?php
						    }elseif( $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'ope' ) {
							    if( $obj_capa->__get('es_fftt') == 'N' ) {
							    ?>
							    <div>
							        <table width="100%" border="0">
							            <tr>
							                <td width="80%"><a href="<?php echo $obj_capa->__get('url')?>" style=" line-height: 20px; margin-left: 10px; font-weight:bold; font-size:11px;">Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?></a></td>
							                <td width="10%"><a href="<?php echo $obj_capa->__get('url')?>" style="float: right;"><img src="../../../img/search_16.png" alt="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" title="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" border="0"/></a></td>
							                <td width="10%">&nbsp;</td>
							            </tr>
							        </table>
							    </div>
							    <?php
							    }
						    }
				    	}
				    }
			    }
			    ?> 
			</div>
		</div>
	</td>
</tr>
</table>


</body>
</html>