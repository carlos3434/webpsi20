<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


$obj_capasarea = new data_ffttCapasArea();
$array_capas_ditrito 	= $obj_capasarea->get_capas_by_tipocapa($conexion, 'DISTRITO');
$array_capas_nodo 		= $obj_capasarea->get_capas_by_tipocapa($conexion, 'NODO');
$array_capas_mdf 		= $obj_capasarea->get_capas_by_tipocapa($conexion, 'MDF');
$array_capas_plano 		= $obj_capasarea->get_capas_by_tipocapa($conexion, 'PLANOS');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<!-- <script type="text/javascript" src="../../../js/administracion/jquery.ui.all.js"></script> -->
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<!-- <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script> -->
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<!-- <script src="../../../js/ui/jquery.ui.core.js"></script>
<script src="../../../js/ui/jquery.ui.widget.js"></script>
<script src="../../../js/ui/jquery.ui.datepicker.js"></script> -->


<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript">
$(function(){
	$("select.clsMultiple").multiselect();
});
</script>
<script type="text/javascript">

var myLayout;
var map = null;
var poligonosDibujar_array = [];
var markers_arr = [];
var capa_exists = 0;

$(document).ready(function () {
	
	myLayout = $('body').layout({
		// enable showOverflow on west-pane so popups will overlap north pane
		west__showOverflowOnHover: true
	
	//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
	});

	load_array=function(element){
        var grupo=new Array();
        $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
        return grupo;
    }

	var M = {
    	initialize: function() {

			if (typeof (google) == "undefined") {
				alert('Verifique su conexion a masp.google.com');
				return false;
			}
    	
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 11,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

			

	        google.maps.event.addListener(map, "click", function(event) {
		        //clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });
        },

        clearMarkers: function() {
        	for (var n = 0, marker; marker = markers_arr[n]; n++) {
        		marker.setVisible(false);
        	}
        },

        popupComponentes: function(x, y) {

        	var zoom_selected = map.getZoom();
            
	        $("#childModal").html('');
	        $("#childModal").css("background-color","#FFFFFF");

	        $.post("v_buscomp_popup.php", {
	    	        x: x,
	    	        y: y, 
	    	        zoom_selected: zoom_selected
	    	    },
	        	function(data){
	            	$("#childModal").html(data);
	        	}
    		);
	
	        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
        },

        clearPoligonos: function() {
        	for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
    			poligonoDibujar.setMap(null);
        	}
        },

        loadPoligono: function(xy_array, z) {

			var datos 			= xy_array['coords'];
			var color_poligono 	= xy_array['color'];
            var poligonoCoords 	= [];

        	if( datos.length > 0) {
        		for (var i in datos) {
            		var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
            		poligonoCoords.push(pto_poligono);
            	}

                // Construct the polygon
                // Note that we don't specify an array or arrays, but instead just
                // a simple array of LatLngs in the paths property
                poligonoDibujar = new google.maps.Polygon({
                	paths: poligonoCoords,
                    strokeColor: color_poligono,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: color_poligono,
                    fillOpacity: 0.35
                });

                //agrego al array las capas seleccionadas
                poligonosDibujar_array.push(poligonoDibujar);

                //seteo la capa en el mapa
                poligonoDibujar.setMap(map);

                //agrego el evento click en cada capa
                google.maps.event.addListener(poligonoDibujar, 'click', function(capaEvent) {
    		   		
    		   		//clear markers
    			    M.clearMarkers();

    		   		marker = new google.maps.Marker({
    		            position: capaEvent.latLng,
    		            map: map
    		        });
    		    	markers_arr.push(marker);
    		    	var Coords = capaEvent.latLng;
    		    	var x = Coords.lng();
    		    	var y = Coords.lat();
    		    	M.popupComponentes(x, y);
    		    	//alert("x=" + x);
    		    });

              	//seteamos que la capa existe
    			capa_exists = 1;

        	}
        	else {
            	//alert('aaa');
        		capa_exists = 0;
        	}		
        }
    };


	$('#btnFiltrar').click(function(){
		var arreglo_distrito = new Array();
		var arreglo_nodo 	 = new Array();
		var arreglo_mdf	 	 = new Array();
		var arreglo_plano	 = new Array();
		
		arreglo_distrito = load_array('multiselect_distrito[]');
		arreglo_nodo 	 = load_array('multiselect_nodo[]');
		arreglo_mdf 	 = load_array('multiselect_mdf[]');
		arreglo_plano 	 = load_array('multiselect_plano[]');
		
		//$("#box_resultado").hide();
		$('#box_loading').show();
		data_content = {
			'action'			: 'buscar',
			'arreglo_distrito' 	: arreglo_distrito,
			'arreglo_nodo' 		: arreglo_nodo, 
			'arreglo_mdf' 		: arreglo_mdf,
			'arreglo_plano' 	: arreglo_plano  
		};
		$.ajax({
	         type:   "POST",
	         url:    "../gescomp.poligono.php",
	         data:   data_content,
	         dataType: "json",
	         success: function(data) {

				if( capa_exists ) {
					//limpiamos todas las capas del mapa
					M.clearPoligonos();
				}

				var poligonos = data['xy_poligono'];

				if( poligonos.length > 0 ) {
					//pintamos los poligonos
					for (var i in poligonos) {
						M.loadPoligono(poligonos[i], i);
					}

					//setea el zoom y el centro
					map.setCenter(new google.maps.LatLng(data['y'], data['x']));
					map.setZoom(15);
				}

				$('#box_loading').hide();
				//$("#box_resultado").empty();
				//$("#box_resultado").append(data);
				//$("#box_resultado").show();
			}
		});
	             
	});



	//M.initialize();


	var sites = <?php echo json_encode($data_markers)?>;
	var punto_sel = {x: <?php echo $_POST['x']?>, y: <?php echo $_POST['y']?>};

	var capa_ico = <?php echo json_encode($data_markers_img)?>;

	var capa_kml = <?php echo json_encode($capa_selected)?>;

	L.loadComponentes();
               
});

</script>
<style type="text/css">
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 7px; 
		overflow: auto;
	} 

	.ui-layout-resizer {
		background: #DDD; 
	} 

	.ui-layout-toggler {
		background: #AAA; 
	}
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
<script>
	$(function() {
		$("#btnFiltrar").button();
	});
	</script>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
    <div class="ui-layout-west">
        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
            <tr>
                <td><span class="l_tit">Dibujar Poligono</span></td>
            </tr>
            <tr>
                <td align="left" class="label">Seleccione Distrito</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="distrito" id="distrito" class="clsMultiple" multiple="multiple">
                    <?php 
                    	foreach( $array_capas_ditrito as $distrito ) {
                    ?>
                    	<option value="<?php echo $distrito['elemento']?>"><?php echo $distrito['elemento']?></option>
                    <?php 
                    	}
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left" class="label">Seleccione Mdf</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="mdf" id="mdf" class="clsMultiple" multiple="multiple">
                    <?php 
                    	foreach( $array_capas_mdf as $mdf ) {
                    ?>
                    	<option value="<?php echo $mdf['elemento']?>"><?php echo $mdf['elemento']?></option>
                    <?php 
                    	}
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left" class="label">Seleccione Nodo</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="nodo" id="nodo" class="clsMultiple" multiple="multiple">
                    <?php 
                    	foreach( $array_capas_nodo as $nodo ) {
                    ?>
                    	<option value="<?php echo $nodo['elemento']?>"><?php echo $nodo['elemento']?></option>
                    <?php 
                    	}
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left" class="label">Seleccione Plano</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="plano" id="plano" class="clsMultiple" multiple="multiple">
                    <?php 
                    	foreach( $array_capas_plano as $plano ) {
                    ?>
                    	<option value="<?php echo $plano['elemento']?>"><?php echo $plano['elemento']?></option>
                    <?php 
                    	}
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <br /><button id="btnFiltrar">Aceptar</button>
                </td>
            </tr>
        </table>
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
        
    </div>
    <div class="ui-layout-center">
        
        <div id="box_resultado">
           
           
           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        </div>
        
    </div>
        
	<div id="parentModal" style="display: none;">
    	<div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>