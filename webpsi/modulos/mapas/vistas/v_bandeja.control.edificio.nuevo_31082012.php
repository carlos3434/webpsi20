<style>
.limpiar_fecha {
    color: #0000FF !important;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#fecha_registro_proyecto, #montante_fecha, #ducteria_interna_fecha, #punto_energia_fecha, #fecha_seguimiento, #fecha_termino_construccion").datepicker({
        yearRange:'-10:+10',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy'
    });
    
    $(".limpiar_fecha").click(function() {
        var campo = $(this).attr('c_fecha');
        $("#" + campo).attr('value', '');
    });
    
    $("#nro_blocks").change(function() {
        var nro_blocks = this.value;
        if(nro_blocks == "") {
            $("#divBlock").html('');
        }else {
            $("#divBlock").html('');
            selhtml = '<div style="width:180px;">';
            selhtml += '<span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>';
            selhtml += '<span style="float:left; width:80px;">Nro. dptos</span>';
            selhtml += '</div>';
            for(var i=1; i<=nro_blocks; i++) {
                selhtml += '<div style="width:180px;">';
                selhtml += '<span style="float:left; width:20px;">#' + i + ' </span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_' + i + '" id="nro_pisos_' + i + '" size="5" value="" /></span>';
                selhtml += '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_' + i + '" id="nro_dptos_' + i + '" size="5" value="" /></span>';
                selhtml += '</div>';
            }
            $("#divBlock").html(selhtml);
        }
    });
    
    soloNumeros = function(obj) {
        var re = /^(-)?[0-9]*$/;
        if (!re.test(obj.value)) {
            obj.value = '';
        }
    }
    
    ListarProvinciasUbigeo =  function() {
        var coddpto = $("#cmb_departamento").attr('value');
        
        $("select[id=cmb_provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_distrito]").html("<option value=''>Seleccione</option>");

        data_content = {
            'coddpto'   : coddpto
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=provinciasUbigeo",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_provincia]").append("<option value='"+data[i]['codProv']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        }); 
    }
    
    ListarDistritosUbigeo =  function() {
        var coddpto = $("#cmb_departamento").attr('value');
        var codprov = $("#cmb_provincia").attr('value');
        
        $("select[id=cmb_distrito]").html("<option value=''>Seleccione</option>");

        data_content = {
            'coddpto'   : coddpto,
            'codprov'   : codprov
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=distritosUbigeo",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_distrito]").append("<option value='"+data[i]['codDist']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        }); 
    }
    
    ListarMdfsZonalUsuario = function() {
        $("select[id=cmb_ura]").html("<option value=''>Seleccione</option>");
        
        var zonal = $("#cmb_zonal").attr('value');
        if(zonal == "") {
            return false;
        }

        data_content = {
            'zonal' : zonal
        };
        $.ajax({
            type:   "POST",
            url:    "modulos/maps/util/functions.php?cmd=obtenerMdfsZonalUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_ura]").append("<option value='"+data[i]['mdf']+"'>"+ data[i]['mdf'] + ' - ' +data[i]['nombre']+"</option>");
                });
            }
        });
    }
    
});
</script>

<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N EDIFICIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="18%" class="da_subtitulo">ITEM</td>
        <td width="32%" class="da_import">(AUTO)</td>
        <td width="18%" class="da_subtitulo">ESTADO</td>
        <td width="32%" class="da_import">
            <select name="cmb_estado" id="cmb_estado">
                <option value="">Seleccione</option>
                <?php foreach($arrObjEdificioEstado as $objEdificioEstado) { ?>
                    <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado') ?>" ><?php echo $objEdificioEstado->__get('_desEstado') ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ZONAL</td>
        <td>
            <select name="cmb_zonal" id="cmb_zonal" onchange="ListarMdfsZonalUsuario();">
                <option value="">Seleccione</option>
                <?php foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) { ?>
                    <option value="<?php echo $objZonal->__get('_abvZonal') ?>" <?php if($objZonal->__get('_abvZonal') == 'LIM') echo 'selected="selected"'; ?> ><?php echo $objZonal->__get('_abvZonal') . ' - ' . $objZonal->__get('_descZonal') ?></option>
                <?php } ?>
            </select>
        </td>
        <td class="da_subtitulo">URA</td>
        <td>
            <select name="cmb_ura" id="cmb_ura">
                <option value="">Seleccione</option>
                <?php 
                foreach( $arrMdf as $mdf ) {
                ?>
                <option value="<?php echo $mdf['mdf']?>" ><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
        <td>
            <input type="text" name="fecha_registro_proyecto" size="11" id="fecha_registro_proyecto" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_registro_proyecto">Limpiar</a>
        </td>
        <td class="da_subtitulo">SECTOR</td>
        <td><input type="text" name="sector" id="sector" value="" /></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MANZANA (TDP)</td>
        <td>
            <input type="text" name="mza_tdp" id="mza_tdp" value="" />
        </td>
        <td class="da_subtitulo">TIPO V&Iacute;A</td>
        <td>
            <select name="idtipo_via" id="idtipo_via">
                <option value="">Seleccione</option>
                <?php 
                foreach( $tipoViaList as $tipoVia ) {
                ?>
                <option value="<?php echo $tipoVia['iddescriptor']?>"><?php echo $tipoVia['nombre']?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
        <td colspan="3">
            <input type="text" name="direccion_obra" id="direccion_obra" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">N&Uacute;MERO</td>
        <td>
            <input type="text" name="numero" id="numero" value="" />
        </td>
        <td class="da_subtitulo">MANZANA</td>
        <td>
            <input type="text" name="mza" id="mza" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">LOTE</td>
        <td>
            <input type="text" name="lote" id="lote" value="" />
        </td>
        <td class="da_subtitulo">TIPO CCHH</td>
        <td>
            <select name="idtipo_cchh" id="idtipo_cchh">
                <option value="">Seleccione</option>
                <?php 
                foreach( $tipoCchhList as $tipoCchh ) {
                ?>
                <option value="<?php echo $tipoCchh['iddescriptor']?>"><?php echo $tipoCchh['nombre']?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CCHH</td>
        <td>
            <input type="text" name="cchh" id="cchh" class="da_editar" value="" />
        </td>
        <td class="da_subtitulo">DEPARTAMENTO</td>
        <td>
            <select name="cmb_departamento" id="cmb_departamento" onchange="ListarProvinciasUbigeo();">
                <option value="">Seleccione</option>
                <?php
                foreach( $arrObjDepartamento as $objDepartamento ) {
                ?>
                <option value="<?php echo $objDepartamento->__get('_codDpto')?>" <?php if('15' == $objDepartamento->__get('_codDpto')) echo 'selected="selected"';?> ><?php echo $objDepartamento->__get('_nombre')?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PROVINCIA</td>
        <td>
            <select name="cmb_provincia" id="cmb_provincia" onchange="ListarDistritosUbigeo();">
                <option value="">Seleccione</option>
                <?php
                foreach( $arrObjProvincia as $objProvincia ) {
                ?>
                <option value="<?php echo $objProvincia->__get('_codProv')?>" <?php if('01' == $objProvincia->__get('_codProv')) echo 'selected="selected"';?> ><?php echo $objProvincia->__get('_nombre')?></option>
                <?php
                }
                ?>
            </select>
        </td>
        <td class="da_subtitulo">DISTRITO</td>
        <td>
            <select name="cmb_distrito" id="cmb_distrito">
                <option value="">Seleccione</option>
                <?php
                foreach( $arrObjDistrito as $objDistrito ) {
                ?>
                <option value="<?php echo $objDistrito->__get('_codDist')?>"><?php echo $objDistrito->__get('_nombre')?></option>
                <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
        <td>
            <input type="text" name="nombre_constructora" id="nombre_constructora" class="da_editar" value="" />
        </td>
        <td class="da_subtitulo">NOMBRE PROYECTO</td>
        <td>
            <input type="text" name="nombre_proyecto" id="nombre_proyecto" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO PROYECTO</td>
        <td>
            <select name="idtipo_proyecto" id="idtipo_proyecto">
                <option value="">Seleccione</option>
                <?php 
                foreach( $tipoProyectoList as $tipoProyecto ) {
                ?>
                <option value="<?php echo $tipoProyecto['iddescriptor']?>"><?php echo $tipoProyecto['nombre']?></option>
                <?php
                }
                ?>
            </select>
        </td>
        <td class="da_subtitulo">PERSONA CONTACTO</td>
        <td>
            <input type="text" name="persona_contacto" id="persona_contacto" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">P&Aacute;GINA WEB</td>
        <td>
            <input type="text" name="pagina_web" id="pagina_web" class="da_editar" value="" />
        </td>
        <td class="da_subtitulo">EMAIL</td>
        <td>
            <input type="text" name="email" id="email" class="da_editar" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
        <td valign="top">
            <select id="nro_blocks" name="nro_blocks" style="width:80px;">
                <option value="">Seleccione</option>
                <?php 
                for($i=1;$i<=$numeroBlocks;$i++) {
                ?>
                <option value="<?php echo $i?>" ><?php echo $i?></option>
                <?php 
                }
                ?>
            </select>
        </td>
        <td colspan="2" valign="top">
            <div id="divBlock">
                
            </div>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
        <td>
            <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                <option value="">Seleccione</option>
                <?php 
                foreach( $tipoInfraestructuraList as $tipoInfraestructura ) {
                ?>
                <option value="<?php echo $tipoInfraestructura['iddescriptor']?>" ><?php echo $tipoInfraestructura['nombre']?></option>
                <?php
                }
                ?>
            </select>
        </td>
        <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
        <td>
            <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="" />
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">% AVANCE</td>
        <td>
            <input type="text" name="avance" id="avance" value="" />
        </td>
        <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N</td>
        <td>
            <input type="text" name="fecha_termino_construccion" id="fecha_termino_construccion" size="11" readonly="readonly" value="" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_termino_construccion">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">MONTANTE</td>
        <td>
            <select name="montante" id="montante" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> &nbsp; 
            Fecha: <input type="text" size="11" name="montante_fecha" id="montante_fecha" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="montante_fecha">Limpiar</a>
        </td>
        <td class="da_subtitulo">MONTANTE (OBS)</td>
        <td>
            <textarea name="montante_obs" id="montante_obs" style="width:90%;height:30px;"></textarea>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DUCTERIA INTERNA</td>
        <td>
            <select name="ducteria_interna" id="ducteria_interna" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> &nbsp; 
            Fecha: <input type="text" size="11" name="ducteria_interna_fecha" id="ducteria_interna_fecha" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="ducteria_interna_fecha">Limpiar</a>
        </td>
        <td class="da_subtitulo">DUCTERIA INTERNA (OBS)</td>
        <td>
            <textarea name="ducteria_interna_obs" id="ducteria_interna_obs" style="width:90%;height:30px;"></textarea>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PUNTO DE ENERGIA</td>
        <td>
            <select name="punto_energia" id="punto_energia" style="width:80px;">
                <option value="">Seleccione</option>
                <option value="S">Si</option>
                <option value="N">No</option>
            </select> &nbsp; 
            Fecha: <input type="text" size="11" name="punto_energia_fecha" id="punto_energia_fecha" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="punto_energia_fecha">Limpiar</a>
        </td>
        <td class="da_subtitulo">PUNTO DE ENERGIA (OBS)</td>
        <td>
            <textarea name="punto_energia_obs" id="punto_energia_obs" style="width:90%;height:30px;"></textarea>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
        <td colspan="3">
            <input type="text" size="11" name="fecha_seguimiento" id="fecha_seguimiento" value="" readonly="readonly" />
            <a href="javascript:void(0);" class="limpiar_fecha" c_fecha="fecha_seguimiento">Limpiar</a>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
        <td colspan="3">
            <textarea name="observacion" id="observacion" style="width:90%;height:60px;"></textarea>
        </td>
    </tr>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody><tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px">
                <input type="button" value="Grabar" name="btn_grabar" title="Grabar" id="btn_grabar" onclick="javascript: insertDatosEdificio();" class="boton">&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" onclick="javascript: cerrarVentana();" class="boton">
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
    <div style="margin: 10px 0 0 0; display: none;" id="msg">
        <img alt="" src="img/loading.gif"> <b>Grabando datos.</b>
    </div>
</div>


