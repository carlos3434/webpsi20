<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<!--<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>-->
<script type="text/javascript" src="../../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

var myLayout;
var map = null;
var markers_arr = [];
var ptos_arr = [];

$(document).ready(function () {
	
	myLayout = $('body').layout({
		// enable showOverflow on west-pane so popups will overlap north pane
		west__showOverflowOnHover: true,
		west: {size:330}
	
	//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
	});

	listarZonales = function() {
        data_content = "action=listarZonales";
        $.ajax({
            type:   "POST",
            url:    "../gescomp.mdf.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selectZonal = '';
                for(var i in datos) {
                    selectZonal += '<option value="' + datos[i]['abvZonal'] + '">' + datos[i]['abvZonal'] + ' - ' + datos[i]['descZonal'] + '</option>'
                }
                  $("#selZonal").append(selectZonal);
            }
        });
    }

	var M = {
    	initialize: function() {

			if (typeof (google) == "undefined") {
				alert('Verifique su conexion a maps.google.com');
				return false;
			}
    	
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 11,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	        //carga el combo zonales
            listarZonales();

        }
    };
	
	$("#btnBuscar").click(function() {
        // lista y agregar los markers en divs "sidebar" y "map"
        listarArmarios();
    });


	M.initialize(); 
});



function listarArmarios() {

	IDzonal	 = $("#selZonal").val();
    datosXY = $("#selDatosXY").val();
    if(IDzonal == "") {
        alert("Ingrese la Zonal.");
        return false;
    }
	
	
	/**
     * makeMarker() ver 0.2
     * creates Marker and InfoWindow on a Map() named 'map'
     * creates sidebar row in a DIV 'sidebar'
     * saves marker to markerArray and markerBounds
     * @param options object for Marker, InfoWindow and SidebarItem
     */
     var infoWindow = new google.maps.InfoWindow();
     var markerBounds = new google.maps.LatLngBounds();
     var markerArray = [];
          
     function makeMarker(options){
		var pushPin = null;
			/*var pushPin = new google.maps.Marker({
             map: map
         });
         pushPin.setOptions(options);*/
			
     	if( options.position != '(0, 0)' ) {
	            pushPin = new google.maps.Marker({
	                map: map
	            });
				
	            pushPin.setOptions(options);
	            google.maps.event.addListener(pushPin, "click", function(){
	                infoWindow.setOptions(options);
	                infoWindow.open(map, pushPin);
	                if(this.sidebarButton)this.sidebarButton.button.focus();
	            });
	            var idleIcon = pushPin.getIcon();

	            markerBounds.extend(options.position);
	            markerArray.push(pushPin);
     	}else {
     		pushPin = new google.maps.Marker({
                map: map
            });

            pushPin.setOptions(options);
     	}
         
         if(options.sidebarItem){
             pushPin.sidebarButton = new SidebarItem(pushPin, options);
             pushPin.sidebarButton.addIn("sidebar");
         }
         
         return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         var tag = opts.sidebarItemType || "span";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "290px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }

	 
	 data_content = "action=buscarMdfs&IDzonal=" + IDzonal + "&datosXY=" + datosXY;
     $("#fongoLoader").show();
     $("#btnBuscar").attr('disabled', 'disabled');
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "../gescomp.mdf.main.php",
         data:   data_content,
         dataType: "json",
         success: function(data) {
		 
			clearMarkers();

    	 	if( data['edificios'].length > 0 ) {


    	 		if( data['edificios_con_xy'].length == 0 ) {

    	 			var datos = data['edificios']; 
    	 			var estados = data['estados'];

    	 			var x1 = y1 = null;
    	 			if( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
    	 				x1 = data['zonal']['x'];
    	 				y1 = data['zonal']['y'];
    	 			}
					
                	for (var i in datos) {
                		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
        				tabla += '<tr id="row' + datos[i]['mdf'] + '" class="unselected">';
        				tabla += '<td width="70">';
        				tabla += '<span id="' + datos[i]['mdf'] + '" onclick="ir_mapa(\'' + datos[i]['mdf'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
        				tabla += '&nbsp;<a class="tooltip" href="#"><img title="Cambiar estado" border="0" src="../../../img/edit_agenda.png"><span>';
        				for(var e in estados) {
        					tabla += '<p onclick="changeEstado(\'' + datos[i]['abv_zonal'] + '\',\'' + datos[i]['mdf'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				tabla += '</span></a></td>';
        				tabla += '<td width="70">' + datos[i]['mdf'] + '</td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['mdf'] + '" name="xy[' + datos[i]['abv_zonal'] + '|' + datos[i]['mdf'] + '][x]" value="' + x + '"></td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['mdf'] + '" name="xy[' + datos[i]['abv_zonal'] + '|' + datos[i]['mdf'] + '][y]" value="' + y + '"></td>';
        				tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/mdf/mdf-' + datos[i]['estado'] + '.gif" width="12"></td>';
        				tabla += '</tr>';
        				tabla += '</table>';

                        /* markers and info window contents */
                        makeMarker({
                        	draggable: false,
                            position: new google.maps.LatLng(y1, x1),
                            title: '',
                            sidebarItem: tabla,
                            icon: "../../../img/markergreen.png"
                        });
                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    //map.setZoom(10);
                    

    	 			
    	 		}else {

    	 		
	    	 		var datos = data['edificios']; 
	    	 		var estados = data['estados'];
	    	     	
	            	for (var i in datos) {
	
	            		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
	            		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
	            		
	            		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
	    				tabla += '<tr id="row' + datos[i]['mdf'] + '" class="unselected">';
	    				tabla += '<td width="70">';
	    				tabla += '<span id="' + datos[i]['mdf'] + '" onclick="ir_mapa(\'' + datos[i]['mdf'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
	    				tabla += '&nbsp;<a class="tooltip" href="#"><img border="0" title="Cambiar estado" src="../../../img/edit_agenda.png"><span>';
	    				for(var e in estados) {
        					tabla += '<p onclick="changeEstado(\'' + datos[i]['abv_zonal'] + '\',\'' + datos[i]['mdf'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/mdf/mdf-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				tabla += '</span></a></td>';
	    				tabla += '<td width="70">' + datos[i]['mdf'] + '</td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['mdf'] + '" name="xy[' + datos[i]['abv_zonal'] + '|' + datos[i]['mdf'] + '][x]" value="' + x + '"></td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['mdf'] + '" name="xy[' + datos[i]['abv_zonal'] + '|' + datos[i]['mdf'] + '][y]" value="' + y + '"></td>';
	    				tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/mdf/mdf-' + datos[i]['estado'] + '.gif" width="12"></td>';
	    				tabla += '</tr>';
	    				tabla += '</table>';
						
						detalle = '<b>Zonal:</b> ' + datos[i]['abv_zonal'] + '<br />';
						detalle += '<b>Mdf:</b> ' + datos[i]['mdf'] + '<br />';
						detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
						
	                    /* markers and info window contents */
	                    makeMarker({
	                    	draggable: false,
	                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
	                        title: datos[i]['mdf'],
	                        sidebarItem: tabla,
	                        content: detalle,
	                        icon: "../../../img/map/mdf/mdf_" + datos[i]['mdf'] + "--" + datos[i]['estado'] + ".gif"
	                    });
	            		
	                }
	
	                /* fit viewport to markers */
	                map.fitBounds(markerBounds);

    	 		}
    	 		
 	 		}else {

				x_zon = data['zonal']['x'];
				y_zon = data['zonal']['y'];

				var latlng = new google.maps.LatLng(y_zon, x_zon);

				/* markers and info window contents */
				makeMarker({
					draggable: false,
					position: new google.maps.LatLng(y_zon, x_zon),
					title: '',
					icon: "../../../img/markergreen.png"
				});
				
				map.setCenter(latlng);
 	 		
 	 		}
         
            //$("#divResult").html(datos);
            $("#btnBuscar").attr('disabled', '');
            $("#btnGrabar").attr('disabled', '');
            //setTimeout("hideLoader()", 1000);
            $("#fongoLoader").hide();
         }
     });
}

function ir_mapa(terminal_sel) {
	$("#trm_selected").val(terminal_sel);
	
	$(".unselected").attr("class", "unselected");
	$(".selected").attr("class", "unselected");
	$("#row" + terminal_sel).attr("class", "selected");

	google.maps.event.addListener(map, "click", function(event) {

		clearMarkers();
		
		marker = new google.maps.Marker({
            position: event.latLng,
            map: map, 
            title: terminal_sel
        });
		markers_arr.push(marker);
		
		if( $("#trm_selected").val() == terminal_sel ) {
			var Coords = event.latLng;
	        //alert("term sel=" + terminal_sel + ", coord=" + Coords);
			
			//var confirmar = confirm("Desea Registrar este lat, lng?");
			//if(confirmar) {
				$("#x" + terminal_sel).val(Coords.lng());
				$("#y" + terminal_sel).val(Coords.lat());
			//}	
		}
  	});
}

function clearMarkers() {
	for (var n = 0, marker; marker = markers_arr[n]; n++) {
		marker.setVisible(false);
	}
}

function Grabar() {

	if( confirm("Esta seguro de guardar los cambios para estos mdfs?") ) {
    	
		IDzonal	 = $("#selZonal").val();
	
		var query = $("input.latlon").serializeArray();
		json = {};
		
		for (i in query) {
			json[query[i].name] = query[i].value;
			//alert("name=" + query[i].name + ", value=" + query[i].value);
		} 
		$("#fongoLoader").show();
        $.ajax({
            type:     "POST",
            dataType: "json",
            url:      "../gescomp.mdf.main.php?action=save&IDzonal=" + IDzonal,
            data:     query,
            success:  function(datos) {
        		//alert("Datos Grabados");
                //$("#divResult").html(datos);
                if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listarArmarios();
	            }
            }
        });
        $("#fongoLoader").hide();
	}
}

function changeEstado(zonal, mdf, estado) {

	if( confirm("Esta seguro de cambiar el estado para este mdf?") ) {
	    data_content = "action=saveEstado&zonal=" + zonal + "&mdf=" + mdf + "&estado=" + estado;
	
		$("#fongoLoader").show();
	    $.ajax({
	        type:     "POST",
	        dataType: "json",
	        url:      "../gescomp.mdf.main.php",
	        data:	  data_content,
	        success:  function(datos) {
	        	//alert("Datos Grabados");
	            //$("#divResult").html(datos);
	            if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listarArmarios();
	            }
	        }
	    });
	    //setTimeout("hideLoader()", 1000);
	    $("#fongoLoader").hide();
	}
}


</script>
<style type="text/css">
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 7px; 
		overflow: auto;
	} 

	.ui-layout-resizer {
		background: #DDD; 
	} 

	.ui-layout-toggler {
		background: #AAA; 
	}
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
</head>
<body>
    <div class="ui-layout-west">
        <table cellpadding="2" cellspacing="0" border="0" style="width:100%; border-collapse:collapse;">
            <tr>
                <td colspan="2"><span class="l_tit">Mantenimiento Mdfs</span></td>
            </tr>
			<tr>
				<td width="150">Zonal:</td>
				<td>
					<select id="selZonal">
						<option value="">Seleccione</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Con X,Y:</td>
				<td>
					<select id="selDatosXY">
						<option value="">Todos</option>
						<option value="conXY">Terminales con X,Y</option>
						<option value="sinXY">Terminales sin X,Y</option>
					</select>
				</td>
			</tr>
            <tr>
                <td colspan="2">
                    <input type="button" id="btnBuscar" value="Buscar">
                </td>
            </tr>
        </table>
		
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="sidebarHeader">
			<table border="0" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">
			<tr>
				<th width="70">#</th>
				<th width="70">Mdf</th>
				<th width="85">X</th>
				<th width="85">Y</th>
				<th width="40">Est</th>
			</tr>
			</table>
		</div>
		<div style="height:280px; width:100%; overflow:auto;">
			<div id="sidebar"></div>
		</div>
		<?php
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' || 
                $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spv' ||
                $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spu' ) {
        ?>
		<input type="button" name="btnGrabar" id="btnGrabar" onclick="Grabar()" value="Grabar Datos" disabled="disabled">
		<?php
        }
        ?>
		<input type="hidden" id="trm_selected" readonly="readonly">
		
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
        
    </div>
    <div class="ui-layout-center">
        <div id="box_resultado">

           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        </div>
        
    </div>
        
	<div id="parentModal" style="display: none;">
    	<div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>