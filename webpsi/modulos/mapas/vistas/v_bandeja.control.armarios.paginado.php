<?php
if ($totalRegistros>0) {

    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
    foreach ($arrObjArmario as $objArmario) {
        ?>
        <tr>
        <td class="celdaX"><?php echo $item++; ?></td>
        <td class="celdaX">

            <a onclick="editarArmario('<?php echo $objArmario->__get('_mtgespkarm')?>')" href="javascript:void(0)">
                <img width="13" height="13" title="Editar" border="0" src="img/pencil.png">
            </a>&nbsp;
            <a title="Fotos" onclick="fotosArmario('<?php echo $objArmario->__get('_mtgespkarm')?>')" href="javascript:void(0)">
            <?php
            if ( $objArmario->__get('_foto1') != '' ) {
            ?>
                <img width="14" height="14" title="Fotos" border="0" src="img/foto.png">
            <?php
            } else {
            ?>
                <img width="14" height="14" title="Sin fotos" border="0" src="img/foto_disabled.png">
            <?php
            }
            ?>
            </a>
        </td>
        <td class="celdaX"><?php echo $objArmario->__get('_mtgespkarm'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_armario'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_zonal'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_mdf'); ?></td>
        <td class="celdaX" align="left"><?php echo strtoupper(substr($objArmario->__get('_direccion'),0,50)); ?><?php if(strlen($objArmario->__get('_direccion')) > 50) echo '...'?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_qcaparmario'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_qparlib'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_qparres'); ?></td>
        <td class="celdaX"><?php echo $objArmario->__get('_qdistrib'); ?></td>
        <td class="celdaX"><?php if( $objArmario->__get('_codEstado') != '00' ) echo $objArmario->__get('_desEstado'); ?></td>
        <td class="celdaX"><?php echo obtenerFecha($objArmario->__get('_fechaEstado')); ?></td>
    </tr>
    <?php
    }

} else {
    ?>
    <tr>
        <td style="height:30px" colspan="13">Ningun registro encontrado con los criterios de b&uacute;squeda ingresados</td>
    </tr>
<?php
}
?>