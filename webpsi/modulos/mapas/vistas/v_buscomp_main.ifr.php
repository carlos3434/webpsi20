<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../js/jquery/jquery-1.3.2.js"></script>



<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.ui.all.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<!-- <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script> -->

<!-- <script src="../../../js/ui/jquery.ui.core.js"></script>
<script src="../../../js/ui/jquery.ui.widget.js"></script>
<script src="../../../js/ui/jquery.ui.datepicker.js"></script> -->


<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript">
$(function(){
	$("select.clsMultiple").multiselect();
});
</script>
<script type="text/javascript">

var myLayout;
var map = null;
var capa_exists = 0;


markers_arr = [];
capa_selected = '';

$(document).ready(function () {
	
	myLayout = $('body').layout({
		// enable showOverflow on west-pane so popups will overlap north pane
		west__showOverflowOnHover: true
	
	//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
	});

	load_array=function(element){
        var grupo=new Array();
        $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
        return grupo;
    }

	


	var M = {
    	initialize: function() {
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 11,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	        google.maps.event.addListener(map, "click", function(event) {
		        //clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });
        }, 

        clearMarkers: function() {
        	for (var n = 0, marker; marker = markers_arr[n]; n++) {
        		marker.setVisible(false);
        	}
        },

        popupComponentes: function(x, y) {

        	var zoom_selected = map.getZoom();
        	alert('zoom=' + zoom_selected);
            
	        $("#childModal").html('');
	        $("#childModal").css("background-color","#FFFFFF");
	        $.post("principal/buscomp.popup.php", {
	    	        x: x,
	    	        y: y, 
	    	        capa_selected: capa_selected,
	    	        zoom_selected: zoom_selected
	    	    },
	        	function(data){
	            	$("#childModal").html(data);
	        	}
	    	);
	
	        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
        },

        loadPoligono: function(xy_array, z) {

			var datos 			= xy_array['coords'];
			var color_poligono 	= xy_array['color'];
            var poligonoCoords 	= [];


        	if( datos.length > 0) {
        		for (var i in datos) {
            		var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
            		poligonoCoords.push(pto_poligono);
            	}

        		alert('capa_exists=' + capa_exists + ', z=' + z);
				//if( capa_exists ) {
		        //    poligonoDibujar.setMap(null);
		    	//}

		    	if( z == 0 ) {
		    		if( capa_exists ) {
		    		poligonoDibujar.setMap(null);
		    		capa_exists = 0;
		    		}
		    	}

                // Construct the polygon
                // Note that we don't specify an array or arrays, but instead just
                // a simple array of LatLngs in the paths property
                poligonoDibujar = new google.maps.Polygon({
                	paths: poligonoCoords,
                    strokeColor: color_poligono,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: color_poligono,
                    fillOpacity: 0.35
                });

                poligonoDibujar.setMap(map);

              	//seteamos que la capa existe
    			capa_exists = 1;

        	}
        	else {
            	alert('aaa');
        		if( capa_exists ) {
            		poligonoDibujar.setMap(null);
    			}
        		capa_exists = 0;
        	}
                  			
        }
    };



	var L = {
    	loadComponentes: function() {
    	
			var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
			//var zoomMap = 6;

			var zoomMap = parseInt(capa_kml['zoom_sel']);
			//alert('zoomMap = ' + zoomMap);
			
		    if( sites.length > 0) {
				latlng = new google.maps.LatLng(punto_sel['y'], punto_sel['x']);
				//zoomMap = 15;
			}else {
				alert("No se encontro ningun punto.");
			}

		    var myOptions = {
		      	zoom: zoomMap,
		      	center: latlng,
		      	mapTypeId: google.maps.MapTypeId.ROADMAP
		    };

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

			infowindow = new google.maps.InfoWindow({
		    	content: "loading..."
		    });

			google.maps.event.addListener(map, "click", function(event) {
				//clear markers
		        M.clearMarkers();
	        	
	        	marker = new google.maps.Marker({
	                position: event.latLng,
	                map: map
	            });
	        	markers_arr.push(marker);
				
	        	var Coords = event.latLng;
	        	var x = Coords.lng();
	        	var y = Coords.lat();
	            M.popupComponentes(x, y);
	        });

			if( sites.length > 0) {
				L.setMarkers(map, sites);
			}


			if( capa_kml['capa'] != '' ) {

				capa_exists = 1;

				//agrega el archivo kml al mapa 'map'
				var capa_sel = capa_kml['capa'] + '.kmz';
				
				ctaLayer = new google.maps.KmlLayer('http://www.webunificada.com/webunificada/modulos/maps/' + capa_sel, {suppressInfoWindows: true, preserveViewport: true});
				ctaLayer.setMap(map);
			}
			
        },
        
        setMarkers: function(map, markers) {

	  		var avg = {
		    	lat: 0,
		      	lng: 0
		    };
	
	        for (var i = 0; i < markers.length; i++) {
	            var sites = markers[i];
	            var siteLatLng = new google.maps.LatLng(sites['y'], sites['x']);

	            var ico = capa_ico[sites['tip']];
	            
				var icono = '';
				var est_caja = '';
	            if( sites['tip'] == 'trm' ) {
	            	
            		if( sites['qparlib'] == 0 ) {
            			est_caja = 3;
            		}else if( sites['qparlib'] > 0 && sites['qparlib'] < 3 ) {
            			est_caja = 2;
            		}else if( sites['qparlib'] > 2 ) {
            			est_caja = 1;
            		}
		            
		            icono = "../../../img/map/terminal/trm_" + sites['title'] + "--" + est_caja + ".gif";
	            }else if( sites['tip'] == 'arm' ) {
		            icono = "../../../img/map/armario/arm_" + sites['title'] + "--" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'mdf' ) {
		            icono = "../../../img/map/mdf/mdf_" + sites['title'] + "--" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'edi' ) {
		            icono = "../../../img/map/edificio/edificio-" + sites['estado'] + ".gif";
	            }else if( sites['tip'] == 'com' ) {
		            icono = "../../../img/map/competencia/" + ico;
	            }else if( sites['tip'] == 'esb' ) {
		            icono = "../../../img/map/zonal/zonal-1.gif";
	            }else {
	            	//icono = "../../../img/map-icons-collection-2.0/icons/" + ico;
	            	icono = "../../../modulos/maps/vistas/icons/" + ico;
	            }	            
	            
	            var marker = new google.maps.Marker({
	                position: siteLatLng,
	                map: map,
	                title: sites['title'],
	                zIndex: sites['i'],
	                html: sites['detalle'],
	                //icon: "../../../img/map-icons-collection-2.0/icons/" + ico
	                icon: icono
	            });
	
	            var contentString = "Some content";
	
	            google.maps.event.addListener(marker, "click", function () {
	                //alert(this.html);
	                infowindow.setContent(this.html);
	                infowindow.open(map, this);
	            });
	            
	            //falta implementar
	            //google.maps.event.trigger(marker, "click");
	            
	            avg.lat += siteLatLng.lat();
      			avg.lng += siteLatLng.lng();
	        }
	        
	        // Center map.
    		map.setCenter(new google.maps.LatLng(avg.lat / markers.length, avg.lng / markers.length));
	        
	     }

    };


	$("#search").click(function() {
    	if($(".capas").is(':checked')) { 

        } else {  
            alert("Ingrese al menos algun criterio de busqueda");  
            return false;  
        }  
	});

               
	$('#btnFiltrar').click(function(){
		var arreglo_poligono 	= new Array();
		arreglo_poligono = load_array('multiselect_poligono[]');
		
		
		//$("#box_resultado").hide();
		$('#box_loading').show();
		//data_content = 'action=buscar&poligono=' + poligono;
		//data_content = 'arreglo_poligono=' + arreglo_poligono;
		data_content = {
			'action'			: 'buscar',
			'arreglo_poligono' 	: arreglo_poligono 
		};
		$.ajax({
	         type:   "POST",
	         url:    "../gescomp.poligono.php",
	         data:   data_content,
	         dataType: "json",
	         success: function(data) {

				//var poligono_xy = data['xy_poligono'];
				
				

				var poligonos = data['xy_poligono'];
				
				for (var i in poligonos) {
					M.loadPoligono(poligonos[i], i);
				}

			

				//seteamos a null
				//poligonoDibujar = null;
				
				//M.loadPoligono(data['xy_poligono']);

				
				$('#box_loading').hide();
				//$("#box_resultado").empty();
				//$("#box_resultado").append(data);
				//$("#box_resultado").show();
			}
		});
	             
	});


	M.initialize();
               
});

</script>
<style type="text/css">
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 7px; 
		overflow: auto;
	} 

	.ui-layout-resizer {
		background: #DDD; 
	} 

	.ui-layout-toggler {
		background: #AAA; 
	}
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
<script>
	$(function() {
		$("#btnFiltrar").button();
	});
	</script>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
    <div class="ui-layout-west">
        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
            <tr>
                <td><span class="l_tit">Dibujar Poligono</span></td>
            </tr>
            <tr>
                <td align="left" class="label">Seleccione poligono</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="poligono" id="poligono" class="clsMultiple" multiple="multiple">
                    	<option value="1">Poligono 1</option>
                    	<option value="2">Poligono 2</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <br /><button id="btnFiltrar">Aceptar</button>
                </td>
            </tr>
        </table>
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
        
    </div>
    <div class="ui-layout-center">
        
        <div id="box_resultado">
           
           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        </div>
        
    </div>
        
        
	<div id="parentModal" style="display: none;">
		<div id="childModal" style="padding: 10px; background: #fff;"></div>
	</div>
      
      
        
    </body>
</html>