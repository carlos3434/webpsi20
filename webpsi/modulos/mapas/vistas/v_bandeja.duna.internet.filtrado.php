<style type="text/css">
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#btn_buscar").click(function() {
		D.BuscarDunaInternet();
	});
	var D = {
		BuscarDunaInternet: function() {
			var tipo = $("#tipo").val();
			alert("Viene el filtro");
			
			var page = 1;
			var tl = $("#tl_registros").val();	
			$.post("modulos/maps/duna.internet.php?cmd=FiltroBusqueda", { 
				pagina: page, total: tl, tipo: tipo }, function(data){
				$("#tb_resultado").empty();
				$("#tb_resultado").append(data);
				loader("end");
			});			
		}	
	};
	
    $(".trigger").mouseover(function(){
		$(".panel").toggle("fast");
		$(this).toggleClass("active");
		return false;
    });

    $('.panel').mouseleave(function(){
       $(".panel").toggle("fast");
       $('.trigger').toggleClass("active");
    });
    //Fin Menu

    change_state=function(idpoligono_tipo,estado,nom_poligono){
        //true para desabilitar
        if(confirm("Esta seguro de cambiar el estado de la Ruta/poligono : "+nom_poligono)){
            loader('start');
            var emisor=$('#emisor').attr('value');
            var respuesta=0;
            var estado_inicial=estado;
            $.post("administracion_poligono.php?cmd=execUpdateStatePoligonoTipo", {
                idpoligono_tipo: idpoligono_tipo, state: estado, 
                emisor:emisor}, function(data){
              loader('end');
              respuesta=data;

          if(respuesta==1){
              if(estado_inicial==1){
                  var nvo_estado=0;
                  $('#alink_state_'+idpoligono_tipo).attr('href','javascript:change_state(\''+idpoligono_tipo+'\',\''+nvo_estado+'\',\''+nom_poligono+'\')');
                  $('#img_state_'+idpoligono_tipo).attr('src','img/estado_deshabilitado.png');
                  $('#img_state_'+idpoligono_tipo).attr('alt','Cambiar estado a Habilitado');
                  $('#img_state_'+idpoligono_tipo).attr('title','Cambiar estado a Habilitado');
              }
              else{
                   nvo_estado=1;
                  $('#alink_state_'+idpoligono_tipo).attr('href','javascript:change_state(\''+idpoligono_tipo+'\',\''+nvo_estado+'\',\''+nom_poligono+'\')');
                  $('#img_state_'+idpoligono_tipo).attr('src','img/estado_habilitado.png');
                  $('#img_state_'+idpoligono_tipo).attr('alt','Cambiar estado a Deshabilitado');
                  $('#img_state_'+idpoligono_tipo).attr('title','Cambiar estado a Deshabilitado');
              }
          }
    });
        }
   }

   editPoligonoTipo=function(idpoligono_tipo){
    
    var emisor=$('#emisor').attr('value');
    loader('start');
    $("#childModal").html('Cargando...');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion_poligono.php?cmd=editPoligonoTipo", {
        idpoligono_tipo: idpoligono_tipo,
        emisor:emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'500px', hide: 'slide', title: 'Editar Tipo Ruta/poligono', position:'top'});

   }
   

   view_ruta_poligono_mapa=function(idpoligono_tipo, tipo){
        var emisor=$('#emisor').attr('value');
        loader('start');
    $("#childModal").html('Cargando...');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("administracion_poligono.php?cmd=viewRutaPoligonoMapa", {
        idpoligono_tipo	: idpoligono_tipo,
		tipo			: tipo,
        emisor			: emisor
    },
    function(data){
        loader('end');
        $("#childModal").html(data);
    });

    $("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Visualizar Rutas/poligonos', position:'top'});

   }
   
   
   view_poligonos=function(idpoligono_tipo, tipo){
        var emisor=$('#emisor').attr('value');
        loader('start');
		$("#childModal").html('Cargando..');
		$("#childModal").css("background-color","#FFFFFF");
		$.post("administracion_poligono.php?cmd=viewPoligonos", {
			idpoligono_tipo	: idpoligono_tipo,
			tipo			: tipo,
			emisor			: emisor
		},
		function(data){
			loader('end');
			$("#childModal").html(data);
		});

		$("#childModal").dialog({modal:true, width:'520px', hide: 'slide', title: 'Listado de Rutas/poligonos', position:'top'});

   }
   
   $('#btn_buscar').click(function(){
       
	   
	   
	   
   });

   $('#img_down_pdf').click(function(){
       alert("Muy pronto..");
   });

   $('#img_down_xls').click(function(){
       alert("Muy pronto..");
   });

});
		
</script>

<?php if($totalRegistros>0):?>
<table id="tb_sup_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
<tr>
<td style="width:55%; height: 30px; text-align: left;">
	Registros encontrados : <b><span id="cont_num_resultados_head"><?php echo $totalRegistros ?></span></b>
</td>
<td style="width:45%;">
	<div  style=" width:360px; position: relative;">
		<div id="paginacion"></div>
	</div>
</td>
</tr>
</table>
<?php endif;?>
<input type="hidden" id="pag_actual" name="pag_actual" value="1" />
<input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros;?>"/>
<div style="width: 100%;">
	<table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;">
		<thead>
			<tr style="line-height: 13px;">
			<th class="headTbl" width="2%">#</th>
			<th class="headTbl" width="40%">Nombre</th>
			<th class="headTbl" width="20%">Tipo antena</th>
			<th class="headTbl" width="20%">Fecha Registro</th>
			<th class="headTbl" width="18%" colspan="2" style="padding-left: 5px;">Opciones</th>
			</tr>
		</thead>
		<tbody>
			<?php
                $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
                foreach ($componentes as $componente){
                    if ($componente['ind_componente']==1) {
                        $state = 1;
                        $message_state = "Habilitado";
                    } else { 
                        $state = 0; 
                        $message_state = "Deshabilitado";
                    }

                    if ($item%2 == 0) {
                        echo '<tr class="tr">'; 
                    } else {
                        echo '<tr>';
                    }
            ?>
			<td class="celdaX"><?php echo $item++; ?></td>
			<td class="celdaX" align="left"><?php echo $componente['campo1']; ?>
			</td>
			<td class="celdaX"><?php echo $componente['campo2']; ?></td>
			<td class="celdaX"><?php echo obtenerFecha($componente['fecha_insert']); ?>
			</td>
			<td class="celdaX">
			    <a href="javascript:editPoligonoTipo('<?php echo $componente['idcomponente'];?>')">
			    <img src="img/pencil_16.png" alt="Editar datos del Area" title="Editar datos del Tipo Ruta/poligono" /></a>
			    <a id="alink_state_<?php echo $componente['idcomponente'];?>" href="javascript:change_state('<?php echo $componente['idcomponente']; ?>','<?php echo $state; ?>','<?php echo $componente['campo1'];?>')">
                <?php if($state==true):?> 
                <img id="img_state_<?php echo $componente['idcomponente'];?>" src="img/estado_habilitado.png" alt="Cambiar estado a Deshabilitado" title="Cambiar estado a Deshabilitado" />
                <?php else: ?>
                <img id="img_state_<?php echo $componente['idcomponente'];?>" src="img/estado_deshabilitado.png" alt="Cambiar estado a Habilitado" title="Cambiar estado a Habilitado" />
                <?php endif; ?>
			    </a>
			</td>
			</tr>
			<?php
                }
            ?>
		</tbody>
	</table>
</div>
<br />
<?php if ($totalRegistros>0): ?>
<table id="tb_pie_tl_registros" style="margin-left:0px;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
<tr>
   <td align="left">Registros encontrados : <b><span id="cont_num_resultados_foot">
   <?php echo $totalRegistros; ?></span></b><br/><br/>
   </td>
</tr>
</table>
<?php endif;?>