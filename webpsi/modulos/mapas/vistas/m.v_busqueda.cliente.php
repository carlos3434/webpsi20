<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript">
function loader(estado) {
    if(estado=='start') {
        $(".bgLoader").css("display","block");
    }
    if(estado=='end') {
        $(".bgLoader").css("display","none");
    }
}
    
$(document).ready(function() {
    detalleCliente = function(ddnTelefono, x, y) {

        if ( x == '' || y == '' ) {
            alert('El telefono: ' + ddnTelefono + ' no dispone de Georeferenciacion');
            return false;
        }
        
        window.top.location = 'm.busqueda.cliente.php?cmd=verMapa&ddnTelefono=' + ddnTelefono + '&x=' + x + '&y=' + y;

    }

    buscarCliente = function() {
        var valor = $('#valor').attr('value');
        
        if( valor.length < 3) {
            alert("Debe ingresar 3 caracteres como minimo");
            return false;
        }
        
        var data_content = { 
            valor : valor
        };

        loader("start");
        $("#btnBuscar").attr('disabled', 'disabled');
        
        $.ajax({
            type:   "POST",
            url:    "m.busqueda.cliente.php?cmd=buscarCliente",
            data:   data_content,
            dataType: "json",
            success: function(data) {

                if ( data['flag'] == '1' ) {

                	var datos = data['clientes'];
                    
                    var str_cliente = '';
                    if( datos.length > 0) {
                        for(var i in datos) {
                            var style_xy = '';
                            var coord_x = '';
                            var coord_y = '';
                            if( (datos[i]['x'] != '' && datos[i]['x'] != null) && (datos[i]['y'] != '' && datos[i]['y'] != null) ) {
                                style_xy = 'style="background-color:#ff0000;"';
                                coord_x = datos[i]['x'];
                                coord_y = datos[i]['y'];
                            }

                            strCabArm = 'Cbl';
                            if ( datos[i]['tipo_red'] == 'F' ) {
                            	strCabArm = 'Arm';
                            }

                            str_cliente += '<tr class="lnk" onclick="detalleCliente(\'' + datos[i]['ddn_telefono'] + '\', \'' + coord_x + '\', \'' + coord_y + '\')">';
                            str_cliente += '<td class="list" ' + style_xy + '>&nbsp;&nbsp;</td>';
                            str_cliente += '<td class="list" style="font-size:10px; vertical-align:top; width:100px;">Zon:' + datos[i]['Zonal'] + '<br />Mdf:' + datos[i]['MDF'] + '<br />' + strCabArm + ':' + datos[i]['cableArmario'] + '<br />Trm:' + datos[i]['Caja'] + '</td>';
                            str_cliente += '<td class="list" style="width:85%!important;color:#1155CC;">' + datos[i]['Cliente'] + '<br />';
                            str_cliente += '<span style="font-size:12px;color:#999999;">' + datos[i]['Direccion'] + '<br />' + datos[i]['DescCiudad'] + '<br />' + datos[i]['DDN'] + ' ' + datos[i]['Telefono'] + '</span>';
                            str_cliente += '</td>';
                            //str_cliente += '<td class="list" style="font-size:12px;color:#999999;padding:0 15px;">' + datos[i]['DescEstadoPar'] + '</td>';
                            str_cliente += '<td class="list"><img src="../../img/btn_submotivo_24.png"></td>';
                            str_cliente += '</tr>';
                        }

                    }else {
                        str_cliente = '<tr class="NOlnk"><td><span style="font-size:12px;color:#999999;font-weight:bold;">Ningun registro encontrado.</span></td></tr>';
                    }

                    
                    $("#total_resultados").html(datos.length);

                    $("#tblLeyenda").show();
                    loader("end");
                    $("#num_resultados").show();
                    $("#btnBuscar").attr('disabled', '');
                    $("#tblPrincipal").empty();
                    
                    $("#tblPrincipal").append(str_cliente);
                	
                } else {
                	alert(data['msg']);

                	$("#tblLeyenda").hide();
                    loader("end");
                    $("#num_resultados").hide();
                    $("#btnBuscar").attr('disabled', '');
                    $("#tblPrincipal").empty();
                    
                }
            }
        });

    }
    
});
</script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="divResultados">
    
    <?php include $dirModulo . 'm.header.php';?>

    <table id="tblPrincipal2" class="principal" width="100%">
    <tr>
        <td class="titulo" colspan="2">Buscar cliente</td>
    </tr>
    </table>
    

    <table id="tblPrincipal2" class="principal" width="100%">
    <tr>
        <td>
            Tel&eacute;fono: <input type="text" name="valor" id="valor" size="10" style="font-size:16px; height:25px; width:120px;" />
            <input type="button" class="searchsubmit" name="btnBuscar" id="btnBuscar" value="Buscar" onclick="buscarCliente()" /> &nbsp; 
        </td>
    </tr>
    <tr>
        <td style="font-size:12px;font-weight:bold;"><div id="num_resultados" style="display:none;">Encontrados (<span id="total_resultados">0</span>)</div></td>
    </tr>
    </table>

    <div style="height:10px;"></div>

    <table border="0" id="tblPrincipal" class="principal" style="border-top:1px solid #DBDBDB;" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    </table>
    
    <br />
    <table border="0" id="tblLeyenda" class="principal" style="display:none;" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <fieldset class="m_leyenda">
                <legend>Leyenda</legend>
                <div style="float:left;width:10px;height:20px;background-color:#ff0000;"></div> 
                <div style="float:left;padding:5px;font-size:12px;">Cliente tiene x,y</div>
            </fieldset>
        </td>
    </tr>
    </table>

</div>

    
<!-- Divs efecto Loader -->
<div id="fongoLoader" class="bgLoader">
    <div class="imgLoader" >
        <span>Cargando...</span>
        <!--<span style="font-size: 9px; color: blue;" id="cancel_loader" ><a href="javascript:void(0)" onclick="loader('end')">Cancelar[X]</a></span>-->
    </div>
</div>
<!-- fin  -->

</body>
</html>