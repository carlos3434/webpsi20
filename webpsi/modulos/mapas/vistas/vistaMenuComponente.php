<table id="tblMenu" border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
	<td width="100">
		<div id="div_menu" style="position:relative;">
			<div class="panel">
				
				<div style="margin-left: 2px;">
					<?php 
					if( !empty($_SESSION['USUARIO_CAPAS']) ) {
				    	foreach( $_SESSION['USUARIO_CAPAS'] as $obj_capa ) {
				    		if( $obj_capa->__get('tiene_tabla') == 'S' ) {
				    	
						    	if( $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'adm' || $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'spu' ) {
							    ?>
									<div>
										<table class="tb_menu" cellspacing="0" cellpadding="2" border="0">
										<tr>
											<td width="200">
												<a style="line-height: 24px; margin-left: 10px; font-size:11px; font-weight:bold; text-decoration:none; color:#00394E;" href="<?php echo $obj_capa->__get('url')?>">Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?></a>
											</td>
											<td width="10">
												<a style="float: right;" href="<?php echo $obj_capa->__get('url')?>"><img border="0" title="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" alt="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" src="../../../img/search_16.png"></a>
											</td>
											<td width="10">&nbsp;</td>
										</tr>
										</table>
									</div>
								<?php 
								}elseif( $_SESSION['USUARIO_PERFIL']->__get('codperfil') == 'ope' ) {
									if( $obj_capa->__get('es_fftt') == 'N' ) {
									    ?>
									    <div>
									        <table width="100%" border="0">
									            <tr>
									                <td width="80%"><a href="<?php echo $obj_capa->__get('url')?>" style=" line-height: 20px; margin-left: 10px; font-weight:bold; font-size:11px;">Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?></a></td>
									                <td width="10%"><a href="<?php echo $obj_capa->__get('url')?>" style="float: right;"><img src="../../../img/search_16.png" alt="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" title="Mantenimiento de <?php echo ucwords(strtolower($obj_capa->__get('nombre')))?>" border="0"/></a></td>
									                <td width="10%">&nbsp;</td>
									            </tr>
									        </table>
									    </div>
									    <?php
									}
								}
				    		}
				    	}
					}
					?>
				</div>	
				
            </div>
                        
            <div id="box_menu_adm">
            	<a class="trigger" href="#">Men&uacute;</a>
            </div>
        </div>
 	</td>
</tr>
</table>