<script type="text/javascript">
$(document).ready(function() {
    
    var tabOpts = { disabled:[2] };
    
    $("#tabsFotosArmario").tabs(tabOpts);
    
    
    $("#fecha_estado").datepicker({ 
        dateFormat: 'dd/mm/yy',
        yearRange:'-20:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    
    $('#frm_adjuntar_fotos').ajaxForm({ 
        dataType    : 'json', 
        beforeSubmit: validate,
        success     : processJson     
    }); 
    
    $("#tabListaFotos, #tabFotosAdjuntar").click(function() {
        var tabOpts = { disabled:[2] };
        $("#tabsFotosArmario").tabs(tabOpts);
    });

    function validate(formData, jqForm, options){
        var query = $("input.required, select.required");
        
        var is_validate = 1;
        $.each( query, function() {
            if( $(this).val() == '' ) {
                alert("Ingrese el campo " + $(this).attr('msg'));
                $(this).focus();
                is_validate = 0;
                return false;
            }
        });
        
        if(!is_validate) {
            return false;
        }
 
        if( $("#foto1").val() == '') {
            alert("Ingrese por lo menos la Foto 1");
            return false;
        }
        
        $("#msg").show();
        $("input[name=btn_guardar]").attr('disabled','disabled');
        
        var error_foto = '';
        var permitida = false;
        var image = $('input[name=imagen_armario[]]').fieldValue(); 

        $.each(image, function(i, item) {
            if(image[i]!=''){
                var extension = (image[i].substring(image[i].lastIndexOf("."))).toLowerCase();
                if (! (extension && /^(.jpg|.jpeg)$/.test(extension))){ //.jpg|.png|.jpeg|.gif
                    // extensiones permitidas
                    error_foto = 'Error: Solo se permiten imagenes de tipo JPG.';
                    // cancela upload
                    $("input[name=btn_guardar]").attr('disabled','');
                    permitida = true;
                }
            }
        });
        if(permitida){ alert(error_foto); return false; }
    }
     
    function processJson(data) { 
        //limpiando el formulario
        $(".frm_fotos").attr('value','');
        
        $("#foto1").replaceWith('<input id="foto1" class="frm_fotos" type="file" size="25" name="imagen_armario[]">');
        $("#foto2").replaceWith('<input id="foto2" class="frm_fotos" type="file" size="25" name="imagen_armario[]">');
        $("#foto3").replaceWith('<input id="foto3" class="frm_fotos" type="file" size="25" name="imagen_armario[]">');
        
        $("#estado").find('option:first').attr('selected','selected');
        
        
        $("#msg").hide();
        $("input[name=btn_guardar]").attr('disabled','');
        
        if(data['tipo'] == 'imagen') { 
            alert(data.mensaje); 
            return false; 
        }
        if(data['flag'] == '1'){
            alert(data['mensaje']);
            
            $("#tabsFotosArmario").tabs('select',0);
            
            //actualiza listado de historico de fotos
            listadoFotosArmario($("#mtgespkarm").val());
            
            //refresca la bandeja
            buscarArmarios();

        }else{
            alert(data['mensaje']);
        } 
    }
    
    
    
});

//carga el primer tab con las fotos del armario
listadoFotosArmario('<?php echo $arrObjArmario[0]->__get('_mtgespkarm')?>');

</script>


<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">DATOS DEL ARMARIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="15%" class="da_subtitulo">ARMARIO</td>
        <td width="15%" class="da_import"><?php echo $arrObjArmario[0]->__get('_armario')?></td>
        <td width="20%" class="da_subtitulo">ZONAL</td>
        <td width="20%" class="da_import"><?php echo $arrObjArmario[0]->__get('_zonal')?></td>
        <td width="15%" class="da_subtitulo">MDF</td>
        <td width="15%" ><?php echo $arrObjArmario[0]->__get('_mdf')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N</td>
        <td colspan="5"><?php echo strtoupper($arrObjArmario[0]->__get('_direccion'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CAPACIDAD</td>
        <td ><?php echo $arrObjArmario[0]->__get('_qcaparmario')?></td>
        <td class="da_subtitulo">PARES LIBRES</td>
        <td><?php echo $arrObjArmario[0]->__get('_qparlib')?></td>
        <td class="da_subtitulo">PARES RESERVADOS</td>
        <td ><?php echo $arrObjArmario[0]->__get('_qparres')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PARES DISTRIBUIDOS</td>
        <td><?php echo $arrObjArmario[0]->__get('_qdistrib')?></td>
        <td class="da_subtitulo">ESTADO ACTUAL</td>
        <td><b><?php echo strtoupper($arrObjArmario[0]->__get('_desEstado'))?></b></td>
        <td class="da_subtitulo">FECHA ESTADO ACTUAL</td>
        <td><b><?php echo obtenerFecha($arrObjArmario[0]->__get('_fechaEstado'))?></b></td>
    </tr>
    </table>
</div>

<div id="content_armario_fotos">

    <div id="tabsFotosArmario" style="width:99%; margin-bottom: 10px;">
        <ul>
            <li><a id="tabListaFotos" href="#divListaFotos" style="color:#1D87BF; font-weight: 600;">Listado de fotos del Armario</a></li>
            <?php
            if( $map == '' ) {
            ?>
            <li><a id="tabFotosAdjuntar" href="#divFotosAdjuntar" style="color:#1D87BF; font-weight: 600;">Adjuntar Fotos del Armario</a></li>
            <?php
            }
            ?>
            <li><a id="tabFotosArmario" href="#divFotosArmario" style="color:#1D87BF; font-weight: 600;">Detalle Fotos del Armario</a></li>
        </ul>

        <div id="divListaFotos">
            
            <div style="margin: 0 0 5px 0; width:100%; float:right;">
                <div style="float:left; margin:7px 0 0 0;">
                    <b>HISTORICO DE FOTOS DEL ARMARIO:</b>
                </div>
                <div id="loading_clientes" style="float:left; margin:7px 0 0 20px;display:none;">
                   <img alt="" src="img/loading.gif"> cargando...
                </div>
            </div>
            <div style="width:100%; overflow-y:scroll; height:150px;">
                <table id="tb_lista_fotos" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%; font-size:10px;">
                <thead>
                    <tr style="line-height:13px;">
                        <th class="headTbl" width="2%">#</th>
                        <th class="headTbl" width="3%">Opc</th>
                        <th class="headTbl" width="10%">Fecha foto</th>
                        <th class="headTbl" width="10%">Estado</th>
                        <th class="headTbl" width="15%">Fecha ingreso</th>
                        <th class="headTbl" width="60%">Observaciones</th>
                    </tr>
                </thead>
                <tbody>      
                </tbody>
                </table>
            </div>

        </div>

        <?php
        if( $map == '' ) {
        ?>
        <div id="divFotosAdjuntar">
            
            <form enctype="multipart/form-data" action="modulos/maps/bandeja.control.armarios.php?cmd=registrarFotos" method="post" name="frm_adjuntar_fotos" id="frm_adjuntar_fotos">
                <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td valign="top" style="padding-top: 10px;">

                        <input type="hidden" id="mtgespkarm" name="mtgespkarm" value="<?php echo $mtgespkarm?>"/>

                        <div id="cont_actu_detalle">

                            <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
                            <tbody>
                            <tr>
                                <td class="da_titulo_gest" colspan="6">FOTOS DEL ARMARIO</td>
                            </tr>
                            <tr style="text-align: left;">
                                <td class="da_subtitulo" width="20%">FECHA ESTADO FOTO</td>
                                <td width="80%">
                                    <input id="fecha_estado" name="fecha_estado" type="text" value="" readonly="readonly" size="8" class="required frm_fotos" msg="Fecha estado foto" /> <em>(*)</em>
                                </td>
                            </tr>
                            <tr style="text-align: left;">
                                <td class="da_subtitulo">ESTADO</td>
                                <td>
                                    <select name="estado" id="estado" class="required frm_fotos" msg="Estado" >
                                        <option value="">Seleccione</option>
                                        <?php
                                        foreach($arrObjArmarioEstados as $objArmarioEstado) {
                                        ?>
                                        <option value="<?php echo $objArmarioEstado->__get('_codArmarioEstado')?>"><?php echo $objArmarioEstado->__get('_desEstado')?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <em>(*)</em>
                                </td>
                            </tr>
                            <tr style="text-align: left;">
                                <td class="da_subtitulo">FOTO 1</td>
                                <td>
                                    <input id="foto1" name="imagen_armario[]" type="file" size="25" class="frm_fotos" /> &nbsp;<em>(*)</em>
                                    <a onclick="limpiarFoto('foto1')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                                    <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
                                </td>
                            </tr>
                            <tr style="text-align: left;">
                                <td class="da_subtitulo">FOTO 2</td>
                                <td>
                                    <input id="foto2" name="imagen_armario[]" type="file" size="25" class="frm_fotos" /> &nbsp; &nbsp; 
                                    <a onclick="limpiarFoto('foto2')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                                    <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
                                </td>
                            </tr>
                            <tr style="text-align: left;">
                                <td class="da_subtitulo">FOTO 3</td>
                                <td>
                                    <input id="foto3" name="imagen_armario[]" type="file" size="25" class="frm_fotos" /> &nbsp; &nbsp; 
                                    <a onclick="limpiarFoto('foto3')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                                    <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="da_subtitulo" style="text-align: left;vertical-align:top;">OBSERVACIONES:</td>
                                <td style="text-align:left;">
                                        <textarea name="observaciones" id="observaciones" rows="4" style="width:95%;" class="frm_fotos">SIN OBSERVACIONES</textarea>
                                </td>
                            </tr>
                            </table>

                            <div style="height:5px;"></div>

                            <table width="100%" cellspacing="1" cellpadding="0" border="0">
                            <tbody><tr>
                                <td width="90%" style="text-align: left; vertical-align: top">
                                    <div style="padding-top: 5px; font-size: 11px">
                                        <input type="submit" value="Registrar" name="guardar" title="Registrar" id="btn_guardar" class="boton">&nbsp;
                                        <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" onclick="javascript: cerrarVentana();" class="boton">
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                            </table>
                        </div>

                        <div style="margin: 10px 0 0 0; display: none;" id="msg">
                                <img alt="" src="img/loading.gif"> <b>Procesando cambios.</b>
                        </div>

                    </td>
                </tr>
                </table>
                <br />
            </form>
            
            
        </div>
        <?php
        }
        ?>
        
        <div id="divFotosArmario">
        </div>
    </div>

</div>

