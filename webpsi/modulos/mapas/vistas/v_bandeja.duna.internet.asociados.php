<thead>
    <tr style="line-height:13px;">
        <th class="headTbl" width="2%">#</th>
        <th class="headTbl" width="3%">Opc</th>
        <th class="headTbl" width="30%">Cliente</th>
        <th class="headTbl" width="40%">Direccion</th>
        <th class="headTbl" width="10%">Telefono</th>
        <th class="headTbl" width="15%">Producto</th>
    </tr>
</thead>
<?php if ( !empty($arrClientesAsociados) ) { ?>
    <tbody>
    <?php
        $itemA = 1;
        foreach ($arrClientesAsociados as $objDunaAsociado) {
    ?>
        <tr>
            <td class="celdaX"><?php echo $itemA++; ?></td>
            <td class="celdaX">
                <a href="javascript:void(0)" onclick="eliminarClienteAsociado('<?php echo $objDunaAsociado->__get('_idDunaAsociado')?>','<?php echo $idComponente?>')" title="Eliminar cliente asociado">
                    <img width="11" height="11" title="Eliminar cliente asociado" src="img/delete_offline.png" style="margin:3px;" />
                </a>
            </td>
            <td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_titular'); ?></td>
            <td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_direccionTelefono'); ?></td>
            <td class="celdaX"><?php echo $objDunaAsociado->__get('_telefono'); ?></td>
            <td class="celdaX"><?php echo $objDunaAsociado->__get('_speedyContratado'); ?></td>
        </tr>
    <?php
        }
    ?>
    </tbody>
<?php
    }
?>       
