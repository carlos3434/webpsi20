<?php
/*
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica
 * Autor: Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 */ 
?>
<style>
.fld_tec {
    background-color: #F9F8F7;
    border: 1px solid #1D87BF;
    border-radius: 3px 3px 3px 3px;
    font-family: Arial;
    margin-bottom: 5px;
    padding: 3px 3px 10px 10px;
    text-align: left;
}
.lgd_tec {
    color: #003366;
    font-size: 11px;
    font-weight: bold;
    padding-left: 5px;
    padding-right: 5px;
    text-align: left;
}
</style>
<fieldset class="fld_tec">
<legend class="lgd_tec label_form">Ubicaci&oacute;n Geogr&aacute;fica</legend>
    <table class="left" style="width: 100%">
    <tbody>
        <tr class="height">
            <td style="width: 6%">Jefatura:
                <input type="hidden" name="negocio" value="<?php echo $tipo_negocio; ?>"/>
                <input type="hidden" name="codtiporegistropex" value="<?php echo $tipo_registro;?>"/>
                <input type="hidden" name="tipored" value="<?php echo $tipo_red;?>"/>
                <input type="hidden" name="codplantaptr" value="<?php echo $tipo_planta;?>"/>
            </td>
            <td style="width: 13%">
                <select id="" name="codjefatura" style="width: 94%" onchange="changeJefatura()"> <!--width: 120px;-->
                    <option value="" selected="selected" >Seleccione</option>
                <?php
                    foreach ($arreglo_jefatura as $row):
                        $nomzonal = ($row->__get('zonal')=="LIM")?"LIMA":$row->__get('jefatura_desc');
                        echo '<option value="'.$row->__get('jefatura').'" title="'.$row->__get('zonal').'" class="'.$row->__get('idempresa').'" lang="'.$nomzonal.'">'.$row->__get('jefatura_desc').'</option>';
                    endforeach;
                ?>     
                </select>
            </td>
            <td style="width: 6%">Zonal:</td>
            <td style="width: 10%">
                <input type="hidden" name="codzonal" />
                <input type="hidden" name="codeecc" />
                <input type="text" name="nomzonal" style="width: 90%" readonly="readonly"/>
            </td>
            <td style="width: 8%">Departamento:</td>
            <td style="width: 14%">
                <select id="" name="coddepartamento" style="width: 90%" class="changeJefatura" onchange="changeDepartamento()"> <!--width: 120px;-->
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>
            <td style="width: 7%">Provincia:</td>
            <td style="width: 15%">
                <select id="" name="codprovincia" style="width: 90%" class="changeJefatura" onchange="changeProvincia()"> <!--width: 120px;-->
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>
            <td style="width: 6%">Distrito:</td>
            <td style="width: 15%">
                <select id="" name="coddistrito" style="width: 99%" class="changeJefatura"> 
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>            
        </tr>
    </tbody>    
</table>  
</fieldset>

<?php if($tipo_planta == 'B'):?>
<fieldset class="fld_tec">
<legend class="lgd_tec label_form">Descripci&oacute;n T&eacute;cnica</legend>
    <table class="left" style="width: 100%">    
    <tbody>
        <tr class="height">
            <td style="width: 6%">MDF:</td>
            <td style="width: 13%">
                <select id="" name="mdf" style="" class="changeJefatura" onchange="changeMdf(false)">
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>
            <td style="width: 6%"><?php echo ($tipo_red=='D')?'C. Directo':'Primario'?>:</td>
            <td style="width: 10%">
            <?php if($tipo_red=='D'){ ?>    
                <select id="" name="primario" class="changeJefatura" onchange="changePrimario(false)" dir="mdf" <?php echo $reestrinciones['mdf'].' '.$reestrinciones['armario'].' '.$reestrinciones['terminal'].' '.$reestrinciones['poste']; ?>>
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            <?php } else { ?>    
                <input type="text" name="primario" readonly="readonly" style="width: 90%" <?php echo $reestrinciones['mdf'].' '.$reestrinciones['armario'].' '.$reestrinciones['acometida'].' '.$reestrinciones['terminal'].' '.$reestrinciones['poste']; ?> />
            <?php } ?>    
            </td>
            <td style="width: 8%">Armario:</td>
            <td style="width: 14%">
                <select id="" name="armario" class="changeJefatura" onchange="changeArmario(false)" dir="mdf" <?php echo $reestrinciones['directa'].' '.$reestrinciones['mdf'].' '.$reestrinciones['alimentador'].' '.$reestrinciones['poste']; ?>>
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>        
            <td style="width: 7%">Secundario:</td>
            <td style="width: 15%">
                <select id="" name="secundario" class="changeJefatura" dir="mdf" lang="armario" <?php echo $reestrinciones['directa'].' '.$reestrinciones['mdf'].' '.$reestrinciones['armario'].' '.$reestrinciones['acometida'].' '.$reestrinciones['alimentador'].' '.$reestrinciones['poste']; ?>>
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>
            <td style="width: 6%">Terminal:</td>
            <td style="width: 15%">
                <select id="" name="terminal" class="changeJefatura" onchange="changeTerminal()" dir="mdf" lang="armario" <?php echo $reestrinciones['mdf'].' '.$reestrinciones['armario'].' '.$reestrinciones['alimentador'].' '.$reestrinciones['terminal'].' '.$reestrinciones['poste']; ?>>
                    <option value="" selected="selected" >Seleccione</option>
                </select>
            </td>
        </tr>
        <tr class="height">
            <td style="width: 6%">Direcci&oacute;n:</td>
            <td colspan="9">
                <input type="text" name="direccioncliente" style="width: 50%; text-transform: uppercase;" readonly="readonly" value="" maxlength="100" />
            </td>
        </tr>
    </tbody>    
</table>
</fieldset>

<?php endif; ?>
<?php if($tipo_planta == 'C'):?>
<fieldset class="fld_tec">
<legend class="lgd_tec label_form">Descripci&oacute;n T&eacute;cnica</legend>
    <table class="left" style="width: 100%">    
    <tbody>
        <tr class="height">
            <td style="width: 6%">Nodo:</td>
            <td style="width: 13%">
                <select id="" name="nodo" style="" class="changeJefatura" onchange="changeNodo(false)">
                    <option value="" selected="selected">Seleccione</option>
                </select>        
            </td>            
            <td style="width: 6%">Troba:</td>
            <td style="width: 10%">
                <select id="" name="troba" style="" class="changeJefatura nodo" onchange="changeTroba(false)" <?php echo $reestrinciones['nodo'].' '.$reestrinciones['poste'];?>>
                    <option value="" selected="selected">Seleccione</option>
                </select>
            </td>
            <td style="width: 8%">Amplificador:</td>
            <td style="width: 14%">
                <select id="" name="amplificador" style=""  class="changeJefatura nodo troba" onchange="changeAmplificador(false)" <?php echo $reestrinciones['nodo'].' '.$reestrinciones['acopador/divisor'].' '.$reestrinciones['troba'].' '.$reestrinciones['poste'].' '.$reestrinciones['cTroba'];?>>
                    <option value="" selected="selected">Seleccione</option>
                </select>
            </td>
            <td style="width: 7%">Tap:</td>
            <td style="width: 15%">
                <select id="" name="tap" style="" class="changeJefatura nodo troba amplificador" onchange="changeTap(false)" <?php echo $reestrinciones['nodo'].' '.$reestrinciones['acopador/divisor'].' '.$reestrinciones['troba'].' '.$reestrinciones['amplificador'].' '.$reestrinciones['poste'].' '.$reestrinciones['cTroba'].' '.$reestrinciones['cAmplificador'];?>>
                    <option value="" selected="selected">Seleccione</option>
                </select>
            </td>
            <td style="width: 6%">Borne:</td>
            <td style="width: 15%">
                <input type="text" name="borne" readonly="readonly" style="" onkeypress="return validarNumber(event)"/>
            </td>
        </tr> 
        <tr class="height">
            <td style="width: 6%">Direcci&oacute;n:</td>
            <td colspan="9">
                <input type="text" name="direccioncliente" style="width: 50%; text-transform: uppercase;" readonly="readonly" value="" maxlength="100" />
            </td>
        </tr>
    </tbody>    
</table>
</fieldset>

<?php endif; ?>
<fieldset class="fld_tec">
<legend class="lgd_tec label_form">Detalle del Trabajo</legend>
    <table class="left" style="width: 100%">    
        <tbody>
            <tr class="height">
                <td style="width: 23%;">Elemento de Red:</td>
                <td style="width: 77%; font-weight: normal"><?php echo $nom_elemento;?>
                    <input name="codelementoptr" type="hidden" value="<?php echo $cod_elemento; ?>"/>
                </td>
            </tr>
            <tr class="height">
                <td>Casuistica:</td>
                <td>
                    <!--<select name="codcasuisticapex" style="min-width: 61%;" onchange="changeCasuistica()">-->
                    <select name="codcasuisticapex">
                        <option value="" selected="selected" >Seleccione</option>
                    <?php
                    foreach ($arreglo_casuistica as $row):
                        echo '<option value="'.$row->__get('idcasuistica').'" class="'.$hide.'">'.utf8_encode($row->__get('nombre')).'</option>';
                    endforeach; 
                    ?>    
                    </select>
                </td>
            </tr>

            <tr class="height">
                <td>Fecha Inicio Trabajo: </td>
                <td>
                    <input type="text" name="fecha_inicio_trabajo" id="fecha_inicio_trabajo" readonly="readonly" size="15"/>
                </td>
            </tr>
            <tr class="height">
                <td>Fecha Fin Trabajo: </td>
                <td>
                    <input type="text" name="fecha_fin_trabajo" id="fecha_fin_trabajo" readonly="readonly" size="15"/>
                </td>
            </tr>

            <tr class="height">
                <td valign="top">Observaci&oacute;n:</td>
                <td>
                    <textarea id="observaciones" rows="4" cols="60" name="observaciones" style="text-transform: uppercase; font-size: 11px;"></textarea>
                </td>
            </tr>
        </tbody>    
    </table>
</fieldset>
<!--
<div style="width: 50%; float:left;">
    <table class="left" style="width: 100%" id="galeriaImagen">
        <caption>Imagenes del Incidente:</caption>
        <tbody>
            <tr id="foto1">
                <td style="width: 28%;" class="cabecera_datos">Archivo de Imagen:<span class="requerido">(Solo archivos tipo .jpg)</span><span class="requerido">(Tama&ntilde;o m&aacute;ximo: 2MB) </span></td>
                <td style="width: 72%; font-weight: normal" class="cabecera_datos">
                    <input name="imagen_pex[]" type="file" size="25" />
                    <img src="img/btn_clear_32.png">
                    <a onclick="limpiarFoto()" href="javascript:void(0)">Limpiar</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>   
-->
<table class="left" style="width: 100%; clear: both;">
    <tbody>
        <tr class="height">
            <td class="left" width="30%">
                <input name="btnGuardar" type="button" style="padding:3px 8px;font-weight:bold;" onclick="registrarTrabajo()" value="Registrar Trabajo"/>
                <input name="btnCancelar" type="button" style="padding:3px 8px;font-weight:bold;" onclick="resetForm()" value="Limpiar Registro"/>
            </td>
            <td class="left" width="70%">
                <div id="box_loading" style="float:left; margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none;">procesando. <img src="img/loading.gif" alt="" title="" /></div>
            </td>
        </tr>
    </tbody>    
</table>
<style type="text/css">
    .requerido { color: red; font-size: 11px; font-weight: bold; display: block; }
</style>
<script type="text/javascript">
$(document).ready(function(){
    //$("#fecha_inicio_trabajo, #fecha_fin_trabajo").datepicker({
    var dates = $("#fecha_inicio_trabajo, #fecha_fin_trabajo").datetimepicker({
        yearRange:'-1:+1',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy',
        onSelect: function(selectedDate) {
                var option = this.id == "fecha_inicio_trabajo" ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
        }
    });
    
    /*
    $("#fecha_fin_trabajo").blur(function() {
        alert("salio");
        
        var fecha_ini = $("#fecha_inicio_trabajo").val();
        var fecha_fin = $("#fecha_fin_trabajo").val();
        
        var f_ini = new Date()
        f_ini.setFullYear(fecha_ini.substring(6,10)); 
        f_ini.setMonth(fecha_ini.substring(3,5) + 1); 
        f_ini.getDate(fecha_ini.substring(0,2)); 
        f_ini.setHours(fecha_ini.substring(11,13)); 
        f_ini.setMinutes(fecha_ini.substring(14,16)); 
        
        var f_fin = new Date()
        f_fin.setFullYear(fecha_fin.substring(6,10)); 
        f_fin.setMonth(fecha_fin.substring(3,5) + 1); 
        f_fin.getDate(fecha_fin.substring(0,2)); 
        f_fin.setHours(fecha_fin.substring(11,13)); 
        f_fin.setMinutes(fecha_fin.substring(14,16)); 
        
        if( f_ini > f_fin ) {
            alert("fecha_ini mayor que fecha_fin");
        }else {
            alert("fecha_ini menor que fecha_fin");
        }

    });
    */
});
</script>
<script type="text/javascript">
//    $("select[name=terminal]").keyup(function(e){
//       if(e.keyCode=='38' || e.keyCode=='40'){
//           changeTerminal();
//       }
//    });
    $("select[name=armario]").change(function(){
        var cod_elemento = $("input[name=codelementoptr]").val();
        if(cod_elemento == '8'){
            //$.post("modulos/innovaPTR/registro.ptr.php?cmd=getDireccion_byArmario", {
            $.post("modulos/maps/registro.trabajo.fftt.php?cmd=getDireccion_byArmario", {
                mdf: $("select[name=mdf]").val(), 
                cod_zonal: $("input[name=codzonal]").val(),
                cablearmario: $(this).val()
            },
            function(data) {
                $('input[name=direccioncliente]').val(data.direccion).attr('readonly','').focus();
            },
            'json');         
        }
    });
</script>
