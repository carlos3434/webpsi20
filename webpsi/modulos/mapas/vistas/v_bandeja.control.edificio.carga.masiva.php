<style>
em {
    color: #ff0000;
}

.legend {
    border: 1px solid #5C9CCC;
    height: 260px;
    overflow: auto;
    padding: 0 5px 0 0 !important;
}

fieldset.formsFieldset {
    border: 1px solid #5C9CCC !important;
    padding: 10px;
}

fieldset.formsFieldset legend {
    color: #5C9CCC !important;
    font-weight: bold;
    padding: 5px;
}

</style>

<script type="text/javascript">
$(document).ready(function () {
    $("#tabsProcesarArchivos").tabs();

    $('#divCargaHeader a').toggle(
        function() {
            $('#divCargaHeader a').html('ocultar detalles');
            $('#divCargaDetalles').show(); 
        }, function () {
            $('#divCargaHeader a').html("ver detalles");
            $('#divCargaDetalles').hide(); 
        }
    );
    
    limpiarArchivo = function(id) {
        //$("#" + id).attr('value','');
        $("#" + id).replaceWith('<input id="' + id + '" class="frm_archivo" type="file" size="25" name="archivo_upload[]">');
    }

    $('#frm_adjuntar_archivos').ajaxForm({ 
        dataType    : 'json', 
        beforeSubmit: validate,
        success     : processJson     
    }); 

    function validate(formData, jqForm, options) {
        var query = $("select.required");
        
        var is_validate = 1;
        $.each( query, function() {
            if ( $(this).val() == '' ) {
                alert("Ingrese el campo " + $(this).attr('msg'));
                $(this).focus();
                is_validate = 0;
                return false;
            }
        });
        
        if (!is_validate) {
            return false;
        }
    	
        if( $("#archivo1").val() == '') {
            alert("Ingrese el archivo que desea cargar");
            return false;
        }

        if ( !confirm("Seguro de realizar la carga masiva?") ) {
    	    return false;
    	}
        
        $("#msg").show();
        $('#msgErrorCarga').empty();
        $('#divMsgErrorCarga').hide();
        $("input[name=btn_cargar]").attr('disabled','disabled');
        
        var error_archivo = '';
        var permitida = false;
        var image = $('input[name=archivo_upload[]]').fieldValue(); 

        $.each(image, function(i, item) {
            if(image[i]!=''){
                var extension = (image[i].substring(image[i].lastIndexOf("."))).toLowerCase();
                if (! (extension && /^(.csv|.csv)$/.test(extension))){ //.csv 
                    // extensiones permitidas
                    error_archivo = 'Error: Solo se permiten archivos de tipo CSV.';
                    // cancela upload 
                    $("input[name=btn_cargar]").attr('disabled','');
                    permitida = true;
                }
            }
        });
        if(permitida) {
            alert(error_archivo); 
            $("#msg").hide();
            $('#divMsgErrorCarga').hide();
            return false; 
        }
    }
     
    function processJson(data) { 
        //limpiando el formulario
        $(".frm_archivo").attr('value','');
        $("#archivo1").replaceWith('<input id="archivo1" class="frm_archivo" type="file" size="25" name="archivo_upload[]">');        

        $("#lugar").find('option:first').attr('selected','selected');
        
        $("#msg").hide();
        $('#divMsgErrorCarga').hide();
        $("input[name=btn_cargar]").attr('disabled','');
        
        if(data['tipo'] == 'imagen') { 
            alert(data.mensaje); 
            return false; 
        }
        else if(data['tipo'] == 'log') { 
            var mensajes = data['msg_log'];
            if ( mensajes.length > 0 ) {
            	$('#divMsgErrorCarga').show();
                $('#msgErrorCarga').empty();

                for (var i = 0; i < mensajes.length; i++) {
                	$('#msgErrorCarga').append('<li>' + mensajes[i] + '</li>');
                }
            }
            
            alert(data.mensaje); 
            return false; 
        }
        
        if (data['flag'] == '1'){
            alert(data['mensaje']);

            //cerramos la ventana 
            $("#childModal").dialog('close');

        } else{
            alert(data['mensaje']);
        } 
    }
    
});
</script>

<div style="width: 100%;" id="divCargaMasiva" >

    <fieldset class="formsFieldset">
    <legend>Proceso de Carga masiva</legend>
                
        <table id="tb_sup_tl_registros" style="margin-left:0px;" border="0" width="100%" cellspacing="1" cellpadding="4" align="center">
        <tr>
            <td style="width:45%; height: 30px; text-align: left;" valign="top">
                <div>
                    
                    <div style="border: 1px solid #5C9CCC; padding:5px; margin-bottom:5px;">
                        <div id="divCargaHeader" style="font-weight:bold; margin-bottom:4px;">
                            FORMATO DE ARCHIVOS FUENTE
                            <a href="javascript:void(0);" style="color:#5C9CCC !important; font-size:9px;">Ver detalles</a>
                        </div>
                        <div id="divCargaDetalles" style="display:none;">
                            <span style="font-weight:bold; color:#004350">Lima</span>
                            <p>Item | <em>(*)</em>Fecha termino construcci&oacute;n | <em>(*)</em>Ducteria interna fecha | <em>(*)</em>Montante fecha | <em>(*)</em>Punto energia fecha | 
                            Ducteria | Montante | Punto energia | Gestion de obras | Estado | Validado por Call | <em>(*)</em>Fecha tratamiento Call | <em>(*)</em>Fecha cableado</p> <br />
                            
                            <span style="font-weight:bold; color:#004350">Provincia</span> <br />
                            <p>Codigo proyecto web | <em>(*)</em>Fecha termino contrucci&oacute;n | Estado | <em>(*)</em>Fecha de cableado | Validado por Call | <em>(*)</em>Fecha tratamiento Call | RUC constructora</p>
                            <br />
                            <em>(*)</em> Formato de fecha (dd/mm/yyyy)
                        </div>
                    </div>
                
                    <form enctype="multipart/form-data" action="modulos/maps/bandeja.control.edificio.php?cmd=cargaMasivaArchivos" method="post" name="frm_adjuntar_archivos" id="frm_adjuntar_archivos">
                    <table id="tbl_upload" class="tb_detalle_actu" width="100%">
                    <tr>
                        <td class="da_titulo_gest" colspan="6">FORMULARIO DE CARGA DE ARCHIVO</td>
                    </tr>
                    <tr style="text-align: left;">
                    <td class="da_subtitulo">LIMA/PROVINCIA</td>
                        <td>
                            <select name="lugar" id="lugar" class="required frm_fotos" msg="Lima/Provincia" >
                                <option value="">Seleccione</option>
                                <option value="Lima">Lima</option>
                                <option value="Provincia">Provincia</option>
                            </select>
                            <em>(*)</em>
                        </td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">ARCHIVO</td>
                        <td>
                            <input id="archivo1" name="archivo_upload[]" type="file" size="25" class="frm_archivo" /> &nbsp;<em>(*)</em>
                            <a onclick="limpiarArchivo('archivo1')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                            <span class="requerido">(Solo archivos tipo .csv - Tama&ntilde;o max: 5Mb) </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="padding: 5px; float:left;">
                                <input type="submit" value="Cargar" name="guardar" title="Cargar" id="btn_cargar" class="boton">
                            </div>
                            <div style="float:left; margin: 15px 0 0 0; display: none;" id="msg">
                                <img alt="" src="img/loading.gif"> <b>Cargando archivo.</b>
                            </div>
                        </td>
                    </tr>
                    </table>
                    
                    <br />
                    <table id="tbl_upload_msg" width="100%">
                    <tr>
                        <td>
                            <div id="divMsgErrorCarga" style="display:none; padding: 5px; max-height: 200px; overflow: auto; border:1px solid #5C9CCC;">
                                <div style="color:#004350; font-weight:bold;">Mensajes de Error</div>
                                    <ul id="msgErrorCarga">
                                    </ul>
                            </div>
                        </td>
                    </tr>
                    </table>
                    </form>
                </div>
            </td>
        </tr>
        </table>

    </fieldset>
    
</div>

		