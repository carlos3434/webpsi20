<?php
if ($totalRegistros>0) {

    $item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
    foreach ($arrObjCapa as $objCapa) {
    ?>
        <tr>
        <td class="celdaX"><?php echo $item++; ?></td>
        <td class="celdaX">
            <a onclick="editarCapa('<?php echo $objCapa->__get('_idCapa')?>')" href="javascript:void(0)">
                <img width="13" height="13" title="Editar" border="0" src="img/pencil.png">
            </a>
        </td>
            <td class="celdaX" align="left">&nbsp; <?php echo $objCapa->__get('_nombre'); ?></td>
            <td class="celdaX"><?php echo $objCapa->__get('_abvCapa'); ?></td>
            <td class="celdaX"><?php echo $objCapa->__get('_ico'); ?></td>
            <td class="celdaX"><?php echo obtenerFecha($objCapa->__get('_fechaInsert')); ?></td>
            <td class="celdaX"><?php echo obtenerFecha($objCapa->__get('_fechaUpdate')); ?></td>
        </tr>
 <?php
    }
} else {
    ?>
    <tr>
        <td style="height:30px" colspan="7">Ningun registro encontrado con los criterios de b&uacute;squeda ingresados</td>
    </tr>
<?php
}
?>