<style>
table.tb_detalle_actu {
    background-color: #F2F2F2;
    font-size: 11px;
}
.tb_detalle_actu td {
    background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo_gest{
    color: #FFFFFF;
    background-color: #70C72A;
    font-weight: bold;
    font-size: 11px;
    line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
    color: #000000;
    font-weight: bold;
    background-color: #E6EEEE;
    font-size: 10px;
}
</style>
<div id="divMasInfo">

    <?php
    if ( !empty($arrMasInfoTerminal) ) {
    ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td valign="top">

            <div id="cont_actu_detalle">

                <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
                <tbody>
                <tr>
                    <td class="da_titulo_gest" colspan="4">MAS INFORMACI&Oacute;N DEL TERMINAL</td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo" width="25%">ZONAL:</td>
                    <td width="25%"><?php echo $arrMasInfoTerminal[0]['czonal']?></td>
                    <td class="da_subtitulo" width="25%">MDF:</td>
                    <td width="25%"><?php echo $arrMasInfoTerminal[0]['cmdf']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">CABLE / ARMARIO</td>
                    <td><?php echo $arrMasInfoTerminal[0]['armcab']?></td>
                    <td class="da_subtitulo">CAJA</td>
                    <td><?php echo $arrMasInfoTerminal[0]['nrocaja']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">TIPO DSLAM:</td>
                    <td><?php echo $arrMasInfoTerminal[0]['tipoDslam']?></td>
                    <td class="da_subtitulo">TIPO DSLAM AO</td>
                    <td><?php echo $arrMasInfoTerminal[0]['tipoDslam_AO']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">Q SPEEDY</td>
                    <td><?php echo $arrMasInfoTerminal[0]['num_clientes_speedy']?></td>
                    <td class="da_subtitulo">ATENUACI&Oacute;N</td>
                    <td><?php echo $arrMasInfoTerminal[0]['att_down_max']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">SE&Ntilde;AL RUIDO</td>
                    <td><?php echo $arrMasInfoTerminal[0]['snr_down_max']?></td>
                    <td class="da_subtitulo">ASSIA MIN</td>
                    <td><?php echo $arrMasInfoTerminal[0]['rservice_m12_min']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">ASSIA MAX</td>
                    <td><?php echo $arrMasInfoTerminal[0]['rservice_m12_max']?></td>
                    <td class="da_subtitulo">CANCELADAS X DSLAM</td>
                    <td><?php echo $arrMasInfoTerminal[0]['canc_xdislam']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">CANCELADAS T&Eacute;CNICAS</td>
                    <td><?php echo $arrMasInfoTerminal[0]['canc_xtecnica']?></td>
                    <td class="da_subtitulo">BITACORA CANCELADAS X DSLAM</td>
                    <td><?php echo $arrMasInfoTerminal[0]['canc_bitacora_dislam']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">BITACORA CANCELADAS T&Eacute;CNICAS</td>
                    <td><?php echo $arrMasInfoTerminal[0]['canc_bitacora_tecnica']?></td>
                    <td class="da_subtitulo">CAMPO 1</td>
                    <td><?php echo $arrMasInfoTerminal[0]['campo1']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">CAMPO 2</td>
                    <td><?php echo $arrMasInfoTerminal[0]['campo2']?></td>
                    <td class="da_subtitulo">CAMPO 3</td>
                    <td><?php echo $arrMasInfoTerminal[0]['campo3']?></td>
                </tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">CAMPO 4</td>
                    <td><?php echo $arrMasInfoTerminal[0]['campo4']?></td>
                    <td class="da_subtitulo">CAMPO 5</td>
                    <td><?php echo $arrMasInfoTerminal[0]['campo5']?></td>
                </tr>
                </table>

                <div style="height:5px;"></div>

            </div>

        </td>
    </tr>
    </table>
    <?php 
    } else {
    ?>
        <p style="padding:10px; text-align:center;">No se ha encontrado informaci&oacute;n adicional para este terminal. <br/> <b>Elija otro terminal o int&eacute;ntelo nuevamente en unos instantes</b></p>
    <?php 
    }
    ?>
    <br />
</div>