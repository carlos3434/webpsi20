<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

$objPoligonoTipo = new Data_PoligonoTipo();
$arrayTipoCapas = $objPoligonoTipo->listar($conexion, 'R', '', '1');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>


<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">

var myLayout;
var map = null;
var poligonosDibujar_array = [];

var poligonoDibujar = null;

//valor por default
var flag_trazo = 'ruta';

var markers_arr = [];
var xy_arr        = [];

$(document).ready(function () {

    myLayout = $('body').layout({
        // enable showOverflow on west-pane so popups will overlap north pane
        west__showOverflowOnHover: true,
        west: {size:350}
    });

    $('#divbtnNuevo').click(function() {
        $('#o_tipocapa').attr('value','');
        $('#divNuevo').show();
    });
    $('#Cancelar').click(function() {
        $('#o_tipocapa').attr('value','');
        $('#divNuevo').hide();
    });
    
    $('#Agregar').click(function() {
        var new_option = jQuery.trim($('#o_tipocapa').attr('value'));
        if ( new_option == '' ) {
            alert("Ingrese un nuevo tipo capa");
            return false;
        }
        
        var tipo = 'P';
        if ( flag_trazo == 'ruta' ) {
            tipo = 'R';
        }
        
        data_content = {
            'action'    : 'agregarTipoCapa',
            'nombre'    : new_option,
            'tipo'        : tipo
        };
        $.ajax({
            type:   "POST",
            url:    "../trazar.ruta.poligono.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                if ( datos['success'] ) {
                    
                    var poligono_tipo = datos['poligono_tipo'];
                    
                    $('#tipocapa').append('<option value="' + poligono_tipo['idpoligono_tipo'] + '">' + poligono_tipo['nombre'] + '</option>');
                    $('#o_tipocapa').attr('value','');
                    $('#divNuevo').hide();
                    
                    
                }else {
                    alert(datos['msg']);
                    return false;
                }
            }
        });

    });
    
    $('#clearMarkers').click(function(){
        //limpia los markers del mapa
        M.clearMarkers();
        
        //elimino elementos del array 
        markers_arr = [];
    });
    
    $('#clearRutaPoligono').click(function(){
        //limpia los poligonos del mapa
        M.clearPoligonos();
        
        //elimino elementos del array 
        xy_arr = [];
    });
    
    $('.clsTrazo').click(function(){
        flag_trazo = $(".clsTrazo:checked").val();
        //alert(flag_trazo);
        
        data_content = {
            'action'    : 'buscaTipoCapa',
            'tipo'        : flag_trazo
        };
        $.ajax({
            type:   "POST",
            url:    "../trazar.ruta.poligono.php",
            data:   data_content,
            dataType: "html",
            success: function(datos) {
                $("#divTipocapa").html(datos);
            }
        });

        M.clearMarkers();
        M.clearPoligonos();
        
        markers_arr = [];
        xy_arr = [];
    });
    
    $('#crearRutaPoligono').click(function() {
    
        if ( flag_trazo == 'ruta' ) {
            if ( xy_arr.length < 2 ) {
                alert("Debe seleccionar 2 puntos como minimo para crear una ruta");
                return false;
            }
        }else if ( flag_trazo == 'poligono' ) {
            if ( xy_arr.length < 3 ) {
                alert("Debe seleccionar 3 puntos como minimo para crear un poligono");
                return false;
            }
        }
    
        M.crearRutaPoligono();
        
    });
    
    $('#btnGrabar').click(function(){
        M.grabarRutaPoligono();
    });

    var M = {
        initialize: function() {

            if (typeof (google) == "undefined") {
                alert('Verifique su conexion a maps.google.com');
                return false;
            }
        
            var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
            var myOptions = {
                  zoom: 14,
                  center: latlng,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            google.maps.event.addListener(map, "click", function(event) {

                //if ( confirm("Desea agregar este punta para trazar su ruta/poligono") ) {
                
                    /*
                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map
                    });
                    markers_arr.push(marker);
                    */
                    
                    var Coords = event.latLng;
                    var x = Coords.lng();
                    var y = Coords.lat();
                
                    xy_arr.push(x + '|' + y);
                    
                    M.clearPoligonos();
                    M.crearRutaPoligono();
                //}
            });
            
            google.maps.event.addListener(map, "rightclick", function(event) {

                //if ( confirm("Desea eliminar el ultimo punto de su ruta/poligono") ) {
                
                    xy_arr.pop();
                    
                    M.clearPoligonos();
                    M.crearRutaPoligono();
                //}
                
            });
            
        },
        
        generaColorPoligono: function() {
            var valores_arr = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
            var color = '';
            for( i=1; i<=6; i++ ) {
                var rand =  Math.ceil(Math.random()*15);
                //alert(rand);
                color += valores_arr[rand];
            }
            //alert('#' + color);
            return '#' + color;
        },
        
        clearMarkers: function() {
            for (var n = 0, marker; marker = markers_arr[n]; n++) {
                marker.setVisible(false);
            }
        },

        clearPoligonos: function() {
            for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
                poligonoDibujar.setMap(null);
            }
        },

        loadPoligono: function(xy_array) {
            var datos             = xy_array['coords'];
            var color_poligono     = xy_array['color'];
            var poligonoCoords     = [];

            if ( datos.length > 0) {
            
                for (var i in datos) {
                    var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                    poligonoCoords.push(pto_poligono);
                }

                // Construct the polygon
                poligonoDibujar = new google.maps.Polygon({
                    paths: poligonoCoords,
                    strokeColor: color_poligono,
                    strokeOpacity: 0.6,
                    strokeWeight: 4,
                    fillColor: color_poligono,
                    fillOpacity: 0.5
                });

                //agrego al array las capas seleccionadas
                poligonosDibujar_array.push(poligonoDibujar);

                //seteo la capa en el mapa
                M.clearPoligonos();
                poligonoDibujar.setMap(map);
                
                
                google.maps.event.addListener(poligonoDibujar, "click", function(event) {

                    var Coords = event.latLng;
                    var x = Coords.lng();
                    var y = Coords.lat();
                
                    xy_arr.push(x + '|' + y);
                    
                    M.clearPoligonos();
                    M.crearRutaPoligono();
                });
                
                google.maps.event.addListener(poligonoDibujar, "rightclick", function(event) {
                
                    xy_arr.pop();
                    
                    M.clearPoligonos();
                    M.crearRutaPoligono();
                });

            }    
        },
        
        loadPolilinea: function(xy_array) {

            var datos             = xy_array['coords'];
            var color_polilinea = xy_array['color'];
            var polilineaCoords = [];

            if ( datos.length > 0) {
                for (var i in datos) {
                    var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                    polilineaCoords.push(pto_polilinea);
                }

                // Construct the polyline                
                polilineaDibujar = new google.maps.Polyline({
                    path: polilineaCoords,
                    strokeColor: color_polilinea,
                    strokeOpacity: 1.0,
                    strokeWeight: 4
                });

                //agrego al array las capas seleccionadas
                poligonosDibujar_array.push(polilineaDibujar);
                
                polilineaDibujar.setMap(map);                
            }    
        },
        
        crearRutaPoligono: function() {
        
            if ( xy_arr.length > 0 ) {
                var xy_array = new Array();

                for( i in xy_arr ) {
                    var xy_values = new Array();
                    xy_values = xy_arr[i].split('|');
                    
                    xy_values['x'] = xy_values[0];
                    xy_values['y'] = xy_values[1];
                    
                    xy_array.push(xy_values);
                }
                
                var xy_total_array = new Array();
                
                xy_total_array['coords'] = xy_array;
                //alert(M.generaColorPoligono());
                xy_total_array['color'] = M.generaColorPoligono();

                
                if ( flag_trazo == 'ruta' ) {
                    M.loadPolilinea(xy_total_array);
                }else if ( flag_trazo == 'poligono' ) {
                    M.loadPoligono(xy_total_array);
                }
            }
            else {
                alert('No existen puntos seleccionados para crear una ruta/poligono');
            }
        
        },
        
        grabarRutaPoligono: function() {
        
            if ( flag_trazo == 'ruta' && xy_arr.length < 2 ) {
                alert("Debe seleccionar 2 puntos como minimo para crear una ruta");
                return false;
            }else if ( flag_trazo == 'poligono' && xy_arr.length < 3 ) {
                alert("Debe seleccionar 3 puntos como minimo para crear un poligono");
                return false;
            }

            var tipocapa     = $("#tipocapa").val();
            var elemento     = $("#elemento").val();
        
            if ( flag_trazo == '' ) {
                alert("Selecione trazar una ruta o poligono");
                return false;
            }
            
            
            if ( tipocapa == '' ) {
                alert("Selecione el tipo de capa");
                return false;
            }
            if ( elemento == '' ) {
                alert("Ingrese un nombre para la ruta/poligono");
                return false;
            }

            if ( confirm("Esta seguro de grabar esta ruta/poligono") ) {
                
                $("#btnGrabar").attr('disabled', 'disabled');
                $("#box_loading").show();
                data_content = {
                    'action'    : 'grabarRutaPoligono',
                    'tipo'        : flag_trazo,
                    'tipocapa'    : tipocapa,
                    'elemento'    : elemento,
                    'xy_arr'     : xy_arr
                };
                $.ajax({
                    type:   "POST",
                    url:    "../trazar.ruta.poligono.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(datos) {
                        if ( datos['success'] ) {
                            alert(datos['msg']);
                            
                            //limpiando el mapa
                            M.clearMarkers();
                            M.clearPoligonos();
                            markers_arr = [];
                            xy_arr = [];
                            
                            $("#elemento").attr('value', '');
                            $("#btnGrabar").removeAttr('disabled');
                            $("#box_loading").hide();
                            
                        }else {
                            alert(datos['msg']);
                            $("#elemento").attr('value','');
                            $("#elemento").focus();
                            $("#btnGrabar").removeAttr('disabled');
                            $("#box_loading").hide();
                        }
                    }
                });

            }
        }
        
    };

    M.initialize();
               
});


</script>
<style type="text/css">
    .sidebar_item {
        float: left;
        padding: 5px 0;
        border-bottom: 1px solid #DDDDDD;
    }
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
        background: #FFF; 
        border: 1px solid #BBB; 
        padding: 7px; 
        overflow: auto;
    } 

    .ui-layout-resizer {
        background: #DDD; 
    } 

    .ui-layout-toggler {
        background: #AAA; 
    }
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
<script>
    $(function() {
        $("#btnGrabar, #crearRutaPoligono").button();
    });
    </script>
    </head>
    <body>
    <input type="hidden" value="1" name="emisor" id="emisor" />
    <div class="ui-layout-west">

        <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
        <tr>
            <td><span class="l_tit">Trazar rutas / poligonos</span></td>
        </tr>
        <tr>
            <td align="left" class="label">
                <input type="radio" class="clsTrazo" name="rdoTrazo" value="ruta" id="ruta" checked="checked"> Trazar rutas <br />
                <input type="radio" class="clsTrazo" name="rdoTrazo" value="poligono" id="poligono"> Trazar poligonos
            </td>
        </tr>
        <tr>
            <td align="left" class="label" style="height: 32px;">
                <!--<a href="javascript:void(0);"><span id="clearMarkers">Limpiar markers</span></a> &nbsp; | &nbsp;-->
                <span id="clearRutaPoligono"><img src="../../../img/btn_clear_32.png" style="width:15px;height:15px;"> <a href="javascript:void(0);">Limpiar ruta/poligono</a></span>
            </td>
        </tr>
        <!--<tr>
            <td align="left" class="label">
                <button id="crearRutaPoligono">Dibujar ruta/poligono</button>
            </td>
        </tr>-->
        </table>
        
        <div style="height: 25px;"></div>
        
        <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
        <tr>
            <td colspan="2"><span class="l_tit">Informaci&oacute;n de la Ruta/Poligono</span></td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td align="left" class="label" width="70">Tipo Capa:</td>
            <td>
                <div id="divTipocapa" style="float:left;">
                <select name="tipocapa" id="tipocapa" style="width:100px; font-size: 11px;height: 20px;padding: 1px;">
                    <option value="">Seleccione</option>
                    <?php
                    foreach ( $arrayTipoCapas as $tipocapa ) {
                    ?>
                    <option value="<?php echo $tipocapa['idpoligono_tipo']?>"><?php echo $tipocapa['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
                </div>
                <div id="divbtnNuevo" style="float:left;"><img title="Agregar nuevo Tipo Capa" src="../../../img/plus_16.png" style="width:13px;height:13px;padding:3px;cursor:pointer;"></div>
                <div id="divNuevo" style="float:left;display:none;">
                    <input size="12" type="text" name="o_tipocapa" id="o_tipocapa" style="font-size: 11px;height: 16px;padding: 1px;">
                    <span id="Agregar"><a href="javascript:void(0);">[+]</a></span> &nbsp;
                    <span id="Cancelar"><a href="javascript:void(0);">[x]</a></span>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" class="label">Nombre:</td>
            <td><input type="text" name="elemento" id="elemento" size="40" style="font-size: 11px;height: 16px;padding: 1px;"></td>
        </tr>
        <tr>
            <td colspan="2" height="40"><button id="btnGrabar">Grabar</button></td>
        </tr>
        </table>
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
            
    </div>
    <div class="ui-layout-center">
        
        <div id="box_resultado">
                      
           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>

        </div>
        
    </div>
        
    <div id="parentModal" style="display: none;">
        <div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>