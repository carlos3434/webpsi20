<script type="text/javascript">
$(document).ready(function() {
    $("#tabsFotosPirata").tabs();
});

//carga el primer tab con las fotos del pirata
fotosPirata('<?php echo $arrObjDuna[0]->__get('_idDuna')?>');
<?php $idDuna = $arrObjDuna[0]->__get('_idDuna') ?>
</script>
<div id="content_pirata_fotos">
    <div id="tabsFotosPirata" style="width:99%; margin-bottom: 10px;">
        <ul>
            <li>
                <a id="tabFotos" href="#divFotos" 
                    onclick="fotosPirata('<?php echo $idDuna ?>')" 
                    style="color:#1D87BF; font-weight: 600;">Fotos del Pirata
                </a>
            </li>
            <li><a id="tabFotosAdjuntar" href="#divFotosAdjuntar" 
                    onclick="adjuntarFotosPirata('<?php echo $idDuna ?>')" 
                    style="color:#1D87BF; font-weight: 600;">Adjuntar Fotos al Pirata
                </a>
            </li>
        </ul>
        <div id="divFotos"></div>
        <div id="divFotosAdjuntar"></div>
    </div>

</div>