<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
        <title>Webunificada - Movil</title>
        <script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <link href="../../css/movil.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">

            $(document).ready(function() {

                var map = null;
                var markers_arr = [];
	
                var item_ok = 1;
                
                var x_coord = '<?php echo $_REQUEST['x'] ?>';
                var y_coord = '<?php echo $_REQUEST['y'] ?>';
                
                var x = '';
                var y = '';
                if( x_coord != '' && y_coord != '' ) {
                    x = x_coord;
                    y = y_coord;
                }
    
                var M = {
                    initialize: function() {
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        if( x != '' && y != '' ) {
                            latlng = new google.maps.LatLng(y,x);
                        }
                        var myOptions = {
                            zoom: 17,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            navigationControl: true,
                            navigationControlOptions: {
                                style: google.maps.NavigationControlStyle.ANDROID,
                                position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			
                        marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: 'vistas/icons/markergreen.png'
                        });
                        markers_arr.push(marker);
	    	
                        google.maps.event.addListener(map, "click", function(event) {
			
                            if(!confirm("Desea establecer este nuevo punto x,y?")) {
                                return false;
                            }
			
                            //clear markers
                            M.clearMarkers();
	        	
                            marker = new google.maps.Marker({
                                position: event.latLng,
                                map: map,
                                icon: 'vistas/icons/markergreen.png'
                            });
                            markers_arr.push(marker);
                            var Coords = event.latLng;
                            var x = Coords.lng();
                            var y = Coords.lat();
				
                            map.setCenter(event.latLng);
				
                            $("#x").attr('value', x);
                            $("#y").attr('value', y);
				
                        });
                    },

                    clearMarkers: function() {
                        for (var n = 0, marker; marker = markers_arr[n]; n++) {
                            marker.setVisible(false);
                        }
                    }

                };


                $(".upload").toggle(function() {
                    $("#div" + $(this).attr('id')).show();
                    $("#" + $(this).attr('id')).attr('value', ' [x] ');
                }, function () {
                    $("#div" + $(this).attr('id')).hide();
                    $("#" + $(this).attr('id')).attr('value', ' .. ');
                }
            );
	
                $("#item, #item_etapa").change(function() {
                    if( $("#item").val() == "" ) {
                        $("#msgItem").html('');
                        return false;
                    }
                    data_content = {
                        'item' 			: $("#item").val() + $("#item_etapa").val(),
                        'item_inicial'	: $("#item_inicial").val()
                    };
                    $("#fongoLoader").show();
                    $.ajax({
                        type:   "POST",
                        url:    "m.buscomp.editar.componente.php?action=searchItem",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            if(datos['success'] == 1) {
                                //ok
                                $("#msgItem").html('<div style="color:#0E774A;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                                item_ok = 1;
                            }else {
                                //item existe
                                $("#msgItem").html('<div style="color:#FF0000;">' + datos['msg'] + '</div>').hide().fadeIn("slow");
                                item_ok = 0;

                            }
                            $("#fongoLoader").hide();
                        }
                    });
                });
	
	
                $("#nro_blocks").change(function() {
                    nro_blocks = this.value;
                    if(nro_blocks == "") {
                        $("#divBlock").html('');
                    }else {
                        $("#divBlock").html('');
                        selhtml = '<div style="width:180px;">';
                        selhtml += '<span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px; font-weight:bold;">Nro. pisos</span>';
                        selhtml += '<span style="float:left; width:80px; font-weight:bold;">Nro. dptos</span>';
                        selhtml += '</div>';
                        for(var i=1; i<=nro_blocks; i++) {
                            selhtml += '<div style="width:180px;">';
                            selhtml += '<span style="float:left; width:20px;">#' + i + ' </span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_' + i + '" id="nro_pisos_' + i + '" size="5" value="" /></span>';
                            selhtml += '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_' + i + '" id="nro_dptos_' + i + '" size="5" value="" /></span>';
                            selhtml += '</div>';
                        }
                        $("#divBlock").html(selhtml);
                    }
                });
	
                $("#selDpto").change(function() {
                    IDdpto = this.value;
                    if(IDdpto == "") {
                        return false;
                    }
                    data_content = "action=buscarProv&IDdpto=" + IDdpto;
                    $.ajax({
                        type:   "POST",
                        url:    "m.buscomp.add.componente.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            var selProv = ''
                            for (var i in datos) {
                                selProv += '<option value="' + datos[i]['codprov'] + '">' + datos[i]['nombre'] + '</option>';
                            }

                            $("#selProv").html(selProv);
                            $("#selDist").html('<option value="">Seleccione</option>');
                        }
                    });
                });
	
                $("#selProv").change(function() {
                    IDdpto = $("#selDpto").val()
                    IDprov = this.value;
                    if(IDdpto == "" && IDprov == "") {
                        return false;
                    }
                    data_content = "action=buscarDist&IDdpto=" + IDdpto + "&IDprov=" + IDprov;
                    $.ajax({
                        type:   "POST",
                        url:    "m.buscomp.add.componente.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                
                            var selDist = ''
                            for (var i in datos) {
                                selDist += '<option value="' + datos[i]['coddist'] + '">' + datos[i]['nombre'] + '</option>';
                            }

                            $("#selDist").html(selDist);
                        }
                    });
                });
	
                $("#GrabarEdificio").click(function() {
                    if( !item_ok ) {
                        alert("El Item que desea registrar ya existe, ingrese uno nuevo.");
                        $("#item").focus();
                        return false;
                    }
	
                    var fecha_registro_proyecto = $("#dia_fecha_registro_proyecto").val() + '-' + $("#mes_fecha_registro_proyecto").val() + '-' + $("#ano_fecha_registro_proyecto").val();
                    if( fecha_registro_proyecto != '' && fecha_registro_proyecto != '--' ) {
                        if( ValidarFecha( fecha_registro_proyecto ) == false ) {
                            alert("Ingrese la fecha de registro de proyecto correcta.");
                            return false;
                        }
                    }else {
                        fecha_registro_proyecto = '';
                    }
	
                    var fecha_termino_construccion = $("#diafechaTerminoConstruccion").val() + '-' + $("#mesfechaTerminoConstruccion").val() + '-' + $("#anofechaTerminoConstruccion").val();
                    if( fecha_termino_construccion != '' && fecha_termino_construccion != '--' ) {
                        if( ValidarFecha( fecha_termino_construccion ) == false ) {
                            alert("Ingrese la fecha de termino de construccion correcta.");
                            return false;
                        }
                    }else {
                        fecha_termino_construccion = '';
                    }
		
                    var montante_fecha = $("#diaMontanteFecha").val() + '-' + $("#mesMontanteFecha").val() + '-' + $("#anoMontanteFecha").val();
                    if( montante_fecha != '' && montante_fecha != '--' ) {
                        if( ValidarFecha( montante_fecha ) == false ) {
                            alert("Ingrese montante fecha correcta.");
                            return false;
                        }
                    }else {
                        montante_fecha = '';
                    }
		
                    var ducteria_interna_fecha = $("#diaDucteriaInternaFecha").val() + '-' + $("#mesDucteriaInternaFecha").val() + '-' + $("#anoDucteriaInternaFecha").val();
                    if( ducteria_interna_fecha != '' && ducteria_interna_fecha != '--' ) {
                        if( ValidarFecha( ducteria_interna_fecha ) == false ) {
                            alert("Ingrese ducteria interna fecha correcta.");
                            return false;
                        }
                    }else {
                        ducteria_interna_fecha = '';
                    }
		
                    var punto_energia_fecha = $("#diaPuntoEnergiaFecha").val() + '-' + $("#mesPuntoEnergiaFecha").val() + '-' + $("#anoPuntoEnergiaFecha").val();
                    if( punto_energia_fecha != '' && punto_energia_fecha != '--' ) {
                        if( ValidarFecha( punto_energia_fecha ) == false ) {
                            alert("Ingrese punto de energia fecha correcta.");
                            return false;
                        }
                    }else {
                        punto_energia_fecha = '';
                    }
		
                    var fecha_seguimiento = $("#diaFechaSeguimiento").val() + '-' + $("#mesFechaSeguimiento").val() + '-' + $("#anoFechaSeguimiento").val();
                    if( fecha_seguimiento != '' && fecha_seguimiento != '--' ) {
                        if( ValidarFecha( fecha_seguimiento ) == false ) {
                            alert("Ingrese fecha de seguimiento correcta.");
                            return false;
                        }
                    }else {
                        fecha_seguimiento = '';
                    }
		
	
                    var data = {
                        x	 					: $("#x").val(),
                        y	 					: $("#y").val(),
                        estado					: $("#estado").val(),
                        item_inicial			: $("#item_inicial").val(),
                        item					: $("#item").val(),
                        item_etapa				: $("#item_etapa").val(),
                        fecha_registro_proyecto : fecha_registro_proyecto,
                        ura	 					: $("#ura").val(),
                        sector	 				: $("#sector").val(),
                        mza_tdp	 				: $("#mza_tdp").val(),
                        idtipo_via	 			: $("#idtipo_via").val(),
                        direccion_obra	 		: $("#direccion_obra").val(),
                        numero	 				: $("#numero").val(),
                        mza	 					: $("#mza").val(),
                        lote	 				: $("#lote").val(),
                        idtipo_cchh	 			: $("#idtipo_cchh").val(),
                        cchh	 				: $("#cchh").val(),
                        distrito	 			: $("#selDpto").val() + $("#selProv").val() + $("#selDist").val(),
                        nombre_constructora	 	: $("#nombre_constructora").val(),
                        nombre_proyecto	 		: $("#nombre_proyecto").val(),
                        idtipo_proyecto	 		: $("#idtipo_proyecto").val(),
                        persona_contacto	 	: $("#persona_contacto").val(),
                        pagina_web	 			: $("#pagina_web").val(),
                        email	 				: $("#email").val(),
                        nro_blocks	 			: $("#nro_blocks").val(),
                        nro_pisos_1	 			: $("#nro_pisos_1").val(),
                        nro_dptos_1	 			: $("#nro_dptos_1").val(),
                        nro_pisos_2	 			: $("#nro_pisos_2").val(),
                        nro_dptos_2	 			: $("#nro_dptos_2").val(),
                        nro_pisos_3	 			: $("#nro_pisos_3").val(),
                        nro_dptos_3	 			: $("#nro_dptos_3").val(),
                        nro_pisos_4	 			: $("#nro_pisos_4").val(),
                        nro_dptos_4	 			: $("#nro_dptos_4").val(),
                        nro_pisos_5	 			: $("#nro_pisos_5").val(),
                        nro_dptos_5	 			: $("#nro_dptos_5").val(),
                        idtipo_infraestructura	: $("#idtipo_infraestructura").val(),
                        nro_dptos_habitados	 	: $("#nro_dptos_habitados").val(),
                        avance	 				: $("#avance").val(),
                        fecha_termino_construccion : fecha_termino_construccion,
                        montante	 			: $("#montante").val(),
                        montante_fecha			: montante_fecha,
                        montante_obs 			: $("#montante_obs").val(),
                        ducteria_interna	 	: $("#ducteria_interna").val(),
                        ducteria_interna_fecha  : ducteria_interna_fecha,
                        ducteria_interna_obs 	: $("#ducteria_interna_obs").val(),
                        punto_energia	 		: $("#punto_energia").val(),
                        punto_energia_fecha		: punto_energia_fecha,
                        punto_energia_obs 		: $("#punto_energia_obs").val(),
                        fecha_seguimiento		: fecha_seguimiento,
                        observacion	 			: $("#observacion").val(),
                        idedificio				: $("#idedificio").val()
                    };

                    if( data['estado'] == "" ) {
                        alert("Ingrese el campo Estado del Edificio.");
                        return false;
                    }
                    if( data['item'] == "" ) {
                        alert("Ingrese el campo Item.");
                        return false;
                    }
                    if( data['fecha_registro_proyecto'] == "" ) {
                        alert("Ingrese el campo Fecha Registro Proyecto.");
                        return false;
                    }
                    if( data['ura'] == "" ) {
                        alert("Ingrese el campo URA.");
                        return false;
                    }
                    if( data['idtipo_via'] == "" ) {
                        alert("Ingrese el campo Tipo Via.");
                        return false;
                    }
                    if( data['idtipo_cchh'] == "" ) {
                        alert("Ingrese el campo Tipo CCHH.");
                        return false;
                    }
                    if( $("#selDpto").val() == "" ) {
                        alert("Ingrese el campo Departamento.");
                        return false;
                    }
                    if( $("#selProv").val() == "" ) {
                        alert("Ingrese el campo Provincia.");
                        return false;
                    }
                    if( $("#selDist").val() == "" ) {
                        alert("Ingrese el campo Distrito.");
                        return false;
                    }
                    if( data['idtipo_proyecto'] == "" ) {
                        alert("Ingrese el campo Tipo Proyecto.");
                        return false;
                    }
                    if( data['nro_blocks'] == "" ) {
                        alert("Ingrese el campo Nro. de Blocks.");
                        return false;
                    }

                    var query = $("input.clsBlocks").serializeArray();
                    for (i in query) {
                        if( query[i].value == "" ) {
                            alert("Ingrese todos los datos en Nro. pisos y Nro. dptos.");
                            $("#" + query[i].name).focus();
                            return false;
                        }
                    } 
		
                    if( data['idtipo_infraestructura'] == "" ) {
                        alert("Ingrese el campo Tipo Infraestructura.");
                        return false;
                    }
    	
                    data_content = data;
                    $("#msg").show();

                    $.ajax({
                        type:   "POST",
                        url:    "m.buscomp.editar.componente.php?action=GrabaEdificio",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {

                            if(datos['success'] == 1) {
                                alert(datos['msg']);
                    
                                if( data['x'] != '' && data['y'] != '' ) {
                                    //redirect al mapa
                                    setTimeout("redirect(" + data['x'] + "," + data['y'] + ",'edi')", 1000);
                                }
                                
                            }else {
                                alert(datos['msg']);
                            }
                
                            $("#msg").hide();
                        }
                    });
		
                });
	
                $("#GrabarCompetencia").click(function() {
                    var data = {
                        x	 					: $("#x").val(),
                        y	 					: $("#y").val(),
                        competencia 			: $("#competencia").val(),
                        competencia_otro	 	: $("#competencia_otro").val(),
                        elementoencontrado 		: $("#elementoencontrado").val(),
                        elementoencontrado_otro	: $("#elementoencontrado_otro").val(),
                        ura						: $("#mdf").val(),
                        direccion				: $("#direccion").val(),
                        observacion				: $("#obs").val(),
                        idcompetencia			: $("#idcompetencia").val()
                    };

                    if( $('#competencia option:selected').text() == 'Otro' && $("#competencia_otro").val() == '' ) {
                        alert("Indique la competencia");
                        return false;
                    }
		
		
                    data_content = data;
                    $("#GrabarCompetencia").attr('disabled', 'disabled');
                    $("#Cancelar").attr('disabled', 'disabled');
                    $.ajax({
                        type:   "POST",
                        url:    "m.buscomp.editar.componente.php?action=GrabaCompetencia",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {

                            if(datos['success'] == 1) {
                                $("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                            }else {
                                $("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                            }
                        }
                    });
		
                    //redirect al mapa
                    setTimeout("redirect(" + data['x'] + "," + data['y'] + ",'com')", 3000);
                });
	
                //carga inicial
                M.initialize();
	
            });
            
            function limpiarXY() {
                $("#x").attr('value', '');
                $("#y").attr('value', '');
            }

            function redirect(x,y, comp) {
                window.top.location = 'm.buscomp.vermapa.php?search=1&x=' + x + '&y=' + y + '&comp[' + comp + ']=' + comp;
            }

            function soloNumeros(obj) {
                var re = /^(-)?[0-9]*$/;
                if (!re.test(obj.value)) {
                    obj.value = '';
                }
            }

            // El Formato es dd-mm-aaaa  
            // Ejemplo: if (Validar('14-08-1981')==false) { alert('Entrada Incorrecta') }  
            // Uso en formularios: onSubmit="return Validar(this.fecha.value)" 
            function ValidarFecha(Cadena){  
                var Fecha= new String(Cadena)   // Crea un string  
                var RealFecha= new Date()   // Para sacar la fecha de hoy  
                // Cadena A�o  
                var Ano= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))  
                // Cadena Mes  
                var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))  
                // Cadena D�a  
                var Dia= new String(Fecha.substring(0,Fecha.indexOf("-")))  
  
                if( Dia == '' || Mes == '' || Ano == '' ) {
                    //alert('Fecha ingresada no es correcta');
                    return false;
                }
  
                // Valido el a�o  
                if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
                    //alert('A�o inv�lido')  
                    return false  
                }  
                // Valido el Mes  
                if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
                    //alert('Mes inv�lido')  
                    return false  
                }  
                // Valido el Dia  
                if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
                    //alert('D�a inv�lido')  
                    return false  
                }  
                if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
                    if (Mes==2 && Dia > 28 || Dia>30) {  
                        //alert('D�a inv�lido')  
                        return false  
                    }  
                }  
    
            }

        </script>
    </head>
    <style>
        #map_canvas {
            height: 300px;
            width: 300px;
        }
    </style>
    <body>


        <?php include '../../m.header.php'; ?>

        <table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr>
                <td class="titulo">Editar Componente</td>
            </tr>
        </table>


        <div id="map_canvas"></div>


        <table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr>
                <td>X: <input type="text" name="x" id="x" value="<?php echo $_REQUEST['x'] ?>" readonly="readonly" /> &nbsp; <a style="font-size:12px;" onclick="limpiarXY()" href="javascript:void(0)">Limpiar X,Y</a><td>
            </tr>
            <tr>
                <td>Y: <input type="text" name="y" id="y" value="<?php echo $_REQUEST['y'] ?>" readonly="readonly" /><td>
            </tr>
        </table>

        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
            <tr>
                <td valign="top">

                    <div style="height:15px;"></div>

                    <?php
                    if ($_REQUEST['capa'] == 'edi') {
                        ?>

                        <table id="tblPrincipal" class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
                            <tr>
                                <td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Informaci&oacute;n principal</td>
                            </tr>
                            <tr>
                                <td width="150" class="compSubTitulo">Estado del Edificio:</td>
                                <td>
                                    <select name="estado" id="estado">
                                        <?php
                                        foreach ($arrEstado as $id => $val) {
                                            $selected = "";
                                            if ($id == $edificio[0]['estado']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $id ?>" <?php echo $selected ?>><?php echo $val ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <?php
                                $etapaFound = substr($edificio[0]['item'], -1, 1);
                                if (array_key_exists($etapaFound, $arrEtapa)) {
                                    $item = substr($edificio[0]['item'], 0, -1);
                                    $etapa = substr($edificio[0]['item'], -1, 1);
                                } else {
                                    $item = $edificio[0]['item'];
                                    $etapa = '';
                                }
                                ?>
                                <td class="compSubTitulo">Item: (<?php echo $edificio[0]['item'] ?>)</td>
                                <td>
                                    <input type="hidden" name="item_inicial" id="item_inicial" size="5" value="<?php echo $edificio[0]['item'] ?>" /> &nbsp;
                                    <input type="text" name="item" id="item" size="5" value="<?php echo $item ?>" style="float: left;" /> &nbsp;
                                    <select name="item_etapa" id="item_etapa" style="width:50px; float:left; margin:0 0 0 5px;">
                                        <option value="">Seleccione</option>
                                        <?php
                                        foreach ($arrEtapa as $idetapa => $etapaStr) {
                                            $selEtapa = "";
                                            if ($idetapa == $etapa) {
                                                $selEtapa = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $idetapa ?>" <?php echo $selEtapa ?>><?php echo $etapaStr ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <span id="msgItem" style="float: left; width: 80px; margin-left: 5px;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Fecha Registro Proyecto:</td>
                                <td>
                                    <?php
                                    $fechaRegistroProyecto = ( $edificio[0]['fecha_registro_proyecto'] != '' && $edificio[0]['fecha_registro_proyecto'] != '0000-00-00') ? date('m/d/Y', strtotime($edificio[0]['fecha_registro_proyecto'])) : '';
                                    $diaFechaRegistroProyecto = date('d', strtotime($edificio[0]['fecha_registro_proyecto']));
                                    $mesFechaRegistroProyecto = date('m', strtotime($edificio[0]['fecha_registro_proyecto']));
                                    $anoFechaRegistroProyecto = date('Y', strtotime($edificio[0]['fecha_registro_proyecto']));
                                    ?>
                                    <select name="dia_fecha_registro_proyecto" id="dia_fecha_registro_proyecto">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($fechaRegistroProyecto != '') {
                                                if ($diaFechaRegistroProyecto == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mes_fecha_registro_proyecto" id="mes_fecha_registro_proyecto">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($fechaRegistroProyecto != '') {
                                                if ($mesFechaRegistroProyecto == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="ano_fecha_registro_proyecto" id="ano_fecha_registro_proyecto">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($fechaRegistroProyecto != '') {
                                                if ($anoFechaRegistroProyecto == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">URA:</td>
                                <td>
                                    <select name="ura" id="ura">
                                        <?php
                                        foreach ($arrMdfs as $mdf) {
                                            $selected = "";
                                            if ($edificio[0]['ura'] == $mdf['mdf']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $mdf['mdf'] ?>" <?php echo $selected ?>><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Sector:</td>
                                <td>
                                    <input type="text" name="sector" id="sector" value="<?php echo $edificio[0]['sector'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Mza. (TdP):</td>
                                <td>
                                    <input type="text" name="mza_tdp" id="mza_tdp" value="<?php echo $edificio[0]['mza_tdp'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Tipo Via:</td>
                                <td>
                                    <select name="idtipo_via" id="idtipo_via">
                                        <?php
                                        foreach ($arrTipoVia as $tipoVia) {
                                            $selected = "";
                                            if ($edificio[0]['idtipo_via'] == $tipoVia['iddescriptor']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $tipoVia['iddescriptor'] ?>" <?php echo $selected ?>><?php echo $tipoVia['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Direcci&oacute;n de Obra (Nombre de Via ):</td>
                                <td>
                                    <input type="text" name="direccion_obra" id="direccion_obra" value="<?php echo $edificio[0]['direccion_obra'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">N&deg;:</td>
                                <td>
                                    <input type="text" name="numero" id="numero" value="<?php echo $edificio[0]['numero'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Mza.:</td>
                                <td>
                                    <input type="text" name="mza" id="mza" value="<?php echo $edificio[0]['mza'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Lote:</td>
                                <td>
                                    <input type="text" name="lote" id="lote" value="<?php echo $edificio[0]['lote'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Tipo CCHH:</td>
                                <td>
                                    <select name="idtipo_cchh" id="idtipo_cchh">
                                        <?php
                                        foreach ($arrTipoCchh as $tipoCchh) {
                                            $selected = "";
                                            if ($edificio[0]['idtipo_cchh'] == $tipoCchh['iddescriptor']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $tipoCchh['iddescriptor'] ?>" <?php echo $selected ?>><?php echo $tipoCchh['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">CCHH:</td>
                                <td>
                                    <input type="text" name="cchh" id="cchh" value="<?php echo $edificio[0]['cchh'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Departamento:</td>
                                <td>
                                    <select name="selDpto" id="selDpto">
                                        <?php
                                        foreach ($arrDpto as $dpto) {
                                            $selected = "";
                                            if (substr($edificio[0]['distrito'], 0, 2) == $dpto['coddpto']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $dpto['coddpto'] ?>" <?php echo $selected ?>><?php echo $dpto['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Provincia:</td>
                                <td>
                                    <div id="divProv">
                                        <select id="selProv">
                                            <?php
                                            foreach ($arrProv as $prov) {
                                                $selected = "";
                                                if (substr($edificio[0]['distrito'], 2, 2) == $prov['codprov']) {
                                                    $selected = "selected";
                                                }
                                                ?>
                                                <option value="<?php echo $prov['codprov'] ?>" <?php echo $selected ?>><?php echo $prov['nombre'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Distrito:</td>
                                <td>
                                    <div id="divDist">
                                        <select id="selDist">
                                            <?php
                                            foreach ($arrDist as $dist) {
                                                $selected = "";
                                                if (substr($edificio[0]['distrito'], 4, 2) == $dist['coddist']) {
                                                    $selected = "selected";
                                                }
                                                ?>
                                                <option value="<?php echo $dist['coddist'] ?>" <?php echo $selected ?>><?php echo $dist['nombre'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Nombre de Constructora:</td>
                                <td>
                                    <input type="text" name="nombre_constructora" id="nombre_constructora" value="<?php echo $edificio[0]['nombre_constructora'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Nombre del proyecto:</td>
                                <td>
                                    <input type="text" name="nombre_proyecto" id="nombre_proyecto" value="<?php echo $edificio[0]['nombre_proyecto'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Tipo Proyecto:</td>
                                <td>
                                    <select name="idtipo_proyecto" id="idtipo_proyecto">
                                        <?php
                                        foreach ($arrTipoProyecto as $tipoProyecto) {
                                            $selected = "";
                                            if ($edificio[0]['idtipo_proyecto'] == $tipoProyecto['iddescriptor']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $tipoProyecto['iddescriptor'] ?>" <?php echo $selected ?>><?php echo $tipoProyecto['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Persona de Contacto:</td>
                                <td>
                                    <input type="text" name="persona_contacto" id="persona_contacto" value="<?php echo $edificio[0]['persona_contacto'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">P&aacute;gina Web:</td>
                                <td>
                                    <input type="text" name="pagina_web" id="pagina_web" value="<?php echo $edificio[0]['pagina_web'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">E-mail:</td>
                                <td>
                                    <input type="text" name="email" id="email" value="<?php echo $edificio[0]['email'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">N&deg; de Blocks:</td>
                                <td>
                                    <select id="nro_blocks" name="nro_blocks" style="width:80px;">
                                        <option value="">Seleccione</option>
                                        <?php
                                        for ($i = 1; $i <= 5; $i++) {
                                            $selected = "";
                                            if ($i == $edificio[0]['nro_blocks']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $selected ?>><?php echo $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo"></td>
                                <td>
                                    <div id="divBlock">
                                        <?php
                                        if ($edificio[0]['nro_blocks'] > 0) {
                                            ?>
                                            <div style="width:180px;">
                                                <span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>
                                                <span style="float:left; width:80px;">Nro. dptos</span>
                                            </div>
                                            <?php
                                            $edificioNumeroBlocks = 5;
                                            if ( $edificio[0]['nro_blocks'] <= 5 ) {
                                                $edificioNumeroBlocks = $edificio[0]['nro_blocks'];
                                            }
                                            for ($i = 1; $i <= $edificioNumeroBlocks; $i++) {
                                                ?>
                                                <div style="width:180px;">
                                                    <span style="float:left; width:20px;">#<?php echo $i ?></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_<?php echo $i ?>" id="nro_pisos_<?php echo $i ?>" size="5" value="<?php echo $edificio[0]['nro_pisos_' . $i . ''] ?>" /></span>
                                                    <span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_<?php echo $i ?>" id="nro_dptos_<?php echo $i ?>" size="5" value="<?php echo $edificio[0]['nro_dptos_' . $i . ''] ?>" /></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Tipo de Infraestructura:</td>
                                <td>
                                    <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                                        <?php
                                        foreach ($arrTipoInfraestructura as $tipoInfraestructura) {
                                            $selected = "";
                                            if ($edificio[0]['idtipo_infraestructura'] == $tipoInfraestructura['iddescriptor']) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?php echo $tipoInfraestructura['iddescriptor'] ?>" <?php echo $selected ?>><?php echo $tipoInfraestructura['nombre'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">N&deg; Dptos Habitados:</td>
                                <td>
                                    <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="<?php echo $edificio[0]['nro_dptos_habitados'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">% Avance:</td>
                                <td>
                                    <input type="text" name="avance" id="avance" value="<?php echo $edificio[0]['avance'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Fecha de Termino Construcci&oacute;n:</td>
                                <td>
                                    <?php
                                    $fechaTerminoConstruccion = ( $edificio[0]['fecha_termino_construccion'] != '' && $edificio[0]['fecha_termino_construccion'] != '0000-00-00') ? date('m/d/Y', strtotime($edificio[0]['fecha_termino_construccion'])) : '';
                                    $diafechaTerminoConstruccion = date('d', strtotime($edificio[0]['fecha_termino_construccion']));
                                    $mesfechaTerminoConstruccion = date('m', strtotime($edificio[0]['fecha_termino_construccion']));
                                    $anofechaTerminoConstruccion = date('Y', strtotime($edificio[0]['fecha_termino_construccion']));
                                    ?>
                                    <select name="diafechaTerminoConstruccion" id="diafechaTerminoConstruccion">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($fechaTerminoConstruccion != '') {
                                                if ($diafechaTerminoConstruccion == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mesfechaTerminoConstruccion" id="mesfechaTerminoConstruccion">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($fechaTerminoConstruccion != '') {
                                                if ($mesfechaTerminoConstruccion == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="anofechaTerminoConstruccion" id="anofechaTerminoConstruccion">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($fechaTerminoConstruccion != '') {
                                                if ($anofechaTerminoConstruccion == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Montante:</td>
                                <td>
                                    <select name="montante" id="montante" style="width:80px;">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $selMontanteS = ( $edificio[0]['montante'] == 'S' ) ? "selected" : "";
                                        $selMontanteN = ( $edificio[0]['montante'] == 'N' ) ? "selected" : "";
                                        ?>
                                        <option value="S" <?php echo $selMontanteS ?>>Si</option>
                                        <option value="N" <?php echo $selMontanteN ?>>No</option>
                                    </select> &nbsp; 

                                    <span class="clsBold">Fecha:</span> 

                                    <?php
                                    $montanteFecha = ( $edificio[0]['montante_fecha'] != '' && $edificio[0]['montante_fecha'] != '0000-00-00' ) ? date('Y-m-d', strtotime($edificio[0]['montante_fecha'])) : '';
                                    $diaMontanteFecha = date('d', strtotime($edificio[0]['montante_fecha']));
                                    $mesMontanteFecha = date('m', strtotime($edificio[0]['montante_fecha']));
                                    $anoMontanteFecha = date('Y', strtotime($edificio[0]['montante_fecha']));
                                    ?>

                                    <select name="diaMontanteFecha" id="diaMontanteFecha">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($montanteFecha != '') {
                                                if ($diaMontanteFecha == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mesMontanteFecha" id="mesMontanteFecha">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($montanteFecha != '') {
                                                if ($mesMontanteFecha == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="anoMontanteFecha" id="anoMontanteFecha">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($montanteFecha != '') {
                                                if ($anoMontanteFecha == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Montante (Observacion):</td>
                                <td>
                                    <textarea name="montante_obs" id="montante_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['montante_obs'] ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Ducteria Interna:</td>
                                <td>
                                    <select name="ducteria_interna" id="ducteria_interna" style="width:80px;">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $selDucteriaS = ( $edificio[0]['ducteria_interna'] == 'S' ) ? "selected" : "";
                                        $selDucteriaN = ( $edificio[0]['ducteria_interna'] == 'N' ) ? "selected" : "";
                                        ?>
                                        <option value="S" <?php echo $selDucteriaS ?>>Si</option>
                                        <option value="N" <?php echo $selDucteriaN ?>>No</option>
                                    </select> &nbsp; 
                                    <span class="clsBold">Fecha:</span> 

                                    <?php
                                    $ducteriaInternaFecha = ( $edificio[0]['ducteria_interna_fecha'] != '' && $edificio[0]['ducteria_interna_fecha'] != '0000-00-00' ) ? date('Y-m-d', strtotime($edificio[0]['ducteria_interna_fecha'])) : '';
                                    $diaDucteriaInternaFecha = date('d', strtotime($edificio[0]['ducteria_interna_fecha']));
                                    $mesDucteriaInternaFecha = date('m', strtotime($edificio[0]['ducteria_interna_fecha']));
                                    $anoDucteriaInternaFecha = date('Y', strtotime($edificio[0]['montante_fecha']));
                                    ?>

                                    <select name="diaDucteriaInternaFecha" id="diaDucteriaInternaFecha">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($ducteriaInternaFecha != '') {
                                                if ($diaDucteriaInternaFecha == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mesDucteriaInternaFecha" id="mesDucteriaInternaFecha">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($ducteriaInternaFecha != '') {
                                                if ($mesDucteriaInternaFecha == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="anoDucteriaInternaFecha" id="anoDucteriaInternaFecha">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($ducteriaInternaFecha != '') {
                                                if ($anoDucteriaInternaFecha == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Ducteria Interna (Observacion):</td>
                                <td>
                                    <textarea name="ducteria_interna_obs" id="ducteria_interna_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['ducteria_interna_obs'] ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Punto de energ&iacute;a:</td>
                                <td>
                                    <select name="punto_energia" id="punto_energia" style="width:80px;">
                                        <option value="">Seleccione</option>
                                        <?php
                                        $selPuntoEnergiaS = ( $edificio[0]['punto_energia'] == 'S' ) ? "selected" : "";
                                        $selPuntoEnergiaN = ( $edificio[0]['punto_energia'] == 'N' ) ? "selected" : "";
                                        ?>
                                        <option value="S" <?php echo $selPuntoEnergiaS ?>>Si</option>
                                        <option value="N" <?php echo $selPuntoEnergiaN ?>>No</option>
                                    </select> &nbsp; 
                                    <span class="clsBold">Fecha:</span> 

                                    <?php
                                    $puntoEnergiaFecha = ( $edificio[0]['punto_energia_fecha'] != '' && $edificio[0]['punto_energia_fecha'] != '0000-00-00' ) ? date('Y-m-d', strtotime($edificio[0]['punto_energia_fecha'])) : '';
                                    $diaPuntoEnergiaFecha = date('d', strtotime($edificio[0]['punto_energia_fecha']));
                                    $mesPuntoEnergiaFecha = date('m', strtotime($edificio[0]['punto_energia_fecha']));
                                    $anoPuntoEnergiaFecha = date('Y', strtotime($edificio[0]['montante_fecha']));
                                    ?>

                                    <select name="diaPuntoEnergiaFecha" id="diaPuntoEnergiaFecha">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($puntoEnergiaFecha != '') {
                                                if ($diaPuntoEnergiaFecha == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mesPuntoEnergiaFecha" id="mesPuntoEnergiaFecha">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($puntoEnergiaFecha != '') {
                                                if ($mesPuntoEnergiaFecha == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="anoPuntoEnergiaFecha" id="anoPuntoEnergiaFecha">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($puntoEnergiaFecha != '') {
                                                if ($anoPuntoEnergiaFecha == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Punto de energia (Observacion):</td>
                                <td>
                                    <textarea name="punto_energia_obs" id="punto_energia_obs" style="width:200px;height:50px;"><?php echo $edificio[0]['punto_energia_obs'] ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Fecha de seguimiento:</td>
                                <td>

                                    <?php
                                    $fechaSeguimiento = ( $edificio[0]['fecha_seguimiento'] != '' && $edificio[0]['fecha_seguimiento'] != '0000-00-00' ) ? date('Y-m-d', strtotime($edificio[0]['fecha_seguimiento'])) : '';
                                    $diaFechaSeguimiento = date('d', strtotime($edificio[0]['fecha_seguimiento']));
                                    $mesFechaSeguimiento = date('m', strtotime($edificio[0]['fecha_seguimiento']));
                                    $anoFechaSeguimiento = date('Y', strtotime($edificio[0]['fecha_seguimiento']));
                                    ?>

                                    <select name="diaFechaSeguimiento" id="diaFechaSeguimiento">
                                        <option value="">Dia</option>
                                        <?php
                                        for ($i = 1; $i <= 31; $i++) {
                                            $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                                            $diaSel = "";
                                            if ($fechaSeguimiento != '') {
                                                if ($diaFechaSeguimiento == $mI) {
                                                    $diaSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $mI ?>" <?php echo $diaSel ?>><?php echo $mI ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="mesFechaSeguimiento" id="mesFechaSeguimiento">
                                        <option value="">Mes</option>
                                        <?php
                                        foreach ($arrMeses as $idmes => $mes) {
                                            $mesSel = "";
                                            if ($fechaSeguimiento != '') {
                                                if ($mesFechaSeguimiento == $idmes) {
                                                    $mesSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $idmes ?>" <?php echo $mesSel ?>><?php echo $mes ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                    <select name="anoFechaSeguimiento" id="anoFechaSeguimiento">
                                        <option value="">A&ntilde;o</option>
                                        <?php
                                        for ($i = $anoActual - 20; $i <= $anoActual + 10; $i++) {
                                            $anoSel = "";
                                            if ($fechaSeguimiento != '') {
                                                if ($anoFechaSeguimiento == $i) {
                                                    $anoSel = "selected";
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>" <?php echo $anoSel ?>><?php echo $i ?></option>
                                        <?php 
                                        } 
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Observaci&oacute;n:</td>
                                <td>
                                    <textarea name="observacion" id="observacion" style="width:200px;height:50px;"><?php echo $edificio[0]['observacion'] ?></textarea>
                                </td>
                            </tr>
                        </table>


                        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                            <tr>
                                <td colspan="2" height="20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="margin: 5px 0; display: none;" id="msg">
                                        <img alt="" src="../../img/loading.gif"> <b>Grabando.</b>
                                    </div>

                                    <input type="submit" name="GrabarEdificio" id="GrabarEdificio" class="submit" value="Grabar Edificio" />
                                    <input type="button" id="Cancelar" class="submit" value="Cancelar" onclick="javascript: history.back();" />
                                </td>
                            </tr>
                        </table>


                        <?php
                    } elseif ($_REQUEST['capa'] == 'com') {
                        ?>


                        <table id="tblPrincipal" class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
                            <tr>
                                <td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Competencia: Informaci&oacute;n principal</td>
                            </tr>
                            <tr>
                                <td width="150" class="compSubTitulo">URA:</td>
                                <td>
                                    <select name="mdf" id="mdf">
<?php
foreach ($arrMdfs as $mdf) {
    $selected = "";
    if ($competencia[0]['ura'] == $mdf['mdf']) {
        $selected = "selected";
    }
?>
                                            <option value="<?php echo $mdf['mdf'] ?>" <?php echo $selected ?>><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre'] ?></option>
<?php
}
?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Direcci&oacute;n:</td>
                                <td>
                                    <input type="text" name="direccion" id="direccion" value="<?php echo $competencia[0]['direccion'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Competencia:</td>
                                <td>
                                    <select name="competencia" id="competencia">
<?php
foreach ($arrCompetencia as $compete) {
    $selectedComp = "";
    if ($competencia[0]['competencia'] == $compete['iddescriptor']) {
        $selectedComp = "selected";
    }
?>
                                            <option value="<?php echo $compete['iddescriptor'] ?>" <?php echo $selectedComp ?> ><?php echo $compete['nombre'] ?></option>
<?php
}
?>
                                    </select> (*)
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Otro:</td>
                                <td>
                                    <input type="text" name="competencia_otro" id="competencia_otro" value="<?php echo $competencia[0]['competencia_otro'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Elemento Encontrado:</td>
                                <td>
                                    <select name="elementoencontrado" id="elementoencontrado">
                                        <option value="">Seleccione</option>
<?php
foreach ($arrElementoEncontrado as $elementoEncontrado) {
    $selectedElem = "";
    //42 = Ninguno - Elemento Encontrado
    if ($competencia[0]['elementoencontrado'] == $elementoEncontrado['iddescriptor']) {
        $selectedElem = "selected";
    }
?>
                                            <option value="<?php echo $elementoEncontrado['iddescriptor'] ?>" <?php echo $selectedElem ?>><?php echo $elementoEncontrado['nombre'] ?></option>
<?php
}
?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Otro:</td>
                                <td>
                                    <input type="text" name="elementoencontrado_otro" id="elementoencontrado_otro" value="<?php echo $competencia[0]['elementoencontrado_otro'] ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="compSubTitulo">Observaci&oacute;n:</td>
                                <td>
                                    <textarea name="obs" id="obs" style="width:200px;height:50px;"><?php echo $competencia[0]['observacion'] ?></textarea>
                                </td>
                            </tr>
                        </table>

                        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
                            <tr>
                                <td colspan="2" height="20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="margin: 5px 0; display: none;" id="msg">
                                        <img alt="" src="../../img/loading.gif"> <b>Grabando.</b>
                                    </div>

                                    <input type="button" name="GrabarCompetencia" id="GrabarCompetencia" class="submit" value="Grabar Competencia" />
                                    <input type="button" id="Cancelar" class="submit" value="Cancelar" onclick="javascript: history.back();" />
                                </td>
                            </tr>
                        </table>

                    <?php
                    }
                    ?>

                </td>
            </tr>
        </table>



        <div id="divMsgFormCapa" style="margin: 10px 0;"></div>

        <input type="hidden" name="idedificio" id="idedificio" value="<?php echo (isset($_REQUEST['idedificio'])) ? $_REQUEST['idedificio'] : ''; ?>" />
        <input type="hidden" name="idcompetencia" id="idcompetencia" value="<?php echo (isset($_REQUEST['idcompetencia'])) ? $_REQUEST['idcompetencia'] : ''; ?>" />

    </body>
</html>