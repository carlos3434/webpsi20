<style type="text/css">
em {
	color: #FF0000;
	margin-left: 4px;
}
#tbl_detalle td {
    padding: 3px;
}
#tb_resultado .tr_selected .celdaX {
	background-color: #C4F4FF;
}
table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}
</style>

<script>
$(document).ready(function() {
    $("#fecha_fin_real_solucion").datetimepicker({
        yearRange:'-1:+1',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy'
    });
});
</script>


<div id="content_pirata_fotos">

	<?php
	if( !empty($registro_trabajo) ) {
            $flag_pendiente = 0;
            $readonly_comentarios = 'readonly="readonly"';
            if( $registro_trabajo[0]['estado'] == 'PENDIENTE' ) { 
                $flag_pendiente = 1;
                $readonly_comentarios = '';
            }
            
            /*$elemento = '';
            if($registro_trabajo[0]['codplantaptr'] == 'B'){ //STB/ADSL

                if( $registro_trabajo[0]['tipored'] == 'D' ) {
                    if( $registro_trabajo[0]['codelementoptr'] == '19' ) { //Cable Alimentador
                        $elemento = $registro_trabajo[0]['ccable'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '3' ) { //terminal
                        $elemento = $registro_trabajo[0]['terminal'];
                    }
                }else if( $registro_trabajo[0]['tipored'] == 'F' ) {
                    if( $registro_trabajo[0]['codelementoptr'] == '19' ) { //Cable Alimentador
                        $elemento = $registro_trabajo[0]['parpri'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '8' ) { //armario
                        $elemento = $registro_trabajo[0]['carmario'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '23' ) { //cable secundario
                        $elemento = $registro_trabajo[0]['cbloque'];
                    }elseif( $registro_trabajo[0]['codelementoptr'] == '3' ) { //terminal
                        $elemento = $registro_trabajo[0]['terminal'];
                    }
                }

            }else if($registro_trabajo[0]['codplantaptr'] == 'C'){ //CATV

                if( $registro_trabajo[0]['codelementoptr'] == '11' ) { //troba
                    $elemento = $registro_trabajo[0]['carmario'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '2' ) { //amplificador
                    $elemento = $registro_trabajo[0]['cbloque'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '1' ) { //tap
                    $elemento = $registro_trabajo[0]['terminal'];
                }elseif( $registro_trabajo[0]['codelementoptr'] == '20' ) { //borne
                    $elemento = $registro_trabajo[0]['borne'];
                }
            }*/
            
            $elemento = obtenerElementoRegistroTrabajoFFTT($registro_trabajo[0]);
            
	?>

	<div id="cont_actu_detalle">
		<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
		<tbody>
		<tr>
                    <td class="da_titulo_gest" colspan="4">DATOS DEL TRABAJO: RTP-<?php echo $registro_trabajo[0]['id_registro_trabajo_planta'] ?></td>
		</tr>
                <tr style="text-align: left;">
                    <td width="25%" class="da_subtitulo">CASUISTICA</td>
                    <td width="75%" colspan="3" style="color: #ff0000; font-size:12px;font-weight:bold;"><?php echo strtoupper($registro_trabajo[0]['casuistica'])?></td>
		</tr>
		<tr style="text-align: left;">
                    <td width="25%" class="da_subtitulo">ZONAL</td>
                    <td width="25%"><?php echo strtoupper($registro_trabajo[0]['zonal'])?></td>
                    <td width="25%" class="da_subtitulo">JEFATURA</td>
                    <td width="25%"><?php echo strtoupper($registro_trabajo[0]['jefatura_desc'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">NEGOCIO</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['negocio'])?></td>
                    <td class="da_subtitulo">MDF</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['mdf'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">ELEMENTO DE RED</td>
                    <td><?php echo strtoupper($registro_trabajo[0]['descripcion'])?></td>
                    <td class="da_subtitulo">ELEMENTO</td>
                    <td><?php echo strtoupper($elemento)?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">ESTADO ACTUAL</td>
                    <td colspan="3" style="color: #ff0000; font-size:12px;font-weight:bold;"><?php echo strtoupper($registro_trabajo[0]['estado'])?></td>
		</tr>
                <tr style="text-align: left;">
                    <td class="da_subtitulo">FECHA INICIO TRABAJO</td>
                    <td>
                        <input type="text" name="fecha_inicio_trabajo" id="fecha_inicio_trabajo" value="<?php echo obtenerFecha($registro_trabajo[0]['fecha_inicio_trabajo'])?>" readonly="readonly" size="15"/>
                    </td>
                    <td class="da_subtitulo">FECHA FIN TRABAJO</td>
                    <td>
                        <input type="text" name="fecha_fin_trabajo" id="fecha_fin_trabajo" value="<?php echo obtenerFecha($registro_trabajo[0]['fecha_fin_trabajo'])?>" readonly="readonly" size="15"/>
                    </td>
		</tr>
                </tbody>
		</table>
                <br />
            
                <table width="100%">
		<tbody>
		<tr>
                    <td align="left" width="30%">
                        <input type="button" name="Grabar" id="RegistrarCambioFecha" value="Grabar" onclick="RegistrarCambioFecha()" class="boton">
                        <input type="button" name="Cancelar" id="Cancelar" value="Cancelar" class="boton" onclick="cerrarVentana()">
                        <input type="hidden" id="id_registro_trabajo_planta" value="<?php echo $registro_trabajo[0]['id_registro_trabajo_planta']?>">
                    </td>
                    <td width="70%">
                        <div id="box_loading" style="float:left; margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none;">procesando. <img src="img/loading.gif" alt="" title="" /></div>
                    </td>
		</tr>
                </tbody>
                </table>
	</div>
        <?php
        }
        ?>

</div>

<script type="text/javascript">
$(document).ready(function(){
    //$("#fecha_inicio_trabajo, #fecha_fin_trabajo").datepicker({
    var dates = $("#fecha_inicio_trabajo, #fecha_fin_trabajo").datetimepicker({
        yearRange:'-1:+1',
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy',
        onSelect: function(selectedDate) {
                var option = this.id == "fecha_inicio_trabajo" ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
        }
    });

});
</script>
