<style>
em {
    color: #ff0000;
}
.limpiar_fecha {
    color: #0000FF !important;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}


table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

.da_editar {
    width: 90%;
}

</style>

<script type="text/javascript">
var idedificio = <?php echo $arrObjEdificio[0]->__get('_idEdificio')?>;
var x = <?php echo ($arrObjEdificio[0]->__get('_x') != '') ? $arrObjEdificio[0]->__get('_x') : '""'?>;
var y = <?php echo ($arrObjEdificio[0]->__get('_y') != '') ? $arrObjEdificio[0]->__get('_y') : '""'?>;
$(document).ready(function () {
    $("#tabsDetalle").tabs();
    
    $("#tabMapaEdificio").click(function() {
        $("#tdMapaEdificio").empty();
        
        $("#tdMapaEdificio").html('<iframe id="ifrMapaPirata" name="ifrMapaPirata" src="modulos/maps/vistas/v_bandeja.control.edificio.mapa.php?idedificio=' + idedificio + '" width="100%" height="400"></iframe>');

    });
});
</script>

<div id="content_pirata_detalle">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td valign="top" style="padding-top: 5px; padding-left: 5px;">

            <div id="tabsDetalle" style="width:99%;">
                <ul>
                    <li><a id="tabDetalleEdificio" href="#cont_edificio_detalle" style="color:#1D87BF; font-weight: 600">Datos del Edificio</a></li>
                    <li><a id="tabMapaEdificio" href="#cont_edificio_mapa" style="color:#1D87BF; font-weight: 600">Mapa de Ubicaci&oacute;n</a></li>
                </ul>

                <div id="cont_edificio_detalle" style="margin: 0 0 15px 0;">
                    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
                    <tbody>
                    <tr>
                        <td class="da_titulo_gest" colspan="6">INFORMACI&Oacute;N EDIFICIO</td>
                    </tr>
                    <tr style="text-align: left;">
                        <td width="18%" class="da_subtitulo">ITEM</td>
                        <td width="32%" class="da_import"><?php echo $arrObjEdificio[0]->__get('_item')?></td>
                        <td width="18%" class="da_subtitulo">ESTADO</td>
                        <td width="32%" class="da_import"><?php echo $arrObjEdificio[0]->__get('_desEstado')?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">ZONAL</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_zonal')?></td>
                        <td class="da_subtitulo">URA</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_ura')?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">FECHA REGISTRO PROYECTO</td>
                        <td><?php if($arrObjEdificio[0]->__get('_fechaRegistroProyecto') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaRegistroProyecto'))?></td>
                        <td class="da_subtitulo">SECTOR</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_sector'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">MANZANA (TDP)</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_mzaTdp'))?></td>
                        <td class="da_subtitulo">TIPO V&Iacute;A</td>
                        <td><?php 
                            foreach( $tipoViaList as $tipoVia ) {
                                if( $arrObjEdificio[0]->__get('_idTipoVia') == $tipoVia['iddescriptor'] ) {
                                    echo $tipoVia['nombre'];
                                    break;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
                        <td colspan="3"><?php echo strtoupper($arrObjEdificio[0]->__get('_direccionObra'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">N&Uacute;MERO</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_numero')?></td>
                        <td class="da_subtitulo">MANZANA</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_mza')?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">LOTE</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_lote')?></td>
                        <td class="da_subtitulo">TIPO CCHH</td>
                        <td>
                            <?php 
                            foreach( $tipoCchhList as $tipoCchh ) {
                                if( $arrObjEdificio[0]->__get('_idTipoCchh') == $tipoCchh['iddescriptor'] ) {
                                    echo $tipoCchh['nombre'];
                                    break;
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">CCHH</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_cchh'))?></td>
                        <td class="da_subtitulo">DEPARTAMENTO</td>
                        <td><?php echo $departamento?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">PROVINCIA</td>
                        <td><?php echo $provincia?></td>
                        <td class="da_subtitulo">DISTRITO</td>
                        <td><?php echo $distrito?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">NOMBRE CONSTRUCTORA</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreConstructora'))?></td>
                        <td class="da_subtitulo">NOMBRE PROYECTO</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_nombreProyecto'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">TIPO PROYECTO</td>
                        <td>
                            <?php 
                            foreach( $tipoProyectoList as $tipoProyecto ) {
                                if( $arrObjEdificio[0]->__get('_idTipoProyecto') == $tipoProyecto['iddescriptor'] ) {
                                    echo $tipoProyecto['nombre'];
                                    break;
                                }
                            }
                            ?>
                        </td>
                        <td class="da_subtitulo">PERSONA CONTACTO</td>
                        <td><?php echo strtoupper($arrObjEdificio[0]->__get('_personaContacto'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">P&Aacute;GINA WEB</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_paginaWeb')?></td>
                        <td class="da_subtitulo">EMAIL</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_email')?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo" valign="top">N&Uacute;MERO BLOCKS</td>
                        <td valign="top"><?php echo $arrObjEdificio[0]->__get('_nroBlocks')?></td>
                        <td colspan="2" valign="top">
                            <div id="divBlock">
                                <?php 
                                if( $arrObjEdificio[0]->__get('_nroBlocks') > 0 ) {
                                ?>
                                    <div style="width:190px;">
                                        <span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px;">Nro. pisos</span>
                                        <span style="float:left; width:80px;">Nro. dptos</span>
                                    </div>
                                    <?php 
                                    $numBlocks = $numeroBlocks;
                                    if( $arrObjEdificio[0]->__get('_nroBlocks') < $numeroBlocks ) {
                                        $numBlocks = $arrObjEdificio[0]->__get('_nroBlocks');
                                    }
                                    for($i=1; $i<=$numBlocks; $i++) {
                                    ?>
                                        <div style="width:190px;">
                                            <span style="float:left; width:20px;">#<?php echo $i?></span>
                                            <span style="float:left; width:80px; padding:2px 0;">
                                                &nbsp;<?php echo $arrObjEdificio[0]->__get('_nroPisos' . $i)?>
                                            </span>
                                            <span style="float:left;">&nbsp;</span>
                                            <span style="float:left; width:80px; padding:2px 0;">
                                                &nbsp;<?php echo $arrObjEdificio[0]->__get('_nroDptos' . $i)?>
                                            </span>
                                        </div>
                                <?php 
                                    }
                                }
                            ?>
                            </div>
                        </td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">TIPO INFRAESTRUCTURA</td>
                        <td>
                            <?php 
                            foreach( $tipoInfraestructuraList as $tipoInfraestructura ) {
                                if( $arrObjEdificio[0]->__get('_idTipoInfraestructura') == $tipoInfraestructura['iddescriptor'] ) {
                                    echo $tipoInfraestructura['nombre'];
                                    break;
                                }
                            }
                            ?>
                        </td>
                        <td class="da_subtitulo">NRO. DEPARTAMENTOS HABITADOS</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_nroDptosHabitados')?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">% AVANCE</td>
                        <td><?php echo $arrObjEdificio[0]->__get('_avance')?></td>
                        <td class="da_subtitulo">FECHA TERMINO CONSTRUCCI&Oacute;N</td>
                        <td><?php if($arrObjEdificio[0]->__get('_fechaTerminoConstruccion') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaTerminoConstruccion'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">MONTANTE</td>
                        <td>
                            <?php
                            echo ( $arrObjEdificio[0]->__get('_montante') == 'S' ) ? "SI" : "NO";
                            ?>
                            &nbsp; 
                            <span style="background-color: #E1E2E2;color: #000000;font-size: 10px;font-weight: bold;padding:2px;">FECHA</span>: <?php if($arrObjEdificio[0]->__get('_montanteFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_montanteFecha'))?> 
                        </td>
                        <td class="da_subtitulo">MONTANTE (OBS)</td>
                        <td><?php echo nl2br($arrObjEdificio[0]->__get('_montanteObs'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">DUCTERIA INTERNA</td>
                        <td>
                            <?php
                            echo ( $arrObjEdificio[0]->__get('_ducteriaInterna') == 'S' ) ? "SI" : "NO";
                            ?>
                            &nbsp; 
                            <span style="background-color: #E1E2E2;color: #000000;font-size: 10px;font-weight: bold;padding:2px;">FECHA</span>: <?php if($arrObjEdificio[0]->__get('_ducteriaInternaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_ducteriaInternaFecha'))?>
                        </td>
                        <td class="da_subtitulo">DUCTERIA INTERNA (OBS)</td>
                        <td><?php echo nl2br($arrObjEdificio[0]->__get('_ducteriaInternaObs'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">PUNTO DE ENERGIA</td>
                        <td>
                            <?php
                            echo ( $arrObjEdificio[0]->__get('_puntoEnergia') == 'S' ) ? "SI" : "NO";
                            ?>
                            &nbsp; 
                            <span style="background-color: #E1E2E2;color: #000000;font-size: 10px;font-weight: bold;padding:2px;">FECHA</span>: <?php if($arrObjEdificio[0]->__get('_puntoEnergiaFecha') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_puntoEnergiaFecha'))?>
                        </td>
                        <td class="da_subtitulo">PUNTO DE ENERGIA (OBS)</td>
                        <td><?php echo nl2br($arrObjEdificio[0]->__get('_puntoEnergiaObs'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">FECHA DE SEGUIMIENTO</td>
                        <td colspan="3"><?php if($arrObjEdificio[0]->__get('_fechaSeguimiento') != '0000-00-00') echo obtenerFecha($arrObjEdificio[0]->__get('_fechaSeguimiento'))?></td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
                        <td colspan="3"><?php echo nl2br($arrObjEdificio[0]->__get('_observacion'))?></td>
                    </tr>
                    </table>

                    <div style="height:5px;"></div>

                </div>
                
                
                <div id="cont_edificio_mapa">
                    <table id="tbl_detalle_mapa" class="tb_detalle_actu" width="100%">
                    <tbody>
                    <tr>
                        <td class="da_titulo_gest" colspan="6">MAPA DE UBICACI&Oacute;N</td>
                    </tr>
                    <tr>
                        <td id="tdMapaEdificio">
                            <!--Dibuja el mapa con el Edificio-->

                        </td>
                    </tr>
                    </table>
                </div>
                
            </div>
        </td>
    </tr>
    </table>
</div>