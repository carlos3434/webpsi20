<div id="content_pirata_asociar">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">			
			<input type="hidden" id="idcomponente" name="idcomponente" value="<?php echo $idComponente; ?>"/>
			<div id="cont_actu_detalle">
				<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
				<tbody>
				<tr>
					<td class="da_titulo_gest" colspan="6">DATOS DEL PIRATA</td>
				</tr>
				<tr style="text-align: left;">
					<td width="15%" class="da_subtitulo">DIRECCI&Oacute;N ANTENA</td>
					<td width="35%"><?php echo strtoupper($arrObjPirata[0]->__get('_direccion')) ?></td>
					<td width="10%" class="da_subtitulo">TIPO ANTENA</td>
                    <td width="15%" class="da_import"><?php echo strtoupper($arrObjPirata[0]->__get('_tipo')) ?></td>
					<td width="10%" class="da_subtitulo">FECHA DETECCI&Oacute;N</td>
					<td width="15%"><?php echo obtenerFecha($arrObjPirata[0]->__get('_fechaInsert')) ?></td>
				</tr>
				</table>
			</div>							
			<div style="margin: 15px 0 5px 0; width:100%; float:right;">
				<div style="float:left; margin:7px 0 0 0;">
					<b>CLIENTES ASOCIADOS AL PIRATA:</b>
				</div>
			</div>
			<div style="width:100%; overflow-y:scroll; height:75px;">
				<table id="tb_resultado_clientes_asociados" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%; font-size:10px;">
				<thead>
					<tr style="line-height:13px;">
						<th class="headTbl" width="2%">#</th>
						<th class="headTbl" width="3%">Opc</th>
						<th class="headTbl" width="30%">Cliente</th>
						<th class="headTbl" width="40%">Direccion</th>
						<th class="headTbl" width="10%">Telefono</th>
						<th class="headTbl" width="15%">Producto</th>
					</tr>
				</thead>
				<tbody>
				<?php
                if ( !empty($arrClientesAsociados) ) {
                    $itemA = 1;
                    foreach ($arrClientesAsociados as $objDunaAsociado) {
                ?>
					<tr>
					<td class="celdaX"><?php echo $itemA++; ?></td>
					<td class="celdaX">
                        <a href="javascript:void(0)" onclick="eliminarClienteAsociado('<?php echo $objDunaAsociado->__get('_idDunaAsociado')?>','<?php echo $idComponente?>')" title="Eliminar cliente asociado">
                            <img width="11" height="11" title="Eliminar cliente asociado" src="img/delete_offline.png" style="margin:3px;" />
                        </a>
					</td>
					<td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_titular'); ?></td>
					<td class="celdaX" align="left"><?php echo $objDunaAsociado->__get('_direccionTelefono'); ?></td>
					<td class="celdaX"><?php echo $objDunaAsociado->__get('_telefono'); ?></td>
					<td class="celdaX"><?php echo $objDunaAsociado->__get('_speedyContratado'); ?></td>
					</tr>
				<?php
                    }
                }
				?>
				</tbody>
				</table>
			</div>
			<?php 
            if ( !empty($arrClientesSpeedy) ) {
            ?>
			<div style="margin: 15px 0 0 0; width:100%; float:right;">
				<div style="float:left; margin:7px 0 0 0;">
                    <b>CLIENTES CERCANOS A LA ANTENA DETECTADA:</b> 
                    &nbsp; (<span style="font-weight:bold;" id="spnNumClientes">
                    <?php echo count($arrClientesSpeedy)?></span> 
                    registros encontrados)
				</div>
			</div>
            <div style="margin: 0 0 5px 0; width:100%; float:right;">
                <div style="float:left; margin:7px 0 0 0;">
                    <input type="radio" id="tipo_listado" name="tipo_listado" onclick="listadoClientesCercanos('trm','<?php echo $idComponente;?>')" value="trm" checked="checked"> Vista Terminales Cercanos &nbsp; 
                    <input type="radio" id="tipo_listado" name="tipo_listado" onclick="listadoClientesCercanos('arm','<?php echo $idComponente;?>')" value="arm"> Vista Armarios Cercanos &nbsp; | &nbsp; 
                    Cantidad de terminales <select id="cant_terminales" onchange="cantidadTerminales()">
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                </div>
                <div id="loading_clientes" style="float:left; margin:12px 0 0 20px;display:none;">
                   <img alt="" src="img/loading.gif"> cargando...
                </div>
                <div style="float:right;">
                    <?php
                    $disabledNoAsociado = 'disabled="disabled"';
                    if ( empty($arrClientesAsociados) ) {
                        $disabledNoAsociado = '';
                    }   
                    ?>
                    <input type="button" <?php echo $disabledNoAsociado?> style="padding:3px 5px;font-weight:bold;" title="Marcar como No Asociado" value="No Asociado" id="NoasociarPirataAbonado" onclick="NoasociarPirataAbonado();" />
                </div>
			</div>			
			<div id="divClientesCercanos" style="width:100%; overflow:auto; height:200px;">			
				<table id="tb_resultado_clientes" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%; font-size:10px;">
				<thead>
					<tr style="line-height:13px;">
						<th class="headTbl" width="2%">#</th>
						<th class="headTbl" width="3%">Opc</th>
						<th class="headTbl" width="25%">Cliente</th>
						<th class="headTbl" width="30%">Direccion</th>
						<th class="headTbl" width="7%">Telefono</th>
                        <th class="headTbl" width="10%">FFTT</th>
						<th class="headTbl" width="15%">Producto</th>
                        <th class="headTbl" width="8%"><a href="javascript:void(0);" onclick="ordenaClientesCercanos('distancia', 'asc')" style="color:#0086C3;">Distancia</a></th>
					</tr>
				</thead>
				<?php
                if ( !empty($arrClientesSpeedy) ) {
                ?>
				<tbody>
				    <?php
                    $item = 1;
                    foreach ($arrClientesSpeedy as $clienteSpeedy) {
                        $flagExists = 0;
                        if ( !empty($arrClientesAsociados) ) {
                            if ( array_key_exists($clienteSpeedy['ddn_telefono'], $arrClientesAsociados) ) {
                                $flagExists = 1;
                            }
                        }

                        $cableArmario = ($clienteSpeedy['tipored'] == 'F') ? $clienteSpeedy['Armario'] : $clienteSpeedy['Cable'];

                    ?>
					<tr <?php if($flagExists) echo 'class="tr_selected"'; ?> id="row_<?php echo $clienteSpeedy['ddn_telefono']?>|<?php echo $clienteSpeedy['tipored']?>">
					<td class="celdaX"><?php echo $item++; ?></td>
					<td class="celdaX">
                    <?php
                    if (!$flagExists) {
                    ?>
                    <a href="javascript:void(0)" onclick="asociarPirataAbonado('<?php echo $idComponente?>','<?php echo $clienteSpeedy['tipored']?>','<?php echo $clienteSpeedy['ddn_telefono']?>')" title="Asociar cliente al Pirata">
                        <img width="13" height="13" title="Asociar cliente al Pirata" src="img/btn_enter_data.png">
                    </a>
                    <?php
                    } else {
                    ?>
                        <img width="13" height="13" title="Cliente ya asociado al Pirata" src="img/btn_enter_data_disabled.png">
                    <?php
                    }
                    ?>
					</td>
					<td class="celdaX" align="left"><?php echo $clienteSpeedy['Cliente']; ?></td>
					<td class="celdaX" align="left"><?php echo $clienteSpeedy['Direccion']; ?></td>
					<td class="celdaX"><?php echo $clienteSpeedy['ddn_telefono']; ?></td>
                    <td class="celdaX"><?php echo $clienteSpeedy['MDF'] . ' - ' . $cableArmario . ' - ' . $clienteSpeedy['Caja']; ?></td>
					<td class="celdaX"><?php echo $clienteSpeedy['PRODUCTO']; ?></td>
                    <td class="celdaX"><?php echo $clienteSpeedy['distancia'] . ' m.'; ?></td>
					</tr>
			     <?php
                    }
                ?>
				</tbody>
				<?php
                }
                ?>
				</table>
			</div>
            <?php
            }
            ?>
            <div style="margin: 20px 0 0 0; display: none;" id="msg">
                <img alt="" src="img/loading.gif"> <b>Procesando cambios.</b>
            </div>
		</td>
	</tr>
	</table>
	<br /><br />
</div>
<script>
    var foto_pirata = <?php echo json_encode(array('foto'=>trim($arrObjPirata[0]->__get('_foto1'))));?>;
</script>
<div id="content_pirata_asociar_form" style="display: none;">
    <div id="childModalUnder" style="padding: 10px; background: #fff;"></div>
</div>
