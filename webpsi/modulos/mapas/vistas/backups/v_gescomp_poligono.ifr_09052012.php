<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>


        <script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
        <script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>


        <script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.filter.js"></script>

        <script type="text/javascript" src="../../../modulos/maps/js/edificios.fftt.js"></script>
        <script type="text/javascript" src="../../../js/jquery/actuaciones.js"></script>

        <link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../css/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" media="all"/>
        <script type="text/javascript">
            $(function(){
                $("select.clsMultiple").multiselect().multiselectfilter();
            });
        </script>
        <script type="text/javascript">

            var myLayout;
            var map = null;
            var poligonosDibujar_array = [];
            var polilineasDibujar_array = [];

            var rutaDibujar = null;
            var rutaDibujar_array = [];

            var ptos_arr = [];
            var markers_arr = [];
            var makeMarkers_arr = [];

            var x_actual = null;
            var y_actual = null;

            var distancia_ruta = 0;
            
            var capa_exists = 0;
            var activar_ruta = 0;

            var num_trm = 0;
            var comp_trm_arr = [];

            //array que almacena los x,y de los puntos de las rutas 
            var xy_arr = [];

            $(document).ready(function () {

                $("#tabs").tabs();
    
                myLayout = $('body').layout({
                    // enable showOverflow on west-pane so popups will overlap north pane
                    west__showOverflowOnHover: true,
                    west: {size:370}
                });

                validarData=function(e){
                    var tecla = (document.all) ? e.keyCode : e.which;
                    if(tecla==17 || e.ctrlKey){
                        return false;
                    } else {
                        if (tecla==8) return true;
                        //var patron =/\w/;//valida solo letras y numeros
                        var patron = /^[A-Za-z+][\s[A-Za-z]+]*$/;
                        var te = String.fromCharCode(tecla);
                        return patron.test(te);
                    }
                }

                load_array=function(element){
                    var grupo=new Array();
                    $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
                    return grupo;
                }
        
                $('#dpto').change(function(){
                    M.buscarProvincias();
                });
        
                $('#prov').change(function(){
                    M.buscarDistritos();
                });


                agregarTerminalARuta = function(mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, x, y) {
                    M.agregarTerminalARuta(mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, x, y)
                }

                grabarRutaAcometida = function() {
                	if ( confirm("Seguro de grabar los cambios?") ) {
                	    M.grabarRutaAcometida();
                	}
                }

                cargaInicial = function() {
                    data_content = "action=cargaInicial";
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {

                            var tipoPoligonos = datos['arrTipoPoligonos'];
                            var tipoRutas = datos['arrTipoRutas'];
                            var zonales = datos['arrZonales'];
                            var distritos = datos['arrZonales'];
                            var dptos = datos['arrDptos'];
                            var provs = datos['arrProvs'];
                            var dists = datos['arrDists'];
                            var tipoPedidos = datos['arrTipoPedido'];

                            //carga combo de tipo de poligonos
                            var selectTipoPoligono = '';
                            for(var i in tipoPoligonos) {
                                selectTipoPoligono += '<option value="' + tipoPoligonos[i]['idpoligono_tipo'] + '">' + tipoPoligonos[i]['nombre'] + '</option>'
                            }
                            $("#tipocapaPoligono").append(selectTipoPoligono);

                            //carga combo de tipo de rutas
                            var selectTipoRuta = '';
                            for(var i in tipoRutas) {
                                selectTipoRuta += '<option value="' + tipoRutas[i]['idpoligono_tipo'] + '">' + tipoRutas[i]['nombre'] + '</option>'
                            }
                            $("#tipocapaRuta").append(selectTipoRuta);

                            //carga combo de zonales
                            var selectZonales = '';
                            var selZonales = '';
                            for(var i in zonales) {
                                selectZonales += '<option ' + sel + ' value="' + zonales[i]['desc_zonal'] + '">' + zonales[i]['desc_zonal'] + '</option>'
                                selZonales += '<option ' + sel + ' value="' + zonales[i]['abv_zonal'] + '">' + zonales[i]['abv_zonal'] + '-' + zonales[i]['desc_zonal'] + '</option>'
                            }
                            //Agregando listado de zonales 
                            //div direcion 
                            $("#zonal").append(selectZonales);
                            //div Busq. de terminales  
                            $("#selZonal").append(selZonales);

                            //carga combo de distritos
                            var selectDistritos = '';
                            for(var i in distritos) {
                                selectDistritos += '<option value="' + distritos[i]['distrito'] + '">' + distritos[i]['distrito'] + '</option>'
                            }
                            $("#selDistrito").append(selectDistritos);

                            //carga combo de dptos
                            var selectDptos = '';
                            for(var i in dptos) {
                                var sel = '';
                                if ( dptos[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectDptos += '<option ' + sel + ' value="' + dptos[i]['coddpto'] + '">' + dptos[i]['nombre'] + '</option>'
                            }
                            $("#dpto").append(selectDptos);

                            //carga combo de provs
                            var selectProvs = '';
                            for(var i in provs) {
                                var sel = '';
                                if ( provs[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectProvs += '<option ' + sel + ' value="' + provs[i]['codprov'] + '">' + provs[i]['nombre'] + '</option>'
                            }
                            $("#prov").append(selectProvs);

                            //carga combo de provs
                            var selectDists = '';
                            for(var i in dists) {
                                selectDists += '<option value="' + dists[i]['coddist'] + '">' + dists[i]['nombre'] + '</option>'
                            }
                            $("#dist").append(selectDists);

                            //carga combo de tipo pedidos 
                            var selectTipoPedido = '';
                            for(var i in tipoPedidos) {
                            	selectTipoPedido += '<option value="' + tipoPedidos[i]['tipoPedido'] + '">' + tipoPedidos[i]['tipoPedido'] + '</option>'
                            }
                            $("#tipo_pedido").append(selectTipoPedido);

                        }
                    });
                }
        

                btnSearchComponentes = function() {
                    if(parent.$(".capas").is(':checked')) { 
                    } else {  
                        alert("Ingrese algun criterio de busqueda");  
                        return false;  
                    }  

                    var x = parent.$("#x").attr('value');
                    var y = parent.$("#y").attr('value');

                    var query = parent.$("input.capas, select.capas").serializeArray();

                    parent.$("#msgBuscomp").show();
                    data_content = {
                        'action'    : 'search',
                        'x'        : x,
                        'y'        : y,
                        'comp'        : {'trm':'1'}
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "v_buscomp_popup.php?action=search&x=" + x + "&y=" + y,
                        data:     query,
                        dataType: "json",
                        success: function(data) {

                            //clear markers
                            //L.clearMarkers();

                            var sites = data['sites'];
                            capa_ico = data['capa_ico'];

                            infowindow = new google.maps.InfoWindow({
                                content: "loading..."
                            });

                            if( sites.length > 0) {
                                M.setMarkers(map, sites);
                            }


                            //rutas encontradas 
                            var rutas = data['xy_rutas'];

                            if( rutas.length > 0 ) {
                                //pintamos los rutas
                                for (var i in rutas) {
                                    M.loadPolilinea(rutas[i], i);
                                }
                            }

                            parent.$("#msgBuscomp").hide();
                            parent.$("#childModal").dialog('close');

                        }
                    });        
                }


                $("#selZonal").change(function() {
                    IDzon = this.value;
                    if(IDzon == "") {
                        return false;
                    }
                    data_content = "action=buscarMdf&IDzon=" + IDzon;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.terminal.main.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {
                            var selectMdf = '';
                            for(var i in datos) {
                                selectMdf += '<option value="' + datos[i]['IDmdf'] + '">' + datos[i]['IDmdf'] + ' - ' + datos[i]['Descmdf'] + '</option>'
                            }
                              $("#selMdf").html(selectMdf);
                        }
                    });

                    $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
                    $("#selCableArmario").html('<option value="">Seleccione</option>');
                });

                $("#selMdf").change(function() {
                    IDzonal      = $("#selZonal").val();
                    IDmdf        = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    if(IDzonal == "" || IDzon == "" || IDtipred == "") {
                        return false;
                    }
                    $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
                    $("#selCableArmario").html('<option value="">Seleccione</option>');
                });

                $("#selTipRed").change(function() {
                    IDzonal      = $("#selZonal").val();
                    IDmdf        = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    if(IDzonal == "" || IDzon == "" || IDtipred == "") {
                        return false;
                    }
                    data_content = "action=buscarCableArmario&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&IDtipred=" + IDtipred;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.terminal.main.php",
                        data:   data_content,
                        success: function(datos) {
                            $("#divCableArmario").html(datos);
                        }
                    });
                });

                //Activar la creacion de la ruta 
                $("#activar_ruta").click(function() {
                    if($("#activar_ruta").is(':checked')) { 
                        activar_ruta = 1;
                    } else {  
                    	activar_ruta = 0;
                    }
                });

                //Activar el formulario para crear la ruta acometida 
                $("#crear_ruta_acometida").click(function() {
                    if($("#crear_ruta_acometida").is(':checked')) { 

                        if ( x_actual == null && y_actual == null ) {
                        	alert("Debe seleccionar la ubicacion del cliente");
                            return false;
                        }
                    	
                    	if ( xy_arr.length < 2 ) {
                            alert("Debe crear su ruta antes de agregar el terminal.");
                            return false;
                        }
                        
                        $('#divBuscarComponentes').hide();
                        $('#divCrearRutaAcometida').show();
                        $('#xy_cliente').html(x_actual + ',' + y_actual);
                        $('#x_cliente').val(x_actual);
                        $('#y_cliente').val(y_actual);
                    } else {  
                    	$('#divBuscarComponentes').show();
                        $('#divCrearRutaAcometida').hide();
                        $('#xy_cliente').html('');
                        $('#x_cliente').val('');
                        $('#y_cliente').val('');
                    }  
                });


                GrabarRutaAcometida = function() {


                }


                var M = {
                    initialize: function() {

                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com');
                            return false;
                        }
        
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var myOptions = {
                            zoom: 11,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


                        //carga inicial de combos
                        cargaInicial();

                        
                        google.maps.event.addListener(map, "dblclick", function(event) {
                            //clear markers
                            M.clearMarkers();
                            M.clearMakeMarkers(); //wil
                
                            marker = new google.maps.Marker({
                                position: event.latLng,
                                map: map
                            });
                            markers_arr.push(marker);
                            var Coords = event.latLng;
                            var x = Coords.lng();
                            var y = Coords.lat();

                            //seteo los x,y actuales 
                            x_actual = x;
                            y_actual = y;

                            //popup, pregunta si quieres buscar o agregar componente
                            M.popupAddSearchComponentes(x,y);
                        });
                        

                        //Creacion de ruta 
                        google.maps.event.addListener(map, "click", function(event) {

                            if (activar_ruta) {
                            	var Coords = event.latLng;
                                var x = Coords.lng();
                                var y = Coords.lat();
                            
                                xy_arr.push(x + '|' + y);
                                
                                M.clearRutas();
                                M.crearRutaPoligono();

                                distancia_ruta = 0;
                                M.calculaDistanciaRuta();
                            }
                        });
                        
                        google.maps.event.addListener(map, "rightclick", function(event) {

                        	if (activar_ruta) {
                        		xy_arr.pop();
                                
                                M.clearRutas();
                                M.crearRutaPoligono();

                                distancia_ruta = 0;
                                M.calculaDistanciaRuta();
                        	}
                        });
                        //FIN Creacion de ruta 
                        
                    },

                    toRad: function(Value) {
                        /** Converts numeric degrees to radians */
                        return Value * Math.PI / 180;
                    },
                    
                    distanciaEntreDosPuntos: function(lon1, lat1, lon2, lat2) {
                    	var R = 6371 * 1000; // km * 1000 = metros 
                    	var dLat = M.toRad(lat2-lat1);
                    	var dLon = M.toRad(lon2-lon1);
                    	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    	        Math.cos(M.toRad(lat1)) * Math.cos(M.toRad(lat2)) *
                    	        Math.sin(dLon/2) * Math.sin(dLon/2);
                    	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    	var d = R * c;

                    	return d;
                    },

                    calculaDistanciaRuta: function() {
                        
                        if ( xy_arr.length > 1 ) {
                            var xy_array = new Array();

                            for( i in xy_arr ) {

                                var n_i = parseInt(i) + 1;
                            	
                            	if ( xy_arr[n_i] != undefined ) {

                            		var xy_values = new Array();
                                    xy_values = xy_arr[i].split('|');
                                    xy_values2 = xy_arr[n_i].split('|');
                                    
                                    xy_values['x'] = xy_values[0];
                                    xy_values['y'] = xy_values[1];

                                    var d = M.distanciaEntreDosPuntos(xy_values[0], xy_values[1], xy_values2[0], xy_values2[1]);

                                    distancia_ruta = distancia_ruta + d;

                            	}
                            }

                            $('#distancia_ruta').val(distancia_ruta.toFixed(2));
                                
                        } else {
                        	$('#distancia_ruta').val('0.00');
                        }
                    },

                    generaColorPoligono: function() {
                        var valores_arr = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
                        var color = '';
                        for( i=1; i<=6; i++ ) {
                            var rand =  Math.ceil(Math.random()*15);
                            color += valores_arr[rand];
                        }
                        return '#' + color;
                    },

                    clearRutas: function() {
                        for (var n = 0, rutaDibujar; rutaDibujar = rutaDibujar_array[n]; n++) {
                        	rutaDibujar.setMap(null);
                        }
                    },

                    agregarTerminalARuta: function(mtgespktrm, zonal, mdf, cable, armario, caja, tipo_red, x, y) {
                        
                        if( xy_arr.length < 2 ) {
                            alert("Debe crear su ruta antes de agregar el terminal.");
                            return false;
                        }

                        //limpio array donde almaceno los arrays
                        comp_trm_arr = [];
                        num_trm = 0;

                        for( var u=0 in comp_trm_arr ) {
                            if( comp_trm_arr[u] == mtgespktrm ) {
                                alert("Terminal ya agregado");
                                return false;
                            }
                        }
                        num_trm++;
                        comp_trm_arr.push(mtgespktrm);

                        var cableArmario = '';
                        if ( tipo_red == 'D' ) {
                        	cableArmario = cable;
                        } else if ( tipo_red == 'F' ) {
                        	cableArmario = armario;
                        }
                        
                        var content = '';
                        content += '<tr>';
                        content += '<td>' + num_trm + '</td>';
                        content += '<td>' + zonal + ' - ' + mdf + ' - ' + cableArmario + ' - ' + caja + ' - ' + tipo_red + '</td>';
                        content += '</tr>';

                        //seteando las fftt 
                        $('#a_zonal').val(zonal);
                        $('#a_mdf').val(mdf);
                        $('#a_cable').val(cable);
                        $('#a_armario').val(armario);
                        $('#a_caja').val(caja);
                        $('#a_tipo_red').val(tipo_red);
                        $('#a_terminal_x').val(x);
                        $('#a_terminal_y').val(y);

                        //$("#tb_resultado_trm tbody").append(content);
                        $("#tb_resultado_trm tbody").html(content);

                    },
                    
                    loadRuta: function(xy_array) {

                        var datos           = xy_array['coords'];
                        var color_polilinea = xy_array['color'];
                        var polilineaCoords = [];

                        if ( datos.length > 0) {
                            for (var i in datos) {
                                var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                polilineaCoords.push(pto_polilinea);
                            }

                            // Construct the polyline                
                            polilineaDibujar = new google.maps.Polyline({
                                path: polilineaCoords,
                                strokeColor: color_polilinea,
                                strokeOpacity: 1.0,
                                strokeWeight: 4
                            });

                            //agrego al array las capas seleccionadas
                            rutaDibujar_array.push(polilineaDibujar);

                            polilineaDibujar.setMap(map);                
                        }    
                    },
                    
                    crearRutaPoligono: function() {
                        
                        if ( xy_arr.length > 0 ) {
                            var xy_array = new Array();

                            for( i in xy_arr ) {
                                var xy_values = new Array();
                                xy_values = xy_arr[i].split('|');
                                
                                xy_values['x'] = xy_values[0];
                                xy_values['y'] = xy_values[1];
                                
                                xy_array.push(xy_values);
                            }
                            
                            var xy_total_array = new Array();
                            
                            xy_total_array['coords'] = xy_array;
                            //alert(M.generaColorPoligono());
                            xy_total_array['color'] = M.generaColorPoligono();

                            
                            M.loadRuta(xy_total_array);
                                
                        }
                        else {
                            alert('No existen puntos seleccionados para crear la ruta');
                        }
                    
                    },


                    grabarRutaAcometida: function() {
                        var data_content = {
                        	cliente_x                   : $("#x_cliente").val(),
                        	cliente_y                   : $("#y_cliente").val(),
                            zonal                       : $("#a_zonal").val(),
                	    	mdf                         : $("#a_mdf").val(),
                	    	cable                       : $("#a_cable").val(),
                	    	armario                     : $("#a_armario").val(),
                	    	terminal                    : $("#a_caja").val(),
                	    	tipo_red                    : $("#a_tipo_red").val(),
                	    	terminal_x                  : $("#a_terminal_x").val(),
                	    	terminal_y                  : $("#a_terminal_y").val(),
                	    	ruta                        : xy_arr,
                	    	distancia_acometida         : $("#distancia_ruta").val(),
                	    	tipo_pedido                 : $("#tipo_pedido").val(),
                	    	pedido                      : $("#pedido").val(),
                        };

                        //validando que exista la ruta
                        if ( xy_arr.length < 2 ) {
                        	alert("No tiene ingresada una ruta");
                            return false;
                        }
                        if( data_content['cliente_x'] == '' && data_content['cliente_y'] == '' ) {
                            alert("No se ha registrado el x,y del cliente");
                            return false;
                        }
                        if( data_content['terminal'] == '' ) {
                            alert("No se ha registrado el terminal");
                            return false;
                        }
                        if( data_content['distancia_acometida'] == '' ) {
                            alert("No se ha registrado la distancia de la ruta");
                            return false;
                        }
                        if( data_content['tipo_pedido'] == '' ) {
                            alert("Ingrese el tipo pedido");
                            return false;
                        }
                        if( data_content['pedido'] == '' ) {
                            alert("Ingrese el pedido");
                            return false;
                        }
                        
                        
                        $("#box_loading2").show();
                        $("input[id=btnGrabarRutaAcometida]").attr('disabled','disabled');
                        pr = $.post("../gescomp.poligono.php?action=grabarRutaAcometida", data_content, function(data){
                            
                            if (data['flag'] == '1'){
                                alert(data['mensaje'] + "\n\nCodigo: " + data['codGen']);
                                
                                $('#divBuscarComponentes').show();
                                $('#divCrearRutaAcometida').hide();

                                //limpiando las variables 
                                $('.clsAcometida').val('');
                                x_actual = null;
                                y_actual = null;
                                xy_arr = [];
                                activar_ruta = 0;

                                //limpiando la ruta 
                                M.clearRutas();

                                //limpiando los markers 
                                M.clearMarkers();

                                //desactivo los checked de 
                                $('#activar_ruta').attr('checked', false);
                                $('#crear_ruta_acometida').attr('checked', false);

                            } else if(data['flag'] == '2') {
                            	alert(data['mensaje']);
                            } else if(data['flag'] == '3') {
                            	alert(data['mensaje']);
                            } else{
                                alert(data['mensaje']);
                            }
                            
                            $("#box_loading2").hide();
                            $("input[id=btnGrabarRutaAcometida]").attr('disabled','');
                        },'json');

                    },

                    setMarkers: function(map, markers) {

                        var avg = {
                            lat: 0,
                            lng: 0
                        };
    
                        for (var i = 0; i < markers.length; i++) {
                            var sites = markers[i];
                            var siteLatLng = new google.maps.LatLng(sites['y'], sites['x']);

                            var ico = capa_ico[sites['tip']];


                            var icono = '';
                            var est_caja = '';
                            if( sites['tip'] == 'trm' ) {
                                est_caja = sites['colorTrm'];
                                icono = "../../../img/map/terminal/trm_" + sites['title'] + "--" + est_caja + ".gif";
                            }else if( sites['tip'] == 'exa' ) {
                                icono = "../../../img/map/posiblesclientes/posiblecliente-3.gif";
                            }else if( sites['tip'] == 'arm' ) {
                                icono = "../../../img/map/armario/arm_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'mdf' ) {
                                icono = "../../../img/map/mdf/mdf_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'trb' ) {
                                icono = "../../../img/map/troba/trb-r.gif";
                            }else if( sites['tip'] == 'tap' ) {
                                icono = "../../../img/map/tap/tap_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'edi' ) {
                                icono = "../../../img/map/edificio/edificio-" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'com' ) {
                                icono = "../../../img/map/competencia/" + ico;
                            }else if( sites['tip'] == 'esb' ) {
                                icono = "../../../img/map/estacionbase/esb-1.gif";
                            }else if( sites['tip'] == 'pcl' ) {
                                icono = "../../../img/map/posiblesclientes/posiblecliente-3.gif";
                            }else if( sites['tip'] == 'loc' ) {
                                icono = "../../../img/map/localidades/localidades.png";
                            }else if( sites['tip'] == 'tpi' ) {
                                icono = "../../../img/map/tpi/tpi-" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'dad' ) {
                                icono = "../../../img/map/dunaadsl/pirata-1.gif";
                            }
                            else {
                                icono = "../../../modulos/maps/vistas/icons/" + ico;
                            }                

                            var marker = new google.maps.Marker({
                                position: siteLatLng,
                                map: map,
                                title: sites['title'],
                                zIndex: sites['i'],
                                html: sites['detalle'],
                                icon: icono
                            });

                            markers_arr.push(marker);

                            var contentString = "Some content";

                            google.maps.event.addListener(marker, "click", function () {
                                //alert(this.html);
                                infowindow.setContent(this.html);
                                infowindow.open(map, this);
                            });

                            avg.lat += siteLatLng.lat();
                            avg.lng += siteLatLng.lng();
                        }
            
                        // Center map.
                        map.setCenter(new google.maps.LatLng(avg.lat / markers.length, avg.lng / markers.length));
            
                    },
        
                    buscarProvincias: function() {
                        $("select[name=prov]").html("<option value=''>Seleccione</option>");
                        $("select[name=dist]").html("<option value=''>Seleccione</option>");
                        data_content = {
                            'action'    : 'buscarProvincias',
                            'dpto'      : $('#dpto').val()
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.poligono.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                
                                $.each(data, function(i, item) {
                                    $("select[name=prov]").append("<option value='"+data[i]['codprov']+"'>"+data[i]['nombre']+"</option>")
                                });

                            }});
                    },
        
                    buscarDistritos: function() {
                        $("select[name=dist]").html("<option value=''>Seleccione</option>");
                        data_content = {
                            'action'    : 'buscarProvicniasDistritos',
                            'dpto'      : $('#dpto').val(),
                            'prov'      : $('#prov').val()
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.poligono.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                
                                $.each(data, function(i, item) {
                                    $("select[name=dist]").append("<option value='"+data[i]['coddist']+"'>"+data[i]['nombre']+"</option>")
                                });

                            }});
                    },
        
                    SearchXY: function() {
                        var xy = $("#xy").attr('value');
                        var punto = xy.split(',');
            
                        //validando si es una coordenada valida
                        if( !esCoordenada(xy) ) {
                            alert("Coordenada no valida!");
                            return false;
                        }
                        $('#box_loading').show();
            
                        M.clearMarkers();
                        M.clearMakeMarkers(); //wil

                        //setea el zoom y el centro
                        map.setCenter(new google.maps.LatLng(punto[1], punto[0]));
                        map.setZoom(16);
            
                        var pto_xy = new google.maps.LatLng(punto[1], punto[0]);
            
                        marker = new google.maps.Marker({
                            position: pto_xy,
                            map: map,
                            title: 'Punto seleccionado',
                            html: 'Punto Seleccionada',
                            icon: "../../../modulos/maps/vistas/icons/markergreen.png"
                        });
                        markers_arr.push(marker);
            
                        $('#box_loading').hide();
                    },

                    clearMarkers: function() {
                        for (var n = 0, marker; marker = markers_arr[n]; n++) {
                            marker.setVisible(false);
                        }
                    },
        
                    clearMakeMarkers: function() {
                        for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                            makeMarker.setVisible(false);
                        }
                    },
        
                    clearPoligonos: function() {
                        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
                            poligonoDibujar.setMap(null);
                        }
                    },
        
                    clearPolilineas: function() {
                        for (var n = 0, polilineaDibujar; polilineaDibujar = polilineasDibujar_array[n]; n++) {
                            polilineaDibujar.setMap(null);
                        }
                    },

                    popupComponentes: function(x, y) {

                        var zoom_selected = map.getZoom();
            
                        $("#childModal").html('');
                        $("#childModal").css("background-color","#FFFFFF");

                        $.post("v_buscomp_popup.php", {
                            x: x,
                            y: y, 
                            zoom_selected: zoom_selected
                        },
                        function(data){
                            $("#childModal").html(data);
                        }
                        );
    
                        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
                    },

                    popupAddSearchComponentes: function(x, y) {

                        var zoom_selected = map.getZoom();
            
                        parent.$("#childModal").html('');
                        parent.$("#childModal").css("background-color","#FFFFFF");

                        $.post("v_buscaragregar_comp_popup.php", {
                            x: x,
                            y: y, 
                            zoom_selected: zoom_selected
                        },
                        function(data){
                            parent.$("#childModal").html(data);
                        }
                        );
    
                        parent.$("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'INFORMACI&Oacute;N DE LAS CAPAS', position:'top'});
                    },

                    loadPoligono: function(xy_array, z) {

                        var datos             = xy_array['coords'];
                        var color_poligono     = xy_array['color'];
                        var tipo             = xy_array['tipo'];
                        var nombre             = xy_array['nombre'];
                        var poligonoCoords     = [];

                        if( datos.length > 0) {
                            for (var i in datos) {
                                var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                poligonoCoords.push(pto_poligono);
                            }

                            poligonoDibujar = new google.maps.Polygon({
                                paths: poligonoCoords,
                                strokeColor: color_poligono,
                                strokeOpacity: 0.6,
                                strokeWeight: 3,
                                fillColor: color_poligono,
                                fillOpacity: 0.5
                            });

                            poligonosDibujar_array.push(poligonoDibujar);
                
                            poligonoDibujar.setMap(map);
                
                
                            var infowindow = new google.maps.InfoWindow();
                
                            // Add a listener for the click event
                            google.maps.event.addListener(poligonoDibujar, 'rightclick', function(event) {
                                infowindow.close();
                
                                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                                infowindow.setContent(contentString);
                                infowindow.setPosition(event.latLng);

                                infowindow.open(map);
                            });
                

                            //agrego el evento click en cada capa
                            google.maps.event.addListener(poligonoDibujar, 'click', function(capaEvent) {
                       
                                //clear markers
                                M.clearMarkers();
                                M.clearMakeMarkers();

                                marker = new google.maps.Marker({
                                    position: capaEvent.latLng,
                                    map: map
                                });

                                markers_arr.push(marker);
                                var Coords = capaEvent.latLng;
                                var x = Coords.lng();
                                var y = Coords.lat();

                                //popup, pregunta si quieres buscar o agregar componente
                                M.popupAddSearchComponentes(x,y);
                            });

                            //seteamos que la capa existe
                            capa_exists = 1;

                        }
                        else {
                            //capa_exists = 0;
                            //M.clearPoligonos();
                        }        
                    },
        
                    loadPolilinea: function(xy_array) {

                        var datos             = xy_array['coords'];
                        var color_polilinea = xy_array['color'];
                        var tipo             = xy_array['tipo'];
                        var nombre             = xy_array['nombre'];
                        var polilineaCoords = [];
            
                        if( datos.length > 0) {
                            for (var i in datos) {
                                var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                polilineaCoords.push(pto_polilinea);
                            }

                            // Construct the polyline                
                            polilineaDibujar = new google.maps.Polyline({
                                path: polilineaCoords,
                                strokeColor: color_polilinea,
                                strokeOpacity: 1.0,
                                strokeWeight: 4
                            });

                            //agrego al array las capas seleccionadas
                            polilineasDibujar_array.push(polilineaDibujar);
                
                            polilineaDibujar.setMap(map);

                
                            var infowindow = new google.maps.InfoWindow();
                
                            // Add a listener for the click event
                            google.maps.event.addListener(polilineaDibujar, 'mouseover', function(event) {
                                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                                infowindow.setContent(contentString);
                                infowindow.setPosition(event.latLng);

                                infowindow.open(map);
                            });
                
                            google.maps.event.addListener(polilineaDibujar, 'mouseout', function(event) {
                                infowindow.close();
                            });
                
                        }else {
                            //M.clearPolilineas();
                        }
                    }
        
                };

                $('#div1').click(function(){
                    $('#sidebar').hide();
                    $('#divInfoResult').hide();
                });
                
                $('#div2').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });
                
                $('#div3').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });
                
                $('#div4').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });

                $('#div5').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });

                $('#clearPoligono').click(function(){
                    M.clearPoligonos();
                });
    
                $('#clearRuta').click(function(){
                    M.clearPolilineas();
                });
    
                $('#btnFiltrarDireccion').click(function(){
                    listar_resultados('direccion');
                });
        
                $('#btnFiltrarUbigeo').click(function(){
                    listar_resultados('ubigeo');
                });
    
                $('#btnFiltrar').click(function(){
                    var arreglo_distrito = new Array();
                    var arreglo_nodo      = new Array();
                    var arreglo_mdf          = new Array();
                    var arreglo_plano     = new Array();
        
                    arreglo_distrito = load_array('multiselect_distrito[]');
                    arreglo_nodo      = load_array('multiselect_nodo[]');
                    arreglo_mdf      = load_array('multiselect_mdf[]');
                    arreglo_plano      = load_array('multiselect_plano[]');
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnFiltrar').attr('disabled', 'disabled');
                    data_content = {
                        'action'            : 'buscar',
                        'arreglo_distrito'     : arreglo_distrito,
                        'arreglo_nodo'         : arreglo_nodo, 
                        'arreglo_mdf'         : arreglo_mdf,
                        'arreglo_plano'     : arreglo_plano  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var poligonos = data['xy_poligono'];

                            if( poligonos.length > 0 ) {
                                //pintamos los poligonos
                                for (var i in poligonos) {
                                    M.loadPoligono(poligonos[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnFiltrar').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
    
                $('#btnDibujarPoligono').click(function(){
                    var tipocapaPoligono      = $('#tipocapaPoligono').val();
                    var arreglo_capaPoligono = new Array();

                    arreglo_capaPoligono      = load_array('multiselect_capaPoligono[]');

                    if ( arreglo_capaPoligono.length < 1 ) {
                        alert("Debe seleccionar por lo menos un poligono");
                        return false;
                    }
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnDibujarPoligono').attr('disabled', 'disabled');
                    data_content = {
                        'action'                : 'buscarPoligono',
                        'tipocapaPoligono'         : tipocapaPoligono,
                        'arreglo_capaPoligono'     : arreglo_capaPoligono  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var poligonos = data['xy_poligonos'];

                            if( poligonos.length > 0 ) {
                                //pintamos los poligonos
                                for (var i in poligonos) {
                                    M.loadPoligono(poligonos[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnDibujarPoligono').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
                $('#btnDibujarRuta').click(function(){
                    var tipocapaRuta      = $('#tipocapaRuta').val();
                    var arreglo_capaRuta = new Array();

                    arreglo_capaRuta      = load_array('multiselect_capaRuta[]');

                    if ( arreglo_capaRuta.length < 1 ) {
                        alert("Debe seleccionar por lo menos una ruta");
                        return false;
                    }
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnDibujarRuta').attr('disabled', 'disabled');
                    data_content = {
                        'action'            : 'buscarRuta',
                        'tipocapaRuta'         : tipocapaRuta,
                        'arreglo_capaRuta'     : arreglo_capaRuta  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var rutas = data['xy_rutas'];

                            if( rutas.length > 0 ) {
                                //pintamos los rutas
                                for (var i in rutas) {
                                    M.loadPolilinea(rutas[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnDibujarRuta').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
                $('#btnFiltrarXY_new').click(function(){
                    var xy = $("#xy").attr('value');
        
                    if( xy != '' ) {
                        //limpiando mapa
                        clearMarkers();
                        clearMakeMarkers();
        
                        M.SearchXY();
                    }else {
                        listar_resultados('cliente');
                    }
                });
    
                $("#tipocapaRuta").change(function() {
                    var idpoligono_tipo = $("#tipocapaRuta").val();
                    data_content = "action=buscarCapasRuta&idpoligono_tipo=" + idpoligono_tipo;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        success: function(datos) {
                            $("#divCapaRuta").html(datos);
                        }
                    });
                });
    
                $("#tipocapaPoligono").change(function() {
                    var idpoligono_tipo = $("#tipocapaPoligono").val();
                    data_content = "action=buscarCapasPoligono&idpoligono_tipo=" + idpoligono_tipo;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        success: function(datos) {
                            $("#divCapaPoligono").html(datos);
                        }
                    });
                });

                $("#zonal").change(function() {
                    var zonal = $("#zonal").val();
                    var zonales_array = [];
                    var zonal_arr = $("#zonal").val();

                    zonales_array.push(zonal);

                    var data_content = {
                        'action'        : 'buscarDistritos',
                        'zonales_array' : zonales_array
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "html",
                        success: function(datos) {
                            if( datos != '' ) {
                                $("#divDistrito").html(datos);
                            }
                        }
                    });
                }); 


                $("#btnBuscar").click(function() {
                    // lista y agregar los markers en divs "sidebar" y "map"
                    listar_terminales();
                });

                M.initialize();
               
            });


            function esDecimal(num) {
                if( /^[-]?[0-9]{1,2}(\.[0-9]{0,20})$/.test(num) ) {
                    return 1;
                }else {
                    return 0;
                }
            }

            function esCoordenada(xy) {
                var num_arr = xy.split(',');

                if( num_arr.length != 2 ) {
                    //alert("Coordenada no valida1.");
                    return false;
                }
                else {
                    if( $.trim(num_arr[0]) == '' && $.trim(num_arr[1]) == '' ) {
                        //alert("Coordenada no valida2.");
                        return false;
                    }else {
                        if( !esDecimal(num_arr[0]) || !esDecimal(num_arr[1]) ) {
                            //alert("Coordenada no valida3.");
                            return false;
                        }
                    }
                }
    
                return true;
            }

            function nuevasImagenesEdificio(idedificio, item, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/agregarImagenesEdificio.popup.php", {
                    idedificio    : idedificio,
                    item        : item,
                    x              : x,
                    y            : y
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Registrar nuevas imagenes para este edificio', position:'top'});
    
            }

            function editarEdificio(idedificio, item, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/editarComponente.popup.php", {
                    idedificio    : idedificio,
                    item        : item,
                    x              : x,
                    y            : y,
                    capa        : 'edi'
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Edificio', position:'top'});
            }

            function editarCompetencia(idcompetencia, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/editarComponente.popup.php", {
                    idcompetencia    : idcompetencia,
                    x              : x,
                    y            : y,
                    capa        : 'com'
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Competencia', position:'top'});
            }


            function listar_resultados(tipo) {

                if( tipo == 'cliente' ) {
                    var xy             = $("#xy").attr('value');
                    var telefono     = $("#telefono").attr('value');
                    var cliente1      = $("#cliente1").attr('value');
                    var cliente2      = $("#cliente2").attr('value');
                    var direccion      = "";
        
                    if(xy == "" && telefono == "" && cliente1 == "" && cliente2 == "" && direccion == "") {
                        alert("Ingrese algun criterio de busqueda.");
                        return false;
                    }    
                }
                else if( tipo == 'direccion' ) {
        
                    var strZonal = $('#zonal').val();
                    var distrito = new Array();

                    var zonal = [];
                    zonal.push(strZonal);
        
                    distrito = load_array('multiselect_selDistrito[]');
    
                    var direccion1     = $('#direccion1').val();
                    var numero         = $('#numero').val();
        
                    if(zonal.length == 0) {
                        alert("Ingrese la zonal");
                        return false;
                    }
                    if(distrito.length == 0) {
                        alert("Ingrese el distrito");
                        return false;
                    }
                    if(direccion1 == "" && numero == "") {
                        alert("Ingrese algun criterio de busqueda.");
                        return false;
                    }
                    if(direccion1 == "" && numero != "") {
                        alert("Ingrese la direccion");
                        return false;
                    }
                }
                else if( tipo == 'ubigeo' ) {
        
                    var dpto = $('#dpto').val();
                    var prov = $('#prov').val();
                    var dist = $('#dist').val();
                    var localidad = $('#localidad').val();


                    if(dpto == "") {
                        alert("Ingrese departamento");
                        return false;
                    }
                    if(prov == "") {
                        alert("Ingrese provincia");
                        return false;
                    }
                }
    
    
                /**
                 * makeMarker() ver 0.2
                 * creates Marker and InfoWindow on a Map() named 'map'
                 * creates sidebar row in a DIV 'sidebar'
                 * saves marker to markerArray and markerBounds
                 * @param options object for Marker, InfoWindow and SidebarItem
                 */
                var infoWindow = new google.maps.InfoWindow();
                var markerBounds = new google.maps.LatLngBounds();
                var markerArray = [];
     
          
                function makeMarker(options){
                    var pushPin = null;
                    /*var pushPin = new google.maps.Marker({
                     map: map
                 });
                 pushPin.setOptions(options);*/
            
                    if( options.position != '(0, 0)' ) {
                        pushPin = new google.maps.Marker({
                            map: map
                        });
                
                        makeMarkers_arr.push(pushPin);
                
                        pushPin.setOptions(options);
                        google.maps.event.addListener(pushPin, "click", function(){
                            infoWindow.setOptions(options);
                            infoWindow.open(map, pushPin);
                            if(this.sidebarButton)this.sidebarButton.button.focus();
                        });
                        var idleIcon = pushPin.getIcon();

                        markerBounds.extend(options.position);
                        markerArray.push(pushPin);
                    }else {
             
                        pushPin = new google.maps.Marker({
                            map: map
                        });
            
                        makeMarkers_arr.push(pushPin);
            
                        pushPin.setOptions(options);
                    }
         
                    if(options.sidebarItem){
                        pushPin.sidebarButton = new SidebarItem(pushPin, options);
                        pushPin.sidebarButton.addIn("sidebar");
                    }
         
                    return pushPin;           
                }

                google.maps.event.addListener(map, "click", function(){
                    infoWindow.close();
                });

                /*
                 * Creates an sidebar item 
                 * @param marker
                 * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                function SidebarItem(marker, opts) {
                    //var tag = opts.sidebarItemType || "button";
                    //var tag = opts.sidebarItemType || "span";
                    var tag = opts.sidebarItemType || "div";
                    var row = document.createElement(tag);
                    row.innerHTML = opts.sidebarItem;
                    row.className = opts.sidebarItemClassName || "sidebar_item";  
                    row.style.display = "block";
                    row.style.width = opts.sidebarItemWidth || "340px";
                    row.onclick = function(){
                        google.maps.event.trigger(marker, 'click');
                    }
                    row.onmouseover = function(){
                        google.maps.event.trigger(marker, 'mouseover');
                    }
                    row.onmouseout = function(){
                        google.maps.event.trigger(marker, 'mouseout');
                    }
                    this.button = row;
                }
                // adds a sidebar item to a <div>
                SidebarItem.prototype.addIn = function(block){
                    if(block && block.nodeType == 1)this.div = block;
                    else
                        this.div = document.getElementById(block)
                        || document.getElementById("sidebar")
                        || document.getElementsByTagName("body")[0];
                    this.div.appendChild(this.button);
                }

     
                var data_content = null;
                if( tipo == 'cliente' ) {
                    data_content = {
                        'action'        : 'buscarCliente',
                        'telefono'         : telefono,
                        'cliente1'         : cliente1,
                        'cliente2'         : cliente2
                    };
                }
                else if( tipo == 'direccion' ) {
                    data_content = {
                        'action'        : 'buscarDireccion',
                        'zonal'     : zonal,
                        'distrito'     : distrito, 
                        'direccion1'     : direccion1,
                        'numero'     : numero  
                    };
                }
                else if( tipo == 'ubigeo' ) {
                    data_content = {
                        'action'    : 'buscarLocalidad',
                        'dpto'      : dpto,
                        'prov'      : prov, 
                        'dist'      : dist,
                        'localidad' : localidad  
                    };
                }
     
                $('#box_loading').show();
                $("#btnFiltrarXY_new").attr('disabled', 'disabled');
                $("#btnFiltrarDireccion").attr('disabled', 'disabled');
                $("#btnFiltrarUbigeo").attr('disabled', 'disabled');
                $("#sidebar").html("");
                var tabla = '';
                var detalle = '';
                $.ajax({
                    type:   "POST",
                    url:    "../gescomp.poligono.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {
         
                        clearMarkers();
                        clearMakeMarkers();

                        if( data['edificios'].length > 0 ) {

                            var datos = data['edificios']; 

                            var num = 1;
                            var x_ini = y_ini = '';
                            var flag_captura_xy = 1;
                            for (var i in datos) {

                                var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                if( flag_captura_xy && x != '' && y != '' ) {
                                    x_ini = datos[i]['x'];
                                    y_ini = datos[i]['y'];
                                    flag_captura_xy = 0;
                                }

                                var color = "color: #FF0000;";
                                if( x != '' && y != '' ) {
                                    color = "color: #000000;";
                                }

                                if( tipo == 'cliente' || tipo == 'direccion' ) {
                                    var cableArmario = datos[i]['Armario'];
                                    var tipo_ca = 'Arm';
                                    if( datos[i]['tipo_red'] == 'D' ) {
                                        cableArmario = datos[i]['Cable'];
                                        tipo_ca = 'Cbl';
                                    }

                                    var zonal = datos[i]['Zonal'];
                                    if( datos[i]['Zonal'] == 'ESTE' || datos[i]['Zonal'] == 'OESTE' || datos[i]['Zonal'] || 'NORTE' && datos[i]['Zonal'] == 'SUR' ) {
                                        zonal = 'LIMA';
                                    }

                                    tabla = '<div style="width:70px; float:left; font-weight:normal;' + color + '">Zon:' + datos[i]['Zonal'] + '<br />Mdf:' + datos[i]['MDF'] + '<br />' + tipo_ca + ':' + datos[i]['cableArmario'] + '<br />Trm:' + datos[i]['Caja'] + '</div>';
                                    tabla += '<div style="width:270px; float:left;">';
                                    tabla += '<div style="font-size:15px;color:#1155CC;">' + datos[i]['Cliente'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#666666;">' + datos[i]['Direccion'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#555555;">' + datos[i]['DDN'] + ' ' + datos[i]['Telefono'] + '</div>';
                                    tabla += '</div>';

                                    detalle = '<b>Telefono:</b> ' + datos[i]['Telefono'] + '<br />';
                                    detalle += '<b>Cliente:</b> ' + datos[i]['Cliente'] + '<br />';
                                    detalle += '<b>Direccion:</b> ' + datos[i]['Direccion'] + '<br /><br />';
                                    detalle += '<b>Zonal:</b> ' + datos[i]['Zonal'] + '<br />';
                                    detalle += '<b>Mdf:</b> ' + datos[i]['MDF'] + '<br />';
                                    if( datos[i]['tipo_red'] == 'D' ) {
                                        detalle += '<b>Cable:</b> ' + datos[i]['Cable'] + '<br />';
                                    }else if( datos[i]['tipo_red'] == 'F' ) {
                                        detalle += '<b>Armario:</b> ' + datos[i]['Armario'] + '<br />';
                                    }
                                    detalle += '<b>Caja:</b> ' + datos[i]['Caja'] + '<br />';
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
                                    detalle += '<a title="Ver clientes asignados a este terminal: ' + datos[i]['Caja'] + '" href="javascript:clientesTerminal(\''+ datos[i]['Zonal'] +'\',\''+ datos[i]['MDF'] +'\',\''+ datos[i]['Cable'] +'\',\''+ datos[i]['Armario'] +'\',\''+ datos[i]['Caja'] +'\',\''+ datos[i]['tipo_red'] +'\');">Ver clientes</a>';        

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['Caja'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/terminal/trm_" + datos[i]['Caja'] + "--1.gif"
                                    });
                        
                                }
                                else if( tipo == 'ubigeo' ) {
                        
                                    tabla = '<div style="width:70px; float:left; font-weight:normal;' + color + '">' +  datos[i]['ubigeo'] + '</div>';
                                    tabla += '<div style="width:270px; float:left;">';
                                    tabla += '<div style="font-size:15px;color:#1155CC;">' + datos[i]['localidad'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#666666;">' + datos[i]['distrito'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#555555;">' + datos[i]['departamento'] + ' / ' + datos[i]['provincia'] + '</div>';
                                    tabla += '</div>';

                                    detalle = '<b>Departamento:</b> ' + datos[i]['departamento'] + '<br />';
                                    detalle += '<b>Provincia:</b> ' + datos[i]['provincia'] + '<br />';
                                    detalle += '<b>Distrito:</b> ' + datos[i]['distrito'] + '<br />';
                                    detalle += '<b>Localidad:</b> ' + datos[i]['localidad'] + '<br />';
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['localidad'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/localidades/localidades.png"
                                    });
                        
                                }

                                num++;
                            }

                            /* fit viewport to markers */
                            //map.fitBounds(markerBounds);

                            map.setZoom(16);
                            map.setCenter(new google.maps.LatLng(y_ini, x_ini));

                        }

                        $("#btnFiltrarXY_new").removeAttr('disabled');
                        $("#btnFiltrarDireccion").removeAttr('disabled');
                        $("#btnFiltrarUbigeo").removeAttr('disabled');
                        $('#box_loading').hide();
                        $('#divInfoResult').show();
                        $('#spanNum').html(data['edificios'].length);
                    }
                });
            }

            function clearMarkers() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }

            function clearMakeMarkers() {
                for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                    makeMarker.setVisible(false);
                }
            }

            function clientesTerminal(zonal, mdf, cable, armario, caja, tipo_red) {
                parent.$("#childModal").html('Cargando datos...');
                parent.$("#childModal").css("background-color","#FFFFFF");
                $.post("principal/clientes_terminal.popup.php", {
                    zonal: zonal,
                    mdf: mdf,
                    cable: cable,
                    armario: armario,
                    caja: caja,
                    tipo_red: tipo_red
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Clientes asignados para este terminal: ' + caja, position:'top'});

            }


            


            function listar_terminales() {

                var IDzonal  = $("#selZonal").val();
                var IDmdf    = $("#selMdf").val();
                var IDtipred = $("#selTipRed").val();
                var IDcableArmario = $("#selCableArmario").val();
                var datosXY = 'conXY';
                
                var tipo = $("#selTipo").val();

                var area_mdf = 'no';
                if($("#poligono").is(':checked')) { 
                    area_mdf = 'si';
                }

                
                if(IDzonal == "" || IDmdf == "" || IDtipred == "" || IDcableArmario == "") {
                    alert("Ingrese todos los criterios de busqueda.");
                    return false;
                }
                
                /**
                 * makeMarker() ver 0.2
                 * creates Marker and InfoWindow on a Map() named 'map'
                 * creates sidebar row in a DIV 'sidebar'
                 * saves marker to markerArray and markerBounds
                 * @param options object for Marker, InfoWindow and SidebarItem
                 */
                 var infoWindow = new google.maps.InfoWindow();
                 var markerBounds = new google.maps.LatLngBounds();
                 var markerArray = [];
                 
                      
                 function makeMarker(options){
                    var pushPin = null;
                        /*var pushPin = new google.maps.Marker({
                         map: map
                     });
                     pushPin.setOptions(options);*/
                        
                     if( options.position != '(0, 0)' ) {
                            pushPin = new google.maps.Marker({
                                map: map
                            });
                            
                            makeMarkers_arr.push(pushPin);
                            
                            pushPin.setOptions(options);
                            google.maps.event.addListener(pushPin, "click", function(){
                                infoWindow.setOptions(options);
                                infoWindow.open(map, pushPin);
                                if(this.sidebarButton)this.sidebarButton.button.focus();
                            });
                            var idleIcon = pushPin.getIcon();

                            markerBounds.extend(options.position);
                            markerArray.push(pushPin);
                     }else {
                         
                         pushPin = new google.maps.Marker({
                            map: map
                        });
                        
                        makeMarkers_arr.push(pushPin);
                        
                        pushPin.setOptions(options);
                     }
                     
                     if(options.sidebarItem){
                         pushPin.sidebarButton = new SidebarItem(pushPin, options);
                         pushPin.sidebarButton.addIn("sidebar");
                     }
                     
                     return pushPin;           
                 }

                 google.maps.event.addListener(map, "click", function(){
                     infoWindow.close();
                 });

                 /*
                  * Creates an sidebar item 
                  * @param marker
                  * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                 function SidebarItem(marker, opts) {
                     //var tag = opts.sidebarItemType || "button";
                     var tag = opts.sidebarItemType || "span";
                     var row = document.createElement(tag);
                     row.innerHTML = opts.sidebarItem;
                     row.className = opts.sidebarItemClassName || "sidebar_item";  
                     row.style.display = "block";
                     row.style.width = opts.sidebarItemWidth || "290px";
                     row.onclick = function(){
                         google.maps.event.trigger(marker, 'click');
                     }
                     row.onmouseover = function(){
                         google.maps.event.trigger(marker, 'mouseover');
                     }
                     row.onmouseout = function(){
                         google.maps.event.trigger(marker, 'mouseout');
                     }
                     this.button = row;
                 }
                 // adds a sidebar item to a <div>
                 SidebarItem.prototype.addIn = function(block){
                     if(block && block.nodeType == 1)this.div = block;
                     else
                         this.div = document.getElementById(block)
                         || document.getElementById("sidebar")
                         || document.getElementsByTagName("body")[0];
                     this.div.appendChild(this.button);
                 }

                 data_content = "action=buscarTerminales&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&IDtipred=" + IDtipred + "&IDcableArmario=" + IDcableArmario + "&datosXY=" + datosXY + "&area_mdf=" + area_mdf;
                 $("#fongoLoader").show();
                 //$("#btnBuscar").attr('disabled', 'disabled');
                 $("#sidebar").html("");
                 var tabla = '';
                 var detalle = '';
                 $.ajax({
                     type:   "POST",
                     url:    "../gescomp.terminal.main.php",
                     data:   data_content,
                     dataType: "json",
                     success: function(data) {
                     
                        clearMarkers();
                        clearMakeMarkers();
                        
                        
                        if( capa_exists ) {
                            //limpiamos todas las capas del mapa
                            clearPoligonos();
                        }

                        var poligonos = data['xy_poligono'];

                        if( poligonos.length > 0 ) {
                            //pintamos los poligonos
                            for (var i in poligonos) {
                                loadPoligono(poligonos[i], i);
                            }
                            
                            //setea el zoom y el centro
                            map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                            //map.setZoom(13);
                        }
                        
                        

                         if( data['edificios'].length > 0 ) {

                             if( data['edificios_con_xy'].length == 0 ) {

                                 var datos = data['edificios']; 
                                 var estados = data['estados'];

                                 var x1 = y1 = null;
                                if( data['armario']['x'] != null && data['armario']['y'] != null ) {
                                    x1 = data['armario']['x'];
                                     y1 = data['armario']['y'];
                                }
                                 else if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                     x1 = data['mdf']['x'];
                                     y1 = data['mdf']['y'];
                                 }else {
                                     x1 = data['zonal']['x'];
                                     y1 = data['zonal']['y'];
                                 }

                                for (var i in datos) {
                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                                    
                                    tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
                                    tabla += '<tr id="row' + datos[i]['caja'] + '" class="unselected">';
                                    tabla += '<td width="70">';
                                    tabla += '<span id="' + datos[i]['caja'] + '"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
                                    for(var e in estados) {
                                        //tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
                                    }

                                    tabla += '<td width="70">' + datos[i]['caja'] + '</td>';
                                    tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['caja'] + '" name="xy[' + datos[i]['mtgespktrm'] + '][x]" value="' + x + '"></td>';
                                    tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['caja'] + '" name="xy[' + datos[i]['mtgespktrm'] + '][y]" value="' + y + '"></td>';
                                    tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/terminal/terminal-1.gif" width="12"></td>';
                                    tabla += '</tr>';
                                    tabla += '</table>';

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(y1, x1),
                                        title: '',
                                        sidebarItem: tabla,
                                        //content: detalle,
                                        icon: "../../../img/markergreen.png"
                                    });
                                    
                                    
                                    
                                }
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);
                                map.setZoom(14);
                                

                                 
                             }else {

                             
                                 var datos = data['edificios']; 
                                 var estados = data['estados'];
                                 
                                for (var i in datos) {
                
                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                    var est_caja = "";
                                    if( tipo == 'capaci' ) {
                                        if( datos[i]['qparlib'] == 0 ) {
                                            est_caja = 3;
                                        }else if( datos[i]['qparlib'] > 0 && datos[i]['qparlib'] < 3 ) {
                                            est_caja = 2;
                                        }else if( datos[i]['qparlib'] > 2 ) {
                                            est_caja = 1;
                                        }
                                    }else if( tipo == 'atenua' ) {
                                        if( datos[i]['atenuacion'] == null ) {
                                            est_caja = 4;
                                        }else if( datos[i]['atenuacion'] >= 0 && datos[i]['atenuacion'] < 11 ) {
                                            est_caja = 5;
                                        }else if( datos[i]['atenuacion'] < 61 ) {
                                            est_caja = 1;
                                        }else if( datos[i]['atenuacion'] > 60 && datos[i]['atenuacion'] < 63 ) {
                                            est_caja = 2;
                                        }else if( datos[i]['atenuacion'] > 62 ) {
                                            est_caja = 3;
                                        }
                                    }

                                    tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
                                    tabla += '<tr id="row' + datos[i]['caja'] + '" class="unselected">';
                                    tabla += '<td width="70">';
                                    tabla += '<span id="' + datos[i]['caja'] + '"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
                                    for(var e in estados) {
                                        //tabla += '<p onclick="changeEstado(\'' + datos[i]['mtgespktrm'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/terminal/terminal-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
                                    }
                                    tabla += '<td width="70">' + datos[i]['caja'] + '</td>';
                                    tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['caja'] + '" name="xy[' + datos[i]['mtgespktrm'] + '][x]" value="' + x + '"></td>';
                                    tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['caja'] + '" name="xy[' + datos[i]['mtgespktrm'] + '][y]" value="' + y + '"></td>';
                                    tabla += '<td width="20"><img border="0" title="' + data['est'][est_caja] + '" src="../../../img/map/terminal/terminal-' + est_caja + '.gif" width="12"></td>';
                                    tabla += '</tr>';
                                    tabla += '</table>';

                                    detalle = '<b>Zonal:</b> ' + datos[i]['zonal'] + '<br />';
                                    detalle += '<b>Mdf:</b> ' + datos[i]['mdf'] + '<br />';
                                    if( datos[i]['tipo_red'] == 'D' ) {
                                        detalle += '<b>Cable:</b> ' + datos[i]['cable'] + '<br />';
                                    }else if( datos[i]['tipo_red'] == 'F' ) {
                                        detalle += '<b>Armario:</b> ' + datos[i]['armario'] + '<br />';
                                    }
                                    detalle += '<b>Caja:</b> ' + datos[i]['caja'] + '<br />';
                                    detalle += '<b>Capacidad:</b> ' + datos[i]['qcapcaja'] + '<br />';
                                    detalle += '<b>Pares libres:</b> ' + datos[i]['qparlib'] + '<br />';
                                    detalle += '<b>Pares reserv.:</b> ' + datos[i]['qparres'] + '<br />';
                                    detalle += '<b>Direccion:</b> ' + datos[i]['direccion'] + '<br />';
                                    detalle += '<b>Pares distrib.:</b> ' + datos[i]['qdistrib'] + '<br /><br />';
                                    if( datos[i]['atenuacion'] == null ) {
                                        detalle += '<b>Atenuaci&oacute;n:</b><br /><br />';
                                    }else {
                                        detalle += '<b>Atenuaci&oacute;n:</b> ' + datos[i]['atenuacion'] + '<br /><br />';
                                    }
                                    if( datos[i]['tipo_red'] == 'F' && data['armario']['x'] != null && data['armario']['y'] != null ) {
                                        detalle += '<b>Distancia a Armario:</b> ' + datos[i]['distancia_armario'] + ' km.<br />';
                                    }
                                    if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                        detalle += '<b>Distancia a Mdf:</b> ' + datos[i]['distancia_mdf'] + ' km.<br /><br />';
                                    }
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
                                    detalle += '<a title="Ver clientes asignados a este terminal" href="javascript:clientesTerminal(\''+ datos[i]['zonal1'] +'\',\''+ datos[i]['mdf'] +'\',\''+ datos[i]['cable'] +'\',\''+ datos[i]['armario'] +'\',\''+ datos[i]['caja'] +'\',\''+ datos[i]['tipo_red'] +'\');">Ver clientes</a>';        

                                    
                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['caja'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/terminal/trm_" + datos[i]['caja'] + "--" + est_caja + ".gif"
                                    });

                                }
                                
                                var xMDF = yMDF = null;
                                 if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                     xMDF = data['mdf']['x'];
                                     yMDF = data['mdf']['y'];
                                    
                                    detalleMDF = '<b>Mdf:</b> ' + data['mdf']['abv'] + ' - ' + data['mdf']['name'] + '<br />';
                                    detalleMDF += '<b>Zonal:</b> ' + data['mdf']['zonal'] + '<br />';
                                    detalleMDF += '<b>Direccion:</b> ' + data['mdf']['direccion'] + '<br /><br />';
                                    detalleMDF += '<b>X,Y:</b> ' + xMDF + ',' + yMDF;
                                    
                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(yMDF, xMDF),
                                        title: data['mdf']['abv'] + ' - ' + data['mdf']['name'],
                                        content: detalleMDF,
                                        icon: "../../../img/map/mdf/mdf_" + data['mdf']['abv'] + "--" + data['mdf']['estado'] + ".gif"
                                    });
                                 }

                                var xArmario = yArmario = null;
                                 if( data['armario']['x'] != null && data['armario']['y'] != null ) {
                                     xArmario = data['armario']['x'];
                                     yArmario = data['armario']['y'];
                                    
                                    detalleArmario = '<b>Armario:</b> ' + data['armario']['armario'] + '<br />';
                                    detalleArmario += '<b>Mdf:</b> ' + data['armario']['mdf'] + '<br />';
                                    detalleArmario += '<b>Zonal:</b> ' + data['armario']['zonal'] + '<br />';
                                    detalleArmario += '<b>X,Y:</b> ' + xArmario + ',' + yArmario;
                                    
                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(yArmario, xArmario),
                                        title: data['armario']['armario'],
                                        content: detalleArmario,
                                        icon: "../../../img/map/armario/arm_" + data['armario']['armario'] + "--4.gif"
                                    });
                                 }
                
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);

                             }
                             
                          }else {
                            
                            var x1 = y1 = null;
                            if( data['armario']['x'] != null && data['armario']['y'] != null ) {
                                x1 = data['armario']['x'];
                                y1 = data['armario']['y'];
                            }
                            else if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
                                x1 = data['mdf']['x'];
                                y1 = data['mdf']['y'];
                            }else {
                                x1 = data['zonal']['x'];
                                y1 = data['zonal']['y'];
                            }
                            
                            var latlng = new google.maps.LatLng(y1, x1);

                            /* markers and info window contents */
                            makeMarker({
                                draggable: false,
                                position: new google.maps.LatLng(y1, x1),
                                title: '',
                                icon: "../../../img/markergreen.png"
                            });
                            
                            map.setCenter(latlng);
                            
                          
                          }

                        $("#btnBuscar").removeAttr('disabled');
                        $("#btnGrabar").removeAttr('disabled');
                        //setTimeout("hideLoader()", 1000);
                        $("#fongoLoader").hide();
                     }
                 });
            }

            

        </script>
        <style type="text/css">
            .sidebar_item {
                float: left;
                padding: 5px 0;
                border-bottom: 1px solid #DDDDDD;
            }
            .ipt{
                border:none;
                color:#0086C3;
            }
            .ipt:focus{
                border:1px solid #0086C3;
            }
            body{
                font-family: Arial;
                font-size: 11px;
            }
            .ui-layout-pane {
                background: #FFF; 
                border: 1px solid #BBB; 
                padding: 7px; 
                overflow: auto;
            } 

            .ui-layout-resizer {
                background: #DDD; 
            } 

            .ui-layout-toggler {
                background: #AAA; 
            }
            .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
            .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
            .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
            .slt{
                border:1px solid #1E9EF9;
                background-color: #FAFAFA;
                height: 18px;
                font-size: 11px;
                color:#0B0E9D;
            }
            .box_checkboxes{
                border:1px solid #4C5E60;
                background: #E7F7F7;
                width:170px;
                height: 240px;
                overflow: auto;
                display:none;
                position: absolute;
                top:0px;
                left:0px;
                opacity:0.9;
            }
            .tb_data_box{
                width: 100%;
                border-collapse:collapse;
                font-family: Arial;
                font-size: 9px;
            }
            
            table.tablesorter {
                background-color: #CDCDCD;
                font-family: Arial,Helvetica,sans-serif;
                font-size: 8pt;
            }
            table.tablesorter thead tr th, table.tablesorter tfoot tr th {
                background-color: #E6EEEE;
                border: 1px solid #FFFFFF;
                font-size: 8pt;
            }
            .headTbl {
                background-image: url("../../../img/bg_head_js.gif");
                /*background-position: center top;*/
                background-repeat: repeat-x;
                color: #000000;
                height: 20px;
                font-weight: normal;
            }
            table.tablesorter tbody td {
                background-color: #FFFFFF;
                color: #3D3D3D;
                vertical-align: middle;
                font-size: 10px;
            }
            .tb_listado {
                border: 1px solid #3889AF;
                border-collapse: collapse;
                border-radius: 5px 5px 5px 5px;
                color: #3C3C3C;
                font-family: Arial;
                font-size: 11px;
                margin-left: 5px;
                width: 98%;
            }

        </style>
        <script>
            $(function() {
                $("#btnFiltrar").button();
                $("#btnFiltrarXY").button();
                $("#btnFiltrarXY_new").button();
                $("#btnFiltrarDireccion").button();
                $("#btnFiltrarUbigeo").button();
        
                $("#btnDibujarRuta, #btnDibujarPoligono").button();
            });
        </script>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
        <div class="ui-layout-west">
        
            <div id="divBuscarComponentes">

                <div id="tabs" style="width:99%; margin-bottom: 10px;">
                    <ul>
                        <li><a id="div1" href="#divPoligono" style="color:#1D87BF; font-weight: 600">Busq. de Componentes</a></li>
                        <li><a id="div2" href="#divSearchXY" style="color:#1D87BF; font-weight: 600">Busq. por cliente</a></li>
                        <li><a id="div3" href="#divDireccion" style="color:#1D87BF; font-weight: 600">Direccion</a></li>
                        <li><a id="div4" href="#divUbigeo" style="color:#1D87BF; font-weight: 600">Localidades</a></li>
                        <li><a id="div5" href="#divTerminales" style="color:#1D87BF; font-weight: 600">Busq. de Terminales</a></li>
                    </ul>
    
                    <div id="divPoligono">
    
                        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                            <tr>
                                <td colspan="2"><span class="l_tit">Poligonos</span></td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Tipo capa</td>
                                <td>
                                    <select name="tipocapaPoligono" id="tipocapaPoligono" style="font-size: 11px;height: 20px;padding: 1px;">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Capa</td>
                                <td>
                                    <div id="divCapaPoligono">
                                        <select name="capaPoligono" id="capaPoligono" style="font-size: 11px;height: 20px;padding: 1px;" class="clsMultiple" multiple="multiple">
                                            <!--<option value="">Seleccione</option>-->
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button id="btnDibujarPoligono">Dibujar Poligono</button> &nbsp; 
                                    <span id="clearPoligono"><img src="../../../img/btn_clear_32.png" style="width:15px;height:15px;"> <a href="javascript:void(0);">Limpiar poligonos</a></span>
                                </td>
                            </tr>
                        </table>
    
    
                        <div style="height:20px;"></div>
    
                        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                            <tr>
                                <td colspan="2"><span class="l_tit">Rutas</span></td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Tipo capa</td>
                                <td>
                                    <select name="tipocapaRuta" id="tipocapaRuta" style="font-size: 11px;height: 20px;padding: 1px;">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Capa</td>
                                <td>
                                    <div id="divCapaRuta">
                                        <select name="capaRuta" id="capaRuta" style="font-size: 11px;height: 20px;padding: 1px;" class="clsMultiple" multiple="multiple">
                                            <!--<option value="">Seleccione</option>-->
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button id="btnDibujarRuta">Dibujar Ruta</button> &nbsp; 
                                    <span id="clearRuta"><img src="../../../img/btn_clear_32.png" style="width:15px;height:15px;"> <a href="javascript:void(0);">Limpiar rutas</a></span>
                                </td>
                            </tr>
                        </table>
    
    
                    </div>
    
                    <div id="divSearchXY">
    
                        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                        <!--<tr>
                                <td><span class="l_tit">B&uacute;squeda por coordenada XY:</span></td>
                    </tr>-->
                            <tr>
                                <td align="left" class="label">Ingrese el punto x,y</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" class="clsSearchXY" id="xy" name="xy" size="35" style="height:18px; padding: 1px; font-size:11px;"><br />
                                        <span style="font-size:11px;">Ejm: -77.07286315,-12.07187324</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Telefono</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" class="clsSearchXY" id="telefono" name="telefono" size="20" style="height:18px; padding: 1px; font-size:11px;"><br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Cliente</td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="text" class="clsSearchXY" id="cliente1" name="cliente1" size="22" style="height:18px; padding: 1px; font-size:11px;"> &nbsp; 
                                        <input type="text" class="clsSearchXY" id="cliente2" name="cliente2" size="22" style="height:18px; padding: 1px; font-size:11px;"><br />
                                            </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br /><button id="btnFiltrarXY_new">Buscar</button>
                                                </td>
                                            </tr>
                                            </table>
    
                                            </div>
    
                    <div id="divDireccion">
    
                                                <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
    
                                                    <tr>
                                                        <td align="left" class="label">Zonal</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <select name="zonal" id="zonal">
                                                                <option value="">Seleccione</option>
                                                            </select>                        
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="label">Distrito</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <div id="divDistrito">
                                                                <select id="selDistrito" class="clsMultiple" multiple="multiple">
                                                                    <!--<option value="">Seleccione</option>-->
                                                                </select>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="label">Direcci&oacute;n</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <input type="text" id="direccion1" name="direccion1" size="35" style="height:18px; padding: 1px; font-size:11px;">
                                                            <!--<input type="text" id="direccion2" name="direccion2" size="16" style="height:18px; padding: 1px; font-size:10px;">--> &nbsp; 
                            Cdra. <input type="text" id="numero" name="numero" size="3" style="height:18px; padding: 1px; font-size:11px;">
                                                                    <br />
                                                                    </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <br /><button id="btnFiltrarDireccion">Buscar</button>
                                                                        </td>
                                                                    </tr>
                                                                    </table>
    
                                                                    </div>
    
                    <div id="divUbigeo">
    
                        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                            <tr>
                                <td align="left" class="label" width="100">Departamento</td>
                                <td align="left">
                                    <select name="dpto" id="dpto">
                                        <option value="">Seleccione</option>
                                    </select>                        
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Provincia</td>
                                <td align="left">
                                    <div id="divProv">
                                        <select name="prov" id="prov">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label">Distrito</td>
                                <td align="left">
                                    <div id="divDist">
                                        <select name="dist" id="dist">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label" colspan="2">Localidad</td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <input type="text" id="localidad" name="localidad" size="35" style="height:18px; padding: 1px; font-size:11px;">
                                        <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br /><button id="btnFiltrarUbigeo">Buscar</button>
                                </td>
                            </tr>
                        </table>
    
                    </div>
    
                    <div id="divTerminales">
    
                        <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                        <tr>
                            <td>Zonal:</td>
                            <td>
                                <div id="divZonal">
                                    <select id="selZonal">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Mdf:</td>
                            <td>
                                <div id="divMdf">
                                    <select id="selMdf">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Tipo de Red:</td>
                            <td>
                                <div id="divTipRed">
                                    <select id="selTipRed">
                                        <option value="">Seleccione</option>
                                        <option value="D">Directa</option>
                                        <option value="F">Flexible</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Cable/Armario:</td>
                            <td>
                                <div id="divCableArmario">
                                    <select id="selCableArmario">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <!-- 
                        <tr>
                            <td>Con X,Y:</td>
                            <td>
                                <select id="selDatosXY">
                                    <option value="">Todos</option>
                                    <option value="conXY">Terminales con X,Y</option>
                                    <option value="sinXY">Terminales sin X,Y</option>
                                </select>
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td>Tipo:</td>
                            <td>
                                <select id="selTipo">
                                    <option value="capaci">Por capacidad</option>
                                    <option value="atenua">Por atenuacion</option>
                                </select>
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" name="poligono" id="poligono" disabled="disabled"> Mostrar poligono(Mdf)
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td colspan="2">
                                <input type="button" id="btnBuscar" value="Buscar">
                            </td>
                        </tr>
                        </table>
                        
                    </div>
                                                                    
                    <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                    
                </div>
                    
                <div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">
    
                    <div id="divInfoResult" style="margin-bottom: 8px; width: 340px; display:none;">
                        <div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
                        <!--<div style="float:right;"><a href="javascript:imprimirSeleccionXY();">Imprimir seleccion</a> | <a href="javascript:imprimirEnMapaXY();">Imprimir en mapa</a></div>-->
                    </div>
    
                    <div id="sidebar"></div>
                </div>

            </div>
            
            <div id="divCrearRutaAcometida" style="display:none;">
            
                <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><span class="l_tit">Devoluci&oacute;n por exceso de Acometida</span></td>
                </tr>
                <tr>
                    <td align="left" class="label">X,Y Cliente:</td>
                    <td align="left" colspan="2">
                        <span id="xy_cliente"></span>
                        <input type="hidden" size="30" name="x_cliente" class="clsAcometida" id="x_cliente" style="font-size:11px;">
                        <input type="hidden" size="30" name="y_cliente" class="clsAcometida" id="y_cliente" style="font-size:11px;">
                    </td>
                </tr>
                <tr>
                    <td align="left" class="label" colspan="3">Terminal:</td>
                </tr>
                <tr>
                    <td align="left" colspan="3">
                        <table id="tb_resultado_trm" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0">
                        <thead>
                            <tr>
                                <th class="headTbl" width="10">#</th>
                                <th class="headTbl">Terminal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--<tr>
                                <td class="celdaX">1</td>
                                <td class="celdaX">023</td>
                            </tr>-->
                        </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="label">Distancia de la Ruta (m.):</td>
                    <td align="left" colspan="2">
                        <input type="text" size="10" name="distancia_ruta" class="clsAcometida" id="distancia_ruta" style="font-size:11px;" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td align="left" class="label">Tipo pedido:</td>
                    <td align="left" colspan="2">
                        <select name="tipo_pedido" id="tipo_pedido" style="font-size:11px;">
                            <option value="">Seleccione</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="label">Pedido:</td>
                    <td align="left" colspan="2">
                        <input type="text" size="30" class="clsAcometida" name="pedido" id="pedido" style="font-size:11px;">
                    </td>
                </tr>
                <tr>
                    <td align="left" class="label" colspan="3">
                        <input class="clsAcometida" type="hidden" name="a_zonal" id="a_zonal" />
                        <input class="clsAcometida" type="hidden" name="a_mdf" id="a_mdf" />
                        <input class="clsAcometida" type="hidden" name="a_cable" id="a_cable" />
                        <input class="clsAcometida" type="hidden" name="a_armario" id="a_armario" />
                        <input class="clsAcometida" type="hidden" name="a_caja" id="a_caja" />
                        <input class="clsAcometida" type="hidden" name="a_tipo_red" id="a_tipo_red" />
                        <input class="clsAcometida" type="hidden" name="a_terminal_x" id="a_terminal_x" />
                        <input class="clsAcometida" type="hidden" name="a_terminal_y" id="a_terminal_y" />
                        <input id="btnGrabarRutaAcometida" class="button" type="button" value="Grabar Ruta Acometida" onclick="grabarRutaAcometida();" style="font-size:11px;"> 
                        <input id="btnCancelar" class="button" type="button" value="Cancelar" onclick="CancelarRutaAcometida();" style="font-size:11px;"> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="box_loading2" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                    </td>
                </tr>
                </table>
                
            </div>
            
        </div>
        <div class="ui-layout-center">

            <div id="box_resultado">

                <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
                
                <?php
                //Validando si es Contrata
                $tieneAccesoPorEecc = 0;
                if ( !empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
                    foreach ( $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa ) {
                        if ( $objEmpresa->__get('_isEecc') == '1' ) {
                            $tieneAccesoPorEecc = 1;
                            break;
                        }
                    }
                }
                
                $tieneAccesoPorArea = 0;
                
                if ( ($_SESSION['USUARIO']->__get('_idArea') == '5' || 
                     $_SESSION['USUARIO']->__get('_idArea') == '6')  ) {
                    $tieneAccesoPorArea = 1;
                } 
                
                if ( $tieneAccesoPorEecc && $tieneAccesoPorArea ) {
                ?>
                <div id="divSearchAddress" style="float:right;width:150px;position:fixed;background:#ffffff;">
                    <div id="divDireccion" class="closed">
                        <div style="display: block;">
                            <input type="checkbox" name="activar_ruta" id="activar_ruta" /> 
                            Crear ruta
                        </div>
                        <div style="display: block;">
                            <input type="checkbox" name="crear_ruta_acometida" id="crear_ruta_acometida" /> 
                            Crear ruta acometida
                        </div>
                    </div>
                </div>
                <?php 
                }
                ?>

            </div>

        </div>
                
        <div id="parentModal" style="display: none;">
            <div id="childModal" style="padding: 10px; background: #fff;"></div>
        </div>

</body>
</html>