<?php
require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

//$isLogin = 0;
$dirModulo = '../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript">
function loader(estado) {
    if(estado=='start') {
        $(".bgLoader").css("display","block");
    }
    if(estado=='end') {
        $(".bgLoader").css("display","none");
    }
}
    
$(document).ready(function() {
    editarEdificio = function(idedificio, x, y) {
        window.top.location = 'm.buscomp.editar.componente.php?capa=edi&idedificio=' + idedificio + '&x=' + x + '&y=' + y;
    }

    buscarEdificio = function() {
        var item = $('#item').attr('value');
        
        if( item.length < 3) {
            alert("Debe ingresar 3 caracteres como minimo");
            return false;
        }
        
        var data_content = { 
            item   : item
        };

        loader("start");
        $("#btnBuscar").attr('disabled', 'disabled');
        
        $.ajax({
            type:   "POST",
            url:    "m.busqueda.edificio.php?cmd=buscarEdificio",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                
                var datos = data['edificios'];
            
                var str_edificio = '';
                if( datos.length > 0) {
                    for(var i in datos) {
                        var style_xy = '';
                        if( datos[i]['x'] != '' && datos[i]['y'] != '' ) {
                            style_xy = 'style="background-color:#ff0000;"';
                        }

                        str_edificio += '<tr class="lnk">';
                        str_edificio += '<td ' + style_xy + '>&nbsp;&nbsp;</td>';
                        str_edificio += '<td class="list" width="30"><img src="../../img/map/edificio/edificio-' + datos[i]['estado'] + '.gif"></td>';
                        str_edificio += '<td class="list" style="width:100%!important;">' + datos[i]['item'] + '<br />';
                        str_edificio += '<span style="font-size:12px;color:#999999;">' + datos[i]['tipo_cchh'] + ' - ' + datos[i]['cchh'] + '<br />' + datos[i]['direccion_obra'] + ' - ' + datos[i]['numero'] + '<br /><b>Zonal</b>: ' + datos[i]['zonal'] + ', <b>Ura</b>: ' + datos[i]['ura'] + ', <b>Sector</b>: ' + datos[i]['sector'] + '</span>';
                        str_edificio += '</td>';
                        str_edificio += '<td class="list" style="font-size:12px;color:#999999;padding:0 15px;">' + datos[i]['des_estado'] + '</td>';
                        str_edificio += '<td class="list"><span class="searchsubmit" style="float: right;" onclick="editarEdificio(\'' + datos[i]['idedificio'] + '\',\'' + datos[i]['x'] + '\',\'' + datos[i]['y'] + '\')"><a href="javascript:void(0)">Editar edif.</a></span></td>';
                        str_edificio += '</tr>';
                    }

                }else {
                    str_edificio = '<tr class="NOlnk"><td><span style="font-size:12px;color:#999999;font-weight:bold;">Ningun registro encontrado.</span></td></tr>';
                }

                $("#num_resultados").show();
                $("#total_resultados").html(datos.length);

                $("#tblPrincipal").empty();
                $("#tblPrincipal").append(str_edificio);
                $("#tblLeyenda").show();
                loader("end");
                $("#btnBuscar").attr('disabled', '');
                
            },
            error: function() {
                alert("Se ha producido un error, vuelva a intentarlo mas tarde.");
                loader("end");
                $("#num_resultados").hide();
                $("#btnBuscar").attr('disabled', '');
                $("#tblPrincipal").empty();
                $("#tblLeyenda").hide();
            }
        });

    }
    
});
</script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="divResultados">
    
    <?php include $dirModulo . 'm.header.php';?>


    <table id="tblPrincipal2" class="principal" width="100%">
    <tr>
        <td class="titulo" colspan="2">Buscar edificio</td>
    </tr>
    </table>
    

    <table id="tblPrincipal2" class="principal" width="100%">
    <tr>
        <td>
            Item: <input type="text" name="item" id="item" size="10" style="font-size:16px; height:25px; width:120px;" />
            <input type="button" class="searchsubmit" name="btnBuscar" id="btnBuscar" value="Buscar" onclick="buscarEdificio()" /> &nbsp; 
        </td>
    </tr>
    <tr>
        <td style="font-size:12px;font-weight:bold;"><div id="num_resultados" style="display:none;">Encontrados (<span id="total_resultados">0</span>)</div></td>
    </tr>
    </table>

    <div style="height:10px;"></div>

    <table border="0" id="tblPrincipal" class="principal" style="border-top:1px solid #DBDBDB;" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="5">&nbsp;</td>
    </tr>
    </table>
    
    <br />
    <table border="0" id="tblLeyenda" class="principal" style="display:none;" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <fieldset class="m_leyenda">
                <legend>Leyenda</legend>
                <div style="float:left;width:10px;height:20px;background-color:#ff0000;"></div> 
                <div style="float:left;padding:5px;font-size:12px;">Edificio tiene x,y</div>
            </fieldset>
        </td>
    </tr>
    </table>

</div>

    
<!-- Divs efecto Loader -->
<div id="fongoLoader" class="bgLoader">
    <div class="imgLoader" >
        <span>Cargando...</span>
        <!--<span style="font-size: 9px; color: blue;" id="cancel_loader" ><a href="javascript:void(0)" onclick="loader('end')">Cancelar[X]</a></span>-->
    </div>
</div>
<!-- fin  -->

</body>
</html>