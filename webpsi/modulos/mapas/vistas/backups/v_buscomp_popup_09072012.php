<?php
require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');

    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }

    return '#' . $color;
}


if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'search' ) {

    $objTerminales  = new Data_FfttTerminales();
    $objCms         = new Data_FfttCms();
    $objCapas       = new Data_FfttCapas();
    $objUbigeo      = new Data_Ubigeo();
    $objDuna        = new Data_DunaAdsl();

    $arrDataMarkers    = array();
    $arrDataMarkersImg = array();
    
    //array para depositar las rutas
    $arrRutas = array();
    
    //marker seleccionado -> icon por default
    $arrDataMarkersImg['pto'] = 'markergreen.png';
    $arrDataMarkers[] = array(
        'title'     => 'pto',
        'y'         => $_REQUEST['y'],
        'x'         => $_REQUEST['x'],
        'i'         => 1,
        'detalle'   => 'Punto seleccionado',
        'tip'       => 'pto'
    );
    
    $i = 2;
    if ( isset($_POST['comp']['trm']) ) {

        $distancia = ( isset($_POST['distancia']['trm']) ) ? 
            $_POST['distancia']['trm'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['trm']) ) ? 
            $_POST['num_compo']['trm'] : 10;
        $capAtenua = ( isset($_POST['cap_ate']['trm']) ) ? 
            $_POST['cap_ate']['trm'] : 'capacidad';
            
        $arrDataMarkersImg['trm'] = 'mobilephonetower.png';
                
        //carga datos de terminales
        $arrTerminales = $objTerminales->getTerminalesByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        
        //wsandoval 28/06/2012
        //Opciones para modulo de Georeferencia
        $objUsuario = new Data_Usuario();
        
        $objUsuario = $objUsuario->offsetGet(
            $conexion, $_SESSION['USUARIO']->__get('_idUsuario')
        );
        $flagGeoRef = $objUsuario->__get('_flagGeoRef');
        $arrGeoRef = unserialize($flagGeoRef);
        //fin wsandoval 28/06/2012

        foreach ($arrTerminales as $terminales) {
    
            if ( $terminales['y'] != '' && $terminales['x'] != '' ) {

                //determinamos el color de la caja (capacidad/atenuacion)
                //colores de la caja (Por capacidad)
                //1: Verde
                //2: Amarillo
                //3: Rojo
                
                //colores de la caja (Por atenuacion)
                //1: Verde
                //2: Amarillo
                //3: Rojo
                //4: Blanco
                //5: Lila
                
                $colorCaja = 1;
                if ( $capAtenua == 'capacidad' ) {
                    if ( $terminales['qparlib'] == 0 ) {
                        $colorCaja = 3;
                    } else if ( $terminales['qparlib'] > 0 && 
                            $terminales['qparlib'] < 3 ) {
                        $colorCaja = 2;
                    } else if ( $terminales['qparlib'] > 2 ) {
                        $colorCaja = 1;
                    }
                } else if ( $capAtenua == 'atenuacion' ) {
                    if ( $terminales['atenuacion'] == '' ) {
                        $colorCaja = 4;
                    } else if ( $terminales['atenuacion'] >= 0 && 
                            $terminales['atenuacion'] < 8 ) {
                        $colorCaja = 4;
                    } else if ( $terminales['atenuacion'] >= 8 &&
                            $terminales['atenuacion'] < 61 ) {
                        $colorCaja = 1;
                    } else if ( $terminales['atenuacion'] >= 61 && 
                            $terminales['atenuacion'] < 66 ) {
                        $colorCaja = 2;
                    } else if ( $terminales['atenuacion'] >= 66 ) {
                        $colorCaja = 3;
                    }
                }
                //FIN determinamos el color de la caja (capacidad/atenuacion)
                
                
                $tieneAccesoPorEecc = 0;
                if ( !empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
                    foreach ( $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa ) {
                        if ( $objEmpresa->__get('_isEecc') == '1' ) {
                            $tieneAccesoPorEecc = 1;
                            break;
                        }
                    }
                }
                
                $tieneAccesoPorArea = 0;
                if ( ($_SESSION['USUARIO']->__get('_idArea') == '5' ||
                        $_SESSION['USUARIO']->__get('_idArea') == '6' ||
                        $_SESSION['USUARIO']->__get('_idArea') == '18') ) {
                    $tieneAccesoPorArea = 1;
                }
                
                
                
                $detalle = '<b>Zonal:</b> ' . $terminales['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $terminales['mdf'] . '<br />';
                if ( $terminales['tipo_red'] == 'D' ) {
                    $detalle .= '<b>Cable:</b> ' . $terminales['cable'] . '<br />';
                } elseif ( $terminales['tipo_red'] == 'F' ) {
                    $detalle .= '<b>Armario:</b> ' . $terminales['armario'] . '<br />';
                }
                $detalle .= '<b>Caja:</b> ' . $terminales['caja'] . '<br />';
                $detalle .= '<b>Capacidad:</b> ' . $terminales['qcapcaja'] . '<br />';
                $detalle .= '<b>Pares libres:</b> ' . $terminales['qparlib'] . '<br />';
                $detalle .= '<b>Pares reserv.:</b> ' . $terminales['qparres'] . '<br />';
                $detalle .= '<b>Pares distrib.:</b> ' . $terminales['qdistrib'] . '<br />';
                $detalle .= '<b>Direccion Caja:</b> ' . $terminales['direccion'] . '<br />';
                $detalle .= '<b>Atenuacion:</b> ' . $terminales['atenuacion'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $terminales['x'] . ',' . $terminales['y'] . '<br />';
                $detalle .= '<b>Distancia al punto seleccionado:</b> ' . 
                    number_format(($terminales['distance']*1000), 2, '.', ' ') . ' m.<br />';
                
                if ( isset($arrGeoRef['verClientes']) && $arrGeoRef['verClientes'] == '1' ) {
                    $detalle .= '<a title="Ver clientes de este terminal: ' . 
                        $terminales['caja'] . '" href="javascript:clientesTerminal(\''. $terminales['zonal1'] .'\',\''. $terminales['mdf'] .'\',\''. $terminales['cable'] .'\',\''. $terminales['armario'] .'\',\''. $terminales['caja'] .'\',\''. $terminales['tipo_red'] .'\');">Ver clientes</a>';
                }
                
                if ( ( $tieneAccesoPorEecc && $tieneAccesoPorArea ) || 
                        $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spu' ) {
                    $detalle .= '&nbsp; <a title="Asignar terminal: ' . $terminales['caja'] . 
                        '" href="javascript:agregarTerminalARuta(\''. $terminales['mtgespktrm'] .'\',\''. $terminales['zonal'] .'\',\''. $terminales['mdf'] .'\',\''. $terminales['cable'] .'\',\''. $terminales['armario'] .'\',\''. $terminales['caja'] .'\',\''. $terminales['tipo_red'] .'\',\''. $terminales['x'] .'\',\''. $terminales['y'] .'\');">Asignar terminal</a>';
                }
                
                if ( isset($arrGeoRef['masInfoTerminal']) && $arrGeoRef['masInfoTerminal'] == '1' ) {
                    $detalle .= '&nbsp;<a title="Ver informacion adicionl del terminal: ' .
                        $terminales['caja'] . '" href="javascript:masInformacionTerminal(\''. $terminales['zonal'] .'\',\''. $terminales['mdf'] .'\',\''. $terminales['cable'] .'\',\''. $terminales['armario'] .'\',\''. $terminales['caja'] .'\');">Mas informacion terminal</a>';
                }
                    
                $arrDataMarkers[] = array(
                    'fftt'      => 'S',
                    'dir'       => 'terminal',
                    'title'     => $terminales['caja'],
                    'estado'    => '1',
                    'colorTrm'  => $colorCaja,
                    'qparlib'   => $terminales['qparlib'],
                    'y'         => $terminales['y'],
                    'x'         => $terminales['x'],
                    'i'         => $i,
                    'detalle'   => $detalle,
                    'tip'       => 'trm'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['exa']) ) {
        $distancia  = ( isset($_POST['distancia']['exa']) ) ? 
            $_POST['distancia']['exa'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['exa']) ) ? 
            $_POST['num_compo']['exa'] : 10;
    
        $arrDataMarkersImg['exa'] = 'powersubstation.png';
        
        $objExcesoAcometida = new Data_ExcesoAcometida();
    
        //carga datos de los registros de exceso de acometida
        $arrObjExcesoAcometida = $objExcesoAcometida->obtenerPorXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], $distancia, $numCompo
        );

        
        foreach ($arrObjExcesoAcometida as $objExcAcometida) {
    
            if ( $objExcAcometida->__get('_clienteX') != '' && 
                $objExcAcometida->__get('_clienteY') != '' ) {
    
                $detalle = '<b>Codigo:</b> ' . $objExcAcometida->__get('_codigo') . '<br />';
                $detalle .= '<b>Zonal:</b> ' . $objExcAcometida->__get('_zonal') . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $objExcAcometida->__get('_mdf') . '<br />';
                $detalle .= '<b>Cable/Armario:</b> ' . $objExcAcometida->__get('_cable') . 
                    $objExcAcometida->__get('_armario') . '<br />';
                $detalle .= '<b>Terminal:</b> ' . $objExcAcometida->__get('_terminal') . '<br />';
                $detalle .= '<b>Tipo Red:</b> ' . $objExcAcometida->__get('_tipoRed') . '<br />';
                $detalle .= '<b>Distancia acometida (m.):</b> ' . 
                    $objExcAcometida->__get('_distanciaAcometida') . '<br />';
                $detalle .= '<b>Tipo pedido:</b> ' . $objExcAcometida->__get('_tipoPedido') . '<br />';
                $detalle .= '<b>Pedido:</b> ' . $objExcAcometida->__get('_pedido') . '<br /><br />';
                $detalle .= '<b>X,Y cliente:</b> ' . $objExcAcometida->__get('_clienteX') . ',' . 
                    $objExcAcometida->__get('_clienteY') . '<br />';
    
                //registrando los clientes de excesos de acometida
                $arrDataMarkers[] = array(
                    'fftt'      => 'N',
                    'dir'       => 'excesoacometida',
                    'title'     => $objExcAcometida->__get('_codigo'),
                    'estado'    => '1',
                    'y'         => $objExcAcometida->__get('_clienteY'),
                    'x'         => $objExcAcometida->__get('_clienteX'),
                    'i'         => $i,
                    'detalle'   => $detalle,
                    'tip'       => 'exa'
                );
                $i++;
                
                $colorCaja = 1;
                //registrando los terminales de excesos de acometida
                $arrDataMarkers[] = array(
                    'fftt'      => 'S',
                    'dir'       => 'terminal',
                    'title'     => $objExcAcometida->__get('_terminal'),
                    'estado'    => '1',
                    'colorTrm'  => $colorCaja,
                    'qparlib'   => '',
                    'y'         => $objExcAcometida->__get('_terminalY'),
                    'x'         => $objExcAcometida->__get('_terminalX'),
                    'i'         => $i,
                    'detalle'   => $objExcAcometida->__get('_terminal'),
                    'tip'       => 'trm'
                );
                $i++;
                
                
                //armado de las rutas de los registros de exceso de acometida
                //lleno el array para la ruta
                if ( trim($objExcAcometida->__get('_ruta')) != '' ) {
                    //capturando los X,Y de los elementos seleccionados
                    $arrPuntosXyCapaRuta = unserialize($objExcAcometida->__get('_ruta'));
                    $arrRutas[] = array(
                        'coords' => $arrPuntosXyCapaRuta,
                        'color'  => generaColorPoligono(),
                        'tipo'   => 'Exceso de Acometida',
                        'nombre' => $objExcAcometida->__get('_codigo')
                    );
                }
            }
        }
    }
    
    if ( isset($_POST['comp']['arm']) ) {
        $distancia  = ( isset($_POST['distancia']['arm']) ) ? $_POST['distancia']['arm'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['arm']) ) ? $_POST['num_compo']['arm'] : 10;
    
        $arrDataMarkersImg['arm'] = 'powersubstation.png';
        
        //carga datos de armarios
        $arrArmarios = $objTerminales->getArmariosByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrArmarios as $armarios) {
    
            if ( $armarios['y'] != '' && $armarios['x'] != '' ) {

                $detalle = '<b>Zonal:</b> ' . $armarios['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $armarios['mdf'] . '<br />';
                $detalle .= '<b>Armario:</b> ' . $armarios['armario'] . '<br />';
                $detalle .= '<b>Capacidad:</b> ' . $armarios['qcaparmario'] . '<br />';
                $detalle .= '<b>Pares libres:</b> ' . $armarios['qparlib'] . '<br />';
                $detalle .= '<b>Pares reserv.:</b> ' . $armarios['qparres'] . '<br />';
                $detalle .= '<b>Pares distrib.:</b> ' . $armarios['qdistrib'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $armarios['x'] . ',' . $armarios['y'] . '<br />';
            
                $arrDataMarkers[] = array(
                    'fftt'      => 'S',
                    'dir'       => 'armario',
                    'title'     => $armarios['armario'],
                    'estado'    => '1',
                    'y'         => $armarios['y'],
                    'x'         => $armarios['x'],
                    'i'         => $i,
                    'detalle'   => $detalle,
                    'tip'       => 'arm'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['mdf']) ) {
        $distancia  = ( isset($_POST['distancia']['mdf']) ) ? $_POST['distancia']['mdf'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['mdf']) ) ? $_POST['num_compo']['mdf'] : 10;
    
        $arrDataMarkersImg['mdf'] = 'tent.png';
    
        //carga datos de mdfs
        $arrMdfs = $objTerminales->getMdfsByXy($conexion, $_REQUEST['x'], $_REQUEST['y'], $distancia, $numCompo);
        foreach ($arrMdfs as $mdfs) {
    
            if ( $mdfs['y'] != '' && $mdfs['x'] != '' ) {
            
                $detalle = '<b>Zonal:</b> ' . $mdfs['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $mdfs['mdf'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $mdfs['direccion'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $mdfs['x'] . ',' . $mdfs['y'] . '<br />';
            
                $arrDataMarkers[] = array(
                    'fftt'     => 'S',
                    'dir'      => 'mdf',
                    'title'    => $mdfs['mdf'],
                    'estado'   => $mdfs['estado'],
                    'y'        => $mdfs['y'],
                    'x'        => $mdfs['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'mdf'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['trb']) ) {
        $distancia  = ( isset($_POST['distancia']['trb']) ) ? $_POST['distancia']['trb'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['trb']) ) ? $_POST['num_compo']['trb'] : 10;
    
        $arrDataMarkersImg['trb'] = 'tent.png';
    
        //carga datos de taps
        $arrTap = $objCms->getTrobasByXy($conexion, $_REQUEST['x'], $_REQUEST['y'], $distancia, $numCompo);
        foreach ($arrTap as $taps) {
    
            if ( $taps['y'] != '' && $taps['x'] != '' ) {
            
                $detalle = '<b>Nodo:</b> ' . $taps['nodo'] . '<br />';
                $detalle .= '<b>Troba:</b> ' . $taps['troba'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $taps['x'] . ',' . $taps['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($taps['distance'], 2) . 'km. <br />';
            
                $arrDataMarkers[] = array(
                    'fftt'        => 'S',
                    'dir'        => 'trb',
                    'title'     => $taps['troba'],
                    'estado'     => '1',
                    'y'             => $taps['y'],
                    'x'             => $taps['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'tip'        => 'trb'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['tap']) ) {
                $distancia  = ( isset($_POST['distancia']['tap']) ) ? $_POST['distancia']['tap'] : 5;
                $numCompo  = ( isset($_POST['num_compo']['tap']) ) ? $_POST['num_compo']['tap'] : 10;
    
        $arrDataMarkersImg['tap'] = 'tent.png';
    
        //carga datos de taps
        $arrTap = $objCms->getTapsByXy($conexion, $_REQUEST['x'], $_REQUEST['y'], $distancia, $numCompo);
        foreach ($arrTap as $taps) {
    
            if ( $taps['y'] != '' && $taps['x'] != '' ) {
            
                $detalle = '<b>Nodo:</b> ' . $taps['nodo'] . '<br />';
                $detalle .= '<b>Troba:</b> ' . $taps['troba'] . '<br />';
                $detalle .= '<b>Amplificador:</b> ' . $taps['amplificador'] . '<br />';
                $detalle .= '<b>Tap:</b> ' . $taps['tap'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $taps['x'] . ',' . $taps['y'] . '<br />';
            
                $arrDataMarkers[] = array(
                    'fftt'     => 'S',
                    'dir'      => 'mdf',
                    'title'    => $taps['tap'],
                    'estado'   => '1',
                    'y'        => $taps['y'],
                    'x'        => $taps['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'tap'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['edi']) ) {
        $distancia  = ( isset($_POST['distancia']['edi']) ) ? $_POST['distancia']['edi'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['edi']) ) ? $_POST['num_compo']['edi'] : 10;
    
        $arrDataMarkersImg['edi'] = 'edificio';
    
        //carga datos de edificios
        $arrEdificios = $objTerminales->getEdificiosByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrEdificios as $edificios) {
    
            if ( $edificios['y'] != '' && $edificios['x'] != '' ) {
            
                $detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . 
                    $edificios['foto1'] . '&dircomp=edi&w=120"><br />';
                $detalle .= '<b>Edificio:</b> ' . $edificios['item'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $edificios['x'] . ',' . $edificios['y'] . '<br />';
                $detalle .= '<a title="Ver Edificio" href="javascript:detalleEdificioParent(\''. $edificios['idedificio'] .'\');">Ver Edificio</a>';
                
                $arrDataMarkers[] = array(
                    'fftt'     => 'N',
                    'dir'      => 'edificio',
                    'title'    => $edificios['item'],
                    'estado'   => $edificios['estado'],
                    'y'        => $edificios['y'],
                    'x'        => $edificios['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'edi'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['com']) ) {
        $distancia  = ( isset($_POST['distancia']['com']) ) ? $_POST['distancia']['com'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['com']) ) ? $_POST['num_compo']['com'] : 10;
    
        $arrDataMarkersImg['com'] = 'competencia.png';
    
        //carga datos de competencias
        $arrCompetencias = $objTerminales->getCompetenciasByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrCompetencias as $competencias) {
    
            if ( $competencias['y'] != '' && $competencias['x'] != '' ) {
            
                $detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . 
                    $competencias['foto1'] . '&dircomp=com&w=120"><br />';
                $detalle .= '<b>Competencia:</b> ' . $competencias['idcompetencia'] . '<br />';
                $detalle .= '<b>URA:</b> ' . $competencias['ura'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $competencias['direccion'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $competencias['x'] . ',' . $competencias['y'] . '<br />';
            
                $arrDataMarkers[] = array(
                    'fftt'     => 'N',
                    'dir'      => 'competencia',
                    'title'    => $competencias['idcompetencia'],
                    'estado'   => 'estado',
                    'y'        => $competencias['y'],
                    'x'        => $competencias['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'com'
                );
                $i++;
            }
        }
    }
        
    //CAPA: TPI
    if ( isset($_POST['comp']['tpi']) ) {
        $distancia  = ( isset($_POST['distancia']['tpi']) ) ? $_POST['distancia']['tpi'] : 10;
        $numCompo  = ( isset($_POST['num_compo']['tpi']) ) ? $_POST['num_compo']['tpi'] : 10;
        
        $objTpi = new Data_Tpi();

        $arrDataMarkersImg['com'] = 'tpi-3.gif';

        //carga datos de tpi
        $arrTpi = $objTpi->getTpiByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrTpi as $tpi) {

            if ( $tpi['y'] != '' && $tpi['x'] != '' ) {

                $detalle = '<b>TELEFONO:</b> ' . $tpi['telefono'] . '<br />';
                $detalle .= '<b>DISTRITO:</b> ' . $tpi['distrito'] . '<br />';
                $detalle .= '<b>TIPO TUP:</b> ' . $tpi['tipotup'] . '<br />';
                $detalle .= '<b>SEGMENTO GESTION:</b> ' . $tpi['segmentogestion'] . '<br />';
                $detalle .= '<b>ARPU:</b> ' . $tpi['i201110'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $tpi['x'] . ',' . $tpi['y'] . '<br />';

                $arrDataMarkers[] = array(
                    'fftt'     => 'N',
                    'dir'      => 'tpi',
                    'title'    => $tpi['telefono'],
                    'estado'   => $tpi['segmentogestion'],
                    'y'        => $tpi['y'],
                    'x'        => $tpi['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'tpi'
                );
                $i++;
            }
        }
    }
    
    //CAPA: POSIBLES CLIENTES
    if ( isset($_POST['comp']['pcl']) ) {
        $distancia  = ( isset($_POST['distancia']['pcl']) ) ? $_POST['distancia']['pcl'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['pcl']) ) ? $_POST['num_compo']['pcl'] : 10;

        $arrDataMarkersImg['com'] = 'posiblecliente-3.gif';

        //carga datos de competencias
        $arrPosiblesClientes = $objTerminales->getPosiblesclientesByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrPosiblesClientes as $posiblecliente) {

            if ( $posiblecliente['y'] != '' && $posiblecliente['x'] != '' ) {

                $detalle = '<b>Posible cliente:</b> ' . $posiblecliente['APELLIDO_FACT'] . ' ' . 
                    $posiblecliente['APE_MAT_FACT'] . ' ' . $posiblecliente['NOMBRE_FACT'] . '<br />';
                $detalle .= '<b>ZONAL:</b> ' . $posiblecliente['ZONAL'] . '<br />';
                $detalle .= '<b>MDF:</b> ' . $posiblecliente['MDF'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $posiblecliente['DIRECCION'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $posiblecliente['x'] . ',' . $posiblecliente['y'] . '<br />';

                $arrDataMarkers[] = array(
                    'fftt'    => 'N',
                    'dir'     => 'posiblesclientes',
                    'title'   => $posiblecliente['APELLIDO_FACT'] . ' ' + $posiblecliente['APE_MAT_FACT'] . 
                        ' ' . $posiblecliente['NOMBRE_FACT'],
                    'estado'  => 'estado',
                    'y'       => $posiblecliente['y'],
                    'x'       => $posiblecliente['x'],
                    'i'       => $i,
                    'detalle' => $detalle,
                    'tip'     => 'pcl'
                );
                $i++;
            }
        }
    }
        
    //CAPA: LOCALIDADES
    if ( isset($_POST['comp']['loc']) ) {
        $distancia  = ( isset($_POST['distancia']['loc']) ) ? $_POST['distancia']['loc'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['loc']) ) ? $_POST['num_compo']['loc'] : 10;

        $arrDataMarkersImg['com'] = 'localidades.png';

        //carga datos de localidades
        $arrayLocalidades = $objUbigeo->buscarLocalidadPorXY(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrayLocalidades as $localidades) {

            if ( $localidades['y_coords'] != '' && $localidades['x_coords'] != '' ) {

                $detalle = '<b>LOCALIDAD:</b> ' . $localidades['localidad'] . '<br />';
                $detalle .= '<b>DEPARTAMENTO:</b> ' . $localidades['departamento'] . '<br />';
                $detalle .= '<b>PROVINCIA:</b> ' . $localidades['provincia'] . '<br />';
                $detalle .= '<b>DISTRITO:</b> ' . $localidades['distrito'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $localidades['x_coords'] . ',' . $localidades['y_coords'] . '<br />';

                $arrDataMarkers[] = array(
                    'fftt'    => 'N',
                    'dir'     => 'localidades',
                    'title'   => $localidades['localidad'],
                    'estado'  => '1',
                    'y'       => $localidades['y_coords'],
                    'x'       => $localidades['x_coords'],
                    'i'       => $i,
                    'detalle' => $detalle,
                    'tip'     => 'loc'
                );
                $i++;
            }
        }
    }
        
    //CAPA: DUNA ADSL
    if ( isset($_POST['comp']['dad']) ) {
        $distancia  = ( isset($_POST['distancia']['dad']) ) ? $_POST['distancia']['dad'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['dad']) ) ? $_POST['num_compo']['dad'] : 10;

        $arrDataMarkersImg['com'] = 'pirata-1.gif';

        //carga datos de piratas
        $arrPirata = $objDuna->buscarPirataPorXY(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrPirata as $pirata) {

            if ( $pirata['y'] != '' && $pirata['x'] != '' ) {

                $detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . 
                    $pirata['foto1'] . '&dircomp=dad&w=120"><br />';
                $detalle .= '<b>CODIGO:</b> ' . $pirata['codigo'] . '<br />';
                $detalle .= '<b>DEPARTAMENTO:</b> ' . $pirata['departamento'] . '<br />';
                $detalle .= '<b>PROVINCIA:</b> ' . $pirata['provincia'] . '<br />';
                $detalle .= '<b>DISTRITO:</b> ' . $pirata['distrito'] . '<br />';
                $detalle .= '<b>DIRECCION:</b> ' . $pirata['direccion'] . '<br />';
                $detalle .= '<b>DIRECCION REF.:</b> ' . $pirata['direccion_referencial'] . '<br />';
                $detalle .= '<b>TELEFONO REF.:</b> ' . $pirata['telefono_referencial'] . '<br />';
                $detalle .= '<b>TELEFONO DETECTADO:</b> ' . $pirata['telefono_detectado'] . '<br />';
                $detalle .= '<b>FFTT:</b> ' . $pirata['fftt'] . '<br />';
                $detalle .= '<b>ESTADO:</b> <span style="color:#ff0000;font-weight:bold;">' . 
                    $pirata['estado_pirata'] . '</span><br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $pirata['x'] . ',' . $pirata['y'] . '<br />';

                $arrDataMarkers[] = array(
                    'fftt'    => 'N',
                    'dir'     => 'dunaadsl',
                    'title'   => $pirata['direccion'],
                    'estado'  => '1',
                    'y'       => $pirata['y'],
                    'x'       => $pirata['x'],
                    'i'       => $i,
                    'detalle' => $detalle,
                    'tip'     => 'dad'
                );
                $i++;
            }
        }
    }
    
    if ( isset($_POST['comp']['esb']) ) {
        $distancia  = ( isset($_POST['distancia']['esb']) ) ? $_POST['distancia']['esb'] : 5;
        $numCompo  = ( isset($_POST['num_compo']['esb']) ) ? $_POST['num_compo']['esb'] : 10;
    
        $arrDataMarkersImg['esb'] = 'estacionbase';
    
        //carga datos de edificios
        $arrEdificios = $objTerminales->getEstacionbasesByXy(
            $conexion, 
            $_REQUEST['x'], 
            $_REQUEST['y'], 
            $distancia, 
            $numCompo
        );
        foreach ($arrEdificios as $edificios) {
    
            if ( $edificios['y'] != '' && $edificios['x'] != '' ) {
            
                $detalle = '<b>Estacion Base:</b> ' . $edificios['nombre'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $edificios['direccion'] . '<br />';
                $detalle .= '<b>Departamento:</b> ' . $edificios['departamento'] . '<br />';
                $detalle .= '<b>Provincia:</b> ' . $edificios['provincia'] . '<br />';
                $detalle .= '<b>Distrito:</b> ' . $edificios['distrito'] . '<br />';
                $detalle .= '<b>X,Y:</b> ' . $edificios['x'] . ',' . $edificios['y'] . '<br />';
                
                $arrDataMarkers[] = array(
                    'fftt'     => 'N',
                    'dir'      => 'estacionbase',
                    'title'    => $edificios['nombre'],
                    'estado'   => 'estado',
                    'y'        => $edificios['y'],
                    'x'        => $edificios['x'],
                    'i'        => $i,
                    'detalle'  => $detalle,
                    'tip'      => 'esb'
                );
                $i++;
            }
        }
    }
    
    
    if ( isset($_POST['capa']) && !empty($_POST['capa']) ) {
    
        $arrcapas = $objCapas->getCapas($conexion);
        $arrayCapa = array();
        foreach ( $arrcapas as $capa ) {
            $arrayCapa[$capa['idcapa']] = array(
                'nombre'    => $capa['nombre'],
                'abv_capa'  => $capa['abv_capa'],
                'ico'       => $capa['ico']
            );
        }
        
        foreach ( $_POST['capa'] as $idCapa => $abvCapa ) {
        
            //almaceno las imagenes segun la capa
            $arrDataMarkersImg[$abvCapa] = $arrayCapa[$idCapa]['ico'];
        
            //carga de campos segun la capa
            $arrCamposCapa = $objCapas->getCamposByCapa($conexion, $idCapa);
            $arrDataCampos = array();
            foreach ( $arrCamposCapa as $campoCapa ) {
                $arrDataCampos['campo' . $campoCapa['campo_nro']] = $campoCapa['campo'];
            }

            $distancia  = ( isset($_POST['distancia'][$idCapa]) ) ? $_POST['distancia'][$idCapa] : 5;
            $numCompo  = ( isset($_POST['num_compo'][$idCapa]) ) ? $_POST['num_compo'][$idCapa] : 10;

            //carga datos de capa = casa, tienda, cabina
            $arrCapas = $objCapas->getCapasByXy(
                $conexion, 
                $idCapa, 
                $arrDataCampos, 
                $_REQUEST['x'], 
                $_REQUEST['y'], 
                $distancia, 
                $numCompo
            );
            foreach ($arrCapas as $capas) {

                $detalle = '<img src="../../../pages/imagenComponente.php?imgcomp=' . $capas['foto1'] . 
                    '&dircomp=' . $arrayCapa[$idCapa]['abv_capa'] . '&w=120"><br />';
                
                foreach ($arrDataCampos as $nroCampo => $campo) {
                    $detalle .= '<b>' . $campo . ':</b> ' . $capas[$nroCampo] . '<br />';
                }
                $detalle .= '<b>X,Y:</b> ' . $capas['x'] . ',' . $capas['y'] . '<br />';
    
                if ( $capas['y'] != '' && $capas['x'] != '' ) {
                    $arrDataMarkers[] = array(
                        'title'     => $capas['campo1'],
                        'y'         => $capas['y'],
                        'x'         => $capas['x'],
                        'i'         => $i,
                        'detalle'   => utf8_encode($detalle),
                        'tip'       => $abvCapa
                    );
                    $i++;
                }
            }    
        }
    }
    
    
    $arrCapaSelected = array(
        'capa'    => '',
        'zoom_sel' => 11
    );
    
    if ( isset($_POST['capa_selected']) && $_POST['capa_selected'] != '' ) {
        $arrCapaSelected = array(
            'capa'    => $_POST['capa_selected'],
            'zoom_sel' => $_POST['zoom_selected']
        );
    }
    
    
    $arrDataResult = array(
        'sites'     => $arrDataMarkers,
        'xy_rutas'  => $arrRutas,
        'punto_sel' => array('x' => $_REQUEST['x'], 'y' => $_REQUEST['y']),
        'capa_ico'  => $arrDataMarkersImg
    );
    
    echo json_encode($arrDataResult);
    exit;

} else {

    $objCapas = new Data_FfttCapas();
    
    $arrCapas = $_SESSION['USUARIO_CAPAS'];

}
?>


<script type="text/javascript">

var capa_ico;
var center_map;
var markers_arr = parent.ifrMain.markers_arr;

$(document).ready(function() {

    $("#cancelar").click(function() {
        $("#childModal").dialog('close');
    });
        
    $("#sel").click(function() {
        if ($("#sel").is(':checked')) { 
            $(".capas").attr('checked', 'checked');
            $("#selTodos").html('Quitar todos');
            $("select.capas").attr('disabled', '');
        }else {  
            $(".capas").attr('checked', '');
            $("#selTodos").html('Seleccionar todos');
            $("select.capas").attr('disabled', 'disabled');
        }  
    });
        
    $(".capas").click(function() {
        var id = $(this).attr('id');

        if ($(this).is(':checked')) { 
            $("#dis" + id).attr('disabled', '');
            $("#num" + id).attr('disabled', '');
            $("#cap" + id).attr('disabled', '');
        }else {  
            $("#dis" + id).attr('disabled', 'disabled');
            $("#num" + id).attr('disabled', 'disabled');
            $("#cap" + id).attr('disabled', 'disabled');
        }  

    });
    
});

</script>
<style>
.tb_listado tbody td {
    border-bottom: 1px solid #CDCDCD;
    padding: 4px;
    text-align: left;
}
</style>


<div style="text-align: left;">

<h3>X,Y seleccionado:</h3>
<?php echo $_REQUEST['x'] . ',' . $_REQUEST['y'] ?>
<br />&nbsp;

<h3>Seleccione las capas que desea buscar:</h3>
<div id="divSelCapas" style="overflow: auto; height: 300px; width: 95%;">
<table class="tb_listado" border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
<tbody>
<tr>
    <td width="5%"><input type="checkbox" class="capas" id="sel" value="" /></td>
    <td width="40%"><span id="selTodos"><b>Seleccionar todos</b></span></td>
    <td width="22%"><b>Distancia</b></td>
    <td width="33%"><b>N&uacute;mero de componentes</b></td>
</tr>
<?php
if ( !empty($arrCapas) ) {
foreach ($arrCapas as $objCapa) {
    if ( $objCapa->__get('_indCapa') == 'S' ) {
    if ( $objCapa->__get('_tieneTabla') == 'S' ) {
    ?>
        <tr>
            <td><input id="_<?php echo $objCapa->__get('_abvCapa')?>" type="checkbox" class="capas" name="comp[<?php echo $objCapa->__get('_abvCapa')?>]" value="<?php echo $objCapa->__get('_abvCapa')?>" /></td>
            <td><?php echo $objCapa->__get('_nombre')?></td>
            <td>
                <select id="dis_<?php echo $objCapa->__get('_abvCapa')?>" disabled="disabled" class="capas" name="distancia[<?php echo $objCapa->__get('_abvCapa')?>]">
                    <option value="5">5 Km.</option>
                    <option value="10">10 Km.</option>
                </select>
            </td>
            <td>
                <select id="num_<?php echo $objCapa->__get('_abvCapa')?>" disabled="disabled" class="capas" name="num_compo[<?php echo $objCapa->__get('_abvCapa')?>]">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">50</option>
                </select>
                
                <?php
                if ( $objCapa->__get('_abvCapa') == 'trm' ) {
                ?>
                    &nbsp; &nbsp;
                    <select id="cap_<?php echo $objCapa->__get('_abvCapa')?>" name="cap_ate[<?php echo $objCapa->__get('_abvCapa')?>]" class="capas" disabled="disabled" >
                        <option value="capacidad">Por capacidad</option>
                        <option value="atenuacion">Por atenuaci&oacute;n</option>
                    </select>
                <?php 
                }
                ?>
                
            </td>
        </tr>
    <?php
    } else {
    ?>
            <tr>
                <td><input id="_<?php echo $objCapa->__get('_abvCapa')?>" type="checkbox" class="capas" name="capa[<?php echo $objCapa->__get('_idCapa')?>]" value="<?php echo $objCapa->__get('_abvCapa')?>" /></td>
                <td><?php echo $objCapa->__get('_nombre')?></td>
                <td>
                    <?php
                    if ( $objCapa->__get('_abvCapa') == 'pcb' ) {
                    ?>
                    <select id="dis_<?php echo $objCapa->__get('_abvCapa')?>" disabled="disabled" class="capas" name="distancia[<?php echo $objCapa->__get('_idCapa')?>]">
                        <option value="5">5 Km.</option>
                        <option value="10">10 Km.</option>
                        <option value="1000">1000 Km.</option>
                    </select>
                    <?php
                    } else {
                    ?>
                    <select id="dis_<?php echo $objCapa->__get('_abvCapa')?>" disabled="disabled" class="capas" name="distancia[<?php echo $objCapa->__get('_idCapa')?>]">
                        <option value="5">5 Km.</option>
                        <option value="10">10 Km.</option>
                    </select>
                    <?php
                    }
                    ?>
                </td>
                <td>
                    <select id="num_<?php echo $objCapa->__get('_abvCapa')?>" disabled="disabled" class="capas" name="num_compo[<?php echo $objCapa->__get('_idCapa')?>]">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                    </select>
                </td>
            </tr>
    <?php 
    }
    }
}
}
?>
</tbody>
</table>
    
<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td colspan="2" height="20">&nbsp;</td>
</tr>
<tr>
    <td colspan="2">
        <input type="submit" name="search" id="search" value="Buscar componentes" onclick="javascript:parent.ifrMain.btnSearchComponentes();" />
        <input type="button" id="cancelar" value="Cancelar" />
    </td>
</tr>
</table>
    
<input type="hidden" name="x" id="x" value="<?php echo $_REQUEST['x']?>" />
<input type="hidden" name="y" id="y" value="<?php echo $_REQUEST['y']?>" />
<input type="hidden" name="zoom_selected" id="zoom_selected" value="<?php echo $_POST['zoom_selected']?>" />
</div>

<div id="msgBuscomp" style="width: 300px; text-align: center; margin: 5px; color: #008800; font-weight: bold; display:none;">Buscando componentes <img src="img/loading.gif" alt="" title="" /></div>

</div>