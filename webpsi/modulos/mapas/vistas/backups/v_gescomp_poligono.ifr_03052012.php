<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>


        <script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
        <script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>


        <script type="text/javascript" src="../../../js/jquery/prettify.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery.multiselect.filter.js"></script>

        <script type="text/javascript" src="../../../modulos/maps/js/edificios.fftt.js"></script>
        <script type="text/javascript" src="../../../js/jquery/actuaciones.js"></script>

        <link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../css/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" media="all"/>
        <script type="text/javascript">
            $(function(){
                $("select.clsMultiple").multiselect().multiselectfilter();
            });
        </script>
        <script type="text/javascript">

            var myLayout;
            var map = null;
            var poligonosDibujar_array = [];
            var polilineasDibujar_array = [];

            var ptos_arr = [];
            var markers_arr = [];
            var makeMarkers_arr = [];

            var capa_exists = 0;

            $(document).ready(function () {

                $("#tabs").tabs();
    
                myLayout = $('body').layout({
                    // enable showOverflow on west-pane so popups will overlap north pane
                    west__showOverflowOnHover: true,
                    west: {size:370}
                });

                validarData=function(e){
                    var tecla = (document.all) ? e.keyCode : e.which;
                    if(tecla==17 || e.ctrlKey){
                        return false;
                    } else {
                        if (tecla==8) return true;
                        //var patron =/\w/;//valida solo letras y numeros
                        var patron = /^[A-Za-z+][\s[A-Za-z]+]*$/;
                        var te = String.fromCharCode(tecla);
                        return patron.test(te);
                    }
                }

                load_array=function(element){
                    var grupo=new Array();
                    $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
                    return grupo;
                }
        
                $('#dpto').change(function(){
                    M.buscarProvincias();
                });
        
                $('#prov').change(function(){
                    M.buscarDistritos();
                });

                cargaInicial = function() {
                    data_content = "action=cargaInicial";
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {

                            var tipoPoligonos = datos['arrTipoPoligonos'];
                            var tipoRutas = datos['arrTipoRutas'];
                            var zonales = datos['arrZonales'];
                            var distritos = datos['arrZonales'];
                            var dptos = datos['arrDptos'];
                            var provs = datos['arrProvs'];
                            var dists = datos['arrDists'];

                            //carga combo de tipo de poligonos
                            var selectTipoPoligono = '';
                            for(var i in tipoPoligonos) {
                                selectTipoPoligono += '<option value="' + tipoPoligonos[i]['idpoligono_tipo'] + '">' + tipoPoligonos[i]['nombre'] + '</option>'
                            }
                            $("#tipocapaPoligono").append(selectTipoPoligono);

                            //carga combo de tipo de rutas
                            var selectTipoRuta = '';
                            for(var i in tipoRutas) {
                                selectTipoRuta += '<option value="' + tipoRutas[i]['idpoligono_tipo'] + '">' + tipoRutas[i]['nombre'] + '</option>'
                            }
                            $("#tipocapaRuta").append(selectTipoRuta);

                            //carga combo de zonales
                            var selectZonales = '';
                            for(var i in zonales) {
                                selectZonales += '<option ' + sel + ' value="' + zonales[i]['desc_zonal'] + '">' + zonales[i]['desc_zonal'] + '</option>'
                            }
                            $("#zonal").append(selectZonales);

                            //carga combo de distritos
                            var selectDistritos = '';
                            for(var i in distritos) {
                                selectDistritos += '<option value="' + distritos[i]['distrito'] + '">' + distritos[i]['distrito'] + '</option>'
                            }
                            $("#selDistrito").append(selectDistritos);

                            //carga combo de dptos
                            var selectDptos = '';
                            for(var i in dptos) {
                                var sel = '';
                                if ( dptos[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectDptos += '<option ' + sel + ' value="' + dptos[i]['coddpto'] + '">' + dptos[i]['nombre'] + '</option>'
                            }
                            $("#dpto").append(selectDptos);

                            //carga combo de provs
                            var selectProvs = '';
                            for(var i in provs) {
                                var sel = '';
                                if ( provs[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectProvs += '<option ' + sel + ' value="' + provs[i]['codprov'] + '">' + provs[i]['nombre'] + '</option>'
                            }
                            $("#prov").append(selectProvs);

                            //carga combo de provs
                            var selectDists = '';
                            for(var i in dists) {
                                selectDists += '<option value="' + dists[i]['coddist'] + '">' + dists[i]['nombre'] + '</option>'
                            }
                            $("#dist").append(selectDists);

                        }
                    });
                }
        

                btnSearchComponentes = function() {
                    if(parent.$(".capas").is(':checked')) { 
                    } else {  
                        alert("Ingrese algun criterio de busqueda");  
                        return false;  
                    }  

                    var x = parent.$("#x").attr('value');
                    var y = parent.$("#y").attr('value');

                    var query = parent.$("input.capas, select.capas").serializeArray();

                    parent.$("#msgBuscomp").show();
                    data_content = {
                        'action'    : 'search',
                        'x'        : x,
                        'y'        : y,
                        'comp'        : {'trm':'1'}
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "v_buscomp_popup.php?action=search&x=" + x + "&y=" + y,
                        data:     query,
                        dataType: "json",
                        success: function(data) {

                            //clear markers
                            //L.clearMarkers();

                            var sites = data['sites'];
                            capa_ico = data['capa_ico'];

                            infowindow = new google.maps.InfoWindow({
                                content: "loading..."
                            });

                            if( sites.length > 0) {
                                M.setMarkers(map, sites);
                            }

                            parent.$("#msgBuscomp").hide();
                            parent.$("#childModal").dialog('close');

                        }
                    });        
                }


                var M = {
                    initialize: function() {

                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com');
                            return false;
                        }
        
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var myOptions = {
                            zoom: 11,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


                        //carga inicial de combos
                        cargaInicial();

                        google.maps.event.addListener(map, "click", function(event) {
                            //clear markers
                            M.clearMarkers();
                            M.clearMakeMarkers(); //wil
                
                            marker = new google.maps.Marker({
                                position: event.latLng,
                                map: map
                            });
                            markers_arr.push(marker);
                            var Coords = event.latLng;
                            var x = Coords.lng();
                            var y = Coords.lat();

                            //popup, pregunta si quieres buscar o agregar componente
                            M.popupAddSearchComponentes(x,y);
                        });
                    },
        
                    setMarkers: function(map, markers) {

                        var avg = {
                            lat: 0,
                            lng: 0
                        };
    
                        for (var i = 0; i < markers.length; i++) {
                            var sites = markers[i];
                            var siteLatLng = new google.maps.LatLng(sites['y'], sites['x']);

                            var ico = capa_ico[sites['tip']];


                            var icono = '';
                            var est_caja = '';
                            if( sites['tip'] == 'trm' ) {

                                if( sites['qparlib'] == 0 ) {
                                    est_caja = 3;
                                }else if( sites['qparlib'] > 0 && sites['qparlib'] < 3 ) {
                                    est_caja = 2;
                                }else if( sites['qparlib'] > 2 ) {
                                    est_caja = 1;
                                }

                                icono = "../../../img/map/terminal/trm_" + sites['title'] + "--" + est_caja + ".gif";
                            }else if( sites['tip'] == 'arm' ) {
                                icono = "../../../img/map/armario/arm_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'mdf' ) {
                                icono = "../../../img/map/mdf/mdf_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'trb' ) {
                                icono = "../../../img/map/troba/trb-r.gif";
                            }else if( sites['tip'] == 'tap' ) {
                                icono = "../../../img/map/tap/tap_" + sites['title'] + "--" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'edi' ) {
                                icono = "../../../img/map/edificio/edificio-" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'com' ) {
                                icono = "../../../img/map/competencia/" + ico;
                            }else if( sites['tip'] == 'esb' ) {
                                icono = "../../../img/map/estacionbase/esb-1.gif";
                            }else if( sites['tip'] == 'pcl' ) {
                                icono = "../../../img/map/posiblesclientes/posiblecliente-3.gif";
                            }else if( sites['tip'] == 'loc' ) {
                                icono = "../../../img/map/localidades/localidades.png";
                            }else if( sites['tip'] == 'tpi' ) {
                                icono = "../../../img/map/tpi/tpi-" + sites['estado'] + ".gif";
                            }else if( sites['tip'] == 'dad' ) {
                                icono = "../../../img/map/dunaadsl/pirata-1.gif";
                            }
                            else {
                                icono = "../../../modulos/maps/vistas/icons/" + ico;
                            }                

                            var marker = new google.maps.Marker({
                                position: siteLatLng,
                                map: map,
                                title: sites['title'],
                                zIndex: sites['i'],
                                html: sites['detalle'],
                                icon: icono
                            });

                            markers_arr.push(marker);

                            var contentString = "Some content";

                            google.maps.event.addListener(marker, "click", function () {
                                //alert(this.html);
                                infowindow.setContent(this.html);
                                infowindow.open(map, this);
                            });

                            avg.lat += siteLatLng.lat();
                            avg.lng += siteLatLng.lng();
                        }
            
                        // Center map.
                        map.setCenter(new google.maps.LatLng(avg.lat / markers.length, avg.lng / markers.length));
            
                    },
        
                    buscarProvincias: function() {
                        $("select[name=prov]").html("<option value=''>Seleccione</option>");
                        $("select[name=dist]").html("<option value=''>Seleccione</option>");
                        data_content = {
                            'action'    : 'buscarProvincias',
                            'dpto'      : $('#dpto').val()
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.poligono.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                
                                $.each(data, function(i, item) {
                                    $("select[name=prov]").append("<option value='"+data[i]['codprov']+"'>"+data[i]['nombre']+"</option>")
                                });

                            }});
                    },
        
                    buscarDistritos: function() {
                        $("select[name=dist]").html("<option value=''>Seleccione</option>");
                        data_content = {
                            'action'    : 'buscarProvicniasDistritos',
                            'dpto'      : $('#dpto').val(),
                            'prov'      : $('#prov').val()
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.poligono.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                
                                $.each(data, function(i, item) {
                                    $("select[name=dist]").append("<option value='"+data[i]['coddist']+"'>"+data[i]['nombre']+"</option>")
                                });

                            }});
                    },
        
                    SearchXY: function() {
                        var xy = $("#xy").attr('value');
                        var punto = xy.split(',');
            
                        //validando si es una coordenada valida
                        if( !esCoordenada(xy) ) {
                            alert("Coordenada no valida!");
                            return false;
                        }
                        $('#box_loading').show();
            
                        M.clearMarkers();
                        M.clearMakeMarkers(); //wil

                        //setea el zoom y el centro
                        map.setCenter(new google.maps.LatLng(punto[1], punto[0]));
                        map.setZoom(16);
            
                        var pto_xy = new google.maps.LatLng(punto[1], punto[0]);
            
                        marker = new google.maps.Marker({
                            position: pto_xy,
                            map: map,
                            title: 'Punto seleccionado',
                            html: 'Punto Seleccionada',
                            icon: "../../../modulos/maps/vistas/icons/markergreen.png"
                        });
                        markers_arr.push(marker);
            
                        $('#box_loading').hide();
                    },

                    clearMarkers: function() {
                        for (var n = 0, marker; marker = markers_arr[n]; n++) {
                            marker.setVisible(false);
                        }
                    },
        
                    clearMakeMarkers: function() {
                        for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                            makeMarker.setVisible(false);
                        }
                    },
        
                    clearPoligonos: function() {
                        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
                            poligonoDibujar.setMap(null);
                        }
                    },
        
                    clearPolilineas: function() {
                        for (var n = 0, polilineaDibujar; polilineaDibujar = polilineasDibujar_array[n]; n++) {
                            polilineaDibujar.setMap(null);
                        }
                    },

                    popupComponentes: function(x, y) {

                        var zoom_selected = map.getZoom();
            
                        $("#childModal").html('');
                        $("#childModal").css("background-color","#FFFFFF");

                        $.post("v_buscomp_popup.php", {
                            x: x,
                            y: y, 
                            zoom_selected: zoom_selected
                        },
                        function(data){
                            $("#childModal").html(data);
                        }
                        );
    
                        $("#childModal").dialog({modal:true, width:'400px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
                    },

                    popupAddSearchComponentes: function(x, y) {

                        var zoom_selected = map.getZoom();
            
                        parent.$("#childModal").html('');
                        parent.$("#childModal").css("background-color","#FFFFFF");

                        $.post("v_buscaragregar_comp_popup.php", {
                            x: x,
                            y: y, 
                            zoom_selected: zoom_selected
                        },
                        function(data){
                            parent.$("#childModal").html(data);
                        }
                        );
    
                        parent.$("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'INFORMACI&Oacute;N DE LAS CAPAS', position:'top'});
                    },

                    loadPoligono: function(xy_array, z) {

                        var datos             = xy_array['coords'];
                        var color_poligono     = xy_array['color'];
                        var tipo             = xy_array['tipo'];
                        var nombre             = xy_array['nombre'];
                        var poligonoCoords     = [];

                        if( datos.length > 0) {
                            for (var i in datos) {
                                var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                poligonoCoords.push(pto_poligono);
                            }

                            poligonoDibujar = new google.maps.Polygon({
                                paths: poligonoCoords,
                                strokeColor: color_poligono,
                                strokeOpacity: 0.6,
                                strokeWeight: 3,
                                fillColor: color_poligono,
                                fillOpacity: 0.5
                            });

                            poligonosDibujar_array.push(poligonoDibujar);
                
                            poligonoDibujar.setMap(map);
                
                
                            var infowindow = new google.maps.InfoWindow();
                
                            // Add a listener for the click event
                            google.maps.event.addListener(poligonoDibujar, 'rightclick', function(event) {
                                infowindow.close();
                
                                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                                infowindow.setContent(contentString);
                                infowindow.setPosition(event.latLng);

                                infowindow.open(map);
                            });
                

                            //agrego el evento click en cada capa
                            google.maps.event.addListener(poligonoDibujar, 'click', function(capaEvent) {
                       
                                //clear markers
                                M.clearMarkers();
                                M.clearMakeMarkers();

                                marker = new google.maps.Marker({
                                    position: capaEvent.latLng,
                                    map: map
                                });

                                markers_arr.push(marker);
                                var Coords = capaEvent.latLng;
                                var x = Coords.lng();
                                var y = Coords.lat();

                                //popup, pregunta si quieres buscar o agregar componente
                                M.popupAddSearchComponentes(x,y);
                            });

                            //seteamos que la capa existe
                            capa_exists = 1;

                        }
                        else {
                            //capa_exists = 0;
                            //M.clearPoligonos();
                        }        
                    },
        
                    loadPolilinea: function(xy_array) {

                        var datos             = xy_array['coords'];
                        var color_polilinea = xy_array['color'];
                        var tipo             = xy_array['tipo'];
                        var nombre             = xy_array['nombre'];
                        var polilineaCoords = [];
            
                        if( datos.length > 0) {
                            for (var i in datos) {
                                var pto_polilinea = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                polilineaCoords.push(pto_polilinea);
                            }

                            // Construct the polyline                
                            polilineaDibujar = new google.maps.Polyline({
                                path: polilineaCoords,
                                strokeColor: color_polilinea,
                                strokeOpacity: 1.0,
                                strokeWeight: 4
                            });

                            //agrego al array las capas seleccionadas
                            polilineasDibujar_array.push(polilineaDibujar);
                
                            polilineaDibujar.setMap(map);

                
                            var infowindow = new google.maps.InfoWindow();
                
                            // Add a listener for the click event
                            google.maps.event.addListener(polilineaDibujar, 'mouseover', function(event) {
                                var contentString = '<b>' + tipo + ' - ' + nombre + '</b>';

                                infowindow.setContent(contentString);
                                infowindow.setPosition(event.latLng);

                                infowindow.open(map);
                            });
                
                            google.maps.event.addListener(polilineaDibujar, 'mouseout', function(event) {
                                infowindow.close();
                            });
                
                        }else {
                            //M.clearPolilineas();
                        }
                    }
        
                };

                $('#div1').click(function(){
                    $('#sidebar').hide();
                    $('#divInfoResult').hide();
                });
                
                $('#div2').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });
                
                $('#div3').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });
                
                $('#div4').click(function(){
                    $('#sidebar').show();
                    $('#divInfoResult').show();
                });

                $('#clearPoligono').click(function(){
                    M.clearPoligonos();
                });
    
                $('#clearRuta').click(function(){
                    M.clearPolilineas();
                });
    
                $('#btnFiltrarDireccion').click(function(){
                    listar_resultados('direccion');
                });
        
                $('#btnFiltrarUbigeo').click(function(){
                    listar_resultados('ubigeo');
                });
    
                $('#btnFiltrar').click(function(){
                    var arreglo_distrito = new Array();
                    var arreglo_nodo      = new Array();
                    var arreglo_mdf          = new Array();
                    var arreglo_plano     = new Array();
        
                    arreglo_distrito = load_array('multiselect_distrito[]');
                    arreglo_nodo      = load_array('multiselect_nodo[]');
                    arreglo_mdf      = load_array('multiselect_mdf[]');
                    arreglo_plano      = load_array('multiselect_plano[]');
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnFiltrar').attr('disabled', 'disabled');
                    data_content = {
                        'action'            : 'buscar',
                        'arreglo_distrito'     : arreglo_distrito,
                        'arreglo_nodo'         : arreglo_nodo, 
                        'arreglo_mdf'         : arreglo_mdf,
                        'arreglo_plano'     : arreglo_plano  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var poligonos = data['xy_poligono'];

                            if( poligonos.length > 0 ) {
                                //pintamos los poligonos
                                for (var i in poligonos) {
                                    M.loadPoligono(poligonos[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnFiltrar').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
    
                $('#btnDibujarPoligono').click(function(){
                    var tipocapaPoligono      = $('#tipocapaPoligono').val();
                    var arreglo_capaPoligono = new Array();

                    arreglo_capaPoligono      = load_array('multiselect_capaPoligono[]');

                    if ( arreglo_capaPoligono.length < 1 ) {
                        alert("Debe seleccionar por lo menos un poligono");
                        return false;
                    }
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnDibujarPoligono').attr('disabled', 'disabled');
                    data_content = {
                        'action'                : 'buscarPoligono',
                        'tipocapaPoligono'         : tipocapaPoligono,
                        'arreglo_capaPoligono'     : arreglo_capaPoligono  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var poligonos = data['xy_poligonos'];

                            if( poligonos.length > 0 ) {
                                //pintamos los poligonos
                                for (var i in poligonos) {
                                    M.loadPoligono(poligonos[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnDibujarPoligono').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
                $('#btnDibujarRuta').click(function(){
                    var tipocapaRuta      = $('#tipocapaRuta').val();
                    var arreglo_capaRuta = new Array();

                    arreglo_capaRuta      = load_array('multiselect_capaRuta[]');

                    if ( arreglo_capaRuta.length < 1 ) {
                        alert("Debe seleccionar por lo menos una ruta");
                        return false;
                    }
        
                    $('#sidebar').hide();
                    $('#box_loading').show();
                    $('#btnDibujarRuta').attr('disabled', 'disabled');
                    data_content = {
                        'action'            : 'buscarRuta',
                        'tipocapaRuta'         : tipocapaRuta,
                        'arreglo_capaRuta'     : arreglo_capaRuta  
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(data) {

                            var rutas = data['xy_rutas'];

                            if( rutas.length > 0 ) {
                                //pintamos los rutas
                                for (var i in rutas) {
                                    M.loadPolilinea(rutas[i], i);
                                }
                    
                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
                                map.setZoom(13);
                            }

                            $('#box_loading').hide();
                            $('#btnDibujarRuta').removeAttr('disabled');
                            $('#divInfoResult').hide();

                        }
                    });
                 
                });
    
                $('#btnFiltrarXY_new').click(function(){
                    var xy = $("#xy").attr('value');
        
                    if( xy != '' ) {
                        //limpiando mapa
                        clearMarkers();
                        clearMakeMarkers();
        
                        M.SearchXY();
                    }else {
                        listar_resultados('cliente');
                    }
                });
    
                $("#tipocapaRuta").change(function() {
                    var idpoligono_tipo = $("#tipocapaRuta").val();
                    data_content = "action=buscarCapasRuta&idpoligono_tipo=" + idpoligono_tipo;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        success: function(datos) {
                            $("#divCapaRuta").html(datos);
                        }
                    });
                });
    
                $("#tipocapaPoligono").change(function() {
                    var idpoligono_tipo = $("#tipocapaPoligono").val();
                    data_content = "action=buscarCapasPoligono&idpoligono_tipo=" + idpoligono_tipo;
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        success: function(datos) {
                            $("#divCapaPoligono").html(datos);
                        }
                    });
                });

                $("#zonal").change(function() {
                    var zonal = $("#zonal").val();
                    var zonales_array = [];
                    var zonal_arr = $("#zonal").val();

                    zonales_array.push(zonal);

                    var data_content = {
                        'action'        : 'buscarDistritos',
                        'zonales_array' : zonales_array
                    };
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.poligono.php",
                        data:   data_content,
                        dataType: "html",
                        success: function(datos) {
                            if( datos != '' ) {
                                $("#divDistrito").html(datos);
                            }
                        }
                    });
                }); 

                M.initialize();
               
            });


            function esDecimal(num) {
                if( /^[-]?[0-9]{1,2}(\.[0-9]{0,20})$/.test(num) ) {
                    return 1;
                }else {
                    return 0;
                }
            }

            function esCoordenada(xy) {
                var num_arr = xy.split(',');

                if( num_arr.length != 2 ) {
                    //alert("Coordenada no valida1.");
                    return false;
                }
                else {
                    if( $.trim(num_arr[0]) == '' && $.trim(num_arr[1]) == '' ) {
                        //alert("Coordenada no valida2.");
                        return false;
                    }else {
                        if( !esDecimal(num_arr[0]) || !esDecimal(num_arr[1]) ) {
                            //alert("Coordenada no valida3.");
                            return false;
                        }
                    }
                }
    
                return true;
            }

            function nuevasImagenesEdificio(idedificio, item, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/agregarImagenesEdificio.popup.php", {
                    idedificio    : idedificio,
                    item        : item,
                    x              : x,
                    y            : y
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'700px', hide: 'slide', title: 'Registrar nuevas imagenes para este edificio', position:'top'});
    
            }

            function editarEdificio(idedificio, item, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/editarComponente.popup.php", {
                    idedificio    : idedificio,
                    item        : item,
                    x              : x,
                    y            : y,
                    capa        : 'edi'
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Edificio', position:'top'});
            }

            function editarCompetencia(idcompetencia, x, y) {
                $("#childModal").html('');
                $("#childModal").css("background-color","#FFFFFF");
                $.post("principal/editarComponente.popup.php", {
                    idcompetencia    : idcompetencia,
                    x              : x,
                    y            : y,
                    capa        : 'com'
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Competencia', position:'top'});
            }


            function listar_resultados(tipo) {

                if( tipo == 'cliente' ) {
                    var xy             = $("#xy").attr('value');
                    var telefono     = $("#telefono").attr('value');
                    var cliente1      = $("#cliente1").attr('value');
                    var cliente2      = $("#cliente2").attr('value');
                    var direccion      = "";
        
                    if(xy == "" && telefono == "" && cliente1 == "" && cliente2 == "" && direccion == "") {
                        alert("Ingrese algun criterio de busqueda.");
                        return false;
                    }    
                }
                else if( tipo == 'direccion' ) {
        
                    var strZonal = $('#zonal').val();
                    var distrito = new Array();

                    var zonal = [];
                    zonal.push(strZonal);
        
                    distrito = load_array('multiselect_selDistrito[]');
    
                    var direccion1     = $('#direccion1').val();
                    var numero         = $('#numero').val();
        
                    if(zonal.length == 0) {
                        alert("Ingrese la zonal");
                        return false;
                    }
                    if(distrito.length == 0) {
                        alert("Ingrese el distrito");
                        return false;
                    }
                    if(direccion1 == "" && numero == "") {
                        alert("Ingrese algun criterio de busqueda.");
                        return false;
                    }
                    if(direccion1 == "" && numero != "") {
                        alert("Ingrese la direccion");
                        return false;
                    }
                }
                else if( tipo == 'ubigeo' ) {
        
                    var dpto = $('#dpto').val();
                    var prov = $('#prov').val();
                    var dist = $('#dist').val();
                    var localidad = $('#localidad').val();


                    if(dpto == "") {
                        alert("Ingrese departamento");
                        return false;
                    }
                    if(prov == "") {
                        alert("Ingrese provincia");
                        return false;
                    }
                }
    
    
                /**
                 * makeMarker() ver 0.2
                 * creates Marker and InfoWindow on a Map() named 'map'
                 * creates sidebar row in a DIV 'sidebar'
                 * saves marker to markerArray and markerBounds
                 * @param options object for Marker, InfoWindow and SidebarItem
                 */
                var infoWindow = new google.maps.InfoWindow();
                var markerBounds = new google.maps.LatLngBounds();
                var markerArray = [];
     
          
                function makeMarker(options){
                    var pushPin = null;
                    /*var pushPin = new google.maps.Marker({
                     map: map
                 });
                 pushPin.setOptions(options);*/
            
                    if( options.position != '(0, 0)' ) {
                        pushPin = new google.maps.Marker({
                            map: map
                        });
                
                        makeMarkers_arr.push(pushPin);
                
                        pushPin.setOptions(options);
                        google.maps.event.addListener(pushPin, "click", function(){
                            infoWindow.setOptions(options);
                            infoWindow.open(map, pushPin);
                            if(this.sidebarButton)this.sidebarButton.button.focus();
                        });
                        var idleIcon = pushPin.getIcon();

                        markerBounds.extend(options.position);
                        markerArray.push(pushPin);
                    }else {
             
                        pushPin = new google.maps.Marker({
                            map: map
                        });
            
                        makeMarkers_arr.push(pushPin);
            
                        pushPin.setOptions(options);
                    }
         
                    if(options.sidebarItem){
                        pushPin.sidebarButton = new SidebarItem(pushPin, options);
                        pushPin.sidebarButton.addIn("sidebar");
                    }
         
                    return pushPin;           
                }

                google.maps.event.addListener(map, "click", function(){
                    infoWindow.close();
                });

                /*
                 * Creates an sidebar item 
                 * @param marker
                 * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                function SidebarItem(marker, opts) {
                    //var tag = opts.sidebarItemType || "button";
                    //var tag = opts.sidebarItemType || "span";
                    var tag = opts.sidebarItemType || "div";
                    var row = document.createElement(tag);
                    row.innerHTML = opts.sidebarItem;
                    row.className = opts.sidebarItemClassName || "sidebar_item";  
                    row.style.display = "block";
                    row.style.width = opts.sidebarItemWidth || "340px";
                    row.onclick = function(){
                        google.maps.event.trigger(marker, 'click');
                    }
                    row.onmouseover = function(){
                        google.maps.event.trigger(marker, 'mouseover');
                    }
                    row.onmouseout = function(){
                        google.maps.event.trigger(marker, 'mouseout');
                    }
                    this.button = row;
                }
                // adds a sidebar item to a <div>
                SidebarItem.prototype.addIn = function(block){
                    if(block && block.nodeType == 1)this.div = block;
                    else
                        this.div = document.getElementById(block)
                        || document.getElementById("sidebar")
                        || document.getElementsByTagName("body")[0];
                    this.div.appendChild(this.button);
                }

     
                var data_content = null;
                if( tipo == 'cliente' ) {
                    data_content = {
                        'action'        : 'buscarCliente',
                        'telefono'         : telefono,
                        'cliente1'         : cliente1,
                        'cliente2'         : cliente2
                    };
                }
                else if( tipo == 'direccion' ) {
                    data_content = {
                        'action'        : 'buscarDireccion',
                        'zonal'     : zonal,
                        'distrito'     : distrito, 
                        'direccion1'     : direccion1,
                        'numero'     : numero  
                    };
                }
                else if( tipo == 'ubigeo' ) {
                    data_content = {
                        'action'    : 'buscarLocalidad',
                        'dpto'      : dpto,
                        'prov'      : prov, 
                        'dist'      : dist,
                        'localidad' : localidad  
                    };
                }
     
                $('#box_loading').show();
                $("#btnFiltrarXY_new").attr('disabled', 'disabled');
                $("#btnFiltrarDireccion").attr('disabled', 'disabled');
                $("#btnFiltrarUbigeo").attr('disabled', 'disabled');
                $("#sidebar").html("");
                var tabla = '';
                var detalle = '';
                $.ajax({
                    type:   "POST",
                    url:    "../gescomp.poligono.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {
         
                        clearMarkers();
                        clearMakeMarkers();

                        if( data['edificios'].length > 0 ) {

                            var datos = data['edificios']; 

                            var num = 1;
                            var x_ini = y_ini = '';
                            var flag_captura_xy = 1;
                            for (var i in datos) {

                                var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                if( flag_captura_xy && x != '' && y != '' ) {
                                    x_ini = datos[i]['x'];
                                    y_ini = datos[i]['y'];
                                    flag_captura_xy = 0;
                                }

                                var color = "color: #FF0000;";
                                if( x != '' && y != '' ) {
                                    color = "color: #000000;";
                                }

                                if( tipo == 'cliente' || tipo == 'direccion' ) {
                                    var cableArmario = datos[i]['Armario'];
                                    var tipo_ca = 'Arm';
                                    if( datos[i]['tipo_red'] == 'D' ) {
                                        cableArmario = datos[i]['Cable'];
                                        tipo_ca = 'Cbl';
                                    }

                                    var zonal = datos[i]['Zonal'];
                                    if( datos[i]['Zonal'] == 'ESTE' || datos[i]['Zonal'] == 'OESTE' || datos[i]['Zonal'] || 'NORTE' && datos[i]['Zonal'] == 'SUR' ) {
                                        zonal = 'LIMA';
                                    }

                                    tabla = '<div style="width:70px; float:left; font-weight:normal;' + color + '">Zon:' + datos[i]['Zonal'] + '<br />Mdf:' + datos[i]['MDF'] + '<br />' + tipo_ca + ':' + datos[i]['cableArmario'] + '<br />Trm:' + datos[i]['Caja'] + '</div>';
                                    tabla += '<div style="width:270px; float:left;">';
                                    tabla += '<div style="font-size:15px;color:#1155CC;">' + datos[i]['Cliente'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#666666;">' + datos[i]['Direccion'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#555555;">' + datos[i]['DDN'] + ' ' + datos[i]['Telefono'] + '</div>';
                                    tabla += '</div>';

                                    detalle = '<b>Telefono:</b> ' + datos[i]['Telefono'] + '<br />';
                                    detalle += '<b>Cliente:</b> ' + datos[i]['Cliente'] + '<br />';
                                    detalle += '<b>Direccion:</b> ' + datos[i]['Direccion'] + '<br /><br />';
                                    detalle += '<b>Zonal:</b> ' + datos[i]['Zonal'] + '<br />';
                                    detalle += '<b>Mdf:</b> ' + datos[i]['MDF'] + '<br />';
                                    if( datos[i]['tipo_red'] == 'D' ) {
                                        detalle += '<b>Cable:</b> ' + datos[i]['Cable'] + '<br />';
                                    }else if( datos[i]['tipo_red'] == 'F' ) {
                                        detalle += '<b>Armario:</b> ' + datos[i]['Armario'] + '<br />';
                                    }
                                    detalle += '<b>Caja:</b> ' + datos[i]['Caja'] + '<br />';
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';
                                    detalle += '<a title="Ver clientes asignados a este terminal: ' + datos[i]['Caja'] + '" href="javascript:clientesTerminal(\''+ datos[i]['Zonal'] +'\',\''+ datos[i]['MDF'] +'\',\''+ datos[i]['Cable'] +'\',\''+ datos[i]['Armario'] +'\',\''+ datos[i]['Caja'] +'\',\''+ datos[i]['tipo_red'] +'\');">Ver clientes</a>';        

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['Caja'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/terminal/trm_" + datos[i]['Caja'] + "--1.gif"
                                    });
                        
                                }
                                else if( tipo == 'ubigeo' ) {
                        
                                    tabla = '<div style="width:70px; float:left; font-weight:normal;' + color + '">' +  datos[i]['ubigeo'] + '</div>';
                                    tabla += '<div style="width:270px; float:left;">';
                                    tabla += '<div style="font-size:15px;color:#1155CC;">' + datos[i]['localidad'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#666666;">' + datos[i]['distrito'] + '</div>';
                                    tabla += '<div style="font-size:11px;color:#555555;">' + datos[i]['departamento'] + ' / ' + datos[i]['provincia'] + '</div>';
                                    tabla += '</div>';

                                    detalle = '<b>Departamento:</b> ' + datos[i]['departamento'] + '<br />';
                                    detalle += '<b>Provincia:</b> ' + datos[i]['provincia'] + '<br />';
                                    detalle += '<b>Distrito:</b> ' + datos[i]['distrito'] + '<br />';
                                    detalle += '<b>Localidad:</b> ' + datos[i]['localidad'] + '<br />';
                                    detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br />';

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: datos[i]['localidad'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/localidades/localidades.png"
                                    });
                        
                                }

                                num++;
                            }

                            /* fit viewport to markers */
                            //map.fitBounds(markerBounds);

                            map.setZoom(16);
                            map.setCenter(new google.maps.LatLng(y_ini, x_ini));

                        }

                        $("#btnFiltrarXY_new").removeAttr('disabled');
                        $("#btnFiltrarDireccion").removeAttr('disabled');
                        $("#btnFiltrarUbigeo").removeAttr('disabled');
                        $('#box_loading').hide();
                        $('#divInfoResult').show();
                        $('#spanNum').html(data['edificios'].length);
                    }
                });
            }

            function clearMarkers() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }

            function clearMakeMarkers() {
                for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                    makeMarker.setVisible(false);
                }
            }

            function clientesTerminal(zonal, mdf, cable, armario, caja, tipo_red) {
                parent.$("#childModal").html('Cargando datos...');
                parent.$("#childModal").css("background-color","#FFFFFF");
                $.post("principal/clientes_terminal.popup.php", {
                    zonal: zonal,
                    mdf: mdf,
                    cable: cable,
                    armario: armario,
                    caja: caja,
                    tipo_red: tipo_red
                },
                function(data){
                    parent.$("#childModal").html(data);
                }
            );

                parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Clientes asignados para este terminal: ' + caja, position:'top'});

            }

        </script>
        <style type="text/css">
            .sidebar_item {
                float: left;
                padding: 5px 0;
                border-bottom: 1px solid #DDDDDD;
            }
            .ipt{
                border:none;
                color:#0086C3;
            }
            .ipt:focus{
                border:1px solid #0086C3;
            }
            body{
                font-family: Arial;
                font-size: 11px;
            }
            .ui-layout-pane {
                background: #FFF; 
                border: 1px solid #BBB; 
                padding: 7px; 
                overflow: auto;
            } 

            .ui-layout-resizer {
                background: #DDD; 
            } 

            .ui-layout-toggler {
                background: #AAA; 
            }
            .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
            .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
            .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
            .slt{
                border:1px solid #1E9EF9;
                background-color: #FAFAFA;
                height: 18px;
                font-size: 11px;
                color:#0B0E9D;
            }
            .box_checkboxes{
                border:1px solid #4C5E60;
                background: #E7F7F7;
                width:170px;
                height: 240px;
                overflow: auto;
                display:none;
                position: absolute;
                top:0px;
                left:0px;
                opacity:0.9;
            }
            .tb_data_box{
                width: 100%;
                border-collapse:collapse;
                font-family: Arial;
                font-size: 9px;
            }

        </style>
        <script>
            $(function() {
                $("#btnFiltrar").button();
                $("#btnFiltrarXY").button();
                $("#btnFiltrarXY_new").button();
                $("#btnFiltrarDireccion").button();
                $("#btnFiltrarUbigeo").button();
        
                $("#btnDibujarRuta, #btnDibujarPoligono").button();
            });
        </script>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
        <div class="ui-layout-west">

            <div id="tabs" style="width:99%; margin-bottom: 10px;">
                <ul>
                    <li><a id="div1" href="#divPoligono" style="color:#1D87BF; font-weight: 600">Busq. de Componentes</a></li>
                    <li><a id="div2" href="#divSearchXY" style="color:#1D87BF; font-weight: 600">Busq. por cliente</a></li>
                    <li><a id="div3" href="#divDireccion" style="color:#1D87BF; font-weight: 600">Direccion</a></li>
                    <li><a id="div4" href="#divUbigeo" style="color:#1D87BF; font-weight: 600">Localidades</a></li>
                </ul>

                <div id="divPoligono">

                    <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                        <tr>
                            <td colspan="2"><span class="l_tit">Poligonos</span></td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Tipo capa</td>
                            <td>
                                <select name="tipocapaPoligono" id="tipocapaPoligono" style="font-size: 11px;height: 20px;padding: 1px;">
                                    <option value="">Seleccione</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Capa</td>
                            <td>
                                <div id="divCapaPoligono">
                                    <select name="capaPoligono" id="capaPoligono" style="font-size: 11px;height: 20px;padding: 1px;" class="clsMultiple" multiple="multiple">
                                        <!--<option value="">Seleccione</option>-->
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button id="btnDibujarPoligono">Dibujar Poligono</button> &nbsp; 
                                <span id="clearPoligono"><img src="../../../img/btn_clear_32.png" style="width:15px;height:15px;"> <a href="javascript:void(0);">Limpiar poligonos</a></span>
                            </td>
                        </tr>
                    </table>


                    <div style="height:20px;"></div>

                    <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                        <tr>
                            <td colspan="2"><span class="l_tit">Rutas</span></td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Tipo capa</td>
                            <td>
                                <select name="tipocapaRuta" id="tipocapaRuta" style="font-size: 11px;height: 20px;padding: 1px;">
                                    <option value="">Seleccione</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Capa</td>
                            <td>
                                <div id="divCapaRuta">
                                    <select name="capaRuta" id="capaRuta" style="font-size: 11px;height: 20px;padding: 1px;" class="clsMultiple" multiple="multiple">
                                        <!--<option value="">Seleccione</option>-->
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <button id="btnDibujarRuta">Dibujar Ruta</button> &nbsp; 
                                <span id="clearRuta"><img src="../../../img/btn_clear_32.png" style="width:15px;height:15px;"> <a href="javascript:void(0);">Limpiar rutas</a></span>
                            </td>
                        </tr>
                    </table>


                </div>

                <div id="divSearchXY">

                    <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                    <!--<tr>
                            <td><span class="l_tit">B&uacute;squeda por coordenada XY:</span></td>
                </tr>-->
                        <tr>
                            <td align="left" class="label">Ingrese el punto x,y</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" class="clsSearchXY" id="xy" name="xy" size="35" style="height:18px; padding: 1px; font-size:11px;"><br />
                                    <span style="font-size:11px;">Ejm: -77.07286315,-12.07187324</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Telefono</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" class="clsSearchXY" id="telefono" name="telefono" size="20" style="height:18px; padding: 1px; font-size:11px;"><br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="label">Cliente</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" class="clsSearchXY" id="cliente1" name="cliente1" size="22" style="height:18px; padding: 1px; font-size:11px;"> &nbsp; 
                                    <input type="text" class="clsSearchXY" id="cliente2" name="cliente2" size="22" style="height:18px; padding: 1px; font-size:11px;"><br />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br /><button id="btnFiltrarXY_new">Buscar</button>
                                            </td>
                                        </tr>
                                        </table>

                                        </div>

                                        <div id="divDireccion">

                                            <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">

                                                <tr>
                                                    <td align="left" class="label">Zonal</td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <select name="zonal" id="zonal">
                                                            <option value="">Seleccione</option>
                                                        </select>                        
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="label">Distrito</td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <div id="divDistrito">
                                                            <select id="selDistrito" class="clsMultiple" multiple="multiple">
                                                                <!--<option value="">Seleccione</option>-->
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="label">Direcci&oacute;n</td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <input type="text" id="direccion1" name="direccion1" size="35" style="height:18px; padding: 1px; font-size:11px;">
                                                        <!--<input type="text" id="direccion2" name="direccion2" size="16" style="height:18px; padding: 1px; font-size:10px;">--> &nbsp; 
                        Cdra. <input type="text" id="numero" name="numero" size="3" style="height:18px; padding: 1px; font-size:11px;">
                                                                <br />
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <br /><button id="btnFiltrarDireccion">Buscar</button>
                                                                    </td>
                                                                </tr>
                                                                </table>

                                                                </div>

                                                                <div id="divUbigeo">

                                                                    <table cellpadding="2" cellspacing="0" style="width:90%; border-collapse:collapse;">
                                                                        <tr>
                                                                            <td align="left" class="label" width="100">Departamento</td>
                                                                            <td align="left">
                                                                                <select name="dpto" id="dpto">
                                                                                    <option value="">Seleccione</option>
                                                                                </select>                        
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="label">Provincia</td>
                                                                            <td align="left">
                                                                                <div id="divProv">
                                                                                    <select name="prov" id="prov">
                                                                                        <option value="">Seleccione</option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="label">Distrito</td>
                                                                            <td align="left">
                                                                                <div id="divDist">
                                                                                    <select name="dist" id="dist">
                                                                                        <option value="">Seleccione</option>
                                                                                    </select>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="label" colspan="2">Localidad</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" colspan="2">
                                                                                <input type="text" id="localidad" name="localidad" size="35" style="height:18px; padding: 1px; font-size:11px;">
                                                                                    <br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <br /><button id="btnFiltrarUbigeo">Buscar</button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </div>

                                                                <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>

                                                                </div>

                                                                <div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">

                                                                    <div id="divInfoResult" style="margin-bottom: 8px; width: 340px; display:none;">
                                                                        <div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
                                                                        <!--<div style="float:right;"><a href="javascript:imprimirSeleccionXY();">Imprimir seleccion</a> | <a href="javascript:imprimirEnMapaXY();">Imprimir en mapa</a></div>-->
                                                                    </div>

                                                                    <div id="sidebar"></div>
                                                                </div>

                                                                </div>
                                                                <div class="ui-layout-center">

                                                                    <div id="box_resultado">

                                                                        <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>

                                                                    </div>

                                                                </div>

                                                                <div id="parentModal" style="display: none;">
                                                                    <div id="childModal" style="padding: 10px; background: #fff;"></div>
                                                                </div>

                                                                </body>
                                                                </html>