<script type="text/javascript">

$(document).ready(function() {

	$("#search").click(function() {
            //var zoom_selected = map.getZoom();
            var zoom_selected = 14;
            var x = $("#x").attr('value');
            var y = $("#y").attr('value');

            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");

            $.post("v_buscomp_popup.php", {
    	        x: x,
    	        y: y, 
    	        zoom_selected: zoom_selected
    	    },
        	function(data){
            	$("#childModal").html(data);
        	}
		);

        $("#childModal").dialog({modal:true, width:'600px', hide: 'slide', title: 'Informaci&oacute;n de las Capas', position:'top'});
        
	});

	$("#add").click(function() {
            var x = $("#x").attr('value');
            var y = $("#y").attr('value');

            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            
            $.post("modulos/maps/vistas/principal/agregarcomp.popup.php", {
    	        x: x,
    	        y: y
    	    },
        	function(data){
    	    	$("#childModal").html(data);
        	}
            );

            $("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Agregar Componente', position:'top'});
	});
	
});


</script>

<div style="text-align: left;">
<h3>X,Y seleccionado:</h3>
<?php
echo $_REQUEST['x'] . ',' . $_REQUEST['y']
?>
<br />&nbsp;

<h3>Elija la opcion de Agregar o Buscar Componentes:</h3>
<table border="0" style="padding:0px; text-align: left;" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
    <td colspan="2" style="padding:10px 0;">
        <input type="submit" class="clsSearch" style="padding:3px;" name="add" id="add" value="Agregar componentes" />
        <input type="submit" class="clsSearch" style="padding:3px;" name="search" id="search" value="Buscar componentes" />
    </td>
</tr>
</table>
<input type="hidden" name="x" id="x" value="<?php echo $_REQUEST['x']?>" />
<input type="hidden" name="y" id="y" value="<?php echo $_REQUEST['y']?>" />
<input type="hidden" name="zoom_selected" id="zoom_selected" value="<?php echo $_POST['zoom_selected']?>" />

</div>