<style>
em {
    color: #ff0000;
}
.headTbl {
    background-image: url("img/bg_head_js.gif");
    background-position: center top;
    background-repeat: repeat-x;
    color: #000000;
    height: 22px;
}
table.tablesorter tbody tr:hover td {
    background-color: #DBEDFE;
}

table.tablesorter .tr_selected td {
    background-color: #FFC444;
}


table.tb_detalle_actu {
	background-color: #F2F2F2;
	font-size: 11px;
}
.tb_detalle_actu td {
	background-color: #FFFFFF;
}
table.tb_detalle_actu .da_titulo{
	color: #FFFFFF;
	background-color: #006495;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
	text-align: left;
	padding: 0 0 0 5px;
}
table.tb_detalle_actu .da_titulo_gest{
	color: #FFFFFF;
	background-color: #1E9EBB;
	font-weight: bold;
	font-size: 11px;
	line-height: 20px;
}
table.tb_detalle_actu .da_subtitulo{
	color: #000000;
	font-weight: bold;
	background-color: #E1E2E2;
	font-size: 10px;
}
table.tb_detalle_actu .da_import{
	font-size: 11px;
	color: red;
	font-weight: bold;
}
table.tb_detalle_actu .da_agenda{
	font-size: 11px;
	font-weight: bold;
}
table a.link_movs{
	color: #FFFFFF;
	font-weight: normal;
}
.boton {
    background: url("img/fndbtnreg.gif") repeat scroll 0 0 transparent;
    border: 0 none;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 11px;
    font-weight: bold;
    height: 30px;
    width: 80px;
}


#pirata_image {
    float: left;
    width: 400px;
    display: block;
}
#pirata_image img{
    border: 1px solid #CACED1;
}

#pirata_thumbs {
	/*overflow: hidden;*/
	float: left;
    width: 140px;
}
#pirata_thumbs img {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    cursor: pointer;
    /*height: 75px;*/
    margin: 0 5px 5px 10px;
    padding: 2px;
    width: 70px;
}
#pirata_thumbs p{
    margin: 5px 0 0 0;
}
#pirata_thumbs span {
    float: left;
    width: 400px;
    border: 1px solid #CACED1;
    margin: 30px 10px 12px 0;
    padding: 2px;
    width: 77px;
}

#pirata_icos {
    /*overflow: hidden;*/
    width: 500px;
    height: 140px;
}
#pirata_icos p{
	margin: 5px 0 0 0;
}
#pirata_icos span{
	float: left;
    width: 400px;
}
#pirata_icos span {
	border: 1px solid #CACED1;
    margin: 30px 10px 12px 0;
    padding: 2px;
    width: 77px;
}

.strPeq {
    font-size: 8px;
}

.hd_orden_asc {
    background-image: url("img/desc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}
.hd_orden_desc {
    background-image: url("img/asc.gif");
    background-position: right center;
    background-repeat: no-repeat;
    padding-right: 16px;
}

</style>
<script type="text/javascript" src="js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="modulos/maps/js/bandeja.fftt.js"></script>


<!-- Cuerpo principal -->
<div id="content">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">
			
			<table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<td class="form_titulo" width="30%">
					<img border="0" title="SubMódulo" alt="SubMódulo" src="img/plus_16.png"> &nbsp;Bandeja Control de Armarios
				</td>
				<td width="70%" align="left">
                                    Estado
                                    <select name="estado" id="estado">
                                        <option value="">Seleccione</option>
                                        <?php foreach ($arrObjArmarioEstado as $objArmario_estado) { ?>
                                            <option value="<?php echo $objArmario_estado->__get('_codArmarioEstado') ?>" ><?php echo $objArmario_estado->__get('_desEstado') ?></option>
                                        <?php } ?>
                                    </select>
                                    
                                </td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                                            <div style="border-bottom:1px solid #999999;margin-bottom:3px;padding-bottom:3px;">
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
                                                    <td width="10%" align="left">Buscar por:</td>
                                                    <td width="90%" align="left">
                                                        Zonal
                                                        <select name="zonal" id="zonal" onchange="obtenerMdfsZonalUsuario();">
                                                            <option value="">Seleccione</option>
                                                            <?php foreach ($_SESSION['USUARIO_ZONAL'] as $obj_zonal) { ?>
                                                                <option value="<?php echo $obj_zonal->__get('_abvZonal') ?>" ><?php echo $obj_zonal->__get('_abvZonal') . ' - ' . $obj_zonal->__get('_descZonal') ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        &nbsp;
                                                        MDF
                                                        <select name="mdf" id="mdf">
                                                            <option value="">Seleccione</option>
                                                        </select>
                                                        <input id="campo_valor" class="ui-autocomplete-input valid input_text" type="text" style="width: 100px;" name="campo_valor">
                                                        <input type="button" id="btn_buscar" name="buscar" value="Buscar"/>
                                                    </td>
						</tr>
                                                </table>
                                            </div>
					</div>
				</td>
			</tr>
			</table>

			
				  
			<div style="width: 100%;" id="divResultado" >
                            
                            <table id="tb_sup_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                            <tr>
                            <td style="width:55%; height: 30px; text-align: left;">
                                    Han sido listado(s) <b><span id="cont_num_resultados_head"><?php echo $totalRegistros ?></span></b> registro(s) &nbsp;|&nbsp; 
                            </td>
                            <td style="width:45%;">
                                    <div  style=" width:360px; position: relative;">
                                            <div id="paginacion"></div>
                                    </div>
                            </td>
                            </tr>
                            </table>

                            <input type="hidden" id="pag_actual" name="pag_actual" value="1"/>
                            <input type="hidden" id="tl_registros" name="tl_registros" value="<?php echo $totalRegistros;?>"/>
			
                            <table id="tb_resultado" class="tablesorter" width="100%" cellspacing="1" cellpadding="2" border="0" align="center" style="overflow: auto; width: 100%;display:none;">
                            <thead>
                                <tr style="line-height:13px;" class="headFilter">
                                    <th class="headTbl" width="2%">#</th>
                                    <th class="headTbl" width="5%">Opciones</th>
                                    <th class="headTbl" width="8%">ID</th>
                                    <th class="headTbl" width="6%"><a href="javascript:void(0)" orderField="armario" orderType="asc">Armario</a></th>
                                    <th class="headTbl" width="5%">Zonal</th>
                                    <th class="headTbl" width="5%"><a href="javascript:void(0)" orderField="mdf" orderType="asc">Mdf</a></th>
                                    <th class="headTbl" width="32%">Direcci&oacute;n</th>
                                    <th class="headTbl" width="6%"><a href="javascript:void(0)" orderField="qcaparmario" orderType="asc">Capacidad</a></th>
                                    <th class="headTbl" width="6%"><a href="javascript:void(0)" orderField="qparlib" orderType="asc">Par libres</a></th>
                                    <th class="headTbl" width="6%">Par reserv.</th>
                                    <th class="headTbl" width="6%">Par distrib.</th>
                                    <th class="headTbl" width="6%">Estado</th>
                                    <th class="headTbl" width="7%">Fec. estado</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            </table>
                            
                            <br />
                            <table id="tb_pie_tl_registros" style="margin-left:0px;display:none;" border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                            <tr>
                               <td align="left">Han sido listado(s) <b><span id="cont_num_resultados_foot"><?php echo $totalRegistros; ?></span></b> registro(s)  <br/><br/></td>
                            </tr>
                            </table>

			</div>
			
			

		</td>
	</tr>
	</table>
	<br /><br />
</div>

<div id="footer"></div>

<!-- Divs efecto Loader -->
<div id="fongoLoader" style="display:none" class="bgLoader">
    <div class="imgLoader" ><span>Cargando...</span> &nbsp; <a id="cancelar_ajax" href="javascript:void(0);">cancelar</a></div>
</div>
<!-- fin  -->
<input type="hidden" value="1" id="emisor" name="emisor" />


<div id="parentModalPirata" style="display: none;">
	<div id="childModalPirata" style="padding: 10px; background: #fff;"></div>
</div>
