<?php
require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


if( isset($_GET['action']) && $_GET['action'] == 'save' ) {
    
    $obj_terminales = new data_ffttTerminales();

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	//Devuelve el array de zonales asignados al usuario
	$array_zonales = $obj_terminales->get_zonales_by_idusuario($conexion, $idusuario);

    $zonales_selected_arr = array();
    foreach( $_POST['xy'] as $pkzonal => $valzonal ) {
    	if( $valzonal['x'] != '' || $valzonal['y'] != '' ) {
    	
    		$zonales_selected_arr[$pkzonal] = array(
    			'x' => $valzonal['x'],
    			'y' => $valzonal['y']
    		);
    	}
    }

    //update solo a los que surgieron cambios para tabla zonal
    $flag = 1;
	foreach( $array_zonales as $zonal ) {
		
		if( array_key_exists($zonal['abv_zonal'], $zonales_selected_arr) ) {
			if( $zonal['x'] != $zonales_selected_arr[$zonal['abv_zonal']]['x'] ||
				$zonal['y'] != $zonales_selected_arr[$zonal['abv_zonal']]['y'] ) {
			
				//grabando los xy en tabla: zonal
    			$result = $obj_terminales->__updateZonalXY($conexion, $zonal['abv_zonal'], $zonales_selected_arr[$zonal['abv_zonal']]['x'], $zonales_selected_arr[$zonal['abv_zonal']]['y']);
				if( !$result ) {
					$flag = 0;
					break;
				}
			}
		}
	}
    $data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarZonales' ) {
	
	$obj_terminales = new data_ffttTerminales();
	
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	//Devuelve el array de zonales asignados al usuario
	$array_zonal = $obj_terminales->get_zonales_by_idusuario($conexion, $idusuario);
	
	$data_markers = array();
	$data_markers_con_xy = array();
	$i = 1;
	foreach ($array_zonal as $zonales) {
		if( $zonales['y'] != '' && $zonales['x'] != '' ) {
			$data_markers_con_xy[] = $zonales;
		}

		$data_markers[] = $zonales;
		$i++;
	}
	
	
	$data_result = array(
		'edificios'			=> $data_markers,
		'edificios_con_xy'  => $data_markers_con_xy
	);
	
	echo json_encode($data_result);
    exit;

}


include 'principal/gescomp.zonal.php';    

?>

