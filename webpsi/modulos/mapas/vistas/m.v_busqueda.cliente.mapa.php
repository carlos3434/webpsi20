<!DOCTYPE html>
<html>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Webunificada - Movil</title>
    <script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
    <link href="../../css/movil.css" rel="stylesheet" type="text/css" />
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map_canvas {
            height: 100%;
        }

        .opc_orden_up {
            background-image: url("../../img/arrow_up_s.gif");
            background-position: right center;
            background-repeat: no-repeat;
            padding-right: 25px;
            color: #ffffff;
        }
        
        .opc_orden_down {
            background-image: url("../../img/arrow_down_s.gif");
            background-position: right center;
            background-repeat: no-repeat;
            padding-right: 25px;
            color: #ffffff;
        }
        
        .tb_detalle_opc td {
            padding: 10px;
            background-color: #000000;
        }
        .opc_lista {
            color: #ffffff;
        }
    </style>

    <script type="text/javascript">
        var map = null;
        var x = '<?php echo ( isset($_REQUEST['x']) && $_REQUEST['x'] != '' ) ? $_REQUEST['x'] : '0';?>';
        var y = '<?php echo ( isset($_REQUEST['y']) && $_REQUEST['y'] != '' ) ? $_REQUEST['y'] : '0';?>';
        var ddnTelefono = '<?php echo ( isset($_REQUEST['ddnTelefono']) && $_REQUEST['ddnTelefono'] != '' ) ? $_REQUEST['ddnTelefono'] : '';?>';
    
        $(document).ready(function() {

        	$(".opc_orden_down").click(function() {
            	$(this).removeClass('opc_orden_down');
            	$(this).addClass('opc_orden_up');
            	$('#divOpciones').show();
            });
            $(".opc_orden_up").click(function() {
            	$(this).removeClass('opc_orden_up');
            	$(this).addClass('opc_orden_down');
            	$('#divOpciones').hide();
            });

        	$('#divOpcEdificio a').toggle(
    	        function() {
    	            $('#divOpcEdificio a').removeClass("opc_orden_up");
    	            $(this).addClass("opc_orden_down"); 
    	            $('#divOpciones').show();
    	        }, function () {
    	            $('#divOpcEdificio a').removeClass("opc_orden_down");
    	            $(this).addClass("opc_orden_up");
    	            $('#divOpciones').hide();
    	        }
    	    );

            var L = {
        		initialize: function() {

        			if (typeof (google) == "undefined") {
                        alert('Verifique su conexion a maps.google.com');
                        return false;
                    }

                    var latlng = new google.maps.LatLng(y, x);
                    var zoomMap = 16;

                    var myOptions = {
                        zoom: zoomMap,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                            position: google.maps.ControlPosition.RIGHT_TOP
                        },
                        navigationControl: true,
                        navigationControlOptions: {
                            style: google.maps.NavigationControlStyle.ANDROID,
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        }
                    };

                    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                    var infowindow = new google.maps.InfoWindow({
                        content: 'Telefono: ' + ddnTelefono
                    });

                    markerInicial = new google.maps.Marker({
                        position: latlng, 
                        map: map, 
                        title: ddnTelefono
                    });

                    google.maps.event.addListener(markerInicial, 'click', function() {
                        infowindow.open(map, markerInicial);
                    });

                }
            };

            //carga inicial
            L.initialize();

        });
    </script>

</head>

<body oncontextmenu="return false">
    <div id="map_canvas"></div>
    
    <div id="divList" class="showResult divOpacity">
        <div id="divOpcEdificio"><a class="opc_orden_up">Opciones</a></div>
        <div id="divOpciones" style="display:none; margin:10px 0 0 0;">
            <div id="lisOpcionesEdi" style="position:absolute; background-color:#CDCDCD; color:#ffffff; width:180px; left:0;">
                <table class="tb_detalle_opc" cellspacing="1" cellpadding="0">
                <tr>
                    <td><a class="opc_lista" href="javascript: history.back();">Regresar</a></td>
                </tr>
                <!--
                <tr>
                    <td><a class="opc_lista" href="">Ver detalle</a></td>
                </tr>
                -->
                </table>
            </div>
        </div>
    </div>
</body>
</html>