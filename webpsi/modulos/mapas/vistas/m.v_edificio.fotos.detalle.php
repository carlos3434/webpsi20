<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<!-- <script type="text/javascript" src="../../js/jquery/jquery.js"></script> -->
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	var imageID = 0;

    $("#pirata_thumbs .nroFoto").click(function() {
    	var image_id = $(this).attr('id');
        var image_name = images[image_id]['image'];

        if ( imageID != image_id ) {
            $('.nroFoto').removeClass('nroFoto_selected');
        	$(this).addClass('nroFoto_selected');

            //alert(image_name);
            $("#loadFotoEdificio").show();
            setTimeout("cargaImagen('" + image_name + "')", 2000);
            
            imageID = image_id;
        }
        
    });

    cargaImagen = function(image_name) {
    	$("#pirata_image").html('<img src="../../pages/imagenComponente.php?imgcomp=' + image_name + '&dircomp=edi&w=480">');
    	$("#loadFotoEdificio").hide();
    }
    
});

var images = <?php echo json_encode($arrDataFotos);?>;
</script>

<style>
em {
    color: #ff0000;
}
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 2px;
}

table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

#pirata_image {
    min-height: 300px;
}

.nroFoto {
    font-size: 14px;
    padding: 4px;
    color: #4B4B4B;
}

.nroFoto_selected {
    background-color: #1E9EBB;
    color: #ffffff;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Detalle fotos del edificio</td>
</tr>
</table>

<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">

    <?php
    if ( !empty($arrDataFotos) ) {
    ?>
    <table cellspacing="1" cellpadding="2" border="0">
    <tr>
        <td width="100%" valign="top">
            <div>
                <div id="loadFotoEdificio" style="display:none; opacity:0.5; color:#ffffff; position:absolute; background-color:#000000; width:120px; padding:5px; font-size:14px;">
                    Cargando...
                </div>
            </div>
            <div id="pirata_image">
                <img src="../../pages/imagenComponente.php?imgcomp=<?php echo $arrDataFotos[0]['image']?>&dircomp=edi&w=480">
            </div>
        </td>
    </tr>
    <tr>
        <td valign="top" width="90">
            <div id="pirata_thumbs">	
                <?php
                $it = 0;
                $i = 1;
                foreach ( $arrDataFotos as $foto) {
                $selected = ( $i == 1 ) ? 'nroFoto_selected' : '';
                ?>                
                <a href="javascript: void(0);" class="nroFoto <?php echo $selected?>" id="<?php echo $it?>">FOTO <?php echo $i?></a> &nbsp;
                <?php
                $it++;
                $i++;
                }
                ?>
            </div>
        </td>
    </tr>
    </table>
    <br />
    
    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tr>
        <td class="da_titulo_gest" colspan="6">DATOS FOTOS DEL EDIFICIO</td>
    </tr>
    <tr style="text-align: left;" width="30%">
        <td width="30%" class="da_subtitulo">FECHA FOTO</td>
        <td width="70%"><?php echo obtenerFecha($arrObjFotosDetalle[0]->__get('_fechaFoto'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td><b><?php echo $arrObjFotosDetalle[0]->__get('_desEstado')?></b></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
        <td><?php echo nl2br($arrObjFotosDetalle[0]->__get('_observaciones'))?></td>
    </tr>
    </table>
    <?php
    } else {
    ?>
    <table cellspacing="1" cellpadding="2" border="0" width="100%" height="200">
    <tr>
        <td valign="middle" align="center">
            <p><b>No existe ninguna foto para este registro.</b><br />
            Agregue fotos en la opci&oacute;n <i>Adjuntar Fotos del Edificio</i></p>
        </td>
    </tr>
    </table>
    <?php
    }
    ?>
    
    <div style="height:5px;"></div>
    
</div>

</body>
</html>
