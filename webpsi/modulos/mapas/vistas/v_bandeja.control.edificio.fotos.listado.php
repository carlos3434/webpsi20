<?php
if ( !empty($arrObjEdificioFotos) ) {

    $itemA = 1;
    foreach ($arrObjEdificioFotos as $objEdificioFoto) {
        $imgMov = 'application_add.png';
        if ( $objEdificioFoto->__get('_ingreso') == '2' ) {
            $imgMov = 'mobile_phone.png';
        }
    ?>
        <tr>
            <td class="celdaX"><?php echo $itemA++; ?></td>
            <td class="celdaX">
                <a href="javascript:void(0)" onclick="verFotoDetalle('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')" title="Ver fotos">
                    <img width="11" height="11" title="Ver fotos" src="img/ico_detalle.jpg" style="margin:3px;" />
                </a>
            </td>
            <td align="center"><img src="img/<?php echo $imgMov?>" width="14" height="14" alt="" title="" /></td>
            <td class="celdaX"><?php echo obtenerFecha($objEdificioFoto->__get('_fechaFoto')); ?></td>
            <td class="celdaX"><?php echo $objEdificioFoto->__get('_desEstado'); ?></td>
            <td class="celdaX"><?php echo obtenerFecha($objEdificioFoto->__get('_fechaInsert')); ?></td>
            <td class="celdaX" style="text-align:left;">
                <?php
                if ( strlen($objEdificioFoto->__get('_observaciones')) > 50  ) {
                ?>
                    <?php echo substr($objEdificioFoto->__get('_observaciones'), 0, 50); ?><a style="color:blue;" href="javascript:void(0)" id="verMas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>" onclick="verMas('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')">m&aacute;s...</a><span style="display:none;" id="mas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>"><?php echo substr($objEdificioFoto->__get('_observaciones'), 50);?></span> <a style="color:blue;display:none;" href="javascript:void(0)" id="ocultarMas_<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>" onclick="ocultarMas('<?php echo $objEdificioFoto->__get('_idEdificioFoto')?>')">ocultar</a>
                <?php
                } else {
                    echo $objEdificioFoto->__get('_observaciones');
                }
                ?>
            </td>
        </tr>
    <?php
    }
}