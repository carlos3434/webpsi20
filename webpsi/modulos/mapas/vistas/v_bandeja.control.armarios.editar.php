<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">DATOS DEL ARMARIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="15%" class="da_subtitulo">ARMARIO</td>
        <td width="15%" class="da_import"><?php echo $arrObjArmario[0]->__get('_armario')?></td>
        <td width="20%" class="da_subtitulo">ZONAL</td>
        <td width="20%" class="da_import"><?php echo $arrObjArmario[0]->__get('_zonal')?></td>
        <td width="15%" class="da_subtitulo">MDF</td>
        <td width="15%" ><?php echo $arrObjArmario[0]->__get('_mdf')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">CAPACIDAD</td>
        <td ><?php echo $arrObjArmario[0]->__get('_qcaparmario')?></td>
        <td class="da_subtitulo">PARES LIBRES</td>
        <td><?php echo $arrObjArmario[0]->__get('_qparlib')?></td>
        <td class="da_subtitulo">PARES RESERVADOS</td>
        <td ><?php echo $arrObjArmario[0]->__get('_qparres')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">PARES DISTRIBUIDOS</td>
        <td><?php echo $arrObjArmario[0]->__get('_qdistrib')?></td>
        <td class="da_subtitulo">ESTADO ACTUAL</td>
        <td><b><?php echo strtoupper($arrObjArmario[0]->__get('_desEstado'))?></b></td>
        <td class="da_subtitulo">FECHA ESTADO ACTUAL</td>
        <td><b><?php echo obtenerFecha($arrObjArmario[0]->__get('_fechaEstado'))?></b></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N</td>
        <td colspan="5">
            <input type="text" name="txt_direccion" id="txt_direccion" value="<?php echo strtoupper($arrObjArmario[0]->__get('_direccion'))?>" style="width:90%" >
            <input type="hidden" name="mtgespkarm" id="mtgespkarm" value="<?php echo $arrObjArmario[0]->__get('_mtgespkarm')?>">
        </td>
    </tr>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody><tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px">
                <input type="button" value="Grabar" name="grabar" title="Grabar" id="btn_grabar" onclick="javascript: updateDatosArmario();" class="boton">&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" onclick="javascript: cerrarVentana();" class="boton">
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    
    <div style="margin: 10px 0 0 0; display: none;" id="msg">
        <img alt="" src="img/loading.gif"> <b>Procesando cambios.</b>
    </div>
</div>


