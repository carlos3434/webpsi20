<script type="text/javascript">
$(document).ready(function() {
    var imageID = 0;
    $("#pirata_thumbs img").click(function() {
        var image_id = $(this).attr('id');
        var image_name = images[image_id]['image'];
        $("#pirata_image").html('<img src="pages/imagenComponente.php?imgcomp=' + image_name + '&dircomp=dad&w=400">');
        imageID = image_id;
    });
});
var images = <?php echo json_encode($dataFotos); ?>;
</script>
<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    <table id="tbl_detalle" class="tb_detalle_actu" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">DATOS DEL PIRATA</td>
    </tr>
    <tr style="text-align: left;">
        <td width="25%" class="da_subtitulo">DIRECCI&Oacute;N ANTENA</td>
        <td width="75%" colspan="3"><?php echo strtoupper($arrObjDuna[0]->__get('_direccion'))?></td>
    </tr>
    <tr style="text-align: left;">
        <td width="25%" class="da_subtitulo">TIPO ANTENA</td>
        <td width="25%" class="da_import"><?php echo strtoupper($arrObjDuna[0]->__get('_tipo'))?></td>
        <td width="25%" class="da_subtitulo">FECHA DETECCI&Oacute;N</td>
        <td width="25%"><?php echo obtenerFecha($arrObjDuna[0]->__get('_fechaInsert'))?></td>
    </tr>
    </table>
</div>
<div id="divAlbunFotos" style="display:block;">
    <?php
    if (!empty($dataFotos)) {
    ?>
    <table cellspacing="1" cellpadding="2" border="0">
    <tr>
        <td width="410" valign="top">
            <div id="pirata_image">
                <img src="pages/imagenComponente.php?imgcomp=<?php echo $arrObjDuna[0]->__get('_foto1')?>&dircomp=dad&w=400">
            </div>
        </td>
        <td valign="top" width="90">
            <div id="pirata_thumbs">	
            <?php
                $it = 0;
                foreach ($dataFotos as $foto) {
            ?>
            <img id="<?php echo $it?>" width="110" src="pages/imagenComponente.php?imgcomp=<?php echo $foto['image']?>&dircomp=dad&w=110">
            <?php
                    $it++;
                }
            ?>
            </div>
        </td>
    </tr>
    </table>
    <?php
    } else {
    ?>    
    <table cellspacing="1" cellpadding="2" border="0" width="100%" height="200">
    <tr>
        <td valign="middle" align="center">
            <p><b>No existe ninguna foto para este pirata.</b><br />
            Agregue fotos en la pesta&ntilde;a <i>Adjuntar Fotos al Pirata</i></p>
        </td>
    </tr>
    </table>    
    <?php
    }
    ?>
</div>