<?php
if($total_registros>0){
?>
                    <thead>
                                    <tr style="line-height:13px;">
                                        <th class="headTbl center" width="2%">#</th>
                                        <th class="headTbl center" width="5%">Opciones</th>
                                        <th class="headTbl center" width="8%">Cod. req.</th>
                                        <th class="headTbl center" width="5%">Zonal</th>
                                        <th class="headTbl center" width="8%">Jefatura</th>
                                        <th class="headTbl center" width="8%">Negocio</th>
                                        <th class="headTbl center" width="7%">Mdf</th>
                                        <th class="headTbl center" width="10%">Elem. Red</th>
                                        <th class="headTbl center" width="8%">Elemento</th>
                                        <th class="headTbl center" width="10%">Fec. registro</th>
                                        <th class="headTbl center" width="10%">Inicio trab.</th>
                                        <th class="headTbl center" width="10%">Fin trab.</th>
                                        <th class="headTbl center" width="10%">Estado</th>
                                    </tr>
				</thead>
				<tbody>
                                <?php
				$item = (($pagina - 1) * TAM_PAG_LISTADO) + 1;
				foreach($trabajos_planta as $trabajo_planta){
                                    
                                    $flag_fueraPlazo = 'celdaX';
                                    $alt_fueraPlazo = '';
                                    if( $trabajo_planta['fecha_fin_trabajo'] < date('Y-m-d H:i:s') && $trabajo_planta['estado'] == 'PENDIENTE' ) {
                                        $flag_fueraPlazo = 'celdaY fueraPlazo';
                                        $alt_fueraPlazo = 'title = "fuera de plazo"';
                                    }
                                    
                                    /*$elemento = '';
                                    if($trabajo_planta['codplantaptr'] == 'B'){ //STB/ADSL
                                        
                                        if( $trabajo_planta['tipored'] == 'D' ) {
                                            if( $trabajo_planta['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                                                $elemento = $trabajo_planta['mdf'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '19' ) { //Cable Alimentador
                                                $elemento = $trabajo_planta['ccable'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '3' ) { //terminal
                                                $elemento = $trabajo_planta['terminal'];
                                            }
                                        }else if( $trabajo_planta['tipored'] == 'F' ) {
                                            if( $trabajo_planta['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                                                $elemento = $trabajo_planta['mdf'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '19' ) { //Cable Alimentador
                                                $elemento = $trabajo_planta['parpri'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '8' ) { //armario
                                                $elemento = $trabajo_planta['carmario'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '23' ) { //cable secundario
                                                $elemento = $trabajo_planta['cbloque'];
                                            }elseif( $trabajo_planta['codelementoptr'] == '3' ) { //terminal
                                                $elemento = $trabajo_planta['terminal'];
                                            }
                                        }

                                    }else if($trabajo_planta['codplantaptr'] == 'C'){ //CATV
                                        
                                        if( $trabajo_planta['codelementoptr'] == '28' ) { //Nodo //Agregado 12/12/2011
                                            $elemento = $trabajo_planta['mdf'];
                                        }if( $trabajo_planta['codelementoptr'] == '11' ) { //troba
                                            $elemento = $trabajo_planta['carmario'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '2' ) { //amplificador
                                            $elemento = $trabajo_planta['cbloque'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '1' ) { //tap
                                            $elemento = $trabajo_planta['terminal'];
                                        }elseif( $trabajo_planta['codelementoptr'] == '20' ) { //borne
                                            $elemento = $trabajo_planta['borne'];
                                        }
                                    }*/
                                    
                                    $elemento = obtenerElementoRegistroTrabajoFFTT($trabajo_planta);
                                    
                                    ?>
                                    <tr>
                                    <td class="<?php echo $flag_fueraPlazo?>" <?php echo $alt_fueraPlazo?>><?php echo $item++; ?></td>
                                    <td class="celdaX">
                                        
                                        <a href="javascript:DetalleRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" title="Ver detalle">
                                            <img width="12" height="12" title="Ver detalle" src="img/ico_detalle.jpg">
                                        </a>
                                        <?php
                                        if( $trabajo_planta['estado'] == 'PENDIENTE' ) {
                                        ?>
                                        <a href="javascript:CambiarFechaRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" title="Cambiar fechas de Registro de Trabajo">
                                            <img width="12" height="12" title="Cambiar fechas de Registro de Trabajo" src="img/icon_horario_psi.png">
                                        </a>
                                        <a title="Gestionar" onclick="GestionarRegistroTrabajo('<?php echo $trabajo_planta['id_registro_trabajo_planta']?>')" href="javascript:void(0)">
                                            <img width="13" height="13" title="Gestionar" src="img/ico_gestionar.jpg">
                                        </a>
                                        <?php
                                        }else {
                                        ?>
                                            <img width="12" height="12" title="Cambiar fechas de Registro de Trabajo" src="img/icon_horario_psi_disabled.png">
                                            <img width="13" height="13" title="Gestionar" src="img/ico_gestionar_disabled.jpg">
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="celdaX"><?php echo 'RTP-' . $trabajo_planta['id_registro_trabajo_planta']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['zonal']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['jefatura_desc']; ?></td>
                                    <td class="celdaX"> <?php echo $trabajo_planta['negocio']; ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['mdf']; ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['descripcion']; ?></td>
                                    <td class="celdaX"><?php echo $elemento; ?></td>
                                    <td class="celdaX"><?php echo obtenerFecha($trabajo_planta['fecha_insert']); ?></td>
                                    <td class="celdaX"><?php echo obtenerFecha($trabajo_planta['fecha_inicio_trabajo']); ?></td>
                                    <td class="<?php echo $flag_fueraPlazo?>" <?php echo $alt_fueraPlazo?>><?php echo obtenerFecha($trabajo_planta['fecha_fin_trabajo']); ?></td>
                                    <td class="celdaX"><?php echo $trabajo_planta['estado']; ?></td>
                                    </tr>
			 <?php
			   }
			 ?>
				</tbody>
<?php
}else{
    ?>
    <tr>
        <td style="height:30px">Ningun registro encontrado</td>
    </tr>
<?php
}
?>