<!-- 
<script type="text/javascript" src="js/jquery/jquery.lightbox.js"></script>
<script type="text/javascript" src="css/jquery.lightbox.css"></script>
-->

<script>
$(document).ready(function() {
 
    var imageID = 0;

    /*
    $("#pirata_image a").lightBox();

    $("#pirata_image a").click(function() {
        alert("nuevo");
    });
    */

    $("#pirata_thumbs img").click(function() {
        var image_id = $(this).attr('id');
        var image_name = images[image_id]['image'];
        //alert(image_name);
        $("#pirata_image").html('<img src="pages/imagenComponente.php?imgcomp=' + image_name + '&dircomp=edi&w=400">');
        imageID = image_id;
    });
});

var images = <?php echo json_encode($arrDataFotos);?>;

</script>

<div id="divAlbunFotos" style="display:block;">
    <?php
    if ( !empty($arrDataFotos) ) {
    ?>
    <table cellspacing="1" cellpadding="2" border="0">
    <tr>
        <td width="410" valign="top">
            <div id="pirata_image">
                <img src="pages/imagenComponente.php?imgcomp=<?php echo $arrDataFotos[0]['image']?>&dircomp=edi&w=400">
            </div>
        </td>
        <td valign="top" width="90">
            <div id="pirata_thumbs">	
                <?php
                $it = 0;
                foreach ( $arrDataFotos as $foto) {
                ?>                
                <img id="<?php echo $it?>" width="110" src="pages/imagenComponente.php?imgcomp=<?php echo $foto['image']?>&dircomp=edi&w=110">
                <?php
                $it++;
                }
                ?>
                    
            </div>
        </td>
        <td valign="top">
            
            <table id="tbl_detalle" class="tb_detalle_actu" width="100%" border="0">
            <tbody>
            <tr>
                <td class="da_titulo_gest" colspan="2">DATOS DE FOTO DEL EDIFICIO</td>
            </tr>
            <tr style="text-align: left;" width="30%">
                <td class="da_subtitulo">FECHA FOTO</td>
                <td><?php echo obtenerFecha($arrObjFotosDetalle[0]->__get('_fechaFoto'))?></td>
            </tr>
            <tr style="text-align: left;">
                <td class="da_subtitulo">ESTADO</td>
                <td><b><?php echo $arrObjFotosDetalle[0]->__get('_desEstado')?></b></td>
            </tr>
            <tr style="text-align: left;">
                <td class="da_subtitulo" valign="top">OBSERVACIONES</td>
                <td><?php echo strtoupper($arrObjFotosDetalle[0]->__get('_observaciones'))?></td>
            </tr>
            </tbody>
            </table>
            
        </td>
    </tr>
    </table>
    <?php
    } else {
    ?>
    
    <table cellspacing="1" cellpadding="2" border="0" width="100%" height="200">
    <tr>
        <td valign="middle" align="center">
            <p><b>No existe ninguna foto para este registro.</b><br />
            Agregue fotos en la pesta&ntilde;a <i>Adjuntar Fotos del Edificio</i></p>
        </td>
    </tr>
    </table>
    
    <?php
    }
    ?>

</div>