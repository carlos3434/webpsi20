<style type="text/css">
em {
    color: #ff0000;
}
</style>
<script type="text/javascript">
    $("#fecha_expediente").datepicker({ 
        dateFormat: 'dd/mm/yy',
        yearRange:'-20:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    
    $('#frm_asociar_pirata').ajaxForm({ 
        dataType    : 'json', 
        beforeSubmit: validate,
        success     : processJson     
    }); 
    
    function validate(formData, jqForm, options){
        var query = $("input.required");
        
        var is_validate = 1;
        $.each( query, function() {
            if( $(this).val() == '' ) {
                alert("Ingrese el campo " + $(this).attr('msg'));
                $(this).focus();
                is_validate = 0;
                return false;
            }
        });
        
        if(!is_validate) {
            return false;
        }
 
        if( $("#foto1").val() == '') {
            alert("Ingrese la Foto 1 Atis");
            return false;
        }
        if( $("#foto2").val() == '') {
            alert("Ingrese la Foto 2 Atis");
            return false;
        }
        
        $("input[name=btn_guardar]").attr('disabled','disabled');
        
        var error_foto = '';
        var permitida = false;
        var image = $('input[name=imagen_atis[]]').fieldValue(); 

        $.each(image, function(i, item) {
            if(image[i]!=''){
                var extension = (image[i].substring(image[i].lastIndexOf("."))).toLowerCase();
                if (! (extension && /^(.jpg|.jpeg)$/.test(extension))){ //.jpg|.png|.jpeg|.gif
                    // extensiones permitidas
                    error_foto = 'Error: Solo se permiten imagenes de tipo JPG.';
                    // cancela upload
                    $("input[name=btn_guardar]").attr('disabled','');
                    permitida = true;
                }
            }
        });
        if(permitida){ alert(error_foto); return false; }
    }
     
    function processJson(data) { 
        $("input[name=btn_guardar]").attr('disabled','');
        if(data['tipo'] == 'imagen') { 
            alert(data.mensaje); 
            return false; 
        }
        if(data['flag'] == '1'){
            alert(data['mensaje']);
            
            $("#NoasociarPirataAbonado").attr('disabled','disabled');
            
            cerrarVentanaAsociarForm();
            
            //actualiza la lista de clientes cercanos
            listadoClientesCercanos('trm', $("#idduna").val());
            
            //actualiza listado de clientes asociados
            listadoClientesAsociados($("#idduna").val());
            buscarDunaInternet();
        }else{
            alert(data['mensaje']);
        } 
    }

</script>


<form enctype="multipart/form-data" action="modulos/maps/bandeja.duna.internet.php?cmd=RegistrarPirata" method="post" name="frm_asociar_pirata" id="frm_asociar_pirata">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">			
			<input type="hidden" id="idduna" name="idduna" class="required" value="<?php echo $idComponente?>"/>
			<div id="cont_actu_detalle">
				<div style="height:15px;"></div>
					<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
					<tbody>
                        <tr>
                            <td class="da_titulo_gest" colspan="6">DATOS DEL EXPEDIENTE</td>
                        </tr>
                        <tr style="text-align: left;">
                            <td class="da_subtitulo" width="15%">EXPEDIENTE</td>
                            <td width="20%">
                                    <input type="text" readonly="readonly" name="expediente" id="expediente" value="<?php echo $arrAbonado[0]['ddn_telefono'] . '-' . $arrAbonado[0]['Inscripcion'] ?>" maxlength="100" class="required" size="20" msg="expediente" /><em>(*)</em>
                            </td>
                            <td class="da_subtitulo" width="15%">FECHA EXPEDIENTE</td>
                            <td width="20%">
                                    <input id="fecha_expediente" name="fecha_expediente" type="text" value="" readonly="readonly" size="8" class="required" msg="fecha de expediente" /><em>(*)</em>
                            </td>
                            <td class="da_subtitulo" width="15%">FECHA DETECCION</td>
                            <td width="15%">
                                    <input type="text" value="" maxlength="50" size="20" name="fecha_deteccion" id="fecha_deteccion" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td class="da_subtitulo" style="text-align:left; vertical-align:top">OBSERVACIONES:</td>
                            <td colspan="5" style="text-align:left;">
                                    <textarea name="observaciones" id="observaciones" cols="80" rows="2">SIN OBSERVACIONES</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:5px;" colspan="6"></td>
                        </tr>
                        <tr>
                            <td class="da_titulo_gest" colspan="6">DATOS DEL CLIENTE</td>
                        </tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">TITULAR</td>
						<td colspan="3">
							<input type="text" value="<?php echo $arrAbonado[0]['Cliente']?>" maxlength="100" onkeypress="return soloLetras(event)" class="required" size="80" name="titular" id="titular" msg="titular" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">NRO. DOCUMENTO/DNI</td>
						<td>
							<input type="text"  value="" maxlength="20" onkeypress="return soloNumeros(event)" class="required" size="10" name="dni" id="dni" msg="nro. documento/dni" /><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">DEPARTAMENTO</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['departamento']?>" maxlength="100" onkeypress="return soloLetras(event)" class="required" size="30" name="departamento" id="departamento" msg="departamento" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">PROVINCIA</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['provincia']?>" maxlength="100" onkeypress="return soloLetras(event)" class="required" size="30" name="provincia" id="provincia" msg="provincia" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">DISTRITO</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['distrito']?>" maxlength="100" onkeypress="return soloLetras(event)" class="required" size="20" name="distrito" id="distrito" msg="distrito" /><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">DIRECCION ANTENA</td>
						<td colspan="5">
							<input type="text" style="border: 2px solid #FF0000;" value="<?php echo $arrObjDuna[0]->__get('_direccion')?>" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="80" name="direccion_antena" id="direccion_antena" msg="direccion antena" /><em>(*)</em>
						</td>
					</tr>
                                        <tr style="text-align: left;">
						<td class="da_subtitulo">DIRECCION TELEFONO</td>
						<td colspan="5">
							<input type="text" style="border: 2px solid #FF0000;" value="<?php echo $arrAbonado[0]['Direccion']?>" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="80" name="direccion_telefono" id="direccion_telefono" msg="direccion telefono" /><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">TELEFONO</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['ddn_telefono']?>" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="telefono" id="telefono" msg="telefono" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">INSCRIPCION</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['Inscripcion']?>" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="inscripcion" id="inscripcion" msg="inscripcion" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">ESTADO LINEA</td>
						<td>
							<input type="text" value="ACTIVO" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="estado_linea" id="estado_linea" msg="estado linea" /><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">SPEEDY CONTRATADO</td>
						<td>
							<input type="text" value="<?php echo $arrAbonado[0]['PRODUCTO']?>" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="speedy_contratado" id="speedy_contratado" msg="speedy contratado" /><em>(*)</em>
						</td>
						<td class="da_subtitulo">ESTADO SPEEDY</td>
						<td colspan="3">
							<input type="text" value="ACTIVO" maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="estado_speedy" id="estado_speedy" msg="estado speedy" /><em>(*)</em>
						</td>
					</tr>
                    <tr>
                        <td style="height:5px;" colspan="6"></td>
                    </tr>
                    <tr>
                        <td class="da_titulo_gest" colspan="6">FOTOS ATIS</td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">FOTO 1</td>
                        <td colspan="5">
                            <input id="foto1" name="imagen_atis[]" type="file" size="25" /> &nbsp;<em>(*)</em>
                            <a onclick="limpiarFoto('foto1')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o m&aacute;ximo: 2MB) </span>
                        </td>
                    </tr>
                    <tr style="text-align: left;">
                        <td class="da_subtitulo">FOTO 2</td>
                        <td colspan="5">
                            <input id="foto2" name="imagen_atis[]" type="file" size="25" /> &nbsp;<em>(*)</em>
                            <a onclick="limpiarFoto('foto2')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
                            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o m&aacute;ximo: 2MB) </span>
                        </td>
                    </tr>
					</table>
					<div style="height:15px;"></div>		
    				<table width="100%" cellspacing="1" cellpadding="0" border="0">
                    <tbody>
                    <tr>
                        <td width="90%" style="text-align: left; vertical-align: top">
                            <div style="padding-top: 5px; font-size: 11px">
                                <input type="submit" value="Registrar" name="guardar" title="Registrar" id="btn_guardar" class="boton">&nbsp;
                                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar_asociar_form" onclick="javascript: cerrarVentanaAsociarForm();" class="boton">
                            </div>
                        </td>
                        <td colspan="6" align="right">
                            <!--
                                <a href="javascript:cerrarAsociarForm()" style="text-decoration: none;">
                                        <img width="100" height="26" title="Regresar para asociar pirata" alt="Regresar para asociar pirata" src="img/btn_return_mov.png">
                                </a>
                            -->
                        </td>
                    </tr>
                    </tbody>
    				</table>				
    				<div style="margin: 10px 0 0 0; display: none;" id="msg">
    					<img alt="" src="img/loading.gif"> <b>Procesando cambios.</b>
    				</div>				
			    </div>
		    </td>
	    </tr>
	</table>
	<br />
</form>