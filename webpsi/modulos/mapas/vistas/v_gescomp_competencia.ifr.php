<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


$array_zonal = $_SESSION['USUARIO_ZONAL'];


$obj_capasarea = new data_ffttCapasArea();
$array_capas_mdf = $obj_capasarea->get_capas_by_tipocapa($conexion, 'MDF');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<!--<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>-->
<script type="text/javascript" src="../../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

<script type="text/javascript" src="../../../js/jquery/prettify.js"></script>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript">

var poligonosDibujar_array = [];
var capa_exists = 0;

var myLayout;
var map = null;
var markers_arr = [];
var makeMarkers_arr = [];
var ptos_arr = [];

$(document).ready(function () {
	
	myLayout = $('body').layout({
		// enable showOverflow on west-pane so popups will overlap north pane
		west__showOverflowOnHover: true
	
	//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
	});

	var M = {
    	initialize: function() {

			if (typeof (google) == "undefined") {
				alert('Verifique su conexion a maps.google.com');
				return false;
			}
    	
	    	var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
	        var myOptions = {
	          	zoom: 11,
	          	center: latlng,
	          	mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        }
    };
	
	
	$("#selZonal").change(function() {
        IDzon = this.value;
        if(IDzon == "") {
            return false;
        }
        data_content = "action=buscarMdf&IDzon=" + IDzon;
        $.ajax({
            type:   "POST",
            url:    "../gescomp.competencia.main.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selectMdf = '';
                for(var i in datos) {
                    selectMdf += '<option value="' + datos[i]['IDmdf'] + '">' + datos[i]['IDmdf'] + ' - ' + datos[i]['Descmdf'] + '</option>'
                }
              	$("#selMdf").html(selectMdf);
            }
        });

        $("#selTipRed").html('<option value="">Seleccione</option><option value="D">Directa</option><option value="F">Flexible</option>');
        $("#selCableArmario").html('<option value="">Seleccione</option>');
		$("#poligono").removeAttr('disabled');
    });
	
	$("#poligono").click(function() {
    	if($("#poligono").is(':checked')) { 
			
			var IDmdf    = $("#selMdf").val();
			
			data_content = "action=buscar&area_mdf=si&IDmdf=" + IDmdf;
			$.ajax({
				type:   "POST",
				url:    "../gescomp.competencia.main.php",
				data:   data_content,
				dataType: "json",
				success: function(data) {
				 
					if( capa_exists ) {
						//limpiamos todas las capas del mapa
						clearPoligonos();
					}

					var poligonos = data['xy_poligono'];
					if( poligonos.length > 0 ) {
						//pintamos los poligonos
						for (var i in poligonos) {
							loadPoligono(poligonos[i], i);
						}
						
						//setea el zoom y el centro
						//map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
						//map.setZoom(13);
					}
				}
			});

        }else {  
			
			if( capa_exists ) {
				//limpiamos todas las capas del mapa
				clearPoligonos();
			}
        }  
	});
	
	$("#btnBuscar").click(function() {
        // lista y agregar los markers en divs "sidebar" y "map"
        listar_edificios();
    });


	M.initialize(); 
});

function clearPoligonos() {
	for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujar_array[n]; n++) {
		poligonoDibujar.setMap(null);
	}
}

function loadPoligono(xy_array, z) {

	var datos 			= xy_array['coords'];
	var color_poligono 	= xy_array['color'];
	var poligonoCoords 	= [];

	if( datos.length > 0) {
		for (var i in datos) {
			var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
			poligonoCoords.push(pto_poligono);
		}

		// Construct the polygon
		// Note that we don't specify an array or arrays, but instead just
		// a simple array of LatLngs in the paths property
		poligonoDibujar = new google.maps.Polygon({
			paths: poligonoCoords,
			strokeColor: color_poligono,
			strokeOpacity: 0.6,
			strokeWeight: 3,
			fillColor: color_poligono,
			fillOpacity: 0.5
		});

		//agrego al array las capas seleccionadas
		poligonosDibujar_array.push(poligonoDibujar);

		//seteo la capa en el mapa
		poligonoDibujar.setMap(map);

		//seteamos que la capa existe
		capa_exists = 1;

	}
	else {
		//alert('aaa');
		capa_exists = 0;
	}		
}


function listar_edificios() {

	IDzonal	 = $("#selZonal").val();
    IDmdf    = $("#selMdf").val();
    datosXY = $("#selDatosXY").val();
	
	var area_mdf = 'no';
	if($("#poligono").is(':checked')) { 
		area_mdf = 'si';
	}
	
    if(IDzonal == "" || IDmdf == "") {
        alert("Ingrese todos los criterios de busqueda.");
        return false;
    }
	
	
	/**
     * makeMarker() ver 0.2
     * creates Marker and InfoWindow on a Map() named 'map'
     * creates sidebar row in a DIV 'sidebar'
     * saves marker to markerArray and markerBounds
     * @param options object for Marker, InfoWindow and SidebarItem
     */
     var infoWindow = new google.maps.InfoWindow();
     var markerBounds = new google.maps.LatLngBounds();
     var markerArray = [];
          
     function makeMarker(options){
		var pushPin = null;
			/*var pushPin = new google.maps.Marker({
             map: map
         });
         pushPin.setOptions(options);*/
			
     	if( options.position != '(0, 0)' ) {
	            pushPin = new google.maps.Marker({
	                map: map
	            });
				
				makeMarkers_arr.push(pushPin);
				
	            pushPin.setOptions(options);
	            google.maps.event.addListener(pushPin, "click", function(){
	                infoWindow.setOptions(options);
	                infoWindow.open(map, pushPin);
	                if(this.sidebarButton)this.sidebarButton.button.focus();
	            });
	            var idleIcon = pushPin.getIcon();

	            markerBounds.extend(options.position);
	            markerArray.push(pushPin);
     	}else {
     		pushPin = new google.maps.Marker({
                map: map
            });
			
			makeMarkers_arr.push(pushPin);
			
            pushPin.setOptions(options);
     	}
         
         if(options.sidebarItem){
             pushPin.sidebarButton = new SidebarItem(pushPin, options);
             pushPin.sidebarButton.addIn("sidebar");
         }
         
         return pushPin;       	
     }

     google.maps.event.addListener(map, "click", function(){
     	infoWindow.close();
     });

     /*
      * Creates an sidebar item 
      * @param marker
      * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
     */
     function SidebarItem(marker, opts) {
         //var tag = opts.sidebarItemType || "button";
         var tag = opts.sidebarItemType || "span";
         var row = document.createElement(tag);
         row.innerHTML = opts.sidebarItem;
         row.className = opts.sidebarItemClassName || "sidebar_item";  
         row.style.display = "block";
         row.style.width = opts.sidebarItemWidth || "290px";
         row.onclick = function(){
             google.maps.event.trigger(marker, 'click');
         }
         row.onmouseover = function(){
             google.maps.event.trigger(marker, 'mouseover');
         }
         row.onmouseout = function(){
             google.maps.event.trigger(marker, 'mouseout');
         }
         this.button = row;
     }
     // adds a sidebar item to a <div>
     SidebarItem.prototype.addIn = function(block){
         if(block && block.nodeType == 1)this.div = block;
         else
             this.div = document.getElementById(block)
             || document.getElementById("sidebar")
             || document.getElementsByTagName("body")[0];
         this.div.appendChild(this.button);
     }


	 data_content = "action=buscarCompetencia&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf + "&datosXY=" + datosXY + "&area_mdf=" + area_mdf;
     $("#fongoLoader").show();
     $("#btnBuscar").attr('disabled', 'disabled');
     $("#sidebar").html("");
     var tabla = '';
     var detalle = '';
     $.ajax({
         type:   "POST",
         url:    "../gescomp.competencia.main.php",
         data:   data_content,
         dataType: "json",
         success: function(data) {
		 
			clearMarkers();
			clearMakeMarkers();
			
			
			if( capa_exists ) {
				//limpiamos todas las capas del mapa
				clearPoligonos();
			}

			var poligonos = data['xy_poligono'];

			if( poligonos.length > 0 ) {
				//pintamos los poligonos
				for (var i in poligonos) {
					loadPoligono(poligonos[i], i);
				}
				
				//setea el zoom y el centro
				map.setCenter(new google.maps.LatLng(data['center_map']['y'], data['center_map']['x']));
				//map.setZoom(13);
			}

			
			if( data['competencias'].length > 0 ) {


    	 		if( data['competencias_con_xy'].length == 0 ) {

    	 			var datos = data['competencias']; 
    	 			var estados = data['estados'];

    	 			var x1 = y1 = null;
    	 			if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
    	 				x1 = data['mdf']['x'];
    	 				y1 = data['mdf']['y'];
    	 			}else if( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
    	 				x1 = data['zonal']['x'];
    	 				y1 = data['zonal']['y'];
    	 			}
					
                	for (var i in datos) {
						var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
                		
                		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
        				tabla += '<tr id="row' + datos[i]['idcompetencia'] + '" class="unselected">';
        				tabla += '<td width="70">';
        				tabla += '<span id="' + datos[i]['idcompetencia'] + '" onclick="ir_mapa(\'' + datos[i]['idcompetencia'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
        				tabla += '&nbsp;<a class="tooltip" href="#"><img border="0" title="Cambiar estado" src="../../../img/edit_agenda.png"><span>';
        				for(var e in estados) {
        					tabla += '<p onclick="changeEstado(\'' + datos[i]['idcompetencia'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/competencia/competencia-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				tabla += '</span></a></td>';
        				tabla += '<td width="70">' + datos[i]['idcompetencia'] + '</td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['idcompetencia'] + '" name="xy[' + datos[i]['idcompetencia'] + '][x]" value="' + x + '"></td>';
        				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['idcompetencia'] + '" name="xy[' + datos[i]['idcompetencia'] + '][y]" value="' + y + '"></td>';
        				tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/competencia/competencia-' + datos[i]['estado'] + '.gif" width="12"></td>';
        				tabla += '</tr>';
        				tabla += '</table>';
						
						/* markers and info window contents */
                        makeMarker({
                        	draggable: false,
                            position: new google.maps.LatLng(y1, x1),
                            title: '',
                            sidebarItem: tabla,
                            //content: detalle,
                            icon: "../../../img/markergreen.png"
                        });
                		
                    }
                	/* fit viewport to markers */
                    map.fitBounds(markerBounds);
                    //map.setZoom(10);

    	 		}else {

    	 		
	    	 		var datos = data['competencias']; 
	    	 		var estados = data['estados'];
	    	     	
	            	for (var i in datos) {
	
	            		var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
	            		var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];
	            		
	            		tabla = '<table border="1" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">';
	    				tabla += '<tr id="row' + datos[i]['idcompetencia'] + '" class="unselected">';
	    				tabla += '<td width="70">';
	    				tabla += '<span id="' + datos[i]['idcompetencia'] + '" onclick="ir_mapa(\'' + datos[i]['idcompetencia'] + '\')"><img border="0" title="Agregar o Editar x,y" src="../../../img/ico_map.png"></span>';
	    				tabla += '&nbsp;<a class="tooltip" href="#"><img border="0" title="Cambiar estado" src="../../../img/edit_agenda.png"><span>';
	    				for(var e in estados) {
        					tabla += '<p onclick="changeEstado(\'' + datos[i]['idcompetencia'] + '\',\'' + estados[e]['id'] + '\')"><img border="0" src="../../../img/map/competencia/competencia-' + estados[e]['id'] + '.gif" width="12"> ' + estados[e]['val'] + '</p>';
        				}
        				tabla += '</span></a></td>';
	    				tabla += '<td width="70">' + datos[i]['idcompetencia'] + '</td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="x' + datos[i]['idcompetencia'] + '" name="xy[' + datos[i]['idcompetencia'] + '][x]" value="' + x + '"></td>';
	    				tabla += '<td width="85"><input readonly="readonly" size="8" class="latlon" type="text" id="y' + datos[i]['idcompetencia'] + '" name="xy[' + datos[i]['idcompetencia'] + '][y]" value="' + y + '"></td>';
	    				tabla += '<td width="20"><img border="0" title="' + data['est'][datos[i]['estado']] + '" src="../../../img/map/competencia/competencia-' + datos[i]['estado'] + '.gif" width="12"></td>';
	    				tabla += '</tr>';
	    				tabla += '</table>';
	
	    				detalle = '<img border="0" src="../../../pages/imagenComponente.php?imgcomp=' + datos[i]['foto1'] + '&dircomp=com&w=120"><br />';
	    				detalle += '<b>Zonal:</b> LIMA<br />';
	    				detalle += '<b>Mdf:</b> ' + datos[i]['ura'] + '<br />';
	    				detalle += '<b>Competencia:</b> ' + datos[i]['idcompetencia'] + '<br />';
	    				detalle += '<b>X,Y:</b> ' + datos[i]['x'] + ',' + datos[i]['y'] + '<br /><br />';
						
						/* markers and info window contents */
	                    makeMarker({
	                    	draggable: false,
	                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
	                        title: datos[i]['idcompetencia'],
	                        //sidebarItem: '<img src="../../../img/map-icons-collection-2.0/icons/table-insert-row.png"> ' + datos[i]['title'] + '<input type="text" value="' + datos[i]['x'] + '" name="id-' + datos[i]['id'] + '" size="15"> <input type="text" value="' + datos[i]['y'] + '" name="id-' + datos[i]['id'] + '" size="15">',
	                        sidebarItem: tabla,
	                        content: detalle,
	                        icon: "../../../img/map/competencia/competencia-" + datos[i]['estado'] + ".gif"
	                    });
	            		
	                }
	
	                /* fit viewport to markers */
	                map.fitBounds(markerBounds);

    	 		}
    	 		
 	 		}else {
				
				var x_arm = y_arm = null;
				if( data['mdf']['x'] != null && data['mdf']['y'] != null ) {
					x_arm = data['mdf']['x'];
					y_arm = data['mdf']['y'];
	 			}else {
					x_arm = data['zonal']['x'];
					y_arm = data['zonal']['y'];
	 			}
				
				var latlng = new google.maps.LatLng(y_arm, x_arm);

				/* markers and info window contents */
				makeMarker({
					draggable: false,
					position: new google.maps.LatLng(y_arm, x_arm),
					title: '',
					icon: "../../../img/markergreen.png"
				});
				
				map.setCenter(latlng);

 	 		}
         
            //$("#divResult").html(datos);
            $("#btnBuscar").attr('disabled', '');
            $("#btnGrabar").attr('disabled', '');
            //setTimeout("hideLoader()", 1000);
            $("#fongoLoader").hide();
         }
     });
}

function ir_mapa(terminal_sel) {
	$("#trm_selected").val(terminal_sel);
	
	$(".unselected").attr("class", "unselected");
	$(".selected").attr("class", "unselected");
	$("#row" + terminal_sel).attr("class", "selected");

	google.maps.event.addListener(map, "click", function(event) {

		clearMarkers();
		
		marker = new google.maps.Marker({
            position: event.latLng,
            map: map, 
            title: terminal_sel
        });
		markers_arr.push(marker);
		
		if( $("#trm_selected").val() == terminal_sel ) {
			var Coords = event.latLng;
	        //alert("term sel=" + terminal_sel + ", coord=" + Coords);
			
			//var confirmar = confirm("Desea Registrar este lat, lng?");
			//if(confirmar) {
				$("#x" + terminal_sel).val(Coords.lng());
				$("#y" + terminal_sel).val(Coords.lat());
			//}	
		}
  	});
}

function clearMarkers() {
	for (var n = 0, marker; marker = markers_arr[n]; n++) {
		marker.setVisible(false);
	}
}

function clearMakeMarkers() {
	for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
		makeMarker.setVisible(false);
	}
}

function changeEstado(competencia, estado) {

	if( confirm("Esta seguro de cambiar el estado para esta competencia?") ) {
	    IDcom	 = competencia;
	    data_content = "action=saveEstado&IDcom=" + IDcom + "&estado=" + estado;
	
		$("#fongoLoader").show();
	    $.ajax({
	        type:     "POST",
	        dataType: "json",
	        url:      "../gescomp.competencia.main.php",
	        data:	  data_content,
	        success:  function(datos) {
	        	//alert("Datos Grabados");
	            //$("#divResult").html(datos);
	            if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listar_edificios();
	            }
	        }
	    });
	    //setTimeout("hideLoader()", 1000);
	    $("#fongoLoader").hide();
	}
}


function editarEdificio(idedificio, item, x, y) {
	//alert('111');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("principal/editarEdificio.popup.php", {
	    	idedificio	: idedificio,
	    	item		: item,
	    	x		  	: x,
	    	y			: y
	    },
    	function(data){
	    	parent.$("#childModal").html(data);
    	}
	);

    parent.$("#childModal").dialog({modal:true, width:'800px', hide: 'slide', title: 'Editar informacion de Edificio', position:'top'});
}


function Grabar() {

	if( confirm("Esta seguro de guardar los cambios para este edificio?") ) {
	
		IDzonal	 = $("#selZonal").val();
        IDmdf    = $("#selMdf").val();
	
		var query = $("input.latlon").serializeArray();
		json = {};
		
		for (i in query) {
			json[query[i].name] = query[i].value;
			//alert("name=" + query[i].name + ", value=" + query[i].value);
		} 
		$("#fongoLoader").show();
        $.ajax({
            type:     "POST",
            dataType: "json",
            url:      "../gescomp.competencia.main.php?action=save&IDzonal=" + IDzonal + "&IDmdf=" + IDmdf,
            data:     query,
            success:  function(datos) {
            	//alert("Datos Grabados");
                //$("#divResult").html(datos);
            	if( !datos['success'] ) {
					alert(datos['msg']);
	            }else {
	            	listar_edificios();
	            }
            }
        });
        //setTimeout("hideLoader()", 1000);
        $("#fongoLoader").hide();
	}
}


</script>
<style type="text/css">
    .ipt{
        border:none;
        color:#0086C3;
    }
    .ipt:focus{
        border:1px solid #0086C3;
    }
    body{
        font-family: Arial;
        font-size: 11px;
    }
    .ui-layout-pane {
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 7px; 
		overflow: auto;
	} 

	.ui-layout-resizer {
		background: #DDD; 
	} 

	.ui-layout-toggler {
		background: #AAA; 
	}
        .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
        .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
        .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
        .slt{
            border:1px solid #1E9EF9;
            background-color: #FAFAFA;
            height: 18px;
            font-size: 11px;
            color:#0B0E9D;
        }
        .box_checkboxes{
            border:1px solid #4C5E60;
            background: #E7F7F7;
            width:170px;
            height: 240px;
            overflow: auto;
            display:none;
            position: absolute;
            top:0px;
            left:0px;
            opacity:0.9;
        }
        .tb_data_box{
            width: 100%;
            border-collapse:collapse;
            font-family: Arial;
            font-size: 9px;
        }
        
</style>
</head>
<body>
    <div class="ui-layout-west">
        <table cellpadding="2" cellspacing="0" border="0" style="width:100%; border-collapse:collapse;">
            <tr>
                <td colspan="2"><span class="l_tit">Mantenimiento Competencia</span></td>
            </tr>
			<tr>
				<td width="150">Zonal:</td>
				<td>
					<select id="selZonal">
						<option value="">Seleccione</option>
						<?php foreach ($array_zonal as $obj_zonal) { ?>
							<option value="<?php echo $obj_zonal->__get('abv_zonal') ?>"><?php echo $obj_zonal->__get('abv_zonal') . ' - ' . $obj_zonal->__get('desc_zonal') ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Mdf:</td>
				<td>
					<div id="divMdf">
						<select id="selMdf">
							<option value="">Seleccione</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>Con X,Y:</td>
				<td>
					<select id="selDatosXY">
						<option value="">Todos</option>
						<option value="conXY">Terminales con X,Y</option>
						<option value="sinXY">Terminales sin X,Y</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="checkbox" name="poligono" id="poligono" disabled="disabled"> Mostrar poligono(Mdf)</td>
			</tr>
            <tr>
                <td colspan="2">
                    <input type="button" id="btnBuscar" value="Buscar">
                </td>
            </tr>
        </table>
		
		<div id="fongoLoader" class="bgLoader">
			<div class="imgLoader">
				<span>Cargando...</span>
			</div>
		</div>
		
		<div id="sidebarHeader">
			<table border="0" class="tb_listado" cellspacing="1" cellpadding="0" align="center" width="100%">
			<tr>
				<th width="70">#</th>
				<th width="70">Armario</th>
				<th width="85">X</th>
				<th width="85">Y</th>
				<th width="40">Est</th>
			</tr>
			</table>
		</div>
		<div style="height:280px; width:100%; overflow:auto;">
			<div id="sidebar"></div>
		</div>
		<input type="button" name="btnGrabar" id="btnGrabar" onclick="Grabar()" value="Grabar Datos" disabled="disabled">
		<input type="hidden" id="trm_selected" readonly="readonly">
		
        
        <div id="box_loading" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
        
    </div>
    <div class="ui-layout-center">
        <div id="box_resultado">

           <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>
        
        </div>
        
    </div>
        
	<div id="parentModal" style="display: none;">
    	<div id="childModal" style="padding: 10px; background: #fff;"></div>
    </div>
        
    </body>
</html>