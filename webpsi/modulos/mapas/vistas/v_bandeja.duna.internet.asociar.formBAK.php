<?php
$flag_duna          = 0;
$idduna             = '';
$expediente         = '';
$fecha_expediente   = date('d/m/Y');
$fecha_deteccion    = obtenerFecha($pirata[0]['fecha_insert']);
if( !empty($clientes_asociados) ) {
    $flag_duna          = 1;
    $idduna             = $clientes_asociados[0]['idduna'];
    $expediente         = $clientes_asociados[0]['expediente'];
    $fecha_expediente   = obtenerFecha($clientes_asociados[0]['fecha_expediente']);
    $fecha_deteccion    = obtenerFecha($clientes_asociados[0]['fecha_deteccion']);
}

if( !$flag_duna ) {
?>
<script>
$(function() {
    $("#fecha_expediente").datepicker({ 
        dateFormat: 'dd/mm/yy',
        yearRange:'-20:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });

});
</script>
<?php
}
?>

<!--<div id="content_pirata_asociar_form">-->
	<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 10px;">
			
			<input type="text" id="idcomponente" name="idcomponente" value="<?php echo $idcomponente;?>"/>
			
			<div id="cont_actu_detalle">
			
				
				<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
				<tbody>
				<tr>
					<td class="da_titulo" colspan="6">DATOS DEL EXPEDIENTE</td>
				</tr>
				<tr style="text-align: left;">
					<td class="da_subtitulo">EXPEDIENTE</td>
					<td>
						<input type="text" size="4" value="<?php echo $idduna;?>" name="idduna" id="idduna">
						<input type="text" name="nro_expediente" id="nro_expediente" value="<?php echo $expediente?>" <?php if($flag_duna) echo 'readonly="readonly"';?> maxlength="100" class="required" size="20"><em>(*)</em>
					</td>
					<td class="da_subtitulo">FECHA EXPEDIENTE</td>
					<td>
						<input id="fecha_expediente" name="fecha_expediente" type="text" value="<?php echo $fecha_expediente?>" readonly="readonly" size="8"><em>(*)</em>
					</td>
					<td class="da_subtitulo">FECHA DETECCION</td>
					<td>
						<input type="text" value="<?php echo $fecha_deteccion?>" <?php if($flag_duna) echo 'readonly="readonly"';?> maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="fecha_deteccion" id="fecha_deteccion" readonly="readonly"><em>(*)</em>
						<input type="hidden" id="idcomponente" value="<?php echo $pirata[0]['idcomponente']?>">
					</td>
				</tr>
				<tr>
					<td class="da_subtitulo" style="text-align:left; vertical-align:top">OBSERVACIONES:</td>
					<td colspan="5" style="text-align:left;">
						<textarea name="observaciones" id="observaciones" cols="80" rows="2" <?php if($flag_duna) echo 'readonly="readonly"';?>><?php echo $pirata[0]['observaciones']?></textarea>
					</td>
				</tr>
				</tbody>
				</table>
				
				<div style="height:15px;"></div>
				
				
				<?php
				if( !empty($abonados_array) ) {
					$i=1;
					foreach( $abonados_array as $abonado ) {
					$flag_exists_asociado = 0;
					if( !empty($clientes_asociados_array) ) {
						if( array_key_exists( $abonado['ddn_telefono'], $clientes_asociados_array ) ) {
							$flag_exists_asociado = 1;
						}
					}
					
					?>
					<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
					<tbody>
                                        <tr>
                                            <td class="da_titulo" colspan="6">DATOS DEL EXPEDIENTE</td>
                                        </tr>
                                        <tr style="text-align: left;">
                                            <td class="da_subtitulo">EXPEDIENTE</td>
                                            <td>
                                                    <input type="text" size="4" value="<?php echo $idduna;?>" name="idduna" id="idduna">
                                                    <input type="text" name="nro_expediente" id="nro_expediente" value="<?php echo $expediente?>" <?php if($flag_duna) echo 'readonly="readonly"';?> maxlength="100" class="required" size="20"><em>(*)</em>
                                            </td>
                                            <td class="da_subtitulo">FECHA EXPEDIENTE</td>
                                            <td>
                                                    <input id="fecha_expediente" name="fecha_expediente" type="text" value="<?php echo $fecha_expediente?>" readonly="readonly" size="8"><em>(*)</em>
                                            </td>
                                            <td class="da_subtitulo">FECHA DETECCION</td>
                                            <td>
                                                    <input type="text" value="<?php echo $fecha_deteccion?>" <?php if($flag_duna) echo 'readonly="readonly"';?> maxlength="50" onkeypress="return soloLetras(event)" class="required" size="20" name="fecha_deteccion" id="fecha_deteccion" readonly="readonly"><em>(*)</em>
                                                    <input type="hidden" id="idcomponente" value="<?php echo $pirata[0]['idcomponente']?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="da_subtitulo" style="text-align:left; vertical-align:top">OBSERVACIONES:</td>
                                            <td colspan="5" style="text-align:left;">
                                                    <textarea name="observaciones" id="observaciones" cols="80" rows="2" <?php if($flag_duna) echo 'readonly="readonly"';?>><?php echo $pirata[0]['observaciones']?></textarea>
                                            </td>
                                        </tr>
					<tr>
						<td style="font-weight:bold; text-align:left; height:25px;" colspan="6">DATOS DEL CLIENTE #<?php echo $i?> <?php if($flag_exists_asociado) echo ' - <span style="color:#ff0000;font-size:10px;"> (CLIENTE ASOCIADO)</span>';?></td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">TITULAR</td>
						<td colspan="3">
							<input type="hidden" size="4" class="cli" value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['idduna_asociado']; else echo '';?>" name="cliente[<?php echo $abonado['ddn_telefono']?>][idduna_asociado]" id="cliente[<?php echo $abonado['ddn_telefono']?>][idduna_asociado]">
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['titular']; else echo $abonado['Cliente'];?>" maxlength="100" onkeypress="return soloLetras(event)" class="cli" size="50" name="cliente[<?php echo $abonado['ddn_telefono']?>][titular]" id="cliente[<?php echo $abonado['ddn_telefono']?>][titular]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">DNI</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['dni']; else echo '';?>" maxlength="20" onkeypress="return soloNumeros(event)" class="cli" size="10" name="cliente[<?php echo $abonado['ddn_telefono']?>][dni]" id="cliente[<?php echo $abonado['ddn_telefono']?>][dni]">
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">DEPARTAMENTO</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['departamento']; else echo $abonado['departamento'];?>" maxlength="100" onkeypress="return soloLetras(event)" class="cli" size="30" name="cliente[<?php echo $abonado['ddn_telefono']?>][departamento]" id="cliente[<?php echo $abonado['ddn_telefono']?>][departamento]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">PROVINCIA</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['provincia']; else echo $abonado['provincia'];?>" maxlength="100" onkeypress="return soloLetras(event)" class="cli" size="30" name="cliente[<?php echo $abonado['ddn_telefono']?>][provincia]" id="cliente[<?php echo $abonado['ddn_telefono']?>][provincia]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">DISTRITO</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['distrito']; else echo $abonado['distrito'];?>" maxlength="100" onkeypress="return soloLetras(event)" class="cli" size="30" name="cliente[<?php echo $abonado['ddn_telefono']?>][distrito]" id="cliente[<?php echo $abonado['ddn_telefono']?>][distrito]"><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">DIRECCION ANTENA</td>
						<td>
							<input type="text" style="border: 2px solid #FF0000;" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['direccion_antena']; else echo $pirata[0]['campo1'];?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="30" name="cliente[<?php echo $abonado['ddn_telefono']?>][direccion_antena]" id="cliente[<?php echo $abonado['ddn_telefono']?>][direccion_antena]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">DIRECCION TELEFONO</td>
						<td>
							<input type="text" style="border: 2px solid #FF0000;" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['direccion_telefono']; else echo $abonado['Direccion'];?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="30" name="cliente[<?php echo $abonado['ddn_telefono']?>][direccion_telefono]" id="cliente[<?php echo $abonado['ddn_telefono']?>][direccion_telefono]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">DIRECCIONES IGUALES</td>
						<td>
							<?php
							if( $flag_exists_asociado ) {
								$dir_igual = $clientes_asociados_array[$abonado['ddn_telefono']]['direcciones_iguales'];
							?>
								<select <?php if($flag_exists_asociado) echo 'disabled="disabled"';?> class="cli" name="cliente[<?php echo $abonado['ddn_telefono']?>][direcciones_iguales]" id="cliente[<?php echo $abonado['ddn_telefono']?>][direcciones_iguales]">
									<option value="S" <?php if($dir_igual == 'S') echo 'selected="selected"' ?>>Si</option>
									<option value="N" <?php if($dir_igual == 'N') echo 'selected="selected"' ?>>No</option>
								</select><em>(*)</em>
							<?
							}else {
							?>
								<select class="cli" name="cliente[<?php echo $abonado['ddn_telefono']?>][direcciones_iguales]" id="cliente[<?php echo $abonado['ddn_telefono']?>][direcciones_iguales]">
									<option value="">Seleccione</option>
									<option value="S">Si</option>
									<option value="N">No</option>
								</select><em>(*)</em>
							<?php
							}
							?>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">TELEFONO</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['telefono']; else echo $abonado['ddn_telefono'];?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="20" name="cliente[<?php echo $abonado['ddn_telefono']?>][telefono]" id="cliente[<?php echo $abonado['ddn_telefono']?>][telefono]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">INSCRIPCION</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['inscripcion']; else echo $abonado['Inscripcion'];?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="20" name="cliente[<?php echo $abonado['ddn_telefono']?>][inscripcion]" id="cliente[<?php echo $abonado['ddn_telefono']?>][inscripcion]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">ESTADO LINEA</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['estado_linea']; else echo '';?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="20" name="cliente[<?php echo $abonado['ddn_telefono']?>][estado_linea]" id="cliente[<?php echo $abonado['ddn_telefono']?>][estado_linea]"><em>(*)</em>
						</td>
					</tr>
					<tr style="text-align: left;">
						<td class="da_subtitulo">SPEEDY CONTRATADO</td>
						<td>
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['speedy_contratado']; else echo $abonado['PRODUCTO'];?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="20" name="cliente[<?php echo $abonado['ddn_telefono']?>][speedy_contratado]" id="cliente[<?php echo $abonado['ddn_telefono']?>][speedy_contratado]"><em>(*)</em>
						</td>
						<td class="da_subtitulo">ESTADO SPEEDY</td>
						<td colspan="3">
							<input type="text" <?php if($flag_exists_asociado) echo 'readonly="readonly"';?> value="<?php if($flag_exists_asociado) echo $clientes_asociados_array[$abonado['ddn_telefono']]['estado_linea']; else echo 'ACTIVO';?>" maxlength="50" onkeypress="return soloLetras(event)" class="cli" size="20" name="cliente[<?php echo $abonado['ddn_telefono']?>][estado_speedy]" id="cliente[<?php echo $abonado['ddn_telefono']?>][estado_speedy]"><em>(*)</em>
						</td>
					</tr>
					</table>
					<div style="height:15px;"></div>
					<?php
					$i++;
					}
				}
				?>
				
				
				<table width="100%" cellspacing="1" cellpadding="0" border="0">
                <tbody><tr>
                    <td width="90%" style="text-align: left; vertical-align: top">
                        <div style="padding-top: 5px; font-size: 11px">
                            <input type="button" value="Registrar" name="guardar" title="Registrar" id="btn_guardar" onclick="javascript: registrarPirata();" class="boton">&nbsp;
                            <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_cerrar" onclick="javascript: cerrarVentana();" class="boton">
                        </div>
                    </td>
					<td colspan="6" align="right">
						<a href="javascript:cerrarAsociarForm()" style="text-decoration: none;">
							<img width="100" height="26" title="Regresar para asociar pirata" alt="Regresar para asociar pirata" src="img/btn_return_mov.png">
						</a>
					</td>
                </tr>
                </tbody>
				</table>
				
				<div style="margin: 10px 0 0 0; display: none;" id="msg">
					<img alt="" src="img/loading.gif"> <b>Procesando cambios.</b>
				</div>
				
			</div>
			

		</td>
	</tr>
	</table>
	<br /><br />
<!--</div>-->
