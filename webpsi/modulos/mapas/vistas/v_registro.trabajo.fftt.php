<script type="text/javascript" src="js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="modulos/maps/js/getData.js?time=<?php echo mktime();?>"></script>
<script type="text/javascript" src="modulos/maps/js/registro.trabajo.fftt.js?time=<?php echo mktime();?>"></script>

<script type="text/javascript" src="js/jquery/jquery-ui-timepicker-addon.js"></script>

<link rel="stylesheet" href="modulos/geplex/css/style.general.css?time=<?php echo mktime();?>" type="text/css" />
<link rel="stylesheet" href="modulos/geplex/css/jquery.autocomplete.css" type="text/css" media="screen" />
<div>
    <table border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
        <thead>
            <tr>
                <td width="100%" colspan="2" class="form_titulo">
                    <img src="img/plus_16.png" alt="SubM�dulo" title="SubM�dulo" border="0"/>&nbsp;Marcaci&oacute;n de trabajos en planta y aver&iacute;as masivas<sup>Beta</sup>
                </td>
            </tr>
            <tr>
                <td colspan="2"><div style="height: 5px"></div></td>
            </tr>    
        </thead>
        <tbody>
<?php if($user_permitido=='0'){?>
            <tr>
                <td width="99%" align="center" class="cabecera_datos" style="padding: 10px;">
                    Disculpe, pero usted no pertenece a un area asignada para realizar Marcaci&oacute;n de trabajos en planta y aver&iacute;as masivas.<br/>
                </td>
            </tr>
        </tbody>
    </table>   
<?php exit;}?>
            <tr>
                <td width="99%" align="left" class="cabecera_datosA">
                    <div style="display: block; border: 1px solid #000000; margin:5px 0 2px 0; padding: 3px; background-color:#F2F2F2;">
                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                        <td width="10%" align="left">Buscar por:</td>
                        <td width="90%">
                            <input type="hidden" id="is_bandeja" value="0">
                            Tipo de Registro:
                            <select id="cmb_tipoRegistro" name="cmb_tipoRegistro" style="width: 120px;" >
                                <option value="" selected="selected" >Seleccione</option>
                            <?php 
                            foreach ($arreglo_registro_pex as $row):  
                                echo '<option value="'.$row->__get('codtiporegistropex').'">'.$row->__get('descripcion').'</option>';
                            endforeach; 
                            ?>       
                            </select>
                            Tipo de Negocio:
                            <select id="cmb_tipoNegocio" name="cmb_tipoNegocio" style="width: 120px;" disabled="disabled">
                                <option value="" selected="selected" >Seleccione</option>
                            <?php 
                            foreach ($arreglo_negocio_pex as $row):  
                                echo '<option value="'.$row->__get('negocio').'">'.$row->__get('negocio').'</option>';
                            endforeach; 
                            ?>        
                            </select>
                            <!--<span class="hide nroTelefono filtroIndividual">Nro. Telefonico:
                                <input name="nroTelefono" class="txtCampo" type="text" value="" style="width: 95px;" onkeypress="return validarNumber(event)"/>
                                <input type="image" src="img/btn_buscar.jpg" name="buscarCliente" title="Buscar Cliente" value="" style="cursor: pointer; vertical-align: middle"/>
                            </span>
                            <span class="hide codCliente filtroIndividual">Cod. de Cliente:
                                <input name="codCliente" class="txtCampo" type="text" value="" style="width: 95px;" onkeypress="return validarNumber(event)"/>
                                <input type="image" src="img/btn_buscar.jpg" name="buscarCliente" title="Buscar Cliente" value="" style="cursor: pointer; vertical-align: middle"/>
                            </span>-->
                            <span class="hide tipored filtroElemento">
                                Tipo Red:
                                <select id="cmb_tipo_red" name="cmb_tipo_red">
                                    <option value="" selected="selected">Seleccione</option>
                                    <option value="D">RED DIRECTA</option>
                                    <option value="F">RED FLEXIBLE</option>
                                </select>
                            </span>    
                            <span class="hide elemento filtroElemento">
                                Elemento de Red:
                                <select id="cmb_elemento" name="cmb_elemento" style="width: 20%" disabled="disabled">
                                    <option value="" selected="selected">Seleccione</option>
                                </select>
                            </span>
                        </td>
                    </tr>
                    </table>
                    </div>
                </td>
                <td width="1%"></td>
            </tr>
            <tr>
                <td colspan="2"><div style="height: 10px"></div></td>
            </tr>
            <tr>
                <td colspan="2">
                    <form id="frmRegistroPEX" name="frmRegistroPEX" action="modulos/maps/registro.trabajo.fftt.php?cmd=registroTrabajo" method="post" style="width: 100%; margin-bottom: 10px;">
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="fongoLoader" class="bgLoader">
        <div class="imgLoader">
            <span>Cargando...</span>
        </div>
    </div>
</div>