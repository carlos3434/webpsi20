<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");



$obj_DescargaData = new Data_DescargaData();
$imprimir_list = $obj_DescargaData->__listSelect($conexion);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<!--<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>-->
<script type="text/javascript" src="../../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
<script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>


<script type="text/javascript" src="../../../js/jquery/jquery.PrintArea.js"></script>

<link href="/mapas/css/maps.css" rel="stylesheet" type="text/css" />
<link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<h1>Concentracion de Averias por Armario</h1>

<div id="tb_listado">
<table border="1" rules="all">
<tr>
	<td>EECC</td>
	<td>ZONAL</td>
	<td>MDF</td>
	<td>ARMARIO</td>
	<td># AVERIAS</td>
</tr>
<?php
foreach( $imprimir_list as $imprimir) {
?>
<tr>
	<td><?php echo $imprimir['eecc']?></td>
	<td><?php echo $imprimir['zonal']?></td>
	<td><?php echo $imprimir['mdf']?></td>
	<td><?php echo $imprimir['carmario']?></td>
	<td><?php echo $imprimir['num_averias']?></td>
</tr>
<?php
}
?>
</table>
</div>

<script>
$(document).ready(function () {
	$("#tb_listado").printArea();
});

</script>

<body>
</html>