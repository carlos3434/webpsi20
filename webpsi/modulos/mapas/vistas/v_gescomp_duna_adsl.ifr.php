<?php
require_once "../../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

        <script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
        <script type="text/javascript" src="../../../js/administracion/jquery.layout.js"></script>
        <script type="text/javascript" src="../../../js/jquery/jquery-ui-1.8.1.custom.min.js"></script>

        <script type="text/javascript" src="../../../js/jquery/prettify.js"></script>

        <script type="text/javascript" src="../../../js/cv.js"></script>

        <link href="../../../css/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../../../css/jquery.multiselect.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../css/jquery.multiselect.filter.css" rel="stylesheet" type="text/css" media="all"/>

        <script type="text/javascript">

            var infoWindow = null;

            var myLayout;
            var map = null;
            var geocoder = null;
            var poligonosDibujarMdf_array = [];

            var ptos_arr = [];
            var markers_arr = [];
            var makeMarkers_arr = [];

            var infoWindow_arr = [];

            var capaMdf_exists = 0;
            var capaTroba_exists = 0;

            var ptos_xy_trm = [];
            var ptos_xy_arm = [];

            $(document).ready(function () {

                myLayout = $('body').layout({
                    // enable showOverflow on west-pane so popups will overlap north pane
                    west__showOverflowOnHover: true,
                    west: {size:420}
                });

                load_array=function(element){
                    var grupo=new Array();
                    $("input[name='"+element+"']:checked").each(function(){grupo.push($(this).val());});
                    return grupo;
                }

                cargaInicial = function() {
                    data_content = "action=cargaInicial";
                    $.ajax({
                        type:   "POST",
                        url:    "../gescomp.duna.adsl.php",
                        data:   data_content,
                        dataType: "json",
                        success: function(datos) {

                        	var zonales = datos['arrZonales'];
                            var dptos = datos['arrDptos'];
                            var provs = datos['arrProvs'];
                            var dists = datos['arrDists'];

                            //carga combo de zonales
                            var selectZonal = '';
                            for(var i in zonales) {
                                selectZonal += '<option ' + sel + ' value="' + zonales[i]['abvZonal'] + '">' + zonales[i]['descZonal'] + '</option>'
                            }
                            $("#selZonal").append(selectZonal);
                            
                            //carga combo de dptos
                            var selectDptos = '';
                            for(var i in dptos) {
                                var sel = '';
                                if ( dptos[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectDptos += '<option ' + sel + ' value="' + dptos[i]['coddpto'] + '">' + dptos[i]['nombre'] + '</option>'
                            }
                            $("#departamento").append(selectDptos);

                            //carga combo de provs
                            var selectProvs = '';
                            for(var i in provs) {
                                var sel = '';
                                if ( provs[i]['nombre'] == 'LIMA' ) {
                                    sel = 'selected';
                                }
                                selectProvs += '<option ' + sel + ' value="' + provs[i]['codprov'] + '">' + provs[i]['nombre'] + '</option>'
                            }
                            $("#provincia").append(selectProvs);

                            //carga combo de provs
                            var selectDists = '';
                            for(var i in dists) {
                                selectDists += '<option value="' + dists[i]['coddist'] + '">' + dists[i]['nombre'] + '</option>'
                            }
                            $("#distrito").append(selectDists);


                        }
                    });
                }
    
                $('#selTodos')
                .filter(':has(:checkbox:checked)')
                .end()
                .click(function(event) {
                    if($("#selTodos").is(':checked')) { 
                        $(".lista_chk").attr('checked', 'checked');
                    }else {  
                        $(".lista_chk").removeAttr('checked');
                    }		
                });

                $("#divDireccion, #divTerminales, #divAsignarXY").toggle(function() {
                    $(this).addClass("opened"); 
                }, function () {
                    $(this).removeClass("opened");
                });
                $("#divDireccion").click(function() {
                    $("#divSearchAddress span").slideToggle("slow,");
                    $("#divSearchTerminales span").hide();
                });
                $("#divTerminales").click(function() {
                    $("#divSearchAddress span").hide();
                    $("#divSearchTerminales span").slideToggle("slow,");
                });
                $("#divAsignarXY").click(function() {
                    $("#divSearchAddress span").hide();
                    $("#divSearchTerminales span").slideToggle("slow,");
                });                
    
    
                $("#direccion").autocomplete({
                    //This bit uses the geocoder to fetch address values
                    source: function(request, response) {
                        geocoder.geocode( {'address': request.term + ', peru' }, function(results, status) {
                            response($.map(results, function(item) {
                                return {
                                    label:  '<div style="font-size:11px;padding:3px 0;background:none;">' + item.formatted_address + '</div>',
                                    value: item.formatted_address,
                                    latitude: item.geometry.location.lat(),
                                    longitude: item.geometry.location.lng(),
                                    title: item.formatted_address
                                }
                            }));
                        })
                    },
                    //This bit is executed upon selection of an address
                    select: function(event, ui) {
                        //$("#latitude").val(ui.item.latitude);
                        //$("#longitude").val(ui.item.longitude);
                        var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            
                        M.clearMarkers();
            
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: ui.item.title,
                            html: 'Punto Seleccionado'//,
                            //icon: "../../../modulos/maps/vistas/icons/markergreen.png"
                        });
                        markers_arr.push(marker);
            
                        map.setCenter(location);
                        map.setZoom(16);
                    }
                });
    
    
                $('#btnFiltrar').click(function(){
                    var departamento    = $("#departamento").val();
                    var provincia       = $("#provincia").val();
                    var distrito       = $("#distrito").val();
                    if(departamento == "" || provincia == "" || distrito == "") {
                        alert("Ingrese el distrito para acortar la busqueda.");
                        return false;
                    }
        
                    listar_resultados();
                });
    
                GrabarDatos = function() {
                    M.GrabarDatos();
                }
    
                map_pointer = function(terminal) {
                    M.GetXYMapa(terminal);
                }
    

                /*Busqueda de FFTT*/
                $("#departamento").change(function() {
                    var departamento = this.value;
                    if(departamento == "") {
                        return false;
                    }
        
                    M.listarProvincias(departamento);
        
                });
    
                $("#provincia").change(function() {
                    var departamento    = $("#departamento").val();
                    var provincia       = $("#provincia").val();
                    if(departamento == "" || provincia == "") {
                        return false;
                    }
        
                    M.listarDistritos(departamento, provincia);

                });
    
                clientesTerminal = function(zonal, mdf, cable, armario, caja, tipo_red) {
                    parent.$("#childModal").html("Cargando...");
                    parent.$("#childModal").css("background-color","#FFFFFF");
                    $.post("principal/clientes_terminal.popup.php", {
                        zonal: zonal,
                        mdf: mdf,
                        cable: cable,
                        armario: armario,
                        caja: caja,
                        tipo_red: tipo_red
                    },
                    function(data){
                        parent.$("#childModal").html(data);
                    }
                );

                    parent.$("#childModal").dialog({modal:true, width:"800px", hide: "slide", title: "Clientes asignados para este terminal: " + caja, position:"top"});

                }


                /*Busqueda de FFTT*/
                $("#selZonal").change(function() {
                    IDzon = this.value;
                    if(IDzon == "") {
                        return false;
                    }
        
                    M.ffttMdfporZonal(IDzon);
        
                });
    
                $("#selMdf").change(function() {
                    IDzonal	  = $("#selZonal").val();
                    IDmdf  	  = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    if(IDzonal == "" || IDzon == "" || IDtipred == "") {
                        return false;
                    }
                    $("select[id=selTipRed]").html("<option value=''>Seleccione</option><option value='D'>Directa</option><option value='F'>Flexible</option>");
                    $("select[id=selCableArmario]").html("<option value=''>Seleccione</option>");
                    $("select[id=selCaja]").html("<option value=''>Seleccione</option>");
                });
    
                $("#selTipRed").change(function() {
                    IDzonal	  = $("#selZonal").val();
                    IDmdf  	  = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    if(IDzonal == "" || IDzon == "" || IDtipred == "") {
                        return false;
                    }
        
                    M.ffttCableArmario(IDzonal, IDmdf, IDtipred);

                });
    
                $("#selCableArmario").change(function() {
                    IDzonal	  = $("#selZonal").val();
                    IDmdf  	  = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    IDcableArmario  = $("#selCableArmario").val();
                    if(IDzonal == "" || IDzon == "" || IDtipred == "" || IDcableArmario == "") {
                        return false;
                    }
        
                    M.ffttCaja(IDzonal, IDmdf, IDtipred, IDcableArmario);
        
                });
    
    
                $("#btnBuscarCaja").click(function() {
                    IDzonal	  = $("#selZonal").val();
                    IDmdf  	  = $("#selMdf").val();
                    IDtipred  = $("#selTipRed").val();
                    IDcableArmario  = $("#selCableArmario").val();
                    mtgespktrm	  = $("#selCaja").val();
        
                    if(IDzonal == "" || IDzon == "" || IDtipred == "" || IDcableArmario == "" || mtgespktrm == "") {
                        return false;
                    }
        
                    M.ffttCajaDetalle(mtgespktrm);

                });
                /*FIN Busqueda de FFTT*/
   
    
                var M = {
                    initialize: function() {

                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com');
                            return false;
                        }
    	
                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var myOptions = {
                            zoom: 11,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            
                        geocoder = new google.maps.Geocoder();

                        //carga inicial de combos
                        cargaInicial();
                    },
        
        
                    /*select anidados para FFTT*/
                    ffttMdfporZonal: function(IDzon) {
                        $("select[id=selMdf]").html("<option value=''>Seleccione</option>");
                        $("select[id=selTipRed]").html("<option value=''>Seleccione</option><option value='D'>Directa</option><option value='F'>Flexible</option>");
                        $("select[id=selCableArmario]").html("<option value=''>Seleccione</option>");
                        $("select[id=selCaja]").html("<option value=''>Seleccione</option>");

                        data_content = "action=buscarMdf&IDzon=" + IDzon;
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.terminal.main.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(datos) {
                                var selectMdf = '';
                                for(var i in datos) {
                                    selectMdf += '<option value="' + datos[i]['IDmdf'] + '">' + datos[i]['IDmdf'] + ' - ' + datos[i]['Descmdf'] + '</option>'
                                }
                                $("#selMdf").html(selectMdf);
                            }
                        });

                    },
        
                    ffttCableArmario: function(IDzonal, IDmdf, IDtipred) {
                        $("select[id=selCableArmario]").html("<option value=''>Seleccione</option>");
                        $("select[id=selCaja]").html("<option value=''>Seleccione</option>");

                        data_content = "action=ffttCableArmario&idZonal=" + IDzonal + "&idMdf=" + IDmdf + "&idTipRed=" + IDtipred;
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.duna.adsl.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                            	$.each(data, function(i, item) {
                                    $("select[id=selCableArmario]").append("<option value='"+data[i]['idCableArmario']+"'>"+data[i]['desCableArmario']+"</option>")
                                });
                            }
                        });

                    },
        
                    ffttCaja: function(IDzonal, IDmdf, IDtipred, IDcableArmario) {
                        $("select[id=selCaja]").html("<option value=''>Seleccione</option>");

                        data_content = {
                            'action'    : 'ffttCaja',
                            'idZonal'   : IDzonal,
                            'idMdf'     : IDmdf,
                            'idTipRed'  : IDtipred,
                            'idCableArmario' : IDcableArmario
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.duna.adsl.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                                $.each(data, function(i, item) {
                                    $("select[id=selCaja]").append("<option value='"+data[i]['mtgespktrm']+"'>"+data[i]['Caja']+"</option>")
                                });
                            }
                        });
                    },
        
                    ffttCajaDetalle: function(mtgespktrm) {
                        $('#box_loading2').show();
                        data_content = {
                            'action'        : 'ffttCajaDetalle',
                            'mtgespktrm'    : mtgespktrm
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.duna.adsl.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(datos) {

                                M.clearMarkers();
                                M.clearInfoWindow();

                                //setea el zoom y el centro
                                map.setCenter(new google.maps.LatLng(datos[0]['y'], datos[0]['x']));
                                map.setZoom(16);

                                var pto_xy = new google.maps.LatLng(datos[0]['y'], datos[0]['x']);

                                var est_caja = 1;
                                if( datos[0]['qparlib'] == 0 ) {
                                    est_caja = 3;
                                } else if( datos[0]['qparlib'] > 0 && datos[0]['qparlib'] < 3 ) {
                                    est_caja = 2;
                                } else if( datos[0]['qparlib'] > 2 ) {
                                    est_caja = 1;
                                }

                                var detalle = '';
                                detalle += '<b>Zonal:</b> ' + datos[0]['zonal'] + '<br />';
                                detalle += '<b>Mdf:</b> ' + datos[0]['mdf'] + '<br />';
                                if( datos[0]['tipo_red'] == 'D' ) {
                                    detalle += '<b>Cable:</b> ' + datos[0]['cable'] + '<br />';
                                }else if( datos[0]['tipo_red'] == 'F' ) {
                                    detalle += '<b>Armario:</b> ' + datos[0]['armario'] + '<br />';
                                }
                                detalle += '<b>Caja:</b> ' + datos[0]['caja'] + '<br />';
                                detalle += '<b>Capacidad:</b> ' + datos[0]['qcapcaja'] + '<br />';
                                detalle += '<b>Pares libres:</b> ' + datos[0]['qparlib'] + '<br />';
                                detalle += '<b>Pares reserv.:</b> ' + datos[0]['qparres'] + '<br />';
                                detalle += '<b>Pares distrib.:</b> ' + datos[0]['qdistrib'] + '<br /><br />';
                                if ( datos[0]['atenuacion'] == null ) {
                                    detalle += '<b>Atenuaci&oacute;n:</b><br /><br />';
                                } else {
                                    detalle += '<b>Atenuaci&oacute;n:</b> ' + datos[0]['atenuacion'] + '<br /><br />';
                                }
                                detalle += '<b>X,Y:</b> ' + datos[0]['x'] + ',' + datos[0]['y'] + '<br />';
                                detalle += '<a title="Ver clientes asignados a este terminal" href="javascript:clientesTerminal(\''+ datos[0]['zonal1'] +'\',\''+ datos[0]['mdf'] +'\',\''+ datos[0]['cable'] +'\',\''+ datos[0]['armario'] +'\',\''+ datos[0]['caja'] +'\',\''+ datos[0]['tipo_red'] +'\');">Ver clientes</a>';		

                                marker = new google.maps.Marker({
                                    position: pto_xy,
                                    map: map,
                                    title: 'Terminal: ' + datos[0]['caja'],
                                    html: detalle,
                                    icon: "../../../img/map/terminal/trm_" + datos[0]['caja'] + "--" + est_caja + ".gif"
                                });
                                //ptos_arr.push(marker);
                                markers_arr.push(marker);


                                var infoWindow = new google.maps.InfoWindow({ 
                                    content: detalle
                                });
                                google.maps.event.addListener(marker, 'click', function() {
                                    infoWindow.open(map,marker);
                                });

                                //almaceno los infowindows
                                infoWindow_arr.push(infoWindow);

                                $('#box_loading2').hide();
                            }
                        });
                    },
        
        
                    /*select anidados para ubigeo*/
                    listarProvincias: function(departamento) {
                        $("select[id=provincia]").html("<option value=''>Seleccione</option>");
                        $("select[id=distrito]").html("<option value=''>Seleccione</option>");

                        data_content = {
                            'action'        : 'listarProvincias',
                            'departamento'  : departamento
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.duna.adsl.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                                $.each(data, function(i, item) {
                                    $("select[id=provincia]").append("<option value='"+data[i]['IDprov']+"'>"+data[i]['Desprov']+"</option>")
                                });
                            }
                        });
                    },
        
                    listarDistritos: function(departamento, provincia) {
                        $("select[id=distrito]").html("<option value=''>Seleccione</option>");

                        data_content = {
                            'action'        : 'listarDistritos',
                            'departamento'  : departamento,
                            'provincia'     : provincia
                        };
                        $.ajax({
                            type:   "POST",
                            url:    "../gescomp.duna.adsl.php",
                            data:   data_content,
                            dataType: "json",
                            success: function(data) {
                                $.each(data, function(i, item) {
                                    $("select[id=distrito]").append("<option value='"+data[i]['IDdist']+"'>"+data[i]['Desdist']+"</option>")
                                });
                            }
                        });
                    },

                    clearMarkers: function() {
                        for (var n = 0, marker; marker = markers_arr[n]; n++) {
                            marker.setVisible(false);
                        }
                    },
		
                    clearMakeMarkers: function() {
                        for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                            makeMarker.setVisible(false);
                        }
                    },

                    clearPoligonosMdf: function() {
                        for (var n = 0, poligonoDibujar; poligonoDibujar = poligonosDibujarMdf_array[n]; n++) {
                            poligonoDibujar.setMap(null);
                        }
                    },
        
                    clearInfoWindow :function () {
                        for(var i=0; i<infoWindow_arr.length; i++){
                            infoWindow_arr[i].close();
                        }
                    },

                    loadPoligono: function(xy_array, tipo) {

                        var datos 		= xy_array['coords'];
                        var color_poligono 	= xy_array['color'];
                        var poligonoCoords 	= [];

                        if( datos.length > 0) {
                            for (var i in datos) {
                                var pto_poligono = new google.maps.LatLng(datos[i]['y'], datos[i]['x']);
                                poligonoCoords.push(pto_poligono);
                            }

                            // Construct the polygon
                            // Note that we don't specify an array or arrays, but instead just
                            // a simple array of LatLngs in the paths property
                            poligonoDibujar = new google.maps.Polygon({
                                paths: poligonoCoords,
                                strokeColor: color_poligono,
                                strokeOpacity: 0.6,
                                strokeWeight: 3,
                                fillColor: color_poligono,
                                fillOpacity: 0.5
                            });

                            //agrego al array las capas seleccionadas
                            if( tipo == 'mdf') {
                                poligonosDibujarMdf_array.push(poligonoDibujar);
                                //seteamos que la capa existe
                                capaMdf_exists = 1;
                            }

                            //seteo la capa en el mapa
                            poligonoDibujar.setMap(map);

                        }
                        else {
                            if( tipo == 'mdf') {
                                capaMdf_exists = 0;
                            }
                        }		
                    },
        
                    GetXYMapa: function(terminal_sel) {
                        $("#trm_selected").val(terminal_sel);
	
                        $(".unselected").attr("class", "unselected");
                        $(".selected").attr("class", "unselected");
                        $("#row_" + terminal_sel).attr("class", "selected");

                        google.maps.event.addListener(map, "click", function(event) {
                            M.clearMarkers();

                            marker = new google.maps.Marker({
                                position: event.latLng,
                                map: map, 
                                title: terminal_sel
                            });
                            markers_arr.push(marker);

                            if( $("#trm_selected").val() == terminal_sel ) {
                                var Coords = event.latLng;

                                $("#x" + terminal_sel).val(Coords.lng());
                                $("#y" + terminal_sel).val(Coords.lat());
                            }
                        });

                    },
        
                    GrabarDatos: function() {
                        var query = $("input.latlon").serializeArray();
                        json = {};

                        for (i in query) {
                            json[query[i].name] = query[i].value;
                        } 
            
                        if( query.length == 0 ) {
                            alert("Ningun pirata listado para ser actualizado");
                            return false;
                        }
            
                        if( confirm("Esta seguro de guardar los cambios?") ) {
	
                            var departamento    = $("#departamento").val();
                            var provincia       = $("#provincia").val();
                            var distrito        = $("#distrito").val();
                
                            $("#box_loading").show();
                            $.ajax({
                                type:     "POST",
                                dataType: "json",
                                url:      "../gescomp.duna.adsl.php?action=grabarDatos&departamento=" + departamento + "&provincia=" + provincia + "&distrito=" + distrito,
                                data:     query,
                                success:  function(datos) {
                                    if( !datos['success'] ) {
                                        alert(datos['msg']);
                                    }else {
                                        alert(datos['msg']);
                                        listar_resultados();
                                    }
                                }
                            });
                            $("#box_loading").hide();
                        }
                    }

                };


                M.initialize();
               
            });




            function listar_resultados() {

                /**
                 * makeMarker() ver 0.2
                 * creates Marker and InfoWindow on a Map() named 'map'
                 * creates sidebar row in a DIV 'sidebar'
                 * saves marker to markerArray and markerBounds
                 * @param options object for Marker, InfoWindow and SidebarItem
                 */
                infoWindow = new google.maps.InfoWindow();
                var markerBounds = new google.maps.LatLngBounds();
                var markerArray = [];


                function makeMarker(options){
                    var pushPin = null;
                    /*var pushPin = new google.maps.Marker({
                    map: map
                });
                pushPin.setOptions(options);*/
			
                    if( options.position != '(0, 0)' ) {
                        pushPin = new google.maps.Marker({
                            map: map
                        });

                        makeMarkers_arr.push(pushPin);

                        pushPin.setOptions(options);
                        google.maps.event.addListener(pushPin, "click", function(){
                            infoWindow.setOptions(options);
                            infoWindow.open(map, pushPin);
                            if(this.sidebarButton)this.sidebarButton.button.focus();
                        });
                        var idleIcon = pushPin.getIcon();

                        markerBounds.extend(options.position);
                        markerArray.push(pushPin);
                    }else {
         	
                        pushPin = new google.maps.Marker({
                            map: map
                        });
			
                        makeMarkers_arr.push(pushPin);
			
                        pushPin.setOptions(options);
                    }
        
                    //almaceno los infowindows
                    infoWindow_arr.push(infoWindow);
         
                    if(options.sidebarItem){
                        pushPin.sidebarButton = new SidebarItem(pushPin, options);
                        pushPin.sidebarButton.addIn("sidebar");
                    }

                    return pushPin;       	
                }

                google.maps.event.addListener(map, "click", function(){
                    infoWindow.close();
                });
     

     

                /*
                 * Creates an sidebar item 
                 * @param marker
                 * @param options object Supported properties: sidebarItem, sidebarItemClassName, sidebarItemWidth,
                 */
                function SidebarItem(marker, opts) {
                    //var tag = opts.sidebarItemType || "button";
                    //var tag = opts.sidebarItemType || "span";
                    var tag = opts.sidebarItemType || "div";
                    var row = document.createElement(tag);
                    row.innerHTML = opts.sidebarItem;
                    row.className = opts.sidebarItemClassName || "sidebar_item";  
                    row.style.display = "block";
                    row.style.width = opts.sidebarItemWidth || "390px";
                    row.onclick = function(){
                        google.maps.event.trigger(marker, 'click');
                    }
                    row.onmouseover = function(){
                        google.maps.event.trigger(marker, 'mouseover');
                    }
                    row.onmouseout = function(){
                        google.maps.event.trigger(marker, 'mouseout');
                    }
                    this.button = row;
                }
                // adds a sidebar item to a <div>
                SidebarItem.prototype.addIn = function(block){
                    if(block && block.nodeType == 1)this.div = block;
                    else
                        this.div = document.getElementById(block)
                        || document.getElementById("sidebar")
                        || document.getElementsByTagName("body")[0];
                    this.div.appendChild(this.button);
                }
	 

                $("#selTodos").removeAttr('checked');
                $("#divSearchAddress span").hide();
                $("#direccion").attr('value', '');

                var datosXY     = $("#selDatosXY").val();
                data_content = {
                    'action'        : 'listarDunaAdsl',
                    'departamento'  : $('#departamento').val(),
                    'provincia'     : $('#provincia').val(),
                    'distrito'      : $('#distrito').val(),
                    'datosXY'       : datosXY
                };

                $('#box_loading').show();
                $("#sidebar").html("");
                var tabla = '';
                var detalle = '';
                $.ajax({
                    type:   "POST",
                    url:    "../gescomp.duna.adsl.php",
                    data:   data_content,
                    dataType: "json",
                    success: function(data) {
		 
                        clearMarkers();
                        clearMakeMarkers();
            
                        clearInfoWindow();

                        ptos_xy_trm = [];
                        ptos_xy_arm = [];


                        if( data['edificios'].length > 0 ) {
			
                            if( data['edificios_con_xy'].length == 0 ) {

                                var datos = data['edificios']; 
                    
                                var x1 = y1 = null;
                                if( data['zonal']['x'] != null && data['zonal']['y'] != null ) {
                                    x1 = data['zonal']['x'];
                                    y1 = data['zonal']['y'];
                                }

                                var num = 1;
                                var x_ini = y_ini = '';
                                var flag_captura_xy = 1;
                                for (var i in datos) {
                        
                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                    if( flag_captura_xy && x != '' && y != '' ) {
                                        x_ini = datos[i]['x'];
                                        y_ini = datos[i]['y'];
                                        flag_captura_xy = 0;
                                    }

                                    var color = "color: #FF0000;";
                                    if( x != '' && y != '' ) {
                                        color = "color: #000000;";
                                    }
                        
                                    var estado = "1";
                                    if( datos[i]['ind_duna'] == 'N' ) {
                                        estado = "3";
                                    }

                                    tabla = '<div id="row_' + datos[i]['idduna'] + '" class="unselected">';
                                    tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['idduna'] + '" class="lista_chk" type="checkbox" value="' + datos[i]['idduna'] + '" name="' + datos[i]['idduna'] + '"><br /><img style="width:14px;padding:0 0 0 4px;" src="../../../img/map/dunaadsl/pirata-' + estado + '.gif"></div>';
                                    tabla += '<div style="width:70px; float:left; font-weight:bold;' + color + '">' + datos[i]['codigo'] + '<br /><span style="font-weight:normal;">Status: ' + datos[i]['estatus'] + '<br />Filtro linea: ' + datos[i]['filtro_linea'] + '<br /><img onclick="map_pointer(\'' + datos[i]['idduna'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></span></div>';
                                    tabla += '<div style="width:240px; float:left;">';
                                    tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['direccion'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion_referencial'] + '<span></div>';
                                    tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['departamento'] + ' - ' + datos[i]['provincia'] + ' - ' + datos[i]['distrito'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">TELF. REFERENCIAL: ' + datos[i]['telefono_referencial'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">TELF. DETECTADO: ' + datos[i]['telefono_detectado'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;font-weight:bold;">FFTT: ' + datos[i]['fftt'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">';
                                    tabla += '<input id="x' + datos[i]['idduna'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idduna'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                                    tabla += '<input id="y' + datos[i]['idduna'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idduna'] + '][y]" size="12" readonly="readonly">';
                                    tabla += '</div>';
                                    tabla += '</div>';
                                    tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="'+ datos[i]['tipo'] +'" >'+ datos[i]['tipo'] +'</span><br /><span style="color:#ff0000;font-size:8px;">' + datos[i]['estado_pirata'] + '</span></div>';
                                    tabla += '</div>';

                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(y1, x1),
                                        title: '',
                                        sidebarItem: tabla,
                                        //content: detalle,
                                        icon: "../../../img/markergreen.png"
                                    });

                		
                                }
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);
                                map.setZoom(14);
                    

    	 			
                            }else {
			

                                var datos = data['edificios']; 

                                var num = 1;
                                var x_ini = y_ini = '';
                                var flag_captura_xy = 1;
                                for (var i in datos) {

                                    var x = ( datos[i]['x'] == null ) ? '' : datos[i]['x'];
                                    var y = ( datos[i]['y'] == null ) ? '' : datos[i]['y'];

                                    if( flag_captura_xy && x != '' && y != '' ) {
                                        x_ini = datos[i]['x'];
                                        y_ini = datos[i]['y'];
                                        flag_captura_xy = 0;
                                    }

                                    var color = "color: #FF0000;";
                                    if( x != '' && y != '' ) {
                                        color = "color: #000000;";
                                    }
                        
                                    var estado = "1";
                                    if( datos[i]['ind_duna'] == 'N' ) {
                                        estado = "3";
                                    }

                                    //llenando array para Imprimir en Mapa
                                    //ptos_xy_trm.push(datos[i]['terminal'] + '|' + datos[i]['averia'] + '|' + datos[i]['telefono'] + '|' + datos[i]['cod_ave'] + '|' + datos[i]['x'] + '|' + datos[i]['y'] + '|' + datos[i]['direccion'] + '|' + datos[i]['zonal'] + '|' + datos[i]['mdf'] + '||' + datos[i]['carmario'] + '|' + datos[i]['cbloque'] + '|' + datos[i]['des_ave']);

                                    tabla = '<div id="row_' + datos[i]['idduna'] + '" class="unselected">';
                                    tabla += '<div style="width:30px; float:left;"><input id="' + datos[i]['idduna'] + '" class="lista_chk" type="checkbox" value="' + datos[i]['idduna'] + '" name="' + datos[i]['idduna'] + '"><br /><img style="width:14px;padding:0 0 0 4px;" src="../../../img/map/dunaadsl/pirata-' + estado + '.gif"></div>';
                                    tabla += '<div style="width:70px; float:left; font-weight:bold;' + color + '">' + datos[i]['codigo'] + '<br /><span style="font-weight:normal;">Status: ' + datos[i]['estatus'] + '<br />Filtro linea: ' + datos[i]['filtro_linea'] + '<br /><img onclick="map_pointer(\'' + datos[i]['idduna'] + '\')" src="../../../img/map_pointer.png" width="16" height="16" title="Agregar o Editar x,y"></span></div>';
                                    tabla += '<div style="width:240px; float:left;">';
                                    tabla += '<div style="font-size:14px;color:#1155CC;">' + datos[i]['direccion'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['direccion_referencial'] + '<span></div>';
                                    tabla += '<div style="font-size:10px;color:#666666;">' + datos[i]['departamento'] + ' - ' + datos[i]['provincia'] + ' - ' + datos[i]['distrito'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">TELF. REFERENCIAL: ' + datos[i]['telefono_referencial'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">TELF. DETECTADO: ' + datos[i]['telefono_detectado'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;font-weight:bold;">FFTT: ' + datos[i]['fftt'] + '</div>';
                                    tabla += '<div style="font-size:10px;color:#555555;">';
                                    tabla += '<input id="x' + datos[i]['idduna'] + '" class="latlon" type="text" value="' + x + '" name="xy[' + datos[i]['idduna'] + '][x]" size="12" readonly="readonly"> &nbsp; ';
                                    tabla += '<input id="y' + datos[i]['idduna'] + '" class="latlon" type="text" value="' + y + '" name="xy[' + datos[i]['idduna'] + '][y]" size="12" readonly="readonly">';
                                    tabla += '</div>';
                                    tabla += '</div>';
                                    tabla += '<div style="width:40px; float:left; font-weight:bold;padding-left:5px;"><span title="'+ datos[i]['tipo'] +'" >'+ datos[i]['tipo'] +'</span><br /><span style="color:#ff0000;font-size:8px;">' + datos[i]['estado_pirata'] + '</span></div>';
                                    tabla += '</div>';


                                    detalle = '<div style="width:400px;">';
                                    detalle += '<div>';
                                    detalle += '<table width="100%">';
                                    detalle += '<tr><td class="subtit_info" style="font-size:14px; color: #1155CC;" colspan="4">DETALLE DEL PIRATA:</td></tr>';
                                    detalle += '<tr><td class="subtit_info">CODIGO:</td><td class="det_info">' + datos[i]['codigo'] + '</td><td class="subtit_info">ESTADO:</td><td class="det_info" style="color:#ff0000;font-weight:bold;">' + datos[i]['estado_pirata'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">DEPARTAMENTO:</td><td class="det_info">' + datos[i]['departamento'] + '</td><td class="subtit_info">PROVINCIA:</td><td class="det_info">' + datos[i]['provincia'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">CIUDAD:</td><td class="det_info">' + datos[i]['ciudad'] + '</td><td class="subtit_info">DISTRITO:</td><td class="det_info">' + datos[i]['distrito'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">TEL.REF:</td><td class="det_info">' + datos[i]['telefono_referencial'] + '</td><td class="subtit_info">TEL.DETECTADO:</td><td class="det_info">' + datos[i]['telefono_detectado'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">DIRECCION:</td><td class="det_info" colspan="3">' + datos[i]['direccion'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">DIRECCION REF.:</td><td class="det_info" colspan="3">' + datos[i]['direccion_referencial'] + '</td></tr>';
                                    detalle += '<tr><td class="subtit_info">FFTT:</td><td class="det_info" colspan="3">' + datos[i]['fftt'] + '</td></tr>';
                                    detalle += '</table>';
                                    detalle += '</div>';
                                    detalle += '<div style="font-size: 12px;margin: 5px 0 0 0;"><span style="color:#777777;">X,Y:</span> <span style="color:#1155CC;">' + datos[i]['x'] + ',' + datos[i]['y'] + '</span></div>';
                                    detalle += '</div>';


                                    /* markers and info window contents */
                                    makeMarker({
                                        draggable: false,
                                        position: new google.maps.LatLng(datos[i]['y'], datos[i]['x']),
                                        title: "Pedido: " + datos[i]['cod_pedido'],
                                        sidebarItem: tabla,
                                        content: detalle,
                                        icon: "../../../img/map/dunaadsl/pirata-" + estado + ".gif"
                                    });
						
                                    num++;
                                }
					
                                /* fit viewport to markers */
                                map.fitBounds(markerBounds);

                                //map.setZoom(16);
                                map.setCenter(new google.maps.LatLng(y_ini, x_ini));
				
                            }


                        }

                        //setTimeout("hideLoader()", 1000);
                        $('#box_loading').hide();
                        $('#spanNum').html(data['edificios'].length);
                    }
                });
            }

            function clearMarkers() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }
    
            function clearInfoWindow() {    
                for(var i=0; i<infoWindow_arr.length; i++){
                    infoWindow_arr[i].close();
                }
            }

            function clearMakeMarkers() {
                for (var n = 0, makeMarker; makeMarker = makeMarkers_arr[n]; n++) {
                    makeMarker.setVisible(false);
                }
            }

        </script>
        <style type="text/css">
            .unselected {
                background: none repeat scroll 0 0 #FFFFFF;
                padding: 4px 0;
                float: left;
            }
            .selected {
                background: none repeat scroll 0 0 #DBEDFE;   
                padding: 4px 0;
                float: left;
            }
            .latlon {
                font-size: 11px;
            }
            .subtit_info {
                font-weight: bold;
                width: 80px;
            }
            .det_info {
                color: #666666;
            }
            .sidebar_item {
                float: left;
                padding: 5px 0;
                border-bottom: 1px solid #DDDDDD;
            }
            .ipt{
                border:none;
                color:#0086C3;
            }
            .ipt:focus{
                border:1px solid #0086C3;
            }
            body{
                font-family: Arial;
                font-size: 11px;
            }
            .ui-layout-pane {
                background: #FFF; 
                border: 1px solid #BBB; 
                padding: 7px; 
                overflow: auto;
            } 

            .ui-layout-resizer {
                background: #DDD; 
            } 

            .ui-layout-toggler {
                background: #AAA; 
            }
            .label{ color:#003366; text-align: left; font-weight: bold; font-size: 11px;}
            .l_tit{ color:#CC0800; font-weight: 800; text-decoration: underline; font-size: 12px;}
            .l_titulo_report{ color:#CC0800; font-weight: 800; font-size: 14px;}
            .slt{
                border:1px solid #1E9EF9;
                background-color: #FAFAFA;
                height: 18px;
                font-size: 11px;
                color:#0B0E9D;
            }
            .box_checkboxes{
                border:1px solid #4C5E60;
                background: #E7F7F7;
                width:170px;
                height: 240px;
                overflow: auto;
                display:none;
                position: absolute;
                top:0px;
                left:0px;
                opacity:0.9;
            }
            .tb_data_box{
                width: 100%;
                border-collapse:collapse;
                font-family: Arial;
                font-size: 9px;
            }
            .showResult {
                position: fixed; 
                bottom: 0; 
                right: 0; 
                padding: 8px; 
                border: 1px solid #000000; 
                margin: 2px; 
                color: #FFFFFF;
                font-size: 12px;
            }
            .divOpacity {
                background-color: #000000;
                -moz-opacity: 0.5;
                opacity:.50;
                filter: alpha(opacity=50);
            }

            #divAverias, #clear {
                cursor: pointer;
            }



            table.tablesorter {
                background-color: #CDCDCD;
                font-family: Arial,Helvetica,sans-serif;
                font-size: 8pt;
            }
            table.tablesorter thead tr th, table.tablesorter tfoot tr th {
                background-color: #E6EEEE;
                border: 1px solid #FFFFFF;
                font-size: 8pt;
            }
            .headTbl {
                background-image: url("../../../img/bg_head_js.gif");
                /*background-position: center top;*/
                background-repeat: repeat-x;
                color: #000000;
                height: 20px;
                font-weight: normal;
            }
            table.tablesorter tbody td {
                background-color: #FFFFFF;
                color: #3D3D3D;
                vertical-align: middle;
                font-size: 10px;
            }





            #divSearchTerminales span {
                background: none repeat scroll 0 0 #FFFFFF;
                border: 1px solid #004350;
                color: #000000;
                display: inline;
                padding: 10px 5px 20px 5px;
                position: absolute;
                width: 250px;
                display: none;
                margin: -1px 0 0 -120px;
            } 
            #divSearchAddress span {
                background: none repeat scroll 0 0 #FFFFFF;
                border: 1px solid #004350;
                color: #000000;
                display: inline;
                padding: 10px 5px 20px 5px;
                position: absolute;
                width: 220px;
                display: none;
                margin: -1px 0 0 0;
            }
            #divChangeEstado span {
                background: none repeat scroll 0 0 #FFFFFF;
                border: 1px solid #004350;
                color: #000000;
                display: inline;
                padding: 2px 5px;
                position: absolute;
                width: 70px;
                display: none;
            }
            #divChangeEstado span p {
                padding: 4px 0;
                margin:0;
                cursor: pointer;
            }
            #divChangeEstado span p:hover {
                background: #DBEDFE;
            }

            #divEstado {
                background-position: left center;
                background-repeat: no-repeat;
                color: #0099CC;
                line-height: 20px;
                padding-left: 20px;
                cursor: pointer;
            }
            #divDireccion, #divTerminales, #divAsignarXY {
                background-color: #FFFFFF;
                background-position: left center;
                background-repeat: no-repeat;
                border: 1px solid #000000;
                color: #0099CC;
                cursor: pointer;
                line-height: 20px;
                padding-left: 20px;
            }

            .closed {
                background-image: url("../../../img/asc.gif");
            }
            .opened {
                background-image: url("../../../img/desc.gif");
            }

        </style>
        <script>
            $(function() {
                $("#btnFiltrar, #btnGrabar").button();
            });
        </script>
    </head>
    <body>
        <input type="hidden" value="1" name="emisor" id="emisor" />
        <div class="ui-layout-west">


            <div id="divPoligono">

                <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
                    <tr>
                        <td colspan="3"><span class="l_tit">Georeferencia - Duna ADSL</span></td>
                    </tr>
                    <tr>
                        <td align="left" class="label" width="100">Departamento:</td>
                        <td align="left" colspan="2">
                            <div id="divDepartamento">
                                <select name="departamento" id="departamento" style="font-size:11px;">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Provincia:</td>
                        <td align="left" colspan="2">
                            <div id="divProvincia">
                                <select name="provincia" id="provincia" style="font-size:11px;">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Distrito:</td>
                        <td align="left" colspan="2">
                            <div id="divDistrito">
                                <select name="distrito" id="distrito" style="font-size:11px;">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="label">Con x,y:</td>
                        <td align="left" colspan="2">
                            <select id="selDatosXY" style="font-size:11px;">
                                <option value="">Todos</option>
                                <option value="conXY">con X,Y</option>
                                <option value="sinXY">sin X,Y</option>
                            </select>
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            <div style="float:left;"><button id="btnFiltrar">Buscar</button></div>  
                        </td>
                        <td>
                            <div id="box_loading" style="float:left; margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                        </td>
                        <td align="right">
                            <button id="btnGrabar" onclick="GrabarDatos()">Grabar Datos</button>
                            <input type="hidden" id="trm_selected" readonly="readonly">
                        </td>
                    </tr>
                </table>

            </div>

            <div style="width:100%; margin-top:10px; border-top: 1px solid #000000; padding-top: 10px;">

                <div id="divInfoResult" style="margin-bottom: 8px; width: 385px;">
                    <div style="float:left;"><span id="spanNum">0</span> registro(s) encontrado(s)</div>
                </div>

                <div id="divTitle" style="background-color: #003366;border: 1px solid #000000;color: #FFFFFF;float: left;padding: 3px 0;width: 385px;">
                    <div style="width:30px;float:left;"><input type="checkbox" name="selTodos" id="selTodos"></div>
                    <div style="width:70px;float:left;">Codigo</div>
                    <div style="width:240px;float:left;">Direcci&oacute;n pirata</div>
                    <div style="width:40px;float:left;">Tipo</div>
                </div>
                <div id="sidebar"></div>
            </div>

        </div>
        <div class="ui-layout-center">

            <div id="box_resultado">

                <div id="map_canvas" style="width: 100%; height: 450px; float: left; background-color: #FFFFFF;"></div>

                <div id="divSearchAddress" style="float:right;width:120px;position:fixed;">
                    <div id="divDireccion" class="closed">
                        <div style="display: block;">Buscar direcci&oacute;n</div>
                    </div>
                    <span>
                        <b>Direcci&oacute;n:</b> <br />
                        <input type="text" name="direccion" id="direccion" size="35" style="font-size:11px;padding:3px 0;margin: 0 0 4px 0;">  <br />
                            <label style="color:#666666;">Ejm: las vegas, puente piedra</label>

                    </span>
                </div>

                <div id="divSearchTerminales" style="float:right;width:120px;position:fixed;margin-left:120px;">
                    <div id="divTerminales" class="closed">
                        <div style="display: block;">Buscar Terminal</div>
                    </div>
                    <span>

                        <table cellpadding="2" cellspacing="0" style="width:100%; border-collapse:collapse;">
                            <tr>
                                <td width="80">Zonal:</td>
                                <td>
                                    <select id="selZonal" style="font-size:11px;">
                                        <option value="">Seleccione</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Mdf:</td>
                                <td>
                                    </select>
                                    <div id="divMdf">
                                        <select id="selMdf" style="font-size:11px;">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Tipo de Red:</td>
                                <td>
                                    <div id="divTipRed">
                                        <select id="selTipRed" style="font-size:11px;">
                                            <option value="">Seleccione</option>
                                            <option value="D">Directa</option>
                                            <option value="F">Flexible</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Cable/Armario:</td>
                                <td>
                                    <div id="divCableArmario">
                                        <select id="selCableArmario" style="font-size:11px;">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Terminal:</td>
                                <td>
                                    <div id="divCaja">
                                        <select id="selCaja" style="font-size:11px;">
                                            <option value="">Seleccione</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" id="btnBuscarCaja" value="Buscar" style="font-size:11px;">
                                </td>
                                <td>
                                    <div id="box_loading2" style=" margin-top: 5px; margin-left: 10px; color:#1C94C4; display: none;">procesando.<img src="../../../img/loading.gif" alt="" title="" /></div>
                                </td>
                            </tr>
                        </table>

                    </span>

                </div>

            </div>

        </div>

        <div id="parentModal" style="display: none;">
            <div id="childModal" style="padding: 10px; background: #fff;"></div>
        </div>

    </body>
</html>