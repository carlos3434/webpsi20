<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<style type="text/css">
body{
    font-family: Arial;
    font-size: 11px;
}
.latlon {
    font-size: 11px;
}
.subtit_info {
    font-weight: bold;
    width: 80px;
}
.det_info {
    color: #666666;
    width:80px;
}
</style>

<script type="text/javascript">

var map = null;
var marker = null;
var markerInicial = null;
var markers_arr = [];
var markersInicial_arr = [];
var flagEditar = <?php echo (isset($_REQUEST['flagEditar']) && $_REQUEST['flagEditar'] == 1 ) ? '1' : '0' ;?>;
var idEdificio = <?php echo $_REQUEST['idedificio']?>;

$(document).ready(function () {
    
    var M = {
        initialize: function() {
            if (typeof (google) == "undefined") {
                alert('Verifique su conexion a maps.google.com');
                return false;
            }

            //var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
            var latlng = new google.maps.LatLng(0,0);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_edificio"), myOptions);
            
            
            M.cargaMapaEdificio(idEdificio);

            //cuando se hace click sobre el mapa
            google.maps.event.addListener(map, "click", function(event) {

            	//solo si viene desde la opcion de editar Edificio
                if ( flagEditar ) {

                	//clear markers
                    M.clearMarkers();
    	
                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map
                    });
                    
                    //agrego el marker al array de markers
                    markers_arr.push(marker);
                    
                    var coords = event.latLng;

                    //Agregando nuevo punto x,y para el edificio 
                    setTimeout(function() { M.updateXyEdificio(idEdificio, coords); }, 1000);  
                	
                }

            });

        },

        clearMarkers: function() {
            for (var n = 0, marker; marker = markers_arr[n]; n++) {
                marker.setVisible(false);
            }
        },

        clearMarkersInicial: function() {
            for (var n = 0, markerInicial; markerInicial = markersInicial_arr[n]; n++) {
                markerInicial.setVisible(false);
            }
        },

        updateXyEdificio: function(idEdificio, coords) {
        	if (confirm("Desea establecer este nuevo punto x,y?")) {

                //grabando el nuevo x,y 
                var data_content = {
    	            'idEdificio' : idEdificio,
    	            'x'          : coords.lng(),
    	            'y'          : coords.lat()
    	        };
    	        $.ajax({
    	            type:   "POST",
    	            url:    "../../../modulos/maps/bandeja.control.edificio.php?cmd=updateXyEdificio",
    	            data:   data_content,
    	            dataType: "json",
    	            success: function(data) {
    	            	alert(data['mensaje']);
    	            	
    	            	if (data['flag'] == '0') {
    	            		//clear markers 
                            M.clearMarkers();
                            
    	                } else {
    	                	//clear markersInicial 
                            M.clearMarkersInicial();

    	                	//limpio el mesaje que tiene X,Y 
                            $("#divMsgMapaEdificio").hide();

                            markerInicial = new google.maps.Marker({
                                position: coords, 
                                map: map, 
                                icon: 'icons/markergreen.png'
                            });  
                            markersInicial_arr.push(markerInicial);

                            //centro el mapa al nuevo punto x,y 
                            map.setCenter(coords);
    	                }
    	            }
    	        });
               
            } else {
            	//clear markers 
                M.clearMarkers();
            }

        },
        
        cargaMapaEdificio: function(idedificio) {
            
            data_content = {
                'idedificio' : idedificio
            };
            $.ajax({
                type:   "POST",
                url:    "../../../modulos/maps/bandeja.control.edificio.php?cmd=cargarMapaEdificio",
                data:   data_content,
                dataType: "json",
                success: function(data) {
                    if( data['flag'] == '1'  ) {
                        
                        var datos = data['marker'];
                        var arrZonal = data['zonal'];
                        var detalle = '';

                        var infowindow = new google.maps.InfoWindow({
                            content: detalle
                        });

                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        if ( arrZonal.length > 0 ) {
                        	latlng = new google.maps.LatLng(arrZonal[0]['y'], arrZonal[0]['x']);
                        }

                        
                        var zoom = 12;
                        if( datos['x'] != '' && datos['y'] != '' ) {
                        	$("#divMsgMapaEdificio").hide();
                            
                            latlng = new google.maps.LatLng(datos['y'],datos['x']);
                            zoom = 16;
                            
                            markerInicial = new google.maps.Marker({
                                position: latlng, 
                                map: map, 
                                title: datos['item'],
                                icon: 'icons/markergreen.png'
                            });  

                            markersInicial_arr.push(markerInicial);

                            google.maps.event.addListener(markerInicial, 'click', function() {
                                infowindow.open(map, markerInicial);
                            });

                        } else {
                        	$("#divMsgMapaEdificio").show();
                        }

                        map.setCenter(latlng);
                        map.setZoom(zoom);
                        
                    }else {
                        alert(data['mensaje']);
                        return false;
                    }

                }
                
            });
        }

    };

    M.initialize();
               
});


</script>

</head>
<body>

    <div id="divMsgMapaEdificio" style="display:none; color:#ff0000; text-align:left;">
        Este edificio no registra X,Y para poder visualizarlo en el mapa
    </div>
    <div id="map_edificio" style="width: 100%; height: 380px; background-color: #FFFFFF;"></div>

</body>
</html>