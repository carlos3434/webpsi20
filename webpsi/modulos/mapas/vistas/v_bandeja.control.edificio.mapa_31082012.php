<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>

<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<style type="text/css">
body{
    font-family: Arial;
    font-size: 11px;
}
.latlon {
    font-size: 11px;
}
.subtit_info {
    font-weight: bold;
    width: 80px;
}
.det_info {
    color: #666666;
    width:80px;
}
</style>

<script type="text/javascript">

var map = null;

$(document).ready(function () {
    
    var M = {
        initialize: function() {
            if (typeof (google) == "undefined") {
                alert('Verifique su conexion a maps.google.com');
                return false;
            }

            //var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
            var latlng = new google.maps.LatLng(0,0);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_edificio"), myOptions);
            
            
            M.cargaMapaEdificio(<?php echo $_REQUEST['idedificio']?>);

        },
        
        cargaMapaEdificio: function(idedificio) {
            
            data_content = {
                'idedificio' : idedificio
            };
            $.ajax({
                type:   "POST",
                url:    "../../../modulos/maps/bandeja.control.edificio.php?cmd=cargarMapaEdificio",
                data:   data_content,
                dataType: "json",
                success: function(data) {
                    if( data['flag'] == '1'  ) {
                        
                        var datos = data['marker'];
                        var detalle = '';

                        var infowindow = new google.maps.InfoWindow({
                            content: detalle
                        });

                        var latlng = new google.maps.LatLng(-12.0692083678587, -77.0407839355469);
                        var zoom = 12;
                        if( datos['x'] != '' && datos['y'] != '' ) {
                            latlng = new google.maps.LatLng(datos['y'],datos['x']);
                            zoom = 16;
                            
                            var marker = new google.maps.Marker({
                                position: latlng, 
                                map: map, 
                                title: datos['item']
                            });  

                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map,marker);
                            });
                        }

                        map.setCenter(latlng);
                        map.setZoom(zoom);
                        
                    }else {
                        alert(data['mensaje']);
                        return false;
                    }

                }
                
            });
        }

    };

    M.initialize();
               
});


</script>

</head>
<body>

    <div id="map_edificio" style="width: 100%; height: 380px; background-color: #FFFFFF;"></div>

</body>
</html>