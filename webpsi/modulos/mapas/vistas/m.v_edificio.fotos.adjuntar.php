<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />



<!-- <script type="text/javascript" src="../../js/jquery/jquery.js"></script> -->
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-ui-1.8.2.custom.min.js"></script>

<link type="text/css" rel="stylesheet" href="../../js/jquery/jqueryui_1.8.2/development-bundle/themes/redmond/jquery.ui.all.css" />
<link href="../../css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="../../js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/functions.js"></script>
<script type="text/javascript" src="../../modulos/maps/js/m.edificios.fftt.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#fecha_foto").datepicker({ 
        dateFormat: 'dd/mm/yy',
        yearRange:'-20:+10',
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1
    });
    
    $(".limpiar_fecha").click(function() {
        var campo = $(this).attr('c_fecha');
        $("#" + campo).attr('value', '');
    });

    limpiarFoto = function(id) {
        //$("#" + id).attr('value','');
        $("#" + id).replaceWith('<input id="' + id + '" class="frm_fotos" type="file" size="15" name="imagen_edificio[]">');
    }

    $('#frm_adjuntar_fotos').ajaxForm({ 
        dataType    : 'json', 
        beforeSubmit: validate,
        success     : processJson     
    });

    function validate(formData, jqForm, options){
        var query = $("input.required, select.required");
        
        var is_validate = 1;
        $.each( query, function() {
            if ( $(this).val() == '' ) {
                alert("Ingrese el campo " + $(this).attr('msg'));
                $(this).focus();
                is_validate = 0;
                return false;
            }
        });
        
        if (!is_validate) {
            return false;
        }
 
        if ( $("#foto1").val() == '') {
            alert("Ingrese por lo menos la Foto 1");
            return false;
        }
        
        $("#msg").show();
        $("input[name=btn_guardar]").attr('disabled','disabled');
        
        var error_foto = '';
        var permitida = false;
        var image = $('input[name=imagen_edificio[]]').fieldValue(); 

        $.each(image, function(i, item) {
            if (image[i]!=''){
                var extension = (image[i].substring(image[i].lastIndexOf("."))).toLowerCase();
                if (! (extension && /^(.jpg|.jpeg)$/.test(extension))){ //.jpg|.png|.jpeg|.gif
                    // extensiones permitidas
                    error_foto = 'Error: Solo se permiten imagenes de tipo JPG.';
                    // cancela upload
                    $("input[name=btn_guardar]").attr('disabled','');
                    permitida = true;
                }
            }
        });
        if (permitida) {
        	$("#msg").hide(); 
            alert(error_foto); 
            return false; 
        }
    }
     
    function processJson(data) { 
        //limpiando el formulario
        $(".frm_fotos").attr('value','');
        
        $("#foto1").replaceWith('<input id="foto1" class="frm_fotos" type="file" size="15" name="imagen_edificio[]">');
        $("#foto2").replaceWith('<input id="foto2" class="frm_fotos" type="file" size="15" name="imagen_edificio[]">');
        $("#foto3").replaceWith('<input id="foto3" class="frm_fotos" type="file" size="15" name="imagen_edificio[]">');
        $("#foto4").replaceWith('<input id="foto4" class="frm_fotos" type="file" size="15" name="imagen_edificio[]">');
        
        $("#estado").find('option:first').attr('selected','selected');
        
        
        $("#msg").hide();
        $("input[name=btn_guardar]").attr('disabled','');
        
        if (data['tipo'] == 'imagen') { 
            alert(data.mensaje); 
            return false; 
        }
        if (data['flag'] == '1'){
            alert(data['mensaje']);

            var idedificio = $('#idEdificio').val();
            window.top.location = 'm.buscomp.add.componente.php?cmd=listadoFotosEdificio&capa=edi&idedificio=' + idedificio;

        }else{
            alert(data['mensaje']);
        } 
    }
    
});
</script>

<style>
em {
    color: #ff0000;
}
.limpiar_fecha {
    color: #0000FF !important;
}

body {
    color: #4B4B4B;
    font: 11px Arial,Helvetica,sans-serif;
}

table.tb_detalle_edi {
    background-color: #F2F2F2;
    font-size: 12px;
}
table.tb_detalle_edi .da_titulo_gest {
    background-color: #1E9EBB;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 20px;
}
table.tb_detalle_edi .da_subtitulo {
    background-color: #E1E2E2;
    color: #000000;
    font-size: 11px;
    font-weight: bold;
}
table.tb_detalle_edi .da_import {
    color: red;
    font-size: 12px;
    font-weight: bold;
}
table.tb_detalle_edi td {
    background-color: #FFFFFF;
    padding: 2px;
}

table.tb_detalle_edi textarea {
    width: 95%;
    font-size: 12px;
}
table.tb_detalle_edi select {
    min-width: 50%;
    font-size: 12px;
}

table.tb_detalle_edi .clsTxtFecha {
    width: 100px;
}

</style>

</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Adjuntar fotos al edificio</td>
</tr>
</table>

<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
    
    <form enctype="multipart/form-data" action="../../modulos/maps/bandeja.control.edificio.php?cmd=registrarFotos" method="post" name="frm_adjuntar_fotos" id="frm_adjuntar_fotos">
    <table id="tbl_detalle_edi" class="tb_detalle_edi" width="100%">
    <tbody>
    <tr>
        <td class="da_titulo_gest" colspan="6">FOTOS DEL EDIFICIO</td>
    </tr>
    <tr style="text-align: left;">
        <td width="30%" class="da_subtitulo">ITEM</td>
        <td width="70%" class="da_import"><?php echo $arrObjEdificio[0]->__get('_item')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td class="da_import"><?php echo $arrObjEdificio[0]->__get('_desEstado')?></td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">DIRECCI&Oacute;N OBRA</td>
        <td>
            <?php echo $tipoVia . ' ' . strtoupper($arrObjEdificio[0]->__get('_direccionObra')) . ' ' . $arrObjEdificio[0]->__get('_numero')?>
            <?php 
            if ( $arrObjEdificio[0]->__get('_sector') != '' ) {
                echo 'Sector: ' . strtoupper($arrObjEdificio[0]->__get('_sector')) . ', ';
            }
            if ( $arrObjEdificio[0]->__get('_mzaTdp') != '' ) {
                echo 'Mza. TDP: ' . strtoupper($arrObjEdificio[0]->__get('_mzaTdp'));
            }
            ?>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo" width="20%">FECHA FOTO</td>
        <td width="80%">
            <input id="fecha_foto" name="fecha_foto" type="text" value="" readonly="readonly" size="8" class="required frm_fotos clsTxtFecha" msg="Fecha foto" /> <em>(*)</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">ESTADO</td>
        <td>
            <select name="estado" id="estado" class="required frm_fotos" msg="Estado" >
                <option value="">Seleccione</option>
                <?php
                foreach ($arrObjEdificioEstado as $objEdificioEstado) {
                ?>
                <option value="<?php echo $objEdificioEstado->__get('_codEdificioEstado')?>"><?php echo $objEdificioEstado->__get('_desEstado')?></option>
                <?php
                }
                ?>
            </select>
            <em>(*)</em>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FOTO 1</td>
        <td>
            <input id="foto1" name="imagen_edificio[]" type="file" size="15" class="frm_fotos" /> &nbsp;<em>(*)</em>
            <a onclick="limpiarFoto('foto1')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FOTO 2</td>
        <td>
            <input id="foto2" name="imagen_edificio[]" type="file" size="15" class="frm_fotos" /> &nbsp; &nbsp; 
            <a onclick="limpiarFoto('foto2')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FOTO 3</td>
        <td>
            <input id="foto3" name="imagen_edificio[]" type="file" size="15" class="frm_fotos" /> &nbsp; &nbsp; 
            <a onclick="limpiarFoto('foto3')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
        </td>
    </tr>
    <tr style="text-align: left;">
        <td class="da_subtitulo">FOTO 4</td>
        <td>
            <input id="foto4" name="imagen_edificio[]" type="file" size="15" class="frm_fotos" /> &nbsp; &nbsp; 
            <a onclick="limpiarFoto('foto4')" href="javascript:void(0)" style="color: #0086C3;">Limpiar</a>
            <span class="requerido">(Solo archivos tipo .jpg -Tama&ntilde;o max: 500Kb) </span>
        </td>
    </tr>
    <tr>
        <td class="da_subtitulo" style="text-align: left;vertical-align:top;">OBSERVACIONES:</td>
        <td style="text-align:left;">
            <textarea name="observaciones" id="observaciones" style="width:95%;height:100px;" class="frm_fotos">SIN OBSERVACIONES</textarea>
        </td>
    </tr>
    </table>
    
    <div style="height:5px;"></div>

    <table width="100%" cellspacing="1" cellpadding="0" border="0">
    <tbody>
    <tr>
        <td width="90%" style="text-align: left; vertical-align: top">
            <div style="padding-top: 5px; font-size: 11px; float:left;">
                <input type="hidden" name="idEdificio" id="idEdificio" value="<?php echo $arrObjEdificio[0]->__get('_idEdificio')?>" />
                <input type="hidden" name="ingreso" id="ingreso" value="2" />
                <input type="submit" value="Grabar" name="btn_guardar" title="Grabar" id="btn_guardar" class="submit">&nbsp;
                <input type="button" value="Cerrar" name="cerrar" title="Cerrar" id="btn_regresar" class="submit" onclick="javascript: history.back();" />
            </div>
            <div style="margin: 10px 0 0 20px; display: none; float:left;" id="msg">
                <img alt="" src="../../img/loading.gif"> <b>Grabando datos.</b>
            </div>
        </td>
    </tr>
    </tbody>
    </table>
    </form>
    
    
</div>

</body>
</html>
