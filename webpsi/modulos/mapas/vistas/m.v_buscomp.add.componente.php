<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(document).ready(function() {

    $(".upload").toggle(function() {
            $("#div" + $(this).attr('id')).show();
            $("#" + $(this).attr('id')).attr('value', ' [x] ');
        }, function () {
            $("#div" + $(this).attr('id')).hide();
            $("#" + $(this).attr('id')).attr('value', ' .. ');
        }
    );
    
    $("#Cancelar").click(function() {
        var x = $("#x").val();
        var y = $("#y").val();
        
        window.top.location = 'm.buscomp.add.php?action=add&x=' + x + '&y=' + y;
    });
    
    $("#nro_blocks").change(function() {
        nro_blocks = this.value;
        if (nro_blocks == "") {
            $("#divBlock").html('');
        }else {
            $("#divBlock").html('');
            selhtml = '<div style="width:180px;">';
            selhtml += '<span style="float:left; width:20px;">&nbsp;</span><span style="float:left; width:80px; font-weight:bold;">Nro. pisos</span>';
            selhtml += '<span style="float:left; width:80px; font-weight:bold;">Nro. dptos</span>';
            selhtml += '</div>';
            for(var i=1; i<=nro_blocks; i++) {
                selhtml += '<div style="width:180px;">';
                selhtml += '<span style="float:left; width:20px;">#' + i + ' </span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_pisos_' + i + '" id="nro_pisos_' + i + '" size="5" value="" /></span>';
                selhtml += '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;"><input type="text" class="clsBlocks" onkeypress="return soloNumeros(this)" name="nro_dptos_' + i + '" id="nro_dptos_' + i + '" size="5" value="" /></span>';
                selhtml += '</div>';
            }
            $("#divBlock").html(selhtml);
        }
    });
    
    $("#selDpto").change(function() {
        IDdpto = this.value;
        if (IDdpto == "") {
            return false;
        }
        data_content = "action=buscarProv&IDdpto=" + IDdpto;
        $.ajax({
            type:   "POST",
            url:    "m.buscomp.add.componente.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                var selProv = ''
                for (var i in datos) {
                    selProv += '<option value="' + datos[i]['codprov'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selProv").html(selProv);
                $("#selDist").html('<option value="">Seleccione</option>');
            }
        });
    });
    
    $("#selProv").change(function() {
        IDdpto = $("#selDpto").val()
        IDprov = this.value;
        if (IDdpto == "" && IDprov == "") {
            return false;
        }
        data_content = "action=buscarDist&IDdpto=" + IDdpto + "&IDprov=" + IDprov;
        $.ajax({
            type:   "POST",
            url:    "m.buscomp.add.componente.php",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                
                var selDist = ''
                for (var i in datos) {
                    selDist += '<option value="' + datos[i]['coddist'] + '">' + datos[i]['nombre'] + '</option>';
                }

                $("#selDist").html(selDist);
            }
        });
    });
    
    $("#GrabarEdificio").click(function() {
        var fecha_registro_proyecto = $("#dia_fecha_registro_proyecto").val() + '-' + $("#mes_fecha_registro_proyecto").val() + '-' + $("#ano_fecha_registro_proyecto").val();
        if ( fecha_registro_proyecto != '' && fecha_registro_proyecto != '--' ) {
            if ( ValidarFecha( fecha_registro_proyecto ) == false ) {
                alert("Ingrese la fecha de registro de proyecto correcta.");
                return false;
            }
        }else {
            fecha_registro_proyecto = '';
        }
    
        var fecha_termino_construccion = $("#dia_fecha_termino_construccion").val() + '-' + $("#mes_fecha_termino_construccion").val() + '-' + $("#ano_fecha_termino_construccion").val();
        if ( fecha_termino_construccion != '' && fecha_termino_construccion != '--' ) {
            if ( ValidarFecha( fecha_termino_construccion ) == false ) {
                alert("Ingrese la fecha de termino de construccion correcta.");
                return false;
            }
        }else {
            fecha_termino_construccion = '';
        }
        
        var montante_fecha = $("#dia_montante_fecha").val() + '-' + $("#mes_montante_fecha").val() + '-' + $("#ano_montante_fecha").val();
        if ( montante_fecha != '' && montante_fecha != '--' ) {
            if ( ValidarFecha( montante_fecha ) == false ) {
                alert("Ingrese montante fecha correcta.");
                return false;
            }
        }else {
            montante_fecha = '';
        }
        
        var ducteria_interna_fecha = $("#dia_ducteria_interna_fecha").val() + '-' + $("#mes_ducteria_interna_fecha").val() + '-' + $("#ano_ducteria_interna_fecha").val();
        if ( ducteria_interna_fecha != '' && ducteria_interna_fecha != '--' ) {
            if ( ValidarFecha( ducteria_interna_fecha ) == false ) {
                alert("Ingrese ducteria interna fecha correcta.");
                return false;
            }
        }else {
            ducteria_interna_fecha = '';
        }
        
        var punto_energia_fecha = $("#dia_punto_energia_fecha").val() + '-' + $("#mes_punto_energia_fecha").val() + '-' + $("#ano_punto_energia_fecha").val();
        if ( punto_energia_fecha != '' && punto_energia_fecha != '--' ) {
            if ( ValidarFecha( punto_energia_fecha ) == false ) {
                alert("Ingrese punto de energia fecha correcta.");
                return false;
            }
        }else {
            punto_energia_fecha = '';
        }
        
        var fecha_seguimiento = $("#dia_fecha_seguimiento").val() + '-' + $("#mes_fecha_seguimiento").val() + '-' + $("#ano_fecha_seguimiento").val();
        if ( fecha_seguimiento != '' && fecha_seguimiento != '--' ) {
            if ( ValidarFecha( fecha_seguimiento ) == false ) {
                alert("Ingrese fecha de seguimiento correcta.");
                return false;
            }
        }else {
            fecha_seguimiento = '';
        }
    
        var data = {
            x                         : $("#x").val(),
            y                         : $("#y").val(),
            estado                    : $("#estado").val(),
            archivo1                 : $("#archivo1").val(),
            archivo2                 : $("#archivo2").val(),
            archivo3                 : $("#archivo3").val(),
            archivo4                 : $("#archivo4").val(),
        fecha_registro_proyecto : fecha_registro_proyecto,
            ura                         : $("#ura").val(),
            sector                     : $("#sector").val(),
            mza_tdp                     : $("#mza_tdp").val(),
            idtipo_via                 : $("#idtipo_via").val(),
            direccion_obra             : $("#direccion_obra").val(),
            numero                     : $("#numero").val(),
            mza                         : $("#mza").val(),
            lote                     : $("#lote").val(),
            idtipo_cchh                 : $("#idtipo_cchh").val(),
            cchh                     : $("#cchh").val(),
            distrito                 : $("#selDpto").val() + $("#selProv").val() + $("#selDist").val(),
            nombre_constructora         : $("#nombre_constructora").val(),
            nombre_proyecto             : $("#nombre_proyecto").val(),
            idtipo_proyecto             : $("#idtipo_proyecto").val(),
            persona_contacto         : $("#persona_contacto").val(),
            pagina_web                 : $("#pagina_web").val(),
            email                     : $("#email").val(),
            nro_blocks                 : $("#nro_blocks").val(),
            nro_pisos_1                 : $("#nro_pisos_1").val(),
            nro_dptos_1                 : $("#nro_dptos_1").val(),
            nro_pisos_2                 : $("#nro_pisos_2").val(),
            nro_dptos_2                 : $("#nro_dptos_2").val(),
            nro_pisos_3                 : $("#nro_pisos_3").val(),
            nro_dptos_3                 : $("#nro_dptos_3").val(),
            nro_pisos_4                 : $("#nro_pisos_4").val(),
            nro_dptos_4                 : $("#nro_dptos_4").val(),
            nro_pisos_5                 : $("#nro_pisos_5").val(),
            nro_dptos_5                 : $("#nro_dptos_5").val(),
            idtipo_infraestructura    : $("#idtipo_infraestructura").val(),
            nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
            avance                     : $("#avance").val(),
        fecha_termino_construccion  : fecha_termino_construccion,
            montante                 : $("#montante").val(),
        montante_fecha            : montante_fecha,
            montante_obs             : $("#montante_obs").val(),
            ducteria_interna         : $("#ducteria_interna").val(),
        ducteria_interna_fecha  : ducteria_interna_fecha,
            ducteria_interna_obs     : $("#ducteria_interna_obs").val(),
            punto_energia             : $("#punto_energia").val(),
        punto_energia_fecha        : punto_energia_fecha,
            punto_energia_obs         : $("#punto_energia_obs").val(),
        fecha_seguimiento        : fecha_seguimiento,
            observacion                 : $("#observacion").val()
        };

        
        if ( data['estado'] == "" ) {
                alert("Ingrese el campo Estado del Edificio.");
                return false;
        }
        if ( data['fecha_registro_proyecto'] == "" ) {
            alert("Ingrese el campo Fecha Registro Proyecto.");
            return false;
        }
        if ( data['ura'] == "" ) {
            alert("Ingrese el campo URA.");
            return false;
        }
        if ( data['idtipo_via'] == "" ) {
            alert("Ingrese el campo Tipo Via.");
            return false;
        }
        if ( data['idtipo_cchh'] == "" ) {
            alert("Ingrese el campo Tipo CCHH.");
            return false;
        }
        if ( $("#selDpto").val() == "" ) {
            alert("Ingrese el campo Departamento.");
            return false;
        }
        if ( $("#selProv").val() == "" ) {
            alert("Ingrese el campo Provincia.");
            return false;
        }
        if ( $("#selDist").val() == "" ) {
            alert("Ingrese el campo Distrito.");
            return false;
        }
        if ( data['idtipo_proyecto'] == "" ) {
            alert("Ingrese el campo Tipo Proyecto.");
            return false;
        }
        if ( data['nro_blocks'] == "" ) {
            alert("Ingrese el campo Nro. de Blocks.");
            return false;
        }
    

        var query = $("input.clsBlocks").serializeArray();
        for (i in query) {
            if ( query[i].value == "" ) {
                alert("Ingrese todos los datos en Nro. pisos y Nro. dptos.");
                $("#" + query[i].name).focus();
                return false;
            }
        } 

        if ( data['idtipo_infraestructura'] == "" ) {
            alert("Ingrese el campo Tipo Infraestructura.");
            return false;
        }

        
        data_content = data;
        $("#msg").show();
        $("#GrabarEdificio").attr('disabled', 'disabled');
        $("#Cancelar").attr('disabled', 'disabled');
        $.ajax({
            type:   "POST",
            url:    "m.buscomp.add.componente.php?action=GrabaEdificio",
            data:   data_content,
            dataType: "json",
            success: function(datos) {
                if (datos['success'] == 1) {
                    alert(datos['msg']);
                    
                    //redirect al mapa
                    setTimeout("redirect(" + data['x'] + "," + data['y'] + ",'edi')", 1000);
                }else {
                    alert(datos['msg']);
                }
                
                $("#msg").hide();
            }
        });
        
        
        
    });
    
    $("#GrabarCompetencia").click(function() {
        var data = {
            x                         : $("#x").val(),
            y                         : $("#y").val(),
            archivo1                 : $("#archivo1").val(),
            archivo2                 : $("#archivo2").val(),
            archivo3                 : $("#archivo3").val(),
            archivo4                 : $("#archivo4").val(),
            competencia             : $("#competencia").val(),
            competencia_otro         : $("#competencia_otro").val(),
            elementoencontrado         : $("#elementoencontrado").val(),
            elementoencontrado_otro    : $("#elementoencontrado_otro").val(),
            ura                        : $("#mdf").val(),
            direccion                : $("#direccion").val(),
            observacion                : $("#obs").val()
        };

        if ( data['competencia'] == "" ) {
            alert("Ingrese el campo Competencia.");
            return false;
        }
        if ( $('#competencia option:selected').text() == 'Otro' && $("#competencia_otro").val() == '' ) {
            alert("Indique la competencia");
            return false;
        }
        
        
        data_content = data;
        $("#GrabarCompetencia").attr('disabled', 'disabled');
        $("#Cancelar").attr('disabled', 'disabled');
        $.ajax({
            type:   "POST",
            url:    "m.buscomp.add.componente.php?action=GrabaCompetencia",
            data:   data_content,
            dataType: "json",
            success: function(datos) {

                if (datos['success'] == 1) {
                       $("#divMsgFormCapa").html('<div style="color:#0E774A;"><span class="saveOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                   }else {
                       $("#divMsgFormCapa").html('<div style="color:#FF0000;"><span class="saveNoOK"></span>' + datos['msg'] + '</div>').hide().fadeIn("slow");
                   }
            }
        });
        
        //redirect al mapa
        setTimeout("redirect(" + data['x'] + "," + data['y'] + ",'com')", 3000);
    });
    
    
    //carga inicial
    //M.initialize();
    
});

function redirect(x,y,comp) {

    window.top.location = 'm.buscomp.vermapa.php?search=1&x=' + x + '&y=' + y + '&comp[' + comp + ']=' + comp;
}

function soloNumeros(obj) {
    var re = /^(-)?[0-9]*$/;
    if (!re.test(obj.value)) {
        obj.value = '';
    }
}


// El Formato es dd-mm-aaaa  
// Ejemplo: if (Validar('14-08-1981')==false) { alert('Entrada Incorrecta') }  
// Uso en formularios: onSubmit="return Validar(this.fecha.value)" 
function ValidarFecha(Cadena){  
    var Fecha= new String(Cadena)   // Crea un string  
    var RealFecha= new Date()   // Para sacar la fecha de hoy  
    // Cadena A�o  
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))  
    // Cadena Mes  
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))  
    // Cadena D�a  
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("-")))  
  
    if ( Dia == '' || Mes == '' || Ano == '' ) {
        //alert('Fecha ingresada no es correcta');
        return false;
    }
  
    // Valido el a�o  
    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
        //alert('A�o inv�lido')  
        return false  
    }  
    // Valido el Mes  
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
        //alert('Mes inv�lido')  
        return false  
    }  
    // Valido el Dia  
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
        //alert('D�a inv�lido')  
        return false  
    }  
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
        if (Mes==2 && Dia > 28 || Dia>30) {  
            //alert('D�a inv�lido')  
            return false  
        }  
    }  
    
}

</script>
</head>
<body>

<?php
include '../../m.header.php';
?>

<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td class="titulo">Agregar Componente</td>
</tr>
</table>



<table class="principal" border="0" style="padding:0px;" cellspacing="0" cellpadding="0" align="center" width="100%">
<tr>
    <td>X: <input type="text" name="x" id="x" value="<?php echo $_REQUEST['x']?>" readonly="readonly" /><td>
</tr>
<tr>
    <td>Y: <input type="text" name="y" id="y" value="<?php echo $_REQUEST['y']?>" readonly="readonly" /><td>
</tr>
</table>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
<tr>
    <td valign="top">
    
        <div style="height:15px;"></div>
        
        <?php
        if ( $_REQUEST['capa'] == 'edi' ) {
        ?>
        
        <table id="tblPrincipal" class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
        <tr>
            <td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Edificio: Informaci&oacute;n principal</td>
        </tr>
        <tr>
            <td width="150" class="compSubTitulo">Estado del Edificio:</td>
            <td>
                <select name="estado" id="estado">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrEstado as $id=>$val ) {
                    ?>
                    <option value="<?php echo $id?>"><?php echo $val?></option>
                    <?php 
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 1:</td>
            <td>
                <input type="text" name="archivo1" id="archivo1" value="" readonly="readonly"/>
                <input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-1" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=edi&archivo=archivo-1" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 2:</td>
            <td>
                <input type="text" name="archivo2" id="archivo2" value="" readonly="readonly"/>
                <input type="button" name="archivo-2" id="archivo-2" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-2" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=edi&archivo=archivo-2" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 3:</td>
            <td>
                <input type="text" name="archivo3" id="archivo3" value="" readonly="readonly"/>
                <input type="button" name="archivo-3" id="archivo-3" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-3" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=edi&archivo=archivo-3" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 4:</td>
            <td>
                <input type="text" name="archivo4" id="archivo4" value="" readonly="readonly"/>
                <input type="button" name="archivo-4" id="archivo-4" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-4" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=edi&archivo=archivo-4" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>

        <tr>
            <td class="compSubTitulo">Fecha Registro Proyecto:</td>
            <td>
                <!--<input type="text" name="fecha_registro_proyecto" size="11" id="fecha_registro_proyecto" value="" readonly="readonly" />-->
                <select name="dia_fecha_registro_proyecto" id="dia_fecha_registro_proyecto">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        if ( $diaActual == $mI ) {
                            $diaSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_fecha_registro_proyecto" id="mes_fecha_registro_proyecto">
                    <option value="">Mes</option>
                    <?php 
                    foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
                        if ( $mesActual == $idmes ) {
                            $mesSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_fecha_registro_proyecto" id="ano_fecha_registro_proyecto">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        if ( $anoActual == $i ) {
                            $anoSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">URA:</td>
            <td>
                <select name="ura" id="ura">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrMdfs as $mdf ) {
                    ?>
                    <option value="<?php echo $mdf['mdf']?>"><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Sector:</td>
            <td>
                <input type="text" name="sector" id="sector" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Mza. (TdP):</td>
            <td>
                <input type="text" name="mza_tdp" id="mza_tdp" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Tipo Via:</td>
            <td>
                <select name="idtipo_via" id="idtipo_via">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrTipoVia as $tipoVia ) {
                    ?>
                    <option value="<?php echo $tipoVia['iddescriptor']?>"><?php echo $tipoVia['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Direcci&oacute;n de Obra (Nombre de Via ):</td>
            <td>
                <input type="text" name="direccion_obra" id="direccion_obra" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">N&deg;:</td>
            <td>
                <input type="text" name="numero" id="numero" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Mza.:</td>
            <td>
                <input type="text" name="mza" id="mza" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Lote:</td>
            <td>
                <input type="text" name="lote" id="lote" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Tipo CCHH:</td>
            <td>
                <select name="idtipo_cchh" id="idtipo_cchh">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrTipoCchh as $tipoCchh ) {
                    ?>
                    <option value="<?php echo $tipoCchh['iddescriptor']?>"><?php echo $tipoCchh['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">CCHH:</td>
            <td>
                <input type="text" name="cchh" id="cchh" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Departamento:</td>
            <td>
                <select name="selDpto" id="selDpto">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrDpto as $dpto ) {
                    ?>
                    <option value="<?php echo $dpto['coddpto']?>"><?php echo $dpto['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Provincia:</td>
            <td>
                <div id="divProv">
                    <select id="selProv">
                        <option value="">Seleccione</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Distrito:</td>
            <td>
                <div id="divDist">
                    <select id="selDist">
                        <option value="">Seleccione</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Nombre de Constructora:</td>
            <td>
                <input type="text" name="nombre_constructora" id="nombre_constructora" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Nombre del proyecto:</td>
            <td>
                <input type="text" name="nombre_proyecto" id="nombre_proyecto" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Tipo Proyecto:</td>
            <td>
                <select name="idtipo_proyecto" id="idtipo_proyecto">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrTipoProyecto as $tipoProyecto ) {
                    ?>
                    <option value="<?php echo $tipoProyecto['iddescriptor']?>"><?php echo $tipoProyecto['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Persona de Contacto:</td>
            <td>
                <input type="text" name="persona_contacto" id="persona_contacto" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">P&aacute;gina Web:</td>
            <td>
                <input type="text" name="pagina_web" id="pagina_web" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">E-mail:</td>
            <td>
                <input type="text" name="email" id="email" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">N&deg; de Blocks:</td>
            <td>
                <select id="nro_blocks" name="nro_blocks" style="width:80px;">
                    <option value="">Seleccione</option>
                    <?php 
                    for ($i=1;$i<=5;$i++) {
                    ?>
                    <option value="<?php echo $i?>"><?php echo $i?></option>
                    <?php 
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo"></td>
            <td>
                <div id="divBlock"></div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Tipo de Infraestructura:</td>
            <td>
                <select name="idtipo_infraestructura" id="idtipo_infraestructura">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrTipoInfraestructura as $tipoInfraestructura ) {
                    ?>
                    <option value="<?php echo $tipoInfraestructura['iddescriptor']?>"><?php echo $tipoInfraestructura['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">N&deg; Dptos Habitados:</td>
            <td>
                <input type="text" name="nro_dptos_habitados" id="nro_dptos_habitados" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">% Avance:</td>
            <td>
                <input type="text" name="avance" id="avance" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Fecha de Termino Construcci&oacute;n:</td>
            <td>
                <select name="dia_fecha_termino_construccion" id="dia_fecha_termino_construccion">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        if ( $diaActual == $mI ) {
                            $diaSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_fecha_termino_construccion" id="mes_fecha_termino_construccion">
                    <option value="">Mes</option>
                    <?php 
                    foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
                        if ( $mesActual == $idmes ) {
                            $mesSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_fecha_termino_construccion" id="ano_fecha_termino_construccion">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        if ( $anoActual == $i ) {
                            $anoSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Montante:</td>
            <td>
                <select name="montante" id="montante" style="width:80px;">
                    <option value="">Seleccione</option>
                    <option value="S">Si</option>
                    <option value="N">No</option>
                </select> &nbsp; 
                <span class="clsBold">Fecha:</span> 
                <select name="dia_montante_fecha" id="dia_montante_fecha">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_montante_fecha" id="mes_montante_fecha">
                    <option value="">Mes</option>
                    <?php 
                        foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
    
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_montante_fecha" id="ano_montante_fecha">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Montante (Observacion):</td>
            <td>
                <textarea name="montante_obs" id="montante_obs" style="width:200px;height:50px;"></textarea>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Ducteria Interna:</td>
            <td>
                <select name="ducteria_interna" id="ducteria_interna"  style="width:80px;">
                    <option value="">Seleccione</option>
                    <option value="S">Si</option>
                    <option value="N">No</option>
                </select> &nbsp; 
                <span class="clsBold">Fecha:</span> 
                <select name="dia_ducteria_interna_fecha" id="dia_ducteria_interna_fecha">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_ducteria_interna_fecha" id="mes_ducteria_interna_fecha">
                    <option value="">Mes</option>
                    <?php 
                    foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_ducteria_interna_fecha" id="ano_ducteria_interna_fecha">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Ducteria Interna (Observacion):</td>
            <td>
                <textarea name="ducteria_interna_obs" id="ducteria_interna_obs" style="width:200px;height:50px;"></textarea>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Punto de energ&iacute;a:</td>
            <td>
                <select name="punto_energia" id="punto_energia"  style="width:80px;">
                    <option value="">Seleccione</option>
                    <option value="S">Si</option>
                    <option value="N">No</option>
                </select> &nbsp; 
                <span class="clsBold">Fecha:</span> 
                <select name="dia_punto_energia_fecha" id="dia_punto_energia_fecha">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_punto_energia_fecha" id="mes_punto_energia_fecha">
                    <option value="">Mes</option>
                    <?php 
                    foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_punto_energia_fecha" id="ano_punto_energia_fecha">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Punto de energia (Observacion):</td>
            <td>
                <textarea name="punto_energia_obs" id="punto_energia_obs" style="width:200px;height:50px;"></textarea>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Fecha de seguimiento:</td>
            <td>
                <select name="dia_fecha_seguimiento" id="dia_fecha_seguimiento">
                    <option value="">Dia</option>
                    <?php 
                    for ($i=1; $i<=31; $i++) {
                        $mI = ( strlen($i) == 1 ) ? '0' . $i : $i;
                        $diaSel = "";
                        if ( $diaActual == $mI ) {
                            $diaSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $mI?>" <?php echo $diaSel?>><?php echo $mI?></option>
                    <?php } ?>
                </select>
                <select name="mes_fecha_seguimiento" id="mes_fecha_seguimiento">
                    <option value="">Mes</option>
                    <?php 
                    foreach ( $arrMeses as $idmes=>$mes ) {
                        $mesSel = "";
                        if ( $mesActual == $idmes ) {
                            $mesSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $idmes?>" <?php echo $mesSel?>><?php echo $mes?></option>
                    <?php } ?>
                </select>
                <select name="ano_fecha_seguimiento" id="ano_fecha_seguimiento">
                    <option value="">A&ntilde;o</option>
                    <?php 
                    for ($i=$anoActual-20;$i<=$anoActual+10;$i++) {
                        $anoSel = "";
                        if ( $anoActual == $i ) {
                            $anoSel = "selected";
                        }
                        ?>
                        <option value="<?php echo $i?>" <?php echo $anoSel?>><?php echo $i?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Observaci&oacute;n:</td>
            <td>
                <textarea name="observacion" id="observacion" style="width:200px;height:50px;"></textarea>
            </td>
        </tr>
        </table>
        
        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
        <tr>
            <td colspan="2" height="20">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                            <div style="margin: 5px 0; display: none;" id="msg">
                                <img alt="" src="../../img/loading.gif"> <b>Grabando.</b>
                            </div>
                            
                <input type="submit" name="GrabarEdificio" id="GrabarEdificio" class="submit" value="Grabar Edificio" />
                <input type="button" id="Cancelar" class="submit" value="Cancelar" />
            </td>
        </tr>
        </table>
        
        <?php
        } elseif ( $_REQUEST['capa'] == 'com' ) {
        ?>
        
        <table id="tblPrincipal" class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
        <tr>
            <td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Competencia: Informaci&oacute;n principal</td>
        </tr>
        <tr>
            <td width="150" class="compSubTitulo">Foto 1:</td>
            <td>
                <input type="text" name="archivo1" id="archivo1" value="" readonly="readonly"/>
                <input type="button" name="archivo-1" id="archivo-1" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-1" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=com&archivo=archivo-1" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 2:</td>
            <td>
                <input type="text" name="archivo2" id="archivo2" value="" readonly="readonly"/>
                <input type="button" name="archivo-2" id="archivo-2" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-2" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=com&archivo=archivo-2" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 3:</td>
            <td>
                <input type="text" name="archivo3" id="archivo3" value="" readonly="readonly"/>
                <input type="button" name="archivo-3" id="archivo-3" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-3" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=com&archivo=archivo-3" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Foto 4:</td>
            <td>
                <input type="text" name="archivo4" id="archivo4" value="" readonly="readonly"/>
                <input type="button" name="archivo-4" id="archivo-4" class="upload" value=".." />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divarchivo-4" style="display:none;">
                <iframe src="../../formUploadImagen.php?req=com&archivo=archivo-4" width="100%" height="100%" style="border: 0px;"></iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">URA:</td>
            <td>
                <select name="mdf" id="mdf">
                    <?php 
                    foreach ( $arrMdfs as $mdf ) {
                    ?>
                    <option value="<?php echo $mdf['mdf']?>"><?php echo $mdf['mdf'] . ' - ' . $mdf['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Direcci&oacute;n:</td>
            <td>
                <input type="text" name="direccion" id="direccion" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Competencia:</td>
            <td>
                <select name="competencia" id="competencia">
                    <option value="">Seleccione</option>
                    <?php 
                    foreach ( $arrCompetencia as $competencia ) {
                    ?>
                    <option value="<?php echo $competencia['iddescriptor']?>"><?php echo $competencia['nombre']?></option>
                    <?php
                    }
                    ?>
                </select> (*)
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Otro:</td>
            <td>
                <input type="text" name="competencia_otro" id="competencia_otro" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Elemento Encontrado:</td>
            <td>
                <select name="elementoencontrado" id="elementoencontrado">
                    <?php 
                    foreach ( $arrElementoEncontrado as $elementoEncontrado ) {
                        $selectedElem = "";
                        //42 = Ninguno - Elemento Encontrado
                        if ( $elementoEncontrado['iddescriptor'] == 42 ) {
                            $selectedElem = "selected";
                        }
                        ?>
                        <option value="<?php echo $elementoEncontrado['iddescriptor']?>" <?php echo $selectedElem?>><?php echo $elementoEncontrado['nombre']?></option>
                    <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Otro:</td>
            <td>
                <input type="text" name="elementoencontrado_otro" id="elementoencontrado_otro" value="" />
            </td>
        </tr>
        <tr>
            <td class="compSubTitulo">Observaci&oacute;n:</td>
            <td>
                <textarea name="obs" id="obs" style="width:200px;height:50px;"></textarea>
            </td>
        </tr>
        </table>
        
        <table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
        <tr>
            <td colspan="2" height="20">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="margin: 5px 0; display: none;" id="msg">
                    <img alt="" src="../../img/loading.gif"> <b>Grabando.</b>
                </div>
                            
                <input type="button" name="GrabarCompetencia" id="GrabarCompetencia" class="submit" value="Grabar Competencia" />
                <input type="button" id="Cancelar" class="submit" value="Cancelar" />
            </td>
        </tr>
        </table>

        <?php
        }
        ?>
        
    </td>
</tr>
</table>


<div id="divMsgFormCapa" style="margin: 10px 0;"></div>


</body>
</html>