<script type="text/javascript">
var num_fotos = 0;
$(document).ready(function() {
    //directorio donde se almacena las fotos de los piratas
    var abv = 'dad';

    $("#divCerrarUploadImagen").click(function() {
    	$("#divUploadImagen").hide();
    });
	
    $(".upload").click(function() {                
        if( images.length > 3 ) {
            alert("Solo puede adjuntar 4 fotos como maximo");
            return false;
        }
        if( num_fotos > 3 ) {
            alert("Solo puede adjuntar 4 fotos como maximo");
            return false;
        }
        
    	$("#divUploadImagen").show();
        var archivo = $(this).attr('id');
        $("#divFormUploadImagen").html('<iframe src="formUploadImagen.php?req=' + abv + '&archivo=' + archivo + '" width="100%" height="100%" style="border: 0px;"></iframe>');
	});
});
var images = <?php echo json_encode($dataFotos);?>;
</script>
<div id="content_pirata_fotos">
	<div id="cont_actu_detalle" style="margin: 0 0 15px 0;">
		<table id="tbl_detalle" class="tb_detalle_actu" width="100%">
		<tbody>
		<tr>
			<td class="da_titulo_gest" colspan="6">DATOS DEL PIRATA</td>
		</tr>
		<tr style="text-align: left;">
            <td width="25%" class="da_subtitulo">DIRECCI&Oacute;N ANTENA</td>
            <td width="75%" colspan="3"><?php echo strtoupper($arrObjDuna[0]->__get('_direccion'))?></td>
        </tr>
        <tr style="text-align: left;">
            <td width="25%" class="da_subtitulo">TIPO ANTENA</td>
            <td width="25%" class="da_import"><?php echo strtoupper($arrObjDuna[0]->__get('_tipo'))?></td>
            <td width="25%" class="da_subtitulo">FECHA DETECCI&Oacute;N</td>
            <td width="25%"><?php echo obtenerFecha($arrObjDuna[0]->__get('_fechaInsert'))?></td>
        </tr>
		</table>
	</div>	
	<?php
    if ($arrObjDuna[0]->__get('_asociado') != 'S') {
    ?>
	<div id="cont_fotos_pirata_adjuntar">
		<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%" style="text-align:left;">
		<tr>
			<td width="100">Adjuntar Foto:</td>
			<td>
				<div style="float:left">
					<input type="text" name="file1" id="file1" value="" readonly="readonly"/>
					<input type="button" name="file-1" id="file-1" class="upload" value=".." />
				</div>
				<div id="addFotoPirata" style="float:left;margin-left:10px;">
					<img src="img/plus_16.png">
					<a href="javascript:void(0)" 
					    onclick="agregarFotoPirata('<?php echo $arrObjDuna[0]->__get('_idDuna')?>')">Agregar foto
					</a>
				</div>
				<div style="display: none; margin:5px 0 0 20px; float:left;" id="msg">
					<img alt="" src="img/loading.gif"> <b>Cargando foto</b>
				</div>
			</td>
		</tr>
		</table>
	</div>
	<?php
    }
    ?>
	
	<div id="divUploadImagen" style="display:none; margin: 10px 0 0 0;">
		<div id="divCerrarUploadImagen" style="width:400px; border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;border-right:1px solid #CCCCCC; text-align:right;"><a href="javascript:void(0);">[X]</a></div>
		<div id="divFormUploadImagen" style="width:400px; border:1px solid #CCCCCC;">			
		</div>
	</div>
	
	<div id="pirata_icos">
        <?php
        if (!empty($dataFotos)) {
            $it = 0;
            foreach ($dataFotos as $foto) {
        ?>
        <span>
            <img id="<?php echo $it?>" width="75" src="pages/imagenComponente.php?imgcomp=<?php echo $foto['image']?>&dircomp=dad&w=75"/>
            <?php
                if ($arrObjDuna[0]->__get('_asociado') != 'S') {
            ?>
            <p>
                <a href="javascript:void(0)" onclick="eliminarFotoPirata('<?php echo $arrObjDuna[0]->__get('_idDuna')?>','<?php echo $foto['campo']?>');">
                    <img width="12" height="12" src="img/delete_offline.png">
                </a>
                <a href="javascript:void(0)" onclick="eliminarFotoPirata('<?php echo $arrObjDuna[0]->__get('_idDuna')?>','<?php echo $foto['campo']?>');">Eliminar</a>
            </p>
            <?php
                }
            ?>
        </span>
        <?php
                $it++;
            }
        } else {
        ?>
        <p style="margin: 40px 0;"><b>No existe ninguna foto para este pirata.</b><br />
        Agregue fotos.</p>
        <?php
        }
        ?>
	</div>
</div>