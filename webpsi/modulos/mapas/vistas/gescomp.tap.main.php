<?php
require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';



$array_zonal = $_SESSION['USUARIO_ZONAL'];


if( isset($_GET['action']) && $_GET['action'] == 'save' ) {

    $obj_cms = new data_ffttCMS();
    
	$array_taps = $obj_cms->__TapXnodoCatv_troba_Amplificador($conexion, $_REQUEST['IDnodo'], $_REQUEST['IDtroba'], $_REQUEST['IDamplificador']);
	
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    
    $taps_selected_arr = array();
    foreach( $_POST['xy'] as $pktrm => $valtrm ) {
    	if( $valtrm['x'] != '' || $valtrm['y'] != '' ) {
    	
    		$taps_selected_arr[$pktrm] = array(
    			'x' => $valtrm['x'],
    			'y' => $valtrm['y']
    		);
    	}
    }
    
    //update solo a los que surgieron cambios para fftt_cms
    $flag = 1;
	foreach( $array_taps as $terminal ) {
		if( array_key_exists($terminal['tap'], $taps_selected_arr) ) {
			if( $terminal['x'] != $taps_selected_arr[$terminal['tap']]['x'] ||
				$terminal['y'] != $taps_selected_arr[$terminal['tap']]['y'] ) {
			
				//grabando los xy en fftt_cms
    			$result = $obj_cms->updateXyTap($conexion, $_REQUEST['IDnodo'], $_REQUEST['IDtroba'], $_REQUEST['IDamplificador'], $terminal['tap'], $taps_selected_arr[$terminal['tap']]['x'], $taps_selected_arr[$terminal['tap']]['y']);
				if( !$result ) {
					$flag = 0;
					break;
				}
			}
		}
	}
	$data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarNodo' ) {

	$obj_UsuarioNodo = new data_UsuarioNodo();

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$array_nodo = $obj_UsuarioNodo->__listarNodos_x_Usuario_Zonal($conexion, $_POST['IDzon'], $idusuario);
	
	$data_arr = array();
	foreach ($array_nodo as $nodo) {
    	$data_arr[] = array(
    		'IDnodo' 	=> $nodo['nodo'],
    		'Descnodo'	=> $nodo['desc_nodo']
    	);
	}
	echo json_encode($data_arr);
	//echo $data_result;
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarTroba' ) {

	$obj_cms = new data_ffttCMS();
	$troba_arr = $obj_cms->__TrobasXnodoCatv($conexion, $_POST['IDnodo']);
		
	$data_arr = array();
	foreach($troba_arr as $troba) {
		$data_arr[] = array(
    		'troba' 	=> $troba['troba']
    	);
	}

	echo json_encode($data_arr);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarAmplificador' ) {

	$obj_cms = new data_ffttCMS();
	$amplificador_arr = $obj_cms->__AmplificadorXnodoCatv_troba($conexion, $_POST['IDnodo'], $_POST['IDtroba']);
		
	$data_arr = array();
	foreach($amplificador_arr as $amplificador) {
		$data_arr[] = array(
    		'amp' 	=> $amplificador['amp']
    	);
	}

	echo json_encode($data_arr);
	exit;
}


elseif( isset($_POST['action']) && $_POST['action'] == 'buscarTaps' ) {

	$obj_zonal = new data_Zonal();
	$obj_cms = new data_ffttCMS();
	
	//carga de datos (x,y) de Zonal
	$array_zonal = $obj_zonal->__list($conexion,'',$_POST['IDzonal']);
	$data_zonal = array(
		'x' => $array_zonal[0]->__get('x'),
		'y' => $array_zonal[0]->__get('y')
	);
	
	/*
	//carga de datos (x,y) de Mdf
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$array_mdf = $obj_cms->get_mdf_by_codmdf($conexion, $_POST['IDmdf']);
	$data_mdf = array(
		'x' => $array_mdf[0]['x'],
		'y' => $array_mdf[0]['y']
	);
	*/
	
	$array_taps = $obj_cms->__TapXnodoCatv_troba_Amplificador($conexion, $_POST['IDnodo'], $_POST['IDtroba'], $_POST['IDamplificador'], $_POST['datosXY']);

	$data_markers = array();
	$data_markers_con_xy = array();
	$i = 1;
	foreach ($array_taps as $taps) {
		if( $taps['y'] != '' && $taps['x'] != '' ) {
			$data_markers_con_xy[] = $taps;
		}

		$data_markers[] = $taps;
		$i++;
	}
	
	$estados_terminales_arr = array(
		array('id'=>'1', 'val'=>'Estado 1'),
		array('id'=>'2', 'val'=>'Estado 2'),
		array('id'=>'3', 'val'=>'Estado 3')
	);
	$estados__arr = array(
		'1' => 'Estado 1',
		'2' => 'Estado 2',
		'3' => 'Estado 3'
	);
	
	$data_result = array(
		'zonal' 			=> $data_zonal,
		'estados'			=> $estados_terminales_arr,
		'est'				=> $estados__arr,
		'edificios'			=> $data_markers,
		'edificios_con_xy'  => $data_markers_con_xy
	);
	
	echo json_encode($data_result);
    exit;
}


include 'principal/gescomp.tap.php';    

?>

