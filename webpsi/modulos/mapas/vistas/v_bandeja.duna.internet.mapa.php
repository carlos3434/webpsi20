<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=es"></script>
<script type="text/javascript" src="../../../js/jquery/jquery.js"></script>
<style type="text/css">
body {
	font-family: Arial;
	font-size: 11px;
}

.latlon {
	font-size: 11px;
}

.subtit_info {
	font-weight: bold;
	width: 80px;
}

.det_info {
	color: #666666;
	width: 80px;
}
</style>
<script type="text/javascript">
var map = null;
$(document).ready(function () {    
    var M = {
        initialize: function() {
            if (typeof (google) == "undefined") {
                alert('Verifique su conexion a maps.google.com');
                return false;
            }
            var latlng = new google.maps.LatLng(0,0);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_pirata"), myOptions);
            M.cargaMapaPirata(<?php echo $_REQUEST['idComponente'] ?>);
        },        
        cargaMapaPirata: function(idcomponente) {          
            data_content = {
                'idcomponente' : idcomponente
            };
            $.ajax({
                type:   "POST",
                url:    "../../../modulos/maps/bandeja.duna.internet.php?cmd=CargarMapaPirata",
                data:   data_content,
                dataType: "json",
                success: function(data) {
                    var datos = data['marker'];
                    var detalle = '<div style="width:440px;">';
                        detalle += '<div>';
                        detalle += '<table width="100%">';
                        detalle += '<tr><td class="subtit_info" style="font-size:14px; color: #1155CC;" colspan="4">DETALLE DEL PIRATA:</td></tr>';
                        detalle += '<tr><td class="subtit_info">CODIGO:</td><td class="det_info" colspan="3">' + datos['codigo'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">DEPARTAMENTO:</td><td class="det_info">' + datos['departamento'] + '</td><td class="subtit_info">PROVINCIA:</td><td class="det_info">' + datos['provincia'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">CIUDAD:</td><td class="det_info">' + datos['ciudad'] + '</td><td class="subtit_info">DISTRITO:</td><td class="det_info">' + datos['distrito'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">TEL.REF:</td><td class="det_info">' + datos['telefono_referencial'] + '</td><td class="subtit_info">TEL.DETECTADO:</td><td class="det_info">' + datos['telefono_detectado'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">DIRECCION:</td><td class="det_info" colspan="3">' + datos['direccion'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">DIRECCION REF.:</td><td class="det_info" colspan="3">' + datos['direccion_referencial'] + '</td></tr>';
                        detalle += '<tr><td class="subtit_info">FFTT:</td><td class="det_info" colspan="3">' + datos['fftt'] + '</td></tr>';
                        detalle += '</table>';
                        detalle += '</div>';
                        detalle += '<div style="font-size: 12px;margin: 5px 0 0 0;"><span style="color:#777777;">X,Y:</span> <span style="color:#1155CC;">' + datos['x'] + ',' + datos['y'] + '</span></div>';
                        detalle += '</div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: detalle
                    });

                    var latlng = new google.maps.LatLng(datos['y'],datos['x']);
                    var marker = new google.maps.Marker({
                        position: latlng, 
                        map: map, 
                        title: datos['direccion']
                    });  

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });
                    map.setCenter(latlng);
                    map.setZoom(16);                   
                }                
            });
        }
    };
    M.initialize();              
});
</script>
</head>
<body>
<div id="map_pirata" style="width: 100%; height: 340px; background-color: #FFFFFF;"></div>
</body>
</html>