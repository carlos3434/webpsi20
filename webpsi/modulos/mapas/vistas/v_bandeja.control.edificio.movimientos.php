<div style="max-height: 450px; overflow: auto;">
    <table width="100%" class="tb_detalle_actu">
    <tr>
        <td class="da_titulo_gest" colspan="10">HISTORIAL DE MOVIMIENTOS</td>
    </tr>
    <tr style="text-align: left;">        
        <td class="da_subtitulo" width="30" align="center">#</td>
        <td class="da_subtitulo" width="20">&nbsp;</td>
        <td class="da_subtitulo" width="90">FECHA MOV.</td>
        <td class="da_subtitulo">USUARIO MOV.</td>
        <td class="da_subtitulo">FECHA SEGUIMIENTO</td>
        <td class="da_subtitulo">ESTADO</td>
        <td class="da_subtitulo">% AVANCE</td>
        <td class="da_subtitulo" width="90">FECHA TERMINO CONSTRUC.</td>
        <td class="da_subtitulo">PERSONA CONTACTO</td>
        <td class="da_subtitulo">OBSERVACIONES</td>
    </tr>
    <?php 
    if ( !empty($arrObjEdificioMov) ) {
    $i = 1;
    foreach ( $arrObjEdificioMov as $objEdificioMov ) {
        $imgMov = 'application_add.png';
        if ( $objEdificioMov->__get('_actualizacion') == '2' ) {
            $imgMov = 'mobile_phone.png';
        }
    ?>
    <tr style="text-align: left; ">        
        <td>
            <table border="0">
                <tr>
                    <td width="10px"><?php echo $i?></td>
                    <td width="20px">
                        <a id="btn_mas_<?php echo $objEdificioMov->__get('_idEdificioMovimiento')?>" href="javascript:masMovimiento('<?php echo $objEdificioMov->__get('_idEdificioMovimiento')?>');" title"Ver mas informacion">[+]</a>
                        <a style="display:none;" id="btn_menos_<?php echo $objEdificioMov->__get('_idEdificioMovimiento')?>" href="javascript:masMovimientoCerrar('<?php echo $objEdificioMov->__get('_idEdificioMovimiento')?>');" title"Ver mas informacion">[-]</a>
                    </td>
                </tr>
            </table>
        </td>
        <td align="center"><img src="img/<?php echo $imgMov?>" width="14" height="14" alt="" title="" /></td>
        <td><?php echo date('d/m/Y H:i', strtotime($objEdificioMov->__get('_fechaInsert')))?></td>
        <td><?php echo $objEdificioMov->__get('_apPat') . ' ' . $objEdificioMov->__get('_apMat') . ', ' . $objEdificioMov->__get('_nombres')?></td>
        <td>
        <?php
            if ( $objEdificioMov->__get('_fechaSeguimiento') != '0000-00-00' && $objEdificioMov->__get('_fechaSeguimiento') != '' ) {
                echo date('d/m/Y', strtotime($objEdificioMov->__get('_fechaSeguimiento'))); 
            } else {
                echo '';    
            }
        ?>
        </td>
        <td><?php echo $objEdificioMov->__get('_desEstado')?></td>
        <td><?php echo $objEdificioMov->__get('_avance')?></td>
        <td>
        <?php 
        if ( $objEdificioMov->__get('_fechaTerminoConstruccion') != '0000-00-00' && $objEdificioMov->__get('_fechaTerminoConstruccion') != '' ) {
            echo date('d/m/Y', strtotime($objEdificioMov->__get('_fechaTerminoConstruccion'))); 
        } else {
            echo '';    
        }
        ?>
        </td>
        <td><?php echo $objEdificioMov->__get('_personaContacto')?></td>
        <td><?php echo $objEdificioMov->__get('_observacion')?></td>
    </tr>
    <tr>
        <td colspan="10">
            <div id="info_mov_<?php echo $objEdificioMov->__get('_idEdificioMovimiento')?>" style="background-color: #F2F2F2; display: none; text-align: left">
                <span style="font-weight: bold">Direcci&oacute;n obra: </span><?php echo $objEdificioMov->__get('_direccionObra')?><br />
                <span style="font-weight: bold">N&uacute;mero: </span><?php echo $objEdificioMov->__get('_numero')?><br />
                <span style="font-weight: bold">Manzana: </span><?php echo $objEdificioMov->__get('_mza')?><br />
                <span style="font-weight: bold">Lote: </span><?php echo $objEdificioMov->__get('_lote')?><br /><br />
                <span style="font-weight: bold">Fecha Tratam. Call: </span>
                <?php 
                if ( $objEdificioMov->__get('_fechaTratamientoCall') != '0000-00-00' && $objEdificioMov->__get('_fechaTratamientoCall') != '' ) {
                    echo date('d/m/Y', strtotime($objEdificioMov->__get('_fechaTratamientoCall'))); 
                } else {
                    echo '';    
                }
                ?>
                <br />
                <span style="font-weight: bold">Nro. de Blocks: </span><?php echo $objEdificioMov->__get('_nroBlocks')?><br />
                <span style="font-weight: bold">Nro. de Pisos: </span><?php echo $objEdificioMov->__get('_nroPisos')?><br />
                <span style="font-weight: bold">Nro. de Dptos: </span><?php echo $objEdificioMov->__get('_nroDptos')?><br />
            </div>
        </td>
    </tr>
    <?php 
    $i++;
    }
    } else {
    ?>
    <tr>
        <td colspan="9" style="padding:20px;">Ningun registro de movimiento encontrado para este edificio.</td>
    </tr>
    <?php 
    }
    ?>
    </table>
</div>
    
<div>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td width="50%" align="left">
                <a style="text-decoration: none;" href="javascript:cerrarMovimientosEdificio()">
                <img src="img/btn_return_mov.png" width="100" height="26" alt="Regresar al detalle" title="Regresar al detalle" />
                </a>
            </td>
            <td width="50%" align="right">
                <strong>LEYENDA:</strong>
                <img src="img/application_add.png" width="14" height="14" alt="Web" title="Web"/> Web
                <img src="img/mobile_phone.png" width="14" height="14" alt="Movil" title="Movil"> Movil
            </td>
        </tr>
    </table>
</div>