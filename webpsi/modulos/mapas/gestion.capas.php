<?php

/**
 * @package     /modulos/maps
 * @name        gestion.capas.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/12/19
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function bandejaMain()
{
    $totalRegistros = 0;
    include 'vistas/v_gestion.capas.php';
}

function filtroBusqueda($pagina='', $total='', $campoValor='', $orderField='', $orderType='')
{
    try {
        global $conexion;

        $objCapa = new Data_Capa();

        $arrFiltro = array();
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $pagina = 1;
        $arrObjCapa = $objCapa->listarCapas($conexion, $pagina, $arrFiltro);

        $totalRegistros = $objCapa->totalCapas($conexion, $arrFiltro);

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_gestion.capas.paginado.php';

        $cadena = '<script type="text/javascript">
                $("#cont_num_resultados_head").html(' . $totalRegistros . ');
                $("#cont_num_resultados_foot").html(' . $totalRegistros . ');
                $("#pag_actual").attr("value", "1");
                $("#tl_registros").attr("value", "' . $totalRegistros . '");
                $("#paginacion").empty();
                ';
                if ($totalRegistros > 0) {
                    $cadena .= '
                    $(function() {
                        $("#paginacion").paginate({
                            count                   : '.$numPaginas.',
                            start                   : 1,
                            display                 : 10,
                            border                  : true,
                            border_color            : "#FFFFFF",
                            text_color              : "#FFFFFF",
                            background_color        : "#0080AF",
                            border_hover_color      : "#CCCCCC",
                            text_hover_color        : "#000000",
                            background_hover_color  : "#FFFFFF",
                            images                  : false,
                            mouse                   : "press",
                            onChange : function(page){
                                loader("start");
                                $("#pag_actual").attr("value", page);
                                var tl=$("#tl_registros").attr("value");
                                $.post("modulos/maps/gestion.capas.php?cmd=cambiarPagina", { 
                                                pagina: page, total: tl, campo_valor: "' . $campoValor . '", order_field: order_field, order_type: order_type   
                                        }, function(data){
                                                $("#tb_resultado tbody").empty();
                                                $("#tb_resultado tbody").append(data);
                                                loader("end");
                                        }
                                );
                            }
                        });
                    });';
                }
            $cadena .= '</script>';
        echo $cadena;
        
    } catch(PDOException $e) {
    }
}

function cambiarPagina($pagina='', $total='', $campoValor='', $orderField='', $orderType='')
{
    try {
        global $conexion;

        if ($pagina == '') $pagina = 1;

        $objCapa = new Data_Capa();


        $arrFiltro = array();
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $arrObjCapa = $objCapa->listarCapas($conexion, $pagina, $arrFiltro);

        $totalRegistros = $objCapa->totalCapas($conexion, $arrFiltro);

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_gestion.capas.paginado.php';
        
    } catch(PDOException $e) {
    }
}


//Editar informacion de la capa
function editarCapa($idcapa)
{
    try {
        global $conexion;

        $objCapa           = new Data_Capa();
        $objCapaDetalle   = new Data_CapaDetalle();

        $arrObjCapa = $objCapa->obtenerCapa($conexion, $idcapa);
        
        $arrObjCapaDetalle = $objCapaDetalle->obtenerCamposCapa($conexion, $idcapa);
        
        $arrayCampos = array();
        foreach ( $arrObjCapaDetalle as $objCapaDetalle ) {
            $arrayCampos[$objCapaDetalle->__get('_campoNro')] = $objCapaDetalle->__get('_campo');
        }

        include 'vistas/v_gestion.capas.editar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
    
}

//Agregar nueva capa
function agregarCapa()
{
    try {
        global $conexion;

        include 'vistas/v_gestion.capas.agregar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
}

function buscarAbvCapa()
{
    try {
        global $conexion;
        
        $objCapa = new Data_Capa();

        $arrObjCapa = $objCapa->obtenerCapaPorAbv($conexion, $_POST['abv_capa']);

        $result = ( !empty($arrObjCapa) ) ? 
            array('flag'=>'0','msg'=>'ABREVIATURA DE CAPA YA EXISTE.') : 
            array('flag'=>'1','msg'=>'OK');

        echo json_encode($result);
        exit;
        
    } catch (PDOException $e) {
        $dataResult = array('flag'=>'0','mensaje'=>'Hubo un error en el Sistema, intentelo nuevamente.', 'tipo'=>'');
        echo json_encode($dataResult);
        exit;
    }
}

function updateDatosCapa($_POST)
{
    try {
        global $conexion;
        
        $idUsuario       = $_SESSION['USUARIO']->__get('_idUsuario');
        $objCapa         = new Data_Capa();
        $objCapaDetalle  = new Data_CapaDetalle();
        
        
        //capturo los campos ingresados a traves del formulario
        $data = array();
        foreach ($_POST as $idcampo => $valcampo) { 
            if ( $valcampo != '' && (substr($idcampo, 0, 5) == 'campo') ) {
                $data[substr($idcampo, 5)] = $valcampo;
            }
        }
    
        //todos los campos de la capa
        $arrObjCapaDetalle = $objCapaDetalle->obtenerCamposCapa($conexion, $_POST['idcapa']);
    
        $arrayCampos = array();
        foreach ( $arrObjCapaDetalle as $objCapaDetalle ) {
            $arrayCampos[$objCapaDetalle->__get('_campoNro')] = $objCapaDetalle->__get('_campo');
        }
        
        //veo cuales campos ya estan y cuales son nuevos, para una capa
        $arrayCamposIns = array();
        $arrayCamposUpd = array();
        foreach ( $data as $idcampo => $valcampo ) {
            if ( array_key_exists($idcampo, $arrayCampos) ) {
                if ( trim($valcampo) != trim($arrayCampos[$idcampo]) ) {
                    $arrayCamposUpd[$idcampo] = $valcampo;
                }
            } else {
                $arrayCamposIns[$idcampo] = $valcampo;
            }
        }
        
        
        //actualizando datos en capa
        $objCapa->updateCapa($conexion, $_POST, $idUsuario);
        
        
        //actualiza el nombre de campo en tabla fftt_capa_detalle
        foreach ( $arrayCamposUpd as $idcampo => $valcampo ) {
            $objCapaDetalle->updateCapaDetalle($conexion, $_POST['idcapa'], $idcampo, $valcampo, $idUsuario);
        }
    
        //inserta un nuevo campo en tabla fftt_capa_detalle
        foreach ( $arrayCamposIns as $idcampo => $valcampo ) {
            $objCapaDetalle->insertCapaDetalle($conexion, $_POST['idcapa'], $idcampo, $valcampo, $idUsuario);
        }
        
        
        $dataResult = array('flag'=>'1','mensaje'=>'Datos grabados correctamente.','tipo'=>'');
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al actualizar la capa, intentelo nuevamente.', 
            'tipo'=>''
        );
        echo json_encode($dataResult);
        exit;
    }
    
}

function insertDatosCapa($_POST)
{
    try {
        global $conexion;
        
        $idUsuario        = $_SESSION['USUARIO']->__get('_idUsuario');
        $objCapa          = new Data_Capa();
        $objCapaDetalle   = new Data_CapaDetalle();
        
    
        //capturo los campos ingresados a traves del formulario
        $data = array();
        foreach ($_POST as $idcampo => $valcampo) { 
            if ( $valcampo != '' && (substr($idcampo, 0, 5) == 'campo') ) {
                $data[substr($idcampo, 5)] = $valcampo;
            }
        }
    
        //insert de tabla fftt_capa, con upload de imagen
        $lastID = $objCapa->insertCapa($conexion, $_POST, $idUsuario);
        
        //insert tabla fftt_capa_detalle
        foreach ( $data as $idcampo => $valcampo ) {
            $objCapaDetalle->insertCapaDetalle($conexion, $lastID, $idcampo, $valcampo, $idUsuario);
        }
        
        //crear el directorio para la capa
        mkdir(FFTT . trim($_POST['abv_capa']));

        
        $dataResult = array('flag'=>'1','mensaje'=>'Datos grabados correctamente.','tipo'=>'');
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al actualizar la capa, intentelo nuevamente.',
            'tipo'=>''
        );
        echo json_encode($dataResult);
        exit;
    }
    
}



if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'buscarAbvCapa':
        buscarAbvCapa($_POST['abv_capa']);
        break;
    case 'insertDatosCapa':
        insertDatosCapa($_POST);
        break;
    case 'updateDatosCapa':
        updateDatosCapa($_POST);
        break;
    case 'agregarCapa':
        agregarCapa();
        break;
    case 'editarCapa':
        editarCapa($_POST['idcapa']);
        break;
    case 'cambiarPagina':
        cambiarPagina(
            $_POST['pagina'], $_POST['total'], $_POST['campo_valor'], 
            $_POST['order_field'], $_POST['order_type']
        );
        break;
    case 'filtroBusqueda':
        filtroBusqueda(
            $_POST['pagina'], $_POST['total'], $_POST['campo_valor'], 
            $_POST['order_field'], $_POST['order_type']
        );
        break;
    default:
        bandejaMain();
        break;
}