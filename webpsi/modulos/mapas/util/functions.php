<?php

/**
 * @package     modulos/maps/util/
 * @name        functions.php
 * @category	Function
 * @author	    Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright	2011 Telefonica del Peru
 * @version     1.0 - 2011/10/14
 */

require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function defaultFunctions()
{
}

function provinciasUbigeo($codDpto)
{
    global $conexion;
    
    $objUbigeo = new Data_Ubigeo();

    $arrObjUbigeo = $objUbigeo->provinciasUbigeo($conexion, $codDpto);

    $arrUbigeo = array();
    foreach ($arrObjUbigeo as $objUbigeo) {
        $arrUbigeo[] = array(
            'codProv' 	=> $objUbigeo->__get('_codProv'),
            'nombre'	=> $objUbigeo->__get('_nombre') 
        );
    }
    echo json_encode($arrUbigeo);
    exit;

}

function distritosUbigeo($codDpto, $codProv)
{
    global $conexion;
    
    $objUbigeo = new Data_Ubigeo();

    $arrObjUbigeo = $objUbigeo->distritosUbigeo($conexion, $codDpto, $codProv);

    $arrUbigeo = array();
    foreach ($arrObjUbigeo as $objUbigeo) {
        $arrUbigeo[] = array(
            'codDist' 	=> $objUbigeo->__get('_codDist'),
            'nombre'	=> $objUbigeo->__get('_nombre') 
        );
    }
    echo json_encode($arrUbigeo);
    exit;

}

function localidadesUbigeo($codDpto, $codProv, $codDist)
{
    global $conexion;

    $objUbigeo = new Data_Ubigeo();

    $arrObjUbigeo = $objUbigeo->buscarLocalidad($conexion, $codDpto, $codProv, $codDist);

    $arrUbigeo = array();
    foreach ($arrObjUbigeo as $objUbigeo) {
        $arrUbigeo[] = array(
            'codLocal' 	=> $objUbigeo->__get('_ubigeo'),
            'nombre'	=> $objUbigeo->__get('_localidad')
        );
    }
    echo json_encode($arrUbigeo);
    exit;

}

function obtenerMdfsZonalUsuario($zonal)
{
    global $conexion;
    $objTerminales = new Data_FfttTerminales();

    $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByZonalAndIdusuario($conexion, $zonal, $idusuario);

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
            'mdf'       => $mdf['mdf'],
            'nombre'    => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
}

function obtenerDepartamentosZonal($zonal)
{
    global $conexion;
    $objUbigeoZonal = new Data_UbigeoZonalEdi();

    $arrObjDepartamento = $objUbigeoZonal->traerDepartamentosZonal($conexion, $zonal);

    $arrData = array();
    foreach ($arrObjDepartamento as $objDepartamento) {
        $arrData[] = array(
            'coddpto'   => $objDepartamento->__get('_codDpto'),
            'nombre'    => $objDepartamento->__get('_nombre')
        );
    }
    echo json_encode($arrData);
    exit;
}

function obtenerProvinciasZonal($zonal, $coddpto)
{
    global $conexion;
    $objUbigeoZonal = new Data_UbigeoZonal();

    $arrObjDepartamento = $objUbigeoZonal->traerProvinciasZonal(
        $conexion, $zonal, $coddpto
    );

    $arrData = array();
    foreach ($arrObjDepartamento as $objDepartamento) {
        $arrData[] = array(
            'codprov'   => $objDepartamento->__get('_codProv'),
            'nombre'    => $objDepartamento->__get('_nombre')
        );
    }
    echo json_encode($arrData);
    exit;
}

function obtenerTipoProyectoPorSegmento($idSegmento)
{
    global $conexion;
    $objTipoProyecto = new Data_TipoProyectoEdi();

    $arrObjTipoProyecto = $objTipoProyecto->listar($conexion, '', $idSegmento);

    $arrData = array();
    foreach ($arrObjTipoProyecto as $objTipoProyecto) {
        $arrData[] = array(
            'codtipo'   => $objTipoProyecto->__get('_idTipoProyecto'),
            'nombre'    => $objTipoProyecto->__get('_desTipoProyecto')
        );
    }
    echo json_encode($arrData);
    exit;
}

function obtenerZonalesPorRegionUsuario($idRegion)
{
    global $conexion;
    $objRegionZonal = new Data_RegionZonalEdi();
    
    $arrRegion[] = $idRegion;
    
    $arrZonal = array();
    foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
        $arrZonal[$objZonal->__get('_abvZonal')] =
        $objZonal->__get('_abvZonal');
    }

    $arrObjZonal = $objRegionZonal->listar($conexion, $arrRegion, $arrZonal);

    $arrDataZonal = array();
    foreach ($arrObjZonal as $objZonal) {
        $arrDataZonal[] = array(
            'zonal'   => $objZonal->__get('_zonal'),
            'nombre'  => $objZonal->__get('_descZonal')
        );
    }
    
    $arrDataResult = array(
        'arrZonal' => $arrDataZonal
    );
    
    echo json_encode($arrDataResult);
    exit;
}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'obtenerZonalesPorRegionUsuario':
        obtenerZonalesPorRegionUsuario($_POST['region']);
        break;
    case 'obtenerTipoProyectoPorSegmento':
        obtenerTipoProyectoPorSegmento($_POST['segmento']);
        break;
    case 'provinciasUbigeo':
        provinciasUbigeo($_POST['coddpto']);
        break;
    case 'distritosUbigeo':
        distritosUbigeo($_POST['coddpto'], $_POST['codprov']);
        break;
    case 'localidadesUbigeo':
        localidadesUbigeo($_POST['coddpto'], $_POST['codprov'], $_POST['coddist']);
        break;
    case 'obtenerMdfsZonalUsuario':
        obtenerMdfsZonalUsuario($_POST['zonal']);
        break;
    case 'obtenerDepartamentosZonal':
        obtenerDepartamentosZonal($_POST['zonal']);
        break;
    case 'obtenerProvinciasZonal':
        obtenerProvinciasZonal($_POST['zonal'], $_POST['coddpto']);
        break;
    default:
        defaultFunctions();
        break;
}