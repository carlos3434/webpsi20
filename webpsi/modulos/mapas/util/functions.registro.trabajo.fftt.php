<?php

/**
 * @package     /modulos/gescom
 * @name        functions.registro.trabajo.fftt.php
 * @category    function
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/12/01
 */

function obtenerElementoRegistroTrabajoFftt( $arrTrabajoPlanta=array() )
{
    $elemento = '';
    
    if ( !empty($arrTrabajoPlanta) ) {
        
        if ($arrTrabajoPlanta['codplantaptr'] == 'B') { //STB/ADSL

            if ( $arrTrabajoPlanta['tipored'] == 'D' ) {
                if ( $arrTrabajoPlanta['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                    $elemento = $arrTrabajoPlanta['mdf'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '19' ) { //Cable Alimentador
                    $elemento = $arrTrabajoPlanta['ccable'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '3' ) { //terminal
                    $elemento = $arrTrabajoPlanta['terminal'];
                }
            } elseif ( $arrTrabajoPlanta['tipored'] == 'F' ) {
                if ( $arrTrabajoPlanta['codelementoptr'] == '27' ) { //Mdf //Agregado 12/12/2011
                    $elemento = $arrTrabajoPlanta['mdf'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '19' ) { //Cable Alimentador
                    $elemento = $arrTrabajoPlanta['parpri'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '8' ) { //armario
                    $elemento = $arrTrabajoPlanta['carmario'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '23' ) { //cable secundario
                    $elemento = $arrTrabajoPlanta['cbloque'];
                } elseif ( $arrTrabajoPlanta['codelementoptr'] == '3' ) { //terminal
                    $elemento = $arrTrabajoPlanta['terminal'];
                }
            }

        } elseif ($arrTrabajoPlanta['codplantaptr'] == 'C') { //CATV

            if ( $arrTrabajoPlanta['codelementoptr'] == '28' ) { //Nodo //Agregado 12/12/2011
                $elemento = $arrTrabajoPlanta['mdf'];
            }if ( $arrTrabajoPlanta['codelementoptr'] == '11' ) { //troba
                $elemento = $arrTrabajoPlanta['carmario'];
            } elseif ( $arrTrabajoPlanta['codelementoptr'] == '2' ) { //amplificador
                $elemento = $arrTrabajoPlanta['cbloque'];
            } elseif ( $arrTrabajoPlanta['codelementoptr'] == '1' ) { //tap
                $elemento = $arrTrabajoPlanta['terminal'];
            } elseif ( $arrTrabajoPlanta['codelementoptr'] == '20' ) { //borne
                $elemento = $arrTrabajoPlanta['borne'];
            }
        }
    }
    
    return $elemento;
}