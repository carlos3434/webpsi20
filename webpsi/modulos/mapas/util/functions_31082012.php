<?php

/**
 * @package     modulos/maps/util/
 * @name        functions.php
 * @category	Function
 * @author	    Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright	2011 Telefonica del Peru
 * @version     1.0 - 2011/10/14
 */

require_once "../../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function defaultFunctions()
{
}

function provinciasUbigeo($codDpto)
{
    global $conexion;
    
    $objUbigeo = new Data_Ubigeo();

    $arrObjUbigeo = $objUbigeo->provinciasUbigeo($conexion, $codDpto);

    $arrUbigeo = array();
    foreach ($arrObjUbigeo as $objUbigeo) {
        $arrUbigeo[] = array(
            'codProv' 	=> $objUbigeo->__get('_codProv'),
            'nombre'	=> $objUbigeo->__get('_nombre') 
        );
    }
    echo json_encode($arrUbigeo);
    exit;

}

function distritosUbigeo($codDpto, $codProv)
{
    global $conexion;
    
    $objUbigeo = new Data_Ubigeo();

    $arrObjUbigeo = $objUbigeo->distritosUbigeo($conexion, $codDpto, $codProv);

    $arrUbigeo = array();
    foreach ($arrObjUbigeo as $objUbigeo) {
        $arrUbigeo[] = array(
            'codDist' 	=> $objUbigeo->__get('_codDist'),
            'nombre'	=> $objUbigeo->__get('_nombre') 
        );
    }
    echo json_encode($arrUbigeo);
    exit;

}

function obtenerMdfsZonalUsuario($zonal)
{
    global $conexion;
    $objTerminales = new data_FfttTerminales();

    $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByZonalAndIdusuario($conexion, $zonal, $idusuario);

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
            'mdf'       => $mdf['mdf'],
            'nombre'    => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'provinciasUbigeo':
        provinciasUbigeo($_POST['coddpto']);
        break;
    case 'distritosUbigeo':
        distritosUbigeo($_POST['coddpto'], $_POST['codprov']);
        break;
    case 'obtenerMdfsZonalUsuario':
        obtenerMdfsZonalUsuario($_POST['zonal']);
        break;
    default:
        defaultFunctions();
        break;
}