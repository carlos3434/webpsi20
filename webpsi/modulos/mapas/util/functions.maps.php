<?php

/**
 * @package     /modulos/maps
 * @name        functions.maps.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/05/14
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


//obtengo las regiones a partir de las zonales que tiene asignado el usuario
function listarRegionesPorZonalUsuario()
{
    global $conexion;
    
    $objRegionZonal    = new Data_RegionZonalEdi();

    $arrZonal = array();
    if (!empty($_SESSION['USUARIO_ZONAL'])) {
        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
            $arrZonal[$objZonal->__get('_abvZonal')] =
            $objZonal->__get('_abvZonal');
        }
    }

    //obtengo las regiones a partir de las zonales que tiene asignado el usuario
    $arrObjRegiones = array();
    if (!empty($arrZonal)) {
        $arrObjRegiones = $objRegionZonal->listar(
            $conexion, array(), $arrZonal
        );
    }

    $arrRegion = array();
    if (!empty($arrObjRegiones)) {
        foreach ( $arrObjRegiones as $objRegion ) {
            $arrRegion[$objRegion->__get('_idRegion')] = array(
                'idregion'  => $objRegion->__get('_idRegion'),
                'nomregion' => $objRegion->__get('_nomRegion'),
                'detregion' => $objRegion->__get('_detRegion')
            );
        }
    }

    return $arrRegion;
}
