<?php

/**
 * @package     /modulos/maps
 * @name        bandeja.control.edificio.php
 * @category    Controller
 * @author        Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright    2011 Telefonica del Peru
 * @version     1.0 - 2011/12/19
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


//numero de Item inicio desde donde parte los nuevos edificios
$item = '50000';

$_SESSION['SUBMODULO_ACTUAL'] = 'maps/bandeja.control.edificio.php';


//obtengo las regiones a partir de las zonales que tiene asignado el usuario
function listarRegionesPorZonalUsuario()
{
    global $conexion;
    
    $objRegionZonal    = new Data_RegionZonalEdi();

    $arrZonal = array();
    if (!empty($_SESSION['USUARIO_ZONAL'])) {
        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
            $arrZonal[$objZonal->__get('_abvZonal')] =
            $objZonal->__get('_abvZonal');
        }
    }

    //obtengo las regiones a partir de las zonales que tiene asignado el usuario
    $arrObjRegiones = array();
    if (!empty($arrZonal)) {
        $arrObjRegiones = $objRegionZonal->listar(
            $conexion, array(), $arrZonal
        );
    }

    $arrRegion = array();
    if (!empty($arrObjRegiones)) {
        foreach ( $arrObjRegiones as $objRegion ) {
            $arrRegion[$objRegion->__get('_idRegion')] = array(
                'idregion'  => $objRegion->__get('_idRegion'),
                'nomregion' => $objRegion->__get('_nomRegion'),
                'detregion' => $objRegion->__get('_detRegion')
            );
        }
    }

    return $arrRegion;
}


function obtenerZonalesPorRegionUsuario($idRegion)
{
    $arrZonales = listarZonalesPorRegionUsuario($idRegion);
    
    $arrDataResult = array(
        'arrZonal' => $arrZonales
    );
    
    echo json_encode($arrDataResult);
    exit;
    
}

//obtengo las zonales por region, validando las zonales 
//que tiene asignado el usuario
function listarZonalesPorRegionUsuario($idRegion)
{
    global $conexion;
    $objRegionZonal = new Data_RegionZonalEdi();

    $arrRegion[] = $idRegion;

    $arrZonal = array();
    if (!empty($_SESSION['USUARIO_ZONAL'])) {
        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
            $arrZonal[$objZonal->__get('_abvZonal')] =
            $objZonal->__get('_abvZonal');
        }
    }
    
    $arrObjZonal = array();
    if (!empty($arrZonal)) {
        $arrObjZonal = $objRegionZonal->listar($conexion, $arrRegion, $arrZonal);
    }
    

    $arrDataZonal = array();
    if (!empty($arrObjZonal)) {
        foreach ($arrObjZonal as $objZonal) {
            $arrDataZonal[] = array(
                'zonal'   => $objZonal->__get('_zonal'),
                'nombre'  => $objZonal->__get('_descZonal')
            );
        }
    }
    
    return $arrDataZonal;
}

function bandejaMain()
{
    global $conexion;

    //$objEdificioEstado = new Data_FfttEdificioEstado();
    $objGestionObra = new Data_GestionObraEdi();
    
    //$arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
    //    $conexion
    //);
    
    $arrObjGestionObra = $objGestionObra->listar($conexion);
    
    //obtengo las regiones a partir de las zonales que tiene asignado el usuario
    $arrRegion = listarRegionesPorZonalUsuario();
    
    $totalRegistros = 0;

    include 'vistas/v_bandeja.control.edificio.php';
}

function filtroBusqueda($pagina='', $total='', $gestionObra='', $empresa='', 
    $region='', $zonal='', $mdf='', $campoValor='', $orderField='', 
    $orderType='', $departamento='', $provincia='', $distrito='')
{
    try {
        global $conexion;
        
        //validando que tenga asignado zonales y mdfs
        if (empty($_SESSION['USUARIO_ZONAL'])) {
            $mensaje = '<tr><td style="height:30px" colspan="15">' .
                'Ningun registro encontrado con los criterios de ' .
                'b&uacute;squeda ingresados</td></tr>';
            echo $mensaje;
            exit;
        }
        

        $objEdificio       = new Data_FfttEdificio();
        $objEdificioEstado = new Data_FfttEdificioEstado();
        $objTerminales     = new Data_FfttTerminales();
        
        $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );

        $arrFiltro = array();
        if ( $gestionObra != '' ) {
            $arrFiltro['gestionObra'] = $gestionObra;
        }
        
        if ( $empresa != '' ) {
            $arrFiltro['empresa'] = $empresa;
        }
        
        if ( $region != '' ) {
            $arrFiltro['region'] = array($region);
        } else {
            
            //obtengo las regiones a partir de las zonales que tiene asignado 
            //el usuario
            $arrRegion = listarRegionesPorZonalUsuario();
            
            $arrDataRegion = array();
            if (!empty($arrRegion)) {
                foreach ($arrRegion as $region) {
                    $arrDataRegion[] = $region['idregion'];
                }
            }
            $arrFiltro['region'] = $arrDataRegion;
        }
        
        if ( $zonal != '' ) {
            $arrFiltro['zonal'] = array($zonal);
        } else {
            $arrData = array();
            if (!empty($_SESSION['USUARIO_ZONAL'])) {
                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                    $arrData[] = $objZonal->__get('_abvZonal');
                }
            }
            $arrFiltro['zonal'] = $arrData;
        }
        
        if ( $mdf != '' ) {
            $arrFiltro['mdf'] = array($mdf);
        } else {
            $arrMdf = array();
            if ( $zonal == '' ) {
                if (!empty($_SESSION['USUARIO_MDF'])) {
                    foreach ($_SESSION['USUARIO_MDF'] as $abvMdf) {
                        $arrMdf[]['mdf'] = $abvMdf;
                    }
                }
                
            } else {
                $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                    $conexion, $zonal, $idusuario
                );
            }
            
            $arrData = array();
            if (!empty($arrMdf)) {
                foreach ($arrMdf as $abvMdf) {
                    $arrData[] = $abvMdf['mdf'];
                }
            }
            $arrFiltro['mdf'] = $arrData;
        }
        
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }
        //Ubigeo
        if ( $departamento != '' ) {
            $arrFiltro['departamento'] = $departamento;
        }
        if ( $provincia != '' ) {
            $arrFiltro['provincia'] = $provincia;
        }
        if ( $distrito != '' ) {
            $arrFiltro['distrito'] = $distrito;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $pagina = 1;
        $arrObjEdificio = $objEdificio->listarEdificios(
            $conexion, $pagina, '', $arrFiltro
        );

        $totalRegistros = $objEdificio->totalEdificios(
            $conexion, '', $arrFiltro
        );

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_bandeja.control.edificio.paginado.php';

        $cadena = '<script type="text/javascript">
                $("#cont_num_resultados_head").html(' . $totalRegistros . ');
                $("#cont_num_resultados_foot").html(' . $totalRegistros . ');
                $("#pag_actual").attr("value", "1");
                $("#tl_registros").attr("value", "' . $totalRegistros . '");
                $("#paginacion").empty();
                ';
                if ($totalRegistros > 0) {
                    $cadena .= '
                    $(function() {
                        $("#paginacion").paginate({
                            count                   : '.$numPaginas.',
                            start                   : 1,
                            display                 : 10,
                            border                  : true,
                            border_color            : "#FFFFFF",
                            text_color              : "#FFFFFF",
                            background_color        : "#0080AF",
                            border_hover_color      : "#CCCCCC",
                            text_hover_color        : "#000000",
                            background_hover_color  : "#FFFFFF",
                            images                  : false,
                            mouse                   : "press",
                            onChange : function(page){
                                loader("start");
                                $("#pag_actual").attr("value", page);
                                var tl=$("#tl_registros").attr("value");
                                $.post("modulos/maps/bandeja.control.edificio.php?cmd=cambiarPaginaEdificio", { 
                                                pagina: page, total: tl, 
                                                gestionObra: "' . $gestionObra . '", 
                                                empresa: "' . $empresa . '",
                                                region: "' . $region . '",
                                                zonal: "' . $zonal . '", 
                                                mdf: "' . $mdf . '", 
                                                campo_valor: "' . $campoValor . '", 
                                                order_field: order_field, 
                                                order_type: order_type, 
                                                departamento: "' . $departamento . '", 
                                                provincia: "' . $provincia . '", 
                                                distrito: "' . $distrito . '" 
                                        }, function(data){
                                                $("#tb_resultado tbody").empty();
                                                $("#tb_resultado tbody").append(data);
                                                loader("end");
                                        }
                                );
                            }
                        });
                    });';
                }
            $cadena .= '</script>';
        echo $cadena;
        
    } catch(PDOException $e) {
        //echo "Failed: " . $e->getMessage();
    }
}

function cambiarPaginaEdificio($pagina='', $total='', $estado='', $zonal='', 
        $mdf='', $campoValor='', $orderField='', $orderType='', 
        $departamento='', $provincia='', $distrito='') 
{
    try {
        global $conexion;

        if ($pagina == '') $pagina = 1;

        $objEdificio = new Data_FfttEdificio();
        $objEdificioEstado    = new Data_FfttEdificioEstado();
        
        $arrObjEdificioEstado    = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );


        $arrFiltro = array();
        if ( $estado != '' ) {
            $arrFiltro['estado'] = $estado;
        }
        
        if ( $zonal != '' ) {
            $arrFiltro['zonal'] = array($zonal);
        } else {
            $arrData = array();
            if (!empty($_SESSION['USUARIO_ZONAL'])) {
                foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                    $arrData[] = $objZonal->__get('_abvZonal');
                }
            }
            $arrFiltro['zonal'] = $arrData;
        }
        
        if ( $mdf != '' ) {
            $arrFiltro['mdf'] = array($mdf);
        } else {
            $arrMdf = array();
            if ( $zonal == '' ) {
                if (!empty($_SESSION['USUARIO_MDF'])) {
                    foreach ($_SESSION['USUARIO_MDF'] as $mdf) {
                        $arrMdf[]['mdf'] = $mdf;
                    }
                }
                
            } else {
                $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                    $conexion, $zonal, $idusuario
                );
            }
            
            $arrData = array();
            if (!empty($arrMdf)) {
                foreach ($arrMdf as $mdf) {
                    $arrData[] = $mdf['mdf'];
                }
            }
            $arrFiltro['mdf'] = $arrData;
        }
        
        if ( $campoValor != '' ) {
            $arrFiltro['campo_valor'] = $campoValor;
        }
        
        //Ubigeo
        if ( $departamento != '' ) {
            $arrFiltro['departamento'] = $departamento;
        }
        if ( $provincia != '' ) {
            $arrFiltro['provincia'] = $provincia;
        }
        if ( $distrito != '' ) {
            $arrFiltro['distrito'] = $distrito;
        }

        if ( $orderField != '' ) {
            $arrFiltro['order_field'] = $orderField;
        }
        if ( $orderType != '' ) {
            $arrFiltro['order_type'] = $orderType;
        }

        $arrObjEdificio = $objEdificio->listarEdificios($conexion, $pagina, '', $arrFiltro);

        $totalRegistros = $objEdificio->totalEdificios($conexion, '', $arrFiltro);

        $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);


        include 'vistas/v_bandeja.control.edificio.paginado.php';
        
    } catch(PDOException $e) {
        //echo "Failed: " . $e->getMessage();
    }
}

//Vista Detalle informacion del Edificio
function detalleEdificio($idedificio) 
{
    try {
        global $conexion;
        
        //variables para el formulario editar
        $numeroBlocks = 5;
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        $objEdificio          = new Data_FfttEdificio();
        $objEdificioEstado    = new Data_FfttEdificioEstado();
        $objCapas             = new Data_FfttCapas();
        $objTerminales        = new Data_FfttTerminales();
        $objUbigeo            = new Data_Ubigeo();

        $tipoCchhList         = $objCapas->getDescriptorByGrupo($conexion, 1);
        $tipoProyectoList     = $objCapas->getDescriptorByGrupo($conexion, 2);
        $tipoInfraestructuraList = $objCapas->getDescriptorByGrupo(
            $conexion, 3
        );
        $tipoViaList          = $objCapas->getDescriptorByGrupo($conexion, 4);

        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );
        
        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idedificio);
        
        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $arrObjEdificio[0]->__get('_zonal'), $idUsuario
        );
        
        
        $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
        $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
        $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
        
        //obteniendo el departamento
        $departamento = '';
        if ( $codDepartamento != '' ) {
            $arrObjDepartamento = $objUbigeo->departamentosUbigeo(
                $conexion, $codDepartamento
            );
            $departamento = ( !empty($arrObjDepartamento) ) ? 
                $arrObjDepartamento[0]->__get('_nombre') : '';
        }
        
        //obteniendo la provincia
        $provincia = '';
        if ( $codDepartamento != '' && $codProvincia != '' ) {
            $arrObjProvincia = $objUbigeo->provinciasUbigeo(
                $conexion, $codDepartamento, $codProvincia
            );
            $provincia = ( !empty($arrObjProvincia) ) ? 
                $arrObjProvincia[0]->__get('_nombre') : '';
        }
        
        //obteniendo el distrito
        $distrito = '';
        if ( $codDepartamento != '' && $codProvincia != '' && 
            $codDistrito != '' ) {
            $arrObjDistrito = $objUbigeo->distritosUbigeo(
                $conexion, $codDepartamento, $codProvincia, $codDistrito
            );
            $distrito = ( !empty($arrObjDistrito) ) ? 
                $arrObjDistrito[0]->__get('_nombre') : '';
        }
        

        include 'vistas/v_bandeja.control.edificio.detalle.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }
}

//Vista Editar informacion del Edificio
function editarEdificio ($idedificio, $iframe=0)
{
    try {
        global $conexion;
        
        //variables para el formulario editar
        $numeroBlocks = 5;
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        $objEdificio           = new Data_FfttEdificio();
        $objEdificioEstado     = new Data_FfttEdificioEstado();
        $objCapas              = new Data_FfttCapas();
        $objTerminales         = new Data_FfttTerminales();
        $objUbigeo             = new Data_Ubigeo();
        $objUbigeoZonal        = new Data_UbigeoZonal();
        $objGestionObra        = new Data_GestionObraEdi();
        $objRegion             = new Data_RegionEdi();
        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objSegmento           = new Data_SegmentoEdi();
        $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
        $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
        $objGestionCompetencia = new Data_GestionCompetenciaEdi();
        $objCoberturaMovil     = new Data_CoberturaMovilEdi();
        $objFacilidadPoste     = new Data_FacilidadPosteEdi();
        $objResponsableCampo   = new Data_ResponsableCampoEdi();
        
        
        $objTipoCchh     = new Data_TipoCchhEdi();
        $objTipoProyecto = new Data_TipoProyectoEdi();
        $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
        $objTipoVia      = new Data_TipoViaEdi();
        
        //lista de estados de edificios
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );
        
        //capturo datos del edificio
        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idedificio);
        
    
        $tipoCchhList           = $objCapas->getDescriptorByGrupo($conexion, 1);
        $tipoProyectoList       = $objCapas->getDescriptorByGrupo($conexion, 2);
        $tipoInfraestructuraList = $objCapas->getDescriptorByGrupo(
            $conexion, 3
        );
        $tipoViaList            = $objCapas->getDescriptorByGrupo($conexion, 4);
        
        //lista de estados de gestion de obra
        $arrObjGestionObra = $objGestionObra->listar($conexion);
        
        //obtengo las regiones a partir de las zonales que tiene asignado 
        //el usuario
        $arrRegion = listarRegionesPorZonalUsuario();
        
        //lista de fuentes de identificacion
        $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listar(
            $conexion
        );
        
        //lista de segmentos
        $arrObjSegmento = $objSegmento->listar($conexion);
        
        //lista de Tipo Cchh
        $arrObjTipoCchh = $objTipoCchh->listar($conexion);
        
        //lista de Tipo Proyecto
        $idSegmento = ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) ? 
            $arrObjEdificio[0]->__get('_idSegmento') : 1;
        $arrObjTipoProyecto = $objTipoProyecto->listar(
            $conexion, '', $idSegmento
        );
        
        //lista de Tipo de via
        $arrObjTipoVia = $objTipoVia->listar($conexion);
        
        //lista de Tipo de infraestructura
        $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($conexion);
        
        //lista de niveles socioeconomicos
        $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($conexion);
        
        //lista de zona de competencia
        $arrObjZonaCompetencia = $objZonaCompetencia->listar($conexion);
        
        //lista de gestion de competencia
        $arrObjGestionCompetencia = $objGestionCompetencia->listar($conexion);
        
        //lista cobertura movil 2G
        $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($conexion);
        
        //lista cobertura movil 3G
        $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($conexion);
        
        //lista facilidad de poste
        $arrObjFacilidadPoste = $objFacilidadPoste->listar($conexion);
        
        //lista de responsable de campo
        $arrObjResponsableCampo = $objResponsableCampo->listar($conexion);

        
        
        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $arrObjEdificio[0]->__get('_zonal'), $idUsuario
        );
        
        
        $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
        $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
        $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
        
        //lista de departamentos
        //$arrObjDepartamento   = $objUbigeo->departamentosUbigeo($conexion);
        $arrObjDepartamento = $objUbigeoZonal->traerDepartamentosZonal(
            $conexion, $arrObjEdificio[0]->__get('_zonal')
        );
        //lista de provincias
        $arrObjProvincia      = $objUbigeo->provinciasUbigeo(
            $conexion, $codDepartamento
        );
        //lista de distritos
        $arrObjDistrito       = $objUbigeo->distritosUbigeo(
            $conexion, $codDepartamento, $codProvincia
        );

        include 'vistas/v_bandeja.control.edificio.editar.php';

    } catch(PDOException $e) {
        echo $e->getMessage();
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }
}

//Vista Ingresar informacion del Edificio
function nuevoEdificio()
{
    try {
        global $conexion;
        
        //variables para el formulario editar
        $numeroBlocks = 5;
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        
        $objEdificioEstado    = new Data_FfttEdificioEstado();
        $objCapas            = new Data_FfttCapas();
        $objTerminales       = new Data_FfttTerminales();
        $objUbigeo            = new Data_Ubigeo();
    
        $tipoCchhList          = $objCapas->getDescriptorByGrupo($conexion, 1);
        $tipoProyectoList      = $objCapas->getDescriptorByGrupo($conexion, 2);
        $tipoInfraestructuraList  = $objCapas->getDescriptorByGrupo(
            $conexion, 3
        );
        $tipoViaList           = $objCapas->getDescriptorByGrupo($conexion, 4);
        

        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );
        
        
        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, 'LIM', $idUsuario
        );
        
        //lista de departamentos
        $arrObjDepartamento = $objUbigeo->departamentosUbigeo($conexion);
        //lista de provincias
        $arrObjProvincia    = $objUbigeo->provinciasUbigeo($conexion, '15');
        //lista de distritos
        $arrObjDistrito   = $objUbigeo->distritosUbigeo($conexion, '15', '01');

        include 'vistas/v_bandeja.control.edificio.nuevo.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
}

function updateDatosEdificio($arrDatos)
{
    try {
        global $conexion;
        
        echo 'salir';exit;
        
        //Inicando la transaccion
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();
        
        
        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objEdificio = new Data_FfttEdificio();
        $objEdificioMovimiento = new Data_FfttEdificioMovimiento();

        $arrDatos['nro_pisos_1'] = ( isset($arrDatos['nro_pisos_1']) ) ? 
            $arrDatos['nro_pisos_1'] : '';
        $arrDatos['nro_dptos_1'] = ( isset($arrDatos['nro_dptos_1']) ) ? 
            $arrDatos['nro_dptos_1'] : '';
                
        //formateando fecha para insert a db
        $arrDatos['fecha_registro_proyecto'] = ( $arrDatos['fecha_registro_proyecto'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_registro_proyecto']) : '0000-00-00';
        $arrDatos['fecha_termino_construccion'] = ( $arrDatos['fecha_termino_construccion'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_termino_construccion']) : '0000-00-00';
        $arrDatos['fecha_tratamiento_call'] = ( $arrDatos['fecha_tratamiento_call'] != '' ) ?
            formatFechaDB($arrDatos['fecha_tratamiento_call']) : '0000-00-00';
        $arrDatos['fecha_seguimiento'] = ( $arrDatos['fecha_seguimiento'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_seguimiento']) : '0000-00-00';

        $arrDatos['montante_fecha']         = ( $arrDatos['montante_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['montante_fecha']) : '0000-00-00';
        $arrDatos['ducteria_interna_fecha'] = ( $arrDatos['ducteria_interna_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['ducteria_interna_fecha']) : '0000-00-00';
        $arrDatos['punto_energia_fecha']    = ( $arrDatos['punto_energia_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['punto_energia_fecha']) : '0000-00-00';
    
        
        //seteamos actualizacion = 1 (actualizado desde web)
        $arrDatos['actualizacion'] = '1';
        
        
        //1. Actualizamos datos del Edificio
        $objEdificio->updateDatosEdificio($conexion, $arrDatos, $idUsuario);
        
        //2. Guardamos el movimiento del edificio (de la actualizacion)
        $objEdificioMovimiento->grabar($conexion, $arrDatos, $idUsuario);
        

        //Si todo salio OK, hacemos commit
        $conexion->commit();
        
        $dataResult = array(
            'flag'=>'1',
            'mensaje'=>'Datos grabados correctamente.'
        );
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        //Si ocurrio algun error, hacemos rollback
        $conexion->rollBack();
        
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>$e->getMessage(),
            'mensaje2'=>'Hubo un error al actualizar el Edificio, ' .
                'intentelo nuevamente.');
        echo json_encode($dataResult);
        exit;
    }
    
}

function insertDatosEdificio($arrDatos)
{
    try {
        global $conexion, $item;
        
        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objEdificio = new Data_FfttEdificio();
        
        $arrDatos['nro_pisos_1'] = ( isset($arrDatos['nro_pisos_1']) ) ? 
            $arrDatos['nro_pisos_1'] : '';
        $arrDatos['nro_dptos_1'] = ( isset($arrDatos['nro_dptos_1']) ) ? 
            $arrDatos['nro_dptos_1'] : '';
        
        //formateando fecha para insert a db
        $arrDatos['fecha_registro_proyecto']    = ( $arrDatos['fecha_registro_proyecto'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_registro_proyecto']) : '0000-00-00';
        $arrDatos['fecha_termino_construccion'] = ( $arrDatos['fecha_termino_construccion'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_termino_construccion']) : '0000-00-00';
        $arrDatos['fecha_seguimiento']          = ( $arrDatos['fecha_seguimiento'] != '' ) ? 
            formatFechaDB($arrDatos['fecha_seguimiento']) : '0000-00-00';
    
        $arrDatos['montante_fecha']         = ( $arrDatos['montante_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['montante_fecha']) : '0000-00-00';
        $arrDatos['ducteria_interna_fecha'] = ( $arrDatos['ducteria_interna_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['ducteria_interna_fecha']) : '0000-00-00';
        $arrDatos['punto_energia_fecha']    = ( $arrDatos['punto_energia_fecha'] != '' ) ? 
            formatFechaDB($arrDatos['punto_energia_fecha']) : '0000-00-00';
    
        
        //array de letras de ETAPAS
        $arrEtapa = array(
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
            'F' => 'F',
            'G' => 'G',
            'H' => 'H',
            'I' => 'I',
            'J' => 'J',
            'K' => 'K',
            'L' => 'L',
            'M' => 'M',
            'N' => 'N',
            'O' => 'O',
            'P' => 'P',
            'Q' => 'Q',
            'R' => 'R',
            'S' => 'S',
            'T' => 'T',
            'U' => 'U',
            'V' => 'V',
            'W' => 'W',
            'X' => 'X',
            'Y' => 'Y',
            'Z' => 'Z'
        );
    
        
        //item por default para inicio de ingreso de nuevos edificios
        //$item = '50000';
        $etapa     = '';
        $codExisteAutogenerado = $objEdificio->getExisteCodigoAutogeneradoEdificios($conexion);
        if ( !empty($codExisteAutogenerado) ) {
            $etapaFound = substr($codExisteAutogenerado[0]['max_item'], -1, 1);
            if ( array_key_exists($etapaFound, $arrEtapa) ) {
                $item  = substr($codExisteAutogenerado[0]['max_item'], 0, -1);
                $etapa = substr($codExisteAutogenerado[0]['max_item'], -1, 1);
            } else {
                $item  = $codExisteAutogenerado[0]['max_item'];
            }
        }
        
        //seteando el nuevo Item para la insersion del nuevo edificio
        $arrDatos['item'] = $item + 1;
        
        //Insertando el Edificio
        $objEdificio->insertDatosEdificio($conexion, $arrDatos, $idUsuario);
        
        
        $dataResult = array(
            'flag'=>'1',
            'mensaje'=>'Datos grabados correctamente.'
        );
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al actualizar el Edificio, ' .
                'intentelo nuevamente.');
        echo json_encode($dataResult);
        exit;
    }
    
}

function cargarMapaEdificio($idedificio)
{
    try {
        global $conexion;

        $objEdificio   = new Data_FfttEdificio();

        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idedificio);

        $arrDataMarkers = array();
        if ( !empty($arrObjEdificio) ) {

            $arrDataMarkers = array(
                'idedificio'    => $arrObjEdificio[0]->__get('_idEdificio'),
                'item'          => $arrObjEdificio[0]->__get('_item'),
                'x'             => $arrObjEdificio[0]->__get('_x'),
                'y'             => $arrObjEdificio[0]->__get('_y')
            );

        }

        $dataResult = array(
            'flag'      => '1',
            'marker'    => $arrDataMarkers
        );
        echo json_encode($dataResult);
        exit;
        
    } catch (PDOException $e) {
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al cargar el mapa de la ubicacion ' .
                'del edificio');
        echo json_encode($dataResult);
        exit;
    }
    
}

function cambiarEstado($idedificio, $estado)
{
    try {
        global $conexion;
        
        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objEdificio = new Data_FfttEdificio();
        
        //actualizando datos en edificio
        $objEdificio->cambiarEstado(
            $conexion, $idedificio, $estado, $idUsuario
        );
        
        
        $dataResult = array(
            'flag'=>'1',
            'mensaje'=>'Datos grabados correctamente.'
        );
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
            'flag'=>'0',
            'mensaje'=>'Hubo un error al actualizar el estado del Edificio, ' .
                'intentelo nuevamente.'
        );
        echo json_encode($dataResult);
        exit;
    }
    
}

function updateXyEdificio($idEdificio, $x, $y)
{
    try {
        global $conexion;

        $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
        $objEdificio = new Data_FfttEdificio();

        //actualizando x,y del Edificio
        $objEdificio->updateXyEdificio(
            $conexion, $idEdificio, $x, $y, $idUsuario
        );

        $dataResult = array(
            'flag'=>'1','mensaje'=>'Datos grabados correctamente.'
        );
        echo json_encode($dataResult);
        exit;

    } catch (PDOException $e) {
        $dataResult = array(
            'flag' => '0',
            'mensaje' => 'Hubo un error al actualizar el Edificio, ' . 
                'intentelo nuevamente.');
        echo json_encode($dataResult);
        exit;
    }
    
}

//lista historico de fotos de edificio
function fotosEdificio($idEdificio, $map='')
{
    try {
        global $conexion;
        
        $objEdificio       = new Data_FfttEdificio();
        $objEdificioEstado = new Data_FfttEdificioEstado();
        
        
        $arrObjEdificio = $objEdificio->obtenerEdificio(
            $conexion, $idEdificio
        );
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );

        include 'vistas/v_bandeja.control.edificio.fotos.main.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }
}

function listadoFotosEdificio($idEdificio)
{
    try {
        global $conexion;

        $objEdificioFotos = new Data_FfttEdificioFotos();

        $arrObjEdificioFotos = $objEdificioFotos->obtenerFotosEdificio(
            $conexion, $idEdificio
        );

        include 'vistas/v_bandeja.control.edificio.fotos.listado.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }

}

function registrarFotos($arrDataFoto, $arrFilesEdificios)
{
    try {
        global $conexion;

        //validando tama�o y dimensiones de fotos
        $validaTamanoImagen   = validateTamanoImagen(
            $arrFilesEdificios['imagen_edificio']
        );
        if ($validaTamanoImagen == '1') {
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "El tama&ntilde;o de 1 o mas archivos no es " .
                    "correcto.\n\n500Kb como maximo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN validando tama�o y dimensiones de fotos de edificio

        //Upload de fotos edificios
        $listNameFotos = moveFile(
            $arrFilesEdificios['imagen_edificio'], 'edi'
        );
        if ( !empty($listNameFotos) && count($listNameFotos) > 0 ) {
            $arrDataFoto['foto_edificio1'] = ( isset($listNameFotos[0]) ) ? 
                $listNameFotos[0] : '';
            $arrDataFoto['foto_edificio2'] = ( isset($listNameFotos[1]) ) ? 
                $listNameFotos[1] : '';
            $arrDataFoto['foto_edificio3'] = ( isset($listNameFotos[2]) ) ? 
                $listNameFotos[2] : '';
            $arrDataFoto['foto_edificio4'] = ( isset($listNameFotos[3]) ) ? 
                $listNameFotos[3] : '';
        } else {
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "No se logro subir las fotos del armario, " .
                    "Vuelva a intentarlo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN Upload de fotos


        $arrFechaFoto = explode("/", $arrDataFoto['fecha_foto']);
        $arrDataFoto['fecha_foto'] = $arrFechaFoto[2] . '-' . 
            $arrFechaFoto[1] . '-' . $arrFechaFoto[0];


        $idUsuario        = $_SESSION['USUARIO']->__get('_idUsuario');
        $objEdificio      = new Data_FfttEdificio();
        $objEdificioFotos = new Data_FfttEdificioFotos();


        //insertando en Edificio Fotos
        $idEdificiosFoto = $objEdificioFotos->insertarFotoEdificio(
            $conexion, $arrDataFoto, $idUsuario
        );

        //actualizando fotos en Edificio
        $objEdificio->updateFotosEdificio($conexion, $arrDataFoto, $idUsuario);


        $arrDataResult = array(
            'flag'    => '1',
            'mensaje' => 'Datos grabados correctamente.','tipo'=>''
        );
        echo json_encode($arrDataResult);
        exit;

    } catch (PDOException $e) {
        $arrDataResult = array(
            'flag'    => '0',
            'mensaje' => 'Hubo un error al registrar las fotos, intentelo ' .
                'nuevamente.',
            'tipo'    => ''
        );
        echo json_encode($arrDataResult);
        exit;
    }
}

//foto detalle
function verFotoDetalle($idEdificiosFoto)
{
    try {
        global $conexion;

        $objEdificioFotos = new Data_FfttEdificioFotos();

        $arrObjFotosDetalle = $objEdificioFotos->obtenerFotosDetalle(
            $conexion, $idEdificiosFoto
        );

        $arrDataFotos = array();
        if ( !empty($arrObjFotosDetalle) ) {
            if ( trim($arrObjFotosDetalle[0]->__get('_foto1')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto1'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto2')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto2'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto3')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto3'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto4')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto4'))
                );
            }
        }

        include 'vistas/v_bandeja.control.edificio.fotos.detalle.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }

}



//$fecha de la forma dd/mm/yyyy y la salida es yyyy-mm-dd
function formatFechaDB($fecha)
{
    
    $arrFecha = explode('/', $fecha);
    $dia = $arrFecha[0];
    $mes = $arrFecha[1];
    $ano = $arrFecha[2];
    
    return $ano . '-' . $mes . '-' . $dia;
}

function validateTamanoImagen($imagen)
{
    $tamano = '0';

    //validando el peso de las imagenes
    foreach ($imagen['size'] as $key => $value) {
        if ( $value > 0 ) {
            //si el archivo pesa mas que 500Kb
            if ($imagen['size'][$key] > 512000 || $imagen['size'][$key] == 0) {
                $tamano = '1';
                break;
            }
        }
    }

    return $tamano;
}

function moveFile($imagen, $directorio)
{
    $nameImage = array();
    $dir = FFTT . $directorio . "/";

    foreach ($imagen['name'] as $key => $value) {
        if (is_uploaded_file($imagen['tmp_name'][$key])) {

            if ($imagen['name'][$key]!= "") {
                $tmpName = explode(".", $imagen['name'][$key]);
                $imagenName = Logic_Utils::constructUrl($tmpName[0]) . '_' . rand(0, 100);
                $extension  = $tmpName[count($tmpName)-1];
                if (isset($imagen['tmp_name'][$key])) {
                    if ( move_uploaded_file($imagen['tmp_name'][$key], $dir . $imagenName.'.'.$extension) ) {
                        $nameImage[] = $imagenName.'.'.$extension;
                    }
                }
            }
        }
    }

    return $nameImage;
}



if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'obtenerZonalesPorRegionUsuario':
        obtenerZonalesPorRegionUsuario($_POST['region']);
        break;
    case 'verFotoDetalle':
        verFotoDetalle($_POST['idedificios_foto']);
        break;
    case 'registrarFotos':
        registrarFotos($_POST, $_FILES);
        break;
    case 'listadoFotosEdificio':
        listadoFotosEdificio($_POST['idEdificio']);
        break;
    case 'fotosEdificio':
        //$map me indica se la ventana fue abierta desde google maps
        //o desde la bandeja
        $_POST['map'] = ( isset($_POST['map']) ) ? $_POST['map'] : '';
        fotosEdificio($_POST['idEdificio'], $_POST['map']);
        break;
    case 'updateXyEdificio':
        updateXyEdificio($_POST['idEdificio'], $_POST['x'], $_POST['y']);
        break;
    case 'cargarMapaEdificio':
        cargarMapaEdificio($_POST['idedificio']);
        break;
    case 'insertDatosEdificio':
        insertDatosEdificio($_POST);
        break;
    case 'nuevoEdificio':
        nuevoEdificio();
        break;
    case 'updateDatosEdificio':
        updateDatosEdificio($_POST);
        break;
    case 'editarEdificio':
        //$iframe me indica si la ventana fue abierta desde google maps 
        //o desde alguna bandeja
        $iframe = ( isset($_POST['iframe']) ) ? 'iframe' : 'bandeja';
        editarEdificio($_POST['idedificio'], $iframe);
        break;
    case 'detalleEdificio':
        detalleEdificio($_POST['idedificio']);
        break;
    case 'cambiarEstado':
        cambiarEstado($_POST['idedificio'], $_POST['estado']);
        break;
    case 'cambiarPaginaEdificio':
        cambiarPaginaEdificio(
            $_POST['pagina'], $_POST['total'], $_POST['estado'], 
            $_POST['zonal'], $_POST['mdf'], $_POST['campo_valor'], 
            $_POST['order_field'], $_POST['order_type'], 
            $_POST['departamento'], $_POST['provincia'], $_POST['distrito']
        );
        break;
    case 'filtroBusqueda':
        filtroBusqueda(
            $_POST['pagina'], $_POST['total'], $_POST['gestionObra'], 
            $_POST['empresa'], $_POST['region'], $_POST['zonal'], $_POST['mdf'], 
            $_POST['campo_valor'], $_POST['order_field'], $_POST['order_type'], 
            $_POST['departamento'], $_POST['provincia'], $_POST['distrito']
        );
        break;
    default:
        bandejaMain();
        break;
}