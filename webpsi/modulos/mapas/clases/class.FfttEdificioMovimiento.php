<?php

/**
 * @package     class/data/
 * @name        class.FfttEdificioMovimiento.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/04/10
 */

class Data_FfttEdificioMovimiento
{

    protected $_table = 'webunificada_fftt.fftt_edificios_movimiento';
    
    protected $_idEdificioMovimiento = '';
    protected $_idEdificio = '';
    protected $_direccionObra = '';
    protected $_numero = '';
    protected $_mza = '';
    protected $_lote = '';
    protected $_personaContacto = '';
    protected $_nroBlocks = '';
    protected $_nroPisos = '';
    protected $_nroDptos = '';
    protected $_avance = '';
    protected $_fechaTerminoConstruccion = '';
    protected $_fechaTratamientoCall = '';
    protected $_observacion = '';
    protected $_estado = '';
    protected $_fechaInsert = '';
    protected $_userInsert = '';
    protected $_fechaSeguimiento = '';
    protected $_indEdificioMovimiento = '';
    protected $_actualizacion = '';

    
    /**
     * Metodo constructor
     * @method __construct
     */
    public function __construct()
    {
    }

    /**
     * Obtiene el valor de un atributo
     *
     * @return string $returnValue
     * @param string $propiedad nombre del atributo a obtener
     */
    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    /**
     * Asigna un valor a un atributo
     *
     * @param string $propiedad nombre del atributo a setear
     * @param string $valor valor a asignar
     */
    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    /**
     * Obtiene el listado de movimientos del edificio
     *
     * @return array $array[] objetos de tipo Data_FfttEdificioMovimiento
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idEdificio id del edificio
     */
    public function listar($conexion, $idEdificio)
    {
        try {
            $sql = "SELECT em.*, ee.des_estado, us.appat, us.apmat, us.nombres  
                    FROM " . $this->_table .  " em 
                    LEFT JOIN webunificada_fftt.fftt_edificios_estado ee 
                        ON em.estado = ee.codedificio_estado 
                    LEFT JOIN webunificada.usuario us 
                        ON em.user_insert = us.idusuario
                    WHERE em.idedificio = '" . $idEdificio . "' 
                    AND em.ind_edificio_movimiento = 'S' 
                    ORDER BY em.fecha_insert DESC";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['idedificio'] != ''):
                    $objEdificioMov = new Data_FfttEdificioMovimiento();
                    $objEdificioMov->__set(
                        '_idEdificioMovimiento', $row['idedificio_movimiento']
                    );
                    $objEdificioMov->__set('_idEdificio', $row['idedificio']);
                    $objEdificioMov->__set(
                        '_direccionObra', $row['direccion_obra']
                    );
                    $objEdificioMov->__set('_numero', $row['numero']);
                    $objEdificioMov->__set('_mza', $row['mza']);
                    $objEdificioMov->__set('_lote', $row['lote']);
                    $objEdificioMov->__set(
                        '_personaContacto', $row['persona_contacto']
                    );
                    $objEdificioMov->__set('_nroBlocks', $row['nro_blocks']);
                    $objEdificioMov->__set('_nroPisos', $row['nro_pisos']);
                    $objEdificioMov->__set('_nroDptos', $row['nro_dptos']);
                    $objEdificioMov->__set('_avance', $row['avance']);
                    $objEdificioMov->__set(
                        '_fechaTerminoConstruccion', 
                        $row['fecha_termino_construccion']
                    );
                    $objEdificioMov->__set(
                        '_observacion', $row['observacion']
                    );
                    $objEdificioMov->__set(
                        '_fechaTratamientoCall',
                        $row['fecha_tratamiento_call']
                    );
                    $objEdificioMov->__set('_estado', $row['estado']);
                    $objEdificioMov->__set(
                        '_fechaInsert', $row['fecha_insert']
                    );
                    $objEdificioMov->__set('_userInsert', $row['user_insert']);
                    $objEdificioMov->__set(
                        '_fechaSeguimiento',
                        $row['fecha_seguimiento']
                    );
                    $objEdificioMov->__set(
                        '_indEdificioMovimiento', 
                        $row['ind_edificio_movimiento']
                    );
                    $objEdificioMov->__set(
                        '_actualizacion', $row['actualizacion']
                    );
                    
                    $objEdificioMov->__set('_desEstado', $row['des_estado']);
                    $objEdificioMov->__set('_apPat', $row['appat']);
                    $objEdificioMov->__set('_apMat', $row['apmat']);
                    $objEdificioMov->__set('_nombres', $row['nombres']);
                    
                    $array[] = $objEdificioMov;
                endif;
            endforeach;
            
            return $array;
            
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    /**
     * Inserta un nuevo movimiento del edificio
     *
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve la exception
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param array $arrDatos array asociativo con los datos a insertar
     * @param int $usuarioIinsert id de usuario que realiza la actualizacion
     */
    public function grabar($conexion, $arrDatos, $usuarioInsert)
    {
        try {
            $sql = "INSERT INTO " . $this->_table 
                    .  " (idedificio, direccion_obra, numero, mza, lote, 
                    persona_contacto, nro_blocks, nro_pisos, nro_dptos, 
                    avance, fecha_termino_construccion, observacion, 
                    fecha_tratamiento_call, estado, fecha_insert, 
                    user_insert, fecha_seguimiento, actualizacion) 
                    VALUES(:idedificio, :direccion_obra, :numero, :mza, :lote, 
                    :persona_contacto, :nro_blocks, :nro_pisos, :nro_dptos, 
                    :avance, :fecha_termino_construccion, :observacion,
                    :fecha_tratamiento_call, :estado, now(), 
                    :user_insert, :fecha_seguimiento, :actualizacion)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idedificio", $arrDatos['idedificio']);
            $bind->bindParam(":direccion_obra", $arrDatos['direccion_obra']);
            $bind->bindParam(":numero", $arrDatos['numero']);
            $bind->bindParam(":mza", $arrDatos['mza']);
            $bind->bindParam(":lote", $arrDatos['lote']);
            $bind->bindParam(
                ":persona_contacto", $arrDatos['persona_contacto']
            );
            $bind->bindParam(":nro_blocks", $arrDatos['nro_blocks']);
            $bind->bindParam(":nro_pisos", $arrDatos['nro_pisos_1']);
            $bind->bindParam(":nro_dptos", $arrDatos['nro_dptos_1']);
            $bind->bindParam(":avance", $arrDatos['avance']);
            $bind->bindParam(
                ":fecha_termino_construccion", 
                $arrDatos['fecha_termino_construccion']
            );
            $bind->bindParam(":observacion", $arrDatos['observacion']);
            $bind->bindParam(
                ":fecha_tratamiento_call",
                $arrDatos['fecha_tratamiento_call']
            );
            $bind->bindParam(":estado", $arrDatos['estado']);
            $bind->bindParam(":user_insert", $usuarioInsert);
            $bind->bindParam(
                ":fecha_seguimiento",
                $arrDatos['fecha_seguimiento']
            );
            $bind->bindParam(":actualizacion", $arrDatos['actualizacion']);
            $bind->execute();
            
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
}