<?php

/**
 * @package     class/data/
 * @name        Data_Poligono
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/21
 */

class Data_Poligono
{

    protected $_table = 'webunificada_fftt.poligono';

    public function listar($conexion, $idPoligonoTipo, $flag='')
    {
        try {
            $strWhere = "";
            if ($flag != '') {
                $strWhere.=" AND flag=" . $flag;
            }

            $sql = "SELECT idpoligono, idpoligono_tipo, nombre, flag 
                    FROM " . $this->_table . " 
                    WHERE 1=1 " . $strWhere . " 
                    AND idpoligono_tipo = '" . $idPoligonoTipo . "' 
                    ORDER BY nombre;";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['idpoligono'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function listXYRutaPoligono($conexion, $idPoligono)
    {
        try {
            $sql = "SELECT po_xy.idpoligono, po_xy.orden, po_xy.x, po_xy.y, 
                    po.nombre AS nombre_poligono, 
                    po_ti.nombre AS nombre_poligono_tipo
                    FROM webunificada_fftt.poligono_xy po_xy, 
                    webunificada_fftt.poligono po, 
                    webunificada_fftt.poligono_tipo po_ti 
                    WHERE po_xy.idpoligono = po.idpoligono
                    AND po.idpoligono_tipo = po_ti.idpoligono_tipo
                    AND po_xy.idpoligono = '" . $idPoligono . "' 
                    ORDER BY po_xy.orden";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $reporte = array();
            while ($data = $bind->fetch(PDO::FETCH_ASSOC)) {
                if ($data['orden'] != '') {
                    $reporte[] = $data;
                }
            }
            return $reporte;
        } catch (PDOException $error) {
            exit();
        }
    }

    public function insertPoligono($conexion, $idPoligonoTipo, $nombre, 
            $userInsert, $flag)
    {
        try {

            $sql = "INSERT INTO " . $this->_table . "(idpoligono_tipo, nombre, 
                fecha_insert, user_insert, flag) 
                VALUES(:idpoligono_tipo, :nombre, now(), :user_insert, :flag)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono_tipo", $idPoligonoTipo);
            $bind->bindParam(":nombre", $nombre);
            $bind->bindParam(":user_insert", $userInsert);
            $bind->bindParam(":flag", $flag);
            $bind->execute();

            return $conexion->lastInsertId();
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updatePoligono($conexion, $idPoligono, $nombre, $userUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table . " SET 
                    nombre = :nombre, 
                    fecha_update = now(), 
                    user_update = :user_update 
                    WHERE idpoligono = :idpoligono";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->bindParam(":nombre", $nombre);
            $bind->bindParam(":user_update", $userUpdate);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }
    
    public function deletePoligono($conexion, $idPoligono)
    {
        try {

            $sql = "DELETE FROM " . $this->_table . " 
                    WHERE idpoligono = :idpoligono";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstadoPoligono($conexion, $idPoligono, $userUpdate,
            $flag)
    {
        try {

            $sql = "UPDATE " . $this->_table . " SET 
                    flag = :flag, 
                    fecha_update = now(), 
                    user_update = :user_update 
                    WHERE idpoligono = :idpoligono";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->bindParam(":flag", $flag);
            $bind->bindParam(":user_update", $userUpdate);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }
    
    public function updateNombrePoligono($conexion, $idPoligono, $nombre, 
            $userUpdate)
    {
        try {

            $sql = "UPDATE " . $this->_table . " SET 
                    nombre = :nombre, 
                    fecha_update = now(), 
                    user_update = :user_update 
                    WHERE idpoligono = :idpoligono";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->bindParam(":nombre", $nombre);
            $bind->bindParam(":user_update", $userUpdate);
            $bind->execute();

            return 1;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function deletePoligonoXY($conexion, $idPoligono)
    {
        try {

            $sql = "DELETE FROM webunificada_fftt.poligono_xy 
                    WHERE idpoligono = :idpoligono";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updatePoligonoXY($conexion, $idPoligono, $orden, $x, $y, 
            $userUpdate)
    {
        try {

            $sql = "INSERT INTO webunificada_fftt.poligono_xy (idpoligono, 
                    orden, x, y, fecha_update, user_update) 
                    VALUES(:idpoligono, :orden, :x, :y, now(), :user_update)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->bindParam(":orden", $orden);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->bindParam(":user_update", $userUpdate);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertPoligonoXY($conexion, $idPoligono, $orden, $x, $y, 
            $userInsert)
    {
        try {

            $sql = "INSERT INTO webunificada_fftt.poligono_xy (idpoligono, 
                    orden, x, y, fecha_insert, user_insert) 
                    VALUES(:idpoligono, :orden, :x, :y, now(), :user_insert)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idpoligono", $idPoligono);
            $bind->bindParam(":orden", $orden);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->bindParam(":user_insert", $userInsert);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function existsPoligono($conexion, $idPoligonoTipo, $poligono)
    {
        try {
            $sql = "SELECT idpoligono, nombre FROM " . $this->_table . " 
                    WHERE idpoligono_tipo = '" . trim($idPoligonoTipo) . "' 
                    AND nombre = '" . trim($poligono) . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            $data = $bind->fetch(PDO::FETCH_ASSOC);

            $flag = 0;
            if (!empty($data)) {
                $flag = 1;
            }

            return $flag;
        } catch (PDOException $error) {
            exit();
        }
    }

}