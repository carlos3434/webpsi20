<?php

/**
 * @package     class/data/
 * @name        Data_Ubigeo
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/21
 */

class Data_Ubigeo
{

    protected $_table = 'webunificada.ubigeo';
    
    protected $_codUbigeo = '';
    protected $_codDpto = '';
    protected $_codProv = '';
    protected $_codDist = '';
    protected $_nombre = '';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }
    
    /**
     * Obtiene el detalle de codigo de ubigeo, sino, devuelve todos los ubigeo
     *
     * @return array $array[] array de objetos de tipo Data_Ubigeo
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param string $codUbigeo codigo de ubigeo
     */
    public function listar($conexion, $codUbigeo='')
    {
        try {
            $strWhere = "";
            if ( $codUbigeo != '' ) {
                $strWhere .= " AND codubigeo = '" . $codUbigeo . "' ";
            }
    
            $sql = "SELECT codubigeo, coddpto, codprov, coddist, nombre
                    FROM " . $this->_table . "
                    WHERE 1=1 
                    " . $strWhere . "
                    ORDER BY codubigeo";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['codubigeo'] != ''):
                $objUbigeo = new Data_Ubigeo();
                $objUbigeo->__set('_codUbigeo', $row['codubigeo']);
                $objUbigeo->__set('_codDpto', $row['coddpto']);
                $objUbigeo->__set('_codProv', $row['codprov']);
                $objUbigeo->__set('_codDist', $row['coddist']);
                $objUbigeo->__set('_nombre', $row['nombre']);
                $array[] = $objUbigeo;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
    public function departamentosUbigeo($conexion, $codDpto='')
    {
        try {
            $strWhere = "";
            if ( $codDpto != '' ) {
                $strWhere .= " and coddpto = '" . $codDpto . "' ";
            }
            
            $sql = "SELECT codubigeo, coddpto, codprov, coddist, nombre 
                    FROM " . $this->_table . " 
                    WHERE codprov = '00'
                    AND coddist = '00' 
                    " . $strWhere . " 
                    ORDER BY nombre";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['codubigeo'] != ''):
                    $objUbigeo = new Data_Ubigeo();
                    $objUbigeo->__set('_codUbigeo', $row['codubigeo']);
                    $objUbigeo->__set('_codDpto', $row['coddpto']);
                    $objUbigeo->__set('_codProv', $row['codprov']);
                    $objUbigeo->__set('_codDist', $row['coddist']);
                    $objUbigeo->__set('_nombre', $row['nombre']);
                    $array[] = $objUbigeo;
                endif;
            endforeach;
            return $array; 
        } catch (PDOException $e) {
            throw $e;
        }    
    }
    
    public function provinciasUbigeo($conexion, $codDpto, $codProv='')
    {
        try {
            $strWhere = "";
            if ( $codProv != '' ) {
                $strWhere .= " AND codprov = '" . $codProv . "' ";
            }
            
            $sql = "SELECT codubigeo, coddpto, codprov, coddist, nombre 
                    FROM " . $this->_table . " 
                    WHERE coddpto = '" . $codDpto . "'
                    AND codprov != '00'
                    AND coddist = '00' 
                    " . $strWhere . " 
                    ORDER BY nombre";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['codubigeo'] != ''):
                    $objUbigeo = new Data_Ubigeo();
                    $objUbigeo->__set('_codUbigeo', $row['codubigeo']);
                    $objUbigeo->__set('_codDpto', $row['coddpto']);
                    $objUbigeo->__set('_codProv', $row['codprov']);
                    $objUbigeo->__set('_codDist', $row['coddist']);
                    $objUbigeo->__set('_nombre', $row['nombre']);
                    $array[] = $objUbigeo;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }    
    }
    
    public function distritosUbigeo($conexion, $codDpto, $codProv, $codDist='')
    {
        try {
            $strWhere = "";
            if ( $codDist != '' ) {
                $strWhere .= " AND coddist = '" . $codDist . "' ";
            }
            
            $sql = "SELECT codubigeo, coddpto, codprov, coddist, nombre 
                    FROM " . $this->_table . " 
                    WHERE coddpto = '" . $codDpto . "' 
                    AND codprov = '" . $codProv . "' 
                    AND coddist != '00' 
                    " . $strWhere . " 
                    ORDER BY nombre";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['codubigeo'] != ''):
                    $objUbigeo = new Data_Ubigeo();
                    $objUbigeo->__set('_codUbigeo', $row['codubigeo']);
                    $objUbigeo->__set('_codDpto', $row['coddpto']);
                    $objUbigeo->__set('_codProv', $row['codprov']);
                    $objUbigeo->__set('_codDist', $row['coddist']);
                    $objUbigeo->__set('_nombre', $row['nombre']);
                    $array[] = $objUbigeo;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }    
    }
    
    public function buscarLocalidad($conexion, $codDpto, $codProv, $codDist='',
            $localidad='')
    {
        try {
            
            $strWhere = "";
            $codUbigeo = $codDpto . $codProv;
            if ( $codDist != '' ) {
                $codUbigeo = $codDpto . $codProv . $codDist;
            }
            if ( $localidad != '' ) {
                $strWhere = " AND localidad LIKE '%" . $localidad . "%' ";
            }
            
            $sql = "SELECT ubigeo, departamento, provincia, distrito, localidad,
                    x_coords AS x, y_coords AS y 
                    FROM webunificada_fftt.inei 
                    WHERE ubigeo LIKE '" . $codUbigeo . "%' 
                    " . $strWhere . " 
                    ORDER BY localidad";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['ubigeo'] != ''):
                    $objUbigeo = new Data_Ubigeo();
                    $objUbigeo->__set('_ubigeo', $row['ubigeo']);
                    $objUbigeo->__set('_departamento', $row['departamento']);
                    $objUbigeo->__set('_provincia', $row['provincia']);
                    $objUbigeo->__set('_distrito', $row['distrito']);
                    $objUbigeo->__set('_localidad', $row['localidad']);
                    $objUbigeo->__set('_x', $row['x']);
                    $objUbigeo->__set('_y', $row['y']);
                    $array[] = $objUbigeo;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }    
    }
    
    public function buscarLocalidadPorXY($conexion, $x, $y, $distancia=10,
            $nroLocalidades=10)
    {
        try {
            
            $sql = "SELECT departamento, provincia, distrito, localidad, 
                    x_coords, y_coords, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y. ") ) * COS( RADIANS( y_coords ) ) * 
                    COS( RADIANS( x_coords ) - RADIANS(" . $x. ") ) + SIN( RADIANS(" . $y. ") ) * 
                    SIN( RADIANS( y_coords ) ) ) ) AS distance 
                    FROM webunificada_fftt.inei  
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroLocalidades . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }    
    }

}