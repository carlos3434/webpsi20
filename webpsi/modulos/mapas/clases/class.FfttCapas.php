<?php
/**
 * @package     class/data/
 * @name        class.ffttCapas.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/09
 */

class Data_FfttCapas
{

    protected $_idCapa = 0;
    protected $_nombre = '';
    protected $_abvCapa = '';
    protected $_esFftt;
    protected $_tieneTabla;
    protected $_indCapa;
    protected $_url = '';
    protected $_ico;
    protected $_table = 'webunificada_fftt.fftt_capa';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function offsetGet($conexion, $idCapa)
    {

        $sql = 'SELECT idcapa, nombre, abv_capa, es_fftt, tiene_tabla, ico,
                url,ind_capa FROM ' . $this->_table . ' 
                WHERE idcapa=' . $idCapa;
        
        $objCapa = new data_ffttCapas();
        foreach ($conexion->query($sql) as $row):
            if ($row['idcapa'] != '') {
                $objCapa->__set('_idCapa', trim($row['idcapa']));
                $objCapa->__set('_nombre', trim($row['nombre']));
                $objCapa->__set('_abvCapa', trim($row['abv_capa']));
                $objCapa->__set('_esFftt', trim($row['es_fftt']));
                $objCapa->__set('_tieneTabla', trim($row['tiene_tabla']));
                $objCapa->__set('_ico', trim($row['ico']));
                $objCapa->__set('_url', trim($row['url']));
                $objCapa->__set('_indCapa', trim($row['ind_capa']));
            }
        endforeach;
        return $objCapa;
    }

    public function offsetGetTMP($conexion, $idCapa)
    {

        $objCapa = new data_ffttCapas();
        $objCapa->__set('_idCapa', "1");
        $objCapa->__set('_nombre', "XXX");
        $objCapa->__set('_abvCapa', "X");
        $objCapa->__set('_url', "X.com");

        return $objCapa;
    }

    public function getCapasAll($conexion)
    {
        try {

            $sql = "SELECT idcapa, nombre, abv_capa, ico, es_fftt, tiene_tabla  
                    FROM webunificada_fftt.fftt_capa ORDER BY nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapas($conexion, $pagina='', $nombre='', $tieneTabla='')
    {
        try {

            $strLimite = '';
            if ($pagina == '') {
                $pagina = 1;
            } else {
                $offset = ($pagina - 1) * TAM_PAG_LISTADO;
                $strLimite = " LIMIT " . $offset . "," . TAM_PAG_LISTADO;
            }

            $strWhere = "";
            if ($nombre != '') {
                $strWhere .= " AND nombre LIKE '%" . $nombre . "%'";
            }

            if ($tieneTabla == '') {
                $strWhere .= " AND tiene_tabla = 'N' ";
            } else {
                $strWhere .= " AND tiene_tabla = '" . $tieneTabla . "' ";
            }

            $sql = "SELECT idcapa, nombre, abv_capa, ico, es_fftt, tiene_tabla  
                    FROM webunificada_fftt.fftt_capa 
                    WHERE 1=1 
                    " . $strWhere . "
                    " . $strLimite;

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapasCount($conexion, $nombre='', $tieneTabla='')
    {
        try {
            $strWhere = "";
            if ($nombre != '') {
                $strWhere .= " AND nombre LIKE '%" . $nombre . "%' ";
            }

            if ($tieneTabla == '') {
                $strWhere .= " AND tiene_tabla = 'N' ";
            } else {
                $strWhere .= " AND tiene_tabla = '" . $tieneTabla . "' ";
            }

            $sql = "SELECT count(1) as 'Total' 
                    FROM webunificada_fftt.fftt_capa 
                    WHERE 1=1 
                    " . $strWhere;
            foreach ($conexion->query($sql) as $row):
                $totalRegistros = $row['Total'];
            endforeach;
            return $totalRegistros;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapaById($conexion, $idCapa)
    {
        try {
            $sql = "SELECT idcapa, nombre, abv_capa, ico 
                    FROM webunificada_fftt.fftt_capa 
                    WHERE idcapa = '" . $idCapa . "'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapaByAbv($conexion, $abvCapa)
    {
        try {
            $sql = "SELECT idcapa, nombre, abv_capa, ico 
                    FROM webunificada_fftt.fftt_capa 
                    WHERE abv_capa = '" . $abvCapa . "' 
                    AND abv_capa != 'ico' 
                    AND ind_capa = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCamposByCapa($conexion, $idCapa)
    {
        try {
            $sql = "SELECT idcapa_detalle, campo_nro, campo, multiple, 
            ind_principal  FROM webunificada_fftt.fftt_capa_detalle 
                    WHERE idcapa = '" . $idCapa . "' 
                    ORDER BY ind_principal DESC";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCamposMultipleByCapaDetalle($conexion, $idCapaDetalle)
    {
        try {
            $sql = "SELECT idcapa_detalle_multiple, idcapa_detalle, opcion 
                    FROM webunificada_fftt.fftt_capa_detalle_multiple 
                    WHERE idcapa_detalle = '" . $idCapaDetalle . "' 
                    AND ind_capa_detalle_multiple = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfsEdificios($conexion)
    {
        try {
            $sql = "SELECT DISTINCT edi.ura, mdf.nombre 
                    FROM webunificada_fftt.fftt_edificios edi, 
                    webunificada.mdfs mdf
                    WHERE edi.ura = mdf.mdf
                    AND edi.ura IS NOT NULL AND edi.ura != '' 
                    ORDER BY mdf.nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfsCompetencias($conexion)
    {
        try {
            $sql = "SELECT DISTINCT com.ura, mdf.nombre 
                    FROM webunificada_fftt.fftt_competencia com, 
                    webunificada.mdfs mdf
                    WHERE com.ura = mdf.mdf
                    AND com.ura IS NOT NULL AND com.ura != '' 
                    ORDER BY mdf.nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getDescriptorByGrupo($conexion, $idDescriptorGrupo)
    {
        try {
            $sql = "SELECT iddescriptor, nombre 
                    FROM webunificada_fftt.fftt_descriptor 
                    WHERE iddescriptor_grupo = '" . $idDescriptorGrupo . "'
                    AND ind_descriptor = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEdificioByItem($conexion, $item, $itemInicial='')
    {

        $strWhere = "";
        if ($itemInicial != '') {
            $strWhere = " AND item != '" . $itemInicial . "' ";
        }

        try {
            $sql = "SELECT idedificio, item   
                    FROM webunificada_fftt.fftt_edificios  
                    WHERE item = '" . $item . "' 
                    " . $strWhere . " 
                    AND ind_edificio = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEdificioById($conexion, $idEdificio)
    {
        try {
            $sql = "SELECT *   
                    FROM webunificada_fftt.fftt_edificios  
                    WHERE idedificio = '" . $idEdificio . "'
                    AND ind_edificio = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCompetenciaById($conexion, $idCompetencia)
    {
        try {
            $sql = "SELECT *   
                    FROM webunificada_fftt.fftt_competencia  
                    WHERE idcompetencia = '" . $idCompetencia . "'
                    AND ind_competencia = 'S'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCapasByXy($conexion, $idCapa, $dataCampos, $x, $y, 
            $distancia=10, $nroTerminales=10)
    {
        try {

            $strWhere = "";
            foreach ($dataCampos as $campo => $nomCampo) {
                $strWhere .= $campo . ", ";
            }

            $sql = "SELECT idcomponente, idcapa, " . $strWhere . "  
                    foto1, x, y, ( 6371 * ACOS( COS( RADIANS(" . $y . ") ) * 
                    COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ")) 
                    + SIN( RADIANS(" . $y . ") ) * SIN( RADIANS( y ) ) ) ) 
                    AS distance 
                    FROM webunificada_fftt.fftt_componente 
                    WHERE idcapa = '" . $idCapa . "' 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getDepartamentos($conexion)
    {
        try {
            $sql = "SELECT coddpto, nombre 
                    FROM webunificada.ubigeo 
                    WHERE coddpto != '00'
                    AND codprov = '00' 
                    AND coddist = '00' 
                    ORDER BY nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getProvinciasByDpto($conexion, $codDpto)
    {
        try {
            $sql = "SELECT codprov, nombre 
                    FROM webunificada.ubigeo 
                    WHERE coddpto = '" . $codDpto . "'
                    AND codprov != '00' 
                    AND coddist = '00' 
                    ORDER BY nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getDistritosByProv($conexion, $codDpto, $codProv)
    {
        try {
            $sql = "SELECT coddist, nombre 
                    FROM webunificada.ubigeo 
                    WHERE coddpto = '" . $codDpto . "'
                    AND codprov = '" . $codProv . "' 
                    AND coddist != '00' 
                    ORDER BY nombre";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getExisteCodigoAutogeneradoEdificios($conexion)
    {
        try {
            $sql = "SELECT idedificio, MAX(item) AS max_item
                    FROM webunificada_fftt.fftt_edificios 
                    WHERE CAST(item AS SIGNED) > '10000'";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['idedificio'] != ''):
                    $array[] = $row;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function updateCapa($conexion, $data, $idUsuario)
    {
        try {
            $idCapa = trim($data['idcapa']);
            $nombre = trim($data['nombre']);

            $strWhere = "";
            if (isset($data['ico'])) {
                $ico = trim($data['ico']);
                $strWhere .= "ico = :ico, ";
            }

            $sql = "UPDATE webunificada_fftt.fftt_capa 
                    SET nombre = :nombre, 
                    " . $strWhere . "
                    fecha_update = now(),
                    user_update = :idusuario   
                    WHERE idcapa = :idcapa";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idcapa", $idCapa);
            $bind->bindParam(":nombre", $nombre);
            if ($ico != '') {
                $bind->bindParam(":ico", $ico);
            }
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertCapa($conexion, $data, $idUsuario)
    {
        try {
            $nombre = trim($data['nombre']);
            $abvCapa = trim($data['abv_capa']);
            $ico = trim($data['ico']);

            $sql = "INSERT INTO webunificada_fftt.fftt_capa(nombre, abv_capa, 
                    ico, fecha_insert, user_insert, ind_capa) 
                    VALUES(:nombre, :abv_capa, :ico, now(), :idusuario, 'S')";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":nombre", $nombre);
            $bind->bindParam(":abv_capa", $abvCapa);
            $bind->bindParam(":ico", $ico);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();

            return $conexion->lastInsertId();
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertCapaDetalle(
            $conexion, $idCapa, $campoNro, $campo, $idUsuario
    )
    {
        try {
            $idCapa = trim($idCapa);

            $sql = "INSERT INTO webunificada_fftt.fftt_capa_detalle(idcapa, 
                    campo_nro, campo, fecha_insert, user_insert, 
                    ind_capa_detalle)
                    VALUES(:idcapa, :campo_nro, :campo, now(), 
            :user_insert, 'S')";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idcapa", $idCapa);
            $bind->bindParam(":campo_nro", $campoNro);
            $bind->bindParam(":campo", $campo);
            $bind->bindParam(":user_insert", $idUsuario);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateCapaDetalle(
        $conexion, $idCapa, $campoNro, $campo, $idUsuario
    )
    {
        try {
            $idCapa = trim($idCapa);

            $sql = "UPDATE webunificada_fftt.fftt_capa_detalle 
                    SET campo = :campo, 
                    fecha_update = now(), 
                    user_update = :user_update  
                    WHERE idcapa = :idcapa 
                    AND campo_nro = :campo_nro";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idcapa", $idCapa);
            $bind->bindParam(":campo_nro", $campoNro);
            $bind->bindParam(":campo", $campo);
            $bind->bindParam(":user_update", $idUsuario);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertComponente($conexion, $data, $idUsuario)
    {
        try {

            $sqlCampos = $sqlValues = "";
            foreach ($data as $idcampo => $valcampo) {
                $sqlCampos .= "" . $idcampo . ", ";
                $sqlValues .= "'" . $valcampo . "', ";
            }

            $sql = "INSERT INTO 
            webunificada_fftt.fftt_componente(" . $sqlCampos .
                    "fecha_insert, user_insert, ind_componente) 
            VALUES(" . $sqlValues . " now(), '" . $idUsuario . "', 'S')";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return $conexion->lastInsertId();
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEdificio($conexion, $data, $idUsuario)
    {
        try {

            $strWhere = "";
            $strWhere .= "foto1 = '" . trim($data['archivo1']) . "', ";
            $strWhere .= "foto2 = '" . trim($data['archivo2']) . "', ";
            $strWhere .= "foto3 = '" . trim($data['archivo3']) . "', ";
            $strWhere .= "foto4 = '" . trim($data['archivo4']) . "', ";

            $sql = "UPDATE webunificada_fftt.fftt_edificios 
                    SET " . $strWhere . "
                    fecha_update = now(),
                    user_update = '" . $idUsuario . "'   
                    WHERE idedificio = '" . $data['idedificio'] . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();
            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEdificioComponente($conexion, $data, $idUsuario)
    {
        try {

            $sqlBlocks = "";
            for ($i = 1; $i <= 5; $i++) {
                if ( isset($data['nro_pisos_' . $i . '']) &&
                    $data['nro_pisos_' . $i . ''] != '' &&
                    isset($data['nro_dptos_' . $i . '']) && 
                    $data['nro_dptos_' . $i . ''] != '') {
                    $sqlBlocks .= "nro_pisos_" . $i . " = '" .
                    addslashes($data['nro_pisos_' . $i . '']) . "',
                    nro_dptos_" . $i . " = '" .
                    addslashes($data['nro_dptos_' . $i . '']) . "', ";
                }
            }
            
            $sqlXy = "";
            if ( $data['x'] != '' && $data['y'] != '' ) {
                $sqlXy = " x = '" . addslashes($data['x']) . "', 
                            y = '" . addslashes($data['y']) . "', ";
            } else {
                $sqlXy = " x = NULL, 
                            y = NULL, ";
            }

            $sql = "UPDATE webunificada_fftt.fftt_edificios SET 
                    " . $sqlXy . " 
                    item = '" . addslashes($data['item']) .
                            addslashes($data['item_etapa']) . "',
                    fecha_registro_proyecto = '" . 
                    addslashes($data['fecha_registro_proyecto']) . "',
                    ura = '" . addslashes($data['ura']) . "', 
                    sector = '" . addslashes($data['sector']) . "', 
                    mza_tdp = '" . addslashes($data['mza_tdp']) . "', 
                    idtipo_via = '" . addslashes($data['idtipo_via']) . "', 
                    direccion_obra = '" . 
                    addslashes($data['direccion_obra']) . "', 
                    numero = '" . addslashes($data['numero']) . "', 
                    mza = '" . addslashes($data['mza']) . "', 
                    lote = '" . addslashes($data['lote']) . "', 
                    idtipo_cchh = '" . addslashes($data['idtipo_cchh']) . "', 
                    cchh = '" . addslashes($data['cchh']) . "', 
                    distrito = '" . addslashes($data['distrito']) . "', 
                    nombre_constructora = '" . 
                    addslashes($data['nombre_constructora']) . "', 
                    nombre_proyecto = '" . 
                    addslashes($data['nombre_proyecto']) . "', 
                    idtipo_proyecto = '" . 
                    addslashes($data['idtipo_proyecto']) . "', 
                    persona_contacto = '" . 
                    addslashes($data['persona_contacto']) . "', 
                    pagina_web = '" . addslashes($data['pagina_web']) . "', 
                    email = '" . addslashes($data['email']) . "', 
                    nro_blocks = '" . addslashes($data['nro_blocks']) . "', 
                    " . $sqlBlocks . "
                    idtipo_infraestructura = '" .
                    addslashes($data['idtipo_infraestructura']) . "', 
                    nro_dptos_habitados = '" .
                    addslashes($data['nro_dptos_habitados']) . "', 
                    avance = '" . addslashes($data['avance']) . "', 
                    fecha_termino_construccion = '" .
                    addslashes($data['fecha_termino_construccion']) . "', 
                    montante = '" . addslashes($data['montante']) . "', 
                    montante_fecha = '" .
                    addslashes($data['montante_fecha']) . "',
                    montante_obs = '" .
                    addslashes($data['montante_obs']) . "',
                    ducteria_interna = '" .
                    addslashes($data['ducteria_interna']) . "', 
                    ducteria_interna_fecha = '" .
                    addslashes($data['ducteria_interna_fecha']) . "', 
                    ducteria_interna_obs = '" .
                    addslashes($data['ducteria_interna_obs']) . "', 
                    punto_energia = '" .
                    addslashes($data['punto_energia']) . "', 
                    punto_energia_fecha = '" .
                    addslashes($data['punto_energia_fecha']) . "',
                    punto_energia_obs = '" .
                    addslashes($data['punto_energia_obs']) . "',
                    fecha_seguimiento = '" .
                    addslashes($data['fecha_seguimiento']) . "', 
                    observacion = '" .
                    addslashes($data['observacion']) . "', 
                    fecha_update = now(), 
                    user_update = '" . $idUsuario . "',
                    estado = '" . addslashes($data['estado']) . "' 
                    WHERE idedificio = '" . $data['idedificio'] . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateCompetenciaComponente($conexion, $data, $idUsuario)
    {
        try {

            $sql = "UPDATE webunificada_fftt.fftt_competencia SET 
                    x = '" . addslashes($data['x']) . "', 
                    y = '" . addslashes($data['y']) . "', 
                    ura = '" . addslashes($data['ura']) . "', 
                    direccion = '" . addslashes($data['direccion']) . "', 
                    competencia = '" . 
                    addslashes($data['competencia']) . "', 
                    competencia_otro = '" .
                    addslashes($data['competencia_otro']) . "',
                    elementoencontrado = '" .
                    addslashes($data['elementoencontrado']) . "',
                    elementoencontrado_otro = '" .
                    addslashes($data['elementoencontrado_otro']) . "',
                    observacion = '" .
                    addslashes($data['observacion']) . "', 
                    fecha_update = now(), 
                    user_update = '" . $idUsuario . "' 
                    WHERE idcompetencia = '" . $data['idcompetencia'] . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertCompetencia($conexion, $data, $idUsuario)
    {
        try {

            $sqlCampos = $sqlValues = "";
            if (trim($data['archivo1']) != '') {
                $sqlCampos .= "foto1, ";
                $sqlValues .= "'" . $data['archivo1'] . "', ";
            }
            if (trim($data['archivo2']) != '') {
                $sqlCampos .= "foto2, ";
                $sqlValues .= "'" . $data['archivo2'] . "', ";
            }
            if (trim($data['archivo3']) != '') {
                $sqlCampos .= "foto3, ";
                $sqlValues .= "'" . $data['archivo3'] . "', ";
            }
            if (trim($data['archivo4']) != '') {
                $sqlCampos .= "foto4, ";
                $sqlValues .= "'" . $data['archivo4'] . "', ";
            }

            $sql = "INSERT INTO webunificada_fftt.fftt_competencia
                    (competencia, competencia_otro, 
                    ura, direccion, observacion, 
                    elementoencontrado, elementoencontrado_otro,
                    " . $sqlCampos . " 
                    x, y, fecha_insert, user_insert, ind_competencia)
                    VALUES('" . addslashes($data['competencia']) . "', 
                    '" . addslashes($data['competencia_otro']) . "',
                    '" . addslashes($data['ura']) . "', '" .
                    addslashes($data['direccion']) . "',
                    '" . addslashes($data['observacion']) . "',
                    '" . addslashes($data['elementoencontrado']) . "', 
                    '" . addslashes($data['elementoencontrado_otro']) . "', 
                    " . $sqlValues . " 
                    '" . addslashes($data['x']) . "', '" .
                    addslashes($data['y']) . "', 
                    now(), '" . $idUsuario . "', 'S')";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertEstacionBase($conexion, $data, $idUsuario)
    {
        try {

            $sqlCampos = $sqlValues = "";
            if (trim($data['archivo1']) != '') {
                $sqlCampos .= "foto1, ";
                $sqlValues .= "'" . $data['archivo1'] . "', ";
            }
            if (trim($data['archivo2']) != '') {
                $sqlCampos .= "foto2, ";
                $sqlValues .= "'" . $data['archivo2'] . "', ";
            }
            if (trim($data['archivo3']) != '') {
                $sqlCampos .= "foto3, ";
                $sqlValues .= "'" . $data['archivo3'] . "', ";
            }
            if (trim($data['archivo4']) != '') {
                $sqlCampos .= "foto4, ";
                $sqlValues .= "'" . $data['archivo4'] . "', ";
            }

            $sql = "INSERT INTO webunificada_fftt.fftt_estacionbase
                    (nombre, departamento, 
                    provincia, distrito, " . $sqlCampos . " 
                    x, y, fecha_insert, user_insert, ind_estacionbase)
                    VALUES('" . addslashes($data['nombre']) . "',
                    '" . addslashes($data['departamento']) . "',
                    '" . addslashes($data['provincia']) . "',
                    '" . addslashes($data['distrito']) . "', " . $sqlValues . "
                    '" . addslashes($data['x']) . "',
                    '" . addslashes($data['y']) . "', now(),
                    '" . $idUsuario . "', 'S')";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertEdificio($conexion, $data, $idUsuario)
    {
        try {

            $sqlCampos = $sqlValues = "";
            if (trim($data['archivo1']) != '') {
                $sqlCampos .= "foto1, ";
                $sqlValues .= "'" . $data['archivo1'] . "', ";
            }
            if (trim($data['archivo2']) != '') {
                $sqlCampos .= "foto2, ";
                $sqlValues .= "'" . $data['archivo2'] . "', ";
            }
            if (trim($data['archivo3']) != '') {
                $sqlCampos .= "foto3, ";
                $sqlValues .= "'" . $data['archivo3'] . "', ";
            }
            if (trim($data['archivo4']) != '') {
                $sqlCampos .= "foto4, ";
                $sqlValues .= "'" . $data['archivo4'] . "', ";
            }

            $sqlBlocks = $sqlXy = "";
            for ($i = 1; $i <= 5; $i++) {
                if ( isset($data['nro_pisos_' . $i . '']) &&
                    $data['nro_pisos_' . $i . ''] != '' &&
                    isset($data['nro_dptos_' . $i . '']) && 
                    $data['nro_dptos_' . $i . ''] != '') {
                    $sqlBlocks .= "nro_pisos_" . $i .
                    ", nro_dptos_" . $i . ", ";
                    $sqlXy .= "'" .
                    addslashes($data['nro_pisos_' . $i . '']) . "',
                    '" . addslashes($data['nro_dptos_' . $i . '']) 
                    . "', ";
                }
            }

            $sql = "INSERT INTO webunificada_fftt.fftt_edificios
                    (item, " . $sqlCampos . " 
                    x, y, fecha_registro_proyecto, 
                    ura, sector, mza_tdp, idtipo_via, direccion_obra, numero, 
                    mza, lote, idtipo_cchh, cchh, distrito, 
                    nombre_constructora, 
                    nombre_proyecto, idtipo_proyecto, persona_contacto,
                    pagina_web, email, nro_blocks, " . $sqlBlocks . " 
                    idtipo_infraestructura, nro_dptos_habitados, avance,
                    fecha_termino_construccion, 
                    montante, montante_fecha, montante_obs, 
                    ducteria_interna, ducteria_interna_fecha,
                    ducteria_interna_obs, 
                    punto_energia, punto_energia_fecha, punto_energia_obs, 
                    fecha_seguimiento, observacion, fecha_insert, user_insert, 
                    ind_edificio, estado, ingreso) 
                    VALUES('" . addslashes($data['item']) . "', " . $sqlValues . " '" . addslashes($data['x']) . "',
                    '" . addslashes($data['y']) . "', '" . addslashes($data['fecha_registro_proyecto']) . "',
                    '" . addslashes($data['ura']) . "', '" . addslashes($data['sector']) . "',
                    '" . addslashes($data['mza_tdp']) . "', '" . addslashes($data['idtipo_via']) . "',
                    '" . addslashes($data['direccion_obra']) . "', '" . addslashes($data['numero']) . "',
                    '" . addslashes($data['mza']) . "', '" . addslashes($data['lote']) . "',
                    '" . addslashes($data['idtipo_cchh']) . "', '" . addslashes($data['cchh']) . "',
                    '" . addslashes($data['distrito']) . "', '" . addslashes($data['nombre_constructora']) . "',
                    '" . addslashes($data['nombre_proyecto']) . "', '" . addslashes($data['idtipo_proyecto']) . "',
                    '" . addslashes($data['persona_contacto']) . "', '" . addslashes($data['pagina_web']) . "',
                    '" . addslashes($data['email']) . "', '" . addslashes($data['nro_blocks']) . "',
                    " . $sqlXy . " '" . addslashes($data['idtipo_infraestructura']) . "',
                    '" . addslashes($data['nro_dptos_habitados']) . "', '" . addslashes($data['avance']) . "',
                    '" . addslashes($data['fecha_termino_construccion']) . "',
                    '" . addslashes($data['montante']) . "', '" . addslashes($data['montante_fecha']) . "',
                    '" . addslashes($data['montante_obs']) . "',
                    '" . addslashes($data['ducteria_interna']) . "',
                    '" . addslashes($data['ducteria_interna_fecha']) . "',
                    '" . addslashes($data['ducteria_interna_obs']) . "',
                    '" . addslashes($data['punto_energia']) . "', '" . addslashes($data['punto_energia_fecha']) . "',
                    '" . addslashes($data['punto_energia_obs']) . "',
                    '" . addslashes($data['fecha_seguimiento']) . "',
                    '" . addslashes($data['observacion']) . "', now(), 
                    '" . $idUsuario . "', 'S',
            '" . addslashes($data['estado']) . "','2')";

            $bind = $conexion->prepare($sql);
            $bind->execute();

            return $conexion->lastInsertId();
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertEdificioFoto($conexion, $data, $idUsuario)
    {
        try {

            $sqlCampos = $sqlValues = "";
            if (trim($data['archivo1']) != '') {
                $sqlCampos .= "foto1, ";
                $sqlValues .= "'" . $data['archivo1'] . "', ";
            }
            if (trim($data['archivo2']) != '') {
                $sqlCampos .= "foto2, ";
                $sqlValues .= "'" . $data['archivo2'] . "', ";
            }
            if (trim($data['archivo3']) != '') {
                $sqlCampos .= "foto3, ";
                $sqlValues .= "'" . $data['archivo3'] . "', ";
            }
            if (trim($data['archivo4']) != '') {
                $sqlCampos .= "foto4, ";
                $sqlValues .= "'" . $data['archivo4'] . "', ";
            }

            $sql = "INSERT INTO webunificada_fftt.fftt_edificios_foto
                    (idedificio, " . $sqlCampos . " 
                    fecha_insert, user_insert)
                    VALUES('" . $data['idedificio'] . "', " . $sqlValues . " 
                    '" . $data['fecha'] . "', '" . $idUsuario . "')";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstadoEdificio(
        $conexion, $edificio, $estado, $idUsuario
    )
    {
        try {
            $edificio  = trim($edificio);
            $estado    = trim($estado);
            $idUsuario = trim($idUsuario);

            $sql = "UPDATE webunificada_fftt.fftt_edificios  
                    SET estado = '" . $estado . "', 
                    user_update = '" . $idUsuario . "', 
                    fecha_update = now() 
                    WHERE idedificio = '" . $edificio . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstadoCompetencia(
        $conexion, $competencia, $estado, $idUsuario
    )
    {
        try {
            $competencia = trim($competencia);
            $estado      = trim($estado);
            $idUsuario   = trim($idUsuario);

            $sql = "UPDATE webunificada_fftt.fftt_competencia  
                    SET estado = '" . $estado . "', 
                    user_update = '" . $idUsuario . "', 
                    fecha_update = now() 
                    WHERE idcompetencia = '" . $competencia . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function insertArmarioFoto($conexion, $data, $idUsuario)
    {
        try {

            $strWhere = $camposMore = "";
            if (trim($data['archivo1']) != '') {
                $strWhere .= "foto1 = :foto1, ";
                $camposMore .= "foto1, ";
                $valuesMore .= ":foto1, ";
            }
            if (trim($data['archivo2']) != '') {
                $strWhere .= "foto2 = :foto2, ";
                $camposMore .= "foto2, ";
                $valuesMore .= ":foto2, ";
            }
            if (trim($data['archivo3']) != '') {
                $strWhere .= "foto3 = :foto3, ";
                $camposMore .= "foto3, ";
                $valuesMore .= ":foto3, ";
            }

            // update fotos para fftt_armarios
            $sqlUno = "UPDATE 
                    webunificada_fftt.fftt_armarios 
                    SET " . $strWhere . "
                    fecha_update = now(),
                    user_update = :idusuario   
                    WHERE mtgespkarm = :mtgespkarm";
            $bind = $conexion->prepare($sqlUno);
            $bind->bindParam(":mtgespkarm", trim($data['mtgespkarm']));
            if (trim($data['archivo1']) != '')
                $bind->bindParam(":foto1", trim($data['archivo1']));
            if (trim($data['archivo2']) != '')
                $bind->bindParam(":foto2", trim($data['archivo2']));
            if (trim($data['archivo3']) != '')
                $bind->bindParam(":foto3", trim($data['archivo3']));
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
           
            $sqlDos = "INSERT webunificada_fftt.fftt_armarios_foto
                    (" . $camposMore . " 
                    mtgespkarm, fecha_insert, user_insert) 
                    VALUES(" . $valuesMore . 
                    " :mtgespkarm, :fecha_insert, :idusuario)";
            $bind = $conexion->prepare($sqlDos);
            $bind->bindParam(":mtgespkarm", trim($data['mtgespkarm']));
            if (trim($data['archivo1']) != '')
                $bind->bindParam(":foto1", trim($data['archivo1']));
            if (trim($data['archivo2']) != '')
                $bind->bindParam(":foto2", trim($data['archivo2']));
            if (trim($data['archivo3']) != '')
                $bind->bindParam(":foto3", trim($data['archivo3']));
            $bind->bindParam(":fecha_insert", trim($data['fecha']));
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->execute();
            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

}