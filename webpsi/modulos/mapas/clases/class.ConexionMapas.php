<?php
/**
 * User: Sergio
 * Date: 02/09/12
 * Time: 08:40 PM
 * To change this template use File | Settings | File Templates.
 */
class ConexionMapas
{
    private $Conexion_ID;

    function conectarBD()
    {
        try {
            $db = new PDO('mysql:host=localhost;dbname=webunificada_fftt', 'desarrollo', '123456');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->Conexion_ID = $db;
            return $this->Conexion_ID;

        } catch (PDOException $e) {
            return 0;

        }
    }

    function TransaccionesOn() {
        return $this->Conexion_ID->beginTransaction();

    }


}


