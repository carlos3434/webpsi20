<?php

/**
 * @package     class/data/
 * @name        class.ffttTerminales.php
 * @category    Model
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/08/09
 */

 /**
 * 2013-05-08 -- Modificado por Sergio Miranda
 * 
 *  
*/
 
class Data_FfttTerminales
{
    protected $_mtgespktrm;
    protected $_caja;
    protected $_x;
    protected $_y;
    protected $_table = 'fftt_terminales';

    public function __construct()
    {
    }

    public function __get($propiedad)
    {
        $returnValue = (string) '';
        $returnValue = $this->$propiedad;
        return (string) $returnValue;
    }

    public function __set($propiedad, $valor)
    {
        $this->$propiedad = $valor;
    }

    public function __offsetGet($conexion, $idTerm)
    {
        $objTerminal = new data_MdfTmp();
        $arrayTerminal = $objTerminal->__list($conexion);
        foreach ($arrayTerminal as $objeto):
            if ($objeto->__get('pktrm') == $idTerm):
                return $objeto;
            endif;
        endforeach;
        return false;
    }

    public function getZonalesByIdUsuario($conexion, $idUsuario)
    {
        try {
			// SERGIO: 2013-03-16 -- Se quita filtro de zonales por usuario

			/*
			$sql = "SELECT zo.abv_zonal, zo.desc_zonal, zo.x, zo.y  
                    FROM webunificada.usuario_zonal uz, webunificada.zonal zo  
                    WHERE uz.idzonal = zo.idzonal 
                    AND uz.idusuario = '" . $idUsuario . "' 
                    ORDER BY zo.abv_zonal";
			*/
			
			$sql = "SELECT zo.abv_zonal, zo.desc_zonal, zo.x, zo.y  
                    FROM webunificada.usuario_zonal uz, webunificada.zonal zo  
                    WHERE uz.idzonal = zo.idzonal GROUP BY 1	
                    ORDER BY zo.abv_zonal";
					
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEstacionBases($conexion)
    {
        try {
            $sql = "SELECT idestacionbase, nombre, departamento, provincia, 
                    distrito, direccion, x, y   
                    FROM webunificada_fftt.fftt_estacionbase 
                    ORDER BY idestacionbase";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfByZonal($conexion, $zonal)
    {
        try {
            $sql = "SELECT zo.abv_zonal, zo.desc_zonal, mdf.X, mdf.Y, mdf.mdf, 
                    mdf.nombre  
                    FROM webunificada.zonal zo, webunificada.mdfs mdf 
                    WHERE zo.abv_zonal = mdf.zonal 
                    AND mdf.zonal = '" . $zonal . "' 
                    ORDER BY mdf.mdf";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfByCodMdf($conexion, $mdf)
    {
        try {
            $sql = "SELECT zonal, ciudad, mdf, nombre, x, y, direccion, estado 
                    FROM webunificada.mdfs 
                    WHERE mdf = '" . $mdf . "'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getArmarioByCodArmario($conexion, $zonal, $mdf, $armario)
    {
        try {
            $sql = "SELECT zonal, mdf, armario, x, y, estado,direccion, coddpto,
                    codprov, coddist  
                    FROM webunificada_fftt.fftt_armarios 
                    WHERE zonal = '" . $zonal . "'
                    AND mdf = '" . $mdf . "' 
                    AND armario = '" . $armario . "'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfByZonalAndIdUsuario(
        $conexion, $zonal, $idUsuario, $datosXY=''
    )
    {
        try {

            $strWhere = "";
            if ($datosXY == 'conXY') {
                $strWhere .= " AND mdf.x IS NOT NULL AND mdf.y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND mdf.x IS NULL AND mdf.y IS NULL ";
            }

            $sql = "SELECT mdf.zonal AS abv_zonal, mdf.ciudad AS desc_zonal, 
                    mdf.mdf, mdf.nombre, mdf.x, mdf.y, mdf.estado  
                    FROM webunificada.usuario_mdfs usm, webunificada.mdfs mdf
                    WHERE usm.codmdf = mdf.mdf
                    AND usm.idusuario = '" . $idUsuario . "' 
                    AND mdf.zonal = '" . $zonal . "' 
                    " . $strWhere . " ORDER BY mdf.mdf";
            
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfByEeccZonalAndIdUsuario(
        $conexion, $eecc, $zonal, $idUsuario, $datosXY=''
    )
    {
        try {

            $strWhere = "";
            if ($datosXY == 'conXY') {
                $strWhere .= " AND mdf.x IS NOT NULL AND mdf.y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND mdf.x IS NULL AND mdf.y IS NULL ";
            }

            if ($eecc != '') {
                $strWhere .= " AND mdf.eecc = '" . $eecc . "' ";
            }

            $sql = "SELECT mdf.zonal AS abv_zonal, mdf.ciudad AS desc_zonal, 
                    mdf.mdf, mdf.nombre, mdf.x, mdf.y, mdf.estado  
                    FROM webunificada.usuario_mdfs usm, webunificada.mdfs mdf
                    WHERE usm.codmdf = mdf.mdf
                    AND usm.idusuario = '" . $idUsuario . "' 
                    AND mdf.zonal = '" . $zonal . "' 
                    " . $strWhere . " 
                    ORDER BY mdf.mdf";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getArmariosByZonalAndMdf(
        $conexion, $zonal, $mdf, $datosXY=''
    )
    {
        try {

            $strWhere = "";
            if ($datosXY == 'conXY') {
                $strWhere .= " AND x IS NOT NULL AND y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND x IS NULL AND y IS NULL ";
            }

            $sql = "SELECT arm.mtgespkarm, arm.zonal, arm.mdf, arm.armario,
                    arm.direccion, arm.qcaparmario, arm.qparlib, arm.qparres,
                    arm.qdistrib, arm.estado, arm.des_estado, arm.fecha_estado,
                    arm.x, arm.y, arm.foto1, arm.des_estado as cod_estado,
                    arm.observaciones, est.des_estado 
                    FROM webunificada_fftt.fftt_armarios arm 
                    LEFT JOIN webunificada_fftt.fftt_armarios_estado est 
                    ON arm.des_estado = est.codarmario_estado 
                    WHERE zonal = '" . $zonal . "' 
                    AND mdf = '" . $mdf . "' 
                    " . $strWhere;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEdificiosByZonalAndMdf(
        $conexion, $zonal, $mdf, $datosXY='', $item=''
    )
    {
        try {

            $strWhere = "";
            if ($mdf != '') {
                $strWhere .= " AND ura = '" . $mdf . "' ";
            }
            if ($item != '') {
                $strWhere .= " AND item = '" . $item . "' ";
            }

            if ($datosXY == 'conXY') {
                $strWhere .= " AND x IS NOT NULL AND y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND x IS NULL AND y IS NULL ";
            }

            $sql = "SELECT idedificio, item, ura,
                    direccion_obra, x, y, estado, foto1 
                    FROM webunificada_fftt.fftt_edificios  
                    WHERE 1=1 
                    " . $strWhere;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCompetenciaByZonalAndMdf(
        $conexion, $zonal, $mdf, $datosXY=''
    )
    {
        try {

            $strWhere = "";
            if ($datosXY == 'conXY') {
                $strWhere .= " AND x IS NOT NULL AND y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND x IS NULL AND y IS NULL ";
            }

            $sql = "SELECT idcompetencia, ura, direccion, x, y, estado, foto1   
                    FROM webunificada_fftt.fftt_competencia 
                    WHERE ura = '" . $mdf . "' 
                    " . $strWhere;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCableArmarioByZonalAndMdfAndTipoRed($conexion, 
            $zonal, $mdf, $tipoRed)
    {
        try {
            $campo = $wstrnig = '';
            if ($tipoRed == 'D') {
                $campo = " cable ";
            } elseif ($tipoRed == 'F') {
                $campo = " armario ";
            }

            if (isset($zonal) && $zonal <> '') {
                //$wstrnig = " AND zonal = '" . $zonal . "' ";
            }

            $sql = "SELECT DISTINCT " . $campo . " AS cablearmario
                    FROM webunificada_fftt.fftt_terminales 
                    WHERE 1 = 1 $wstrnig
                    AND mdf = '" . $mdf . "' 
                    AND tipo_red = '" . $tipoRed . "'";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    /*
     * Function modificada por Steven - 08/10/2011
     * Agregue a la function el codterminal para obtener el departamento, 
     * provincia y distrito del mismo
     */

    public function getTerminales($conexion, $zonal, $mdf, $tipoRed, 
            $cableArmario, $datosXY='', $codTerminal='')
    {
        try {
            if ($tipoRed == 'D') {
                $campo = " cable ";
            } elseif ($tipoRed == 'F') {
                $campo = " armario ";
            }

            $strWhere = "";
            if ($datosXY == 'conXY') {
                $strWhere .= " AND x IS NOT NULL AND y IS NOT NULL ";
            } elseif ($datosXY == 'sinXY') {
                $strWhere .= " AND x IS NULL AND y IS NULL ";
            }

            /*
             * Aumento de esta opcion cuando llega una el codterminal
             */
            if ($codTerminal != '') {
                $strWhere .= " AND caja = '" . $codTerminal . "'";
            }
            //Where zonal = '".$zonal."';
            $sql = "SELECT * 
                    FROM webunificada_fftt.fftt_terminales 
                    WHERE 1=1 
                    AND mdf = '" . $mdf . "' 
                    AND tipo_red = '" . $tipoRed . "' 
                    AND " . $campo . " = '" . $cableArmario . "' 
                    " . $strWhere . " ORDER BY caja";
            $res = mysql_query($sql) or die(mysql_error());
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }





    
    public function getTerminalesByUbigeo($conexion, $dpto, $prov, $dist, 
        $sector='', $mza='')
    {
        try {

            $strWhere = "";
            if ($sector != '') {
                $strWhere .= " AND sector = '" . $sector . "' ";
            }
            if ($mza != '') {
                $strWhere .= " AND manzana = '" . $mza . "' ";
            }
    
            $sql = "SELECT *
                    FROM webunificada_fftt.fftt_terminales 
                    WHERE coddpto = '" . $dpto . "' 
                    AND codprov = '" . $prov . "' 
                    AND coddist = '" . $dist . "' 
                    " . $strWhere;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getTerminalByMtgespktrm($conexion, $mtgespktrm)
    {
        try {
            $sql = "SELECT * 
                    FROM webunificada_fftt.fftt_terminales 
                    WHERE mtgespktrm = '" . $mtgespktrm . "'";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getTerminalesByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {
            $sql = "SELECT mtgespktrm, zonal, zonal1, mdf, cable, armario, caja,
                    qcapcaja, qparlib, qparres, qdistrib, tipo_red, 
                    atenuacion, atenuacion_max, atenuacion_min, x, y, 
                    direccion, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y . ") ) 
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") ) 
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_terminales 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getDireccionTerminal($conexion, $mtgespktrm)
    {
        try {

            $sql = "SELECT direccion FROM webunificada_fftt.fftt_terminales 
                    WHERE mtgespktrm = '" . $mtgespktrm . "' 
                    LIMIT 1";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getDireccionTap($conexion, $mtgespktap)
    {
        try {

            $sql = "SELECT IF(direc7 != '' AND direc8 != '', CONCAT(direc2, ' ',
                    direc3, ' ', direc4, ' Mz.', direc7, ' Lt.', direc8 , ' - ',
                    direc9, ' ', direc10), 
                    CONCAT(direc2, ' ', direc3, ' ', direc4, ' - ', direc9, ' ',
                    direc10) ) AS direccion, x_coords, y_coords  
                    FROM webunificada_fftt.fftt_cms  
                    WHERE CONCAT(nodo,troba2,amp,tap) = '" . $mtgespktap . "' 
                    LIMIT 1";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getArmariosByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT mtgespkarm, zonal, mdf, armario, estado, qcaparmario,
                    qparlib, qparres, qdistrib, x, y, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_armarios  
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getMdfsByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT zonal, mdf, nombre, direccion, estado, x, y, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada.mdfs   
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEdificiosByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT *, ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_edificios 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getCompetenciasByXy($conexion, $x, $y, $distancia=10,
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT *, ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_competencia 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getPosiblesClientesByXy($conexion, $x, $y, $distancia=10, 
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT *, ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.posibles_clientes 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getEstacionbasesByXy($conexion, $x, $y, $distancia=10,
            $nroTerminales=10)
    {
        try {

            $sql = "SELECT idestacionbase, nombre, departamento, provincia, 
                    distrito, direccion, x, y, 
                    ( 6371 * ACOS( COS( RADIANS(" . $y . ") )
                    * COS( RADIANS( y ) ) * 
                    COS( RADIANS( x ) - RADIANS(" . $x . ") )
                    + SIN( RADIANS(" . $y . ") ) * 
                    SIN( RADIANS( y ) ) ) ) AS distance 
                    FROM webunificada_fftt.fftt_estacionbase 
                    HAVING distance < " . $distancia . " 
                    ORDER BY distance LIMIT 0," . $nroTerminales . "";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getClientesByTerminal($conexion, $zonal, $mdf, $cable, 
            $armario, $caja='', $tipoRed='', $marcaSpd='')
    {
        try {

            $strWhere = "";
            if ($tipoRed == 'D') {
                $tabla = "webunificada_fftt.fftt_directa ";
                $campo = "cli.ParAlimentador AS Par ";
                $order = "cli.ParAlimentador";
                $strWhere .= " AND cli.Cable = '" . $cable . "' ";
            } elseif ($tipoRed == 'F') {
                $tabla = "webunificada_fftt.fftt_secundaria ";
                $campo = "cli.Pardistribuidor AS Par ";
                $order = "cli.Pardistribuidor";
                $strWhere .= " AND cli.Armario = '" . $armario . "' ";
            }

            if ($caja != '') {
                $strWhere .= " AND cli.Caja = '" . $caja . "' ";
            }
            if ($marcaSpd != '') {
                $strWhere .= " AND cli.marca_spd = '" . $marcaSpd . "' ";
            }

            $sql = "SELECT " . $campo . ", cli.Cliente, cli.Direccion, 
                    cli.Circuito, cli.Telefono, cli.DescEstadoPar, 
                    cli.marca_spd, cli.ddn_telefono, cli.PRODUCTO,
                    cli.VELOCIDAD, abo.tipodslam, abo.att_down, abo.snr_down, 
                    abo.rservice_m12, abo.Mpproducto, abo.sugerencia, 
                    abo.campo1, abo.campo2, abo.campo3 
                    FROM " . $tabla . " cli 
                        LEFT JOIN webunificada_fftt.data_adicional_abonados abo 
                        ON  cli.ddn_telefono = abo.telefono 
                    WHERE cli.Zonal = '" . $zonal . "' 
                    AND cli.MDF = '" . $mdf . "' 
                    " . $strWhere . " 
                    ORDER BY " . $order;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function getClientesByTerminales($conexion, $zonal, $mdf, $cable, 
            $armario, $cajaArr=array(), $tipoRed='', $marcaSpd='')
    {
        try {

            $strWhere = "";
            if ($tipoRed == 'D') {
                $tabla = "webunificada_fftt.fftt_directa ";
                $campo = "ParAlimentador AS Par, Zonal, MDF, Cable, Caja ";
                $order = "ParAlimentador";
                $strWhere .= " AND Cable = '" . $cable . "' ";
            } elseif ($tipoRed == 'F') {
                $tabla = "webunificada_fftt.fftt_secundaria ";
                $campo = "Pardistribuidor AS Par, Zonal, MDF, Armario, Caja ";
                $order = "Pardistribuidor";
                $strWhere .= " AND Armario = '" . $armario . "' ";
            }

            if (!empty($cajaArr)) {
                $strWhere .= " AND Caja 
                            IN ('" . implode('\',\'', $cajaArr) . "') ";
            }
            if ($marcaSpd != '') {
                $strWhere .= " AND marca_spd = '" . $marcaSpd . "' ";
            }

            $sql = "SELECT " . $campo . ", Cliente, Direccion, Circuito,
                    Telefono, DescEstadoPar, marca_spd, x, y,
                    ddn_telefono, PRODUCTO, VELOCIDAD    
                    FROM " . $tabla . " 
                    WHERE Zonal = '" . $zonal . "' 
                    AND MDF = '" . $mdf . "' 
                    " . $strWhere . " 
                    ORDER BY " . $order;
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function __list($conexion, $mdf='', $tipRed= '', $cableArmario = '')
    {
        try {
            $where = '';
            if ($mdf != '' && $mdf != '-1') {
                $where.=" AND codmdf = '" . $mdf . "'";
            }
            if ($tipRed != '' && $tipRed != '-1') {
                $where.=" AND tipred = '" . $tipRed . "'";
            }
            if ($cableArmario != '' && $cableArmario != '-1') {
                if ($tipRed == 'D') {
                    $where.=" AND nrocbl = '" . $cableArmario . "'";
                } elseif ($tipRed == 'F') {
                    $where.=" AND nrodsa = '" . $cableArmario . "'";
                }
            }

            $sql = "SELECT codmdf, nrodsa, nrocbl, nrocaja, pktrm, qcapcaja, 
                    qparlib, qparres, qdistrib, fulmov, tipred, x, y 
                    FROM webunificada_fftt.fftt_terminales 
                    WHERE 1=1" . $where . " ORDER BY nrocaja";

            $array = array();
            foreach ($conexion->query($sql) as $row):
                if ($row['codmdf'] != ''):
                    $objTerminal = new data_Terminales();
                    $objTerminal->__set('_codmdf', $row['codmdf']);
                    $objTerminal->__set('_nrodsa', $row['nrodsa']);
                    $objTerminal->__set('_nrocbl', $row['nrocbl']);
                    $objTerminal->__set('_nrocaja', $row['nrocaja']);
                    $objTerminal->__set('_pktrm', $row['pktrm']);
                    $objTerminal->__set('_qcapcaja', $row['qcapcaja']);
                    $objTerminal->__set('_qparlib', $row['qparlib']);
                    $objTerminal->__set('_qparres', $row['qparres']);
                    $objTerminal->__set('_qdistrib', $row['qdistrib']);
                    $objTerminal->__set('_fulmov', $row['fulmov']);
                    $objTerminal->__set('_tipred', $row['tipred']);
                    $objTerminal->__set('_x', $row['x']);
                    $objTerminal->__set('_y', $row['y']);
                    $array[] = $objTerminal;
                endif;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function updateXY($conexion, $mtgespktrm, $x, $y)
    {
        try {
            $mtgespktrm = trim($mtgespktrm);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada_fftt.fftt_terminales 
                    SET x = :x, 
                    y = :y, 
                    fecha_update = now()   
                    WHERE mtgespktrm = :mtgespktrm";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":mtgespktrm", $mtgespktrm);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstacionBaseXY($conexion, $idEstacionBase, $x, $y)
    {
        try {
            $idEstacionBase = trim($idEstacionBase);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada_fftt.fftt_estacionbase 
                    SET x = :x, 
                    y = :y 
                    WHERE idestacionbase = :idestacionbase";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idestacionbase", $idEstacionBase);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateZonalXY($conexion, $abvZonal, $x, $y)
    {
        try {
            $abvZonal = trim($abvZonal);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada.zonal 
                    SET x = :x, 
                    y = :y 
                    WHERE abv_zonal = :abv_zonal";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":abv_zonal", $abvZonal);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateMdfXY($conexion, $zonal, $mdf, $x, $y)
    {
        try {
            $zonal = trim($zonal);
            $mdf = trim($mdf);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada.mdfs 
                    SET x = :x, 
                    y = :y 
                    WHERE zonal = :zonal 
                    AND mdf = :mdf";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":zonal", $zonal);
            $bind->bindParam(":mdf", $mdf);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstadoMdf($conexion, $zonal, $mdf, $estado)
    {
        try {
            $zonal = trim($zonal);
            $mdf = trim($mdf);
            $estado = trim($estado);

            $sql = "UPDATE webunificada.mdfs 
                    SET estado = :estado 
                    WHERE zonal = :zonal 
                    AND mdf = :mdf";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":zonal", $zonal);
            $bind->bindParam(":mdf", $mdf);
            $bind->bindParam(":estado", $estado);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateXYhistorial($conexion, $mtgespktrm, $x, $y,
            $idUsuario='', $ipUsuario='')
    {
        try {
            $mtgespktrm = trim($mtgespktrm);
            $x = trim($x);
            $y = trim($y);
            $idUsuario = trim($idUsuario);
            $ipUsuario = trim($ipUsuario);

            $sql = "INSERT INTO webunificada_fftt.fftt_terminales_historial(
                    idusuario, mtgespktrm, x, y, fecha_update, ipusuario) 
                    VALUES(:idusuario, :mtgespktrm, :x, :y, now(), :ipusuario)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idusuario", $idUsuario);
            $bind->bindParam(":mtgespktrm", $mtgespktrm);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->bindParam(":ipusuario", $ipUsuario);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateArmarioXY($conexion, $mtgespkarm, $x, $y, $userUpdate)
    {
        try {
            $mtgespkarm = trim($mtgespkarm);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada_fftt.fftt_armarios 
                    SET x = '" . $x . "', 
                    y = '" . $y . "', 
                    user_update = '" . $userUpdate . "', 
                    fecha_update = now()   
                    WHERE mtgespkarm = '" . $mtgespkarm . "'";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":mtgespkarm", $mtgespkarm);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->bindParam(":user_update", $userUpdate);
            $bind->execute();
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function updateEdificioXY(
        $conexion, $idEdificio, $x, $y, $userUpdate
    )
    {
        try {
            $idEdificio = trim($idEdificio);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada_fftt.fftt_edificios 
                    SET x = '" . $x . "', 
                    y = '" . $y . "', 
                    user_update = '" . $userUpdate . "', 
                    fecha_update = now()   
                    WHERE idedificio = '" . $idEdificio . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateCompetenciaXY($conexion, $idCompetencia, $x, $y,
            $userUpdate)
    {
        try {
            $idCompetencia = trim($idCompetencia);
            $x = trim($x);
            $y = trim($y);

            $sql = "UPDATE webunificada_fftt.fftt_competencia 
                    SET x = '" . $x . "', 
                    y = '" . $y . "', 
                    user_update = '" . $userUpdate . "', 
                    fecha_update = now()   
                    WHERE idcompetencia = '" . $idCompetencia . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();

            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    public function updateEstadoArmario($conexion, $zonal, $mdf, $armario,
            $estado, $idUsuario)
    {
        try {
            $zonal = trim($zonal);
            $mdf = trim($mdf);
            $armario = trim($armario);
            $estado = trim($estado);
            $idUsuario = trim($idUsuario);

            $sql = "UPDATE webunificada_fftt.fftt_armarios 
                    SET estado = '" . $estado . "', 
                    user_update = '" . $idUsuario . "', 
                    fecha_update = now() 
                    WHERE zonal = '" . $zonal . "' 
                    AND mdf = '" . $mdf . "' 
                    AND armario = '" . $armario . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function updateZonaSegura($conexion, $mtgespkarm, $zona)
    {
        try {

            $sql = "UPDATE webunificada_fftt.fftt_terminales SET 
                    zona_peligrosa=" . $zona . " 
                    WHERE mtgespktrm='" . $mtgespkarm . "'";
            $bind = $conexion->prepare($sql);
            $bind->execute();
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    public function esZonaSegura($conexion, $tipoPedido, $negocio, $mdf, $cable,
            $armario, $terminal)
    {
        try {
            $esPeligrosa = 0;

            if ($tipoPedido != '' && $negocio != '') {
                $sql = '';

                if ($tipoPedido == 'Provision'):
                    if ($negocio == 'ADS' || $negocio == 'STB'):
                        $sql = "SELECT zona_peligrosa 
                            FROM webunificada_fftt.fftt_terminales WHERE 1=1";

                        if ($mdf != '' && $mdf != '-')
                            $sql .= " AND mdf = '" . $mdf . "'";

                        if ($cable != '' && $cable != '-') {
                            if ($armario != '' && $armario != '-')
                                $sql .= " AND (cable = '" . $cable . "' 
                                        OR armario = '" . $armario . "')";
                            else
                                $sql .= " AND cable = '" . $cable . "'";
                        } else {
                            if ($armario != '' && $armario != '-')
                                $sql .= " AND armario = '" . $armario . "'";
                        }

                        if ($terminal != '' && $terminal != '-')
                            $sql .= " AND caja = '" . $terminal . "'";
                    endif;

                    if ($negocio == 'CMS'):
                        $sql = "SELECT zona_peligrosa 
                                FROM webunificada_fftt.fftt_cms WHERE 1 = 1";

                        if ($mdf != '' && $mdf != '-')
                            $sql .= " AND nodo = '" . $mdf . "'";

                        if ($armario != '' && $armario != '-')
                            $sql .= " AND troba2 = '" . $armario . "'";

                        if ($terminal != '' && $terminal != '-')
                            $sql .= " AND tap = '" . $terminal . "'";
                    endif;
                endif;

                if ($tipoPedido == 'Averia'):
                    if ($negocio == 'ADSL' || $negocio == 'STB'):
                        $sql = "SELECT zona_peligrosa
                            FROM webunificada_fftt.fftt_terminales WHERE 1 = 1";

                        if ($mdf != '' && $mdf != '-')
                            $sql .= " AND mdf = '" . $mdf . "'";

                        if ($cable != '' && $cable != '-') {
                            if ($armario != '' && $armario != '-')
                                $sql .= " AND (cable = '" . $cable . "'
                                        OR armario = '" . $armario . "')";
                            else
                                $sql .= " AND cable = '" . $cable . "'";
                        } else {
                            if ($armario != '' && $armario != '-')
                                $sql .= " AND armario = '" . $armario . "'";
                        }

                        if ($terminal != '' && $terminal != '-')
                            $sql .= " AND caja = '" . $terminal . "'";
                    endif;

                    if ($negocio == 'CATV'):
                        $sql = "SELECT zona_peligrosa
                                FROM webunificada_fftt.fftt_cms WHERE 1=1";

                        if ($mdf != '' && $mdf != '-')
                            $sql .= " AND nodo = '" . $mdf . "'";

                        if ($armario != '' && $armario != '-')
                            $sql .= " AND troba2 = '" . $armario . "'";

                        if ($terminal != '' && $terminal != '-')
                            $sql .= " AND tap = '" . $terminal . "'";
                    endif;
                endif;

                if ($sql != ''):
                    foreach ($conexion->query($sql) as $row):
                        if ($row['zona_peligrosa'] == 1):
                            $esPeligrosa = 1;
                        endif;
                    endforeach;
                endif;
            }
            return $esPeligrosa;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    //busqueda de cliente en "Busqueda de componentes"
    public function listClientes($conexion, $tipoRed='F', $telefono='',
            $clienteUno='', $clienteDos='', $direccion='', $ddnTelefono=''
    )
    {
        try {

            $tabla = 'fftt_secundaria';
            $cableArmario = 'Armario';
            $oCableArmario = 'Cable';
            $bloque = 'Bloque,';
            if ($tipoRed == 'D') {
                $tabla = 'fftt_directa';
                $cableArmario = 'Cable';
                $oCableArmario = 'Armario';
                $bloque = '';
            }

            $strWhere = "";
            if ($telefono != '') {
                $strWhere .= " AND Telefono = '" . 
                    addslashes($telefono) . "' ";
            }
            if ($clienteUno != '') {
                $strWhere .= " AND Cliente LIKE '%" . 
                    addslashes($clienteUno) . "%' ";
            }
            if ($clienteDos != '') {
                $strWhere .= " AND Cliente LIKE '%" . 
                    addslashes($clienteDos) . "%' ";
            }
            if ($direccion != '') {
                $strWhere .= " AND Direccion LIKE '" . 
                    addslashes($direccion) . "%' ";
            }
            if ($ddnTelefono != '') {
                $strWhere .= " AND ddn_telefono = '" . 
                    addslashes($ddnTelefono) . "' ";
            }

            $sql = "SELECT '" . $tipoRed . "' AS tipo_red, Zonal," . $bloque . "
                    MDF, " . $cableArmario . " AS cableArmario, 
                    '' AS " . $oCableArmario . ", " . $cableArmario . ", Caja,
                    Cliente, Direccion, DDN, Telefono,
                    Inscripcion*1 AS Inscripcion, x, y, departamento, provincia,
                    distrito, ddn_telefono, PRODUCTO, VELOCIDAD, DescCiudad,
                    DescEstadoPar    
                    FROM webunificada_fftt." . $tabla . " 
                    WHERE 1=1 
                    " . $strWhere . " 
                    LIMIT 100";
            $array = array();
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function listClientesDireccion(
            $conexion, $tipoRed='', $zonal=array(),
            $distrito=array(), $direccionUno='')
    {
        try {

            $tabla = 'fftt_secundaria';
            $cableArmario = 'Armario';
            $oCableArmario = 'Cable';
            $bloque = 'Bloque,';
            if ($tipoRed == 'D') {
                $tabla = 'fftt_directa';
                $cableArmario = 'Cable';
                $oCableArmario = 'Armario';
                $bloque = '';
            }

            $strWhere = "";
            if (!empty($zonal)) {
                if (count($zonal) == 1) {
                    if ($zonal[0] == 'LIMA') {
                        $strWhere .= " AND Zonal 
                                        IN ('ESTE','OESTE','NORTE','SUR') ";
                    } else {
                        $strWhere .= " AND Zonal = '" . $zonal[0] . "'";
                    }
                } else {

                    $arrZonal = array();
                    foreach ($zonal as $pZonal) {
                        if ($pZonal == 'LIMA') {
                            $arrZonal[] = 'ESTE';
                            $arrZonal[] = 'OESTE';
                            $arrZonal[] = 'NORTE';
                            $arrZonal[] = 'SUR';
                        } else {
                            $arrZonal[] = $pZonal;
                        }
                    }

                    $strWhere .= " AND Zonal 
                                   IN ('" . implode('\',\'', $arrZonal) . "') ";
                }
            }
            if (!empty($distrito)) {
                if (count($distrito) == 1) {
                    $strWhere .= " AND distrito = '" . $distrito[0] . "' ";
                } else {
                    $strWhere .= " AND distrito 
                                   IN ('" . implode('\',\'', $distrito) . "') ";
                }
            }
            if ($direccionUno != '') {

                $arrDireccion = explode(' ', $direccionUno);

                if (!empty($arrDireccion)) {
                    foreach ($arrDireccion as $direccion) {
                        $strWhere .= " AND Direccion 
                                       LIKE '%" . $direccion . "%' ";
                    }
                }
            }

            $sql = "SELECT '" . $tipoRed . "' AS tipo_red, Zonal," . $bloque . "
                    MDF, " . $cableArmario . " AS cableArmario, 
                    '' AS " . $oCableArmario . ", " . $cableArmario . ", Caja,
                    Cliente, Direccion, DDN, Telefono, x, y 
                    FROM webunificada_fftt." . $tabla . "   
                    WHERE 1=1 
                    " . $strWhere . " 
                    LIMIT 500";
            $array = array();
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getDistritoByZonalFromFfttDirecta($conexion, $zonal, 
            $arrZonal=array())
    {
        try {

            $strWhere = "";
            if (!empty($arrZonal)) {
                if (count($arrZonal) == 1) {
                    if ($arrZonal[0] == 'LIMA') {
                        $strWhere .= " AND Zonal IN " . 
                            "('ESTE','OESTE','NORTE','SUR') ";
                    } else {
                        $strWhere .= " AND Zonal = '" . $arrZonal[0] . "'";
                    }
                } else {
                    $arrZonal = array();
                    foreach ($arrZonal as $pZonal) {
                        if ($pZonal == 'LIMA') {
                            $arrZonal[] = 'ESTE';
                            $arrZonal[] = 'OESTE';
                            $arrZonal[] = 'NORTE';
                            $arrZonal[] = 'SUR';
                        } else {
                            $arrZonal[] = $pZonal;
                        }
                    }

                    $strWhere .= " AND Zonal
                                IN ('" . implode('\',\'', $arrZonal) . "') ";
                }
            } else {
                if ($zonal == 'LIMA') {
                    $strWhere .= " AND Zonal
                    IN ('ESTE','OESTE','NORTE','SUR') ";
                } else {
                    $strWhere .= " AND Zonal = '" . $zonal . "'";
                }
            }


            $sql = "SELECT DISTINCT distrito
                    FROM webunificada_fftt.fftt_secundaria 
                    WHERE 1=1 
                    " . $strWhere . " 
                    ORDER BY distrito";
            $array = array();
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }

    /**
     * Devuelve un listado de todos los cables secundarios a partir del Armario.
     *
     * @author       Steven Ostin Fern�ndez Casasola <sfernandezc@gmd.com.pe>
     * @copyright    2010 Planificacion y Soluciones Informaticas
     * @method       getSecundario_by_Armario_Zonal_MDF
     * @param        string $zonal, es la zonal donde se encuentra el MDF
     * @param        string $mdf, es el MDF del cual sale el Armario
     * @param        string $armario, es el Armario en el
     *               cual se debe saber sus cables 
     *               Secundarios.
     * @return       array $array, devuelve un listado de todos los 
     *               cables secundarios a partir del Armario.
     */
    public function getSecundarioByArmarioZonalMdf(
        $conexion, $zonal, $mdf, $armario
    )
    {
     // Where zonal = '".$zonal."';
        try {
            $sql = "SELECT distinct Bloque 
                    FROM webunificada_fftt.fftt_secundaria
                    WHERE 1=1 
                    AND mdf = '" . $mdf . "' 
                    AND Armario = '" . $armario . "'";
            $array = array();
            foreach ($conexion->query($sql) as $row):
                $array[] = $row;
            endforeach;
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * Devuelve informacion adicional del terminal. Agregado 20/06/2012
     *
     * @author       Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @method       obtenerMasDatosTerminal
     * @param        string $mtgespktrm key de terminal
     * @return       array $array informacion adicional de terminal
     */
    public function obtenerMasDatosTerminal($conexion, $mtgespktrm='')
    {
        try {
            $array = array();
            if ( $mtgespktrm != '' ) {
                $sql = "SELECT *
                    FROM webunificada_fftt.atenuacion_adsl_total
                    WHERE llave = '" . $mtgespktrm . "'";
                $array = $conexion->query($sql);
                $array = $array->fetchAll(PDO::FETCH_ASSOC);
            }
            
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * Actualiza xy de abonados de la red secundaria. Agregado 11/07/2012
     *
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @method actualizarXyFfttSecundaria
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve 0
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param string $zonal zonal
     * @param string $mdf mdf
     * @param string $armario armario
     * @param string $caja caja
     * @param string $x coordenada x
     * @param string $y coordenada y
     */
    public function actualizarXyFfttSecundaria(
            $conexion, $zonal, $mdf, $armario, $caja, $x, $y
    )
    {
        try {
            $zonal = trim($zonal);
            $mdf = trim($mdf);
            $armario = trim($armario);
            $caja = trim($caja);
            $x = trim($x);
            $y = trim($y);
    
            $sql = "UPDATE webunificada_fftt.fftt_secundaria
                    SET x = :x,
                    y = :y
                    WHERE Zonal = :zonal
                    AND MDF = :mdf
                    AND Armario = :armario
                    AND Caja = :caja";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":zonal", $zonal);
            $bind->bindParam(":mdf", $mdf);
            $bind->bindParam(":armario", $armario);
            $bind->bindParam(":caja", $caja);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();
    
            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }
    
    /**
     * Actualiza xy de abonados de la red directa. Agregado 11/07/2012
     *
     * @author Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
     * @method actualizarXyFfttDirecta
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve 0
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param string $zonal zonal
     * @param string $mdf mdf
     * @param string $cable cable
     * @param string $caja caja
     * @param string $x coordenada x
     * @param string $y coordenada y
     */
    public function actualizarXyFfttDirecta(
            $conexion, $zonal, $mdf, $cable, $caja, $x, $y
    )
    {
        try {
            $zonal = trim($zonal);
            $mdf = trim($mdf);
            $cable = trim($cable);
            $caja = trim($caja);
            $x = trim($x);
            $y = trim($y);
    
            $sql = "UPDATE webunificada_fftt.fftt_directa
                    SET x = :x,
                    y = :y
                    WHERE Zonal = :zonal
                    AND MDF = :mdf
                    AND Cable = :cable
                    AND Caja = :caja";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":zonal", $zonal);
            $bind->bindParam(":mdf", $mdf);
            $bind->bindParam(":cable", $cable);
            $bind->bindParam(":caja", $caja);
            $bind->bindParam(":x", $x);
            $bind->bindParam(":y", $y);
            $bind->execute();
    
            return 1;
        } catch (PDOException $error) {
            return 0;
        }
    }

    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de los cables primarios que se encuentran en los
     * mdfs seleccionados.
     * @method obtenerCablePrimario
     * @param array $arrMdf Arreglo de MDFs.
     * @param string $tipo Tipo de Red : Directa - Flexible.
     * @return array $array Informacion de los cables primarios.
     */
    public function obtenerCablePrimario($arrMdf, $tipo)
    {
        global $conexion;
    
        try {
            $strWhere = '';
            if ($tipo == 'D') {
                $nomBaseDatos = "webunificada_fftt.fftt_directa";
                $sql = "SELECT mdf AS mdf,cable AS cable "
                     . "FROM " . $nomBaseDatos . " WHERE 1 = 1 ";
            }
            if ($tipo == 'F') {
                $nomBaseDatos = "webunificada_fftt.fftt_pararmarios";
                $sql = "SELECT mdf AS mdf,cable AS cable "
                     . "FROM " . $nomBaseDatos . " WHERE 1 = 1 ";
            }
            if (!empty($arrMdf)) {
                $indice = 0;
                $numElementos = count($arrMdf);
                $strWhere .= "AND mdf IN (";
                foreach ($arrMdf as $key=>$value) {
                    $strWhere .= "'" . $value . "'";
                    if ($indice < $numElementos - 1) {
                        $strWhere .= ",";
                    }
                    $indice++;
                }
                $strWhere .= ") ";
            }
            $strWhere .= "GROUP BY mdf,cable";
            $sql .= $strWhere;
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de los cables secundarios que se encuentran en los
     * armarios seleccionados.
     * @method obtenerCableSecundario
     * @param array $arrMdf Arreglo de MDFs.
     * @param array $arrArmario Arreglo de Armarios.
     * @return array $array Informacion de los cables secundarios.
     */
    public function obtenerCableSecundario($arrMdf, $arrArmario)
    {
        global $conexion;

        $valores = '';
        try {
            $nomBaseDatos = "webunificada_fftt.fftt_secundaria";
            $sql = "SELECT mdf AS mdf,armario AS armario,bloque AS cable "
                 . "FROM " . $nomBaseDatos . " WHERE 1 = 1 ";
            $strWhere = '';
            if (!empty($arrArmario)) {
                $indice = 0;
                $numElementos = count($arrArmario);
                foreach ($arrArmario as $key=>$value) {
                    if ($value != '') {
                        $arrElemento = explode('-', $value);
                        if (count($arrElemento)) {
                            $value = $arrElemento[0] . '-' . $arrElemento[2];
                        }
                        $valores .= "'" . $value . "'";
                        if ($indice < $numElementos - 1) {
                            $valores .= ",";
                        }
                    }
                    $indice++;
                }
                if ($valores != '') {
                    $strWhere .= "AND CONCAT(mdf,'-',armario) IN (";
                    $strWhere .= $valores;
                    $strWhere .= ") ";
                } else {
                    if (!empty($arrMdf)) {
                        $indice = 0;
                        $numElementos = count($arrMdf);
                        foreach ($arrMdf as $key=>$value) {
                            if ($value != '') {
                                $valores .= "'" . $value . "'";
                                if ($indice < $numElementos - 1) {
                                    $valores .= ",";
                                }
                            }
                            $indice++;
                        }
                        if ($valores != '') {
                            $strWhere .= "AND mdf IN (";
                            $strWhere .= $valores;
                            $strWhere .= ") ";
                        }
                    }
                }
            }
            $strWhere .= "GROUP BY mdf,armario,bloque";
            $sql .= $strWhere;
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de los armarios que se encuentran en los cables
     * primarios seleccionados.
     * @method obtenerArmario
     * @param array $arrMdf Arreglo de MDFs.
     * @param array $arrCable Arreglo de Cables Primarios.
     * @return array $array Informacion de los armarios.
     */
    public function obtenerArmario($arrMdf, $arrCable)
    {
        global $conexion;
    
        try {
            $nomBaseDatos = "webunificada_fftt.fftt_pararmarios";
            $sql = "SELECT mdf AS mdf,cable AS cable,armario AS armario "
                 . "FROM " . $nomBaseDatos . " WHERE 1 = 1 ";
            $strWhere = '';
            if (!empty($arrCable)) {
                $indice = 0;
                $numElementos = count($arrCable);
                $strWhere .= "AND CONCAT(mdf,'-',cable) IN (";
                foreach ($arrCable as $key=>$value) {
                    $strWhere .= "'" . $value . "'";
                    if ($indice < $numElementos - 1) {
                        $strWhere .= ",";
                    }
                    $indice++;
                }
                $strWhere .= ") ";
            }
            $strWhere .= "GROUP BY mdf,cable,armario";
            $sql .= $strWhere;
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de las trobas que se encuentran en los nodos
     * seleccionados.
     * @method obtenerTroba
     * @param array $arrNodo Arreglo de Nodos.     
     * @return array $array Informacion de las trobas.
     */
    public function obtenerTroba($arrNodo)
    {
        global $conexion;
    
        try {
            $sql = "SELECT nodo AS nodo,troba2 AS troba "
                 . "FROM webunificada_fftt.fftt_cms WHERE 1 = 1 ";
            $strWhere = '';
            if (!empty($arrNodo)) {
                $indice = 0;
                $numElementos = count($arrNodo);
                $strWhere .= "AND nodo IN (";
                foreach ($arrNodo as $key=>$value) {
                    $strWhere .= "'" . $value . "'";
                    if ($indice < $numElementos - 1) {
                        $strWhere .= ",";
                    }
                    $indice++;
                }
                $strWhere .= ") ";
            }
            $strWhere .= "GROUP BY nodo,troba2";
            $sql .= $strWhere;
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de los amplificadores que se encuentran en las 
     * trobas seleccionadas.
     * @method obtenerTroba
     * @param array $arrNodo Arreglo de Nodos.
     * @param array $arrTroba Arreglo de Trobas.
     * @return array $array Informacion de los amplificadores.
     */
    public function obtenerAmplificador($arrNodo, $arrTroba)
    {
        global $conexion;
    
        $valores = '';
        try {
            $sql = "SELECT nodo AS nodo,troba2 AS troba,amp AS amplificador "
                 . "FROM webunificada_fftt.fftt_cms WHERE 1 = 1 ";
            $strWhere = '';
            if (!empty($arrTroba)) {
                $indice = 0;
                $numElementos = count($arrTroba);
                foreach ($arrTroba as $key=>$value) {
                    if ($value != '') {
                        $valores .= "'" . $value . "'";
                        if ($indice < $numElementos - 1) {
                            $valores .= ",";
                        }
                    }
                    $indice++;
                }
                if ($valores != '') {
                    $strWhere .= "AND CONCAT(nodo,'-',troba2) IN (";
                    $strWhere .= $valores;
                    $strWhere .= ") ";
                } else {
                    if (!empty($arrNodo)) {
                        $indice = 0;
                        $numElementos = count($arrNodo);
                        foreach ($arrNodo as $key=>$value) {
                            if ($value != '') {
                                $valores .= "'" . $value . "'";
                                if ($indice < $numElementos - 1) {
                                    $valores .= ",";
                                }
                            }
                            $indice++;
                        }
                        if ($valores != '') {
                            $strWhere .= "AND nodo IN (";
                            $strWhere .= $valores;
                            $strWhere .= ") ";
                        }
                    }       
                }
            }
            $strWhere .= "GROUP BY nodo,troba2,amplificador";
            $sql .= $strWhere;
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
    
    /**
     * @author Fernando Enrique Esteban Valerio <festeban@gmd.com.pe>
     * Devuelve informacion de las cabeceras ADSL que se encuentran en los
     * mdfs seleccionados.
     * @method obtenerCabeceraAdsl
     * @param array $arrMdf Arreglo de Mdfs.
     * @return array $array Informacion de las cabeceras ADSL.
     */
    public function obtenerCabeceraAdsl($arrMdf)
    {
        global $conexion;
    
        $valores = '';
        try {
            $sql = "SELECT mdf AS mdf,tipospd AS cabecera "
                 . "FROM webunificada_fftt.planta_speedy WHERE 1 = 1 ";
            $strWhere = '';
            if (!empty($arrMdf)) {
                $indice = 0;
                $numElementos = count($arrMdf);
                foreach ($arrMdf as $key=>$value) {
                    if ($value != '') {
                        $valores .= "'" . $value . "'";
                        if ($indice < $numElementos - 1) {
                            $valores .= ",";
                        }
                    }
                    $indice++;
                }
                if ($valores != '') {
                    $strWhere .= "AND mdf IN (";
                    $strWhere .= $valores;
                    $strWhere .= ") AND tipospd != '' ";
                }
            }
            $strWhere .= "GROUP BY mdf,tipospd";
            $sql .= $strWhere;           
            $array = $conexion->query($sql);
            $array = $array->fetchAll(PDO::FETCH_ASSOC);
            return $array;
        } catch (PDOException $error) {
            return $error;
            exit();
        }
    }
}
