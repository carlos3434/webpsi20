<?php
/**
 * @copyright   GMD
 * @category    class
 * @name        Data_SubmodulosAccesos
 * @author      Gonzalo Chacaltana Buleje <gchacaltana@gmd.com.pe>
 * @package     class/data/
 * @version     1.0 17/05/2012 
 */

class Data_SubmodulosAccesos
{
    /**
     * Propiedades de la Clase Data_SubmodulosAccesos
     */
    
    /**
     * ID del registro de Submodulos Accesos.
     * @property  $_idAccesoSubmodulo
     * @access  protected
     * @var     int
     */
    
    protected $_idAccesoSubmodulo = '';
    
    /**
     * ID del submodulo.
     * @property  $_idSubmodulo
     * @access  protected
     * @var     int
     */
    
    protected $_idSubmodulo = '';
    
    /**
     * ID del tipo de perfil
     * @property  $_idPerfil
     * @access  protected
     * @var     int
     */
    
    protected $_idPerfil = '';
    
    /**
     * ID de la empresa.
     * @property  $_idEmpresa
     * @access  protected
     * @var     int
     */
    
    protected $_idEmpresa = '';
    
    /**
     * ID del area.
     * @property  $_idArea
     * @access  protected
     * @var     int
     */
    
    protected $_idArea = '';
    
    /**
     * ID de la zonal.
     * @property  $_idZonal
     * @access  protected
     * @var     int
     */
    
    protected $_idZonal = '';
    
    /**
     * Indicador de estado del registro.
     * @property  $_flag
     * @access  protected
     * @var     int
     */
    
    protected $_flag = '';
    
    /**
     * Tabla a manipular.
     * @property  $_table
     * @access  protected
     * @var     string
     */
    
    protected $_table = 'webunificada.submodulosAccesos';
    
    public function getIdAccesoSubmodulo()
    {
        return $this->_idAccesoSubmodulo;
    }

    public function setIdAccesoSubmodulo($idAccesoSubmodulo)
    {
        $this->_idAccesoSubmodulo = $idAccesoSubmodulo;
    }

    public function getIdSubmodulo()
    {
        return $this->_idSubmodulo;
    }

    public function setIdSubmodulo($idSubmodulo)
    {
        $this->_idSubmodulo = $idSubmodulo;
    }

    public function getIdPerfil()
    {
        return $this->_idPerfil;
    }

    public function setIdPerfil($idPerfil)
    {
        $this->_idPerfil = $idPerfil;
    }

    public function getIdEmpresa()
    {
        return $this->_idEmpresa;
    }

    public function setIdEmpresa($idEmpresa)
    {
        $this->_idEmpresa = $idEmpresa;
    }

    public function getIdArea()
    {
        return $this->_idArea;
    }

    public function setIdArea($idArea)
    {
        $this->_idArea = $idArea;
    }

    public function getIdZonal()
    {
        return $this->_idZonal;
    }

    public function setIdZonal($idZonal)
    {
        $this->_idZonal = $idZonal;
    }

    public function getFlag()
    {
        return $this->_flag;
    }

    public function setFlag($flag)
    {
        $this->_flag = $flag;
    }

    public function getTable()
    {
        return $this->_table;
    }
    
    /**
     * Constructor de la Clase.
     */
    public function __construct()
    {
    }
    
    /**
     * Metodo que valida si un usuario segun su perfil tiene
     * permisos para acceder a un submodulo.
     * @method verificarAccesoSubmodulo
     * @param object $conexion, Objeto de conexion.
     * @param int $idSubmodulo, Id del submodulo.
     * @param int $idPerfil, Id del perfil.
     * @param int $idEmpresa, Id Empresa.
     * @param int $idArea, Id Area.
     * @param int $idzonal, Id de la zonal.
     * @return bool, true or false.
     */
    public function verificarAccesoSubmodulo($conexion, $idPerfil, 
        $idEmpresa, $idArea = 0, $idzonal = 0)
    {
        (string)$sql = "";
        $permiso = 0;
        (int)$idSubmoduloActual = 0;
        /**
         * Obteniendo el Id del submodulo actual.
         */
        if (isset($_SESSION['USUARIO_MENU'])) {
            foreach ($_SESSION['USUARIO_MENU'] as $row) {
                if ($row->__get('_urlModulo').$row->__get('_urlSubModulo') == $_SESSION['SUBMODULO_ACTUAL']) {
                    $idSubmoduloActual = $row->__get('_idSubModulo');
                    break;
                }
            }
        }
        
        if ($idSubmoduloActual == 0) {
            echo "Error al obtener el ID del submodulo actual.";
            exit;
        }
        
        $sql = "SELECT IF (COUNT(*)>0,1,0) AS estadoAcceso 
        FROM ".$this->getTable()." 
        WHERE idsubmodulo=:idsubmodulo 
        AND IF (idperfil!=0,(idperfil=:idperfil),0=0) 
        AND IF (idempresa!=0,(idempresa=:idempresa),0=0) 
        AND IF (idarea!=0,(idarea=:idarea),0=0) 
        AND IF (idzonal!=0,(idzonal=:idzonal),0=0)";
		
		echo $sql;

        try {
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idsubmodulo", $idSubmoduloActual, PDO::PARAM_INT);
            $bind->bindParam(":idperfil", $idPerfil, PDO::PARAM_INT);
            $bind->bindParam(":idempresa", $idEmpresa, PDO::PARAM_INT);
            $bind->bindParam(":idarea", $idArea, PDO::PARAM_INT);
            $bind->bindParam(":idzonal", $idzonal, PDO::PARAM_INT);
            $bind->execute();
            foreach ($bind->fetchAll(PDO::FETCH_ASSOC) as $row) :
                if ( $row['estadoAcceso'] == '1' ) {
                    $permiso = 1;
                }
            endforeach;
            return $permiso;
        } catch (PDOException $e) {
            echo "Error al obtener permiso de acceso a submodulo.";
        }
    }
    
    /**
     * Elimino todos los accesos asignados al submodulo
     *
     * @author Wilder Sandoval H. <wsandoval@gmd.com.pe>
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve la exception
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idSubmodulo id del submodulo
     *
     * Agregado el 17/07/2012
     */
    public function eliminarSubmoduloAccesos($conexion, $idSubmodulo)
    {
        try {
            $sql = "DELETE FROM " . $this->getTable() . " 
                WHERE idsubmodulo = :idsubmodulo";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idsubmodulo", $idSubmodulo);
            $bind->execute();
    
            return 1;
        }catch(PDOException $e) {
            throw $e;
        }
    }
    
    /**
     * Inserta un acceso al submodulo
     *
     * @author Wilder Sandoval H. <wsandoval@gmd.com.pe>
     * @return boolean 1 si lleva a cabo la actualizacion,
     * sino devuelve la exception
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param array $arrDatos array asociativo con los datos a insertar
     * 
     * Agregado el 17/07/2012
     */
    public function grabarSubmoduloAccesos($conexion, $arrDatos)
    {
        try {
            $sql = "INSERT INTO " . $this->getTable() 
                    .  " (idsubmodulo, idarea, flag)
                    VALUES(:idsubmodulo, :idarea, :flag)";
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idsubmodulo", $arrDatos['idSubmodulo']);
            $bind->bindParam(":idarea", $arrDatos['idArea']);
            $bind->bindParam(":flag", $arrDatos['flag']);
            $bind->execute();
    
            return 1;
        } catch(PDOException $e) {
            throw $e;
        }
    }
    
    /**
     * Listado de accesos de submodulos
     *
     * @author Wilder Sandoval H. <wsandoval@gmd.com.pe>
     * @return array 
     * @param object $conexion Objeto de conexion a la BD creada con PDO
     * @param int $idSubmodulo id del submodulo
     * @param int $flag flag: habilitado o deshabilitado
     *
     * Agregado el 18/07/2012
     */
    public function listarSubmoduloAccesos($conexion, $idSubmodulo, $flag=1)
    {    
        try {
            
            $strWhere = "";
            if ( $flag != '' ) {
                $strWhere .= " AND flag = :flag ";
            }
            
            $sql = "SELECT idAccesoSubmodulo, idsubmodulo, idperfil, 
                    idempresa, idarea, idzonal, flag 
                    FROM " . $this->getTable() .  " 
                    WHERE idsubmodulo = :idsubmodulo " . $strWhere;
            
            $bind = $conexion->prepare($sql);
            $bind->bindParam(":idsubmodulo", $idSubmodulo);
            if ( $flag != '' ) {
                $bind->bindParam(":flag", $flag);
            }
            $bind->execute();
            
            $array = array();
            foreach ($bind->fetchAll(PDO::FETCH_ASSOC) as $row) {
                if ( $row['idsubmodulo'] != '' ) {
                    $objSubmodulosAccesos = new Data_SubmodulosAccesos();
                    $objSubmodulosAccesos->setIdAccesoSubmodulo(
                        $row['idsubmodulo']
                    );
                    $objSubmodulosAccesos->setIdSubmodulo($row['idsubmodulo']);
                    $objSubmodulosAccesos->setIdPerfil($row['idperfil']);
                    $objSubmodulosAccesos->setIdEmpresa($row['idempresa']);
                    $objSubmodulosAccesos->setIdArea($row['idarea']);
                    $objSubmodulosAccesos->setIdZonal($row['idzonal']);
                    $objSubmodulosAccesos->setFlag($row['flag']);
                    
                    $array[] = $objSubmodulosAccesos;
                }
            }
            
            return $array;
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
}