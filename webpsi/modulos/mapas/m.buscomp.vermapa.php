<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.vermapa.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$isLogin = 0;
$dirModulo = '../../';

if (isset($_REQUEST['search'])) {

    $_REQUEST['x'] = ( isset($_REQUEST['x']) && $_REQUEST['x'] != '' ) ? 
        $_REQUEST['x'] : '-77.0407839355469';
    $_REQUEST['y'] = ( isset($_REQUEST['y']) && $_REQUEST['y'] != '' ) ? 
        $_REQUEST['y'] : '-12.0692083678587';


    $objTerminales = new Data_FfttTerminales();
    $objCms = new Data_FfttCms();
    $objCapas = new Data_FfttCapas();

    $arrDataMarkers = array();
    $arrDataMarkersImg = array();

    //marker seleccionado -> icon por default
    $arrDataMarkersImg['pto'] = 'markergreen.png';
    $arrDataMarkers[] = array(
        'title' => 'pto',
        'y' => $_REQUEST['y'],
        'x' => $_REQUEST['x'],
        'i' => 1,
        'detalle' => 'Punto seleccionado',
        'tip' => 'pto'
    );

    $i = 2;
    if (isset($_REQUEST['comp']['trm'])) {

        $arrDataMarkersImg['trm'] = 'mobilephonetower.png';
        //carga datos de terminales
        $arrayTerminales = $objTerminales->getTerminalesByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );

        foreach ($arrayTerminales as $terminales) {

            if ($terminales['y'] != '' && $terminales['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Zonal:</b> ' . $terminales['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $terminales['mdf'] . '<br />';
                if ($terminales['tipo_red'] == 'D') {
                    $detalle .= '<b>Cable:</b> ' . $terminales['cable'] . '<br />';
                } elseif ($terminales['tipo_red'] == 'F') {
                    $detalle .= '<b>Armario:</b> ' . $terminales['armario'] . '<br />';
                }
                $detalle .= '<b>Caja:</b> ' . $terminales['caja'] . '<br />';
                $detalle .= '<b>Capacidad:</b> ' . $terminales['qcapcaja'] . '<br />';
                $detalle .= '<b>Pares libres:</b> ' . $terminales['qparlib'] . '<br />';
                $detalle .= '<b>Pares reserv.:</b> ' . $terminales['qparres'] . '<br />';
                $detalle .= '<b>Pares distrib.:</b> ' . $terminales['qdistrib'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $terminales['x'] . ',' . $terminales['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($terminales['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'S',
                    'dir' => 'terminal',
                    'title' => $terminales['caja'],
                    //'estado' => $terminales['estado'],
                    'qparlib' => $terminales['qparlib'],
                    'y' => $terminales['y'],
                    'x' => $terminales['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'trm'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['arm'])) {

        $arrDataMarkersImg['arm'] = 'powersubstation.png';

        //carga datos de armarios
        $arrayArmarios = $objTerminales->getArmariosByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayArmarios as $armarios) {

            if ($armarios['y'] != '' && $armarios['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Zonal:</b> ' . $armarios['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $armarios['mdf'] . '<br />';
                $detalle .= '<b>Armario:</b> ' . $armarios['armario'] . '<br />';
                $detalle .= '<b>Capacidad:</b> ' . $armarios['qcaparmario'] . '<br />';
                $detalle .= '<b>Pares libres:</b> ' . $armarios['qparlib'] . '<br />';
                $detalle .= '<b>Pares reserv.:</b> ' . $armarios['qparres'] . '<br />';
                $detalle .= '<b>Pares distrib.:</b> ' . $armarios['qdistrib'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $armarios['x'] . ',' . $armarios['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($armarios['distance'], 2) . 'km.</div>';


                $arrDataMarkers[] = array(
                    'fftt' => 'S',
                    'dir' => 'armario',
                    'title' => $armarios['armario'],
                    'estado' => $armarios['estado'],
                    'y' => $armarios['y'],
                    'x' => $armarios['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'arm'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['mdf'])) {

        $arrDataMarkersImg['mdf'] = 'tent.png';

        //carga datos de mdfs
        $arrayMdfs = $objTerminales->getMdfsByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayMdfs as $mdfs) {

            if ($mdfs['y'] != '' && $mdfs['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Zonal:</b> ' . $mdfs['zonal'] . '<br />';
                $detalle .= '<b>Mdf:</b> ' . $mdfs['mdf'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $mdfs['direccion'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $mdfs['x'] . ',' . $mdfs['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($mdfs['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'S',
                    'dir' => 'mdf',
                    'title' => $mdfs['mdf'],
                    'estado' => $mdfs['estado'],
                    'y' => $mdfs['y'],
                    'x' => $mdfs['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'mdf'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['trb'])) {

        $arrDataMarkersImg['trb'] = 'tent.png';

        //carga datos de taps
        $arrayTroba = $objCms->getTrobasByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayTroba as $troba) {

            if ($troba['y'] != '' && $troba['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Nodo:</b> ' . $troba['nodo'] . '<br />';
                $detalle .= '<b>Troba:</b> ' . $troba['troba'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $troba['x'] . ',' . $troba['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($troba['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'S',
                    'dir' => 'trb',
                    'title' => $troba['troba'],
                    'estado' => '1',
                    'y' => $troba['y'],
                    'x' => $troba['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'trb'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['tap'])) {

        $arrDataMarkersImg['tap'] = 'tent.png';

        //carga datos de taps
        $arrayTap = $objCms->getTapsByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayTap as $taps) {

            if ($taps['y'] != '' && $taps['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Nodo:</b> ' . $taps['nodo'] . '<br />';
                $detalle .= '<b>Troba:</b> ' . $taps['troba'] . '<br />';
                $detalle .= '<b>Amplificador:</b> ' . $taps['amplificador'] . '<br />';
                $detalle .= '<b>Tap:</b> ' . $taps['tap'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $taps['x'] . ',' . $taps['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($taps['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'S',
                    'dir' => 'mdf',
                    'title' => $taps['tap'],
                    'estado' => '1',
                    'y' => $taps['y'],
                    'x' => $taps['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'tap'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['edi'])) {
        
        $objEdificio = new Data_FfttEdificio();

        $arrDataMarkersImg['edi'] = 'edificio';

        //carga datos de edificios
        //$arrayEdificios = $objTerminales->getEdificiosByXy(
        //    $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        //;
        
        //Busca todos los edificios sin importar su region, zonal, empresa
        //pero solo podra editar los que le pertenece
        $arrayEdificios = $objEdificio->buscarEdificiosPorXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayEdificios as $edificios) {

            if ($edificios['y'] != '' && $edificios['x'] != '') {

                $detalle = '<div class="clsInfoMap"><img src="../../pages/imagenComponente.php?imgcomp=' . $edificios['foto1'] . '&dircomp=edi&w=120"><br />';
                $detalle .= '<b>Edificio:</b> ' . $edificios['item'] . '<br />';
                $detalle .= '<b>URA:</b> ' . $edificios['ura'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $edificios['direccion_obra'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $edificios['x'] . ',' . $edificios['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($edificios['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'N',
                    'dir' => 'edificio',
                    'title' => $edificios['item'],
                    'estado' => $edificios['estado'],
                    'y' => $edificios['y'],
                    'x' => $edificios['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'edi'
                );
                $i++;
            }
        }
    }

    if (isset($_REQUEST['comp']['com'])) {

        $arrDataMarkersImg['com'] = 'competencia.png';

        //carga datos de competencias
        $arrayCompetencias = $objTerminales->getCompetenciasByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayCompetencias as $competencias) {

            if ($competencias['y'] != '' && $competencias['x'] != '') {

                $detalle = '<div class="clsInfoMap"><img src="../../pages/imagenComponente.php?imgcomp=' . $competencias['foto1'] . '&dircomp=com&w=120"><br />';
                $detalle .= '<b>Competencia:</b> ' . $competencias['idcompetencia'] . '<br />';
                $detalle .= '<b>URA:</b> ' . $competencias['ura'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $competencias['direccion'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $competencias['x'] . ',' . $competencias['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($competencias['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'N',
                    'dir' => 'competencia',
                    'title' => $competencias['idcompetencia'],
                    'estado' => $competencias['estado'],
                    'y' => $competencias['y'],
                    'x' => $competencias['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'com'
                );
                $i++;
            }
        }
    }


    if (isset($_REQUEST['comp']['esb'])) {

        $arrDataMarkersImg['esb'] = 'estacionbase';

        //carga datos de edificios
        $arrayEstacionBase = $objTerminales->getEstacionbasesByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        foreach ($arrayEstacionBase as $estacionbase) {

            if ($estacionbase['y'] != '' && $estacionbase['x'] != '') {

                $detalle = '<div class="clsInfoMap"><b>Estacion Base:</b> ' . $estacionbase['nombre'] . '<br />';
                $detalle .= '<b>Direccion:</b> ' . $estacionbase['direccion'] . '<br />';
                $detalle .= '<b>Departamento:</b> ' . $estacionbase['departamento'] . '<br />';
                $detalle .= '<b>Provincia:</b> ' . $estacionbase['provincia'] . '<br />';
                $detalle .= '<b>Distrito:</b> ' . $estacionbase['distrito'] . '<br /><br />';
                $detalle .= '<b>X,Y:</b> ' . $estacionbase['x'] . ',' . $estacionbase['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($estacionbase['distance'], 2) . 'km.</div>';

                $arrDataMarkers[] = array(
                    'fftt' => 'N',
                    'dir' => 'estacionbase',
                    'title' => $estacionbase['nombre'],
                    'estado' => 'estado',
                    'y' => $estacionbase['y'],
                    'x' => $estacionbase['x'],
                    'i' => $i,
                    'detalle' => $detalle,
                    'tip' => 'esb'
                );
                $i++;
            }
        }
    }


    if (isset($_REQUEST['capa']) && !empty($_REQUEST['capa'])) {

        $arrCapas = $objCapas->getCapas($conexion);
        $arrayCapa = array();
        foreach ($arrCapas as $capa) {
            $arrayCapa[$capa['idcapa']] = array(
                'nombre' => $capa['nombre'],
                'abv_capa' => $capa['abv_capa'],
                'ico' => $capa['ico']
            );
        }


        foreach ($_REQUEST['capa'] as $idCapa => $abvCapa) {

            //almaceno las imagenes segun la capa
            $arrDataMarkersImg[$abvCapa] = $arrayCapa[$idCapa]['ico'];

            //carga de campos segun la capa
            $camposCapaArr = $objCapas->getCamposByCapa($conexion, $idCapa);
            $arrDataCampos = array();
            foreach ($camposCapaArr as $campoCapa) {
                $arrDataCampos['campo' . $campoCapa['campo_nro']] = $campoCapa['campo'];
            }

            //carga datos de capa = casa, tienda, cabina
            $arrayCapas = $objCapas->getCapasByXy($conexion, $idCapa, $arrDataCampos, $_REQUEST['x'], $_REQUEST['y'], 10, 10);
            foreach ($arrayCapas as $capas) {

                $detalle = '<div class="clsInfoMap"><img src="../../pages/imagenComponente.php?imgcomp=' . $capas['foto1'] . '&dircomp=' . $arrayCapa[$idCapa]['abv_capa'] . '&w=120"><br />';
                foreach ($arrDataCampos as $nroCampo => $campo) {
                    $detalle .= '<b>' . $campo . ':</b> ' . $capas[$nroCampo] . '<br />';
                }
                $detalle .= '<br /><b>X,Y:</b> ' . $capas['x'] . ',' . $capas['y'] . '<br />';
                $detalle .= '<b>Distancia:</b> ' . number_format($capas['distance'], 2) . 'km.</div>';

                if ($capas['y'] != '' && $capas['x'] != '') {
                    $arrDataMarkers[] = array(
                        'title' => $capas['campo1'],
                        'y' => $capas['y'],
                        'x' => $capas['x'],
                        'i' => $i,
                        'detalle' => utf8_encode($detalle),
                        'tip' => $abvCapa
                    );
                    $i++;
                }
            }
        }
    }


    $capaSelected = array(
        'capa' => '',
        'zoom_sel' => 11
    );

    if (isset($_REQUEST['capa_selected']) && $_REQUEST['capa_selected'] != '') {
        $capaSelected = array(
            'capa' => $_REQUEST['capa_selected'],
            'zoom_sel' => $_REQUEST['zoom_selected']
        );
    }
    
    $arrDataResult = array(
        'sites' => $arrDataMarkers,
        'punto_sel' => array('x' => $_REQUEST['x'], 'y' => $_REQUEST['y']),
        'capa_ico' => $arrDataMarkersImg,
        'comps_sel' => $_REQUEST['comp'],
        'capas_sel' => ( isset($_REQUEST['capa']) ) ? $_REQUEST['capa'] : array()
    );

}

include 'vistas/m.v_buscomp.vermapa.php';