<?php
require_once("clases/class.UsuarioModulos.php");
require_once("clases/class.UsuarioModulosAdmin.php");

session_start();
//@ is authorized?
if (empty($_SESSION['exp_user']) || @$_SESSION['exp_user']['expires'] < time()) {
	$RUTA = "/enviosms_final/";
    header("location:".$RUTA."login/login.html"); //@ redirect
} else {
    $_SESSION['exp_user']['expires'] = time() + (45 * 60); //@ renew 45 minutes
}

$IDUSUARIO = $_SESSION['exp_user']['id'];
$USUARIO = $_SESSION['exp_user']['usuario'];
$RUTA = "/enviosms_final/";

function pintar_cabecera() {
 
    $IDUSUARIO = $_SESSION['exp_user']['id'];
    $USUARIO = $_SESSION['exp_user']['usuario'];
    global $RUTA;
	$ruta = $RUTA;
	
	$UM = new UsuarioModulos();
	$UMA = new UsuarioModulosAdmin();
	$arrModulos = $UM->ListadoModulosUsuario($IDUSUARIO);
	$arrModulosAdmin = $UMA->ListadoModulosAdminUsuario($IDUSUARIO);
		
	/*echo 	$_SESSION['IS_MOBILE'] ;
	echo    $_SESSION['PREF_MOBILE'] ;
	*/
	
	?>

	<input type="hidden" name="txt_ruta" id="txt_ruta" value='<?=$ruta?>' />
<div id="top-bar">
	<a href="#" onclick="home();">
		<img src="<?=$RUTA?>/imagenes/logo-movistar.jpg" alt="DZone" class="floatleft" width="200" height="55"  />
	</a>
	<div id="right-side">
		<img src="<?=$RUTA?>/menu/images/usericon.jpg" alt="user icon" />&ensp;
		<a href="#" class="first"><?php echo $USUARIO; ?></a>&ensp;
		<a href="<?=$RUTA?>/cerrar_sesion.php?logout=555">Salir</a> &emsp;
		<!--<form id="main-search">  -->
		<!-- <label for="search-field" id="search-field-label">Search</label> -->
		<!-- <input type="text" tabindex="1" maxlength="255" id="search-field"/>
		<!--<input type="image" alt="Search" value="Search" src="images/search.png" id="search-button"/>  -->
		<!-- </form> -->
		
	</div>
</div>

	

<div class="blue">  
<ul id="mega-menu-4" class="mega-menu">
	<li><a href="/enviosms_final/main.php">Inicio</a></li>
	<li><a href="#">Modulos</a>
		<ul>
			<?php
			$i=0;
			//var_dump($arrModulos);
			foreach ($arrModulos as $modulo) {
				$arrSubModulos = $UM->ListadoSubModulosUsuario($modulo['idmodulo'], $IDUSUARIO);
			?>
			<li><a href="#"><?=$modulo["modulo"]?></a>
				<ul>
				<?php
				foreach ($arrSubModulos as $submodulo) {
					?>				
					<li><a href="#" onclick="abrir_pagina('<?=$submodulo["path"]?>', '<?=$_SESSION['PREF_MOBILE']?>'  )" ><?=$submodulo["submodulo"]?></a></li>
					<?php
				}
					?>
				</ul>
			</li>
			<?php
			}
			?>

		</ul>
	</li>
	
		<li><a href="#">Administracion</a>
		<ul>
			<?php
			$i=0;
			//var_dump($arrModulosAdmin);
			foreach ($arrModulosAdmin as $moduloAdmin) {
				$arrSubModulosAdmin = $UMA->ListadoSubModulosAdminUsuario($moduloAdmin['idmodulo_admin'], $IDUSUARIO);
			?>
			<li><a href="#"><?=$moduloAdmin["modulo_admin"]?></a>
				<ul>
				<?php
				foreach ($arrSubModulosAdmin as $submoduloAdmin) {
					?>				
					<li>
						<a href="#" onclick="abrir_pagina('<?=$submoduloAdmin["path"]?>', '<?=$_SESSION['PREF_MOBILE']?>'  )" >
							<?=$submoduloAdmin["submodulo_admin"]?></a>
					</li>
					<?php
				}
					?>
				</ul>
			</li>
			<?php
			}
			?>

		</ul>
	</li>

<li><a href="#">Mi Cuenta</a>
<ul>
	<li><a href="#">Mis Datos</a></li>
	<li><a href="#">Cambiar Contrase&ntilde;a</a></li>
	<li><a href="<?=$RUTA?>/cerrar_sesion.php?logout=555" >Salir</a></li>
</ul>
</li>

</div>

<!--<div style="height: 350px;"></div>	-->
			
			
			


    <?php
}
?>