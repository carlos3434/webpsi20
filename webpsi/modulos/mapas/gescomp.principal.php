<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.principal.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 * @version     2.0 - 2013/10/11 -- Adecuaciones por Sergio Miranda
 */

//require_once "../../config/web.config.php";

define("APP_FDR", "webpsi/");
define("APP_DIR", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDR);
define("APP_URL", "http://" . $_SERVER['SERVER_NAME'] . APP_FDR);

$inc = APP_DIR."includes.php";

include_once $inc;
require_once 'autoload.php';

//require_once APP_DIR . 'session.php';

function vistaPrincipal()
{
    global $conexion;

    //include "vistas/v_gescomp.principal.php";
	echo pintar_cabecera(); 
	?>
	<iframe src="vistas/v_gescomp_poligono.ifr.php" width="1200px" height="650px">
	<?php
	//include "vistas/v_gescomp_poligono.ifr.php";
}

//carga de la vista principal
vistaPrincipal();