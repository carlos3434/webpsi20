<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.terminal.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 * @version     2.0 - 2013/05/08
 */

//require_once "../../config/web.config.php";
include_once  'autoload.php';
//require_once APP_DIR . 'session.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');
    
    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }
    
    return '#' . $color;
}

function buscarTerminales($_POST)
{
    global $conexion;

    $objZonal = new Data_Zonal();
    $objTerminales = new Data_FfttTerminales();
    
    //carga de datos (x,y) de Zonal
    $arrZonal = $objZonal->listar($conexion, '', $_POST['IDzonal']);
    $arrDataZonal = array(
        'abv'   => $arrZonal[0]->__get('_abvZonal'),
        'name'  => $arrZonal[0]->__get('_descZonal'),
        'x'     => $arrZonal[0]->__get('_x'),
        'y'     => $arrZonal[0]->__get('_y')
    );
    
    //carga de datos (x,y) de Mdf
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByCodMdf($conexion, $_POST['IDmdf']);
    $arrDataMdf = array(
        'abv'    => $arrMdf[0]['mdf'],
        'name'    => $arrMdf[0]['nombre'],
        'estado'=> $arrMdf[0]['estado'],
        'x'     => $arrMdf[0]['x'],
        'y'     => $arrMdf[0]['y']
    );
    
    $arrDataArmario = array();
    if ( $_POST['IDtipred'] == 'F' ) {
        //carga de datos (x,y) de Armario
        $arrArmario = $objTerminales->getArmarioByCodArmario(
            $conexion, $_POST['IDzonal'], $_POST['IDmdf'], 
            $_POST['IDcableArmario']
        );
        $arrDataArmario = array(
            'zonal'   => $arrArmario[0]['zonal'],
            'mdf'     => $arrArmario[0]['mdf'],
            'armario' => $arrArmario[0]['armario'],
            'x'       => $arrArmario[0]['x'],
            'y'       => $arrArmario[0]['y'],
            'estado'  => $arrArmario[0]['estado'],
        );
    }
    
    
    $arrTerminales = $objTerminales->getTerminales(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred'], 
        $_POST['IDcableArmario'], $_POST['datosXY']
    );

    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrTerminales as $terminales) {
        if ( $terminales['y'] != '' && $terminales['x'] != '' ) {
            $arrDataMarkersConXy[] = $terminales;
        }

        $arrDataMarkers[] = $terminales;
        $i++;
    }
    
    
    $arrPoligonos = array();
    $promX = $promY = 0;
    // ======================== DATA DE POLIGONOS =========================== //
    if ( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
        $objCapasArea = new Data_FfttCapasArea();

        $idMdf = $_POST['IDmdf'];
        
        $puntosXyMdf = $objCapasArea->getXysByTipoCapaAndElemento(
            $conexion, 'MDF', $idMdf
        );
        $arrPoligonos[] = array(
            'coords' => $puntosXyMdf,
            'color'     => generaColorPoligono()
        ); 

        
        $countXy = count($arrPoligonos[0]['coords']);
        
        $promX = $promY = 0;
        if ( $countXy > 0 ) {
            $sumX = $sumY = 0;
            foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
            }
            
            $promX = $sumX / $countXy;
            $promY = $sumY / $countXy;
        }

    }
    // ========================= FIN DATA DE POLIGONOS ====================== //
    
    
    $arrEstadosTerminales = array(
        array('id'=>'1', 'val'=>'Estado 1'),
        array('id'=>'2', 'val'=>'Estado 2'),
        array('id'=>'3', 'val'=>'Estado 3')
    );
    $arrEstados = array(
        '1' => 'Estado 1',
        '2' => 'Estado 2',
        '3' => 'Estado 3'
    );
    
    $dataResult = array(
        'zonal'             => $arrDataZonal,
        'mdf'               => $arrDataMdf,
        'armario'           => $arrDataArmario,
        'estados'           => $arrEstadosTerminales,
        'est'               => $arrEstados,
        'edificios'         => $arrDataMarkers,
        'edificios_con_xy'  => $arrDataMarkersConXy,
        'xy_poligono'       => $arrPoligonos,
        'center_map'        => array(
            'x' => $promX,
            'y' => $promY
        )
    );
    
    echo json_encode($dataResult);
    exit;
}

function buscar($_POST)
{
    global $conexion;

    $arrPoligonos = array();
    $promX = $promY = 0;
    // ============== DATA DE POLIGONOS ===================================== //
    if ( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
        $objCapasArea = new Data_FfttCapasArea();

        $idMdf = $_POST['IDmdf'];
        
        $puntosXyMdf = $objCapasArea->getXysByTipoCapaAndElemento(
            $conexion, 'MDF', $idMdf
        );
        $arrPoligonos[] = array(
            'coords' => $puntosXyMdf,
            'color'  => generaColorPoligono()
        ); 

        
        $countXy = count($arrPoligonos[0]['coords']);
        
        $promX = $promY = 0;
        if ( $countXy > 0 ) {
            $sumX = $sumY = 0;
            foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
            }
            
            $promX = $sumX / $countXy;
            $promY = $sumY / $countXy;
        }
        
        $dataResult = array(
            'xy_poligono' => $arrPoligonos,
            'center_map'  => array(
                'x' => $promX,
                'y' => $promY
            )
        );
    
        echo json_encode($dataResult);
        exit;
        
    }
    // ================== FIN DATA DE POLIGONOS ============================= //
}

function save($_POST, $_GET)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();

    $arrTerminales = $objTerminales->getTerminales(
        $conexion, $_GET['IDzonal'], $_GET['IDmdf'], $_GET['IDtipred'], 
        $_GET['IDcableArmario']
    );

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $ipUsuario = Logic_Utils::getIP();

    $arrTerminalesSelected = array();
    foreach ( $_POST['xy'] as $pktrm => $valtrm ) {
        if ( $valtrm['x'] != '' || $valtrm['y'] != '' ) {

            $arrTerminalesSelected[$pktrm] = array(
                'x' => $valtrm['x'],
                'y' => $valtrm['y']
            );
        }
    }

    //update solo a los que surgieron cambios para fftt_terminales_historial
    $flag = 1;
    foreach ( $arrTerminales as $terminal ) {

        if ( array_key_exists($terminal['mtgespktrm'], $arrTerminalesSelected) ) {
            if ( $terminal['x'] != $arrTerminalesSelected[$terminal['mtgespktrm']]['x'] ||
                    $terminal['y'] != $arrTerminalesSelected[$terminal['mtgespktrm']]['y'] ) {
                
                //grabando los xy en fftt_terminales
                $result = $objTerminales->updateXY(
                    $conexion, $terminal['mtgespktrm'], 
                    $arrTerminalesSelected[$terminal['mtgespktrm']]['x'], 
                    $arrTerminalesSelected[$terminal['mtgespktrm']]['y']
                );
                
                //wsandoval 11/07/2012
                //Actualizo tambien xy en fftt_directa/fftt_secundaria
                if ( $_GET['IDtipred'] == 'D' ) {
                    $objTerminales->actualizarXyFfttDirecta(
                        $conexion, $terminal['zonal1'], $terminal['mdf'], 
                        $terminal['cable'], $terminal['caja'],
                        $arrTerminalesSelected[$terminal['mtgespktrm']]['x'],
                        $arrTerminalesSelected[$terminal['mtgespktrm']]['y']
                    );
                } elseif ( $_GET['IDtipred'] == 'F' ) {
                    $objTerminales->actualizarXyFfttSecundaria(
                        $conexion, $terminal['zonal1'], $terminal['mdf'],
                        $terminal['armario'], $terminal['caja'],
                        $arrTerminalesSelected[$terminal['mtgespktrm']]['x'],
                        $arrTerminalesSelected[$terminal['mtgespktrm']]['y']
                    );
                }
                //fin wsandoval 11/07/2012
                
                if ( !$result ) {
                    $flag = 0;
                    break;
                }

                //grabando en el historial
                $objTerminales->updateXYhistorial(
                    $conexion, $terminal['mtgespktrm'], 
                    $terminal['x'], $terminal['y'], $idUsuario, $ipUsuario
                );
                
            }
        }
    }

    $dataResult = ($flag) ?
    array('success'=>1,'msg'=>'Datos grabados correctamente.') : 
    array('success'=>0,'msg'=>'Error al grabar los datos.');

    echo json_encode($dataResult);
    exit;
}

function buscarCableArmario($_POST)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $arrArmarioCable = $objTerminales->getCableArmarioByZonalAndMdfAndTipoRed(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred']
    );

    $dataResult = '<select id="selCableArmario">';
    foreach ($arrArmarioCable as $armarioCable) {
        $dataResult .= '<option value="' . $armarioCable['cablearmario'] . 
        '">' . $armarioCable['cablearmario'] . '</option>';
    }
    $dataResult .= '</select>';

    echo $dataResult;
    exit;
}

function buscarCableArmario_XY_Clientes($_POST)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $arrArmarioCable = $objTerminales->getCableArmarioByZonalAndMdfAndTipoRed(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred']
    );

    $dataResult = "<select id='selCableArmario_AsignarXY_Clientes' onchange='changeCableArmario_AsignarXY_Clientes();' >";
    foreach ($arrArmarioCable as $armarioCable) {
        $dataResult .= '<option value="' . $armarioCable['cablearmario'] . 
        '">' . $armarioCable['cablearmario'] . '</option>';
    }
    $dataResult .= '</select>';

    echo $dataResult;
    exit;
}

function buscarTerminales_XY_Clientes($_POST)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales_Smc();
    $arrTerminales = $objTerminales->getTerminalesByZonalMdfTipoRedArmCable(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred'], $_POST['IDcableArmario']
    );
    //var_dump($arrTerminales);

    $dataResult = '<select id="selCajaTerminal_AsignarXY_Clientes">';
    foreach ($arrTerminales as $cajaTerminal) {
        $dataResult .= '<option value="' . $cajaTerminal['caja'] . 
        '">' . $cajaTerminal['caja'] . '</option>';
    }
    $dataResult .= '</select>';

    echo $dataResult;
    exit;
}

function buscarMdf($_POST)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();
    
	// SERGIO 2013-05-08 --> POR EL MOMENTO NO BUSCAR MDFS POR USUARIO
    //$idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
	$idUsuario = 1235;
    $arrMdf = $objTerminales->getMdfByZonalAndIdUsuario(
        $conexion, $_POST['IDzon'], $idUsuario
    );

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
                'IDmdf'     => $mdf['mdf'],
                'Descmdf'    => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
}

function buscarMdf_AsignarXY_Clientes($_POST)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();
    
    // SERGIO 2013-05-08 --> POR EL MOMENTO NO BUSCAR MDFS POR USUARIO
    //$idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $idUsuario = 1235;
    $arrMdf = $objTerminales->getMdfByZonalAndIdUsuario(
        $conexion, $_POST['IDzon'], $idUsuario
    );

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
                'IDmdf'     => $mdf['mdf'],
                'Descmdf'    => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
}




function listarZonales()
{
    $arrZonal = $_SESSION['USUARIO_ZONAL'];

    $arrData = array();
    foreach ($arrZonal as $objZonal) {
        $arrData[] = array(
            'abvZonal'     => $objZonal->__get('_abvZonal'),
            'descZonal'    => $objZonal->__get('_descZonal')
        );
    }
    echo json_encode($arrData);
    exit;
}

function vistaInicial()
{
    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" ' . 
        'src="modulos/maps/vistas/v_gescomp_terminal.ifr.php" ' .
        'width="100%" height="480"></iframe>';
    
}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'buscarTerminales':
        buscarTerminales($_POST);
        break;
    case 'buscar':
        buscar($_POST);
        break;
    case 'save':
        save($_POST, $_GET);
        break;
    case 'buscarCableArmario':
        buscarCableArmario($_POST);
        break;
    case 'buscarCableArmario_XY_Clientes':
        buscarCableArmario_XY_Clientes($_POST);
        break;
    case 'buscarMdf':
        buscarMdf($_POST);
        break;
    case 'buscarMdf_AsignarXY_Clientes':
        buscarMdf_AsignarXY_Clientes($_POST);
        break;
    case 'buscarTerminales_XY_Clientes':
        buscarTerminales_XY_Clientes($_POST);
        break;
    case 'listarZonales':
        listarZonales();
        break;
    default:
        vistaInicial();
        break;
}