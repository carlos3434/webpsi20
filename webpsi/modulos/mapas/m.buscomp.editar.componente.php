<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.editar.componente.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

require_once APP_DIR . 'modulos/maps/util/functions.maps.php';



/*
if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'searchItem' ) {

    $objCapas = new Data_FfttCapas();
    
    $itemExists = $objCapas->getEdificioByItem(
        $conexion, $_POST['item'], $_POST['item_inicial']
    );
    
    if ( empty( $itemExists ) ) {
        $arrResult = array(
            'success'    => 1,
            'msg'        => 'Ok'
        );
    } else {
        $arrResult = array(
            'success'    => 0,
            'msg'        => 'Item existe'
        );
    }
    
    echo json_encode($arrResult);
    exit;

} elseif ( isset($_REQUEST['action']) && 
    $_REQUEST['action'] == 'GrabaEdificio' ) {

    $objCapas = new Data_FfttCapas();
    
    //formateando fecha para insert a db
    $_POST['fecha_registro_proyecto']      = ( $_POST['fecha_registro_proyecto'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_registro_proyecto'])) : '0000-00-00';
    $_POST['fecha_termino_construccion'] = ( $_POST['fecha_termino_construccion'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_termino_construccion'])) : '0000-00-00';
    $_POST['fecha_seguimiento']          = ( $_POST['fecha_seguimiento'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['fecha_seguimiento'])) : '0000-00-00';

    $_POST['montante_fecha']             = ( $_POST['montante_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['montante_fecha'])) : '0000-00-00';
    $_POST['ducteria_interna_fecha']     = ( $_POST['ducteria_interna_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['ducteria_interna_fecha'])) : '0000-00-00';
    $_POST['punto_energia_fecha']         = ( $_POST['punto_energia_fecha'] != '' ) ? 
        date('Y-m-d', strtotime($_POST['punto_energia_fecha'])) : '0000-00-00';
    
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $arrDataResult = $objCapas->updateEdificioComponente(
        $conexion, $_POST, $idUsuario
    );

    $arrResult = ($arrDataResult) ? 
        array('success'=>1,'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0,'msg'=>'Error al grabar los datos.');

    
    echo json_encode($arrResult);
    exit;
    
} elseif ( isset($_REQUEST['action']) && 
    $_REQUEST['action'] == 'GrabaCompetencia' ) {

    $objCapas = new Data_FfttCapas();
    
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    $arrDataResult = $objCapas->updateCompetenciaComponente(
        $conexion, $_POST, $idUsuario
    );

    $arrResult = ($arrDataResult) ? 
        array('success'=>1,'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0,'msg'=>'Error al grabar los datos.');

    
    echo json_encode($arrResult);
    exit;
    
}


if ( $_REQUEST['capa'] == 'edi' ) {
    
    $objCapas             = new Data_FfttCapas();
    $objTerminales        = new Data_FfttTerminales();
    $objEdificioEstado    = new Data_FfttEdificioEstado();
    
    $arrTipoCchh             = $objCapas->getDescriptorByGrupo($conexion, 1);
    $arrTipoProyecto         = $objCapas->getDescriptorByGrupo($conexion, 2);
    $arrTipoInfraestructura  = $objCapas->getDescriptorByGrupo($conexion, 3);
    $arrTipoVia              = $objCapas->getDescriptorByGrupo($conexion, 4);
    
        
    //si no hay x,y entonces listo todas los mdf de lima
    $arrMdfs = $objTerminales->getMdfByZonal($conexion, 'LIM');
    if ( $_REQUEST['x'] != '' && $_REQUEST['y'] != '' ) {
        $arrMdfs = $objTerminales->getMdfsByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5
        );
    }
        
    $edificio = $objCapas->getEdificioById($conexion, $_REQUEST['idedificio']);

    if ( empty($edificio) ) {
        echo '<p>Edificio no existe</p>';
        exit;
    }
    
    
    $codDep     = substr($edificio[0]['distrito'], 0, 2);
    $codProv    = substr($edificio[0]['distrito'], 2, 2);
    $arrDpto  = $objCapas->getDepartamentos($conexion);
    $arrProv  = $objCapas->getProvinciasByDpto($conexion, $codDep);
    $arrDist  = $objCapas->getDistritosByProv($conexion, $codDep, $codProv);
    
        
    //estados de los edificios
    $arrEstado = array();
    $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
        $conexion
    );
    if ( !empty($arrObjEdificioEstado) ) {
        foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
            $arrEstado[$objEdificioEstado->__get('_codEdificioEstado')] = $objEdificioEstado->__get('_desEstado');
        }
    }
    
    
    $arrEtapa = array(
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
        'F' => 'F',
        'G' => 'G',
        'H' => 'H',
        'I' => 'I',
        'J' => 'J',
        'K' => 'K',
        'L' => 'L',
        'M' => 'M',
        'N' => 'N',
        'O' => 'O',
        'P' => 'P',
        'Q' => 'Q',
        'R' => 'R',
        'S' => 'S',
        'T' => 'T',
        'U' => 'U',
        'V' => 'V',
        'W' => 'W',
        'X' => 'X',
        'Y' => 'Y',
        'Z' => 'Z'
    );
    
    $arrMeses = array(
        '01' => 'Enero',
        '02' => 'Febrero',
        '03' => 'Marzo',
        '04' => 'Abril',
        '05' => 'Mayo',
        '06' => 'Junio',
        '07' => 'Julio',
        '08' => 'Agosto',
        '09' => 'Setiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre'
    );

    $anoActual = date('Y');
    $mesActual = date('m');
    $diaActual = date('d');
    
    include 'vistas/m.v_buscomp.editar.componente.php'; 

} elseif ( $_REQUEST['capa'] == 'com' ) {

    $objCapas = new Data_FfttCapas();
    $objTerminales = new Data_FfttTerminales();

    //opciones multiple -> competencia
    $arrCompetencia             = $objCapas->getDescriptorByGrupo($conexion, 5);
    $arrElementoEncontrado     = $objCapas->getDescriptorByGrupo($conexion, 6);
    
    $arrMdfs = $objTerminales->getMdfsByXy(
        $conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5
    );
    
    $competencia = $objCapas->getCompetenciaById(
        $conexion, $_REQUEST['idcompetencia']
    );


    if ( empty($competencia) ) {
        echo '<p>Competencia no existe</p>';
        exit;
    }

    include 'vistas/m.v_buscomp.editar.componente.php'; 

}
*/

function editarEdificio ($idedificio, $iframe=0)
{
    try {
        global $conexion;
        
        $isLogin = 0;
        $dirModulo = '../../';

        //Etapas para proyecto Edificios [A-Z]
        $arrEtapaProyecto = array(
            'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E',
            'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I', 'J' => 'J',
            'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N', 'O' => 'O',
            'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T',
            'U' => 'U', 'V' => 'V', 'W' => 'W', 'X' => 'X', 'Y' => 'Y',
            'Z' => 'Z'
        );

        //variables para el formulario editar
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

        $objEdificio           = new Data_FfttEdificio();

        //capturo datos del edificio
        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idedificio);

        if ( empty($arrObjEdificio) ) {
            throw new PDOException();
        }


        $objEdificioEstado     = new Data_FfttEdificioEstado();
        $objCapas              = new Data_FfttCapas();
        $objTerminales         = new Data_FfttTerminales();
        $objUbigeo             = new Data_Ubigeo();
        $objUbigeoZonal        = new Data_UbigeoZonalEdi();
        $objGestionObra        = new Data_GestionObraEdi();
        $objRegion             = new Data_RegionEdi();
        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objSegmento           = new Data_SegmentoEdi();
        $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
        $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
        $objGestionCompetencia = new Data_GestionCompetenciaEdi();
        $objCoberturaMovil     = new Data_CoberturaMovilEdi();
        $objFacilidadPoste     = new Data_FacilidadPosteEdi();
        $objResponsableCampo   = new Data_ResponsableCampoEdi();


        $objTipoCchh     = new Data_TipoCchhEdi();
        $objTipoProyecto = new Data_TipoProyectoEdi();
        $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
        $objTipoVia      = new Data_TipoViaEdi();

        //lista de estados de edificios
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );


        //capturo item y etapa del edificio
        $item  = $arrObjEdificio[0]->__get('_item');
        $etapa = '';
        $etapaFound = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
        if ( array_key_exists($etapaFound, $arrEtapaProyecto) ) {
            $item  = substr($arrObjEdificio[0]->__get('_item'), 0, -1);
            $etapa = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
        }

        //lista de estados de gestion de obra
        $arrObjGestionObra = $objGestionObra->listar($conexion);

        //obtengo las regiones a partir de las zonales que tiene asignado
        //el usuario
        $arrRegion = listarRegionesPorZonalUsuario();

        //lista de fuentes de identificacion
        $arrObjFuenteIdentificacion = array();
        if ( $arrObjEdificio[0]->__get('_idRegion') != '' ) {
            $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listarPorRegion(
                $conexion, $arrObjEdificio[0]->__get('_idRegion')
            );
        }

        //lista de segmentos
        $arrObjSegmento = $objSegmento->listar($conexion);

        //lista de Tipo Cchh
        $arrObjTipoCchh = $objTipoCchh->listar($conexion);

        //lista de Tipo Proyecto
        $idSegmento = ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) ?
        $arrObjEdificio[0]->__get('_idSegmento') : 1;
        $arrObjTipoProyecto = $objTipoProyecto->listar(
            $conexion, '', $idSegmento
        );

        //lista de Tipo de via
        $arrObjTipoVia = $objTipoVia->listar($conexion);

        //lista de Tipo de infraestructura
        $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($conexion);

        //lista de niveles socioeconomicos
        $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($conexion);

        //lista de zona de competencia
        $arrObjZonaCompetencia = $objZonaCompetencia->listar($conexion);

        //lista de gestion de competencia
        $arrObjGestionCompetencia = $objGestionCompetencia->listar($conexion);

        //lista cobertura movil 2G
        $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($conexion);

        //lista cobertura movil 3G
        $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($conexion);

        //lista facilidad de poste
        $arrObjFacilidadPoste = $objFacilidadPoste->listar($conexion);

        //lista de responsable de campo
        $arrObjResponsableCampo = $objResponsableCampo->listar($conexion);



        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $arrObjEdificio[0]->__get('_zonal'), $idUsuario
        );


        $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
        $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
        $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
        $codLocalidad    = $arrObjEdificio[0]->__get('_localidad');

        //lista de departamentos
        //$arrObjDepartamento   = $objUbigeo->departamentosUbigeo($conexion);
        $arrObjDepartamento = array();
        if ( $arrObjEdificio[0]->__get('_zonal') != '' ) {
            $arrObjDepartamento = $objUbigeoZonal->traerDepartamentosZonal(
                $conexion, $arrObjEdificio[0]->__get('_zonal')
            );
        }
        //lista de provincias
        $arrObjProvincia = array();
        if ( $codDepartamento != '' ) {
            $arrObjProvincia = $objUbigeo->provinciasUbigeo(
                $conexion, $codDepartamento
            );
        }
        //lista de distritos
        $arrObjDistrito = array();
        if ( $codDepartamento != '' && $codProvincia != '' ) {
            $arrObjDistrito = $objUbigeo->distritosUbigeo(
                $conexion, $codDepartamento, $codProvincia
            );
        }
        //lista de localidades
        $arrObjLocalidad = array();
        if ( $codDepartamento != '' && $codProvincia != '' &&
                $codDistrito != '' ) {
            $arrObjLocalidad = $objUbigeo->buscarLocalidad(
                $conexion, $codDepartamento, $codProvincia, $codDistrito
            );
        }


        include 'vistas/m.v_edificio.editar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                'en unos minutos.';
        exit;
    }
}



if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'editarEdificio':
        //$iframe me indica si la ventana fue abierta desde google maps 
        //o desde alguna bandeja
        $iframe = ( isset($_REQUEST['iframe']) ) ? 'iframe' : 'bandeja';
        editarEdificio($_REQUEST['idedificio'], $iframe);
        break;
    default:
        cargaDefault();
        break;
}