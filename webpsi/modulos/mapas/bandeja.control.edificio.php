<?php

/**
 * @package     /modulos/maps
 * @name        bandeja.control.edificio.php
 * @category    Controller
 * @author        Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright    2011 Telefonica del Peru
 * @version     1.0 - 2011/12/19
 */

/*require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';
*/

include_once  'autoload.php';

$_SESSION['SUBMODULO_ACTUAL'] = 'mapss/bandeja.control.edificio.php';

$cnxObj = new ConexionMapas();
$conexion = $cnxObj->conectarBD();


class BandejaControlEdificio
{

    protected $_conexion = null;
    
    //maximo tamano de foto a cargar
    protected $_sizeFoto = 512000; //500Kb
    
    //maximo tamano de archivo a cargar
    protected $_sizeFile = 5120000; //5Mb
    
    //numero de Item inicio desde donde parte los nuevos edificios
    protected $_itemInicial = '50000';
    
    //Etapas para proyecto Edificios [A-Z]
    protected $_arrEtapaProyecto = array(
        'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E',
        'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I', 'J' => 'J',
        'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N', 'O' => 'O',
        'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T',
        'U' => 'U', 'V' => 'V', 'W' => 'W', 'X' => 'X', 'Y' => 'Y',
        'Z' => 'Z'
    );

    
    /**
     * @method  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        global $conexion;
        
        //Inicializo la conexion a DB
        $this->_conexion = $conexion;

        
        if (!isset($_GET['cmd']))
            $_GET['cmd'] = '';
        
        switch ($_GET['cmd']) {
            case 'provinciasUbigeoEdi':
                $this->provinciasUbigeoEdi(
                    $_POST['idregion'], $_POST['coddpto']
                );
                break;
            case 'distritosUbigeoEdi':
                $this->distritosUbigeoEdi(
                    $_POST['idregion'], $_POST['coddpto'], $_POST['codprov']
                );
                break;
            case 'cargaMasiva';
                $this->cargaMasiva();
                break;
            case 'cargaMasivaArchivos';
                $this->cargaMasivaArchivos($_POST, $_FILES);
                break;
            case 'verMovimientos':
                $this->verMovimientos($_POST['idEdificio']);
                break;
            case 'buscarItem':
                $this->buscarItem($_POST['item'], $_POST['item_inicial']);
                break;
            case 'obtenerZonalesPorRegionUsuario':
                $this->obtenerZonalesPorRegionUsuario($_POST['region']);
                break;
            case 'verFotoDetalle':
                $this->verFotoDetalle($_POST['idedificios_foto']);
                break;
            case 'registrarFotos':
                $this->registrarFotos($_POST, $_FILES);
                break;
            case 'listadoFotosEdificio':
                $this->listadoFotosEdificio($_POST['idEdificio']);
                break;
            case 'fotosEdificio':
                //$map me indica se la ventana fue abierta desde google maps
                //o desde la bandeja
                $_POST['map'] = ( isset($_POST['map']) ) ? $_POST['map'] : '';
                $this->fotosEdificio($_POST['idEdificio'], $_POST['map']);
                break;
            case 'updateXyEdificio':
                $this->updateXyEdificio(
                    $_POST['idEdificio'], $_POST['x'], $_POST['y']
                );
                break;
            case 'cargarMapaEdificio':
                $this->cargarMapaEdificio($_POST['idedificio']);
                break;
            case 'insertDatosEdificio':
                $this->insertDatosEdificio($_POST);
                break;
            case 'nuevoEdificio':
                $this->nuevoEdificio();
                break;
            case 'updateDatosEdificio':
                $this->updateDatosEdificio($_POST);
                break;
            case 'editarEdificio':
                //$iframe me indica si la ventana fue abierta desde google maps
                //o desde alguna bandeja
                $iframe = ( isset($_POST['iframe']) ) ? 'iframe' : 'bandeja';
                $this->editarEdificio($_POST['idedificio'], $iframe);
                break;
            case 'detalleEdificio':
                $this->detalleEdificio($_POST['idedificio']);
                break;
            case 'cambiarEstado':
                $this->cambiarEstado($_POST['idedificio'], $_POST['estado']);
                break;
            case 'cambiarPaginaEdificio':
                $this->cambiarPaginaEdificio(
                    $_POST['pagina'], $_POST['total'], $_POST['gestionObra'],
                    $_POST['estado'], $_POST['empresa'], $_POST['region'],
                    $_POST['zonal'], $_POST['mdf'], $_POST['campo_filtro'],
                    $_POST['campo_valor'], $_POST['order_field'], 
                    $_POST['order_type'], $_POST['departamento'], 
                    $_POST['provincia'], $_POST['distrito'],
                    $_POST['georeferencia'], $_POST['fotos']
                );
                break;
            case 'filtroBusqueda':
                $this->filtroBusqueda(
                    $_POST['pagina'], $_POST['total'], $_POST['gestionObra'],
                    $_POST['estado'], $_POST['empresa'], $_POST['region'],
                    $_POST['zonal'], $_POST['mdf'], $_POST['campo_filtro'],
                    $_POST['campo_valor'], $_POST['order_field'], 
                    $_POST['order_type'], $_POST['departamento'], 
                    $_POST['provincia'], $_POST['distrito'],
                    $_POST['georeferencia'], $_POST['fotos']
                );
                break;
            default:
                $this->bandejaMain();
                break;
        }
    }
    

    public function bandejaMain()
    {
        $objEdificioEstado = new Data_FfttEdificioEstado();
        $objGestionObra = new Data_GestionObraEdi();
        $objAcceso = new Data_SubmodulosAccesos();
    
		// SERGIO 2013-05-08 -- QUITAR VALIDACION DE PERMISO A SUBMODULO	
        //Validacion de acceso al submodulo
        /*$acceso = $objAcceso->verificarAccesoSubmodulo(
            $this->_conexion,
            $_SESSION['USUARIO_PERFIL']->__get('_idPerfil'),
            $_SESSION['USUARIO']->__get('_idEmpresa'),
            $_SESSION['USUARIO']->__get('_idArea'),
            $_SESSION['USUARIO']->__get('_idZonal')
        );
        if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'spu' ) {
            $acceso = 1;
        }
		*/
		// SERGIO 2013-05-08 -- QUITAR VALIDACION DE PERMISO A SUBMODULO	
		$acceso = 1;
        //FIN Validacion de acceso alsubmodulos
    
        if ( $acceso ) {
            //lista de estados de edificios
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    
            $arrObjGestionObra = $objGestionObra->listar($this->_conexion);
    
            //obtengo las regiones a partir de las zonales que tiene asignado
            //el usuario
            $arrRegion = $this->listarRegionesPorZonalUsuario();
    
            //validacion de bandeja: Empresas, Zonales (datos usuario validos)
            $strValidacionBandeja = '';
            $flagValidoBandeja = 1;
            if (empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
                $flagValidoBandeja = 0;
                $strValidacionBandeja .= 'Empresa(s)';
            }
            if (empty($_SESSION['USUARIO_ZONAL'])) {
                $flagValidoBandeja = 0;
                $strValidacionBandeja .= ' Zonal(es)';
            }
    
            $totalRegistros = 0;
        }
    
        include 'vistas/v_bandeja.control.edificio.php';
    }


    public function filtroBusqueda($pagina='', $total='', $gestionObra='', 
        $estado='', $empresa='', $region='', $zonal='', $mdf='', 
        $campoFiltro='', $campoValor='', $orderField='', $orderType='', 
        $departamento='', $provincia='', $distrito='', $georeferencia='', 
        $fotos='')
    {
        try {
            
            //validando que tenga asignado zonales y mdfs
            if (empty($_SESSION['USUARIO_ZONAL'])) {
                $mensaje = '<tr><td style="height:30px" colspan="15">' .
                        'Ningun registro encontrado con los criterios de ' .
                        'b&uacute;squeda ingresados</td></tr>';
                echo $mensaje;
                exit;
            }
    
    
            $objEdificio       = new Data_FfttEdificio();
            $objEdificioEstado = new Data_FfttEdificioEstado();
            $objTerminales     = new Data_FfttTerminales();
    
            $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    
            $arrFiltro = array();
            if ( $gestionObra != '' ) {
                $arrFiltro['gestionObra'] = $gestionObra;
            }
            if ( $estado != '' ) {
                $arrFiltro['estado'] = $estado;
            }
    
            if ( $empresa != '' ) {
                $arrFiltro['empresa'] = array($empresa);
            } else {
                //listado de empresas que tiene asignado
                $arrDataEmpresa = array();
                if (!empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
                    foreach (
                        $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa
                    ) {
                        $arrDataEmpresa[] = $objEmpresa->__get('_idEmpresa');
                    }
                }
                $arrFiltro['empresa'] = $arrDataEmpresa;
            }
    
            if ( $region != '' ) {
                $arrFiltro['region'] = array($region);
            } else {
    
                //obtengo las regiones a partir de las zonales que tiene 
                //asignado el usuario
                $arrRegion = $this->listarRegionesPorZonalUsuario();
    
                $arrDataRegion = array();
                if (!empty($arrRegion)) {
                    foreach ($arrRegion as $region) {
                        $arrDataRegion[] = $region['idregion'];
                    }
                }
                $arrFiltro['region'] = $arrDataRegion;
            }
    
            if ( $zonal != '' ) {
                $arrFiltro['zonal'] = array($zonal);
            } else {
                $arrData = array();
                if (!empty($_SESSION['USUARIO_ZONAL'])) {
                    foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                        $arrData[] = $objZonal->__get('_abvZonal');
                    }
                }
                $arrFiltro['zonal'] = $arrData;
            }
    
            if ( $mdf != '' ) {
                $arrFiltro['mdf'] = array($mdf);
            } else {
                /*
                 $arrMdf = array();
                if ( $zonal == '' ) {
                if (!empty($_SESSION['USUARIO_MDF'])) {
                foreach ($_SESSION['USUARIO_MDF'] as $abvMdf) {
                $arrMdf[]['mdf'] = $abvMdf;
                }
                }
    
                } else {
                $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                        $this->_conexion, $zonal, $idusuario
                );
                }
    
                $arrData = array();
                if (!empty($arrMdf)) {
                foreach ($arrMdf as $abvMdf) {
                $arrData[] = $abvMdf['mdf'];
                }
                }
                $arrFiltro['mdf'] = $arrData;
                */
            }
    
            if ( $campoFiltro != '' ) {
                $arrFiltro['campo_filtro'] = $campoFiltro;
            }
            if ( $campoValor != '' ) {
                $arrFiltro['campo_valor'] = $campoValor;
            }
            //Ubigeo
            if ( $departamento != '' ) {
                $arrFiltro['departamento'] = $departamento;
            }
            if ( $provincia != '' ) {
                $arrFiltro['provincia'] = $provincia;
            }
            if ( $distrito != '' ) {
                $arrFiltro['distrito'] = $distrito;
            }
            //Georeferencia y fotos
            if ( $georeferencia != '' ) {
                $arrFiltro['georeferencia'] = $georeferencia;
            }
            if ( $fotos != '' ) {
                $arrFiltro['fotos'] = $fotos;
            }
    
            if ( $orderField != '' ) {
                $arrFiltro['order_field'] = $orderField;
            }
            if ( $orderType != '' ) {
                $arrFiltro['order_type'] = $orderType;
            }
    
            $pagina = 1;
            $arrObjEdificio = $objEdificio->listarEdificios(
                $this->_conexion, $pagina, '', $arrFiltro
            );
    
            $totalRegistros = $objEdificio->totalEdificios(
                $this->_conexion, '', $arrFiltro
            );
    
            $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);
    
    
            include 'vistas/v_bandeja.control.edificio.paginado.php';
    
            $cadena = '<script type="text/javascript">
            $("#cont_num_resultados_head").html(' . $totalRegistros . ');
            $("#cont_num_resultados_foot").html(' . $totalRegistros . ');
            $("#pag_actual").attr("value", "1");
            $("#tl_registros").attr("value", "' . $totalRegistros . '");
            $("#paginacion").empty();
            ';
            if ($totalRegistros > 0) {
                $cadena .= '
                $(function() {
                $("#paginacion").paginate({
                count                   : '.$numPaginas.',
                start                   : 1,
                display                 : 10,
                border                  : true,
                border_color            : "#FFFFFF",
                text_color              : "#FFFFFF",
                background_color        : "#0080AF",
                border_hover_color      : "#CCCCCC",
                text_hover_color        : "#000000",
                background_hover_color  : "#FFFFFF",
                images                  : false,
                mouse                   : "press",
                onChange : function(page){
                loader("start");
                $("#pag_actual").attr("value", page);
                var tl=$("#tl_registros").attr("value");
                $.post("modulos/maps/bandeja.control.edificio.php?cmd=cambiarPaginaEdificio", {
                pagina: page, total: tl,
                gestionObra: "' . $gestionObra . '",
                empresa: "' . $empresa . '",
                estado: "' . $estado . '",
                region: "' . $region . '",
                zonal: "' . $zonal . '",
                mdf: "' . $mdf . '",
                campo_filtro: "' . $campoFiltro . '",
                campo_valor: "' . $campoValor . '",
                order_field: order_field,
                order_type: order_type,
                departamento: "' . $departamento . '",
                provincia: "' . $provincia . '",
                distrito: "' . $distrito . '",
                georeferencia: "' . $georeferencia . '",
                fotos: "' . $fotos . '"
            }, function(data){
            $("#tb_resultado tbody").empty();
            $("#tb_resultado tbody").append(data);
            loader("end");
            }
            );
            }
            });
            });';
            }
            $cadena .= '</script>';
            echo $cadena;
    
        } catch(PDOException $e) {
            //echo "Failed: " . $e->getMessage();
        }
    }
    
    public function cambiarPaginaEdificio($pagina='', $total='', 
        $gestionObra='', $estado='', $empresa='', $region='', $zonal='', 
        $mdf='', $campoFiltro='', $campoValor='', $orderField='', 
        $orderType='', $departamento='', $provincia='', $distrito='', 
        $georeferencia='', $fotos=''
    )
    {
        try {

            if ($pagina == '') $pagina = 1;
    
            $objEdificio       = new Data_FfttEdificio();
            $objEdificioEstado = new Data_FfttEdificioEstado();
            $objTerminales     = new Data_FfttTerminales();
    
            $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    

            $arrFiltro = array();
            if ( $gestionObra != '' ) {
                $arrFiltro['gestionObra'] = $gestionObra;
            }
            if ( $estado != '' ) {
                $arrFiltro['estado'] = $estado;
            }
    
            if ( $empresa != '' ) {
                $arrFiltro['empresa'] = $empresa;
            } else {
                //listado de empresas que tiene asignado
                $arrDataEmpresa = array();
                if (!empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
                    foreach ($_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa) {
                        $arrDataEmpresa[] = $objEmpresa->__get('_idEmpresa');
                    }
                }
                $arrFiltro['empresa'] = $arrDataEmpresa;
            }
    
            if ( $region != '' ) {
                $arrFiltro['region'] = array($region);
            } else {
    
                //obtengo las regiones a partir de las zonales que tiene 
                //asignado el usuario
                $arrRegion = $this->listarRegionesPorZonalUsuario();
    
                $arrDataRegion = array();
                if (!empty($arrRegion)) {
                    foreach ($arrRegion as $region) {
                        $arrDataRegion[] = $region['idregion'];
                    }
                }
                $arrFiltro['region'] = $arrDataRegion;
            }
    
            if ( $zonal != '' ) {
                $arrFiltro['zonal'] = array($zonal);
            } else {
                $arrData = array();
                if (!empty($_SESSION['USUARIO_ZONAL'])) {
                    foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                        $arrData[] = $objZonal->__get('_abvZonal');
                    }
                }
                $arrFiltro['zonal'] = $arrData;
            }
    
            if ( $mdf != '' ) {
                $arrFiltro['mdf'] = array($mdf);
            } else {
                /*
                 $arrMdf = array();
                if ( $zonal == '' ) {
                if (!empty($_SESSION['USUARIO_MDF'])) {
                foreach ($_SESSION['USUARIO_MDF'] as $abvMdf) {
                $arrMdf[]['mdf'] = $abvMdf;
                }
                }
    
                } else {
                $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                        $this->_conexion, $zonal, $idusuario
                );
                }
    
                $arrData = array();
                if (!empty($arrMdf)) {
                foreach ($arrMdf as $abvMdf) {
                $arrData[] = $abvMdf['mdf'];
                }
                }
                $arrFiltro['mdf'] = $arrData;
                */
            }
    
            if ( $campoFiltro != '' ) {
                $arrFiltro['campo_filtro'] = $campoFiltro;
            }
            if ( $campoValor != '' ) {
                $arrFiltro['campo_valor'] = $campoValor;
            }
            //Ubigeo
            if ( $departamento != '' ) {
                $arrFiltro['departamento'] = $departamento;
            }
            if ( $provincia != '' ) {
                $arrFiltro['provincia'] = $provincia;
            }
            if ( $distrito != '' ) {
                $arrFiltro['distrito'] = $distrito;
            }
            //Georeferencia y fotos
            if ( $georeferencia != '' ) {
                $arrFiltro['georeferencia'] = $georeferencia;
            }
            if ( $fotos != '' ) {
                $arrFiltro['fotos'] = $fotos;
            }
    
            if ( $orderField != '' ) {
                $arrFiltro['order_field'] = $orderField;
            }
            if ( $orderType != '' ) {
                $arrFiltro['order_type'] = $orderType;
            }
    
            $arrObjEdificio = $objEdificio->listarEdificios(
                $this->_conexion, $pagina, '', $arrFiltro
            );
    
            $totalRegistros = $objEdificio->totalEdificios(
                $this->_conexion, '', $arrFiltro
            );
    
            $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);
    
    
            include 'vistas/v_bandeja.control.edificio.paginado.php';
    
        } catch(PDOException $e) {
            //echo "Failed: " . $e->getMessage();
        }
    }
    

    //Vista Detalle informacion del Edificio
    public function detalleEdificio($idedificio)
    {
        try {

            //variables para el formulario editar
            $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio = new Data_FfttEdificio();
    
            //capturo datos del edificio
            $arrObjEdificio = $objEdificio->obtenerEdificio(
                $this->_conexion, $idedificio
            );
    
            if ( empty($arrObjEdificio) ) {
                throw new PDOException();
            }

            $objEdificioEstado     = new Data_FfttEdificioEstado();
            $objCapas              = new Data_FfttCapas();
            $objTerminales         = new Data_FfttTerminales();
            $objUbigeo             = new Data_Ubigeo();
            $objUbigeoZonal        = new Data_UbigeoZonalEdi();
            $objGestionObra        = new Data_GestionObraEdi();
            $objRegion             = new Data_RegionEdi();
            $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
            $objSegmento           = new Data_SegmentoEdi();
            $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
            $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
            $objGestionCompetencia = new Data_GestionCompetenciaEdi();
            $objCoberturaMovil     = new Data_CoberturaMovilEdi();
            $objFacilidadPoste     = new Data_FacilidadPosteEdi();
            $objResponsableCampo   = new Data_ResponsableCampoEdi();
    
            $objTipoCchh     = new Data_TipoCchhEdi();
            $objTipoProyecto = new Data_TipoProyectoEdi();
            $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
            $objTipoVia      = new Data_TipoViaEdi();
    
            $objUsuario      = new Data_Usuario();
    
    
            //lista de estados de gestion de obra
            $gestionObra = '';
            if ( $arrObjEdificio[0]->__get('_idGestionObra') ) {
                $arrObjGestionObra = $objGestionObra->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idGestionObra')
                );
                if ( !empty($arrObjGestionObra) ) {
                    $gestionObra = $arrObjGestionObra[0]->__get(
                        '_desGestionObra'
                    );
                }
            }
    
    
            //lista de estados de edificios
            $estadoEdificio = '';
            if ( $arrObjEdificio[0]->__get('_estado') != '' ) {
                $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                    $this->_conexion, $arrObjEdificio[0]->__get('_estado')
                );
                if ( !empty($arrObjEdificioEstado) ) {
                    $estadoEdificio = $arrObjEdificioEstado[0]->__get('_desEstado');
                }
            }
    
    
            //obtengo las regiones a partir de las zonales que tiene asignado
            //el usuario
            $arrRegion = $this->listarRegionesPorZonalUsuario();
    
            //lista de fuentes de identificacion
            $fuenteIdentificacion = '';
            if ( $arrObjEdificio[0]->__get('_idFuenteIdentificacion') != '' ) {
                $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idFuenteIdentificacion')
                );
                if ( !empty($arrObjFuenteIdentificacion) ) {
                    $fuenteIdentificacion = $arrObjFuenteIdentificacion[0]->__get('_desFuenteIdentificacion');
                }
            }
    
            //lista de segmentos
            $segmento = '';
            if ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) {
                $arrObjSegmento = $objSegmento->listar(
                    $this->_conexion, $arrObjEdificio[0]->__get('_idSegmento')
                );
                if ( !empty($arrObjSegmento) ) {
                    $segmento = $arrObjSegmento[0]->__get('_desSegmento');
                }
            }
    
            //lista de Tipo Cchh
            $tipoCchh = '';
            if ( $arrObjEdificio[0]->__get('_idTipoCchh') != '' ) {
                $arrObjTipoCchh = $objTipoCchh->listar(
                    $this->_conexion, $arrObjEdificio[0]->__get('_idTipoCchh')
                );
                if ( !empty($arrObjTipoCchh) ) {
                    $tipoCchh = $arrObjTipoCchh[0]->__get('_desTipoCchh');
                }
            }
    
            //lista de Tipo Proyecto
            $tipoProyecto = '';
            if ( $arrObjEdificio[0]->__get('_idTipoProyecto') != '' ) {
                $arrObjTipoProyecto = $objTipoProyecto->listar(
                    $this->_conexion, $arrObjEdificio[0]->__get('_idTipoProyecto')
                );
                if ( !empty($arrObjTipoProyecto) ) {
                    $tipoProyecto = $arrObjTipoProyecto[0]->__get('_desTipoProyecto');
                }
            }
    
            //lista de Tipo de via
            $tipoVia = '';
            if ( $arrObjEdificio[0]->__get('_idTipoVia')!= '' ) {
                $arrObjTipoVia = $objTipoVia->listar(
                    $this->_conexion, $arrObjEdificio[0]->__get('_idTipoVia')
                );
                if ( !empty($arrObjTipoVia) ) {
                    $tipoVia = $arrObjTipoVia[0]->__get('_desTipoVia');
                }
            }
    
            //lista de Tipo de infraestructura
            $tipoInfraestructura = '';
            if ( $arrObjEdificio[0]->__get('_idTipoInfraestructura') != '' ) {
                $arrObjTipoInfraestructura = $objTipoInfraestructura->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idTipoInfraestructura')
                );
                if ( !empty($arrObjTipoInfraestructura) ) {
                    $tipoInfraestructura = $arrObjTipoInfraestructura[0]->__get('_desTipoInfraestructura');
                }
            }
    
            //lista de niveles socioeconomicos
            $nivelSocioeconomico = '';
            if ( $arrObjEdificio[0]->__get('_idNivelSocioeconomico') != '' ) {
                $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idNivelSocioeconomico')
                );
                if ( !empty($arrObjNivelSocioeconomico) ) {
                    $nivelSocioeconomico = $arrObjNivelSocioeconomico[0]->__get('_desNivelSocioeconomico');
                }
            }
    
            //lista de zona de competencia
            $zonaCompetencia = '';
            if ( $arrObjEdificio[0]->__get('_idZonaCompetencia') != '' ) {
                $arrObjZonaCompetencia = $objZonaCompetencia->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idZonaCompetencia')
                );
                if ( !empty($arrObjZonaCompetencia) ) {
                    $zonaCompetencia = $arrObjZonaCompetencia[0]->__get('_desZonaCompetencia');
                }
            }
    
            //lista de gestion de competencia
            $gestionCompetencia = '';
            if ( $arrObjEdificio[0]->__get('_idGestionCompetencia') != '' ) {
                $arrObjGestionCompetencia = $objGestionCompetencia->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idGestionCompetencia')
                );
                if ( !empty($arrObjGestionCompetencia) ) {
                    $gestionCompetencia = $arrObjGestionCompetencia[0]->__get('_desGestionCompetencia');
                }
            }
    
            //lista cobertura movil 2G
            $coberturaMovil2g = '';
            if ( $arrObjEdificio[0]->__get('_idCoberturaMovil2G') != '' ) {
                $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idCoberturaMovil2G')
                );
                if ( !empty($arrObjCoberturaMovil2G) ) {
                    $coberturaMovil2g = $arrObjCoberturaMovil2G[0]->__get('_desCoberturaMovil2G');
                }
            }
    
            //lista cobertura movil 3G
            $coberturaMovil3g = '';
            if ( $arrObjEdificio[0]->__get('_idCoberturaMovil3G') != '' ) {
                $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idCoberturaMovil3G')
                );
                if ( !empty($arrObjCoberturaMovil3G) ) {
                    $coberturaMovil3g = $arrObjCoberturaMovil3G[0]->__get('_desCoberturaMovil3G');
                }
            }
    
            //lista facilidad de poste
            $facilidadPoste = '';
            if ( $arrObjEdificio[0]->__get('_idFacilidadPoste') != '' ) {
                $arrObjFacilidadPoste = $objFacilidadPoste->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idFacilidadPoste')
                );
                if ( !empty($arrObjFacilidadPoste) ) {
                    $facilidadPoste = $arrObjFacilidadPoste[0]->__get('_desFacilidadPoste');
                }
            }
    
            //lista de responsable de campo
            $responsableCampo = '';
            if ( $arrObjEdificio[0]->__get('_idResponsableCampo') != '' ) {
                $arrObjResponsableCampo = $objResponsableCampo->listar(
                    $this->_conexion, 
                    $arrObjEdificio[0]->__get('_idResponsableCampo')
                );
                if ( !empty($arrObjResponsableCampo) ) {
                    $responsableCampo = $arrObjResponsableCampo[0]->__get('_desResponsableCampo');
                }
            }
    

            //lista de mdfs por zonal
            $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                $this->_conexion, 
                $arrObjEdificio[0]->__get('_zonal'), $idUsuario
            );
    

            $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
            $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
            $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
            $codLocalidad    = $arrObjEdificio[0]->__get('_localidad');
    
            //lista de departamentos
            $departamento = '';
            if ( $codDepartamento != '' ) {
                $arrObjDepartamento = $objUbigeo->departamentosUbigeo(
                    $this->_conexion, $codDepartamento
                );
                if ( !empty($arrObjDepartamento) ) {
                    $departamento = $arrObjDepartamento[0]->__get('_nombre');
                }
            }
    
            //lista de provincias
            $provincia = '';
            if ( $codProvincia != '' ) {
                $arrObjProvincia = $objUbigeo->provinciasUbigeo(
                    $this->_conexion, $codDepartamento, $codProvincia
                );
                if ( !empty($arrObjProvincia) ) {
                    $provincia = $arrObjProvincia[0]->__get('_nombre');
                }
            }
    
            //lista de distritos
            $distrito = '';
            if ( $codDistrito != '' ) {
                $arrObjDistrito = $objUbigeo->distritosUbigeo(
                    $this->_conexion, $codDepartamento, $codProvincia, 
                    $codDistrito
                );
                if ( !empty($arrObjDistrito) ) {
                    $distrito = $arrObjDistrito[0]->__get('_nombre');
                }
            }
    
            //lista de localidades
            $localidad = '';
            if ( $codLocalidad != '' ) {
                $arrObjLocalidad = $objUbigeo->buscarLocalidad(
                    $this->_conexion, $codDepartamento, $codProvincia, 
                    $codDistrito
                );
                if ( !empty($arrObjLocalidad) ) {
                    foreach ( $arrObjLocalidad as $objLocalidad ) {
                        if ($codLocalidad == $objLocalidad->__get('_ubigeo')) {
                            $localidad = $objLocalidad->__get('_localidad');
                            break;
                        }
                    }
                }
            }
    

            include 'vistas/v_bandeja.control.edificio.detalle.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                    'en unos minutos.';
            exit;
        }
    }
    
    
    //Vista Editar informacion del Edificio
    public function editarEdificio ($idedificio, $iframe=0)
    {
        try {
    
            //variables para el formulario editar
            $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio = new Data_FfttEdificio();
    
            //capturo datos del edificio
            $arrObjEdificio = $objEdificio->obtenerEdificio(
                $this->_conexion, $idedificio
            );
    
            if ( empty($arrObjEdificio) ) {
                throw new PDOException();
            }
    
            $objEdificioEstado     = new Data_FfttEdificioEstado();
            $objCapas              = new Data_FfttCapas();
            $objTerminales         = new Data_FfttTerminales();
            $objUbigeo             = new Data_Ubigeo();
            $objUbigeoZonal        = new Data_UbigeoZonalEdi();
            $objGestionObra        = new Data_GestionObraEdi();
            $objRegion             = new Data_RegionEdi();
            $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
            $objSegmento           = new Data_SegmentoEdi();
            $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
            $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
            $objGestionCompetencia = new Data_GestionCompetenciaEdi();
            $objCoberturaMovil     = new Data_CoberturaMovilEdi();
            $objFacilidadPoste     = new Data_FacilidadPosteEdi();
            $objResponsableCampo   = new Data_ResponsableCampoEdi();
            $objRegionUbigeo       = new Data_RegionUbigeoEdi();
    
    
            $objTipoCchh     = new Data_TipoCchhEdi();
            $objTipoProyecto = new Data_TipoProyectoEdi();
            $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
            $objTipoVia      = new Data_TipoViaEdi();
    
            //lista de estados de edificios
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    
    
            //capturo item y etapa del edificio
            $item  = $arrObjEdificio[0]->__get('_item');
            $etapa = '';
            $etapaFound = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
            if ( array_key_exists($etapaFound, $this->_arrEtapaProyecto) ) {
                $item  = substr($arrObjEdificio[0]->__get('_item'), 0, -1);
                $etapa = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
            }
    
            //lista de estados de gestion de obra
            $arrObjGestionObra = $objGestionObra->listar($this->_conexion);
    
            //obtengo las regiones a partir de las zonales que tiene asignado
            //el usuario
            $arrRegion = $this->listarRegionesPorZonalUsuario();
    
            //lista de fuentes de identificacion
            $arrObjFuenteIdentificacion = array();
            if ( $arrObjEdificio[0]->__get('_idRegion') != '' ) {
                $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listarPorRegion(
                    $this->_conexion, $arrObjEdificio[0]->__get('_idRegion')
                );
            }
    
            //lista de segmentos
            $arrObjSegmento = $objSegmento->listar($this->_conexion);
    
            //lista de Tipo Cchh
            $arrObjTipoCchh = $objTipoCchh->listar($this->_conexion);
    
            //lista de Tipo Proyecto
            $idSegmento = ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) ?
            $arrObjEdificio[0]->__get('_idSegmento') : 1;
            $arrObjTipoProyecto = $objTipoProyecto->listar(
                $this->_conexion, '', $idSegmento
            );
    
            //lista de Tipo de via
            $arrObjTipoVia = $objTipoVia->listar($this->_conexion);
    
            //lista de Tipo de infraestructura
            $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($this->_conexion);
    
            //lista de niveles socioeconomicos
            $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($this->_conexion);
    
            //lista de zona de competencia
            $arrObjZonaCompetencia = $objZonaCompetencia->listar($this->_conexion);
    
            //lista de gestion de competencia
            $arrObjGestionCompetencia = $objGestionCompetencia->listar($this->_conexion);
    
            //lista cobertura movil 2G
            $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($this->_conexion);
    
            //lista cobertura movil 3G
            $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($this->_conexion);
    
            //lista facilidad de poste
            $arrObjFacilidadPoste = $objFacilidadPoste->listar($this->_conexion);
    
            //lista de responsable de campo
            $arrObjResponsableCampo = $objResponsableCampo->listar($this->_conexion);
    

            //lista de mdfs por zonal
            $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
                $this->_conexion, $arrObjEdificio[0]->__get('_zonal'), 
                $idUsuario
            );
    

            $idRegion = $arrObjEdificio[0]->__get('_idRegion');
            $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
            $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
            $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
            $codLocalidad    = $arrObjEdificio[0]->__get('_localidad');
    
            //lista de departamentos
            $arrObjDepartamento = array();
            if ( $idRegion != '' ) {
                $arrObjDepartamento = $objRegionUbigeo->traerDepartamentosRegion(
                    $this->_conexion, $idRegion
                );
            }
            //lista de provincias
            $arrObjProvincia = array();
            if ( $codDepartamento != '' ) {
                $arrObjProvincia = $objRegionUbigeo->traerProvinciasRegion(
                    $this->_conexion, $idRegion, $codDepartamento
                );
            }
            //lista de distritos
            $arrObjDistrito = array();
            if ( $codDepartamento != '' && $codProvincia != '' ) {
                $arrObjDistrito = $objRegionUbigeo->traerDistritosRegion(
                    $this->_conexion, $idRegion, $codDepartamento, 
                    $codProvincia
                );
            }
            //lista de localidades
            $arrObjLocalidad = array();
            if ( $codDepartamento != '' && $codProvincia != '' &&
                $codDistrito != '' ) {
                $arrObjLocalidad = $objUbigeo->buscarLocalidad(
                    $this->_conexion, $codDepartamento, $codProvincia, 
                    $codDistrito
                );
            }
    
            //Listado de etapas de proyecto
            $arrEtapaProyecto = $this->_arrEtapaProyecto;
    
            include 'vistas/v_bandeja.control.edificio.editar.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                    'en unos minutos.';
            exit;
        }
    }
    
    
    //Vista Ingresar informacion del Edificio
    public function nuevoEdificio()
    {
        try {
            
            //variables para el formulario editar
            $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
            $objEdificio           = new Data_FfttEdificio();
            $objEdificioEstado     = new Data_FfttEdificioEstado();
            $objCapas              = new Data_FfttCapas();
            $objTerminales         = new Data_FfttTerminales();
            $objUbigeo             = new Data_Ubigeo();
            $objUbigeoZonal        = new Data_UbigeoZonalEdi();
            $objGestionObra        = new Data_GestionObraEdi();
            $objRegion             = new Data_RegionEdi();
            $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
            $objSegmento           = new Data_SegmentoEdi();
            $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
            $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
            $objGestionCompetencia = new Data_GestionCompetenciaEdi();
            $objCoberturaMovil     = new Data_CoberturaMovilEdi();
            $objFacilidadPoste     = new Data_FacilidadPosteEdi();
            $objResponsableCampo   = new Data_ResponsableCampoEdi();
    
            $objTipoCchh     = new Data_TipoCchhEdi();
            $objTipoProyecto = new Data_TipoProyectoEdi();
            $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
            $objTipoVia      = new Data_TipoViaEdi();
    
            //lista de estados de edificios
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    
            //seteamos valores por default
            //$idGestionObra = 1; //No
            $idGestionObra = '';
            //$codEstado = '01'; //No iniciado
            $codEstado = '';
            $megaproyecto = '';
            $validacionCall = '';
            $fechaTratamientoCall = '';
    
    
            //lista de estados de gestion de obra
            $arrObjGestionObra = $objGestionObra->listar($this->_conexion);
    
            //obtengo las regiones a partir de las zonales que tiene asignado
            //el usuario
            $arrRegion = $this->listarRegionesPorZonalUsuario();
    
            //lista de fuentes de identificacion
            $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listar(
                $this->_conexion
            );
    
            //lista de segmentos
            $arrObjSegmento = $objSegmento->listar($this->_conexion);
    
            //lista de Tipo Cchh
            $arrObjTipoCchh = $objTipoCchh->listar($this->_conexion);
    
            //lista de Tipo de via
            $arrObjTipoVia = $objTipoVia->listar($this->_conexion);
    
            //lista de Tipo de infraestructura
            $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($this->_conexion);
    
            //lista de niveles socioeconomicos
            $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($this->_conexion);
    
            //lista de zona de competencia
            $arrObjZonaCompetencia = $objZonaCompetencia->listar($this->_conexion);
    
            //lista de gestion de competencia
            $arrObjGestionCompetencia = $objGestionCompetencia->listar($this->_conexion);
    
            //lista cobertura movil 2G
            $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($this->_conexion);
    
            //lista cobertura movil 3G
            $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($this->_conexion);
    
            //lista facilidad de poste
            $arrObjFacilidadPoste = $objFacilidadPoste->listar($this->_conexion);
    
            //lista de responsable de campo
            $arrObjResponsableCampo = $objResponsableCampo->listar($this->_conexion);
    
    
            //seteamos zonal por default
            $codZonal = '';
            if ( !empty($_SESSION['USUARIO_ZONAL']) ) {
                $codZonal  = $_SESSION['USUARIO_ZONAL'][0]->__get('_abvZonal');
            }
    
            //lista de departamentos
            $arrObjDepartamento = $objUbigeoZonal->traerDepartamentosZonal(
                $this->_conexion, $codZonal
            );
    
            include 'vistas/v_bandeja.control.edificio.nuevo.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
            exit;
        }
    }
    
    
    function updateDatosEdificio($arrDatos)
    {
        try {

            //Inicando la transaccion
            $this->_conexion->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
            );
            $this->_conexion->beginTransaction();
    
    
            $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio = new Data_FfttEdificio();
            $objEdificioMovimiento = new Data_FfttEdificioMovimiento();
    
    
            //capturando el Item segun el perfil
            $item = $arrDatos['item'];
            if ( $_SESSION['USUARIO_PERFIL']->__get('_codPerfil') == 'adm' ) {
                $item = $arrDatos['item'] . $arrDatos['item_etapa'];
    
                //Validando si existe un Edificio con el item ingresado
                $existeEdificio = $objEdificio->obtenerPorItem(
                    $this->_conexion, $item, $arrDatos['item_inicial']
                );
    
                if ( !empty( $existeEdificio ) ) {
                    $dataResult = array(
                        'flag'    => '0',
                        'mensaje' => 'Item: ' . $item . ' ya existe, ' .
                            'ingrese uno nuevo.');
                    echo json_encode($dataResult);
                    exit;
                }
                //FIN Validando si existe un Edificio con el item ingresado
            }
            $arrDatos['item'] = $item;
    
    
            //seteando el x,y
            $arrDatos['x'] = ( isset($arrDatos['x']) && $arrDatos['x'] != '' ) ?
                $arrDatos['x'] : NULL;
            $arrDatos['y'] = ( isset($arrDatos['y']) && $arrDatos['y'] != '' ) ?
                $arrDatos['y'] : NULL;
            
            $arrDatos['nro_pisos_1'] = ( isset($arrDatos['nro_pisos_1']) && $arrDatos['nro_pisos_1'] != '' ) ?
                $arrDatos['nro_pisos_1'] : NULL;
            $arrDatos['nro_dptos_1'] = ( isset($arrDatos['nro_dptos_1']) && $arrDatos['nro_dptos_1'] != '' ) ?
                $arrDatos['nro_dptos_1'] : NULL;
    
            //formateando fecha para insert a db
            $arrDatos['fecha_cableado'] = ($arrDatos['fecha_cableado'] != '') ?
                $this->formatFechaDB($arrDatos['fecha_cableado']) : '0000-00-00';
            $arrDatos['fecha_registro_proyecto'] = ( $arrDatos['fecha_registro_proyecto'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_registro_proyecto']) : '0000-00-00';
            $arrDatos['fecha_termino_construccion'] = ( $arrDatos['fecha_termino_construccion'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_termino_construccion']) : '0000-00-00';
            $arrDatos['fecha_tratamiento_call'] = ( $arrDatos['fecha_tratamiento_call'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_tratamiento_call']) : '0000-00-00';
            $arrDatos['fecha_seguimiento'] = ( $arrDatos['fecha_seguimiento'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_seguimiento']) : '0000-00-00';
    
            $arrDatos['montante_fecha']         = ( $arrDatos['montante_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['montante_fecha']) : '0000-00-00';
            $arrDatos['ducteria_interna_fecha'] = ( $arrDatos['ducteria_interna_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['ducteria_interna_fecha']) : '0000-00-00';
            $arrDatos['punto_energia_fecha']    = ( $arrDatos['punto_energia_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['punto_energia_fecha']) : '0000-00-00';
    
    
            //seteamos actualizacion = 1 = desde web | 2 = desde movil
            //$arrDatos['actualizacion'] = '1';
            $arrDatos['actualizacion'] = ( isset($arrDatos['actualizacion']) ) ?
                $arrDatos['actualizacion'] : '1';
    
    
            //1. Actualizamos datos del Edificio
            $objEdificio->updateDatosEdificio(
                $this->_conexion, $arrDatos, $idUsuario
            );
    
            //2. Guardamos el movimiento del edificio (de la actualizacion)
            $objEdificioMovimiento->grabar(
                $this->_conexion, $arrDatos, $idUsuario
            );
    
    
            //Si todo salio OK, hacemos commit
            $this->_conexion->commit();
    
            $dataResult = array(
                    'flag'=>'1',
                    'mensaje'=>'Datos grabados correctamente.'
            );
            echo json_encode($dataResult);
            exit;
    
        } catch (PDOException $e) {
            //Si ocurrio algun error, hacemos rollback
            $this->_conexion->rollBack();
    
            $dataResult = array(
                    'flag'=>'0',
                    'mensaje'=>'Hubo un error al actualizar el Edificio, ' .
                    'intentelo nuevamente.');
            echo json_encode($dataResult);
            exit;
        }
    
    }
    
    
    public function insertDatosEdificio($arrDatos)
    {
        try {
    
            //Inicando la transaccion
            $this->_conexion->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
            );
            $this->_conexion->beginTransaction();
    
    
            $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
            $idEmpresa   = $_SESSION['USUARIO']->__get('_idEmpresa');
            $objEdificio = new Data_FfttEdificio();
            $objEdificioMovimiento = new Data_FfttEdificioMovimiento();
    
            //seteamos la empresa que tiene asignado el usuario
            //$arrDatos['idempresa'] = $idEmpresa;
    
    
            //item por default para inicio de ingreso de nuevos edificios
            //$this->_itemInicial = '50000';
            $etapa = '';
            $itemAutogenerado = $objEdificio->getExisteCodigoAutogeneradoEdificios(
                $this->_conexion, $this->_itemInicial
            );
            if ( !empty($itemAutogenerado) ) {
                $etapaFound = substr($itemAutogenerado[0]['max_item'], -1, 1);
                if ( array_key_exists($etapaFound, $this->_arrEtapaProyecto) ) {
                    $item  = substr($itemAutogenerado[0]['max_item'], 0, -1);
                    $etapa = substr($itemAutogenerado[0]['max_item'], -1, 1);
                } else {
                    $item  = $itemAutogenerado[0]['max_item'];
                }
            }
    
            //seteando el nuevo Item para la insersion del nuevo edificio
            $arrDatos['item'] = $item + 1;
    
            //seteando estado del edificio
            //Por default seteamos estado = 01 = Iniciativa (Nuevos edificios)
            if ( $arrDatos['estado'] == '' ) {
                $arrDatos['estado'] = '01';
            }
    
            //seteando gestion de obra, por default NULL
            if ( $arrDatos['idgestion_obra'] == '' ) {
                $arrDatos['idgestion_obra'] = NULL;
            }
    
    
            //seteando el x,y
            $arrDatos['x'] = ( isset($arrDatos['x']) ) ?
                $arrDatos['x'] : NULL;
            $arrDatos['y'] = ( isset($arrDatos['y']) ) ?
                $arrDatos['y'] : NULL;
    
    
            $arrDatos['nro_pisos_1'] = ( isset($arrDatos['nro_pisos_1']) && $arrDatos['nro_pisos_1'] != '' ) ?
                $arrDatos['nro_pisos_1'] : NULL;
            $arrDatos['nro_dptos_1'] = ( isset($arrDatos['nro_dptos_1']) && $arrDatos['nro_dptos_1'] != '' ) ?
                $arrDatos['nro_dptos_1'] : NULL;
    
            //formateando fecha para insert a db
            $arrDatos['fecha_cableado'] = ( $arrDatos['fecha_cableado'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_cableado']) : '0000-00-00';
            $arrDatos['fecha_registro_proyecto'] = ( $arrDatos['fecha_registro_proyecto'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_registro_proyecto']) : '0000-00-00';
            $arrDatos['fecha_termino_construccion'] = ( $arrDatos['fecha_termino_construccion'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_termino_construccion']) : '0000-00-00';
            $arrDatos['fecha_tratamiento_call'] = ( $arrDatos['fecha_tratamiento_call'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_tratamiento_call']) : '0000-00-00';
            $arrDatos['fecha_seguimiento'] = ( $arrDatos['fecha_seguimiento'] != '' ) ?
                $this->formatFechaDB($arrDatos['fecha_seguimiento']) : '0000-00-00';
    
            $arrDatos['montante_fecha']         = ( $arrDatos['montante_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['montante_fecha']) : '0000-00-00';
            $arrDatos['ducteria_interna_fecha'] = ( $arrDatos['ducteria_interna_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['ducteria_interna_fecha']) : '0000-00-00';
            $arrDatos['punto_energia_fecha']    = ( $arrDatos['punto_energia_fecha'] != '' ) ?
                $this->formatFechaDB($arrDatos['punto_energia_fecha']) : '0000-00-00';
    
    
            //seteamos ingreso = 1 (ingresado desde web) | 2 = desde movil
            $arrDatos['ingreso'] = ( isset($arrDatos['ingreso']) ) ? 
                $arrDatos['ingreso'] : '1';
            //$arrDatos['ingreso'] = '1';
    
            //seteamos actualizacion = 1 (actualizado desde web) | 2 = desde movil
            $arrDatos['actualizacion'] = $arrDatos['ingreso'];
            //$arrDatos['actualizacion'] = '1';
    
    
            //1. Insertamos datos del Edificio
            $idEdificio = $objEdificio->insertDatosEdificio(
                $this->_conexion, $arrDatos, $idUsuario
            );
    
            //seteamos el ultimo id isertado en edificios
            $arrDatos['idedificio'] = $idEdificio;
    
            //2. Guardamos el movimiento del edificio (de la actualizacion)
            $objEdificioMovimiento->grabar($this->_conexion, $arrDatos, $idUsuario);
    
    
            //obtenemos el edificio nuevo ingresado
            $objEdificioNuevo = $objEdificio->obtenerEdificio(
                $this->_conexion, $idEdificio
            );
            $nuevoItem = '';
            if ( !empty($objEdificioNuevo) ) {
                $nuevoItem = $objEdificioNuevo[0]->__get('_item');
            }
    
            //Si todo salio OK, hacemos commit
            $this->_conexion->commit();
    
            $dataResult = array(
                    'flag'    => '1',
                    'mensaje' => 'Datos grabados correctamente.',
                    'item'    => $nuevoItem
            );
            echo json_encode($dataResult);
            exit;
    
        } catch (PDOException $e) {
            //Si ocurrio algun error, hacemos rollback
            $this->_conexion->rollBack();
    
            $dataResult = array(
                'flag'     => '0',
                'mensaje'  => 'Hubo un error al ingresar el nuevo Edificio, ' .
                    'intentelo nuevamente.',
                'mensaje2' => $e->getMessage()
            );
            echo json_encode($dataResult);
            exit;
        }
    
    }
    
    public function cargarMapaEdificio($idedificio)
    {
        try {

            $objEdificio   = new Data_FfttEdificio();
    
            $arrObjEdificio = $objEdificio->obtenerEdificio(
                $this->_conexion, $idedificio
            );
    
            $arrDataMarkers = array();
            if ( !empty($arrObjEdificio) ) {
                $arrDataMarkers = array(
                    'idedificio' => $arrObjEdificio[0]->__get('_idEdificio'),
                    'item'       => $arrObjEdificio[0]->__get('_item'),
                    'x'          => $arrObjEdificio[0]->__get('_x'),
                    'y'          => $arrObjEdificio[0]->__get('_y')
                );
            }
    
            //Capturando la zonal a la que pertenece el usuario
            $idZonal = $_SESSION['USUARIO']->__get('_idZonal');
    
            //carga de datos (x,y) de Zonal
            $objZonal = new Data_Zonal();
            $arrZonal = $objZonal->listar($this->_conexion, $idZonal);
            $arrDataZonal = array();
            if ( !empty($arrZonal) ) {
                $arrDataZonal[] = array(
                    'abv'   => $arrZonal[0]->__get('_abvZonal'),
                    'name'  => $arrZonal[0]->__get('_descZonal'),
                    'x'     => $arrZonal[0]->__get('_x'),
                    'y'     => $arrZonal[0]->__get('_y')
                );
            }
            //FIN Capturando la zonal a la que pertenece el usuario
    
    
            $dataResult = array(
                'flag'      => '1',
                'marker'    => $arrDataMarkers,
                'zonal'     => $arrDataZonal
            );
            echo json_encode($dataResult);
            exit;
    
        } catch (PDOException $e) {
            $dataResult = array(
                'flag'    => '0',
                'mensaje' => 'Hubo un error al cargar el mapa de la ' .
                    'ubicacion del edificio');
            echo json_encode($dataResult);
            exit;
        }
    }
    
    
    public function cambiarEstado($idedificio, $estado)
    {
        try {

            $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio = new Data_FfttEdificio();
    
            //actualizando datos en edificio
            $objEdificio->cambiarEstado(
                $this->_conexion, $idedificio, $estado, $idUsuario
            );
    
    
            $dataResult = array(
                    'flag'=>'1',
                    'mensaje'=>'Datos grabados correctamente.'
            );
            echo json_encode($dataResult);
            exit;
    
        } catch (PDOException $e) {
            $dataResult = array(
                    'flag'=>'0',
                    'mensaje'=>'Hubo un error al actualizar el estado del Edificio, ' .
                    'intentelo nuevamente.'
            );
            echo json_encode($dataResult);
            exit;
        }
    }
    
    
    public function updateXyEdificio($idEdificio, $x, $y)
    {
        try {

            $idUsuario   = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio = new Data_FfttEdificio();
    
            //actualizando x,y del Edificio
            $objEdificio->updateXyEdificio(
                $this->_conexion, $idEdificio, $x, $y, $idUsuario
            );
    
            $dataResult = array(
                'flag'    => '1',
                'mensaje' => 'Datos grabados correctamente.'
            );
            echo json_encode($dataResult);
            exit;
    
        } catch (PDOException $e) {
            $dataResult = array(
                    'flag' => '0',
                    'mensaje' => 'Hubo un error al actualizar el Edificio, ' .
                    'intentelo nuevamente.');
            echo json_encode($dataResult);
            exit;
        }
    }
    
    
    //lista historico de fotos de edificio
    public function fotosEdificio($idEdificio, $map='')
    {
        try {
            
            $objEdificio       = new Data_FfttEdificio();
            $objEdificioEstado = new Data_FfttEdificioEstado();
    
    
            $arrObjEdificio = $objEdificio->obtenerEdificio(
                $this->_conexion, $idEdificio
            );
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $this->_conexion
            );
    
            include 'vistas/v_bandeja.control.edificio.fotos.main.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                    'en unos minutos.';
            exit;
        }
    }
    
    
    public function listadoFotosEdificio($idEdificio)
    {
        try {

            $objEdificioFotos = new Data_FfttEdificioFotos();
    
            $arrObjEdificioFotos = $objEdificioFotos->obtenerFotosEdificio(
                $this->_conexion, $idEdificio
            );
    
            include 'vistas/v_bandeja.control.edificio.fotos.listado.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a ' .
                'intentarlo en unos minutos.';
            exit;
        }
    }
    
    
    public function registrarFotos($arrDataFoto, $arrFilesEdificios)
    {
        try {
            
            //Inicando la transaccion
            $this->_conexion->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
            );
            $this->_conexion->beginTransaction();
    
    
            //validando tama�o y dimensiones de fotos, maximo 500Kb
            $validaTamanoImagen   = $this->validateTamanoImagen(
                $arrFilesEdificios['imagen_edificio'], $this->_sizeFoto
            );
            if ($validaTamanoImagen == '1') {
                $mensaje = array(
                    "flag"    => "0",
                    "mensaje" => "El tama&ntilde;o de 1 o mas archivos no " .
                        "es correcto.\n\n500Kb como maximo.",
                    "tipo"    => "imagen"
                );
                echo json_encode($mensaje);
                exit;
            }
            //FIN validando tama�o y dimensiones de fotos de edificio
    
            //Upload de fotos edificios
            $listNameFotos = $this->moveFile(
                $arrFilesEdificios['imagen_edificio'], 'edi'
            );
            if ( !empty($listNameFotos) && count($listNameFotos) > 0 ) {
                $arrDataFoto['foto_edificio1'] = ( isset($listNameFotos[0]) ) ?
                    $listNameFotos[0] : '';
                $arrDataFoto['foto_edificio2'] = ( isset($listNameFotos[1]) ) ?
                    $listNameFotos[1] : '';
                $arrDataFoto['foto_edificio3'] = ( isset($listNameFotos[2]) ) ?
                    $listNameFotos[2] : '';
                $arrDataFoto['foto_edificio4'] = ( isset($listNameFotos[3]) ) ?
                    $listNameFotos[3] : '';
            } else {
                $mensaje = array(
                    "flag"    => "0",
                    "mensaje" => "No se logro subir las fotos del armario, " .
                        "Vuelva a intentarlo.",
                    "tipo"    => "imagen"
                );
                echo json_encode($mensaje);
                exit;
            }
            //FIN Upload de fotos
    
    
            $arrFechaFoto = explode("/", $arrDataFoto['fecha_foto']);
            $arrDataFoto['fecha_foto'] = $arrFechaFoto[2] . '-' .
                $arrFechaFoto[1] . '-' . $arrFechaFoto[0];
    
            //seteamos ingreso = 1 (actualizado desde web) | 2 = desde movil
            $arrDataFoto['ingreso'] = ( isset($arrDataFoto['ingreso']) ) ?
                $arrDataFoto['ingreso'] : '1';
    
    
            $idUsuario        = $_SESSION['USUARIO']->__get('_idUsuario');
            $objEdificio      = new Data_FfttEdificio();
            $objEdificioFotos = new Data_FfttEdificioFotos();
    
    
            //insertando en Edificio Fotos
            $objEdificioFotos->insertarFotoEdificio(
                $this->_conexion, $arrDataFoto, $idUsuario
            );
    
            //actualizando fotos en Edificio
            $objEdificio->updateFotosEdificio(
                $this->_conexion, $arrDataFoto, $idUsuario
            );
    
    
            //Si todo salio OK, hacemos commit
            $this->_conexion->commit();
    
            $arrDataResult = array(
                'flag'    => '1',
                'mensaje' => 'Datos grabados correctamente.',
                'tipo'=>''
            );
            echo json_encode($arrDataResult);
            exit;
    
        } catch (PDOException $e) {
            //Si ocurrio algun error, hacemos rollback
            $this->_conexion->rollBack();
    
            $arrDataResult = array(
                'flag'    => '0',
                'mensaje' => 'Hubo un error al registrar las fotos, ' .
                    'intentelo nuevamente.', 
                'mensaje2' => $e->getMessage(),
                'tipo'    => ''
            );
            echo json_encode($arrDataResult);
            exit;
        }
    }
    
    
    //foto detalle
    public function verFotoDetalle($idEdificiosFoto)
    {
        try {

            $objEdificioFotos = new Data_FfttEdificioFotos();
    
            $arrObjFotosDetalle = $objEdificioFotos->obtenerFotosDetalle(
                $this->_conexion, $idEdificiosFoto
            );
    
            $arrDataFotos = array();
            if ( !empty($arrObjFotosDetalle) ) {
                if ( trim($arrObjFotosDetalle[0]->__get('_foto1')) != '' ) {
                    $arrDataFotos[] = array(
                        'image'=>trim($arrObjFotosDetalle[0]->__get('_foto1'))
                    );
                }
                if ( trim($arrObjFotosDetalle[0]->__get('_foto2')) != '' ) {
                    $arrDataFotos[] = array(
                        'image'=>trim($arrObjFotosDetalle[0]->__get('_foto2'))
                    );
                }
                if ( trim($arrObjFotosDetalle[0]->__get('_foto3')) != '' ) {
                    $arrDataFotos[] = array(
                        'image'=>trim($arrObjFotosDetalle[0]->__get('_foto3'))
                    );
                }
                if ( trim($arrObjFotosDetalle[0]->__get('_foto4')) != '' ) {
                    $arrDataFotos[] = array(
                        'image'=>trim($arrObjFotosDetalle[0]->__get('_foto4'))
                    );
                }
            }
    
            include 'vistas/v_bandeja.control.edificio.fotos.detalle.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                    'en unos minutos.';
            exit;
        }
    }
    
    
    public function verMovimientos($idEdificio)
    {
        try {
            
            $objEdificioMov = new Data_FfttEdificioMovimiento();
    
            $arrObjEdificioMov = $objEdificioMov->listar(
                $this->_conexion, $idEdificio
            );
    
            include 'vistas/v_bandeja.control.edificio.movimientos.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a ' .
                'intentarlo en unos minutos.';
            exit;
        }
    }
    
    
    public function buscarItem($item, $itemInicial)
    {
        try {
            
            $objEdificio = new Data_FfttEdificio();
    
            $existeEdificio = $objEdificio->obtenerPorItem(
                $this->_conexion, $item, $itemInicial
            );
    
            if ( empty( $existeEdificio ) ) {
                $arrResult = array(
                    'success' => 1,
                    'msg'     => 'OK'
                );
            } else {
                $arrResult = array(
                    'success' => 2,
                    'msg'     => 'ITEM EXISTE'
                );
            }
    
            echo json_encode($arrResult);
            exit;
    
        } catch(PDOException $e) {
            $arrDataResult = array(
                    'success'    => 0,
                    'msg' => 'Hubo un error en el Sistema, intentelo ' .
                    'nuevamente.'
            );
            echo json_encode($arrDataResult);
            exit;
        }
    }
    
    
    public function cargaMasiva()
    {
        try {
            
            include 'vistas/v_bandeja.control.edificio.carga.masiva.php';
    
        } catch(PDOException $e) {
            echo 'Se ha producido un error en el Sistema. Vuelva a ' .
                'intentarlo en unos minutos.';
            exit;
        }
    }
    
    
    public function cargaMasivaArchivos($arrDataArchivo, $arrFilesEdificios)
    {
        try {

            $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
            //Inicando la transaccion
            $this->_conexion->setAttribute(
                PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION
            );
            $this->_conexion->beginTransaction();
    
    
            //1. validando tama�o, maximo 5Mb
            $validaTamanoImagen   = $this->validateTamanoImagen(
                $arrFilesEdificios['archivo_upload'], $this->_sizeFile
            );
            if ($validaTamanoImagen == '1') {
                $mensaje = array(
                    "flag"    => "0",
                    "mensaje" => "El tama&ntilde;o del archivo no es " .
                        "correcto.\n\n5Mb como maximo.",
                    "tipo"    => "imagen"
                );
                echo json_encode($mensaje);
                exit;
            }
            //FIN validando tama�o
    
            //2. Upload de archivo de carga masiva
            $listNameFotos = $this->moveFile(
                $arrFilesEdificios['archivo_upload'], 'edi'
            );
            if ( !empty($listNameFotos) && count($listNameFotos) > 0 ) {
                $arrDataArchivo['archivo_edificio1'] = ( isset($listNameFotos[0]) ) ?
                    $listNameFotos[0] : '';
            } else {
                $mensaje = array(
                    "flag"    => "0",
                    "mensaje" => "No se logro subir las fotos del armario, " .
                    "Vuelva a intentarlo.",
                    "tipo"    => "imagen"
                );
                echo json_encode($mensaje);
                exit;
            }
            //FIN Upload de fotos
    
    
            //Actualizando a partir del archivo cargado
            $dir = FFTT . "edi/";
            $archivo = $dir . $arrDataArchivo['archivo_edificio1'];
    
    
            $validacion = 1;
            $i = 1;
            if ( file_exists($archivo) ) {
    
                $objEdificioEstado = new Data_FfttEdificioEstado();
                $objGestionObra    = new Data_GestionObraEdi();
    
                //lista de estados de edificios
                $arrEdificioEstado = array();
                $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                    $this->_conexion
                );
                foreach ( $arrObjEdificioEstado as $objEdificioEstado ) {
                    $desEstado = trim($objEdificioEstado->__get('_desEstado'));
                    $desEstado = Logic_Utils::constructUrl($desEstado);
    
                    $arrEdificioEstado[$desEstado] = $objEdificioEstado->__get(
                        '_codEdificioEstado'
                    );
                }
    
                //lista de estados de gestion de obra
                $arrGestionObra = array();
                $arrObjGestionObra = $objGestionObra->listar($this->_conexion);
                foreach ( $arrObjGestionObra as $objGestionObra ) {
                    $desGestionObra = trim($objGestionObra->__get('_desGestionObra'));
                    $desGestionObra = Logic_Utils::constructUrl($desGestionObra);
    
                    $arrGestionObra[$desGestionObra] = $objGestionObra->__get(
                        '_idGestionObra'
                    );
                }
    
    
                //3. Validacion de registros del archivo cargado
                $arrMensajeError = array();
                if ( $arrDataArchivo['lugar'] == 'Lima' ) {
    
                    $arrMensajeError = $this->validacionRegistrosCargaMasiva(
                        $archivo, $arrEdificioEstado, $arrGestionObra, 'Lima'
                    );
    
                } elseif ( $arrDataArchivo['lugar'] == 'Provincia' ) {
    
                    $arrMensajeError = $this->validacionRegistrosCargaMasiva(
                        $archivo, $arrEdificioEstado, $arrGestionObra, 
                        'Provincia'
                    );
    
                }
                //FIN Validacion de registros del archivo cargado
    
    
                if ( !empty($arrMensajeError) ) {
                    $mensaje = array(
                        "flag"    => "0",
                        "mensaje" => "Existen algunos registros del archivo " .
                            "no validos. Verifique los mensajes de error y " .
                            "vuelva a intentarlo.",
                        "tipo"    => "log",
                        "msg_log" => $arrMensajeError
                    );
                    echo json_encode($mensaje);
                    exit;
                }
    

                //4. Insertando en tabla temporal y actualizacion masiva
                $objEdificio = new Data_FfttEdificio();
    
                if ( $arrDataArchivo['lugar'] == 'Lima' ) {
    
                    $this->grabarRegistrosCargaMasiva(
                        $archivo, $arrEdificioEstado, $arrGestionObra, 'Lima'
                    );
    
                    //Realizando la actualizacion masiva Lima
                    $objEdificio->actualizaCargaMasivaLima($this->_conexion);
    
                } elseif ( $arrDataArchivo['lugar'] == 'Provincia' ) {
    
                    $this->grabarRegistrosCargaMasiva(
                        $archivo, $arrEdificioEstado, $arrGestionObra,
                        'Provincia'
                    );
    
                    //Realizando la actualizacion masiva Provincia
                    $objEdificio->actualizaCargaMasivaProvincia(
                        $this->_conexion
                    );
    
                }
                //FIN Insertando en tabla temporal y actualizacion masiva
    
    
                //Si todo salio OK, hacemos commit
                $this->_conexion->commit();
                
                
                //eliminamos el archivo fuente cargado
                $archivoBorrado = unlink($archivo);
                
    
                $arrDataResult = array(
                    'flag'    => '1',
                    'mensaje' => 'Datos grabados correctamente.',
                    'tipo'    => ''
                );
                echo json_encode($arrDataResult);
                exit;
    
    
            } else {
                $arrDataResult = array(
                    'flag'    => '0',
                    'mensaje' => 'Hubo un error al cargar el archivo, ' .
                        'intentelo nuevamente.',
                    'tipo'    => ''
                );
                echo json_encode($arrDataResult);
                exit;
            }
            //FIN Actualiza a partir del archivo cargado
    
    
            $arrDataResult = array(
                    'flag'    => '1',
                    'mensaje' => 'Archivo cargado correctamente.','tipo'=>''
            );
            echo json_encode($arrDataResult);
            exit;
    
        } catch (PDOException $e) {
            //Si ocurrio algun error, hacemos rollback
            $this->_conexion->rollBack();
    
            $arrDataResult = array(
                'flag'     => '0',
                'mensaje'  => 'Hubo un error al subir el archivo, intentelo ' .
                    'nuevamente.',
                'mensaje2' => $e->getMessage(),
                'tipo'     => ''
            );
            echo json_encode($arrDataResult);
            exit;
        }
    }
    
    
    private function validacionRegistrosCargaMasiva(
        $archivo, $arrEdificioEstado, $arrGestionObra, $lugar)
    {
    
        $fp = fopen($archivo, "r");
        $i = 1;
        $arrMensajeError = array();
        while (( $data = fgetcsv($fp, 10000, ",")) !== FALSE ) {
    
            if ( trim($data[0]) != '' && $i > 1 ) {
    
                if ( $lugar == 'Lima' ) {
    
                    $item                 = ( isset($data[0]) ) ? trim($data[0]) : '';
    
                    $fechaTerminoConstruccion = ( isset($data[1]) ) ? trim($data[1]) : '';
                    $ducteriaInternaFecha = ( isset($data[2]) ) ? trim($data[2]) : '';
                    $montanteFecha        = ( isset($data[3]) ) ? trim($data[3]) : '';
                    $puntoEnergiaFecha    = ( isset($data[4]) ) ? trim($data[4]) : '';
                    $ducteriaInterna      = ( isset($data[5]) ) ? trim($data[5]) : '';
                    $montante             = ( isset($data[6]) ) ? trim($data[6]) : '';
                    $puntoEnergia         = ( isset($data[7]) ) ? trim($data[7]) : '';
    
                    $gestionObra          = ( isset($data[8]) ) ? trim(utf8_encode($data[8])) : '';
                    $estado               = ( isset($data[9]) ) ? trim(utf8_encode($data[9])) : '';
                    $validacionCall       = ( isset($data[10]) ) ? trim($data[10]) : '';
                    $fechaTratamientoCall = ( isset($data[11]) ) ? trim($data[11]) : '';
    
                    $fechaCableado        = ( isset($data[12]) ) ? trim($data[12]) : '';
    
                    //quitando caracteres especiales a gestionObra y estado
                    $gestionObraUrl = Logic_Utils::constructUrl($gestionObra);
                    $estadoUrl      = Logic_Utils::constructUrl($estado);
    
    
                    //validando los campos
                    $nuevaFechaTerminoConstruccion = '';
                    if ( $fechaTerminoConstruccion != '' ) {
                        $fechaTerminoCons = explode('/', $fechaTerminoConstruccion);
                        if ( count($fechaTerminoCons) == 3 ) {
                            $nuevaFechaTerminoConstruccion = $fechaTerminoCons[2] . '-' .
                                $fechaTerminoCons[1] . '-' . $fechaTerminoCons[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaTerminoConstruccion)) != $fechaTerminoConstruccion ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                            $fechaTerminoConstruccion . ' no es un valor de Fecha termino construccion valido';
                    }
    
                    $nuevaDucteriaInternaFecha = '';
                    if ( $ducteriaInternaFecha != '' ) {
                        $ducteriaInternaFec = explode('/', $ducteriaInternaFecha);
                        if ( count($ducteriaInternaFec) == 3 ) {
                            $nuevaDucteriaInternaFecha = $ducteriaInternaFec[2] . '-' .
                                    $ducteriaInternaFec[1] . '-' . $ducteriaInternaFec[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaDucteriaInternaFecha)) != $ducteriaInternaFecha ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $ducteriaInternaFecha . ' no es un valor de Fecha de ducteria interna valido';
                    }
    
                    $nuevaMontanteFecha = '';
                    if ( $montanteFecha != '' ) {
                        $montanteFec = explode('/', $montanteFecha);
                        if ( count($montanteFec) == 3 ) {
                            $nuevaMontanteFecha = $montanteFec[2] . '-' .
                                    $montanteFec[1] . '-' . $montanteFec[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaMontanteFecha)) != $montanteFecha ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $montanteFecha . ' no es un valor de Fecha de montante valido';
                    }
    
                    $nuevaPuntoEnergiaFecha = '';
                    if ( $puntoEnergiaFecha != '' ) {
                        $puntoEnergiaFec = explode('/', $puntoEnergiaFecha);
                        if ( count($puntoEnergiaFec) == 3 ) {
                            $nuevaPuntoEnergiaFecha = $puntoEnergiaFec[2] . '-' .
                                    $puntoEnergiaFec[1] . '-' . $puntoEnergiaFec[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaPuntoEnergiaFecha)) != $puntoEnergiaFecha ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $puntoEnergiaFecha . ' no es un valor de Fecha de punto energia valido';
                    }
    
                    if ( strtoupper($ducteriaInterna) != 'SI' &&
                            strtoupper($ducteriaInterna) != 'NO' &&
                            strtoupper($ducteriaInterna) != '' ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $ducteriaInterna .
                        ' no es un valor de Ducteria interna valido, solo esta permitido: SI y NO';
                    }
    
                    if ( strtoupper($montante) != 'SI' &&
                            strtoupper($montante) != 'NO' &&
                            strtoupper($montante) != '' ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $montante .
                        ' no es un valor de Montante valido, solo esta permitido: SI y NO';
                    }
    
                    if ( strtoupper($puntoEnergia) != 'SI' &&
                            strtoupper($puntoEnergia) != 'NO' &&
                            strtoupper($puntoEnergia) != '' ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $puntoEnergia .
                        ' no es un valor de Punto de energia valido, solo esta permitido: SI y NO';
                    }
    
                    if ( !isset($arrGestionObra[$gestionObraUrl]) ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $gestionObra .
                        ' no es un valor de Gestion de Obra valido';
                    }
    
                    if ( !isset($arrEdificioEstado[$estadoUrl]) ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $estado . 
                        ' no es un valor de Estado valido';
                    }
    
                    if ( strtoupper($validacionCall) != 'SI' && strtoupper($validacionCall) != 'NO' &&
                            strtoupper($validacionCall) != '' ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $validacionCall .
                        ' no es un valor de Validacion Call valido, solo esta permitido: SI y NO';
                    }
    
                    $nuevaFechaTratamientoCall = '';
                    if ( $fechaTratamientoCall != '' ) {
                        $fechaTratCall = explode('/', $fechaTratamientoCall);
                        if ( count($fechaTratCall) == 3 ) {
                            $nuevaFechaTratamientoCall = $fechaTratCall[2] . '-' .
                                    $fechaTratCall[1] . '-' . $fechaTratCall[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaTratamientoCall)) != $fechaTratamientoCall ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $fechaTratamientoCall . ' no es un valor de Fecha tratamiento call valido';
                    }
    
                    $nuevaFechaCableado = '';
                    if ( $fechaCableado != '' ) {
                        $fechaCabl = explode('/', $fechaCableado);
                        if ( count($fechaCabl) == 3 ) {
                            $nuevaFechaCableado = $fechaCabl[2] . '-' .
                                    $fechaCabl[1] . '-' . $fechaCabl[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaCableado)) != $fechaCableado ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $fechaCableado . ' no es un valor de Fecha cableado valido';
                    }
                    //FIN validando los campos
    
    
                } elseif ( $lugar == 'Provincia' ) {
    
                    $codigoProyectoWeb    = ( isset($data[0]) ) ? trim($data[0]) : '';
    
                    $fechaTerminoConstruccion = ( isset($data[1]) ) ? trim($data[1]) : '';
    
                    $estado               = ( isset($data[2]) ) ? trim(utf8_encode($data[2])) : '';
                    $fechaCableado        = ( isset($data[3]) ) ? trim($data[3]) : '';
                    $validacionCall       = ( isset($data[4]) ) ? trim($data[4]) : '';
                    $fechaTratamientoCall = ( isset($data[5]) ) ? trim($data[5]) : '';
                    $rucConstructora      = ( isset($data[6]) ) ? trim($data[6]) : '';
    
                    //quitando caracteres especiales a gestionObra y estado
                    $estadoUrl      = Logic_Utils::constructUrl($estado);
    
    
                    //validando los campos
                    $nuevaFechaTerminoConstruccion = '';
                    if ( $fechaTerminoConstruccion != '' ) {
                        $fechaTerminoCons = explode('/', $fechaTerminoConstruccion);
                        if ( count($fechaTerminoCons) == 3 ) {
                            $nuevaFechaTerminoConstruccion = $fechaTerminoCons[2] . '-' .
                                    $fechaTerminoCons[1] . '-' . $fechaTerminoCons[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaTerminoConstruccion)) != $fechaTerminoConstruccion ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $fechaTerminoConstruccion . ' no es un valor de Fecha termino construccion valido';
                    }
    
                    if ( !isset($arrEdificioEstado[$estadoUrl]) ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $estado .
                        ' no es un valor de Estado valido';
                    }
    
                    $nuevaFechaCableado = '';
                    if ( $fechaCableado != '' ) {
                        $fechaCabl = explode('/', $fechaCableado);
                        if ( count($fechaCabl) == 3 ) {
                            $nuevaFechaCableado = $fechaCabl[2] . '-' .
                                    $fechaCabl[1] . '-' . $fechaCabl[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaCableado)) != $fechaCableado ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $fechaCableado . ' no es un valor de Fecha de cableado valido';
                    }
    
                    if ( strtoupper($validacionCall) != 'SI' && strtoupper($validacionCall) != 'NO' &&
                            strtoupper($validacionCall) != '' ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' . $validacionCall .
                        ' no es un valor de Validacion Call valido, solo esta permitido: SI y NO';
                    }
    
                    $nuevaFechaTratamientoCall = '';
                    if ( $fechaTratamientoCall != '' ) {
                        $fechaTratCall = explode('/', $fechaTratamientoCall);
                        if ( count($fechaTratCall) == 3 ) {
                            $nuevaFechaTratamientoCall = $fechaTratCall[2] . '-' .
                                    $fechaTratCall[1] . '-' . $fechaTratCall[0];
                        }
                    }
                    if ( date('d/m/Y', strtotime($nuevaFechaTratamientoCall)) != $fechaTratamientoCall ) {
                        $arrMensajeError[] = 'Linea ' . $i . ': ' .
                                $fechaTratamientoCall . ' no es un valor de Fecha de tratamiento call valido';
                    }
                    //FIN validando los campos
                }
            }
    
            $i++;
        }
        fclose($fp);
    
        return $arrMensajeError;
    }
    
    
    private function grabarRegistrosCargaMasiva(
        $archivo, $arrEdificioEstado, $arrGestionObra, $lugar)
    {
        try {
            
            $objEdificioCargaLima = new Data_FfttEdificioCargaLima();
            $objEdificioCargaProvincia = new Data_FfttEdificioCargaProvincia();
            
            if ( $lugar == 'Lima' ) {
                //Antes de insertar, limpio la tabla
                $objEdificioCargaLima->limpiarTabla($this->_conexion);
    
            } elseif ( $lugar == 'Provincia' ) {
                //Antes de insertar, limpio la tabla
                $objEdificioCargaProvincia->limpiarTabla($this->_conexion);
            }
            
    
            $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
            $openFile = fopen($archivo, "r");
            $i = 1;
            while (( $data = fgetcsv($openFile, 10000, ",")) !== FALSE ) {
    
                if ( trim($data[0]) != '' && $i > 1 ) {
    
                    if ( $lugar == 'Lima' ) {
    
                        $item                 = ( isset($data[0]) ) ? trim($data[0]) : '';
    
                        $fechaTerminoConstruccion = ( isset($data[1]) ) ? trim($data[1]) : '';
                        $ducteriaInternaFecha = ( isset($data[2]) ) ? trim($data[2]) : '';
                        $montanteFecha        = ( isset($data[3]) ) ? trim($data[3]) : '';
                        $puntoEnergiaFecha    = ( isset($data[4]) ) ? trim($data[4]) : '';
                        $ducteriaInterna      = ( isset($data[5]) ) ? trim($data[5]) : '';
                        $montante             = ( isset($data[6]) ) ? trim($data[6]) : '';
                        $puntoEnergia         = ( isset($data[7]) ) ? trim($data[7]) : '';
    
                        $gestionObra          = ( isset($data[8]) ) ? trim(utf8_encode($data[8])) : '';
                        $estado               = ( isset($data[9]) ) ? trim(utf8_encode($data[9])) : '';
                        $validacionCall       = ( isset($data[10]) ) ? trim($data[10]) : '';
                        $fechaTratamientoCall = ( isset($data[11]) ) ? trim($data[11]) : '';
    
                        $fechaCableado        = ( isset($data[12]) ) ? trim($data[12]) : '';
    
                        //quitando caracteres especiales a gestionObra y estado
                        $gestionObraUrl = Logic_Utils::constructUrl($gestionObra);
                        $estadoUrl      = Logic_Utils::constructUrl($estado);
    
    
                        //validando los campos
                        $nuevaFechaTerminoConstruccion = '';
                        if ( $fechaTerminoConstruccion != '' ) {
                            $fechaTerminoCons = explode('/', $fechaTerminoConstruccion);
                            if ( count($fechaTerminoCons) == 3 ) {
                                $nuevaFechaTerminoConstruccion = $fechaTerminoCons[2] . '-' .
                                        $fechaTerminoCons[1] . '-' . $fechaTerminoCons[0];
                            }
                        }
    
                        $nuevaDucteriaInternaFecha = '';
                        if ( $ducteriaInternaFecha != '' ) {
                            $ducteriaInternaFec = explode('/', $ducteriaInternaFecha);
                            if ( count($ducteriaInternaFec) == 3 ) {
                                $nuevaDucteriaInternaFecha = $ducteriaInternaFec[2] . '-' .
                                        $ducteriaInternaFec[1] . '-' . $ducteriaInternaFec[0];
                            }
                        }
    
                        $nuevaMontanteFecha = '';
                        if ( $montanteFecha != '' ) {
                            $montanteFec = explode('/', $montanteFecha);
                            if ( count($montanteFec) == 3 ) {
                                $nuevaMontanteFecha = $montanteFec[2] . '-' .
                                        $montanteFec[1] . '-' . $montanteFec[0];
                            }
                        }
    
                        $nuevaPuntoEnergiaFecha = '';
                        if ( $puntoEnergiaFecha != '' ) {
                            $puntoEnergiaFec = explode('/', $puntoEnergiaFecha);
                            if ( count($puntoEnergiaFec) == 3 ) {
                                $nuevaPuntoEnergiaFecha = $puntoEnergiaFec[2] . '-' .
                                        $puntoEnergiaFec[1] . '-' . $puntoEnergiaFec[0];
                            }
                        }
    
                        if ( strtoupper($ducteriaInterna) == 'SI' ) {
                            $ducteriaInterna = 'S';
                        } elseif ( strtoupper($ducteriaInterna) == 'NO' ) {
                            $ducteriaInterna = 'N';
                        }
    
                        if ( strtoupper($montante) == 'SI' ) {
                            $montante = 'S';
                        } elseif ( strtoupper($montante) == 'NO' ) {
                            $montante = 'N';
                        }
    
                        if ( strtoupper($puntoEnergia) == 'SI' ) {
                            $puntoEnergia = 'S';
                        } elseif ( strtoupper($puntoEnergia) == 'NO' ) {
                            $puntoEnergia = 'N';
                        }
    
                        if ( strtoupper($validacionCall) == 'SI' ) {
                            $validacionCall = '1';
                        } elseif ( strtoupper($validacionCall) == 'NO' ) {
                            $validacionCall = '0';
                        }
    
                        $nuevaFechaTratamientoCall = '';
                        if ( $fechaTratamientoCall != '' ) {
                            $fechaTratCall = explode('/', $fechaTratamientoCall);
                            if ( count($fechaTratCall) == 3 ) {
                                $nuevaFechaTratamientoCall = $fechaTratCall[2] . '-' .
                                        $fechaTratCall[1] . '-' . $fechaTratCall[0];
                            }
                        }
    
                        $nuevaFechaCableado = '';
                        if ( $fechaCableado != '' ) {
                            $fechaCabl = explode('/', $fechaCableado);
                            if ( count($fechaCabl) == 3 ) {
                                $nuevaFechaCableado = $fechaCabl[2] . '-' .
                                        $fechaCabl[1] . '-' . $fechaCabl[0];
                            }
                        }
                        //FIN validando los campos
    
    
                        $arrDatos = array(
                            'item'         => $item,
                            'fecha_termino_construccion' => $nuevaFechaTerminoConstruccion,
                            'ducteria_interna_fecha' => $nuevaDucteriaInternaFecha,
                            'montante_fecha' => $nuevaMontanteFecha,
                            'punto_energia_fecha' => $nuevaPuntoEnergiaFecha,
                            'ducteria_interna' => $ducteriaInterna,
                            'montante' => $montante,
                            'punto_energia' => $puntoEnergia,
                            'gestion_obra' => $arrGestionObra[$gestionObraUrl],
                            'estado'       => $arrEdificioEstado[$estadoUrl],
                            'validacion_call' => $validacionCall,
                            'fecha_tratamiento_call' => $nuevaFechaTratamientoCall,
                            'fecha_cableado' => $nuevaFechaCableado,
                            'flag'           => '1'
                        );
    
                        //Grabando el registro de los archivos fuente
                        $objEdificioCargaLima->grabar(
                            $this->_conexion, $arrDatos, $idUsuario
                        );
    
                    } elseif ( $lugar == 'Provincia' ) {
    
                        $codigoProyectoWeb    = ( isset($data[0]) ) ? trim($data[0]) : '';
    
                        $fechaTerminoConstruccion = ( isset($data[1]) ) ? trim($data[1]) : '';
    
                        $estado               = ( isset($data[2]) ) ? trim(utf8_encode($data[2])) : '';
                        $fechaCableado        = ( isset($data[3]) ) ? trim($data[3]) : '';
                        $validacionCall       = ( isset($data[4]) ) ? trim($data[4]) : '';
                        $fechaTratamientoCall = ( isset($data[5]) ) ? trim($data[5]) : '';
                        $rucConstructora      = ( isset($data[6]) ) ? trim($data[6]) : '';
    
                        //quitando caracteres especiales a gestionObra y estado
                        $estadoUrl      = Logic_Utils::constructUrl($estado);
    
    
                        //validando los campos
                        $nuevaFechaTerminoConstruccion = '';
                        if ( $fechaTerminoConstruccion != '' ) {
                            $fechaTerminoCons = explode('/', $fechaTerminoConstruccion);
                            if ( count($fechaTerminoCons) == 3 ) {
                                $nuevaFechaTerminoConstruccion = $fechaTerminoCons[2] . '-' .
                                        $fechaTerminoCons[1] . '-' . $fechaTerminoCons[0];
                            }
                        }
    
                        $nuevaFechaCableado = '';
                        if ( $fechaCableado != '' ) {
                            $fechaCabl = explode('/', $fechaCableado);
                            if ( count($fechaCabl) == 3 ) {
                                $nuevaFechaCableado = $fechaCabl[2] . '-' .
                                        $fechaCabl[1] . '-' . $fechaCabl[0];
                            }
                        }
    
                        if ( strtoupper($validacionCall) == 'SI' ) {
                            $validacionCall = '1';
                        } elseif ( strtoupper($validacionCall) == 'NO' ) {
                            $validacionCall = '0';
                        }
    
                        $nuevaFechaTratamientoCall = '';
                        if ( $fechaTratamientoCall != '' ) {
                            $fechaTratCall = explode('/', $fechaTratamientoCall);
                            if ( count($fechaTratCall) == 3 ) {
                                $nuevaFechaTratamientoCall = $fechaTratCall[2] . '-' .
                                        $fechaTratCall[1] . '-' . $fechaTratCall[0];
                            }
                        }
                        //FIN validando los campos
    
                        $arrDatos = array(
                            'codigo_proyecto_web' => $codigoProyectoWeb,
                            'fecha_termino_construccion' => $nuevaFechaTerminoConstruccion,
                            'estado'         => $arrEdificioEstado[$estadoUrl],
                            'fecha_cableado'      => $nuevaFechaCableado,
                            'validacion_call' => $validacionCall,
                            'fecha_tratamiento_call' => $nuevaFechaTratamientoCall,
                            'ruc_constructora' => $rucConstructora,
                            'flag'           => '1'
                        );
    
                        //Grabando el registro de los archivos fuente
                        $objEdificioCargaProvincia->grabar(
                            $this->_conexion, $arrDatos, $idUsuario
                        );
    
                    }
                }
                
                $i++;
            }
            fclose($openFile);
    
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
    
    public function provinciasUbigeoEdi($idRegion, $codDpto)
    {

        $objRegionUbigeo = new Data_RegionUbigeoEdi();
    
        $arrObjUbigeo = $objRegionUbigeo->traerProvinciasRegion(
            $this->_conexion, $idRegion, $codDpto
        );
    
        $arrUbigeo = array();
        if ( !empty($arrObjUbigeo) ) {
            foreach ($arrObjUbigeo as $objUbigeo) {
                $arrUbigeo[] = array(
                    'codProv' 	=> $objUbigeo->__get('_codProv'),
                    'nombre'	=> $objUbigeo->__get('_nombre')
                );
            }
        }
        echo json_encode($arrUbigeo);
        exit;
    }
    
    
    public function distritosUbigeoEdi($idRegion, $codDpto, $codProv)
    {

        $objRegionUbigeo = new Data_RegionUbigeoEdi();
    
        $arrObjUbigeo = $objRegionUbigeo->traerDistritosRegion(
            $this->_conexion, $idRegion, $codDpto, $codProv
        );
    
        $arrUbigeo = array();
        if ( !empty($arrObjUbigeo) ) {
            foreach ($arrObjUbigeo as $objUbigeo) {
                $arrUbigeo[] = array(
                    'codDist' => $objUbigeo->__get('_codDist'),
                    'nombre'  => $objUbigeo->__get('_nombre')
                );
            }
        }
        echo json_encode($arrUbigeo);
        exit;
    }
    
    
    //$fecha de la forma dd/mm/yyyy y la salida es yyyy-mm-dd
    private function formatFechaDB($fecha)
    {
        $arrFecha = explode('/', $fecha);
        $dia = $arrFecha[0];
        $mes = $arrFecha[1];
        $ano = $arrFecha[2];
    
        return $ano . '-' . $mes . '-' . $dia;
    }
    
    
    private function validateTamanoImagen($imagen, $size)
    {
        $tamano = '0';
    
        //validando el peso de las imagenes
        foreach ($imagen['size'] as $key => $value) {
            if ( $value > 0 ) {
                //si el archivo pesa mas que $size
                if ($imagen['size'][$key] > $size || $imagen['size'][$key] == 0) {
                    $tamano = '1';
                    break;
                }
            }
        }
    
        return $tamano;
    }
    
    
    private function moveFile($imagen, $directorio)
    {
        $nameImage = array();
        $dir = FFTT . $directorio . "/";
    
        foreach ($imagen['name'] as $key => $value) {
            if (is_uploaded_file($imagen['tmp_name'][$key])) {
    
                if ($imagen['name'][$key]!= "") {
                    $tmpName = explode(".", $imagen['name'][$key]);
                    $imagenName = Logic_Utils::constructUrl($tmpName[0]) . '_' . date('Ymd_His');
                    $extension  = $tmpName[count($tmpName)-1];
                    if (isset($imagen['tmp_name'][$key])) {
                        if ( move_uploaded_file($imagen['tmp_name'][$key], $dir . $imagenName.'.'.$extension) ) {
                            $nameImage[] = $imagenName.'.'.$extension;
                        }
                    }
                }
            }
        }
    
        return $nameImage;
    }

    
    //obtengo regiones a partir de las zonales que tiene asignado el usuario
    private function listarRegionesPorZonalUsuario()
    {

        $objRegionZonal    = new Data_RegionZonalEdi();
    
        $arrZonal = array();
        if (!empty($_SESSION['USUARIO_ZONAL'])) {
            foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                $arrZonal[$objZonal->__get('_abvZonal')] =
                $objZonal->__get('_abvZonal');
            }
        }
    
        //obtengo las regiones a partir de las zonales que tiene asignado el 
        //usuario
        $arrObjRegiones = array();
        if (!empty($arrZonal)) {
            $arrObjRegiones = $objRegionZonal->listar(
                $this->_conexion, array(), $arrZonal
            );
        }
    
        $arrRegion = array();
        if (!empty($arrObjRegiones)) {
            foreach ( $arrObjRegiones as $objRegion ) {
                $arrRegion[$objRegion->__get('_idRegion')] = array(
                    'idregion'  => $objRegion->__get('_idRegion'),
                    'nomregion' => $objRegion->__get('_nomRegion'),
                    'detregion' => $objRegion->__get('_detRegion')
                );
            }
        }
    
        return $arrRegion;
    }
    
    
    public function obtenerZonalesPorRegionUsuario($idRegion)
    {

        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objRegionUbigeo = new Data_RegionUbigeoEdi();
    
        //listado de zonales por region y usuario
        $arrZonales = $this->listarZonalesPorRegionUsuario($idRegion);
    
        //listado de fuente de indentificacion por region
        $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listarPorRegion(
            $this->_conexion, $idRegion
        );
    
        $arrFuenteIdentificacion = array();
        if ( !empty($arrObjFuenteIdentificacion) ) {
            foreach ( $arrObjFuenteIdentificacion as $objFuenteIdent ) {
                $arrFuenteIdentificacion[] = array(
                    'codigo' => $objFuenteIdent->__get('_idFuenteIdentificacion'),
                    'nombre' => $objFuenteIdent->__get('_desFuenteIdentificacion')
                );
            }
        }
    
        //listado de departamentos por region
        $arrObjDepartamentos = $objRegionUbigeo->traerDepartamentosRegion(
            $this->_conexion, $idRegion
        );
    
        $arrDepartamentos = array();
        if ( !empty($arrObjDepartamentos) ) {
            foreach ( $arrObjDepartamentos as $objDepartamento) {
                $arrDepartamentos[] = array(
                    'coddpto' => $objDepartamento->__get('_codDpto'),
                    'nombre' => $objDepartamento->__get('_nombre')
                );
            }
        }
    
    
        $arrDataResult = array(
                'arrZonal' => $arrZonales,
                'arrFuenteIdentificacion' => $arrFuenteIdentificacion,
                'arrDepartamento' => $arrDepartamentos
        );
    
        echo json_encode($arrDataResult);
        exit;
    }
    
    
    //obtengo las zonales por region, validando las zonales
    //que tiene asignado el usuario
    private function listarZonalesPorRegionUsuario($idRegion)
    {
        
        $objRegionZonal = new Data_RegionZonalEdi();
    
        $arrRegion[] = $idRegion;
    
        $arrZonal = array();
        if (!empty($_SESSION['USUARIO_ZONAL'])) {
            foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
                $arrZonal[$objZonal->__get('_abvZonal')] =
                $objZonal->__get('_abvZonal');
            }
        }
    
        $arrObjZonal = array();
        if (!empty($arrZonal)) {
            $arrObjZonal = $objRegionZonal->listar(
                $this->_conexion, $arrRegion, $arrZonal
            );
        }

        $arrDataZonal = array();
        if (!empty($arrObjZonal)) {
            foreach ($arrObjZonal as $objZonal) {
                $arrDataZonal[] = array(
                    'zonal'   => $objZonal->__get('_zonal'),
                    'nombre'  => $objZonal->__get('_descZonal')
                );
            }
        }
    
        return $arrDataZonal;
    }

    
}

$objBandejaControlEdificio = new BandejaControlEdificio();

