<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/10/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$objCapas = new Data_FfttCapas();

$arrayCapas = $_SESSION['USUARIO_CAPAS'];

$isLogin = 0;
$dirModulo = '../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
        <title>Webunificada - Movil</title>
        <script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <link href="../../css/movil.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">

            var watchid = null;
            var num_llamadas = 0;
            $(document).ready(function() {
	
                $('#tblPrincipal tr.lnk')
                .filter(':has(:checkbox:checked)')
                .addClass('rowSelectedOpacity')
                .end()
                .click(function(event) {
                    $(this).toggleClass('rowSelectedOpacity');
                    if (event.target.type !== 'checkbox') {
                        $(':checkbox', this).attr('checked', function() {
                            return !this.checked;
                        });
                    }
                });
	
                $('#tblPrincipal tr.lnkAll')
                .filter(':has(:checkbox:checked)')
                .end()
                .click(function(event) {
                    if (event.target.type !== 'checkbox') {
                        $('.capas').attr('checked', function() {
                            return !this.checked;
                        });
                    }
			
                    if ($("#sel").is(':checked')) { 
                        $(".capas").attr('checked', 'checked');
                        $("#tblPrincipal .lnk").addClass('rowSelectedOpacity')
                        $("#selTodos").html('Quitar todos');
                    }else {  
                        $(".capas").attr('checked', '');
                        $("#tblPrincipal .lnk").removeClass('rowSelectedOpacity')
                        $("#selTodos").html('Seleccionar todos');
                    }
                });
	
                var M = {

                    getLocation: function() {
		
                        if (typeof (google) == "undefined") {
                            alert('Verifique su conexion a maps.google.com, vuelva a recargar la pagina');
                            return false;
                        }
			
                        // Try W3C Geolocation (Preferred)
                        if (navigator.geolocation) {

                        	//Original 
                            //M.getPosicionActual();
                            
                            //Para pruebas
                            //seteo manualmente el xy para pruebas
                        	$('#x').attr('value', -77.07341032063886);
                            $('#y').attr('value', -12.07261289528506);

                            //habilito el boton de buscar
                            $('#search').removeAttr("disabled");
                            //FIN Para pruebas

                        }else {
                            alert("Tu navegador no soporta la API de geolocalizacion");
                        }

			
                    },
		
                    getPosicionActual: function() {
                        alert("Capturando su posicion, espere un momento.");
                        watchid = navigator.geolocation.watchPosition(M.obtenerPosicion, M.gestiona_errores, {
                            enableHighAccuracy 	: true,
                            maximumAge			: 20000,
                            timeout				: 10000
                        }
                    );
                    },

                    obtenerPosicion: function(position) {
			
                        num_llamadas++;
			
                        if ( num_llamadas >= 2 ) {
                            if ( confirm("Desea capturar el x,y de su posicion actual1?  Precision: " + position.coords.accuracy + "m.") ) {
                                $('#x').attr('value', position.coords.longitude);
                                $('#y').attr('value', position.coords.latitude);
					
                                //cancelo la busqueda de la posicion repetitiva
                                navigator.geolocation.clearWatch(watchid);
					
                                //habilito el boton de buscar
                                $('#search').removeAttr("disabled");
                            }
                        }
                    },
			
                    gestiona_errores: function(err) {
                        if (err.code == 1) {
                            alert("Se ha denegado la geolocalizaci�n.");
                        }else if (err.code == 2) {
                            alert("Geolocalizacion no disponible.");
                        }else if (err.code == 3) {
                            alert("Tiempo de espera agotado");
                        }else {
                            alert("ERROR: "+ err.message);
                        }
			

                    },
		
                    activarSearch: function() {
                        $('#search').removeAttr("disabled");
                    }
		
                };
	
                $("#search").click(function() {
                    if ($(".capas").is(':checked')) { 
                    } else {  
                        alert("Ingrese algun criterio de busqueda");  
                        return false;  
                    }  
                });

                $("#addComponente").click(function() {
                    var x = '<?php if ( isset($_REQUEST['x']) ) echo $_REQUEST['x'] ?>';
                    var y = '<?php if ( isset($_REQUEST['y']) ) echo $_REQUEST['y'] ?>';
		
                    window.top.location = "m.buscomp.add.php?action=add&x=" + x + "&y=" + y;
                });
	
	
	
	
                <?php
                if (isset($_REQUEST['x']) && isset($_REQUEST['y'])) {
                ?>
                    M.activarSearch();
    	
                <?php
                } else {
                ?>
                    //obtengo el X,Y
                    M.getLocation();
                <?php
                }
                ?>
	
            });

        </script>

    </head>
    <body>

        <?php include '../../m.header.php'; ?>

        <form method="post" action="m.buscomp.vermapa.php">

            <table id="tblPrincipal" class="principal" width="100%">
                <tr>
                    <td class="titulo">B&uacute;squeda de componentes</td>
                </tr>
            </table>

            <?php
            if (isset($_REQUEST['x']) && isset($_REQUEST['y'])) {
                ?>
                <p><input type="button" class="submit" name="addComponente" id="addComponente" value="Agregar un nuevo componente" /></p>
                <?php
            }
            ?>

            <table id="tblPrincipal" class="principal" width="100%">
                <tr>
                    <td class="clsSubtitle">X: <input type="text" size="15" name="x" id="x" value="
                    <?php 
                    if (isset($_REQUEST['x'])) { 
                        echo $_REQUEST['x']; 
                    } else { 
                        echo ''; 
                    } 
                    ?>" readonly="readonly" /><td>
                </tr>
                <tr>
                    <td class="clsSubtitle">Y: <input type="text" size="15" name="y" id="y" value="
                    <?php 
                    if (isset($_REQUEST['y'])) { 
                        echo $_REQUEST['y']; 
                    } else { 
                        echo ''; 
                    } ?>
                    " readonly="readonly" /><td>
                </tr>
            </table>

            <table id="tblPrincipal" class="principal" width="100%">
                <tr>
                    <td class="descPrincipal">Seleccione uno o mas componentes para realizar la b&uacute;squeda</td>
                </tr>
            </table>

            <table id="tblPrincipal" class="principal" width="100%">
                <tr id="chkAll" class="lnkAll">
                    <td class="subtitulo"><span class="imgList"><input type="checkbox" class="capas" id="sel" value="" /></span><span id="selTodos">Seleccionar todos</span></td>
                </tr>
                <?php
                if (!empty($arrayCapas)) {

                    foreach ($arrayCapas as $objCapa) {
                        if ( $objCapa->__get('_indCapa') == 'S' ) {
                            if ($objCapa->__get('_tieneTabla') == 'S') {
                                ?>
                                <tr class="lnk">
                                    <td class="list"><span class="imgList"><input type="checkbox" class="capas" name="comp[<?php echo $objCapa->__get('_abvCapa') ?>]" value="<?php echo $objCapa->__get('_abvCapa') ?>" /></span><?php echo ucfirst(strtolower($objCapa->__get('_nombre'))) ?></td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr class="lnk"> 
                                    <td class="list"><span class="imgList"><input type="checkbox" class="capas" name="capa[<?php echo $objCapa->__get('_idCapa') ?>]" value="<?php echo $objCapa->__get('_abvCapa') ?>" /></span><?php echo ucfirst(strtolower($objCapa->__get('_nombre'))) ?></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                }
                ?>
                <tr>
                    <td class="submit">
                        <input type="submit" name="search" id="search" value="Buscar componentes" class="submit" disabled="disabled" />
                    </td>
                </tr>
            </table>
        </form>


    </body>
</html>