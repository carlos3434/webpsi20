<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.clientes.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$dirModulo = '../../';

$objTerminales = new Data_FfttTerminales();

$arrClientes = $objTerminales->getClientesByTerminal(
    $conexion, $_REQUEST['zonal'], $_REQUEST['mdf'], $_REQUEST['cable'], 
    $_REQUEST['armario'], $_REQUEST['caja'], $_REQUEST['tipo_red']
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php include '../../m.header.php';?>

<div style="margin: 5px;">
<table class="tableBorder lslistado" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">
<tr>
    <td colspan="6" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Clientes asignados a este terminal <?php echo $_REQUEST['caja']?></td>
</tr>
<tr>
    <th width="3%" style="text-align:center;">#</th>
    <th width="5%" style="text-align:center;">Par</th>
    <th width="30%" style="text-align:left;">Cliente</th>
    <th width="40%" style="text-align:left;">Direccion</th>
    <th width="10%" style="text-align:center;">Telefono</th>
    <th width="12%" style="text-align:center;">Estado</th>
</tr>
<?php 
if ( !empty($arrClientes) ) {

    $i = 1;
    foreach ( $arrClientes as $cliente ) {
    ?>
    <tr>
        <td style="text-align:center;"><?php echo $i?></td>
        <td style="text-align:center;"><?php echo ($cliente['Par'] != '') ? $cliente['Par'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Cliente'] != '') ? $cliente['Cliente'] : '&nbsp;'?></td>
        <td style="text-align:left;"><?php echo ($cliente['Direccion'] != '') ? $cliente['Direccion'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['Telefono'] != '') ? $cliente['Telefono'] : '&nbsp;'?></td>
        <td style="text-align:center;"><?php echo ($cliente['DescEstadoPar'] != '') ? $cliente['DescEstadoPar'] : '&nbsp;'?></td>
    </tr>
    <?php 
    $i++;
    }
} else {
?>
    <tr>
        <td colspan="6" style="text-align:center; height:50px;">No se encontro ningun cliente para este terminal.</td>
    </tr>
<?php 
}
?>
</table>

</div>

</body>
</html>