<?php
//require_once 'config/web.config.php';

define("APP_FDR", "webpsi/");
define("APP_DIR", $_SERVER['DOCUMENT_ROOT'] . "/" . APP_FDR);
define("APP_URL", "http://" . $_SERVER['SERVER_NAME'] . APP_FDR);



require_once "clases/class.ConexionMapas.php";
require_once "clases/class.FfttTerminales.php";
require_once "clases/class.FfttCapas.php";
require_once "clases/class.FfttCapasArea.php";
require_once "clases/class.FfttEdificioEstado.php";
require_once "clases/class.FfttCms.php";
require_once "clases/class.Poligono.php";
require_once "clases/class.PoligonoTipo.php";
require_once "clases/class.Ubigeo.php";
require_once "clases/class.TipoPedido.php";
require_once "clases/class.Usuario.php";
require_once "clases/class.UsuarioCapa.php";
require_once "clases/class.GestionObraEdi.php";
require_once "clases/class.SubmodulosAccesos.php";
require_once "clases/class.RegionZonalEdi.php";


require_once "clases/class.DunaAdsl.php";
require_once "clases/class.Zonal.php";


require_once "clases/class.FfttTerminales_smc.php";

//require_once "bbb.php";

//$APP_DIR = "/modulos";
$cab = APP_DIR."cabecera.php";

include_once $cab;

$inc = APP_DIR."includes.php";
//include_once $inc;


function __autoload($className)
{
    $className = 'class.'.$className;
    $arrayClassName= explode("_", $className);
    $elemUlt = $arrayClassName[(count($arrayClassName)-1)];
    unset($arrayClassName[(count($arrayClassName)-1)]);

    $base = strtolower(implode("/", $arrayClassName));

    //$ruta = APP_DIR  . $base . "/class.".$elemUlt . '.php';
	
	$ruta = "clases.".$elemUlt . '.php';
	
	echo $ruta;
	
    if(file_exists($ruta))
        require_once  APP_DIR  . $base ."/class.".$elemUlt . '.php';
}

/*
$objConexion = new Data_DB();

$conexion = $objConexion->conn($dbWu["webunificada"]["type"], $dbWu["webunificada"]["host"], base64_decode($dbWu["webunificada"]["user"]), base64_decode($dbWu["webunificada"]["pass"]), $dbWu["webunificada"]["name"]);

*/


# In PHP 5.2 or higher we don't need to bring this in
if (!function_exists('json_encode')) {
    require_once 'jsonwrapper_inner.php';
} 


