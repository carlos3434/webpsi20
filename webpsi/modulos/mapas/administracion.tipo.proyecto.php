<?php

/**
 * [Web Unificada]
 * Controlador de la Administracion de Segmento.
 * @package    /modulos/maps/
 * @name       administracion.tipo.proyecto.php
 * @category   Controller
 * @author     Fernando Esteban Valerio <festeban@gmd.com.pe>
 * @copyright  GMD S.A.
 * @version    1.0
 */

class AdministracionTipoProyecto extends Data_TipoProyectoEdi
{
    /**
     * Directorio
     * @access protected
     * @var string
     */
    protected $_ruta;
    
    /**
     * Numero de pagina
     * @access protected
     * @var int
     */
    protected $_pagina;
    
    /**
     * @name  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        parent::__construct();
        $this->_pagina = 1;
        $this->_ruta = 'vistas/administracion/tipo_proyecto/';
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getRuta
     * Obtener el valor del atributo "_ruta".
     * @return string
     */
    public function getRuta()
    {
        return $this->_ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setRuta
     * Asignar un valor al atributo "_ruta".
     * @param string $ruta Ruta donde se ubican las vistas del mantenimiento de
     * segmento.
     */
    public function setRuta($ruta)
    {
        $this->_ruta = $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name getPagina
     * Obtener el valor del atributo "_pagina".
     * @return string
     */
    public function getPagina()
    {
        return $this->_pagina;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name setRuta
     * Asignar un valor al atributo "_pagina".
     * @param string $pagina Numero de pagina.
     */
    public function setPagina($pagina)
    {
        $this->_pagina = $pagina;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaAgregar
     * Mostrar el formulario de registro de un tipo de proyecto.
     * @global conexion $conexion Objeto de conexion a la BD creada con PDO.
     * @return void
     */
    public function vistaAgregar()
    {
        global $conexion;
    
        $objSegmento = new Data_SegmentoEdi();
        $arrSegmento = $objSegmento->listar($conexion);
        $ruta = $this->getRuta() . 'v_tipo_proyecto_add.phtml';
        include $ruta;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name guardar
     * Registrar la informacion del tipo de proyecto.
     * @param int $idSegmento Id del segmento.
     * @param string $nombre Nombre del tipo de proyecto.
     * @return string
     */
    public function guardar($idSegmento, $nombre)
    {
        $arrParametros = array();
        $arrParametros['id'] = NULL;
        $arrParametros['segmento'] = $idSegmento;
        $arrParametros['desc'] = $nombre;
        $arrParametros['flag'] = 1;
        
        $mensaje = '';
        $objeto = $this->obtener($idSegmento, $nombre);
        if ($objeto->getIdTipoProyecto()) {
            $mensaje = 'existe';
        } else {
            $id = $this->registrar($arrParametros);
            if ($id) {
                $mensaje = 'ok';
            } else {
                $mensaje = 'error';
            }
        }
        echo $mensaje;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name actualizar
     * Actualizar la informacion del tipo de proyecto.
     * @param int $id Id del tipo de proyecto.     
     * @param int $flag Estado del tipo de proyecto.
     * @return string
     */
    public function actualizar($id, $flag=1)
    {
        global $conexion;
    
        if (is_numeric($id) && is_numeric($flag)) {
            $arrParametros = array();
            $arrParametros['id'] = $id;
            $arrParametros['flag'] = $flag;

            $resultado = $this->cambiarEstado($arrParametros);
            if ($resultado) {
                echo 'ok';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name cambiarPagina
     * Mostrar los tipos de proyecto ubicados en la pagina seleccionada.
     * @param int $pagina Numero de pagina.
     * @param int $total Total de registros.
     * @param int $idSegmento Id del segmento.
     * @return void
     */
    public function cambiarPagina($pagina, $total, $idSegmento)
    {
        $arrTipoProyecto = $this->filtrar($pagina, $idSegmento);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->getRuta() . 'v_tipo_proyecto_paginado.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaMantenimiento
     * Mostrar el modulo de administracion de tipo de proyecto.
     * @global object $conexion Objeto de conexion a la BD creada con PDO.
     * @param int $opcion Opcion.
     * @param int $idSegmento Id del segmento.
     * @return void
     */
    public function vistaMantenimiento($opcion=0, $idSegmento=0)
    {
        global $conexion;
    
        // Obtener los segmentos
        $objSegmento = new Data_SegmentoEdi();
        $arrSegmento = $objSegmento->listar($conexion);
        
        $cadena = '';
        $vista = $this->getRuta() . 'v_tipo_proyecto_lista.phtml';
        $pagina = $this->getPagina();
        $total = $this->contar($idSegmento);
        $arrTipoProyecto = $this->filtrar($pagina, $idSegmento);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . '$("#pag_actual").attr("value", page);'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#id").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaTipoProyecto",'
                    . '{ pagina: page, total: tl, filtro: filtro }, '
                    . 'function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){';
            if ($total > 0) {
                $cadena .= '$("#tb_resultado").show();';
            } else {
                $cadena .= '$("#tb_resultado").hide();'; 
            }
            $cadena .= '$("#cont_num_resultados_head").empty();'
                     . '$("#cont_num_resultados_head").html(' . $total . ');'
                     . '$("#tl_registros").val(' . $total . ');'
                     . '$("#pag_actual").val("' . $pagina . '");'
                     . '$("#paginacion").hide();'
                     . '});'
                     . '</script>';
        }
        if ($total > 0) {
            if (!$opcion) {
                $vista = $this->getRuta() . 'v_tipo_proyecto_lista.phtml';
            } else {
                $vista = $this->getRuta() . 'v_tipo_proyecto_paginado.phtml';
            }
        }
        include $vista;
        echo $cadena;
    }
}

