<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.list.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$isLogin = 0;
$dirModulo = '../../';


//limpiando los markers
unset($_SESSION['MARKERS']);

//array de los nuevos markers
$_SESSION['MARKERS'] = array();

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'search' ) {
    
    $_REQUEST['x'] = ( isset($_REQUEST['x']) && $_REQUEST['x'] != '' ) ? 
        $_REQUEST['x'] : '-77.0407839355469';
    $_REQUEST['y'] = ( isset($_REQUEST['y']) && $_REQUEST['y'] != '' ) ? 
        $_REQUEST['y'] : '-12.0692083678587';
    
    
    $objTerminales = new Data_FfttTerminales();
    $objCms        = new Data_FfttCms();
    $objCapas      = new Data_FfttCapas();
    $objEdificioEstado = new Data_FfttEdificioEstado();

    $arrDataMarkers       = array();
    $arrDataMarkersImg = array();
    
    //marker seleccionado -> icon por default
    $arrDataMarkersImg['pto'] = 'markergreen.png';
    
    $i = 1;
    if ( isset($_REQUEST['comp']['trm']) ) {
    
        $arrDataMarkersImg['trm'] = 'mobilephonetower.png';
        //carga datos de terminales
        $arrayTerminales = $objTerminales->getTerminalesByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );

        $posTrm = 1;
        foreach ($arrayTerminales as $terminales) {
    
            if ( $terminales['y'] != '' && $terminales['x'] != '' ) {
                $color = '#7BB197';
                if ( $terminales['qparlib'] == 0 ) {
                    $color = '#DF4A36';
                    $terminales['estado'] = 1;
                } else if ( $terminales['qparlib'] > 0 && 
                    $terminales['qparlib'] < 3 ) {
                    $color = '#FFF85D';
                    $terminales['estado'] = 2;
                } else if ( $terminales['qparlib'] > 2 ) {
                    $color = '#79B094';
                    $terminales['estado'] = 3;
                }
                
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Terminal: ' . $terminales['caja'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Zonal</td><td>' . $terminales['zonal'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Mdf</td><td>' . $terminales['mdf'] . '</td></tr>';
                if ( $terminales['tipo_red'] == 'D' ) {
                    $detalle .= '<tr><td class="compSubTitulo">Cable</td><td>' . $terminales['cable'] . '</td></tr>';
                } elseif ( $terminales['tipo_red'] == 'F' ) {
                    $detalle .= '<tr><td class="compSubTitulo">Armario</td><td>' . $terminales['armario'] . '</td></tr>';
                }
                $detalle .= '<tr><td class="compSubTitulo">Caja</td><td>' . $terminales['caja'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Capacidad</td><td>' . $terminales['qcapcaja'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares libres</td><td>' . $terminales['qparlib'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares reserv.</td><td>' . $terminales['qparres'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares distrib.</td><td>' . $terminales['qdistrib'] . '</td></tr>';
                $detalle .= '</table>';
                
                $detalle .= '<div style="height: 10px;"></div>';
                $detalle .= '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td><a href="m.buscomp.clientes.php?zonal=' . $terminales['zonal1'] . '&mdf=' . $terminales['mdf'] . '&cable=' . $terminales['cable'] . '&armario=' . $terminales['armario'] . '&caja=' . $terminales['caja'] . '&tipo_red=' . $terminales['tipo_red'] . '">Ver clientes</a></td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['trm'][$posTrm] = $detalle;
                
                $arrTrmDetalle[] = array(
                    'title'     => $terminales['caja'],
                    'estado'     => $terminales['estado'],
                    'qparlib'    => $terminales['qparlib'],
                    'y'             => $terminales['y'],
                    'x'             => $terminales['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $terminales['distance'],
                    'pos'        => $posTrm,
                    'tip'        => 'trm',
                    'lnk'        => '<span class="searchsubmit" style="float: right;"><a href="m.buscomp.clientes.php?zonal=' . $terminales['zonal1'] . '&mdf=' . $terminales['mdf'] . '&cable=' . $terminales['cable'] . '&armario=' . $terminales['armario'] . '&caja=' . $terminales['caja'] . '&tipo_red=' . $terminales['tipo_red'] . '">Ver clientes</a></span>',
                    'img'        => "../../img/map/terminal/trm_" . $terminales['caja'] . "--" . $terminales['estado'] . ".gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['trm'] = array(
                    'name'        => 'Terminales',
                    'data'        => $arrTrmDetalle
                );
                $i++;
            }
            $posTrm++;
        }
    }
    
    if ( isset($_REQUEST['comp']['arm']) ) {
    
        $arrDataMarkersImg['arm'] = 'powersubstation.png';
        
        //carga datos de armarios
        $arrayArmarios = $objTerminales->getArmariosByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posArm = 1;
        foreach ($arrayArmarios as $armarios) {
    
            if ( $armarios['y'] != '' && $armarios['x'] != '' ) {
                $color = '#7BB197';
                if ( $armarios['estado'] == '1' ) {
                    $color = '#7BB197';
                } elseif ( $armarios['estado'] == '2' ) {
                    $color = '#FFF86A';
                } elseif ( $armarios['estado'] == '3' ) {
                    $color = '#E97F7A';
                }
            
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Armario: ' . $armarios['armario'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Zonal</td><td>' . $armarios['zonal'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Mdf</td><td>' . $armarios['mdf'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Armario</td><td>' . $armarios['armario'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Capacidad</td><td>' . $armarios['qcaparmario'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares libres</td><td>' . $armarios['qparlib'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares reserv.</td><td>' . $armarios['qparres'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Pares distrib.</td><td>' . $armarios['qdistrib'] . '</td></tr>';
                $detalle .= '</table>';
            
                $_SESSION['MARKERS']['arm'][$posArm] = $detalle;
                
                $arrArmDetalle[] = array(
                    'title'     => $armarios['armario'],
                    'estado'     => $armarios['estado'],
                    'y'             => $armarios['y'],
                    'x'             => $armarios['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $armarios['distance'],
                    'pos'        => $posArm,
                    'tip'        => 'arm',
                    'lnk'        => '',
                    'img'        => "../../img/map/armario/arm_" . $armarios['armario'] . "--" . $armarios['estado'] . ".gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['arm'] = array(
                    'name'        => 'Armarios',
                    'data'        => $arrArmDetalle
                );
                $i++;
            }
            $posArm++;
        }
    }
    
    if ( isset($_REQUEST['comp']['mdf']) ) {
    
        $arrDataMarkersImg['mdf'] = 'tent.png';
    
        //carga datos de mdfs
        $arrayMdfs = $objTerminales->getMdfsByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posMdf = 1;
        foreach ($arrayMdfs as $mdfs) {
    
            if ( $mdfs['y'] != '' && $mdfs['x'] != '' ) {
                $color = '#7BB197';
                if ( $mdfs['estado'] == '1' ) {
                    $color = '#7BB197';
                } elseif ( $mdfs['estado'] == '2' ) {
                    $color = '#FFF86A';
                } elseif ( $mdfs['estado'] == '3' ) {
                    $color = '#E97F7A';
                }
                
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle MDF: ' . $mdfs['mdf'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Zonal</td><td>' . $mdfs['zonal'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Mdf</td><td>' . $mdfs['mdf'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Direccion</td><td>' . $mdfs['direccion'] . '</td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['mdf'][$posMdf] = $detalle;
                
                $mdfDetalle[] = array(
                    'title'     => $mdfs['mdf'],
                    'estado'     => $mdfs['estado'],
                    'y'             => $mdfs['y'],
                    'x'             => $mdfs['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $mdfs['distance'],
                    'pos'        => $posMdf,
                    'tip'        => 'mdf',
                    'lnk'        => '',
                    'img'        => "../../img/map/mdf/mdf_" . $mdfs['mdf'] . "--" . $mdfs['estado'] . ".gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['mdf'] = array(
                    'name'        => 'Mdfs',
                    'data'        => $mdfDetalle
                );
                $i++;
            }
            $posMdf++;
        }
    }
    
    if ( isset($_REQUEST['comp']['trb']) ) {

        $arrDataMarkersImg['trb'] = 'tent.png';
    
        //carga datos de taps
        $arrayTap = $objCms->getTrobasByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posTrb = 1;
        foreach ($arrayTap as $taps) {
    
            if ( $taps['y'] != '' && $taps['x'] != '' ) {
                $color = '#DA251D';
            
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Troba: ' . $taps['troba'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Nodo</td><td>' . $taps['nodo'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Troba</td><td>' . $taps['troba'] . '</td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['trb'][$posTrb] = $detalle;
                
                $arrTrbDetalle[] = array(
                    'title'     => $taps['troba'],
                    'estado'     => '1',
                    'y'             => $taps['y'],
                    'x'             => $taps['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $taps['distance'],
                    'pos'        => $posTrb,
                    'tip'        => 'trb',
                    'lnk'        => '',
                    'img'        => "../../img/map/troba/trb-r.gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['trb'] = array(
                    'name'        => 'Trobas',
                    'data'        => $arrTrbDetalle
                );
                $i++;
            }
            $posTrb++;
        }
    }
    
    if ( isset($_REQUEST['comp']['tap']) ) {

        $arrDataMarkersImg['tap'] = 'tent.png';
    
        //carga datos de taps
        $arrayTap = $objCms->getTapsByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posTap = 1;
        foreach ($arrayTap as $taps) {
    
            if ( $taps['y'] != '' && $taps['x'] != '' ) {
                $color = '#FFF500';
                
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Tap: ' . $taps['tap'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Nodo</td><td>' . $taps['nodo'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Troba</td><td>' . $taps['troba'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Amplificador</td><td>' . $taps['amplificador'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Tap</td><td>' . $taps['tap'] . '</td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['tap'][$posTap] = $detalle;
                
                $arrTapDetalle[] = array(
                    'title'     => $taps['tap'],
                    'estado'     => '1',
                    'y'             => $taps['y'],
                    'x'             => $taps['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $taps['distance'],
                    'pos'        => $posTap,
                    'tip'        => 'tap',
                    'lnk'        => '',
                    'img'        => "../../img/map/tap/tap_" . $taps['tap'] . "--1.gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['tap'] = array(
                    'name'        => 'Taps',
                    'data'        => $arrTapDetalle
                );
                $i++;
            }
            $posTap++;
        }
    }
    
    if ( isset($_REQUEST['comp']['edi']) ) {

        $arrDataMarkersImg['edi'] = 'edificio';
        
        $arrTipoCchh            = $objCapas->getDescriptorByGrupo($conexion, 1);
        $arrTipoProyecto        = $objCapas->getDescriptorByGrupo($conexion, 2);
        $arrTipoInfraestructura = $objCapas->getDescriptorByGrupo($conexion, 3);
        $arrTipoVia             = $objCapas->getDescriptorByGrupo($conexion, 4);
        
        $arrEstado = array(
            'I' => array('estado' => 'Iniciado',     'color' => '#E5E400'),
            'P' => array('estado' => 'En proceso',     'color' => '#88B750'),
            'T' => array('estado' => 'Terminado',     'color' => '#C74000')
        );
        
    
        //carga datos de edificios
        $arrayEdificios = $objTerminales->getEdificiosByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posEdi = 1;
        foreach ($arrayEdificios as $edificios) {
    
            if ( $edificios['y'] != '' && $edificios['x'] != '' ) {
                
                /*if ( array_key_exists($edificios['estado'], $arrEstado) ) {
                    $color     = $arrEstado[$edificios['estado']]['color'];
                    $estado = $arrEstado[$edificios['estado']]['estado'];
                } else {
                    $color  = '#E5E400';
                    $estado = 'Sin estado';
                }*/
                
                $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado($conexion, $edificios['estado']);
                $estado = '';
                $color  = '#E5E400';
                if ( !empty($arrObjEdificioEstado) ) {
                    $estado = $arrObjEdificioEstado[0]->__get('_desEstado');
                }

                $ura = $objTerminales->getMdfByCodMdf($conexion, $edificios['ura']);
                
                $tipovia = '';
                foreach ( $arrTipoVia as $tipoVia ) {
                    if ( $edificios['idtipo_via'] == $tipoVia['iddescriptor'] ) {
                        $tipovia = $tipoVia['nombre'];
                        break;
                    }
                }
                
                $tipocchh = '';
                foreach ( $arrTipoCchh as $tipoCchh ) {
                    if ( $edificios['idtipo_cchh'] == $tipoCchh['iddescriptor'] ) {
                        $tipocchh = $tipoCchh['nombre'];
                        break;
                    }
                }
                
                $tipoproyecto = '';
                foreach ( $arrTipoProyecto as $tipoProyecto ) {
                    if ( $edificios['idtipo_proyecto'] == $tipoProyecto['iddescriptor'] ) {
                        $tipoproyecto = $tipoProyecto['nombre'];
                        break;
                    }
                }
                
                $tipoinfraestructura = '';
                foreach ( $arrTipoInfraestructura as $tipoInfraestructura ) {
                    if ( $edificios['idtipo_infraestructura'] == $tipoInfraestructura['iddescriptor'] ) {
                        $tipoinfraestructura = $tipoInfraestructura['nombre'];
                        break;
                    }
                }
                
                $codDep   = substr($edificios['distrito'], 0, 2);
                $codProv = substr($edificios['distrito'], 2, 2);
                $arrDpto  = $objCapas->getDepartamentos($conexion);
                $arrProv  = $objCapas->getProvinciasByDpto($conexion, $codDep);
                $arrDist  = $objCapas->getDistritosByProv($conexion, $codDep, $codProv);
                
                foreach ( $arrDpto as $dpto ) {
                    if ( substr($edificios['distrito'], 0, 2) == $dpto['coddpto'] ) {
                        $departamento = $dpto['nombre'];
                        break;
                    }
                }
                
                foreach ( $arrProv as $prov ) {
                    if ( substr($edificios['distrito'], 2, 2) == $prov['codprov'] ) {
                        $provincia = $prov['nombre'];
                        break;
                    }
                }
                
                foreach ( $arrDist as $dist ) {
                    if ( substr($edificios['distrito'], 4, 2) == $dist['coddist'] ) {
                        $distrito = $dist['nombre'];
                        break;
                    }
                }
                
                $fechaTerminoConstruccion = ( $edificios['fecha_termino_construccion'] != '0000-00-00' ) ? 
                    date('m/d/Y', strtotime($edificios['fecha_termino_construccion'])) : '';
                $montanteFecha = ( $edificios['montante_fecha'] != '0000-00-00' && $edificios['montante_fecha'] != '' ) ? 
                    date('m/d/Y', strtotime($edificios['montante_fecha'])) : '';
                $ducteriaInternaFecha = ( $edificios['ducteria_interna_fecha'] != '0000-00-00' && $edificios['ducteria_interna_fecha'] != '' ) ? 
                    date('m/d/Y', strtotime($edificios['ducteria_interna_fecha'])) : '';
                $puntoEnergiaFecha = ( $edificios['punto_energia_fecha'] != '0000-00-00' && $edificios['punto_energia_fecha'] != '' ) ? 
                    date('m/d/Y', strtotime($edificios['punto_energia_fecha'])) : '';
                $fechaSeguimiento = ( $edificios['fecha_seguimiento'] != '0000-00-00') ? 
                    date('m/d/Y', strtotime($edificios['fecha_seguimiento'])) : '';
                
                
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Edificio: ' . $edificios['item'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Estado</td><td>' . $estado . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Item</td><td>' . $edificios['item'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Foto</td><td><img border="0" src="../../pages/imagenComponente.php?imgcomp=' . $edificios['foto1'] . '&dircomp=edi&w=150"><br /></td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Fecha Reg. proyecto</td><td>' . substr($edificios['fecha_registro_proyecto'], 0, 10) . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">URA</td><td>' . $ura[0]['mdf'] . ' - ' . $ura[0]['nombre'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Sector</td><td>' . $edificios['sector'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Mza. (TdP)</td><td>' . $edificios['mza_tdp'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Direccion de Obra</td><td>' . $tipovia . ' ' . $edificios['direccion_obra'] . ' ' . $edificios['numero'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Mza.</td><td>' . $edificios['mza'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Lote</td><td>' . $edificios['lote'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">CCHH.</td><td>' . $tipocchh . ' ' . $edificios['cchh'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Departamento</td><td>' . $departamento . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Provincia</td><td>' . $provincia . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Distrito</td><td>' . $distrito . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Nombre de Constructora</td><td>' . $edificios['nombre_constructora'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Nombre del proyecto</td><td>' . $edificios['nombre_proyecto'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Tipo proyecto</td><td>' . $tipoproyecto . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Persona contacto</td><td>' . $edificios['persona_contacto'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Pagina web</td><td>' . $edificios['pagina_web'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">E-mail</td><td>' . $edificios['email'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">N&deg; de Blocks</td><td>' . $edificios['nro_blocks'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo"></td><td>';
                $detalle .= '<div style="width:200px; display:block; height:20px;">';
                $detalle .= '<span style="float:left; width:40px;">&nbsp;</span><span style="float:left; width:80px; font-weight:bold;">Nro. pisos</span>';
                $detalle .= '<span style="float:left; width:80px;font-weight:bold;">Nro. dptos</span>';
                $detalle .= '</div>';
                $edificioNumeroBlocks = 5;
                if ( $edificios['nro_blocks'] <= 5 ) {
                    $edificioNumeroBlocks = $edificios['nro_blocks'];
                }
                for ($a=1; $a<=$edificioNumeroBlocks; $a++) {
                    $detalle .= '<div style="width:200px; display:block; height:20px;">';
                    $detalle .= '<span style="float:left; width:40px; padding:2px 0;">#' . $a . '</span><span style="float:left; width:80px; padding:2px 0;">' . $edificios['nro_pisos_' . $a] . '</span>';
                    $detalle .= '<span style="float:left;"></span><span style="float:left; width:80px; padding:2px 0;">' . $edificios['nro_dptos_' . $a] . '</span>';
                    $detalle .= '</div>';
                }
                $detalle .= '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Tipo Infraestructura</td><td>' . $tipoinfraestructura . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">N&deg; Dptos Habitados</td><td>' . $edificios['nro_dptos_habitados'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">% Avance</td><td>' . $edificios['avance'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Fecha de Termino Construcci&oacute;n</td><td>' . $fechaTerminoConstruccion . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Montante</td><td>' . $edificios['montante'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Montante (Fecha)</td><td>' . $montanteFecha . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Montante (Observacion)</td><td>' . $edificios['montante_obs'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Ducteria Interna</td><td>' . $edificios['ducteria_interna'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Ducteria Interna (Fecha)</td><td>' . $ducteriaInternaFecha . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Ducteria Interna (Observacion)</td><td>' . $edificios['ducteria_interna_obs'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Punto de Energia</td><td>' . $edificios['punto_energia'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Punto de Energia (Fecha)</td><td>' . $puntoEnergiaFecha . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Punto de Energia (Observacion)</td><td>' . $edificios['punto_energia_obs'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Fecha de seguimiento</td><td>' . $fechaSeguimiento . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Observaci&oacute;n</td><td>' . $edificios['observacion'] . '</td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['edi'][$posEdi] = $detalle;
                
                $arrEdiDetalle[] = array(
                    'title'     => $edificios['item'],
                    'idedificio'=> $edificios['idedificio'],
                    'estado'     => $edificios['estado'],
                    'y'             => $edificios['y'],
                    'x'             => $edificios['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $edificios['distance'],
                    'pos'        => $posEdi,
                    'tip'        => 'edi',
                    'lnk'        => '<span class="searchsubmit" style="float: right;"><a href="m.buscomp.add.componente.php?cmd=editarEdificio&capa=edi&idedificio=' . $edificios['idedificio'] . '&x=' . $edificios['x'] . '&y=' . $edificios['y'] . '">Editar edif.</a></span>',
                    'img'        => "../../img/map/edificio/edificio-" + $edificios['estado'] + ".gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['edi'] = array(
                    'name'        => 'Edificios',
                    'data'        => $arrEdiDetalle
                );
                $i++;
            }
            $posEdi++;
        }
    }
    
    if ( isset($_REQUEST['comp']['com']) ) {

        $arrDataMarkersImg['com'] = 'competencia.png';
        
        $color  = '#E5E400';
        
        $arrCompetencia            = $objCapas->getDescriptorByGrupo($conexion, 5);
        $arrElementoEncontrado     = $objCapas->getDescriptorByGrupo($conexion, 6);
    
        //carga datos de competencias
        $arrayCompetencias = $objTerminales->getCompetenciasByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posCom = 1;
        foreach ($arrayCompetencias as $competencias) {
    
            if ( $competencias['y'] != '' && $competencias['x'] != '' ) {
            
                $color = '#7BB197';
                if ( $competencias['estado'] == '1' ) {
                    $color = '#7BB197';
                } elseif ( $competencias['estado'] == '2' ) {
                    $color = '#FFF86A';
                } elseif ( $competencias['estado'] == '3' ) {
                    $color = '#E97F7A';
                }
            
                $ura = $objTerminales->getMdfByCodMdf($conexion, $competencias['ura']);
                
                $competencia = '';
                foreach ( $arrCompetencia as $compet ) {
                    if ( $competencias['competencia'] == $compet['iddescriptor'] ) {
                        $competencia = $compet['nombre'];
                        break;
                    }
                }
                
                $elementoencontrado = '';
                foreach ( $arrElementoEncontrado as $elementoEncontrado ) {
                    if ( $competencias['elementoencontrado'] == $elementoEncontrado['iddescriptor'] ) {
                        $elementoencontrado = $elementoEncontrado['nombre'];
                        break;
                    }
                }

                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Competencia: ' . $competencias['idcompetencia'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Competencia</td><td>' . $competencias['idcompetencia'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Foto</td><td><img border="0" src="../../pages/imagenComponente.php?imgcomp=' . $competencias['foto1'] . '&dircomp=com&w=150"><br /></td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">URA</td><td>' . $ura[0]['mdf'] . ' - ' . $ura[0]['nombre'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Direcci&oacute;n</td><td>' . $competencias['direccion'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Competencia</td><td>' . $competencia . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Otro</td><td>' . $competencias['competencia_otro'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Elemento encontrado</td><td>' . $elementoencontrado . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Otro</td><td>' . $competencias['elementoencontrado_otro'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Observacion</td><td>' . $competencias['observacion'] . '</td></tr>';
                $detalle .= '</table>';
                
                $_SESSION['MARKERS']['com'][$posCom] = $detalle;
                
                $arrComDetalle[] = array(
                    'title'     => $competencias['idcompetencia'],
                    'estado'     => $competencias['estado'],
                    'y'             => $competencias['y'],
                    'x'             => $competencias['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $competencias['distance'],
                    'pos'        => $posCom,
                    'tip'        => 'com',
                    'lnk'        => '<span class="searchsubmit" style="float: right;"><a href="m.buscomp.editar.componente.php?capa=com&idcompetencia=' . $competencias['idcompetencia'] . '&x=' . $competencias['x'] . '&y=' . $competencias['y'] . '">Editar com.</a></span>',
                    'img'        => "../../img/map/competencia/competencia-" . $competencias['estado'] . ".gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['com'] = array(
                    'name'        => 'Competencias',
                    'data'        => $arrComDetalle
                );
                $i++;
            }
            $posCom++;
        }
    }
    
    
    if ( isset($_REQUEST['comp']['esb']) ) {

        $arrDataMarkersImg['esb'] = 'estacionbase';
        $color = '#3399CC';
    
        //carga datos de edificios
        $arrayEdificios = $objTerminales->getEstacionbasesByXy(
            $conexion, $_REQUEST['x'], $_REQUEST['y'], 5, 10
        );
        
        $posEsb = 1;
        foreach ($arrayEdificios as $edificios) {
    
            if ( $edificios['y'] != '' && $edificios['x'] != '' ) {
            
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Estacion Base: ' . $edificios['nombre'] . '</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Estacion Base</td><td>' . $edificios['nombre'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Direcci&oacute;n</td><td>' . $edificios['direccion'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Departamento</td><td>' . $edificios['departamento'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Provincia</td><td>' . $edificios['provincia'] . '</td></tr>';
                $detalle .= '<tr><td class="compSubTitulo">Distrito</td><td>' . $edificios['distrito'] . '</td></tr>';
                $detalle .= '</table>';
            
                $_SESSION['MARKERS']['esb'][$posEsb] = $detalle;
                
                $arrEsbDetalle[] = array(
                    'title'     => $edificios['nombre'],
                    'estado'     => '1',
                    'y'             => $edificios['y'],
                    'x'             => $edificios['x'],
                    'i'             => $i,
                    'detalle'    => $detalle,
                    'distance'    => $edificios['distance'],
                    'pos'        => $posEsb,
                    'tip'        => 'esb',
                    'lnk'        => '',
                    'img'        => "../../img/map/estacionbase/esb-1.gif",
                    'color'        => $color
                );
                
                $arrDataMarkers['esb'] = array(
                    'name'        => 'Estaciones Base',
                    'data'        => $arrEsbDetalle
                );
                $i++;
            }
            $posEsb++;
        }
    }
    

    /*======================== INFORMACION DE CAPAS ==========================*/
    
    if ( isset($_REQUEST['capa']) && !empty($_REQUEST['capa']) ) {
    
        $arrCapas = $objCapas->getCapas($conexion);
        $arrayCapa = array();
        foreach ( $arrCapas as $capa ) {
            $arrayCapa[$capa['idcapa']] = array(
                'nombre'    => $capa['nombre'],
                'abv_capa'    => $capa['abv_capa'],
                'ico'        => $capa['ico']
            );
        }

    
        foreach ( $_REQUEST['capa'] as $idCapa => $abvCapa ) {
        
            //almaceno las imagenes segun la capa
            $arrDataMarkersImg[$abvCapa] = $arrayCapa[$idCapa]['ico'];
        
            //carga de campos segun la capa
            $camposCapaArr = $objCapas->getCamposByCapa($conexion, $idCapa);
            $arrDataCampos = array();
            $campoPrincipal = 'campo1';
            foreach ( $camposCapaArr as $campoCapa ) {
                if ($campoCapa['ind_principal'] == 1) {
                    $campoPrincipal = 'campo' . $campoCapa['campo_nro'];
                }
                $arrDataCampos['campo' . $campoCapa['campo_nro']] = $campoCapa['campo'];
            }

            //carga datos de capa = casa, tienda, cabina
            $arrayCapas = $objCapas->getCapasByXy(
                $conexion, $idCapa, $arrDataCampos, $_REQUEST['x'], 
                $_REQUEST['y'], 10, 10
            );
            
            $arrCapDetalle = array();
            $posCap = 1;
            foreach ($arrayCapas as $capas) {
                
                $detalle = '<table class="tableBorder lsdetalle" border="0" cellspacing="1" cellpadding="4" align="center" width="100%">';
                $detalle .= '<tr><td colspan="2" style="font-size: 14px;font-weight: bold;padding: 2px 5px;background-color: #92A2B6;border-bottom: solid 1px #888888;color: #FFFFFF;">Detalle Capa</td></tr>';
                $detalle .= '<tr><td width="100" class="compSubTitulo">Foto</td><td><img src="../../pages/imagenComponente.php?imgcomp=' . $capas['foto1'] . '&dircomp=' . $arrayCapa[$idCapa]['abv_capa'] . '&w=120"></td></tr>';
                foreach ($arrDataCampos as $nroCampo => $campo) {
                    $detalle .= '<tr><td class="compSubTitulo">' . $campo . '</td><td>' . $capas[$nroCampo] . '</td></tr>';
                }
                $detalle .= '</table>';
    
                if ( $capas['y'] != '' && $capas['x'] != '' ) {
                    
                    $_SESSION['MARKERS'][$abvCapa][$posCap] = $detalle;
                
                    $arrCapDetalle[] = array(
                        'title'     => $capas[$campoPrincipal],
                        'estado'     => '1',
                        'y'             => $capas['y'],
                        'x'             => $capas['x'],
                        'i'             => $i,
                        'detalle'    => utf8_encode($detalle),
                        'distance'    => $capas['distance'],
                        'pos'        => $posCap,
                        'tip'        => $abvCapa,
                        'lnk'        => '',
                        'img'        => '',
                        'color'        => $color
                    );
                    
                    $arrDataMarkers[$abvCapa] = array(
                        'name'        => $arrayCapa[$capas['idcapa']]['nombre'],
                        'data'        => $arrCapDetalle
                    );
                    $i++;
                
                }
                $posCap++;
            }    
        }
        
    }
    
    
    $capaSelected = array(
        'capa'    => '',
        'zoom_sel' => 11
    );
    
    if ( isset($_REQUEST['capa_selected']) && $_REQUEST['capa_selected'] != '' ) {
        $capaSelected = array(
            'capa'    => $_REQUEST['capa_selected'],
            'zoom_sel' => $_REQUEST['zoom_selected']
        );
    }

    $arrDataResult = array(
        'sites'     => $arrDataMarkers,
        'punto_sel'    => array('x' => $_REQUEST['x'], 'y' => $_REQUEST['y']),
        'capa_ico'    => $arrDataMarkersImg, 
        'comps_sel'    => $_REQUEST['comp'], 
        'capas_sel'    => ( isset($_REQUEST['capa']) ) ? $_REQUEST['capa'] : array()
    );
    
    //echo json_encode($arrDataMarkers);
    //exit;
}


include 'vistas/m.v_buscomp.list.php'; 