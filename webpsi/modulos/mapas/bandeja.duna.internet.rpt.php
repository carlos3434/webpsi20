<?php
/**
 * @author Wilder Sandoval H
 * @copyright 2011
 */
 
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

function limpia_cadena($value)
{
   $nopermitidos = array("'",'\\','<','>',"\"",";"); 
   $nuevaCadena = str_replace($nopermitidos, "", $value); 
   return $nuevaCadena;
}

function cleanStr($str)
{
    $search	= array('\t', '\n', '\r\n');
    $replace = array('', '', '');
    return str_replace($search, $replace, trim($str));
}

$file = "/tmp/rpt_duna_internet.xls";

$filename = $file;

$file = fopen($filename, "w+");

$titulos = "DIRECCION\tTIPO ANTENA\tCOORD X\tCOORD Y\tFOTO 1"
         . "\tFECHA DETECCION\tEXPEDIENTE\tVERSION\tFECHA EXPEDIENTE\tASOCIADO"
         . "\tFOTO CORRECTA\tESTADO\tOBSERVACIONES\tTITULAR\tDNI"
         . "\tDEPARTAMENTO\tPROVINCIA\tDISTRITO"
         . "\tDIRECCION ANTENA\tDIRECCION TELEFONO\tDIRECCIONES IGUALES"
         . "\tTELEFONO\tINSCRIPCION\tESTADO LINEA\tSPEEDY CONTRATADO"
         . "\tESTADO SPEEDY";

fwrite($file, $titulos.'\r\n');

$sql = 'SELECT UCASE(comp.campo1) AS direccion, UCASE(comp.campo2) AS '
     . 'tipo_antena, comp.x, comp.y, comp.foto1, comp.fecha_insert, ' 
     . 'duna.expediente, duna.version, duna.fecha_deteccion, '
     . 'duna.fecha_expediente, duna.asociado, duna.foto_correcta, '
     . 'duna.estado, duna.observaciones, daso.titular, daso.dni, '
     . 'daso.departamento, daso.provincia, daso.distrito, '
     . 'daso.direccion_antena, daso.direccion_telefono, '
     . 'daso.direcciones_iguales, daso.telefono, daso.inscripcion, '
     . 'daso.estado_linea, daso.speedy_contratado, daso.estado_speedy '
     . 'FROM webunificada_fftt.fftt_componente comp LEFT JOIN '
     . 'webunificada_fftt.duna duna ON comp.idcomponente = duna.idcomponente '
     . 'AND duna.ind_duna = "S" LEFT JOIN webunificada_fftt.duna_asociado daso '
     . 'ON duna.idduna = daso.idduna AND daso.ind_duna_asociado = "S" ' 
     . 'WHERE comp.idcapa = "21" ' 
     . 'ORDER BY comp.fecha_insert DESC';

$i = 0;

$bind = $conexion->prepare($sql);
$bind->execute();

while ($row = $bind->fetch(PDO::FETCH_ASSOC)) {	
    // Pintamos una linea en el archivo de texto, primero en una variable
    $filaData = cleanStr(limpia_cadena($row['direccion'])) . "\t";
    $filaData .= $row['tipo_antena'] . "\t";
    $filaData .= $row['x'] . "\t";
    $filaData .= $row['y'] . "\t";
    $filaData .= $row['foto1'] . "\t";
    $filaData .= $row['fecha_insert'] . "\t";
    $filaData .= $row['expediente'] . "\t";
    $filaData .= $row['version'] . "\t";
    $filaData .= $row['fecha_expediente'] . "\t";
    $filaData .= $row['asociado'] . "\t";
    $filaData .= $row['foto_correcta'] . "\t";
    $filaData .= $row['estado'] . "\t";
    $filaData .= cleanStr(limpia_cadena($row['observaciones'])) . "\t";
    $filaData .= cleanStr(limpia_cadena($row['titular'])) . "\t";
    $filaData .= $row['dni'] . "\t";
    $filaData .= $row['departamento'] . "\t";
    $filaData .= $row['provincia'] . "\t";
    $filaData .= $row['distrito'] . "\t";
    $filaData .= cleanStr(limpia_cadena($row['direccion_antena'])) . "\t";
    $filaData .= cleanStr(limpia_cadena($row['direccion_telefono'])) . "\t";
    $filaData .= $row['direcciones_iguales'] . "\t";
    $filaData .= $row['telefono'] . "\t";
    $filaData .= $row['inscripcion'] . "\t";
    $filaData .= $row['estado_linea'] . "\t";
    $filaData .= $row['speedy_contratado'] . "\t";
    $filaData .= $row['estado_speedy'] . "\t";
    
    fwrite($file, $filaData . "\r\n");
    $i++;
}

$filename = realpath($filename);
$fileExtension = strtolower(substr(strrchr($filename, "."), 1));

switch ($fileExtension) {
    case "pdf": $ctype="application/pdf";
        break;
    
    case "exe": $ctype="application/octet-stream"; 
        break;
        
    case "zip": $ctype="application/zip";
        break;
        
    case "doc": $ctype="application/msword";
        break;
        
    case "xls": $ctype="application/vnd.ms-excel";
        break;
        
    case "ppt": $ctype="application/vnd.ms-powerpoint";
        break;
    
    case "gif": $ctype="image/gif";
        break;
    
    case "png": $ctype="image/png";
        break;
    
    case "jpe": case "jpeg":
        
    case "jpg": $ctype="image/jpg";
        break;

    default: $ctype="application/force-download";
}

if (!file_exists($filename)) {
    die("NO FILE HERE");
}

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false);
header("Content-Type: $ctype");
header("Content-Disposition: attachment; filename=\"".basename($filename)."\";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".@filesize($filename));
set_time_limit(0);
@readfile("$filename") or die("File not found.");


