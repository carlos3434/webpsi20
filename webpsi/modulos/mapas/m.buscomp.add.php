<?php

/**
 * @package     /
 * @name        m.buscomp.add.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$objCapas = new Data_FfttCapas();
    
$arrayCapas = $_SESSION['USUARIO_CAPAS'];


$isLogin = 0;
$dirModulo = '../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<?php
if ( isset($_REQUEST['action']) && $_GET['action'] == 'add' ) {
?>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        
        var map = null;
        var markers_arr = [];
        
        var M = {
            initialize: function() {
                var latlng = new google.maps.LatLng(<?php echo $_REQUEST['y']?>, <?php echo $_REQUEST['x']?>);
                var myOptions = {
                    zoom: 17,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ANDROID,
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    }
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: 'vistas/icons/markergreen.png'
                });
                markers_arr.push(marker);
                
                google.maps.event.addListener(map, "click", function(event) {
                
                    if (!confirm("Desea establecer este nuevo punto x,y?")) {
                        return false;
                    }
                
                    //clear markers
                    M.clearMarkers();
                    
                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map,
                        icon: 'vistas/icons/markergreen.png'
                    });
                    markers_arr.push(marker);
                    var Coords = event.latLng;
                    var x = Coords.lng();
                    var y = Coords.lat();
                    
                    map.setCenter(event.latLng);
                    
                    $("#x").attr('value', x);
                    $("#y").attr('value', y);
                    
                });
            },

            clearMarkers: function() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }

        };
        

        $('#tblPrincipal tr.lnk').click(function(event) {
            $(this).toggleClass('rowSelected');
            
            var capa = $(this).find("td").attr("id");
            var cmd = $(this).find("td").attr("cmd");
        
            window.top.location = 'm.buscomp.add.componente.php?cmd=' + cmd + '&capa=' + capa + '&x=' + $("#x").attr('value') + '&y=' + $("#y").attr('value');
        });
        
        
        //carga inicial
        M.initialize();
        
    });
    </script>

<?php
} elseif ( isset($_REQUEST['action']) && $_GET['action'] == 'new_add' ) {
?>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
    
    var initialLocation;
    var watchid = null;
    var num_llamadas = 0;
    
    var map = null;
    var markers_arr = [];
    var marker;
    
    $(document).ready(function() {
    
        $('#tblPrincipal tr.lnk').click(function(event) {
            $(this).toggleClass('rowSelected');
            
            if ( $("#x").attr('value') == '' || $("#y").attr('value') == '' ) {
                alert('No se tiene el x, y de su posicion, compruebe nuevamente.');
                $(this).removeClass('rowSelected');
                return false;
            }
            
            var capa = $(this).find("td").attr("id");
            var cmd = $(this).find("td").attr("cmd");
        
            window.top.location = 'm.buscomp.add.componente.php?cmd=' + cmd + '&capa=' + capa + '&x=' + $("#x").attr('value') + '&y=' + $("#y").attr('value');
        });
    
    
        var M = {

            getLocation: function() {
            
                //alert("Ingresando");
            
                if (typeof (google) == "undefined") {
                    alert('Verifique su conexion a maps.google.com, vuelva a recargar la pagina');
                    return false;
                }
                
                // Try W3C Geolocation (Preferred)
                if (navigator.geolocation) {

                    M.getPosicionActual();

                }else {
                    alert("Tu navegador no soporta la API de geolocalizacion");
                }

                
            },
            
            getPosicionActual: function() {
                alert("Capturando su posicion, espere un momento.");
                watchid = navigator.geolocation.watchPosition(M.obtenerPosicion, M.gestiona_errores, {
                        enableHighAccuracy     : true,
                        maximumAge            : 20000,
                        timeout                : 10000
                    }
                );
            },

            obtenerPosicion: function(position) {
                
                num_llamadas++;
                
                if ( num_llamadas >= 2 ) {
                
                    initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                
                    if ( confirm("Desea capturar el x,y de su posicion actual1? Precision: " + position.coords.accuracy + "m.") ) {
                        $('#x').attr('value', position.coords.longitude);
                        $('#y').attr('value', position.coords.latitude);
                        
                        //cancelo la busqueda de la posicion repetitiva
                        navigator.geolocation.clearWatch(watchid);
                        
                        //pinta el mapa y la posicion actual
                        M.showMapa();
                        
                    }
                }
            },
                
            gestiona_errores: function(err) {
                if (err.code == 1) {
                    alert("Se ha denegado la geolocalización.");
                }else if (err.code == 2) {
                    alert("Geolocalización no disponible.");
                }else if (err.code == 3) {
                    alert("Tiempo de espera agotado");
                }else {
                    alert("ERROR: "+ err.message);
                }
                

            },
            
            clearMarkers: function() {
                for (var n = 0, marker; marker = markers_arr[n]; n++) {
                    marker.setVisible(false);
                }
            }

            showMapa: function() {
                var latlng = new google.maps.LatLng(initialLocation.lat(), initialLocation.lng());
                var myOptions = {
                    zoom: 17,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    navigationControl: true,
                    navigationControlOptions: {
                        style: google.maps.NavigationControlStyle.ANDROID,
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    }
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: 'vistas/icons/markergreen.png'
                });
                markers_arr.push(marker);
                
                google.maps.event.addListener(map, "click", function(event) {
                
                    if (!confirm("Desea establecer este nuevo punto x,y?")) {
                        return false;
                    }
                
                    //clear markers
                    M.clearMarkers();
                    
                    marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map,
                        icon: 'vistas/icons/markergreen.png'
                    });
                    markers_arr.push(marker);
                    var Coords = event.latLng;
                    var x = Coords.lng();
                    var y = Coords.lat();
                    
                    map.setCenter(event.latLng);
                    
                    $("#x").attr('value', x);
                    $("#y").attr('value', y);
                    
                });
            
            }

            
        };
        
        //obtengo el X,Y
        M.getLocation();
    
    });

    </script>
    
<?php
}
?>

<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php include $dirModulo . 'm.header.php';?>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="titulo">Agregar Componente</td>
</tr>
</table>

<div id="map_canvas" style="height: 300px; width: 100%;"></div>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="clsSubtitle">X: <input type="text" size="15" name="x" id="x" value="<?php echo $_REQUEST['x']?>" readonly="readonly" /><td>
</tr>
<tr>
    <td class="clsSubtitle">Y: <input type="text" size="15" name="y" id="y" value="<?php echo $_REQUEST['y']?>" readonly="readonly" /><td>
</tr>
</table>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="descPrincipal">Seleccione la capa que desea agregar al punto x,y elegido</td>
</tr>
</table>

<table id="tblPrincipal" class="principal" width="100%">
<tr>
    <td class="subtitulo">Listado de capas</td>
</tr>
<?php
if ( !empty($arrayCapas) ) {
foreach ($arrayCapas as $objCapa) {
    if ( $objCapa->__get('_esFftt') == 'N' ) {
    if ( $objCapa->__get('_abvCapa') == 'edi' ) {
    ?>
        <tr class="lnk">
            <td class="list" cmd="nuevoEdificio" id="<?php echo $objCapa->__get('_abvCapa')?>"><a href="javascript:void(0);"><?php echo $objCapa->__get('_nombre')?></a></td>
        </tr>
    <?php
    }
    }
}
}
?>
</table>
<div style="height:20px;"></div>

</body>
</html>