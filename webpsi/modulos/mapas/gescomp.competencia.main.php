<?php
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function genera_color_poligono() {
	$valores_arr = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
	
	$color = '';
	for( $i=1; $i<=6; $i++ ) {
		$rand = rand(1, 16);
		$color .= $valores_arr[$rand];
	}
	
	return '#' . $color;
}

$array_zonal = $_SESSION['USUARIO_ZONAL'];


if( isset($_GET['action']) && $_GET['action'] == 'save' ) {

    $obj_terminales = new data_ffttTerminales();

	$array_edificios = $obj_terminales->get_competencia_by_zonal_and_mdf($conexion, $_REQUEST['IDzonal'], $_REQUEST['IDmdf']);
	
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    
    $edificios_selected_arr = array();
    foreach( $_POST['xy'] as $pkedi => $valedi ) {
    	if( $valedi['x'] != '' || $valedi['y'] != '' ) {
    	
    		$edificios_selected_arr[$pkedi] = array(
    			'x' => $valedi['x'],
    			'y' => $valedi['y']
    		);
    	}
    }
    
    //update solo a los que surgieron cambios en fftt_competencia
    $flag = 1;
	foreach( $array_edificios as $edificio ) {
		
		if( array_key_exists($edificio['idcompetencia'], $edificios_selected_arr) ) {
			if( $edificio['x'] != $edificios_selected_arr[$edificio['idcompetencia']]['x'] ||
				$edificio['y'] != $edificios_selected_arr[$edificio['idcompetencia']]['y'] ) {
			
				//grabando los xy en fftt_competencia
    			$result = $obj_terminales->__updateCompetenciaXY($conexion, $edificio['idcompetencia'], $edificios_selected_arr[$edificio['idcompetencia']]['x'], $edificios_selected_arr[$edificio['idcompetencia']]['y'], $idusuario);
				if( !$result ) {
					$flag = 0;
					break;
				}
			}
		}
	}
	
	$data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'saveEstado' ) {
    
    $obj_capas = new data_ffttCapas();

    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    
    //grabando estado de armarios en fftt_edificios
    $result = $obj_capas->__updateEstadoCompetencia($conexion, $_POST['IDcom'], $_POST['estado'], $idusuario);
    
    $data_result = ($result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarMdf' ) {

	$obj_terminales = new data_ffttTerminales();

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$array_mdf = $obj_terminales->get_mdf_by_zonal_and_idusuario($conexion, $_POST['IDzon'], $idusuario);
	
	$data_arr = array();
	foreach ($array_mdf as $mdf) {
    	$data_arr[] = array(
    		'IDmdf' 	=> $mdf['mdf'],
    		'Descmdf'	=> $mdf['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarCompetencia' ) {

	$obj_zonal = new data_Zonal();
	$obj_terminales = new data_ffttTerminales();
	
	//carga de datos (x,y) de Zonal
	$array_zonal = $obj_zonal->__list($conexion,'',$_POST['IDzonal']);
	$data_zonal = array(
		'x' => $array_zonal[0]->__get('x'),
		'y' => $array_zonal[0]->__get('y')
	);
	
	//carga de datos (x,y) de Mdf
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$array_mdf = $obj_terminales->get_mdf_by_codmdf($conexion, $_POST['IDmdf']);
	$data_mdf = array(
		'x' => $array_mdf[0]['x'],
		'y' => $array_mdf[0]['y']
	);
	
	
	$array_competencias = $obj_terminales->get_competencia_by_zonal_and_mdf($conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['datosXY']);
	
	$data_markers = array();
	$data_markers_con_xy = array();
	$i = 1;
	foreach ($array_competencias as $competencias) {
		if( $competencias['y'] != '' && $competencias['x'] != '' ) {
			$data_markers_con_xy[] = $competencias;
		}

		$data_markers[] = $competencias;
		$i++;
	}
	
	
	$poligonos_array = array();
	$prom_x = $prom_y = 0;
	// ========================================== DATA DE POLIGONOS ======================================= //
	if( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
		$obj_capasarea = new data_ffttCapasArea();

		$IDmdf = $_POST['IDmdf'];
		//seteamos 'ANU0-ANCON' porque falta actualizar el campo elemento = mdf de tabla fftt_capas_area
		
		//$IDmdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';
		
		//$IDmdf = 'ANU0-ANCON';
		
		$puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $IDmdf);
		$poligonos_array[] = array(
			'coords' => $puntosXY_mdf,
			'color'	 => genera_color_poligono()
		); 

		
		$count_xy = count($poligonos_array[0]['coords']);
		
		$sum_x = $sum_y = 0;
		foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
			$sum_x += $puntoXY['x'];
			$sum_y += $puntoXY['y'];
		}
		
		$prom_x = $sum_x / $count_xy;
		$prom_y = $sum_y / $count_xy;
		
	}
	// ========================================== FIN DATA DE POLIGONOS ======================================= //
	
	
	
	$estados_competencia_arr = array(
		array('id'=>'1', 'val'=>'Estado 1'),
		array('id'=>'2', 'val'=>'Estado 2'),
		array('id'=>'3', 'val'=>'Estado 3')
	);
	$estados__arr = array(
		'1' => 'Estado 1',
		'2' => 'Estado 2',
		'3' => 'Estado 3'
	);
	
	$data_result = array(
		'zonal' 			=> $data_zonal,
		'mdf'				=> $data_mdf,
		'estados'			=> $estados_competencia_arr,
		'est'				=> $estados__arr,
		'competencias'		=> $data_markers,
		'competencias_con_xy' => $data_markers_con_xy,
		'xy_poligono' 		=> $poligonos_array,
		'center_map'  		=> array(
			'x' => $prom_x,
			'y' => $prom_y
		)
	);
	
	echo json_encode($data_result);
    exit;

}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscar' ) {

	$poligonos_array = array();
	$prom_x = $prom_y = 0;
	// ========================================== DATA DE POLIGONOS ======================================= //
	if( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
		$obj_capasarea = new data_ffttCapasArea();

		$IDmdf = $_POST['IDmdf'];
		//seteamos 'ANU0-ANCON' porque falta actualizar el campo elemento = mdf de tabla fftt_capas_area
		
		//$IDmdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';
		
		//$IDmdf = 'ANU0-ANCON';
		
		$puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $IDmdf);
		$poligonos_array[] = array(
			'coords' => $puntosXY_mdf,
			'color'	 => genera_color_poligono()
		); 

		
		$count_xy = count($poligonos_array[0]['coords']);
		
		$sum_x = $sum_y = 0;
		foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
			$sum_x += $puntoXY['x'];
			$sum_y += $puntoXY['y'];
		}
		
		$prom_x = $sum_x / $count_xy;
		$prom_y = $sum_y / $count_xy;
		
		$data_result = array(
			'xy_poligono' => $poligonos_array,
			'center_map'  => array(
					'x' => $prom_x,
					'y' => $prom_y
			)
		);
	
		echo json_encode($data_result);
		exit;
		
	}
	// ========================================== FIN DATA DE POLIGONOS ======================================= //
	

}

else {
//include 'principal/gescomp.php';    

?>


<!-- template por default -->
<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_competencia.ifr.php" width="100%" height="480"></iframe>

<?php
}
?>
