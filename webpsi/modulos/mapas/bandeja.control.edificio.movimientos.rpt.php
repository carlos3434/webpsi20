<?php

/**
 * @package     /modulos/maps
 * @name        bandeja.control.edificio.movimientos.rpt.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2012/05/04
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function limpiaCadena($value)
{
    $noPermitidos = array("'",'\\','<','>',"\"",";","\n","\r"); 
    $nuevaCadena = str_replace($noPermitidos, "", $value);
    
    return $nuevaCadena;
}

function cleanStr($str)
{
    $search    = array("\t", "\n", "\r\n");
    $replace    = array("", "", "");
    
    return str_replace($search, $replace, trim($str));
}

//obtengo las regiones a partir de las zonales que tiene asignado el usuario
function listarRegionesPorZonalUsuario()
{
    global $conexion;

    $objRegionZonal    = new Data_RegionZonalEdi();

    $arrZonal = array();
    if (!empty($_SESSION['USUARIO_ZONAL'])) {
        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
            $arrZonal[$objZonal->__get('_abvZonal')] =
            $objZonal->__get('_abvZonal');
        }
    }

    //obtengo las regiones a partir de las zonales que tiene asignado el usuario
    $arrObjRegiones = array();
    if (!empty($arrZonal)) {
        $arrObjRegiones = $objRegionZonal->listar(
            $conexion, array(), $arrZonal
        );
    }

    $arrRegion = array();
    if (!empty($arrObjRegiones)) {
        foreach ( $arrObjRegiones as $objRegion ) {
            $arrRegion[$objRegion->__get('_idRegion')] = array(
                    'idregion'  => $objRegion->__get('_idRegion'),
                    'nomregion' => $objRegion->__get('_nomRegion'),
                    'detregion' => $objRegion->__get('_detRegion')
            );
        }
    }

    return $arrRegion;
}

//archivo que se genera
$file = "/tmp/rpt_edificios_mov.xls";

$fileName = $file;

$file = fopen($fileName, "w+");

$titulos = "ITEM\tCODIGO PROYECTO WEB\tFOTO 1\tFOTO 2\tFOTO 3\tFOTO 4";
$titulos .= "\tX\tY\tFECHA REGISTRO PROYECTO\tFECHA TERMINO CONSTRUCCION";
$titulos .= "\tQUINCENA\tFUENTE DE IDENTIFICACION\tSEGMENTO";
$titulos .= "\tTIPO PROYECTO\tREGION\tZONAL\tURA";
$titulos .= "\tARMARIO\tFACILIDADES POSTE";
$titulos .= "\tSECTOR\tMZA. TDP\tTIPO VIA\tDIRECCION OBRA\tNUMERO";
$titulos .= "\tMZA\tLOTE\tTIPO CCHH\tCCHH";
$titulos .= "\tDEPARTAMENTO\tPROVINCIA\tDISTRITO";
$titulos .= "\tLOCALIDAD\tNSE";
$titulos .= "\tNOMBRE CONSTRUCTORA\tRUC CONSTRUCTORA";
$titulos .= "\tNOMBRE PROYECTO\tTIPO PROYECTO";
$titulos .= "\tPERSONA CONTACTO\tPAGINA WEB\tEMAIL";
$titulos .= "\tNRO. BLOCKS\tNRO. PISOS 1\tNRO. DPTOS 1";
$titulos .= "\tTIPO INFRAESTRUCTURA\tZONA COMPETENCIA\tGESTION COMPETENCIA";
$titulos .= "\tCOBERTURA MOVIL 2G\tCOBERTURA MOVIL 3G";
$titulos .= "\tVIVEN\tNRO. DPTOS. HABITADOS\tAVANCE";
$titulos .= "\tDUCTERIA INTERNA FECHA\tMONTANTE FECHA\tPUNTO ENERGIA FECHA";
$titulos .= "\tVALIDADO X CALL\tFECHA TRATAMCALL\tMEGAPROYECTOS";
$titulos .= "\tFECHA SEGUIMIENTO\tRESPONSABLES CAMPO";
$titulos .= "\tOBSERVACION\tGESTION DE OBRAS\tUSUARIO INSERT\tFECHA INSERT";
$titulos .= "\tUSUARIO UPDATE\tFECHA UPDATE\tINGRESO";
$titulos .= "\tSTATUS\tFECHA CABLEADO";

fwrite($file, $titulos . "\r\n");



$objUbigeo     = new Data_Ubigeo;
$objEdificio   = new Data_FfttEdificio();
$objTerminales = new Data_FfttTerminales();

$idusuario = $_SESSION['USUARIO']->__get('_idUsuario');

$arrObjUbigeo = $objUbigeo->listar($conexion);

//creo este array con todos los ubigeos
$arrUbigeo = array();
if ( !empty($arrObjUbigeo) ) {
    foreach ( $arrObjUbigeo as $objectUbigeo ) {
        $arrUbigeo[$objectUbigeo->__get('_codUbigeo')] = $objectUbigeo->__get('_nombre'); 
    }
}


$arrFiltro = array();

$gestionObra = ( isset($_REQUEST['gestionObra']) ) ? $_REQUEST['gestionObra'] : '';
$estado = ( isset($_REQUEST['estado']) ) ? $_REQUEST['estado'] : '';
$empresa = ( isset($_REQUEST['empresa']) ) ? $_REQUEST['empresa'] : '';
$region = ( isset($_REQUEST['region']) ) ? $_REQUEST['region'] : '';
$zonal = ( isset($_REQUEST['zonal']) ) ? $_REQUEST['zonal'] : '';
$mdf = ( isset($_REQUEST['mdf']) ) ? $_REQUEST['mdf'] : '';
$departamento = ( isset($_REQUEST['departamento']) ) ? $_REQUEST['departamento'] : '';
$provincia = ( isset($_REQUEST['provincia']) ) ? $_REQUEST['provincia'] : '';
$distrito = ( isset($_REQUEST['distrito']) ) ? $_REQUEST['distrito'] : '';

$georeferencia = ( isset($_REQUEST['georeferencia']) ) ? $_REQUEST['georeferencia'] : '';
$fotos = ( isset($_REQUEST['fotos']) ) ? $_REQUEST['fotos'] : '';

$campoFiltro = ( isset($_REQUEST['campo_filtro']) ) ? $_REQUEST['campo_filtro'] : '';
$campoValor = ( isset($_REQUEST['campo_valor']) ) ? $_REQUEST['campo_valor'] : '';


if ( $gestionObra != '' ) {
    $arrFiltro['gestionObra'] = $gestionObra;
}

if ( $estado != '' ) {
    $arrFiltro['estado'] = $estado;
}

if ( $empresa != '' ) {
    $arrFiltro['empresa'] = $empresa;
} else {
    
    //listado de empresas que tiene asignado
    $arrDataEmpresa = array();
    if (!empty($_SESSION['USUARIO_EMPRESA_DETALLE'])) {
        foreach (
            $_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa
        ) {
            $arrDataEmpresa[] = $objEmpresa->__get('_idEmpresa');
        }
    }
    $arrFiltro['empresa'] = $arrDataEmpresa;
}

if ( $region != '' ) {
    $arrFiltro['region'] = array($region);
} else {

    //obtengo las regiones a partir de las zonales que tiene asignado
    //el usuario
    $arrRegion = listarRegionesPorZonalUsuario();

    $arrDataRegion = array();
    if (!empty($arrRegion)) {
        foreach ($arrRegion as $region) {
            $arrDataRegion[] = $region['idregion'];
        }
    }
    $arrFiltro['region'] = $arrDataRegion;
}

if ( $zonal != '' ) {
    $arrFiltro['zonal'] = array($zonal);
} else {
    $arrData = array();
    if (!empty($_SESSION['USUARIO_ZONAL'])) {
        foreach ($_SESSION['USUARIO_ZONAL'] as $objZonal) {
            $arrData[] = $objZonal->__get('_abvZonal');
        }
    }
    $arrFiltro['zonal'] = $arrData;
}

if ( $mdf != '' ) {
    $arrFiltro['mdf'] = array($mdf);
} else {
    /*
    $arrMdf = array();
    if ( $zonal == '' ) {
        if (!empty($_SESSION['USUARIO_MDF'])) {
            foreach ($_SESSION['USUARIO_MDF'] as $abvMdf) {
                $arrMdf[]['mdf'] = $abvMdf;
            }
        }

    } else {
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $zonal, $idusuario
        );
    }

    $arrData = array();
    if (!empty($arrMdf)) {
        foreach ($arrMdf as $abvMdf) {
            $arrData[] = $abvMdf['mdf'];
        }
    }
    $arrFiltro['mdf'] = $arrData;
    */
}

if ( $campoFiltro != '' ) {
    $arrFiltro['campo_filtro'] = $campoFiltro;
}
if ( $campoValor != '' ) {
    $arrFiltro['campo_valor'] = $campoValor;
}
//Ubigeo
if ( $departamento != '' ) {
    $arrFiltro['departamento'] = $departamento;
}
if ( $provincia != '' ) {
    $arrFiltro['provincia'] = $provincia;
}
if ( $distrito != '' ) {
    $arrFiltro['distrito'] = $distrito;
}
//Georeferencia y fotos
if ( $georeferencia != '' ) {
    $arrFiltro['georeferencia'] = $georeferencia;
}
if ( $fotos != '' ) {
    $arrFiltro['fotos'] = $fotos;
}



$arrObjEdificios = $objEdificio->rptEdificiosMovimiento($conexion, $arrFiltro);

$i = 0;
$item = '';
$ultimoItem = '0000';
$checkItem = 0;
foreach ( $arrObjEdificios as $objEdificio ) {
    
    $item = $objEdificio->__get('_item');
    
    $strDepartamento = substr($objEdificio->__get('_distrito'), 0, 2) . '0000';
    $strProvincia    = substr($objEdificio->__get('_distrito'), 0, 4) . '00';
    $strDistrito     = substr($objEdificio->__get('_distrito'), 0, 6);

    $departamento = ( isset($arrUbigeo[$strDepartamento]) ) ? $arrUbigeo[$strDepartamento] : '';
    $provincia    = ( isset($arrUbigeo[$strProvincia]) ) ? $arrUbigeo[$strProvincia] : '';
    $distrito     = ( isset($arrUbigeo[$strDistrito]) ) ? $arrUbigeo[$strDistrito] : '';
    
    
    $quincena = '';
    if ($objEdificio->__get('_fechaTerminoConstruccion') != '0000-00-00' && 
        $objEdificio->__get('_fechaTerminoConstruccion') != '') {
        $diaQuincena = date('d', strtotime($objEdificio->__get('_fechaTerminoConstruccion')));
        $mesQuincena = date('m', strtotime($objEdificio->__get('_fechaTerminoConstruccion')));
        $anoQuincena = date('y', strtotime($objEdificio->__get('_fechaTerminoConstruccion')));
        $dia = '1Q';
        if ( (int)$diaQuincena > 15 ) {
            $dia = '2Q';
        }
        $quincena = $dia . '-' . $mesQuincena . '-' . $anoQuincena;
    }
    
    
    $ingreso = ''; 
    if ( $objEdificio->__get('_ingreso') == '1' ) {
        $ingreso = 'WEB';
    } elseif ( $objEdificio->__get('_ingreso') == '2' ) {
        $ingreso = 'MOVIL';
    }    
    
    $viven = '';
    if ( $objEdificio->__get('_viven') == '1' ) {
        $viven = 'SI';
    } elseif ( $objEdificio->__get('_viven') == '0' ) {
        $viven = 'NO';
    }
    
    $validacionCall = '';
    if ( $objEdificio->__get('_validacionCall') == '1' ) {
        $validacionCall = 'SI';
    } elseif ( $objEdificio->__get('_validacionCall') == '0' ) {
        $validacionCall = 'NO';
    }
    
    $megaproyecto = '';
    if ( $objEdificio->__get('_megaproyecto') == '1' ) {
        $megaproyecto = 'SI';
    } elseif ( $objEdificio->__get('_megaproyecto') == '0' ) {
        $megaproyecto = 'NO';
    }
    
    //validando Fechas
    if ($objEdificio->__get('_fechaRegistroProyecto') == '0000-00-00') {
        $objEdificio->__set('_fechaRegistroProyecto', '');
    }
    if ($objEdificio->__get('_fechaTerminoConstruccion') == '0000-00-00') {
        $objEdificio->__set('_fechaTerminoConstruccion', '');
    }
    if ($objEdificio->__get('_fechaTratamientoCall') == '0000-00-00') {
        $objEdificio->__set('_fechaTratamientoCall', '');
    }
    if ($objEdificio->__get('_fechaSeguimiento') == '0000-00-00') {
        $objEdificio->__set('_fechaSeguimiento', '');
    }
    if ($objEdificio->__get('_fechaCableado') == '0000-00-00') {
        $objEdificio->__set('_fechaCableado', '');
    }
    if ($objEdificio->__get('_ducteriaInternaFecha') == '0000-00-00') {
        $objEdificio->__set('_ducteriaInternaFecha', '');
    }
    if ($objEdificio->__get('_montanteFecha') == '0000-00-00') {
        $objEdificio->__set('_montanteFecha', '');
    }
    if ($objEdificio->__get('_puntoEnergiaFecha') == '0000-00-00') {
        $objEdificio->__set('_puntoEnergiaFecha', '');
    }


    //chequeando que es la cebecera de edificio
    $checkItem = 0;
    
    //registrando el edificio cabecera
    if ( $objEdificio->__get('_item') != $ultimoItem ) {
    
        $filaData = cleanStr(limpiaCadena($objEdificio->__get('_item'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_codigoProyectoWeb'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_foto1'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_foto2'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_foto3'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_foto4'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_x'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_y'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaRegistroProyecto'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaTerminoConstruccion'))) . "\t";
        $filaData .= $quincena . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desFuenteIdentificacion'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desSegmento'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_tipoProyecto'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nomRegion'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_zonal'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_ura'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_armario'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desFacilidadPoste'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_sector'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_mzaTdp'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_tipoVia'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_direccionObra'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_numero'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_mza'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_lote'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_tipoCchh'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_cchh'))) . "\t";

        $filaData .= $departamento . "\t";
        $filaData .= $provincia . "\t";
        $filaData .= $distrito . "\t";
        
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desLocalidad'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desNivelSocioeconomico'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nombreConstructora'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_rucConstructora'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nombreProyecto'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_tipoProyecto'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_personaContacto'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_paginaWeb'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_email'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroBlocks'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroPisos1'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroDptos1'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desTipoInfraestructura'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desZonaCompetencia'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desGestionCompetencia'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desCoberturaMovil2G'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desCoberturaMovil3G'))) . "\t";
        $filaData .= $viven . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroDptosHabitados'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_avance'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_ducteriaInternaFecha'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_montanteFecha'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_puntoEnergiaFecha'))) . "\t";
        $filaData .= $validacionCall . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaTratamientoCall'))) . "\t";
        $filaData .= $megaproyecto . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaSeguimiento'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desResponsableCampo'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_observacion'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desGestionObra'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_loginInsert'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaInsert'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_loginUpdate'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaUpdate'))) . "\t";
        $filaData .= $ingreso . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_desEstado'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaCableado'))) . "\t";
        
        fwrite($file, $filaData . "\r\n");
        
        //checkeando que es la cabecera de edificio
        $checkItem = 1;
    }
        
    
    if ( !$checkItem ) {
    
        //registro los movimientos
    
        $actualizacion = '';
        if ( $objEdificio->__get('_actualizacionMov') == '1' ) {
            $actualizacion = 'WEB';
        } elseif ( $objEdificio->__get('_actualizacionMov') == '2' ) {
            $actualizacion = 'MOVIL';
        }
        
        if ($objEdificio->__get('_fechaTerminoConstruccionMov') == '0000-00-00') {
            $objEdificio->__set('_fechaTerminoConstruccionMov', '');
        }
        if ($objEdificio->__get('_fechaTratamientoCallMov') == '0000-00-00') {
            $objEdificio->__set('_fechaTratamientoCallMov', '');
        }
        if ($objEdificio->__get('_fechaSeguimientoMov') == '0000-00-00') {
            $objEdificio->__set('_fechaSeguimientoMov', '');
        }
        
        $filaData = cleanStr(limpiaCadena($objEdificio->__get('_item'))) . "\t";
        $filaData .= "\t"; //_codigoProyectoWeb
        $filaData .= "\t"; //_foto1
        $filaData .= "\t"; //_foto2
        $filaData .= "\t"; //_foto3
        $filaData .= "\t"; //_foto4
        $filaData .= "\t"; //x
        $filaData .= "\t"; //y
        $filaData .= "\t"; //_fechaRegistroProyecto
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaTerminoConstruccionMov'))) . "\t";
        $filaData .= "\t"; //quincena
        $filaData .= "\t"; //_desFuenteIdentificacion
        $filaData .= "\t"; //_desSegmento
        $filaData .= "\t"; //_tipoProyecto
        $filaData .= "\t"; //_nomRegion
        $filaData .= "\t"; //_zonal
        $filaData .= "\t"; //_ura
        $filaData .= "\t"; //_armario
        $filaData .= "\t"; //_desFacilidadPoste
        $filaData .= "\t"; //_sector
        $filaData .= "\t"; //_mzaTdp
        $filaData .= "\t"; //_tipoVia
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_direccionObraMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_numeroMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_mzaMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_loteMov'))) . "\t";
        $filaData .= "\t"; //_tipoCchh
        $filaData .= "\t"; //_cchh
        
        $filaData .= "\t"; //_departamento
        $filaData .= "\t"; //_provincia
        $filaData .= "\t"; //_distrito
        
        $filaData .= "\t"; //_desLocalidad
        $filaData .= "\t"; //_desNivelSocioeconomico
        $filaData .= "\t"; //_nombreConstructora
        $filaData .= "\t"; //_rucConstructora
        $filaData .= "\t"; //_nombreProyecto
        $filaData .= "\t"; //cleanStr(limpiaCadena($objEdificio->__get('_tipoProyecto'))) . 
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_personaContactoMov'))) . "\t";
        $filaData .= "\t"; //_paginaWeb
        $filaData .= "\t"; //_email
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroBlocksMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroPisosMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_nroDptosMov'))) . "\t";
        $filaData .= "\t"; //_desTipoInfraestructura
        $filaData .= "\t"; //_desZonaCompetencia
        $filaData .= "\t"; //_desGestionCompetencia
        $filaData .= "\t"; //_desCoberturaMovil2G
        $filaData .= "\t"; //_desCoberturaMovil3G
        $filaData .= "\t"; //_viven
        $filaData .= "\t"; //_nroDptosHabitados
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_avanceMov'))) . "\t";
        $filaData .= "\t"; //_ducteriaInternaFecha
        $filaData .= "\t"; //_montanteFecha
        $filaData .= "\t"; //_puntoEnergiaFecha
        $filaData .= "\t"; //_validacionCall
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaTratamientoCallMov'))) . "\t";
        $filaData .= "\t"; //_megaproyecto
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaSeguimientoMov'))) . "\t";
        $filaData .= "\t"; //_desResponsableCampo
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_observacionMov'))) . "\t";
        $filaData .= "\t"; //_desGestionObra
        $filaData .= "\t"; //_userInsert
        $filaData .= "\t"; //_fechaInsert
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_userInsertMov'))) . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_fechaInsertMov'))) . "\t";
        $filaData .= $actualizacion . "\t";
        $filaData .= cleanStr(limpiaCadena($objEdificio->__get('_estadoMov'))) . "\t";
        $filaData .= "\t"; //_fechaCableado
    
        //FIN registro los movimientos
        
        fwrite($file, $filaData . "\r\n");
        
    }

    
    //seteando el ultimo item
    $ultimoItem = $objEdificio->__get('_item');

    $i++;    
}

fclose($file);


$fileName = realpath($fileName);
$fileExtension = strtolower(substr(strrchr($fileName, "."), 1));

switch ($fileExtension) {
    case "pdf": $ctype="application/pdf"; 
        break;
    case "exe": $ctype="application/octet-stream"; 
        break;
    case "zip": $ctype="application/zip"; 
        break;
    case "doc": $ctype="application/msword"; 
        break;
    case "xls": $ctype="application/vnd.ms-excel";
        break;
    case "ppt": $ctype="application/vnd.ms-powerpoint";
        break;
    case "gif": $ctype="image/gif";
        break;
    case "png": $ctype="image/png";
        break;
    default: $ctype="application/force-download";
}

if (!file_exists($fileName)) {
    die("NO FILE HERE");
}

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false);
header("Content-Type: $ctype");
header("Content-Disposition: attachment; filename=\"".basename($fileName)."\";");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".@filesize($fileName));
set_time_limit(0);
@readfile("$fileName") or die("File not found.");