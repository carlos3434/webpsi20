<?php

/**
 * [Web Unificada] :: Controlador de la Administracion de Responsable de Campo.
 * @package    /modulos/maps/
 * @name       administracion.responsable.campo.php
 * @category   Controller
 * @author     Fernando Esteban Valerio <festeban@gmd.com.pe>
 * @copyright  GMD S.A.
 * @version    1.0
 */

class AdministracionResponsableCampo extends Data_ResponsableCampoEdi
{
    /**
     * Directorio
     * @access protected
     * @var string
     */
    protected $_ruta;

    /**
     * Numero de pagina
     * @access protected
     * @var int
     */
    protected $_pagina;
    
    /**
     * @method  __construct
     * Metodo constructor de la clase
     */
    public function __construct()
    {
        $this->_pagina = 1;
        $this->_ruta = 'vistas/administracion/responsable/';
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method getRuta
     * Obtener el valor del atributo "ruta".
     * @return string
     */
    public function getRuta()
    {
        return $this->_ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method setRuta
     * Asignar un valor al atributo "ruta".
     * @param string $ruta Valor a asignar.
     */
    public function setRuta($ruta)
    {
        $this->_ruta = $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method getPagina
     * Obtener el valor del atributo "pagina".
     * @return string
     */
    public function getPagina()
    {
        return $this->_pagina;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @method setRuta
     * Asignar un valor al atributo "pagina".
     * @param string $pagina Valor a asignar.
     */
    public function setPagina($pagina)
    {
        $this->_pagina = $pagina;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaAgregar
     * Mostrar el formulario de registro de un responsable de campo.
     * @return void
     */
    public function vistaAgregar()
    {
        $ruta = $this->getRuta() . 'v_responsable_add.phtml';
        include $ruta;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaEditar
     * Mostrar el formulario de edicion de un responsable de campo.
     * @param int $id Id del responsable de campo.
     * @return void
     */
    public function vistaEditar($id)
    {
        $vista = '';
        if (is_numeric($id)) {
            $objeto = $this->obtener($id);
            if ($objeto->getIdResponsableCampo()) {
                $vista = $this->getRuta() . 'v_responsable_edit.phtml';
            } else {
                $vista = 'vistas/administracion/v_error.phtml';
            }
        } else {
            $vista = 'vistas/administracion/v_error.phtml';
        }
        include $vista;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name guardar
     * Registrar la informacion del responsable de campo.
     * @param string $nombre Nombre del responsable de campo.
     * @return string $mensaje Mensaje de respuesta.
     */
    public function guardar($nombre)
    {
        $arrParametros = array();
        $arrParametros['id'] = NULL;
        $arrParametros['desc'] = $nombre;
        $arrParametros['flag'] = 1;
    
        $id = 0;
        $mensaje = '';
        $objeto = $this->obtener($id, $nombre);
        if ($objeto->getIdResponsableCampo()) {
            $mensaje = 'existe';
        } else {
            $id = $this->registrar($arrParametros);
            if ($id) {
                $mensaje = 'ok';
            } else {
                $mensaje = 'error';
            }
        }
        echo $mensaje;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name actualizar
     * Actualizar la informacion del responsable de campo.
     * @param int $id Id del responsable de campo.
     * @param string $nombre Nombre del responsable de campo.
     * @param int $flag Estado del responsable de campo.
     * @return string $mensaje Mensaje de respuesta.
     */
    public function actualizar($id, $nombre, $flag=1)
    {
        $mensaje = '';
        if (is_numeric($id) && is_numeric($flag)) {
            $arrParametros = array();
            $arrParametros['id'] = $id;
            $arrParametros['desc'] = $nombre;
            $arrParametros['flag'] = $flag;

            $numElementos = $this->buscar($id, $nombre);
            if ($numElementos) {
                $mensaje = 'existe';
            } else {
                $resultado = $this->registrar($arrParametros);
                if ($resultado) {
                    $mensaje = 'ok';
                } else {
                    $mensaje = 'error';
                }
            }
        } else {
            $mensaje = 'error';
        }
        echo $mensaje;
    }

    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name cambiarPagina
     * Mostrar los responsables de campo ubicados en la pagina seleccionada.
     * @param int $pagina Numero de pagina.
     * @param int $total Total de registros.
     * @param string $nombre Nombre del responsable de campo.
     * @return void
     */
    public function cambiarPagina($pagina, $total, $nombre)
    {
        $arrResponsable = $this->filtrar($pagina, $nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        $vista = $this->getRuta() . 'v_responsable_paginado.phtml';
        include $vista;
        $cadena = '<script type="text/javascript">'
                . '$(document).ready(function(){'
                . '$("#cont_num_resultados_head").empty();'
                . '$("#cont_num_resultados_head").html(' . $total . ');'
                . '$("#tl_registros").val("' . $total . '");'
                . '$("#pag_actual").val("' . $pagina . '");'
                . '});'
                . '</script>';
        echo $cadena;
    }
    
    /**
     * @author Fernando Esteban Valerio <festeban@gmd.com.pe>
     * @name vistaMantenimiento
     * Mostrar el modulo de administracion de responsable de campo.
     * @param int $opcion Opcion.
     * @param string $nombre Nombre del responsable de campo.
     * @return void
     */
    public function vistaMantenimiento($opcion=0, $nombre='')
    {
        $cadena = '';
        $vista = $this->getRuta() . 'v_responsable_lista.phtml';
        $pagina = $this->getPagina();
        $total = $this->contar($nombre);
        $arrResponsable = $this->filtrar($pagina, $nombre);
        $numPaginas = ceil($total/TAM_PAG_LISTADO_MAP);
        if ($numPaginas > 1) {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){'
                    . '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#tl_registros").val("' . $total . '");'
                    . '$("#paginacion").show();'
                    . '});'
                    . '</script>'
                    . '<script type="text/javascript">'
                    . '$(function() {'
                    . '$("#paginacion").paginate({'
                    . 'count : ' . $numPaginas . ','
                    . 'start : 1,'
                    . 'display : 5,'
                    . 'border : true,'
                    . 'border_color : "#FFFFFF",'
                    . 'text_color : "#FFFFFF",'
                    . 'background_color : "#0080AF",'
                    . 'border_hover_color : "#CCCCCC",'
                    . 'text_hover_color : "#000000",'
                    . 'background_hover_color : "#FFFFFF",'
                    . 'images : false,'
                    . 'mouse : "press",'
                    . 'onChange : function(page){'
                    . 'loader("start");'
                    . '$("#pag_actual").attr("value", page);'
                    . 'var tl = $("#tl_registros").attr("value");'
                    . 'var filtro = $("#desc").attr("value");'
                    . '$.post("modulos/maps/administracion.maps.php?'
                    . 'cmd=cambiarPaginaResponsable", { pagina: page, '
                    . 'total: tl, filtro: filtro }, function(data){'
                    . '$("#tb_resultado").empty();'
                    . '$("#tb_resultado").append(data);'
                    . 'loader("end");'
                    . '});'
                    . '}'
                    . '});'
                    . '});'
                    . '</script>';
        } else {
            $cadena = '<script type="text/javascript">'
                    . '$(document).ready(function(){';
            if ($total > 0) {
                $cadena .= '$("#tb_resultado").show();';
            } else {
                $cadena .= '$("#tb_resultado").hide();'; 
            }
            $cadena .= '$("#cont_num_resultados_head").empty();'
                    . '$("#cont_num_resultados_head").html(' . $total . ');'
                    . '$("#tl_registros").val(' . $total . ');'
                    . '$("#pag_actual").val("' . $pagina . '");'
                    . '$("#paginacion").hide();'
                    . '});'
                    . '</script>';
        }
        if ($total > 0) {
            if (!$opcion) {
                $vista = $this->getRuta() . 'v_responsable_lista.phtml';
            } else {
                $vista = $this->getRuta() . 'v_responsable_paginado.phtml';
            }
        }
        include $vista;
        echo $cadena;
    }
}