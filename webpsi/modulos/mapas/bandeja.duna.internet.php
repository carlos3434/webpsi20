<?php

ini_set('error_reporting', E_ALL);
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

/**
 *
 * [Web Unificada] :: Bandeja Duna Internet
 *
 * @package     /modulos/maps
 * @name        bandeja.duna.internet.php
 * @category	Controller
 * @author	    Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright	2011 Telefonica del Peru
 * @version     1.0 - 2011/10/14
 */

$etiquetaTitulo = ETIQUETA_TITULO . ' | Bandeja Duna Internet';
$nombreModulo = 'modulos/maps/';

function listDunaInternet()
{
    global $conexion;

    $objDuna = new Data_DunaAdsl();
    $objDunaEstado = new Data_DunaEstado();
    $objUbigeo = new Data_Ubigeo();
    
    $arrObjDunaEstado = $objDunaEstado->listarDunaEstado($conexion);
    $arrObjDepartamento = $objUbigeo->departamentosUbigeo($conexion);

    $pagina = 1;
    $arrObjDuna = $objDuna->listarDunaAdsl($conexion, $pagina);

    $totalRegistros = $objDuna->totalDunaAdsl($conexion);

    $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);

    $filtroArray = array();
    
    include 'vistas/v_bandeja.duna.internet.php';

    $cadena = '<script type="text/javascript">
                    $(function() {
                        $("#paginacion").paginate({
                            count                   : '.$numPaginas.',
                            start                   : 1,
                            display                 : 10,
                            border                  : true,
                            border_color            : "#FFFFFF",
                            text_color              : "#FFFFFF",
                            background_color        : "#0080AF",
                            border_hover_color      : "#CCCCCC",
                            text_hover_color        : "#000000",
                            background_hover_color  : "#FFFFFF",
                            images                  : false,
                            mouse                   : "press",
                            onChange : function(page){
                                loader("start");
                                $("#pag_actual").attr("value", page);
                                var tl=$("#tl_registros").attr("value");
                                $.post("modulos/maps/bandeja.duna.internet.php?cmd=cambiarPaginaDunaInternet", { 
                                    pagina: page, total: tl, tipo: "", 
                                    estado: "", foto: "", campo_filtro: "", 
                                    campo_valor: "", departamento: "", 
                                    provincia: "", distrito: ""  
                                    }, function(data){
                                        $("#tb_resultado").empty();
                                        $("#tb_resultado").append(data);
                                        loader("end");
                                    }
                                );
                            }
                        });
                    });
                </script>';
    echo $cadena;
    
}

function cambiarPaginaDunaInternet($pagina='', $total='', $tipo='', $estado='', 
    $foto='', $campoFiltro='', $campoValor='', $departamento='', $provincia='', 
    $distrito='')
{
    global $conexion, $nombreModulo;

    if($pagina == '') $pagina = 1;

    $objDuna = new Data_DunaAdsl();
    
    $filtroArray = array();
    if ( $tipo != '' ) {
        $filtroArray['tipo'] = $tipo;
    }
    
    if ( $estado != '' ) {
        $filtroArray['estado'] = $estado;
    }
    
    if ( $foto != '' ) {
        $filtroArray['foto'] = $foto;
    }
    
    if ( $campoFiltro != '' ) {
        $filtroArray['campo_filtro'] = $campoFiltro;
    }
    
    if ( $campoValor != '' ) {
        $filtroArray['campo_valor'] = $campoValor;
    }
    
    if ( $departamento != '' ) {
        $filtroArray['departamento'] = $departamento;
    }
    
    if ( $provincia != '' ) {
        $filtroArray['provincia'] = $provincia;
    }
    
    if ( $distrito != '' ) {
        $filtroArray['distrito'] = $distrito;
    }

    $arrObjDuna = $objDuna->listarDunaAdsl($conexion, $pagina, '', $filtroArray);

    $totalRegistros = $objDuna->totalDunaAdsl($conexion, $filtroArray);
    
    $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);

    include 'vistas/v_bandeja.duna.internet.paginado.php';
}

function filtroBusqueda($pagina='',$total='', $tipo='', $estado='', $foto='', 
    $campoFiltro='', $campoValor='', $departamento='', $provincia='', 
    $distrito='')
{
    global $conexion, $nombreModulo;

    if($pagina == '') $pagina = 1;

    $objDuna = new Data_DunaAdsl();

    $filtroArray = array();
    
    if ( $tipo != '' ) {
        $filtroArray['tipo'] = $tipo;
    }
    
    if ( $estado != '' ) {
        $filtroArray['estado'] = $estado;
    }
    
    if ( $foto != '' ) {
        $filtroArray['foto'] = $foto;
    }
    
    if ( $campoFiltro != '' ) {
        $filtroArray['campo_filtro'] = $campoFiltro;
    }
    
    if ( $campoValor != '' ) {
        $filtroArray['campo_valor'] = $campoValor;
    }
    
    if ( $departamento != '' ) {
        $filtroArray['departamento'] = $departamento;
    }
    
    if ( $provincia != '' ) {
        $filtroArray['provincia'] = $provincia;
    }
    
    if ( $distrito != '' ) {
        $filtroArray['distrito'] = $distrito;
    }

    $pagina = 1;
    $arrObjDuna = $objDuna->listarDunaAdsl($conexion, $pagina, '', $filtroArray);

    $totalRegistros = $objDuna->totalDunaAdsl($conexion, $filtroArray);

    $numPaginas = ceil($totalRegistros/TAM_PAG_LISTADO);

    include 'vistas/v_bandeja.duna.internet.paginado.php';

    $cadena = '<script type="text/javascript">
                $("#cont_num_resultados_head").html(' . $totalRegistros . ');
                $("#cont_num_resultados_foot").html(' . $totalRegistros . ');
                $("#tl_registros").attr("value", "' . $totalRegistros . '");
                $("#paginacion").empty();
                ';
    if ($totalRegistros > 0) {
        $cadena .= '
        $(function() {
            $("#paginacion").paginate({
                count                   : '.$numPaginas.',
                start                   : 1,
                display                 : 10,
                border                  : true,
                border_color            : "#FFFFFF",
                text_color              : "#FFFFFF",
                background_color        : "#0080AF",
                border_hover_color      : "#CCCCCC",
                text_hover_color        : "#000000",
                background_hover_color  : "#FFFFFF",
                images                  : false,
                mouse                   : "press",
                onChange : function(page){
                    loader("start");
                    $("#pag_actual").attr("value", page);
                    var tl=$("#tl_registros").attr("value");
                    $.post("modulos/maps/bandeja.duna.internet.php?cmd=cambiarPaginaDunaInternet", { 
                    pagina: page, total: tl, tipo: "' . $tipo . '", 
                    estado: "' . $estado . '", foto: "' . $foto . '", 
                    campo_filtro: "' . $campoFiltro . '", 
                    campo_valor: "' . $campoValor . '", 
                    departamento: "' . $departamento . '", 
                    provincia: "' . $provincia . '", 
                    distrito: "' . $distrito . '"
                    }, function(data){
                            $("#tb_resultado").empty();
                            $("#tb_resultado").append(data);
                            loader("end");
                    }
                    );
                }
            });
        });';
    }
    $cadena .= '</script>';
    echo $cadena;
}

function listadoClientesCercanos($tipoListado, $idComponente, 
    $numTerminales=20, $orden='', $campo='')
{
    global $conexion;

    $objDunaAsociado = new Data_DunaAsociado();
    
    //listado de clientes asociados
    $arrClientesAsociados = array();
    $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados($conexion, $idComponente);

    if ( !empty($arrObjClientesAsociados) ) {
        foreach ( $arrObjClientesAsociados as $objDunaAsociado ) {
            $arrClientesAsociados[$objDunaAsociado->__get('_telefono')] = $objDunaAsociado;
        }
    }
    
    //listado de clientes speedy cercanos
    //Vista Armarios Cercanos = arm
    $arrClientesSpeedy = listarClientesSpeedy(
        $idComponente, 
        $tipoListado, 
        $numTerminales, 
        $orden, 
        $campo
    );
    
    if ( $orden == '' ) {
        $orden = 'asc';
        $ordenIco = '';
    } elseif ( $orden == 'asc' ) {
        $orden = 'desc';
        $ordenIco = 'desc';
    } elseif ( $orden == 'desc' ) {
        $orden = '';
        $ordenIco = 'asc';
    }
    
    include 'vistas/v_bandeja.duna.internet.cercanos.php';
}

function listadoClientesAsociados($idComponente)
{
    global $conexion;

    $objDunaAsociado = new Data_DunaAsociado();
    
    //listado de clientes asociados
    $arrClientesAsociados = array();
    $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados($conexion, $idComponente);

    if ( !empty($arrObjClientesAsociados) ) {
        foreach ( $arrObjClientesAsociados as $objDunaAsociado ) {
            $arrClientesAsociados[$objDunaAsociado->__get('_telefono')] = $objDunaAsociado;
        }
    }

    include 'vistas/v_bandeja.duna.internet.asociados.php';
}

function eliminarClienteAsociado($idDunaAsociado, $idComponente)
{
    try {
        global $conexion;
        
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();
   
        $objDuna = new Data_DunaAdsl();
        $objDunaAsociado = new Data_DunaAsociado();

        $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');

        //elimino el cliente asociado al pirata
        $objDunaAsociado->eliminarClienteAsociado(
            $conexion, 
            $idDunaAsociado, 
            $idusuario
        );

        //listado de clientes asociados
        $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados(
            $conexion, 
            $idComponente
        );
        
        //si esta vacio cambio de estado a != de asociado
        $flagClientesAsociados = 1;
        if ( empty($arrObjClientesAsociados) ) {
            //03 -> DETECTADO EMISOR CON FOTO
            //04 -> DETECTADO EMISOR SIN FOTO            
            $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
            $estadoPirata = '04';
            if ( $arrObjDuna[0]->__get('_foto1') != '' ) {
                $estadoPirata = '03';
            }
            
            //actualizo el estado del pirata
            //necesario para las busquedas en la bandeja
            $objDuna->updateEstadoDunaAdsl(
                $conexion, 
                $idComponente, 
                $estadoPirata, 
                $idusuario
            );
            
            $flagClientesAsociados = 0;
        }
                
        $conexion->commit(); 
        
        $dataResult = array('flag'=>1, 
                             'mensaje'=>'Cliente asociado eliminado correctamente.', 
                             'flag_asociados'=>$flagClientesAsociados);
        echo json_encode($dataResult);
        exit;
        
    } catch (PDOException $e) {
        $conexion->rollBack();        
        $dataResult = array('flag'=>0, 'mensaje'=>$e->getMessage());
        echo json_encode($dataResult);
        exit;
    }
}

function detallePirata($idComponente)
{
    global $conexion;

    $objDuna = new Data_DunaAdsl();
    $objDunaAsociado = new Data_DunaAsociado();
    $objUbigeo = new Data_Ubigeo();

    $arrObjPirata = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
     
    //datos de ubigeo
    $arrObjDepartamento = $objUbigeo->departamentosUbigeo(
        $conexion, 
        $arrObjPirata[0]->__get('_coddpto')
    );
    
    $arrObjProvincia = $objUbigeo->provinciasUbigeo(
        $conexion, 
        $arrObjPirata[0]->__get('_coddpto'), 
        $arrObjPirata[0]->__get('_codprov')
    );
    
    $arrObjDistrito = $objUbigeo->distritosUbigeo(
        $conexion, 
        $arrObjPirata[0]->__get('_coddpto'), 
        $arrObjPirata[0]->__get('_codprov'), 
        $arrObjPirata[0]->__get('_coddist')
    );
    
    //listado de clientes asociados
    $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados(
        $conexion, 
        $idComponente
    );

    include 'vistas/v_bandeja.duna.internet.detalle.php';
}

function cargarMapaPirata($idComponente)
{
    global $conexion;

    $objDuna   = new Data_DunaAdsl();

    $arrObjPirata = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
    
    if ( !empty($arrObjPirata) ) {
        $dataMarkers = array(
            'idduna'                => $arrObjPirata[0]->__get('_idduna'),
            'item'                  => $arrObjPirata[0]->__get('_item'),
            'zonal'                 => $arrObjPirata[0]->__get('_zonal'),
            'departamento'          => $arrObjPirata[0]->__get('_departamento'),
            'provincia'             => $arrObjPirata[0]->__get('_provincia'),
            'ciudad'                => $arrObjPirata[0]->__get('_ciudad'),
            'distrito'              => $arrObjPirata[0]->__get('_distrito'),
            'direccion'             => $arrObjPirata[0]->__get('_direccion'),
            'fftt'                  => $arrObjPirata[0]->__get('_fftt'),
            'direccion_referencial' => $arrObjPirata[0]->__get('_direccion_referencial'),
            'telefono_referencial'  => $arrObjPirata[0]->__get('_telefono_referencial'),
            'telefono_detectado'    => $arrObjPirata[0]->__get('_telefono_detectado'),
            'filtro_linea'          => $arrObjPirata[0]->__get('_filtro_linea'),
            'codigo'                => $arrObjPirata[0]->__get('_codigo'),
            'estatus'               => $arrObjPirata[0]->__get('_estatus'),
            'tipo'                  => $arrObjPirata[0]->__get('_tipo'),
            'x'                     => $arrObjPirata[0]->__get('_x'),
            'y'                     => $arrObjPirata[0]->__get('_y')
        );
        
    } else {              
    }

    $dataResult = array('marker' => $dataMarkers);
    echo json_encode($dataResult);
    exit;
}

//lista clientes por terminales agrupados en Armarios o Cables
function asociarPirata($idComponente)
{
    global $conexion;

    $objDuna  = new Data_DunaAdsl();
    $objDunaAsociado  = new Data_DunaAsociado();

    $arrObjPirata = $objDuna->obtenerDunaAdsl($conexion, $idComponente);  
    $arrClientesSpeedy = array();
    if ( !empty($arrObjPirata) && $arrObjPirata[0]->__get('_x') != '' 
         && $arrObjPirata[0]->__get('_y') != '' ) {        
        if ( $arrObjPirata[0]->__get('_estado') == '' 
             || $arrObjPirata[0]->__get('_estado') != '05' ) {
            //listado de clientes asociados
            $arrClientesAsociados = array();
            $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados($conexion, $idComponente);

            if ( !empty($arrObjClientesAsociados) ) {
                foreach ( $arrObjClientesAsociados as $objDunaAsociado ) {
                    $arrClientesAsociados[$objDunaAsociado->__get('_telefono')] = $objDunaAsociado;
                }
            }
            
            //listado de clientes speedy cercanos
            //vista por default -> Vista Terminales Cercanos = trm, 
            //listar los 20 primeros terminales mas cercanos
            $arrClientesSpeedy = listarClientesSpeedy($idComponente, 'trm');
        }
        include 'vistas/v_bandeja.duna.internet.asociar.php';
    } else {
        $mensaje = '<p style="padding:40px;"><b>No se tiene registrado el x,y '
                 . 'para este pirata.</b> <br />Debe ingresar el x,y de este '
                 . 'pirata para poder <b>Asociarlo</b></p>';
        echo $mensaje;
        exit;
    }
}

//lista clientes terminales por terminal
function asociarPirata1EraVersion($idComponente)
{
    global $conexion, $nombreModulo;

    $numeros = array(0,1,2,3,4,5,6,7,8,9);

    //capa: DUNA INTERNET id=21
    $idDuna = 21;

    $objTerminales = new Data_FfttTerminales();
    $objComponente = new Data_Componente();
    $objDunaAsociado = new Data_DunaAsociado();

    $pirata = $objComponente->__get_componente($conexion, $idDuna, $idComponente);

    $direccionPirata = $pirata[0]['campo1'];
    $direccionParcial = '';
    if ( !empty($numeros) ) {
        foreach ( $numeros as $num ) {            
            $pos = strpos($direccionPirata, (string)$num);
            if ($pos !== false) {
                $direccionParcial = strtoupper(substr($direccionPirata, 0, $pos-4));
                //si es caracter => si es valido
                break;
            }
        }
    }
  
    if ( !empty($pirata) && $pirata[0]['x'] != '' && $pirata[0]['y'] != '' ) {

        //listado de clientes asociados
        $arrClientesAsociados = array();
        $arrObjClientesAsociados = $objDunaAsociado->getClientesAsociados($conexion, $idComponente);

        if ( !empty($arrObjClientesAsociados) ) {
            foreach ( $arrObjClientesAsociados as $clienteAsociado ) {
                $arrClientesAsociados[$clienteAsociado['telefono']] = $clienteAsociado;
            }
        }


        $arrTerminales = $objTerminales->getTerminalesByXy($conexion, $pirata[0]['x'], $pirata[0]['y'], 10, 20);

        $arrClientesSpeedy = array();
        $arrClientesSpeedySimilarDireccion = array();
        $arrClientesSpeedyDiferenteDireccion = array();
        if ( !empty($arrTerminales) ) {
            foreach ( $arrTerminales as $terminales) {
                $arrClientesSpeedy = $objTerminales->getClientesByTerminal(
                    $conexion, 
                    $terminales['zonal1'], 
                    $terminales['mdf'], 
                    $terminales['cable'], 
                    $terminales['armario'], 
                    $terminales['caja'], 
                    $terminales['tipo_red'], 
                    'S'
                );
                foreach ( $arrClientesSpeedy as $clientes ) {
                    if ( $clientes['Telefono'] != '' ) {
                        $clientes['tipored'] = $terminales['tipo_red'];

                        $pos = strpos(strtoupper($clientes['Direccion']), $direccionParcial);
                        //echo "pos=" . $pos;

                        if ($pos !== false) {
                            $arrClientesSpeedySimilarDireccion[] = $clientes;
                        } else {
                            $arrClientesSpeedyDiferenteDireccion[] = $clientes;
                        }
                    }
                }

            }
        }        
        $arrClientesSpeedy = array_merge(
            $arrClientesSpeedySimilarDireccion, 
            $arrClientesSpeedyDiferenteDireccion
        );

        include 'vistas/v_bandeja.duna.internet.asociar.php';
    } else {
          echo 'Pirata no se encuentra en la DB, o no tiene x,y';
          exit;
    }
}

function asociarPirataBAK($idComponente)
{
    global $conexion, $nombreModulo;

    $numeros = array(0,1,2,3,4,5,6,7,8,9);

    //capa: DUNA INTERNET id=21
    $idDuna = 21;

    $objTerminales = new Data_FfttTerminales();
    $objComponente = new Data_Componente();
    $objDunaAsociado = new Data_DunaAsociado();

    $pirata = $objComponente->__get_componente($conexion, $idDuna, $idComponente);

    $direccionPirata = $pirata[0]['campo1'];
    $direccionParcial = '';
    if ( !empty($numeros) ) {
        foreach ( $numeros as $num ) {
            $pos = strpos($direccionPirata, (string)$num);
            if ($pos !== false) {
                $direccionParcial = strtoupper(substr($direccionPirata, 0, $pos-4));
                break;
            }
        }
    }

    if ( !empty($pirata) && $pirata[0]['x'] != '' && $pirata[0]['y'] != '' ) {
        //listado de clientes asociados
        $arrClientesAsociados = array();
        $arrObjClientesAsociados = $objDunaAsociado->getClientesAsociados($conexion, $idComponente);

        if ( !empty($arrObjClientesAsociados) ) {
            foreach ( $arrObjClientesAsociados as $clienteAsociado ) {
                $arrClientesAsociados[$clienteAsociado['telefono']] = $clienteAsociado;
            }
        }

        $arrTerminales = $objTerminales->getTerminalesByXy($conexion, $pirata[0]['x'], $pirata[0]['y'], 10, 20);

        $arrClientesSpeedy = array();
        $arrClientesSpeedySimilarDireccion = array();
        $arrClientesSpeedyDiferenteDireccion = array();
        if ( !empty($arrTerminales) ) {

            $armariosCercanos = array();
            foreach ( $arrTerminales as $terminales) {
                $armariosCercanos[trim($terminales['armario'])] = array(
                        'zonal'		=> $terminales['zonal'],
                        'zonal1'	=> $terminales['zonal1'],
                        'mdf'		=> $terminales['mdf'],
                        'cable'		=> $terminales['cable'],
                        'armario'	=> $terminales['armario'],
                        'tipo_red'	=> $terminales['tipo_red']
                );
            }
            foreach ( $armariosCercanos as $armarioKey => $armarioData ) {
                $arrClientesSpeedy = $objTerminales->getClientesByTerminal(
                    $conexion, 
                    $armarioData['zonal1'], 
                    $armarioData['mdf'], 
                    $armarioData['cable'], 
                    $armarioData['armario'], 
                    '', 
                    $armarioData['tipo_red'], 
                    'S'
                );
                foreach ( $arrClientesSpeedy as $clientes ) {
                    if ( $clientes['Telefono'] != '' ) {
                        $clientes['tipored'] = $armarioData['tipo_red'];
                        $pos = strpos(strtoupper($clientes['Direccion']), $direccionParcial);
                        if ($pos !== false) {
                            $arrClientesSpeedySimilarDireccion[] = $clientes;
                        } else {
                            $arrClientesSpeedyDiferenteDireccion[] = $clientes;
                        }
                    }
                }

            }
        }
        $arrClientesSpeedy = array_merge(
            $arrClientesSpeedySimilarDireccion, 
            $arrClientesSpeedyDiferenteDireccion
        );
        
        include 'vistas/v_bandeja.duna.internet.asociar.php';

    } else {
        echo 'Pirata no se encuentra en la DB, o no tiene x,y';
        exit;
    }
}

function fotosPirataMain($idComponente)
{
    global $conexion;

    $objDuna = new Data_DunaAdsl();

    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);

    $dataFotos = array();
    if ( trim($arrObjDuna[0]->__get('_foto1')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto1')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto2')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto2')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto3')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto3')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto4')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto4')));
    }

    include 'vistas/v_bandeja.duna.internet.fotos.main.php';
}

function fotosPirata($idComponente)
{
    global $conexion;

    $objDuna = new Data_DunaAdsl();

    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);

    $dataFotos = array();
    if ( trim($arrObjDuna[0]->__get('_foto1')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto1')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto2')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto2')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto3')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto3')));
    }
    if ( trim($arrObjDuna[0]->__get('_foto4')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto4')));
    }

    include 'vistas/v_bandeja.duna.internet.fotos.php';
}

function adjuntarFotosPirata($idComponente)
{
    global $conexion;

    $objDuna = new Data_DunaAdsl();
    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
    
    $dataFotos = array();
    if ( trim($arrObjDuna[0]->__get('_foto1')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto1')), 
                             'campo' => 'foto1');
    }
    if ( trim($arrObjDuna[0]->__get('_foto2')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto2')), 
                             'campo' => 'foto2');
    }
    if ( trim($arrObjDuna[0]->__get('_foto3')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto3')), 
                             'campo' => 'foto3');
    }
    if ( trim($arrObjDuna[0]->__get('_foto4')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto4')), 
                             'campo' => 'foto4');
    }

    include 'vistas/v_bandeja.duna.internet.fotos_adjuntar.php';
}

function asociarPirataAbonado($idComponente, $tipoRed, $ddnTelefono)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    $objDuna = new Data_DunaAdsl();
    $objDunaAsociado = new Data_DunaAsociado();
    
    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);

    $telefono = $clienteUno = $clienteDos = $direccion = '';
    
    //datos del cliente abonado
    $arrAbonado = $objTerminales->listClientes(
        $conexion, 
        $tipoRed, 
        $telefono, 
        $clienteUno, 
        $clienteDos, 
        $direccion, 
        $ddnTelefono
    );

    include 'vistas/v_bandeja.duna.internet.asociar.form.php';
}

function asociarPirataAbonadoBAK($idComponente, $clientesArray)
{
    global $conexion;

    $objTerminales     = new Data_FfttTerminales();
    $objDuna           = new Data_DunaAdsl();
    $objDunaAsociado  = new Data_DunaAsociado();
    
    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
       
    //listado de clientes asociados
    $arrClientesAsociados = array();
    $arrObjClientesAsociados = $objDunaAsociado->obtenerClientesAsociados($conexion, $idComponente);

    if ( !empty($arrObjClientesAsociados) ) {
        foreach ( $arrObjClientesAsociados as $objDunaAsociado ) {
            $arrClientesAsociados[$objDunaAsociado->__get('_telefono')] = $objDunaAsociado;
        }
    }
    
    $telefono = $clienteUno = $clienteDos = $direccion = '';
    $abonadosArray = array();
    if ( !empty($clientesArray) ) {
        foreach ( $clientesArray as $clienteArr ) {

            $cliente = explode("|", $clienteArr);
            $ddnTelefono = $cliente[0];
            $tipoRed = $cliente[1];

            $abonadoArr = $objTerminales->listClientes(
                $conexion, 
                $tipoRed, 
                $telefono, 
                $clienteUno, 
                $clienteDos, 
                $direccion, 
                $ddnTelefono
            );
            $abonadosArray[] = $abonadoArr[0];
        }
    }

    include 'vistas/v_bandeja.duna.internet.asociar.form.php';

}

function noasociarPirataAbonado($idComponente)
{
    try {
        global $conexion;

        $objDuna   = new Data_DunaAdsl();
        
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
        $estado     = '05'; // ESTADO: NO ASOCIADO
        
        $idDuna = $objDuna->updateEstadoDunaAdsl($conexion, $idComponente, $estado, $idUsuario);

        $flag = 1;
        if ( !$idDuna ) {
            $flag = 0;
        }
        
        $dataResult = ($flag) ? array('success'=>1,
                                      'msg'=>'Datos grabados correctamente.') : 
                                      array('success'=>0, 
                                            'msg'=>'Error al grabar los datos.');

        echo json_encode($dataResult);
        exit;
    } catch (PDOException $e) {
        $dataResult = array('success'=>0,'msg'=>$e->getMessage());

        echo json_encode($dataResult);
        exit;
    }
}

function registrarPirata($arrDataPirata, $arrFilesAtis)
{
    try {
        global $conexion;
        
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();
        
        //validando tama�o de fotos atis
        $validaTamanoImagen = validateTamanoImagen($arrFilesAtis['imagen_atis']);
        if ($validaTamanoImagen == '1') { 
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "El tama&ntilde;o de 1 o mas archivos no es 
                    correcto.\n\nSolo se permite 2MB como maximo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN validando tama�o de fotos atis
        
        //Upload de fotos atis
        $listNameFotos = moveFile($arrFilesAtis['imagen_atis'], 'dad');
        
        if ( !empty($listNameFotos) && count($listNameFotos) == 2 ) {
            $arrDataPirata['foto_atis1'] = $listNameFotos[0];
            $arrDataPirata['foto_atis2'] = $listNameFotos[1];
        } else {
            $mensaje = array(
                "flag"      => "0",
                "mensaje"   => "No se logro subir las dos fotos Atis, Vuelva a intentarlo.",
                "tipo"      => "imagen"
            );
            echo json_encode($mensaje);
            exit;
        }
        //FIN Upload de fotos atis
        
        $idusuario          = $_SESSION['USUARIO']->__get('_idUsuario');
        $objDunaAsociado  = new Data_DunaAsociado();
        $objDuna           = new Data_DunaAdsl();

        $arrFechaExpediente = explode("/", $arrDataPirata['fecha_expediente']);
        $fechaExpediente = $arrFechaExpediente[2] . '-' . $arrFechaExpediente[1] . '-' . $arrFechaExpediente[0];

        if ( $arrDataPirata['fecha_deteccion'] != '' ) {
            $fechaDeteccionArr = explode("/", $arrDataPirata['fecha_deteccion']);
            $cadena = substr($fechaDeteccionArr[2], 0, 4) . '-' 
                    . $fechaDeteccionArr[1] . '-' 
                    . $fechaDeteccionArr[0] 
                    . substr($fechaDeteccion, 10, 6);
            
            $fechaDeteccion = $cadena;
        } else {
            $fechaDeteccion = NULL;
        }

        //inserto al cliente como pirata
        $result = $objDunaAsociado->insertDunaAsociado(
            $conexion, 
            $arrDataPirata['idduna'], 
            $arrDataPirata['expediente'], 
            $fechaDeteccion, 
            $fechaExpediente, 
            $arrDataPirata['foto_atis1'], 
            $arrDataPirata['foto_atis2'], 
            $arrDataPirata['titular'], 
            $arrDataPirata['dni'], 
            $arrDataPirata['departamento'], 
            $arrDataPirata['provincia'], 
            $arrDataPirata['distrito'], 
            $arrDataPirata['direccion_antena'], 
            $arrDataPirata['direccion_telefono'], 
            'S', 
            $arrDataPirata['telefono'], 
            $arrDataPirata['inscripcion'], 
            $arrDataPirata['estado_linea'], 
            $arrDataPirata['speedy_contratado'], 
            $arrDataPirata['estado_speedy'], 
            $idusuario, 
            'S'
        );
        
        //actualizo el estado del pirata
        //06 -> ASOCIADO SIN FOTO
        //07 -> ASOCIADO CON FOTO

        $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $arrDataPirata['idduna']);
        $estadoPirata = '06';
        if ( $arrObjDuna[0]->__get('_foto1') != '' ) {
            $estadoPirata = '07';
        }

        //necesario para las busquedas en la bandeja, actualiza estado
        $objDuna->updateEstadoDunaAdsl($conexion, $arrDataPirata['idduna'], $estadoPirata, $idusuario);
        //FIN actualizo el estado del pirata
               
        $conexion->commit(); 
        
        $dataResult = array('flag'=>'1','mensaje'=>'Datos grabados correctamente.','tipo'=>'');
        echo json_encode($dataResult);
        exit;
    } catch (PDOException $e) {
        $conexion->rollBack();       
        $dataResult = array('flag'=>'0','mensaje'=>$e->getMessage(), 'tipo'=>'');
        echo json_encode($dataResult);
        exit;
    }
}

function registrarPirataBAK($clientesArray, $idDuna, $idComponente, 
    $expediente, $fechaExpediente, $fechaDeteccion, $observaciones)
{
    try {
        global $conexion;

        $idusuario = $_SESSION['USUARIO']->__get('_idUsuario');
        $objDuna = new Data_Duna();
        $objDunaAsociado = new Data_DunaAsociado();

        $arrFechaExpediente = explode("/", $fechaExpediente);
        $fechaExpediente = $arrFechaExpediente[2] . '-' . $arrFechaExpediente[1] . '-' . $arrFechaExpediente[0];

        $fechaDeteccionArr = explode("/", $fechaDeteccion);
        $cadena = substr($fechaDeteccionArr[2], 0, 4).'-'
                . $fechaDeteccionArr[1].'-'
                . $fechaDeteccionArr[0]
                . substr($fechaDeteccion, 10, 6);
        
        $fechaDeteccion = $cadena;
        
        //listado de clientes asociados
        $arrClientesAsociados = array();
        $arrObjClientesAsociados = $objDunaAsociado->getClientesAsociados($conexion, $idComponente);

        if ( !empty($arrObjClientesAsociados) ) {
            foreach ( $arrObjClientesAsociados as $clienteAsociado ) {
                $arrClientesAsociados[] = $clienteAsociado['idduna_asociado'];
            }
        }

        $flag = 1;
        //validanado si ya existe algun cliente asociado al pirata
        if ( trim($idDuna) != '' ) {
            if ( !empty($clientesArray) ) {
                $clientesSelArray = array();
                foreach ( $clientesArray as $ddnTelefono => $cliente ) {
                    if ( isset($cliente['idduna_asociado']) 
                         && $cliente['idduna_asociado'] == '' ) {
                        $cliente['foto1_atis'] = '1';
                        $cliente['foto2_atis'] = '2';

                        $result = $objDunaAsociado->insertDunaAsociado(
                            $conexion, 
                            $idDuna, $cliente['titular'], 
                            $cliente['dni'], 
                            $cliente['departamento'], 
                            $cliente['provincia'], 
                            $cliente['distrito'],
                            $cliente['direccion_antena'], 
                            $cliente['direccion_telefono'], 
                            'S', 
                            $cliente['telefono'],
                            $cliente['inscripcion'], 
                            $cliente['estado_linea'], 
                            $cliente['speedy_contratado'], 
                            $cliente['estado_speedy'], 
                            $cliente['foto1_atis'], 
                            $cliente['foto2_atis'],
                            $idusuario, 
                            'S'
                        );
                        if ( !$result ) {
                            $flag = 0;
                            break;
                        }
                    } else {
                        $clientesSelArray[] = $cliente['idduna_asociado'];
                    }
                }

                //si no existe el nuevo cliente asociado -> lo ind_duna_asociado = 'N'
                foreach ( $arrClientesAsociados as $idDunaAsociado ) {
                    if ( !in_array($idDunaAsociado, $clientesSelArray) ) {
                        //si no existe el nuevo cliente asociado -> lo ind_duna_asociado = 'N'
                        $objDunaAsociado->disabledDunaAsociado(
                            $conexion, 
                            $idDunaAsociado, 
                            $idusuario
                        );
                    }
                }
            }
        } else {
            if ( !empty($clientesArray) ) {
                foreach ( $clientesArray as $ddnTelefono => $cliente ) {
                    $idduna = $objDuna->insertDuna(
                        $conexion, 
                        $idComponente, 
                        $expediente, 
                        '', 
                        $fechaExpediente, 
                        $fechaDeteccion, 
                        $observaciones, 
                        'S', 
                        'S', 
                        'ASOCIADO', 
                        $idusuario, 
                        'S'
                    );
                    
                    if ( $idduna ) {
                        $cliente['foto1_atis'] = '1';
                        $cliente['foto2_atis'] = '2';
                        $result = $objDunaAsociado->insertDunaAsociado(
                            $conexion, 
                            $idduna, 
                            $cliente['titular'], 
                            $cliente['dni'], 
                            $cliente['departamento'], 
                            $cliente['provincia'], 
                            $cliente['distrito'],
                            $cliente['direccion_antena'], 
                            $cliente['direccion_telefono'], 
                            'S', 
                            $cliente['telefono'],
                            $cliente['inscripcion'], 
                            $cliente['estado_linea'], 
                            $cliente['speedy_contratado'], 
                            $cliente['estado_speedy'], 
                            $cliente['foto1_atis'], 
                            $cliente['foto2_atis'],
                            $idusuario, 
                            'S'
                        );

                        if ( !$result ) {
                            $flag = 0;
                            break;
                        }
                    } else {
                        $flag = 0;
                        break;
                    }
                }
            }
        }

        $dataResult = ($flag) ? array('success'=>1,
                                      'msg'=>'Datos grabados correctamente.') : 
                                      array('success'=>0,
                                            'msg'=>'Error al grabar los datos.');
        echo json_encode($dataResult);
        exit;
    } catch (PDOException $e) {
        $dataResult = array('success'=>0,'msg'=>$e->getMessage());
        echo json_encode($dataResult);
        exit;
    }
}

function eliminarFotoPirata($idComponente, $campo)
{
    try {
        global $conexion;
        
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();

        $idusuario  = $_SESSION['USUARIO']->__get('_idUsuario');
        $objDuna   = new Data_DunaAdsl();

        $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
        
        $foto1 = $foto2 = $foto3 = $foto4 = '';
        if ( $campo == 'foto1' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto2');
            $foto2 = $arrObjDuna[0]->__get('_foto3');
            $foto3 = $arrObjDuna[0]->__get('_foto4');
            $foto4 = NULL;
        } elseif ( $campo == 'foto2' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $arrObjDuna[0]->__get('_foto3');
            $foto3 = $arrObjDuna[0]->__get('_foto4');
            $foto4 = NULL;
        } elseif ( $campo == 'foto3' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $arrObjDuna[0]->__get('_foto2');
            $foto3 = $arrObjDuna[0]->__get('_foto4');
            $foto4 = NULL;
        } elseif ( $campo == 'foto4' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $arrObjDuna[0]->__get('_foto2');
            $foto3 = $arrObjDuna[0]->__get('_foto3');
            $foto4 = NULL;
        }

        //actualiza las fotos del pirata
        $flag = $objDuna->eliminarFotoPirata(
            $conexion, 
            $idComponente, 
            $foto1, 
            $foto2, 
            $foto3, 
            $foto4, 
            $idusuario
        );
        
        //actualizo el estado del pirata
        //03 -> DETECTADO EMISOR CON FOTO
        //04 -> DETECTADO EMISOR SIN FOTO
        //06 -> ASOCIADO SIN FOTO
        //07 -> ASOCIADO CON FOTO

        $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
        
        $flagCambiaEstado = 0;
        if ( $arrObjDuna[0]->__get('_estado') == '03' 
             && $arrObjDuna[0]->__get('_foto1') == '' ) {
            $estadoPirata = '04';
            $flagCambiaEstado = 1;
        } elseif ( $arrObjDuna[0]->__get('_estado') == '07' 
                   && $arrObjDuna[0]->__get('_foto1') == '' ) {
            $estadoPirata = '06';
            $flagCambiaEstado = 1;
        }

        if ( $flagCambiaEstado ) {
            //necesario para las busquedas en la bandeja, actualiza estado
            $objDuna->updateEstadoDunaAdsl(
                $conexion, 
                $idComponente, 
                $estadoPirata, 
                $idusuario
            );
        }
        //FIN actualizo el estado del pirata
        
        $result = listarFotosPirata($idComponente);

        echo $flag . '|' . $result;
        
        $conexion->commit(); 
    } catch (PDOException $e) {
        $conexion->rollBack();
        $result = '';
    }
}

function agregarFotoPirata($idComponente, $foto)
{
    try {
        global $conexion;
        
        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conexion->beginTransaction();
                
        $idusuario  = $_SESSION['USUARIO']->__get('_idUsuario');
        $objDuna   = new Data_DunaAdsl();

        $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);

        $foto1 = $foto2 = $foto3 = $foto4 = '';
        if ( $arrObjDuna[0]->__get('_foto1') == '' 
             && $arrObjDuna[0]->__get('_foto2') == '' 
             && $arrObjDuna[0]->__get('_foto3') == '' 
             && $arrObjDuna[0]->__get('_foto4') == '' ) {
            $foto1 = $foto;
            $foto2 = NULL;
            $foto3 = NULL;
            $foto4 = NULL;
        } elseif ( $arrObjDuna[0]->__get('_foto1') != '' 
                   && $arrObjDuna[0]->__get('_foto2') == '' 
                   && $arrObjDuna[0]->__get('_foto3') == '' 
                   && $arrObjDuna[0]->__get('_foto4') == '' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $foto;
            $foto3 = NULL;
            $foto4 = NULL;
        } elseif ( $arrObjDuna[0]->__get('_foto1') != '' 
                   && $arrObjDuna[0]->__get('_foto2') != '' 
                   && $arrObjDuna[0]->__get('_foto3') == '' 
                   && $arrObjDuna[0]->__get('_foto4') == '' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $arrObjDuna[0]->__get('_foto2');
            $foto3 = $foto;
            $foto4 = NULL;
        } elseif ( $arrObjDuna[0]->__get('_foto1') != '' 
                   && $arrObjDuna[0]->__get('_foto2') != '' 
                   && $arrObjDuna[0]->__get('_foto3') != '' 
                   && $arrObjDuna[0]->__get('_foto4') == '' ) {
            $foto1 = $arrObjDuna[0]->__get('_foto1');
            $foto2 = $arrObjDuna[0]->__get('_foto2');
            $foto3 = $arrObjDuna[0]->__get('_foto3');
            $foto4 = $foto;
        }
        
        //actualiza las fotos del pirata
        $flag = 0;
        if ( $foto1 == '' && $foto2 == '' && $foto3 == '' && $foto4 == '' ) {
            //error - solo se puede subir 4 fotos
            $flag = 2;
        } else {
            $flag = $objDuna->insertarFotoPirata(
                $conexion, 
                $idComponente, 
                $foto1, 
                $foto2, 
                $foto3, 
                $foto4, 
                $idusuario
            );
        }
        
        //actualizo el estado del pirata
        //03 -> DETECTADO EMISOR CON FOTO
        //04 -> DETECTADO EMISOR SIN FOTO
        //06 -> ASOCIADO SIN FOTO
        //07 -> ASOCIADO CON FOTO

        //$arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
        
        $flagCambiaEstado = 0;
        if ( $arrObjDuna[0]->__get('_estado') == '04' ) {
            $estadoPirata = '03';
            $flagCambiaEstado = 1;
        } elseif ( $arrObjDuna[0]->__get('_estado') == '06' ) {
            $estadoPirata = '07';
            $flagCambiaEstado = 1;
        }

        if ( $flagCambiaEstado ) {
            //necesario para las busquedas en la bandeja, actualiza estado
            $objDuna->updateEstadoDunaAdsl(
                $conexion, 
                $idComponente, 
                $estadoPirata, 
                $idusuario
            );
        }
        //FIN actualizo el estado del pirata
        
        $result = listarFotosPirata($idComponente);

        echo $flag . '|' . $result;
                
        $conexion->commit(); 
    } catch (PDOException $e) {
        $conexion->rollBack();        
        $result = '';
    }
}

function listarFotosPirata($idComponente)
{
    global $conexion;

    $objDuna   = new Data_DunaAdsl();

    $arrObjDuna = $objDuna->obtenerDunaAdsl($conexion, $idComponente);

    $dataFotos = array();
    if ( trim($arrObjDuna[0]->__get('_foto1')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto1')), 
                             'campo' => 'foto1');
    }
    if ( trim($arrObjDuna[0]->__get('_foto2')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto2')), 
                             'campo' => 'foto2');
    }
    if ( trim($arrObjDuna[0]->__get('_foto3')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto3')), 
                             'campo' => 'foto3');
    }
    if ( trim($arrObjDuna[0]->__get('_foto4')) != '' ) {
        $dataFotos[] = array('image' => trim($arrObjDuna[0]->__get('_foto4')), 
                             'campo' => 'foto4');
    }

    $result = '';
    $it = 0;
    foreach ( $dataFotos as $foto) {
        $result .= '<span>'
                 . '<img id="'.$it.'" width="75" src="pages/'
                 . 'imagenComponente.php?imgcomp='.$foto['image']
                 . '&dircomp=dad&w=75"><p><a href="javascript:void(0)" '
                 . 'onclick="eliminarFotoPirata(\''
                 . $arrObjDuna[0]->__get('_idduna').'\',\''.$foto['campo']
                 . '\');"><img width="12" height="12" '
                 . 'src="img/delete_offline.png"></a><a href="javascript:'
                 . 'void(0)" onclick="eliminarFotoPirata(\''
                 . $arrObjDuna[0]->__get('_idduna').'\',\''
                 . $foto['campo'].'\');">Eliminar</a></p></span>';
        $it++;
    }
    
    $result .= '<script>var num_fotos = ' . count($dataFotos)  . ';</script>';

    return $result;
}

function listarClientesSpeedy($idComponente, $tipoListado='trm', 
    $numTerminales=20, $orden='', $campo='')
{
    global $conexion;
    
    $numeros = array(0,1,2,3,4,5,6,7,8,9);

    $objTerminales = new Data_FfttTerminales();
    $objDuna = new Data_DunaAdsl();

    $arrObjPirata = $objDuna->obtenerDunaAdsl($conexion, $idComponente);
    $arrClientesSpeedy = array();

    if ( $arrObjPirata[0]->__get('_estado') == '' 
         || $arrObjPirata[0]->__get('_estado') != '05' ) {

        $direccionPirata = $arrObjPirata[0]->__get('_direccion');
        $direccionParcial = '';
        if ( !empty($numeros) ) {
            foreach ( $numeros as $num ) {
                $pos = strpos($direccionPirata, (string)$num);
                if ($pos !== false) {
                    $direccionParcial = strtoupper(substr($direccionPirata, 0, $pos-4));
                    //si es caracter => si es valido
                    break;
                }
            }
        }

        //LISTADO DE CLIENTES CERCANOS CUANDO EL TIPO ES TERMINAL -> TRM
        //listado terminales cercanos al pirata
        //distancia = 10km, numero de terminales = 20
        $arrTerminales = $objTerminales->getTerminalesByXy(
            $conexion, 
            $arrObjPirata[0]->__get('_x'), 
            $arrObjPirata[0]->__get('_y'), 
            10, 
            $numTerminales
        );

        $arrTerminalesAgrupados = array();
        if ( !empty($arrTerminales) ) {
            foreach ( $arrTerminales as $terminales) {
                $cadena = $terminales['tipo_red'] . '|' 
                        . $terminales['zonal'] . '|' 
                        . $terminales['zonal1'] . '|' 
                        . $terminales['mdf'] . '|' 
                        . $terminales['cable'] . '|' 
                        . $terminales['armario'];
                
                $arrTerminalesAgrupados[$cadena][] = $terminales['caja'];
            }
        }
        //FIN LISTADO DE CLIENTES CERCANOS CUANDO EL TIPO ES TERMINAL -> TRM

        //LISTADO DE CLIENTES CERCANOS CUANDO EL TIPO ES ARMARIO -> ARM
        if ( $tipoListado == 'arm' ) {
            //agrupando los Armarios
            if ( !empty($arrTerminalesAgrupados) ) {
                foreach ( $arrTerminalesAgrupados as $strFftt => $arrTerminalesArmario ) {
                    $arrFftt = explode('|', $strFftt);
                    $tipoRed   = trim($arrFftt[0]);
                    $zonal      = trim($arrFftt[1]);
                    $zonalUno     = trim($arrFftt[2]);
                    $mdf        = trim($arrFftt[3]);
                    $cable      = trim($arrFftt[4]);
                    $armario    = trim($arrFftt[5]);

                    if ( $tipoRed == 'F' ) {
                        $arrTerminales = $objTerminales->get_terminales(
                            $conexion, 
                            $zonal, 
                            $mdf, 
                            $tipoRed, 
                            $cable.$armario
                        );
                        $arrCajas = array();
                        foreach ($arrTerminales as $terminales) {
                            $arrCajas[] = $terminales['caja'];
                        }

                        $arrTerminalesAgrupados[$strFftt] = $arrCajas;
                    }
                }
            }
        }
        //FIN LISTADO DE CLIENTES CERCANOS CUANDO EL TIPO ES ARMARIO -> ARM
        
        $arrClientesSpeedySimilarDireccion = array();
        $arrClientesSpeedyDiferenteDireccion = array();
        if ( !empty($arrTerminalesAgrupados) ) {
            foreach ( $arrTerminalesAgrupados as $strFftt => $arrTerminales ) {
                $arrFftt = explode('|', $strFftt);
                $tipoRed = trim($arrFftt[0]);
                $zonal = trim($arrFftt[1]);
                $zonalUno = trim($arrFftt[2]);
                $mdf = trim($arrFftt[3]);
                $cable = trim($arrFftt[4]);
                $armario = trim($arrFftt[5]);

                $arrClientesSpeedy = $objTerminales->getClientesByTerminales(
                    $conexion, 
                    $zonalUno, 
                    $mdf, 
                    $cable, 
                    $armario, 
                    $arrTerminales, 
                    $tipoRed, 
                    'S'
                );

                foreach ( $arrClientesSpeedy as $clientes ) {
                    //seteamos la distancia entre el cliente y el pirata
                    //distancia expresada en km
                    $distancia = '';
                    if ( $clientes['y'] != '' && $clientes['x'] != '' ) {
                        $distancia = distance(
                            $clientes['y'], 
                            $clientes['x'], 
                            $arrObjPirata[0]->__get('_y'), 
                            $arrObjPirata[0]->__get('_x'), 
                            'K'
                        );
                        $distancia = number_format(($distancia*1000), 2);
                    }
                    
                    $clientes['distancia'] = $distancia;
                    
                    if ( $clientes['Telefono'] != '' ) {                        
                        $clientes['tipored'] = $tipoRed;
                        $pos = strpos(
                            strtoupper($clientes['Direccion']), 
                            $direccionParcial
                        );
                        
                        if ($pos !== false) {
                            $arrClientesSpeedySimilarDireccion[] = $clientes;
                        } else {
                            $arrClientesSpeedyDiferenteDireccion[] = $clientes;
                        }
                    }
                }
            }
        }

        $arrClientesSpeedy = array_merge(
            $arrClientesSpeedySimilarDireccion, 
            $arrClientesSpeedyDiferenteDireccion
        );
        
        // ordenamiento por campo distancia de menor a mayor
        // campo = distancia
        if ( $orden == 'asc' ) {
            $arrClientesSpeedy = orderMultiDimensionalArray($arrClientesSpeedy, $campo, false);
        } elseif ( $orden == 'desc' ) {
            $arrClientesSpeedy = orderMultiDimensionalArray($arrClientesSpeedy, $campo, true);
        }
    }
    return $arrClientesSpeedy;
}

function orderMultiDimensionalArray ($toOrderArray, $field, $inverse = false) 
{  
    $position = array();
    $newRow = array();
    foreach ($toOrderArray as $key => $row) {
        $position[$key]  = $row[$field];
        $newRow[$key] = $row;
    }
    if ($inverse) {
        arsort($position);
    } else {
        asort($position);
    }
    $returnArray = array();
    foreach ($position as $key => $pos) {
        $returnArray[] = $newRow[$key];
    }
    return $returnArray;
} 

function distance($lat1, $lon1, $lat2, $lon2, $unit='K')
{         
    $theta = $lon1 - $lon2;
    $formulaU = sin(deg2rad($lat1)) * sin(deg2rad($lat2));
    $formulaD = cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = $formulaU + $formulaD;
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}

function validateTamanoImagen($imagen)
{
    $tamano = '0';
    foreach ($imagen['size'] as $key => $value) {        
        if ($imagen['size'][$key] > 2048000 || $imagen['size'][$key] == 0 ) {
            $tamano = '1';
            break;
        }
    }
    return $tamano;
}

function moveFile($imagen, $directorio)
{
    $nameImage = array();
    $dir = FFTT . $directorio . "/";

    foreach ($imagen['name'] as $key => $value) {
        if (is_uploaded_file($imagen['tmp_name'][$key])) {
            if ($imagen['name'][$key]!= "") {
                $tmpName = explode(".", $imagen['name'][$key]);
                $imagenName = Logic_Utils::constructUrl($tmpName[0]). '_' .rand(0, 100);
                $extension = $tmpName[count($tmpName)-1];
                if (isset($imagen['tmp_name'][$key])) {
                    if ( move_uploaded_file($imagen['tmp_name'][$key], $dir . $imagenName.'.'.$extension) ) {
                        $nameImage[] = $imagenName. '.' .$extension;
                    }
                }
            }
        }
    }
    return $nameImage;
}

if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'cambiarPaginaDunaInternet':
        cambiarPaginaDunaInternet(
            $_POST['pagina'], 
            $_POST['total'], 
            $_POST['tipo'], 
            $_POST['estado'], 
            $_POST['foto'], 
            $_POST['campo_filtro'], 
            $_POST['campo_valor'], 
            $_POST['departamento'], 
            $_POST['provincia'], 
            $_POST['distrito']
        );
        break;
        
    case 'filtroBusqueda':
        filtroBusqueda(
            $_POST['pagina'], 
            $_POST['total'], 
            $_POST['tipo'], 
            $_POST['estado'], 
            $_POST['foto'], 
            $_POST['campo_filtro'], 
            $_POST['campo_valor'], 
            $_POST['departamento'], 
            $_POST['provincia'], 
            $_POST['distrito']
        );
        break;
        
    case 'listadoClientesCercanos':
        $_POST['num_terminales'] = ( isset($_POST['num_terminales']) && $_POST['num_terminales'] != '' ) ? $_POST['num_terminales'] : 20;
        listadoClientesCercanos(
            $_POST['tipo_listado'], 
            $_POST['idcomponente'], 
            $_POST['num_terminales'], 
            $_POST['orden'], 
            $_POST['campo']
        );
        break;
        
    case 'listadoClientesAsociados':
        listadoClientesAsociados($_POST['idcomponente']);
        break;
        
    case 'detallePirata':
        detallePirata($_POST['idcomponente']);
        break;
        
    case 'asociarPirata':
        asociarPirata($_POST['idcomponente']);
        break;
        
    case 'asociarPirataAbonado':
        asociarPirataAbonado(
            $_POST['idcomponente'], 
            $_POST['tipored'], 
            $_POST['ddn_telefono']
        );
        break;
        
    case 'NoasociarPirataAbonado':
        noasociarPirataAbonado($_POST['idcomponente']);
        break;
        
    case 'registrarPirata':
        registrarPirata($_POST, $_FILES);
        break;
        
    case 'eliminarClienteAsociado':
        eliminarClienteAsociado($_POST['idduna_asociado'], $_POST['idduna']);
        break;
        
    case 'fotosPirataMain':
        fotosPirataMain($_POST['idcomponente']);
        break;
        
    case 'fotosPirata':
        fotosPirata($_POST['idcomponente']);
        break;
        
    case 'adjuntarFotosPirata':
        adjuntarFotosPirata($_POST['idcomponente']);
        break;
        
    case 'agregarFotoPirata':
        agregarFotoPirata($_POST['idcomponente'], $_POST['foto']);
        break;
        
    case 'eliminarFotoPirata':
        eliminarFotoPirata($_POST['idcomponente'], $_POST['campo']);
        break;
        
    case 'cargarMapaPirata':
        cargarMapaPirata($_POST['idcomponente']);
        break;
        
    default:
        listDunaInternet();
        break;
}
