<?php

/**
 * @package     /modulos/maps
 * @name        trazar.ruta.poligono.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/12/05
 */

require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';


if ( isset($_POST['action']) && $_POST['action'] == 'buscaTipoCapa' ) {

    $objPoligonoTipo = new Data_PoligonoTipo();

    $arrayTipoCapas = array();
    if ( $_POST['tipo'] == 'poligono' ) {
    
        $arrayTipoCapas = $objPoligonoTipo->listar($conexion, 'P', '', '1');
        
    } elseif ( $_POST['tipo'] == 'ruta' ) {
    
        $arrayTipoCapas = $objPoligonoTipo->listar($conexion, 'R', '', '1');
    }
    
    if ( !empty($arrayTipoCapas) ) {
        $selTipoCapa = '<select name="tipocapa" id="tipocapa" ';
        $selTipoCapa .= 'style="width:100px; font-size: 11px;height: 20px;padding: 1px;">';
        foreach ( $arrayTipoCapas as $tipocapa ) {
            $selTipoCapa .= '<option value="' . $tipocapa['idpoligono_tipo'] . '">' . $tipocapa['nombre'] . '</option>';
        }
        $selTipoCapa .= '</select>';
    }

    echo $selTipoCapa;
    exit;
    
} elseif ( isset($_POST['action']) && $_POST['action'] == 'agregarTipoCapa' ) {

    $flag = 0;
    $mensaje = 'Error al grabar los datos.';

    $objPoligonoTipo = new Data_PoligonoTipo();

    $idPoligonoTipo = $objPoligonoTipo->insertPoligonoTipo(
        $conexion, $_POST['nombre'], $_POST['tipo'], 
        $_SESSION['USUARIO']->__get('_idUsuario'), '1'
    );

    if ( $idPoligonoTipo ) {
    
        $poligonoTipo = $objPoligonoTipo->getPoligonoTipo(
            $conexion, $idPoligonoTipo
        );
        $flag = 1;
    }

    $arrDataResult = ($flag) ? 
        array('success'=>1, 'msg'=>'Datos grabados correctamente.', 'poligono_tipo'=>$poligonoTipo[0]) : 
        array('success'=>0, 'msg'=>$mensaje);
    
    echo json_encode($arrDataResult);
    exit;

} elseif ( isset($_POST['action']) && $_POST['action'] == 'grabarRutaPoligono' ) {

    $flag = 0;
    $mensaje = 'Error al grabar los datos.';

    $objPoligono = new Data_Poligono();
    
    $existsElemento = $objPoligono->existsPoligono(
        $conexion, $_POST['tipocapa'], $_POST['elemento']
    );
    
    if ( $existsElemento ) {
        $mensaje = "La ruta/poligono ya existe, ingrese una nueva.";
    } else {
        if ( !empty($_POST['xy_arr']) ) {

            $idPoligono = $objPoligono->insertPoligono(
                $conexion, $_POST['tipocapa'], $_POST['elemento'], 
                $_SESSION['USUARIO']->__get('_idUsuario'), '1'
            );
            
            if ( $idPoligono ) {
                $orden = 1;
                foreach ( $_POST['xy_arr'] as $xyPto ) {
                    $arrXy = explode('|', $xyPto);
                    $x = $arrXy[0];
                    $y = $arrXy[1];
                    
                    $objPoligono->insertPoligonoXY(
                        $conexion, $idPoligono, $orden, $x, $y, 
                        $_SESSION['USUARIO']->__get('_idUsuario')
                    );

                    $orden++;
                }
                
                $flag = 1;
            }
        }
    }

    $arrDataResult = ($flag) ? 
        array('success'=>1,'msg'=>'Datos grabados correctamente.') : 
        array('success'=>0,'msg'=>$mensaje);
    
    echo json_encode($arrDataResult);
    exit;
    
} else {

    //template por default
    $template = '<iframe id="ifrMain" name="ifrMain" ';
    $template .= 'src="modulos/maps/vistas/v_trazar_ruta_poligono.ifr.php" ';
    $template .= 'width="100%" height="480"></iframe>';
    
    echo $template;
}