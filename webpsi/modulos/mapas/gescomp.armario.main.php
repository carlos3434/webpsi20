<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.armario.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'A', 'B', 'C', 'D', 'E', 'F');

    $color = '';
    for ($i = 1; $i <= 6; $i++) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }

    return '#' . $color;
}

function save($_POST, $_GET)
{
    global $conexion;
    
    $objTerminales = new Data_FfttTerminales();

    $arrArmarios = $objTerminales->getArmariosByZonalAndMdf($conexion, $_REQUEST['IDzonal'], $_REQUEST['IDmdf']);

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    $arrArmariosSelected = array();
    foreach ($_POST['xy'] as $pkarm => $valarm) {
        if ($valarm['x'] != '' || $valarm['y'] != '') {

            $arrArmariosSelected[$pkarm] = array(
                'x' => $valarm['x'],
                'y' => $valarm['y']
            );
        }
    }

    //update solo a los que surgieron cambios en fftt_armarios
    foreach ($arrArmarios as $armario) {

        if (array_key_exists($armario['mtgespkarm'], $arrArmariosSelected)) {
            if ($armario['x'] != $arrArmariosSelected[$armario['mtgespkarm']]['x'] ||
                    $armario['y'] != $arrArmariosSelected[$armario['mtgespkarm']]['y']) {

                //grabando los xy en fftt_armarios
                $objTerminales->updateArmarioXY(
                    $conexion, $armario['mtgespkarm'], 
                    $arrArmariosSelected[$armario['mtgespkarm']]['x'], 
                    $arrArmariosSelected[$armario['mtgespkarm']]['y'], 
                    $idUsuario
                );
            }
        }
    }

    $arrDataResult = array(
        'ok' => 'Datos Grabados'
    );

    echo json_encode($arrDataResult);
    exit;
} 

function saveEstado($_POST)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    //grabando estado de armarios en fftt_armarios
    $objTerminales->updateEstadoArmario(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDarm'], 
        $_POST['estado'], $idUsuario
    );

    $arrDataResult = array(
        'ok' => 'Datos Grabados'
    );

    echo json_encode($arrDataResult);
    exit;
} 

function buscarMdf($_POST)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByZonalAndIdUsuario($conexion, $_POST['IDzon'], $idUsuario);

    $arrData = array();
    foreach ($arrMdf as $mdf) {
        $arrData[] = array(
            'IDmdf' => $mdf['mdf'],
            'Descmdf' => $mdf['nombre']
        );
    }
    echo json_encode($arrData);
    exit;
} 

function buscarArmarios($_POST)
{
    global $conexion;

    $objZonal = new Data_Zonal();
    $objTerminales = new Data_FfttTerminales();

    //carga de datos (x,y) de Zonal
    $arrZonal = $objZonal->listar($conexion, '', $_POST['IDzonal']);
    $arrDataZonal = array(
        'x' => $arrZonal[0]->__get('_x'),
        'y' => $arrZonal[0]->__get('_y')
    );

    //carga de datos (x,y) de Mdf
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdf = $objTerminales->getMdfByCodMdf($conexion, $_POST['IDmdf']);
    $arrDataMdf = array(
        'x' => $arrMdf[0]['x'],
        'y' => $arrMdf[0]['y']
    );

    $arrArmarios = $objTerminales->getArmariosByZonalAndMdf(
        $conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['datosXY']
    );

    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrArmarios as $armarios) {
        if ($armarios['y'] != '' && $armarios['x'] != '') {
            $armarios['fecha_estado'] = obtenerFecha($armarios['fecha_estado']);
            $arrDataMarkersConXy[] = $armarios;
        }

        $arrDataMarkers[] = $armarios;
        $i++;
    }


    $arrPoligonos = array();
    $promX = $promY = 0;
    // ======================== DATA DE POLIGONOS =========================== //
    if (isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si') {
        $objCapasArea = new Data_FfttCapasArea();

        $idMdf = $_POST['IDmdf'];

        $arrPuntosXyMdf = $objCapasArea->getXysByTipoCapaAndElemento($conexion, 'MDF', $idMdf);
        $arrPoligonos[] = array(
            'coords' => $arrPuntosXyMdf,
            'color' => generaColorPoligono()
        );


        $countXy = count($arrPoligonos[0]['coords']);
        
        $promX = $promY = 0;
        if ( $countXy > 0 ) {
            $sumX = $sumY = 0;
            foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
            }
        
            $promX = $sumX / $countXy;
            $promY = $sumY / $countXy;
        }
        
    }
    // ========================== FIN DATA DE POLIGONOS ===================== //



    $arrDataResult = array(
        'zonal' => $arrDataZonal,
        'mdf' => $arrDataMdf,
        'armarios' => $arrDataMarkers,
        'armarios_con_xy' => $arrDataMarkersConXy,
        'xy_poligono' => $arrPoligonos,
        'center_map' => array(
            'x' => $promX,
            'y' => $promY
        )
    );

    echo json_encode($arrDataResult);
    exit;
} 

function buscar($_POST)
{
    global $conexion;

    $arrPoligonos = array();
    $promX = $promY = 0;
    // ========================= DATA DE POLIGONOS ========================== //
    if (isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si') {
        $objCapasArea = new Data_FfttCapasArea();

        $idMdf = $_POST['IDmdf'];

        $arrPuntosXyMdf = $objCapasArea->getXysByTipoCapaAndElemento($conexion, 'MDF', $idMdf);
        $arrPoligonos[] = array(
            'coords' => $arrPuntosXyMdf,
            'color' => generaColorPoligono()
        );


        $countXy = count($arrPoligonos[0]['coords']);

        $sumX = $sumY = 0;
        foreach ($arrPoligonos[0]['coords'] as $puntoXY) {
            $sumX += $puntoXY['x'];
            $sumY += $puntoXY['y'];
        }

        $promX = $sumX / $countXy;
        $promY = $sumY / $countXy;

        $arrDataResult = array(
            'xy_poligono' => $arrPoligonos,
            'center_map' => array(
                'x' => $promX,
                'y' => $promY
            )
        );

        echo json_encode($arrDataResult);
        exit;
    }
    // ========================= FIN DATA DE POLIGONOS ====================== //
}

function listarZonales()
{
    $arrZonal = $_SESSION['USUARIO_ZONAL'];

    $arrData = array();
    foreach ($arrZonal as $objZonal) {
        $arrData[] = array(
            'abvZonal'     => $objZonal->__get('_abvZonal'),
            'descZonal'    => $objZonal->__get('_descZonal')
        );
    }
    echo json_encode($arrData);
    exit;
}


function vistaInicial()
{
    //template por default
    $template = '<iframe id="ifrMain" name="ifrMain" ';
    $template .= 'src="modulos/maps/vistas/v_gescomp_armario.ifr.php" ';
    $template .= 'width="100%" height="480"></iframe>';
    
    echo $template;

}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'save':
        save($_POST, $_GET);
        break;
    case 'saveEstado':
        saveEstado($_POST);
        break;
    case 'buscarMdf':
        buscarMdf($_POST);
        break;
    case 'buscarArmarios':
        buscarArmarios($_POST);
        break;
    case 'buscar':
        buscar($_POST);
        break;
    case 'listarZonales':
        listarZonales();
        break;
    default:
        vistaInicial();
        break;
}