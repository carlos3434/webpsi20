<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.mdf.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');
    
    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }
    
    return '#' . $color;
}

function save($_POST, $_GET)
{
    global $conexion;

    $objTerminales = new Data_FfttTerminales();
    
    //Devuelve el array de mdfs por zonal
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdfs = $objTerminales->getMdfByZonalAndIdUsuario($conexion, $_REQUEST['IDzonal'], $idUsuario);
        
    $arrMdfsSelected = array();
    foreach ( $_POST['xy'] as $pkmdf => $valmdf ) {
        if ( $valmdf['x'] != '' || $valmdf['y'] != '' ) {
        
            $arrMdfsSelected[$pkmdf] = array(
                'x' => $valmdf['x'],
                'y' => $valmdf['y']
            );
        }
    }
    
    //update solo a los que surgieron cambios para fftt_terminales_historial
    $flag = 1;
    foreach ( $arrMdfs as $mdf ) {
        
        if ( array_key_exists($mdf['abv_zonal'] . '|' . $mdf['mdf'], $arrMdfsSelected) ) {
            if ( $mdf['x'] != $arrMdfsSelected[$mdf['abv_zonal'] . '|' . $mdf['mdf']]['x'] ||
                $mdf['y'] != $arrMdfsSelected[$mdf['abv_zonal'] . '|' . $mdf['mdf']]['y'] ) {
            
                //grabando los xy en tabla: mdfs
                $result = $objTerminales->updateMdfXY($conexion, $mdf['abv_zonal'], $mdf['mdf'], $arrMdfsSelected[$mdf['abv_zonal'] . '|' . $mdf['mdf']]['x'], $arrMdfsSelected[$mdf['abv_zonal'] . '|' . $mdf['mdf']]['y']);
                if ( !$result ) {
                    $flag = 0;
                    break;
                }
            }
        }
    }
    
    $arrDataResult = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($arrDataResult);
    exit;
}

function saveEstado($_POST)
{
    global $conexion;
    
    $objCapas = new Data_FfttTerminales();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    
    //grabando estado de armarios en fftt_edificios
    $result = $objCapas->updateEstadoMdf($conexion, $_POST['zonal'], $_POST['mdf'], $_POST['estado']);
    
    $arrDataResult = ($result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($arrDataResult);
    exit;
}

function buscarMdfs($_POST)
{
    global $conexion;

    $objZonal = new Data_Zonal();
    $objTerminales = new Data_FfttTerminales();
    
    //cargando datos de zonal
    $arrZonal = $objZonal->listar($conexion, '', $_POST['IDzonal']);
    $arrDataZonal = array(
        'x' => $arrZonal[0]->__get('_x'),
        'y' => $arrZonal[0]->__get('_y')
    );
    
    //Devuelve el array de mdfs por zonal - usuario
    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrMdfs = $objTerminales->getMdfByZonalAndIdUsuario($conexion, $_POST['IDzonal'], $idUsuario, $_POST['datosXY']);
    
    
    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrMdfs as $mdfs) {
        if ( $mdfs['y'] != '' && $mdfs['x'] != '' ) {
            $arrDataMarkersConXy[] = $mdfs;
        }

        $arrDataMarkers[] = $mdfs;
        $i++;
    }
    
    $arrEstadosMdfs = array(
        array('id'=>'1', 'val'=>'Estado 1'),
        array('id'=>'2', 'val'=>'Estado 2'),
        array('id'=>'3', 'val'=>'Estado 3')
    );
    $arrEstados = array(
        '1' => 'Estado 1',
        '2' => 'Estado 2',
        '3' => 'Estado 3'
    );
    
    $arrDataResult = array(
        'zonal'             => $arrDataZonal,
        'estados'           => $arrEstadosMdfs,
        'est'               => $arrEstados,
        'edificios'         => $arrDataMarkers,
        'edificios_con_xy'  => $arrDataMarkersConXy
    );
    
    echo json_encode($arrDataResult);
    exit;
}

function listarZonales()
{
    $arrZonal = $_SESSION['USUARIO_ZONAL'];

    $arrData = array();
    foreach ($arrZonal as $objZonal) {
        $arrData[] = array(
                'abvZonal'     => $objZonal->__get('_abvZonal'),
                'descZonal'    => $objZonal->__get('_descZonal')
        );
    }
    echo json_encode($arrData);
    exit;
}

function vistaInicial()
{
    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_mdf.ifr.php" width="100%" height="480"></iframe>';

}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'save':
        save($_POST, $_GET);
        break;
    case 'saveEstado':
        saveEstado($_POST);
        break;
    case 'buscarMdfs':
        buscarMdfs($_POST);
        break;
    case 'listarZonales':
        listarZonales();
        break;
    default:
        vistaInicial();
        break;
}
