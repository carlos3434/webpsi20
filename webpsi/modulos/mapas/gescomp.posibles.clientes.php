<?php
require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';




function genera_color_poligono() {
    $valores_arr = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');

    $color = '';
    for( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 16);
        $color .= $valores_arr[$rand];
    }

    return '#' . $color;
}

function MdfsxZonal($zonal) {
    global $conexion;

    $script = '<script type="text/javascript">
        $(function(){
                $("select.clsMultiple").multiselect().multiselectfilter();
        });
    </script>';
    
    $obj_terminales = new data_ffttTerminales();

    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    $array_mdf = $obj_terminales->get_mdf_by_zonal_and_idusuario($conexion, $zonal, $idusuario);

    $selMdf = '<select id="mdf" name="mdf" class="clsMultiple" multiple="multiple">';
    foreach($array_mdf as $mdf) {
            $selMdf .= '<option value="' . $mdf['mdf'] . '">' . $mdf['mdf'] . ' - ' . $mdf['nombre'] . '</option>';
    }
    $selMdf .= '</select>';

    echo $script . $selMdf;
    exit;
}


function DibujarMdf($area_mdf, $arreglo_mdf) {
    global $conexion;
    
    $poligonos_array = array();
    $prom_x = $prom_y = 0;

    if( isset($area_mdf) && $area_mdf == 'si' ) {
        $obj_capasarea = new data_ffttCapasArea();

        $poligonos_array = array();

        //lleno el array para el tipo NODO
        if( !empty( $arreglo_mdf ) ) {
            foreach( $arreglo_mdf as $mdf ) {
                //$mdfs_arr[] = $mdf_arr[0];
                //capturando los X,Y de los elementos seleccionados
                $puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $mdf, 0);
                $poligonos_array[] = array(
                    'coords'    => $puntosXY_mdf,
                    'color'     => genera_color_poligono()
                ); 
            }
        }

        
        $count_xy = count($poligonos_array[0]['coords']);

        $sum_x = $sum_y = 0;
        foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
            $sum_x += $puntoXY['x'];
            $sum_y += $puntoXY['y'];
        }

        $prom_x = $sum_x / $count_xy;
        $prom_y = $sum_y / $count_xy;

        $data_result = array(
            'xy_poligono' => $poligonos_array,
            'center_map'  => array(
                'x' => $prom_x,
                'y' => $prom_y
            )
        );

        echo json_encode($data_result);
        exit;

    }
}

function ListarPosiblesClientes($zonal, $arreglo_mdf, $datosXY) {
    global $conexion;

    $obj_zonal = new data_Zonal();
    $obj_terminales = new data_ffttTerminales();
    $obj_posiblesClientes = new data_PosibleCliente();

    //carga de datos (x,y) de Zonal
    $array_zonal = $obj_zonal->__list($conexion, '', $zonal);
    $data_zonal = array(
        'x' => $array_zonal[0]->__get('x'),
        'y' => $array_zonal[0]->__get('y')
    );

    //carga de datos (x,y) de Mdf
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    $array_mdf = $obj_terminales->get_mdf_by_codmdf($conexion, $arreglo_mdf[0]);
    $data_mdf = array(
        'x' => $array_mdf[0]['x'],
        'y' => $array_mdf[0]['y']
    );
    
    $array_posiblesClientes = $obj_posiblesClientes->__listPosiblesClientes($conexion, $zonal, $arreglo_mdf, $datosXY);
    
    $data_markers = array();
    $data_markers_con_xy = array();
    $i = 1;
    foreach ($array_posiblesClientes as $edificios) {
        if( $edificios['y'] != '' && $edificios['x'] != '' ) {
            $data_markers_con_xy[] = $edificios;
        }

        $data_markers[] = $edificios;
        $i++;
    }

        
    $data_result = array(
        'zonal'             => $data_zonal,
        'mdf'               => $data_mdf,
        'edificios'         => $data_markers,
        'edificios_con_xy'  => $data_markers_con_xy
    );
	
    echo json_encode($data_result);
    exit;   
}

function GrabarDatos($zonal, $mdf_array, $xy) {
    global $conexion;
    
    $arreglo_mdf = explode(',', $mdf_array);

    $obj_posiblesClientes = new data_PosibleCliente();

    $array_posiblesClientes = $obj_posiblesClientes->__listPosiblesClientes($conexion, $zonal, $arreglo_mdf);
    
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    
    $posiblesclientes_selected_arr = array();
    foreach( $xy as $pkposcli => $valposcli ) {
    	if( $valposcli['x'] != '' || $valposcli['y'] != '' ) {
    	
            $posiblesclientes_selected_arr[$pkposcli] = array(
                    'x' => $valposcli['x'],
                    'y' => $valposcli['y']
            );
    	}
    }
    
    //update solo a los que surgieron cambios en posibles_clientes
    $flag = 1;
    foreach( $array_posiblesClientes as $posiblecliente ) {
        if( array_key_exists($posiblecliente['idposible_cliente'], $posiblesclientes_selected_arr) ) {
            
            if( $posiblecliente['x'] != $posiblesclientes_selected_arr[$posiblecliente['idposible_cliente']]['x'] ||
                $posiblecliente['y'] != $posiblesclientes_selected_arr[$posiblecliente['idposible_cliente']]['y'] ) {

                //grabando los xy en posibles_clientes
                $result = $obj_posiblesClientes->__updateXYPosibleCliente($conexion, $posiblecliente['idposible_cliente'], $posiblesclientes_selected_arr[$posiblecliente['idposible_cliente']]['x'], $posiblesclientes_selected_arr[$posiblecliente['idposible_cliente']]['y'], $idusuario);
                if( !$result ) {
                    $flag = 0;
                    break;
                }
            }
        }
    }

    $data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
    exit;
}

function CambiarEstado($ids_arr, $estado) {
    global $conexion;

    $obj_posiblesClientes = new data_PosibleCliente();
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    
    $flag = 1;
    if( !empty($ids_arr) ) {
        //update estado en posibles_clientes
        $result = $obj_posiblesClientes->__updateEstadoPosibleCliente($conexion, $ids_arr, $estado, $idusuario);
        if( !$result ) {
            $flag = 0;
            break;
        }
    }

    $data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
    exit;
}

//listados anidados de FFTT
function ffttMdfporZonal( $zonal ) {
    global $conexion;
    
    $obj_terminales = new data_ffttTerminales();

    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    $array_mdf = $obj_terminales->get_mdf_by_zonal_and_idusuario($conexion, $zonal, $idusuario);

    $data_arr = array();
    foreach ($array_mdf as $mdf) {
        $data_arr[] = array(
            'IDmdf' 	=> $mdf['mdf'],
            'Descmdf'	=> $mdf['nombre']
        );
    }
    echo json_encode($data_arr);
    exit;
}

function ffttCableArmario($IDzonal, $IDmdf, $IDtipred) {
    global $conexion;
    
    $obj_terminales = new data_ffttTerminales();
    $armarioCable_arr = $obj_terminales->get_cable_armario_by_zonal_and_mdf_and_tipored($conexion, $IDzonal, $IDmdf, $IDtipred);

    $data_arr = array();
    foreach($armarioCable_arr as $armarioCable) {
        $data_arr[] = array(
            'IDcableArmario' 	=> $armarioCable['cablearmario'],
            'DescableArmario'	=> $armarioCable['cablearmario']
        );
    }
    echo json_encode($data_arr);
    exit;
}

function ffttCaja($IDzonal, $IDmdf, $IDtipred, $IDcableArmario) {
    global $conexion;
    
    $obj_terminales = new data_ffttTerminales();
    $caja_arr = $obj_terminales->get_terminales($conexion, $IDzonal, $IDmdf, $IDtipred, $IDcableArmario);

    $data_arr = array();
    foreach($caja_arr as $caja) {
        $data_arr[] = array(
            'mtgespktrm'    => $caja['mtgespktrm'],
            'Caja'          => $caja['caja']
        );
    }
    echo json_encode($data_arr);
    exit;
}

function ffttCajaDetalle($mtgespktrm) {
    global $conexion;
    
    $obj_terminales = new data_ffttTerminales();
    $caja_arr = $obj_terminales->get_terminal_by_mtgespktrm($conexion, $mtgespktrm);

    $data_arr = array();
    if( !empty($caja_arr) ) {
        $data_arr = $caja_arr;
    }
    echo json_encode($data_arr);
    exit;
}


function IniDefault() {

    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_posibles_clientes.ifr.php" width="100%" height="480"></iframe>';
    
}



        
if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'CambiarEstado':
        CambiarEstado($_POST['ids_arr'], $_POST['estado']);
        break;
    case 'GrabarDatos':
        GrabarDatos($_GET['zonal'], $_GET['arreglo_mdf'], $_POST['xy']);
        break;
    case 'ListarPosiblesClientes':
        ListarPosiblesClientes($_POST['zonal'], $_POST['arreglo_mdf'], $_POST['datosXY']);
        break;
    case 'DibujarMdf':
        DibujarMdf($_POST['area_mdf'], $_POST['arreglo_mdf']);
        break;
    case 'MdfsxZonal':
        MdfsxZonal($_POST['zonal']);
        break;
    case 'ffttMdfporZonal':
        ffttMdfporZonal($_POST['zonal']);
        break;
    case 'ffttCableArmario':
        ffttCableArmario($_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred']);
        break;
    case 'ffttCaja':
        ffttCaja($_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred'], $_POST['IDcableArmario']);
        break;
    case 'ffttCajaDetalle':
        ffttCajaDetalle($_POST['mtgespktrm']);
        break;
    default:
        IniDefault();
        break;
}

?>