<?php

/**
 * @package     modulos/maps/
 * @name        gescomp.tap.main.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/07/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function generaColorPoligono()
{
    $arrValores = array('0','1','2','3','4','5','6','7','8','9',
            'A','B','C','D','E','F');
    
    $color = '';
    for ( $i=1; $i<=6; $i++ ) {
        $rand = rand(1, 15);
        $color .= $arrValores[$rand];
    }
    
    return '#' . $color;
}

function save($_POST, $_GET)
{
    global $conexion;

    $objCms = new Data_FfttCms();

    $arrTaps = $objCms->tapXNodoCatvTrobaAmplificador(
        $conexion, 
        $_REQUEST['IDnodo'], 
        $_REQUEST['IDtroba'], 
        $_REQUEST['IDamplificador']
    );

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

    $arrTapsSelected = array();
    foreach ($_POST['xy'] as $pktrm => $valtrm) {
        if ($valtrm['x'] != '' || $valtrm['y'] != '') {

            $arrTapsSelected[$pktrm] = array(
                'x' => $valtrm['x'],
                'y' => $valtrm['y']
            );
        }
    }

    //update solo a los que surgieron cambios para fftt_cms
    $flag = 1;
    foreach ($arrTaps as $terminal) {
        if (array_key_exists($terminal['tap'], $arrTapsSelected)) {
            if ($terminal['x'] != $arrTapsSelected[$terminal['tap']]['x'] ||
                    $terminal['y'] != $arrTapsSelected[$terminal['tap']]['y']) {

                //grabando los xy en fftt_cms
                $result = $objCms->updateXyTap(
                    $conexion, 
                    $_REQUEST['IDnodo'], 
                    $_REQUEST['IDtroba'], 
                    $_REQUEST['IDamplificador'], 
                    $terminal['tap'], 
                    $arrTapsSelected[$terminal['tap']]['x'], 
                    $arrTapsSelected[$terminal['tap']]['y']
                );
                if (!$result) {
                    $flag = 0;
                    break;
                }
            }
        }
    }
    
    $arrDataResult = ($flag) ? array('success' => 1, 'msg' => 'Datos grabados correctamente.') : array('success' => 0, 'msg' => 'Error al grabar los datos.');

    echo json_encode($arrDataResult);
    exit;
} 

function buscarNodo($_POST)
{
    global $conexion;

    $objUsuarioNodo = new data_UsuarioNodo();

    $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');
    $arrNodo = $objUsuarioNodo->listarNodosXUsuarioZonal($conexion, $_POST['IDzon'], $idUsuario, '', 'CATV');

    $arrData = array();
    foreach ($arrNodo as $nodo) {
        $arrData[] = array(
            'IDnodo' => $nodo['nodo'],
            'Descnodo' => $nodo['desc_nodo']
        );
    }
    echo json_encode($arrData);
    exit;
}

function buscarTroba($_POST)
{
    global $conexion;

    $objCms = new Data_FfttCms();
    $arrTroba = $objCms->trobasXNodoCatv($conexion, $_POST['IDnodo']);

    $arrData = array();
    foreach ($arrTroba as $troba) {
        $arrData[] = array(
            'troba' => $troba['troba']
        );
    }

    echo json_encode($arrData);
    exit;
}

function buscarAmplificador($_POST)
{
    global $conexion;

    $objCms = new Data_FfttCms();
    $arrAmplificador = $objCms->amplificadorXNodoCatvTroba($conexion, $_POST['IDnodo'], $_POST['IDtroba']);

    $arrData = array();
    foreach ($arrAmplificador as $amplificador) {
        $arrData[] = array(
            'amp' => $amplificador['amp']
        );
    }

    echo json_encode($arrData);
    exit;
}

function buscarTaps($_POST)
{
    global $conexion;

    $objZonal = new Data_Zonal();
    $objCms = new Data_FfttCms();

    //carga de datos (x,y) de Zonal
    $arrZonal = $objZonal->listar($conexion, '', $_POST['IDzonal']);
    $arrDataZonal = array(
        'x' => $arrZonal[0]->__get('_x'),
        'y' => $arrZonal[0]->__get('_y')
    );

    $arrTaps = $objCms->tapXNodoCatvTrobaAmplificador(
        $conexion, 
        $_POST['IDnodo'], 
        $_POST['IDtroba'], 
        $_POST['IDamplificador'], 
        $_POST['datosXY']
    );

    $arrDataMarkers = array();
    $arrDataMarkersConXy = array();
    $i = 1;
    foreach ($arrTaps as $taps) {
        if ($taps['y'] != '' && $taps['x'] != '') {
            $arrDataMarkersConXy[] = $taps;
        }

        $arrDataMarkers[] = $taps;
        $i++;
    }

    $arrEstadosTerminales = array(
        array('id' => '1', 'val' => 'Estado 1'),
        array('id' => '2', 'val' => 'Estado 2'),
        array('id' => '3', 'val' => 'Estado 3')
    );
    $arrEstados = array(
        '1' => 'Estado 1',
        '2' => 'Estado 2',
        '3' => 'Estado 3'
    );

    $arrDataResult = array(
        'zonal' => $arrDataZonal,
        'estados' => $arrEstadosTerminales,
        'est' => $arrEstados,
        'edificios' => $arrDataMarkers,
        'edificios_con_xy' => $arrDataMarkersConXy
    );

    echo json_encode($arrDataResult);
    exit;
} 

function buscar($_POST)
{
    global $conexion;

    $arrPoligonos = array();
    $promX = $promY = 0;
    // ======================= DATA DE POLIGONOS ============================ //
    if (isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si') {
        $objCapasarea = new Data_FfttCapasArea();

        $idMdf = $_POST['IDmdf'];

        $arrPuntosXyMdf = $objCapasarea->getXysByTipoCapaAndElemento($conexion, 'NODO', $idMdf);
        $arrPoligonos[] = array(
            'coords' => $arrPuntosXyMdf,
            'color' => generaColorPoligono()
        );


        $countXy = count($arrPoligonos[0]['coords']);

        
        $promX = $promY = 0;
        if ( $countXy > 0 ) {
            $sumX = $sumY = 0;
            foreach ( $arrPoligonos[0]['coords'] as $puntoXY ) {
                $sumX += $puntoXY['x'];
                $sumY += $puntoXY['y'];
            }
        
            $promX = $sumX / $countXy;
            $promY = $sumY / $countXy;
        }
        

        $arrDataResult = array(
            'xy_poligono' => $arrPoligonos,
            'center_map' => array(
                'x' => $promX,
                'y' => $promY
            )
        );

        echo json_encode($arrDataResult);
        exit;
    }
    // ========================================== FIN DATA DE POLIGONOS ======================================= //
}

function listarZonales()
{
    $arrZonal = $_SESSION['USUARIO_ZONAL'];

    $arrData = array();
    foreach ($arrZonal as $objZonal) {
        $arrData[] = array(
                'abvZonal'     => $objZonal->__get('_abvZonal'),
                'descZonal'    => $objZonal->__get('_descZonal')
        );
    }
    echo json_encode($arrData);
    exit;
}

function vistaInicial()
{
    //template por default
    echo '<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_tap.ifr.php" width="100%" height="480"></iframe>';

}


if (!isset($_REQUEST['action']))
    $_REQUEST['action'] = '';

switch ($_REQUEST['action']) {
    case 'save':
        save($_POST, $_GET);
        break;
    case 'buscarNodo':
        buscarNodo($_POST);
        break;
    case 'buscarTroba':
        buscarTroba($_POST);
        break;
    case 'buscarAmplificador':
        buscarAmplificador($_POST);
        break;
    case 'buscarTaps':
        buscarTaps($_POST);
        break;
    case 'buscar':
        buscar($_POST);
        break;
    case 'listarZonales':
        listarZonales();
        break;
    default:
        vistaInicial();
        break;
}