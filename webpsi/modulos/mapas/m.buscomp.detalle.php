<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.detalle.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

$isLogin = 0;
$dirModulo = '../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; height=device-height; maximum-scale=1.4; initial-scale=1.0; user-scalable=yes"/>
<title>Webunificada - Movil</title>
<script type="text/javascript" src="../../js/jquery/jquery-latest.js"></script>
<link href="../../css/movil.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php include '../../m.header.php';?>


<div style="margin: 5px;"><?php echo $_SESSION['MARKERS'][$_REQUEST['abv_comp']][$_REQUEST['pos_comp']]?></div>

</body>
</html>