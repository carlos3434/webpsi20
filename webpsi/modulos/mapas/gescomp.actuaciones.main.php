<?php
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';


function genera_color_poligono() {
	$valores_arr = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
	
	$color = '';
	for( $i=1; $i<=6; $i++ ) {
		$rand = rand(1, 16);
		$color .= $valores_arr[$rand];
	}
	
	return '#' . $color;
}

$array_zonal = $_SESSION['USUARIO_ZONAL'];


if( isset($_GET['action']) && $_GET['action'] == 'save' ) {

    $obj_terminales = new data_ffttTerminales();
    
	$array_terminales = $obj_terminales->get_terminales($conexion, $_REQUEST['IDzonal'], $_REQUEST['IDmdf'], $_REQUEST['IDtipred'], $_REQUEST['IDcableArmario']);
    
    $idusuario = $_SESSION['USUARIO']->__get('idusuario');
    $ipusuario = Logic_Utils::getIP();
    
    $terminales_selected_arr = array();
    foreach( $_POST['xy'] as $pktrm => $valtrm ) {
    	if( $valtrm['x'] != '' || $valtrm['y'] != '' ) {
    	
    		$terminales_selected_arr[$pktrm] = array(
    			'x' => $valtrm['x'],
    			'y' => $valtrm['y']
    		);
    	}
    }
    
    //update solo a los que surgieron cambios para fftt_terminales_historial
    $flag = 1;
	foreach( $array_terminales as $terminal ) {
		
		if( array_key_exists($terminal['mtgespktrm'], $terminales_selected_arr) ) {
			if( $terminal['x'] != $terminales_selected_arr[$terminal['mtgespktrm']]['x'] ||
				$terminal['y'] != $terminales_selected_arr[$terminal['mtgespktrm']]['y'] ) {
			
				//grabando los xy en map_terminales
    			$result = $obj_terminales->__updateXY($conexion, $terminal['mtgespktrm'], $terminales_selected_arr[$terminal['mtgespktrm']]['x'], $terminales_selected_arr[$terminal['mtgespktrm']]['y']);
				if( !$result ) {
					$flag = 0;
					break;
				}

				//grabando en el historial 
    			$obj_terminales->__updateXYhistorial($conexion, $terminal['mtgespktrm'], $terminal['x'], $terminal['y'], 
    					$idusuario, $ipusuario);
			}
		}
	}
	$data_result = ($flag) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
    
    echo json_encode($data_result);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarZonales' ) {

	$obj_zonalEmpresa = new data_ZonalEmpresa();

	$array_zonal = $obj_zonalEmpresa->__listZonalByEmpresa($conexion, $_POST['IDeecc']);
	
	$data_arr = array();
	foreach ($array_zonal as $zonal_obj) {
    	$data_arr[] = array(
    		'IDzonal' 	=> $zonal_obj->__get('zonal'),
    		'Desczonal'	=> $zonal_obj->__get('zonal')
    	);
	}
	echo json_encode($data_arr);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarMdf' ) {

	$obj_terminales = new data_ffttTerminales();
	$obj_empresa 	= new data_Empresa();

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$empresa = $obj_empresa->__offsetGet($conexion, $_POST['EECC']);
	
	$array_mdf = $obj_terminales->get_mdf_by_eecc_zonal_and_idusuario($conexion, $empresa->__get('desc_empresa'), $_POST['IDzon'], $idusuario);
	
	$data_arr = array();
	foreach ($array_mdf as $mdf) {
    	$data_arr[] = array(
    		'IDmdf' 	=> $mdf['mdf'],
    		'Descmdf'	=> $mdf['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscarCableArmario' ) {

	$obj_terminales = new data_ffttTerminales();
	$armarioCable_arr = $obj_terminales->get_cable_armario_by_zonal_and_mdf_and_tipored($conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred']);
		
	$data_result = '<select id="selCableArmario">';
	foreach($armarioCable_arr as $armarioCable) {
		$data_result .= '<option value="' . $armarioCable['cablearmario'] . '">' . $armarioCable['cablearmario'] . '</option>';
	}
	$data_result .= '</select>';
	
	echo $data_result;
	exit;
}


elseif( isset($_POST['action']) && $_POST['action'] == 'buscarTerminales' ) {

	$obj_zonal = new data_Zonal();
	$obj_terminales = new data_ffttTerminales();
	$obj_pen_pais = new data_PenPais();
	
	//carga de datos (x,y) de Zonal
	$array_zonal = $obj_zonal->__list($conexion,'',$_POST['IDzonal']);
	$data_zonal = array(
		'abv'	=> $array_zonal[0]->__get('abv_zonal'),
		'name'	=> $array_zonal[0]->__get('desc_zonal'),
		'x'    	=> $array_zonal[0]->__get('x'),
		'y' 	=> $array_zonal[0]->__get('y')
	);
	
	//carga de datos (x,y) de Mdf
	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	$array_mdf = $obj_terminales->get_mdf_by_codmdf($conexion, $_POST['IDmdf']);
	$data_mdf = array(
		'abv'	=> $array_mdf[0]['mdf'],
		'name'	=> $array_mdf[0]['nombre'],
		'estado'=> $array_mdf[0]['estado'],
		'x' 	=> $array_mdf[0]['x'],
		'y' 	=> $array_mdf[0]['y']
	);
	
	
	//carga de datos (x,y) de Armario
	$array_armario = $obj_terminales->get_armario_by_codarmario($conexion, $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDcableArmario']);
	$data_armario = array(
		'zonal'	=> $array_armario[0]['zonal'],
		'mdf'	=> $array_armario[0]['mdf'],
		'armario'=> $array_armario[0]['armario'],
		'x' 	=> $array_armario[0]['x'],
		'y' 	=> $array_armario[0]['y'],
		'estado'=> $array_armario[0]['estado'],
	);
	
	$array_actuaciones = $obj_pen_pais->__listarActuacionesXY($conexion, $_POST['EECC'], $_POST['IDzonal'], $_POST['IDmdf'], $_POST['IDtipred'], $_POST['IDcableArmario'], $_POST['datosXY']);

	$data_markers = array();
	$data_markers_con_xy = array();
	$i = 1;
	//foreach ($array_actuaciones as $obj_actuaciones) {
	foreach ($array_actuaciones as $actuaciones) {
	
		if( $actuaciones['y'] != '' && $actuaciones['x'] != '' ) {
			$data_markers_con_xy[] = $actuaciones;
		}
	
		/*
		if( $obj_actuaciones->__get('ordenada_y') != '' && $obj_actuaciones->__get('ordenada_x') != '' ) {
			$data_markers_con_xy[] = array(
				'negocio' 	=> $obj_actuaciones->__get('negocio'),
				'averia' 	=> $obj_actuaciones->__get('averia'),
				'telefono' 	=> $obj_actuaciones->__get('telefono'),
				'cliente' 	=> $obj_actuaciones->__get('cliente'),
				'zonal'		=> $obj_actuaciones->__get('zonal'),
				'mdf'		=> $obj_actuaciones->__get('mdf'),
				'carmario'	=> $obj_actuaciones->__get('carmario'),
				'ccable'	=> $obj_actuaciones->__get('ccable'),
				'tipo_red'	=> $obj_actuaciones->__get('tipo_red'),
				'caja'		=> $obj_actuaciones->__get('caja'),
				'x' 		=> $obj_actuaciones->__get('ordenada_x'),
				'y' 		=> $obj_actuaciones->__get('ordenada_y')
			);
		}
		*/

		$data_markers[] = $actuaciones;
		
		/*
		$data_markers[] = array(
			'negocio' 	=> $obj_actuaciones->__get('negocio'),
			'averia' 	=> $obj_actuaciones->__get('averia'),
			'telefono' 	=> $obj_actuaciones->__get('telefono'),
			'cliente' 	=> $obj_actuaciones->__get('cliente'),
			'zonal'		=> $obj_actuaciones->__get('zonal'),
			'mdf'		=> $obj_actuaciones->__get('mdf'),
			'carmario'	=> $obj_actuaciones->__get('carmario'),
			'ccable'	=> $obj_actuaciones->__get('ccable'),
			'tipo_red'	=> $obj_actuaciones->__get('tipo_red'),
			'caja'		=> $obj_actuaciones->__get('caja'),
			'x' 		=> $obj_actuaciones->__get('ordenada_x'),
			'y' 		=> $obj_actuaciones->__get('ordenada_y')
		);
		*/
		$i++;
	}
	
	
	$poligonos_array = array();
	$prom_x = $prom_y = 0;
	// ========================================== DATA DE POLIGONOS ======================================= //
	if( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
		$obj_capasarea = new data_ffttCapasArea();

		$IDmdf = $_POST['IDmdf'];
		//seteamos 'ANU0-ANCON' porque falta actualizar el campo elemento = mdf de tabla fftt_capas_area
		
		//$IDmdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';
		
		//$IDmdf = 'ANU0-ANCON';
		
		$puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $IDmdf);
		$poligonos_array[] = array(
			'coords' => $puntosXY_mdf,
			'color'	 => genera_color_poligono()
		); 

		
		$count_xy = count($poligonos_array[0]['coords']);
		
		$sum_x = $sum_y = 0;
		foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
			$sum_x += $puntoXY['x'];
			$sum_y += $puntoXY['y'];
		}
		
		$prom_x = $sum_x / $count_xy;
		$prom_y = $sum_y / $count_xy;
		
	}
	// ========================================== FIN DATA DE POLIGONOS ======================================= //
	
	
	
	$estados_terminales_arr = array(
		array('id'=>'1', 'val'=>'Estado 1'),
		array('id'=>'2', 'val'=>'Estado 2'),
		array('id'=>'3', 'val'=>'Estado 3')
	);
	$estados__arr = array(
		'1' => 'Estado 1',
		'2' => 'Estado 2',
		'3' => 'Estado 3'
	);
	
	$data_result = array(
		'zonal' 			=> $data_zonal,
		'mdf'				=> $data_mdf,
		'armario'			=> $data_armario,
		'estados'			=> $estados_terminales_arr,
		'est'				=> $estados__arr,
		'edificios'			=> $data_markers,
		'edificios_con_xy'  => $data_markers_con_xy,
		'xy_poligono' 		=> $poligonos_array,
		'center_map'  		=> array(
			'x' => $prom_x,
			'y' => $prom_y
		)
	);
	
	echo json_encode($data_result);
    exit;
}

elseif( isset($_POST['action']) && $_POST['action'] == 'buscar' ) {

	$poligonos_array = array();
	$prom_x = $prom_y = 0;
	// ========================================== DATA DE POLIGONOS ======================================= //
	if( isset($_POST['area_mdf']) && $_POST['area_mdf'] == 'si' ) {
		$obj_capasarea = new data_ffttCapasArea();

		$IDmdf = $_POST['IDmdf'];
		//seteamos 'ANU0-ANCON' porque falta actualizar el campo elemento = mdf de tabla fftt_capas_area
		
		//$IDmdf = ( $_POST['IDmdf'] == 'ANU0' ) ? 'ANU0-ANCON' : 'CA-CALLAO';
		
		//$IDmdf = 'ANU0-ANCON';
		
		$puntosXY_mdf = $obj_capasarea->get_xys_by_tipocapa_and_elemento($conexion, 'MDF', $IDmdf);
		$poligonos_array[] = array(
			'coords' => $puntosXY_mdf,
			'color'	 => genera_color_poligono()
		); 

		
		$count_xy = count($poligonos_array[0]['coords']);
		
		$sum_x = $sum_y = 0;
		foreach( $poligonos_array[0]['coords'] as $puntoXY ) {
			$sum_x += $puntoXY['x'];
			$sum_y += $puntoXY['y'];
		}
		
		$prom_x = $sum_x / $count_xy;
		$prom_y = $sum_y / $count_xy;
		
		$data_result = array(
			'xy_poligono' => $poligonos_array,
			'center_map'  => array(
					'x' => $prom_x,
					'y' => $prom_y
			)
		);
	
		echo json_encode($data_result);
		exit;
		
	}
	// ========================================== FIN DATA DE POLIGONOS ======================================= //
	

}

else {
//include 'principal/gescomp.php';    

?>


<!-- template por default -->
<iframe id="ifrMain" name="ifrMain" src="modulos/maps/vistas/v_gescomp_actuaciones.ifr.php" width="100%" height="480"></iframe>

<?php
}
?>
