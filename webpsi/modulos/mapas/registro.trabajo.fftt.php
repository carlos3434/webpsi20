<?php
/*
 * Sistema Web Unificada
 * Planificacion y Soluciones Informaticas - Telefonica
 * Autor: Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 */ 
set_time_limit(10000);
ini_set('mysql.connect_timeout', 120);
require_once "../../config/web.config.php";
include_once APP_DIR . 'autoload.php';
include_once APP_DIR . 'session.php';

$nombre_modulo = '';
$etiqueta_titulo = ETIQUETA_TITULO . ' | Marcacion de trabajos en planta y averias masivas';

include_once APP_DIR . 'modulos/geplex/util/conversionesData.php';

$_SESSION['SUBMODULO_ACTUAL'] = 'maps/registro.trabajo.fftt.php';

class RegistroTrabajo extends ConversionesData {
    public $tipo_negocio    = '';
    public $tipo_red        = '';
    public $tipo_registro   = '';
    public $campo_busqueda  = '';
    public $cod_elemento    = '';
    public $nom_elemento    = '';
    public $cod_casuistica  = '';
    public $cod_zonal       = '';
    public $nom_zonal       = '';
    public $cod_eecc        = '';
    public $cod_jefatura    = '';
    public $cod_departamento= '';
    public $cod_provincia   = '';
    
    public $tipo_planta  = '';
    public $nodo         = '';
    public $troba        = '';
    public $amplificador = '';
    
    public function __construct() {
        if (!isset($_GET['cmd']))
            $_GET['cmd'] = '';
        
        switch ($_GET['cmd']) {
            case "getElemento_by_Negocio_and_TipoRed":
                $variables = $this->__set_by_Post($_POST);
                
                $this->getElemento_by_Negocio_and_TipoRed($variables);
                break;
            case "getMdfNodo_byZonal_Ecc_Tipo":
                $variables = $this->__set_by_Post($_POST);
                
                $this->getMdfNodo_byZonal_Ecc_Tipo($variables);
                break;
            case "getDepartamento_by_Zonal":
                $variables = $this->__set_by_Post($_POST);
                
                $this->getDepartamento_byZonal($variables);
                break;
            case "buscarElemento":
                $variables = $this->__set_by_Post($_POST);

                $this->viewBuscarElemento($variables);
                break;
            case "registroTrabajo":
                $variables = $_POST;
                
                $this->ingresoRegistroTrabajo($variables);
                break;
            case "getProvincia_by_Departamento_Zonal":
                
                $this->getProvincia_by_Departamento_Zonal($_POST['cod_zonal'],$_POST['cod_departamento']);
                break;
            case "getDistrito_byProvinciaZonalJefatura":
                
                $this->getDistrito_byProvinciaZonalJefatura($_POST['cod_zonal'],$_POST['cod_jefatura'],$_POST['cod_departamento'],$_POST['cod_provincia']);
                break;
            case "getTroba_byNodo":

                $this->getTrobaByNodo($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['nodo']);
                break;
            case "getAmplificador_byTrobaNodo":
                
                $this->getAmplificadorbyTrobaNodo($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['nodo'],$_POST['troba']);
                break;
            case "getTap_byAmplificadorTrobaNodo":
                
                $this->getTapbyAmplificadorTrobaNodo($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['nodo'],$_POST['troba'],$_POST['amplificador']);
                break;
            case "getCable_Armario_Terminal_Zonal_MDF":

                $this->getCableArmarioTerminalZonalMDF($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['tipo_red'],$_POST['mdf'],$_POST['cod_zonal']);
                break;
            case "getTerminal_by_Armario_Zonal_MDF":

                $this->getTerminalbyArmarioZonalMDF($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['tipo_red'],$_POST['mdf'],$_POST['cod_zonal'],$_POST['cablearmario']);
                break;
            case "getSecundario_by_Armario_Zonal_MDF":

                $this->getSecundariobyArmarioZonalMDF($_POST['tipo_planta'],$_POST['cod_elemento'],$_POST['tipo_red'],$_POST['mdf'],$_POST['nom_zonal'], $_POST['armario']);
                break;
            case "getDireccion_byArmario":
               
                $this->getDireccion_byArmario($_POST['cablearmario'],$_POST['mdf'],$_POST['cod_zonal']);
                break;
            default:
                $this->registromain();
                break;
        }
    }
    
    public function __set_by_Post($arreglo_post){
        foreach ($arreglo_post as $key => $value):
            $this->__set($key,$value);
        endforeach;
        return $this;
    }
    
    public function __get($propiedad) {
        $return_value = (string) '';
        $return_value = $this->$propiedad;
        return (string) $return_value;
    }

    public function __set($propiedad, $valor) {
        $this->$propiedad = $valor;
    }   
    
    public function getElemento_by_Negocio_and_TipoRed($variables) {
        try {
            global $conexion, $etiqueta_titulo, $nombre_modulo;
            
            $tipo_negocio   = $variables->__get('tipo_negocio');
            $tipo_red       = $variables->__get('tipo_red');

            $tipo_planta    = $this->change_Negocio_by_Planta($tipo_negocio);
            
            $arreglo_elemento = $this->getElemento_by_Planta_and_TipoRed($tipo_planta,$tipo_red);
            echo json_encode($arreglo_elemento);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getMdfNodo_byZonal_Ecc_Tipo($variables) {
        try {
            global $conexion, $etiqueta_titulo, $nombre_modulo;
            $tipo_negocio   = $variables->__get('tipo_negocio');
            $cod_eecc       = $variables->__get('cod_eecc');
            $cod_zonal      = $variables->__get('cod_zonal');
            $cod_jefatura   = $variables->__get('cod_jefatura');
            $tipo_planta    = $this->change_Negocio_by_Planta($tipo_negocio);
            
            $mdf_nodo = $this->getMdfNodo_byZonalEccTipo($tipo_planta,$cod_zonal,$cod_jefatura,$cod_eecc);
            echo json_encode($mdf_nodo);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
    public function getDepartamento_byZonal($variables) {
        try {
            global $conexion, $etiqueta_titulo, $nombre_modulo;
            $cod_zonal      = $variables->__get('cod_zonal');
            
            $arreglo_departamento = $this->getDepartamento_by_Zonal($cod_zonal);
            echo json_encode($arreglo_departamento);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
       
    public function viewBuscarElemento($variables) {
        try {
            global $conexion, $etiqueta_titulo, $nombre_modulo;
            $tipo_negocio   = $variables->__get('tipo_negocio');
            $tipo_registro  = $variables->__get('tipo_registro');
            $tipo_red       = $variables->__get('tipo_red');
            $cod_elemento   = $variables->__get('cod_elemento');
            $nom_elemento   = $variables->__get('nom_elemento');
            $tipo_planta    = $this->change_Negocio_by_Planta($tipo_negocio);
            
            $is_tdp     = ($_SESSION['USUARIO']->__get('idempresa')=='1')?'1':'0';
            $is_eecc    = ($_SESSION['USUARIO']->__get('idempresa')!='1')?'1':'0';

            $temp_negocio = ($tipo_negocio == 'ADSL') ? 'STB' : $tipo_negocio;

            $obj_jefatura = new data_ffttJefaturaZonales();
            
            if( $is_tdp == '1' ) {
                $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion);
            }else {
                $arreglo_jefatura = $obj_jefatura->__getJefaturaZonal_CodJefatura($conexion,'','','', $_SESSION['USUARIO']->__get('idempresa'));
            }
            
            //$obj_casuistica_pex = new data_CasuisticaPEX();
            //$arreglo_casuistica = $obj_casuistica_pex->__getCasuistica_by_Elemento_and_Negocio($conexion, $temp_negocio, $cod_elemento);
            
            $obj_casuistica = new data_ffttCasuistica();
            $arreglo_casuistica = $obj_casuistica->__list($conexion);
            
            $reestrinciones = $this->viewfiltrarIndividual($tipo_planta,$cod_elemento,$tipo_red);
            
            
//    validateObservacion();
            //include $nombre_modulo . 'view/pex/registro/frm.registro.elemento.php';
            include $nombre_modulo . 'vistas/v_registro.trabajo.fftt.elemento.php';
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    
    public function ingresoRegistroTrabajo($variables){
        try {
            global $conexion, $etiqueta_titulo, $nombre_modulo;
          
            $data = array ();
            $data['estado']             = 'PENDIENTE';
            $data['codjefatura']        = $variables['codjefatura'];
            $data['codplantaptr']       = $variables['codplantaptr'];
            $data['negocio']            = $variables['negocio'];
            $data['codeecc']            = $variables['codeecc'];
            $data['codempresa']         = $_SESSION['USUARIO']->__get('idempresa');
            $data['zonal']              = $variables['codzonal'];
            $data['coddepartamento']    = $variables['coddepartamento'];
            $data['codprovincia']       = $variables['codprovincia'];
            $data['coddistrito']        = $variables['coddistrito'];
            $data['direccion']          = (isset($variables['direccioncliente']))?$variables['direccioncliente']:''; 
            $data['idarea_usuario']     = $_SESSION['USUARIO']->__get('idarea');
            $data['idcasuistica']       = $variables['codcasuisticapex'];
            $data['codelementoptr']     = $variables['codelementoptr'];
            
            if($variables['codplantaptr'] == 'B'){ //STB/ADSL
                //para red F -> parpri = primario => cable alimentador
                //para red D -> parpri = primario => cable
                
                $data['tipored']       = $variables['tipored'];
                $data['mdf']           = $variables['mdf'];
                if( $variables['tipored'] == 'D' ) {
                    $data['carmario']  = '';
                    $data['ccable']    = (isset($variables['primario']))?$variables['primario']:''; 
                    $data['parpri']    = '';
                    $data['cbloque']   = '';
                }else if( $variables['tipored'] == 'F' ) {
                    $data['carmario']  = (isset($variables['armario']))?$variables['armario']:'';
                    $data['ccable']    = '';
                    $data['parpri']    = (isset($variables['primario']))?$variables['primario']:''; 
                    $data['cbloque']   = (isset($variables['secundario']))?$variables['secundario']:'';
                }
                $data['terminal']      = (isset($variables['terminal']))?$variables['terminal']:'';
                $data['borne']         = '';
                
            }else if($variables['codplantaptr'] == 'C'){ //CATV
                $data['tipored']       = '';
                $data['mdf']           = $variables['nodo'];
                $data['carmario']      = (isset($variables['troba']))?$variables['troba']:'';
                $data['ccable']        = '';
                $data['parpri']        = '';
                $data['cbloque']       = (isset($variables['amplificador']))?$variables['amplificador']:'';
                $data['terminal']      = (isset($variables['tap']))?$variables['tap']:'';
                $data['borne']         = (isset($variables['borne']))?$variables['borne']:'';
            }
            
            $data['pkfftt']            = $this->__getPK_FFTT($variables['codplantaptr'], $variables);
            $data['cant_reiterada']    = $this->__getCantidadReiteradas($data['pkfftt'], $variables['codplantaptr']);   
            //echo 'can=' . $data['cant_reiterada'];
            
            $f_ini = substr($variables['fecha_inicio_trabajo'],6,4) . '-' . substr($variables['fecha_inicio_trabajo'],3,2) . '-' . substr($variables['fecha_inicio_trabajo'],0,2) . ' ' . substr($variables['fecha_inicio_trabajo'],11,2) . ':' . substr($variables['fecha_inicio_trabajo'],14,2);
            $f_fin = substr($variables['fecha_fin_trabajo'],6,4) . '-' . substr($variables['fecha_fin_trabajo'],3,2) . '-' . substr($variables['fecha_fin_trabajo'],0,2) . ' ' . substr($variables['fecha_fin_trabajo'],11,2) . ':' . substr($variables['fecha_fin_trabajo'],14,2);
            
            $data['fecha_inicio_trabajo']   = $f_ini;
            $data['fecha_fin_trabajo']      = $f_fin;
            $data['observaciones']     = $variables['observaciones'];         
            $data['user_insert']       = $_SESSION['USUARIO']->__get('idusuario');     
            $data['fecha_insert']      = date('Y-m-d H:i:s');     
            
            
            $incidencia_pendiente = $this->__getIncidenciaPendientes($data['pkfftt'], $variables['codplantaptr']); 

            if( !empty($incidencia_pendiente) ) {
                $mensaje = array("flag"=>"0","mensaje"=>"Actualmente hay un trabajo ya reportado con las mismas FFTT.\n\nEl Nro de trabajo es: RTP-".$incidencia_pendiente[0]['id_registro_trabajo_planta']);
                echo json_encode($mensaje);
            }else{

                $obj_registro_trabajo = new data_RegistroTrabajoPlanta();
                $mensaje = $obj_registro_trabajo->__save($conexion, $data);
                
                echo json_encode($mensaje);
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }    
    }
    
    public function __getPK_FFTT($tipo_planta, $variables){
                
        $pkfftt = '';
        if($tipo_planta == 'B'){ //STB/ADSL
            $pkfftt .= $variables['mdf'] . '||';
            if( $variables['tipored'] == 'D' ) {
                $pkfftt .= '||';
                $pkfftt .= (isset($variables['primario'])) ? $variables['primario'] . '||' : '||'; 
                $pkfftt .= '||';
                $pkfftt .= '||';
            }else if( $variables['tipored'] == 'F' ) {
                $pkfftt .= (isset($variables['armario'])) ? $variables['armario'] . '||' : '||';
                $pkfftt .= '||';
                $pkfftt .= (isset($variables['primario'])) ? $variables['primario'] . '||' : '||'; 
                $pkfftt .= (isset($variables['secundario'])) ? $variables['secundario'] . '||' : '||';
            }
            $pkfftt .= (isset($variables['terminal'])) ? $variables['terminal'] . '||' : '||';
            $pkfftt .= '||';
            $pkfftt .= (isset($variables['codelementoptr'])) ? $variables['codelementoptr'] : '';
            
            if($variables['codelementoptr'] == '8'){
                $pkfftt .= "-".$variables['codcasuisticapex'];
            }

        }else if($tipo_planta == 'C'){ //CATV
            $pkfftt .= $variables['nodo'] . '||';
            $pkfftt .= (isset($variables['troba'])) ? $variables['troba'] . '||' : '||';
            $pkfftt .= '||';
            $pkfftt .= '||';
            $pkfftt .= (isset($variables['amplificador'])) ? $variables['amplificador'] . '||' : '||';
            $pkfftt .= (isset($variables['tap'])) ? $variables['tap'] . '||' : '||';
            $pkfftt .= (isset($variables['borne']))?$variables['borne'] . '||' : '||';
            $pkfftt .= (isset($variables['codelementoptr'])) ? $variables['codelementoptr'] : '';
            
            if($variables['codelementoptr'] == '11'){
                $pkfftt .= "-".$variables['codcasuisticapex'];
            }  
        }

        return $pkfftt;
    }
    
    
    public function __getCantidadReiteradas($pkfftt, $codplantaptr){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        $obj_registro_pex = new data_RegistroTrabajoPlanta();
        $liquidadas_pex = $obj_registro_pex->__getCantidadIncidenciaLiquidadas($conexion,$pkfftt,$codplantaptr);
        $cant_liquidadas_pex = count($liquidadas_pex);
        return $cant_liquidadas_pex;
    }
    
    
    
    public function __getIncidenciaPendientes($pkfftt,$codplantaptr){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        $obj_registro_trabajo = new data_RegistroTrabajoPlanta();
        $pendientes_pex = $obj_registro_trabajo->__getIncidenciaPendientes($conexion,$pkfftt,$codplantaptr);
        return $pendientes_pex;
        
    }
    
    public function registromain() {
        //echo 'Registro Trabajos FFtt';exit;
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        // Carga de Vista principal
        $obj_tipo_negocio       = new data_TipoNegocio();
        $arreglo_negocio_pex    = $obj_tipo_negocio->__listNegociosInnova($conexion);

        $obj_tipo_registro_pex  = new data_TipoRegistroPEX();
        $arreglo_registro_pex   = $obj_tipo_registro_pex->listarTipoRegistroPEX($conexion);
        
        $id_area    = $_SESSION['USUARIO']->__get('idarea');
        /*
         * Validacion para que solos usuarios de las areas puedan registrar
         * 41 - MANTENIMIENTO DE PLANTA EXTERNA TDP
         */
        if($id_area == '41'){ $user_permitido = '1'; }
        else{ $user_permitido = '0'; } 

        include $nombre_modulo.'vistas/v_registro.trabajo.fftt.php';
    }
    
    public function getProvincia_by_Departamento_Zonal($cod_zonal,$cod_departamento){
        global $conexion, $etiqueta_titulo, $nombre_modulo;

        $arreglo_provincia = $this->getProvincia_by_ZonalDpto($cod_zonal,$cod_departamento);
        echo json_encode($arreglo_provincia);
    }
    
    public function getDistrito_byProvinciaZonalJefatura($cod_zonal,$cod_jefatura,$cod_departamento,$cod_provincia){
        global $conexion, $etiqueta_titulo, $nombre_modulo;

        $arreglo_distrito = $this->getDistrito_by_ZonalJefaturaProvincia($cod_zonal,$cod_jefatura,$cod_departamento,$cod_provincia);
        echo json_encode($arreglo_distrito);
    }
    
    public function getTrobaByNodo($tipo_planta,$cod_elemento,$nodo){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_Trobas = $this->getTrobas_by_Nodo($tipo_planta,$nodo);
        echo json_encode($arreglo_Trobas);
    }
    
    public function getAmplificadorbyTrobaNodo($tipo_planta,$cod_elemento,$nodo,$troba){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_Amplificar = $this->getAmplificador_by_Nodo_Troba($tipo_planta,$nodo,$troba);
        echo json_encode($arreglo_Amplificar);
    }
    
    public function getTapbyAmplificadorTrobaNodo($tipo_planta,$cod_elemento,$nodo,$troba,$amplificador){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_Tap = $this->getTap_by_Nodo_Troba_Amplificador($tipo_planta,$nodo,$troba,$amplificador);
        echo json_encode($arreglo_Tap);
    }
    
    public function getCableArmarioTerminalZonalMDF($tipo_planta,$cod_elemento,$tipo_red,$mdf,$cod_zonal){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_CableArmarioTerminal = $this->getCable_Armario_Terminal_Zonal_MDF($tipo_planta, $cod_elemento, $tipo_red, $mdf, $cod_zonal);
        echo json_encode($arreglo_CableArmarioTerminal);
    }
    
    public function getTerminalbyArmarioZonalMDF($tipo_planta,$cod_elemento,$tipo_red,$mdf,$cod_zonal,$cableArmario){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_TerminalByArmario = $this->getTerminal_by_Armario_Zonal_MDF($tipo_planta, $cod_elemento, $tipo_red, $mdf, $cod_zonal, $cableArmario);
        echo json_encode($arreglo_TerminalByArmario);
    }
    
    public function getSecundariobyArmarioZonalMDF($tipo_planta,$cod_elemento,$tipo_red,$mdf,$nom_zonal,$cablearmario){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $tipo_planta   = $this->change_Negocio_by_Planta('',$tipo_planta);
        $arreglo_SecundarioByArmario = $this->getSecundario_by_Armario_Zonal_MDF($tipo_planta, $cod_elemento, $tipo_red, $mdf, $nom_zonal, $cablearmario);
        echo json_encode($arreglo_SecundarioByArmario);
    }
    
    public function getDireccion_byArmario($cablearmario,$mdf,$cod_zonal){
        global $conexion, $etiqueta_titulo, $nombre_modulo;
        
        $obj_fftt_terminales = new data_ffttTerminales();
        $arreglo_direccion_armario = $obj_fftt_terminales->get_armario_by_codarmario($conexion, $cod_zonal, $mdf, $cablearmario);
        $direccion = $arreglo_direccion_armario[0]['direccion'];
        $dpto = $arreglo_direccion_armario[0]['coddpto'];
        $prov = $arreglo_direccion_armario[0]['codprov'];
        $dist = $arreglo_direccion_armario[0]['coddist'];
        
        $ubigeo = new data_TipoRegistroPEX();
        $arr_prov = $ubigeo->get_provincia_by_coddpto($conexion, $dpto,$prov);
        $arr_dist = $ubigeo->get_distrito_by_prov($conexion, $dpto,$prov,$dist);
        
        $nomprov = (isset($arr_prov[0]['nombre']))?$arr_prov[0]['nombre']:'';
        $nomdist = (isset($arr_dist[0]['nombre']))?$arr_dist[0]['nombre']:'';
        
        $responce = array('direccion'=>$direccion,
                          'dpto'=>$dpto,
                          'nomdpto'=>'',
                          'prov'=>$prov,
                          'nomprov'=>$nomprov,
                          'dist'=>$dist,
                          'nomdist'=>$nomdist);
        echo json_encode($responce);
    }
    
}

$obj = new RegistroTrabajo();

?>

