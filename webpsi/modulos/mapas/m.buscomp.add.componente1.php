<?php
require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

//$obj_capas = new data_ffttCapas();
	
$array_capas = $_SESSION['USUARIO_CAPAS'];

$is_login = 0;
$dir_modulo = '../../';



if( isset($_POST['action']) && $_POST['action'] == 'buscarProv' ) {

	$obj_capas = new data_ffttCapas();

	$array_prov = $obj_capas->get_provincias_by_dpto($conexion, $_POST['IDdpto']);
	
	$data_arr = array();
	foreach ($array_prov as $prov) {
    	$data_arr[] = array(
    		'codprov' 	=> $prov['codprov'],
    		'nombre'	=> $prov['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;

}
elseif( isset($_POST['action']) && $_POST['action'] == 'buscarDist' ) {

	$obj_capas = new data_ffttCapas();

	$array_dist = $obj_capas->get_distritos_by_prov($conexion, $_POST['IDdpto'], $_POST['IDprov']);
	
	$data_arr = array();
	foreach ($array_dist as $dist) {
    	$data_arr[] = array(
    		'coddist' 	=> $dist['coddist'],
    		'nombre'	=> $dist['nombre']
    	);
	}
	echo json_encode($data_arr);
	exit;

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaEdificio' ) {
	
	$etapa_arr = array(
		'A' => 'A',
		'B' => 'B',
		'C' => 'C',
		'D' => 'D',
		'E' => 'E',
		'F' => 'F',
		'G' => 'G',
		'H' => 'H',
		'I' => 'I',
		'J' => 'J',
		'K' => 'K',
		'L' => 'L',
		'M' => 'M',
		'N' => 'N',
		'O' => 'O',
		'P' => 'P',
		'Q' => 'Q',
		'R' => 'R',
		'S' => 'S',
		'T' => 'T',
		'U' => 'U',
		'V' => 'V',
		'W' => 'W',
		'X' => 'X',
		'Y' => 'Y',
		'Z' => 'Z'
	);
	

	$obj_capas = new data_ffttCapas();

	//formateando fecha para insert a db
	$_POST['fecha_registro_proyecto'] 	 = ( $_POST['fecha_registro_proyecto'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_registro_proyecto'])) : '0000-00-00 00:00:00';
	$_POST['fecha_termino_construccion'] = ( $_POST['fecha_termino_construccion'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_termino_construccion'])) : '0000-00-00 00:00:00';
	$_POST['fecha_seguimiento'] 		 = ( $_POST['fecha_seguimiento'] != '' ) ? date('Y-m-d', strtotime($_POST['fecha_seguimiento'])) : '0000-00-00 00:00:00';
	
	$_POST['montante_fecha'] 			= ( $_POST['montante_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['montante_fecha'])) : '0000-00-00 00:00:00';
	$_POST['ducteria_interna_fecha'] 	= ( $_POST['ducteria_interna_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['ducteria_interna_fecha'])) : '0000-00-00 00:00:00';
	$_POST['punto_energia_fecha'] 		= ( $_POST['punto_energia_fecha'] != '' ) ? date('Y-m-d', strtotime($_POST['punto_energia_fecha'])) : '0000-00-00 00:00:00';
	
	//formateando distrito
	//$_POST['distrito'] = $_POST['selDpto'] . $_POST['selProv'] . $_POST['selDist'];
	
	$cod_existe_autogenerado = $obj_capas->get_existe_codigo_autogenerado_edificios($conexion);
	if( empty($cod_existe_autogenerado) ) {
		//item por default para inicio de ingreso de nuevos edificios
		//$item = 'X10001';
		$item = '10001';
	}else {
		/*
		$item_str = substr($cod_existe_autogenerado[0]['max_item'], 1);
		$item 	  = (int)$item_str + 1;
		$item	  = 'X' . $item;
		*/
	
		$etapa_found 	= substr($cod_existe_autogenerado[0]['max_item'], -1, 1);
		if( array_key_exists($etapa_found, $etapa_arr) ) {
			$item 	= substr($cod_existe_autogenerado[0]['max_item'], 0, -1);
			$etapa 	= substr($cod_existe_autogenerado[0]['max_item'], -1, 1);
		}else {
			$item 	= $cod_existe_autogenerado[0]['max_item'];
			$etapa 	= '';
		}

	}
	
	$_POST['fecha']	= date('Y-m-d');
	$_POST['item']	= $item + 1;
	

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$lastID = $obj_capas->__insertEdificio($conexion, $_POST, $idusuario);

	if( $lastID ) {
		$result = array('success'=>1,'msg'=>'Datos grabados correctamente.');
		
		if( $_POST['archivo1'] != '' || $_POST['archivo2'] != '' || $_POST['archivo3'] != '' || $_POST['archivo3'] != '' ) {
		
			$_POST['idedificio'] = $lastID;
			$data_result = $obj_capas->__insertEdificioFoto($conexion, $_POST, $idusuario);
	
			$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');
		}
		
	}else {
		$result = array('success'=>0,'msg'=>'Error al grabar los datos.');
	}
	
	echo json_encode($result);
	exit;

}
elseif( isset($_REQUEST['action']) && $_REQUEST['action'] == 'GrabaCompetencia' ) {

	$obj_capas = new data_ffttCapas();

	$idusuario = $_SESSION['USUARIO']->__get('idusuario');
	
	$data_result = $obj_capas->__insertCompetencia($conexion, $_POST, $idusuario);

	$result = ($data_result) ? array('success'=>1,'msg'=>'Datos grabados correctamente.') : array('success'=>0,'msg'=>'Error al grabar los datos.');

	echo json_encode($result);
	exit;
	
}



if( $_REQUEST['capa'] == 'edi' ) {

	$meses_arr = array(
		'01' => 'Enero',
		'02' => 'Febrero',
		'03' => 'Marzo',
		'04' => 'Abril',
		'05' => 'Mayo',
		'06' => 'Junio',
		'07' => 'Julio',
		'08' => 'Agosto',
		'09' => 'Setiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre'
	);

	$ano_actual = date('Y');
	$mes_actual = date('m');
	$dia_actual = date('d');

	$obj_capas = new data_ffttCapas();
	$obj_terminales = new data_ffttTerminales();

	$estado_arr = array(
		'I' => 'Iniciado',
		'P' => 'En proceso',
		'T' => 'Terminado'
	);

	$mdfs_array = $obj_terminales->get_mdfs_by_xy($conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5);
		
	//opciones multiple -> edificio
	$tipo_cchh_list 			= $obj_capas->get_descriptor_by_grupo($conexion, 1);
	$tipo_proyecto_list 		= $obj_capas->get_descriptor_by_grupo($conexion, 2);
	$tipo_infraestructura_list 	= $obj_capas->get_descriptor_by_grupo($conexion, 3);
	$tipo_via_list 				= $obj_capas->get_descriptor_by_grupo($conexion, 4);

	$dpto_list  = $obj_capas->get_departamentos($conexion);
	
	include 'vistas/m.v_buscomp.add.componente.php'; 

}
elseif( $_REQUEST['capa'] == 'com' ) {

	$obj_capas = new data_ffttCapas();
	$obj_terminales = new data_ffttTerminales();

	//opciones multiple -> competencia
	$competencia_list 			= $obj_capas->get_descriptor_by_grupo($conexion, 5);
	$elemento_encontrado_list 	= $obj_capas->get_descriptor_by_grupo($conexion, 6);
	
	$mdfs_array = $obj_terminales->get_mdfs_by_xy($conexion, $_REQUEST['x'], $_REQUEST['y'], 10, 5);

	include 'vistas/m.v_buscomp.add.componente.php'; 

}






