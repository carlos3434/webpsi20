<?php

/**
 * @package     /modulos/maps
 * @name        m.buscomp.add.componente.php
 * @category    Controller
 * @author      Wilder Sandoval Huamani <wsandoval@gmd.com.pe>
 * @copyright   2011 Telefonica del Peru
 * @version     1.0 - 2011/11/25
 */

require_once "../../config/web.config.php";
require_once APP_DIR . 'autoload.php';
require_once APP_DIR . 'session.php';

require_once APP_DIR . 'modulos/maps/util/functions.maps.php';


$isLogin = 0;
$dirModulo = '../../';


function cargaDefault()
{
    echo 'Default';
    exit;
}

function nuevoEdificio()
{
    try {
        global $conexion, $isLogin, $dirModulo;
        
        $isLogin = 0;
        $dirModulo = '../../';

        //variables para el formulario editar
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

        $objEdificio           = new Data_FfttEdificio();
        $objEdificioEstado     = new Data_FfttEdificioEstado();
        $objCapas              = new Data_FfttCapas();
        $objTerminales         = new Data_FfttTerminales();
        $objUbigeo             = new Data_Ubigeo();
        $objUbigeoZonal        = new Data_UbigeoZonalEdi();
        $objGestionObra        = new Data_GestionObraEdi();
        $objRegion             = new Data_RegionEdi();
        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objSegmento           = new Data_SegmentoEdi();
        $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
        $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
        $objGestionCompetencia = new Data_GestionCompetenciaEdi();
        $objCoberturaMovil     = new Data_CoberturaMovilEdi();
        $objFacilidadPoste     = new Data_FacilidadPosteEdi();
        $objResponsableCampo   = new Data_ResponsableCampoEdi();

        $objTipoCchh     = new Data_TipoCchhEdi();
        $objTipoProyecto = new Data_TipoProyectoEdi();
        $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
        $objTipoVia      = new Data_TipoViaEdi();

        //lista de estados de edificios
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );

        //seteamos valores por default
        //$idGestionObra = 1; //No
        $idGestionObra = '';
        //$codEstado = '01'; //No iniciado
        $codEstado = '';
        $megaproyecto = '';
        $validacionCall = '';
        $fechaTratamientoCall = '';


        //lista de estados de gestion de obra
        $arrObjGestionObra = $objGestionObra->listar($conexion);

        //obtengo las regiones a partir de las zonales que tiene asignado
        //el usuario
        $arrRegion = listarRegionesPorZonalUsuario();

        //lista de fuentes de identificacion
        $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listar(
            $conexion
        );

        //lista de segmentos
        $arrObjSegmento = $objSegmento->listar($conexion);

        //lista de Tipo Cchh
        $arrObjTipoCchh = $objTipoCchh->listar($conexion);

        //lista de Tipo de via
        $arrObjTipoVia = $objTipoVia->listar($conexion);

        //lista de Tipo de infraestructura
        $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($conexion);

        //lista de niveles socioeconomicos
        $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($conexion);

        //lista de zona de competencia
        $arrObjZonaCompetencia = $objZonaCompetencia->listar($conexion);

        //lista de gestion de competencia
        $arrObjGestionCompetencia = $objGestionCompetencia->listar($conexion);

        //lista cobertura movil 2G
        $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($conexion);

        //lista cobertura movil 3G
        $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($conexion);

        //lista facilidad de poste
        $arrObjFacilidadPoste = $objFacilidadPoste->listar($conexion);

        //lista de responsable de campo
        $arrObjResponsableCampo = $objResponsableCampo->listar($conexion);


        //seteamos zonal por default
        $codZonal = '';
        if ( !empty($_SESSION['USUARIO_ZONAL']) ) {
            $codZonal  = $_SESSION['USUARIO_ZONAL'][0]->__get('_abvZonal');
        }

        //lista de departamentos
        //$arrObjDepartamento   = $objUbigeo->departamentosUbigeo($conexion);
        $arrObjDepartamento = $objUbigeoZonal->traerDepartamentosZonal(
            $conexion, $codZonal
        );

        include 'vistas/m.v_edificio.nuevo.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo en unos minutos.';
        exit;
    }
}

function editarEdificio ($idEdificio)
{
    try {
        global $conexion, $isLogin, $dirModulo;

        //$isLogin = 0;
        //$dirModulo = '../../';

        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

        $objEdificio = new Data_FfttEdificio();

        //capturo datos del edificio
        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idEdificio);

        if ( empty($arrObjEdificio) ) {
            include 'vistas/m.v_edificio.editar.php';
            exit;
        }
        
        
        //validacion de pertenencia del edificio
        $perteneceEmpresa = 0;
        if ( empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
            throw new PDOException();
        } else {
            foreach ($_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa) {
                if ( $arrObjEdificio[0]->__get('_idEmpresa') == $objEmpresa->__get('_idEmpresa') ) {
                    $perteneceEmpresa = 1;
                    break;
                }
            }
        }
        
        if ( !$perteneceEmpresa ) {
            detalleEdificio($idEdificio);
            exit;
        }
        //FIN validacion de pertenencia del edificio

        
        //Etapas para proyecto Edificios [A-Z]
        $arrEtapaProyecto = array(
            'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E',
            'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I', 'J' => 'J',
            'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N', 'O' => 'O',
            'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T',
            'U' => 'U', 'V' => 'V', 'W' => 'W', 'X' => 'X', 'Y' => 'Y',
            'Z' => 'Z'
        );

        $objEdificioEstado     = new Data_FfttEdificioEstado();
        $objCapas              = new Data_FfttCapas();
        $objTerminales         = new Data_FfttTerminales();
        $objUbigeo             = new Data_Ubigeo();
        $objUbigeoZonal        = new Data_UbigeoZonalEdi();
        $objGestionObra        = new Data_GestionObraEdi();
        $objRegion             = new Data_RegionEdi();
        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objSegmento           = new Data_SegmentoEdi();
        $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
        $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
        $objGestionCompetencia = new Data_GestionCompetenciaEdi();
        $objCoberturaMovil     = new Data_CoberturaMovilEdi();
        $objFacilidadPoste     = new Data_FacilidadPosteEdi();
        $objResponsableCampo   = new Data_ResponsableCampoEdi();
        $objRegionUbigeo       = new Data_RegionUbigeoEdi();


        $objTipoCchh     = new Data_TipoCchhEdi();
        $objTipoProyecto = new Data_TipoProyectoEdi();
        $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
        $objTipoVia      = new Data_TipoViaEdi();

        //lista de estados de edificios
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );


        //capturo item y etapa del edificio
        $item  = $arrObjEdificio[0]->__get('_item');
        $etapa = '';
        $etapaFound = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
        if ( array_key_exists($etapaFound, $arrEtapaProyecto) ) {
            $item  = substr($arrObjEdificio[0]->__get('_item'), 0, -1);
            $etapa = substr($arrObjEdificio[0]->__get('_item'), -1, 1);
        }

        //lista de estados de gestion de obra
        $arrObjGestionObra = $objGestionObra->listar($conexion);

        //obtengo las regiones a partir de las zonales que tiene asignado
        //el usuario
        $arrRegion = listarRegionesPorZonalUsuario();

        //lista de fuentes de identificacion
        $arrObjFuenteIdentificacion = array();
        if ( $arrObjEdificio[0]->__get('_idRegion') != '' ) {
            $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listarPorRegion(
                $conexion, $arrObjEdificio[0]->__get('_idRegion')
            );
        }

        //lista de segmentos
        $arrObjSegmento = $objSegmento->listar($conexion);

        //lista de Tipo Cchh
        $arrObjTipoCchh = $objTipoCchh->listar($conexion);

        //lista de Tipo Proyecto
        $idSegmento = ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) ?
        $arrObjEdificio[0]->__get('_idSegmento') : 1;
        $arrObjTipoProyecto = $objTipoProyecto->listar(
            $conexion, '', $idSegmento
        );

        //lista de Tipo de via
        $arrObjTipoVia = $objTipoVia->listar($conexion);

        //lista de Tipo de infraestructura
        $arrObjTipoInfraestructura = $objTipoInfraestructura->listar($conexion);

        //lista de niveles socioeconomicos
        $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar($conexion);

        //lista de zona de competencia
        $arrObjZonaCompetencia = $objZonaCompetencia->listar($conexion);

        //lista de gestion de competencia
        $arrObjGestionCompetencia = $objGestionCompetencia->listar($conexion);

        //lista cobertura movil 2G
        $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G($conexion);

        //lista cobertura movil 3G
        $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G($conexion);

        //lista facilidad de poste
        $arrObjFacilidadPoste = $objFacilidadPoste->listar($conexion);

        //lista de responsable de campo
        $arrObjResponsableCampo = $objResponsableCampo->listar($conexion);



        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $arrObjEdificio[0]->__get('_zonal'), $idUsuario
        );


        $idRegion = $arrObjEdificio[0]->__get('_idRegion');
        $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
        $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
        $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
        $codLocalidad    = $arrObjEdificio[0]->__get('_localidad');

        //lista de departamentos
        $arrObjDepartamento = array();
        if ( $idRegion != '' ) {
            $arrObjDepartamento = $objRegionUbigeo->traerDepartamentosRegion(
                $conexion, $idRegion
            );
        }
        //lista de provincias
        $arrObjProvincia = array();
        if ( $codDepartamento != '' ) {
            $arrObjProvincia = $objRegionUbigeo->traerProvinciasRegion(
                $conexion, $idRegion, $codDepartamento
            );
        }
        //lista de distritos
        $arrObjDistrito = array();
        if ( $codDepartamento != '' && $codProvincia != '' ) {
            $arrObjDistrito = $objRegionUbigeo->traerDistritosRegion(
                $conexion, $idRegion, $codDepartamento, $codProvincia
            );
        }
        //lista de localidades
        $arrObjLocalidad = array();
        if ( $codDepartamento != '' && $codProvincia != '' &&
                $codDistrito != '' ) {
            $arrObjLocalidad = $objUbigeo->buscarLocalidad(
                $conexion, $codDepartamento, $codProvincia, $codDistrito
            );
        }
        
        
        //Capturando la zonal a la que pertenece el usuario        
        $idZonal = $_SESSION['USUARIO']->__get('_idZonal');
  
        //carga de datos (x,y) de Zonal
        $objZonal = new Data_Zonal();
        $arrZonal = $objZonal->listar($conexion, $idZonal);
        $arrDataZonal = array();
        if ( !empty($arrZonal) ) {
            $arrDataZonal[] = array(
                'abv'   => $arrZonal[0]->__get('_abvZonal'),
                'name'  => $arrZonal[0]->__get('_descZonal'),
                'x'     => $arrZonal[0]->__get('_x'),
                'y'     => $arrZonal[0]->__get('_y')
            );
        }
        $jsonZonal = json_encode($arrDataZonal);
        //FIN Capturando la zonal a la que pertenece el usuario
        

        include 'vistas/m.v_edificio.editar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                'en unos minutos.';
        exit;
    }
}

function detalleEdificio($idEdificio)
{
    try {
        global $conexion, $isLogin, $dirModulo;

        //Etapas para proyecto Edificios [A-Z]
        $arrEtapaProyecto = array(
            'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E',
            'F' => 'F', 'G' => 'G', 'H' => 'H', 'I' => 'I', 'J' => 'J',
            'K' => 'K', 'L' => 'L', 'M' => 'M', 'N' => 'N', 'O' => 'O',
            'P' => 'P', 'Q' => 'Q', 'R' => 'R', 'S' => 'S', 'T' => 'T',
            'U' => 'U', 'V' => 'V', 'W' => 'W', 'X' => 'X', 'Y' => 'Y',
            'Z' => 'Z'
        );

        //variables para el formulario editar
        $idUsuario = $_SESSION['USUARIO']->__get('_idUsuario');

        $objEdificio           = new Data_FfttEdificio();

        //capturo datos del edificio
        $arrObjEdificio = $objEdificio->obtenerEdificio($conexion, $idEdificio);

        if ( empty($arrObjEdificio) ) {
            throw new PDOException();
        }


        $objEdificioEstado     = new Data_FfttEdificioEstado();
        $objCapas              = new Data_FfttCapas();
        $objTerminales         = new Data_FfttTerminales();
        $objUbigeo             = new Data_Ubigeo();
        $objUbigeoZonal        = new Data_UbigeoZonalEdi();
        $objGestionObra        = new Data_GestionObraEdi();
        $objRegion             = new Data_RegionEdi();
        $objFuenteIdentificacion = new Data_FuenteIdentificacionEdi();
        $objSegmento           = new Data_SegmentoEdi();
        $objNivelSocioeconomico = new Data_NivelSocioeconomicoEdi();
        $objZonaCompetencia    = new Data_ZonaCompetenciaEdi();
        $objGestionCompetencia = new Data_GestionCompetenciaEdi();
        $objCoberturaMovil     = new Data_CoberturaMovilEdi();
        $objFacilidadPoste     = new Data_FacilidadPosteEdi();
        $objResponsableCampo   = new Data_ResponsableCampoEdi();

        $objTipoCchh     = new Data_TipoCchhEdi();
        $objTipoProyecto = new Data_TipoProyectoEdi();
        $objTipoInfraestructura = new Data_TipoInfraestructuraEdi();
        $objTipoVia      = new Data_TipoViaEdi();


        //lista de estados de gestion de obra
        $gestionObra = '';
        if ( $arrObjEdificio[0]->__get('_idGestionObra') ) {
            $arrObjGestionObra = $objGestionObra->listar(
                $conexion, $arrObjEdificio[0]->__get('_idGestionObra')
            );
            if ( !empty($arrObjGestionObra) ) {
                $gestionObra = $arrObjGestionObra[0]->__get('_desGestionObra');
            }
        }


        //lista de estados de edificios
        $estadoEdificio = '';
        if ( $arrObjEdificio[0]->__get('_estado') != '' ) {
            $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
                $conexion, $arrObjEdificio[0]->__get('_estado')
            );
            if ( !empty($arrObjEdificioEstado) ) {
                $estadoEdificio = $arrObjEdificioEstado[0]->__get('_desEstado');
            }
        }


        //obtengo las regiones a partir de las zonales que tiene asignado
        //el usuario
        $arrRegion = listarRegionesPorZonalUsuario();

        //lista de fuentes de identificacion
        $fuenteIdentificacion = '';
        if ( $arrObjEdificio[0]->__get('_idFuenteIdentificacion') != '' ) {
            $arrObjFuenteIdentificacion = $objFuenteIdentificacion->listar(
                $conexion, $arrObjEdificio[0]->__get('_idFuenteIdentificacion')
            );
            if ( !empty($arrObjFuenteIdentificacion) ) {
                $fuenteIdentificacion = $arrObjFuenteIdentificacion[0]->__get('_desFuenteIdentificacion');
            }
        }

        //lista de segmentos
        $segmento = '';
        if ( $arrObjEdificio[0]->__get('_idSegmento') != '' ) {
            $arrObjSegmento = $objSegmento->listar(
                $conexion, $arrObjEdificio[0]->__get('_idSegmento')
            );
            if ( !empty($arrObjSegmento) ) {
                $segmento = $arrObjSegmento[0]->__get('_desSegmento');
            }
        }

        //lista de Tipo Cchh
        $tipoCchh = '';
        if ( $arrObjEdificio[0]->__get('_idTipoCchh') != '' ) {
            $arrObjTipoCchh = $objTipoCchh->listar(
                $conexion, $arrObjEdificio[0]->__get('_idTipoCchh')
            );
            if ( !empty($arrObjTipoCchh) ) {
                $tipoCchh = $arrObjTipoCchh[0]->__get('_desTipoCchh');
            }
        }

        //lista de Tipo Proyecto
        $tipoProyecto = '';
        if ( $arrObjEdificio[0]->__get('_idTipoProyecto') != '' ) {
            $arrObjTipoProyecto = $objTipoProyecto->listar(
                $conexion, $arrObjEdificio[0]->__get('_idTipoProyecto')
            );
            if ( !empty($arrObjTipoProyecto) ) {
                $tipoProyecto = $arrObjTipoProyecto[0]->__get('_desTipoProyecto');
            }
        }

        //lista de Tipo de via
        $tipoVia = '';
        if ( $arrObjEdificio[0]->__get('_idTipoVia')!= '' ) {
            $arrObjTipoVia = $objTipoVia->listar(
                $conexion, $arrObjEdificio[0]->__get('_idTipoVia')
            );
            if ( !empty($arrObjTipoVia) ) {
                $tipoVia = $arrObjTipoVia[0]->__get('_desTipoVia');
            }
        }

        //lista de Tipo de infraestructura
        $tipoInfraestructura = '';
        if ( $arrObjEdificio[0]->__get('_idTipoInfraestructura') != '' ) {
            $arrObjTipoInfraestructura = $objTipoInfraestructura->listar(
                $conexion, $arrObjEdificio[0]->__get('_idTipoInfraestructura')
            );
            if ( !empty($arrObjTipoInfraestructura) ) {
                $tipoInfraestructura = $arrObjTipoInfraestructura[0]->__get('_desTipoInfraestructura');
            }
        }

        //lista de niveles socioeconomicos
        $nivelSocioeconomico = '';
        if ( $arrObjEdificio[0]->__get('_idNivelSocioeconomico') != '' ) {
            $arrObjNivelSocioeconomico = $objNivelSocioeconomico->listar(
                $conexion, $arrObjEdificio[0]->__get('_idNivelSocioeconomico')
            );
            if ( !empty($arrObjNivelSocioeconomico) ) {
                $nivelSocioeconomico = $arrObjNivelSocioeconomico[0]->__get('_desNivelSocioeconomico');
            }
        }

        //lista de zona de competencia
        $zonaCompetencia = '';
        if ( $arrObjEdificio[0]->__get('_idZonaCompetencia') != '' ) {
            $arrObjZonaCompetencia = $objZonaCompetencia->listar(
                $conexion, $arrObjEdificio[0]->__get('_idZonaCompetencia')
            );
            if ( !empty($arrObjZonaCompetencia) ) {
                $zonaCompetencia = $arrObjZonaCompetencia[0]->__get('_desZonaCompetencia');
            }
        }

        //lista de gestion de competencia
        $gestionCompetencia = '';
        if ( $arrObjEdificio[0]->__get('_idGestionCompetencia') != '' ) {
            $arrObjGestionCompetencia = $objGestionCompetencia->listar(
                $conexion, $arrObjEdificio[0]->__get('_idGestionCompetencia')
            );
            if ( !empty($arrObjGestionCompetencia) ) {
                $gestionCompetencia = $arrObjGestionCompetencia[0]->__get('_desGestionCompetencia');
            }
        }

        //lista cobertura movil 2G
        $coberturaMovil2g = '';
        if ( $arrObjEdificio[0]->__get('_idCoberturaMovil2G') != '' ) {
            $arrObjCoberturaMovil2G = $objCoberturaMovil->listar2G(
                $conexion, $arrObjEdificio[0]->__get('_idCoberturaMovil2G')
            );
            if ( !empty($arrObjCoberturaMovil2G) ) {
                $coberturaMovil2g = $arrObjCoberturaMovil2G[0]->__get('_desCoberturaMovil2G');
            }
        }

        //lista cobertura movil 3G
        $coberturaMovil3g = '';
        if ( $arrObjEdificio[0]->__get('_idCoberturaMovil3G') != '' ) {
            $arrObjCoberturaMovil3G = $objCoberturaMovil->listar3G(
                $conexion, $arrObjEdificio[0]->__get('_idCoberturaMovil3G')
            );
            if ( !empty($arrObjCoberturaMovil3G) ) {
                $coberturaMovil3g = $arrObjCoberturaMovil3G[0]->__get('_desCoberturaMovil3G');
            }
        }

        //lista facilidad de poste
        $facilidadPoste = '';
        if ( $arrObjEdificio[0]->__get('_idFacilidadPoste') != '' ) {
            $arrObjFacilidadPoste = $objFacilidadPoste->listar(
                $conexion, $arrObjEdificio[0]->__get('_idFacilidadPoste')
            );
            if ( !empty($arrObjFacilidadPoste) ) {
                $facilidadPoste = $arrObjFacilidadPoste[0]->__get('_desFacilidadPoste');
            }
        }

        //lista de responsable de campo
        $responsableCampo = '';
        if ( $arrObjEdificio[0]->__get('_idResponsableCampo') != '' ) {
            $arrObjResponsableCampo = $objResponsableCampo->listar(
                $conexion, $arrObjEdificio[0]->__get('_idResponsableCampo')
            );
            if ( !empty($arrObjResponsableCampo) ) {
                $responsableCampo = $arrObjResponsableCampo[0]->__get('_desResponsableCampo');
            }
        }



        //lista de mdfs por zonal
        $arrMdf = $objTerminales->getMdfByZonalAndIdusuario(
            $conexion, $arrObjEdificio[0]->__get('_zonal'), $idUsuario
        );


        $codDepartamento = substr($arrObjEdificio[0]->__get('_distrito'), 0, 2);
        $codProvincia    = substr($arrObjEdificio[0]->__get('_distrito'), 2, 2);
        $codDistrito     = substr($arrObjEdificio[0]->__get('_distrito'), 4, 2);
        $codLocalidad    = $arrObjEdificio[0]->__get('_localidad');

        //lista de departamentos
        $departamento = '';
        if ( $codDepartamento != '' ) {
            $arrObjDepartamento = $objUbigeo->departamentosUbigeo(
                $conexion, $codDepartamento
            );
            if ( !empty($arrObjDepartamento) ) {
                $departamento = $arrObjDepartamento[0]->__get('_nombre');
            }
        }

        //lista de provincias
        $provincia = '';
        if ( $codProvincia != '' ) {
            $arrObjProvincia = $objUbigeo->provinciasUbigeo(
                $conexion, $codDepartamento, $codProvincia
            );
            if ( !empty($arrObjProvincia) ) {
                $provincia = $arrObjProvincia[0]->__get('_nombre');
            }
        }

        //lista de distritos
        $distrito = '';
        if ( $codDistrito != '' ) {
            $arrObjDistrito = $objUbigeo->distritosUbigeo(
                $conexion, $codDepartamento, $codProvincia, $codDistrito
            );
            if ( !empty($arrObjDistrito) ) {
                $distrito = $arrObjDistrito[0]->__get('_nombre');
            }
        }

        //lista de localidades
        $localidad = '';
        if ( $codLocalidad != '' ) {
            $arrObjLocalidad = $objUbigeo->buscarLocalidad(
                $conexion, $codDepartamento, $codProvincia, $codDistrito
            );
            if ( !empty($arrObjLocalidad) ) {
                foreach ( $arrObjLocalidad as $objLocalidad ) {
                    if ($codLocalidad == $objLocalidad->__get('_ubigeo')) {
                        $localidad = $objLocalidad->__get('_localidad');
                        break;
                    }
                }
            }
        }


        //validacion de pertenencia del edificio
        $perteneceEmpresa = 0;
        if ( empty($_SESSION['USUARIO_EMPRESA_DETALLE']) ) {
            throw new PDOException();
        } else {
            foreach ($_SESSION['USUARIO_EMPRESA_DETALLE'] as $objEmpresa) {
                if ( $arrObjEdificio[0]->__get('_idEmpresa') == $objEmpresa->__get('_idEmpresa') ) {
                    $perteneceEmpresa = 1;
                    break;
                }
            }
        }
        //FIN validacion de pertenencia del edificio
        
        
        include 'vistas/m.v_edificio.detalle.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }
}

function adjuntarFotosEdificio($idEdificio)
{
    try {
        global $conexion, $isLogin, $dirModulo;

        $objEdificio = new Data_FfttEdificio();
        $objTipoVia  = new Data_TipoViaEdi();
        $objEdificioEstado = new Data_FfttEdificioEstado();

        $arrObjEdificio = $objEdificio->obtenerEdificio(
            $conexion, $idEdificio
        );
        
        $arrObjEdificioEstado = $objEdificioEstado->obtenerEdificioEstado(
            $conexion
        );
        
        //lista de Tipo de via
        $tipoVia = '';
        if ( $arrObjEdificio[0]->__get('_idTipoVia')!= '' ) {
            $arrObjTipoVia = $objTipoVia->listar(
                $conexion, $arrObjEdificio[0]->__get('_idTipoVia')
            );
            if ( !empty($arrObjTipoVia) ) {
                $tipoVia = $arrObjTipoVia[0]->__get('_desTipoVia');
            }
        }

        include 'vistas/m.v_edificio.fotos.adjuntar.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
                'en unos minutos.';
        exit;
    }
}

function listadoFotosEdificio($idEdificio)
{
    try {
        global $conexion, $isLogin, $dirModulo;

        $objEdificioFotos = new Data_FfttEdificioFotos();

        $arrObjEdificioFotos = $objEdificioFotos->obtenerFotosEdificio(
            $conexion, $idEdificio
        );

        include 'vistas/m.v_edificio.fotos.listado.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }

}

function detalleFotosEdificio($idEdificiosFoto)
{
    try {
        global $conexion, $isLogin, $dirModulo;

        $objEdificioFotos = new Data_FfttEdificioFotos();

        $arrObjFotosDetalle = $objEdificioFotos->obtenerFotosDetalle(
            $conexion, $idEdificiosFoto
        );

        $arrDataFotos = array();
        if ( !empty($arrObjFotosDetalle) ) {
            if ( trim($arrObjFotosDetalle[0]->__get('_foto1')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto1'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto2')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto2'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto3')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto3'))
                );
            }
            if ( trim($arrObjFotosDetalle[0]->__get('_foto4')) != '' ) {
                $arrDataFotos[] = array(
                    'image' => trim($arrObjFotosDetalle[0]->__get('_foto4'))
                );
            }
        }

        include 'vistas/m.v_edificio.fotos.detalle.php';

    } catch(PDOException $e) {
        echo 'Se ha producido un error en el Sistema. Vuelva a intentarlo ' .
            'en unos minutos.';
        exit;
    }

}


if (!isset($_GET['cmd']))
    $_GET['cmd'] = '';

switch ($_GET['cmd']) {
    case 'nuevoEdificio':
        nuevoEdificio();
        break;
    case 'editarEdificio':
        editarEdificio($_REQUEST['idedificio']);
        break;
    case 'detalleEdificio':
        detalleEdificio($_REQUEST['idedificio']);
        break;
    case 'adjuntarFotosEdificio':
        adjuntarFotosEdificio($_REQUEST['idedificio']);
        break;
    case 'listadoFotosEdificio':
        listadoFotosEdificio($_REQUEST['idedificio']);
        break;
    case 'detalleFotosEdificio':
        detalleFotosEdificio($_REQUEST['idedificiofoto']);
        break;
    default:
        cargaDefault();
        break;
}
