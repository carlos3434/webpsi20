var pr = null;
var order_field = '';
var order_type = '';
$(document).ready(function(){

    $("#cancelar_ajax").click(function(){
        pr.abort();
        loader("end");
    });

    $("#btn_buscar").click(function() {
        order_field = '';
        order_type = '';
        $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
        
        D.BuscarEdificios();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            order_field = '';
            order_type = '';
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
            
            D.BuscarEdificios();
        }
    });
    
    
    //Cambiar estado del edificio
    cambiarEstado = function(idedificio) {
        var estado = $("#chg_estado_" + idedificio).val();
        
        D.CambiarEstado(idedificio, estado);
    }
    
    fotosEdificio = function(idEdificio) {
        D.FotosEdificio(idEdificio);
    }
    
    listadoFotosEdificio = function(idEdificio) {
        D.ListadoFotosEdificio(idEdificio);
    }
    
    verFotoDetalle = function(idarmarios_foto) {
        D.VerFotoDetalle(idarmarios_foto);
    }
    
    buscarEdificios = function() {
        D.BuscarEdificios();
    }
    
    detalleEdificio = function(idedificio) {
        D.DetalleEdificio(idedificio);
    }
    
    detalleEdificioParent = function(idedificio) {
        D.DetalleEdificioParent(idedificio);
    }
    
    verMovimientosEdificio = function(idedificio) {
        D.VerMovimientos(idedificio);
    }
    
    cerrarMovimientosEdificio = function() {
        D.CerrarMovimientos();
    }
    
    editarEdificio = function(idedificio) {
        D.EditarEdificio(idedificio);
    }
    
    editarEdificioParent = function(idedificio) {
        D.EditarEdificioParent(idedificio);
    }

    nuevoEdificio = function() {
        D.NuevoEdificio();
    }
    
    updateDatosEdificio = function() {
		D.UpdateDatosEdificio();
    }
    
    insertDatosEdificio = function() {
		D.InsertDatosEdificio();
    }

    
    verMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).hide();
        $("#ocultarMas_" + mtgespkarm).show();
        $("#mas_" + mtgespkarm).show();
    }
    ocultarMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).show();
        $("#ocultarMas_" + mtgespkarm).hide();
        $("#mas_" + mtgespkarm).hide();
    }

    
    obtenerZonalesPorRegionUsuario = function() {
    	$("select[id=zonal]").html("<option value=''>Seleccione</option>");
        $("select[id=mdf]").html("<option value=''>Seleccione</option>");
        $("select[id=departamento]").html("<option value=''>Seleccione</option>");
        $("select[id=provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=distrito]").html("<option value=''>Seleccione</option>");
        $("#campo_valor").attr('value','');
        
        var region = $("#region").attr('value');
        if(region == "") {
            return false;
        }

        data_content = {
            'region' : region
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/bandeja.control.edificio.php?cmd=obtenerZonalesPorRegionUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	var arrZonal = data['arrZonal'];
            	if ( arrZonal.length > 0 ) {
            		$.each(arrZonal, function(i, item) {
                        $("select[id=zonal]").append("<option value='"+arrZonal[i]['zonal']+"'>"+ arrZonal[i]['zonal'] + ' - ' +arrZonal[i]['nombre']+"</option>");
                    });
            	}
            }
        });
        
    }
    
    listarZonalesPorRegionUsuario = function() {
    	$("select[id=idfuente_identificacion]").html("<option value=''>Seleccione</option>");
    	$("select[id=cmb_zonal]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_ura]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_departamento]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_distrito]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_localidad]").html("<option value=''>Seleccione</option>");
        
        var region = $("#cmb_region").attr('value');
        if(region == "") {
            return false;
        }

        data_content = {
            'region' : region
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/bandeja.control.edificio.php?cmd=obtenerZonalesPorRegionUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	var arrZonal = data['arrZonal'];
            	var arrFuenteIdentificacion = data['arrFuenteIdentificacion'];
            	var arrDepartamento = data['arrDepartamento'];
            	if ( arrZonal.length > 0 ) {
            		$.each(arrZonal, function(i, item) {
                        $("select[id=cmb_zonal]").append("<option value='"+arrZonal[i]['zonal']+"'>"+ arrZonal[i]['zonal'] + ' - ' +arrZonal[i]['nombre']+"</option>");
                    });
            	}
            	if ( arrFuenteIdentificacion.length > 0 ) {
            		$.each(arrFuenteIdentificacion, function(i, item) {
                        $("select[id=idfuente_identificacion]").append("<option value='"+arrFuenteIdentificacion[i]['codigo']+"'>"+arrFuenteIdentificacion[i]['nombre']+"</option>");
                    });
            	}
            	if ( arrDepartamento.length > 0 ) {
            		$.each(arrDepartamento, function(i, item) {
                        $("select[id=cmb_departamento]").append("<option value='"+arrDepartamento[i]['coddpto']+"'>"+arrDepartamento[i]['nombre']+"</option>");
                    });
            	}
            }
        });
    }
    
    ListarProvinciasUbigeoEdi =  function() {
    	var idregion = $("#cmb_region").attr('value');
        var coddpto  = $("#cmb_departamento").attr('value');
        
        $("select[id=cmb_provincia]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_distrito]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_localidad]").html("<option value=''>Seleccione</option>");

        data_content = {
    		'idregion' : idregion,
            'coddpto'  : coddpto
        };
        
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/bandeja.control.edificio.php?cmd=provinciasUbigeoEdi",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	if ( data.length > 0 ) {
	                $.each(data, function(i, item) {
	                    $("select[id=cmb_provincia]").append("<option value='"+data[i]['codProv']+"'>"+data[i]['nombre']+"</option>");
	                });
            	}
            }
        });
    }
    
    ListarDistritosUbigeoEdi =  function() {
    	var idregion = $("#cmb_region").attr('value');
        var coddpto = $("#cmb_departamento").attr('value');
        var codprov = $("#cmb_provincia").attr('value');
        
        $("select[id=cmb_distrito]").html("<option value=''>Seleccione</option>");
        $("select[id=cmb_localidad]").html("<option value=''>Seleccione</option>");

        data_content = {
    		'idregion' : idregion,
            'coddpto'   : coddpto,
            'codprov'   : codprov
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/bandeja.control.edificio.php?cmd=distritosUbigeoEdi",
            data:   data_content,
            dataType: "json",
            success: function(data) {
            	if ( data.length > 0 ) {
	                $.each(data, function(i, item) {
	                    $("select[id=cmb_distrito]").append("<option value='"+data[i]['codDist']+"'>"+data[i]['nombre']+"</option>");
	                });
            	}
            }
        }); 
    }
    
    ListarLocalidadesUbigeo =  function() {
        var coddpto = $("#cmb_departamento").attr('value');
        var codprov = $("#cmb_provincia").attr('value');
        var coddist = $("#cmb_distrito").attr('value');
        
        $("select[id=cmb_localidad]").html("<option value=''>Seleccione</option>");

        data_content = {
            'coddpto'   : coddpto,
            'codprov'   : codprov,
            'coddist'   : coddist
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/util/functions.php?cmd=localidadesUbigeo",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_localidad]").append("<option value='"+data[i]['codLocal']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        }); 
    }
    
    ListarMdfsZonalUsuario = function() {
        $("select[id=cmb_ura]").html("<option value=''>Seleccione</option>");
        
        var zonal = $("#cmb_zonal").attr('value');
        if (zonal == "") {
            return false;
        }

        data_content = {
            'zonal' : zonal
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/util/functions.php?cmd=obtenerMdfsZonalUsuario",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_ura]").append("<option value='"+data[i]['mdf']+"'>"+ data[i]['mdf'] + ' - ' +data[i]['nombre']+"</option>");
                });
            }
        });
        /*
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/util/functions.php?cmd=obtenerDepartamentosZonal",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_departamento]").append("<option value='"+data[i]['coddpto']+"'>"+data[i]['nombre']+"</option>");
                });
            }
        });
        */
    }

    ListarTipoProyectoPorSegmento = function() {
    	$("select[id=cmb_tipo_proyecto]").html("<option value=''>Seleccione</option>");
        
        var segmento = $("#cmb_segmento").attr('value');
        if (segmento == "") {
            return false;
        }

        data_content = {
            'segmento' : segmento
        };
        $.ajax({
            type:   "POST",
            url:    "../../modulos/maps/util/functions.php?cmd=obtenerTipoProyectoPorSegmento",
            data:   data_content,
            dataType: "json",
            success: function(data) {
                $.each(data, function(i, item) {
                    $("select[id=cmb_tipo_proyecto]").append("<option value='"+data[i]['codtipo']+"'>"+ data[i]['nombre'] +"</option>");
                });
            }
        });
    }

    var D = {

        BuscarEdificios: function() {
            var gestionObra = $("#gestionObra").val();
            var empresa		= $("#empresa").val();
            var region      = $("#region").val();
            var zonal       = $("#zonal").val();
            var mdf         = $("#mdf").val();
            var campo_valor = $.trim($("#campo_valor").val());

            var departamento    = $("#departamento").val();
            var provincia       = $("#provincia").val();
            var distrito        = $("#distrito").val();

            var page    = $("#pag_actual").val();
            var tl      = $("#tl_registros").val();
            
            if ( region == '' ) {
            	alert("Debe seleccionar una region");
            	return false;
            }
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                gestionObra : gestionObra,
                empresa     : empresa,
                region		: region,
                zonal       : zonal, 
                mdf         : mdf, 
                departamento: departamento,
                provincia   : provincia,
                distrito    : distrito,
                campo_valor : campo_valor,
                order_field : order_field,
                order_type  : order_type
            };

            loader("start");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=filtroBusqueda", data_content, function(data){
            	$("#msgEdificios").hide();
                $("#tb_sup_tl_registros").show();
                $("#tb_pie_tl_registros").show();
                
                $("#tb_resultado").show();
                $("#tb_resultado tbody").empty();
                $("#tb_resultado tbody").append(data);
                loader("end");
            });
        },
        
        CambiarEstado: function(idedificio, estado) {
            var data_content = {
                idedificio  : idedificio,
                estado      : estado
            }
            
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=cambiarEstado", data_content, function(data){
                if(data['flag'] == '1'){
                    //refresca la bandeja
                    buscarEdificios();
                }else{
                    alert(data['mensaje']);
                }
            },'json');
        },
        
        FotosEdificio: function(idEdificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=fotosEdificio", {
            	idEdificio : idEdificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'FOTOS DEL EDIFICIO', position:'top'});

        },
        
        ListadoFotosEdificio: function(idEdificio) {
            $("#loading_fotos_edificio").show();
            $.post("modulos/maps/bandeja.control.edificio.php?cmd=listadoFotosEdificio", {
                idEdificio : idEdificio
            },
            function(data){
                $("#tb_lista_fotos tbody").empty();
                $("#tb_lista_fotos tbody").append(data);
                $("#loading_fotos_edificio").hide();
            });
        },
        
        VerFotoDetalle: function(idedificios_foto) {
            $("#tabsFotosEdificio").tabs("enable", 2);
            $("#tabsFotosEdificio").tabs('select',2);
            
            $("#divFotosEdificio").html('Cargando fotos...');
            $.post("modulos/maps/bandeja.control.edificio.php?cmd=verFotoDetalle", {
            	idedificios_foto: idedificios_foto
            },
            function(data){
                $("#divFotosEdificio").html(data);
            });
        },
        
        DetalleEdificio: function(idedificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=detalleEdificio", {
                idedificio: idedificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INFORMACIÓN DEL EDIFICIO', position:'top'});

        },
        
        cargaMasiva: function() {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=cargaMasiva", {},
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'60%', hide: 'slide', title: 'CARGA MASIVA', position:'top'});

        },
        
        DetalleEdificioParent: function(idedificio) {
            loader('start');
            parent.$("#childModal").html('Cargando...');
            parent.$("#childModal").css("background-color","#FFFFFF");
            pr = $.post("../bandeja.control.edificio.php?cmd=detalleEdificio", {
                idedificio: idedificio
            },
            function(data){
                parent.$("#childModal").html(data);
                loader('end');
            });

            parent.$("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INFORMACIÓN DEL EDIFICIO', position:'top'});

        },
        
        VerMovimientos: function(idEdificio) {
        	$("#divDetalle").hide();
        	$("#divMovimientos").show();
            loader('start');
            $("#divMovimientos").html('Cargando...');
            $("#divMovimientos").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=verMovimientos", {
                idEdificio: idEdificio
            },
            function(data){
                $("#divMovimientos").html(data);
                loader('end');
            });
            
        },
        
        CerrarMovimientos: function() {
        	$("#divDetalle").show();
        	$("#divMovimientos").html('');
        	$("#divMovimientos").hide();
        },
        
        EditarEdificio: function(idedificio) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=editarEdificio", {
                idedificio: idedificio
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'EDITAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        EditarEdificioParent: function(idedificio) {
            loader('start');
            parent.$("#childModal").html('Cargando...');
            parent.$("#childModal").css("background-color","#FFFFFF");
            pr = $.post("../bandeja.control.edificio.php?cmd=editarEdificio", {
                idedificio: idedificio,
                iframe     : 1
            },
            function(data){
                parent.$("#childModal").html(data);
                loader('end');
            });

            parent.$("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'EDITAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        NuevoEdificio: function() {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.edificio.php?cmd=nuevoEdificio", {},
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'INGRESAR INFORMACION DEL EDIFICIO', position:'top'});

        },
        
        UpdateDatosEdificio: function() {
            var data_content = {
	            item_inicial                : $.trim($("#item_inicial").val()),
	        	item                        : $.trim($("#item").val()),
	        	item_etapa                  : $.trim($("#item_etapa").val()),
	        	x							: $("#x").val(),
        		y							: $("#y").val(),
        		idempresa                   : $.trim($("#idempresa").val()),
	        	idgestion_obra				: $("#idgestion_obra").val(),
	        	codigo_proyecto_web			: $.trim($("#codigo_proyecto_web").val()),
	        	estado						: $("#estado").val(),
	        	idregion					: $("#cmb_region").val(),
	        	idfuente_identificacion     : $("#idfuente_identificacion").val(),
	        	zonal                       : $("#cmb_zonal").val(),
		    	ura                         : $("#cmb_ura").val(),
		    	fecha_registro_proyecto     : $("#fecha_registro_proyecto").val(),
		    	sector                      : $("#sector").val(),
		    	mza_tdp                     : $("#mza_tdp").val(),
		    	idtipo_via                  : $("#idtipo_via").val(),
		    	direccion_obra              : $("#direccion_obra").val(),
		    	numero                      : $("#numero").val(),
		    	mza                         : $("#mza").val(),
		    	lote                        : $("#lote").val(),
		    	idtipo_cchh                 : $("#idtipo_cchh").val(),
		    	cchh                        : $("#cchh").val(),
		    	distrito                    : $("#cmb_departamento").val() + $("#cmb_provincia").val() + $("#cmb_distrito").val(),
		    	localidad                   : $("#cmb_localidad").val(),
		    	idsegmento                  : $("#cmb_segmento").val(),
		    	cmb_tipo_proyecto           : $("#cmb_tipo_proyecto").val(),
		    	nombre_proyecto             : $("#nombre_proyecto").val(),
		    	nombre_constructora         : $("#nombre_constructora").val(),
		    	ruc_constructora            : $("#ruc_constructora").val(),
		    	idnivel_socioeconomico      : $("#cmb_nivel_socioeconomico").val(),
		    	persona_contacto            : $("#persona_contacto").val(),
		    	idzona_competencia          : $("#cmb_zona_competencia").val(),
		    	idgestion_competencia       : $("#cmb_gestion_competencia").val(),
		    	idcobertura_movil_2g        : $("#cmb_cobertura_movil_2g").val(),
		    	idcobertura_movil_3g        : $("#cmb_cobertura_movil_3g").val(),
		    	pagina_web                  : $("#pagina_web").val(),
		    	email                       : $("#email").val(),
		    	nro_blocks                  : $("#nro_blocks").val(),
		    	nro_pisos_1                 : $("#nro_pisos_1").val(),
		    	nro_dptos_1                 : $("#nro_dptos_1").val(),
		    	viven                       : $("#viven").val(),
		    	megaproyecto                : $("#megaproyecto").val(),
		    	nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
		    	idtipo_infraestructura      : $("#idtipo_infraestructura").val(),
		    	avance                      : $("#avance").val(),
		    	fecha_termino_construccion  : $("#fecha_termino_construccion").val(),
		    	montante                    : $("#montante").val(),
		    	montante_fecha              : $("#montante_fecha").val(),
		    	ducteria_interna            : $("#ducteria_interna").val(),
		    	ducteria_interna_fecha      : $("#ducteria_interna_fecha").val(),
		    	punto_energia               : $("#punto_energia").val(),
		    	punto_energia_fecha         : $("#punto_energia_fecha").val(),
		    	idfacilidad_poste           : $("#idfacilidad_poste").val(),
		    	idresponsable_campo         : $("#idresponsable_campo").val(),
		    	armario         			: $("#armario").val(),
		    	fecha_cableado              : $("#fecha_cableado").val(),
		    	validacion_call             : $("#validacion_call").val(),
		    	fecha_tratamiento_call      : $("#fecha_tratamiento_call").val(),
		    	fecha_seguimiento           : $("#fecha_seguimiento").val(),
		    	observacion                 : $("#observacion").val(),
		    	idedificio                  : $("#idedificio").val(),
		    	actualizacion			    : '2' //2 = movil
            };

            
            //validacion de Formulario de Insert y Update para edificios
        	if ( !D.validaFormularioEdificio() ) {
        		return false;
        	}
        	
        	
            if ( !confirm("Seguro de grabar los cambios?") ) {
            	return false;
            }
            
            
            $("#msg").show();
            $("input[name=btn_grabar]").attr('disabled','disabled');
            pr = $.post("../../modulos/maps/bandeja.control.edificio.php?cmd=updateDatosEdificio", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    window.top.location = "../../modulos/maps/m.buscomp.vermapa.php?search=1&x=" + data_content['x'] + "&y=" + data_content['y'] + "&comp[edi]=edi";

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_grabar]").attr('disabled','');
            },'json');

        },
        
        validaFormularioEdificio: function() {
        	if( $("#idempresa").val() == '' ) {
                alert("Ingrese el campo EMPRESA");
                $("#idempresa").focus();
                return false; 
            }
        	if( $("#cmb_region").val() == '' ) {
                alert("Ingrese el campo REGION");
                $("#cmb_region").focus();
                return false;
            }
            if( $("#cmb_zonal").val() == '' ) {
                alert("Ingrese el campo ZONAL");
                $("#cmb_zonal").focus();
                return false;
            }
            if( $("#cmb_ura").val() == '' ) {
                alert("Ingrese el campo URA");
                $("#cmb_ura").focus();
                return false;
            }
            if( $("#sector").val() == '' ) {
                alert("Ingrese el campo SECTOR");
                $("#sector").focus();
                return false;
            }
            if( $("#mza_tdp").val() == '' ) {
                alert("Ingrese el campo MANZANA (TDP)");
                $("#mza_tdp").focus();
                return false;
            }
            if( $("#mza_tdp").val() == '' ) {
                alert("Ingrese el campo TIPO VIA");
                $("#mza_tdp").focus();
                return false;
            }
            if( $("#direccion_obra").val() == '' ) {
                alert("Ingrese el campo DIRECCION OBRA");
                $("#direccion_obra").focus();
                return false;
            }
            if( $("#numero").val() == '' ) {
                alert("Ingrese el campo NUMERO");
                $("#numero").focus();
                return false;
            }
            if( $("#cmb_departamento").val() == '' ) {
                alert("Ingrese el campo DEPARTAMENTO");
                $("#cmb_departamento").focus();
                return false;
            }
            if( $("#cmb_provincia").val() == '' ) {
                alert("Ingrese el campo PROVINCIA");
                $("#cmb_provincia").focus();
                return false;
            }
            if( $("#cmb_distrito").val() == '' ) {
                alert("Ingrese el campo DISTRITO");
                $("#cmb_distrito").focus();
                return false;
            }
            if( $("#cmb_segmento").val() == '' ) {
                alert("Ingrese el campo SEGMENTO");
                $("#cmb_segmento").focus();
                return false;
            }
            if( $("#cmb_tipo_proyecto").val() == '' ) {
                alert("Ingrese el campo TIPO PROYECTO");
                $("#cmb_tipo_proyecto").focus();
                return false;
            }
            if( $("#cmb_zona_competencia").val() == '' ) {
                alert("Ingrese el campo ZONA COMPETENCIA");
                $("#cmb_zona_competencia").focus();
                return false;
            }
            if( $("#cmb_cobertura_movil_2g").val() == '' ) {
                alert("Ingrese el campo COBERTURA MOVIL 2G");
                $("#cmb_cobertura_movil_2g").focus();
                return false;
            }
            if( $("#cmb_cobertura_movil_3g").val() == '' ) {
                alert("Ingrese el campo COBERTURA MOVIL 3G");
                $("#cmb_cobertura_movil_3g").focus();
                return false;
            }
            if( $("#viven").val() == '' ) {
                alert("Ingrese el campo VIVEN");
                $("#viven").focus();
                return false;
            }
            if( $("#avance").val() == '' ) {
                alert("Ingrese el campo AVANCE");
                $("#avance").focus();
                return false;
            }
            if( $("#fecha_termino_construccion").val() == '' ) {
                alert("Ingrese el campo FECHA TERMINO CONSTRUCCION");
                $("#fecha_termino_construccion").focus();
                return false;
            }
            if( $("#fecha_seguimiento").val() == '' ) {
                alert("Ingrese el campo FECHA DE SEGUIMIENTO");
                $("#fecha_seguimiento").focus();
                return false;
            }
            
            //validacion cuando GESTION DE OBRA = 2 = 'En Curso' 
            if ( $("#idgestion_obra").val() == '2' ) {
            	
            	if( $("#persona_contacto").val() == '' ) {
                    alert("Ingrese el campo PERSONA CONTACTO");
                    $("#persona_contacto").focus();
                    return false;
                }
                if( $("#cmb_gestion_competencia").val() == '' ) {
                    alert("Ingrese el campo GESTION COMPETENCIA");
                    $("#cmb_gestion_competencia").focus();
                    return false;
                }
                if( $("#montante").val() == '' ) {
                    alert("Ingrese el campo MONTANTE");
                    $("#montante").focus();
                    return false;
                }
                if( $("#montante_fecha").val() == '' ) {
                    alert("Ingrese el campo MONTANTE FECHA");
                    $("#montante_fecha").focus();
                    return false;
                }
                if( $("#ducteria_interna").val() == '' ) {
                    alert("Ingrese el campo DUCTERIA INTERNA");
                    $("#ducteria_interna").focus();
                    return false;
                }
                if( $("#ducteria_interna_fecha").val() == '' ) {
                    alert("Ingrese el campo DUCTERIA INTERNA FECHA");
                    $("#ducteria_interna_fecha").focus();
                    return false;
                }
            }
            //validacion cuando GESTION DE OBRA = 2 = 'En Curso'
            
            return true;
            
        },
        
        InsertDatosEdificio: function() {
        	var data_content = {
	            //item_inicial                : $("#item_inicial").val(),
	        	//item                        : $("#item").val(),
	        	//item_etapa                  : $("#item_etapa").val(),
        		x							: $("#x").val(),
        		y							: $("#y").val(),
        		idempresa                   : $.trim($("#idempresa").val()),
	        	idgestion_obra				: $("#idgestion_obra").val(),
	        	codigo_proyecto_web			: $.trim($("#codigo_proyecto_web").val()),
	        	estado						: $("#estado").val(),
	        	idregion					: $("#cmb_region").val(),
	        	idfuente_identificacion     : $("#idfuente_identificacion").val(),
	        	zonal                       : $("#cmb_zonal").val(),
		    	ura                         : $("#cmb_ura").val(),
		    	fecha_registro_proyecto     : $("#fecha_registro_proyecto").val(),
		    	sector                      : $("#sector").val(),
		    	mza_tdp                     : $("#mza_tdp").val(),
		    	idtipo_via                  : $("#idtipo_via").val(),
		    	direccion_obra              : $("#direccion_obra").val(),
		    	numero                      : $("#numero").val(),
		    	mza                         : $("#mza").val(),
		    	lote                        : $("#lote").val(),
		    	idtipo_cchh                 : $("#idtipo_cchh").val(),
		    	cchh                        : $("#cchh").val(),
		    	distrito                    : $("#cmb_departamento").val() + $("#cmb_provincia").val() + $("#cmb_distrito").val(),
		    	localidad                   : $("#cmb_localidad").val(),
		    	idsegmento                  : $("#cmb_segmento").val(),
		    	cmb_tipo_proyecto           : $("#cmb_tipo_proyecto").val(),
		    	nombre_proyecto             : $("#nombre_proyecto").val(),
		    	nombre_constructora         : $("#nombre_constructora").val(),
		    	ruc_constructora            : $("#ruc_constructora").val(),
		    	idnivel_socioeconomico      : $("#cmb_nivel_socioeconomico").val(),
		    	persona_contacto            : $("#persona_contacto").val(),
		    	idzona_competencia          : $("#cmb_zona_competencia").val(),
		    	idgestion_competencia       : $("#cmb_gestion_competencia").val(),
		    	idcobertura_movil_2g        : $("#cmb_cobertura_movil_2g").val(),
		    	idcobertura_movil_3g        : $("#cmb_cobertura_movil_3g").val(),
		    	pagina_web                  : $("#pagina_web").val(),
		    	email                       : $("#email").val(),
		    	nro_blocks                  : $("#nro_blocks").val(),
		    	nro_pisos_1                 : $("#nro_pisos_1").val(),
		    	nro_dptos_1                 : $("#nro_dptos_1").val(),
		    	viven                       : $("#viven").val(),
		    	megaproyecto                : $("#megaproyecto").val(),
		    	nro_dptos_habitados         : $("#nro_dptos_habitados").val(),
		    	idtipo_infraestructura      : $("#idtipo_infraestructura").val(),
		    	avance                      : $("#avance").val(),
		    	fecha_termino_construccion  : $("#fecha_termino_construccion").val(),
		    	montante                    : $("#montante").val(),
		    	montante_fecha              : $("#montante_fecha").val(),
		    	ducteria_interna            : $("#ducteria_interna").val(),
		    	ducteria_interna_fecha      : $("#ducteria_interna_fecha").val(),
		    	punto_energia               : $("#punto_energia").val(),
		    	punto_energia_fecha         : $("#punto_energia_fecha").val(),
		    	idfacilidad_poste           : $("#idfacilidad_poste").val(),
		    	idresponsable_campo         : $("#idresponsable_campo").val(),
		    	armario         			: $("#armario").val(),
		    	fecha_cableado              : $("#fecha_cableado").val(),
		    	validacion_call             : $("#validacion_call").val(),
		    	fecha_tratamiento_call      : $("#fecha_tratamiento_call").val(),
		    	fecha_seguimiento           : $("#fecha_seguimiento").val(),
		    	observacion                 : $("#observacion").val(),
		    	ingreso						: '2' //2 = movil
            };
            
        	
        	//validacion de Formulario de Insert y Update para edificios
        	if ( !D.validaFormularioEdificio() ) {
        		return false;
        	}
        	
        	
            if ( !confirm("Seguro de grabar nuevo edificio?") ) {
            	return false;
            }
            
            
            $("#msg").show();
            $("input[name=btn_grabar]").attr('disabled','disabled');
            pr = $.post("../../modulos/maps/bandeja.control.edificio.php?cmd=insertDatosEdificio", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje'] + "\n\n" + "Item: " + data['item']);
                    
                    window.top.location = "../../modulos/maps/m.buscomp.vermapa.php?search=1&x=" + data_content['x'] + "&y=" + data_content['y'] + "&comp[edi]=edi";

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_grabar]").attr('disabled','');

            },'json');

        }
        
    };
});