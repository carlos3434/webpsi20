var pr = null;
var order_field = '';
var order_type = '';
$(document).ready(function(){

    $("#cancelar_ajax").click(function(){
        pr.abort();
        loader("end");
    });

    $("#btn_buscar").click(function() {
        order_field = '';
        order_type = '';
        $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
        
        D.BuscarArmarios();
    });

    $("#campo_valor").keyup(function(e) {
        if(e.keyCode == 13) {
            order_field = '';
            order_type = '';
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc hd_orden_desc");
            
            D.BuscarArmarios();
        }
    });
    
    $('#tb_resultado thead tr.headFilter a').click(function(event) {
        order_field  = $(this).attr("orderField");
        order_type   = $(this).attr("orderType");
        
        $(this).attr("orderType","desc");
        if( order_type == 'desc' ) {
            $(this).attr("orderType","asc");
        }
        
        D.BuscarArmarios();
    });
    
    $('#tb_resultado thead tr.headFilter a').toggle(
        function() {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_asc");
            $(this).addClass("hd_orden_desc"); 
        }, function () {
            $('#tb_resultado thead tr.headFilter a').removeClass("hd_orden_desc");
            $(this).addClass("hd_orden_asc");
        }
    );
        
    buscarArmarios = function() {
        D.BuscarArmarios();
    }
    
    fotosArmario = function(mtgespkarm) {
        D.FotosArmario(mtgespkarm);
    }
    
    editarArmario = function(mtgespkarm) {
        D.EditarArmario(mtgespkarm);
    }
    
    updateDatosArmario = function() {
        D.UpdateDatosArmario();
    }
    
    listadoFotosArmario = function(mtgespkarm) {
        D.ListadoFotosArmario(mtgespkarm);
    }
    
    verFotoDetalle = function(idarmarios_foto) {
        D.VerFotoDetalle(idarmarios_foto);
    }
    
    verMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).hide();
        $("#ocultarMas_" + mtgespkarm).show();
        $("#mas_" + mtgespkarm).show();
    }
    ocultarMas = function(mtgespkarm) {
        $("#verMas_" + mtgespkarm).show();
        $("#ocultarMas_" + mtgespkarm).hide();
        $("#mas_" + mtgespkarm).hide();
    }
    
    cerrarVentana = function() {
        $("#childModal").dialog('close');
    }

    limpiarFoto = function(id) {
        //$("#" + id).attr('value','');
        $("#" + id).replaceWith('<input id="' + id + '" class="frm_fotos" type="file" size="25" name="imagen_armario[]">');
    }

    var D = {

        BuscarArmarios: function() {
            var estado      = $("#estado").val();
            var zonal       = $("#zonal").val();
            var mdf         = $("#mdf").val();
            var campo_valor = $.trim($("#campo_valor").val());
            
            
            //var departamento    = $("#departamento").val();
            //var provincia       = $("#provincia").val();
            //var distrito        = $("#distrito").val();

            var page    = $("#pag_actual").val();
            var tl      = $("#tl_registros").val();
            
            var data_content = { 
                pagina      : page, 
                total       : tl, 
                estado      : estado,
                zonal       : zonal, 
                mdf         : mdf, 
                campo_valor : campo_valor,
                order_field : order_field,
                order_type  : order_type
            };

            loader("start");
            pr = $.post("modulos/maps/bandeja.control.armarios.php?cmd=filtroBusqueda", data_content, function(data){
                $("#tb_sup_tl_registros").show();
                $("#tb_pie_tl_registros").show();
                
                $("#tb_resultado").show();
                $("#tb_resultado tbody").empty();
                $("#tb_resultado tbody").append(data);
                loader("end");
            });

        },
        
        FotosArmario: function(mtgespkarm) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.armarios.php?cmd=fotosArmario", {
                mtgespkarm: mtgespkarm
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'75%', hide: 'slide', title: 'FOTOS DEL ARMARIO', position:'top'});

        },
        
        EditarArmario: function(mtgespkarm) {
            loader('start');
            $("#childModal").html('Cargando...');
            $("#childModal").css("background-color","#FFFFFF");
            pr = $.post("modulos/maps/bandeja.control.armarios.php?cmd=editarArmario", {
                mtgespkarm: mtgespkarm
            },
            function(data){
                $("#childModal").html(data);
                loader('end');
            });

            $("#childModal").dialog({modal:true, width:'70%', hide: 'slide', title: 'EDITAR INFORMACION DEL ARMARIO', position:'top'});

        },
        
        UpdateDatosArmario: function() {
            var data_content = {
                mtgespkarm : $("#mtgespkarm").val(),
                direccion  : $("#txt_direccion").val()
            }
            
            $("#msg").show();
            $("input[name=btn_guardar]").attr('disabled','disabled');
            pr = $.post("modulos/maps/bandeja.control.armarios.php?cmd=updateDatosArmario", data_content, function(data){
                
                if(data['flag'] == '1'){
                    alert(data['mensaje']);

                    //refresca la bandeja
                    buscarArmarios();
                    
                    //cerramos la ventana
                    cerrarVentana();

                }else{
                    alert(data['mensaje']);
                }
                
                $("#msg").hide();
                $("input[name=btn_guardar]").attr('disabled','');
            },'json');

        },
        
        ListadoFotosArmario: function(mtgespkarm) {
            
            $("#loading_fotos_armario").show();
            $.post("modulos/maps/bandeja.control.armarios.php?cmd=listadoFotosArmario", {
                mtgespkarm    : mtgespkarm
            },
            function(data){
                $("#tb_lista_fotos tbody").empty();
                $("#tb_lista_fotos tbody").append(data);
                $("#loading_fotos_armario").hide();
            });

        },
        
        VerFotoDetalle: function(idarmarios_foto) {
            $("#tabsFotosArmario").tabs("enable", 2);
            $("#tabsFotosArmario").tabs('select',2);
            
            $("#divFotosArmario").html('Cargando fotos...');
            $.post("modulos/maps/bandeja.control.armarios.php?cmd=verFotoDetalle", {
                idarmarios_foto: idarmarios_foto
            },
            function(data){
                $("#divFotosArmario").html(data);
            });
        }
        
    };
});