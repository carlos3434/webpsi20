var AdministracionMaps = function(){};

AdministracionMaps.prototype.Regresar = function(){
    $.post("modulos/maps/administracion.maps.php", {path: "" }, function(data){
    	$("#content").empty();
        $("#content").append(data);
    });
};

/* Fuente Identificación */
AdministracionMaps.prototype.MantFuente = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantFuente", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddFuente = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addFuente", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaFuente = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaFuente", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditFuente = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editFuente", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Fuente de Identificación',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateFuente = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado de la fuente de identificación: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateFuente", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateFuente(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateFuente(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Fuente Identificación X Región */
AdministracionMaps.prototype.MantFuenteXRegion = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantFuenteXRegion", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddFuenteXRegion = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addFuenteXRegion", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaFuenteXRegion = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaFuenteXRegion", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.ChangeStateFuenteXRegion = function(id,estado,region,fuente){
    if(confirm("Esta seguro de cambiar el estado de (Región : " + region + " - Fuente de Identificación : " + fuente + ")")){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateFuenteXRegion", { id: id, estado: estado }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateFuenteXRegion(\''+id+'\',\''+nvo_estado+'\',\''+region+'\',\''+fuente+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateFuenteXRegion(\''+id+'\',\''+nvo_estado+'\',\''+region+'\',\''+fuente+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Segmento */
AdministracionMaps.prototype.MantSegmento = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantSegmento", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddSegmento = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addSegmento", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaSegmento = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaSegmento", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditSegmento = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editSegmento", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Segmento',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateSegmento = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado del segmento: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateSegmento", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateSegmento(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateSegmento(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Nivel Socioeconomico */
AdministracionMaps.prototype.MantNse = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantNse", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddNse = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addNse", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaNse = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaNse", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditNse = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editNse", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Nivel Socioeconómico',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateNse = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado del nivel socioeconómico: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateNse", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateNse(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateNse(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Zona de Competencia */
AdministracionMaps.prototype.MantZona = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantZona", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddZona = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addZona", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaZona = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaZona", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditZona = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editZona", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Zona de Competencia',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateZona = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado de la zona de competencia: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateZona", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateZona(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateZona(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Gestion de Competencia */
AdministracionMaps.prototype.MantGestion = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantGestion", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddGestion = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addGestion", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaGestion = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaGestion", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditGestion = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editGestion", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Gestión de Competencia',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateGestion = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado de la gestión de competencia: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateGestion", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateGestion(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateGestion(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Gestión de Obra */
AdministracionMaps.prototype.MantObra = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantObra", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddObra = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addObra", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaObra = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaObra", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditObra = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editObra", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Gestión de Obra',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateObra = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado de la gestión de obra: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateObra", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateObra(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateObra(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Cobertura Movil 2G/3G */
AdministracionMaps.prototype.MantCobertura = function(tipo){
    $.post("modulos/maps/administracion.maps.php?cmd=mantCobertura",{ tipo : tipo }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddCobertura = function(tipo){
    $.post("modulos/maps/administracion.maps.php?cmd=addCobertura",{ tipo: tipo }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaCobertura = function(tipo){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaCobertura", { tipo: $("#tipo").val(), valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditCobertura = function(tipo, id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editCobertura", { tipo: tipo, id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Cobertura Movil',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateCobertura = function(tipo,id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado de la cobertura móvil : " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateCobertura", { tipo: tipo, id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateCobertura(\''+tipo+'\',\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateCobertura(\''+tipo+'\',\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Tipo de Proyecto */
AdministracionMaps.prototype.MantTipoProyecto = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantTipoProyecto", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddTipoProyecto = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addTipoProyecto", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaTipoProyecto = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaTipoProyecto", { id: $("#id").val() },function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.ChangeStateTipoProyecto = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado del tipo de Proyecto: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateTipoProyecto", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateTipoProyecto(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateTipoProyecto(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Responsable de Campo */
AdministracionMaps.prototype.MantResponsable = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantResponsable", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddResponsable = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addResponsable", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.BuscaResponsable = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaResponsable", { valor: $("#desc").val() }, function(data){
        loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditResponsable = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editResponsable", { id: id, }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Responsable de Campo',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateResponsable = function(id,estado,descripcion){
    if(confirm("Esta seguro de cambiar el estado del responsable de campo: " + descripcion)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateResponsable", { id: id, estado: estado, desc: descripcion }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateResponsable(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateResponsable(\''+id+'\',\''+nvo_estado+'\',\''+descripcion+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

/* Ubigeo */
AdministracionMaps.prototype.MantUbigeo = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=mantUbigeo", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.AddUbigeo = function(){
    $.post("modulos/maps/administracion.maps.php?cmd=addUbigeo", {path: "" }, function(data){
        $("#content").empty();
        $("#content").append(data);
    });
};

AdministracionMaps.prototype.MostrarDepartamentos = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=mostrarDepartamento", { id: $("#id").val() }, function(data){
    	$("#dpto").show();
        $("#dpto").empty();
        $("#dpto").append(data);
    });
};

AdministracionMaps.prototype.BuscaUbigeo = function(){    
    loader('start');
    $.post("modulos/maps/administracion.maps.php?cmd=buscaUbigeo", { id: $("#id").val(), dpto: $("#dpto").val() }, function(data){
    	loader('end');
        $("#tb_resultado").empty();
        $("#tb_resultado").append(data);
    });
};

AdministracionMaps.prototype.EditUbigeo = function(id){
    loader('start');
    $("#childModal").html('');
    $("#childModal").css("background-color","#FFFFFF");
    $.post("modulos/maps/administracion.maps.php?cmd=editUbigeo", { id: id }, function(data){
        loader('end');
        $("#childModal").html(data);
    });
    $("#childModal").dialog({
        modal:true,
        width: '500px',
        hide: 'slide',
        title: 'Editar Ubigeo',
        position:'top'
    });
};

AdministracionMaps.prototype.ChangeStateUbigeo = function(id,estado,region,dpto,indicador){
	var etiqueta = 'Departamento : ';
	if (indicador != '') {
		etiqueta = 'Provincia : ';
	}
	if(confirm("Esta seguro de cambiar el estado de " + etiqueta + dpto)){
        loader('start');
        var estado_inicial = estado;
        $.post("modulos/maps/administracion.maps.php?cmd=updateUbigeo", { id: id, estado: estado }, function(data){
            loader('end');
            if (data == 'ok') {
                if (estado_inicial == 0) {
                    var nvo_estado = 1;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateUbigeo(\''+id+'\',\''+nvo_estado+'\',\''+region+'\',\''+dpto+'\')');
                    $('#img_state_'+id).attr('src','img/estado_deshabilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Habilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Habilitado');
                } else {
                    nvo_estado = 0;
                    $('#alink_state_'+id).attr('href','javascript:AdministracionMaps.prototype.ChangeStateUbigeo(\''+id+'\',\''+nvo_estado+'\',\''+region+'\',\''+dpto+'\')');
                    $('#img_state_'+id).attr('src','img/estado_habilitado.png');
                    $('#img_state_'+id).attr('alt','Cambiar estado a Deshabilitado');
                    $('#img_state_'+id).attr('title','Cambiar estado a Deshabilitado');
                }
            }
        });
    }
};

var objAdministracionMaps = new AdministracionMaps();