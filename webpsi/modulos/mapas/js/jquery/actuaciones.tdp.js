function buscar_tdp() {
    
    switchDivsResultados('main');

    controlesOff_tdp();
    loader('start');
    
	$("#catv_tip_req").multiselect("disable");
	$("#catv_cod_motivo").multiselect("disable");
	$("#gestel_pref").multiselect("disable");
	$("#cmb_fil_especiales").multiselect("disable");
	$("#gestel_cod_serv").multiselect("disable");
	$("#gestel_cod_mov").multiselect("disable");

    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
   // var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }

    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1"); 
    var arraAsoc=new Array();
    var arrFiltro=new Array('ckh_gestionada','ckh_criticos','ckh_visita','ckh_con_speedy','ckh_crm','ckh_movistar','chk_rutina_off');
    $("#cmb_fil_especiales option").each(function (i,v) {
    	if(($(this).is(':selected')) && ($(this).val()==arrFiltro[i])) {
    		var campo=$(this).val();
    		arraAsoc[campo]=1;
    	} else {
    		var campo=$(this).val();
    		arraAsoc[campo]=0;
    	}
    });

    var flag_gestionado = '';
    if(arraAsoc['ckh_gestionada']=='1'){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if(arraAsoc['ckh_criticos']=='1'){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if(arraAsoc['ckh_visita']=='1'){
        flag_visita = '2';
    }
    
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if(arraAsoc['ckh_con_speedy']=='1'){
        flag_con_speedy = "1";
    }
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");

    
    var flag_crm = "";
    if(arraAsoc['ckh_crm']=='1'){
        flag_crm = "1";
    }
    var flag_movistar = "";
    if(arraAsoc['ckh_movistar']=='1'){
        flag_movistar = "1";
    }

    var flag_rutina_off = "";
    if(arraAsoc['chk_rutina_off']=='1'){
        flag_rutina_off = "1";
    }    

    var tecnico     = $("#cmb_tecnico").attr("value");

    var tipo_gestion = $("#cmb_tipo_gestion").attr("value");    /* Agregado por Czegarraa el 15/02/2012*/

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    //============ Combos Multiselect ======================
    
    var arrTiReq = $("#catv_tip_req").val();
    var arrCodMot = $("#catv_cod_motivo").val();
    var arrPref = $("#gestel_pref").val();
    var arrServ = $("#gestel_cod_serv").val();
    var arrMov = $("#gestel_cod_mov").val();
    //======================================================
    $.post("modulos/gescom/bandeja.tdp.php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        
        arrTiReq:arrTiReq,
        arrCodMot:arrCodMot,
        arrPref:arrPref,
        arrServ:arrServ,
        arrMov:arrMov,
        
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
        flag_crm:flag_crm,
        flag_movistar:flag_movistar,
        tecnico:tecnico,
        flag_rutina_off:flag_rutina_off,
        tipo_gestion:tipo_gestion
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_tdp();

        if(tipo=="Averia"){                            	
        	$("#catv_tip_req").multiselect('enable');
        	$("#catv_cod_motivo").multiselect('enable');
        	$("#gestel_pref").multiselect('disable');
        	$("#cmb_fil_especiales").multiselect('enable');
        	$("#gestel_cod_serv").multiselect('disable');
        	$("#gestel_cod_mov").multiselect('disable');
        } else if(tipo=="Provision") {
        	$("#catv_tip_req").multiselect('enable');
        	$("#catv_cod_motivo").multiselect('enable');
        	$("#gestel_pref").multiselect('enable');
        	$("#gestel_cod_serv").multiselect('enable');
        	$("#gestel_cod_mov").multiselect('enable');
        	$("#cmb_fil_especiales").multiselect('enable');   	
        } else {
        	$("#catv_tip_req").multiselect('disable');
        	$("#catv_cod_motivo").multiselect('disable');
        	$("#gestel_pref").multiselect('disable');
        	$("#cmb_fil_especiales").multiselect('enable');
        	$("#gestel_cod_serv").multiselect('disable');
        	$("#gestel_cod_mov").multiselect('disable');
        }
    	
        loader('end');
    });
}
function cargaTipoRequerimiento(Dtipo){
    var parametros = {'tipo'   : Dtipo };
	$.ajax({
	type    :"POST",
	url     :"modulos/gescom/bandeja.tdp.php?cmd=cargaTipoRequerimiento",                 
	data    :parametros,
	dataType: "json",
	success : function(json) {             
		  $("#catv_tip_req").html(""); 	  
	   $.each(json,function(x,item){   	
	     $('#catv_tip_req').append("<option value='"+json[x].cod+"'>"+json[x].des+"</option>");                                              
	   }); 
	   $("#catv_tip_req").multiselect("refresh"); 
	}
	});	
}
function ordenar_tdp(orden, direccion) {
    $("#orden").attr("value",orden);
    $("#direccion").attr("value",direccion);
    buscar_tdp();
}

function reasignarContrata() {

        /*if($("#cmb_tipo").attr("value")!='Averia'){
            alert("ATENCION: Esta operacion esta permitida solamente para Averias!");
            return;
        }*/

	var chx = getCheckedBoxFrom("table", "tb_resultado", "1")
//alert(chx); return;
	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/bandeja.tdp.php?cmd=vistaReasignarEmpresa&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Re-Asignar Contrata',
			position : 'top'
		});
	}
}

function controlesOff_tdp() {

    $("#cmb_tipo").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');

    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_reiteradas").attr("disabled",'disable');
    $("#btn_buscar_filtro").attr("disabled",'disable');
    $("#cmb_seg_prioridad").attr("disabled",'disable');
}

function controlesOn_tdp() {

    $("#cmb_tipo").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');

    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');

    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_reiteradas").attr("disabled",'');
    $("#btn_buscar_filtro").attr("disabled",'');
    $("#cmb_seg_prioridad").attr("disabled",'');
}
