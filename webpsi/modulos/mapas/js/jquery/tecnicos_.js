/**
 *
 * [Nombre_proyecto_aplicacion] :: Asignaci�n de Tecnicos
 *
 *
 * @category	script
 * @author	Carlos Zegarra Adrianzen <czegarraa@gmd.com.pe>
 * @copyright	2011 Planificacion y Soluciones Informaticas
 * @filesource
 *
 * @package     /js/jquery/
 *
 *
 *
 */

function asignarTecnicoPedido() {

	var chx = getCheckedBoxFrom("div", "content", "1")

	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/tecnicos.asigna.php?cmd=vistaAsignarTecnico&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Asignar Tecnicos',
			position : 'top'
		});
	}
}

function iniTecnicosActuacion__(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }

    //switchDivsResultados('main');
    loader('start');
    var tipo = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc = $("#cmb_abc").attr("value");
    var cmb_gest= $("#cmb_gest").attr("value");
    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var cmb_user_aseg= $("#cmb_user_aseg").attr("value");

    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }

    $.post("modulos/gescom/tecnicos"+strVista+".php?cmd=listarActuacionesIni", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        cmb_gest:cmb_gest,
	iduser_aseg:cmb_user_aseg,
        flag_liquidadas:flag_liquidadas
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');
        controlesOn();
    });
}

function iniTecnicosActuacion(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }

    switchDivsResultados('main');
    controlesOff_tec();
    loader('start');

    var tipo = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc = $("#cmb_abc").attr("value");
    var cmb_gest= $("#cmb_gest").attr("value");
    var campo = $("#campo").attr("value");
    var valor = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");

    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }

    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");

    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if($("#ckh_visita").attr('checked')){
        flag_visita = '2';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");
    var celula   = $("#cmb_celulas").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    if(empresa==''){
        alert("Por favor, seleccione una Empresa!");
        loader('end');
        controlesOn_tec();
        $("#cmb_empresa").focus();
        return false;

    }
    $.post("modulos/gescom/tecnicos"+strVista+".php?cmd=listarActuacionesIni", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
        celula:celula
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        loader('end');
        controlesOn_tec();
    });
}

function buscarTecnicosActuacion__(vista) {
    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }
    //switchDivsResultados('main');
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    //alert(lista_segmento);
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var celula   = $("#cmb_celulas").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");

    controlesOff();
    $.post("modulos/gescom/tecnicos"+strVista+".php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        celula:celula
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn();
        loader('end');
    });
}

function buscarTecnicosActuacion(vista) {

    var strVista='';
    if(vista){
        strVista = '.'+vista;
    }

    controlesOff_tec();
    loader('start');

    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var abc     = $("#cmb_abc").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $('#valor').attr("value").replace(/^\s+|\s+$/g, "");
    var flag_liquidadas = '';
    if($("#ckh_liquidadas").attr('checked')){
        flag_liquidadas = '1';
    }
    var orden   = $("#orden").attr("value");
    var direccion = $("#direccion").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var estado  = $("#cmb_estado").attr("value");
    var zonal   = $("#cmb_zonal").attr("value");
    var mdf     = $("#cmb_mdf").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_segmento = getCheckedBoxFrom("div", "div_list_segmento", "1");
    //alert(lista_segmento);
    var flag_gestionado = '';
    if($("#ckh_gestionada").attr('checked')){
        flag_gestionado = '1';
    }
    var flag_criticos = '';
    if($("#ckh_criticos").attr('checked')){
        flag_criticos = '1';
    }
    var flag_visita = '';
    if($("#ckh_visita").attr('checked')){
        flag_visita = '2';
    }
    var nro_reiteradas = $("#cmb_reiteradas").attr("value");
    var segmento = $("#cmb_segmento").attr("value");

    var flag_con_speedy = "";
    if($("#ckh_con_speedy").attr("checked")){
        flag_con_speedy = "1";
    }
    var segm_prioridad = $("#cmb_seg_prioridad").attr("value");
    var celula   = $("#cmb_celulas").attr("value");

    $("#pag_actual").attr("value",'1');
    var pagina = $("#pag_actual").attr("value");
    
    $.post("modulos/gescom/tecnicos"+strVista+".php?cmd=filtrarActuaciones", {
        tipo:tipo,
        negocio:negocio,
        abc:abc,
        campo:campo,
        valor:valor,
        orden:orden,
        direccion:direccion,
        pagina:pagina,
        empresa:empresa,
        estado:estado,
        zonal:zonal,
        mdf:mdf,
        lista_mdf:lista_mdf,
        flag_gestionado:flag_gestionado,
        flag_liquidadas:flag_liquidadas,
        flag_criticos:flag_criticos,
        nro_reiteradas:nro_reiteradas,
        segmento:segmento,
        lista_segmento:lista_segmento,
        flag_con_speedy:flag_con_speedy,
        flag_visita:flag_visita,
        segm_prioridad:segm_prioridad,
        celula:celula
    }, function(data){
        $("#tb_resultado").empty();
        $("#tb_resultado").html(data);
        controlesOn_tec();
        loader('end');
    });
}

function asignarTecnicoActuacion() {

	var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/tecnicos.asigna.php?cmd=vistaAsignarTecnico&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Asignar Tecnicos',
			position : 'top'
		});
	}
}

function asignarTecnicoActuacionTdp() {

    if($("#cmb_tipo").attr("value")!='Averia'){
            alert("ATENCION: Esta operacion esta permitida solamente para Averias!");
            return;
        }

    if($("#cmb_tipo").attr("value")!='Averia'){
            alert("ATENCION: Esta operacion esta permitida solamente para Averias!");
            return;
        }

	var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/bandeja.tdp.php?cmd=vistaAsignarTecnico&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Asignar Tecnicos',
			position : 'top'
		});
	}
}

function asignarTecnicoActuacionTdpMapa() {

	var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

	if (chx == "") {
		alert("Debe seleccionar al menos una actuacion");
	} else {
                var random = (Math.random()*10);
		$.post("modulos/gescom/bandeja.tdp.php?cmd=vistaAsignarTecnicoMapa&rdn="+random+"", {
                        strActu : chx
		}, function(data) {
			$("#childModal").html(data);
		});

		$("#childModal").dialog({
			modal : true,
			width : '500px',
			hide : 'slide',
			title : 'Asignar Tecnicos',
			position : 'top'
		});
	}
}

function controlesOff_tec() {

    $("#cmb_tipo").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');

    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');
    //$("#btn_buscar_ini").attr("disabled",'disable');
    $("#btn_limpiar").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_reiteradas").attr("disabled",'disable');
    $("#btn_buscar_filtro").attr("disabled",'disable');
    $("#cmb_seg_prioridad").attr("disabled",'disable');

    $("#cmb_celulas").attr("disabled",'disable');
}

function controlesOn_tec() {

    $("#cmb_tipo").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');

    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');
    //$("#btn_buscar_ini").attr("disabled",'');
    $("#btn_limpiar").attr("disabled",'');

    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_reiteradas").attr("disabled",'');
    $("#btn_buscar_filtro").attr("disabled",'');
    $("#cmb_seg_prioridad").attr("disabled",'');

    $("#cmb_celulas").attr("disabled",'');

    //$("#cmb_gest").attr("disabled",'');
}

// Funciones del visor de Asignacion de Tecnicos //

/**
 * Genera el listado con totales de actuaciones (No asignadas, Pre asignadas y Asignadas a tecnicos)
 * agrupadas por c�lulas
 *
 * @method buscar_visor_asignacion
 * @return vista
 */
function buscar_visor_asignacion() {

    $("#cont_resultado_main").css("display",'block');
    $("#cont_grupo_asignacion").css("display",'none');

    controlesOff_visor_asigna();
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var lista_negocio = getCheckedBoxFrom("div", "div_list_negocio", "1");
    
    var zonal   = $("#cmb_zonal").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    
    var abc     = $("#cmb_abc").attr("value");
    var prioridad     = $("#cmb_seg_prioridad").attr("value");

    if(tipo==''){
        alert("Seleccione el Tipo de pedido!");
        $("#cmb_tipo").focus();
        loader('end');
        controlesOn_visor_asigna();
        return;
    }

    $("#cmb_empresa").css("color","#000");
    $("#cmb_empresa").css("font-weight","normal");

    if(empresa==''){
        alert("Por favor, seleccione una Empresa!");
        loader('end');
        controlesOn_visor_asigna();
        $("#cmb_empresa").css("color","#FF0000");
        $("#cmb_empresa").css("font-weight","bold");
        $("#cmb_empresa").focus();
        return false;

    }
    //controlesOff_new();
    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=cargarVisorGrupoCelula", {
        zonal:zonal,
        empresa:empresa,
        tipo:tipo,
        negocio:negocio,
        lista_negocio:lista_negocio,
        lista_mdf:lista_mdf,
        abc:abc,
        prioridad:prioridad
    }, function(data){
        $("#cont_resultado_main").empty();
        $("#cont_resultado_main").html(data);
        controlesOn_visor_asigna();
        loader('end');
    });

    $("#cont_list_mdf").css("display","none");


}

/**
* Genera el listado con los totales de actuaciones (No asignadas, Pre asignadas y Asignadas a tecnicos)
* agrupadas por MDF/Nodo
*
* @method expande_celula_mdf
* @param int celula
* @return vista
*/
function expande_celula_mdf(celula) {

    $("#btn_expande_"+celula).css("display",'none');
    $("#btn_contrae_"+celula).css("display",'block');

    controlesOff_visor_asigna();
    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var negocio = $("#cmb_negocio").attr("value");
    var lista_negocio = getCheckedBoxFrom("div", "div_list_negocio", "1");
    var zonal   = $("#cmb_zonal").attr("value");
    var empresa = $("#cmb_empresa").attr("value");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var abc     = $("#cmb_abc").attr("value");
    var prioridad     = $("#cmb_seg_prioridad").attr("value");

    if(tipo==''){
        alert("Seleccione el Tipo de pedido!");
        $("#cmb_tipo").focus();
        loader('end');
        controlesOn_visor_asigna();
        return;
    }

    $("#cmb_empresa").css("color","#000");
    $("#cmb_empresa").css("font-weight","normal");

    if(empresa==''){
        alert("Por favor, seleccione una Empresa!");
        loader('end');
        controlesOn_visor_asigna();
        $("#cmb_empresa").css("color","#FF0000");
        $("#cmb_empresa").css("font-weight","bold");
        $("#cmb_empresa").focus();
        return false;

    }
    //controlesOff_new();
    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=cargarVisorCelulaMdfs", {
        zonal:zonal,
        empresa:empresa,
        tipo:tipo,
        negocio:negocio,
        lista_negocio:lista_negocio,
        lista_mdf:lista_mdf,
        abc:abc,
        prioridad:prioridad,
        celula:celula
    }, function(data){
        $("#cont_celula_"+celula).empty();
        $("#cont_celula_"+celula).html(data);
        controlesOn_visor_asigna();
        loader('end');
    });
}

/**
* Cierra el contenedor del listado con los totales de actuaciones aprupagas por MDF/Nodo
*
* @method contrae_celula_mdf
* @param int celula
* @return void
*/
function contrae_celula_mdf(celula) {

    $("#cont_celula_"+celula).empty();
    $("#btn_expande_"+celula).css("display",'block');
    $("#btn_contrae_"+celula).css("display",'none');
    //$("#cont_celula_"+celula).css("display",'none');
}

/**
* Genera el listado de actuaciones seg�n una C�lula y/o MDF/Nodo
*
* @method listaVisorAsignaTec
* @param string tipo
* @param string negocio
* @param string mdf
* @param int empresa
* @param string zonal
* @param string abc
* @param int prioridad
* @param int celula
* @param string asignacion
* @param int pagina
* @return vista
*/
function listaVisorAsignaTec(tipo,negocio,mdf,empresa,zonal,abc,prioridad,celula,asignacion,pagina){
    
    $("#cont_resultado_main").css("display",'none');
    $("#cont_grupo_asignacion").css("display",'block');
    controlesOff_visor_asigna();
    $("#txt_flag_origen_busqueda").attr("value","filtrado");
    var lista_mdf = getCheckedBoxFrom("div", "div_list_mdfs", "1");
    var lista_negocio = getCheckedBoxFrom("div", "div_list_negocio", "1");

    $("#txt_select_celula").val(celula);
    $("#txt_select_mdf").val(mdf);
    $("#txt_select_asignacion").val(asignacion);

    loader('start');
    $("#lista_grupo_asignacion").html("Cargando...");
    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=listarCelulaMdf", {
        tipo:tipo,
        negocio:negocio,
        mdf:mdf,
        abc:abc,
        empresa:empresa,
        zonal:zonal,
        asignacion:asignacion,
        pagina:pagina,
        lista_mdf:lista_mdf,
        celula:celula,
        prioridad:prioridad,
        lista_negocio:lista_negocio,

    }, function(data){
        $("#lista_grupo_asignacion").empty();
        $("#lista_grupo_asignacion").html("Cargando...");
        $("#lista_grupo_asignacion").html(data);
        //controlesOn_new();
        loader('end');
    });
}

/**
* Activa todos los controles del visor de Asignacion de tecnicos
*
* @method controlesOn_visor_asigna
* @return void
*/
function controlesOn_visor_asigna() {

    $("#cmb_tipo").attr("disabled",'');
    $("#btn_buscar_ini").attr("disabled",'');

    $("#cmb_empresa").attr("disabled",'');
    $("#cmb_negocio").attr("disabled",'');
    $("#cmb_zonal").attr("disabled",'');
    $("#cmb_estado").attr("disabled",'');
    $("#cmb_abc").attr("disabled",'');
    $("#cmb_seg_prioridad").attr("disabled",'');

    $("#campo").attr("disabled",'');
    $("#valor").attr("disabled",'');


}

/**
* Desactiva todos los controles del visor de Asignacion de tecnicos
*
* @method controlesOff_visor_asigna
* @return void
*/
function controlesOff_visor_asigna() {

    $("#cmb_tipo").attr("disabled",'disable');
    $("#btn_buscar_ini").attr("disabled",'disable');

    $("#cmb_empresa").attr("disabled",'disable');
    $("#cmb_negocio").attr("disabled",'disable');
    $("#cmb_zonal").attr("disabled",'disable');
    $("#cmb_estado").attr("disabled",'disable');
    $("#cmb_abc").attr("disabled",'disable');
    $("#cmb_seg_prioridad").attr("disabled",'disable');

    $("#campo").attr("disabled",'disable');
    $("#valor").attr("disabled",'disable');

}

/**
* Cierra el contenedor con el listado de actuaciones seg�n una C�lula y/o MDF/Nodo
*
* @method returnVisorAsigna
* @return void
*/
function returnVisorAsigna(){
    $("#cont_resultado_main").css("display",'block');
    $("#cont_grupo_asignacion").css("display",'none');
    controlesOn_visor_asigna();
}

/**
* Selecciona/Deselecciona los checks del listao de actuaciones seg�n una C�lula y/o MDF/Nodo
*
* @method select_unselect_check
* @return void
*/
function select_unselect_check(){
    
    if($("#chk_all").is(':checked')){
        $('.lista_chk').attr('checked',true);
    } else {
        $('.lista_chk').attr('checked',false);
    }
    
}

/**
* Genera la vista para la asignaci�n de actuaciones a una celula
*
* @method asignaCelulaVisor
* @return void
*/
function asignaCelulaVisor(){
    
    $("#childModal").html('Cargando...');

    var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

    if (chx == "") {
            alert("Debe seleccionar al menos una actuacion!");
    } else {
            var random = (Math.random()*10);
            var tipo    = $("#cmb_tipo").attr("value");
            var zonal   = $("#cmb_zonal").attr("value");
            var empresa = $("#cmb_empresa").attr("value");
            
            loader('start');
            $.post("modulos/gescom/asignacion.tecnicos.php?cmd=vistaPreAsignarCelulaVisor&rdn="+random+"", {
                    strActu : chx,
                    tipo:tipo,
                    zonal:zonal,
                    empresa:empresa
            }, function(data) {
                    $("#childModal").html(data);
            });
            $("#childModal").dialog({
                    modal : true,
                    width : '500px',
                    hide : 'slide',
                    title : 'Pre-Asignar a Celulas',
                    position : 'top'
            });
            loader('end');
    }

}

/**
* Genera la vista para la asignaci�n de actuaciones a un tecnico
*
* @method asignaTecnicoVisor
* @return void
*/
function asignaTecnicoVisor(){

    $("#childModal").html('Cargando...');

    var chx = getCheckedBoxFrom("table", "tb_resultado", "1")

    if (chx == "") {
            alert("Debe seleccionar al menos una actuacion!");
    } else {
            var origen_busqueda = $("#txt_flag_origen_busqueda").attr("value");
            var zonal; var empresa;
            if(origen_busqueda=='busqueda'){
                zonal   = $("#txt_tmp_zonal_busq").attr("value");
                empresa = $("#txt_tmp_empresa_busq").attr("value");    
            }else {
                zonal   = $("#cmb_zonal").attr("value");
                empresa = $("#cmb_empresa").attr("value");    
            }

            var random = (Math.random()*10);
            var tipo    = $("#cmb_tipo").attr("value");
            var celula = $("#txt_select_celula").attr("value");
            
            loader('start');
            $.post("modulos/gescom/asignacion.tecnicos.php?cmd=vistaAsignarTecnicoVisor&rdn="+random+"", {
                    strActu : chx,
                    tipo:tipo,
                    zonal:zonal,
                    empresa:empresa,
                    celula:celula
            }, function(data) {
                    $("#childModal").html(data);
            });
            $("#childModal").dialog({
                    modal : true,
                    width : '600px',
                    hide : 'slide',
                    title : 'Asignar Tecnicos',
                    position : 'top'
            });
            loader('end');
    }

}


/* Alerta para agendas con tecnico asignado ausente */

function iniAgendaTecAusente() {
    agendaTecAusente();
    setInterval("agendaTecAusente()", 60000);
}

function agendaTecAusente(){
    
    //$("#cont_info_alertas_reagenda").html(".");
    
    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=alertaAgendasTecnicoAusente", {}, function(data){

        /* obtiene el valor actual en el input de la alerta */
        var num_agendas = $("#txt_cont_alertas_agenda_ausente").attr("value");
        var arrRetornaCadenaAgendadas = data.split("-agendas|");

        retornaNumAgendadas = arrRetornaCadenaAgendadas[0];     // total de agendas
        retornaListaAgendadas = arrRetornaCadenaAgendadas[1];   // cadena con nro de agendas por tipo de pedido
        
        /* Si el total de agendas es mayor q cero */
        if(retornaNumAgendadas>0){

            var arrAgendasTipo = retornaListaAgendadas.split(",");

            /* totales de agendas para averias */
            var agendaAveria = arrAgendasTipo[0];
            var totalAgendaAveria = agendaAveria.split(".")[0];

            /* totales de agendas para provision */
            var agendaProvision = arrAgendasTipo[1];
            var totalAgendaProvision = agendaProvision.split(".")[0];

            /* totales de agendas para rutinas */
            var agendaRutina = arrAgendasTipo[2];
            var totalAgendaRutina = agendaRutina.split(".")[0];
        
            strAlertaAgenda = "";

            /* Genero el div para cada tipo de pedido con agendas con tecnico ausente */
            if(totalAgendaAveria>0){
                strAlertaAgenda = strAlertaAgenda + "<div onclick=\"listarAgendasAusente('Averia')\" title=\"Ver agendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px; margin-top:3px; margin-left:3px;\">Averias: &nbsp;&nbsp;<span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_agenda_ausente_averia\">"+totalAgendaAveria+"</span></div>";
            }
            if(totalAgendaProvision>0){
                strAlertaAgenda = strAlertaAgenda + "<div onclick=\"listarAgendasAusente('Provision')\" title=\"Ver agendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px;  margin-top:3px; margin-left:3px;\">Provision: <span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_agenda_ausente_provision\">"+totalAgendaProvision+"</span></div>";
            }
            if(totalAgendaRutina>0){
                strAlertaAgenda = strAlertaAgenda + "<div onclick=\"listarAgendasAusente('Rutina')\" title=\"Ver agendamientos\" style=\"cursor:pointer; color:#666; display:block; width:70px;  margin-top:3px; margin-left:3px;\">Rutinas: <span style=\"color:red; font-weight: bold; font-size: 13px;\" id=\"cont_info_alertas_agenda_ausente_rutina\">"+totalAgendaRutina+"</span></div>";
            }
            $("#cont_agendas_ausente_tipo").html(strAlertaAgenda);

        }

        /* Si existen agendas con tecnico ausente muestro la alerta */
        if(retornaNumAgendadas>0){
            if(num_agendas=='0'){
                document.getElementById('txt_cont_alertas_agenda_ausente').value=retornaNumAgendadas;
                $("#cont_alerta_agendas_tecnico_ausente").show("slide", {
                    direction: "down"
                }, 1000);
            }
        } else {
            /* Si no existen agendas con tecnico ausente oculto la alerta */
            if(num_agendas!='0'){
                document.getElementById('txt_cont_alertas_agenda_ausente').value=retornaNumAgendadas;
                $("#cont_alerta_agendas_tecnico_ausente").hide("slide", {
                    direction: "down"
                }, 1000);
            }
        }
    });
    
}

function listarAgendasAusente(tipo){
    //alert(tipo_pedido);

    $("#cont_resultado_main").css("display",'none');
    
    $("#cont_resultados_agendas_ausente").css("display",'block');
    controlesOff_visor_asigna();
    loader('start');
    $("#cont_resultados_agendas_ausente").empty();
    $("#cont_resultados_agendas_ausente").html("Cargando...");
    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=listarAgendasTecnicoAusente", {
        tipo:tipo
    }, function(data){
        $("#cont_resultados_agendas_ausente").empty();
        $("#cont_resultados_agendas_ausente").html(data);
        loader('end');
        //controlesOn_new();
    });

}

function returnListarAgendasAusente(){
    $("#cont_resultado_main").css("display",'block');
    $("#cont_resultados_agendas_ausente").css("display",'none');
    controlesOn_visor_asigna();

}

/*  */

function buscar_pedido_asignacion(){

    controlesOff_visor_asigna();

    $("#txt_flag_origen_busqueda").attr("value","busqueda");

    loader('start');
    var tipo    = $("#cmb_tipo").attr("value");
    var campo   = $("#campo").attr("value");
    var valor   = $("#valor").attr("value");

    if(valor==''){
        alert("Ingrese el Nro de pedido!");
        loader('end');
        controlesOn_visor_asigna();
        $("#valor").focus();
        return;
    }

    if(tipo==''){
        alert("Seleccione el Tipo de pedido!");
        loader('end');
        controlesOn_visor_asigna();
        $("#cmb_tipo").focus();
        return;
    }

    $("#cont_resultado_main").css("display",'none');
    $("#cont_grupo_asignacion").css("display",'block');



    $.post("modulos/gescom/asignacion.tecnicos.php?cmd=buscarPedido", {
        valor:valor,
        tipo:tipo,
        campo:campo
    }, function(data){
        //$("#cont_resultado_main").empty();
        //$("#cont_resultado_main").html(data);
        $("#lista_grupo_asignacion").empty();
        $("#lista_grupo_asignacion").html(data);
        controlesOn_visor_asigna();
        loader('end');
    });

}


function imprimirBoletasDespacho(){

	var chkVal = getCheckedBoxFrom("table", "tb_resultado", "1");
    var arrChkVal = chkVal.split(",");  
    var arrChk = new Array();
    var i=0;
    for(r=1;r<arrChkVal.length;r++){
        exp1 = arrChkVal[r].split("|");
        arrChk[i] = exp1[0]+"|"+exp1[1]+"|"+exp1[2]+"|"+exp1[3]+"|"+exp1[4]+"|"+exp1[7];
        i++;
    }
    
    if (arrChk.length>0){           
        $("#childModal").html("");                
        $.post("modulos/gescom/vistas/principal/boleta_eecc_masivo.php", {
            printAll : "Ok",
            seleccion : arrChk
        },function(data){            
            $("#childModal").html(data);
            var ficha = document.getElementById("printAllId");
  		    var ventimp = window.open(' ', 'popimpr',"_blank", "top=0, width=20,height=20,scrollbars=NO");
  		    ventimp.document.write( ficha.innerHTML );
  		    ventimp.document.close();
  		    ventimp.print( );
  		    ventimp.close();
        });
        $("#childModal").dialog({
            modal : true,
            width : '750px',
            maxHeight : '500px',
            hide : 'slide',
            position : 'top',
            title : "Imprimir boleta"
        });
    }else{
        alert("Debe seleccionar una o más actuaciones a imprimir!");
    }
}


function imprimirBoletasDespachoTmp(){

	        
    $("#childModal").html("");                
    $.post("printAreaModal.php", {
        printAll : "Ok"
    },function(data){            
        $("#childModal").html(data);
        setTimeout(function(){
    		$("div#myPrintAreaModal").printArea();	
    	},10000);
    });
    $("#childModal").dialog({
        modal : true,
        width : '750px',
        maxHeight : '500px',
        hide : 'slide',
        position : 'top',
        title : "Imprimir boleta"
    });
    
}

//var cont_img = 0;

function contador_imp_boleta_ind(){
	//alert("carga ok individual");
	setTimeout(function(){
		$("div#printAllId").printArea();	
	},5000);
    

	/*
    var ventana = window.open("", "", "");
	var contenido = "<html><body onload='window.print();window.close();'>";
	contenido = contenido + document.getElementById("printAllId").innerHTML + "</body></html>";
	ventana.document.open();
	ventana.document.write(contenido);
	ventana.document.close();
	*/
    
}

function contador_imp_boleta_mas(){
    cont_img++;
    //alert(cont_aver+" - "+cont_img);
    if(cont_aver==cont_img){
        $("div#printAllId").printArea();
    }
}